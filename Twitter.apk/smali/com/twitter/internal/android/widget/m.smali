.class Lcom/twitter/internal/android/widget/m;
.super Landroid/support/v4/widget/ViewDragHelper$Callback;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;


# direct methods
.method private constructor <init>(Lcom/twitter/internal/android/widget/HiddenDrawerLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/internal/android/widget/m;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    invoke-direct {p0}, Landroid/support/v4/widget/ViewDragHelper$Callback;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/internal/android/widget/HiddenDrawerLayout;Lcom/twitter/internal/android/widget/l;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/internal/android/widget/m;-><init>(Lcom/twitter/internal/android/widget/HiddenDrawerLayout;)V

    return-void
.end method


# virtual methods
.method public clampViewPositionHorizontal(Landroid/view/View;II)I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/m;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    iget-object v1, v1, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-static {p2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public getViewHorizontalDragRange(Landroid/view/View;)I
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/m;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    iget-object v0, v0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    return v0
.end method

.method public onViewCaptured(Landroid/view/View;I)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/internal/android/widget/m;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    iget v0, v0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->i:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/m;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->b()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/m;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    const/4 v1, 0x5

    iput v1, v0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->i:I

    return-void
.end method

.method public onViewDragStateChanged(I)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/internal/android/widget/m;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    iget-object v0, v0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->h:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v0}, Landroid/support/v4/widget/ViewDragHelper;->getViewDragState()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/m;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    iget-object v0, v0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/m;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->c()V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/m;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    const/4 v1, 0x4

    iput v1, v0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->i:I

    iget-object v0, p0, Lcom/twitter/internal/android/widget/m;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    iget-object v0, v0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->e:Lcom/twitter/internal/android/widget/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/m;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    iget-object v0, v0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->e:Lcom/twitter/internal/android/widget/n;

    invoke-interface {v0}, Lcom/twitter/internal/android/widget/n;->b()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/internal/android/widget/m;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    const/4 v1, 0x3

    iput v1, v0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->i:I

    iget-object v0, p0, Lcom/twitter/internal/android/widget/m;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    iget-object v0, v0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->e:Lcom/twitter/internal/android/widget/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/m;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    iget-object v0, v0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->e:Lcom/twitter/internal/android/widget/n;

    invoke-interface {v0}, Lcom/twitter/internal/android/widget/n;->a()V

    goto :goto_0
.end method

.method public onViewPositionChanged(Landroid/view/View;IIII)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/internal/android/widget/m;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    iget-object v0, v0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/RectLayoutParams;

    if-eqz v0, :cond_0

    iget v1, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->a:I

    add-int/2addr v1, p4

    iput v1, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->a:I

    iget v1, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->c:I

    add-int/2addr v1, p4

    iput v1, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->c:I

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/m;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->h()V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/m;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->invalidate()V

    return-void
.end method

.method public onViewReleased(Landroid/view/View;FF)V
    .locals 3

    const/4 v1, 0x0

    const/4 v2, 0x0

    cmpl-float v0, p2, v1

    if-gtz v0, :cond_0

    cmpl-float v0, p2, v1

    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/m;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    iget-object v1, v1, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    if-lt v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/m;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    const/4 v1, 0x1

    iput v1, v0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->i:I

    iget-object v0, p0, Lcom/twitter/internal/android/widget/m;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    iget-object v0, v0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->h:Landroid/support/v4/widget/ViewDragHelper;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/m;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    iget-object v1, v1, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/ViewDragHelper;->settleCapturedViewAt(II)Z

    :goto_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/m;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->invalidate()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/internal/android/widget/m;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    const/4 v1, 0x2

    iput v1, v0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->i:I

    iget-object v0, p0, Lcom/twitter/internal/android/widget/m;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    iget-object v0, v0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->h:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v0, v2, v2}, Landroid/support/v4/widget/ViewDragHelper;->settleCapturedViewAt(II)Z

    goto :goto_0
.end method

.method public tryCaptureView(Landroid/view/View;I)Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/m;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    iget-boolean v0, v0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->k:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/m;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    iget-boolean v0, v0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->j:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/m;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    iget-object v0, v0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->d:Landroid/view/View;

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
