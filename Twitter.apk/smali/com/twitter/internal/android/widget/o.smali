.class Lcom/twitter/internal/android/widget/o;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

.field private final b:Landroid/support/v4/widget/ScrollerCompat;

.field private final c:Landroid/support/v4/widget/ScrollerCompat;

.field private d:Landroid/support/v4/widget/ScrollerCompat;

.field private e:I

.field private f:I


# direct methods
.method constructor <init>(Lcom/twitter/internal/android/widget/HiddenDrawerLayout;Landroid/content/Context;Landroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/internal/android/widget/o;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p3, :cond_0

    if-nez p4, :cond_0

    invoke-static {p2}, Landroid/support/v4/widget/ScrollerCompat;->create(Landroid/content/Context;)Landroid/support/v4/widget/ScrollerCompat;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/o;->c:Landroid/support/v4/widget/ScrollerCompat;

    iput-object v0, p0, Lcom/twitter/internal/android/widget/o;->b:Landroid/support/v4/widget/ScrollerCompat;

    :goto_0
    const/16 v0, 0xc8

    iput v0, p0, Lcom/twitter/internal/android/widget/o;->f:I

    return-void

    :cond_0
    if-eqz p3, :cond_1

    invoke-static {p2, p3}, Landroid/support/v4/widget/ScrollerCompat;->create(Landroid/content/Context;Landroid/view/animation/Interpolator;)Landroid/support/v4/widget/ScrollerCompat;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/o;->b:Landroid/support/v4/widget/ScrollerCompat;

    :goto_1
    if-eqz p3, :cond_2

    invoke-static {p2, p4}, Landroid/support/v4/widget/ScrollerCompat;->create(Landroid/content/Context;Landroid/view/animation/Interpolator;)Landroid/support/v4/widget/ScrollerCompat;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/o;->c:Landroid/support/v4/widget/ScrollerCompat;

    goto :goto_0

    :cond_1
    invoke-static {p2}, Landroid/support/v4/widget/ScrollerCompat;->create(Landroid/content/Context;)Landroid/support/v4/widget/ScrollerCompat;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/o;->b:Landroid/support/v4/widget/ScrollerCompat;

    goto :goto_1

    :cond_2
    invoke-static {p2}, Landroid/support/v4/widget/ScrollerCompat;->create(Landroid/content/Context;)Landroid/support/v4/widget/ScrollerCompat;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/o;->c:Landroid/support/v4/widget/ScrollerCompat;

    goto :goto_0
.end method

.method private a(II)V
    .locals 6

    const/4 v1, 0x0

    if-lez p2, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/o;->d:Landroid/support/v4/widget/ScrollerCompat;

    move v2, v1

    move v3, v1

    move v4, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/widget/ScrollerCompat;->startScroll(IIIII)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/o;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void

    :cond_0
    if-lez p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    if-nez v1, :cond_2

    invoke-direct {p0, p1}, Lcom/twitter/internal/android/widget/o;->d(I)V

    :cond_2
    invoke-direct {p0, v1}, Lcom/twitter/internal/android/widget/o;->a(Z)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/o;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->invalidate()V

    goto :goto_0
.end method

.method private a(Z)V
    .locals 2

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/internal/android/widget/o;->e:I

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/o;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    const/4 v1, 0x0

    iput v1, v0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->f:F

    iget-object v0, p0, Lcom/twitter/internal/android/widget/o;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    const/4 v1, 0x4

    iput v1, v0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->i:I

    iget-object v0, p0, Lcom/twitter/internal/android/widget/o;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->a()V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/o;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->c()V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/o;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    iget-object v0, v0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->e:Lcom/twitter/internal/android/widget/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/o;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    iget-object v0, v0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->e:Lcom/twitter/internal/android/widget/n;

    invoke-interface {v0}, Lcom/twitter/internal/android/widget/n;->b()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/internal/android/widget/o;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->h()V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/o;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    const/4 v1, 0x3

    iput v1, v0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->i:I

    iget-object v0, p0, Lcom/twitter/internal/android/widget/o;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    iget-object v0, v0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->e:Lcom/twitter/internal/android/widget/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/o;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    iget-object v0, v0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->e:Lcom/twitter/internal/android/widget/n;

    invoke-interface {v0}, Lcom/twitter/internal/android/widget/n;->a()V

    goto :goto_0
.end method

.method private d(I)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/internal/android/widget/o;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    iget-object v0, v0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/RectLayoutParams;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/o;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    iget-boolean v1, v1, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->b:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/internal/android/widget/o;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    iget v1, v1, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->a:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    neg-int p1, p1

    :cond_0
    iget v1, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->b:I

    add-int/2addr v1, p1

    iput v1, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->b:I

    iget v1, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->d:I

    add-int/2addr v1, p1

    iput v1, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->d:I

    iget-object v0, p0, Lcom/twitter/internal/android/widget/o;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    iget-object v0, v0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->d:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->offsetTopAndBottom(I)V

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/twitter/internal/android/widget/o;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    iget v1, v1, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->a:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    neg-int p1, p1

    :cond_2
    iget v1, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->a:I

    add-int/2addr v1, p1

    iput v1, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->a:I

    iget v1, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->c:I

    add-int/2addr v1, p1

    iput v1, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->c:I

    iget-object v0, p0, Lcom/twitter/internal/android/widget/o;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    iget-object v0, v0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->d:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->offsetLeftAndRight(I)V

    goto :goto_0
.end method


# virtual methods
.method a(I)V
    .locals 0

    iput p1, p0, Lcom/twitter/internal/android/widget/o;->f:I

    return-void
.end method

.method b(I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/o;->b:Landroid/support/v4/widget/ScrollerCompat;

    iput-object v0, p0, Lcom/twitter/internal/android/widget/o;->d:Landroid/support/v4/widget/ScrollerCompat;

    iget v0, p0, Lcom/twitter/internal/android/widget/o;->f:I

    neg-int v0, v0

    invoke-direct {p0, v0, p1}, Lcom/twitter/internal/android/widget/o;->a(II)V

    return-void
.end method

.method c(I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/o;->c:Landroid/support/v4/widget/ScrollerCompat;

    iput-object v0, p0, Lcom/twitter/internal/android/widget/o;->d:Landroid/support/v4/widget/ScrollerCompat;

    iget v0, p0, Lcom/twitter/internal/android/widget/o;->f:I

    invoke-direct {p0, v0, p1}, Lcom/twitter/internal/android/widget/o;->a(II)V

    return-void
.end method

.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/internal/android/widget/o;->d:Landroid/support/v4/widget/ScrollerCompat;

    invoke-virtual {v0}, Landroid/support/v4/widget/ScrollerCompat;->computeScrollOffset()Z

    move-result v1

    invoke-virtual {v0}, Landroid/support/v4/widget/ScrollerCompat;->getCurrY()I

    move-result v2

    iget v0, p0, Lcom/twitter/internal/android/widget/o;->e:I

    sub-int v3, v0, v2

    if-lez v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v3}, Lcom/twitter/internal/android/widget/o;->d(I)V

    if-eqz v1, :cond_1

    iput v2, p0, Lcom/twitter/internal/android/widget/o;->e:I

    iget-object v0, p0, Lcom/twitter/internal/android/widget/o;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->h()V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/o;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->post(Ljava/lang/Runnable;)Z

    :goto_1
    iget-object v0, p0, Lcom/twitter/internal/android/widget/o;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->invalidate()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-direct {p0, v0}, Lcom/twitter/internal/android/widget/o;->a(Z)V

    goto :goto_1
.end method
