.class public Lcom/twitter/internal/android/widget/ToolBar;
.super Landroid/view/ViewGroup;
.source "Twttr"


# instance fields
.field private A:Z

.field private B:Landroid/widget/ImageView;

.field private C:Landroid/widget/PopupWindow;

.field private D:I

.field private E:Landroid/graphics/Rect;

.field private F:I

.field private G:Landroid/view/ViewGroup;

.field private H:Landroid/view/View;

.field private I:Z

.field private J:I

.field private K:Z

.field private final a:I

.field private final b:I

.field private final c:Z

.field private final d:Landroid/graphics/drawable/Drawable;

.field private final e:I

.field private final f:I

.field private final g:Lcom/twitter/internal/android/widget/ar;

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:Ljava/util/List;

.field private final m:Lcom/twitter/internal/android/widget/at;

.field private final n:Lcom/twitter/internal/android/widget/aq;

.field private final o:Lcom/twitter/internal/android/widget/au;

.field private final p:Ljava/lang/String;

.field private q:Lcom/twitter/internal/android/widget/as;

.field private r:Ljava/util/LinkedHashMap;

.field private s:Ljava/util/ArrayList;

.field private t:Ljava/util/ArrayList;

.field private u:Ljava/util/ArrayList;

.field private v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

.field private w:Z

.field private x:Z

.field private y:I

.field private z:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget v0, Lcom/twitter/internal/android/b;->toolBarStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/internal/android/widget/ToolBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    const/4 v4, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->r:Ljava/util/LinkedHashMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->s:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->t:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->u:Ljava/util/ArrayList;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->E:Landroid/graphics/Rect;

    sget-object v0, Lcom/twitter/internal/android/f;->ToolBar:[I

    invoke-virtual {p1, p2, v0, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->a:I

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->b:I

    const/16 v1, 0xd

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->c:Z

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->d:Landroid/graphics/drawable/Drawable;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->p:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->h:I

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->i:I

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setTitle(Ljava/lang/CharSequence;)V

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setIcon(Landroid/graphics/drawable/Drawable;)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setUpIndicator(Landroid/graphics/drawable/Drawable;)V

    invoke-super {p0, v4, v4, v4, v4}, Landroid/view/ViewGroup;->setPadding(IIII)V

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/twitter/internal/android/c;->preferred_popup_width:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->f:I

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->f:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->e:I

    new-instance v1, Lcom/twitter/internal/android/widget/at;

    invoke-direct {v1}, Lcom/twitter/internal/android/widget/at;-><init>()V

    iput-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->m:Lcom/twitter/internal/android/widget/at;

    new-instance v1, Lcom/twitter/internal/android/widget/au;

    invoke-direct {v1}, Lcom/twitter/internal/android/widget/au;-><init>()V

    iput-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->o:Lcom/twitter/internal/android/widget/au;

    new-instance v1, Lcom/twitter/internal/android/widget/aq;

    invoke-direct {v1}, Lcom/twitter/internal/android/widget/aq;-><init>()V

    iput-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->n:Lcom/twitter/internal/android/widget/aq;

    new-instance v1, Lcom/twitter/internal/android/widget/ar;

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->m:Lcom/twitter/internal/android/widget/at;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/twitter/internal/android/widget/ar;-><init>(Lcom/twitter/internal/android/widget/at;Lcom/twitter/internal/android/widget/aj;)V

    iput-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->g:Lcom/twitter/internal/android/widget/ar;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->l:Ljava/util/List;

    const/16 v1, 0xc

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->J:I

    invoke-virtual {v0, v4, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->j:I

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->k:I

    const/16 v1, 0xa

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->F:I

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBar;->h()V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private a(Landroid/widget/ListAdapter;)I
    .locals 10

    const/4 v2, 0x0

    const/4 v0, 0x0

    invoke-interface {p1}, Landroid/widget/ListAdapter;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-interface {p1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v8

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->G:Landroid/view/ViewGroup;

    if-nez v1, :cond_1

    new-instance v1, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->G:Landroid/view/ViewGroup;

    :cond_1
    move v5, v0

    move v1, v0

    move-object v3, v2

    move v4, v0

    :goto_1
    if-ge v5, v8, :cond_2

    invoke-interface {p1, v5}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v0

    if-eq v0, v1, :cond_3

    move-object v1, v2

    :goto_2
    iget-object v3, p0, Lcom/twitter/internal/android/widget/ToolBar;->G:Landroid/view/ViewGroup;

    invoke-interface {p1, v5, v1, v3}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6, v7}, Landroid/view/View;->measure(II)V

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v3}, Landroid/view/View;->getPaddingLeft()I

    move-result v9

    add-int/2addr v1, v9

    invoke-virtual {v3}, Landroid/view/View;->getPaddingRight()I

    move-result v9

    add-int/2addr v1, v9

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v4

    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move v1, v0

    goto :goto_1

    :cond_2
    move v0, v4

    goto :goto_0

    :cond_3
    move v0, v1

    move-object v1, v3

    goto :goto_2
.end method

.method static synthetic a(Lcom/twitter/internal/android/widget/ToolBar;)Lcom/twitter/internal/android/widget/ToolBarHomeView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    return-object v0
.end method

.method static a(Ljava/util/Collection;Lcom/twitter/internal/android/widget/at;)Ljava/util/ArrayList;
    .locals 10

    const/4 v3, 0x0

    const/4 v9, -0x1

    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0, p1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    const/4 v0, 0x0

    move-object v1, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhn;

    invoke-virtual {v0}, Lhn;->p()I

    move-result v7

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lhn;->p()I

    move-result v1

    :goto_1
    if-eq v7, v9, :cond_6

    sub-int v1, v7, v1

    add-int/lit8 v2, v1, -0x1

    :goto_2
    if-gtz v2, :cond_0

    if-ne v7, v9, :cond_2

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhn;

    invoke-virtual {v1}, Lhn;->p()I

    move-result v8

    if-ne v8, v9, :cond_5

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v2, -0x1

    :goto_3
    move v2, v1

    goto :goto_2

    :cond_1
    move v1, v3

    goto :goto_1

    :cond_2
    if-eq v7, v9, :cond_3

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    move-object v1, v0

    goto :goto_0

    :cond_4
    return-object v4

    :cond_5
    move v1, v2

    goto :goto_3

    :cond_6
    move v2, v3

    goto :goto_2
.end method

.method private a(IIII)V
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->I:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->H:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;

    iget v0, v0, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;->a:I

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->H:Landroid/view/View;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/view/View;->layout(IIII)V

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->H:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->H:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int v1, p3, v1

    invoke-virtual {v0, v1, p2, p3, p4}, Landroid/view/View;->layout(IIII)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
    .end packed-switch
.end method

.method private a(Landroid/view/View;)V
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_0

    invoke-virtual {p0, p1}, Lcom/twitter/internal/android/widget/ToolBar;->detachViewFromParent(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method private a(Lhn;Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->l:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, p2}, Lcom/twitter/internal/android/widget/ToolBar;->a(Landroid/view/View;)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/internal/android/widget/ToolBar;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->u:Ljava/util/ArrayList;

    return-object v0
.end method

.method private b(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/twitter/internal/android/widget/ToolBar;)Lcom/twitter/internal/android/widget/as;
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->q:Lcom/twitter/internal/android/widget/as;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/internal/android/widget/ToolBar;)Landroid/widget/PopupWindow;
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->C:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method private g()Lcom/twitter/internal/android/widget/ToolBarHomeView;
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/twitter/internal/android/widget/ToolBarHomeView;

    invoke-direct {v1, v0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    iget v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->h:I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->h:I

    invoke-virtual {v1, v0, v2}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->a(Landroid/content/Context;I)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    sget v1, Lcom/twitter/internal/android/d;->home:I

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->setId(I)V

    new-instance v0, Lhn;

    invoke-direct {v0, p0, v3}, Lhn;-><init>(Lcom/twitter/internal/android/widget/ToolBar;Z)V

    sget v1, Lcom/twitter/internal/android/d;->home:I

    invoke-virtual {v0, v1}, Lhn;->a(I)Lhn;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    new-instance v2, Lcom/twitter/internal/android/widget/aj;

    invoke-direct {v2, p0, v0}, Lcom/twitter/internal/android/widget/aj;-><init>(Lcom/twitter/internal/android/widget/ToolBar;Lhn;)V

    invoke-virtual {v1, v2}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    new-instance v2, Lcom/twitter/internal/android/widget/ak;

    invoke-direct {v2, p0, v0}, Lcom/twitter/internal/android/widget/ak;-><init>(Lcom/twitter/internal/android/widget/ToolBar;Lhn;)V

    invoke-virtual {v1, v2}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    invoke-virtual {p0, v0, v3}, Lcom/twitter/internal/android/widget/ToolBar;->addView(Landroid/view/View;I)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->w:Z

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->setClickable(Z)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->w:Z

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->setLongClickable(Z)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->w:Z

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->a(Z)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    return-object v0
.end method

.method private getStartIndex()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private h()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v3, p0, Lcom/twitter/internal/android/widget/ToolBar;->J:I

    and-int/lit8 v0, v3, 0x4

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setDisplayShowHomeAsUpEnabled(Z)V

    and-int/lit8 v0, v3, 0x2

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-direct {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setDisplayHomeEnabled(Z)V

    and-int/lit8 v0, v3, 0x8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setDisplayShowTitleEnabled(Z)V

    and-int/lit8 v0, v3, 0x10

    if-eqz v0, :cond_3

    :goto_3
    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setDisplayFullExpandEnabled(Z)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3
.end method

.method private setDisplayHomeEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->c(Z)V

    :cond_0
    return-void
.end method

.method private setExtraWidth(I)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->K:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBar;->g()Lcom/twitter/internal/android/widget/ToolBarHomeView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->a(I)V

    goto :goto_0
.end method

.method private setUpIndicator(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBar;->g()Lcom/twitter/internal/android/widget/ToolBarHomeView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->b(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method


# virtual methods
.method a(Lhn;)Landroid/view/View;
    .locals 2

    invoke-virtual {p1}, Lhn;->j()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lhn;->e()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lhn;->e()Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lhn;->d()Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public a(I)Lhn;
    .locals 2

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->r:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhn;

    return-object v0
.end method

.method a()V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->C:Landroid/widget/PopupWindow;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/twitter/internal/android/widget/DropDownListView;

    sget v0, Lcom/twitter/internal/android/b;->dropDownListViewStyle:I

    invoke-direct {v2, v1, v6, v0}, Lcom/twitter/internal/android/widget/DropDownListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {v2, v4}, Lcom/twitter/internal/android/widget/DropDownListView;->setFocusable(Z)V

    invoke-virtual {v2, v4}, Lcom/twitter/internal/android/widget/DropDownListView;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->g:Lcom/twitter/internal/android/widget/ar;

    invoke-virtual {v2, v0}, Lcom/twitter/internal/android/widget/DropDownListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v0, Lcom/twitter/internal/android/widget/am;

    invoke-direct {v0, p0}, Lcom/twitter/internal/android/widget/am;-><init>(Lcom/twitter/internal/android/widget/ToolBar;)V

    invoke-virtual {v2, v0}, Lcom/twitter/internal/android/widget/DropDownListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    const/4 v0, -0x1

    invoke-virtual {v2, v0}, Lcom/twitter/internal/android/widget/DropDownListView;->setSelection(I)V

    new-instance v0, Lcom/twitter/internal/android/widget/an;

    invoke-direct {v0, p0}, Lcom/twitter/internal/android/widget/an;-><init>(Lcom/twitter/internal/android/widget/ToolBar;)V

    invoke-virtual {v2, v0}, Lcom/twitter/internal/android/widget/DropDownListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v0, v3, :cond_1

    new-instance v0, Landroid/widget/PopupWindow;

    sget v3, Lcom/twitter/internal/android/b;->toolBarPopupWindowStyle:I

    invoke-direct {v0, v1, v6, v3}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    :goto_0
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    invoke-virtual {v0, v4}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    invoke-virtual {v0, v4}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->C:Landroid/widget/PopupWindow;

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->g:Lcom/twitter/internal/android/widget/ar;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->l:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ar;->a(Ljava/util/List;)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->C:Landroid/widget/PopupWindow;

    const/4 v1, -0x2

    invoke-virtual {v0, v5, v1}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->g:Lcom/twitter/internal/android/widget/ar;

    invoke-direct {p0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(Landroid/widget/ListAdapter;)I

    move-result v1

    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->e:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->f:I

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setContentWidth(I)V

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->B:Landroid/widget/ImageView;

    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->j:I

    iget v3, p0, Lcom/twitter/internal/android/widget/ToolBar;->k:I

    iget v4, p0, Lcom/twitter/internal/android/widget/ToolBar;->D:I

    invoke-virtual/range {v0 .. v5}, Landroid/widget/PopupWindow;->update(Landroid/view/View;IIII)V

    :goto_1
    return-void

    :cond_1
    new-instance v0, Landroid/widget/PopupWindow;

    invoke-direct {v0, v1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->D:I

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setWidth(I)V

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->B:Landroid/widget/ImageView;

    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->j:I

    iget v3, p0, Lcom/twitter/internal/android/widget/ToolBar;->k:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    goto :goto_1
.end method

.method protected a(IIIIZ)V
    .locals 10

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->u:Ljava/util/ArrayList;

    if-eqz p5, :cond_5

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v2, v1

    move v3, p1

    move v1, v0

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhn;

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(Lhn;)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    sub-int v6, p3, v5

    if-ge v6, v3, :cond_0

    const/4 v4, 0x1

    :goto_1
    invoke-virtual {v0}, Lhn;->j()I

    move-result v9

    if-eqz v4, :cond_1

    and-int/lit8 v4, v9, 0x2

    if-nez v4, :cond_1

    invoke-direct {p0, v0, v8}, Lcom/twitter/internal/android/widget/ToolBar;->a(Lhn;Landroid/view/View;)V

    move v0, v1

    move v1, v2

    move v2, v3

    :goto_2
    move v3, v2

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Lhn;->i()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-direct {p0, v8}, Lcom/twitter/internal/android/widget/ToolBar;->b(Landroid/view/View;)V

    invoke-virtual {v0}, Lhn;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    if-nez v2, :cond_2

    invoke-virtual {v8, p1, p2, v5, p4}, Landroid/view/View;->layout(IIII)V

    const/4 v2, 0x1

    move v3, v5

    :goto_3
    add-int/2addr v1, v5

    move v0, v1

    move v1, v2

    move v2, v3

    goto :goto_2

    :cond_2
    invoke-virtual {v8, v6, p2, p3, p4}, Landroid/view/View;->layout(IIII)V

    move p3, v6

    goto :goto_3

    :cond_3
    invoke-direct {p0, v8}, Lcom/twitter/internal/android/widget/ToolBar;->a(Landroid/view/View;)V

    move v0, v1

    move v1, v2

    move v2, v3

    goto :goto_2

    :cond_4
    invoke-direct {p0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setExtraWidth(I)V

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/internal/android/widget/ToolBar;->a(IIII)V

    :goto_4
    return-void

    :cond_5
    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v1

    move v1, v0

    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhn;

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(Lhn;)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    add-int v4, p1, v7

    if-le v4, p3, :cond_6

    const/4 v3, 0x1

    :goto_6
    invoke-virtual {v0}, Lhn;->j()I

    move-result v8

    if-eqz v3, :cond_7

    and-int/lit8 v3, v8, 0x2

    if-nez v3, :cond_7

    invoke-direct {p0, v0, v6}, Lcom/twitter/internal/android/widget/ToolBar;->a(Lhn;Landroid/view/View;)V

    move v0, v1

    move v1, v2

    :goto_7
    move v2, v1

    move v1, v0

    goto :goto_5

    :cond_6
    const/4 v3, 0x0

    goto :goto_6

    :cond_7
    invoke-virtual {v0}, Lhn;->i()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-direct {p0, v6}, Lcom/twitter/internal/android/widget/ToolBar;->b(Landroid/view/View;)V

    invoke-virtual {v0}, Lhn;->b()Z

    move-result v0

    if-eqz v0, :cond_8

    if-nez v2, :cond_8

    sub-int v0, p3, v7

    invoke-virtual {v6, v0, p2, p3, p4}, Landroid/view/View;->layout(IIII)V

    const/4 v2, 0x1

    move p3, v0

    :goto_8
    add-int/2addr v1, v7

    move v0, v1

    move v1, v2

    goto :goto_7

    :cond_8
    invoke-virtual {v6, p1, p2, v4, p4}, Landroid/view/View;->layout(IIII)V

    move p1, v4

    goto :goto_8

    :cond_9
    invoke-direct {p0, v6}, Lcom/twitter/internal/android/widget/ToolBar;->a(Landroid/view/View;)V

    move v0, v1

    move v1, v2

    goto :goto_7

    :cond_a
    invoke-direct {p0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setExtraWidth(I)V

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/internal/android/widget/ToolBar;->a(IIII)V

    goto :goto_4
.end method

.method public a(Landroid/view/View;Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;)V
    .locals 4

    const/4 v3, -0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->H:Landroid/view/View;

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->H:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-ne v2, p0, :cond_5

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->H:Landroid/view/View;

    invoke-virtual {p0, v2}, Lcom/twitter/internal/android/widget/ToolBar;->removeView(Landroid/view/View;)V

    :goto_1
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->H:Landroid/view/View;

    :cond_0
    if-eqz p1, :cond_1

    if-eqz p2, :cond_6

    invoke-virtual {p1, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    :goto_2
    iput-object p1, p0, Lcom/twitter/internal/android/widget/ToolBar;->H:Landroid/view/View;

    iput-boolean v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->I:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->H:Landroid/view/View;

    if-eqz v0, :cond_3

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->requestLayout()V

    :cond_3
    return-void

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->H:Landroid/view/View;

    invoke-virtual {p0, v2, v1}, Lcom/twitter/internal/android/widget/ToolBar;->removeDetachedView(Landroid/view/View;Z)V

    goto :goto_1

    :cond_6
    new-instance v2, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;

    invoke-direct {v2, v3, v3}, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2
.end method

.method public a(Ljava/util/Collection;)V
    .locals 11

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getChildCount()I

    move-result v0

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhn;

    invoke-virtual {v0}, Lhn;->a()I

    move-result v6

    invoke-virtual {v0}, Lhn;->j()I

    move-result v7

    and-int/lit8 v2, v7, 0x2

    if-eqz v2, :cond_4

    move v2, v3

    :goto_2
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getContext()Landroid/content/Context;

    move-result-object v8

    new-instance v9, Lcom/twitter/internal/android/widget/ToolBarItemView;

    invoke-direct {v9, v8}, Lcom/twitter/internal/android/widget/ToolBarItemView;-><init>(Landroid/content/Context;)V

    iget v10, p0, Lcom/twitter/internal/android/widget/ToolBar;->i:I

    if-eqz v10, :cond_1

    iget v10, p0, Lcom/twitter/internal/android/widget/ToolBar;->i:I

    invoke-virtual {v9, v8, v10}, Lcom/twitter/internal/android/widget/ToolBarItemView;->a(Landroid/content/Context;I)V

    :cond_1
    invoke-virtual {v9, v6}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setId(I)V

    invoke-virtual {v0}, Lhn;->c()I

    move-result v8

    invoke-virtual {v9, v8}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setImageResource(I)V

    invoke-virtual {v0}, Lhn;->k()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v9, v8}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setLabel(Ljava/lang/CharSequence;)V

    and-int/lit8 v7, v7, 0x4

    if-nez v7, :cond_2

    invoke-virtual {v0}, Lhn;->c()I

    move-result v7

    if-nez v7, :cond_5

    if-eqz v2, :cond_5

    :cond_2
    move v2, v3

    :goto_3
    invoke-virtual {v9, v2}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setWithText(Z)V

    invoke-virtual {v0}, Lhn;->l()Z

    move-result v2

    invoke-virtual {v9, v2}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setEnabled(Z)V

    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->a:I

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->a:I

    invoke-virtual {v9, v2}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setBackgroundResource(I)V

    :cond_3
    new-instance v2, Lcom/twitter/internal/android/widget/ao;

    invoke-direct {v2, p0, v0}, Lcom/twitter/internal/android/widget/ao;-><init>(Lcom/twitter/internal/android/widget/ToolBar;Lhn;)V

    invoke-virtual {v9, v2}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v2, Lcom/twitter/internal/android/widget/ap;

    invoke-direct {v2, p0, v0}, Lcom/twitter/internal/android/widget/ap;-><init>(Lcom/twitter/internal/android/widget/ToolBar;Lhn;)V

    invoke-virtual {v9, v2}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    invoke-virtual {v0, v9}, Lhn;->a(Lcom/twitter/internal/android/widget/ToolBarItemView;)Lhn;

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(Lhn;)Landroid/view/View;

    move-result-object v7

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    invoke-virtual {p0, v7, v1, v8, v3}, Lcom/twitter/internal/android/widget/ToolBar;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->r:Ljava/util/LinkedHashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v6, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v1, v2

    goto/16 :goto_1

    :cond_4
    move v2, v4

    goto :goto_2

    :cond_5
    move v2, v4

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->r:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->m:Lcom/twitter/internal/android/widget/at;

    invoke-static {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(Ljava/util/Collection;Lcom/twitter/internal/android/widget/at;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->s:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->m:Lcom/twitter/internal/android/widget/at;

    invoke-static {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(Ljava/util/Collection;Lcom/twitter/internal/android/widget/at;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->t:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->t:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->o:Lcom/twitter/internal/android/widget/au;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->t:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->n:Lcom/twitter/internal/android/widget/aq;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->requestLayout()V

    goto/16 :goto_0
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setVisibility(I)V

    return-void
.end method

.method public b(I)V
    .locals 1

    iget v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->J:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->J:I

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBar;->h()V

    return-void
.end method

.method public b(Lhn;)Z
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Landroid/view/View;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Landroid/view/View;

    invoke-virtual {p1}, Lhn;->e()Landroid/view/View;

    move-result-object v2

    if-eq v0, v2, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->removeView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getChildCount()I

    move-result v3

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getStartIndex()I

    move-result v0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_4

    invoke-virtual {p0, v2}, Lcom/twitter/internal/android/widget/ToolBar;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->r:Ljava/util/LinkedHashMap;

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhn;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lhn;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Landroid/view/View;

    invoke-virtual {p1, v1}, Lhn;->a(Z)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->x:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    iget v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->y:I

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->setVisibility(I)V

    :cond_5
    :goto_2
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->requestLayout()V

    const/4 v0, 0x1

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->d(Z)V

    goto :goto_2
.end method

.method public c()V
    .locals 1

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setVisibility(I)V

    return-void
.end method

.method public c(Lhn;)Z
    .locals 5

    const/16 v4, 0x8

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Landroid/view/View;

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lhn;->e()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Landroid/view/View;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getChildCount()I

    move-result v2

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getStartIndex()I

    move-result v1

    :goto_1
    if-ge v1, v2, :cond_1

    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->addView(Landroid/view/View;)V

    invoke-virtual {p1, v0}, Lhn;->a(Z)V

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->x:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    invoke-virtual {v1}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->getVisibility()I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->y:I

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    invoke-virtual {v1, v4}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->setVisibility(I)V

    :cond_2
    :goto_2
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->requestLayout()V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    invoke-virtual {v1, v0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->d(Z)V

    goto :goto_2
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    instance-of v0, p1, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(Lhn;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Landroid/view/View;

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/twitter/internal/android/widget/ToolBar;->a(Lhn;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lhn;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->A:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->a()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->C:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->C:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->C:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->C:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->g:Lcom/twitter/internal/android/widget/ar;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ar;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    const/4 v1, -0x2

    new-instance v0, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;

    invoke-direct {v0, v1, v1}, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    new-instance v0, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    new-instance v0, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;

    invoke-direct {v0, p1}, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public getDisplayOptions()I
    .locals 1

    iget v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->J:I

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->e()Z

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    iget v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->F:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->F:I

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;

    invoke-virtual {p0, v1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(Landroid/view/View;Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    const/4 v2, 0x0

    sub-int v4, p5, p3

    sub-int v3, p4, p2

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_9

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->getMeasuredWidth()I

    move-result v1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    invoke-virtual {v0, v2, v2, v1, v4}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->layout(IIII)V

    :goto_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Landroid/view/View;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->B:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->A:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->B:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v0

    iget-object v5, p0, Lcom/twitter/internal/android/widget/ToolBar;->B:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/twitter/internal/android/widget/ToolBar;->B:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getChildCount()I

    move-result v6

    iget-object v7, p0, Lcom/twitter/internal/android/widget/ToolBar;->B:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {p0, v5, v6, v7, v8}, Lcom/twitter/internal/android/widget/ToolBar;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    :cond_1
    iget-object v5, p0, Lcom/twitter/internal/android/widget/ToolBar;->B:Landroid/widget/ImageView;

    sub-int v6, v3, v0

    invoke-virtual {v5, v6, v2, v3, v4}, Landroid/widget/ImageView;->layout(IIII)V

    sub-int/2addr v3, v0

    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->K:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->t:Ljava/util/ArrayList;

    :goto_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhn;

    invoke-virtual {v0}, Lhn;->j()I

    move-result v6

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(Lhn;)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v0}, Lhn;->c()I

    move-result v8

    if-nez v8, :cond_3

    and-int/lit8 v8, v6, 0x2

    if-eqz v8, :cond_4

    :cond_3
    if-nez v6, :cond_7

    :cond_4
    invoke-direct {p0, v0, v7}, Lcom/twitter/internal/android/widget/ToolBar;->a(Lhn;Landroid/view/View;)V

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->B:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(Landroid/view/View;)V

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->s:Ljava/util/ArrayList;

    goto :goto_3

    :cond_7
    iget-object v6, p0, Lcom/twitter/internal/android/widget/ToolBar;->u:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_8
    iget-boolean v5, p0, Lcom/twitter/internal/android/widget/ToolBar;->c:Z

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/internal/android/widget/ToolBar;->a(IIIIZ)V

    goto :goto_1

    :cond_9
    move v1, v2

    goto/16 :goto_0
.end method

.method protected onMeasure(II)V
    .locals 13

    const/16 v12, 0x8

    const/4 v5, 0x1

    const/4 v1, 0x0

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getMeasuredHeight()I

    move-result v0

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getMeasuredWidth()I

    move-result v2

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->getVisibility()I

    move-result v0

    if-eq v0, v12, :cond_1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    invoke-static {v2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v0, v3, v6}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->measure(II)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->getMeasuredWidth()I

    move-result v0

    :goto_0
    iget-object v3, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Landroid/view/View;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Landroid/view/View;

    sub-int v0, v2, v0

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v3, v0, v6}, Landroid/view/View;->measure(II)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    sub-int v0, v2, v0

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->r:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v2, v1

    move v3, v0

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhn;

    invoke-virtual {v0}, Lhn;->j()I

    move-result v9

    and-int/lit8 v4, v9, 0x2

    if-eqz v4, :cond_4

    move v4, v5

    :goto_3
    invoke-virtual {v0}, Lhn;->i()Z

    move-result v10

    if-eqz v10, :cond_5

    if-eqz v9, :cond_3

    invoke-virtual {v0}, Lhn;->c()I

    move-result v11

    if-nez v11, :cond_5

    if-nez v4, :cond_5

    :cond_3
    move v4, v5

    :goto_4
    or-int/2addr v2, v4

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(Lhn;)Landroid/view/View;

    move-result-object v0

    if-eqz v9, :cond_12

    if-eqz v10, :cond_12

    iget v4, p0, Lcom/twitter/internal/android/widget/ToolBar;->b:I

    iget v9, p0, Lcom/twitter/internal/android/widget/ToolBar;->b:I

    invoke-virtual {v0, v4, v1, v9, v1}, Landroid/view/View;->setPadding(IIII)V

    invoke-virtual {v0, v7, v6}, Landroid/view/View;->measure(II)V

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    sub-int/2addr v3, v0

    move v0, v3

    :goto_5
    move v3, v0

    goto :goto_2

    :cond_4
    move v4, v1

    goto :goto_3

    :cond_5
    move v4, v1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->H:Landroid/view/View;

    if-eqz v0, :cond_8

    if-lez v3, :cond_d

    move v0, v5

    :goto_6
    iput-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->I:Z

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->I:Z

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->H:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->H:Landroid/view/View;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getChildCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    iget-object v8, p0, Lcom/twitter/internal/android/widget/ToolBar;->H:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    invoke-virtual {p0, v0, v4, v8}, Lcom/twitter/internal/android/widget/ToolBar;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    :cond_7
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->H:Landroid/view/View;

    const/high16 v4, -0x80000000

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v0, v3, v6}, Landroid/view/View;->measure(II)V

    move v3, v1

    :cond_8
    :goto_7
    if-gez v3, :cond_f

    move v0, v5

    :goto_8
    iput-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->K:Z

    if-nez v2, :cond_9

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->K:Z

    if-eqz v0, :cond_10

    :cond_9
    :goto_9
    iput-boolean v5, p0, Lcom/twitter/internal/android/widget/ToolBar;->A:Z

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->A:Z

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->B:Landroid/widget/ImageView;

    if-nez v0, :cond_c

    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->a:I

    if-eqz v2, :cond_a

    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->a:I

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :cond_a
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    new-instance v2, Lcom/twitter/internal/android/widget/al;

    invoke-direct {v2, p0}, Lcom/twitter/internal/android/widget/al;-><init>(Lcom/twitter/internal/android/widget/ToolBar;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v2, Lcom/twitter/internal/android/d;->overflow:I

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setId(I)V

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBar;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->b:I

    iget v3, p0, Lcom/twitter/internal/android/widget/ToolBar;->b:I

    invoke-virtual {v0, v2, v1, v3, v1}, Landroid/widget/ImageView;->setPadding(IIII)V

    invoke-virtual {v0, v7, v6}, Landroid/widget/ImageView;->measure(II)V

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->p:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBar;->p:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_b
    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->B:Landroid/widget/ImageView;

    :cond_c
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->B:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->B:Landroid/widget/ImageView;

    invoke-virtual {v0, v7, v6}, Landroid/widget/ImageView;->measure(II)V

    goto/16 :goto_1

    :cond_d
    move v0, v1

    goto/16 :goto_6

    :cond_e
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->H:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(Landroid/view/View;)V

    goto :goto_7

    :cond_f
    move v0, v1

    goto :goto_8

    :cond_10
    move v5, v1

    goto :goto_9

    :cond_11
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->B:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->B:Landroid/widget/ImageView;

    invoke-virtual {v0, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    :cond_12
    move v0, v3

    goto/16 :goto_5
.end method

.method public setContentWidth(I)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->C:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->E:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->E:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->E:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    add-int/2addr v0, p1

    iput v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->D:I

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Lcom/twitter/internal/android/widget/ToolBar;->D:I

    goto :goto_0
.end method

.method public setCustomView(Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(Landroid/view/View;Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;)V

    return-void
.end method

.method public setDisplayFullExpandEnabled(Z)V
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->x:Z

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->z:Landroid/view/View;

    if-eqz v1, :cond_1

    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    invoke-virtual {v1}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->getVisibility()I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->y:I

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->d(Z)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->setVisibility(I)V

    :cond_1
    :goto_1
    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/ToolBar;->x:Z

    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/twitter/internal/android/widget/ToolBar;->y:I

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->setVisibility(I)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->d(Z)V

    goto :goto_1
.end method

.method public setDisplayOptions(I)V
    .locals 0

    iput p1, p0, Lcom/twitter/internal/android/widget/ToolBar;->J:I

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBar;->h()V

    return-void
.end method

.method public setDisplayShowHomeAsUpEnabled(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->w:Z

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->a(Z)V

    :cond_1
    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/ToolBar;->w:Z

    goto :goto_0
.end method

.method public setDisplayShowTitleEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->b(Z)V

    :cond_0
    return-void
.end method

.method public final setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBar;->g()Lcom/twitter/internal/android/widget/ToolBarHomeView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public setNumber(I)V
    .locals 1

    if-gtz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBar;->g()Lcom/twitter/internal/android/widget/ToolBarHomeView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->b(I)V

    goto :goto_0
.end method

.method public setOnToolBarItemSelectedListener(Lcom/twitter/internal/android/widget/as;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/internal/android/widget/ToolBar;->q:Lcom/twitter/internal/android/widget/as;

    return-void
.end method

.method public final setPadding(IIII)V
    .locals 0

    return-void
.end method

.method public final setSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->b(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBar;->g()Lcom/twitter/internal/android/widget/ToolBarHomeView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final setTitleDescription(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBar;->v:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->c(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
