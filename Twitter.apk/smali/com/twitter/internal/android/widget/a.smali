.class Lcom/twitter/internal/android/widget/a;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/view/View;

.field private final b:I

.field private final c:I

.field private final d:F

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:Landroid/graphics/drawable/Drawable;

.field private i:I

.field private j:I

.field private k:Lcom/twitter/internal/android/widget/ai;

.field private l:F

.field private m:Landroid/animation/Animator;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/content/Context;I)V
    .locals 5

    const/4 v4, -0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/twitter/internal/android/widget/a;->l:F

    iput-object p1, p0, Lcom/twitter/internal/android/widget/a;->a:Landroid/view/View;

    sget-object v0, Lcom/twitter/internal/android/f;->BadgeIndicator:[I

    invoke-virtual {p2, p3, v0}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/a;->b:I

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/a;->c:I

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/a;->d:F

    const/4 v1, 0x6

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/a;->f:I

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/a;->e:I

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/a;->g:I

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/internal/android/widget/a;->h:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x7

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/a;->i:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private a(FFJLandroid/view/animation/Interpolator;Landroid/animation/Animator$AnimatorListener;)Landroid/animation/Animator;
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput p1, v0, v1

    const/4 v1, 0x1

    aput p2, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p5}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v0, p3, p4}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/twitter/internal/android/widget/b;

    invoke-direct {v1, p0}, Lcom/twitter/internal/android/widget/b;-><init>(Lcom/twitter/internal/android/widget/a;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    invoke-virtual {v0, p6}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/internal/android/widget/a;Landroid/animation/Animator;)Landroid/animation/Animator;
    .locals 0

    iput-object p1, p0, Lcom/twitter/internal/android/widget/a;->m:Landroid/animation/Animator;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/internal/android/widget/a;Lcom/twitter/internal/android/widget/ai;)Lcom/twitter/internal/android/widget/ai;
    .locals 0

    iput-object p1, p0, Lcom/twitter/internal/android/widget/a;->k:Lcom/twitter/internal/android/widget/ai;

    return-object p1
.end method

.method private d()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    iget-object v0, p0, Lcom/twitter/internal/android/widget/a;->m:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/a;->m:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/a;->m:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    :cond_0
    return-void
.end method


# virtual methods
.method a(F)V
    .locals 1

    iput p1, p0, Lcom/twitter/internal/android/widget/a;->l:F

    iget-object v0, p0, Lcom/twitter/internal/android/widget/a;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/twitter/internal/android/widget/a;->i:I

    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    const/4 v2, 0x1

    iget v0, p0, Lcom/twitter/internal/android/widget/a;->i:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/twitter/internal/android/widget/a;->k:Lcom/twitter/internal/android/widget/ai;

    if-eqz v0, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/a;->m:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/a;->m:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/a;->m:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/a;->k:Lcom/twitter/internal/android/widget/ai;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ai;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    iget v1, p0, Lcom/twitter/internal/android/widget/a;->l:F

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->save(I)I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v0

    invoke-virtual {p1, v1, v1, v3, v0}, Landroid/graphics/Canvas;->scale(FFFF)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/a;->k:Lcom/twitter/internal/android/widget/ai;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/ai;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v0, p0, Lcom/twitter/internal/android/widget/a;->i:I

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/a;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/internal/android/widget/a;->j:I

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/a;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public a(ZIIIILandroid/graphics/Rect;I)V
    .locals 7

    iget v0, p0, Lcom/twitter/internal/android/widget/a;->i:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    iget-object v2, p0, Lcom/twitter/internal/android/widget/a;->k:Lcom/twitter/internal/android/widget/ai;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/twitter/internal/android/widget/ai;->getIntrinsicWidth()I

    move-result v0

    iget v1, p0, Lcom/twitter/internal/android/widget/a;->e:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-virtual {v2}, Lcom/twitter/internal/android/widget/ai;->getIntrinsicHeight()I

    move-result v0

    iget v1, p0, Lcom/twitter/internal/android/widget/a;->f:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v4

    sub-int v0, p4, p2

    sub-int/2addr v0, v3

    if-eqz p6, :cond_1

    iget v1, p6, Landroid/graphics/Rect;->top:I

    iget v5, p6, Landroid/graphics/Rect;->right:I

    div-int/lit8 v6, v3, 0x2

    sub-int/2addr v5, v6

    invoke-static {v5, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    :goto_0
    add-int/2addr v3, v0

    add-int/2addr v4, v1

    invoke-virtual {v2, v0, v1, v3, v4}, Lcom/twitter/internal/android/widget/ai;->setBounds(IIII)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/twitter/internal/android/widget/a;->i:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/a;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    sub-int v0, p4, p2

    invoke-static {v0, v2}, Lhl;->a(II)I

    move-result v4

    iget v0, p0, Lcom/twitter/internal/android/widget/a;->g:I

    const/4 v5, -0x1

    if-eq v0, v5, :cond_3

    sub-int v0, p5, p3

    iget-object v5, p0, Lcom/twitter/internal/android/widget/a;->a:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v0, v5

    iget v5, p0, Lcom/twitter/internal/android/widget/a;->g:I

    sub-int/2addr v0, v5

    sub-int/2addr v0, v3

    :goto_2
    add-int/2addr v2, v4

    add-int/2addr v3, v0

    invoke-virtual {v1, v4, v0, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_1

    :cond_3
    sub-int v0, p5, p7

    iget-object v5, p0, Lcom/twitter/internal/android/widget/a;->a:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v0, v5

    invoke-static {v0, v3}, Lhl;->a(II)I

    move-result v0

    add-int/2addr v0, p7

    goto :goto_2
.end method

.method a()Z
    .locals 1

    iget v0, p0, Lcom/twitter/internal/android/widget/a;->j:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/a;->k:Lcom/twitter/internal/android/widget/ai;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/a;->h:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/internal/android/widget/a;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/a;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/a;->h:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/a;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    :cond_0
    return-void
.end method

.method public b(I)V
    .locals 10

    const/16 v7, 0xb

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    iget v0, p0, Lcom/twitter/internal/android/widget/a;->j:I

    if-eq v0, p1, :cond_1

    iget v0, p0, Lcom/twitter/internal/android/widget/a;->i:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_6

    iget v0, p0, Lcom/twitter/internal/android/widget/a;->j:I

    iput p1, p0, Lcom/twitter/internal/android/widget/a;->j:I

    if-gtz p1, :cond_3

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v7, :cond_2

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/a;->d()V

    const-wide/16 v3, 0xc8

    new-instance v5, Landroid/view/animation/AnticipateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/AnticipateInterpolator;-><init>()V

    new-instance v6, Lcom/twitter/internal/android/widget/c;

    invoke-direct {v6, p0}, Lcom/twitter/internal/android/widget/c;-><init>(Lcom/twitter/internal/android/widget/a;)V

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/internal/android/widget/a;->a(FFJLandroid/view/animation/Interpolator;Landroid/animation/Animator$AnimatorListener;)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/a;->m:Landroid/animation/Animator;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/a;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/a;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->refreshDrawableState()V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/a;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/a;->k:Lcom/twitter/internal/android/widget/ai;

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/twitter/internal/android/widget/a;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/internal/android/widget/a;->k:Lcom/twitter/internal/android/widget/ai;

    if-nez v4, :cond_4

    new-instance v4, Lcom/twitter/internal/android/widget/ai;

    invoke-direct {v4, v3}, Lcom/twitter/internal/android/widget/ai;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/twitter/internal/android/widget/a;->k:Lcom/twitter/internal/android/widget/ai;

    iget-object v4, p0, Lcom/twitter/internal/android/widget/a;->k:Lcom/twitter/internal/android/widget/ai;

    iget v5, p0, Lcom/twitter/internal/android/widget/a;->c:I

    invoke-virtual {v4, v5}, Lcom/twitter/internal/android/widget/ai;->a(I)V

    iget-object v4, p0, Lcom/twitter/internal/android/widget/a;->k:Lcom/twitter/internal/android/widget/ai;

    iget v5, p0, Lcom/twitter/internal/android/widget/a;->d:F

    invoke-virtual {v4, v5}, Lcom/twitter/internal/android/widget/ai;->a(F)V

    iget v4, p0, Lcom/twitter/internal/android/widget/a;->b:I

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/twitter/internal/android/widget/a;->k:Lcom/twitter/internal/android/widget/ai;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget v6, p0, Lcom/twitter/internal/android/widget/a;->b:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/twitter/internal/android/widget/ai;->a(Landroid/graphics/drawable/Drawable;)V

    :cond_4
    const/16 v4, 0x14

    if-gt p1, v4, :cond_5

    iget-object v4, p0, Lcom/twitter/internal/android/widget/a;->k:Lcom/twitter/internal/android/widget/ai;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Lcom/twitter/internal/android/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;)V

    :goto_2
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v3, v7, :cond_0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/a;->d()V

    const-wide/16 v6, 0xfa

    new-instance v8, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {v8}, Landroid/view/animation/OvershootInterpolator;-><init>()V

    new-instance v9, Lcom/twitter/internal/android/widget/d;

    invoke-direct {v9, p0}, Lcom/twitter/internal/android/widget/d;-><init>(Lcom/twitter/internal/android/widget/a;)V

    move-object v3, p0

    move v4, v2

    move v5, v1

    invoke-direct/range {v3 .. v9}, Lcom/twitter/internal/android/widget/a;->a(FFJLandroid/view/animation/Interpolator;Landroid/animation/Animator$AnimatorListener;)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/a;->m:Landroid/animation/Animator;

    goto :goto_0

    :cond_5
    iget-object v4, p0, Lcom/twitter/internal/android/widget/a;->k:Lcom/twitter/internal/android/widget/ai;

    const-string/jumbo v5, "20+"

    invoke-virtual {v4, v3, v5}, Lcom/twitter/internal/android/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_6
    iget v0, p0, Lcom/twitter/internal/android/widget/a;->i:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iput p1, p0, Lcom/twitter/internal/android/widget/a;->j:I

    iget-object v0, p0, Lcom/twitter/internal/android/widget/a;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/twitter/internal/android/widget/a;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/a;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :cond_7
    iget-object v0, p0, Lcom/twitter/internal/android/widget/a;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    goto/16 :goto_1
.end method

.method c()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/a;->m:Landroid/animation/Animator;

    return-void
.end method
