.class Lcom/twitter/internal/android/widget/ToolBarHomeView;
.super Landroid/view/View;
.source "Twttr"


# static fields
.field private static final a:Landroid/text/TextPaint;

.field private static final b:[I


# instance fields
.field private final A:Lcom/twitter/internal/android/widget/aw;

.field private final c:I

.field private final d:I

.field private final e:I

.field private f:F

.field private g:F

.field private h:Landroid/content/res/ColorStateList;

.field private i:Landroid/graphics/Typeface;

.field private j:Landroid/text/StaticLayout;

.field private k:Ljava/lang/CharSequence;

.field private l:Landroid/text/StaticLayout;

.field private m:Ljava/lang/CharSequence;

.field private n:I

.field private o:Landroid/graphics/Point;

.field private p:Landroid/graphics/Point;

.field private q:Landroid/graphics/drawable/Drawable;

.field private r:Landroid/graphics/drawable/Drawable;

.field private s:Lcom/twitter/internal/android/widget/ai;

.field private t:Landroid/graphics/drawable/Drawable;

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v1, 0x1

    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    sput-object v0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->a:Landroid/text/TextPaint;

    new-array v0, v1, [I

    const/4 v1, 0x0

    sget v2, Lcom/twitter/internal/android/b;->state_collapsed:I

    aput v2, v0, v1

    sput-object v0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->b:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget v0, Lcom/twitter/internal/android/b;->toolBarHomeStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->o:Landroid/graphics/Point;

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->p:Landroid/graphics/Point;

    sget-object v0, Lcom/twitter/internal/android/f;->ToolBarHomeView:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->a(Landroid/content/res/TypedArray;)V

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->d:I

    const/4 v1, 0x4

    const/high16 v2, -0x1000000

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->e:I

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int v1, v1

    iput v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->c:I

    iput-boolean v3, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->u:Z

    iput-boolean v3, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->v:Z

    iput-boolean v3, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->w:Z

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->isInEditMode()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/twitter/internal/android/widget/aw;

    invoke-direct {v1, p0, v0}, Lcom/twitter/internal/android/widget/aw;-><init>(Lcom/twitter/internal/android/widget/ToolBarHomeView;Landroid/content/res/TypedArray;)V

    iput-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->A:Lcom/twitter/internal/android/widget/aw;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->A:Lcom/twitter/internal/android/widget/aw;

    invoke-static {p0, v1}, Landroid/support/v4/view/ViewCompat;->setAccessibilityDelegate(Landroid/view/View;Landroid/support/v4/view/AccessibilityDelegateCompat;)V

    :goto_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->A:Lcom/twitter/internal/android/widget/aw;

    goto :goto_0
.end method

.method private a(Landroid/graphics/drawable/Drawable;I)I
    .locals 4

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->getMeasuredHeight()I

    move-result v1

    invoke-static {v0, v1}, Lhl;->a(II)I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    add-int v3, p2, v2

    add-int/2addr v0, v1

    invoke-virtual {p1, p2, v1, v3, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {p0, p1}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    return v2
.end method

.method private a(Landroid/content/res/TypedArray;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x1

    const/high16 v1, 0x41800000    # 16.0f

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->f:F

    const/4 v0, 0x5

    const/high16 v1, 0x41f00000    # 30.0f

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->g:F

    invoke-virtual {p1, v2}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->h:Landroid/content/res/ColorStateList;

    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    const/4 v1, 0x7

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->y:Z

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->h:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->b()V

    :goto_0
    invoke-static {v0}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->i:Landroid/graphics/Typeface;

    return-void

    :cond_0
    const/high16 v1, -0x1000000

    iput v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->n:I

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/internal/android/widget/ToolBarHomeView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->v:Z

    return v0
.end method

.method private b()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->h:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->getDrawableState()[I

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    iget v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->n:I

    if-eq v0, v1, :cond_0

    iput v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->n:I

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->invalidate()V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/twitter/internal/android/widget/ToolBarHomeView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->u:Z

    return v0
.end method

.method private c()I
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->d()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method private c(I)V
    .locals 12

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->c()I

    move-result v0

    sub-int v11, v0, p1

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->k:Ljava/lang/CharSequence;

    sget-object v4, Lcom/twitter/internal/android/widget/ToolBarHomeView;->a:Landroid/text/TextPaint;

    iget v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->f:F

    invoke-virtual {v4, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    new-instance v0, Landroid/text/StaticLayout;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    invoke-static {v1, v4}, Lhl;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v5

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    sget-object v10, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move v9, v2

    invoke-direct/range {v0 .. v11}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->j:Landroid/text/StaticLayout;

    return-void
.end method

.method private d()I
    .locals 2

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->u:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->t:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->t:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->w:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->q:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    return v0
.end method


# virtual methods
.method public a(I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->c(I)V

    return-void
.end method

.method public a(Landroid/content/Context;I)V
    .locals 1

    sget-object v0, Lcom/twitter/internal/android/f;->ToolBarHomeView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->a(Landroid/content/res/TypedArray;)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->q:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_0

    iput-object p1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->r:Landroid/graphics/drawable/Drawable;

    iput-object p1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->q:Landroid/graphics/drawable/Drawable;

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->z:I

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->requestLayout()V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    if-nez p1, :cond_2

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->j:Landroid/text/StaticLayout;

    if-eqz v2, :cond_1

    :goto_0
    iput-object v3, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->j:Landroid/text/StaticLayout;

    iput-object v3, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->k:Ljava/lang/CharSequence;

    :goto_1
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->requestLayout()V

    :cond_0
    invoke-virtual {p0, p1}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->j:Landroid/text/StaticLayout;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->j:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    :cond_3
    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->y:Z

    if-eqz v1, :cond_4

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->k:Ljava/lang/CharSequence;

    :goto_2
    invoke-virtual {p0, p1}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->setContentDescription(Ljava/lang/CharSequence;)V

    iput-object v3, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->j:Landroid/text/StaticLayout;

    goto :goto_1

    :cond_4
    iput-object p1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->k:Ljava/lang/CharSequence;

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public a(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->u:Z

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->u:Z

    invoke-virtual {p0, p1}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->setClickable(Z)V

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->requestLayout()V

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->x:Z

    return v0
.end method

.method public b(I)V
    .locals 5

    iget v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->z:I

    if-eq p1, v0, :cond_1

    if-lez p1, :cond_3

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v0, 0x64

    if-ge p1, v0, :cond_2

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->s:Lcom/twitter/internal/android/widget/ai;

    if-nez v2, :cond_0

    new-instance v2, Lcom/twitter/internal/android/widget/ai;

    invoke-direct {v2, v1}, Lcom/twitter/internal/android/widget/ai;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->s:Lcom/twitter/internal/android/widget/ai;

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->s:Lcom/twitter/internal/android/widget/ai;

    iget v3, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->e:I

    invoke-virtual {v2, v3}, Lcom/twitter/internal/android/widget/ai;->a(I)V

    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->d:I

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->s:Lcom/twitter/internal/android/widget/ai;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->d:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/internal/android/widget/ai;->a(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->s:Lcom/twitter/internal/android/widget/ai;

    invoke-virtual {v2, v1, v0}, Lcom/twitter/internal/android/widget/ai;->a(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->s:Lcom/twitter/internal/android/widget/ai;

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->q:Landroid/graphics/drawable/Drawable;

    :goto_1
    iput p1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->z:I

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->requestLayout()V

    :cond_1
    return-void

    :cond_2
    const-string/jumbo v0, "99+"

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->r:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->q:Landroid/graphics/drawable/Drawable;

    goto :goto_1
.end method

.method public b(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->t:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_0

    iput-object p1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->t:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->requestLayout()V

    :cond_0
    return-void
.end method

.method public b(Ljava/lang/CharSequence;)V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    if-nez p1, :cond_2

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->l:Landroid/text/StaticLayout;

    if-eqz v2, :cond_1

    :goto_0
    iput-object v3, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->l:Landroid/text/StaticLayout;

    iput-object v3, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->m:Ljava/lang/CharSequence;

    :goto_1
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->requestLayout()V

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->l:Landroid/text/StaticLayout;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->l:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    :cond_3
    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->y:Z

    if-eqz v1, :cond_4

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->m:Ljava/lang/CharSequence;

    :goto_2
    iput-object v3, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->l:Landroid/text/StaticLayout;

    goto :goto_1

    :cond_4
    iput-object p1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->m:Ljava/lang/CharSequence;

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public b(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->v:Z

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->v:Z

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->requestLayout()V

    goto :goto_0
.end method

.method public c(Ljava/lang/CharSequence;)V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->A:Lcom/twitter/internal/android/widget/aw;

    iput-object p1, v0, Lcom/twitter/internal/android/widget/aw;->d:Ljava/lang/CharSequence;

    :cond_0
    return-void
.end method

.method public c(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->w:Z

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->w:Z

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->requestLayout()V

    goto :goto_0
.end method

.method public d(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->x:Z

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->x:Z

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->refreshDrawableState()V

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->requestLayout()V

    goto :goto_0
.end method

.method public dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->A:Lcom/twitter/internal/android/widget/aw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->A:Lcom/twitter/internal/android/widget/aw;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/aw;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected drawableStateChanged()V
    .locals 2

    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->h:Landroid/content/res/ColorStateList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->h:Landroid/content/res/ColorStateList;

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->b()V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->getDrawableState()[I

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    :cond_1
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->t:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->t:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    :cond_2
    return-void
.end method

.method protected onCreateDrawableState(I)[I
    .locals 2

    add-int/lit8 v0, p1, 0x1

    invoke-super {p0, v0}, Landroid/view/View;->onCreateDrawableState(I)[I

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->x:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/twitter/internal/android/widget/ToolBarHomeView;->b:[I

    invoke-static {v0, v1}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->mergeDrawableStates([I[I)[I

    :cond_0
    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    sget-object v0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->a:Landroid/text/TextPaint;

    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->u:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->t:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->t:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->w:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->q:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_1
    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->x:Z

    if-nez v1, :cond_3

    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->v:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->j:Landroid/text/StaticLayout;

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->n:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    iget v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->f:F

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->i:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->save(I)I

    move-result v1

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->o:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->o:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->j:Landroid/text/StaticLayout;

    invoke-virtual {v2, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->l:Landroid/text/StaticLayout;

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->g:F

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->p:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->p:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->l:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    :cond_2
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :cond_3
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    sub-int v1, p5, p3

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->getPaddingLeft()I

    move-result v0

    iget-boolean v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->u:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->t:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->t:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v2, v0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->a(Landroid/graphics/drawable/Drawable;I)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-boolean v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->w:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->q:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->q:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v2, v0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->a(Landroid/graphics/drawable/Drawable;I)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-boolean v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->x:Z

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->v:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->j:Landroid/text/StaticLayout;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->l:Landroid/text/StaticLayout;

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->c:I

    add-int/2addr v0, v2

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->o:Landroid/graphics/Point;

    iget-object v3, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->j:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    iget-object v4, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->l:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    invoke-static {v3, v1}, Lhl;->a(II)I

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Point;->set(II)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->p:Landroid/graphics/Point;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->j:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->c:I

    add-int/2addr v0, v2

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->o:Landroid/graphics/Point;

    iget-object v3, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->j:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    invoke-static {v3, v1}, Lhl;->a(II)I

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Point;->set(II)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 15

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->getSuggestedMinimumHeight()I

    move-result v1

    move/from16 v0, p2

    invoke-static {v1, v0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->getDefaultSize(II)I

    move-result v14

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->d()I

    move-result v13

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->c()I

    move-result v12

    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->v:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->j:Landroid/text/StaticLayout;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->l:Landroid/text/StaticLayout;

    if-nez v1, :cond_2

    :cond_0
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->j:Landroid/text/StaticLayout;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->k:Ljava/lang/CharSequence;

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->c(I)V

    :cond_1
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->l:Landroid/text/StaticLayout;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->m:Ljava/lang/CharSequence;

    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->m:Ljava/lang/CharSequence;

    sget-object v5, Lcom/twitter/internal/android/widget/ToolBarHomeView;->a:Landroid/text/TextPaint;

    iget v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->g:F

    invoke-virtual {v5, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    new-instance v1, Landroid/text/StaticLayout;

    const/4 v3, 0x0

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v4

    invoke-static {v2, v5}, Lhl;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v6

    sget-object v7, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    const/4 v10, 0x0

    sget-object v11, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-direct/range {v1 .. v12}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    iput-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->l:Landroid/text/StaticLayout;

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->isInEditMode()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->A:Lcom/twitter/internal/android/widget/aw;

    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->c:I

    add-int/2addr v2, v13

    iput v2, v1, Lcom/twitter/internal/android/widget/aw;->a:I

    :cond_3
    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->x:Z

    if-nez v1, :cond_7

    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->v:Z

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->j:Landroid/text/StaticLayout;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->l:Landroid/text/StaticLayout;

    if-eqz v1, :cond_6

    iget v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->c:I

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->j:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->l:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getWidth()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v1, v13

    :goto_0
    if-lez v1, :cond_4

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    :cond_4
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->isInEditMode()Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->A:Lcom/twitter/internal/android/widget/aw;

    iget-object v3, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->A:Lcom/twitter/internal/android/widget/aw;

    iget v3, v3, Lcom/twitter/internal/android/widget/aw;->a:I

    sub-int v3, v1, v3

    iput v3, v2, Lcom/twitter/internal/android/widget/aw;->b:I

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->A:Lcom/twitter/internal/android/widget/aw;

    iput v14, v2, Lcom/twitter/internal/android/widget/aw;->c:I

    :cond_5
    invoke-virtual {p0, v1, v14}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->setMeasuredDimension(II)V

    return-void

    :cond_6
    iget v1, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->c:I

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->j:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getWidth()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v1, v13

    goto :goto_0

    :cond_7
    move v1, v13

    goto :goto_0
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->q:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarHomeView;->t:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_0

    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
