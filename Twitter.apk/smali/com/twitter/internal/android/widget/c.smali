.class Lcom/twitter/internal/android/widget/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/internal/android/widget/a;


# direct methods
.method public constructor <init>(Lcom/twitter/internal/android/widget/a;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/internal/android/widget/c;->a:Lcom/twitter/internal/android/widget/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/c;->a:Lcom/twitter/internal/android/widget/a;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/a;->a(F)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/c;->a:Lcom/twitter/internal/android/widget/a;

    invoke-static {v0, v2}, Lcom/twitter/internal/android/widget/a;->a(Lcom/twitter/internal/android/widget/a;Lcom/twitter/internal/android/widget/ai;)Lcom/twitter/internal/android/widget/ai;

    iget-object v0, p0, Lcom/twitter/internal/android/widget/c;->a:Lcom/twitter/internal/android/widget/a;

    invoke-static {v0, v2}, Lcom/twitter/internal/android/widget/a;->a(Lcom/twitter/internal/android/widget/a;Landroid/animation/Animator;)Landroid/animation/Animator;

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/c;->a:Lcom/twitter/internal/android/widget/a;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/a;->a(F)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/c;->a:Lcom/twitter/internal/android/widget/a;

    invoke-static {v0, v2}, Lcom/twitter/internal/android/widget/a;->a(Lcom/twitter/internal/android/widget/a;Lcom/twitter/internal/android/widget/ai;)Lcom/twitter/internal/android/widget/ai;

    iget-object v0, p0, Lcom/twitter/internal/android/widget/c;->a:Lcom/twitter/internal/android/widget/a;

    invoke-static {v0, v2}, Lcom/twitter/internal/android/widget/a;->a(Lcom/twitter/internal/android/widget/a;Landroid/animation/Animator;)Landroid/animation/Animator;

    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0

    return-void
.end method
