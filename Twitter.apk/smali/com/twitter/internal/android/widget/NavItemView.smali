.class public Lcom/twitter/internal/android/widget/NavItemView;
.super Landroid/view/View;
.source "Twttr"


# static fields
.field private static final a:Landroid/text/TextPaint;


# instance fields
.field private final b:F

.field private final c:Landroid/graphics/Point;

.field private final d:Landroid/graphics/Rect;

.field private final e:Landroid/content/res/ColorStateList;

.field private final f:Lcom/twitter/internal/android/widget/ax;

.field private final g:I

.field private final h:I

.field private final i:I

.field private j:Lcom/twitter/internal/android/widget/a;

.field private k:Landroid/text/StaticLayout;

.field private l:Ljava/lang/CharSequence;

.field private m:I

.field private n:I

.field private o:I

.field private p:Landroid/graphics/drawable/Drawable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    sput-object v0, Lcom/twitter/internal/android/widget/NavItemView;->a:Landroid/text/TextPaint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    sget v1, Lcom/twitter/internal/android/b;->navItemStyle:I

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/internal/android/widget/NavItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget v0, Lcom/twitter/internal/android/b;->navItemStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/internal/android/widget/NavItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->c:Landroid/graphics/Point;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->d:Landroid/graphics/Rect;

    sget-object v0, Lcom/twitter/internal/android/f;->NavItemView:[I

    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->e:Landroid/content/res/ColorStateList;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->e:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/NavItemView;->a()V

    :cond_0
    const/4 v1, 0x3

    const/high16 v2, 0x41900000    # 18.0f

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->b:F

    invoke-static {p1}, Lcom/twitter/internal/android/widget/ax;->a(Landroid/content/Context;)Lcom/twitter/internal/android/widget/ax;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->f:Lcom/twitter/internal/android/widget/ax;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->i:I

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->g:I

    invoke-virtual {v0, v4, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->h:I

    new-instance v1, Lcom/twitter/internal/android/widget/a;

    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-direct {v1, p0, p1, v2}, Lcom/twitter/internal/android/widget/a;-><init>(Landroid/view/View;Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->j:Lcom/twitter/internal/android/widget/a;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private a()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->e:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getDrawableState()[I

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    iget v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->m:I

    if-eq v0, v1, :cond_0

    iput v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->m:I

    :cond_0
    return-void
.end method

.method private b()Landroid/text/TextPaint;
    .locals 3

    sget-object v0, Lcom/twitter/internal/android/widget/NavItemView;->a:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->f:Lcom/twitter/internal/android/widget/ax;

    iget v2, p0, Lcom/twitter/internal/android/widget/NavItemView;->h:I

    invoke-virtual {v1, v2}, Lcom/twitter/internal/android/widget/ax;->a(I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    :goto_0
    iget v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->b:F

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    iget v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->m:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->f:Lcom/twitter/internal/android/widget/ax;

    iget v2, p0, Lcom/twitter/internal/android/widget/NavItemView;->g:I

    invoke-virtual {v1, v2}, Lcom/twitter/internal/android/widget/ax;->a(I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    goto :goto_0
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 1

    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/NavItemView;->a()V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->j:Lcom/twitter/internal/android/widget/a;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/a;->b()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->p:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->i:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->p:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->j:Lcom/twitter/internal/android/widget/a;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/a;->a(Landroid/graphics/Canvas;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->k:Landroid/text/StaticLayout;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/NavItemView;->b()Landroid/text/TextPaint;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->save(I)I

    move-result v0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->c:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/twitter/internal/android/widget/NavItemView;->c:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->k:Landroid/text/StaticLayout;

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->p:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->i:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->p:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->p:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    sub-int v2, p4, p2

    invoke-static {v2, v1}, Lhl;->a(II)I

    move-result v2

    sub-int v3, p5, p3

    invoke-static {v3, v0}, Lhl;->a(II)I

    move-result v3

    add-int v7, v3, v0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->p:Landroid/graphics/drawable/Drawable;

    add-int/2addr v1, v2

    invoke-virtual {v0, v2, v3, v1, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->p:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v6

    :goto_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->j:Lcom/twitter/internal/android/widget/a;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v7}, Lcom/twitter/internal/android/widget/a;->a(ZIIIILandroid/graphics/Rect;I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->k:Landroid/text/StaticLayout;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/NavItemView;->b()Landroid/text/TextPaint;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getPaddingLeft()I

    move-result v2

    sub-int v3, p4, p2

    sub-int/2addr v3, v2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getWidth()I

    move-result v4

    invoke-static {v3, v4}, Lhl;->a(II)I

    move-result v3

    add-int/2addr v3, v2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getPaddingTop()I

    move-result v4

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v5

    sub-int v6, p5, p3

    sub-int/2addr v6, v4

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getPaddingBottom()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-static {v6, v5}, Lhl;->a(II)I

    move-result v6

    add-int/2addr v6, v4

    iget-object v7, p0, Lcom/twitter/internal/android/widget/NavItemView;->c:Landroid/graphics/Point;

    invoke-virtual {v7, v3, v6}, Landroid/graphics/Point;->set(II)V

    invoke-virtual {v1}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v1

    iget v7, v1, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iget v8, v1, Landroid/graphics/Paint$FontMetricsInt;->top:I

    sub-int/2addr v7, v8

    iget v1, v1, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    sub-int v1, v7, v1

    add-int v7, v6, v1

    iget-object v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->d:Landroid/graphics/Rect;

    sub-int v4, v6, v4

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getWidth()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int v2, v6, v5

    invoke-virtual {v1, v3, v4, v0, v2}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v6, p0, Lcom/twitter/internal/android/widget/NavItemView;->d:Landroid/graphics/Rect;

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getPaddingTop()I

    move-result v7

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->k:Landroid/text/StaticLayout;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->p:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    iget v2, p0, Lcom/twitter/internal/android/widget/NavItemView;->i:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-static {v0, p1}, Lcom/twitter/internal/android/widget/NavItemView;->getDefaultSize(II)I

    move-result v0

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-static {v1, p2}, Lcom/twitter/internal/android/widget/NavItemView;->getDefaultSize(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/internal/android/widget/NavItemView;->setMeasuredDimension(II)V

    :goto_0
    iput p1, p0, Lcom/twitter/internal/android/widget/NavItemView;->n:I

    return-void

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getWidth()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getPaddingBottom()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/2addr v0, v2

    invoke-static {v1, p1}, Lcom/twitter/internal/android/widget/NavItemView;->getDefaultSize(II)I

    move-result v1

    invoke-static {v0, p2}, Lcom/twitter/internal/android/widget/NavItemView;->getDefaultSize(II)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/twitter/internal/android/widget/NavItemView;->setMeasuredDimension(II)V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    goto :goto_0
.end method

.method public setBadgeMode(I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->j:Lcom/twitter/internal/android/widget/a;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/a;->a(I)V

    return-void
.end method

.method public setIconResource(I)V
    .locals 2

    iget v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->i:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->o:I

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput p1, p0, Lcom/twitter/internal/android/widget/NavItemView;->o:I

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->p:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->requestLayout()V

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->invalidate()V

    goto :goto_0
.end method

.method public setIndicatorCount(I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->j:Lcom/twitter/internal/android/widget/a;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/a;->b(I)V

    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 12

    const/4 v0, 0x1

    const/4 v2, 0x0

    if-nez p1, :cond_2

    iget-object v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->l:Ljava/lang/CharSequence;

    if-eqz v1, :cond_1

    :goto_0
    iput-object p1, p0, Lcom/twitter/internal/android/widget/NavItemView;->l:Ljava/lang/CharSequence;

    :goto_1
    if-eqz v0, :cond_0

    if-eqz p1, :cond_5

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/NavItemView;->b()Landroid/text/TextPaint;

    move-result-object v4

    invoke-static {p1, v4}, Lhl;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v5

    iget v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->n:I

    invoke-static {v0}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    add-int v11, v0, v5

    :goto_2
    new-instance v0, Landroid/text/StaticLayout;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    sget-object v10, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object v1, p1

    move v9, v2

    invoke-direct/range {v0 .. v11}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->k:Landroid/text/StaticLayout;

    :goto_3
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->requestLayout()V

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->invalidate()V

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/twitter/internal/android/widget/NavItemView;->l:Ljava/lang/CharSequence;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    iput-object p1, p0, Lcom/twitter/internal/android/widget/NavItemView;->l:Ljava/lang/CharSequence;

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    iget v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->n:I

    invoke-static {v0}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/NavItemView;->getPaddingRight()I

    move-result v1

    sub-int v11, v0, v1

    goto :goto_2

    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->k:Landroid/text/StaticLayout;

    goto :goto_3
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/NavItemView;->j:Lcom/twitter/internal/android/widget/a;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/a;->a(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
