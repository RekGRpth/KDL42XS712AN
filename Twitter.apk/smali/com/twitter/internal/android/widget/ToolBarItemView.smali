.class public Lcom/twitter/internal/android/widget/ToolBarItemView;
.super Landroid/view/View;
.source "Twttr"


# static fields
.field private static final a:[I

.field private static final b:Landroid/text/TextPaint;


# instance fields
.field private final c:I

.field private final d:Landroid/graphics/Point;

.field private e:F

.field private f:Landroid/graphics/Typeface;

.field private g:Landroid/content/res/ColorStateList;

.field private h:Landroid/graphics/drawable/Drawable;

.field private i:Landroid/text/StaticLayout;

.field private j:I

.field private k:Z

.field private l:I

.field private m:Ljava/lang/String;

.field private n:Lcom/twitter/internal/android/widget/a;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    new-array v0, v3, [I

    const/4 v1, 0x0

    sget v2, Lcom/twitter/internal/android/b;->state_numbered:I

    aput v2, v0, v1

    sput-object v0, Lcom/twitter/internal/android/widget/ToolBarItemView;->a:[I

    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0, v3}, Landroid/text/TextPaint;-><init>(I)V

    sput-object v0, Lcom/twitter/internal/android/widget/ToolBarItemView;->b:Landroid/text/TextPaint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    sget v1, Lcom/twitter/internal/android/b;->toolBarItemStyle:I

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/internal/android/widget/ToolBarItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget v0, Lcom/twitter/internal/android/b;->toolBarItemStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/internal/android/widget/ToolBarItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->d:Landroid/graphics/Point;

    sget-object v0, Lcom/twitter/internal/android/f;->ToolBarItemView:[I

    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const/high16 v1, 0x41200000    # 10.0f

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int v1, v1

    iput v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->c:I

    new-instance v1, Lcom/twitter/internal/android/widget/a;

    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-direct {v1, p0, p1, v2}, Lcom/twitter/internal/android/widget/a;-><init>(Landroid/view/View;Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->n:Lcom/twitter/internal/android/widget/a;

    invoke-direct {p0, v0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setTextAppearance(Landroid/content/res/TypedArray;)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private a()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->g:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getDrawableState()[I

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    iget v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->l:I

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setTextColor(I)V

    :cond_0
    return-void
.end method

.method private b()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->i:Landroid/text/StaticLayout;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->k:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->h:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setTextAppearance(Landroid/content/res/TypedArray;)V
    .locals 2

    const/4 v0, 0x2

    const/high16 v1, 0x41600000    # 14.0f

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->e:F

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->g:Landroid/content/res/ColorStateList;

    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->f:Landroid/graphics/Typeface;

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->g:Landroid/content/res/ColorStateList;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->a()V

    :goto_0
    return-void

    :cond_0
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->l:I

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;I)V
    .locals 1

    sget-object v0, Lcom/twitter/internal/android/f;->ToolBarItemView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setTextAppearance(Landroid/content/res/TypedArray;)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method protected drawableStateChanged()V
    .locals 2

    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    :cond_0
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->a()V

    return-void
.end method

.method protected onCreateDrawableState(I)[I
    .locals 2

    sget-object v0, Lcom/twitter/internal/android/widget/ToolBarItemView;->a:[I

    array-length v0, v0

    add-int/2addr v0, p1

    invoke-super {p0, v0}, Landroid/view/View;->onCreateDrawableState(I)[I

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->n:Lcom/twitter/internal/android/widget/a;

    invoke-virtual {v1}, Lcom/twitter/internal/android/widget/a;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/twitter/internal/android/widget/ToolBarItemView;->a:[I

    invoke-static {v0, v1}, Lcom/twitter/internal/android/widget/ToolBarItemView;->mergeDrawableStates([I[I)[I

    :cond_0
    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->n:Lcom/twitter/internal/android/widget/a;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/a;->c()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->save(I)I

    move-result v0

    sget-object v1, Lcom/twitter/internal/android/widget/ToolBarItemView;->b:Landroid/text/TextPaint;

    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->e:F

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->l:I

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->f:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->d:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->d:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->i:Landroid/text/StaticLayout;

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->n:Lcom/twitter/internal/android/widget/a;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/a;->a(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 8

    sub-int v0, p4, p2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    sub-int v1, p5, p3

    iget v2, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->j:I

    invoke-static {v0, v2}, Lhl;->a(II)I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getPaddingLeft()I

    move-result v2

    add-int/2addr v0, v2

    const/4 v7, 0x0

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/twitter/internal/android/widget/ToolBarItemView;->b:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->d:Landroid/graphics/Point;

    iget v4, v2, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iget v5, v2, Landroid/graphics/Paint$FontMetricsInt;->top:I

    sub-int/2addr v4, v5

    iget v2, v2, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    sub-int v2, v4, v2

    invoke-static {v2, v1}, Lhl;->a(II)I

    move-result v2

    invoke-virtual {v3, v0, v2}, Landroid/graphics/Point;->set(II)V

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->i:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getWidth()I

    move-result v2

    iget v3, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->c:I

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    iget-object v3, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    invoke-static {v1, v2}, Lhl;->a(II)I

    move-result v1

    iget-object v4, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->h:Landroid/graphics/drawable/Drawable;

    add-int/2addr v3, v0

    add-int v5, v1, v2

    invoke-virtual {v4, v0, v1, v3, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    add-int v7, v1, v2

    :cond_1
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->n:Lcom/twitter/internal/android/widget/a;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v6

    :goto_0
    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v7}, Lcom/twitter/internal/android/widget/a;->a(ZIIIILandroid/graphics/Rect;I)V

    return-void

    :cond_2
    const/4 v6, 0x0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    :cond_0
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->c:I

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->i:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getWidth()I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->j:I

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getSuggestedMinimumWidth()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0, p1}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getDefaultSize(II)I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getSuggestedMinimumHeight()I

    move-result v1

    invoke-static {v1, p2}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getDefaultSize(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setMeasuredDimension(II)V

    return-void
.end method

.method public setBadgeMode(I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->n:Lcom/twitter/internal/android/widget/a;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/a;->a(I)V

    return-void
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->h:Landroid/graphics/drawable/Drawable;

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->requestLayout()V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public setImageResource(I)V
    .locals 1

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public setLabel(Ljava/lang/CharSequence;)V
    .locals 9

    const/4 v1, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    if-nez p1, :cond_3

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->i:Landroid/text/StaticLayout;

    if-eqz v0, :cond_0

    move v7, v8

    :cond_0
    iput-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->i:Landroid/text/StaticLayout;

    iput-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->m:Ljava/lang/String;

    :cond_1
    :goto_0
    if-eqz v7, :cond_2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->requestLayout()V

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->invalidate()V

    :cond_2
    return-void

    :cond_3
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->i:Landroid/text/StaticLayout;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->m:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_4
    sget-object v2, Lcom/twitter/internal/android/widget/ToolBarItemView;->b:Landroid/text/TextPaint;

    iget v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->e:F

    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->f:Landroid/graphics/Typeface;

    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    new-instance v0, Landroid/text/StaticLayout;

    invoke-static {v1, v2}, Lhl;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v3

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->i:Landroid/text/StaticLayout;

    iput-object v1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->m:Ljava/lang/String;

    move v7, v8

    goto :goto_0
.end method

.method public setLabelResource(I)V
    .locals 1

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setLabel(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setNumber(I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->n:Lcom/twitter/internal/android/widget/a;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/a;->b(I)V

    return-void
.end method

.method public setTextColor(I)V
    .locals 1

    iget v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->l:I

    if-eq p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput p1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->l:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->invalidate()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setWithText(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->k:Z

    if-eq p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->k:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->requestLayout()V

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/ToolBarItemView;->invalidate()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->n:Lcom/twitter/internal/android/widget/a;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/a;->a(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ToolBarItemView;->h:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
