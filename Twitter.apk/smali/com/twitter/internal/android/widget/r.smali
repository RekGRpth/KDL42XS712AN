.class Lcom/twitter/internal/android/widget/r;
.super Landroid/database/DataSetObserver;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/internal/android/widget/HorizontalListView;


# direct methods
.method public constructor <init>(Lcom/twitter/internal/android/widget/HorizontalListView;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/internal/android/widget/r;->a:Lcom/twitter/internal/android/widget/HorizontalListView;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/r;->a:Lcom/twitter/internal/android/widget/HorizontalListView;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/r;->a:Lcom/twitter/internal/android/widget/HorizontalListView;

    iget-object v1, v1, Lcom/twitter/internal/android/widget/HorizontalListView;->d:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    iput v1, v0, Lcom/twitter/internal/android/widget/HorizontalListView;->e:I

    iget-object v0, p0, Lcom/twitter/internal/android/widget/r;->a:Lcom/twitter/internal/android/widget/HorizontalListView;

    iput v2, v0, Lcom/twitter/internal/android/widget/HorizontalListView;->g:I

    iget-object v0, p0, Lcom/twitter/internal/android/widget/r;->a:Lcom/twitter/internal/android/widget/HorizontalListView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/twitter/internal/android/widget/HorizontalListView;->f:Z

    iget-object v0, p0, Lcom/twitter/internal/android/widget/r;->a:Lcom/twitter/internal/android/widget/HorizontalListView;

    iput-boolean v2, v0, Lcom/twitter/internal/android/widget/HorizontalListView;->c:Z

    iget-object v0, p0, Lcom/twitter/internal/android/widget/r;->a:Lcom/twitter/internal/android/widget/HorizontalListView;

    iget-object v0, v0, Lcom/twitter/internal/android/widget/HorizontalListView;->b:Lcom/twitter/internal/util/f;

    invoke-virtual {v0}, Lcom/twitter/internal/util/f;->b()V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/r;->a:Lcom/twitter/internal/android/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->requestLayout()V

    return-void
.end method

.method public onInvalidated()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/r;->a:Lcom/twitter/internal/android/widget/HorizontalListView;

    iput v2, v0, Lcom/twitter/internal/android/widget/HorizontalListView;->e:I

    iget-object v0, p0, Lcom/twitter/internal/android/widget/r;->a:Lcom/twitter/internal/android/widget/HorizontalListView;

    iput v2, v0, Lcom/twitter/internal/android/widget/HorizontalListView;->g:I

    iget-object v0, p0, Lcom/twitter/internal/android/widget/r;->a:Lcom/twitter/internal/android/widget/HorizontalListView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/twitter/internal/android/widget/HorizontalListView;->f:Z

    iget-object v0, p0, Lcom/twitter/internal/android/widget/r;->a:Lcom/twitter/internal/android/widget/HorizontalListView;

    iput-boolean v2, v0, Lcom/twitter/internal/android/widget/HorizontalListView;->c:Z

    iget-object v0, p0, Lcom/twitter/internal/android/widget/r;->a:Lcom/twitter/internal/android/widget/HorizontalListView;

    iget-object v0, v0, Lcom/twitter/internal/android/widget/HorizontalListView;->b:Lcom/twitter/internal/util/f;

    invoke-virtual {v0}, Lcom/twitter/internal/util/f;->b()V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/r;->a:Lcom/twitter/internal/android/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->requestLayout()V

    return-void
.end method
