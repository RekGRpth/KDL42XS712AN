.class Lcom/twitter/internal/android/widget/ad;
.super Landroid/database/DataSetObserver;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/internal/android/widget/PopupEditText;


# direct methods
.method public constructor <init>(Lcom/twitter/internal/android/widget/PopupEditText;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/internal/android/widget/ad;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 2

    invoke-super {p0}, Landroid/database/DataSetObserver;->onChanged()V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ad;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    iget-boolean v0, v0, Lcom/twitter/internal/android/widget/PopupEditText;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ad;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    iget-boolean v0, v0, Lcom/twitter/internal/android/widget/PopupEditText;->c:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ad;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->a()V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ad;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/twitter/internal/android/widget/PopupEditText;->d:Z

    :cond_1
    return-void
.end method

.method public onInvalidated()V
    .locals 1

    invoke-super {p0}, Landroid/database/DataSetObserver;->onInvalidated()V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ad;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->b()V

    return-void
.end method
