.class public Lcom/twitter/internal/android/widget/HiddenDrawerLayout;
.super Landroid/view/ViewGroup;
.source "Twttr"


# instance fields
.field private A:Landroid/view/View;

.field private B:I

.field final a:I

.field final b:Z

.field c:Landroid/view/View;

.field d:Landroid/view/View;

.field e:Lcom/twitter/internal/android/widget/n;

.field f:F

.field g:Z

.field final h:Landroid/support/v4/widget/ViewDragHelper;

.field i:I

.field j:Z

.field k:Z

.field private final l:I

.field private final m:I

.field private final n:Landroid/graphics/Rect;

.field private final o:Landroid/graphics/Paint;

.field private final p:I

.field private final q:I

.field private final r:Landroid/graphics/Rect;

.field private final s:I

.field private final t:Z

.field private u:Lcom/twitter/internal/android/widget/o;

.field private v:Z

.field private w:F

.field private x:F

.field private y:I

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    sget v1, Lcom/twitter/internal/android/b;->hiddenDrawerStyle:I

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget v0, Lcom/twitter/internal/android/b;->hiddenDrawerStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9

    const/4 v8, 0x4

    const/4 v1, 0x0

    const/4 v7, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->n:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->o:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->r:Landroid/graphics/Rect;

    iput v8, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->i:I

    sget-object v0, Lcom/twitter/internal/android/f;->HiddenDrawerLayout:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v5

    const/16 v0, 0xfa

    invoke-virtual {v5, v7, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->l:I

    const/16 v0, 0xfa

    invoke-virtual {v5, v7, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->m:I

    invoke-virtual {v5, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    invoke-virtual {v5, v3, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v6

    if-eqz v0, :cond_1

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    move-object v4, v0

    :goto_0
    if-eqz v6, :cond_2

    invoke-static {p1, v6}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    :goto_1
    new-instance v6, Lcom/twitter/internal/android/widget/o;

    invoke-direct {v6, p0, p1, v4, v0}, Lcom/twitter/internal/android/widget/o;-><init>(Lcom/twitter/internal/android/widget/HiddenDrawerLayout;Landroid/content/Context;Landroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;)V

    iput-object v6, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->u:Lcom/twitter/internal/android/widget/o;

    invoke-virtual {v5, v8, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->q:I

    const/4 v0, 0x5

    const v4, -0x66000001

    invoke-virtual {v5, v0, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->p:I

    const/4 v0, 0x6

    invoke-virtual {v5, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->B:I

    const/4 v0, 0x7

    invoke-virtual {v5, v0, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->a:I

    iget v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->a:I

    if-eq v0, v3, :cond_0

    iget v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->a:I

    if-nez v0, :cond_3

    :cond_0
    move v0, v3

    :goto_2
    iput-boolean v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->b:Z

    const/16 v0, 0x9

    invoke-virtual {v5, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->t:Z

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->t:Z

    if-eqz v0, :cond_4

    const/16 v0, 0x8

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledEdgeSlop()I

    move-result v2

    mul-int/lit8 v2, v2, 0x3

    invoke-virtual {v5, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->s:I

    :goto_3
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    iput v2, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->y:I

    const/high16 v2, 0x3f000000    # 0.5f

    new-instance v3, Lcom/twitter/internal/android/widget/m;

    invoke-direct {v3, p0, v1}, Lcom/twitter/internal/android/widget/m;-><init>(Lcom/twitter/internal/android/widget/HiddenDrawerLayout;Lcom/twitter/internal/android/widget/l;)V

    invoke-static {p0, v2, v3}, Landroid/support/v4/widget/ViewDragHelper;->create(Landroid/view/ViewGroup;FLandroid/support/v4/widget/ViewDragHelper$Callback;)Landroid/support/v4/widget/ViewDragHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->h:Landroid/support/v4/widget/ViewDragHelper;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->h:Landroid/support/v4/widget/ViewDragHelper;

    const/high16 v2, 0x43c80000    # 400.0f

    mul-float/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/ViewDragHelper;->setMinVelocity(F)V

    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :cond_1
    move-object v4, v1

    goto/16 :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    iput v2, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->s:I

    goto :goto_3
.end method

.method private a(I)V
    .locals 2

    iget v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->i:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->i:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->b()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->i:I

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->u:Lcom/twitter/internal/android/widget/o;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/o;->b(I)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->e:Lcom/twitter/internal/android/widget/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->e:Lcom/twitter/internal/android/widget/n;

    invoke-interface {v0}, Lcom/twitter/internal/android/widget/n;->c()V

    goto :goto_0
.end method

.method private a(Landroid/view/View;II)V
    .locals 4

    const/high16 v3, 0x40000000    # 2.0f

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/RectLayoutParams;

    iget v1, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->leftMargin:I

    sub-int v1, p2, v1

    iget v2, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->rightMargin:I

    sub-int/2addr v1, v2

    iget v2, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->topMargin:I

    sub-int v2, p3, v2

    iget v0, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->bottomMargin:I

    sub-int v0, v2, v0

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;ZIII)Z
    .locals 10

    const/4 v2, 0x1

    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    move-object v6, p1

    check-cast v6, Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    move-result v8

    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v9

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v7, v0

    :goto_0
    if-ltz v7, :cond_2

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    add-int v0, p4, v8

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v3

    if-lt v0, v3, :cond_1

    add-int v0, p4, v8

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v3

    if-ge v0, v3, :cond_1

    add-int v0, p5, v9

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    if-lt v0, v3, :cond_1

    add-int v0, p5, v9

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v3

    if-ge v0, v3, :cond_1

    add-int v0, p4, v8

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v3

    sub-int v4, v0, v3

    add-int v0, p5, v9

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    sub-int v5, v0, v3

    move-object v0, p0

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->a(Landroid/view/View;ZIII)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_1
    return v2

    :cond_1
    add-int/lit8 v0, v7, -0x1

    move v7, v0

    goto :goto_0

    :cond_2
    if-eqz p2, :cond_3

    neg-int v0, p3

    invoke-static {p1, v0}, Landroid/support/v4/view/ViewCompat;->canScrollHorizontally(Landroid/view/View;I)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private b(I)V
    .locals 2

    iget v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->i:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->i:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->i:I

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->u:Lcom/twitter/internal/android/widget/o;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/o;->c(I)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->e:Lcom/twitter/internal/android/widget/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->e:Lcom/twitter/internal/android/widget/n;

    invoke-interface {v0}, Lcom/twitter/internal/android/widget/n;->d()V

    goto :goto_0
.end method


# virtual methods
.method a()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->d:Landroid/view/View;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->a(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->d:Landroid/view/View;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->b(Landroid/view/View;I)V

    return-void
.end method

.method a(Landroid/view/View;I)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/RectLayoutParams;

    iget v1, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->topMargin:I

    add-int/2addr v1, p2

    iput v1, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->b:I

    iget v1, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->b:I

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->d:I

    goto :goto_0
.end method

.method b()V
    .locals 4

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->z:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    iget v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->a:I

    const/4 v3, 0x3

    if-ne v0, v3, :cond_1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->getMeasuredWidth()I

    move-result v0

    sub-int/2addr v0, v2

    :goto_1
    add-int/2addr v2, v0

    invoke-virtual {v1, v2}, Landroid/view/View;->offsetLeftAndRight(I)V

    invoke-virtual {p0, v1, v0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->b(Landroid/view/View;I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->z:Z

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->invalidate()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method b(Landroid/view/View;I)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/RectLayoutParams;

    iget v1, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->leftMargin:I

    add-int/2addr v1, p2

    iput v1, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->a:I

    iget v1, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->a:I

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->c:I

    return-void
.end method

.method c()V
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->z:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    neg-int v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->offsetLeftAndRight(I)V

    invoke-virtual {p0, v0, v1}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->b(Landroid/view/View;I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->z:Z

    goto :goto_0
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    instance-of v0, p1, Lcom/twitter/internal/android/widget/RectLayoutParams;

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public computeScroll()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->h:Landroid/support/v4/widget/ViewDragHelper;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/ViewDragHelper;->continueSettling(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public d()Z
    .locals 2

    iget v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->i:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->d:Landroid/view/View;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v1, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->r:Landroid/graphics/Rect;

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->b:Z

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->a:I

    if-nez v0, :cond_4

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->getHeight()I

    move-result v3

    invoke-virtual {v1, v4, v0, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    :goto_0
    iget v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->B:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->i:I

    if-eq v0, v5, :cond_1

    iget v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->i:I

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    :cond_0
    iget v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->i:I

    const/4 v2, 0x5

    if-ne v0, v2, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->o:Landroid/graphics/Paint;

    iget v2, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->B:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->o:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_2
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    iget v2, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->f:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_3

    iget v2, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->p:I

    const/high16 v3, -0x1000000

    and-int/2addr v2, v3

    ushr-int/lit8 v2, v2, 0x18

    int-to-float v2, v2

    iget v3, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->f:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    shl-int/lit8 v2, v2, 0x18

    iget v3, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->p:I

    const v4, 0xffffff

    and-int/2addr v3, v4

    or-int/2addr v2, v3

    iget-object v3, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->o:Landroid/graphics/Paint;

    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->o:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_3
    :goto_1
    return v0

    :cond_4
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->getWidth()I

    move-result v0

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v2

    invoke-virtual {v1, v4, v4, v0, v2}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    :cond_5
    iget v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->a:I

    if-ne v0, v5, :cond_6

    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->getHeight()I

    move-result v3

    invoke-virtual {v1, v0, v4, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    :cond_6
    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->getHeight()I

    move-result v2

    invoke-virtual {v1, v4, v4, v0, v2}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    :cond_7
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    goto :goto_1
.end method

.method public e()V
    .locals 1

    iget v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->l:I

    invoke-direct {p0, v0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->a(I)V

    return-void
.end method

.method public f()V
    .locals 1

    iget v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->m:I

    invoke-direct {p0, v0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->b(I)V

    return-void
.end method

.method public g()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->b(I)V

    return-void
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    const/4 v1, -0x1

    new-instance v0, Lcom/twitter/internal/android/widget/RectLayoutParams;

    invoke-direct {v0, v1, v1}, Lcom/twitter/internal/android/widget/RectLayoutParams;-><init>(II)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    new-instance v0, Lcom/twitter/internal/android/widget/RectLayoutParams;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/twitter/internal/android/widget/RectLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    new-instance v0, Lcom/twitter/internal/android/widget/RectLayoutParams;

    invoke-direct {v0, p1}, Lcom/twitter/internal/android/widget/RectLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method h()V
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->f:F

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->f:F

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "HiddenDrawerLayout must contain at least two views."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->c:Landroid/view/View;

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->d:Landroid/view/View;

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v6, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return v2

    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v1, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v3, v0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->n:Landroid/graphics/Rect;

    iget v4, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->i:I

    iget-object v5, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->d:Landroid/view/View;

    invoke-virtual {v5, v0}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v5

    iget v7, v0, Landroid/graphics/Rect;->left:I

    iget v8, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->s:I

    add-int/2addr v7, v8

    iput v7, v0, Landroid/graphics/Rect;->right:I

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v7

    iget-boolean v8, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->t:Z

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->A:Landroid/view/View;

    if-eqz v8, :cond_7

    iget-object v8, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->A:Landroid/view/View;

    invoke-virtual {v8, v0}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    :goto_1
    if-eqz v7, :cond_1

    iget-boolean v7, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->g:Z

    if-nez v7, :cond_2

    :cond_1
    if-eqz v0, :cond_8

    :cond_2
    move v0, v6

    :goto_2
    iput-boolean v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->k:Z

    if-eqz v5, :cond_9

    if-eq v4, v9, :cond_3

    if-ne v4, v6, :cond_9

    :cond_3
    move v0, v6

    :goto_3
    iput-boolean v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->j:Z

    :cond_4
    if-eq v4, v9, :cond_5

    if-ne v4, v6, :cond_6

    :cond_5
    if-eqz v5, :cond_6

    iput-boolean v6, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->v:Z

    move v2, v6

    :cond_6
    int-to-float v0, v1

    iput v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->w:F

    int-to-float v0, v3

    iput v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->x:F

    goto :goto_0

    :cond_7
    move v0, v2

    goto :goto_1

    :cond_8
    move v0, v2

    goto :goto_2

    :cond_9
    move v0, v2

    goto :goto_3

    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget v3, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->w:F

    sub-float v7, v0, v3

    iget v3, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->x:F

    sub-float v3, v1, v3

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->y:I

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-ltz v4, :cond_0

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const/high16 v5, 0x3f000000    # 0.5f

    mul-float/2addr v4, v5

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v3, v4, v3

    if-lez v3, :cond_0

    iget-boolean v3, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->k:Z

    if-eqz v3, :cond_a

    cmpl-float v3, v7, v8

    if-lez v3, :cond_a

    float-to-int v3, v7

    float-to-int v4, v0

    float-to-int v5, v1

    move-object v0, p0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->a(Landroid/view/View;ZIII)Z

    move-result v0

    if-nez v0, :cond_a

    move v0, v6

    :goto_4
    if-eqz v0, :cond_b

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->setAction(I)V

    iget-object v1, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->h:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v1, p1}, Landroid/support/v4/widget/ViewDragHelper;->processTouchEvent(Landroid/view/MotionEvent;)V

    iput-boolean v2, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->v:Z

    move v2, v0

    goto/16 :goto_0

    :cond_a
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->j:Z

    if-eqz v0, :cond_c

    cmpg-float v0, v7, v8

    if-gez v0, :cond_c

    move v0, v6

    goto :goto_4

    :pswitch_2
    iput v8, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->w:F

    iput v8, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->x:F

    iput-boolean v2, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->v:Z

    iput-boolean v2, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->j:Z

    iput-boolean v2, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->k:Z

    goto/16 :goto_0

    :cond_b
    move v2, v0

    goto/16 :goto_0

    :cond_c
    move v0, v2

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->getChildCount()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v4, 0x8

    if-ne v0, v4, :cond_0

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/RectLayoutParams;

    iget v4, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->a:I

    iget v5, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->b:I

    iget v6, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->c:I

    iget v0, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->d:I

    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/view/View;->layout(IIII)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 7

    const/high16 v4, 0x40000000    # 2.0f

    const/4 v6, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    if-ne v0, v4, :cond_0

    if-eq v1, v4, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "DrawerLayout must be measured with MeasureSpec.EXACTLY."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {p0, v2, v3}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->setMeasuredDimension(II)V

    iget-object v1, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->c:Landroid/view/View;

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->b:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->q:I

    sub-int v0, v3, v0

    invoke-direct {p0, v1, v2, v0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->a(Landroid/view/View;II)V

    :goto_0
    iget v4, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->a:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_3

    sub-int v4, v3, v0

    invoke-virtual {p0, v1, v4}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->a(Landroid/view/View;I)V

    :goto_1
    iget-object v4, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->u:Lcom/twitter/internal/android/widget/o;

    invoke-virtual {v4, v0}, Lcom/twitter/internal/android/widget/o;->a(I)V

    iget-object v4, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->d:Landroid/view/View;

    invoke-direct {p0, v4, v2, v3}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->a(Landroid/view/View;II)V

    iget v3, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->i:I

    packed-switch v3, :pswitch_data_0

    :goto_2
    return-void

    :cond_2
    iget v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->q:I

    sub-int v0, v2, v0

    invoke-direct {p0, v1, v0, v3}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->a(Landroid/view/View;II)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v1, v6}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->a(Landroid/view/View;I)V

    goto :goto_1

    :pswitch_0
    iget-boolean v3, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->b:Z

    if-eqz v3, :cond_5

    iget v3, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->a:I

    if-nez v3, :cond_4

    invoke-virtual {p0, v4, v0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->a(Landroid/view/View;I)V

    :goto_3
    invoke-virtual {p0, v4, v6}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->b(Landroid/view/View;I)V

    :goto_4
    iget v3, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->a:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_7

    sub-int v0, v2, v0

    invoke-virtual {p0, v1, v0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->b(Landroid/view/View;I)V

    goto :goto_2

    :cond_4
    neg-int v3, v0

    invoke-virtual {p0, v4, v3}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->a(Landroid/view/View;I)V

    goto :goto_3

    :cond_5
    iget v3, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->a:I

    const/4 v5, 0x2

    if-ne v3, v5, :cond_6

    invoke-virtual {p0, v4, v0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->b(Landroid/view/View;I)V

    :goto_5
    invoke-virtual {p0, v4, v6}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->a(Landroid/view/View;I)V

    goto :goto_4

    :cond_6
    neg-int v3, v0

    invoke-virtual {p0, v4, v3}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->b(Landroid/view/View;I)V

    goto :goto_5

    :cond_7
    invoke-virtual {p0, v1, v6}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->b(Landroid/view/View;I)V

    goto :goto_2

    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->a()V

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    neg-int v0, v0

    invoke-virtual {p0, v1, v0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->b(Landroid/view/View;I)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    iget-object v1, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->h:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v1, p1}, Landroid/support/v4/widget/ViewDragHelper;->processTouchEvent(Landroid/view/MotionEvent;)V

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    :pswitch_0
    iget v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->w:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->x:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->j:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->k:Z

    if-eqz v1, :cond_0

    :cond_1
    iget v1, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->y:I

    iget v2, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->y:I

    mul-int/2addr v1, v2

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iput-boolean v3, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->v:Z

    goto :goto_0

    :pswitch_1
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->v:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->f()V

    :cond_2
    iput-boolean v3, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->j:Z

    iput-boolean v3, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->k:Z

    iput v2, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->w:F

    iput v2, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->x:F

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setDragHandle(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->A:Landroid/view/View;

    return-void
.end method

.method public setDrawerListener(Lcom/twitter/internal/android/widget/n;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->e:Lcom/twitter/internal/android/widget/n;

    return-void
.end method

.method public setDrawerOpenable(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->g:Z

    return-void
.end method
