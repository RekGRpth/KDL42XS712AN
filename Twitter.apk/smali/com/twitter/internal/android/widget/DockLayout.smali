.class public Lcom/twitter/internal/android/widget/DockLayout;
.super Landroid/view/ViewGroup;
.source "Twttr"


# instance fields
.field private A:Z

.field private B:Z

.field private C:Z

.field private D:Z

.field private E:I

.field private F:I

.field private G:I

.field private H:I

.field private I:I

.field private J:I

.field private K:Z

.field private final a:I

.field private final b:I

.field private final c:Lcom/twitter/internal/android/widget/i;

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:Landroid/graphics/Rect;

.field private k:Landroid/view/View;

.field private l:Landroid/view/View;

.field private m:Lcom/twitter/internal/android/widget/h;

.field private n:I

.field private o:I

.field private p:I

.field private q:Z

.field private r:I

.field private s:Landroid/view/VelocityTracker;

.field private t:I

.field private u:Lcom/twitter/internal/android/widget/k;

.field private v:Lcom/twitter/internal/android/widget/k;

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    sget v1, Lcom/twitter/internal/android/b;->dockLayoutStyle:I

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/internal/android/widget/DockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget v0, Lcom/twitter/internal/android/b;->dockLayoutStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/internal/android/widget/DockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    const/16 v1, 0x2710

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->j:Landroid/graphics/Rect;

    iput v4, p0, Lcom/twitter/internal/android/widget/DockLayout;->r:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->t:I

    iput v1, p0, Lcom/twitter/internal/android/widget/DockLayout;->F:I

    iput v1, p0, Lcom/twitter/internal/android/widget/DockLayout;->I:I

    sget-object v0, Lcom/twitter/internal/android/f;->DockLayout:[I

    invoke-virtual {p1, p2, v0, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/DockLayout;->a:I

    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/DockLayout;->b:I

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    iput v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->f:I

    new-instance v2, Lcom/twitter/internal/android/widget/i;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/twitter/internal/android/widget/i;-><init>(Lcom/twitter/internal/android/widget/DockLayout;Lcom/twitter/internal/android/widget/g;)V

    iput-object v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->c:Lcom/twitter/internal/android/widget/i;

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v2

    iput v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->d:I

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/DockLayout;->e:I

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    const/4 v2, 0x3

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/DockLayout;->g:I

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/DockLayout;->h:I

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/DockLayout;->i:I

    const/4 v1, 0x6

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/internal/android/widget/DockLayout;->x:Z

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/internal/android/widget/DockLayout;I)I
    .locals 0

    iput p1, p0, Lcom/twitter/internal/android/widget/DockLayout;->F:I

    return p1
.end method

.method private a(II)Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/DockLayout;->k:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/DockLayout;->k:Landroid/view/View;

    iget-object v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->j:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    iget-object v1, p0, Lcom/twitter/internal/android/widget/DockLayout;->j:Landroid/graphics/Rect;

    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    or-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Lcom/twitter/internal/android/widget/DockLayout;->l:Landroid/view/View;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/internal/android/widget/DockLayout;->l:Landroid/view/View;

    iget-object v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->j:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    iget-object v1, p0, Lcom/twitter/internal/android/widget/DockLayout;->j:Landroid/graphics/Rect;

    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    or-int/2addr v0, v1

    :cond_1
    return v0
.end method

.method static synthetic a(Lcom/twitter/internal/android/widget/DockLayout;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->y:Z

    return v0
.end method

.method static synthetic b(Lcom/twitter/internal/android/widget/DockLayout;I)I
    .locals 0

    iput p1, p0, Lcom/twitter/internal/android/widget/DockLayout;->I:I

    return p1
.end method

.method static synthetic b(Lcom/twitter/internal/android/widget/DockLayout;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->z:Z

    return v0
.end method

.method static synthetic c(Lcom/twitter/internal/android/widget/DockLayout;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/DockLayout;->d()V

    return-void
.end method

.method static synthetic d(Lcom/twitter/internal/android/widget/DockLayout;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->k:Landroid/view/View;

    return-object v0
.end method

.method private d()V
    .locals 8

    const/16 v7, 0x2710

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->k:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/DockLayout;->l:Landroid/view/View;

    if-eqz v0, :cond_0

    iget v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->E:I

    iget v3, p0, Lcom/twitter/internal/android/widget/DockLayout;->F:I

    iget v4, p0, Lcom/twitter/internal/android/widget/DockLayout;->G:I

    mul-int/2addr v3, v4

    div-int/lit16 v3, v3, 0x2710

    add-int/2addr v2, v3

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    sub-int v3, v2, v3

    if-eqz v3, :cond_0

    invoke-virtual {v0, v3}, Landroid/view/View;->offsetTopAndBottom(I)V

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v3

    invoke-virtual {p0, v0, v3, v2}, Lcom/twitter/internal/android/widget/DockLayout;->a(Landroid/view/View;II)Lcom/twitter/internal/android/widget/RectLayoutParams;

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/DockLayout;->f()V

    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->F:I

    if-nez v0, :cond_2

    iput-boolean v5, p0, Lcom/twitter/internal/android/widget/DockLayout;->A:Z

    iput-boolean v6, p0, Lcom/twitter/internal/android/widget/DockLayout;->B:Z

    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->H:I

    iget v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->I:I

    iget v3, p0, Lcom/twitter/internal/android/widget/DockLayout;->J:I

    mul-int/2addr v2, v3

    div-int/lit16 v2, v2, 0x2710

    sub-int/2addr v0, v2

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    sub-int v2, v0, v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v2}, Landroid/view/View;->offsetTopAndBottom(I)V

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, Lcom/twitter/internal/android/widget/DockLayout;->a(Landroid/view/View;II)Lcom/twitter/internal/android/widget/RectLayoutParams;

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/DockLayout;->g()V

    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->I:I

    if-nez v0, :cond_4

    iput-boolean v5, p0, Lcom/twitter/internal/android/widget/DockLayout;->C:Z

    iput-boolean v6, p0, Lcom/twitter/internal/android/widget/DockLayout;->D:Z

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->F:I

    if-ne v0, v7, :cond_3

    iput-boolean v5, p0, Lcom/twitter/internal/android/widget/DockLayout;->A:Z

    iput-boolean v5, p0, Lcom/twitter/internal/android/widget/DockLayout;->B:Z

    goto :goto_0

    :cond_3
    iput-boolean v6, p0, Lcom/twitter/internal/android/widget/DockLayout;->A:Z

    goto :goto_0

    :cond_4
    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->I:I

    if-ne v0, v7, :cond_5

    iput-boolean v5, p0, Lcom/twitter/internal/android/widget/DockLayout;->C:Z

    iput-boolean v5, p0, Lcom/twitter/internal/android/widget/DockLayout;->D:Z

    goto :goto_1

    :cond_5
    iput-boolean v6, p0, Lcom/twitter/internal/android/widget/DockLayout;->C:Z

    goto :goto_1
.end method

.method static synthetic e(Lcom/twitter/internal/android/widget/DockLayout;)I
    .locals 1

    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->F:I

    return v0
.end method

.method private e()V
    .locals 5

    const/16 v4, 0x1388

    const/16 v3, 0xfa

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->k:Landroid/view/View;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->l:Landroid/view/View;

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->k:Landroid/view/View;

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->F:I

    if-ge v2, v4, :cond_2

    :cond_1
    :goto_1
    if-eqz v0, :cond_4

    invoke-virtual {p0, v3}, Lcom/twitter/internal/android/widget/DockLayout;->b(I)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->l:Landroid/view/View;

    if-eqz v2, :cond_5

    iget v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->I:I

    if-lt v2, v4, :cond_1

    move v0, v1

    goto :goto_1

    :cond_4
    invoke-virtual {p0, v3}, Lcom/twitter/internal/android/widget/DockLayout;->a(I)V

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method static synthetic f(Lcom/twitter/internal/android/widget/DockLayout;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->l:Landroid/view/View;

    return-object v0
.end method

.method private f()V
    .locals 5

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->m:Lcom/twitter/internal/android/widget/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->k:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->m:Lcom/twitter/internal/android/widget/h;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/DockLayout;->k:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->k:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    iget-object v3, p0, Lcom/twitter/internal/android/widget/DockLayout;->k:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v3

    iget-object v4, p0, Lcom/twitter/internal/android/widget/DockLayout;->k:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/twitter/internal/android/widget/h;->a(IIII)V

    :cond_0
    return-void
.end method

.method static synthetic g(Lcom/twitter/internal/android/widget/DockLayout;)I
    .locals 1

    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->I:I

    return v0
.end method

.method private g()V
    .locals 5

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->m:Lcom/twitter/internal/android/widget/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->l:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->m:Lcom/twitter/internal/android/widget/h;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/DockLayout;->l:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->l:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    iget-object v3, p0, Lcom/twitter/internal/android/widget/DockLayout;->l:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v3

    iget-object v4, p0, Lcom/twitter/internal/android/widget/DockLayout;->l:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/twitter/internal/android/widget/h;->b(IIII)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected a(Landroid/view/View;II)Lcom/twitter/internal/android/widget/RectLayoutParams;
    .locals 3

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/RectLayoutParams;

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, p2

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v2, p3

    invoke-virtual {v0, p2, p3, v1, v2}, Lcom/twitter/internal/android/widget/RectLayoutParams;->a(IIII)V

    goto :goto_0
.end method

.method public a()V
    .locals 1

    const/16 v0, 0xfa

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/DockLayout;->a(I)V

    return-void
.end method

.method public a(I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->k:Landroid/view/View;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->l:Landroid/view/View;

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->c:Lcom/twitter/internal/android/widget/i;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/i;->b(I)V

    :cond_1
    return-void
.end method

.method public b()V
    .locals 1

    const/16 v0, 0xfa

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/DockLayout;->b(I)V

    return-void
.end method

.method public b(I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->k:Landroid/view/View;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->l:Landroid/view/View;

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->c:Lcom/twitter/internal/android/widget/i;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/i;->a(I)V

    :cond_1
    return-void
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->x:Z

    return v0
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    instance-of v0, p1, Lcom/twitter/internal/android/widget/RectLayoutParams;

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    const/4 v9, 0x2

    const/4 v5, -0x1

    const/16 v8, 0x2710

    const/4 v7, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lcom/twitter/internal/android/widget/DockLayout;->onFilterTouchEventForSecurity(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->s:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->s:Landroid/view/VelocityTracker;

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->s:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    :cond_1
    :goto_1
    return v1

    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-direct {p0, v0, v2}, Lcom/twitter/internal/android/widget/DockLayout;->a(II)Z

    move-result v3

    if-eqz v3, :cond_2

    iput-boolean v7, p0, Lcom/twitter/internal/android/widget/DockLayout;->w:Z

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_1

    :cond_2
    iput v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->o:I

    iput v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->p:I

    iput v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->n:I

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->t:I

    goto :goto_0

    :pswitch_2
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->w:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->q:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->x:Z

    if-eqz v0, :cond_4

    :cond_3
    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->r:I

    if-eq v0, v7, :cond_4

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->s:Landroid/view/VelocityTracker;

    const/16 v2, 0x3e8

    iget v3, p0, Lcom/twitter/internal/android/widget/DockLayout;->e:I

    int-to-float v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->s:Landroid/view/VelocityTracker;

    iget v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->t:I

    invoke-static {v0, v2}, Landroid/support/v4/view/VelocityTrackerCompat;->getYVelocity(Landroid/view/VelocityTracker;I)F

    move-result v0

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v2

    iget v3, p0, Lcom/twitter/internal/android/widget/DockLayout;->d:I

    if-le v2, v3, :cond_6

    if-gez v0, :cond_5

    const/16 v0, 0x64

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/DockLayout;->b(I)V

    :cond_4
    :goto_2
    iput v1, p0, Lcom/twitter/internal/android/widget/DockLayout;->r:I

    iput v5, p0, Lcom/twitter/internal/android/widget/DockLayout;->t:I

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->s:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->s:Landroid/view/VelocityTracker;

    iput-boolean v1, p0, Lcom/twitter/internal/android/widget/DockLayout;->w:Z

    goto :goto_0

    :cond_5
    const/16 v0, 0x64

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/DockLayout;->a(I)V

    goto :goto_2

    :cond_6
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/DockLayout;->e()V

    goto :goto_2

    :pswitch_3
    iget-object v3, p0, Lcom/twitter/internal/android/widget/DockLayout;->k:Landroid/view/View;

    iget-object v4, p0, Lcom/twitter/internal/android/widget/DockLayout;->l:Landroid/view/View;

    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->t:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    if-ne v0, v5, :cond_7

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->t:I

    move v0, v1

    :cond_7
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    float-to-int v5, v2

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    float-to-int v0, v0

    iget-boolean v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->w:Z

    if-nez v2, :cond_9

    iget-boolean v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->q:Z

    if-nez v2, :cond_9

    if-nez v3, :cond_8

    if-eqz v4, :cond_9

    :cond_8
    iget v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->r:I

    if-ne v2, v7, :cond_a

    :cond_9
    iput v5, p0, Lcom/twitter/internal/android/widget/DockLayout;->n:I

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto/16 :goto_1

    :cond_a
    iget v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->r:I

    if-nez v2, :cond_b

    iget v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->p:I

    sub-int/2addr v2, v5

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    iget v6, p0, Lcom/twitter/internal/android/widget/DockLayout;->f:I

    if-le v2, v6, :cond_e

    iput v9, p0, Lcom/twitter/internal/android/widget/DockLayout;->r:I

    :cond_b
    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->r:I

    if-ne v0, v9, :cond_d

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->K:Z

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->c:Lcom/twitter/internal/android/widget/i;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/i;->b()V

    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->i:I

    if-nez v0, :cond_f

    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->n:I

    sub-int/2addr v0, v5

    mul-int/lit16 v0, v0, 0x2710

    iget v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->G:I

    div-int/2addr v0, v2

    move v2, v0

    :goto_3
    if-lez v2, :cond_11

    if-eqz v3, :cond_c

    iget-boolean v3, p0, Lcom/twitter/internal/android/widget/DockLayout;->y:Z

    if-nez v3, :cond_c

    iget v3, p0, Lcom/twitter/internal/android/widget/DockLayout;->F:I

    if-lez v3, :cond_c

    iget v3, p0, Lcom/twitter/internal/android/widget/DockLayout;->F:I

    sub-int v2, v3, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->F:I

    iget v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->F:I

    if-nez v2, :cond_c

    invoke-virtual {p0, v7}, Lcom/twitter/internal/android/widget/DockLayout;->setTopDocked(Z)V

    :cond_c
    if-eqz v4, :cond_d

    iget-boolean v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->z:Z

    if-nez v2, :cond_d

    iget v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->I:I

    if-lez v2, :cond_d

    iget v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->I:I

    sub-int v0, v2, v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->I:I

    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->I:I

    if-nez v0, :cond_d

    invoke-virtual {p0, v7}, Lcom/twitter/internal/android/widget/DockLayout;->setBottomDocked(Z)V

    :cond_d
    :goto_4
    iput v5, p0, Lcom/twitter/internal/android/widget/DockLayout;->n:I

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/DockLayout;->d()V

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/DockLayout;->invalidate()V

    goto/16 :goto_0

    :cond_e
    iget v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->o:I

    sub-int v0, v2, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->f:I

    if-le v0, v2, :cond_b

    iput v7, p0, Lcom/twitter/internal/android/widget/DockLayout;->r:I

    iput v5, p0, Lcom/twitter/internal/android/widget/DockLayout;->n:I

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto/16 :goto_1

    :cond_f
    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->i:I

    if-ne v0, v7, :cond_10

    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->n:I

    sub-int/2addr v0, v5

    mul-int/lit16 v0, v0, 0x2710

    iget v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->J:I

    div-int/2addr v0, v2

    move v2, v0

    goto :goto_3

    :cond_10
    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->n:I

    sub-int/2addr v0, v5

    mul-int/lit16 v0, v0, 0x2710

    iget v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->G:I

    div-int v2, v0, v2

    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->n:I

    sub-int/2addr v0, v5

    mul-int/lit16 v0, v0, 0x2710

    iget v6, p0, Lcom/twitter/internal/android/widget/DockLayout;->J:I

    div-int/2addr v0, v6

    goto :goto_3

    :cond_11
    if-eqz v3, :cond_12

    iget v3, p0, Lcom/twitter/internal/android/widget/DockLayout;->F:I

    sub-int v2, v3, v2

    invoke-static {v8, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->F:I

    iget v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->F:I

    if-ne v2, v8, :cond_12

    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/DockLayout;->setTopDocked(Z)V

    :cond_12
    if-eqz v4, :cond_d

    iget-boolean v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->z:Z

    if-nez v2, :cond_d

    iget v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->I:I

    if-ge v2, v8, :cond_d

    iget v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->I:I

    sub-int v0, v2, v0

    invoke-static {v8, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->I:I

    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->I:I

    if-ne v0, v8, :cond_d

    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/DockLayout;->setBottomDocked(Z)V

    goto :goto_4

    :pswitch_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    float-to-int v2, v2

    invoke-direct {p0, v1, v2}, Lcom/twitter/internal/android/widget/DockLayout;->a(II)Z

    move-result v3

    if-eqz v3, :cond_13

    iput-boolean v7, p0, Lcom/twitter/internal/android/widget/DockLayout;->w:Z

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto/16 :goto_1

    :cond_13
    iput v1, p0, Lcom/twitter/internal/android/widget/DockLayout;->o:I

    iput v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->p:I

    iput v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->n:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->t:I

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    const/4 v1, -0x1

    new-instance v0, Lcom/twitter/internal/android/widget/RectLayoutParams;

    invoke-direct {v0, v1, v1}, Lcom/twitter/internal/android/widget/RectLayoutParams;-><init>(II)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    new-instance v0, Lcom/twitter/internal/android/widget/RectLayoutParams;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/DockLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/twitter/internal/android/widget/RectLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    new-instance v0, Lcom/twitter/internal/android/widget/RectLayoutParams;

    invoke-direct {v0, p1}, Lcom/twitter/internal/android/widget/RectLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method protected final getBottomDockView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->l:Landroid/view/View;

    return-object v0
.end method

.method getBottomPeek()I
    .locals 2

    iget v1, p0, Lcom/twitter/internal/android/widget/DockLayout;->h:I

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->l:Landroid/view/View;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    goto :goto_0
.end method

.method protected final getTopDockView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->k:Landroid/view/View;

    return-object v0
.end method

.method getTopPeek()I
    .locals 2

    iget v1, p0, Lcom/twitter/internal/android/widget/DockLayout;->g:I

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->k:Landroid/view/View;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/DockLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "DockLayout must contain at least two children"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->a:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->a:I

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/DockLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->k:Landroid/view/View;

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->k:Landroid/view/View;

    instance-of v0, v0, Lcom/twitter/internal/android/widget/k;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->k:Landroid/view/View;

    check-cast v0, Lcom/twitter/internal/android/widget/k;

    iput-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->u:Lcom/twitter/internal/android/widget/k;

    :cond_1
    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->b:I

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->b:I

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/DockLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->l:Landroid/view/View;

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->l:Landroid/view/View;

    instance-of v0, v0, Lcom/twitter/internal/android/widget/k;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->l:Landroid/view/View;

    check-cast v0, Lcom/twitter/internal/android/widget/k;

    iput-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->v:Lcom/twitter/internal/android/widget/k;

    :cond_2
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/DockLayout;->getChildCount()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/DockLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/RectLayoutParams;

    iget v4, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->a:I

    iget v5, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->b:I

    iget v6, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->c:I

    iget v0, v0, Lcom/twitter/internal/android/widget/RectLayoutParams;->d:I

    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/view/View;->layout(IIII)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->c:Lcom/twitter/internal/android/widget/i;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/i;->a()V

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/DockLayout;->f()V

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/DockLayout;->g()V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 9

    const/4 v6, 0x1

    const/4 v3, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/internal/android/widget/DockLayout;->setMeasuredDimension(II)V

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/DockLayout;->getChildCount()I

    move-result v8

    move v7, v3

    :goto_0
    if-ge v7, v8, :cond_1

    invoke-virtual {p0, v7}, Lcom/twitter/internal/android/widget/DockLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_0

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/internal/android/widget/DockLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    invoke-virtual {p0, v1, v3, v3}, Lcom/twitter/internal/android/widget/DockLayout;->a(Landroid/view/View;II)Lcom/twitter/internal/android/widget/RectLayoutParams;

    :cond_0
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->k:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/DockLayout;->l:Landroid/view/View;

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/DockLayout;->getTopPeek()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    sub-int/2addr v2, v4

    iput v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->E:I

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/DockLayout;->getTopPeek()I

    move-result v4

    sub-int/2addr v2, v4

    iput v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->G:I

    iget v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->E:I

    iget v4, p0, Lcom/twitter/internal/android/widget/DockLayout;->F:I

    iget v5, p0, Lcom/twitter/internal/android/widget/DockLayout;->G:I

    mul-int/2addr v4, v5

    div-int/lit16 v4, v4, 0x2710

    add-int/2addr v2, v4

    invoke-virtual {p0, v0, v3, v2}, Lcom/twitter/internal/android/widget/DockLayout;->a(Landroid/view/View;II)Lcom/twitter/internal/android/widget/RectLayoutParams;

    :goto_1
    if-eqz v1, :cond_6

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/DockLayout;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/DockLayout;->getBottomPeek()I

    move-result v2

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->H:I

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/DockLayout;->getBottomPeek()I

    move-result v2

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->J:I

    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->H:I

    iget v2, p0, Lcom/twitter/internal/android/widget/DockLayout;->I:I

    iget v4, p0, Lcom/twitter/internal/android/widget/DockLayout;->J:I

    mul-int/2addr v2, v4

    div-int/lit16 v2, v2, 0x2710

    sub-int/2addr v0, v2

    invoke-virtual {p0, v1, v3, v0}, Lcom/twitter/internal/android/widget/DockLayout;->a(Landroid/view/View;II)Lcom/twitter/internal/android/widget/RectLayoutParams;

    :goto_2
    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->G:I

    if-nez v0, :cond_2

    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->J:I

    if-eqz v0, :cond_3

    :cond_2
    move v3, v6

    :cond_3
    iput-boolean v3, p0, Lcom/twitter/internal/android/widget/DockLayout;->K:Z

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->K:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->i:I

    if-nez v0, :cond_7

    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->G:I

    if-nez v0, :cond_7

    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->J:I

    iput v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->G:I

    :cond_4
    :goto_3
    return-void

    :cond_5
    iput v3, p0, Lcom/twitter/internal/android/widget/DockLayout;->G:I

    goto :goto_1

    :cond_6
    iput v3, p0, Lcom/twitter/internal/android/widget/DockLayout;->J:I

    goto :goto_2

    :cond_7
    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->i:I

    if-ne v0, v6, :cond_4

    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->J:I

    if-nez v0, :cond_4

    iget v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->G:I

    iput v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->J:I

    goto :goto_3
.end method

.method public setAutoUnlockEnabled(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/DockLayout;->x:Z

    return-void
.end method

.method setBottomDocked(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->z:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->C:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->D:Z

    if-eq p1, v0, :cond_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->v:Lcom/twitter/internal/android/widget/k;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->v:Lcom/twitter/internal/android/widget/k;

    invoke-interface {v0, p1}, Lcom/twitter/internal/android/widget/k;->setDocked(Z)V

    :cond_3
    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->m:Lcom/twitter/internal/android/widget/h;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_4

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->m:Lcom/twitter/internal/android/widget/h;

    invoke-interface {v0}, Lcom/twitter/internal/android/widget/h;->a()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->m:Lcom/twitter/internal/android/widget/h;

    invoke-interface {v0}, Lcom/twitter/internal/android/widget/h;->d()V

    goto :goto_0
.end method

.method public setBottomLocked(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->z:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/DockLayout;->z:Z

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/DockLayout;->e()V

    :cond_0
    return-void
.end method

.method public setBottomView(Landroid/view/View;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/internal/android/widget/DockLayout;->k:Landroid/view/View;

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->k:Landroid/view/View;

    instance-of v0, v0, Lcom/twitter/internal/android/widget/k;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/twitter/internal/android/widget/k;

    iput-object p1, p0, Lcom/twitter/internal/android/widget/DockLayout;->u:Lcom/twitter/internal/android/widget/k;

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/DockLayout;->postInvalidate()V

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->u:Lcom/twitter/internal/android/widget/k;

    goto :goto_0
.end method

.method public setDockListener(Lcom/twitter/internal/android/widget/h;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/internal/android/widget/DockLayout;->m:Lcom/twitter/internal/android/widget/h;

    return-void
.end method

.method public setLocked(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->q:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/DockLayout;->q:Z

    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/DockLayout;->y:Z

    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/DockLayout;->z:Z

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/DockLayout;->e()V

    :cond_0
    return-void
.end method

.method setTopDocked(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->y:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->A:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->B:Z

    if-eq p1, v0, :cond_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->u:Lcom/twitter/internal/android/widget/k;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->u:Lcom/twitter/internal/android/widget/k;

    invoke-interface {v0, p1}, Lcom/twitter/internal/android/widget/k;->setDocked(Z)V

    :cond_3
    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->m:Lcom/twitter/internal/android/widget/h;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_4

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->m:Lcom/twitter/internal/android/widget/h;

    invoke-interface {v0}, Lcom/twitter/internal/android/widget/h;->b()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->m:Lcom/twitter/internal/android/widget/h;

    invoke-interface {v0}, Lcom/twitter/internal/android/widget/h;->c()V

    goto :goto_0
.end method

.method public setTopLocked(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->y:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/DockLayout;->y:Z

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/DockLayout;->e()V

    :cond_0
    return-void
.end method

.method public setTopView(Landroid/view/View;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/internal/android/widget/DockLayout;->k:Landroid/view/View;

    iget-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->k:Landroid/view/View;

    instance-of v0, v0, Lcom/twitter/internal/android/widget/k;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/twitter/internal/android/widget/k;

    iput-object p1, p0, Lcom/twitter/internal/android/widget/DockLayout;->u:Lcom/twitter/internal/android/widget/k;

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/DockLayout;->postInvalidate()V

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/DockLayout;->u:Lcom/twitter/internal/android/widget/k;

    goto :goto_0
.end method
