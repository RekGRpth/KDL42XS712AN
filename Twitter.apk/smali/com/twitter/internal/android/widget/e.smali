.class public Lcom/twitter/internal/android/widget/e;
.super Landroid/view/inputmethod/InputConnectionWrapper;
.source "Twttr"


# instance fields
.field private a:Lcom/twitter/internal/android/widget/f;


# direct methods
.method public constructor <init>(Landroid/view/inputmethod/InputConnection;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/view/inputmethod/InputConnectionWrapper;-><init>(Landroid/view/inputmethod/InputConnection;Z)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/internal/android/widget/f;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/internal/android/widget/e;->a:Lcom/twitter/internal/android/widget/f;

    return-void
.end method

.method public beginBatchEdit()Z
    .locals 2

    invoke-super {p0}, Landroid/view/inputmethod/InputConnectionWrapper;->beginBatchEdit()Z

    move-result v0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/e;->a:Lcom/twitter/internal/android/widget/f;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/e;->a:Lcom/twitter/internal/android/widget/f;

    invoke-interface {v1}, Lcom/twitter/internal/android/widget/f;->t()V

    :cond_0
    return v0
.end method

.method public endBatchEdit()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->a:Lcom/twitter/internal/android/widget/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/e;->a:Lcom/twitter/internal/android/widget/f;

    invoke-interface {v0}, Lcom/twitter/internal/android/widget/f;->u()V

    :cond_0
    invoke-super {p0}, Landroid/view/inputmethod/InputConnectionWrapper;->endBatchEdit()Z

    move-result v0

    return v0
.end method
