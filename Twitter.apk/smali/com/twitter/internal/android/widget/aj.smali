.class Lcom/twitter/internal/android/widget/aj;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lhn;

.field final synthetic b:Lcom/twitter/internal/android/widget/ToolBar;


# direct methods
.method constructor <init>(Lcom/twitter/internal/android/widget/ToolBar;Lhn;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/internal/android/widget/aj;->b:Lcom/twitter/internal/android/widget/ToolBar;

    iput-object p2, p0, Lcom/twitter/internal/android/widget/aj;->a:Lhn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/internal/android/widget/aj;->b:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(Lcom/twitter/internal/android/widget/ToolBar;)Lcom/twitter/internal/android/widget/ToolBarHomeView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/internal/android/widget/aj;->b:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/ToolBar;->b(Lcom/twitter/internal/android/widget/ToolBar;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhn;

    invoke-virtual {v0}, Lhn;->h()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lhn;->g()Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/twitter/internal/android/widget/aj;->b:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/ToolBar;->c(Lcom/twitter/internal/android/widget/ToolBar;)Lcom/twitter/internal/android/widget/as;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/aj;->b:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/ToolBar;->c(Lcom/twitter/internal/android/widget/ToolBar;)Lcom/twitter/internal/android/widget/as;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/aj;->a:Lhn;

    invoke-interface {v0, v1}, Lcom/twitter/internal/android/widget/as;->a(Lhn;)Z

    goto :goto_0
.end method
