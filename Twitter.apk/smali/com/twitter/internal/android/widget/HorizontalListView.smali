.class public final Lcom/twitter/internal/android/widget/HorizontalListView;
.super Landroid/widget/AdapterView;
.source "Twttr"

# interfaces
.implements Lcom/twitter/internal/android/widget/k;


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:Lcom/twitter/internal/android/widget/t;

.field private E:Lcom/twitter/internal/android/widget/v;

.field private F:I

.field private G:Landroid/database/DataSetObserver;

.field private H:I

.field private I:I

.field private J:Landroid/graphics/drawable/Drawable;

.field private K:Landroid/graphics/Rect;

.field private L:I

.field private M:I

.field private N:I

.field private O:Lcom/twitter/internal/android/widget/s;

.field private P:Landroid/graphics/drawable/Drawable;

.field private Q:I

.field private R:Landroid/graphics/Paint;

.field private S:Ljava/lang/Runnable;

.field private T:Lcom/twitter/internal/android/widget/w;

.field private U:Z

.field private V:I

.field private W:Landroid/view/VelocityTracker;

.field private Z:Z

.field private aa:Z

.field private ab:Z

.field private ac:Z

.field protected final b:Lcom/twitter/internal/util/f;

.field protected c:Z

.field protected d:Landroid/widget/ListAdapter;

.field protected e:I

.field protected f:Z

.field protected g:I

.field private final h:I

.field private final i:Lcom/twitter/internal/android/widget/x;

.field private final j:[Z

.field private final k:I

.field private final l:F

.field private final m:I

.field private final n:I

.field private final o:I

.field private final p:Lcom/twitter/internal/android/widget/u;

.field private final q:Landroid/graphics/drawable/Drawable;

.field private final r:Landroid/graphics/drawable/Drawable;

.field private final s:Landroid/graphics/drawable/Drawable;

.field private final t:Landroid/graphics/Rect;

.field private final u:I

.field private final v:I

.field private w:Lcom/twitter/internal/android/widget/y;

.field private x:Lcom/twitter/internal/android/widget/y;

.field private y:Lcom/twitter/internal/android/widget/y;

.field private z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget v0, Lcom/twitter/internal/android/b;->horizontalListViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    const/16 v5, 0xa

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/high16 v1, -0x80000000

    const/4 v4, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Lcom/twitter/internal/util/f;

    invoke-direct {v0}, Lcom/twitter/internal/util/f;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->b:Lcom/twitter/internal/util/f;

    iput v4, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->g:I

    new-instance v0, Lcom/twitter/internal/android/widget/x;

    invoke-direct {v0, p0}, Lcom/twitter/internal/android/widget/x;-><init>(Lcom/twitter/internal/android/widget/HorizontalListView;)V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->i:Lcom/twitter/internal/android/widget/x;

    new-array v0, v3, [Z

    iput-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->j:[Z

    new-instance v0, Lcom/twitter/internal/android/widget/u;

    invoke-direct {v0, p0}, Lcom/twitter/internal/android/widget/u;-><init>(Lcom/twitter/internal/android/widget/HorizontalListView;)V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->p:Lcom/twitter/internal/android/widget/u;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->t:Landroid/graphics/Rect;

    new-instance v0, Lcom/twitter/internal/android/widget/y;

    invoke-direct {v0, v2}, Lcom/twitter/internal/android/widget/y;-><init>(Lcom/twitter/internal/android/widget/q;)V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->w:Lcom/twitter/internal/android/widget/y;

    new-instance v0, Lcom/twitter/internal/android/widget/y;

    invoke-direct {v0, v2}, Lcom/twitter/internal/android/widget/y;-><init>(Lcom/twitter/internal/android/widget/q;)V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->x:Lcom/twitter/internal/android/widget/y;

    new-instance v0, Lcom/twitter/internal/android/widget/y;

    invoke-direct {v0, v2}, Lcom/twitter/internal/android/widget/y;-><init>(Lcom/twitter/internal/android/widget/q;)V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->y:Lcom/twitter/internal/android/widget/y;

    iput v4, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->C:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->F:I

    iput v4, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->I:I

    iput v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->L:I

    iput v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->M:I

    iput v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->V:I

    sget-object v0, Lcom/twitter/internal/android/f;->HorizontalListView:[I

    invoke-virtual {p1, p2, v0, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->k:I

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->l:F

    const/4 v2, 0x4

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->h:I

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->q:Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x6

    invoke-virtual {v0, v2, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->u:I

    const/4 v2, 0x7

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->v:I

    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->setDividerWidth(I)V

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    iput v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->m:I

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v2

    iput v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->n:I

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->o:I

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->r:Landroid/graphics/drawable/Drawable;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/internal/android/widget/HorizontalListView;)I
    .locals 1

    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->F:I

    return v0
.end method

.method static synthetic a(Lcom/twitter/internal/android/widget/HorizontalListView;I)I
    .locals 0

    iput p1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->F:I

    return p1
.end method

.method private a(IIIZ)Landroid/view/View;
    .locals 7

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->f:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->i:Lcom/twitter/internal/android/widget/x;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/x;->a(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v6, 0x1

    move-object v0, p0

    move v1, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(ILandroid/view/View;IIZZ)V

    :goto_0
    return-object v2

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->j:[Z

    invoke-direct {p0, p1, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(I[Z)Landroid/view/View;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->j:[Z

    const/4 v1, 0x0

    aget-boolean v6, v0, v1

    move-object v0, p0

    move v1, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(ILandroid/view/View;IIZZ)V

    goto :goto_0
.end method

.method private a(I[Z)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->i:Lcom/twitter/internal/android/widget/x;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/x;->b(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->d:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1, v1, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    if-eq v0, v1, :cond_0

    iget-object v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->i:Lcom/twitter/internal/android/widget/x;

    invoke-virtual {v2, v1, p1}, Lcom/twitter/internal/android/widget/x;->a(Landroid/view/View;I)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x1

    aput-boolean v2, p2, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->d:Landroid/widget/ListAdapter;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private a()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->W:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->W:Landroid/view/VelocityTracker;

    return-void
.end method

.method private a(IIF)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->q:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->t:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->left:I

    sub-int v1, p1, v1

    iget v2, v0, Landroid/graphics/Rect;->right:I

    sub-int v2, p2, v2

    iget-object v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->x:Lcom/twitter/internal/android/widget/y;

    invoke-virtual {v3, v1, p3}, Lcom/twitter/internal/android/widget/y;->a(IF)I

    move-result v1

    iget-object v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->y:Lcom/twitter/internal/android/widget/y;

    invoke-virtual {v3, v2, p3}, Lcom/twitter/internal/android/widget/y;->a(IF)I

    move-result v2

    iget v3, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v1

    iput v3, v0, Landroid/graphics/Rect;->left:I

    iget v3, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, v2

    iput v3, v0, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v0}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    if-nez v1, :cond_2

    if-eqz v2, :cond_0

    :cond_2
    const/4 v1, 0x0

    iget v2, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getRight()I

    move-result v3

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0, v1, v2, v3, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->invalidate(IIII)V

    goto :goto_0
.end method

.method private a(ILandroid/view/View;IIZZ)V
    .locals 5

    const/4 v1, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/HorizontalListView$LayoutParams;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/HorizontalListView$LayoutParams;

    invoke-virtual {p2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    move-object v4, v0

    if-eqz p6, :cond_5

    if-eqz p5, :cond_4

    move v0, v1

    :goto_0
    invoke-virtual {p0, p2, v0, v4}, Lcom/twitter/internal/android/widget/HorizontalListView;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    :goto_1
    if-eqz p6, :cond_1

    invoke-virtual {p2}, Landroid/view/View;->isLayoutRequested()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_1
    move v0, v3

    :goto_2
    if-eqz v0, :cond_8

    iget v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->H:I

    invoke-direct {p0, p2, v1, v4}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(Landroid/view/View;ILcom/twitter/internal/android/widget/HorizontalListView$LayoutParams;)V

    :goto_3
    invoke-virtual {p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    if-eqz p5, :cond_9

    :goto_4
    if-eqz v0, :cond_a

    add-int v0, p3, v1

    add-int v1, p4, v4

    invoke-virtual {p2, p3, p4, v0, v1}, Landroid/view/View;->layout(IIII)V

    :goto_5
    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->B:I

    if-ne p1, v0, :cond_2

    move v2, v3

    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-eq v2, v0, :cond_3

    invoke-virtual {p2, v2}, Landroid/view/View;->setSelected(Z)V

    :cond_3
    return-void

    :cond_4
    move v0, v2

    goto :goto_0

    :cond_5
    if-eqz p5, :cond_6

    :goto_6
    invoke-virtual {p0, p2, v1, v4, v3}, Lcom/twitter/internal/android/widget/HorizontalListView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    goto :goto_1

    :cond_6
    move v1, v2

    goto :goto_6

    :cond_7
    move v0, v2

    goto :goto_2

    :cond_8
    invoke-virtual {p0, p2}, Lcom/twitter/internal/android/widget/HorizontalListView;->cleanupLayoutState(Landroid/view/View;)V

    goto :goto_3

    :cond_9
    sub-int/2addr p3, v1

    goto :goto_4

    :cond_a
    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v0

    sub-int v0, p3, v0

    invoke-virtual {p2, v0}, Landroid/view/View;->offsetLeftAndRight(I)V

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v0

    sub-int v0, p4, v0

    invoke-virtual {p2, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    goto :goto_5
.end method

.method private a(IZ)V
    .locals 2

    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->I:I

    sub-int v0, p1, v0

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/view/View;->setSelected(Z)V

    :cond_0
    return-void
.end method

.method private a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;I)V
    .locals 6

    const/4 v5, 0x5

    const/4 v4, 0x3

    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    if-ne p3, v4, :cond_3

    iget v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->I:I

    if-nez v1, :cond_3

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getOverflowLeft()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    :cond_0
    :goto_0
    if-lez v0, :cond_2

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingTop()I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    if-ne p3, v4, :cond_4

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->left:I

    iget v2, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->right:I

    :cond_1
    :goto_1
    invoke-virtual {p2, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_2
    return-void

    :cond_3
    if-ne p3, v5, :cond_0

    iget v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->I:I

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildCount()I

    move-result v2

    add-int/2addr v1, v2

    iget v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->e:I

    if-ne v1, v2, :cond_0

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getOverflowRight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    :cond_4
    if-ne p3, v5, :cond_1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    iget v2, v1, Landroid/graphics/Rect;->right:I

    sub-int v0, v2, v0

    iput v0, v1, Landroid/graphics/Rect;->left:I

    goto :goto_1
.end method

.method private a(Landroid/view/View;I)V
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/HorizontalListView$LayoutParams;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/HorizontalListView$LayoutParams;

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(Landroid/view/View;ILcom/twitter/internal/android/widget/HorizontalListView$LayoutParams;)V

    return-void
.end method

.method private a(Landroid/view/View;ILcom/twitter/internal/android/widget/HorizontalListView$LayoutParams;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p3, Lcom/twitter/internal/android/widget/HorizontalListView$LayoutParams;->height:I

    invoke-static {p2, v0, v1}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v1

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->c:Z

    if-eqz v0, :cond_0

    iget v0, p3, Lcom/twitter/internal/android/widget/HorizontalListView$LayoutParams;->c:I

    :goto_0
    if-lez v0, :cond_1

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    :goto_1
    invoke-virtual {p1, v0, v1}, Landroid/view/View;->measure(II)V

    return-void

    :cond_0
    iget v0, p3, Lcom/twitter/internal/android/widget/HorizontalListView$LayoutParams;->width:I

    goto :goto_0

    :cond_1
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_1
.end method

.method static synthetic a(Lcom/twitter/internal/android/widget/HorizontalListView;Landroid/view/View;Z)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/twitter/internal/android/widget/HorizontalListView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method private a(Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/twitter/internal/android/widget/HorizontalListView;)I
    .locals 1

    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->N:I

    return v0
.end method

.method private b()V
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->setPressed(Z)V

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildCount()I

    move-result v2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/View;->setPressed(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/twitter/internal/android/widget/HorizontalListView;Landroid/view/View;Z)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/twitter/internal/android/widget/HorizontalListView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method private b(Z)V
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildCount()I

    move-result v1

    if-eqz p1, :cond_2

    if-lez v1, :cond_1

    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->I:I

    add-int/2addr v0, v1

    iget v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->e:I

    if-ge v0, v2, :cond_0

    add-int/lit8 v0, v1, -0x1

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    iget v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->Q:I

    add-int/2addr v0, v2

    :goto_0
    iget v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->I:I

    add-int/2addr v1, v2

    invoke-direct {p0, v1, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->d(II)V

    :goto_1
    return-void

    :cond_0
    add-int/lit8 v0, v1, -0x1

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    iget v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->k:I

    add-int/2addr v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v0

    goto :goto_0

    :cond_2
    if-lez v1, :cond_3

    iget v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->I:I

    if-nez v1, :cond_4

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    iget v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->k:I

    sub-int/2addr v0, v1

    :cond_3
    :goto_2
    iget v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->I:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->c(II)V

    goto :goto_1

    :cond_4
    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    iget v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->Q:I

    sub-int/2addr v0, v1

    goto :goto_2
.end method

.method private b(II)Z
    .locals 13

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez p1, :cond_1

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildCount()I

    move-result v7

    if-gez p2, :cond_6

    move v0, v1

    :goto_1
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v8

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingRight()I

    move-result v4

    sub-int v9, v3, v4

    add-int/lit8 v3, v7, -0x1

    invoke-virtual {p0, v3}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v4

    invoke-virtual {p0, v2}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v5

    iget v10, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->I:I

    if-eqz v0, :cond_8

    add-int v3, v10, v7

    iget v6, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->e:I

    if-ge v3, v6, :cond_7

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getOverflowRight()I

    move-result v3

    iget v6, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->Q:I

    add-int/2addr v3, v6

    move v6, v3

    :goto_2
    if-eqz v0, :cond_b

    add-int v3, v10, v7

    iget v5, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->e:I

    if-lt v3, v5, :cond_2

    add-int v3, v4, p1

    iget v4, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->k:I

    sub-int v4, v9, v4

    if-lt v3, v4, :cond_a

    :cond_2
    :goto_3
    if-eqz v1, :cond_0

    if-eqz v0, :cond_d

    move v4, v2

    move v3, v2

    :goto_4
    if-ge v4, v7, :cond_3

    invoke-virtual {p0, v4}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/View;->getRight()I

    move-result v5

    add-int/2addr v5, p1

    if-lt v5, v8, :cond_c

    :cond_3
    move v4, v2

    :cond_4
    if-lez v3, :cond_5

    invoke-virtual {p0, v4, v3}, Lcom/twitter/internal/android/widget/HorizontalListView;->detachViewsFromParent(II)V

    if-eqz v0, :cond_5

    iget v4, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->I:I

    add-int/2addr v3, v4

    iput v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->I:I

    :cond_5
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildCount()I

    move-result v3

    :goto_5
    if-ge v2, v3, :cond_e

    invoke-virtual {p0, v2}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/view/View;->offsetLeftAndRight(I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_6
    move v0, v2

    goto :goto_1

    :cond_7
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getOverflowRight()I

    move-result v3

    iget v6, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->k:I

    add-int/2addr v3, v6

    move v6, v3

    goto :goto_2

    :cond_8
    if-lez v10, :cond_9

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getOverflowLeft()I

    move-result v3

    iget v6, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->Q:I

    add-int/2addr v3, v6

    move v6, v3

    goto :goto_2

    :cond_9
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getOverflowLeft()I

    move-result v3

    iget v6, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->k:I

    add-int/2addr v3, v6

    move v6, v3

    goto :goto_2

    :cond_a
    move v1, v2

    goto :goto_3

    :cond_b
    if-gtz v10, :cond_2

    add-int v3, v5, p1

    iget v4, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->k:I

    add-int/2addr v4, v8

    if-le v3, v4, :cond_2

    move v1, v2

    goto :goto_3

    :cond_c
    add-int/lit8 v5, v3, 0x1

    iget-object v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->i:Lcom/twitter/internal/android/widget/x;

    add-int v11, v10, v4

    invoke-virtual {v3, v9, v11}, Lcom/twitter/internal/android/widget/x;->a(Landroid/view/View;I)V

    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v5

    goto :goto_4

    :cond_d
    add-int/lit8 v3, v7, -0x1

    move v5, v3

    move v4, v2

    move v3, v2

    :goto_6
    if-ltz v5, :cond_4

    invoke-virtual {p0, v5}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getLeft()I

    move-result v8

    add-int/2addr v8, p1

    if-le v8, v9, :cond_4

    add-int/lit8 v4, v3, 0x1

    iget-object v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->i:Lcom/twitter/internal/android/widget/x;

    add-int v8, v10, v5

    invoke-virtual {v3, v7, v8}, Lcom/twitter/internal/android/widget/x;->a(Landroid/view/View;I)V

    add-int/lit8 v3, v5, -0x1

    move v12, v3

    move v3, v4

    move v4, v5

    move v5, v12

    goto :goto_6

    :cond_e
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->invalidate()V

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    if-ge v6, v2, :cond_0

    invoke-direct {p0, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->b(Z)V

    goto/16 :goto_0
.end method

.method static synthetic b(Lcom/twitter/internal/android/widget/HorizontalListView;I)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/twitter/internal/android/widget/HorizontalListView;->c(I)Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/twitter/internal/android/widget/HorizontalListView;)I
    .locals 1

    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->I:I

    return v0
.end method

.method private c()V
    .locals 9

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->invalidate()V

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->f:Z

    iget-boolean v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->Z:Z

    iget v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->I:I

    iget-object v4, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->i:Lcom/twitter/internal/android/widget/x;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildCount()I

    move-result v5

    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-ge v0, v5, :cond_1

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    add-int v8, v3, v0

    invoke-virtual {v4, v7, v8}, Lcom/twitter/internal/android/widget/x;->a(Landroid/view/View;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v4, v3, v5}, Lcom/twitter/internal/android/widget/x;->a(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->detachAllViewsFromParent()V

    if-eqz v6, :cond_2

    if-eqz v2, :cond_3

    :cond_2
    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->A:I

    invoke-direct {p0, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->d(I)V

    :goto_1
    invoke-virtual {v4}, Lcom/twitter/internal/android/widget/x;->a()V

    iput-boolean v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->Z:Z

    iput-boolean v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->f:Z

    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->I:I

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildCount()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->g:I

    return-void

    :cond_3
    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-direct {p0, v3, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->d(II)V

    goto :goto_1
.end method

.method private c(II)V
    .locals 6

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v3

    move v0, v1

    :goto_0
    if-le p2, v3, :cond_0

    if-ltz p1, :cond_0

    invoke-direct {p0, p1, p2, v2, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(IIIZ)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v4

    iget v5, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->Q:I

    sub-int p2, v4, v5

    add-int/lit8 p1, p1, -0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->I:I

    sub-int v0, v1, v0

    iput v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->I:I

    return-void
.end method

.method private c(I)Z
    .locals 1

    invoke-direct {p0, p1, p1}, Lcom/twitter/internal/android/widget/HorizontalListView;->b(II)Z

    move-result v0

    return v0
.end method

.method private d()V
    .locals 11

    const/4 v10, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildCount()I

    move-result v5

    iget v4, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->e:I

    if-ne v5, v4, :cond_4

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getMeasuredWidth()I

    move-result v6

    add-int/lit8 v1, v4, -0x1

    iget v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->Q:I

    mul-int v7, v1, v2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    iget v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->k:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    add-int v2, v1, v7

    move v1, v0

    move v3, v2

    :goto_0
    if-ge v1, v5, :cond_0

    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    add-int/2addr v3, v8

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    if-le v6, v3, :cond_4

    sub-int v1, v6, v2

    div-int v2, v1, v4

    sub-int v1, v3, v7

    sub-int v1, v6, v1

    div-int v6, v1, v4

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v1

    iget v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->k:I

    add-int/2addr v1, v3

    move v3, v0

    move v4, v1

    :goto_1
    if-ge v3, v5, :cond_3

    invoke-virtual {p0, v3}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->v:I

    if-ne v0, v10, :cond_2

    move v1, v2

    :goto_2
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/HorizontalListView$LayoutParams;

    iput v1, v0, Lcom/twitter/internal/android/widget/HorizontalListView$LayoutParams;->c:I

    add-int v0, v4, v1

    const/high16 v8, 0x40000000    # 2.0f

    invoke-static {v1, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    iget v9, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->H:I

    invoke-virtual {v7, v8, v9}, Landroid/view/View;->measure(II)V

    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v8

    invoke-virtual {v7}, Landroid/view/View;->getBottom()I

    move-result v9

    invoke-virtual {v7, v4, v8, v0, v9}, Landroid/view/View;->layout(IIII)V

    add-int v0, v4, v1

    add-int/lit8 v1, v5, -0x1

    if-ge v3, v1, :cond_1

    iget v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->Q:I

    add-int/2addr v0, v1

    :cond_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v4, v0

    goto :goto_1

    :cond_2
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, v6

    move v1, v0

    goto :goto_2

    :cond_3
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->e()V

    :cond_4
    iput-boolean v10, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->c:Z

    return-void
.end method

.method private d(I)V
    .locals 8

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v2

    iget v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->k:I

    add-int v4, v2, v3

    if-nez p1, :cond_1

    invoke-direct {p0, v1, v4}, Lcom/twitter/internal/android/widget/HorizontalListView;->d(II)V

    :cond_0
    :goto_0
    iput v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->I:I

    return-void

    :cond_1
    add-int/lit8 v2, p1, -0x1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v3

    invoke-direct {p0, v2, v3}, Lcom/twitter/internal/android/widget/HorizontalListView;->d(II)V

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildCount()I

    move-result v5

    add-int v3, v2, v5

    iget v6, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->e:I

    if-ne v3, v6, :cond_6

    add-int/lit8 v3, v5, -0x1

    invoke-virtual {p0, v3}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getMeasuredWidth()I

    move-result v6

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingRight()I

    move-result v7

    sub-int/2addr v6, v7

    iget v7, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->k:I

    sub-int/2addr v6, v7

    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v3

    sub-int/2addr v6, v3

    move v3, v1

    :goto_1
    if-ge v3, v5, :cond_2

    invoke-virtual {p0, v3}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/view/View;->offsetLeftAndRight(I)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v3

    invoke-direct {p0, v2, v3}, Lcom/twitter/internal/android/widget/HorizontalListView;->c(II)V

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildCount()I

    move-result v2

    iget v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->e:I

    if-ne v2, v3, :cond_5

    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v3

    if-le v3, v4, :cond_4

    sub-int v3, v4, v3

    move v0, v1

    :goto_2
    if-ge v0, v2, :cond_3

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/view/View;->offsetLeftAndRight(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    move v0, v1

    :cond_4
    :goto_3
    if-eqz v0, :cond_0

    sub-int v0, v1, p1

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->Q:I

    iget v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->h:I

    add-int/2addr v2, v3

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    if-ge v0, v2, :cond_0

    sub-int v0, v2, v0

    invoke-direct {p0, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->c(I)Z

    goto/16 :goto_0

    :cond_5
    iget v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->e:I

    sub-int/2addr v1, v2

    goto :goto_3

    :cond_6
    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_7

    iget v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->Q:I

    iget v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->h:I

    add-int/2addr v1, v3

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    if-le v0, v1, :cond_7

    sub-int v0, v1, v0

    invoke-direct {p0, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->c(I)Z

    :cond_7
    move v1, v2

    goto/16 :goto_0
.end method

.method private d(II)V
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    :goto_0
    if-ge p2, v1, :cond_0

    iget v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->e:I

    if-ge p1, v2, :cond_0

    const/4 v2, 0x1

    invoke-direct {p0, p1, p2, v0, v2}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(IIIZ)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v2

    iget v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->Q:I

    add-int p2, v2, v3

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/twitter/internal/android/widget/HorizontalListView;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->c()V

    return-void
.end method

.method static synthetic e(Lcom/twitter/internal/android/widget/HorizontalListView;)Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->J:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private e()V
    .locals 7

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->q:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->A:I

    iget v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->I:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->t:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getHeight()I

    move-result v3

    if-eqz v2, :cond_3

    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->A:I

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v0

    iget v4, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->k:I

    add-int/2addr v0, v4

    :goto_1
    iget v4, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->u:I

    sub-int v4, v3, v4

    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v2

    iget v5, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->Q:I

    add-int/2addr v2, v5

    invoke-virtual {v1, v0, v4, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    :goto_2
    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    iget v0, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getWidth()I

    move-result v2

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0, v6, v0, v2, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->invalidate(IIII)V

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v0

    iget v4, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->Q:I

    sub-int/2addr v0, v4

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getWidth()I

    move-result v0

    iget v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->A:I

    iget v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->I:I

    if-ge v2, v3, :cond_4

    iget v0, v1, Landroid/graphics/Rect;->right:I

    neg-int v0, v0

    iput v0, v1, Landroid/graphics/Rect;->left:I

    iput v6, v1, Landroid/graphics/Rect;->right:I

    goto :goto_2

    :cond_4
    iget v2, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v0

    iput v2, v1, Landroid/graphics/Rect;->right:I

    iput v0, v1, Landroid/graphics/Rect;->left:I

    goto :goto_2
.end method

.method private e(I)V
    .locals 1

    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->z:I

    if-eq v0, p1, :cond_0

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->f()V

    iput p1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->z:I

    :cond_0
    return-void
.end method

.method private e(II)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(IZ)V

    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(IZ)V

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->invalidate()V

    return-void
.end method

.method static synthetic f(Lcom/twitter/internal/android/widget/HorizontalListView;)I
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getWindowAttachCount()I

    move-result v0

    return v0
.end method

.method private f()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->w:Lcom/twitter/internal/android/widget/y;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/y;->a()V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->x:Lcom/twitter/internal/android/widget/y;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/y;->a()V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->y:Lcom/twitter/internal/android/widget/y;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/y;->a()V

    return-void
.end method

.method static synthetic g(Lcom/twitter/internal/android/widget/HorizontalListView;)I
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getWindowAttachCount()I

    move-result v0

    return v0
.end method

.method private g()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->f()V

    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->B:I

    iput v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->A:I

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->e()V

    return-void
.end method

.method private getOverflowLeft()I
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildCount()I

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    sub-int v0, v1, v0

    goto :goto_0
.end method

.method private getOverflowRight()I
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v1

    sub-int v0, v1, v0

    goto :goto_0
.end method

.method private h()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->E:Lcom/twitter/internal/android/widget/v;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->E:Lcom/twitter/internal/android/widget/v;

    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->F:I

    const/4 v2, 0x4

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, p0, v0}, Lcom/twitter/internal/android/widget/v;->a(Lcom/twitter/internal/android/widget/HorizontalListView;Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic h(Lcom/twitter/internal/android/widget/HorizontalListView;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->h()V

    return-void
.end method

.method static synthetic i(Lcom/twitter/internal/android/widget/HorizontalListView;)I
    .locals 1

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getOverflowLeft()I

    move-result v0

    return v0
.end method

.method static synthetic j(Lcom/twitter/internal/android/widget/HorizontalListView;)I
    .locals 1

    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->k:I

    return v0
.end method

.method static synthetic k(Lcom/twitter/internal/android/widget/HorizontalListView;)I
    .locals 1

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getOverflowRight()I

    move-result v0

    return v0
.end method

.method static synthetic l(Lcom/twitter/internal/android/widget/HorizontalListView;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->e()V

    return-void
.end method


# virtual methods
.method public a(II)I
    .locals 4

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->K:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->K:Landroid/graphics/Rect;

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->K:Landroid/graphics/Rect;

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_0
    if-ltz v1, :cond_2

    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v2, v0}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_1

    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->I:I

    add-int/2addr v0, v1

    :goto_1
    return v0

    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_2
    const/4 v0, -0x1

    goto :goto_1
.end method

.method final a(IIIII)I
    .locals 7

    const/4 v6, -0x1

    iget-object v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->d:Landroid/widget/ListAdapter;

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingRight()I

    move-result v1

    add-int p4, v0, v1

    :cond_0
    :goto_0
    return p4

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->k:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v0

    const/4 v0, 0x0

    if-ne p3, v6, :cond_2

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    add-int/lit8 p3, v2, -0x1

    :cond_2
    iget-object v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->i:Lcom/twitter/internal/android/widget/x;

    iget-object v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->j:[Z

    :goto_1
    if-gt p2, p3, :cond_5

    invoke-direct {p0, p2, v3}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(I[Z)Landroid/view/View;

    move-result-object v4

    invoke-direct {p0, v4, p1}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(Landroid/view/View;I)V

    invoke-virtual {v2, v4, v6}, Lcom/twitter/internal/android/widget/x;->a(Landroid/view/View;I)V

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    iget v5, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->Q:I

    add-int/2addr v4, v5

    add-int/2addr v1, v4

    if-lt v1, p4, :cond_3

    if-ltz p5, :cond_0

    if-le p2, p5, :cond_0

    if-lez v0, :cond_0

    if-eq v1, p4, :cond_0

    move p4, v0

    goto :goto_0

    :cond_3
    if-ltz p5, :cond_4

    if-lt p2, p5, :cond_4

    move v0, v1

    :cond_4
    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    :cond_5
    move p4, v1

    goto :goto_0
.end method

.method public a(Landroid/util/AttributeSet;)Lcom/twitter/internal/android/widget/HorizontalListView$LayoutParams;
    .locals 2

    new-instance v0, Lcom/twitter/internal/android/widget/HorizontalListView$LayoutParams;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/twitter/internal/android/widget/HorizontalListView$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public a(I)V
    .locals 2

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->aa:Z

    :cond_0
    :goto_0
    iput p1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->C:I

    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->aa:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->aa:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->ac:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->requestLayout()V

    :cond_2
    if-nez p1, :cond_0

    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->C:I

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->g()V

    goto :goto_0
.end method

.method public a(IF)V
    .locals 10

    const/4 v7, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    cmpl-float v0, p2, v7

    if-eqz v0, :cond_0

    add-int/lit8 v3, p1, 0x1

    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->I:I

    sub-int v0, p1, v0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildCount()I

    move-result v4

    if-ltz v0, :cond_0

    if-lt v0, v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->C:I

    const/4 v6, 0x2

    if-ne v0, v6, :cond_4

    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->B:I

    if-eq p1, v0, :cond_4

    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->B:I

    if-eq v3, v0, :cond_4

    move v0, v1

    :goto_1
    iget v6, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->A:I

    if-le v6, p1, :cond_8

    invoke-direct {p0, p1}, Lcom/twitter/internal/android/widget/HorizontalListView;->e(I)V

    iget v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->I:I

    if-nez v3, :cond_2

    move v2, v1

    :cond_2
    if-eqz v2, :cond_5

    if-nez p1, :cond_5

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v1

    iget v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->k:I

    add-int/2addr v1, v3

    :goto_2
    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v3

    sub-int v3, v1, v3

    if-lez v3, :cond_7

    if-eqz v2, :cond_6

    if-nez p1, :cond_6

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v1

    iget v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->k:I

    add-int/2addr v1, v2

    :goto_3
    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v2, p2

    if-nez v0, :cond_3

    if-lez v3, :cond_3

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->w:Lcom/twitter/internal/android/widget/y;

    invoke-virtual {v0, v3, v2}, Lcom/twitter/internal/android/widget/y;->a(IF)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->c(I)Z

    :cond_3
    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v0

    add-int/2addr v0, v1

    iget v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->Q:I

    add-int/2addr v0, v3

    invoke-direct {p0, v1, v0, v2}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(IIF)V

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v1

    iget v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->Q:I

    add-int/2addr v1, v3

    iget v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->h:I

    add-int/2addr v1, v3

    goto :goto_2

    :cond_6
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v1

    iget v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->h:I

    add-int/2addr v1, v2

    goto :goto_3

    :cond_7
    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v1

    iget v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->Q:I

    sub-int/2addr v1, v2

    goto :goto_3

    :cond_8
    cmpl-float v6, p2, v7

    if-lez v6, :cond_0

    invoke-direct {p0, v3}, Lcom/twitter/internal/android/widget/HorizontalListView;->e(I)V

    iget v6, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->I:I

    sub-int/2addr v3, v6

    if-ltz v3, :cond_0

    if-ge v3, v4, :cond_0

    invoke-virtual {p0, v3}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    iget v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->I:I

    add-int/2addr v3, v4

    iget v7, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->e:I

    if-ne v3, v7, :cond_b

    :goto_4
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->k:I

    sub-int v7, v2, v3

    add-int/lit8 v2, v4, -0x1

    invoke-virtual {p0, v2}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v2

    iget v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->Q:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->h:I

    add-int v8, v2, v3

    if-eqz v1, :cond_c

    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result v2

    sub-int v2, v8, v2

    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v3

    sub-int v3, v7, v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    move v3, v2

    :goto_5
    if-gez v3, :cond_e

    if-eqz v1, :cond_d

    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result v2

    add-int/2addr v2, v3

    iget v9, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->Q:I

    sub-int/2addr v2, v9

    :goto_6
    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v6

    add-int/2addr v6, v2

    iget v9, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->Q:I

    add-int/2addr v6, v9

    if-nez v0, :cond_a

    if-gez v3, :cond_a

    if-eqz v1, :cond_9

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v0

    if-le v0, v8, :cond_9

    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v0

    if-le v0, v7, :cond_a

    :cond_9
    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->w:Lcom/twitter/internal/android/widget/y;

    invoke-virtual {v0, v3, p2}, Lcom/twitter/internal/android/widget/y;->a(IF)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->c(I)Z

    :cond_a
    invoke-direct {p0, v2, v6, p2}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(IIF)V

    goto/16 :goto_0

    :cond_b
    move v1, v2

    goto :goto_4

    :cond_c
    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result v2

    sub-int v2, v8, v2

    move v3, v2

    goto :goto_5

    :cond_d
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v2

    iget v9, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->h:I

    add-int/2addr v2, v9

    goto :goto_6

    :cond_e
    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result v2

    iget v9, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->Q:I

    sub-int/2addr v2, v9

    goto :goto_6
.end method

.method a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->P:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public b(I)V
    .locals 1

    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->C:I

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/twitter/internal/android/widget/HorizontalListView;->setSelection(I)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->B:I

    iput p1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->B:I

    invoke-direct {p0, v0, p1}, Lcom/twitter/internal/android/widget/HorizontalListView;->e(II)V

    goto :goto_0
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    instance-of v0, p1, Lcom/twitter/internal/android/widget/HorizontalListView$LayoutParams;

    return v0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 14

    iget v4, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->Q:I

    if-lez v4, :cond_3

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->P:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    move v3, v0

    :goto_0
    if-eqz v3, :cond_8

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v0

    iput v0, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, v5, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildCount()I

    move-result v6

    iget v7, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->I:I

    iget-object v8, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->d:Landroid/widget/ListAdapter;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getScrollX()I

    move-result v9

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->isOpaque()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-super {p0}, Landroid/widget/AdapterView;->isOpaque()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    move v2, v0

    :goto_1
    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->R:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->R:Landroid/graphics/Paint;

    :cond_0
    iget-object v10, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->R:Landroid/graphics/Paint;

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->U:Z

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingRight()I

    move-result v0

    :goto_2
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getWidth()I

    move-result v11

    add-int/2addr v11, v9

    sub-int/2addr v11, v0

    const/4 v0, 0x0

    :goto_3
    if-ge v0, v6, :cond_7

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v12

    invoke-virtual {v12}, Landroid/view/View;->getLeft()I

    move-result v12

    if-le v12, v1, :cond_2

    add-int v13, v7, v0

    invoke-interface {v8, v13}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v13

    if-eqz v13, :cond_6

    add-int/lit8 v13, v6, -0x1

    if-eq v0, v13, :cond_1

    add-int v13, v7, v0

    add-int/lit8 v13, v13, 0x1

    invoke-interface {v8, v13}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v13

    if-eqz v13, :cond_6

    :cond_1
    sub-int v13, v12, v4

    iput v13, v5, Landroid/graphics/Rect;->left:I

    iput v12, v5, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0, p1, v5}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    :cond_2
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    const/4 v0, 0x0

    move v3, v0

    goto/16 :goto_0

    :cond_4
    const/4 v0, 0x0

    move v2, v0

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    const/4 v0, 0x0

    goto :goto_2

    :cond_6
    if-eqz v2, :cond_2

    sub-int v13, v12, v4

    iput v13, v5, Landroid/graphics/Rect;->left:I

    iput v12, v5, Landroid/graphics/Rect;->right:I

    invoke-virtual {p1, v5, v10}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_4

    :cond_7
    if-lez v6, :cond_8

    if-lez v9, :cond_8

    if-eqz v3, :cond_8

    iput v11, v5, Landroid/graphics/Rect;->left:I

    add-int v0, v11, v4

    iput v0, v5, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0, p1, v5}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    :cond_8
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->dispatchDraw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->q:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_9

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xa

    if-gt v0, v1, :cond_c

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    :goto_5
    iget-object v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    if-lez v0, :cond_9

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :cond_9
    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->r:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->r:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x3

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;I)V

    :cond_a
    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->s:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->s:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x5

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;I)V

    :cond_b
    return-void

    :cond_c
    const/4 v0, 0x0

    goto :goto_5
.end method

.method protected drawableStateChanged()V
    .locals 2

    invoke-super {p0}, Landroid/widget/AdapterView;->drawableStateChanged()V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->q:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    :cond_0
    return-void
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 3

    new-instance v0, Lcom/twitter/internal/android/widget/HorizontalListView$LayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Lcom/twitter/internal/android/widget/HorizontalListView$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(Landroid/util/AttributeSet;)Lcom/twitter/internal/android/widget/HorizontalListView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    new-instance v0, Lcom/twitter/internal/android/widget/HorizontalListView$LayoutParams;

    invoke-direct {v0, p1}, Lcom/twitter/internal/android/widget/HorizontalListView$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public bridge synthetic getAdapter()Landroid/widget/Adapter;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Landroid/widget/ListAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->d:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method public getSelectedView()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getSelection()I
    .locals 1

    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->A:I

    return v0
.end method

.method protected onCreateDrawableState(I)[I
    .locals 2

    sget-object v0, Lcom/twitter/internal/android/widget/HorizontalListView;->a:[I

    array-length v0, v0

    add-int/2addr v0, p1

    invoke-super {p0, v0}, Landroid/widget/AdapterView;->onCreateDrawableState(I)[I

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->ab:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/twitter/internal/android/widget/HorizontalListView;->a:[I

    invoke-static {v0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->mergeDrawableStates([I[I)[I

    :cond_0
    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/AdapterView;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->i:Lcom/twitter/internal/android/widget/x;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/x;->b()V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->O:Lcom/twitter/internal/android/widget/s;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->O:Lcom/twitter/internal/android/widget/s;

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    const/4 v1, 0x0

    invoke-super/range {p0 .. p5}, Landroid/widget/AdapterView;->onLayout(ZIIII)V

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->aa:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->ac:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->aa:Z

    iget-boolean v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->f:Z

    iget-boolean v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->Z:Z

    if-nez p1, :cond_1

    if-eqz v2, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildCount()I

    move-result v4

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_2

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->forceLayout()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->c()V

    if-nez p1, :cond_3

    if-eqz v3, :cond_4

    :cond_3
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->e()V

    :cond_4
    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->v:I

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->c:Z

    if-eqz v0, :cond_5

    if-nez p1, :cond_5

    if-eqz v2, :cond_6

    :cond_5
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->d()V

    :cond_6
    iput-boolean v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->aa:Z

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 10

    const/4 v3, -0x1

    const/4 v2, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v8

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->d:Landroid/widget/ListAdapter;

    if-nez v0, :cond_2

    move v0, v2

    :goto_0
    iput v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->e:I

    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->e:I

    if-lez v0, :cond_3

    if-eqz v5, :cond_0

    if-nez v8, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->j:[Z

    invoke-direct {p0, v2, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(I[Z)Landroid/view/View;

    move-result-object v7

    invoke-direct {p0, v7, p2}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(Landroid/view/View;I)V

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iget-object v9, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->i:Lcom/twitter/internal/android/widget/x;

    invoke-virtual {v9, v7, v3}, Lcom/twitter/internal/android/widget/x;->a(Landroid/view/View;I)V

    move v7, v0

    move v0, v1

    :goto_1
    if-nez v5, :cond_4

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingRight()I

    move-result v3

    add-int/2addr v1, v3

    add-int v4, v1, v0

    :cond_1
    :goto_2
    if-nez v8, :cond_8

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v0, v7

    :goto_3
    iget v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->l:F

    const/4 v3, 0x0

    cmpl-float v1, v1, v3

    if-lez v1, :cond_6

    iget v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->e:I

    if-lez v1, :cond_6

    iget v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->Q:I

    iget v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->e:I

    mul-int/2addr v1, v3

    iget v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->k:I

    mul-int/lit8 v3, v3, 0x2

    sub-int v3, v4, v3

    sub-int v1, v3, v1

    iget v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->e:I

    div-int/2addr v1, v3

    if-lt v1, v0, :cond_5

    int-to-float v3, v1

    iget v5, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->l:F

    int-to-float v6, v0

    mul-float/2addr v5, v6

    cmpg-float v3, v3, v5

    if-gtz v3, :cond_5

    :goto_4
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildCount()I

    move-result v3

    if-ge v2, v3, :cond_5

    invoke-virtual {p0, v2}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iput v1, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_2
    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->d:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getSuggestedMinimumWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getSuggestedMinimumHeight()I

    move-result v0

    move v7, v0

    move v0, v1

    goto :goto_1

    :cond_4
    const/high16 v0, -0x80000000

    if-ne v5, v0, :cond_1

    move-object v0, p0

    move v1, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(IIIII)I

    move-result v4

    goto :goto_2

    :cond_5
    move v2, v1

    :cond_6
    iget-object v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->D:Lcom/twitter/internal/android/widget/t;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->D:Lcom/twitter/internal/android/widget/t;

    invoke-interface {v1, v2}, Lcom/twitter/internal/android/widget/t;->a(I)V

    :cond_7
    invoke-virtual {p0, v4, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->setMeasuredDimension(II)V

    iput p2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->H:I

    return-void

    :cond_8
    move v0, v6

    goto :goto_3
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11

    const/4 v10, 0x3

    const/4 v9, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/high16 v8, -0x80000000

    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->e:I

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->W:Landroid/view/VelocityTracker;

    if-nez v0, :cond_1

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->W:Landroid/view/VelocityTracker;

    :cond_1
    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->W:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    packed-switch v0, :pswitch_data_0

    :cond_2
    :goto_1
    move v0, v1

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0, v3, v4}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(II)I

    move-result v0

    iget-boolean v5, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->f:Z

    if-nez v5, :cond_4

    iget v5, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->F:I

    const/4 v6, 0x4

    if-eq v5, v6, :cond_4

    if-ltz v0, :cond_4

    iget-object v5, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->d:Landroid/widget/ListAdapter;

    invoke-interface {v5, v0}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v5

    if-eqz v5, :cond_4

    iput v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->F:I

    iget-object v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->O:Lcom/twitter/internal/android/widget/s;

    if-nez v2, :cond_3

    new-instance v2, Lcom/twitter/internal/android/widget/s;

    invoke-direct {v2, p0}, Lcom/twitter/internal/android/widget/s;-><init>(Lcom/twitter/internal/android/widget/HorizontalListView;)V

    iput-object v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->O:Lcom/twitter/internal/android/widget/s;

    :cond_3
    iget-object v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->O:Lcom/twitter/internal/android/widget/s;

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v5

    int-to-long v5, v5

    invoke-virtual {p0, v2, v5, v6}, Lcom/twitter/internal/android/widget/HorizontalListView;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_4
    invoke-direct {p0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(Z)V

    iget-object v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->p:Lcom/twitter/internal/android/widget/u;

    invoke-virtual {v2}, Lcom/twitter/internal/android/widget/u;->a()V

    iput v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->L:I

    iput v4, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->M:I

    iput v8, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->V:I

    iput v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->N:I

    goto :goto_1

    :pswitch_1
    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->F:I

    packed-switch v0, :pswitch_data_1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->W:Landroid/view/VelocityTracker;

    const/16 v2, 0x3e8

    iget v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->o:I

    int-to-float v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->n:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_5

    iget-object v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->p:Lcom/twitter/internal/android/widget/u;

    neg-float v0, v0

    float-to-int v0, v0

    invoke-virtual {v2, v0}, Lcom/twitter/internal/android/widget/u;->a(I)V

    :cond_5
    :goto_2
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->a()V

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->b()V

    iput v8, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->M:I

    iput v8, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->L:I

    iput v8, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->V:I

    goto :goto_1

    :pswitch_2
    iget v5, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->N:I

    invoke-virtual {p0, v3, v4}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(II)I

    move-result v0

    iget v4, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->I:I

    sub-int v4, v5, v4

    invoke-virtual {p0, v4}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v6

    if-le v3, v6, :cond_c

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getWidth()I

    move-result v6

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getPaddingRight()I

    move-result v7

    sub-int/2addr v6, v7

    if-ge v3, v6, :cond_c

    if-ne v0, v5, :cond_c

    move v0, v1

    :goto_3
    if-eqz v4, :cond_f

    invoke-virtual {v4}, Landroid/view/View;->hasFocusable()Z

    move-result v3

    if-nez v3, :cond_f

    if-eqz v0, :cond_f

    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->F:I

    if-eqz v0, :cond_6

    invoke-virtual {v4, v2}, Landroid/view/View;->setPressed(Z)V

    :cond_6
    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->T:Lcom/twitter/internal/android/widget/w;

    if-nez v0, :cond_7

    new-instance v0, Lcom/twitter/internal/android/widget/w;

    invoke-direct {v0, p0}, Lcom/twitter/internal/android/widget/w;-><init>(Lcom/twitter/internal/android/widget/HorizontalListView;)V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->T:Lcom/twitter/internal/android/widget/w;

    :cond_7
    iget-object v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->T:Lcom/twitter/internal/android/widget/w;

    iput v5, v2, Lcom/twitter/internal/android/widget/w;->a:I

    invoke-virtual {v2}, Lcom/twitter/internal/android/widget/w;->a()V

    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->F:I

    if-eqz v0, :cond_8

    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->F:I

    if-ne v0, v1, :cond_e

    :cond_8
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getHandler()Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->O:Lcom/twitter/internal/android/widget/s;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_9
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->f:Z

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->d:Landroid/widget/ListAdapter;

    invoke-interface {v0, v5}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_d

    iput v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->F:I

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->c()V

    invoke-virtual {v4, v1}, Landroid/view/View;->setPressed(Z)V

    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->setPressed(Z)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->J:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->J:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_a

    instance-of v3, v0, Landroid/graphics/drawable/TransitionDrawable;

    if-eqz v3, :cond_a

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    :cond_a
    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->S:Ljava/lang/Runnable;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->S:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_b
    new-instance v0, Lcom/twitter/internal/android/widget/q;

    invoke-direct {v0, p0, v4, v2}, Lcom/twitter/internal/android/widget/q;-><init>(Lcom/twitter/internal/android/widget/HorizontalListView;Landroid/view/View;Lcom/twitter/internal/android/widget/w;)V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->S:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->S:Ljava/lang/Runnable;

    invoke-static {}, Landroid/view/ViewConfiguration;->getPressedStateDuration()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/internal/android/widget/HorizontalListView;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_4
    move v0, v1

    goto/16 :goto_0

    :cond_c
    move v0, v2

    goto :goto_3

    :cond_d
    iput v9, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->F:I

    goto :goto_4

    :cond_e
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->f:Z

    if-nez v0, :cond_f

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->d:Landroid/widget/ListAdapter;

    invoke-interface {v0, v5}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-virtual {v2}, Lcom/twitter/internal/android/widget/w;->run()V

    :cond_f
    iput v9, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->F:I

    goto/16 :goto_2

    :pswitch_3
    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->L:I

    if-eq v0, v8, :cond_18

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_10

    iput v9, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->F:I

    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto/16 :goto_0

    :cond_10
    iget v5, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->L:I

    sub-int v5, v3, v5

    iget v6, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->F:I

    if-eq v6, v10, :cond_16

    iget v6, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->I:I

    if-nez v6, :cond_11

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getOverflowLeft()I

    move-result v6

    iget v7, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->k:I

    add-int/2addr v6, v7

    if-nez v6, :cond_11

    if-gtz v5, :cond_12

    :cond_11
    iget v6, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->I:I

    iget v7, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->g:I

    if-ne v6, v7, :cond_13

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getOverflowRight()I

    move-result v6

    iget v7, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->k:I

    add-int/2addr v6, v7

    if-nez v6, :cond_13

    if-gez v5, :cond_13

    :cond_12
    invoke-direct {p0, v2}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(Z)V

    goto/16 :goto_1

    :cond_13
    iget v6, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->M:I

    if-eq v6, v8, :cond_14

    iget v6, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->M:I

    sub-int v6, v4, v6

    iget v7, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->m:I

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    if-gt v7, v6, :cond_14

    invoke-direct {p0, v2}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(Z)V

    goto/16 :goto_1

    :cond_14
    iget v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->m:I

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v6

    if-gt v2, v6, :cond_2

    iget v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->e:I

    if-ne v0, v2, :cond_15

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getOverflowLeft()I

    move-result v0

    iget v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->k:I

    add-int/2addr v0, v2

    if-nez v0, :cond_15

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getOverflowRight()I

    move-result v0

    iget v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->k:I

    add-int/2addr v0, v2

    if-eqz v0, :cond_2

    :cond_15
    iput v10, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->F:I

    :cond_16
    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->V:I

    if-eq v3, v0, :cond_2

    if-lez v5, :cond_19

    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->m:I

    sub-int v0, v5, v0

    :goto_5
    iget v2, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->V:I

    if-eq v2, v8, :cond_17

    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->V:I

    sub-int v0, v3, v0

    :cond_17
    invoke-direct {p0, v5, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->b(II)Z

    iput v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->V:I

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->e()V

    :cond_18
    iput v3, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->L:I

    iput v4, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->M:I

    goto/16 :goto_1

    :cond_19
    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->m:I

    add-int/2addr v0, v5

    goto :goto_5

    :pswitch_4
    invoke-direct {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->b()V

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->a()V

    iput v8, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->L:I

    iput v8, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->M:I

    iput v8, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->V:I

    iput v9, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->F:I

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public requestLayout()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->ac:Z

    invoke-super {p0}, Landroid/widget/AdapterView;->requestLayout()V

    return-void
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, Lcom/twitter/internal/android/widget/HorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->d:Landroid/widget/ListAdapter;

    if-eq p1, v0, :cond_2

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->G:Landroid/database/DataSetObserver;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/internal/android/widget/r;

    invoke-direct {v0, p0}, Lcom/twitter/internal/android/widget/r;-><init>(Lcom/twitter/internal/android/widget/HorizontalListView;)V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->G:Landroid/database/DataSetObserver;

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->d:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->d:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->G:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->G:Landroid/database/DataSetObserver;

    invoke-interface {p1, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    iput-object p1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->d:Landroid/widget/ListAdapter;

    invoke-interface {p1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->e:I

    :cond_2
    return-void
.end method

.method public setChildWidthListener(Lcom/twitter/internal/android/widget/t;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->D:Lcom/twitter/internal/android/widget/t;

    return-void
.end method

.method public setClipToPadding(Z)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/AdapterView;->setClipToPadding(Z)V

    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->U:Z

    return-void
.end method

.method public setDivider(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->Q:I

    :goto_0
    iput-object p1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->P:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->requestLayout()V

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->invalidate()V

    return-void

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->Q:I

    goto :goto_0
.end method

.method public final setDividerWidth(I)V
    .locals 1

    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->Q:I

    if-eq p1, v0, :cond_0

    iput p1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->Q:I

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->requestLayout()V

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->invalidate()V

    :cond_0
    return-void
.end method

.method public setDocked(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->ab:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->ab:Z

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->refreshDrawableState()V

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->invalidate()V

    :cond_0
    return-void
.end method

.method public setOnScrollListener(Lcom/twitter/internal/android/widget/v;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->E:Lcom/twitter/internal/android/widget/v;

    return-void
.end method

.method public setPressed(Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/AdapterView;->setPressed(Z)V

    :cond_0
    return-void
.end method

.method public setSelection(I)V
    .locals 3

    if-ltz p1, :cond_0

    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->e:I

    if-lt p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setSelection: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->A:I

    if-eq v0, p1, :cond_2

    iput p1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->A:I

    iput p1, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->B:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/internal/android/widget/HorizontalListView;->Z:Z

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->requestLayout()V

    :cond_2
    return-void
.end method
