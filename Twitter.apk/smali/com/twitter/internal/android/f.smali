.class public final Lcom/twitter/internal/android/f;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final BadgeIndicator:[I

.field public static final BadgeIndicator_badgeMode:I = 0x7

.field public static final BadgeIndicator_indicatorDrawable:I = 0x0

.field public static final BadgeIndicator_indicatorMarginBottom:I = 0x1

.field public static final BadgeIndicator_numberBackground:I = 0x2

.field public static final BadgeIndicator_numberColor:I = 0x3

.field public static final BadgeIndicator_numberMinHeight:I = 0x6

.field public static final BadgeIndicator_numberMinWidth:I = 0x5

.field public static final BadgeIndicator_numberTextSize:I = 0x4

.field public static final DockLayout:[I

.field public static final DockLayout_autoUnlock:I = 0x6

.field public static final DockLayout_bottomDockId:I = 0x1

.field public static final DockLayout_bottomPeek:I = 0x4

.field public static final DockLayout_scrollDrive:I = 0x5

.field public static final DockLayout_topDockId:I = 0x0

.field public static final DockLayout_topPeek:I = 0x3

.field public static final DockLayout_turtle:I = 0x2

.field public static final GroupedRowView:[I

.field public static final GroupedRowView_borderColor:I = 0x4

.field public static final GroupedRowView_borderHeight:I = 0x5

.field public static final GroupedRowView_cardStyle:I = 0x0

.field public static final GroupedRowView_fillColor:I = 0x3

.field public static final GroupedRowView_gapSize:I = 0x6

.field public static final GroupedRowView_groupStyle:I = 0x1

.field public static final GroupedRowView_single:I = 0x2

.field public static final HiddenDrawerLayout:[I

.field public static final HiddenDrawerLayout_bgColorHint:I = 0x6

.field public static final HiddenDrawerLayout_closeAnimDuration:I = 0x3

.field public static final HiddenDrawerLayout_closeInterpolator:I = 0x1

.field public static final HiddenDrawerLayout_draggable:I = 0x9

.field public static final HiddenDrawerLayout_draggableEdgeSize:I = 0x8

.field public static final HiddenDrawerLayout_drawerDirection:I = 0x7

.field public static final HiddenDrawerLayout_gutterColor:I = 0x5

.field public static final HiddenDrawerLayout_gutterSize:I = 0x4

.field public static final HiddenDrawerLayout_openAnimDuration:I = 0x2

.field public static final HiddenDrawerLayout_openInterpolator:I = 0x0

.field public static final HorizontalListView:[I

.field public static final HorizontalListView_dividerWidth:I = 0x1

.field public static final HorizontalListView_edgePadding:I = 0x2

.field public static final HorizontalListView_fillMode:I = 0x7

.field public static final HorizontalListView_fillWidthHeightRatio:I = 0x3

.field public static final HorizontalListView_leftFadeInDrawable:I = 0x8

.field public static final HorizontalListView_listDivider:I = 0x0

.field public static final HorizontalListView_rightFadeInDrawable:I = 0x9

.field public static final HorizontalListView_scrollDrawable:I = 0x5

.field public static final HorizontalListView_scrollHeight:I = 0x6

.field public static final HorizontalListView_scrollOffset:I = 0x4

.field public static final NavItemView:[I

.field public static final NavItemView_badgeIndicatorStyle:I = 0x0

.field public static final NavItemView_displayMode:I = 0x5

.field public static final NavItemView_selectedTextStyle:I = 0x1

.field public static final NavItemView_textColor:I = 0x2

.field public static final NavItemView_textSize:I = 0x3

.field public static final NavItemView_textStyle:I = 0x4

.field public static final NotchView:[I

.field public static final NotchView_notchBg:I = 0x1

.field public static final NotchView_notchDrawable:I = 0x0

.field public static final NotchView_notchLeftOffset:I = 0x2

.field public static final OverlayImageView:[I

.field public static final OverlayImageView_overlayDrawable:I = 0x0

.field public static final PopupEditText:[I

.field public static final PopupEditText_popupMenuXOffset:I = 0x0

.field public static final PopupEditText_popupMenuYOffset:I = 0x1

.field public static final PopupEditText_showAsDropdown:I = 0x2

.field public static final PopupEditText_showFullScreen:I = 0x3

.field public static final PopupEditText_showPopupOnInitialFocus:I = 0x5

.field public static final PopupEditText_threshold:I = 0x4

.field public static final RoundedGroupedRowView:[I

.field public static final RoundedGroupedRowView_cardStyle:I = 0x0

.field public static final RoundedGroupedRowView_cornerRadius:I = 0xd

.field public static final RoundedGroupedRowView_dividerColor:I = 0x11

.field public static final RoundedGroupedRowView_dividerHeight:I = 0x10

.field public static final RoundedGroupedRowView_groupStyle:I = 0x1

.field public static final RoundedGroupedRowView_inset:I = 0x7

.field public static final RoundedGroupedRowView_insetBottom:I = 0xb

.field public static final RoundedGroupedRowView_insetBottomFillColor:I = 0xc

.field public static final RoundedGroupedRowView_insetLeft:I = 0x8

.field public static final RoundedGroupedRowView_insetRight:I = 0xa

.field public static final RoundedGroupedRowView_insetTop:I = 0x9

.field public static final RoundedGroupedRowView_shadowColor:I = 0x2

.field public static final RoundedGroupedRowView_shadowDx:I = 0x3

.field public static final RoundedGroupedRowView_shadowDy:I = 0x4

.field public static final RoundedGroupedRowView_shadowRadius:I = 0x5

.field public static final RoundedGroupedRowView_single:I = 0x6

.field public static final RoundedGroupedRowView_strokeColor:I = 0xf

.field public static final RoundedGroupedRowView_strokeWidth:I = 0xe

.field public static final SegmentedControl:[I

.field public static final SegmentedControl_segmentDivider:I = 0x7

.field public static final SegmentedControl_segmentLabels:I = 0x8

.field public static final SegmentedControl_shadowColor:I = 0x0

.field public static final SegmentedControl_shadowDx:I = 0x1

.field public static final SegmentedControl_shadowDy:I = 0x2

.field public static final SegmentedControl_shadowRadius:I = 0x3

.field public static final SegmentedControl_src:I = 0x6

.field public static final SegmentedControl_textColor:I = 0x4

.field public static final SegmentedControl_textSize:I = 0x5

.field public static final ShadowTextView:[I

.field public static final ShadowTextView_shadowColor:I = 0x0

.field public static final ShadowTextView_shadowDx:I = 0x1

.field public static final ShadowTextView_shadowDy:I = 0x2

.field public static final ShadowTextView_shadowRadius:I = 0x3

.field public static final ToolBar:[I

.field public static final ToolBarHomeView:[I

.field public static final ToolBarHomeView_allCaps:I = 0x7

.field public static final ToolBarHomeView_numberBackground:I = 0x3

.field public static final ToolBarHomeView_numberColor:I = 0x4

.field public static final ToolBarHomeView_subtitleTextSize:I = 0x5

.field public static final ToolBarHomeView_textColor:I = 0x0

.field public static final ToolBarHomeView_textSize:I = 0x1

.field public static final ToolBarHomeView_textStyle:I = 0x2

.field public static final ToolBarHomeView_upIndicatorDescription:I = 0x6

.field public static final ToolBarItem:[I

.field public static final ToolBarItemView:[I

.field public static final ToolBarItemView_badgeIndicatorStyle:I = 0x0

.field public static final ToolBarItemView_textColor:I = 0x1

.field public static final ToolBarItemView_textSize:I = 0x2

.field public static final ToolBarItem_actionLayout:I = 0xb

.field public static final ToolBarItem_android_contentDescription:I = 0x5

.field public static final ToolBarItem_android_enabled:I = 0x1

.field public static final ToolBarItem_android_icon:I = 0x0

.field public static final ToolBarItem_android_id:I = 0x2

.field public static final ToolBarItem_android_title:I = 0x4

.field public static final ToolBarItem_android_visible:I = 0x3

.field public static final ToolBarItem_order:I = 0x6

.field public static final ToolBarItem_overflowIcon:I = 0x9

.field public static final ToolBarItem_primaryItem:I = 0xa

.field public static final ToolBarItem_priority:I = 0x7

.field public static final ToolBarItem_showAsAction:I = 0xc

.field public static final ToolBarItem_subtitle:I = 0x8

.field public static final ToolBarLayout:[I

.field public static final ToolBarLayout_android_layout_gravity:I = 0x0

.field public static final ToolBar_popupMenuXOffset:I = 0x0

.field public static final ToolBar_popupMenuYOffset:I = 0x1

.field public static final ToolBar_stackFromRight:I = 0xd

.field public static final ToolBar_toolBarCustomViewId:I = 0xa

.field public static final ToolBar_toolBarDisplayOptions:I = 0xc

.field public static final ToolBar_toolBarHomeStyle:I = 0x2

.field public static final ToolBar_toolBarIcon:I = 0x7

.field public static final ToolBar_toolBarItemBackground:I = 0x4

.field public static final ToolBar_toolBarItemPadding:I = 0x5

.field public static final ToolBar_toolBarItemStyle:I = 0x3

.field public static final ToolBar_toolBarOverflowContentDescription:I = 0xb

.field public static final ToolBar_toolBarOverflowDrawable:I = 0x9

.field public static final ToolBar_toolBarTitle:I = 0x6

.field public static final ToolBar_toolBarUpIndicator:I = 0x8

.field public static final ViewPagerScrollBar:[I

.field public static final ViewPagerScrollBar_tabDrawable:I = 0x0

.field public static final ViewPagerScrollBar_tabMaxHeight:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v1, 0x7

    const/4 v5, 0x6

    const/4 v4, 0x3

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/twitter/internal/android/f;->BadgeIndicator:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/twitter/internal/android/f;->DockLayout:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/twitter/internal/android/f;->GroupedRowView:[I

    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/twitter/internal/android/f;->HiddenDrawerLayout:[I

    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/twitter/internal/android/f;->HorizontalListView:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/twitter/internal/android/f;->NavItemView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/twitter/internal/android/f;->NotchView:[I

    new-array v0, v3, [I

    const v1, 0x7f0100d3    # com.twitter.android.R.attr.overlayDrawable

    aput v1, v0, v2

    sput-object v0, Lcom/twitter/internal/android/f;->OverlayImageView:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/twitter/internal/android/f;->PopupEditText:[I

    const/16 v0, 0x12

    new-array v0, v0, [I

    fill-array-data v0, :array_8

    sput-object v0, Lcom/twitter/internal/android/f;->RoundedGroupedRowView:[I

    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_9

    sput-object v0, Lcom/twitter/internal/android/f;->SegmentedControl:[I

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_a

    sput-object v0, Lcom/twitter/internal/android/f;->ShadowTextView:[I

    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_b

    sput-object v0, Lcom/twitter/internal/android/f;->ToolBar:[I

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_c

    sput-object v0, Lcom/twitter/internal/android/f;->ToolBarHomeView:[I

    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_d

    sput-object v0, Lcom/twitter/internal/android/f;->ToolBarItem:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_e

    sput-object v0, Lcom/twitter/internal/android/f;->ToolBarItemView:[I

    new-array v0, v3, [I

    const v1, 0x10100b3    # android.R.attr.layout_gravity

    aput v1, v0, v2

    sput-object v0, Lcom/twitter/internal/android/f;->ToolBarLayout:[I

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_f

    sput-object v0, Lcom/twitter/internal/android/f;->ViewPagerScrollBar:[I

    return-void

    :array_0
    .array-data 4
        0x7f010065    # com.twitter.android.R.attr.indicatorDrawable
        0x7f010066    # com.twitter.android.R.attr.indicatorMarginBottom
        0x7f010067    # com.twitter.android.R.attr.numberBackground
        0x7f010068    # com.twitter.android.R.attr.numberColor
        0x7f010069    # com.twitter.android.R.attr.numberTextSize
        0x7f01006a    # com.twitter.android.R.attr.numberMinWidth
        0x7f01006b    # com.twitter.android.R.attr.numberMinHeight
        0x7f01006c    # com.twitter.android.R.attr.badgeMode
    .end array-data

    :array_1
    .array-data 4
        0x7f010083    # com.twitter.android.R.attr.topDockId
        0x7f010084    # com.twitter.android.R.attr.bottomDockId
        0x7f010085    # com.twitter.android.R.attr.turtle
        0x7f010086    # com.twitter.android.R.attr.topPeek
        0x7f010087    # com.twitter.android.R.attr.bottomPeek
        0x7f010088    # com.twitter.android.R.attr.scrollDrive
        0x7f010089    # com.twitter.android.R.attr.autoUnlock
    .end array-data

    :array_2
    .array-data 4
        0x7f010005    # com.twitter.android.R.attr.cardStyle
        0x7f010012    # com.twitter.android.R.attr.groupStyle
        0x7f010038    # com.twitter.android.R.attr.single
        0x7f0100a9    # com.twitter.android.R.attr.fillColor
        0x7f0100aa    # com.twitter.android.R.attr.borderColor
        0x7f0100ab    # com.twitter.android.R.attr.borderHeight
        0x7f0100ac    # com.twitter.android.R.attr.gapSize
    .end array-data

    :array_3
    .array-data 4
        0x7f0100ad    # com.twitter.android.R.attr.openInterpolator
        0x7f0100ae    # com.twitter.android.R.attr.closeInterpolator
        0x7f0100af    # com.twitter.android.R.attr.openAnimDuration
        0x7f0100b0    # com.twitter.android.R.attr.closeAnimDuration
        0x7f0100b1    # com.twitter.android.R.attr.gutterSize
        0x7f0100b2    # com.twitter.android.R.attr.gutterColor
        0x7f0100b3    # com.twitter.android.R.attr.bgColorHint
        0x7f0100b4    # com.twitter.android.R.attr.drawerDirection
        0x7f0100b5    # com.twitter.android.R.attr.draggableEdgeSize
        0x7f0100b6    # com.twitter.android.R.attr.draggable
    .end array-data

    :array_4
    .array-data 4
        0x7f0100b7    # com.twitter.android.R.attr.listDivider
        0x7f0100b8    # com.twitter.android.R.attr.dividerWidth
        0x7f0100b9    # com.twitter.android.R.attr.edgePadding
        0x7f0100ba    # com.twitter.android.R.attr.fillWidthHeightRatio
        0x7f0100bb    # com.twitter.android.R.attr.scrollOffset
        0x7f0100bc    # com.twitter.android.R.attr.scrollDrawable
        0x7f0100bd    # com.twitter.android.R.attr.scrollHeight
        0x7f0100be    # com.twitter.android.R.attr.fillMode
        0x7f0100bf    # com.twitter.android.R.attr.leftFadeInDrawable
        0x7f0100c0    # com.twitter.android.R.attr.rightFadeInDrawable
    .end array-data

    :array_5
    .array-data 4
        0x7f010003    # com.twitter.android.R.attr.badgeIndicatorStyle
        0x7f010032    # com.twitter.android.R.attr.selectedTextStyle
        0x7f01004f    # com.twitter.android.R.attr.textColor
        0x7f010050    # com.twitter.android.R.attr.textSize
        0x7f010051    # com.twitter.android.R.attr.textStyle
        0x7f0100cf    # com.twitter.android.R.attr.displayMode
    .end array-data

    :array_6
    .array-data 4
        0x7f0100d0    # com.twitter.android.R.attr.notchDrawable
        0x7f0100d1    # com.twitter.android.R.attr.notchBg
        0x7f0100d2    # com.twitter.android.R.attr.notchLeftOffset
    .end array-data

    :array_7
    .array-data 4
        0x7f010029    # com.twitter.android.R.attr.popupMenuXOffset
        0x7f01002a    # com.twitter.android.R.attr.popupMenuYOffset
        0x7f0100e6    # com.twitter.android.R.attr.showAsDropdown
        0x7f0100e7    # com.twitter.android.R.attr.showFullScreen
        0x7f0100e8    # com.twitter.android.R.attr.threshold
        0x7f0100e9    # com.twitter.android.R.attr.showPopupOnInitialFocus
    .end array-data

    :array_8
    .array-data 4
        0x7f010005    # com.twitter.android.R.attr.cardStyle
        0x7f010012    # com.twitter.android.R.attr.groupStyle
        0x7f010033    # com.twitter.android.R.attr.shadowColor
        0x7f010034    # com.twitter.android.R.attr.shadowDx
        0x7f010035    # com.twitter.android.R.attr.shadowDy
        0x7f010036    # com.twitter.android.R.attr.shadowRadius
        0x7f010038    # com.twitter.android.R.attr.single
        0x7f0100f7    # com.twitter.android.R.attr.inset
        0x7f0100f8    # com.twitter.android.R.attr.insetLeft
        0x7f0100f9    # com.twitter.android.R.attr.insetTop
        0x7f0100fa    # com.twitter.android.R.attr.insetRight
        0x7f0100fb    # com.twitter.android.R.attr.insetBottom
        0x7f0100fc    # com.twitter.android.R.attr.insetBottomFillColor
        0x7f0100fd    # com.twitter.android.R.attr.cornerRadius
        0x7f0100fe    # com.twitter.android.R.attr.strokeWidth
        0x7f0100ff    # com.twitter.android.R.attr.strokeColor
        0x7f010100    # com.twitter.android.R.attr.dividerHeight
        0x7f010101    # com.twitter.android.R.attr.dividerColor
    .end array-data

    :array_9
    .array-data 4
        0x7f010033    # com.twitter.android.R.attr.shadowColor
        0x7f010034    # com.twitter.android.R.attr.shadowDx
        0x7f010035    # com.twitter.android.R.attr.shadowDy
        0x7f010036    # com.twitter.android.R.attr.shadowRadius
        0x7f01004f    # com.twitter.android.R.attr.textColor
        0x7f010050    # com.twitter.android.R.attr.textSize
        0x7f010103    # com.twitter.android.R.attr.src
        0x7f010104    # com.twitter.android.R.attr.segmentDivider
        0x7f010105    # com.twitter.android.R.attr.segmentLabels
    .end array-data

    :array_a
    .array-data 4
        0x7f010033    # com.twitter.android.R.attr.shadowColor
        0x7f010034    # com.twitter.android.R.attr.shadowDx
        0x7f010035    # com.twitter.android.R.attr.shadowDy
        0x7f010036    # com.twitter.android.R.attr.shadowRadius
    .end array-data

    :array_b
    .array-data 4
        0x7f010029    # com.twitter.android.R.attr.popupMenuXOffset
        0x7f01002a    # com.twitter.android.R.attr.popupMenuYOffset
        0x7f010052    # com.twitter.android.R.attr.toolBarHomeStyle
        0x7f010053    # com.twitter.android.R.attr.toolBarItemStyle
        0x7f010124    # com.twitter.android.R.attr.toolBarItemBackground
        0x7f010125    # com.twitter.android.R.attr.toolBarItemPadding
        0x7f010126    # com.twitter.android.R.attr.toolBarTitle
        0x7f010127    # com.twitter.android.R.attr.toolBarIcon
        0x7f010128    # com.twitter.android.R.attr.toolBarUpIndicator
        0x7f010129    # com.twitter.android.R.attr.toolBarOverflowDrawable
        0x7f01012a    # com.twitter.android.R.attr.toolBarCustomViewId
        0x7f01012b    # com.twitter.android.R.attr.toolBarOverflowContentDescription
        0x7f01012c    # com.twitter.android.R.attr.toolBarDisplayOptions
        0x7f01012d    # com.twitter.android.R.attr.stackFromRight
    .end array-data

    :array_c
    .array-data 4
        0x7f01004f    # com.twitter.android.R.attr.textColor
        0x7f010050    # com.twitter.android.R.attr.textSize
        0x7f010051    # com.twitter.android.R.attr.textStyle
        0x7f010067    # com.twitter.android.R.attr.numberBackground
        0x7f010068    # com.twitter.android.R.attr.numberColor
        0x7f01012e    # com.twitter.android.R.attr.subtitleTextSize
        0x7f01012f    # com.twitter.android.R.attr.upIndicatorDescription
        0x7f010130    # com.twitter.android.R.attr.allCaps
    .end array-data

    :array_d
    .array-data 4
        0x1010002    # android.R.attr.icon
        0x101000e    # android.R.attr.enabled
        0x10100d0    # android.R.attr.id
        0x1010194    # android.R.attr.visible
        0x10101e1    # android.R.attr.title
        0x1010273    # android.R.attr.contentDescription
        0x7f01001f    # com.twitter.android.R.attr.order
        0x7f01002b    # com.twitter.android.R.attr.priority
        0x7f010131    # com.twitter.android.R.attr.subtitle
        0x7f010132    # com.twitter.android.R.attr.overflowIcon
        0x7f010133    # com.twitter.android.R.attr.primaryItem
        0x7f010134    # com.twitter.android.R.attr.actionLayout
        0x7f010135    # com.twitter.android.R.attr.showAsAction
    .end array-data

    :array_e
    .array-data 4
        0x7f010003    # com.twitter.android.R.attr.badgeIndicatorStyle
        0x7f01004f    # com.twitter.android.R.attr.textColor
        0x7f010050    # com.twitter.android.R.attr.textSize
    .end array-data

    :array_f
    .array-data 4
        0x7f0101bc    # com.twitter.android.R.attr.tabDrawable
        0x7f0101bd    # com.twitter.android.R.attr.tabMaxHeight
    .end array-data
.end method
