.class public final Lcom/twitter/internal/android/d;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final always:I = 0x7f090026

.field public static final bold:I = 0x7f090008

.field public static final bottom:I = 0x7f090003

.field public static final collapseActionView:I = 0x7f090028

.field public static final decoupled:I = 0x7f09000f

.field public static final disabled:I = 0x7f09000a

.field public static final distribute_evenly:I = 0x7f090019

.field public static final distribute_remainder:I = 0x7f09001a

.field public static final down:I = 0x7f090017

.field public static final fullExpand:I = 0x7f090023

.field public static final headerBottom:I = 0x7f090006

.field public static final headerMiddle:I = 0x7f090005

.field public static final headerTop:I = 0x7f090004

.field public static final home:I = 0x7f090045

.field public static final homeAsUp:I = 0x7f090021

.field public static final icon:I = 0x7f09001f

.field public static final ifRoom:I = 0x7f090025

.field public static final indicator:I = 0x7f09000b

.field public static final italic:I = 0x7f090009

.field public static final left:I = 0x7f09000d

.field public static final middle:I = 0x7f090002

.field public static final never:I = 0x7f090024

.field public static final none:I = 0x7f090000

.field public static final normal:I = 0x7f090007

.field public static final number:I = 0x7f09000c

.field public static final overflow:I = 0x7f09004b

.field public static final overflow_item_icon:I = 0x7f090207

.field public static final overflow_item_subtitle:I = 0x7f090209

.field public static final overflow_item_title:I = 0x7f090208

.field public static final right:I = 0x7f09000e

.field public static final showHome:I = 0x7f090020

.field public static final showTitle:I = 0x7f090022

.field public static final text:I = 0x7f09001e

.field public static final top:I = 0x7f090001

.field public static final up:I = 0x7f090018

.field public static final withText:I = 0x7f090027
