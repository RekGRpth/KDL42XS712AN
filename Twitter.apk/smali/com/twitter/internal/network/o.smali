.class public Lcom/twitter/internal/network/o;
.super Lcom/twitter/internal/network/l;
.source "Twttr"


# instance fields
.field protected final b:Lcom/squareup/okhttp/j;


# direct methods
.method public constructor <init>(Lcom/twitter/internal/network/j;)V
    .locals 4

    invoke-direct {p0, p1}, Lcom/twitter/internal/network/l;-><init>(Lcom/twitter/internal/network/j;)V

    new-instance v0, Lcom/squareup/okhttp/j;

    invoke-direct {v0}, Lcom/squareup/okhttp/j;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/network/o;->b:Lcom/squareup/okhttp/j;

    iget-object v0, p0, Lcom/twitter/internal/network/o;->b:Lcom/squareup/okhttp/j;

    invoke-virtual {p0}, Lcom/twitter/internal/network/o;->b()Ljava/net/Proxy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/j;->a(Ljava/net/Proxy;)Lcom/squareup/okhttp/j;

    iget-object v0, p0, Lcom/twitter/internal/network/o;->b:Lcom/squareup/okhttp/j;

    const-wide/16 v1, 0x4e20

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/okhttp/j;->a(JLjava/util/concurrent/TimeUnit;)V

    iget-object v0, p0, Lcom/twitter/internal/network/o;->b:Lcom/squareup/okhttp/j;

    const-wide/32 v1, 0x15f90

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/okhttp/j;->b(JLjava/util/concurrent/TimeUnit;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;Ljava/net/URI;Lcom/twitter/internal/network/i;)Lcom/twitter/internal/network/HttpOperation;
    .locals 1

    new-instance v0, Lcom/twitter/internal/network/n;

    invoke-direct {v0, p0, p2, p1, p3}, Lcom/twitter/internal/network/n;-><init>(Lcom/twitter/internal/network/l;Ljava/net/URI;Lcom/twitter/internal/network/HttpOperation$RequestMethod;Lcom/twitter/internal/network/i;)V

    return-object v0
.end method

.method public a(Ljava/net/URL;)Ljava/net/HttpURLConnection;
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/network/o;->b:Lcom/squareup/okhttp/j;

    invoke-virtual {v0, p1}, Lcom/squareup/okhttp/j;->a(Ljava/net/URL;)Ljava/net/HttpURLConnection;

    move-result-object v0

    return-object v0
.end method
