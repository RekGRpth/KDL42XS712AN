.class public Lcom/twitter/internal/network/a;
.super Lcom/twitter/internal/network/HttpOperation;
.source "Twttr"


# instance fields
.field private final e:Lorg/apache/http/client/HttpClient;


# direct methods
.method public constructor <init>(Lorg/apache/http/client/HttpClient;Ljava/net/URI;Lcom/twitter/internal/network/HttpOperation$RequestMethod;Lcom/twitter/internal/network/i;)V
    .locals 2

    invoke-direct {p0, p3, p2, p4}, Lcom/twitter/internal/network/HttpOperation;-><init>(Lcom/twitter/internal/network/HttpOperation$RequestMethod;Ljava/net/URI;Lcom/twitter/internal/network/i;)V

    iput-object p1, p0, Lcom/twitter/internal/network/a;->e:Lorg/apache/http/client/HttpClient;

    const-string/jumbo v0, "Accept-Encoding"

    const-string/jumbo v1, "gzip"

    invoke-virtual {p0, v0, v1}, Lcom/twitter/internal/network/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/internal/network/HttpOperation;

    return-void
.end method


# virtual methods
.method protected synthetic a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    check-cast p1, Lorg/apache/http/HttpResponse;

    invoke-virtual {p0, p1}, Lcom/twitter/internal/network/a;->f(Lorg/apache/http/HttpResponse;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Ljava/lang/String;I)Ljava/lang/String;
    .locals 1

    check-cast p1, Lorg/apache/http/HttpResponse;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/internal/network/a;->a(Lorg/apache/http/HttpResponse;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lorg/apache/http/HttpResponse;)Ljava/lang/String;
    .locals 1

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContentEncoding()Lorg/apache/http/Header;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lorg/apache/http/HttpResponse;Ljava/lang/String;I)Ljava/lang/String;
    .locals 2

    invoke-interface {p1, p2}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v0

    array-length v1, v0

    if-le v1, p3, :cond_0

    aget-object v0, v0, p3

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a()Lorg/apache/http/client/methods/HttpRequestBase;
    .locals 3

    sget-object v0, Lcom/twitter/internal/network/b;->a:[I

    iget-object v1, p0, Lcom/twitter/internal/network/a;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "No HttpRequest defined for a "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/internal/network/a;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/internal/network/a;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    iget-object v1, p0, Lcom/twitter/internal/network/a;->c:Ljava/net/URI;

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    :goto_0
    return-object v0

    :pswitch_1
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    iget-object v1, p0, Lcom/twitter/internal/network/a;->c:Ljava/net/URI;

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/net/URI;)V

    goto :goto_0

    :pswitch_2
    new-instance v0, Lorg/apache/http/client/methods/HttpPut;

    iget-object v1, p0, Lcom/twitter/internal/network/a;->c:Ljava/net/URI;

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/net/URI;)V

    goto :goto_0

    :pswitch_3
    new-instance v0, Lorg/apache/http/client/methods/HttpDelete;

    iget-object v1, p0, Lcom/twitter/internal/network/a;->c:Ljava/net/URI;

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpDelete;-><init>(Ljava/net/URI;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected bridge synthetic a(Ljava/lang/Object;I)V
    .locals 0

    check-cast p1, Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/internal/network/a;->a(Lorg/apache/http/client/methods/HttpRequestBase;I)V

    return-void
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    check-cast p1, Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/internal/network/a;->a(Lorg/apache/http/client/methods/HttpRequestBase;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Lorg/apache/http/HttpEntity;)V
    .locals 0

    check-cast p1, Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/internal/network/a;->a(Lorg/apache/http/client/methods/HttpRequestBase;Lorg/apache/http/HttpEntity;)V

    return-void
.end method

.method protected a(Lorg/apache/http/client/methods/HttpRequestBase;)V
    .locals 0

    invoke-virtual {p1}, Lorg/apache/http/client/methods/HttpRequestBase;->abort()V

    return-void
.end method

.method protected a(Lorg/apache/http/client/methods/HttpRequestBase;I)V
    .locals 2

    invoke-virtual {p1}, Lorg/apache/http/client/methods/HttpRequestBase;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    const-string/jumbo v1, "http.socket.timeout"

    invoke-interface {v0, v1, p2}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    return-void
.end method

.method protected a(Lorg/apache/http/client/methods/HttpRequestBase;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p1, p2, p3}, Lorg/apache/http/client/methods/HttpRequestBase;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected a(Lorg/apache/http/client/methods/HttpRequestBase;Lorg/apache/http/HttpEntity;)V
    .locals 0

    if-eqz p2, :cond_0

    check-cast p1, Lorg/apache/http/client/methods/HttpEntityEnclosingRequestBase;

    invoke-virtual {p1, p2}, Lorg/apache/http/client/methods/HttpEntityEnclosingRequestBase;->setEntity(Lorg/apache/http/HttpEntity;)V

    :cond_0
    return-void
.end method

.method protected synthetic b(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lorg/apache/http/HttpResponse;

    invoke-virtual {p0, p1}, Lcom/twitter/internal/network/a;->e(Lorg/apache/http/HttpResponse;)I

    move-result v0

    return v0
.end method

.method protected b()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "Apache"

    return-object v0
.end method

.method protected b(Lorg/apache/http/HttpResponse;)Ljava/lang/String;
    .locals 1

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(Lorg/apache/http/client/methods/HttpRequestBase;)Lorg/apache/http/HttpResponse;
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/network/a;->e:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0, p1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method protected c(Lorg/apache/http/HttpResponse;)I
    .locals 2

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v0

    long-to-int v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected synthetic c()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/internal/network/a;->a()Lorg/apache/http/client/methods/HttpRequestBase;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic c(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {p0, p1}, Lcom/twitter/internal/network/a;->b(Lorg/apache/http/client/methods/HttpRequestBase;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method protected d(Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;
    .locals 2

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->isStreaming()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected synthetic d(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {p0, p1}, Lcom/twitter/internal/network/a;->a(Lorg/apache/http/client/methods/HttpRequestBase;)V

    return-void
.end method

.method protected e(Lorg/apache/http/HttpResponse;)I
    .locals 1

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    return v0
.end method

.method protected synthetic e(Ljava/lang/Object;)Ljava/io/InputStream;
    .locals 1

    check-cast p1, Lorg/apache/http/HttpResponse;

    invoke-virtual {p0, p1}, Lcom/twitter/internal/network/a;->d(Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic f(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lorg/apache/http/HttpResponse;

    invoke-virtual {p0, p1}, Lcom/twitter/internal/network/a;->c(Lorg/apache/http/HttpResponse;)I

    move-result v0

    return v0
.end method

.method protected f(Lorg/apache/http/HttpResponse;)Ljava/lang/String;
    .locals 1

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic g(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    check-cast p1, Lorg/apache/http/HttpResponse;

    invoke-virtual {p0, p1}, Lcom/twitter/internal/network/a;->b(Lorg/apache/http/HttpResponse;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic h(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    check-cast p1, Lorg/apache/http/HttpResponse;

    invoke-virtual {p0, p1}, Lcom/twitter/internal/network/a;->a(Lorg/apache/http/HttpResponse;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
