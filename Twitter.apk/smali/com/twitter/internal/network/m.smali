.class public Lcom/twitter/internal/network/m;
.super Lcom/twitter/internal/network/HttpOperation;
.source "Twttr"


# instance fields
.field private final e:Lcom/twitter/internal/network/l;


# direct methods
.method public constructor <init>(Lcom/twitter/internal/network/l;Ljava/net/URI;Lcom/twitter/internal/network/HttpOperation$RequestMethod;Lcom/twitter/internal/network/i;)V
    .locals 0

    invoke-direct {p0, p3, p2, p4}, Lcom/twitter/internal/network/HttpOperation;-><init>(Lcom/twitter/internal/network/HttpOperation$RequestMethod;Ljava/net/URI;Lcom/twitter/internal/network/i;)V

    iput-object p1, p0, Lcom/twitter/internal/network/m;->e:Lcom/twitter/internal/network/l;

    return-void
.end method

.method private b(Ljava/net/HttpURLConnection;Lorg/apache/http/HttpEntity;)V
    .locals 2

    new-instance v0, Ljava/io/BufferedOutputStream;

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-interface {p2, v0}, Lorg/apache/http/HttpEntity;->writeTo(Ljava/io/OutputStream;)V

    invoke-static {v0}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-interface {p2}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    return-void
.end method


# virtual methods
.method protected synthetic a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    check-cast p1, Ljava/net/HttpURLConnection;

    invoke-virtual {p0, p1}, Lcom/twitter/internal/network/m;->i(Ljava/net/HttpURLConnection;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Ljava/lang/String;I)Ljava/lang/String;
    .locals 1

    check-cast p1, Ljava/net/HttpURLConnection;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/internal/network/m;->a(Ljava/net/HttpURLConnection;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/net/HttpURLConnection;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getContentEncoding()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/net/HttpURLConnection;Ljava/lang/String;I)Ljava/lang/String;
    .locals 2

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, p3, :cond_0

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a()Ljava/net/HttpURLConnection;
    .locals 3

    iget-object v0, p0, Lcom/twitter/internal/network/m;->e:Lcom/twitter/internal/network/l;

    invoke-virtual {p0}, Lcom/twitter/internal/network/m;->h()Ljava/net/URI;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URI;->toURL()Ljava/net/URL;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/internal/network/l;->a(Ljava/net/URL;)Ljava/net/HttpURLConnection;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/internal/network/m;->g()Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    const-string/jumbo v1, "Accept-Encoding"

    const-string/jumbo v2, "gzip"

    invoke-virtual {p0, v1, v2}, Lcom/twitter/internal/network/m;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/internal/network/HttpOperation;

    return-object v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;I)V
    .locals 0

    check-cast p1, Ljava/net/HttpURLConnection;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/internal/network/m;->a(Ljava/net/HttpURLConnection;I)V

    return-void
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    check-cast p1, Ljava/net/HttpURLConnection;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/internal/network/m;->a(Ljava/net/HttpURLConnection;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Lorg/apache/http/HttpEntity;)V
    .locals 0

    check-cast p1, Ljava/net/HttpURLConnection;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/internal/network/m;->a(Ljava/net/HttpURLConnection;Lorg/apache/http/HttpEntity;)V

    return-void
.end method

.method protected a(Ljava/net/HttpURLConnection;I)V
    .locals 0

    invoke-virtual {p1, p2}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    return-void
.end method

.method protected a(Ljava/net/HttpURLConnection;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p1, p2, p3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected a(Ljava/net/HttpURLConnection;Lorg/apache/http/HttpEntity;)V
    .locals 3

    if-eqz p2, :cond_3

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    invoke-interface {p2}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "Content-Type"

    invoke-virtual {p0, v1}, Lcom/twitter/internal/network/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "Content-Type"

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-interface {p2}, Lorg/apache/http/HttpEntity;->isChunked()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x1000

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setChunkedStreamingMode(I)V

    :goto_0
    return-void

    :cond_1
    invoke-interface {p2}, Lorg/apache/http/HttpEntity;->isStreaming()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p2}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v0

    long-to-int v0, v0

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "Content-Length"

    invoke-interface {p2}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/internal/network/m;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/internal/network/HttpOperation;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    goto :goto_0
.end method

.method protected synthetic b(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Ljava/net/HttpURLConnection;

    invoke-virtual {p0, p1}, Lcom/twitter/internal/network/m;->h(Ljava/net/HttpURLConnection;)I

    move-result v0

    return v0
.end method

.method protected b()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "HttpURLConnection"

    return-object v0
.end method

.method protected b(Ljava/net/HttpURLConnection;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected c(Ljava/net/HttpURLConnection;)I
    .locals 1

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v0

    return v0
.end method

.method protected synthetic c()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/internal/network/m;->a()Ljava/net/HttpURLConnection;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic c(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Ljava/net/HttpURLConnection;

    invoke-virtual {p0, p1}, Lcom/twitter/internal/network/m;->g(Ljava/net/HttpURLConnection;)Ljava/net/HttpURLConnection;

    move-result-object v0

    return-object v0
.end method

.method protected d(Ljava/net/HttpURLConnection;)Ljava/io/InputStream;
    .locals 1

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    goto :goto_0
.end method

.method protected synthetic d(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/net/HttpURLConnection;

    invoke-virtual {p0, p1}, Lcom/twitter/internal/network/m;->f(Ljava/net/HttpURLConnection;)V

    return-void
.end method

.method protected synthetic e(Ljava/lang/Object;)Ljava/io/InputStream;
    .locals 1

    check-cast p1, Ljava/net/HttpURLConnection;

    invoke-virtual {p0, p1}, Lcom/twitter/internal/network/m;->d(Ljava/net/HttpURLConnection;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method protected e(Ljava/net/HttpURLConnection;)V
    .locals 0

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->disconnect()V

    return-void
.end method

.method protected synthetic f(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Ljava/net/HttpURLConnection;

    invoke-virtual {p0, p1}, Lcom/twitter/internal/network/m;->c(Ljava/net/HttpURLConnection;)I

    move-result v0

    return v0
.end method

.method protected f(Ljava/net/HttpURLConnection;)V
    .locals 0

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->disconnect()V

    return-void
.end method

.method protected synthetic g(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    check-cast p1, Ljava/net/HttpURLConnection;

    invoke-virtual {p0, p1}, Lcom/twitter/internal/network/m;->b(Ljava/net/HttpURLConnection;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected g(Ljava/net/HttpURLConnection;)Ljava/net/HttpURLConnection;
    .locals 1

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->connect()V

    invoke-virtual {p0}, Lcom/twitter/internal/network/m;->f()Lorg/apache/http/HttpEntity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, v0}, Lcom/twitter/internal/network/m;->b(Ljava/net/HttpURLConnection;Lorg/apache/http/HttpEntity;)V

    :cond_0
    return-object p1
.end method

.method protected h(Ljava/net/HttpURLConnection;)I
    .locals 1

    :try_start_0
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getResponseCode()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    goto :goto_0
.end method

.method protected synthetic h(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    check-cast p1, Ljava/net/HttpURLConnection;

    invoke-virtual {p0, p1}, Lcom/twitter/internal/network/m;->a(Ljava/net/HttpURLConnection;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected i(Ljava/net/HttpURLConnection;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic i(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/net/HttpURLConnection;

    invoke-virtual {p0, p1}, Lcom/twitter/internal/network/m;->e(Ljava/net/HttpURLConnection;)V

    return-void
.end method
