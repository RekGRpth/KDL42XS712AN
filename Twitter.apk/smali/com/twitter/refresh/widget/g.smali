.class Lcom/twitter/refresh/widget/g;
.super Landroid/widget/HeaderViewListAdapter;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/refresh/widget/RefreshableListView;

.field private final b:Lcom/twitter/refresh/widget/f;

.field private c:Z

.field private d:Landroid/widget/ListAdapter;

.field private final e:Ljava/util/ArrayList;

.field private final f:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Lcom/twitter/refresh/widget/RefreshableListView;Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/widget/ListAdapter;Lcom/twitter/refresh/widget/f;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/refresh/widget/g;->a:Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/HeaderViewListAdapter;-><init>(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/widget/ListAdapter;)V

    iput-object p4, p0, Lcom/twitter/refresh/widget/g;->d:Landroid/widget/ListAdapter;

    iput-object p2, p0, Lcom/twitter/refresh/widget/g;->e:Ljava/util/ArrayList;

    iput-object p3, p0, Lcom/twitter/refresh/widget/g;->f:Ljava/util/ArrayList;

    iput-object p5, p0, Lcom/twitter/refresh/widget/g;->b:Lcom/twitter/refresh/widget/f;

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/g;->b()V

    return-void
.end method

.method private a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/g;->getHeadersCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/refresh/widget/g;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView$FixedViewInfo;

    iget-object v0, v0, Landroid/widget/ListView$FixedViewInfo;->view:Landroid/view/View;

    :goto_0
    return-object v0

    :cond_0
    sub-int v2, p1, v0

    iget-object v0, p0, Lcom/twitter/refresh/widget/g;->d:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/refresh/widget/g;->d:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-ge v2, v0, :cond_2

    iget-object v0, p0, Lcom/twitter/refresh/widget/g;->d:Landroid/widget/ListAdapter;

    invoke-interface {v0, v2, p2, p3}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v0, v1

    :cond_2
    sub-int v0, v2, v0

    iget-object v2, p0, Lcom/twitter/refresh/widget/g;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    if-gez v0, :cond_4

    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/twitter/refresh/widget/g;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView$FixedViewInfo;

    iget-object v0, v0, Landroid/widget/ListView$FixedViewInfo;->view:Landroid/view/View;

    goto :goto_0

    :cond_4
    move v1, v0

    goto :goto_1
.end method


# virtual methods
.method a()I
    .locals 1

    invoke-super {p0}, Landroid/widget/HeaderViewListAdapter;->getCount()I

    move-result v0

    return v0
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final b()V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/refresh/widget/g;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/refresh/widget/g;->b:Lcom/twitter/refresh/widget/f;

    invoke-super {p0, v0}, Landroid/widget/HeaderViewListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/refresh/widget/g;->c:Z

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/refresh/widget/g;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/refresh/widget/g;->b:Lcom/twitter/refresh/widget/f;

    invoke-super {p0, v0}, Landroid/widget/HeaderViewListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/refresh/widget/g;->c:Z

    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/refresh/widget/g;->a:Lcom/twitter/refresh/widget/RefreshableListView;

    iget v0, v0, Lcom/twitter/refresh/widget/RefreshableListView;->c:I

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/twitter/refresh/widget/g;->a:Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-virtual {v0}, Lcom/twitter/refresh/widget/RefreshableListView;->getRefreshHeaderPosition()I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    if-ge p1, v0, :cond_1

    invoke-super {p0, p1}, Landroid/widget/HeaderViewListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_1
    add-int/lit8 v0, p1, -0x1

    invoke-super {p0, v0}, Landroid/widget/HeaderViewListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    iget-object v0, p0, Lcom/twitter/refresh/widget/g;->a:Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-virtual {v0}, Lcom/twitter/refresh/widget/RefreshableListView;->getRefreshHeaderPosition()I

    move-result v0

    if-ne p1, v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    if-ge p1, v0, :cond_1

    invoke-super {p0, p1}, Landroid/widget/HeaderViewListAdapter;->getItemId(I)J

    move-result-wide v0

    goto :goto_0

    :cond_1
    add-int/lit8 v0, p1, -0x1

    invoke-super {p0, v0}, Landroid/widget/HeaderViewListAdapter;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 1

    iget-object v0, p0, Lcom/twitter/refresh/widget/g;->a:Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-virtual {v0}, Lcom/twitter/refresh/widget/RefreshableListView;->getRefreshHeaderPosition()I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    if-ge p1, v0, :cond_1

    invoke-super {p0, p1}, Landroid/widget/HeaderViewListAdapter;->getItemViewType(I)I

    move-result v0

    goto :goto_0

    :cond_1
    add-int/lit8 v0, p1, -0x1

    invoke-super {p0, v0}, Landroid/widget/HeaderViewListAdapter;->getItemViewType(I)I

    move-result v0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/twitter/refresh/widget/g;->a:Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-virtual {v0}, Lcom/twitter/refresh/widget/RefreshableListView;->getRefreshHeaderPosition()I

    move-result v0

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/refresh/widget/g;->a:Lcom/twitter/refresh/widget/RefreshableListView;

    const/16 v1, 0x22

    invoke-virtual {v0, v1}, Lcom/twitter/refresh/widget/RefreshableListView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/refresh/widget/g;->a:Lcom/twitter/refresh/widget/RefreshableListView;

    iget-object v0, v0, Lcom/twitter/refresh/widget/RefreshableListView;->b:Landroid/widget/RelativeLayout;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/refresh/widget/g;->a:Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-static {v0}, Lcom/twitter/refresh/widget/RefreshableListView;->e(Lcom/twitter/refresh/widget/RefreshableListView;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_1
    if-ge p1, v0, :cond_2

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/refresh/widget/g;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_2
    add-int/lit8 v0, p1, -0x1

    invoke-direct {p0, v0, p2, p3}, Lcom/twitter/refresh/widget/g;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/refresh/widget/g;->a:Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-virtual {v0}, Lcom/twitter/refresh/widget/RefreshableListView;->getRefreshHeaderPosition()I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/refresh/widget/g;->a:Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-virtual {v0}, Lcom/twitter/refresh/widget/RefreshableListView;->getRefreshHeaderPosition()I

    move-result v0

    if-ge p1, v0, :cond_1

    invoke-super {p0, p1}, Landroid/widget/HeaderViewListAdapter;->isEnabled(I)Z

    move-result v0

    goto :goto_0

    :cond_1
    add-int/lit8 v0, p1, -0x1

    invoke-super {p0, v0}, Landroid/widget/HeaderViewListAdapter;->isEnabled(I)Z

    move-result v0

    goto :goto_0
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/refresh/widget/g;->b:Lcom/twitter/refresh/widget/f;

    invoke-virtual {v0, p1}, Lcom/twitter/refresh/widget/f;->a(Landroid/database/DataSetObserver;)V

    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/refresh/widget/g;->b:Lcom/twitter/refresh/widget/f;

    invoke-virtual {v0, p1}, Lcom/twitter/refresh/widget/f;->b(Landroid/database/DataSetObserver;)V

    return-void
.end method
