.class public final Lcom/twitter/refresh/widget/RefreshableListView;
.super Landroid/widget/ListView;
.source "Twttr"

# interfaces
.implements Lcom/twitter/refresh/widget/j;


# instance fields
.field private final A:Ljava/lang/String;

.field private B:I

.field private C:Z

.field private D:Z

.field private final E:Ljava/util/ArrayList;

.field private final F:Ljava/util/ArrayList;

.field private G:I

.field private H:Lcom/twitter/refresh/widget/c;

.field final a:Landroid/widget/Scroller;

.field final b:Landroid/widget/RelativeLayout;

.field c:I

.field private final d:Lcom/twitter/refresh/widget/i;

.field private final e:Landroid/view/animation/Animation;

.field private final f:Landroid/view/animation/Animation;

.field private final g:Landroid/widget/ProgressBar;

.field private final h:Landroid/widget/ImageView;

.field private final i:Landroid/widget/TextView;

.field private final j:Landroid/view/View;

.field private k:Z

.field private l:Landroid/view/View;

.field private m:Landroid/widget/TextView;

.field private final n:I

.field private o:Lcom/twitter/refresh/widget/g;

.field private p:Lcom/twitter/refresh/widget/f;

.field private q:Lcom/twitter/refresh/widget/d;

.field private r:Lcom/twitter/refresh/widget/e;

.field private final s:I

.field private t:I

.field private u:I

.field private final v:I

.field private w:I

.field private x:I

.field private final y:Ljava/lang/String;

.field private final z:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/refresh/widget/RefreshableListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget v0, Lla;->refreshableListViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/refresh/widget/RefreshableListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    const/4 v6, -0x1

    const/4 v5, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v5, p0, Lcom/twitter/refresh/widget/RefreshableListView;->x:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->E:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->F:Ljava/util/ArrayList;

    sget-object v0, Llc;->RefreshableListView:[I

    invoke-virtual {p1, p2, v0, p3, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    const/4 v0, 0x5

    invoke-virtual {v2, v0, v6}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->s:I

    const/4 v0, 0x6

    invoke-virtual {v2, v0, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    new-instance v0, Landroid/widget/Scroller;

    invoke-direct {v0, p1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->a:Landroid/widget/Scroller;

    new-instance v0, Lcom/twitter/refresh/widget/i;

    invoke-direct {v0, p0}, Lcom/twitter/refresh/widget/i;-><init>(Lcom/twitter/refresh/widget/RefreshableListView;)V

    iput-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->d:Lcom/twitter/refresh/widget/i;

    const/4 v0, 0x7

    invoke-virtual {v2, v0, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->e:Landroid/view/animation/Animation;

    const/16 v0, 0x8

    invoke-virtual {v2, v0, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->f:Landroid/view/animation/Animation;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v2, v5, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    invoke-virtual {v0, v3, p0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    sget v3, Llb;->refresh_divider:I

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    sget v1, Llb;->refresh_loading:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/twitter/refresh/widget/RefreshableListView;->g:Landroid/widget/ProgressBar;

    sget v1, Llb;->refresh_icon:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/twitter/refresh/widget/RefreshableListView;->h:Landroid/widget/ImageView;

    sget v1, Llb;->refresh_text:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/twitter/refresh/widget/RefreshableListView;->i:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->b:Landroid/widget/RelativeLayout;

    new-instance v1, Landroid/view/View;

    invoke-direct {v1, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/twitter/refresh/widget/RefreshableListView;->j:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/refresh/widget/RefreshableListView;->j:Landroid/view/View;

    new-instance v3, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v3, v6, v5, v6}, Landroid/widget/AbsListView$LayoutParams;-><init>(III)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/twitter/refresh/widget/RefreshableListView;->v:I

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    new-instance v3, Landroid/widget/AbsListView$LayoutParams;

    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-direct {v3, v4, v1, v6}, Landroid/widget/AbsListView$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v0, 0x1

    invoke-virtual {v2, v0, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->n:I

    const/4 v0, 0x3

    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->y:Ljava/lang/String;

    const/4 v0, 0x2

    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->z:Ljava/lang/String;

    const/4 v0, 0x4

    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->A:Ljava/lang/String;

    const/16 v0, 0xa

    invoke-virtual {v2, v0, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->G:I

    const/16 v0, 0x9

    invoke-virtual {v2, v0, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->k:Z

    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/refresh/widget/RefreshableListView;)I
    .locals 1

    iget v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->G:I

    return v0
.end method

.method private a(Ljava/util/ArrayList;)V
    .locals 6

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView$FixedViewInfo;

    iget-object v1, v0, Landroid/widget/ListView$FixedViewInfo;->view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/AbsListView$LayoutParams;

    if-eqz v1, :cond_0

    iget-object v0, v0, Landroid/widget/ListView$FixedViewInfo;->view:Landroid/view/View;

    new-instance v3, Landroid/widget/AbsListView$LayoutParams;

    iget v4, v1, Landroid/widget/AbsListView$LayoutParams;->width:I

    iget v1, v1, Landroid/widget/AbsListView$LayoutParams;->height:I

    const/4 v5, -0x2

    invoke-direct {v3, v4, v1, v5}, Landroid/widget/AbsListView$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/twitter/refresh/widget/RefreshableListView;)Lcom/twitter/refresh/widget/f;
    .locals 1

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->p:Lcom/twitter/refresh/widget/f;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/refresh/widget/RefreshableListView;)I
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getBottomPaddingOffset()I

    move-result v0

    return v0
.end method

.method private c(I)V
    .locals 1

    if-lez p1, :cond_0

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->d:Lcom/twitter/refresh/widget/i;

    invoke-virtual {v0, p1}, Lcom/twitter/refresh/widget/i;->a(I)V

    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/twitter/refresh/widget/RefreshableListView;)V
    .locals 0

    invoke-super {p0}, Landroid/widget/ListView;->layoutChildren()V

    return-void
.end method

.method static synthetic e(Lcom/twitter/refresh/widget/RefreshableListView;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->j:Landroid/view/View;

    return-object v0
.end method

.method private g()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->q:Lcom/twitter/refresh/widget/d;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a(Ljava/util/ArrayList;I)Landroid/view/View;
    .locals 3

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView$FixedViewInfo;

    iget-object v0, v0, Landroid/widget/ListView$FixedViewInfo;->view:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method a(Ljava/util/ArrayList;Ljava/lang/Object;)Landroid/view/View;
    .locals 3

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView$FixedViewInfo;

    iget-object v0, v0, Landroid/widget/ListView$FixedViewInfo;->view:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method a(I)V
    .locals 2

    iget v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->B:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->B:I

    return-void
.end method

.method a(II)V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getChildCount()I

    move-result v0

    :goto_0
    if-ge p1, v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/twitter/refresh/widget/RefreshableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/view/View;->offsetTopAndBottom(I)V

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(ILandroid/view/View;Ljava/lang/Object;Z)V
    .locals 2

    invoke-direct {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/widget/ListView$FixedViewInfo;

    invoke-direct {v0, p0}, Landroid/widget/ListView$FixedViewInfo;-><init>(Landroid/widget/ListView;)V

    iput-object p2, v0, Landroid/widget/ListView$FixedViewInfo;->view:Landroid/view/View;

    iput-object p3, v0, Landroid/widget/ListView$FixedViewInfo;->data:Ljava/lang/Object;

    iput-boolean p4, v0, Landroid/widget/ListView$FixedViewInfo;->isSelectable:Z

    iget-object v1, p0, Lcom/twitter/refresh/widget/RefreshableListView;->E:Ljava/util/ArrayList;

    invoke-virtual {v1, p1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p2, p3, p4}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/refresh/widget/b;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/twitter/refresh/widget/RefreshableListView;->setRefreshListener(Lcom/twitter/refresh/widget/d;)V

    invoke-virtual {p0, p1}, Lcom/twitter/refresh/widget/RefreshableListView;->setRefreshDataProvider(Lcom/twitter/refresh/widget/c;)V

    invoke-virtual {p0, p1}, Lcom/twitter/refresh/widget/RefreshableListView;->setRefreshVisibilityListener(Lcom/twitter/refresh/widget/e;)V

    return-void
.end method

.method public a(Z)V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->l:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->l:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->D:Z

    return v0
.end method

.method public addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V
    .locals 2

    invoke-direct {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/widget/ListView$FixedViewInfo;

    invoke-direct {v0, p0}, Landroid/widget/ListView$FixedViewInfo;-><init>(Landroid/widget/ListView;)V

    iput-object p1, v0, Landroid/widget/ListView$FixedViewInfo;->view:Landroid/view/View;

    iput-object p2, v0, Landroid/widget/ListView$FixedViewInfo;->data:Ljava/lang/Object;

    iput-boolean p3, v0, Landroid/widget/ListView$FixedViewInfo;->isSelectable:Z

    iget-object v1, p0, Lcom/twitter/refresh/widget/RefreshableListView;->F:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    goto :goto_0
.end method

.method public addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->E:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/twitter/refresh/widget/RefreshableListView;->a(ILandroid/view/View;Ljava/lang/Object;Z)V

    return-void
.end method

.method public b()V
    .locals 2

    invoke-direct {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->g()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->h:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->h:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->g:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->i:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/refresh/widget/RefreshableListView;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v0, 0x20

    invoke-virtual {p0, v0}, Lcom/twitter/refresh/widget/RefreshableListView;->setMode(I)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/refresh/widget/RefreshableListView;->b(I)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->p:Lcom/twitter/refresh/widget/f;

    invoke-virtual {v0}, Lcom/twitter/refresh/widget/f;->onChanged()V

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/refresh/widget/RefreshableListView;->setVisible(Z)V

    goto :goto_0
.end method

.method b(II)V
    .locals 7

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/twitter/refresh/widget/RefreshableListView;->b:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-virtual {p0, v1}, Lcom/twitter/refresh/widget/RefreshableListView;->b(I)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p0, v2, p1, v3}, Lcom/twitter/refresh/widget/RefreshableListView;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    :goto_0
    const/4 v5, 0x2

    invoke-virtual {p0, v5}, Lcom/twitter/refresh/widget/RefreshableListView;->setMode(I)V

    if-eqz v4, :cond_0

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->isLayoutRequested()Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_0
    :goto_1
    if-eqz v1, :cond_5

    iget v4, p0, Lcom/twitter/refresh/widget/RefreshableListView;->w:I

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getListPaddingLeft()I

    move-result v5

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getListPaddingRight()I

    move-result v6

    add-int/2addr v5, v6

    iget v6, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {v4, v5, v6}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v4

    iget v3, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-lez v3, :cond_4

    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {v3, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    :goto_2
    invoke-virtual {v2, v4, v0}, Landroid/widget/RelativeLayout;->measure(II)V

    :goto_3
    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getMeasuredHeight()I

    move-result v3

    sub-int v4, p2, v3

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getListPaddingLeft()I

    move-result v5

    if-eqz v1, :cond_6

    add-int/2addr v0, v5

    add-int v1, v4, v3

    invoke-virtual {v2, v5, v4, v0, v1}, Landroid/widget/RelativeLayout;->layout(IIII)V

    :goto_4
    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->p:Lcom/twitter/refresh/widget/f;

    invoke-virtual {v0}, Lcom/twitter/refresh/widget/f;->onChanged()V

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->r:Lcom/twitter/refresh/widget/e;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->r:Lcom/twitter/refresh/widget/e;

    invoke-interface {v0}, Lcom/twitter/refresh/widget/e;->b_()V

    :cond_1
    return-void

    :cond_2
    invoke-virtual {p0, v2, p1, v3, v1}, Lcom/twitter/refresh/widget/RefreshableListView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    invoke-virtual {p0, v1}, Lcom/twitter/refresh/widget/RefreshableListView;->setMode(I)V

    goto :goto_0

    :cond_3
    move v1, v0

    goto :goto_1

    :cond_4
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_2

    :cond_5
    invoke-virtual {p0, v2}, Lcom/twitter/refresh/widget/RefreshableListView;->cleanupLayoutState(Landroid/view/View;)V

    goto :goto_3

    :cond_6
    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getLeft()I

    move-result v0

    sub-int v0, v5, v0

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->offsetLeftAndRight(I)V

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getTop()I

    move-result v0

    sub-int v0, v4, v0

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->offsetTopAndBottom(I)V

    goto :goto_4
.end method

.method b(I)Z
    .locals 1

    iget v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->B:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 13

    const-wide/high16 v2, -0x8000000000000000L

    const/16 v12, 0x20

    const/4 v7, -0x1

    const/4 v5, 0x0

    iget-object v8, p0, Lcom/twitter/refresh/widget/RefreshableListView;->q:Lcom/twitter/refresh/widget/d;

    if-eqz v8, :cond_0

    invoke-virtual {p0, v12}, Lcom/twitter/refresh/widget/RefreshableListView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->g:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->i:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/refresh/widget/RefreshableListView;->z:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getFirstVisiblePosition()I

    move-result v9

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/refresh/widget/RefreshableListView;->b(I)Z

    move-result v10

    iget-object v11, p0, Lcom/twitter/refresh/widget/RefreshableListView;->H:Lcom/twitter/refresh/widget/c;

    if-eqz v11, :cond_1

    invoke-interface {v11}, Lcom/twitter/refresh/widget/c;->A()Lcom/twitter/refresh/widget/a;

    move-result-object v0

    iget v6, v0, Lcom/twitter/refresh/widget/a;->c:I

    iget v4, v0, Lcom/twitter/refresh/widget/a;->a:I

    iget-wide v0, v0, Lcom/twitter/refresh/widget/a;->b:J

    :goto_0
    invoke-interface {v8}, Lcom/twitter/refresh/widget/d;->f()V

    cmp-long v2, v0, v2

    if-eqz v2, :cond_2

    invoke-interface {v11, v0, v1}, Lcom/twitter/refresh/widget/c;->a(J)I

    move-result v0

    :goto_1
    invoke-virtual {p0, v12}, Lcom/twitter/refresh/widget/RefreshableListView;->a(I)V

    if-ne v0, v4, :cond_6

    invoke-interface {v8}, Lcom/twitter/refresh/widget/d;->aj()V

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getRefreshHeaderPosition()I

    move-result v0

    if-nez v9, :cond_4

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->isInTouchMode()Z

    move-result v1

    if-eqz v1, :cond_4

    sub-int v1, v0, v9

    if-lez v1, :cond_3

    add-int/lit8 v0, v1, -0x1

    invoke-virtual {p0, v0}, Lcom/twitter/refresh/widget/RefreshableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    :goto_2
    invoke-virtual {p0, v1}, Lcom/twitter/refresh/widget/RefreshableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    sub-int v0, v1, v0

    invoke-direct {p0, v0}, Lcom/twitter/refresh/widget/RefreshableListView;->c(I)V

    :cond_0
    :goto_3
    return-void

    :cond_1
    move-wide v0, v2

    move v4, v5

    move v6, v5

    goto :goto_0

    :cond_2
    move v0, v7

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getListPaddingTop()I

    move-result v0

    goto :goto_2

    :cond_4
    if-eqz v10, :cond_5

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->d()V

    :goto_4
    invoke-virtual {p0, v4, v6}, Lcom/twitter/refresh/widget/RefreshableListView;->setSelectionFromTop(II)V

    invoke-virtual {p0, v5}, Lcom/twitter/refresh/widget/RefreshableListView;->setVisible(Z)V

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->p:Lcom/twitter/refresh/widget/f;

    invoke-virtual {v0}, Lcom/twitter/refresh/widget/f;->onChanged()V

    goto :goto_4

    :cond_6
    invoke-interface {v8}, Lcom/twitter/refresh/widget/d;->t_()V

    if-eqz v10, :cond_8

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->d()V

    :goto_5
    if-eq v0, v7, :cond_7

    invoke-virtual {p0, v0, v6}, Lcom/twitter/refresh/widget/RefreshableListView;->setSelectionFromTop(II)V

    :cond_7
    invoke-virtual {p0, v5}, Lcom/twitter/refresh/widget/RefreshableListView;->setVisible(Z)V

    goto :goto_3

    :cond_8
    iget-object v1, p0, Lcom/twitter/refresh/widget/RefreshableListView;->p:Lcom/twitter/refresh/widget/f;

    invoke-virtual {v1}, Lcom/twitter/refresh/widget/f;->onChanged()V

    goto :goto_5
.end method

.method protected computeVerticalScrollOffset()I
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->isSmoothScrollbarEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/twitter/refresh/widget/RefreshableListView;->b(I)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getFirstVisiblePosition()I

    move-result v1

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getChildCount()I

    move-result v2

    iget v3, p0, Lcom/twitter/refresh/widget/RefreshableListView;->c:I

    add-int/lit8 v3, v3, -0x1

    if-lez v3, :cond_0

    const/4 v4, 0x1

    if-lt v1, v4, :cond_0

    if-lez v2, :cond_0

    invoke-virtual {p0, v0}, Lcom/twitter/refresh/widget/RefreshableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    if-lez v2, :cond_0

    add-int/lit8 v1, v1, -0x1

    mul-int/lit8 v1, v1, 0x64

    mul-int/lit8 v4, v4, 0x64

    div-int v2, v4, v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getScrollY()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v2, v4

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const/high16 v3, 0x42c80000    # 100.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    add-int/2addr v1, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-super {p0}, Landroid/widget/ListView;->computeVerticalScrollOffset()I

    move-result v0

    goto :goto_0
.end method

.method protected computeVerticalScrollRange()I
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->isSmoothScrollbarEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/refresh/widget/RefreshableListView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->c:I

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getScrollY()I

    move-result v2

    mul-int/lit8 v0, v1, 0x64

    const/4 v3, 0x0

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    if-eqz v2, :cond_0

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    int-to-float v1, v1

    mul-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-super {p0}, Landroid/widget/ListView;->computeVerticalScrollRange()I

    move-result v0

    goto :goto_0
.end method

.method d()V
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/refresh/widget/RefreshableListView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->b:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne p0, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->b:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v0}, Lcom/twitter/refresh/widget/RefreshableListView;->detachViewFromParent(Landroid/view/View;)V

    :cond_0
    const/16 v0, 0x5e

    invoke-virtual {p0, v0}, Lcom/twitter/refresh/widget/RefreshableListView;->a(I)V

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->p:Lcom/twitter/refresh/widget/f;

    invoke-virtual {v0}, Lcom/twitter/refresh/widget/f;->onChanged()V

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->r:Lcom/twitter/refresh/widget/e;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->r:Lcom/twitter/refresh/widget/e;

    invoke-interface {v0}, Lcom/twitter/refresh/widget/e;->x_()V

    :cond_1
    return-void
.end method

.method protected detachViewsFromParent(II)V
    .locals 1

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/twitter/refresh/widget/RefreshableListView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/ListView;->detachViewsFromParent(II)V

    goto :goto_0
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 5

    invoke-direct {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->b:Landroid/widget/RelativeLayout;

    if-ne p2, v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getRefreshHeaderPosition()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getFirstVisiblePosition()I

    move-result v1

    sub-int/2addr v0, v1

    if-lez v0, :cond_0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/twitter/refresh/widget/RefreshableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getScrollX()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v2

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getWidth()I

    move-result v3

    add-int/2addr v3, v1

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v4

    invoke-virtual {p1, v1, v0, v3, v4}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    iget v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->s:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ListView;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :goto_1
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getScrollY()I

    move-result v0

    goto :goto_0

    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ListView;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    goto :goto_1
.end method

.method public e()Z
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x8
    .end annotation

    const/4 v0, 0x1

    iget v1, p0, Lcom/twitter/refresh/widget/RefreshableListView;->x:I

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v3, 0x7

    if-le v2, v3, :cond_2

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getFirstVisiblePosition()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    const/16 v3, 0xf

    if-le v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getListPaddingTop()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/twitter/refresh/widget/RefreshableListView;->setSelectionFromTop(II)V

    :cond_0
    invoke-virtual {p0, v1}, Lcom/twitter/refresh/widget/RefreshableListView;->smoothScrollToPosition(I)V

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v1}, Lcom/twitter/refresh/widget/RefreshableListView;->setSelection(I)V

    goto :goto_0
.end method

.method f()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->g()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->o:Lcom/twitter/refresh/widget/g;

    invoke-virtual {v0}, Lcom/twitter/refresh/widget/g;->a()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->c:I

    goto :goto_0
.end method

.method protected findViewTraversal(I)Landroid/view/View;
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/ListView;->findViewTraversal(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->E:Ljava/util/ArrayList;

    invoke-virtual {p0, v0, p1}, Lcom/twitter/refresh/widget/RefreshableListView;->a(Ljava/util/ArrayList;I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->F:Ljava/util/ArrayList;

    invoke-virtual {p0, v0, p1}, Lcom/twitter/refresh/widget/RefreshableListView;->a(Ljava/util/ArrayList;I)Landroid/view/View;

    move-result-object v0

    goto :goto_1
.end method

.method protected findViewWithTagTraversal(Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/ListView;->findViewWithTagTraversal(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->E:Ljava/util/ArrayList;

    invoke-virtual {p0, v0, p1}, Lcom/twitter/refresh/widget/RefreshableListView;->a(Ljava/util/ArrayList;Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->F:Ljava/util/ArrayList;

    invoke-virtual {p0, v0, p1}, Lcom/twitter/refresh/widget/RefreshableListView;->a(Ljava/util/ArrayList;Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    goto :goto_1
.end method

.method public getFooterViewsCount()I
    .locals 1

    invoke-direct {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->F:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v0

    goto :goto_0
.end method

.method public getHeaderViewsCount()I
    .locals 1

    invoke-direct {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->E:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    goto :goto_0
.end method

.method public getRefreshHeaderPosition()I
    .locals 4

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->g()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/twitter/refresh/widget/RefreshableListView;->E:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, p0, Lcom/twitter/refresh/widget/RefreshableListView;->G:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    iget v2, p0, Lcom/twitter/refresh/widget/RefreshableListView;->G:I

    add-int/lit8 v3, v1, 0x1

    if-ge v2, v3, :cond_2

    iget v2, p0, Lcom/twitter/refresh/widget/RefreshableListView;->G:I

    iget v3, p0, Lcom/twitter/refresh/widget/RefreshableListView;->c:I

    if-ge v2, v3, :cond_2

    iget v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->G:I

    goto :goto_0

    :cond_2
    iget-boolean v2, p0, Lcom/twitter/refresh/widget/RefreshableListView;->k:Z

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/twitter/refresh/widget/RefreshableListView;->c:I

    if-ge v1, v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method protected layoutChildren()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->g()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-super {p0}, Landroid/widget/ListView;->layoutChildren()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, Lcom/twitter/refresh/widget/RefreshableListView;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->c:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->E:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/twitter/refresh/widget/RefreshableListView;->a(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->F:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/twitter/refresh/widget/RefreshableListView;->a(Ljava/util/ArrayList;)V

    :cond_2
    invoke-super {p0}, Landroid/widget/ListView;->layoutChildren()V

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->o:Lcom/twitter/refresh/widget/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->o:Lcom/twitter/refresh/widget/g;

    invoke-virtual {v0}, Lcom/twitter/refresh/widget/g;->b()V

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->f()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->C:Z

    invoke-super {p0}, Landroid/widget/ListView;->onAttachedToWindow()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->o:Lcom/twitter/refresh/widget/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->o:Lcom/twitter/refresh/widget/g;

    invoke-virtual {v0}, Lcom/twitter/refresh/widget/g;->c()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->d:Lcom/twitter/refresh/widget/i;

    invoke-virtual {p0, v0}, Lcom/twitter/refresh/widget/RefreshableListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->C:Z

    invoke-super {p0}, Landroid/widget/ListView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->C:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getSelectedItemPosition()I

    move-result v0

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->o:Lcom/twitter/refresh/widget/g;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->f()V

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ListView;->onFocusChanged(ZILandroid/graphics/Rect;)V

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->g()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-super {p0, p1}, Landroid/widget/ListView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    iget v3, p0, Lcom/twitter/refresh/widget/RefreshableListView;->t:I

    sub-int v4, v2, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_1
    :goto_1
    :pswitch_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0

    :pswitch_1
    iput v2, p0, Lcom/twitter/refresh/widget/RefreshableListView;->t:I

    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->u:I

    goto :goto_1

    :pswitch_2
    iput v2, p0, Lcom/twitter/refresh/widget/RefreshableListView;->u:I

    const/16 v2, 0x20

    invoke-virtual {p0, v2}, Lcom/twitter/refresh/widget/RefreshableListView;->b(I)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getFirstVisiblePosition()I

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getFirstVisiblePosition()I

    move-result v2

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getRefreshHeaderPosition()I

    move-result v3

    sub-int v5, v3, v2

    if-nez v2, :cond_1

    if-ltz v5, :cond_1

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getChildCount()I

    move-result v2

    if-ge v5, v2, :cond_1

    add-int/lit8 v2, v5, 0x1

    invoke-virtual {p0, v2}, Lcom/twitter/refresh/widget/RefreshableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    move v3, v2

    :goto_2
    if-lez v5, :cond_4

    invoke-virtual {p0, v0}, Lcom/twitter/refresh/widget/RefreshableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getListPaddingTop()I

    move-result v6

    if-lt v2, v6, :cond_2

    move v0, v1

    :cond_2
    add-int/lit8 v2, v5, -0x1

    invoke-virtual {p0, v2}, Lcom/twitter/refresh/widget/RefreshableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v2

    :goto_3
    if-eqz v0, :cond_1

    if-lt v3, v2, :cond_1

    iget v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->v:I

    if-le v4, v0, :cond_1

    goto :goto_0

    :cond_3
    move v3, v0

    goto :goto_2

    :cond_4
    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getListPaddingTop()I

    move-result v0

    move v2, v0

    move v0, v1

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onMeasure(II)V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->f()V

    iput p1, p0, Lcom/twitter/refresh/widget/RefreshableListView;->w:I

    invoke-super {p0, p1, p2}, Landroid/widget/ListView;->onMeasure(II)V

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    check-cast p1, Lcom/twitter/refresh/widget/RefreshableListView$SavedState;

    invoke-virtual {p1}, Lcom/twitter/refresh/widget/RefreshableListView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/ListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget v0, p1, Lcom/twitter/refresh/widget/RefreshableListView$SavedState;->c:I

    iput v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->G:I

    iget-boolean v0, p1, Lcom/twitter/refresh/widget/RefreshableListView$SavedState;->a:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p1, Lcom/twitter/refresh/widget/RefreshableListView$SavedState;->b:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x20

    invoke-virtual {p0, v0}, Lcom/twitter/refresh/widget/RefreshableListView;->setMode(I)V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    invoke-super {p0}, Landroid/widget/ListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    new-instance v1, Lcom/twitter/refresh/widget/RefreshableListView$SavedState;

    invoke-direct {v1, v0}, Lcom/twitter/refresh/widget/RefreshableListView$SavedState;-><init>(Landroid/os/Parcelable;)V

    invoke-direct {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->g()Z

    move-result v0

    iput-boolean v0, v1, Lcom/twitter/refresh/widget/RefreshableListView$SavedState;->a:Z

    const/16 v0, 0x20

    invoke-virtual {p0, v0}, Lcom/twitter/refresh/widget/RefreshableListView;->b(I)Z

    move-result v0

    iput-boolean v0, v1, Lcom/twitter/refresh/widget/RefreshableListView$SavedState;->b:Z

    iget v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->G:I

    iput v0, v1, Lcom/twitter/refresh/widget/RefreshableListView$SavedState;->c:I

    return-object v1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11

    const/high16 v2, -0x80000000

    const/16 v10, 0x40

    const/16 v9, 0x10

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->g()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    :cond_0
    :goto_0
    return v4

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/twitter/refresh/widget/RefreshableListView;->b(I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v5, v1

    iget v1, p0, Lcom/twitter/refresh/widget/RefreshableListView;->t:I

    sub-int v1, v5, v1

    packed-switch v0, :pswitch_data_0

    :cond_2
    :goto_1
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    goto :goto_0

    :pswitch_0
    iput v5, p0, Lcom/twitter/refresh/widget/RefreshableListView;->t:I

    iput v2, p0, Lcom/twitter/refresh/widget/RefreshableListView;->u:I

    invoke-virtual {p0, v10}, Lcom/twitter/refresh/widget/RefreshableListView;->setMode(I)V

    goto :goto_1

    :pswitch_1
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, Lcom/twitter/refresh/widget/RefreshableListView;->a(I)V

    const/16 v0, 0x20

    invoke-virtual {p0, v0}, Lcom/twitter/refresh/widget/RefreshableListView;->b(I)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/refresh/widget/RefreshableListView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getFirstVisiblePosition()I

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getRefreshHeaderPosition()I

    move-result v1

    if-lez v1, :cond_3

    add-int/lit8 v0, v1, -0x1

    invoke-virtual {p0, v0}, Lcom/twitter/refresh/widget/RefreshableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    :goto_2
    invoke-virtual {p0, v1}, Lcom/twitter/refresh/widget/RefreshableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    if-le v2, v0, :cond_4

    iget-object v1, p0, Lcom/twitter/refresh/widget/RefreshableListView;->q:Lcom/twitter/refresh/widget/d;

    invoke-interface {v1, v3}, Lcom/twitter/refresh/widget/d;->c(Z)V

    sub-int v0, v2, v0

    invoke-direct {p0, v0}, Lcom/twitter/refresh/widget/RefreshableListView;->c(I)V

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getListPaddingTop()I

    move-result v0

    goto :goto_2

    :cond_4
    if-le v1, v0, :cond_5

    iget-object v2, p0, Lcom/twitter/refresh/widget/RefreshableListView;->q:Lcom/twitter/refresh/widget/d;

    invoke-interface {v2, v4}, Lcom/twitter/refresh/widget/d;->c(Z)V

    sub-int v0, v1, v0

    invoke-direct {p0, v0}, Lcom/twitter/refresh/widget/RefreshableListView;->c(I)V

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->d()V

    goto :goto_1

    :pswitch_2
    const/16 v0, 0x20

    invoke-virtual {p0, v0}, Lcom/twitter/refresh/widget/RefreshableListView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_6

    iput v5, p0, Lcom/twitter/refresh/widget/RefreshableListView;->u:I

    goto :goto_1

    :cond_6
    iget v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->u:I

    if-eq v0, v2, :cond_9

    iget v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->u:I

    sub-int v0, v5, v0

    :goto_3
    iget v2, p0, Lcom/twitter/refresh/widget/RefreshableListView;->u:I

    if-lt v5, v2, :cond_a

    move v2, v3

    :goto_4
    iput v5, p0, Lcom/twitter/refresh/widget/RefreshableListView;->u:I

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getFirstVisiblePosition()I

    move-result v5

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getRefreshHeaderPosition()I

    move-result v6

    sub-int v8, v6, v5

    if-nez v5, :cond_10

    if-ltz v8, :cond_10

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getChildCount()I

    move-result v5

    if-ge v8, v5, :cond_10

    add-int/lit8 v5, v8, 0x1

    invoke-virtual {p0, v5}, Lcom/twitter/refresh/widget/RefreshableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    if-eqz v5, :cond_b

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v5

    move v7, v5

    :goto_5
    if-lez v8, :cond_d

    invoke-virtual {p0, v4}, Lcom/twitter/refresh/widget/RefreshableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    if-eqz v5, :cond_c

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v5

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getListPaddingTop()I

    move-result v6

    if-lt v5, v6, :cond_c

    move v5, v3

    :goto_6
    add-int/lit8 v6, v8, -0x1

    invoke-virtual {p0, v6}, Lcom/twitter/refresh/widget/RefreshableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    move-result v6

    :goto_7
    if-eqz v5, :cond_10

    if-lt v7, v6, :cond_10

    iget v5, p0, Lcom/twitter/refresh/widget/RefreshableListView;->v:I

    if-le v1, v5, :cond_10

    invoke-virtual {p0, v3}, Lcom/twitter/refresh/widget/RefreshableListView;->requestDisallowInterceptTouchEvent(Z)V

    invoke-virtual {p0, v10}, Lcom/twitter/refresh/widget/RefreshableListView;->b(I)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {p0, v10}, Lcom/twitter/refresh/widget/RefreshableListView;->a(I)V

    :cond_7
    invoke-virtual {p0, v4}, Lcom/twitter/refresh/widget/RefreshableListView;->setChildrenDrawingCacheEnabled(Z)V

    invoke-virtual {p0, v4}, Lcom/twitter/refresh/widget/RefreshableListView;->setChildrenDrawnWithCacheEnabled(Z)V

    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/twitter/refresh/widget/RefreshableListView;->b(I)Z

    move-result v1

    if-eqz v1, :cond_f

    int-to-float v0, v0

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    invoke-virtual {p0, v8, v0}, Lcom/twitter/refresh/widget/RefreshableListView;->a(II)V

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->invalidate()V

    invoke-virtual {p0, v8}, Lcom/twitter/refresh/widget/RefreshableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    if-lt v0, v6, :cond_e

    if-eqz v2, :cond_8

    invoke-virtual {p0, v9}, Lcom/twitter/refresh/widget/RefreshableListView;->b(I)Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {p0, v9}, Lcom/twitter/refresh/widget/RefreshableListView;->setMode(I)V

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->i:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/refresh/widget/RefreshableListView;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->h:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/twitter/refresh/widget/RefreshableListView;->e:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->q:Lcom/twitter/refresh/widget/d;

    invoke-interface {v0}, Lcom/twitter/refresh/widget/d;->ah()V

    :cond_8
    :goto_8
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/twitter/refresh/widget/RefreshableListView;->setMode(I)V

    move v4, v3

    goto/16 :goto_0

    :cond_9
    move v0, v1

    goto/16 :goto_3

    :cond_a
    move v2, v4

    goto/16 :goto_4

    :cond_b
    move v7, v4

    goto/16 :goto_5

    :cond_c
    move v5, v4

    goto :goto_6

    :cond_d
    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getListPaddingTop()I

    move-result v5

    move v6, v5

    move v5, v3

    goto :goto_7

    :cond_e
    if-nez v2, :cond_8

    invoke-virtual {p0, v9}, Lcom/twitter/refresh/widget/RefreshableListView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p0, v9}, Lcom/twitter/refresh/widget/RefreshableListView;->a(I)V

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->i:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/refresh/widget/RefreshableListView;->z:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->h:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/twitter/refresh/widget/RefreshableListView;->f:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->q:Lcom/twitter/refresh/widget/d;

    invoke-interface {v0}, Lcom/twitter/refresh/widget/d;->ai()V

    goto :goto_8

    :cond_f
    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getDividerHeight()I

    move-result v0

    sub-int v0, v7, v0

    invoke-virtual {p0, v8, v0}, Lcom/twitter/refresh/widget/RefreshableListView;->b(II)V

    goto :goto_8

    :cond_10
    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->d()V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, Lcom/twitter/refresh/widget/RefreshableListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->o:Lcom/twitter/refresh/widget/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->o:Lcom/twitter/refresh/widget/g;

    invoke-virtual {v0}, Lcom/twitter/refresh/widget/g;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    new-instance v0, Lcom/twitter/refresh/widget/f;

    invoke-direct {v0, p0}, Lcom/twitter/refresh/widget/f;-><init>(Lcom/twitter/refresh/widget/RefreshableListView;)V

    iput-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->p:Lcom/twitter/refresh/widget/f;

    invoke-direct {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->l:Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, Lcom/twitter/refresh/widget/RefreshableListView;->n:I

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/refresh/widget/RefreshableListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    sget v1, Llb;->footer_content:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/refresh/widget/RefreshableListView;->l:Landroid/view/View;

    sget v1, Llb;->footer_dot:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->m:Landroid/widget/TextView;

    :cond_1
    new-instance v0, Lcom/twitter/refresh/widget/g;

    iget-object v2, p0, Lcom/twitter/refresh/widget/RefreshableListView;->E:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/twitter/refresh/widget/RefreshableListView;->F:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/twitter/refresh/widget/RefreshableListView;->p:Lcom/twitter/refresh/widget/f;

    move-object v1, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/refresh/widget/g;-><init>(Lcom/twitter/refresh/widget/RefreshableListView;Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/widget/ListAdapter;Lcom/twitter/refresh/widget/f;)V

    iput-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->o:Lcom/twitter/refresh/widget/g;

    :cond_2
    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->o:Lcom/twitter/refresh/widget/g;

    invoke-virtual {v0}, Lcom/twitter/refresh/widget/g;->a()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->c:I

    :goto_0
    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->o:Lcom/twitter/refresh/widget/g;

    invoke-super {p0, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    :goto_1
    return-void

    :cond_3
    iput-object v2, p0, Lcom/twitter/refresh/widget/RefreshableListView;->o:Lcom/twitter/refresh/widget/g;

    iput v3, p0, Lcom/twitter/refresh/widget/RefreshableListView;->c:I

    goto :goto_0

    :cond_4
    invoke-super {p0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_1
.end method

.method setMode(I)V
    .locals 1

    iget v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->B:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->B:I

    return-void
.end method

.method public setPullAfterHeadersEnabled(Z)V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->k:Z

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean p1, p0, Lcom/twitter/refresh/widget/RefreshableListView;->k:Z

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->p:Lcom/twitter/refresh/widget/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->p:Lcom/twitter/refresh/widget/f;

    invoke-virtual {v0}, Lcom/twitter/refresh/widget/f;->onChanged()V

    goto :goto_0
.end method

.method public final setRefreshDataProvider(Lcom/twitter/refresh/widget/c;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/refresh/widget/RefreshableListView;->H:Lcom/twitter/refresh/widget/c;

    return-void
.end method

.method public setRefreshHeaderPosition(I)V
    .locals 3

    invoke-direct {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->G:I

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getHeaderViewsCount()I

    move-result v0

    if-ge p1, v0, :cond_2

    const/4 v0, -0x1

    if-ge p1, v0, :cond_3

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iput p1, p0, Lcom/twitter/refresh/widget/RefreshableListView;->G:I

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->p:Lcom/twitter/refresh/widget/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->p:Lcom/twitter/refresh/widget/f;

    invoke-virtual {v0}, Lcom/twitter/refresh/widget/f;->onChanged()V

    goto :goto_0
.end method

.method public final setRefreshListener(Lcom/twitter/refresh/widget/d;)V
    .locals 2

    invoke-direct {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->g()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/refresh/widget/RefreshableListView;->getHeaderViewsCount()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "setRefreshListener must be called before addHeaderView."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/twitter/refresh/widget/RefreshableListView;->q:Lcom/twitter/refresh/widget/d;

    return-void
.end method

.method public final setRefreshVisibilityListener(Lcom/twitter/refresh/widget/e;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/refresh/widget/RefreshableListView;->r:Lcom/twitter/refresh/widget/e;

    return-void
.end method

.method public setTopPosition(I)V
    .locals 0

    iput p1, p0, Lcom/twitter/refresh/widget/RefreshableListView;->x:I

    return-void
.end method

.method setVisible(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->D:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/twitter/refresh/widget/RefreshableListView;->D:Z

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->r:Lcom/twitter/refresh/widget/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/refresh/widget/RefreshableListView;->r:Lcom/twitter/refresh/widget/e;

    invoke-interface {v0, p1}, Lcom/twitter/refresh/widget/e;->b(Z)V

    :cond_0
    return-void
.end method
