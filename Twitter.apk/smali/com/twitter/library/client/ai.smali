.class Lcom/twitter/library/client/ai;
.super Lcom/twitter/library/service/a;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/library/client/aa;

.field private final b:I


# direct methods
.method constructor <init>(Lcom/twitter/library/client/aa;I)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/client/ai;->a:Lcom/twitter/library/client/aa;

    invoke-direct {p0}, Lcom/twitter/library/service/a;-><init>()V

    iput p2, p0, Lcom/twitter/library/client/ai;->b:I

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/internal/android/service/a;)V
    .locals 0

    check-cast p1, Lcom/twitter/library/service/b;

    invoke-virtual {p0, p1}, Lcom/twitter/library/client/ai;->a(Lcom/twitter/library/service/b;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/b;)V
    .locals 7

    const/4 v6, 0x0

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    iget-object v2, v1, Lcom/twitter/library/service/p;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/library/client/ai;->a:Lcom/twitter/library/client/aa;

    invoke-static {v1}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/aa;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/client/Session;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/twitter/library/client/ai;->a:Lcom/twitter/library/client/aa;

    invoke-static {v3}, Lcom/twitter/library/client/aa;->c(Lcom/twitter/library/client/aa;)Ljava/util/ArrayList;

    move-result-object v3

    iget v4, p0, Lcom/twitter/library/client/ai;->b:I

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/library/client/ai;->a:Lcom/twitter/library/client/aa;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->d(Lcom/twitter/library/client/aa;)V

    iget-object v0, p0, Lcom/twitter/library/client/ai;->a:Lcom/twitter/library/client/aa;

    invoke-static {v0, v2}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/aa;Ljava/lang/String;)Z

    move-result v4

    sget-object v0, Lcom/twitter/library/client/Session$LoginStatus;->a:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_1
    if-ltz v2, :cond_5

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/z;

    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/client/z;->a(Lcom/twitter/library/client/Session;Z)V

    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_1

    :pswitch_1
    invoke-virtual {v0}, Lcom/twitter/library/service/e;->c()I

    move-result v0

    const/16 v4, 0x191

    if-ne v0, v4, :cond_3

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_0

    invoke-static {}, Lcom/twitter/library/client/aa;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "SessionManager"

    const-string/jumbo v4, "Invalid credentials. The auth token has expired."

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v0, p0, Lcom/twitter/library/client/ai;->a:Lcom/twitter/library/client/aa;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->d(Lcom/twitter/library/client/aa;)V

    iget-object v0, p0, Lcom/twitter/library/client/ai;->a:Lcom/twitter/library/client/aa;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->e(Lcom/twitter/library/client/aa;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/twitter/library/util/a;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v5, p0, Lcom/twitter/library/client/ai;->a:Lcom/twitter/library/client/aa;

    invoke-static {v5, v2}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/aa;Ljava/lang/String;)Z

    move-result v5

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-virtual {v0, v4, v6}, Landroid/accounts/AccountManager;->setPassword(Landroid/accounts/Account;Ljava/lang/String;)V

    sget-object v2, Lcom/twitter/library/util/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v6}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/twitter/library/client/Session$LoginStatus;->a:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_3
    if-ltz v2, :cond_4

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/z;

    invoke-virtual {v0, v1, v5}, Lcom/twitter/library/client/z;->c(Lcom/twitter/library/client/Session;Z)V

    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_3

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->a()V

    goto/16 :goto_0

    :cond_5
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->a()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
