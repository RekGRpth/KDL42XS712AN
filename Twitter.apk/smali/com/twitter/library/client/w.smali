.class public Lcom/twitter/library/client/w;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Lcom/twitter/library/client/w;


# instance fields
.field private final b:Lcom/twitter/library/service/k;

.field private final c:Lcom/twitter/library/client/s;

.field private final d:Lcom/twitter/internal/android/service/j;

.field private final e:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/twitter/internal/android/service/j;

    invoke-direct {v0}, Lcom/twitter/internal/android/service/j;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/client/w;->d:Lcom/twitter/internal/android/service/j;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/client/w;->e:Landroid/content/Context;

    new-instance v0, Lcom/twitter/library/client/s;

    iget-object v1, p0, Lcom/twitter/library/client/w;->e:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/twitter/library/client/s;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/library/client/w;->c:Lcom/twitter/library/client/s;

    new-instance v0, Lcom/twitter/library/service/k;

    invoke-direct {v0}, Lcom/twitter/library/service/k;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/client/w;->b:Lcom/twitter/library/service/k;

    iget-object v0, p0, Lcom/twitter/library/client/w;->b:Lcom/twitter/library/service/k;

    new-instance v1, Lcom/twitter/library/client/x;

    invoke-direct {v1, p0}, Lcom/twitter/library/client/x;-><init>(Lcom/twitter/library/client/w;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/k;->a(Lcom/twitter/library/service/a;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/client/w;)Lcom/twitter/internal/android/service/j;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/w;->d:Lcom/twitter/internal/android/service/j;

    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/twitter/library/client/w;
    .locals 2

    const-class v1, Lcom/twitter/library/client/w;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/client/w;->a:Lcom/twitter/library/client/w;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/library/client/w;

    invoke-direct {v0, p0}, Lcom/twitter/library/client/w;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/library/client/w;->a:Lcom/twitter/library/client/w;

    :cond_0
    sget-object v0, Lcom/twitter/library/client/w;->a:Lcom/twitter/library/client/w;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private c(Lcom/twitter/library/service/a;)Lcom/twitter/library/service/k;
    .locals 2

    new-instance v0, Lcom/twitter/library/service/k;

    invoke-direct {v0}, Lcom/twitter/library/service/k;-><init>()V

    iget-object v1, p0, Lcom/twitter/library/client/w;->b:Lcom/twitter/library/service/k;

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/k;->a(Lcom/twitter/library/service/a;)V

    if-eqz p1, :cond_0

    invoke-virtual {v0, p1}, Lcom/twitter/library/service/k;->a(Lcom/twitter/library/service/a;)V

    :cond_0
    iget-object v1, p0, Lcom/twitter/library/client/w;->c:Lcom/twitter/library/client/s;

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/k;->a(Lcom/twitter/library/service/a;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/twitter/library/client/w;->d:Lcom/twitter/internal/android/service/j;

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/j;->a()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/client/w;->e:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/twitter/library/client/w;->e:Landroid/content/Context;

    const-class v4, Lcom/twitter/internal/android/service/AsyncService;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    :cond_0
    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/service/j;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/service/b;Lcom/twitter/library/service/a;)Ljava/lang/String;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Request cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-direct {p0, p2}, Lcom/twitter/library/client/w;->c(Lcom/twitter/library/service/a;)Lcom/twitter/library/service/k;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/library/service/b;->a(Lcom/twitter/internal/android/service/m;)Lcom/twitter/internal/android/service/a;

    invoke-virtual {p0, p1}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/w;->d:Lcom/twitter/internal/android/service/j;

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/j;->b()V

    return-void
.end method

.method public a(Lcom/twitter/library/service/a;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/w;->b:Lcom/twitter/library/service/k;

    invoke-virtual {v0, p1}, Lcom/twitter/library/service/k;->a(Lcom/twitter/library/service/a;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/b;Lcom/twitter/internal/android/service/k;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/w;->b:Lcom/twitter/library/service/k;

    invoke-virtual {p1, v0}, Lcom/twitter/library/service/b;->a(Lcom/twitter/internal/android/service/m;)Lcom/twitter/internal/android/service/a;

    invoke-virtual {p1, p2}, Lcom/twitter/library/service/b;->a(Lcom/twitter/internal/android/service/k;)Lcom/twitter/internal/android/service/a;

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->d()V

    return-void
.end method

.method public a(Lcom/twitter/library/service/b;Lcom/twitter/internal/android/service/k;Lcom/twitter/library/client/Session;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/w;->c:Lcom/twitter/library/client/s;

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/library/client/s;->a(Lcom/twitter/library/service/b;Lcom/twitter/internal/android/service/k;Lcom/twitter/library/client/Session;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/b;IILcom/twitter/library/service/c;)Z
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Request cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-eqz p4, :cond_2

    new-instance v0, Lcom/twitter/library/service/d;

    invoke-direct {v0, p2, p4}, Lcom/twitter/library/service/d;-><init>(ILcom/twitter/library/service/c;)V

    :goto_0
    invoke-virtual {p1, p3}, Lcom/twitter/library/service/b;->d(I)Lcom/twitter/library/service/b;

    move-result-object v1

    invoke-direct {p0, v0}, Lcom/twitter/library/client/w;->c(Lcom/twitter/library/service/a;)Lcom/twitter/library/service/k;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/service/b;->a(Lcom/twitter/internal/android/service/m;)Lcom/twitter/internal/android/service/a;

    invoke-virtual {p0, p1}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    if-eqz p4, :cond_1

    invoke-interface {p4, p2, p3, p1}, Lcom/twitter/library/service/c;->a(IILcom/twitter/library/service/b;)V

    :cond_1
    const/4 v0, 0x1

    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/w;->d:Lcom/twitter/internal/android/service/j;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/service/j;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public b(Lcom/twitter/library/service/a;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/w;->b:Lcom/twitter/library/service/k;

    invoke-virtual {v0, p1}, Lcom/twitter/library/service/k;->b(Lcom/twitter/library/service/a;)V

    return-void
.end method
