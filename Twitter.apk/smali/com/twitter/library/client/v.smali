.class public Lcom/twitter/library/client/v;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Landroid/net/Uri;

.field public final b:Landroid/net/Uri;

.field public final c:Z

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Z

.field public i:I


# direct methods
.method public constructor <init>(Landroid/net/Uri;Landroid/net/Uri;Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/client/v;->a:Landroid/net/Uri;

    iput-object p2, p0, Lcom/twitter/library/client/v;->b:Landroid/net/Uri;

    iput-boolean p3, p0, Lcom/twitter/library/client/v;->c:Z

    iput-object v0, p0, Lcom/twitter/library/client/v;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/client/v;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/client/v;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/client/v;->g:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/client/v;->h:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/library/client/v;->i:I

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Landroid/net/Uri;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/client/v;->a:Landroid/net/Uri;

    iput-object p2, p0, Lcom/twitter/library/client/v;->b:Landroid/net/Uri;

    iput-boolean p3, p0, Lcom/twitter/library/client/v;->c:Z

    iput-object p4, p0, Lcom/twitter/library/client/v;->d:Ljava/lang/String;

    iput-object p5, p0, Lcom/twitter/library/client/v;->e:Ljava/lang/String;

    iput-object p6, p0, Lcom/twitter/library/client/v;->f:Ljava/lang/String;

    iput-object p7, p0, Lcom/twitter/library/client/v;->g:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/client/v;->h:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/library/client/v;->i:I

    return-void
.end method
