.class public abstract Lcom/twitter/library/client/j;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;I)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;ILjava/lang/String;[I)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;ILjava/lang/String;[ILcom/twitter/library/api/account/LvEligibilityResponse;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;JLjava/lang/String;ILjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/io/File;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;I)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;IJ)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;IIILjava/util/ArrayList;[Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;IJJ)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;IJJIZ)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;IZLjava/lang/String;Lcom/twitter/library/api/search/TwitterTypeAheadGroup;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;I[J)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JI)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JII)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JIILjava/util/ArrayList;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JIJJLcom/twitter/library/api/TwitterUser;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JIZ)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JJII[Lcom/twitter/library/api/TwitterUser;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JJLjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JLcom/twitter/library/api/ActivitySummary;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;Lcom/twitter/library/api/TwitterStatus$Translation;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;Lcom/twitter/library/api/TwitterUser;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;Lcom/twitter/library/api/UserSettings;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;Ljava/util/ArrayList;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;[J)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/util/HashMap;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;I[ILjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;I[ILjava/lang/String;J)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;I[ILjava/lang/String;JI)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;I[ILjava/lang/String;Lcom/twitter/library/api/TwitterUser;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;I[ILjava/lang/String;[J)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Z)V
    .locals 0

    return-void
.end method

.method public a(Ljava/util/HashMap;)V
    .locals 0

    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b(Lcom/twitter/library/client/Session;ILjava/lang/String;[I)V
    .locals 0

    return-void
.end method

.method public b(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public b(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0

    return-void
.end method

.method public b(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;IJJ)V
    .locals 0

    return-void
.end method

.method public b(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 0

    return-void
.end method

.method public b(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;Lcom/twitter/library/api/TwitterUser;)V
    .locals 0

    return-void
.end method

.method public b(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;Ljava/util/ArrayList;)V
    .locals 0

    return-void
.end method

.method public c(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public c(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0

    return-void
.end method

.method public c(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;IJJ)V
    .locals 0

    return-void
.end method

.method public c(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 0

    return-void
.end method

.method public d(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public d(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0

    return-void
.end method

.method public d(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 0

    return-void
.end method

.method public e(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public e(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 0

    return-void
.end method

.method public f(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public f(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 0

    return-void
.end method

.method public g(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public g(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 0

    return-void
.end method

.method public h(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public i(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public j(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public k(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public l(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    return-void
.end method
