.class Lcom/twitter/library/client/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/internal/android/widget/as;


# instance fields
.field final synthetic a:Lcom/twitter/library/client/AbsFragmentActivity;


# direct methods
.method constructor <init>(Lcom/twitter/library/client/AbsFragmentActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/client/a;->a:Lcom/twitter/library/client/AbsFragmentActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lhn;)Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/client/a;->a:Lcom/twitter/library/client/AbsFragmentActivity;

    invoke-static {v0}, Lcom/twitter/library/client/AbsFragmentActivity;->a(Lcom/twitter/library/client/AbsFragmentActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/as;

    invoke-interface {v0, p1}, Lcom/twitter/internal/android/widget/as;->a(Lhn;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/client/a;->a:Lcom/twitter/library/client/AbsFragmentActivity;

    invoke-virtual {v0, p1}, Lcom/twitter/library/client/AbsFragmentActivity;->a(Lhn;)Z

    move-result v0

    goto :goto_0
.end method

.method public b(Lhn;)Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/client/a;->a:Lcom/twitter/library/client/AbsFragmentActivity;

    invoke-static {v0}, Lcom/twitter/library/client/AbsFragmentActivity;->a(Lcom/twitter/library/client/AbsFragmentActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/as;

    invoke-interface {v0, p1}, Lcom/twitter/internal/android/widget/as;->b(Lhn;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/client/a;->a:Lcom/twitter/library/client/AbsFragmentActivity;

    invoke-virtual {v0, p1}, Lcom/twitter/library/client/AbsFragmentActivity;->b(Lhn;)Z

    move-result v0

    goto :goto_0
.end method
