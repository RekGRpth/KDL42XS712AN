.class Lcom/twitter/library/client/af;
.super Lcom/twitter/library/service/a;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/library/client/aa;

.field private final b:I


# direct methods
.method constructor <init>(Lcom/twitter/library/client/aa;I)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/client/af;->a:Lcom/twitter/library/client/aa;

    invoke-direct {p0}, Lcom/twitter/library/service/a;-><init>()V

    iput p2, p0, Lcom/twitter/library/client/af;->b:I

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/internal/android/service/a;)V
    .locals 0

    check-cast p1, Lcom/twitter/library/service/b;

    invoke-virtual {p0, p1}, Lcom/twitter/library/client/af;->a(Lcom/twitter/library/service/b;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/b;)V
    .locals 8

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/client/af;->a:Lcom/twitter/library/client/aa;

    invoke-static {v2}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/aa;)Ljava/util/HashMap;

    move-result-object v2

    iget-object v1, v1, Lcom/twitter/library/service/p;->a:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/client/Session;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/twitter/library/client/af;->a:Lcom/twitter/library/client/aa;

    invoke-static {v2}, Lcom/twitter/library/client/aa;->b(Lcom/twitter/library/client/aa;)Ljava/util/HashMap;

    move-result-object v2

    iget-object v3, p1, Lcom/twitter/library/service/b;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/client/ag;

    iget v3, p0, Lcom/twitter/library/client/af;->b:I

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    check-cast p1, Lcom/twitter/library/api/account/s;

    invoke-virtual {p1}, Lcom/twitter/library/api/account/s;->e()[I

    move-result-object v3

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/twitter/library/client/af;->a:Lcom/twitter/library/client/aa;

    invoke-virtual {v4, v1}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/Session;)Ljava/lang/String;

    :cond_2
    invoke-virtual {p1}, Lcom/twitter/library/api/account/s;->f()Lcom/twitter/library/network/OneFactorLoginResponse;

    move-result-object v4

    if-eqz v2, :cond_0

    invoke-interface {v2, v1, v0, v4, v3}, Lcom/twitter/library/client/ag;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/service/e;Lcom/twitter/library/network/OneFactorLoginResponse;[I)V

    goto :goto_0

    :pswitch_1
    const/4 v3, 0x0

    check-cast p1, Lcom/twitter/library/api/account/q;

    invoke-virtual {p1}, Lcom/twitter/library/api/account/q;->f()[I

    move-result-object v5

    invoke-virtual {p1}, Lcom/twitter/library/api/account/q;->g()Lcom/twitter/library/network/LoginResponse;

    move-result-object v4

    :try_start_0
    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/twitter/library/client/af;->a:Lcom/twitter/library/client/aa;

    invoke-virtual {p1}, Lcom/twitter/library/api/account/q;->e()Lcom/twitter/library/api/TwitterUser;

    move-result-object v7

    invoke-static {v6, v1, v4, v7}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/aa;Lcom/twitter/library/client/Session;Lcom/twitter/library/network/LoginResponse;Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    :goto_1
    if-eqz v2, :cond_0

    invoke-interface {v2, v0, v1, v5, v3}, Lcom/twitter/library/client/ag;->a(Lcom/twitter/library/service/e;Lcom/twitter/library/client/Session;[ILjava/lang/String;)V

    goto :goto_0

    :cond_3
    :try_start_1
    iget-object v4, p0, Lcom/twitter/library/client/af;->a:Lcom/twitter/library/client/aa;

    invoke-virtual {p1}, Lcom/twitter/library/api/account/q;->h()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v1, v6}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/aa;Lcom/twitter/library/client/Session;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v4

    const/16 v6, 0x190

    invoke-virtual {v0, v6, v4}, Lcom/twitter/library/service/e;->a(ILjava/lang/Exception;)V

    goto :goto_1

    :pswitch_2
    check-cast p1, Lcom/twitter/library/api/account/r;

    invoke-virtual {p1}, Lcom/twitter/library/api/account/r;->e()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/library/api/account/r;->f()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1}, Lcom/twitter/library/api/account/r;->g()Ljava/lang/String;

    move-result-object v3

    if-eqz v2, :cond_0

    invoke-interface {v2, v3, v1, v0}, Lcom/twitter/library/client/ag;->a(Ljava/lang/String;Ljava/lang/Boolean;[I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
