.class Lcom/twitter/library/client/ab;
.super Lcom/twitter/library/service/a;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/library/client/aa;


# direct methods
.method constructor <init>(Lcom/twitter/library/client/aa;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/client/ab;->a:Lcom/twitter/library/client/aa;

    invoke-direct {p0}, Lcom/twitter/library/service/a;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/internal/android/service/a;)V
    .locals 0

    check-cast p1, Lcom/twitter/library/service/b;

    invoke-virtual {p0, p1}, Lcom/twitter/library/client/ab;->a(Lcom/twitter/library/service/b;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/b;)V
    .locals 10

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v2, p1

    check-cast v2, Lcom/twitter/library/api/account/c;

    invoke-virtual {v2}, Lcom/twitter/library/api/account/c;->f()[I

    move-result-object v4

    invoke-virtual {v2}, Lcom/twitter/library/api/account/c;->h()Z

    move-result v7

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/twitter/library/service/e;

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/client/ab;->a:Lcom/twitter/library/client/aa;

    invoke-static {v1}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/aa;)Ljava/util/HashMap;

    move-result-object v1

    iget-object v0, v0, Lcom/twitter/library/service/p;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/client/Session;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v3}, Lcom/twitter/library/service/e;->a()Z

    move-result v8

    iget-object v0, p0, Lcom/twitter/library/client/ab;->a:Lcom/twitter/library/client/aa;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->b(Lcom/twitter/library/client/aa;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v9, v2, Lcom/twitter/library/api/account/c;->a:Ljava/lang/String;

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/ac;

    if-eqz v8, :cond_4

    :try_start_0
    iget-object v8, p0, Lcom/twitter/library/client/ab;->a:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/api/account/c;->g()Lcom/twitter/library/network/LoginResponse;

    move-result-object v9

    invoke-virtual {v2}, Lcom/twitter/library/api/account/c;->e()Lcom/twitter/library/api/TwitterUser;

    move-result-object v2

    invoke-static {v8, v1, v9, v2}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/aa;Lcom/twitter/library/client/Session;Lcom/twitter/library/network/LoginResponse;Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v8

    if-eqz v0, :cond_0

    if-nez v7, :cond_3

    move v2, v6

    :goto_1
    invoke-interface {v0, v1, v8, v2}, Lcom/twitter/library/client/ac;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    sget-object v2, Lcom/twitter/library/client/Session$LoginStatus;->a:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    if-eqz v0, :cond_0

    const/4 v2, 0x2

    invoke-virtual {v3}, Lcom/twitter/library/service/e;->c()I

    move-result v3

    if-nez v7, :cond_2

    move v5, v6

    :cond_2
    invoke-interface/range {v0 .. v5}, Lcom/twitter/library/client/ac;->a(Lcom/twitter/library/client/Session;II[IZ)V

    goto :goto_0

    :cond_3
    move v2, v5

    goto :goto_1

    :cond_4
    sget-object v2, Lcom/twitter/library/client/Session$LoginStatus;->a:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    if-eqz v0, :cond_0

    invoke-virtual {v3}, Lcom/twitter/library/service/e;->c()I

    move-result v3

    if-nez v7, :cond_5

    move v5, v6

    :cond_5
    move v2, v6

    invoke-interface/range {v0 .. v5}, Lcom/twitter/library/client/ac;->a(Lcom/twitter/library/client/Session;II[IZ)V

    goto :goto_0
.end method
