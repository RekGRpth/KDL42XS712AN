.class Lcom/twitter/library/client/ad;
.super Lcom/twitter/library/service/a;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/library/client/aa;

.field private final b:I


# direct methods
.method constructor <init>(Lcom/twitter/library/client/aa;I)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/client/ad;->a:Lcom/twitter/library/client/aa;

    invoke-direct {p0}, Lcom/twitter/library/service/a;-><init>()V

    iput p2, p0, Lcom/twitter/library/client/ad;->b:I

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/internal/android/service/a;)V
    .locals 0

    check-cast p1, Lcom/twitter/library/service/b;

    invoke-virtual {p0, p1}, Lcom/twitter/library/client/ad;->a(Lcom/twitter/library/service/b;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/b;)V
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x1

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/client/ad;->a:Lcom/twitter/library/client/aa;

    invoke-static {v2}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/aa;)Ljava/util/HashMap;

    move-result-object v2

    iget-object v1, v1, Lcom/twitter/library/service/p;->a:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/client/Session;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    check-cast p1, Lcom/twitter/library/api/account/i;

    invoke-virtual {p1}, Lcom/twitter/library/api/account/i;->f()[I

    move-result-object v3

    invoke-virtual {p1}, Lcom/twitter/library/api/account/i;->g()Lcom/twitter/library/network/LoginResponse;

    move-result-object v4

    iget-object v2, p0, Lcom/twitter/library/client/ad;->a:Lcom/twitter/library/client/aa;

    invoke-static {v2}, Lcom/twitter/library/client/aa;->b(Lcom/twitter/library/client/aa;)Ljava/util/HashMap;

    move-result-object v2

    iget-object v5, p1, Lcom/twitter/library/api/account/i;->a:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/client/ae;

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->A()Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_2

    if-eqz v4, :cond_2

    iget v5, v4, Lcom/twitter/library/network/LoginResponse;->c:I

    if-ne v5, v7, :cond_2

    iget-object v5, v4, Lcom/twitter/library/network/LoginResponse;->b:Lcom/twitter/library/network/LoginVerificationRequiredResponse;

    iget v5, v5, Lcom/twitter/library/network/LoginVerificationRequiredResponse;->e:I

    if-ne v5, v7, :cond_2

    sget-object v0, Lcom/twitter/library/client/Session$LoginStatus;->a:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    if-eqz v2, :cond_0

    iget-object v0, v4, Lcom/twitter/library/network/LoginResponse;->b:Lcom/twitter/library/network/LoginVerificationRequiredResponse;

    invoke-interface {v2, v1, v0}, Lcom/twitter/library/client/ae;->b(Lcom/twitter/library/client/Session;Lcom/twitter/library/network/LoginVerificationRequiredResponse;)V

    goto :goto_0

    :cond_2
    if-eqz v4, :cond_4

    iget v5, v4, Lcom/twitter/library/network/LoginResponse;->c:I

    if-ne v5, v7, :cond_4

    iget-object v5, v4, Lcom/twitter/library/network/LoginResponse;->b:Lcom/twitter/library/network/LoginVerificationRequiredResponse;

    iget v5, v5, Lcom/twitter/library/network/LoginVerificationRequiredResponse;->e:I

    if-eq v5, v6, :cond_3

    iget-object v5, v4, Lcom/twitter/library/network/LoginResponse;->b:Lcom/twitter/library/network/LoginVerificationRequiredResponse;

    iget v5, v5, Lcom/twitter/library/network/LoginVerificationRequiredResponse;->e:I

    if-nez v5, :cond_4

    :cond_3
    sget-object v0, Lcom/twitter/library/client/Session$LoginStatus;->a:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    if-eqz v2, :cond_0

    iget-object v0, v4, Lcom/twitter/library/network/LoginResponse;->b:Lcom/twitter/library/network/LoginVerificationRequiredResponse;

    invoke-interface {v2, v1, v0}, Lcom/twitter/library/client/ae;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/network/LoginVerificationRequiredResponse;)V

    goto :goto_0

    :cond_4
    iget-object v5, p1, Lcom/twitter/library/api/account/i;->e:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/client/ad;->a:Lcom/twitter/library/client/aa;

    invoke-virtual {p1}, Lcom/twitter/library/api/account/i;->e()Lcom/twitter/library/api/TwitterUser;

    move-result-object v6

    invoke-static {v0, v1, v4, v6}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/aa;Lcom/twitter/library/client/Session;Lcom/twitter/library/network/LoginResponse;Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v0

    if-eqz v2, :cond_0

    invoke-interface {v2, v1, v0}, Lcom/twitter/library/client/ae;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/twitter/library/client/ad;->a:Lcom/twitter/library/client/aa;

    invoke-static {v0, v1, v5}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/aa;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    if-eqz v2, :cond_0

    invoke-interface {v2, v1, v7, v3}, Lcom/twitter/library/client/ae;->a(Lcom/twitter/library/client/Session;I[I)V

    goto :goto_0

    :cond_5
    if-eqz v2, :cond_6

    iget v0, p0, Lcom/twitter/library/client/ad;->b:I

    if-ne v0, v6, :cond_7

    check-cast p1, Lcom/twitter/library/api/account/j;

    invoke-virtual {p1}, Lcom/twitter/library/api/account/j;->h()Z

    move-result v0

    :goto_1
    if-eqz v0, :cond_8

    invoke-interface {v2, v1, v6, v3}, Lcom/twitter/library/client/ae;->b(Lcom/twitter/library/client/Session;I[I)V

    :cond_6
    :goto_2
    iget-object v0, p0, Lcom/twitter/library/client/ad;->a:Lcom/twitter/library/client/aa;

    invoke-static {v0, v1, v5}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/aa;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    const/4 v0, 0x0

    goto :goto_1

    :cond_8
    invoke-interface {v2, v1, v6, v3}, Lcom/twitter/library/client/ae;->a(Lcom/twitter/library/client/Session;I[I)V

    goto :goto_2
.end method
