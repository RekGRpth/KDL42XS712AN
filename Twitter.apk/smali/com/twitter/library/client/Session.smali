.class public Lcom/twitter/library/client/Session;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:Lcom/twitter/library/client/Session$LoginStatus;

.field private final b:Ljava/lang/String;

.field private c:Z

.field private d:Ljava/lang/String;

.field private e:Lcom/twitter/library/network/OAuthToken;

.field private f:Lcom/twitter/library/network/OAuth2Token;

.field private g:Lcom/twitter/library/api/TwitterUser;

.field private h:Lcom/twitter/library/api/RateLimit;

.field private i:Lcom/twitter/library/api/UserSettings;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/library/client/y;

    invoke-direct {v0}, Lcom/twitter/library/client/y;-><init>()V

    sput-object v0, Lcom/twitter/library/client/Session;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x6

    invoke-static {v0}, Lcom/twitter/internal/util/j;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/client/Session;->b:Ljava/lang/String;

    sget-object v0, Lcom/twitter/library/client/Session$LoginStatus;->a:Lcom/twitter/library/client/Session$LoginStatus;

    iput-object v0, p0, Lcom/twitter/library/client/Session;->a:Lcom/twitter/library/client/Session$LoginStatus;

    new-instance v0, Lcom/twitter/library/network/OAuth2Token;

    const-string/jumbo v1, "bearer_token"

    sget-object v2, Lcom/twitter/library/network/n;->f:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/network/OAuth2Token;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/library/client/Session;->f:Lcom/twitter/library/network/OAuth2Token;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/Session$LoginStatus;

    iput-object v0, p0, Lcom/twitter/library/client/Session;->a:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/client/Session;->b:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/client/Session;->d:Ljava/lang/String;

    const-class v0, Lcom/twitter/library/network/OAuthToken;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/network/OAuthToken;

    iput-object v0, p0, Lcom/twitter/library/client/Session;->e:Lcom/twitter/library/network/OAuthToken;

    const-class v0, Lcom/twitter/library/network/OAuth2Token;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/network/OAuth2Token;

    iput-object v0, p0, Lcom/twitter/library/client/Session;->f:Lcom/twitter/library/network/OAuth2Token;

    const-class v0, Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    iput-object v0, p0, Lcom/twitter/library/client/Session;->g:Lcom/twitter/library/api/TwitterUser;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/client/Session;->c:Z

    iput-object v1, p0, Lcom/twitter/library/client/Session;->d:Ljava/lang/String;

    iput-object v1, p0, Lcom/twitter/library/client/Session;->e:Lcom/twitter/library/network/OAuthToken;

    iput-object v1, p0, Lcom/twitter/library/client/Session;->g:Lcom/twitter/library/api/TwitterUser;

    iput-object v1, p0, Lcom/twitter/library/client/Session;->i:Lcom/twitter/library/api/UserSettings;

    return-void
.end method

.method public a(Lcom/twitter/library/api/RateLimit;)V
    .locals 0

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/twitter/library/client/Session;->h:Lcom/twitter/library/api/RateLimit;

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/api/TwitterUser;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/client/Session;->g:Lcom/twitter/library/api/TwitterUser;

    return-void
.end method

.method public a(Lcom/twitter/library/api/UserSettings;)V
    .locals 0

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/twitter/library/client/Session;->i:Lcom/twitter/library/api/UserSettings;

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session$LoginStatus;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/client/Session;->a:Lcom/twitter/library/client/Session$LoginStatus;

    return-void
.end method

.method public a(Lcom/twitter/library/network/OAuthToken;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/client/Session;->e:Lcom/twitter/library/network/OAuthToken;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/client/Session;->d:Ljava/lang/String;

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/client/Session;->c:Z

    return-void
.end method

.method public b()Lcom/twitter/library/client/Session$LoginStatus;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/Session;->a:Lcom/twitter/library/client/Session$LoginStatus;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/Session;->b:Ljava/lang/String;

    return-object v0
.end method

.method public d()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/client/Session;->a:Lcom/twitter/library/client/Session$LoginStatus;

    sget-object v1, Lcom/twitter/library/client/Session$LoginStatus;->c:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/Session$LoginStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/Session;->d:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_0

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/library/client/Session;->b:Ljava/lang/String;

    check-cast p1, Lcom/twitter/library/client/Session;

    iget-object v1, p1, Lcom/twitter/library/client/Session;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Lcom/twitter/library/api/TwitterUser;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/Session;->g:Lcom/twitter/library/api/TwitterUser;

    return-object v0
.end method

.method public g()J
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/client/Session;->g:Lcom/twitter/library/api/TwitterUser;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/client/Session;->g:Lcom/twitter/library/api/TwitterUser;

    iget-wide v0, v0, Lcom/twitter/library/api/TwitterUser;->userId:J

    goto :goto_0
.end method

.method public h()Lcom/twitter/library/network/OAuthToken;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/Session;->e:Lcom/twitter/library/network/OAuthToken;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/Session;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public i()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/client/Session;->c:Z

    return v0
.end method

.method public j()Lcom/twitter/library/api/UserSettings;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/Session;->i:Lcom/twitter/library/api/UserSettings;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/Session;->a:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object v0, p0, Lcom/twitter/library/client/Session;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/client/Session;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/client/Session;->e:Lcom/twitter/library/network/OAuthToken;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/twitter/library/client/Session;->f:Lcom/twitter/library/network/OAuth2Token;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/twitter/library/client/Session;->g:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
