.class public Lcom/twitter/library/client/t;
.super Lcom/twitter/library/client/r;
.source "Twttr"


# static fields
.field private static final d:Z


# instance fields
.field private final e:Lcom/twitter/library/api/TwitterUser;

.field private f:I

.field private g:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/twitter/library/client/App;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/library/client/t;->d:Z

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/api/TwitterUser;)V
    .locals 1

    const-class v0, Lcom/twitter/library/client/t;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/client/r;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/library/client/t;->c(I)Lcom/twitter/library/service/b;

    iput-object p3, p0, Lcom/twitter/library/client/t;->e:Lcom/twitter/library/api/TwitterUser;

    return-void
.end method


# virtual methods
.method protected final b(Lcom/twitter/library/service/e;)V
    .locals 7

    sget-boolean v0, Lcom/twitter/library/client/t;->d:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/twitter/library/service/e;->a(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/client/t;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/client/t;->e:Lcom/twitter/library/api/TwitterUser;

    new-instance v2, Lcom/twitter/library/platform/DataSyncResult;

    iget-object v3, v1, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    iget-wide v4, v1, Lcom/twitter/library/api/TwitterUser;->userId:J

    iget-object v6, p0, Lcom/twitter/library/client/t;->l:Landroid/content/Context;

    invoke-static {v6}, Lcom/twitter/library/platform/PushService;->c(Landroid/content/Context;)Z

    move-result v6

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/twitter/library/platform/DataSyncResult;-><init>(Ljava/lang/String;JZ)V

    new-instance v3, Lcom/twitter/library/platform/g;

    iget-object v4, p0, Lcom/twitter/library/client/t;->l:Landroid/content/Context;

    iget-object v5, v0, Lcom/twitter/library/service/p;->e:Ljava/lang/String;

    iget-object v0, v0, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v3, v4, v1, v5, v0}, Lcom/twitter/library/platform/g;-><init>(Landroid/content/Context;Lcom/twitter/library/api/TwitterUser;Ljava/lang/String;Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {p0}, Lcom/twitter/library/client/t;->t()Z

    move-result v0

    invoke-virtual {v3, v0}, Lcom/twitter/library/platform/g;->a(Z)Lcom/twitter/library/platform/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/platform/g;->a()J

    move-result-wide v3

    new-instance v1, Landroid/content/SyncResult;

    invoke-direct {v1}, Landroid/content/SyncResult;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/platform/g;->a(Landroid/content/SyncResult;Lcom/twitter/library/platform/DataSyncResult;)Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/twitter/library/service/e;->a(Z)V

    invoke-virtual {p1}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-wide v3, p0, Lcom/twitter/library/client/t;->g:J

    iget-object v0, v2, Lcom/twitter/library/platform/DataSyncResult;->f:Lcom/twitter/library/platform/e;

    if-eqz v0, :cond_2

    iget-object v0, v2, Lcom/twitter/library/platform/DataSyncResult;->f:Lcom/twitter/library/platform/e;

    iget v0, v0, Lcom/twitter/library/platform/e;->c:I

    :goto_1
    iput v0, p0, Lcom/twitter/library/client/t;->f:I

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public e()V
    .locals 2

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/library/client/t;->f:I

    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/twitter/library/client/t;->g:J

    return-void
.end method

.method public f()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/client/t;->f:I

    return v0
.end method

.method public g()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/client/t;->g:J

    return-wide v0
.end method
