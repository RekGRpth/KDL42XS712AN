.class public abstract Lcom/twitter/library/client/AbsFragmentActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "Twttr"


# instance fields
.field private final a:Ljava/util/LinkedHashSet;

.field private b:Lcom/twitter/internal/android/widget/ToolBar;

.field private c:Lcom/twitter/internal/android/widget/ToolBar;

.field private d:Lhm;

.field private e:Ljava/util/ArrayList;

.field private f:Z

.field private g:Z

.field private h:Landroid/content/Intent;

.field private i:Lcom/twitter/library/client/w;

.field private j:Ljava/util/ArrayList;

.field private k:Lcom/twitter/library/client/aa;

.field private l:Lcom/twitter/library/client/c;

.field private m:Lcom/twitter/library/client/e;

.field private n:Z

.field private o:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->a:Ljava/util/LinkedHashSet;

    return-void
.end method

.method private a(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 2

    const-string/jumbo v0, "intent.extra.ANCESTOR"

    invoke-virtual {p0}, Lcom/twitter/library/client/AbsFragmentActivity;->s_()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/library/client/AbsFragmentActivity;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->e:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/library/client/AbsFragmentActivity;)Lcom/twitter/library/client/e;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->m:Lcom/twitter/library/client/e;

    return-object v0
.end method

.method private b(Landroid/content/Intent;)V
    .locals 4

    const-string/jumbo v0, "intent.extra.ANCESTOR"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "intent.extra.ANCESTOR"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->h:Landroid/content/Intent;

    const-string/jumbo v0, "intent.extra.ANCESTOR"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/client/AbsFragmentActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/twitter/library/client/AbsFragmentActivity;->o:Z

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string/jumbo v2, "account_name"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v0, v2}, Lcom/twitter/library/client/aa;->d(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->o:Z

    :cond_1
    const-string/jumbo v0, "account_name"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method private d(Lhn;)Z
    .locals 1

    invoke-virtual {p1}, Lhn;->m()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lhn;->m()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/client/AbsFragmentActivity;->startActivity(Landroid/content/Intent;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected abstract O()V
.end method

.method public final R()Lcom/twitter/internal/android/widget/ToolBar;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->b:Lcom/twitter/internal/android/widget/ToolBar;

    return-object v0
.end method

.method public final S()Lhm;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->d:Lhm;

    return-object v0
.end method

.method public final T()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->b:Lcom/twitter/internal/android/widget/ToolBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->b:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final U()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->b:Lcom/twitter/internal/android/widget/ToolBar;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final V()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->b:Lcom/twitter/internal/android/widget/ToolBar;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->f:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->b:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {p0, v0}, Lcom/twitter/library/client/AbsFragmentActivity;->a(Lcom/twitter/internal/android/widget/ToolBar;)Z

    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->b:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->requestLayout()V

    goto :goto_0
.end method

.method protected W()V
    .locals 1

    invoke-static {p0}, Landroid/support/v4/app/NavUtils;->getParentActivityIntent(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, p0, v0}, Lcom/twitter/library/client/AbsFragmentActivity;->b(Landroid/app/Activity;Landroid/content/Intent;)V

    return-void
.end method

.method protected X()Lcom/twitter/library/client/aa;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->k:Lcom/twitter/library/client/aa;

    return-object v0
.end method

.method protected final Y()Lcom/twitter/library/client/Session;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->k:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method protected Z()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->i:Lcom/twitter/library/client/w;

    invoke-virtual {v0, p1}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a(IILcom/twitter/library/service/b;)V
    .locals 0

    return-void
.end method

.method public abstract a(Landroid/os/Bundle;Lcom/twitter/library/client/e;)V
.end method

.method public final a(Lcom/twitter/internal/android/widget/as;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method protected final a(Lcom/twitter/library/client/z;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->a:Ljava/util/LinkedHashSet;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected final a(Lcom/twitter/library/service/a;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->i:Lcom/twitter/library/client/w;

    invoke-virtual {v0, p1}, Lcom/twitter/library/client/w;->a(Lcom/twitter/library/service/a;)V

    return-void
.end method

.method protected a(Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected final a(Lcom/twitter/library/service/b;II)Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->l:Lcom/twitter/library/client/c;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/library/client/c;

    invoke-direct {v0, p0}, Lcom/twitter/library/client/c;-><init>(Lcom/twitter/library/client/AbsFragmentActivity;)V

    iput-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->l:Lcom/twitter/library/client/c;

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->i:Lcom/twitter/library/client/w;

    iget-object v1, p0, Lcom/twitter/library/client/AbsFragmentActivity;->l:Lcom/twitter/library/client/c;

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/twitter/library/client/w;->a(Lcom/twitter/library/service/b;IILcom/twitter/library/service/c;)Z

    move-result v0

    return v0
.end method

.method protected a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public a(Lhn;)Z
    .locals 2

    invoke-virtual {p1}, Lhn;->a()I

    move-result v0

    sget v1, Lig;->home:I

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/client/AbsFragmentActivity;->j()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/library/client/AbsFragmentActivity;->d(Lhn;)Z

    move-result v0

    goto :goto_0
.end method

.method public final aa()Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->h:Landroid/content/Intent;

    return-object v0
.end method

.method protected ab()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->n:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/client/AbsFragmentActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;)Lcom/twitter/library/client/e;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected b(IILcom/twitter/library/service/b;)V
    .locals 0

    return-void
.end method

.method protected b(Landroid/app/Activity;Landroid/content/Intent;)V
    .locals 1

    const/high16 v0, 0x4000000

    invoke-virtual {p2, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, p2}, Lcom/twitter/library/client/AbsFragmentActivity;->d(Landroid/content/Intent;)V

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public final b(Lcom/twitter/internal/android/widget/as;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method protected final b(Lcom/twitter/library/service/a;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->i:Lcom/twitter/library/client/w;

    invoke-virtual {v0, p1}, Lcom/twitter/library/client/w;->b(Lcom/twitter/library/service/a;)V

    return-void
.end method

.method protected b(Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected b(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b(Lhn;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final c(Landroid/content/Intent;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/client/AbsFragmentActivity;->h:Landroid/content/Intent;

    return-void
.end method

.method public c(Lhn;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/twitter/library/client/AbsFragmentActivity;->d(Lhn;)Z

    move-result v0

    return v0
.end method

.method public d(Landroid/content/Intent;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    const/4 v2, -0x1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-super {p0, p1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, v2}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method protected d(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected e(Ljava/lang/String;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected abstract j()V
.end method

.method public onAttachedToWindow()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onAttachedToWindow()V

    invoke-virtual {p0}, Lcom/twitter/library/client/AbsFragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setFormat(I)V

    return-void
.end method

.method public onBackPressed()V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->o:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->o:Z

    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    return-void
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/16 v4, 0x8

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/twitter/library/client/w;->a(Landroid/content/Context;)Lcom/twitter/library/client/w;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->i:Lcom/twitter/library/client/w;

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->k:Lcom/twitter/library/client/aa;

    invoke-virtual {p0, p1}, Lcom/twitter/library/client/AbsFragmentActivity;->b(Landroid/os/Bundle;)Lcom/twitter/library/client/e;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/twitter/library/client/e;

    invoke-direct {v0}, Lcom/twitter/library/client/e;-><init>()V

    move-object v2, v0

    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, v2, Lcom/twitter/library/client/e;->h:Z

    iput-object v2, p0, Lcom/twitter/library/client/AbsFragmentActivity;->m:Lcom/twitter/library/client/e;

    iget v0, v2, Lcom/twitter/library/client/e;->i:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/client/AbsFragmentActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/twitter/library/client/AbsFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iget-boolean v1, v2, Lcom/twitter/library/client/e;->j:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/library/client/AbsFragmentActivity;->k:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->d()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/twitter/library/client/AbsFragmentActivity;->O()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    move-object v2, v0

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_5

    const-string/jumbo v1, "pending_reqs"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/client/AbsFragmentActivity;->j:Ljava/util/ArrayList;

    :goto_2
    new-instance v1, Lcom/twitter/library/client/d;

    invoke-direct {v1, p0}, Lcom/twitter/library/client/d;-><init>(Lcom/twitter/library/client/AbsFragmentActivity;)V

    invoke-virtual {p0, v1}, Lcom/twitter/library/client/AbsFragmentActivity;->a(Lcom/twitter/library/client/z;)V

    invoke-direct {p0, v0}, Lcom/twitter/library/client/AbsFragmentActivity;->b(Landroid/content/Intent;)V

    sget v0, Lig;->toolbar:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/client/AbsFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    iput-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->b:Lcom/twitter/internal/android/widget/ToolBar;

    sget v1, Lig;->utilitybar:I

    invoke-virtual {p0, v1}, Lcom/twitter/library/client/AbsFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/internal/android/widget/ToolBar;

    iput-object v1, p0, Lcom/twitter/library/client/AbsFragmentActivity;->c:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {p0, p1, v2}, Lcom/twitter/library/client/AbsFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/library/client/e;)V

    if-nez v0, :cond_3

    if-eqz v1, :cond_0

    :cond_3
    new-instance v3, Lhm;

    invoke-direct {v3, p0}, Lhm;-><init>(Landroid/app/Activity;)V

    iput-object v3, p0, Lcom/twitter/library/client/AbsFragmentActivity;->d:Lhm;

    if-eqz v0, :cond_4

    iget v2, v2, Lcom/twitter/library/client/e;->k:I

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/ToolBar;->setDisplayOptions(I)V

    invoke-virtual {p0, v3, v0}, Lcom/twitter/library/client/AbsFragmentActivity;->a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/library/client/AbsFragmentActivity;->f:Z

    iget-boolean v2, p0, Lcom/twitter/library/client/AbsFragmentActivity;->f:Z

    if-eqz v2, :cond_7

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/twitter/library/client/AbsFragmentActivity;->e:Ljava/util/ArrayList;

    new-instance v2, Lcom/twitter/library/client/a;

    invoke-direct {v2, p0}, Lcom/twitter/library/client/a;-><init>(Lcom/twitter/library/client/AbsFragmentActivity;)V

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/ToolBar;->setOnToolBarItemSelectedListener(Lcom/twitter/internal/android/widget/as;)V

    invoke-virtual {p0, v0}, Lcom/twitter/library/client/AbsFragmentActivity;->a(Lcom/twitter/internal/android/widget/ToolBar;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v0, v5}, Lcom/twitter/internal/android/widget/ToolBar;->setVisibility(I)V

    :cond_4
    :goto_3
    if-eqz v1, :cond_0

    invoke-virtual {p0, v3, v1}, Lcom/twitter/library/client/AbsFragmentActivity;->b(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->g:Z

    iget-boolean v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->g:Z

    if-eqz v0, :cond_9

    new-instance v0, Lcom/twitter/library/client/b;

    invoke-direct {v0, p0}, Lcom/twitter/library/client/b;-><init>(Lcom/twitter/library/client/AbsFragmentActivity;)V

    invoke-virtual {v1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setOnToolBarItemSelectedListener(Lcom/twitter/internal/android/widget/as;)V

    invoke-virtual {p0, v1}, Lcom/twitter/library/client/AbsFragmentActivity;->b(Lcom/twitter/internal/android/widget/ToolBar;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {v1, v5}, Lcom/twitter/internal/android/widget/ToolBar;->setVisibility(I)V

    goto/16 :goto_1

    :cond_5
    new-instance v1, Ljava/util/ArrayList;

    const/4 v3, 0x5

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/twitter/library/client/AbsFragmentActivity;->j:Ljava/util/ArrayList;

    goto :goto_2

    :cond_6
    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/ToolBar;->setVisibility(I)V

    goto :goto_3

    :cond_7
    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/ToolBar;->setVisibility(I)V

    iput-object v6, p0, Lcom/twitter/library/client/AbsFragmentActivity;->b:Lcom/twitter/internal/android/widget/ToolBar;

    goto :goto_3

    :cond_8
    invoke-virtual {v1, v4}, Lcom/twitter/internal/android/widget/ToolBar;->setVisibility(I)V

    goto/16 :goto_1

    :cond_9
    invoke-virtual {v1, v4}, Lcom/twitter/internal/android/widget/ToolBar;->setVisibility(I)V

    iput-object v6, p0, Lcom/twitter/library/client/AbsFragmentActivity;->c:Lcom/twitter/internal/android/widget/ToolBar;

    goto/16 :goto_1
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    invoke-virtual {p0, p1}, Lcom/twitter/library/client/AbsFragmentActivity;->setIntent(Landroid/content/Intent;)V

    invoke-direct {p0, p1}, Lcom/twitter/library/client/AbsFragmentActivity;->b(Landroid/content/Intent;)V

    return-void
.end method

.method protected onPause()V
    .locals 3

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onPause()V

    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->a:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/z;

    iget-object v2, p0, Lcom/twitter/library/client/AbsFragmentActivity;->k:Lcom/twitter/library/client/aa;

    invoke-virtual {v2, v0}, Lcom/twitter/library/client/aa;->b(Lcom/twitter/library/client/z;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 3

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->m:Lcom/twitter/library/client/e;

    iget-boolean v0, v0, Lcom/twitter/library/client/e;->j:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->k:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/library/client/AbsFragmentActivity;->O()V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->a:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/z;

    iget-object v2, p0, Lcom/twitter/library/client/AbsFragmentActivity;->k:Lcom/twitter/library/client/aa;

    invoke-virtual {v2, v0}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/z;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "pending_reqs"

    iget-object v1, p0, Lcom/twitter/library/client/AbsFragmentActivity;->j:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStart()V

    invoke-static {}, Lcom/twitter/library/client/l;->b()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->n:Z

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStop()V

    invoke-static {}, Lcom/twitter/library/client/l;->c()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->n:Z

    return-void
.end method

.method protected onTitleChanged(Ljava/lang/CharSequence;I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->b:Lcom/twitter/internal/android/widget/ToolBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->b:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/ToolBar;->setTitle(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->onTitleChanged(Ljava/lang/CharSequence;I)V

    goto :goto_0
.end method

.method protected s_()Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->m:Lcom/twitter/library/client/e;

    iget-boolean v0, v0, Lcom/twitter/library/client/e;->l:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/client/AbsFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/client/AbsFragmentActivity;->h:Landroid/content/Intent;

    goto :goto_0
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/twitter/library/client/AbsFragmentActivity;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    invoke-super {p0, v0, p2}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    invoke-direct {p0, p1}, Lcom/twitter/library/client/AbsFragmentActivity;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    invoke-super {p0, v0, p2, p3}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    return-void
.end method

.method public startActivityFromFragment(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V
    .locals 1

    invoke-direct {p0, p2}, Lcom/twitter/library/client/AbsFragmentActivity;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    invoke-super {p0, p1, v0, p3}, Landroid/support/v4/app/FragmentActivity;->startActivityFromFragment(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V

    return-void
.end method
