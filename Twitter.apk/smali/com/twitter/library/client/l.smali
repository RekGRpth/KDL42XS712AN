.class public Lcom/twitter/library/client/l;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:I

.field private static b:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/twitter/library/client/l;->b:Ljava/util/ArrayList;

    return-void
.end method

.method public static a(Lcom/twitter/library/client/k;)V
    .locals 1

    sget-object v0, Lcom/twitter/library/client/l;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/twitter/library/client/l;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public static a()Z
    .locals 1

    sget v0, Lcom/twitter/library/client/l;->a:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()V
    .locals 2

    sget v0, Lcom/twitter/library/client/l;->a:I

    if-nez v0, :cond_0

    sget-object v0, Lcom/twitter/library/client/l;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    sget-object v0, Lcom/twitter/library/client/l;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/k;

    invoke-interface {v0}, Lcom/twitter/library/client/k;->b()V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    sget v0, Lcom/twitter/library/client/l;->a:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/twitter/library/client/l;->a:I

    return-void
.end method

.method public static b(Lcom/twitter/library/client/k;)V
    .locals 1

    sget-object v0, Lcom/twitter/library/client/l;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public static c()V
    .locals 2

    sget v0, Lcom/twitter/library/client/l;->a:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/twitter/library/client/l;->a:I

    sget v0, Lcom/twitter/library/client/l;->a:I

    if-nez v0, :cond_0

    sget-object v0, Lcom/twitter/library/client/l;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    sget-object v0, Lcom/twitter/library/client/l;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/k;

    invoke-interface {v0}, Lcom/twitter/library/client/k;->a()V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method
