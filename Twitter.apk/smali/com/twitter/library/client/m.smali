.class public Lcom/twitter/library/client/m;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field static final a:Z


# instance fields
.field private final b:Landroid/os/Handler;

.field private final c:Landroid/content/Context;

.field private d:Ljava/lang/ref/WeakReference;

.field private e:Lcom/twitter/library/client/r;

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:J

.field private final n:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Lcom/twitter/library/client/App;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "AutoRefresher"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/library/client/m;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Landroid/content/Context;II)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/library/client/m;->b:Landroid/os/Handler;

    new-instance v0, Lcom/twitter/library/client/n;

    invoke-direct {v0, p0}, Lcom/twitter/library/client/n;-><init>(Lcom/twitter/library/client/m;)V

    iput-object v0, p0, Lcom/twitter/library/client/m;->n:Ljava/lang/Runnable;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/client/m;->c:Landroid/content/Context;

    iput p3, p0, Lcom/twitter/library/client/m;->j:I

    iput p2, p0, Lcom/twitter/library/client/m;->i:I

    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/twitter/library/client/m;->m:J

    iput v2, p0, Lcom/twitter/library/client/m;->k:I

    iput-boolean v2, p0, Lcom/twitter/library/client/m;->g:Z

    const v0, 0x7fffffff

    iput v0, p0, Lcom/twitter/library/client/m;->l:I

    return-void
.end method

.method private h()V
    .locals 6

    iget-boolean v0, p0, Lcom/twitter/library/client/m;->f:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/library/client/m;->h:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/twitter/library/client/m;->a:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "AutoRefresher"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "schedule() mCanAutoRefresh: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/twitter/library/client/m;->g:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/client/m;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/library/client/m;->n:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/twitter/library/client/m;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/library/client/m;->n:Ljava/lang/Runnable;

    iget v2, p0, Lcom/twitter/library/client/m;->j:I

    int-to-long v2, v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    return-void
.end method

.method private i()V
    .locals 3

    sget-boolean v0, Lcom/twitter/library/client/m;->a:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "AutoRefresher"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "cancel() mCanAutoRefresh: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/twitter/library/client/m;->g:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/client/m;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/library/client/m;->n:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method private j()V
    .locals 5

    const/4 v4, 0x0

    sget-boolean v0, Lcom/twitter/library/client/m;->a:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "AutoRefresher"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "RESET(----): mCanStart:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/twitter/library/client/m;->g:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " marker: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/library/client/m;->m:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " numItems: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/twitter/library/client/m;->k:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-boolean v4, p0, Lcom/twitter/library/client/m;->g:Z

    iput v4, p0, Lcom/twitter/library/client/m;->k:I

    const v0, 0x7fffffff

    iput v0, p0, Lcom/twitter/library/client/m;->l:I

    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/twitter/library/client/m;->m:J

    return-void
.end method

.method private k()Lcom/twitter/library/service/a;
    .locals 5

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/library/client/m;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/o;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/twitter/library/client/o;->aA()Lcom/twitter/library/service/c;

    move-result-object v0

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_1

    new-instance v0, Lcom/twitter/library/service/k;

    invoke-direct {v0}, Lcom/twitter/library/service/k;-><init>()V

    new-instance v3, Lcom/twitter/library/service/d;

    const/4 v4, 0x0

    invoke-direct {v3, v4, v1}, Lcom/twitter/library/service/d;-><init>(ILcom/twitter/library/service/c;)V

    invoke-virtual {v0, v3}, Lcom/twitter/library/service/k;->a(Lcom/twitter/library/service/a;)V

    new-instance v1, Lcom/twitter/library/client/p;

    invoke-direct {v1, p0, v2}, Lcom/twitter/library/client/p;-><init>(Lcom/twitter/library/client/m;Lcom/twitter/library/client/n;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/k;->a(Lcom/twitter/library/service/a;)V

    :goto_1
    return-object v0

    :cond_0
    move-object v1, v2

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/twitter/library/client/p;

    invoke-direct {v0, p0, v2}, Lcom/twitter/library/client/p;-><init>(Lcom/twitter/library/client/m;Lcom/twitter/library/client/n;)V

    goto :goto_1
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/client/m;->i:I

    return v0
.end method

.method public a(Lcom/twitter/library/client/o;)Lcom/twitter/library/client/m;
    .locals 1

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/library/client/m;->d:Ljava/lang/ref/WeakReference;

    return-object p0
.end method

.method public a(Lcom/twitter/library/client/r;)Lcom/twitter/library/client/m;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/client/m;->e:Lcom/twitter/library/client/r;

    return-object p0
.end method

.method public a(Z)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/client/m;->f:Z

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/twitter/library/client/m;->j()V

    :cond_0
    invoke-direct {p0}, Lcom/twitter/library/client/m;->i()V

    return-void
.end method

.method a(ZJI)V
    .locals 4

    if-eqz p1, :cond_0

    const v0, 0x7fffffff

    iput v0, p0, Lcom/twitter/library/client/m;->l:I

    iget-boolean v0, p0, Lcom/twitter/library/client/m;->g:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/library/client/m;->k:I

    add-int/2addr v0, p4

    iput v0, p0, Lcom/twitter/library/client/m;->k:I

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-lez v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/library/client/m;->m:J

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iput-wide p2, p0, Lcom/twitter/library/client/m;->m:J

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/client/m;->h:Z

    invoke-direct {p0}, Lcom/twitter/library/client/m;->h()V

    sget-boolean v0, Lcom/twitter/library/client/m;->a:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "AutoRefresher"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onRefreshComplete() success:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " marker: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " New: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " total: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/twitter/library/client/m;->k:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/client/m;->k:I

    return v0
.end method

.method b(Z)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/library/client/m;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/o;

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/twitter/library/client/m;->i()V

    invoke-direct {p0}, Lcom/twitter/library/client/m;->j()V

    :cond_0
    iget-boolean v1, p0, Lcom/twitter/library/client/m;->h:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/twitter/library/client/m;->e:Lcom/twitter/library/client/r;

    if-eqz v1, :cond_4

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/twitter/library/client/o;->B()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    if-nez v0, :cond_4

    if-eqz p1, :cond_4

    :cond_2
    iput-boolean p1, p0, Lcom/twitter/library/client/m;->g:Z

    const v0, 0x7fffffff

    iput v0, p0, Lcom/twitter/library/client/m;->l:I

    sget-boolean v0, Lcom/twitter/library/client/m;->a:Z

    if-eqz v0, :cond_3

    if-eqz p1, :cond_6

    const-string/jumbo v0, ""

    move-object v1, v0

    :goto_0
    if-eqz p1, :cond_7

    const-string/jumbo v0, "true"

    :goto_1
    const-string/jumbo v2, "AutoRefresher"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "performRefresh("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, ") polling: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " marker: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v3, p0, Lcom/twitter/library/client/m;->m:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/client/m;->h:Z

    iget-object v0, p0, Lcom/twitter/library/client/m;->e:Lcom/twitter/library/client/r;

    invoke-virtual {v0, p1}, Lcom/twitter/library/client/r;->c(Z)Lcom/twitter/library/service/b;

    iget-object v0, p0, Lcom/twitter/library/client/m;->e:Lcom/twitter/library/client/r;

    invoke-virtual {v0}, Lcom/twitter/library/client/r;->e()V

    iget-object v0, p0, Lcom/twitter/library/client/m;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/client/w;->a(Landroid/content/Context;)Lcom/twitter/library/client/w;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/client/m;->e:Lcom/twitter/library/client/r;

    invoke-direct {p0}, Lcom/twitter/library/client/m;->k()Lcom/twitter/library/service/a;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/w;->a(Lcom/twitter/library/service/b;Lcom/twitter/library/service/a;)Ljava/lang/String;

    :cond_4
    iget-boolean v0, p0, Lcom/twitter/library/client/m;->h:Z

    if-nez v0, :cond_5

    invoke-direct {p0}, Lcom/twitter/library/client/m;->h()V

    sget-boolean v0, Lcom/twitter/library/client/m;->a:Z

    if-eqz v0, :cond_5

    const-string/jumbo v0, "AutoRefresher"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "refreshTimeline(!!!!) !!FAILED!! polling: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    return-void

    :cond_6
    const-string/jumbo v0, "*******"

    move-object v1, v0

    goto :goto_0

    :cond_7
    const-string/jumbo v0, "=FALSE="

    goto :goto_1
.end method

.method public c()Lcom/twitter/library/client/r;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/m;->e:Lcom/twitter/library/client/r;

    return-object v0
.end method

.method public d()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/library/client/m;->b(Z)V

    return-void
.end method

.method public e()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/client/m;->f:Z

    invoke-direct {p0}, Lcom/twitter/library/client/m;->h()V

    return-void
.end method

.method public f()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/library/client/m;->i()V

    invoke-direct {p0}, Lcom/twitter/library/client/m;->j()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/client/m;->f:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/client/m;->e:Lcom/twitter/library/client/r;

    iget-object v0, p0, Lcom/twitter/library/client/m;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    return-void
.end method

.method public g()V
    .locals 6

    iget-object v0, p0, Lcom/twitter/library/client/m;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/o;

    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lcom/twitter/library/client/m;->g:Z

    if-eqz v1, :cond_1

    iget-wide v1, p0, Lcom/twitter/library/client/m;->m:J

    const-wide v3, 0x7fffffffffffffffL

    cmp-long v1, v1, v3

    if-eqz v1, :cond_1

    invoke-interface {v0}, Lcom/twitter/library/client/o;->az()I

    move-result v1

    iget v2, p0, Lcom/twitter/library/client/m;->l:I

    if-eq v1, v2, :cond_0

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    invoke-interface {v0, v1}, Lcom/twitter/library/client/o;->f(I)J

    move-result-wide v2

    iget-wide v4, p0, Lcom/twitter/library/client/m;->m:J

    cmp-long v0, v4, v2

    if-gtz v0, :cond_2

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/library/client/m;->b(Z)V

    :cond_0
    :goto_0
    sget-boolean v0, Lcom/twitter/library/client/m;->a:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "AutoRefresher"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "markPosition() lastPos: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/twitter/library/client/m;->l:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " currPos: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void

    :cond_2
    iput v1, p0, Lcom/twitter/library/client/m;->l:I

    goto :goto_0
.end method
