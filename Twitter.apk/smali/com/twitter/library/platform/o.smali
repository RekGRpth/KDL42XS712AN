.class Lcom/twitter/library/platform/o;
.super Lcom/twitter/library/platform/h;
.source "Twttr"

# interfaces
.implements Landroid/location/LocationListener;


# static fields
.field private static c:Lcom/twitter/library/platform/o;


# instance fields
.field private d:Ljava/util/List;

.field private e:Landroid/location/LocationManager;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/twitter/library/platform/i;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/platform/h;-><init>(Landroid/content/Context;Lcom/twitter/library/platform/i;)V

    const-string/jumbo v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/twitter/library/platform/o;->e:Landroid/location/LocationManager;

    invoke-virtual {p0}, Lcom/twitter/library/platform/o;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/o;->d:Ljava/util/List;

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;Lcom/twitter/library/platform/i;)Lcom/twitter/library/platform/o;
    .locals 2

    const-class v1, Lcom/twitter/library/platform/o;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/platform/o;->c:Lcom/twitter/library/platform/o;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/library/platform/o;

    invoke-direct {v0, p0, p1}, Lcom/twitter/library/platform/o;-><init>(Landroid/content/Context;Lcom/twitter/library/platform/i;)V

    sput-object v0, Lcom/twitter/library/platform/o;->c:Lcom/twitter/library/platform/o;

    :cond_0
    sget-object v0, Lcom/twitter/library/platform/o;->c:Lcom/twitter/library/platform/o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Landroid/location/Location;Landroid/location/Location;)Z
    .locals 5

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Landroid/location/Location;->getTime()J

    move-result-wide v1

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v3

    sub-long/2addr v1, v3

    const-wide/16 v3, 0x3e8

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    invoke-virtual {p2}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected a()Ljava/util/List;
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/platform/o;->e:Landroid/location/LocationManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getProviders(Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public b()Landroid/location/Location;
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/platform/o;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :try_start_0
    iget-object v3, p0, Lcom/twitter/library/platform/o;->e:Landroid/location/LocationManager;

    invoke-virtual {v3, v0}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lcom/twitter/library/platform/o;->a(Landroid/location/Location;Landroid/location/Location;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    move-object v1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_1

    :cond_0
    return-object v1

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public c()V
    .locals 7

    invoke-virtual {p0}, Lcom/twitter/library/platform/o;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/platform/o;->e:Landroid/location/LocationManager;

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public d()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/platform/o;->e:Landroid/location/LocationManager;

    invoke-virtual {v0, p0}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/platform/o;->b:Lcom/twitter/library/platform/i;

    invoke-interface {v0, p1}, Lcom/twitter/library/platform/i;->a(Landroid/location/Location;)V

    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0

    return-void
.end method
