.class public Lcom/twitter/library/platform/PushService;
.super Lcom/google/android/gcm/GCMBaseIntentService;
.source "Twttr"


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field public static final d:Ljava/lang/String;

.field public static final e:Ljava/lang/String;

.field private static final f:Z

.field private static final g:Ljava/util/HashMap;

.field private static final h:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private static i:Z

.field private static j:Z

.field private static k:Ljava/lang/String;

.field private static final l:Landroid/os/Handler;

.field private static m:Z

.field private static n:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/16 v5, 0xb

    const/4 v4, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string/jumbo v0, ".c2dm.add"

    invoke-static {v0}, Lcom/twitter/library/client/App;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/platform/PushService;->a:Ljava/lang/String;

    const-string/jumbo v0, ".c2dm.update"

    invoke-static {v0}, Lcom/twitter/library/client/App;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/platform/PushService;->b:Ljava/lang/String;

    const-string/jumbo v0, ".c2dm.del"

    invoke-static {v0}, Lcom/twitter/library/client/App;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/platform/PushService;->c:Ljava/lang/String;

    const-string/jumbo v0, ".c2dm.registered"

    invoke-static {v0}, Lcom/twitter/library/client/App;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/platform/PushService;->d:Ljava/lang/String;

    const-string/jumbo v0, ".c2dm.error"

    invoke-static {v0}, Lcom/twitter/library/client/App;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/platform/PushService;->e:Ljava/lang/String;

    invoke-static {}, Lcom/twitter/library/client/App;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "PushService"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/twitter/library/platform/PushService;->f:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v5}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/twitter/library/platform/PushService;->g:Ljava/util/HashMap;

    const-string/jumbo v0, ""

    sput-object v0, Lcom/twitter/library/platform/PushService;->k:Ljava/lang/String;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/twitter/library/platform/PushService;->l:Landroid/os/Handler;

    sput-boolean v2, Lcom/twitter/library/platform/PushService;->m:Z

    const-string/jumbo v0, ""

    sput-object v0, Lcom/twitter/library/platform/PushService;->n:Ljava/lang/String;

    sget-object v0, Lcom/twitter/library/platform/PushService;->g:Ljava/util/HashMap;

    const-string/jumbo v3, "tweet"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/platform/PushService;->g:Ljava/util/HashMap;

    const-string/jumbo v1, "mention"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/platform/PushService;->g:Ljava/util/HashMap;

    const-string/jumbo v1, "direct_message"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/platform/PushService;->g:Ljava/util/HashMap;

    const-string/jumbo v1, "favorited"

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/platform/PushService;->g:Ljava/util/HashMap;

    const-string/jumbo v1, "retweeted"

    const/4 v3, 0x5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/platform/PushService;->g:Ljava/util/HashMap;

    const-string/jumbo v1, "followed"

    const/4 v3, 0x6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/platform/PushService;->g:Ljava/util/HashMap;

    const-string/jumbo v1, "followed_request"

    const/4 v3, 0x7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/platform/PushService;->g:Ljava/util/HashMap;

    const-string/jumbo v1, "login_verification_request"

    const/16 v3, 0x8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/platform/PushService;->g:Ljava/util/HashMap;

    const-string/jumbo v1, "generic"

    const/16 v3, 0x9

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/platform/PushService;->g:Ljava/util/HashMap;

    const-string/jumbo v1, "lifeline"

    const/16 v3, 0xa

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/platform/PushService;->g:Ljava/util/HashMap;

    const-string/jumbo v1, "media_tagged"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/twitter/library/platform/PushService;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void

    :cond_0
    move v0, v2

    goto/16 :goto_0
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "49625052041"

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/gcm/GCMBaseIntentService;-><init>([Ljava/lang/String;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/accounts/Account;I)I
    .locals 8

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-static {p0, p1}, Lcom/twitter/library/platform/PushService;->d(Landroid/content/Context;Landroid/accounts/Account;)Z

    move-result v7

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-eqz p2, :cond_1

    move v1, v6

    :goto_0
    invoke-static {v0, p1, v1}, Lcom/twitter/library/platform/PushService;->a(Landroid/content/Context;Landroid/accounts/Account;Z)V

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/twitter/library/util/a;->b(Landroid/accounts/AccountManager;Landroid/accounts/Account;)Lcom/twitter/library/network/OAuthToken;

    move-result-object v2

    invoke-static {v0}, Lcom/google/android/gcm/a;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    move-object v1, p1

    move v4, p2

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/platform/PushService;->a(Landroid/content/Context;Landroid/accounts/Account;Lcom/twitter/library/network/OAuthToken;Ljava/lang/String;IZ)I

    move-result v1

    if-ne v1, v6, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, p1, v2, v3}, Lcom/twitter/library/platform/PushService;->a(Landroid/content/Context;Landroid/accounts/Account;J)V

    :cond_0
    if-eqz v7, :cond_2

    new-instance v2, Landroid/content/Intent;

    sget-object v3, Lcom/twitter/library/platform/PushService;->b:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "push_return_code"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v2

    sget-object v3, Lcom/twitter/library/provider/w;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    :goto_1
    return v1

    :cond_1
    move v1, v5

    goto :goto_0

    :cond_2
    new-instance v2, Landroid/content/Intent;

    sget-object v3, Lcom/twitter/library/platform/PushService;->a:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "push_return_code"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v2

    sget-object v3, Lcom/twitter/library/provider/w;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;Landroid/accounts/Account;Lcom/twitter/library/network/OAuthToken;Ljava/lang/String;IZ)I
    .locals 13

    const/4 v2, 0x3

    if-eqz p2, :cond_5

    invoke-static {p0}, Lcom/twitter/library/network/aa;->a(Landroid/content/Context;)Lcom/twitter/library/network/aa;

    move-result-object v1

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string/jumbo v5, "account"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "push_destinations"

    aput-object v5, v3, v4

    invoke-virtual {v1, v3}, Lcom/twitter/library/network/aa;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, ".json"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "udid"

    invoke-static {p0}, Lcom/twitter/library/platform/PushService;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v3, "enabled_for"

    move/from16 v0, p4

    invoke-static {v1, v3, v0}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    const-string/jumbo v3, "app_version"

    const/4 v4, 0x3

    invoke-static {v1, v3, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    invoke-static {v1}, Lcom/twitter/library/platform/PushService;->a(Ljava/lang/StringBuilder;)V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    if-eqz v3, :cond_0

    const-string/jumbo v4, "lang"

    invoke-static {v3}, Lcom/twitter/library/util/Util;->b(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v4, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    if-eqz p3, :cond_1

    const-string/jumbo v3, "token"

    move-object/from16 v0, p3

    invoke-static {v1, v3, v0}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const/16 v3, 0x31

    invoke-static {v3}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move/from16 v0, p5

    invoke-static {p0, v4, p2, v0, v3}, Lcom/twitter/library/platform/PushService;->a(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/network/OAuthToken;ZLcom/twitter/internal/network/i;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v4

    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/twitter/library/platform/PushService;->k:Ljava/lang/String;

    :cond_2
    invoke-virtual {v3}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4}, Lcom/twitter/internal/network/HttpOperation;->k()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v4}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v3

    iget v3, v3, Lcom/twitter/internal/network/k;->a:I

    const/16 v5, 0x130

    if-ne v3, v5, :cond_4

    :cond_3
    const/4 v2, 0x0

    sget-object v3, Lcom/twitter/library/provider/NotificationSetting;->a:Lcom/twitter/library/provider/NotificationSetting;

    move/from16 v0, p4

    invoke-virtual {v3, v0}, Lcom/twitter/library/provider/NotificationSetting;->e(I)I

    move-result v3

    or-int/2addr v2, v3

    sget-object v3, Lcom/twitter/library/provider/NotificationSetting;->c:Lcom/twitter/library/provider/NotificationSetting;

    move/from16 v0, p4

    invoke-virtual {v3, v0}, Lcom/twitter/library/provider/NotificationSetting;->e(I)I

    move-result v3

    or-int/2addr v2, v3

    sget-object v3, Lcom/twitter/library/provider/NotificationSetting;->b:Lcom/twitter/library/provider/NotificationSetting;

    move/from16 v0, p4

    invoke-virtual {v3, v0}, Lcom/twitter/library/provider/NotificationSetting;->e(I)I

    move-result v3

    or-int/2addr v2, v3

    sget-object v3, Lcom/twitter/library/provider/NotificationSetting;->d:Lcom/twitter/library/provider/NotificationSetting;

    move/from16 v0, p4

    invoke-virtual {v3, v0}, Lcom/twitter/library/provider/NotificationSetting;->e(I)I

    move-result v3

    or-int/2addr v2, v3

    sget-object v3, Lcom/twitter/library/provider/NotificationSetting;->h:Lcom/twitter/library/provider/NotificationSetting;

    move/from16 v0, p4

    invoke-virtual {v3, v0}, Lcom/twitter/library/provider/NotificationSetting;->e(I)I

    move-result v3

    or-int/2addr v2, v3

    sget-object v3, Lcom/twitter/library/provider/NotificationSetting;->l:Lcom/twitter/library/provider/NotificationSetting;

    move/from16 v0, p4

    invoke-virtual {v3, v0}, Lcom/twitter/library/provider/NotificationSetting;->e(I)I

    move-result v3

    or-int/2addr v2, v3

    sget-object v3, Lcom/twitter/library/provider/NotificationSetting;->m:Lcom/twitter/library/provider/NotificationSetting;

    move/from16 v0, p4

    invoke-virtual {v3, v0}, Lcom/twitter/library/provider/NotificationSetting;->e(I)I

    move-result v3

    or-int/2addr v2, v3

    sget-object v3, Lcom/twitter/library/provider/NotificationSetting;->e:Lcom/twitter/library/provider/NotificationSetting;

    move/from16 v0, p4

    invoke-virtual {v3, v0}, Lcom/twitter/library/provider/NotificationSetting;->e(I)I

    move-result v3

    sget-object v5, Lcom/twitter/library/provider/NotificationSetting;->g:Lcom/twitter/library/provider/NotificationSetting;

    move/from16 v0, p4

    invoke-virtual {v5, v0}, Lcom/twitter/library/provider/NotificationSetting;->e(I)I

    move-result v5

    sget-object v6, Lcom/twitter/library/provider/NotificationSetting;->i:Lcom/twitter/library/provider/NotificationSetting;

    move/from16 v0, p4

    invoke-virtual {v6, v0}, Lcom/twitter/library/provider/NotificationSetting;->e(I)I

    move-result v6

    sget-object v7, Lcom/twitter/library/provider/NotificationSetting;->f:Lcom/twitter/library/provider/NotificationSetting;

    move/from16 v0, p4

    invoke-virtual {v7, v0}, Lcom/twitter/library/provider/NotificationSetting;->e(I)I

    move-result v7

    sget-object v8, Lcom/twitter/library/provider/NotificationSetting;->j:Lcom/twitter/library/provider/NotificationSetting;

    move/from16 v0, p4

    invoke-virtual {v8, v0}, Lcom/twitter/library/provider/NotificationSetting;->e(I)I

    move-result v8

    sget-object v9, Lcom/twitter/library/provider/NotificationSetting;->k:Lcom/twitter/library/provider/NotificationSetting;

    move/from16 v0, p4

    invoke-virtual {v9, v0}, Lcom/twitter/library/provider/NotificationSetting;->e(I)I

    move-result v9

    sget-object v10, Lcom/twitter/library/provider/NotificationSetting;->n:Lcom/twitter/library/provider/NotificationSetting;

    move/from16 v0, p4

    invoke-virtual {v10, v0}, Lcom/twitter/library/provider/NotificationSetting;->e(I)I

    move-result v10

    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v12, "notif_mention"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v11, v12, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v2, "notif_message"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v2, "notif_tweet"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v2, "notif_experimental"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v2, "notif_lifeline_alerts"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v2, "notif_recommendations"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v2, "notif_news"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v2, "notif_vit_notable_event"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {p0}, Lcom/twitter/library/provider/f;->a(Landroid/content/Context;)Lcom/twitter/library/provider/f;

    move-result-object v2

    iget-object v3, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v11, v5}, Lcom/twitter/library/provider/f;->a(Ljava/lang/String;Landroid/content/ContentValues;Z)I

    const/4 v2, 0x1

    :cond_4
    invoke-virtual {v4}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v3

    iget v3, v3, Lcom/twitter/internal/network/k;->a:I

    const/16 v4, 0x191

    if-ne v3, v4, :cond_5

    const-string/jumbo v3, "Too many devices"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v2, 0x2

    :cond_5
    return v2
.end method

.method public static a(Landroid/content/Context;Landroid/accounts/Account;)J
    .locals 4

    invoke-static {p0, p1}, Lcom/twitter/library/platform/PushService;->f(Landroid/content/Context;Landroid/accounts/Account;)Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v1, "last_refresh."

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/network/OAuthToken;Z)Lcom/twitter/internal/network/HttpOperation;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/twitter/library/platform/PushService;->a(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/network/OAuthToken;ZLcom/twitter/internal/network/i;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/network/OAuthToken;ZLcom/twitter/internal/network/i;)Lcom/twitter/internal/network/HttpOperation;
    .locals 2

    new-instance v0, Lcom/twitter/library/network/d;

    invoke-direct {v0, p0, p1}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    sget-object v1, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/twitter/library/network/d;->a(Z)Lcom/twitter/library/network/d;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/n;

    invoke-direct {v1, p2}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    return-object v0
.end method

.method static a(Landroid/os/Bundle;Lcom/twitter/library/platform/notifications/b;Landroid/content/SharedPreferences;)Lcom/twitter/library/platform/q;
    .locals 9

    const/4 v8, 0x0

    const/4 v2, 0x0

    const-string/jumbo v0, "users"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string/jumbo v0, "users"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lcom/twitter/library/platform/PushService;->f:Z

    if-eqz v1, :cond_0

    :try_start_0
    const-string/jumbo v1, "PushService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Notification Payload - users: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    invoke-static {v0, p1}, Lcom/twitter/library/platform/notifications/c;->b(Ljava/lang/String;Lcom/twitter/library/util/w;)Lcom/twitter/library/platform/notifications/g;

    move-result-object v0

    move-object v3, v0

    :goto_1
    if-eqz v3, :cond_1

    iget-object v0, v3, Lcom/twitter/library/platform/notifications/g;->a:Lcom/twitter/library/platform/notifications/f;

    if-nez v0, :cond_6

    :cond_1
    if-nez v3, :cond_5

    const-string/jumbo v0, "Missing users"

    :goto_2
    if-eqz p1, :cond_2

    invoke-virtual {p1, v0}, Lcom/twitter/library/platform/notifications/b;->a(Ljava/lang/String;)V

    :cond_2
    sget-boolean v1, Lcom/twitter/library/platform/PushService;->f:Z

    if-eqz v1, :cond_3

    const-string/jumbo v1, "PushService"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    :goto_3
    return-object v2

    :cond_4
    move-object v3, v2

    goto :goto_1

    :cond_5
    const-string/jumbo v0, "Missing recipient"

    goto :goto_2

    :cond_6
    if-eqz p1, :cond_7

    iget-object v0, v3, Lcom/twitter/library/platform/notifications/g;->a:Lcom/twitter/library/platform/notifications/f;

    iget-wide v0, v0, Lcom/twitter/library/platform/notifications/f;->a:J

    iput-wide v0, p1, Lcom/twitter/library/platform/notifications/b;->a:J

    :cond_7
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v0, "recipient_name"

    iget-object v1, v3, Lcom/twitter/library/platform/notifications/g;->a:Lcom/twitter/library/platform/notifications/f;

    iget-object v1, v1, Lcom/twitter/library/platform/notifications/f;->b:Ljava/lang/String;

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "priority"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    const-string/jumbo v0, "priority"

    const-string/jumbo v1, "priority"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    :goto_4
    const-string/jumbo v0, "tweet"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    const-string/jumbo v0, "tweet"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lcom/twitter/library/platform/PushService;->f:Z

    if-eqz v1, :cond_9

    :try_start_1
    const-string/jumbo v1, "PushService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Notification Payload - tweet: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_9
    :goto_5
    invoke-static {v0, p1}, Lcom/twitter/library/platform/notifications/c;->a(Ljava/lang/String;Lcom/twitter/library/util/w;)Lcom/twitter/library/platform/notifications/e;

    move-result-object v0

    move-object v1, v0

    :goto_6
    const-string/jumbo v0, "text"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    const-string/jumbo v0, "text"

    const-string/jumbo v5, "text"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :goto_7
    const-string/jumbo v0, "title"

    const-string/jumbo v5, "title"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "scribe_target"

    const-string/jumbo v5, "scribe_target"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "collapse_key"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    sget-object v0, Lcom/twitter/library/platform/PushService;->g:Ljava/util/HashMap;

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_a

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const/16 v7, 0x9

    if-ne v6, v7, :cond_13

    :cond_a
    const-string/jumbo v0, "collapse_key"

    const-string/jumbo v2, "generic"

    invoke-virtual {v4, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "notification_setting_key"

    invoke-virtual {v4, v0, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "recipient_id"

    iget-object v2, v3, Lcom/twitter/library/platform/notifications/g;->a:Lcom/twitter/library/platform/notifications/f;

    iget-wide v6, v2, Lcom/twitter/library/platform/notifications/f;->a:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "uri"

    const-string/jumbo v2, "uri"

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v3, Lcom/twitter/library/platform/notifications/g;->b:Lcom/twitter/library/platform/notifications/f;

    if-eqz v0, :cond_b

    const-string/jumbo v0, "sender_id"

    iget-object v2, v3, Lcom/twitter/library/platform/notifications/g;->b:Lcom/twitter/library/platform/notifications/f;

    iget-wide v6, v2, Lcom/twitter/library/platform/notifications/f;->a:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    if-eqz v1, :cond_c

    const-string/jumbo v0, "status_id"

    iget-wide v6, v1, Lcom/twitter/library/platform/notifications/e;->a:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "_opt_out_count"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-interface {p2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v5, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :goto_8
    new-instance v2, Lcom/twitter/library/platform/q;

    invoke-direct {v2}, Lcom/twitter/library/platform/q;-><init>()V

    iput-object v1, v2, Lcom/twitter/library/platform/q;->a:Lcom/twitter/library/platform/notifications/e;

    iput-object v3, v2, Lcom/twitter/library/platform/q;->b:Lcom/twitter/library/platform/notifications/g;

    iput-object v4, v2, Lcom/twitter/library/platform/q;->d:Landroid/os/Bundle;

    goto/16 :goto_3

    :cond_d
    const-string/jumbo v0, "priority"

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "Missing priority"

    if-eqz p1, :cond_e

    const-string/jumbo v0, "Missing priority"

    invoke-virtual {p1, v0}, Lcom/twitter/library/platform/notifications/b;->a(Ljava/lang/String;)V

    :cond_e
    sget-boolean v0, Lcom/twitter/library/platform/PushService;->f:Z

    if-eqz v0, :cond_8

    const-string/jumbo v0, "PushService"

    const-string/jumbo v1, "Missing priority"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_f
    move-object v1, v2

    goto/16 :goto_6

    :cond_10
    if-eqz v1, :cond_11

    const-string/jumbo v0, "text"

    iget-object v5, v1, Lcom/twitter/library/platform/notifications/e;->d:Ljava/lang/String;

    invoke-virtual {v4, v0, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    :cond_11
    const-string/jumbo v0, "Missing text"

    if-eqz p1, :cond_12

    const-string/jumbo v0, "Missing text"

    invoke-virtual {p1, v0}, Lcom/twitter/library/platform/notifications/b;->a(Ljava/lang/String;)V

    :cond_12
    sget-boolean v0, Lcom/twitter/library/platform/PushService;->f:Z

    if-eqz v0, :cond_3

    const-string/jumbo v0, "PushService"

    const-string/jumbo v1, "Missing text"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_13
    const-string/jumbo v6, "collapse_key"

    invoke-virtual {v4, v6, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v6, "recipient_id"

    iget-object v7, v3, Lcom/twitter/library/platform/notifications/g;->a:Lcom/twitter/library/platform/notifications/f;

    iget-wide v7, v7, Lcom/twitter/library/platform/notifications/f;->a:J

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v1, :cond_14

    const-string/jumbo v6, "status_id"

    iget-wide v7, v1, Lcom/twitter/library/platform/notifications/e;->a:J

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :goto_9
    iget-object v6, v3, Lcom/twitter/library/platform/notifications/g;->b:Lcom/twitter/library/platform/notifications/f;

    if-eqz v6, :cond_15

    const-string/jumbo v0, "sender_name"

    iget-object v2, v3, Lcom/twitter/library/platform/notifications/g;->b:Lcom/twitter/library/platform/notifications/f;

    iget-object v2, v2, Lcom/twitter/library/platform/notifications/f;->b:Ljava/lang/String;

    invoke-virtual {v4, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    :cond_14
    const-string/jumbo v6, "status_id"

    const-string/jumbo v7, "0"

    invoke-virtual {v4, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_9

    :cond_15
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v6, 0x8

    if-ne v0, v6, :cond_16

    const-string/jumbo v0, "sender_name"

    const-string/jumbo v2, ""

    invoke-virtual {v4, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    :cond_16
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Missing sender for collapse_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz p1, :cond_17

    invoke-virtual {p1, v0}, Lcom/twitter/library/platform/notifications/b;->a(Ljava/lang/String;)V

    :cond_17
    sget-boolean v1, Lcom/twitter/library/platform/PushService;->f:Z

    if-eqz v1, :cond_3

    const-string/jumbo v1, "PushService"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :catch_0
    move-exception v1

    goto/16 :goto_5

    :catch_1
    move-exception v1

    goto/16 :goto_0
.end method

.method public static a()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/twitter/library/platform/PushService;->k:Ljava/lang/String;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/accounts/Account;J)V
    .locals 2

    invoke-static {p0, p1}, Lcom/twitter/library/platform/PushService;->f(Landroid/content/Context;Landroid/accounts/Account;)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v1, "last_refresh."

    invoke-virtual {v0, v1, p2, p3}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;J)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/accounts/Account;Z)V
    .locals 2

    invoke-static {p0, p1}, Lcom/twitter/library/platform/PushService;->f(Landroid/content/Context;Landroid/accounts/Account;)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v1, "enabled"

    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;Z)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V

    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/content/SharedPreferences;)V
    .locals 10

    const/4 v1, 0x0

    invoke-interface {p1}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    sget-object v2, Lcom/twitter/library/util/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    if-eqz v2, :cond_0

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    new-instance v5, Lcom/twitter/library/client/f;

    iget-object v6, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string/jumbo v7, "c2dm"

    invoke-direct {v5, p0, v6, v7}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "last_refresh."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p1, v6}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    const-string/jumbo v7, "last_refresh."

    const-wide/16 v8, 0x0

    invoke-interface {p1, v6, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    invoke-virtual {v5, v7, v8, v9}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;J)Lcom/twitter/library/client/f;

    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "reg_enabled_for."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    const-string/jumbo v6, "reg_enabled_for."

    invoke-interface {p1, v4, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v5, v6, v4}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;I)Lcom/twitter/library/client/f;

    :cond_3
    invoke-virtual {v5}, Lcom/twitter/library/client/f;->d()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/platform/PushService$DebugNotificationException;)V
    .locals 4

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    const-wide/32 v2, 0xfe6479

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Package: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/crashlytics/android/d;->c(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Registration ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/crashlytics/android/d;->c(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Device ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lcom/twitter/library/platform/PushService;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/crashlytics/android/d;->c(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/crashlytics/android/d;->a(Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method private static a(Landroid/content/SharedPreferences;II)V
    .locals 3

    const/4 v0, 0x1

    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    if-nez p1, :cond_0

    move p1, v0

    :cond_0
    if-ne p1, v0, :cond_1

    const-string/jumbo v0, "reg_id"

    invoke-interface {v1, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "backoff"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "backoff_ceil"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "last_refresh."

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :cond_1
    const-string/jumbo v0, "ver"

    invoke-interface {v1, v0, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/twitter/library/platform/PushService;->m:Z

    sput-object p0, Lcom/twitter/library/platform/PushService;->n:Ljava/lang/String;

    return-void
.end method

.method private static a(Ljava/lang/StringBuilder;)V
    .locals 2

    invoke-static {}, Lcom/twitter/library/client/App;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "environment"

    const/4 v1, 0x2

    invoke-static {p0, v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/accounts/AccountManager;Landroid/accounts/Account;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p0, p2}, Lcom/twitter/library/platform/PushService;->d(Landroid/content/Context;Landroid/accounts/Account;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-static {p1, p2}, Lcom/twitter/library/util/a;->b(Landroid/accounts/AccountManager;Landroid/accounts/Account;)Lcom/twitter/library/network/OAuthToken;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {p0}, Lcom/twitter/library/platform/PushService;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v2, v3}, Lcom/twitter/library/platform/PushService;->a(Landroid/content/Context;Lcom/twitter/library/network/OAuthToken;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p0, p2, v1}, Lcom/twitter/library/platform/PushService;->a(Landroid/content/Context;Landroid/accounts/Account;Z)V

    const-wide/16 v1, 0x0

    invoke-static {p0, p2, v1, v2}, Lcom/twitter/library/platform/PushService;->a(Landroid/content/Context;Landroid/accounts/Account;J)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Lcom/twitter/library/network/OAuthToken;Ljava/lang/String;)Z
    .locals 5

    const/4 v4, 0x0

    invoke-static {p0}, Lcom/twitter/library/network/aa;->a(Landroid/content/Context;)Lcom/twitter/library/network/aa;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const-string/jumbo v2, "account"

    aput-object v2, v1, v4

    const/4 v2, 0x1

    const-string/jumbo v3, "push_destinations"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "destroy"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/aa;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "udid"

    invoke-static {v0, v1, p2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/twitter/library/platform/PushService;->a(Ljava/lang/StringBuilder;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p1, v4}, Lcom/twitter/library/platform/PushService;->a(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/network/OAuthToken;Z)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->k()Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/content/Context;Landroid/accounts/Account;)I
    .locals 13

    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    if-nez p1, :cond_0

    :goto_0
    return v8

    :cond_0
    const/16 v6, 0x7d5

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/provider/i;->a:Landroid/net/Uri;

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "notif_mention"

    aput-object v3, v2, v8

    const-string/jumbo v3, "notif_message"

    aput-object v3, v2, v7

    const-string/jumbo v3, "notif_tweet"

    aput-object v3, v2, v10

    const-string/jumbo v3, "notif_experimental"

    aput-object v3, v2, v11

    const-string/jumbo v3, "notif_lifeline_alerts"

    aput-object v3, v2, v12

    const/4 v3, 0x5

    const-string/jumbo v4, "notif_recommendations"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "notif_news"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string/jumbo v4, "notif_vit_notable_event"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    if-eqz v9, :cond_a

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v9, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_8

    invoke-interface {v9, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    :goto_1
    invoke-interface {v9, v7}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_7

    invoke-interface {v9, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    :goto_2
    invoke-interface {v9, v10}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_6

    invoke-interface {v9, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    :goto_3
    invoke-interface {v9, v11}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_5

    invoke-interface {v9, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    :goto_4
    invoke-interface {v9, v12}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_4

    invoke-interface {v9, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    :goto_5
    const/4 v5, 0x5

    invoke-interface {v9, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v5

    if-nez v5, :cond_3

    const/4 v5, 0x5

    invoke-interface {v9, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    :goto_6
    const/4 v6, 0x6

    invoke-interface {v9, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v6

    if-nez v6, :cond_2

    const/4 v6, 0x6

    invoke-interface {v9, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    :goto_7
    const/4 v10, 0x7

    invoke-interface {v9, v10}, Landroid/database/Cursor;->isNull(I)Z

    move-result v10

    if-nez v10, :cond_1

    const/4 v7, 0x7

    invoke-interface {v9, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_1
    :goto_8
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :goto_9
    sget-object v9, Lcom/twitter/library/provider/NotificationSetting;->a:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v9, v0}, Lcom/twitter/library/provider/NotificationSetting;->d(I)I

    move-result v9

    or-int/2addr v8, v9

    sget-object v9, Lcom/twitter/library/provider/NotificationSetting;->c:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v9, v0}, Lcom/twitter/library/provider/NotificationSetting;->d(I)I

    move-result v9

    or-int/2addr v8, v9

    sget-object v9, Lcom/twitter/library/provider/NotificationSetting;->b:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v9, v0}, Lcom/twitter/library/provider/NotificationSetting;->d(I)I

    move-result v9

    or-int/2addr v8, v9

    sget-object v9, Lcom/twitter/library/provider/NotificationSetting;->d:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v9, v0}, Lcom/twitter/library/provider/NotificationSetting;->d(I)I

    move-result v9

    or-int/2addr v8, v9

    sget-object v9, Lcom/twitter/library/provider/NotificationSetting;->h:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v9, v0}, Lcom/twitter/library/provider/NotificationSetting;->d(I)I

    move-result v9

    or-int/2addr v8, v9

    sget-object v9, Lcom/twitter/library/provider/NotificationSetting;->l:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v9, v0}, Lcom/twitter/library/provider/NotificationSetting;->d(I)I

    move-result v9

    or-int/2addr v8, v9

    sget-object v9, Lcom/twitter/library/provider/NotificationSetting;->m:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v9, v0}, Lcom/twitter/library/provider/NotificationSetting;->d(I)I

    move-result v0

    or-int/2addr v0, v8

    sget-object v8, Lcom/twitter/library/provider/NotificationSetting;->e:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v8, v1}, Lcom/twitter/library/provider/NotificationSetting;->d(I)I

    move-result v1

    or-int/2addr v0, v1

    sget-object v1, Lcom/twitter/library/provider/NotificationSetting;->g:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v1, v2}, Lcom/twitter/library/provider/NotificationSetting;->d(I)I

    move-result v1

    or-int/2addr v0, v1

    sget-object v1, Lcom/twitter/library/provider/NotificationSetting;->i:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v1, v3}, Lcom/twitter/library/provider/NotificationSetting;->d(I)I

    move-result v1

    or-int/2addr v0, v1

    sget-object v1, Lcom/twitter/library/provider/NotificationSetting;->f:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v1, v4}, Lcom/twitter/library/provider/NotificationSetting;->d(I)I

    move-result v1

    or-int/2addr v0, v1

    sget-object v1, Lcom/twitter/library/provider/NotificationSetting;->j:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v1, v5}, Lcom/twitter/library/provider/NotificationSetting;->d(I)I

    move-result v1

    or-int/2addr v0, v1

    sget-object v1, Lcom/twitter/library/provider/NotificationSetting;->k:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v1, v6}, Lcom/twitter/library/provider/NotificationSetting;->d(I)I

    move-result v1

    or-int/2addr v0, v1

    sget-object v1, Lcom/twitter/library/provider/NotificationSetting;->n:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v1, v7}, Lcom/twitter/library/provider/NotificationSetting;->d(I)I

    move-result v1

    or-int v8, v0, v1

    goto/16 :goto_0

    :cond_2
    move v6, v7

    goto :goto_7

    :cond_3
    move v5, v7

    goto/16 :goto_6

    :cond_4
    move v4, v7

    goto/16 :goto_5

    :cond_5
    move v3, v7

    goto/16 :goto_4

    :cond_6
    move v2, v8

    goto/16 :goto_3

    :cond_7
    move v1, v7

    goto/16 :goto_2

    :cond_8
    move v0, v6

    goto/16 :goto_1

    :cond_9
    move v5, v7

    move v4, v7

    move v3, v7

    move v2, v8

    move v1, v7

    move v0, v6

    move v6, v7

    goto/16 :goto_8

    :cond_a
    move v5, v7

    move v4, v7

    move v3, v7

    move v2, v8

    move v1, v7

    move v0, v6

    move v6, v7

    goto/16 :goto_9
.end method

.method public static b(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/twitter/library/client/App;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public static declared-synchronized c(Landroid/content/Context;)Z
    .locals 2

    const-class v1, Lcom/twitter/library/platform/PushService;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/twitter/library/platform/PushService;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :try_start_1
    invoke-static {p0}, Lcom/google/android/gcm/a;->a(Landroid/content/Context;)V

    invoke-static {}, Lcom/twitter/library/client/App;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/gcm/a;->b(Landroid/content/Context;)V

    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/twitter/library/platform/PushService;->j:Z
    :try_end_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    const/4 v0, 0x1

    :try_start_2
    sput-boolean v0, Lcom/twitter/library/platform/PushService;->i:Z

    :cond_1
    sget-boolean v0, Lcom/twitter/library/platform/PushService;->j:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v1

    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    :try_start_3
    sput-boolean v0, Lcom/twitter/library/platform/PushService;->j:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static c(Landroid/content/Context;Landroid/accounts/Account;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gcm/a;->h(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1, p1, v0}, Lcom/twitter/library/platform/PushService;->a(Landroid/content/Context;Landroid/accounts/Account;Z)V

    :goto_0
    return v0

    :cond_0
    invoke-static {v1}, Lcom/twitter/library/platform/PushService;->e(Landroid/content/Context;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gcm/a;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static d(Landroid/content/Context;Landroid/accounts/Account;)Z
    .locals 3

    invoke-static {p0, p1}, Lcom/twitter/library/platform/PushService;->f(Landroid/content/Context;Landroid/accounts/Account;)Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v1, "enabled"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static e(Landroid/content/Context;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v3, 0x0

    sget-object v0, Lcom/twitter/library/platform/PushService;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v3, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "49625052041"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/gcm/a;->a(Landroid/content/Context;[Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static e(Landroid/content/Context;Landroid/accounts/Account;)V
    .locals 4

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/twitter/library/platform/PushService;->a(Landroid/content/Context;Landroid/accounts/AccountManager;Landroid/accounts/Account;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/twitter/library/platform/PushService;->f(Landroid/content/Context;)Z

    new-instance v1, Landroid/content/Intent;

    sget-object v2, Lcom/twitter/library/platform/PushService;->c:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "push_return_code"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/provider/w;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static e(Landroid/content/Context;Ljava/lang/String;)V
    .locals 13

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v8

    sget-object v1, Lcom/twitter/library/util/a;->a:Ljava/lang/String;

    invoke-virtual {v8, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v9

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    const/4 v6, 0x0

    array-length v12, v9

    const/4 v1, 0x0

    move v7, v1

    :goto_0
    if-ge v7, v12, :cond_2

    aget-object v1, v9, v7

    invoke-static {v0, v1}, Lcom/twitter/library/platform/PushService;->d(Landroid/content/Context;Landroid/accounts/Account;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v0, v1}, Lcom/twitter/library/platform/PushService;->a(Landroid/content/Context;Landroid/accounts/Account;)J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    add-long/2addr v2, v4

    cmp-long v2, v2, v10

    if-ltz v2, :cond_1

    :cond_0
    add-int/lit8 v1, v6, 0x1

    :goto_1
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    move v6, v1

    goto :goto_0

    :cond_1
    invoke-static {v8, v1}, Lcom/twitter/library/util/a;->b(Landroid/accounts/AccountManager;Landroid/accounts/Account;)Lcom/twitter/library/network/OAuthToken;

    move-result-object v2

    invoke-static {v0, v1}, Lcom/twitter/library/platform/PushService;->b(Landroid/content/Context;Landroid/accounts/Account;)I

    move-result v4

    const/4 v5, 0x1

    move-object v3, p1

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/platform/PushService;->a(Landroid/content/Context;Landroid/accounts/Account;Lcom/twitter/library/network/OAuthToken;Ljava/lang/String;IZ)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    invoke-static {v0, v1, v10, v11}, Lcom/twitter/library/platform/PushService;->a(Landroid/content/Context;Landroid/accounts/Account;J)V

    add-int/lit8 v1, v6, 0x1

    goto :goto_1

    :cond_2
    array-length v1, v9

    if-ne v6, v1, :cond_3

    const-wide/32 v1, 0x5265c00

    invoke-static {v0, v1, v2}, Lcom/google/android/gcm/a;->a(Landroid/content/Context;J)V

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gcm/a;->a(Landroid/content/Context;Z)V

    :cond_3
    return-void

    :cond_4
    move v1, v6

    goto :goto_1
.end method

.method private static f(Landroid/content/Context;Landroid/accounts/Account;)Lcom/twitter/library/client/f;
    .locals 4

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/platform/PushService;->h(Landroid/content/Context;)V

    new-instance v1, Lcom/twitter/library/client/f;

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string/jumbo v3, "c2dm"

    invoke-direct {v1, v0, v2, v3}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method public static f(Landroid/content/Context;)Z
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    sget-object v3, Lcom/twitter/library/util/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v5

    array-length v6, v5

    move v3, v2

    move v0, v2

    :goto_0
    if-ge v3, v6, :cond_1

    aget-object v7, v5, v3

    invoke-static {v4, v7}, Lcom/twitter/library/platform/PushService;->d(Landroid/content/Context;Landroid/accounts/Account;)Z

    move-result v7

    if-nez v7, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    array-length v3, v5

    if-ne v0, v3, :cond_2

    sget-object v0, Lcom/twitter/library/platform/PushService;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {v4}, Lcom/google/android/gcm/a;->c(Landroid/content/Context;)V

    move v0, v1

    :goto_1
    return v0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public static f(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gcm/a;->h(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {v1, p1}, Lcom/twitter/library/util/a;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {v1, v2}, Lcom/twitter/library/platform/PushService;->d(Landroid/content/Context;Landroid/accounts/Account;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static g(Landroid/content/Context;)V
    .locals 1

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gcm/a;->d(Landroid/content/Context;)V

    return-void
.end method

.method private g(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5

    if-nez p2, :cond_0

    const-string/jumbo p2, "unknown"

    :cond_0
    invoke-static {p1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "notification:status_bar::"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ":push_data_received"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-virtual {v1, v0}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->c(I)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method private static h(Landroid/content/Context;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x2

    const-string/jumbo v0, "c2dm"

    invoke-virtual {p0, v0, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "ver"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "ver"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-ge v1, v2, :cond_0

    invoke-static {v0, v1, v2}, Lcom/twitter/library/platform/PushService;->a(Landroid/content/SharedPreferences;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {v0, v3, v2}, Lcom/twitter/library/platform/PushService;->a(Landroid/content/SharedPreferences;II)V

    invoke-static {p0, v0}, Lcom/twitter/library/platform/PushService;->a(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    goto :goto_0
.end method


# virtual methods
.method a(Lcom/twitter/library/platform/q;)Lcom/twitter/library/platform/DataSyncResult;
    .locals 21

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/twitter/library/platform/q;->d:Landroid/os/Bundle;

    const-string/jumbo v3, "recipient_name"

    invoke-virtual {v11, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/twitter/library/util/a;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v3

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return-object v3

    :cond_0
    const-string/jumbo v3, "collapse_key"

    invoke-virtual {v11, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    sget-object v3, Lcom/twitter/library/platform/PushService;->g:Ljava/util/HashMap;

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v12, v3

    check-cast v12, Ljava/lang/Integer;

    if-nez v12, :cond_1

    const/4 v3, 0x0

    goto :goto_0

    :cond_1
    const-string/jumbo v3, "priority"

    invoke-virtual {v11, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v3, "recipient_id"

    invoke-virtual {v11, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v6, "status_id"

    invoke-virtual {v11, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v6, "sender_name"

    invoke-virtual {v11, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v9, "sender_id"

    invoke-virtual {v11, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "title"

    invoke-virtual {v11, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string/jumbo v10, "text"

    invoke-virtual {v11, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string/jumbo v14, "scribe_target"

    invoke-virtual {v11, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    const-string/jumbo v14, "notification_setting_key"

    invoke-virtual {v11, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    sget-boolean v14, Lcom/twitter/library/platform/PushService;->f:Z

    if-eqz v14, :cond_2

    const-string/jumbo v14, "PushService"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "collapse_key: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v14, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v5, "PushService"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "priority:"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v5, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v5, "PushService"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "sender_name: "

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v5, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v5, "PushService"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "recipient_name: "

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v5, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v5, "PushService"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "recipient_id: "

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v5, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v5, "PushService"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "status_id: "

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v5, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v5, "PushService"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "text: "

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v5, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v5, "PushService"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "title: "

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v5, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v5, "PushService"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "scribe_target: "

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v5, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v5, "PushService"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v17, "notification_setting_key: "

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v5, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v17

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/provider/f;->a(Landroid/content/Context;)Lcom/twitter/library/provider/f;

    move-result-object v19

    move-object/from16 v0, p0

    move-wide/from16 v1, v17

    invoke-static {v0, v1, v2}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v3

    new-instance v14, Lcom/twitter/library/platform/DataSyncResult;

    const/4 v5, 0x1

    move-wide/from16 v0, v17

    invoke-direct {v14, v4, v0, v1, v5}, Lcom/twitter/library/platform/DataSyncResult;-><init>(Ljava/lang/String;JZ)V

    new-instance v20, Lcom/twitter/library/platform/e;

    invoke-direct/range {v20 .. v20}, Lcom/twitter/library/platform/e;-><init>()V

    const/4 v5, 0x1

    move-object/from16 v0, v20

    iput v5, v0, Lcom/twitter/library/platform/e;->c:I

    move-object/from16 v0, v20

    iput-object v10, v0, Lcom/twitter/library/platform/e;->e:Ljava/lang/String;

    move-object/from16 v0, v20

    iput-object v13, v0, Lcom/twitter/library/platform/e;->d:Ljava/lang/String;

    move-object/from16 v0, v20

    iput-object v15, v0, Lcom/twitter/library/platform/e;->i:Ljava/lang/String;

    move-object/from16 v0, v20

    iput-object v6, v0, Lcom/twitter/library/platform/e;->h:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/twitter/library/platform/q;->a:Lcom/twitter/library/platform/notifications/e;

    if-eqz v5, :cond_3

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/twitter/library/platform/q;->a:Lcom/twitter/library/platform/notifications/e;

    iget-object v5, v5, Lcom/twitter/library/platform/notifications/e;->e:Ljava/lang/String;

    :goto_1
    move-object/from16 v0, v20

    iput-object v5, v0, Lcom/twitter/library/platform/e;->k:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/twitter/library/platform/q;->a:Lcom/twitter/library/platform/notifications/e;

    if-eqz v5, :cond_4

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/twitter/library/platform/q;->a:Lcom/twitter/library/platform/notifications/e;

    iget-boolean v5, v5, Lcom/twitter/library/platform/notifications/e;->g:Z

    if-eqz v5, :cond_4

    const/4 v5, 0x1

    :goto_2
    move-object/from16 v0, v20

    iput-boolean v5, v0, Lcom/twitter/library/platform/e;->l:Z

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    :goto_3
    move-object/from16 v0, v20

    iput v5, v0, Lcom/twitter/library/platform/e;->o:I

    const/4 v5, 0x0

    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v13, 0x10

    if-lt v7, v13, :cond_6

    const/4 v7, 0x1

    move v15, v7

    :goto_4
    if-eqz v15, :cond_f

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/twitter/library/platform/q;->b:Lcom/twitter/library/platform/notifications/g;

    move-object/from16 v0, v20

    iput-object v7, v0, Lcom/twitter/library/platform/e;->s:Lcom/twitter/library/platform/notifications/g;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/twitter/library/platform/q;->a:Lcom/twitter/library/platform/notifications/e;

    move-object/from16 v0, v20

    iput-object v7, v0, Lcom/twitter/library/platform/e;->r:Lcom/twitter/library/platform/notifications/e;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/twitter/library/platform/q;->c:Ljava/util/List;

    if-eqz v7, :cond_f

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/twitter/library/platform/q;->c:Ljava/util/List;

    move-object v13, v5

    :goto_5
    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_3
    const/4 v5, 0x0

    goto :goto_1

    :cond_4
    const/4 v5, 0x0

    goto :goto_2

    :cond_5
    const/4 v5, 0x0

    goto :goto_3

    :cond_6
    const/4 v7, 0x0

    move v15, v7

    goto :goto_4

    :pswitch_0
    const-string/jumbo v5, "tweet"

    const/4 v7, 0x1

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5, v7}, Lcom/twitter/library/provider/f;->b(Ljava/lang/String;Ljava/lang/String;I)I

    invoke-virtual {v3, v6}, Lcom/twitter/library/provider/az;->a(Ljava/lang/String;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lcom/twitter/library/platform/e;->a(Lcom/twitter/library/api/TwitterUser;)Lcom/twitter/library/platform/e;

    const/4 v5, 0x1

    move-object/from16 v0, v20

    iput v5, v0, Lcom/twitter/library/platform/e;->b:I

    invoke-static {v8}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    move-object/from16 v0, v20

    iput-wide v7, v0, Lcom/twitter/library/platform/e;->f:J

    const/4 v5, 0x1

    invoke-virtual/range {v20 .. v20}, Lcom/twitter/library/platform/e;->a()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v20

    iget-wide v8, v0, Lcom/twitter/library/platform/e;->g:J

    invoke-virtual/range {v3 .. v10}, Lcom/twitter/library/provider/az;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;JLjava/lang/String;)I

    move-result v3

    move-object/from16 v0, v20

    iput v3, v0, Lcom/twitter/library/platform/e;->n:I

    if-eqz v15, :cond_7

    if-nez v13, :cond_7

    sget-object v13, Lcom/twitter/library/platform/notifications/a;->a:Ljava/util/List;

    :cond_7
    move-object/from16 v0, v20

    iput-object v0, v14, Lcom/twitter/library/platform/DataSyncResult;->f:Lcom/twitter/library/platform/e;

    :cond_8
    :goto_6
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/platform/PushService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v5, v14, Lcom/twitter/library/platform/DataSyncResult;->i:Lcom/twitter/library/platform/e;

    if-eqz v5, :cond_9

    invoke-static {v3, v4}, Lcom/twitter/library/platform/TwitterDataSyncService;->i(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_9

    invoke-static {v3, v4}, Lcom/twitter/library/platform/TwitterDataSyncService;->d(Landroid/content/Context;Ljava/lang/String;)V

    :cond_9
    if-eqz v15, :cond_a

    if-eqz v13, :cond_a

    move-wide/from16 v0, v17

    invoke-static {v3, v0, v1}, Lju;->b(Landroid/content/Context;J)Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/HashMap;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_a

    const-string/jumbo v5, "android_notification_actions_v2_1718"

    move-wide/from16 v0, v17

    invoke-static {v3, v0, v1, v5}, Lju;->b(Landroid/content/Context;JLjava/lang/String;)V

    const-string/jumbo v5, "android_notification_actions_v2_1718"

    move-wide/from16 v0, v17

    invoke-static {v3, v0, v1, v5}, Lju;->a(Landroid/content/Context;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v5, "notification_actions_enabled"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    move-object/from16 v0, v20

    iput-object v13, v0, Lcom/twitter/library/platform/e;->t:Ljava/util/List;

    :cond_a
    :goto_7
    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Lcom/twitter/library/provider/f;->a(Ljava/lang/String;)I

    move-result v3

    iput v3, v14, Lcom/twitter/library/platform/DataSyncResult;->d:I

    move-object v3, v14

    goto/16 :goto_0

    :pswitch_1
    const/4 v5, 0x1

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Lcom/twitter/library/provider/f;->a(Ljava/lang/String;I)I

    invoke-virtual {v3, v6}, Lcom/twitter/library/provider/az;->a(Ljava/lang/String;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lcom/twitter/library/platform/e;->a(Lcom/twitter/library/api/TwitterUser;)Lcom/twitter/library/platform/e;

    invoke-static {v8}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    move-object/from16 v0, v20

    iput-wide v7, v0, Lcom/twitter/library/platform/e;->f:J

    const/4 v5, 0x2

    invoke-virtual/range {v20 .. v20}, Lcom/twitter/library/platform/e;->a()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v20

    iget-wide v8, v0, Lcom/twitter/library/platform/e;->g:J

    invoke-virtual/range {v3 .. v10}, Lcom/twitter/library/provider/az;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;JLjava/lang/String;)I

    move-result v5

    move-object/from16 v0, v20

    iput v5, v0, Lcom/twitter/library/platform/e;->n:I

    invoke-virtual {v3}, Lcom/twitter/library/provider/az;->l()[Lcom/twitter/library/platform/d;

    move-result-object v3

    move-object/from16 v0, v20

    iput-object v3, v0, Lcom/twitter/library/platform/e;->q:[Lcom/twitter/library/platform/d;

    move-object/from16 v0, v20

    iget-object v3, v0, Lcom/twitter/library/platform/e;->q:[Lcom/twitter/library/platform/d;

    array-length v3, v3

    move-object/from16 v0, v20

    iput v3, v0, Lcom/twitter/library/platform/e;->b:I

    if-eqz v15, :cond_b

    if-nez v13, :cond_b

    sget-object v13, Lcom/twitter/library/platform/notifications/a;->a:Ljava/util/List;

    :cond_b
    const/4 v3, 0x1

    iput v3, v14, Lcom/twitter/library/platform/DataSyncResult;->e:I

    move-object/from16 v0, v20

    iput-object v0, v14, Lcom/twitter/library/platform/DataSyncResult;->i:Lcom/twitter/library/platform/e;

    goto/16 :goto_6

    :pswitch_2
    invoke-virtual {v3}, Lcom/twitter/library/provider/az;->g()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    const-string/jumbo v7, "message"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v7, v5}, Lcom/twitter/library/provider/f;->b(Ljava/lang/String;Ljava/lang/String;I)I

    invoke-virtual {v3, v6}, Lcom/twitter/library/provider/az;->a(Ljava/lang/String;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lcom/twitter/library/platform/e;->a(Lcom/twitter/library/api/TwitterUser;)Lcom/twitter/library/platform/e;

    const/4 v5, 0x7

    invoke-virtual/range {v20 .. v20}, Lcom/twitter/library/platform/e;->a()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v20

    iget-wide v8, v0, Lcom/twitter/library/platform/e;->g:J

    invoke-virtual/range {v3 .. v10}, Lcom/twitter/library/provider/az;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;JLjava/lang/String;)I

    move-result v5

    move-object/from16 v0, v20

    iput v5, v0, Lcom/twitter/library/platform/e;->n:I

    invoke-virtual {v3}, Lcom/twitter/library/provider/az;->k()[Lcom/twitter/library/platform/d;

    move-result-object v3

    move-object/from16 v0, v20

    iput-object v3, v0, Lcom/twitter/library/platform/e;->q:[Lcom/twitter/library/platform/d;

    move-object/from16 v0, v20

    iget-object v3, v0, Lcom/twitter/library/platform/e;->q:[Lcom/twitter/library/platform/d;

    array-length v3, v3

    move-object/from16 v0, v20

    iput v3, v0, Lcom/twitter/library/platform/e;->b:I

    move-object/from16 v0, v20

    iput-object v0, v14, Lcom/twitter/library/platform/DataSyncResult;->g:Lcom/twitter/library/platform/e;

    goto/16 :goto_6

    :pswitch_3
    const/4 v5, 0x2

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Lcom/twitter/library/provider/f;->a(Ljava/lang/String;I)I

    invoke-virtual {v3, v6}, Lcom/twitter/library/provider/az;->a(Ljava/lang/String;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lcom/twitter/library/platform/e;->a(Lcom/twitter/library/api/TwitterUser;)Lcom/twitter/library/platform/e;

    invoke-static {v8}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    move-object/from16 v0, v20

    iput-wide v7, v0, Lcom/twitter/library/platform/e;->f:J

    const/4 v5, 0x4

    invoke-virtual/range {v20 .. v20}, Lcom/twitter/library/platform/e;->a()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v20

    iget-wide v8, v0, Lcom/twitter/library/platform/e;->g:J

    invoke-virtual/range {v3 .. v10}, Lcom/twitter/library/provider/az;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;JLjava/lang/String;)I

    move-result v5

    move-object/from16 v0, v20

    iput v5, v0, Lcom/twitter/library/platform/e;->n:I

    invoke-virtual {v3}, Lcom/twitter/library/provider/az;->l()[Lcom/twitter/library/platform/d;

    move-result-object v3

    move-object/from16 v0, v20

    iput-object v3, v0, Lcom/twitter/library/platform/e;->q:[Lcom/twitter/library/platform/d;

    move-object/from16 v0, v20

    iget-object v3, v0, Lcom/twitter/library/platform/e;->q:[Lcom/twitter/library/platform/d;

    array-length v3, v3

    move-object/from16 v0, v20

    iput v3, v0, Lcom/twitter/library/platform/e;->b:I

    const/4 v3, 0x2

    iput v3, v14, Lcom/twitter/library/platform/DataSyncResult;->e:I

    move-object/from16 v0, v20

    iput-object v0, v14, Lcom/twitter/library/platform/DataSyncResult;->i:Lcom/twitter/library/platform/e;

    goto/16 :goto_6

    :pswitch_4
    const/16 v5, 0x8

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Lcom/twitter/library/provider/f;->a(Ljava/lang/String;I)I

    invoke-virtual {v3, v6}, Lcom/twitter/library/provider/az;->a(Ljava/lang/String;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lcom/twitter/library/platform/e;->a(Lcom/twitter/library/api/TwitterUser;)Lcom/twitter/library/platform/e;

    invoke-static {v8}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    move-object/from16 v0, v20

    iput-wide v7, v0, Lcom/twitter/library/platform/e;->f:J

    const/4 v5, 0x3

    invoke-virtual/range {v20 .. v20}, Lcom/twitter/library/platform/e;->a()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v20

    iget-wide v8, v0, Lcom/twitter/library/platform/e;->g:J

    invoke-virtual/range {v3 .. v10}, Lcom/twitter/library/provider/az;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;JLjava/lang/String;)I

    move-result v5

    move-object/from16 v0, v20

    iput v5, v0, Lcom/twitter/library/platform/e;->n:I

    invoke-virtual {v3}, Lcom/twitter/library/provider/az;->l()[Lcom/twitter/library/platform/d;

    move-result-object v3

    move-object/from16 v0, v20

    iput-object v3, v0, Lcom/twitter/library/platform/e;->q:[Lcom/twitter/library/platform/d;

    move-object/from16 v0, v20

    iget-object v3, v0, Lcom/twitter/library/platform/e;->q:[Lcom/twitter/library/platform/d;

    array-length v3, v3

    move-object/from16 v0, v20

    iput v3, v0, Lcom/twitter/library/platform/e;->b:I

    const/16 v3, 0x8

    iput v3, v14, Lcom/twitter/library/platform/DataSyncResult;->e:I

    move-object/from16 v0, v20

    iput-object v0, v14, Lcom/twitter/library/platform/DataSyncResult;->i:Lcom/twitter/library/platform/e;

    goto/16 :goto_6

    :pswitch_5
    const/4 v5, 0x4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Lcom/twitter/library/provider/f;->a(Ljava/lang/String;I)I

    invoke-virtual {v3, v6}, Lcom/twitter/library/provider/az;->a(Ljava/lang/String;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lcom/twitter/library/platform/e;->a(Lcom/twitter/library/api/TwitterUser;)Lcom/twitter/library/platform/e;

    const/4 v5, 0x5

    invoke-virtual/range {v20 .. v20}, Lcom/twitter/library/platform/e;->a()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v20

    iget-wide v8, v0, Lcom/twitter/library/platform/e;->g:J

    const/4 v11, 0x0

    invoke-virtual/range {v3 .. v11}, Lcom/twitter/library/provider/az;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;JLjava/lang/String;Z)I

    move-result v5

    move-object/from16 v0, v20

    iput v5, v0, Lcom/twitter/library/platform/e;->n:I

    invoke-virtual {v3}, Lcom/twitter/library/provider/az;->l()[Lcom/twitter/library/platform/d;

    move-result-object v3

    move-object/from16 v0, v20

    iput-object v3, v0, Lcom/twitter/library/platform/e;->q:[Lcom/twitter/library/platform/d;

    move-object/from16 v0, v20

    iget-object v3, v0, Lcom/twitter/library/platform/e;->q:[Lcom/twitter/library/platform/d;

    array-length v3, v3

    move-object/from16 v0, v20

    iput v3, v0, Lcom/twitter/library/platform/e;->b:I

    const/4 v3, 0x4

    iput v3, v14, Lcom/twitter/library/platform/DataSyncResult;->e:I

    move-object/from16 v0, v20

    iput-object v0, v14, Lcom/twitter/library/platform/DataSyncResult;->i:Lcom/twitter/library/platform/e;

    if-eqz v15, :cond_8

    if-nez v13, :cond_8

    sget-object v13, Lcom/twitter/library/platform/notifications/a;->b:Ljava/util/List;

    goto/16 :goto_6

    :pswitch_6
    const/16 v5, 0x20

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Lcom/twitter/library/provider/f;->a(Ljava/lang/String;I)I

    invoke-virtual {v3, v6}, Lcom/twitter/library/provider/az;->a(Ljava/lang/String;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lcom/twitter/library/platform/e;->a(Lcom/twitter/library/api/TwitterUser;)Lcom/twitter/library/platform/e;

    const/4 v5, 0x1

    move-object/from16 v0, v20

    iput v5, v0, Lcom/twitter/library/platform/e;->b:I

    const/16 v5, 0x8

    invoke-virtual/range {v20 .. v20}, Lcom/twitter/library/platform/e;->a()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v20

    iget-wide v8, v0, Lcom/twitter/library/platform/e;->g:J

    const/4 v11, 0x0

    invoke-virtual/range {v3 .. v11}, Lcom/twitter/library/provider/az;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;JLjava/lang/String;Z)I

    move-result v3

    move-object/from16 v0, v20

    iput v3, v0, Lcom/twitter/library/platform/e;->n:I

    const/16 v3, 0x20

    iput v3, v14, Lcom/twitter/library/platform/DataSyncResult;->e:I

    move-object/from16 v0, v20

    iput-object v0, v14, Lcom/twitter/library/platform/DataSyncResult;->i:Lcom/twitter/library/platform/e;

    if-eqz v15, :cond_8

    if-nez v13, :cond_8

    sget-object v13, Lcom/twitter/library/platform/notifications/a;->c:Ljava/util/List;

    goto/16 :goto_6

    :pswitch_7
    const/16 v5, 0x40

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Lcom/twitter/library/provider/f;->a(Ljava/lang/String;I)I

    const/4 v5, 0x1

    move-object/from16 v0, v20

    iput v5, v0, Lcom/twitter/library/platform/e;->b:I

    const/16 v5, 0x9

    invoke-virtual/range {v20 .. v20}, Lcom/twitter/library/platform/e;->a()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v20

    iget-wide v8, v0, Lcom/twitter/library/platform/e;->g:J

    const/4 v11, 0x1

    invoke-virtual/range {v3 .. v11}, Lcom/twitter/library/provider/az;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;JLjava/lang/String;Z)I

    move-result v3

    move-object/from16 v0, v20

    iput v3, v0, Lcom/twitter/library/platform/e;->n:I

    const/16 v3, 0x40

    iput v3, v14, Lcom/twitter/library/platform/DataSyncResult;->e:I

    move-object/from16 v0, v20

    iput-object v0, v14, Lcom/twitter/library/platform/DataSyncResult;->j:Lcom/twitter/library/platform/e;

    goto/16 :goto_6

    :pswitch_8
    const/16 v5, 0x80

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Lcom/twitter/library/provider/f;->a(Ljava/lang/String;I)I

    invoke-virtual {v3, v6}, Lcom/twitter/library/provider/az;->a(Ljava/lang/String;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lcom/twitter/library/platform/e;->a(Lcom/twitter/library/api/TwitterUser;)Lcom/twitter/library/platform/e;

    const/4 v5, 0x1

    move-object/from16 v0, v20

    iput v5, v0, Lcom/twitter/library/platform/e;->b:I

    invoke-static {v8}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    move-object/from16 v0, v20

    iput-wide v7, v0, Lcom/twitter/library/platform/e;->f:J

    const/16 v5, 0xb

    invoke-virtual/range {v20 .. v20}, Lcom/twitter/library/platform/e;->a()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v20

    iget-wide v8, v0, Lcom/twitter/library/platform/e;->g:J

    invoke-virtual/range {v3 .. v10}, Lcom/twitter/library/provider/az;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;JLjava/lang/String;)I

    move-result v3

    move-object/from16 v0, v20

    iput v3, v0, Lcom/twitter/library/platform/e;->n:I

    const/16 v3, 0x80

    iput v3, v14, Lcom/twitter/library/platform/DataSyncResult;->e:I

    move-object/from16 v0, v20

    iput-object v0, v14, Lcom/twitter/library/platform/DataSyncResult;->k:Lcom/twitter/library/platform/e;

    goto/16 :goto_6

    :pswitch_9
    const/16 v5, 0x200

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Lcom/twitter/library/provider/f;->a(Ljava/lang/String;I)I

    invoke-virtual {v3, v6}, Lcom/twitter/library/provider/az;->a(Ljava/lang/String;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lcom/twitter/library/platform/e;->a(Lcom/twitter/library/api/TwitterUser;)Lcom/twitter/library/platform/e;

    invoke-static {v8}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    move-object/from16 v0, v20

    iput-wide v7, v0, Lcom/twitter/library/platform/e;->f:J

    const/16 v5, 0xc

    invoke-virtual/range {v20 .. v20}, Lcom/twitter/library/platform/e;->a()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v20

    iget-wide v8, v0, Lcom/twitter/library/platform/e;->g:J

    invoke-virtual/range {v3 .. v10}, Lcom/twitter/library/provider/az;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;JLjava/lang/String;)I

    move-result v5

    move-object/from16 v0, v20

    iput v5, v0, Lcom/twitter/library/platform/e;->n:I

    invoke-virtual {v3}, Lcom/twitter/library/provider/az;->l()[Lcom/twitter/library/platform/d;

    move-result-object v3

    move-object/from16 v0, v20

    iput-object v3, v0, Lcom/twitter/library/platform/e;->q:[Lcom/twitter/library/platform/d;

    move-object/from16 v0, v20

    iget-object v3, v0, Lcom/twitter/library/platform/e;->q:[Lcom/twitter/library/platform/d;

    array-length v3, v3

    move-object/from16 v0, v20

    iput v3, v0, Lcom/twitter/library/platform/e;->b:I

    const/16 v3, 0x200

    iput v3, v14, Lcom/twitter/library/platform/DataSyncResult;->e:I

    move-object/from16 v0, v20

    iput-object v0, v14, Lcom/twitter/library/platform/DataSyncResult;->i:Lcom/twitter/library/platform/e;

    goto/16 :goto_6

    :pswitch_a
    const/16 v5, 0x100

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Lcom/twitter/library/provider/f;->a(Ljava/lang/String;I)I

    const/4 v5, 0x1

    move-object/from16 v0, v20

    iput v5, v0, Lcom/twitter/library/platform/e;->b:I

    if-eqz v8, :cond_c

    invoke-static {v8}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    :goto_8
    move-object/from16 v0, v20

    iput-wide v5, v0, Lcom/twitter/library/platform/e;->f:J

    if-eqz v9, :cond_d

    invoke-static {v9}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    :goto_9
    move-object/from16 v0, v20

    iput-wide v5, v0, Lcom/twitter/library/platform/e;->g:J

    const/16 v5, 0xa

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v20

    iget-wide v8, v0, Lcom/twitter/library/platform/e;->g:J

    invoke-virtual/range {v3 .. v10}, Lcom/twitter/library/provider/az;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;JLjava/lang/String;)I

    move-result v3

    move-object/from16 v0, v20

    iput v3, v0, Lcom/twitter/library/platform/e;->n:I

    const-string/jumbo v3, "uri"

    invoke-virtual {v11, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    iput-object v3, v0, Lcom/twitter/library/platform/e;->j:Ljava/lang/String;

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    iput-object v0, v1, Lcom/twitter/library/platform/e;->m:Ljava/lang/String;

    const/16 v3, 0x100

    iput v3, v14, Lcom/twitter/library/platform/DataSyncResult;->e:I

    move-object/from16 v0, v20

    iput-object v0, v14, Lcom/twitter/library/platform/DataSyncResult;->l:Lcom/twitter/library/platform/e;

    goto/16 :goto_6

    :cond_c
    const-wide/16 v5, 0x0

    goto :goto_8

    :cond_d
    const-wide/16 v5, 0x0

    goto :goto_9

    :cond_e
    const/4 v3, 0x0

    move-object/from16 v0, v20

    iput-object v3, v0, Lcom/twitter/library/platform/e;->t:Ljava/util/List;

    goto/16 :goto_7

    :cond_f
    move-object v13, v5

    goto/16 :goto_5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_a
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method a(Landroid/os/Bundle;)Lcom/twitter/library/platform/q;
    .locals 4

    const-string/jumbo v0, "schema"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    sget-boolean v1, Lcom/twitter/library/platform/PushService;->f:Z

    if-eqz v1, :cond_0

    const-string/jumbo v1, "PushService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Payload schema="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    new-instance v0, Lcom/twitter/library/platform/notifications/b;

    invoke-direct {v0, p0}, Lcom/twitter/library/platform/notifications/b;-><init>(Landroid/content/Context;)V

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/twitter/library/platform/PushService;->a(Landroid/os/Bundle;Lcom/twitter/library/platform/notifications/b;Landroid/content/SharedPreferences;)Lcom/twitter/library/platform/q;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_1
    :goto_1
    return-object v0

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    :cond_3
    new-instance v0, Lcom/twitter/library/platform/q;

    invoke-direct {v0}, Lcom/twitter/library/platform/q;-><init>()V

    iput-object p1, v0, Lcom/twitter/library/platform/q;->d:Landroid/os/Bundle;

    iget-object v1, v0, Lcom/twitter/library/platform/q;->d:Landroid/os/Bundle;

    const-string/jumbo v2, "recipient_id"

    const-string/jumbo v3, "user_id"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    sget-boolean v0, Lcom/twitter/library/platform/PushService;->f:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "PushService"

    const-string/jumbo v1, "Push received."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Lcom/twitter/library/platform/PushService;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/platform/PushService$DebugNotificationException;

    const-string/jumbo v2, "Received push"

    invoke-direct {v1, v2}, Lcom/twitter/library/platform/PushService$DebugNotificationException;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v0, v1}, Lcom/twitter/library/platform/PushService;->a(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/platform/PushService$DebugNotificationException;)V

    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "collapse_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/twitter/library/platform/PushService;->g(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/twitter/library/platform/PushService;->a(Landroid/os/Bundle;)Lcom/twitter/library/platform/q;

    move-result-object v0

    if-nez v0, :cond_3

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-virtual {p0, v0}, Lcom/twitter/library/platform/PushService;->a(Lcom/twitter/library/platform/q;)Lcom/twitter/library/platform/DataSyncResult;

    move-result-object v0

    if-eqz v0, :cond_2

    new-instance v1, Landroid/content/Intent;

    sget-object v2, Lcom/twitter/library/platform/TwitterDataSyncService;->a:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "data"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    sget-object v0, Lcom/twitter/library/provider/w;->a:Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Lcom/twitter/library/platform/PushService;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/twitter/library/platform/PushService;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/twitter/library/platform/PushService;->e:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/twitter/library/provider/w;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    sget-boolean v0, Lcom/twitter/library/platform/PushService;->m:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/library/platform/PushService;->l:Landroid/os/Handler;

    new-instance v1, Lcom/twitter/library/platform/r;

    sget v2, Lil;->preference_notification_error:I

    invoke-direct {v1, p0, v2}, Lcom/twitter/library/platform/r;-><init>(Lcom/twitter/library/platform/PushService;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    sput-boolean v3, Lcom/twitter/library/platform/PushService;->m:Z

    :cond_0
    return-void
.end method

.method protected c(Landroid/content/Context;Ljava/lang/String;)V
    .locals 13

    const/4 v12, 0x1

    const/4 v5, 0x0

    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v7

    sget-object v0, Lcom/twitter/library/util/a;->a:Ljava/lang/String;

    invoke-virtual {v7, v0}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    array-length v11, v8

    move v6, v5

    :goto_0
    if-ge v6, v11, :cond_1

    aget-object v1, v8, v6

    invoke-static {p1, v1}, Lcom/twitter/library/platform/PushService;->d(Landroid/content/Context;Landroid/accounts/Account;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v7, v1}, Lcom/twitter/library/util/a;->b(Landroid/accounts/AccountManager;Landroid/accounts/Account;)Lcom/twitter/library/network/OAuthToken;

    move-result-object v2

    invoke-static {p1, v1}, Lcom/twitter/library/platform/PushService;->b(Landroid/content/Context;Landroid/accounts/Account;)I

    move-result v4

    move-object v0, p1

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/platform/PushService;->a(Landroid/content/Context;Landroid/accounts/Account;Lcom/twitter/library/network/OAuthToken;Ljava/lang/String;IZ)I

    move-result v0

    if-ne v0, v12, :cond_0

    invoke-static {p1, v1, v9, v10}, Lcom/twitter/library/platform/PushService;->a(Landroid/content/Context;Landroid/accounts/Account;J)V

    :cond_0
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :cond_1
    const-wide/32 v0, 0x5265c00

    invoke-static {p1, v0, v1}, Lcom/google/android/gcm/a;->a(Landroid/content/Context;J)V

    invoke-static {p1, v12}, Lcom/google/android/gcm/a;->a(Landroid/content/Context;Z)V

    sget-object v0, Lcom/twitter/library/platform/PushService;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/twitter/library/platform/PushService;->d:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/twitter/library/provider/w;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/twitter/library/platform/PushService$DebugNotificationException;

    const-string/jumbo v1, "Registered with GCM"

    invoke-direct {v0, v1}, Lcom/twitter/library/platform/PushService$DebugNotificationException;-><init>(Ljava/lang/String;)V

    invoke-static {p1, p2, v0}, Lcom/twitter/library/platform/PushService;->a(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/platform/PushService$DebugNotificationException;)V

    :cond_2
    sget-boolean v0, Lcom/twitter/library/platform/PushService;->m:Z

    if-eqz v0, :cond_3

    sget-object v0, Lcom/twitter/library/platform/PushService;->n:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/twitter/library/util/a;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/twitter/library/platform/PushService;->b(Landroid/content/Context;Landroid/accounts/Account;)I

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/twitter/library/platform/PushService;->a(Landroid/content/Context;Landroid/accounts/Account;I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/twitter/library/platform/PushService;->l:Landroid/os/Handler;

    new-instance v1, Lcom/twitter/library/platform/r;

    sget v2, Lil;->preference_notification_error:I

    invoke-direct {v1, p0, v2}, Lcom/twitter/library/platform/r;-><init>(Lcom/twitter/library/platform/PushService;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_1
    sput-boolean v5, Lcom/twitter/library/platform/PushService;->m:Z

    :cond_3
    return-void

    :pswitch_0
    sget-object v0, Lcom/twitter/library/platform/PushService;->l:Landroid/os/Handler;

    new-instance v1, Lcom/twitter/library/platform/r;

    sget v2, Lil;->preference_notification_success:I

    invoke-direct {v1, p0, v2}, Lcom/twitter/library/platform/r;-><init>(Lcom/twitter/library/platform/PushService;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    :pswitch_1
    sget-object v0, Lcom/twitter/library/platform/PushService;->l:Landroid/os/Handler;

    new-instance v1, Lcom/twitter/library/platform/r;

    sget v2, Lil;->preference_notification_too_many_devices:I

    invoke-direct {v1, p0, v2}, Lcom/twitter/library/platform/r;-><init>(Lcom/twitter/library/platform/PushService;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected d(Landroid/content/Context;Ljava/lang/String;)V
    .locals 6

    const/4 v1, 0x0

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    sget-object v0, Lcom/twitter/library/util/a;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    invoke-static {p1, v2, v5}, Lcom/twitter/library/platform/PushService;->a(Landroid/content/Context;Landroid/accounts/AccountManager;Landroid/accounts/Account;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/twitter/library/platform/PushService;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void
.end method
