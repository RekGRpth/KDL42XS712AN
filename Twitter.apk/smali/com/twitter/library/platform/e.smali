.class public final Lcom/twitter/library/platform/e;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:[Lcom/twitter/library/platform/d;


# instance fields
.field public b:I

.field public c:I

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:J

.field public g:J

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Z

.field public m:Ljava/lang/String;

.field public n:I

.field public o:I

.field public p:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public q:[Lcom/twitter/library/platform/d;

.field public r:Lcom/twitter/library/platform/notifications/e;

.field public s:Lcom/twitter/library/platform/notifications/g;

.field public t:Ljava/util/List;

.field u:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/twitter/library/platform/d;

    sput-object v0, Lcom/twitter/library/platform/e;->a:[Lcom/twitter/library/platform/d;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/twitter/library/platform/e;->a:[Lcom/twitter/library/platform/d;

    iput-object v0, p0, Lcom/twitter/library/platform/e;->q:[Lcom/twitter/library/platform/d;

    return-void
.end method


# virtual methods
.method a(Lcom/twitter/library/api/TwitterStatus;)Lcom/twitter/library/platform/e;
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/twitter/library/api/TwitterStatus;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/platform/e;->e:Ljava/lang/String;

    iget-wide v0, p1, Lcom/twitter/library/api/TwitterStatus;->a:J

    iput-wide v0, p0, Lcom/twitter/library/platform/e;->f:J

    :cond_0
    return-object p0
.end method

.method a(Lcom/twitter/library/api/TwitterUser;)Lcom/twitter/library/platform/e;
    .locals 2

    if-eqz p1, :cond_0

    iget-wide v0, p1, Lcom/twitter/library/api/TwitterUser;->userId:J

    iput-wide v0, p0, Lcom/twitter/library/platform/e;->g:J

    iget-object v0, p1, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/platform/e;->h:Ljava/lang/String;

    iget-object v0, p1, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/platform/e;->u:Ljava/lang/String;

    :cond_0
    return-object p0
.end method

.method public a()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/platform/e;->u:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/platform/e;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/platform/e;->u:Ljava/lang/String;

    goto :goto_0
.end method
