.class public Lcom/twitter/library/platform/TwitterDataSyncService;
.super Landroid/app/IntentService;
.source "Twttr"


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:Ljava/util/HashMap;

.field private static final c:Ljava/lang/Object;

.field private static d:Lcom/twitter/library/platform/b;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x3

    const-string/jumbo v0, ".poll.data"

    invoke-static {v0}, Lcom/twitter/library/client/App;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/platform/TwitterDataSyncService;->a:Ljava/lang/String;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v3}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/twitter/library/platform/TwitterDataSyncService;->b:Ljava/util/HashMap;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/twitter/library/platform/TwitterDataSyncService;->c:Ljava/lang/Object;

    const/4 v0, 0x0

    sput-object v0, Lcom/twitter/library/platform/TwitterDataSyncService;->d:Lcom/twitter/library/platform/b;

    sget-object v0, Lcom/twitter/library/platform/TwitterDataSyncService;->b:Ljava/util/HashMap;

    const-string/jumbo v1, "sync_account"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/platform/TwitterDataSyncService;->b:Ljava/util/HashMap;

    const-string/jumbo v1, "on_poll_alarm_ev"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/platform/TwitterDataSyncService;->b:Ljava/util/HashMap;

    const-string/jumbo v1, "update_alarm"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const-string/jumbo v0, "SyncService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)J
    .locals 3

    new-instance v0, Lcom/twitter/library/client/f;

    const-string/jumbo v1, "activity_times"

    invoke-direct {v0, p0, p1, v1}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v1, 0x0

    invoke-virtual {v0, p2, v1, v2}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/library/platform/TwitterDataSyncService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "update_alarm"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/os/Bundle;ZLandroid/accounts/Account;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/twitter/library/provider/w;->c:Ljava/lang/String;

    invoke-static {p3, v1}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/twitter/library/provider/w;->c:Ljava/lang/String;

    invoke-static {p3, v1}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_0

    invoke-static {p0}, Lcom/twitter/library/platform/TwitterDataSyncService;->c(Landroid/content/Context;)V

    sget-object v1, Lcom/twitter/library/provider/w;->c:Ljava/lang/String;

    invoke-static {p3, v1, p1}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    :goto_0
    return v0

    :cond_0
    if-eqz p2, :cond_1

    invoke-static {p0}, Lcom/twitter/library/platform/TwitterDataSyncService;->c(Landroid/content/Context;)V

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/library/platform/TwitterDataSyncService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "sync_account"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "account"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "_data"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 4

    invoke-static {p0, p1}, Lcom/twitter/library/platform/TwitterDataSyncService;->c(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {p0, p1}, Lcom/twitter/library/platform/TwitterDataSyncService;->e(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "last_home_sync"

    invoke-static {p0, p1, v0}, Lcom/twitter/library/platform/TwitterDataSyncService;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    new-instance v0, Lcom/twitter/library/client/f;

    const-string/jumbo v1, "activity_times"

    invoke-direct {v0, p0, p1, v1}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, p2, v1, v2}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;J)Lcom/twitter/library/client/f;

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V

    return-void
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 4

    invoke-static {p0}, Lcom/twitter/library/platform/TwitterDataSyncService;->d(Landroid/content/Context;)J

    move-result-wide v0

    const-wide/32 v2, 0xea60

    add-long/2addr v0, v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;)J
    .locals 2

    const-string/jumbo v0, "last_home_sync"

    invoke-static {p0, p1, v0}, Lcom/twitter/library/platform/TwitterDataSyncService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method private static c(Landroid/content/Context;)V
    .locals 1

    const-string/jumbo v0, "last_sync"

    invoke-static {p0, v0}, Lcom/twitter/library/platform/TwitterDataSyncService;->l(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method private static d(Landroid/content/Context;)J
    .locals 2

    const-string/jumbo v0, "last_sync"

    invoke-static {p0, v0}, Lcom/twitter/library/platform/TwitterDataSyncService;->k(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method static d(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "last_mention_push"

    invoke-static {p0, p1, v0}, Lcom/twitter/library/platform/TwitterDataSyncService;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static e(Landroid/content/Context;Ljava/lang/String;)J
    .locals 2

    const-string/jumbo v0, "last_mention_push"

    invoke-static {p0, p1, v0}, Lcom/twitter/library/platform/TwitterDataSyncService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static f(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 4

    invoke-static {p0, p1}, Lcom/twitter/library/platform/TwitterDataSyncService;->h(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {p0, p1}, Lcom/twitter/library/platform/TwitterDataSyncService;->j(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static g(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "last_activity_sync"

    invoke-static {p0, p1, v0}, Lcom/twitter/library/platform/TwitterDataSyncService;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static h(Landroid/content/Context;Ljava/lang/String;)J
    .locals 2

    const-string/jumbo v0, "last_activity_sync"

    invoke-static {p0, p1, v0}, Lcom/twitter/library/platform/TwitterDataSyncService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method static i(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "last_activity_push"

    invoke-static {p0, p1, v0}, Lcom/twitter/library/platform/TwitterDataSyncService;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static j(Landroid/content/Context;Ljava/lang/String;)J
    .locals 2

    const-string/jumbo v0, "last_activity_push"

    invoke-static {p0, p1, v0}, Lcom/twitter/library/platform/TwitterDataSyncService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method private static k(Landroid/content/Context;Ljava/lang/String;)J
    .locals 3

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-interface {v0, p1, v1, v2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private static l(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-interface {v0, p1, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    sget-object v0, Lcom/twitter/library/platform/TwitterDataSyncService;->d:Lcom/twitter/library/platform/b;

    invoke-virtual {v0}, Lcom/twitter/library/platform/b;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    sget-object v1, Lcom/twitter/library/platform/TwitterDataSyncService;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/platform/TwitterDataSyncService;->d:Lcom/twitter/library/platform/b;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/library/platform/b;

    invoke-virtual {p0}, Lcom/twitter/library/platform/TwitterDataSyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/twitter/library/platform/b;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/library/platform/TwitterDataSyncService;->d:Lcom/twitter/library/platform/b;

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 7

    const/4 v4, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/twitter/library/platform/TwitterDataSyncService;->b:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    new-instance v1, Lcom/twitter/library/platform/b;

    invoke-direct {v1, p0}, Lcom/twitter/library/platform/b;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/content/SyncResult;

    invoke-direct {v2}, Landroid/content/SyncResult;-><init>()V

    const-string/jumbo v0, "account"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    const-string/jumbo v3, "_data"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v1, v0, v3, v2}, Lcom/twitter/library/platform/b;->a(Landroid/accounts/Account;Landroid/os/Bundle;Landroid/content/SyncResult;)V

    goto :goto_0

    :pswitch_1
    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v2, Lcom/twitter/library/platform/b;

    invoke-direct {v2, p0}, Lcom/twitter/library/platform/b;-><init>(Landroid/content/Context;)V

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v0, "show_notif"

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "fs_config"

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-static {p0}, Lcom/twitter/library/platform/PushService;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "messages"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_2
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    sget-object v4, Lcom/twitter/library/util/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v4

    array-length v5, v4

    move v0, v1

    :goto_1
    if-ge v0, v5, :cond_4

    aget-object v1, v4, v0

    invoke-virtual {v2, v1}, Lcom/twitter/library/platform/b;->a(Landroid/accounts/Account;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-static {p0}, Lcom/twitter/library/platform/TwitterDataSyncService;->c(Landroid/content/Context;)V

    sget-object v6, Lcom/twitter/library/provider/w;->c:Ljava/lang/String;

    invoke-static {v1, v6, v3}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    invoke-static {p0}, Lcom/twitter/library/platform/b;->a(Landroid/content/Context;)V

    goto/16 :goto_0

    :pswitch_2
    invoke-static {p0}, Lcom/twitter/library/platform/b;->a(Landroid/content/Context;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
