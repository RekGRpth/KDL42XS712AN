.class public final enum Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;
.super Ljava/lang/Enum;
.source "Twttr"


# static fields
.field public static final enum a:Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;

.field public static final enum b:Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;

.field public static final enum c:Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;

.field public static final enum d:Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;

.field private static final synthetic e:[Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;

    const-string/jumbo v1, "DECIDER_ENABLED"

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;->a:Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;

    new-instance v0, Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;

    const-string/jumbo v1, "PLAY_SERVICES_ENABLED"

    invoke-direct {v0, v1, v3}, Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;->b:Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;

    new-instance v0, Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;

    const-string/jumbo v1, "UPDATE_DURATION"

    invoke-direct {v0, v1, v4}, Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;->c:Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;

    new-instance v0, Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;

    const-string/jumbo v1, "UPDATE_INTERVAL"

    invoke-direct {v0, v1, v5}, Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;->d:Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;

    sget-object v1, Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;->a:Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;

    aput-object v1, v0, v2

    sget-object v1, Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;->b:Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;->c:Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;->d:Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;

    aput-object v1, v0, v5

    sput-object v0, Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;->e:[Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;
    .locals 1

    const-class v0, Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;

    return-object v0
.end method

.method public static values()[Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;
    .locals 1

    sget-object v0, Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;->e:[Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;

    invoke-virtual {v0}, [Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;

    return-object v0
.end method
