.class Lcom/twitter/library/platform/f;
.super Lcom/twitter/library/platform/h;
.source "Twttr"

# interfaces
.implements Lcom/google/android/gms/common/c;
.implements Lcom/google/android/gms/location/d;


# static fields
.field private static c:Lcom/twitter/library/platform/f;


# instance fields
.field private d:Z

.field private e:Lcom/google/android/gms/location/LocationRequest;

.field private f:Lcom/google/android/gms/location/c;

.field private g:Landroid/content/Context;

.field private h:Lcom/google/android/gms/common/d;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/twitter/library/platform/i;Lcom/google/android/gms/common/d;)V
    .locals 3

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/platform/h;-><init>(Landroid/content/Context;Lcom/twitter/library/platform/i;)V

    iput-object p1, p0, Lcom/twitter/library/platform/f;->g:Landroid/content/Context;

    iput-object p3, p0, Lcom/twitter/library/platform/f;->h:Lcom/google/android/gms/common/d;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/platform/f;->d:Z

    invoke-static {}, Lcom/google/android/gms/location/LocationRequest;->a()Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/f;->e:Lcom/google/android/gms/location/LocationRequest;

    iget-object v0, p0, Lcom/twitter/library/platform/f;->e:Lcom/google/android/gms/location/LocationRequest;

    const-wide/16 v1, 0x7d0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/location/LocationRequest;->a(J)Lcom/google/android/gms/location/LocationRequest;

    iget-object v0, p0, Lcom/twitter/library/platform/f;->e:Lcom/google/android/gms/location/LocationRequest;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/location/LocationRequest;->b(J)Lcom/google/android/gms/location/LocationRequest;

    iget-object v0, p0, Lcom/twitter/library/platform/f;->e:Lcom/google/android/gms/location/LocationRequest;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/LocationRequest;->a(I)Lcom/google/android/gms/location/LocationRequest;

    iget-object v0, p0, Lcom/twitter/library/platform/f;->e:Lcom/google/android/gms/location/LocationRequest;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/LocationRequest;->b(I)Lcom/google/android/gms/location/LocationRequest;

    new-instance v0, Lcom/google/android/gms/location/c;

    invoke-direct {v0, p1, p0, p3}, Lcom/google/android/gms/location/c;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;)V

    iput-object v0, p0, Lcom/twitter/library/platform/f;->f:Lcom/google/android/gms/location/c;

    iget-object v0, p0, Lcom/twitter/library/platform/f;->f:Lcom/google/android/gms/location/c;

    invoke-virtual {v0}, Lcom/google/android/gms/location/c;->b()V

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;Lcom/twitter/library/platform/i;Lcom/google/android/gms/common/d;)Lcom/twitter/library/platform/f;
    .locals 2

    const-class v1, Lcom/twitter/library/platform/f;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/platform/f;->c:Lcom/twitter/library/platform/f;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/library/platform/f;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/library/platform/f;-><init>(Landroid/content/Context;Lcom/twitter/library/platform/i;Lcom/google/android/gms/common/d;)V

    sput-object v0, Lcom/twitter/library/platform/f;->c:Lcom/twitter/library/platform/f;

    :cond_0
    sget-object v0, Lcom/twitter/library/platform/f;->c:Lcom/twitter/library/platform/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Ljava/lang/Exception;)Z
    .locals 3

    instance-of v0, p1, Landroid/os/DeadObjectException;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/crashlytics/android/d;->a(Ljava/lang/Throwable;)V

    new-instance v0, Lcom/google/android/gms/location/c;

    iget-object v1, p0, Lcom/twitter/library/platform/f;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/library/platform/f;->h:Lcom/google/android/gms/common/d;

    invoke-direct {v0, v1, p0, v2}, Lcom/google/android/gms/location/c;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;)V

    iput-object v0, p0, Lcom/twitter/library/platform/f;->f:Lcom/google/android/gms/location/c;

    iget-object v0, p0, Lcom/twitter/library/platform/f;->f:Lcom/google/android/gms/location/c;

    invoke-virtual {v0}, Lcom/google/android/gms/location/c;->b()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public a(Landroid/location/Location;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/platform/f;->b:Lcom/twitter/library/platform/i;

    invoke-interface {v0, p1}, Lcom/twitter/library/platform/i;->a(Landroid/location/Location;)V

    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/platform/f;->d:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/platform/f;->c()V

    :cond_0
    return-void
.end method

.method public b()Landroid/location/Location;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/library/platform/f;->f:Lcom/google/android/gms/location/c;

    invoke-virtual {v1}, Lcom/google/android/gms/location/c;->c()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/twitter/library/platform/f;->f:Lcom/google/android/gms/location/c;

    invoke-virtual {v1}, Lcom/google/android/gms/location/c;->a()Landroid/location/Location;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-direct {p0, v1}, Lcom/twitter/library/platform/f;->a(Ljava/lang/Exception;)Z

    move-result v2

    if-nez v2, :cond_0

    throw v1
.end method

.method public c()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/platform/f;->f:Lcom/google/android/gms/location/c;

    invoke-virtual {v0}, Lcom/google/android/gms/location/c;->c()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/platform/f;->d:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/platform/f;->f:Lcom/google/android/gms/location/c;

    iget-object v1, p0, Lcom/twitter/library/platform/f;->e:Lcom/google/android/gms/location/LocationRequest;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/gms/location/c;->a(Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/d;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/twitter/library/platform/f;->a(Ljava/lang/Exception;)Z

    move-result v1

    if-nez v1, :cond_0

    throw v0
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/platform/f;->f:Lcom/google/android/gms/location/c;

    invoke-virtual {v0}, Lcom/google/android/gms/location/c;->c()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/platform/f;->d:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/platform/f;->f:Lcom/google/android/gms/location/c;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/location/c;->a(Lcom/google/android/gms/location/d;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/twitter/library/platform/f;->a(Ljava/lang/Exception;)Z

    move-result v1

    if-nez v1, :cond_0

    throw v0
.end method
