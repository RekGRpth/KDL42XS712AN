.class public Lcom/twitter/library/platform/DataSyncResult;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J

.field public final c:Z

.field public d:I

.field public e:I

.field public f:Lcom/twitter/library/platform/e;

.field public g:Lcom/twitter/library/platform/e;

.field public h:Lcom/twitter/library/platform/e;

.field public i:Lcom/twitter/library/platform/e;

.field public j:Lcom/twitter/library/platform/e;

.field public k:Lcom/twitter/library/platform/e;

.field public l:Lcom/twitter/library/platform/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/library/platform/c;

    invoke-direct {v0}, Lcom/twitter/library/platform/c;-><init>()V

    sput-object v0, Lcom/twitter/library/platform/DataSyncResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/platform/DataSyncResult;->a:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/twitter/library/platform/DataSyncResult;->b:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/library/platform/DataSyncResult;->c:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/platform/DataSyncResult;->d:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/platform/DataSyncResult;->e:I

    invoke-direct {p0, p1}, Lcom/twitter/library/platform/DataSyncResult;->b(Landroid/os/Parcel;)Lcom/twitter/library/platform/e;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/DataSyncResult;->f:Lcom/twitter/library/platform/e;

    invoke-direct {p0, p1}, Lcom/twitter/library/platform/DataSyncResult;->b(Landroid/os/Parcel;)Lcom/twitter/library/platform/e;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/DataSyncResult;->g:Lcom/twitter/library/platform/e;

    invoke-direct {p0, p1}, Lcom/twitter/library/platform/DataSyncResult;->b(Landroid/os/Parcel;)Lcom/twitter/library/platform/e;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/DataSyncResult;->i:Lcom/twitter/library/platform/e;

    invoke-direct {p0, p1}, Lcom/twitter/library/platform/DataSyncResult;->b(Landroid/os/Parcel;)Lcom/twitter/library/platform/e;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/DataSyncResult;->h:Lcom/twitter/library/platform/e;

    invoke-direct {p0, p1}, Lcom/twitter/library/platform/DataSyncResult;->b(Landroid/os/Parcel;)Lcom/twitter/library/platform/e;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/DataSyncResult;->j:Lcom/twitter/library/platform/e;

    invoke-direct {p0, p1}, Lcom/twitter/library/platform/DataSyncResult;->b(Landroid/os/Parcel;)Lcom/twitter/library/platform/e;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/DataSyncResult;->k:Lcom/twitter/library/platform/e;

    invoke-direct {p0, p1}, Lcom/twitter/library/platform/DataSyncResult;->b(Landroid/os/Parcel;)Lcom/twitter/library/platform/e;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/DataSyncResult;->l:Lcom/twitter/library/platform/e;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;JZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/platform/DataSyncResult;->a:Ljava/lang/String;

    iput-wide p2, p0, Lcom/twitter/library/platform/DataSyncResult;->b:J

    iput-boolean p4, p0, Lcom/twitter/library/platform/DataSyncResult;->c:Z

    return-void
.end method

.method public static a(Landroid/os/Parcel;)Lcom/twitter/library/platform/e;
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-instance v3, Lcom/twitter/library/platform/e;

    invoke-direct {v3}, Lcom/twitter/library/platform/e;-><init>()V

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, v3, Lcom/twitter/library/platform/e;->c:I

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, v3, Lcom/twitter/library/platform/e;->b:I

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/twitter/library/platform/e;->d:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/twitter/library/platform/e;->e:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, v3, Lcom/twitter/library/platform/e;->f:J

    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, v3, Lcom/twitter/library/platform/e;->g:J

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/twitter/library/platform/e;->h:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/twitter/library/platform/e;->i:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/twitter/library/platform/e;->j:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/twitter/library/platform/e;->k:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    iput-boolean v0, v3, Lcom/twitter/library/platform/e;->l:Z

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, v3, Lcom/twitter/library/platform/e;->n:I

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/twitter/library/platform/e;->u:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/twitter/library/platform/e;->m:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    new-instance v0, Lcom/twitter/library/platform/notifications/e;

    invoke-direct {v0, p0}, Lcom/twitter/library/platform/notifications/e;-><init>(Landroid/os/Parcel;)V

    iput-object v0, v3, Lcom/twitter/library/platform/e;->r:Lcom/twitter/library/platform/notifications/e;

    :cond_0
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    new-instance v0, Lcom/twitter/library/platform/notifications/g;

    invoke-direct {v0, p0}, Lcom/twitter/library/platform/notifications/g;-><init>(Landroid/os/Parcel;)V

    iput-object v0, v3, Lcom/twitter/library/platform/e;->s:Lcom/twitter/library/platform/notifications/g;

    :cond_1
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v4

    const/4 v0, -0x1

    if-ne v4, v0, :cond_4

    const/4 v0, 0x0

    :cond_2
    iput-object v0, v3, Lcom/twitter/library/platform/e;->t:Ljava/util/List;

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_5

    new-array v1, v0, [Lcom/twitter/library/platform/d;

    iput-object v1, v3, Lcom/twitter/library/platform/e;->q:[Lcom/twitter/library/platform/d;

    :goto_1
    if-ge v2, v0, :cond_6

    new-instance v1, Lcom/twitter/library/platform/d;

    invoke-direct {v1}, Lcom/twitter/library/platform/d;-><init>()V

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v4

    iput v4, v1, Lcom/twitter/library/platform/d;->a:I

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v4

    iput v4, v1, Lcom/twitter/library/platform/d;->b:I

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/twitter/library/platform/d;->c:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/twitter/library/platform/d;->d:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/twitter/library/platform/d;->e:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, v1, Lcom/twitter/library/platform/d;->f:J

    iget-object v4, v3, Lcom/twitter/library/platform/e;->q:[Lcom/twitter/library/platform/d;

    aput-object v1, v4, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    move v1, v2

    :goto_2
    if-ge v1, v4, :cond_2

    new-instance v5, Lcom/twitter/library/platform/notifications/a;

    invoke-direct {v5, p0}, Lcom/twitter/library/platform/notifications/a;-><init>(Landroid/os/Parcel;)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_5
    sget-object v0, Lcom/twitter/library/platform/e;->a:[Lcom/twitter/library/platform/d;

    iput-object v0, v3, Lcom/twitter/library/platform/e;->q:[Lcom/twitter/library/platform/d;

    :cond_6
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, v3, Lcom/twitter/library/platform/e;->o:I

    return-object v3
.end method

.method public static a(Landroid/os/Parcel;Lcom/twitter/library/platform/e;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v0, p1, Lcom/twitter/library/platform/e;->c:I

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p1, Lcom/twitter/library/platform/e;->b:I

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p1, Lcom/twitter/library/platform/e;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/twitter/library/platform/e;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v3, p1, Lcom/twitter/library/platform/e;->f:J

    invoke-virtual {p0, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v3, p1, Lcom/twitter/library/platform/e;->g:J

    invoke-virtual {p0, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p1, Lcom/twitter/library/platform/e;->h:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/twitter/library/platform/e;->i:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/twitter/library/platform/e;->j:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/twitter/library/platform/e;->k:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-boolean v0, p1, Lcom/twitter/library/platform/e;->l:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p1, Lcom/twitter/library/platform/e;->n:I

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p1, Lcom/twitter/library/platform/e;->u:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/twitter/library/platform/e;->m:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/twitter/library/platform/e;->r:Lcom/twitter/library/platform/notifications/e;

    if-nez v0, :cond_4

    move v0, v2

    :goto_1
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p1, Lcom/twitter/library/platform/e;->r:Lcom/twitter/library/platform/notifications/e;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/twitter/library/platform/e;->r:Lcom/twitter/library/platform/notifications/e;

    invoke-virtual {v0, p0}, Lcom/twitter/library/platform/notifications/e;->a(Landroid/os/Parcel;)V

    :cond_0
    iget-object v0, p1, Lcom/twitter/library/platform/e;->s:Lcom/twitter/library/platform/notifications/g;

    if-nez v0, :cond_1

    move v1, v2

    :cond_1
    invoke-virtual {p0, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p1, Lcom/twitter/library/platform/e;->s:Lcom/twitter/library/platform/notifications/g;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/twitter/library/platform/e;->s:Lcom/twitter/library/platform/notifications/g;

    invoke-virtual {v0, p0}, Lcom/twitter/library/platform/notifications/g;->a(Landroid/os/Parcel;)V

    :cond_2
    iget-object v0, p1, Lcom/twitter/library/platform/e;->t:Ljava/util/List;

    if-nez v0, :cond_5

    const/4 v0, -0x1

    :goto_2
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p1, Lcom/twitter/library/platform/e;->t:Ljava/util/List;

    if-eqz v0, :cond_6

    iget-object v0, p1, Lcom/twitter/library/platform/e;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p1, Lcom/twitter/library/platform/e;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/platform/notifications/a;

    invoke-virtual {v0, p0}, Lcom/twitter/library/platform/notifications/a;->a(Landroid/os/Parcel;)V

    goto :goto_3

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    :cond_5
    iget-object v0, p1, Lcom/twitter/library/platform/e;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_2

    :cond_6
    iget-object v0, p1, Lcom/twitter/library/platform/e;->q:[Lcom/twitter/library/platform/d;

    array-length v0, v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    :goto_4
    if-ge v2, v0, :cond_7

    iget-object v1, p1, Lcom/twitter/library/platform/e;->q:[Lcom/twitter/library/platform/d;

    aget-object v1, v1, v2

    iget v3, v1, Lcom/twitter/library/platform/d;->a:I

    invoke-virtual {p0, v3}, Landroid/os/Parcel;->writeInt(I)V

    iget v3, v1, Lcom/twitter/library/platform/d;->b:I

    invoke-virtual {p0, v3}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v3, v1, Lcom/twitter/library/platform/d;->c:Ljava/lang/String;

    invoke-virtual {p0, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v3, v1, Lcom/twitter/library/platform/d;->d:Ljava/lang/String;

    invoke-virtual {p0, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v3, v1, Lcom/twitter/library/platform/d;->e:Ljava/lang/String;

    invoke-virtual {p0, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v3, v1, Lcom/twitter/library/platform/d;->f:J

    invoke-virtual {p0, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_7
    iget v0, p1, Lcom/twitter/library/platform/e;->o:I

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method private b(Landroid/os/Parcel;)Lcom/twitter/library/platform/e;
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    if-eqz v0, :cond_1

    invoke-static {p1}, Lcom/twitter/library/platform/DataSyncResult;->a(Landroid/os/Parcel;)Lcom/twitter/library/platform/e;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b(Landroid/os/Parcel;Lcom/twitter/library/platform/e;)V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p2, :cond_1

    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    if-eqz v2, :cond_0

    invoke-static {p1, p2}, Lcom/twitter/library/platform/DataSyncResult;->a(Landroid/os/Parcel;Lcom/twitter/library/platform/e;)V

    :cond_0
    return-void

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public a()Z
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/platform/DataSyncResult;->f:Lcom/twitter/library/platform/e;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/library/platform/DataSyncResult;->f:Lcom/twitter/library/platform/e;

    iget v0, v0, Lcom/twitter/library/platform/e;->c:I

    add-int/2addr v0, v1

    :goto_0
    iget-object v2, p0, Lcom/twitter/library/platform/DataSyncResult;->g:Lcom/twitter/library/platform/e;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/library/platform/DataSyncResult;->g:Lcom/twitter/library/platform/e;

    iget v2, v2, Lcom/twitter/library/platform/e;->c:I

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lcom/twitter/library/platform/DataSyncResult;->i:Lcom/twitter/library/platform/e;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/twitter/library/platform/DataSyncResult;->i:Lcom/twitter/library/platform/e;

    iget v2, v2, Lcom/twitter/library/platform/e;->c:I

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Lcom/twitter/library/platform/DataSyncResult;->h:Lcom/twitter/library/platform/e;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/twitter/library/platform/DataSyncResult;->h:Lcom/twitter/library/platform/e;

    iget v2, v2, Lcom/twitter/library/platform/e;->c:I

    add-int/2addr v0, v2

    :cond_2
    if-lez v0, :cond_3

    const/4 v1, 0x1

    :cond_3
    return v1

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/platform/DataSyncResult;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/twitter/library/platform/DataSyncResult;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-boolean v0, p0, Lcom/twitter/library/platform/DataSyncResult;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/platform/DataSyncResult;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/platform/DataSyncResult;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/platform/DataSyncResult;->f:Lcom/twitter/library/platform/e;

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/platform/DataSyncResult;->b(Landroid/os/Parcel;Lcom/twitter/library/platform/e;)V

    iget-object v0, p0, Lcom/twitter/library/platform/DataSyncResult;->g:Lcom/twitter/library/platform/e;

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/platform/DataSyncResult;->b(Landroid/os/Parcel;Lcom/twitter/library/platform/e;)V

    iget-object v0, p0, Lcom/twitter/library/platform/DataSyncResult;->i:Lcom/twitter/library/platform/e;

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/platform/DataSyncResult;->b(Landroid/os/Parcel;Lcom/twitter/library/platform/e;)V

    iget-object v0, p0, Lcom/twitter/library/platform/DataSyncResult;->h:Lcom/twitter/library/platform/e;

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/platform/DataSyncResult;->b(Landroid/os/Parcel;Lcom/twitter/library/platform/e;)V

    iget-object v0, p0, Lcom/twitter/library/platform/DataSyncResult;->j:Lcom/twitter/library/platform/e;

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/platform/DataSyncResult;->b(Landroid/os/Parcel;Lcom/twitter/library/platform/e;)V

    iget-object v0, p0, Lcom/twitter/library/platform/DataSyncResult;->k:Lcom/twitter/library/platform/e;

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/platform/DataSyncResult;->b(Landroid/os/Parcel;Lcom/twitter/library/platform/e;)V

    iget-object v0, p0, Lcom/twitter/library/platform/DataSyncResult;->l:Lcom/twitter/library/platform/e;

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/platform/DataSyncResult;->b(Landroid/os/Parcel;Lcom/twitter/library/platform/e;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
