.class public Lcom/twitter/library/platform/notifications/g;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Lcom/twitter/library/platform/notifications/f;

.field public final b:Lcom/twitter/library/platform/notifications/f;

.field public final c:Lcom/twitter/library/platform/notifications/f;


# direct methods
.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/twitter/library/platform/notifications/f;

    invoke-direct {v0, p1}, Lcom/twitter/library/platform/notifications/f;-><init>(Landroid/os/Parcel;)V

    iput-object v0, p0, Lcom/twitter/library/platform/notifications/g;->a:Lcom/twitter/library/platform/notifications/f;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    new-instance v0, Lcom/twitter/library/platform/notifications/f;

    invoke-direct {v0, p1}, Lcom/twitter/library/platform/notifications/f;-><init>(Landroid/os/Parcel;)V

    iput-object v0, p0, Lcom/twitter/library/platform/notifications/g;->b:Lcom/twitter/library/platform/notifications/f;

    new-instance v0, Lcom/twitter/library/platform/notifications/f;

    invoke-direct {v0, p1}, Lcom/twitter/library/platform/notifications/f;-><init>(Landroid/os/Parcel;)V

    iput-object v0, p0, Lcom/twitter/library/platform/notifications/g;->c:Lcom/twitter/library/platform/notifications/f;

    :goto_0
    return-void

    :cond_0
    iput-object v2, p0, Lcom/twitter/library/platform/notifications/g;->b:Lcom/twitter/library/platform/notifications/f;

    iput-object v2, p0, Lcom/twitter/library/platform/notifications/g;->c:Lcom/twitter/library/platform/notifications/f;

    goto :goto_0
.end method

.method public constructor <init>(Lcom/twitter/library/platform/notifications/f;Lcom/twitter/library/platform/notifications/f;Lcom/twitter/library/platform/notifications/f;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/platform/notifications/g;->a:Lcom/twitter/library/platform/notifications/f;

    iput-object p2, p0, Lcom/twitter/library/platform/notifications/g;->b:Lcom/twitter/library/platform/notifications/f;

    if-nez p3, :cond_0

    :goto_0
    iput-object p2, p0, Lcom/twitter/library/platform/notifications/g;->c:Lcom/twitter/library/platform/notifications/f;

    return-void

    :cond_0
    move-object p2, p3

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/platform/notifications/g;->c:Lcom/twitter/library/platform/notifications/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/platform/notifications/g;->c:Lcom/twitter/library/platform/notifications/f;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/f;->e:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/os/Parcel;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/platform/notifications/g;->a:Lcom/twitter/library/platform/notifications/f;

    invoke-virtual {v0, p1}, Lcom/twitter/library/platform/notifications/f;->a(Landroid/os/Parcel;)V

    iget-object v0, p0, Lcom/twitter/library/platform/notifications/g;->b:Lcom/twitter/library/platform/notifications/f;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/platform/notifications/g;->b:Lcom/twitter/library/platform/notifications/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/platform/notifications/g;->b:Lcom/twitter/library/platform/notifications/f;

    invoke-virtual {v0, p1}, Lcom/twitter/library/platform/notifications/f;->a(Landroid/os/Parcel;)V

    iget-object v0, p0, Lcom/twitter/library/platform/notifications/g;->c:Lcom/twitter/library/platform/notifications/f;

    invoke-virtual {v0, p1}, Lcom/twitter/library/platform/notifications/f;->a(Landroid/os/Parcel;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
