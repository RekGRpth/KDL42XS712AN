.class public Lcom/twitter/library/service/n;
.super Lcom/twitter/internal/android/service/a;
.source "Twttr"


# instance fields
.field public final d:J

.field public final e:Ljava/lang/String;

.field private final f:Landroid/content/Context;

.field private final g:Landroid/app/NotificationManager;

.field private h:[I

.field private i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;JLjava/lang/String;)V
    .locals 1

    const-class v0, Lcom/twitter/library/service/n;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/internal/android/service/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/service/n;->f:Landroid/content/Context;

    const-string/jumbo v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/twitter/library/service/n;->g:Landroid/app/NotificationManager;

    iput-wide p2, p0, Lcom/twitter/library/service/n;->d:J

    iput-object p4, p0, Lcom/twitter/library/service/n;->e:Ljava/lang/String;

    return-void
.end method

.method private a(JLjava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/service/n;->f:Landroid/content/Context;

    invoke-static {v0, p1, p2, p3, p4}, Lcom/twitter/library/service/n;->a(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 9

    const/4 v4, 0x0

    const/4 v8, 0x0

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    const-string/jumbo v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/app/NotificationManager;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/provider/an;->a:Landroid/net/Uri;

    invoke-static {v1, p1, p2}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "notif_id"

    aput-object v3, v2, v8

    move-object v3, p4

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_2

    :cond_0
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {v6, v1}, Landroid/app/NotificationManager;->cancel(I)V

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_2
    invoke-virtual {v6, v8}, Landroid/app/NotificationManager;->cancel(I)V

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {p0}, Lcom/twitter/library/provider/f;->a(Landroid/content/Context;)Lcom/twitter/library/provider/f;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/twitter/library/provider/f;->b(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    invoke-virtual {v6, v0}, Landroid/app/NotificationManager;->cancel(I)V

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-static {p0, p1, p2}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v0

    invoke-virtual {v0, p4, v4}, Lcom/twitter/library/provider/az;->b(Ljava/lang/String;[Ljava/lang/String;)I

    return-object v7
.end method


# virtual methods
.method public a(I)Lcom/twitter/library/service/n;
    .locals 0

    iput p1, p0, Lcom/twitter/library/service/n;->i:I

    return-object p0
.end method

.method public a([I)Lcom/twitter/library/service/n;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/service/n;->h:[I

    return-object p0
.end method

.method protected a(Lcom/twitter/internal/android/service/AsyncService;)Ljava/util/List;
    .locals 6

    iget-wide v0, p0, Lcom/twitter/library/service/n;->d:J

    iget-object v2, p0, Lcom/twitter/library/service/n;->e:Ljava/lang/String;

    iget v3, p0, Lcom/twitter/library/service/n;->i:I

    packed-switch v3, :pswitch_data_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :pswitch_0
    iget-object v2, p0, Lcom/twitter/library/service/n;->h:[I

    iget-object v3, p0, Lcom/twitter/library/service/n;->f:Landroid/content/Context;

    invoke-static {v3, v0, v1}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/library/provider/az;->a([I)I

    new-instance v0, Ljava/util/ArrayList;

    array-length v1, v2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    array-length v3, v2

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_0

    aget v4, v2, v1

    iget-object v5, p0, Lcom/twitter/library/service/n;->g:Landroid/app/NotificationManager;

    invoke-virtual {v5, v4}, Landroid/app/NotificationManager;->cancel(I)V

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :pswitch_1
    const-string/jumbo v3, "type IN(4,3,5,2,12,6)"

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/twitter/library/service/n;->a(JLjava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const-string/jumbo v3, "type=7 "

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/twitter/library/service/n;->a(JLjava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected synthetic b(Lcom/twitter/internal/android/service/AsyncService;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/library/service/n;->a(Lcom/twitter/internal/android/service/AsyncService;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
