.class public Lcom/twitter/library/service/AuthTokenService;
.super Landroid/app/Service;
.source "Twttr"


# instance fields
.field private a:Landroid/os/Looper;

.field private b:Landroid/os/Handler;

.field private c:Landroid/os/Handler;

.field private d:Lcom/twitter/library/network/aa;

.field private final e:Landroid/os/IBinder;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lcom/twitter/library/service/g;

    invoke-direct {v0, p0}, Lcom/twitter/library/service/g;-><init>(Lcom/twitter/library/service/AuthTokenService;)V

    iput-object v0, p0, Lcom/twitter/library/service/AuthTokenService;->e:Landroid/os/IBinder;

    return-void
.end method

.method private a(Lcom/twitter/library/service/i;ILcom/twitter/library/network/OAuthToken;Ljava/lang/String;J)V
    .locals 9

    iget-object v8, p0, Lcom/twitter/library/service/AuthTokenService;->c:Landroid/os/Handler;

    new-instance v0, Lcom/twitter/library/service/j;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move-wide v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/service/j;-><init>(Lcom/twitter/library/service/AuthTokenService;Lcom/twitter/library/service/i;ILcom/twitter/library/network/OAuthToken;Ljava/lang/String;J)V

    invoke-virtual {v8, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private c(Lcom/twitter/library/service/i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    const-wide/16 v5, 0x0

    const/4 v3, 0x0

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-static {p4}, Lcom/twitter/library/util/a;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/library/util/a;->b(Landroid/accounts/AccountManager;Landroid/accounts/Account;)Lcom/twitter/library/network/OAuthToken;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/service/AuthTokenService;->a(Lcom/twitter/library/service/i;ILcom/twitter/library/network/OAuthToken;Ljava/lang/String;J)V

    :goto_0
    return-void

    :cond_0
    new-instance v1, Lcom/twitter/library/network/n;

    invoke-direct {v1, v0}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/twitter/library/service/AuthTokenService;->d:Lcom/twitter/library/network/aa;

    iget-object v2, v2, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "/oauth/access_token"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "x_reverse_auth_target"

    invoke-static {v0, v2, p2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "x_reverse_auth_parameters"

    invoke-static {v0, v2, p3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    const/16 v4, 0x200

    invoke-direct {v2, v4}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v4, Lcom/twitter/internal/network/e;

    invoke-direct {v4, v2, v3}, Lcom/twitter/internal/network/e;-><init>(Ljava/io/OutputStream;Lcom/twitter/internal/network/p;)V

    new-instance v7, Lcom/twitter/library/network/d;

    invoke-direct {v7, p0, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    sget-object v0, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v7, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/twitter/library/network/n;->a(Ljava/lang/String;Z)Ljava/util/TreeMap;

    move-result-object v5

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v0

    iget v2, v0, Lcom/twitter/internal/network/k;->a:I

    new-instance v3, Lcom/twitter/library/network/OAuthToken;

    const-string/jumbo v0, "oauth_token"

    invoke-virtual {v5, v0}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string/jumbo v1, "oauth_token_secret"

    invoke-virtual {v5, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v3, v0, v1}, Lcom/twitter/library/network/OAuthToken;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "screen_name"

    invoke-virtual {v5, v0}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string/jumbo v0, "user_id"

    invoke-virtual {v5, v0}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/service/AuthTokenService;->a(Lcom/twitter/library/service/i;ILcom/twitter/library/network/OAuthToken;Ljava/lang/String;J)V

    goto/16 :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v0

    iget v2, v0, Lcom/twitter/internal/network/k;->a:I

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/service/AuthTokenService;->a(Lcom/twitter/library/service/i;ILcom/twitter/library/network/OAuthToken;Ljava/lang/String;J)V

    goto/16 :goto_0
.end method


# virtual methods
.method public a(Lcom/twitter/library/service/i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7

    iget-object v6, p0, Lcom/twitter/library/service/AuthTokenService;->b:Landroid/os/Handler;

    new-instance v0, Lcom/twitter/library/service/h;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/service/h;-><init>(Lcom/twitter/library/service/AuthTokenService;Lcom/twitter/library/service/i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move-result v0

    return v0
.end method

.method b(Lcom/twitter/library/service/i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    const/4 v3, 0x0

    new-instance v0, Lcom/twitter/library/network/n;

    invoke-direct {v0, v3, p2, p3}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/twitter/library/service/AuthTokenService;->d:Lcom/twitter/library/network/aa;

    iget-object v2, v2, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "/oauth/request_token"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "x_auth_mode"

    const-string/jumbo v4, "reverse_auth"

    invoke-static {v1, v2, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    const/16 v4, 0x200

    invoke-direct {v2, v4}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v4, Lcom/twitter/internal/network/e;

    invoke-direct {v4, v2, v3}, Lcom/twitter/internal/network/e;-><init>(Ljava/io/OutputStream;Lcom/twitter/internal/network/p;)V

    new-instance v5, Lcom/twitter/library/network/d;

    invoke-direct {v5, p0, v1}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    sget-object v1, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v5, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    invoke-direct {p0, p1, p2, v0, p4}, Lcom/twitter/library/service/AuthTokenService;->c(Lcom/twitter/library/service/i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const/4 v2, 0x0

    const-wide/16 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/service/AuthTokenService;->a(Lcom/twitter/library/service/i;ILcom/twitter/library/network/OAuthToken;Ljava/lang/String;J)V

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/service/AuthTokenService;->e:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-static {p0}, Lcom/twitter/library/network/aa;->a(Landroid/content/Context;)Lcom/twitter/library/network/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/service/AuthTokenService;->d:Lcom/twitter/library/network/aa;

    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "AuthTokenService"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/service/AuthTokenService;->a:Landroid/os/Looper;

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/library/service/AuthTokenService;->a:Landroid/os/Looper;

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/library/service/AuthTokenService;->b:Landroid/os/Handler;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/service/AuthTokenService;->c:Landroid/os/Handler;

    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    iget-object v0, p0, Lcom/twitter/library/service/AuthTokenService;->a:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    return-void
.end method
