.class Lcom/twitter/library/service/j;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/twitter/library/service/AuthTokenService;

.field private final b:Lcom/twitter/library/service/i;

.field private final c:Lcom/twitter/library/network/OAuthToken;

.field private final d:I

.field private final e:Ljava/lang/String;

.field private final f:J


# direct methods
.method public constructor <init>(Lcom/twitter/library/service/AuthTokenService;Lcom/twitter/library/service/i;ILcom/twitter/library/network/OAuthToken;Ljava/lang/String;J)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/service/j;->a:Lcom/twitter/library/service/AuthTokenService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/twitter/library/service/j;->b:Lcom/twitter/library/service/i;

    iput p3, p0, Lcom/twitter/library/service/j;->d:I

    iput-object p4, p0, Lcom/twitter/library/service/j;->c:Lcom/twitter/library/network/OAuthToken;

    iput-object p5, p0, Lcom/twitter/library/service/j;->e:Ljava/lang/String;

    iput-wide p6, p0, Lcom/twitter/library/service/j;->f:J

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v0, p0, Lcom/twitter/library/service/j;->b:Lcom/twitter/library/service/i;

    iget v1, p0, Lcom/twitter/library/service/j;->d:I

    iget-object v2, p0, Lcom/twitter/library/service/j;->c:Lcom/twitter/library/network/OAuthToken;

    iget-object v3, p0, Lcom/twitter/library/service/j;->e:Ljava/lang/String;

    iget-wide v4, p0, Lcom/twitter/library/service/j;->f:J

    invoke-interface/range {v0 .. v5}, Lcom/twitter/library/service/i;->a(ILcom/twitter/library/network/OAuthToken;Ljava/lang/String;J)V

    return-void
.end method
