.class public final enum Lcom/twitter/library/service/TimelineHelper$FilterType;
.super Ljava/lang/Enum;
.source "Twttr"


# static fields
.field public static final enum a:Lcom/twitter/library/service/TimelineHelper$FilterType;

.field public static final enum b:Lcom/twitter/library/service/TimelineHelper$FilterType;

.field public static final enum c:Lcom/twitter/library/service/TimelineHelper$FilterType;

.field private static final synthetic d:[Lcom/twitter/library/service/TimelineHelper$FilterType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/library/service/TimelineHelper$FilterType;

    const-string/jumbo v1, "None"

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/service/TimelineHelper$FilterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/service/TimelineHelper$FilterType;->a:Lcom/twitter/library/service/TimelineHelper$FilterType;

    new-instance v0, Lcom/twitter/library/service/TimelineHelper$FilterType;

    const-string/jumbo v1, "FavoritePeople"

    invoke-direct {v0, v1, v3}, Lcom/twitter/library/service/TimelineHelper$FilterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/service/TimelineHelper$FilterType;->b:Lcom/twitter/library/service/TimelineHelper$FilterType;

    new-instance v0, Lcom/twitter/library/service/TimelineHelper$FilterType;

    const-string/jumbo v1, "MediaOnly"

    invoke-direct {v0, v1, v4}, Lcom/twitter/library/service/TimelineHelper$FilterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/service/TimelineHelper$FilterType;->c:Lcom/twitter/library/service/TimelineHelper$FilterType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/twitter/library/service/TimelineHelper$FilterType;

    sget-object v1, Lcom/twitter/library/service/TimelineHelper$FilterType;->a:Lcom/twitter/library/service/TimelineHelper$FilterType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/twitter/library/service/TimelineHelper$FilterType;->b:Lcom/twitter/library/service/TimelineHelper$FilterType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/twitter/library/service/TimelineHelper$FilterType;->c:Lcom/twitter/library/service/TimelineHelper$FilterType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/twitter/library/service/TimelineHelper$FilterType;->d:[Lcom/twitter/library/service/TimelineHelper$FilterType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/library/service/TimelineHelper$FilterType;
    .locals 1

    const-class v0, Lcom/twitter/library/service/TimelineHelper$FilterType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/TimelineHelper$FilterType;

    return-object v0
.end method

.method public static values()[Lcom/twitter/library/service/TimelineHelper$FilterType;
    .locals 1

    sget-object v0, Lcom/twitter/library/service/TimelineHelper$FilterType;->d:[Lcom/twitter/library/service/TimelineHelper$FilterType;

    invoke-virtual {v0}, [Lcom/twitter/library/service/TimelineHelper$FilterType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/service/TimelineHelper$FilterType;

    return-object v0
.end method
