.class public abstract Lcom/twitter/library/service/b;
.super Lcom/twitter/internal/android/service/a;
.source "Twttr"


# static fields
.field private static final d:Z

.field public static final h:[I

.field public static final i:[Ljava/lang/String;

.field public static final j:[I


# instance fields
.field private e:I

.field private f:Z

.field private g:I

.field public final k:Landroid/os/Bundle;

.field protected final l:Landroid/content/Context;

.field protected final m:Lcom/twitter/library/network/aa;

.field private n:Lcom/twitter/library/service/p;

.field private o:Ljava/lang/ref/WeakReference;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x0

    new-array v1, v0, [I

    sput-object v1, Lcom/twitter/library/service/b;->h:[I

    new-array v1, v0, [Ljava/lang/String;

    sput-object v1, Lcom/twitter/library/service/b;->i:[Ljava/lang/String;

    new-array v1, v0, [I

    sput-object v1, Lcom/twitter/library/service/b;->j:[I

    invoke-static {}, Lcom/twitter/library/client/App;->m()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "APIRequest"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    sput-boolean v0, Lcom/twitter/library/service/b;->d:Z

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p2}, Lcom/twitter/internal/android/service/a;-><init>(Ljava/lang/String;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/service/b;->k:Landroid/os/Bundle;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/service/b;->l:Landroid/content/Context;

    iget-object v0, p0, Lcom/twitter/library/service/b;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/network/aa;->a(Landroid/content/Context;)Lcom/twitter/library/network/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/service/b;->m:Lcom/twitter/library/network/aa;

    new-instance v0, Lcom/twitter/library/service/f;

    invoke-direct {v0}, Lcom/twitter/library/service/f;-><init>()V

    invoke-virtual {p0, v0}, Lcom/twitter/library/service/b;->a(Lcom/twitter/internal/android/service/l;)Lcom/twitter/internal/android/service/a;

    return-void
.end method

.method public static b(Landroid/os/Bundle;)[I
    .locals 1

    const-string/jumbo v0, "custom_errors"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "custom_errors"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/twitter/library/service/b;->h:[I

    goto :goto_0
.end method

.method public static c(Ljava/util/ArrayList;)[I
    .locals 4

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v1, v3, [I

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/al;

    iget v0, v0, Lcom/twitter/library/api/al;->a:I

    aput v0, v1, v2

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_1
    return-object v0

    :cond_1
    sget-object v0, Lcom/twitter/library/service/b;->h:[I

    goto :goto_1
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/e;)Lcom/twitter/internal/network/HttpOperation;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/twitter/library/service/p;)Lcom/twitter/library/service/b;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/service/b;->n:Lcom/twitter/library/service/p;

    return-object p0
.end method

.method public final a(Ljava/lang/String;D)Lcom/twitter/library/service/b;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/service/b;->k:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2, p3}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    return-object p0
.end method

.method public final a(Ljava/lang/String;I)Lcom/twitter/library/service/b;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/service/b;->k:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object p0
.end method

.method public final a(Ljava/lang/String;J)Lcom/twitter/library/service/b;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/service/b;->k:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    return-object p0
.end method

.method public final a(Ljava/lang/String;Landroid/os/Parcelable;)Lcom/twitter/library/service/b;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/service/b;->k:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/io/Serializable;)Lcom/twitter/library/service/b;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/service/b;->k:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    return-object p0
.end method

.method public final a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/service/b;->k:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-object p0
.end method

.method public final a(Ljava/lang/String;[I)Lcom/twitter/library/service/b;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/service/b;->k:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    return-object p0
.end method

.method public final a(Ljava/lang/String;[J)Lcom/twitter/library/service/b;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/service/b;->k:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    return-object p0
.end method

.method public final a(Ljava/lang/String;[Landroid/os/Parcelable;)Lcom/twitter/library/service/b;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/service/b;->k:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    return-object p0
.end method

.method public final a(Ljava/lang/String;[Ljava/lang/CharSequence;)Lcom/twitter/library/service/b;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/service/b;->k:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putCharSequenceArray(Ljava/lang/String;[Ljava/lang/CharSequence;)V

    return-object p0
.end method

.method protected final a(Lcom/twitter/internal/android/service/a;)Lcom/twitter/library/service/e;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/library/service/b;->v()Lcom/twitter/internal/android/service/AsyncService;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/service/a;->c(Lcom/twitter/internal/android/service/AsyncService;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    return-object v0
.end method

.method protected a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)V
    .locals 0

    invoke-virtual {p2, p1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/service/b;->k:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method protected synthetic b(Lcom/twitter/internal/android/service/AsyncService;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/library/service/b;->d(Lcom/twitter/internal/android/service/AsyncService;)Lcom/twitter/library/service/e;

    move-result-object v0

    return-object v0
.end method

.method protected final b(Lcom/twitter/internal/android/service/a;)V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/library/service/b;->v()Lcom/twitter/internal/android/service/AsyncService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/service/AsyncService;->a(Lcom/twitter/internal/android/service/a;)V

    return-void
.end method

.method protected b(Lcom/twitter/library/service/e;)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/twitter/library/service/b;->a(Lcom/twitter/library/service/e;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Do NOT call HttpOperation.execute() from onCreateHttpOperation()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/twitter/library/service/b;->a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)V

    :cond_1
    return-void
.end method

.method public final c(I)Lcom/twitter/library/service/b;
    .locals 0

    iput p1, p0, Lcom/twitter/library/service/b;->e:I

    return-object p0
.end method

.method public final c(Landroid/os/Bundle;)Lcom/twitter/library/service/b;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/service/b;->k:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    return-object p0
.end method

.method public final c(Z)Lcom/twitter/library/service/b;
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/service/b;->f:Z

    return-object p0
.end method

.method public final d(I)Lcom/twitter/library/service/b;
    .locals 0

    iput p1, p0, Lcom/twitter/library/service/b;->g:I

    return-object p0
.end method

.method protected final d(Lcom/twitter/internal/android/service/AsyncService;)Lcom/twitter/library/service/e;
    .locals 4

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/library/service/b;->o:Ljava/lang/ref/WeakReference;

    new-instance v0, Lcom/twitter/library/service/e;

    invoke-direct {v0}, Lcom/twitter/library/service/e;-><init>()V

    invoke-virtual {p0, v0}, Lcom/twitter/library/service/b;->b(Lcom/twitter/library/service/e;)V

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->f()Lcom/twitter/library/api/RateLimit;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, v0, Lcom/twitter/library/service/e;->a:Landroid/os/Bundle;

    const-string/jumbo v3, "rate_limit"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    invoke-virtual {v0}, Lcom/twitter/library/service/e;->b()Ljava/lang/Exception;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/twitter/library/service/b;->l:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Ljava/lang/Throwable;)V

    :cond_1
    sget-boolean v1, Lcom/twitter/library/service/b;->d:Z

    if-eqz v1, :cond_2

    const-string/jumbo v1, "APIRequest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Action complete: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/twitter/library/service/b;->e:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", success: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v1, p0, Lcom/twitter/library/service/b;->o:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->clear()V

    return-object v0
.end method

.method public final r()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/service/b;->e:I

    return v0
.end method

.method public final s()Lcom/twitter/library/service/p;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/service/b;->n:Lcom/twitter/library/service/p;

    return-object v0
.end method

.method public final t()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/service/b;->f:Z

    return v0
.end method

.method public final u()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/service/b;->g:I

    return v0
.end method

.method protected final v()Lcom/twitter/internal/android/service/AsyncService;
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/service/b;->o:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/service/b;->o:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/service/AsyncService;

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "queueAndExecute cannot be executed outside of onExecute"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v0
.end method
