.class public abstract Lcom/twitter/library/service/o;
.super Lcom/twitter/library/service/b;
.source "Twttr"


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V
    .locals 1

    new-instance v0, Lcom/twitter/library/service/p;

    invoke-direct {v0, p3}, Lcom/twitter/library/service/p;-><init>(Lcom/twitter/library/client/Session;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/p;)V

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/p;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "SessionStamp cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0, p3}, Lcom/twitter/library/service/o;->a(Lcom/twitter/library/service/p;)Lcom/twitter/library/service/b;

    return-void
.end method


# virtual methods
.method protected final w()Lcom/twitter/library/provider/az;
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/service/o;->l:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/twitter/library/service/o;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    iget-wide v1, v1, Lcom/twitter/library/service/p;->c:J

    invoke-static {v0, v1, v2}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v0

    return-object v0
.end method
