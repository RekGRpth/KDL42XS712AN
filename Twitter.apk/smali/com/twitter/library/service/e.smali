.class public Lcom/twitter/library/service/e;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Landroid/os/Bundle;

.field private b:I

.field private c:Z

.field private d:Ljava/lang/Exception;

.field private e:Ljava/lang/String;

.field private f:Lcom/twitter/internal/network/k;

.field private g:Lcom/twitter/library/api/RateLimit;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/service/e;->a:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v0}, Lcom/twitter/library/service/e;->a(ILjava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public a(ILjava/lang/Exception;)V
    .locals 1

    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/library/service/e;->a(ILjava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/library/service/e;->a(ILjava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public a(ILjava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/service/e;->c:Z

    iput p1, p0, Lcom/twitter/library/service/e;->b:I

    iput-object p2, p0, Lcom/twitter/library/service/e;->e:Ljava/lang/String;

    iput-object p3, p0, Lcom/twitter/library/service/e;->d:Ljava/lang/Exception;

    return-void
.end method

.method public a(Lcom/twitter/internal/network/HttpOperation;)V
    .locals 2

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->k()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/service/e;->c:Z

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v0

    iget v1, v0, Lcom/twitter/internal/network/k;->a:I

    iput v1, p0, Lcom/twitter/library/service/e;->b:I

    iget-object v1, v0, Lcom/twitter/internal/network/k;->c:Ljava/lang/Exception;

    iput-object v1, p0, Lcom/twitter/library/service/e;->d:Ljava/lang/Exception;

    iget-object v1, v0, Lcom/twitter/internal/network/k;->b:Ljava/lang/String;

    iput-object v1, p0, Lcom/twitter/library/service/e;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/service/e;->f:Lcom/twitter/internal/network/k;

    invoke-static {p1}, Lcom/twitter/library/network/aa;->a(Lcom/twitter/internal/network/HttpOperation;)Lcom/twitter/library/api/RateLimit;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/service/e;->g:Lcom/twitter/library/api/RateLimit;

    return-void
.end method

.method public a(Lcom/twitter/library/service/e;)V
    .locals 2

    iget-boolean v0, p1, Lcom/twitter/library/service/e;->c:Z

    iput-boolean v0, p0, Lcom/twitter/library/service/e;->c:Z

    iget v0, p1, Lcom/twitter/library/service/e;->b:I

    iput v0, p0, Lcom/twitter/library/service/e;->b:I

    iget-object v0, p1, Lcom/twitter/library/service/e;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/service/e;->e:Ljava/lang/String;

    iget-object v0, p1, Lcom/twitter/library/service/e;->d:Ljava/lang/Exception;

    iput-object v0, p0, Lcom/twitter/library/service/e;->d:Ljava/lang/Exception;

    iget-object v0, p1, Lcom/twitter/library/service/e;->f:Lcom/twitter/internal/network/k;

    iput-object v0, p0, Lcom/twitter/library/service/e;->f:Lcom/twitter/internal/network/k;

    iget-object v0, p1, Lcom/twitter/library/service/e;->g:Lcom/twitter/library/api/RateLimit;

    iput-object v0, p0, Lcom/twitter/library/service/e;->g:Lcom/twitter/library/api/RateLimit;

    iget-object v0, p0, Lcom/twitter/library/service/e;->a:Landroid/os/Bundle;

    iget-object v1, p1, Lcom/twitter/library/service/e;->a:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/service/e;->c:Z

    return-void
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/service/e;->c:Z

    return v0
.end method

.method public b()Ljava/lang/Exception;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/service/e;->d:Ljava/lang/Exception;

    return-object v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/service/e;->b:I

    return v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/service/e;->e:Ljava/lang/String;

    return-object v0
.end method

.method public e()Lcom/twitter/internal/network/k;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/service/e;->f:Lcom/twitter/internal/network/k;

    return-object v0
.end method

.method public f()Lcom/twitter/library/api/RateLimit;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/service/e;->g:Lcom/twitter/library/api/RateLimit;

    return-object v0
.end method
