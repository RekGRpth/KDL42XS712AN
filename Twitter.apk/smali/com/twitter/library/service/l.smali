.class public final Lcom/twitter/library/service/l;
.super Lcom/twitter/internal/android/service/l;
.source "Twttr"


# instance fields
.field private final a:Ljava/util/HashSet;

.field private final b:J

.field private final c:J

.field private final d:I

.field private e:I


# direct methods
.method public constructor <init>(I)V
    .locals 7

    const-wide/16 v2, 0x1

    const-wide/16 v4, 0x8

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    move-object v0, p0

    move v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/service/l;-><init>(IJJLjava/util/concurrent/TimeUnit;)V

    return-void
.end method

.method public constructor <init>(IJJLjava/util/concurrent/TimeUnit;)V
    .locals 8

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x1f4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x1f5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x1f6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0x1f7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const/16 v2, 0x1f8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const/16 v2, 0x1f9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    move-object v0, p0

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/service/l;-><init>(IJJLjava/util/concurrent/TimeUnit;Ljava/util/Collection;)V

    return-void
.end method

.method public constructor <init>(IJJLjava/util/concurrent/TimeUnit;Ljava/util/Collection;)V
    .locals 2

    invoke-direct {p0}, Lcom/twitter/internal/android/service/l;-><init>()V

    iput p1, p0, Lcom/twitter/library/service/l;->d:I

    invoke-virtual {p6, p2, p3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/service/l;->b:J

    invoke-virtual {p6, p4, p5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/service/l;->c:J

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p7}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/twitter/library/service/l;->a:Ljava/util/HashSet;

    return-void
.end method

.method private a(IJ)J
    .locals 4

    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    int-to-double v2, p1

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    long-to-double v2, p2

    mul-double/2addr v0, v2

    double-to-long v0, v0

    return-wide v0
.end method


# virtual methods
.method public a(Lcom/twitter/internal/android/service/k;)Z
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v2

    if-nez v2, :cond_0

    iget v2, p0, Lcom/twitter/library/service/l;->d:I

    if-lez v2, :cond_1

    iget v2, p0, Lcom/twitter/library/service/l;->e:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/twitter/library/service/l;->e:I

    iget v3, p0, Lcom/twitter/library/service/l;->d:I

    if-lt v2, v3, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    invoke-virtual {v0}, Lcom/twitter/library/service/e;->e()Lcom/twitter/internal/network/k;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/twitter/library/service/l;->a:Ljava/util/HashSet;

    iget v0, v0, Lcom/twitter/internal/network/k;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public b(Lcom/twitter/internal/android/service/k;)J
    .locals 5

    iget-wide v0, p0, Lcom/twitter/library/service/l;->c:J

    iget v2, p0, Lcom/twitter/library/service/l;->e:I

    iget-wide v3, p0, Lcom/twitter/library/service/l;->b:J

    invoke-direct {p0, v2, v3, v4}, Lcom/twitter/library/service/l;->a(IJ)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method
