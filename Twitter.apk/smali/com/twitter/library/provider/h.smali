.class final Lcom/twitter/library/provider/h;
.super Landroid/content/UriMatcher;
.source "Twttr"


# direct methods
.method constructor <init>(I)V
    .locals 3

    invoke-direct {p0, p1}, Landroid/content/UriMatcher;-><init>(I)V

    sget-object v0, Lcom/twitter/library/provider/GlobalDatabaseProvider;->a:Ljava/lang/String;

    const-string/jumbo v1, "user_values"

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/library/provider/h;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/library/provider/GlobalDatabaseProvider;->a:Ljava/lang/String;

    const-string/jumbo v1, "user_values/#"

    const/4 v2, 0x2

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/library/provider/h;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/library/provider/GlobalDatabaseProvider;->a:Ljava/lang/String;

    const-string/jumbo v1, "activity_states"

    const/4 v2, 0x3

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/library/provider/h;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/library/provider/GlobalDatabaseProvider;->a:Ljava/lang/String;

    const-string/jumbo v1, "activity_states/*"

    const/4 v2, 0x4

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/library/provider/h;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/library/provider/GlobalDatabaseProvider;->a:Ljava/lang/String;

    const-string/jumbo v1, "account_settings"

    const/4 v2, 0x5

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/library/provider/h;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/twitter/library/provider/GlobalDatabaseProvider;->a:Ljava/lang/String;

    const-string/jumbo v1, "account_settings/*"

    const/4 v2, 0x6

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/library/provider/h;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method
