.class public final enum Lcom/twitter/library/provider/NotificationSetting;
.super Ljava/lang/Enum;
.source "Twttr"


# static fields
.field public static final enum a:Lcom/twitter/library/provider/NotificationSetting;

.field public static final enum b:Lcom/twitter/library/provider/NotificationSetting;

.field public static final enum c:Lcom/twitter/library/provider/NotificationSetting;

.field public static final enum d:Lcom/twitter/library/provider/NotificationSetting;

.field public static final enum e:Lcom/twitter/library/provider/NotificationSetting;

.field public static final enum f:Lcom/twitter/library/provider/NotificationSetting;

.field public static final enum g:Lcom/twitter/library/provider/NotificationSetting;

.field public static final enum h:Lcom/twitter/library/provider/NotificationSetting;

.field public static final enum i:Lcom/twitter/library/provider/NotificationSetting;

.field public static final enum j:Lcom/twitter/library/provider/NotificationSetting;

.field public static final enum k:Lcom/twitter/library/provider/NotificationSetting;

.field public static final enum l:Lcom/twitter/library/provider/NotificationSetting;

.field public static final enum m:Lcom/twitter/library/provider/NotificationSetting;

.field public static final enum n:Lcom/twitter/library/provider/NotificationSetting;

.field private static final synthetic o:[Lcom/twitter/library/provider/NotificationSetting;


# instance fields
.field public final enabledFor:I

.field public final enabledForAll:I

.field public final settingEnabled:I

.field public final settingEnabledAll:I


# direct methods
.method static constructor <clinit>()V
    .locals 14

    const/4 v3, 0x2

    const/16 v5, 0x8

    const/4 v6, 0x4

    const/4 v4, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/library/provider/NotificationSetting;

    const-string/jumbo v1, "MENTIONS"

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/provider/NotificationSetting;-><init>(Ljava/lang/String;IIIII)V

    sput-object v0, Lcom/twitter/library/provider/NotificationSetting;->a:Lcom/twitter/library/provider/NotificationSetting;

    new-instance v7, Lcom/twitter/library/provider/NotificationSetting;

    const-string/jumbo v8, "RETWEETS"

    const/16 v12, 0x100

    const/16 v13, 0x80

    move v9, v4

    move v10, v5

    move v11, v6

    invoke-direct/range {v7 .. v13}, Lcom/twitter/library/provider/NotificationSetting;-><init>(Ljava/lang/String;IIIII)V

    sput-object v7, Lcom/twitter/library/provider/NotificationSetting;->b:Lcom/twitter/library/provider/NotificationSetting;

    new-instance v7, Lcom/twitter/library/provider/NotificationSetting;

    const-string/jumbo v8, "FAVORITES"

    const/16 v10, 0x20

    const/16 v11, 0x10

    const/16 v12, 0x40

    const/16 v13, 0x20

    move v9, v3

    invoke-direct/range {v7 .. v13}, Lcom/twitter/library/provider/NotificationSetting;-><init>(Ljava/lang/String;IIIII)V

    sput-object v7, Lcom/twitter/library/provider/NotificationSetting;->c:Lcom/twitter/library/provider/NotificationSetting;

    new-instance v7, Lcom/twitter/library/provider/NotificationSetting;

    const-string/jumbo v8, "FOLLOWS"

    const/4 v9, 0x3

    const/16 v11, 0x40

    const/16 v13, 0x10

    move v10, v2

    move v12, v2

    invoke-direct/range {v7 .. v13}, Lcom/twitter/library/provider/NotificationSetting;-><init>(Ljava/lang/String;IIIII)V

    sput-object v7, Lcom/twitter/library/provider/NotificationSetting;->d:Lcom/twitter/library/provider/NotificationSetting;

    new-instance v7, Lcom/twitter/library/provider/NotificationSetting;

    const-string/jumbo v8, "MESSAGES"

    move v9, v6

    move v10, v2

    move v11, v4

    move v12, v2

    move v13, v4

    invoke-direct/range {v7 .. v13}, Lcom/twitter/library/provider/NotificationSetting;-><init>(Ljava/lang/String;IIIII)V

    sput-object v7, Lcom/twitter/library/provider/NotificationSetting;->e:Lcom/twitter/library/provider/NotificationSetting;

    new-instance v7, Lcom/twitter/library/provider/NotificationSetting;

    const-string/jumbo v8, "LIFELINE_ALERTS"

    const/4 v9, 0x5

    const/16 v13, 0x2000

    move v10, v2

    move v11, v4

    move v12, v2

    invoke-direct/range {v7 .. v13}, Lcom/twitter/library/provider/NotificationSetting;-><init>(Ljava/lang/String;IIIII)V

    sput-object v7, Lcom/twitter/library/provider/NotificationSetting;->f:Lcom/twitter/library/provider/NotificationSetting;

    new-instance v7, Lcom/twitter/library/provider/NotificationSetting;

    const-string/jumbo v8, "TWEETS"

    const/4 v9, 0x6

    const/16 v13, 0x200

    move v10, v2

    move v11, v4

    move v12, v2

    invoke-direct/range {v7 .. v13}, Lcom/twitter/library/provider/NotificationSetting;-><init>(Ljava/lang/String;IIIII)V

    sput-object v7, Lcom/twitter/library/provider/NotificationSetting;->g:Lcom/twitter/library/provider/NotificationSetting;

    new-instance v7, Lcom/twitter/library/provider/NotificationSetting;

    const-string/jumbo v8, "ADDRESS_BOOK"

    const/4 v9, 0x7

    const/16 v11, 0x100

    const/16 v13, 0x800

    move v10, v2

    move v12, v2

    invoke-direct/range {v7 .. v13}, Lcom/twitter/library/provider/NotificationSetting;-><init>(Ljava/lang/String;IIIII)V

    sput-object v7, Lcom/twitter/library/provider/NotificationSetting;->h:Lcom/twitter/library/provider/NotificationSetting;

    new-instance v7, Lcom/twitter/library/provider/NotificationSetting;

    const-string/jumbo v8, "EXPERIMENTAL"

    const/16 v13, 0x1000

    move v9, v5

    move v10, v2

    move v11, v4

    move v12, v2

    invoke-direct/range {v7 .. v13}, Lcom/twitter/library/provider/NotificationSetting;-><init>(Ljava/lang/String;IIIII)V

    sput-object v7, Lcom/twitter/library/provider/NotificationSetting;->i:Lcom/twitter/library/provider/NotificationSetting;

    new-instance v7, Lcom/twitter/library/provider/NotificationSetting;

    const-string/jumbo v8, "RECOMMENDATIONS"

    const/16 v9, 0x9

    const/16 v13, 0x4000

    move v10, v2

    move v11, v4

    move v12, v2

    invoke-direct/range {v7 .. v13}, Lcom/twitter/library/provider/NotificationSetting;-><init>(Ljava/lang/String;IIIII)V

    sput-object v7, Lcom/twitter/library/provider/NotificationSetting;->j:Lcom/twitter/library/provider/NotificationSetting;

    new-instance v7, Lcom/twitter/library/provider/NotificationSetting;

    const-string/jumbo v8, "NEWS"

    const/16 v9, 0xa

    const/high16 v13, 0x20000

    move v10, v2

    move v11, v4

    move v12, v2

    invoke-direct/range {v7 .. v13}, Lcom/twitter/library/provider/NotificationSetting;-><init>(Ljava/lang/String;IIIII)V

    sput-object v7, Lcom/twitter/library/provider/NotificationSetting;->k:Lcom/twitter/library/provider/NotificationSetting;

    new-instance v7, Lcom/twitter/library/provider/NotificationSetting;

    const-string/jumbo v8, "RETWEETED_MENTION"

    const/16 v9, 0xb

    const/16 v11, 0x200

    const v13, 0x8000

    move v10, v2

    move v12, v2

    invoke-direct/range {v7 .. v13}, Lcom/twitter/library/provider/NotificationSetting;-><init>(Ljava/lang/String;IIIII)V

    sput-object v7, Lcom/twitter/library/provider/NotificationSetting;->l:Lcom/twitter/library/provider/NotificationSetting;

    new-instance v7, Lcom/twitter/library/provider/NotificationSetting;

    const-string/jumbo v8, "FAVORITED_MENTION"

    const/16 v9, 0xc

    const/16 v11, 0x400

    const/high16 v13, 0x10000

    move v10, v2

    move v12, v2

    invoke-direct/range {v7 .. v13}, Lcom/twitter/library/provider/NotificationSetting;-><init>(Ljava/lang/String;IIIII)V

    sput-object v7, Lcom/twitter/library/provider/NotificationSetting;->m:Lcom/twitter/library/provider/NotificationSetting;

    new-instance v7, Lcom/twitter/library/provider/NotificationSetting;

    const-string/jumbo v8, "VIT_NOTABLE_EVENT"

    const/16 v9, 0xd

    const/high16 v13, 0x40000

    move v10, v2

    move v11, v4

    move v12, v2

    invoke-direct/range {v7 .. v13}, Lcom/twitter/library/provider/NotificationSetting;-><init>(Ljava/lang/String;IIIII)V

    sput-object v7, Lcom/twitter/library/provider/NotificationSetting;->n:Lcom/twitter/library/provider/NotificationSetting;

    const/16 v0, 0xe

    new-array v0, v0, [Lcom/twitter/library/provider/NotificationSetting;

    sget-object v1, Lcom/twitter/library/provider/NotificationSetting;->a:Lcom/twitter/library/provider/NotificationSetting;

    aput-object v1, v0, v2

    sget-object v1, Lcom/twitter/library/provider/NotificationSetting;->b:Lcom/twitter/library/provider/NotificationSetting;

    aput-object v1, v0, v4

    sget-object v1, Lcom/twitter/library/provider/NotificationSetting;->c:Lcom/twitter/library/provider/NotificationSetting;

    aput-object v1, v0, v3

    const/4 v1, 0x3

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->d:Lcom/twitter/library/provider/NotificationSetting;

    aput-object v2, v0, v1

    sget-object v1, Lcom/twitter/library/provider/NotificationSetting;->e:Lcom/twitter/library/provider/NotificationSetting;

    aput-object v1, v0, v6

    const/4 v1, 0x5

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->f:Lcom/twitter/library/provider/NotificationSetting;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->g:Lcom/twitter/library/provider/NotificationSetting;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->h:Lcom/twitter/library/provider/NotificationSetting;

    aput-object v2, v0, v1

    sget-object v1, Lcom/twitter/library/provider/NotificationSetting;->i:Lcom/twitter/library/provider/NotificationSetting;

    aput-object v1, v0, v5

    const/16 v1, 0x9

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->j:Lcom/twitter/library/provider/NotificationSetting;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->k:Lcom/twitter/library/provider/NotificationSetting;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->l:Lcom/twitter/library/provider/NotificationSetting;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->m:Lcom/twitter/library/provider/NotificationSetting;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->n:Lcom/twitter/library/provider/NotificationSetting;

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/library/provider/NotificationSetting;->o:[Lcom/twitter/library/provider/NotificationSetting;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIII)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/twitter/library/provider/NotificationSetting;->settingEnabledAll:I

    iput p4, p0, Lcom/twitter/library/provider/NotificationSetting;->settingEnabled:I

    iput p5, p0, Lcom/twitter/library/provider/NotificationSetting;->enabledForAll:I

    iput p6, p0, Lcom/twitter/library/provider/NotificationSetting;->enabledFor:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/library/provider/NotificationSetting;
    .locals 1

    const-class v0, Lcom/twitter/library/provider/NotificationSetting;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/NotificationSetting;

    return-object v0
.end method

.method public static values()[Lcom/twitter/library/provider/NotificationSetting;
    .locals 1

    sget-object v0, Lcom/twitter/library/provider/NotificationSetting;->o:[Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v0}, [Lcom/twitter/library/provider/NotificationSetting;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/provider/NotificationSetting;

    return-object v0
.end method


# virtual methods
.method public a(I)I
    .locals 1

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    iget v0, p0, Lcom/twitter/library/provider/NotificationSetting;->enabledFor:I

    goto :goto_0

    :pswitch_1
    iget v0, p0, Lcom/twitter/library/provider/NotificationSetting;->enabledForAll:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b(I)I
    .locals 1

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    iget v0, p0, Lcom/twitter/library/provider/NotificationSetting;->settingEnabled:I

    goto :goto_0

    :pswitch_1
    iget v0, p0, Lcom/twitter/library/provider/NotificationSetting;->settingEnabledAll:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public c(I)I
    .locals 1

    iget v0, p0, Lcom/twitter/library/provider/NotificationSetting;->settingEnabled:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/twitter/library/provider/NotificationSetting;->settingEnabledAll:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(I)I
    .locals 1

    iget v0, p0, Lcom/twitter/library/provider/NotificationSetting;->settingEnabled:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/library/provider/NotificationSetting;->enabledFor:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/twitter/library/provider/NotificationSetting;->settingEnabledAll:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/library/provider/NotificationSetting;->enabledForAll:I

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e(I)I
    .locals 1

    iget v0, p0, Lcom/twitter/library/provider/NotificationSetting;->enabledForAll:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/library/provider/NotificationSetting;->settingEnabledAll:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/twitter/library/provider/NotificationSetting;->enabledFor:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/library/provider/NotificationSetting;->settingEnabled:I

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
