.class public Lcom/twitter/library/provider/b;
.super Lcom/twitter/library/provider/m;
.source "Twttr"


# instance fields
.field private final i:Ljava/util/BitSet;

.field private final j:Ljava/util/BitSet;

.field private final k:Ljava/util/BitSet;

.field private final l:Landroid/util/SparseIntArray;

.field private final m:Landroid/util/SparseIntArray;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/twitter/library/provider/m;-><init>(Landroid/database/Cursor;)V

    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/provider/b;->i:Ljava/util/BitSet;

    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/provider/b;->j:Ljava/util/BitSet;

    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/provider/b;->k:Ljava/util/BitSet;

    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/provider/b;->l:Landroid/util/SparseIntArray;

    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/provider/b;->m:Landroid/util/SparseIntArray;

    return-void
.end method

.method private a(III)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/provider/b;->k:Ljava/util/BitSet;

    invoke-virtual {v0, p1}, Ljava/util/BitSet;->set(I)V

    iget-object v0, p0, Lcom/twitter/library/provider/b;->l:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    iget-object v0, p0, Lcom/twitter/library/provider/b;->m:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1, p3}, Landroid/util/SparseIntArray;->put(II)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 15

    const/4 v14, 0x2

    const/4 v5, 0x0

    const/4 v1, -0x1

    iget-object v0, p0, Lcom/twitter/library/provider/b;->i:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    iget-object v0, p0, Lcom/twitter/library/provider/b;->j:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    iget-object v0, p0, Lcom/twitter/library/provider/b;->k:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    iget-object v0, p0, Lcom/twitter/library/provider/b;->l:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    iget-object v0, p0, Lcom/twitter/library/provider/b;->m:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    iget-object v10, p0, Lcom/twitter/library/provider/b;->e:Landroid/database/Cursor;

    if-nez v10, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/provider/b;->c:Ljava/util/List;

    :goto_0
    return-void

    :cond_0
    iget-object v11, p0, Lcom/twitter/library/provider/b;->i:Ljava/util/BitSet;

    iget-object v12, p0, Lcom/twitter/library/provider/b;->j:Ljava/util/BitSet;

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_7

    sget v0, Lcom/twitter/library/provider/bi;->f:I

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {v11, v5}, Ljava/util/BitSet;->set(I)V

    move v2, v1

    move v3, v1

    move v4, v5

    move v6, v0

    move v7, v5

    move v0, v1

    :cond_1
    sget v8, Lcom/twitter/library/provider/bi;->f:I

    invoke-interface {v10, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    if-eq v6, v8, :cond_3

    if-ne v6, v14, :cond_2

    if-eq v0, v1, :cond_2

    invoke-direct {p0, v0, v3, v2}, Lcom/twitter/library/provider/b;->a(III)V

    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v5

    :cond_2
    add-int/lit8 v6, v7, -0x1

    invoke-virtual {v12, v6}, Ljava/util/BitSet;->set(I)V

    invoke-virtual {v11, v7}, Ljava/util/BitSet;->set(I)V

    move v6, v8

    :cond_3
    invoke-interface {v10}, Landroid/database/Cursor;->getPosition()I

    move-result v9

    if-ne v8, v14, :cond_9

    add-int/lit8 v4, v4, 0x1

    const/4 v2, 0x4

    if-gt v4, v2, :cond_5

    const/4 v2, 0x1

    if-ne v4, v2, :cond_8

    move v3, v9

    :cond_4
    :goto_1
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v13, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v7, v7, 0x1

    :cond_5
    move v2, v9

    :goto_2
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-nez v8, :cond_1

    if-ne v6, v14, :cond_6

    if-eq v0, v1, :cond_6

    invoke-direct {p0, v0, v3, v2}, Lcom/twitter/library/provider/b;->a(III)V

    :cond_6
    add-int/lit8 v0, v7, -0x1

    invoke-virtual {v12, v0}, Ljava/util/BitSet;->set(I)V

    :cond_7
    iput-object v13, p0, Lcom/twitter/library/provider/b;->c:Ljava/util/List;

    goto :goto_0

    :cond_8
    const/4 v2, 0x3

    if-le v4, v2, :cond_4

    move v0, v7

    goto :goto_1

    :cond_9
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v13, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v7, v7, 0x1

    goto :goto_2
.end method

.method public getExtras()Landroid/os/Bundle;
    .locals 4

    iget-object v0, p0, Lcom/twitter/library/provider/b;->e:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/provider/b;->getPosition()I

    move-result v1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v2, "start"

    iget-object v3, p0, Lcom/twitter/library/provider/b;->i:Ljava/util/BitSet;

    invoke-virtual {v3, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v2, "end"

    iget-object v3, p0, Lcom/twitter/library/provider/b;->j:Ljava/util/BitSet;

    invoke-virtual {v3, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    goto :goto_0
.end method

.method public getInt(I)I
    .locals 2

    sget v0, Lcom/twitter/library/provider/bi;->f:I

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/b;->k:Ljava/util/BitSet;

    invoke-virtual {p0}, Lcom/twitter/library/provider/b;->getPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/library/provider/m;->getInt(I)I

    move-result v0

    goto :goto_0
.end method

.method public moveToPosition(I)Z
    .locals 2

    iget-object v1, p0, Lcom/twitter/library/provider/b;->e:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    invoke-super {p0, p1}, Lcom/twitter/library/provider/m;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/provider/b;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
