.class public final Lcom/twitter/library/provider/cl;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x15

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "user_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "username"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "image_url"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "user_flags"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "location"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "description"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "followers"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "web_url"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "friends"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "profile_created"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "statuses"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "friendship"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "updated"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "favorites"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "bg_color"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "friendship_time"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "header_url"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "description_entities"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "url_entities"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/library/provider/cl;->a:[Ljava/lang/String;

    return-void
.end method
