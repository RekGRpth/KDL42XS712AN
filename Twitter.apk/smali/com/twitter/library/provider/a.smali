.class public Lcom/twitter/library/provider/a;
.super Lcom/twitter/library/provider/m;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/library/provider/m;-><init>(Landroid/database/Cursor;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 40

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/library/provider/a;->e:Landroid/database/Cursor;

    move-object/from16 v39, v0

    if-eqz v39, :cond_2

    invoke-interface/range {v39 .. v39}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/provider/a;->c:Ljava/util/List;

    :goto_0
    const/4 v4, 0x3

    move-object/from16 v0, v39

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    cmp-long v4, v7, v2

    if-eqz v4, :cond_3

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/provider/a;->c:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    new-instance v1, Lcom/twitter/library/api/search/TwitterSearchQuery;

    const/4 v2, 0x1

    move-object/from16 v0, v39

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    move-object/from16 v0, v39

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    move-object/from16 v0, v39

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/api/search/TwitterSearchQuery;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/util/ArrayList;)V

    move-object/from16 v36, v1

    move-wide/from16 v37, v7

    :goto_1
    new-instance v1, Lcom/twitter/library/api/TwitterUser;

    const/4 v2, 0x4

    move-object/from16 v0, v39

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const/4 v4, 0x0

    const/4 v5, 0x6

    move-object/from16 v0, v39

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x7

    move-object/from16 v0, v39

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const-wide/16 v19, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const-wide/16 v24, 0x0

    const/16 v26, 0x0

    const-wide/16 v27, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    invoke-direct/range {v1 .. v35}, Lcom/twitter/library/api/TwitterUser;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IZZZZZLjava/lang/String;IJIZIJLcom/twitter/library/api/TwitterStatus;JILcom/twitter/library/api/PromotedContent;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/TwitterUserMetadata;ZZ)V

    if-eqz v36, :cond_1

    move-object/from16 v0, v36

    iget-object v2, v0, Lcom/twitter/library/api/search/TwitterSearchQuery;->i:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-interface/range {v39 .. v39}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_4

    :goto_2
    return-void

    :cond_2
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/twitter/library/provider/a;->c:Ljava/util/List;

    goto :goto_2

    :cond_3
    move-object/from16 v36, v1

    move-wide/from16 v37, v2

    goto :goto_1

    :cond_4
    move-object/from16 v1, v36

    move-wide/from16 v2, v37

    goto/16 :goto_0
.end method

.method public getExtras()Landroid/os/Bundle;
    .locals 4

    new-instance v1, Landroid/os/Bundle;

    invoke-super {p0}, Lcom/twitter/library/provider/m;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/library/provider/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/search/TwitterSearchQuery;

    const-string/jumbo v2, "name"

    iget-object v3, v0, Lcom/twitter/library/api/search/TwitterSearchQuery;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "query"

    iget-object v3, v0, Lcom/twitter/library/api/search/TwitterSearchQuery;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "users"

    iget-object v0, v0, Lcom/twitter/library/api/search/TwitterSearchQuery;->i:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-object v1
.end method

.method public getLong(I)J
    .locals 2

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/a;->c:Ljava/util/List;

    iget v1, p0, Lcom/twitter/library/provider/a;->d:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/search/TwitterSearchQuery;

    iget-wide v0, v0, Lcom/twitter/library/api/search/TwitterSearchQuery;->g:J

    :goto_0
    return-wide v0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/library/provider/m;->getLong(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/a;->c:Ljava/util/List;

    iget v1, p0, Lcom/twitter/library/provider/a;->d:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/search/TwitterSearchQuery;

    iget-object v0, v0, Lcom/twitter/library/api/search/TwitterSearchQuery;->a:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/provider/a;->c:Ljava/util/List;

    iget v1, p0, Lcom/twitter/library/provider/a;->d:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/search/TwitterSearchQuery;

    iget-object v0, v0, Lcom/twitter/library/api/search/TwitterSearchQuery;->b:Ljava/lang/String;

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Lcom/twitter/library/provider/m;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
