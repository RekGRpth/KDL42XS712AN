.class public abstract Lcom/twitter/library/provider/m;
.super Landroid/database/CursorWrapper;
.source "Twttr"


# instance fields
.field protected final a:Lcom/twitter/library/provider/o;

.field protected final b:I

.field protected c:Ljava/util/List;

.field protected d:I

.field protected e:Landroid/database/Cursor;

.field volatile f:Z

.field volatile g:Z

.field volatile h:Z

.field private final i:Landroid/database/ContentObservable;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 1

    const/16 v0, 0x190

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/provider/m;-><init>(Landroid/database/Cursor;I)V

    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;I)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/database/CursorWrapper;-><init>(Landroid/database/Cursor;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/library/provider/m;->d:I

    if-gtz p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Limit must be greater than 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lcom/twitter/library/provider/o;

    invoke-direct {v0, p0}, Lcom/twitter/library/provider/o;-><init>(Lcom/twitter/library/provider/m;)V

    iput-object v0, p0, Lcom/twitter/library/provider/m;->a:Lcom/twitter/library/provider/o;

    new-instance v0, Landroid/database/ContentObservable;

    invoke-direct {v0}, Landroid/database/ContentObservable;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/provider/m;->i:Landroid/database/ContentObservable;

    iput-object p1, p0, Lcom/twitter/library/provider/m;->e:Landroid/database/Cursor;

    iput p2, p0, Lcom/twitter/library/provider/m;->b:I

    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/twitter/library/provider/p;

    invoke-direct {v0, p0}, Lcom/twitter/library/provider/p;-><init>(Lcom/twitter/library/provider/m;)V

    invoke-interface {p1, v0}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    new-instance v0, Lcom/twitter/library/provider/n;

    invoke-direct {v0, p0}, Lcom/twitter/library/provider/n;-><init>(Lcom/twitter/library/provider/m;)V

    invoke-interface {p1, v0}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/twitter/library/provider/m;)Landroid/database/ContentObservable;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/provider/m;->i:Landroid/database/ContentObservable;

    return-object v0
.end method

.method public static a(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_1

    const/4 v0, 0x0

    :cond_0
    return-object v0

    :cond_1
    move-object v0, p0

    :goto_0
    instance-of v1, v0, Landroid/database/CursorWrapper;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/database/CursorWrapper;

    invoke-virtual {v0}, Landroid/database/CursorWrapper;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public abstract a()V
.end method

.method public final b()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/provider/m;->f:Z

    invoke-virtual {p0}, Lcom/twitter/library/provider/m;->a()V

    iput-boolean v1, p0, Lcom/twitter/library/provider/m;->f:Z

    iget-boolean v0, p0, Lcom/twitter/library/provider/m;->g:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/twitter/library/provider/m;->g:Z

    iget-object v0, p0, Lcom/twitter/library/provider/m;->i:Landroid/database/ContentObservable;

    iget-boolean v1, p0, Lcom/twitter/library/provider/m;->h:Z

    invoke-virtual {v0, v1}, Landroid/database/ContentObservable;->dispatchChange(Z)V

    :cond_0
    return-void
.end method

.method public c()Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/provider/m;->c:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/provider/m;->c:Ljava/util/List;

    iget v1, p0, Lcom/twitter/library/provider/m;->d:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/provider/m;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/m;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPosition()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/provider/m;->d:I

    return v0
.end method

.method public moveToFirst()Z
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/library/provider/m;->moveToPosition(I)Z

    move-result v0

    return v0
.end method

.method public moveToLast()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/provider/m;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/twitter/library/provider/m;->moveToPosition(I)Z

    move-result v0

    return v0
.end method

.method public moveToNext()Z
    .locals 1

    iget v0, p0, Lcom/twitter/library/provider/m;->d:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/library/provider/m;->moveToPosition(I)Z

    move-result v0

    return v0
.end method

.method public moveToPosition(I)Z
    .locals 3

    const/4 v0, 0x0

    const/4 v2, -0x1

    invoke-virtual {p0}, Lcom/twitter/library/provider/m;->getCount()I

    move-result v1

    if-gt p1, v2, :cond_0

    iput v2, p0, Lcom/twitter/library/provider/m;->d:I

    :goto_0
    return v0

    :cond_0
    if-lt p1, v1, :cond_1

    iput v1, p0, Lcom/twitter/library/provider/m;->d:I

    goto :goto_0

    :cond_1
    iput p1, p0, Lcom/twitter/library/provider/m;->d:I

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public moveToPrevious()Z
    .locals 1

    iget v0, p0, Lcom/twitter/library/provider/m;->d:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/twitter/library/provider/m;->moveToPosition(I)Z

    move-result v0

    return v0
.end method

.method public registerContentObserver(Landroid/database/ContentObserver;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/provider/m;->i:Landroid/database/ContentObservable;

    invoke-virtual {v0, p1}, Landroid/database/ContentObservable;->registerObserver(Landroid/database/ContentObserver;)V

    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/provider/m;->a:Lcom/twitter/library/provider/o;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/o;->registerObserver(Ljava/lang/Object;)V

    return-void
.end method

.method public requery()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/twitter/library/provider/m;->a:Lcom/twitter/library/provider/o;

    invoke-virtual {v2, v0}, Lcom/twitter/library/provider/o;->a(Z)V

    iget-object v3, p0, Lcom/twitter/library/provider/m;->e:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/twitter/library/provider/m;->e:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->requery()Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/provider/m;->b()V

    invoke-virtual {v2, v1}, Lcom/twitter/library/provider/o;->a(Z)V

    invoke-virtual {v2}, Lcom/twitter/library/provider/o;->notifyChanged()V

    return v0
.end method

.method public unregisterContentObserver(Landroid/database/ContentObserver;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/provider/m;->i:Landroid/database/ContentObservable;

    invoke-virtual {v0, p1}, Landroid/database/ContentObservable;->unregisterObserver(Ljava/lang/Object;)V

    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/provider/m;->a:Lcom/twitter/library/provider/o;

    invoke-virtual {v0, p1}, Lcom/twitter/library/provider/o;->unregisterObserver(Ljava/lang/Object;)V

    return-void
.end method
