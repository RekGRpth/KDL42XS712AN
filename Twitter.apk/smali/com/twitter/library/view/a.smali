.class public abstract Lcom/twitter/library/view/a;
.super Landroid/text/style/ClickableSpan;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/view/b;


# instance fields
.field private final a:I

.field private final b:Z

.field private final c:Z

.field private d:Z


# direct methods
.method public constructor <init>(I)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/library/view/a;-><init>(IZZ)V

    return-void
.end method

.method public constructor <init>(IZZ)V
    .locals 0

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    iput p1, p0, Lcom/twitter/library/view/a;->a:I

    iput-boolean p2, p0, Lcom/twitter/library/view/a;->b:Z

    iput-boolean p3, p0, Lcom/twitter/library/view/a;->c:Z

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/view/a;->d:Z

    return-void
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/view/a;->d:Z

    return v0
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/view/a;->b:Z

    if-eqz v0, :cond_0

    iget v0, p1, Landroid/text/TextPaint;->linkColor:I

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/library/view/a;->d:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/twitter/library/view/a;->a:I

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/twitter/library/view/a;->a:I

    iput v0, p1, Landroid/text/TextPaint;->bgColor:I

    :goto_0
    iget-boolean v0, p0, Lcom/twitter/library/view/a;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    iput v0, p1, Landroid/text/TextPaint;->bgColor:I

    goto :goto_0
.end method
