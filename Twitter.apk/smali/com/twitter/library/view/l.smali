.class public Lcom/twitter/library/view/l;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/view/View;

.field private b:Landroid/text/Layout;

.field private c:F

.field private d:F

.field private final e:Landroid/os/Handler;

.field private final f:I

.field private g:Lcom/twitter/library/view/b;

.field private h:Lcom/twitter/library/view/b;

.field private i:I

.field private j:I


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/text/Layout;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/twitter/library/view/n;

    invoke-direct {v0, p0}, Lcom/twitter/library/view/n;-><init>(Lcom/twitter/library/view/l;)V

    iput-object v0, p0, Lcom/twitter/library/view/l;->e:Landroid/os/Handler;

    iput-object p1, p0, Lcom/twitter/library/view/l;->a:Landroid/view/View;

    iput-object p2, p0, Lcom/twitter/library/view/l;->b:Landroid/text/Layout;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/view/l;->f:I

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/view/l;F)F
    .locals 0

    iput p1, p0, Lcom/twitter/library/view/l;->c:F

    return p1
.end method

.method static synthetic a(Lcom/twitter/library/view/l;Landroid/text/Layout;)Landroid/text/Layout;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/view/l;->b:Landroid/text/Layout;

    return-object p1
.end method

.method private a()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/library/view/l;->h:Lcom/twitter/library/view/b;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/twitter/library/view/b;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0, v2}, Lcom/twitter/library/view/b;->a(Z)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/view/l;->h:Lcom/twitter/library/view/b;

    iput v2, p0, Lcom/twitter/library/view/l;->i:I

    iput v2, p0, Lcom/twitter/library/view/l;->j:I

    invoke-direct {p0}, Lcom/twitter/library/view/l;->b()V

    :cond_0
    return-void
.end method

.method public static a(Landroid/widget/TextView;)V
    .locals 2

    new-instance v0, Lcom/twitter/library/view/l;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/view/l;-><init>(Landroid/view/View;Landroid/text/Layout;)V

    new-instance v1, Lcom/twitter/library/view/m;

    invoke-direct {v1, v0}, Lcom/twitter/library/view/m;-><init>(Lcom/twitter/library/view/l;)V

    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method private a(Lcom/twitter/library/view/b;)V
    .locals 1

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lcom/twitter/library/view/b;->a(Z)V

    iput-object p1, p0, Lcom/twitter/library/view/l;->h:Lcom/twitter/library/view/b;

    invoke-direct {p0}, Lcom/twitter/library/view/l;->b()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/view/l;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/library/view/l;->a()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/view/l;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/library/view/l;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/library/view/l;->g:Lcom/twitter/library/view/b;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/view/l;->g:Lcom/twitter/library/view/b;

    iget-object v1, p0, Lcom/twitter/library/view/l;->a:Landroid/view/View;

    invoke-interface {v0, v1}, Lcom/twitter/library/view/b;->onClick(Landroid/view/View;)V

    iget-object v0, p0, Lcom/twitter/library/view/l;->e:Landroid/os/Handler;

    const/4 v1, 0x3

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/view/l;->g:Lcom/twitter/library/view/b;

    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/twitter/library/view/l;F)F
    .locals 0

    iput p1, p0, Lcom/twitter/library/view/l;->d:F

    return p1
.end method

.method private b()V
    .locals 6

    iget-object v0, p0, Lcom/twitter/library/view/l;->a:Landroid/view/View;

    iget v1, p0, Lcom/twitter/library/view/l;->c:F

    float-to-int v1, v1

    iget v2, p0, Lcom/twitter/library/view/l;->d:F

    float-to-int v2, v2

    iget v3, p0, Lcom/twitter/library/view/l;->c:F

    float-to-int v3, v3

    iget-object v4, p0, Lcom/twitter/library/view/l;->b:Landroid/text/Layout;

    invoke-virtual {v4}, Landroid/text/Layout;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget v4, p0, Lcom/twitter/library/view/l;->d:F

    float-to-int v4, v4

    iget-object v5, p0, Lcom/twitter/library/view/l;->b:Landroid/text/Layout;

    invoke-virtual {v5}, Landroid/text/Layout;->getHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->invalidate(IIII)V

    return-void
.end method


# virtual methods
.method public a(FF)V
    .locals 0

    iput p1, p0, Lcom/twitter/library/view/l;->c:F

    iput p2, p0, Lcom/twitter/library/view/l;->d:F

    return-void
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .locals 12

    const/4 v3, 0x0

    const/4 v11, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    iget-object v4, p0, Lcom/twitter/library/view/l;->b:Landroid/text/Layout;

    invoke-virtual {v4}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    instance-of v5, v0, Landroid/text/Spanned;

    if-eqz v5, :cond_0

    check-cast v0, Landroid/text/Spanned;

    :goto_0
    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    return v0

    :cond_0
    move-object v0, v3

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    and-int/lit16 v5, v5, 0xff

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    iget v7, p0, Lcom/twitter/library/view/l;->c:F

    sub-float/2addr v6, v7

    float-to-int v6, v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    iget v8, p0, Lcom/twitter/library/view/l;->d:F

    sub-float/2addr v7, v8

    float-to-int v7, v7

    if-ltz v6, :cond_2

    invoke-virtual {v4}, Landroid/text/Layout;->getWidth()I

    move-result v8

    if-ge v6, v8, :cond_2

    if-ltz v7, :cond_2

    invoke-virtual {v4}, Landroid/text/Layout;->getHeight()I

    move-result v8

    if-lt v7, v8, :cond_3

    :cond_2
    invoke-direct {p0}, Lcom/twitter/library/view/l;->a()V

    move v0, v1

    goto :goto_1

    :cond_3
    iget-object v8, p0, Lcom/twitter/library/view/l;->h:Lcom/twitter/library/view/b;

    if-eqz v8, :cond_5

    iget v8, p0, Lcom/twitter/library/view/l;->i:I

    sub-int v8, v6, v8

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v8

    iget v9, p0, Lcom/twitter/library/view/l;->f:I

    if-gt v8, v9, :cond_4

    iget v8, p0, Lcom/twitter/library/view/l;->j:I

    sub-int v8, v7, v8

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v8

    iget v9, p0, Lcom/twitter/library/view/l;->f:I

    if-le v8, v9, :cond_5

    :cond_4
    invoke-direct {p0}, Lcom/twitter/library/view/l;->a()V

    move v0, v1

    goto :goto_1

    :cond_5
    invoke-virtual {v4, v7}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v8

    int-to-float v9, v6

    invoke-virtual {v4, v8}, Landroid/text/Layout;->getLineLeft(I)F

    move-result v10

    cmpg-float v9, v9, v10

    if-ltz v9, :cond_6

    int-to-float v9, v6

    invoke-virtual {v4, v8}, Landroid/text/Layout;->getLineRight(I)F

    move-result v10

    cmpl-float v9, v9, v10

    if-lez v9, :cond_7

    :cond_6
    invoke-direct {p0}, Lcom/twitter/library/view/l;->a()V

    move v0, v1

    goto :goto_1

    :cond_7
    iget-object v9, p0, Lcom/twitter/library/view/l;->g:Lcom/twitter/library/view/b;

    if-eqz v9, :cond_8

    packed-switch v5, :pswitch_data_0

    :cond_8
    :goto_2
    :pswitch_0
    if-nez v5, :cond_9

    iput v6, p0, Lcom/twitter/library/view/l;->i:I

    iput v7, p0, Lcom/twitter/library/view/l;->j:I

    :cond_9
    if-nez v5, :cond_a

    int-to-float v3, v6

    invoke-virtual {v4, v8, v3}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    move-result v3

    const-class v4, Lcom/twitter/library/view/b;

    invoke-interface {v0, v3, v3, v4}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/view/b;

    array-length v3, v0

    if-lez v3, :cond_c

    aget-object v0, v0, v1

    invoke-direct {p0, v0}, Lcom/twitter/library/view/l;->a(Lcom/twitter/library/view/b;)V

    move v0, v2

    goto/16 :goto_1

    :pswitch_1
    invoke-direct {p0}, Lcom/twitter/library/view/l;->a()V

    iget-object v0, p0, Lcom/twitter/library/view/l;->e:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/twitter/library/view/l;->e:Landroid/os/Handler;

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v1

    int-to-long v3, v1

    invoke-virtual {v0, v11, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    move v0, v2

    goto/16 :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/twitter/library/view/l;->e:Landroid/os/Handler;

    invoke-virtual {v0, v11}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/twitter/library/view/l;->g:Lcom/twitter/library/view/b;

    iget-object v1, p0, Lcom/twitter/library/view/l;->a:Landroid/view/View;

    invoke-interface {v0, v1}, Lcom/twitter/library/view/b;->a(Landroid/view/View;)V

    iput-object v3, p0, Lcom/twitter/library/view/l;->g:Lcom/twitter/library/view/b;

    move v0, v2

    goto/16 :goto_1

    :pswitch_3
    iget-object v9, p0, Lcom/twitter/library/view/l;->e:Landroid/os/Handler;

    invoke-virtual {v9, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v9, p0, Lcom/twitter/library/view/l;->e:Landroid/os/Handler;

    invoke-virtual {v9, v11}, Landroid/os/Handler;->removeMessages(I)V

    iput-object v3, p0, Lcom/twitter/library/view/l;->g:Lcom/twitter/library/view/b;

    goto :goto_2

    :cond_a
    if-ne v5, v2, :cond_c

    iget-object v0, p0, Lcom/twitter/library/view/l;->h:Lcom/twitter/library/view/b;

    if-eqz v0, :cond_c

    invoke-static {}, Ljy;->b()Z

    move-result v1

    if-eqz v1, :cond_b

    iput-object v0, p0, Lcom/twitter/library/view/l;->g:Lcom/twitter/library/view/b;

    iget-object v0, p0, Lcom/twitter/library/view/l;->e:Landroid/os/Handler;

    invoke-static {}, Ljy;->d()I

    move-result v1

    int-to-long v3, v1

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :goto_3
    move v0, v2

    goto/16 :goto_1

    :cond_b
    iget-object v1, p0, Lcom/twitter/library/view/l;->a:Landroid/view/View;

    invoke-interface {v0, v1}, Lcom/twitter/library/view/b;->onClick(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/twitter/library/view/l;->a()V

    goto :goto_3

    :cond_c
    move v0, v1

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
