.class public final enum Lcom/twitter/library/view/TweetActionType;
.super Ljava/lang/Enum;
.source "Twttr"


# static fields
.field public static final enum a:Lcom/twitter/library/view/TweetActionType;

.field public static final enum b:Lcom/twitter/library/view/TweetActionType;

.field public static final enum c:Lcom/twitter/library/view/TweetActionType;

.field public static final enum d:Lcom/twitter/library/view/TweetActionType;

.field public static final enum e:Lcom/twitter/library/view/TweetActionType;

.field public static final enum f:Lcom/twitter/library/view/TweetActionType;

.field public static final enum g:Lcom/twitter/library/view/TweetActionType;

.field public static final enum h:Lcom/twitter/library/view/TweetActionType;

.field private static final synthetic i:[Lcom/twitter/library/view/TweetActionType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/twitter/library/view/TweetActionType;

    const-string/jumbo v1, "None"

    invoke-direct {v0, v1, v3}, Lcom/twitter/library/view/TweetActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/view/TweetActionType;->a:Lcom/twitter/library/view/TweetActionType;

    new-instance v0, Lcom/twitter/library/view/TweetActionType;

    const-string/jumbo v1, "Favorite"

    invoke-direct {v0, v1, v4}, Lcom/twitter/library/view/TweetActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/view/TweetActionType;->b:Lcom/twitter/library/view/TweetActionType;

    new-instance v0, Lcom/twitter/library/view/TweetActionType;

    const-string/jumbo v1, "Retweet"

    invoke-direct {v0, v1, v5}, Lcom/twitter/library/view/TweetActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/view/TweetActionType;->c:Lcom/twitter/library/view/TweetActionType;

    new-instance v0, Lcom/twitter/library/view/TweetActionType;

    const-string/jumbo v1, "Reply"

    invoke-direct {v0, v1, v6}, Lcom/twitter/library/view/TweetActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/view/TweetActionType;->d:Lcom/twitter/library/view/TweetActionType;

    new-instance v0, Lcom/twitter/library/view/TweetActionType;

    const-string/jumbo v1, "Follow"

    invoke-direct {v0, v1, v7}, Lcom/twitter/library/view/TweetActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/view/TweetActionType;->e:Lcom/twitter/library/view/TweetActionType;

    new-instance v0, Lcom/twitter/library/view/TweetActionType;

    const-string/jumbo v1, "Delete"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/view/TweetActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/view/TweetActionType;->f:Lcom/twitter/library/view/TweetActionType;

    new-instance v0, Lcom/twitter/library/view/TweetActionType;

    const-string/jumbo v1, "Share"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/view/TweetActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/view/TweetActionType;->g:Lcom/twitter/library/view/TweetActionType;

    new-instance v0, Lcom/twitter/library/view/TweetActionType;

    const-string/jumbo v1, "Dismiss"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/view/TweetActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/view/TweetActionType;->h:Lcom/twitter/library/view/TweetActionType;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/twitter/library/view/TweetActionType;

    sget-object v1, Lcom/twitter/library/view/TweetActionType;->a:Lcom/twitter/library/view/TweetActionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/twitter/library/view/TweetActionType;->b:Lcom/twitter/library/view/TweetActionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/twitter/library/view/TweetActionType;->c:Lcom/twitter/library/view/TweetActionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/twitter/library/view/TweetActionType;->d:Lcom/twitter/library/view/TweetActionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/twitter/library/view/TweetActionType;->e:Lcom/twitter/library/view/TweetActionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/twitter/library/view/TweetActionType;->f:Lcom/twitter/library/view/TweetActionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/twitter/library/view/TweetActionType;->g:Lcom/twitter/library/view/TweetActionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/twitter/library/view/TweetActionType;->h:Lcom/twitter/library/view/TweetActionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/library/view/TweetActionType;->i:[Lcom/twitter/library/view/TweetActionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/library/view/TweetActionType;
    .locals 1

    const-class v0, Lcom/twitter/library/view/TweetActionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/view/TweetActionType;

    return-object v0
.end method

.method public static values()[Lcom/twitter/library/view/TweetActionType;
    .locals 1

    sget-object v0, Lcom/twitter/library/view/TweetActionType;->i:[Lcom/twitter/library/view/TweetActionType;

    invoke-virtual {v0}, [Lcom/twitter/library/view/TweetActionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/view/TweetActionType;

    return-object v0
.end method
