.class public Lcom/twitter/library/card/property/a;
.super Landroid/util/SparseArray;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/util/SparseArray;-><init>()V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/util/SparseArray;-><init>(I)V

    return-void
.end method

.method public static a(Ljava/lang/Class;Ljava/io/ObjectInput;)Lcom/twitter/library/card/property/a;
    .locals 5

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v2

    if-gez v2, :cond_1

    const/4 v0, 0x0

    :cond_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/twitter/library/card/property/a;

    invoke-direct {v0, v2}, Lcom/twitter/library/card/property/a;-><init>(I)V

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v3

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/twitter/library/card/property/a;->put(ILjava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static a(Lcom/twitter/library/card/property/a;Ljava/io/ObjectOutput;)V
    .locals 3

    if-nez p0, :cond_1

    const/4 v0, -0x1

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/card/property/a;->size()I

    move-result v1

    invoke-interface {p1, v1}, Ljava/io/ObjectOutput;->writeInt(I)V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/twitter/library/card/property/a;->keyAt(I)I

    move-result v2

    invoke-interface {p1, v2}, Ljava/io/ObjectOutput;->writeInt(I)V

    invoke-virtual {p0, v0}, Lcom/twitter/library/card/property/a;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/library/card/property/a;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/library/card/property/a;->size()I

    move-result v3

    check-cast p1, Lcom/twitter/library/card/property/a;

    invoke-virtual {p1}, Lcom/twitter/library/card/property/a;->size()I

    move-result v2

    if-eq v3, v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_0

    invoke-virtual {p0, v2}, Lcom/twitter/library/card/property/a;->keyAt(I)I

    move-result v4

    invoke-virtual {p1, v2}, Lcom/twitter/library/card/property/a;->keyAt(I)I

    move-result v5

    if-eq v4, v5, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    invoke-virtual {p0, v2}, Lcom/twitter/library/card/property/a;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v2}, Lcom/twitter/library/card/property/a;->valueAt(I)Ljava/lang/Object;

    move-result-object v5

    if-nez v4, :cond_5

    if-eqz v5, :cond_7

    move v0, v1

    goto :goto_0

    :cond_5
    if-nez v5, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public hashCode()I
    .locals 6

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/twitter/library/card/property/a;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {p0, v2}, Lcom/twitter/library/card/property/a;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    mul-int/lit8 v5, v0, 0x1f

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1
    add-int v3, v5, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_1

    :cond_1
    return v0
.end method
