.class public Lcom/twitter/library/card/property/Style;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field private static final serialVersionUID:J = 0x67647b254ec97d11L


# instance fields
.field public background:Lcom/twitter/library/card/property/Fill;

.field public border:Lcom/twitter/library/card/property/Border;

.field public color:Ljava/lang/Integer;

.field public cornerRadius:Ljava/lang/Float;

.field public fontBold:Ljava/lang/Boolean;

.field public fontItalic:Ljava/lang/Boolean;

.field public fontName:Ljava/lang/String;

.field public fontSize:Ljava/lang/Float;

.field public fontUnderline:Ljava/lang/Boolean;

.field public id:I

.field public lineHeight:Ljava/lang/Float;

.field public margin:Lcom/twitter/library/card/property/Spacing;

.field public maxSizeX:Ljava/lang/Float;

.field public maxSizeY:Ljava/lang/Float;

.field public opacity:Ljava/lang/Float;

.field public padding:Lcom/twitter/library/card/property/Spacing;

.field public positionX:Ljava/lang/Float;

.field public positionY:Ljava/lang/Float;

.field public shadow:Lcom/twitter/library/card/property/Shadow;

.field public sizeX:Ljava/lang/Float;

.field public sizeY:Ljava/lang/Float;

.field public visible:Ljava/lang/Boolean;

.field public visibleChildIndex:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->positionX:Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->positionY:Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->sizeX:Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->sizeY:Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->maxSizeX:Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->maxSizeY:Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->margin:Lcom/twitter/library/card/property/Spacing;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->opacity:Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->visible:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->shadow:Lcom/twitter/library/card/property/Shadow;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->background:Lcom/twitter/library/card/property/Fill;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->border:Lcom/twitter/library/card/property/Border;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->padding:Lcom/twitter/library/card/property/Spacing;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->fontName:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->fontSize:Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->color:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->lineHeight:Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->fontBold:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->fontUnderline:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->fontItalic:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->cornerRadius:Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->visibleChildIndex:Ljava/lang/Integer;

    return-void
.end method

.method public a(Lcom/twitter/library/card/property/Style;)V
    .locals 1

    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->positionX:Ljava/lang/Float;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->positionX:Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->positionX:Ljava/lang/Float;

    :cond_0
    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->positionY:Ljava/lang/Float;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->positionY:Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->positionY:Ljava/lang/Float;

    :cond_1
    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->sizeX:Ljava/lang/Float;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->sizeX:Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->sizeX:Ljava/lang/Float;

    :cond_2
    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->sizeY:Ljava/lang/Float;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->sizeY:Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->sizeY:Ljava/lang/Float;

    :cond_3
    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->maxSizeX:Ljava/lang/Float;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->maxSizeX:Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->maxSizeX:Ljava/lang/Float;

    :cond_4
    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->maxSizeY:Ljava/lang/Float;

    if-eqz v0, :cond_5

    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->maxSizeY:Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->maxSizeY:Ljava/lang/Float;

    :cond_5
    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->margin:Lcom/twitter/library/card/property/Spacing;

    if-eqz v0, :cond_6

    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->margin:Lcom/twitter/library/card/property/Spacing;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->margin:Lcom/twitter/library/card/property/Spacing;

    :cond_6
    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->opacity:Ljava/lang/Float;

    if-eqz v0, :cond_7

    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->opacity:Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->opacity:Ljava/lang/Float;

    :cond_7
    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->visible:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->visible:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->visible:Ljava/lang/Boolean;

    :cond_8
    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->shadow:Lcom/twitter/library/card/property/Shadow;

    if-eqz v0, :cond_9

    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->shadow:Lcom/twitter/library/card/property/Shadow;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->shadow:Lcom/twitter/library/card/property/Shadow;

    :cond_9
    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->background:Lcom/twitter/library/card/property/Fill;

    if-eqz v0, :cond_a

    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->background:Lcom/twitter/library/card/property/Fill;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->background:Lcom/twitter/library/card/property/Fill;

    :cond_a
    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->border:Lcom/twitter/library/card/property/Border;

    if-eqz v0, :cond_b

    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->border:Lcom/twitter/library/card/property/Border;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->border:Lcom/twitter/library/card/property/Border;

    :cond_b
    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->padding:Lcom/twitter/library/card/property/Spacing;

    if-eqz v0, :cond_c

    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->padding:Lcom/twitter/library/card/property/Spacing;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->padding:Lcom/twitter/library/card/property/Spacing;

    :cond_c
    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->fontName:Ljava/lang/String;

    if-eqz v0, :cond_d

    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->fontName:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->fontName:Ljava/lang/String;

    :cond_d
    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->fontSize:Ljava/lang/Float;

    if-eqz v0, :cond_e

    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->fontSize:Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->fontSize:Ljava/lang/Float;

    :cond_e
    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->color:Ljava/lang/Integer;

    if-eqz v0, :cond_f

    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->color:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->color:Ljava/lang/Integer;

    :cond_f
    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->lineHeight:Ljava/lang/Float;

    if-eqz v0, :cond_10

    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->lineHeight:Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->lineHeight:Ljava/lang/Float;

    :cond_10
    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->fontBold:Ljava/lang/Boolean;

    if-eqz v0, :cond_11

    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->fontBold:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->fontBold:Ljava/lang/Boolean;

    :cond_11
    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->fontUnderline:Ljava/lang/Boolean;

    if-eqz v0, :cond_12

    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->fontUnderline:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->fontUnderline:Ljava/lang/Boolean;

    :cond_12
    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->fontItalic:Ljava/lang/Boolean;

    if-eqz v0, :cond_13

    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->fontItalic:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->fontItalic:Ljava/lang/Boolean;

    :cond_13
    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->cornerRadius:Ljava/lang/Float;

    if-eqz v0, :cond_14

    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->cornerRadius:Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->cornerRadius:Ljava/lang/Float;

    :cond_14
    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->visibleChildIndex:Ljava/lang/Integer;

    if-eqz v0, :cond_15

    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->visibleChildIndex:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->visibleChildIndex:Ljava/lang/Integer;

    :cond_15
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/library/card/property/Style;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/twitter/library/card/property/Style;

    iget v2, p0, Lcom/twitter/library/card/property/Style;->id:I

    iget v3, p1, Lcom/twitter/library/card/property/Style;->id:I

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->background:Lcom/twitter/library/card/property/Fill;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->background:Lcom/twitter/library/card/property/Fill;

    iget-object v3, p1, Lcom/twitter/library/card/property/Style;->background:Lcom/twitter/library/card/property/Fill;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/Fill;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p1, Lcom/twitter/library/card/property/Style;->background:Lcom/twitter/library/card/property/Fill;

    if-nez v2, :cond_4

    :cond_6
    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->border:Lcom/twitter/library/card/property/Border;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->border:Lcom/twitter/library/card/property/Border;

    iget-object v3, p1, Lcom/twitter/library/card/property/Style;->border:Lcom/twitter/library/card/property/Border;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/Border;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p1, Lcom/twitter/library/card/property/Style;->border:Lcom/twitter/library/card/property/Border;

    if-nez v2, :cond_7

    :cond_9
    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->color:Ljava/lang/Integer;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->color:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/twitter/library/card/property/Style;->color:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    goto :goto_0

    :cond_b
    iget-object v2, p1, Lcom/twitter/library/card/property/Style;->color:Ljava/lang/Integer;

    if-nez v2, :cond_a

    :cond_c
    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->cornerRadius:Ljava/lang/Float;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->cornerRadius:Ljava/lang/Float;

    iget-object v3, p1, Lcom/twitter/library/card/property/Style;->cornerRadius:Ljava/lang/Float;

    invoke-virtual {v2, v3}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    goto :goto_0

    :cond_e
    iget-object v2, p1, Lcom/twitter/library/card/property/Style;->cornerRadius:Ljava/lang/Float;

    if-nez v2, :cond_d

    :cond_f
    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->fontBold:Ljava/lang/Boolean;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->fontBold:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/twitter/library/card/property/Style;->fontBold:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    goto :goto_0

    :cond_11
    iget-object v2, p1, Lcom/twitter/library/card/property/Style;->fontBold:Ljava/lang/Boolean;

    if-nez v2, :cond_10

    :cond_12
    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->fontItalic:Ljava/lang/Boolean;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->fontItalic:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/twitter/library/card/property/Style;->fontItalic:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    goto/16 :goto_0

    :cond_14
    iget-object v2, p1, Lcom/twitter/library/card/property/Style;->fontItalic:Ljava/lang/Boolean;

    if-nez v2, :cond_13

    :cond_15
    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->fontName:Ljava/lang/String;

    if-eqz v2, :cond_17

    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->fontName:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/property/Style;->fontName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    :cond_16
    move v0, v1

    goto/16 :goto_0

    :cond_17
    iget-object v2, p1, Lcom/twitter/library/card/property/Style;->fontName:Ljava/lang/String;

    if-nez v2, :cond_16

    :cond_18
    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->fontSize:Ljava/lang/Float;

    if-eqz v2, :cond_1a

    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->fontSize:Ljava/lang/Float;

    iget-object v3, p1, Lcom/twitter/library/card/property/Style;->fontSize:Ljava/lang/Float;

    invoke-virtual {v2, v3}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1b

    :cond_19
    move v0, v1

    goto/16 :goto_0

    :cond_1a
    iget-object v2, p1, Lcom/twitter/library/card/property/Style;->fontSize:Ljava/lang/Float;

    if-nez v2, :cond_19

    :cond_1b
    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->fontUnderline:Ljava/lang/Boolean;

    if-eqz v2, :cond_1d

    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->fontUnderline:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/twitter/library/card/property/Style;->fontUnderline:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1e

    :cond_1c
    move v0, v1

    goto/16 :goto_0

    :cond_1d
    iget-object v2, p1, Lcom/twitter/library/card/property/Style;->fontUnderline:Ljava/lang/Boolean;

    if-nez v2, :cond_1c

    :cond_1e
    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->lineHeight:Ljava/lang/Float;

    if-eqz v2, :cond_20

    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->lineHeight:Ljava/lang/Float;

    iget-object v3, p1, Lcom/twitter/library/card/property/Style;->lineHeight:Ljava/lang/Float;

    invoke-virtual {v2, v3}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_21

    :cond_1f
    move v0, v1

    goto/16 :goto_0

    :cond_20
    iget-object v2, p1, Lcom/twitter/library/card/property/Style;->lineHeight:Ljava/lang/Float;

    if-nez v2, :cond_1f

    :cond_21
    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->margin:Lcom/twitter/library/card/property/Spacing;

    if-eqz v2, :cond_23

    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->margin:Lcom/twitter/library/card/property/Spacing;

    iget-object v3, p1, Lcom/twitter/library/card/property/Style;->margin:Lcom/twitter/library/card/property/Spacing;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/Spacing;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_24

    :cond_22
    move v0, v1

    goto/16 :goto_0

    :cond_23
    iget-object v2, p1, Lcom/twitter/library/card/property/Style;->margin:Lcom/twitter/library/card/property/Spacing;

    if-nez v2, :cond_22

    :cond_24
    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->maxSizeX:Ljava/lang/Float;

    if-eqz v2, :cond_26

    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->maxSizeX:Ljava/lang/Float;

    iget-object v3, p1, Lcom/twitter/library/card/property/Style;->maxSizeX:Ljava/lang/Float;

    invoke-virtual {v2, v3}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_27

    :cond_25
    move v0, v1

    goto/16 :goto_0

    :cond_26
    iget-object v2, p1, Lcom/twitter/library/card/property/Style;->maxSizeX:Ljava/lang/Float;

    if-nez v2, :cond_25

    :cond_27
    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->maxSizeY:Ljava/lang/Float;

    if-eqz v2, :cond_29

    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->maxSizeY:Ljava/lang/Float;

    iget-object v3, p1, Lcom/twitter/library/card/property/Style;->maxSizeY:Ljava/lang/Float;

    invoke-virtual {v2, v3}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2a

    :cond_28
    move v0, v1

    goto/16 :goto_0

    :cond_29
    iget-object v2, p1, Lcom/twitter/library/card/property/Style;->maxSizeY:Ljava/lang/Float;

    if-nez v2, :cond_28

    :cond_2a
    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->opacity:Ljava/lang/Float;

    if-eqz v2, :cond_2c

    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->opacity:Ljava/lang/Float;

    iget-object v3, p1, Lcom/twitter/library/card/property/Style;->opacity:Ljava/lang/Float;

    invoke-virtual {v2, v3}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2d

    :cond_2b
    move v0, v1

    goto/16 :goto_0

    :cond_2c
    iget-object v2, p1, Lcom/twitter/library/card/property/Style;->opacity:Ljava/lang/Float;

    if-nez v2, :cond_2b

    :cond_2d
    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->padding:Lcom/twitter/library/card/property/Spacing;

    if-eqz v2, :cond_2f

    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->padding:Lcom/twitter/library/card/property/Spacing;

    iget-object v3, p1, Lcom/twitter/library/card/property/Style;->padding:Lcom/twitter/library/card/property/Spacing;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/Spacing;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_30

    :cond_2e
    move v0, v1

    goto/16 :goto_0

    :cond_2f
    iget-object v2, p1, Lcom/twitter/library/card/property/Style;->padding:Lcom/twitter/library/card/property/Spacing;

    if-nez v2, :cond_2e

    :cond_30
    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->positionX:Ljava/lang/Float;

    if-eqz v2, :cond_32

    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->positionX:Ljava/lang/Float;

    iget-object v3, p1, Lcom/twitter/library/card/property/Style;->positionX:Ljava/lang/Float;

    invoke-virtual {v2, v3}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_33

    :cond_31
    move v0, v1

    goto/16 :goto_0

    :cond_32
    iget-object v2, p1, Lcom/twitter/library/card/property/Style;->positionX:Ljava/lang/Float;

    if-nez v2, :cond_31

    :cond_33
    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->positionY:Ljava/lang/Float;

    if-eqz v2, :cond_35

    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->positionY:Ljava/lang/Float;

    iget-object v3, p1, Lcom/twitter/library/card/property/Style;->positionY:Ljava/lang/Float;

    invoke-virtual {v2, v3}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_36

    :cond_34
    move v0, v1

    goto/16 :goto_0

    :cond_35
    iget-object v2, p1, Lcom/twitter/library/card/property/Style;->positionY:Ljava/lang/Float;

    if-nez v2, :cond_34

    :cond_36
    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->shadow:Lcom/twitter/library/card/property/Shadow;

    if-eqz v2, :cond_38

    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->shadow:Lcom/twitter/library/card/property/Shadow;

    iget-object v3, p1, Lcom/twitter/library/card/property/Style;->shadow:Lcom/twitter/library/card/property/Shadow;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/Shadow;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_39

    :cond_37
    move v0, v1

    goto/16 :goto_0

    :cond_38
    iget-object v2, p1, Lcom/twitter/library/card/property/Style;->shadow:Lcom/twitter/library/card/property/Shadow;

    if-nez v2, :cond_37

    :cond_39
    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->sizeX:Ljava/lang/Float;

    if-eqz v2, :cond_3b

    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->sizeX:Ljava/lang/Float;

    iget-object v3, p1, Lcom/twitter/library/card/property/Style;->sizeX:Ljava/lang/Float;

    invoke-virtual {v2, v3}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3c

    :cond_3a
    move v0, v1

    goto/16 :goto_0

    :cond_3b
    iget-object v2, p1, Lcom/twitter/library/card/property/Style;->sizeX:Ljava/lang/Float;

    if-nez v2, :cond_3a

    :cond_3c
    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->sizeY:Ljava/lang/Float;

    if-eqz v2, :cond_3e

    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->sizeY:Ljava/lang/Float;

    iget-object v3, p1, Lcom/twitter/library/card/property/Style;->sizeY:Ljava/lang/Float;

    invoke-virtual {v2, v3}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3f

    :cond_3d
    move v0, v1

    goto/16 :goto_0

    :cond_3e
    iget-object v2, p1, Lcom/twitter/library/card/property/Style;->sizeY:Ljava/lang/Float;

    if-nez v2, :cond_3d

    :cond_3f
    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->visible:Ljava/lang/Boolean;

    if-eqz v2, :cond_41

    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->visible:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/twitter/library/card/property/Style;->visible:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_42

    :cond_40
    move v0, v1

    goto/16 :goto_0

    :cond_41
    iget-object v2, p1, Lcom/twitter/library/card/property/Style;->visible:Ljava/lang/Boolean;

    if-nez v2, :cond_40

    :cond_42
    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->visibleChildIndex:Ljava/lang/Integer;

    if-eqz v2, :cond_43

    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->visibleChildIndex:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/twitter/library/card/property/Style;->visibleChildIndex:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    goto/16 :goto_0

    :cond_43
    iget-object v2, p1, Lcom/twitter/library/card/property/Style;->visibleChildIndex:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget v0, p0, Lcom/twitter/library/card/property/Style;->id:I

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->positionX:Ljava/lang/Float;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->positionX:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->positionY:Ljava/lang/Float;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->positionY:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->sizeX:Ljava/lang/Float;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->sizeX:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->sizeY:Ljava/lang/Float;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->sizeY:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->maxSizeX:Ljava/lang/Float;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->maxSizeX:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->maxSizeY:Ljava/lang/Float;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->maxSizeY:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->margin:Lcom/twitter/library/card/property/Spacing;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->margin:Lcom/twitter/library/card/property/Spacing;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/Spacing;->hashCode()I

    move-result v0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->opacity:Ljava/lang/Float;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->opacity:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->visible:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->visible:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->shadow:Lcom/twitter/library/card/property/Shadow;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->shadow:Lcom/twitter/library/card/property/Shadow;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/Shadow;->hashCode()I

    move-result v0

    :goto_9
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->background:Lcom/twitter/library/card/property/Fill;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->background:Lcom/twitter/library/card/property/Fill;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/Fill;->hashCode()I

    move-result v0

    :goto_a
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->border:Lcom/twitter/library/card/property/Border;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->border:Lcom/twitter/library/card/property/Border;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/Border;->hashCode()I

    move-result v0

    :goto_b
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->padding:Lcom/twitter/library/card/property/Spacing;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->padding:Lcom/twitter/library/card/property/Spacing;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/Spacing;->hashCode()I

    move-result v0

    :goto_c
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->fontName:Ljava/lang/String;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->fontName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_d
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->fontSize:Ljava/lang/Float;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->fontSize:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    :goto_e
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->color:Ljava/lang/Integer;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->color:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    :goto_f
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->lineHeight:Ljava/lang/Float;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->lineHeight:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    :goto_10
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->fontBold:Ljava/lang/Boolean;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->fontBold:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    :goto_11
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->fontUnderline:Ljava/lang/Boolean;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->fontUnderline:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    :goto_12
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->fontItalic:Ljava/lang/Boolean;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->fontItalic:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    :goto_13
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->cornerRadius:Ljava/lang/Float;

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->cornerRadius:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    :goto_14
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/library/card/property/Style;->visibleChildIndex:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/library/card/property/Style;->visibleChildIndex:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto/16 :goto_0

    :cond_2
    move v0, v1

    goto/16 :goto_1

    :cond_3
    move v0, v1

    goto/16 :goto_2

    :cond_4
    move v0, v1

    goto/16 :goto_3

    :cond_5
    move v0, v1

    goto/16 :goto_4

    :cond_6
    move v0, v1

    goto/16 :goto_5

    :cond_7
    move v0, v1

    goto/16 :goto_6

    :cond_8
    move v0, v1

    goto/16 :goto_7

    :cond_9
    move v0, v1

    goto/16 :goto_8

    :cond_a
    move v0, v1

    goto/16 :goto_9

    :cond_b
    move v0, v1

    goto/16 :goto_a

    :cond_c
    move v0, v1

    goto/16 :goto_b

    :cond_d
    move v0, v1

    goto/16 :goto_c

    :cond_e
    move v0, v1

    goto/16 :goto_d

    :cond_f
    move v0, v1

    goto/16 :goto_e

    :cond_10
    move v0, v1

    goto :goto_f

    :cond_11
    move v0, v1

    goto :goto_10

    :cond_12
    move v0, v1

    goto :goto_11

    :cond_13
    move v0, v1

    goto :goto_12

    :cond_14
    move v0, v1

    goto :goto_13

    :cond_15
    move v0, v1

    goto :goto_14
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/property/Style;->id:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->positionX:Ljava/lang/Float;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->positionY:Ljava/lang/Float;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->sizeX:Ljava/lang/Float;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->sizeY:Ljava/lang/Float;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->maxSizeX:Ljava/lang/Float;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->maxSizeY:Ljava/lang/Float;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Spacing;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->margin:Lcom/twitter/library/card/property/Spacing;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->opacity:Ljava/lang/Float;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->visible:Ljava/lang/Boolean;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Shadow;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->shadow:Lcom/twitter/library/card/property/Shadow;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Fill;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->background:Lcom/twitter/library/card/property/Fill;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Border;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->border:Lcom/twitter/library/card/property/Border;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Spacing;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->padding:Lcom/twitter/library/card/property/Spacing;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->fontName:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->fontSize:Ljava/lang/Float;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->color:Ljava/lang/Integer;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->lineHeight:Ljava/lang/Float;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->fontBold:Ljava/lang/Boolean;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->fontUnderline:Ljava/lang/Boolean;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->fontItalic:Ljava/lang/Boolean;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->cornerRadius:Ljava/lang/Float;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p0, Lcom/twitter/library/card/property/Style;->visibleChildIndex:Ljava/lang/Integer;

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    iget v0, p0, Lcom/twitter/library/card/property/Style;->id:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->positionX:Ljava/lang/Float;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->positionY:Ljava/lang/Float;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->sizeX:Ljava/lang/Float;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->sizeY:Ljava/lang/Float;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->maxSizeX:Ljava/lang/Float;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->maxSizeY:Ljava/lang/Float;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->margin:Lcom/twitter/library/card/property/Spacing;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->opacity:Ljava/lang/Float;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->visible:Ljava/lang/Boolean;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->shadow:Lcom/twitter/library/card/property/Shadow;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->background:Lcom/twitter/library/card/property/Fill;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->border:Lcom/twitter/library/card/property/Border;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->padding:Lcom/twitter/library/card/property/Spacing;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->fontName:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->fontSize:Ljava/lang/Float;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->color:Ljava/lang/Integer;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->lineHeight:Ljava/lang/Float;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->fontBold:Ljava/lang/Boolean;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->fontUnderline:Ljava/lang/Boolean;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->fontItalic:Ljava/lang/Boolean;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->cornerRadius:Ljava/lang/Float;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Style;->visibleChildIndex:Ljava/lang/Integer;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    return-void
.end method
