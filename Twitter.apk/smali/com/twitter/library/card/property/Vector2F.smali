.class public Lcom/twitter/library/card/property/Vector2F;
.super Landroid/graphics/PointF;
.source "Twttr"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field public static final a:Lcom/twitter/library/card/property/Vector2F;

.field private static final serialVersionUID:J = -0x43f2bda7725822f2L


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/twitter/library/card/property/Vector2F;->a()Lcom/twitter/library/card/property/Vector2F;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/card/property/Vector2F;->a:Lcom/twitter/library/card/property/Vector2F;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/graphics/PointF;-><init>()V

    return-void
.end method

.method public constructor <init>(FF)V
    .locals 0

    invoke-direct {p0}, Landroid/graphics/PointF;-><init>()V

    iput p1, p0, Lcom/twitter/library/card/property/Vector2F;->x:F

    iput p2, p0, Lcom/twitter/library/card/property/Vector2F;->y:F

    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/card/property/Vector2F;)V
    .locals 1

    invoke-direct {p0}, Landroid/graphics/PointF;-><init>()V

    iget v0, p1, Lcom/twitter/library/card/property/Vector2F;->x:F

    iput v0, p0, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget v0, p1, Lcom/twitter/library/card/property/Vector2F;->y:F

    iput v0, p0, Lcom/twitter/library/card/property/Vector2F;->y:F

    return-void
.end method

.method public static a()Lcom/twitter/library/card/property/Vector2F;
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Lcom/twitter/library/card/property/Vector2F;

    invoke-direct {v0, v1, v1}, Lcom/twitter/library/card/property/Vector2F;-><init>(FF)V

    return-object v0
.end method

.method public static b()Lcom/twitter/library/card/property/Vector2F;
    .locals 2

    const/high16 v1, 0x7fc00000    # NaNf

    new-instance v0, Lcom/twitter/library/card/property/Vector2F;

    invoke-direct {v0, v1, v1}, Lcom/twitter/library/card/property/Vector2F;-><init>(FF)V

    return-object v0
.end method


# virtual methods
.method public a(I)F
    .locals 1

    if-nez p1, :cond_0

    iget v0, p0, Lcom/twitter/library/card/property/Vector2F;->x:F

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/twitter/library/card/property/Vector2F;->y:F

    goto :goto_0
.end method

.method public a(F)V
    .locals 1

    iget v0, p0, Lcom/twitter/library/card/property/Vector2F;->x:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget v0, p0, Lcom/twitter/library/card/property/Vector2F;->y:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/twitter/library/card/property/Vector2F;->y:F

    return-void
.end method

.method public a(IF)V
    .locals 0

    if-nez p1, :cond_0

    iput p2, p0, Lcom/twitter/library/card/property/Vector2F;->x:F

    :goto_0
    return-void

    :cond_0
    iput p2, p0, Lcom/twitter/library/card/property/Vector2F;->y:F

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/card/property/Vector2F;)V
    .locals 2

    iget v0, p0, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget v1, p1, Lcom/twitter/library/card/property/Vector2F;->x:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget v0, p0, Lcom/twitter/library/card/property/Vector2F;->y:F

    iget v1, p1, Lcom/twitter/library/card/property/Vector2F;->y:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/twitter/library/card/property/Vector2F;->y:F

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/library/card/property/Vector2F;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/twitter/library/card/property/Vector2F;

    iget v2, p1, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget v3, p0, Lcom/twitter/library/card/property/Vector2F;->x:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget v2, p1, Lcom/twitter/library/card/property/Vector2F;->y:F

    iget v3, p0, Lcom/twitter/library/card/property/Vector2F;->y:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    const/4 v1, 0x0

    const/4 v3, 0x0

    iget v0, p0, Lcom/twitter/library/card/property/Vector2F;->x:F

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/library/card/property/Vector2F;->x:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/twitter/library/card/property/Vector2F;->y:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    iget v1, p0, Lcom/twitter/library/card/property/Vector2F;->y:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1

    invoke-interface {p1}, Ljava/io/ObjectInput;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/property/Vector2F;->x:F

    invoke-interface {p1}, Ljava/io/ObjectInput;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/property/Vector2F;->y:F

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/twitter/library/card/property/Vector2F;->x:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/twitter/library/card/property/Vector2F;->y:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    iget v0, p0, Lcom/twitter/library/card/property/Vector2F;->x:F

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeFloat(F)V

    iget v0, p0, Lcom/twitter/library/card/property/Vector2F;->y:F

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeFloat(F)V

    return-void
.end method
