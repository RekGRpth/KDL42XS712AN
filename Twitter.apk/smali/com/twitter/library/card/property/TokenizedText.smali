.class public Lcom/twitter/library/card/property/TokenizedText;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field private static final serialVersionUID:J = -0x62d8755ec87a2151L


# instance fields
.field public textTokenGroups:[Lcom/twitter/library/card/property/TextTokenGroup;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/library/card/property/TokenizedText;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/twitter/library/card/property/TokenizedText;

    iget-object v2, p0, Lcom/twitter/library/card/property/TokenizedText;->textTokenGroups:[Lcom/twitter/library/card/property/TextTokenGroup;

    iget-object v3, p1, Lcom/twitter/library/card/property/TokenizedText;->textTokenGroups:[Lcom/twitter/library/card/property/TextTokenGroup;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/property/TokenizedText;->textTokenGroups:[Lcom/twitter/library/card/property/TextTokenGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/property/TokenizedText;->textTokenGroups:[Lcom/twitter/library/card/property/TextTokenGroup;

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1

    const-class v0, [Lcom/twitter/library/card/property/TextTokenGroup;

    invoke-static {v0, p1}, Lcom/twitter/library/util/Util;->a(Ljava/lang/Class;Ljava/io/ObjectInput;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/card/property/TextTokenGroup;

    iput-object v0, p0, Lcom/twitter/library/card/property/TokenizedText;->textTokenGroups:[Lcom/twitter/library/card/property/TextTokenGroup;

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/property/TokenizedText;->textTokenGroups:[Lcom/twitter/library/card/property/TextTokenGroup;

    invoke-static {v0, p1}, Lcom/twitter/library/util/Util;->a([Ljava/lang/Object;Ljava/io/ObjectOutput;)V

    return-void
.end method
