.class public Lcom/twitter/library/card/property/ApiRequest;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field private static final serialVersionUID:J = 0x64a0c26bae8bb96dL


# instance fields
.field public apiProxyRule:Ljava/lang/String;

.field public failureActionId:I

.field public method:I

.field public parameters:[Lcom/twitter/library/card/property/ApiRequestParameter;

.field public successActionId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/library/card/property/ApiRequest;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/twitter/library/card/property/ApiRequest;

    iget-object v2, p0, Lcom/twitter/library/card/property/ApiRequest;->apiProxyRule:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/card/property/ApiRequest;->apiProxyRule:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/property/ApiRequest;->apiProxyRule:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p1, Lcom/twitter/library/card/property/ApiRequest;->apiProxyRule:Ljava/lang/String;

    if-nez v2, :cond_3

    :cond_5
    iget v2, p0, Lcom/twitter/library/card/property/ApiRequest;->failureActionId:I

    iget v3, p1, Lcom/twitter/library/card/property/ApiRequest;->failureActionId:I

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget v2, p0, Lcom/twitter/library/card/property/ApiRequest;->method:I

    iget v3, p1, Lcom/twitter/library/card/property/ApiRequest;->method:I

    if-eq v2, v3, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget v2, p0, Lcom/twitter/library/card/property/ApiRequest;->successActionId:I

    iget v3, p1, Lcom/twitter/library/card/property/ApiRequest;->successActionId:I

    if-eq v2, v3, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Lcom/twitter/library/card/property/ApiRequest;->parameters:[Lcom/twitter/library/card/property/ApiRequestParameter;

    iget-object v3, p1, Lcom/twitter/library/card/property/ApiRequest;->parameters:[Lcom/twitter/library/card/property/ApiRequestParameter;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget v0, p0, Lcom/twitter/library/card/property/ApiRequest;->method:I

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/ApiRequest;->parameters:[Lcom/twitter/library/card/property/ApiRequestParameter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/card/property/ApiRequest;->parameters:[Lcom/twitter/library/card/property/ApiRequestParameter;

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/twitter/library/card/property/ApiRequest;->successActionId:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/twitter/library/card/property/ApiRequest;->failureActionId:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/library/card/property/ApiRequest;->apiProxyRule:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/library/card/property/ApiRequest;->apiProxyRule:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/property/ApiRequest;->method:I

    const-class v0, [Lcom/twitter/library/card/property/ApiRequestParameter;

    invoke-static {v0, p1}, Lcom/twitter/library/util/Util;->a(Ljava/lang/Class;Ljava/io/ObjectInput;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/card/property/ApiRequestParameter;

    iput-object v0, p0, Lcom/twitter/library/card/property/ApiRequest;->parameters:[Lcom/twitter/library/card/property/ApiRequestParameter;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/property/ApiRequest;->successActionId:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/property/ApiRequest;->failureActionId:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/property/ApiRequest;->apiProxyRule:Ljava/lang/String;

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    iget v0, p0, Lcom/twitter/library/card/property/ApiRequest;->method:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/card/property/ApiRequest;->parameters:[Lcom/twitter/library/card/property/ApiRequestParameter;

    invoke-static {v0, p1}, Lcom/twitter/library/util/Util;->a([Ljava/lang/Object;Ljava/io/ObjectOutput;)V

    iget v0, p0, Lcom/twitter/library/card/property/ApiRequest;->successActionId:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/card/property/ApiRequest;->failureActionId:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/card/property/ApiRequest;->apiProxyRule:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    return-void
.end method
