.class public Lcom/twitter/library/card/property/Fill;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field private static final serialVersionUID:J = -0x1219f90c4989e666L


# instance fields
.field public angle:F

.field private mColors:[I

.field private mCount:I

.field private mStops:[F

.field public solid:I

.field public type:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/card/property/Fill;)I
    .locals 1

    iget v0, p0, Lcom/twitter/library/card/property/Fill;->mCount:I

    return v0
.end method

.method static synthetic b(Lcom/twitter/library/card/property/Fill;)[F
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/property/Fill;->mStops:[F

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/library/card/property/Fill;)[I
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/property/Fill;->mColors:[I

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/card/property/Fill;->mCount:I

    return v0
.end method

.method public a(FI)V
    .locals 2

    const/16 v1, 0x8

    iget v0, p0, Lcom/twitter/library/card/property/Fill;->mCount:I

    if-nez v0, :cond_1

    new-array v0, v1, [F

    iput-object v0, p0, Lcom/twitter/library/card/property/Fill;->mStops:[F

    new-array v0, v1, [I

    iput-object v0, p0, Lcom/twitter/library/card/property/Fill;->mColors:[I

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/card/property/Fill;->mStops:[F

    iget v1, p0, Lcom/twitter/library/card/property/Fill;->mCount:I

    aput p1, v0, v1

    iget-object v0, p0, Lcom/twitter/library/card/property/Fill;->mColors:[I

    iget v1, p0, Lcom/twitter/library/card/property/Fill;->mCount:I

    aput p2, v0, v1

    iget v0, p0, Lcom/twitter/library/card/property/Fill;->mCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/library/card/property/Fill;->mCount:I

    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/twitter/library/card/property/Fill;->mCount:I

    if-ne v0, v1, :cond_0

    goto :goto_0
.end method

.method public a(I)V
    .locals 1

    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/library/card/property/Fill;->type:I

    iput p1, p0, Lcom/twitter/library/card/property/Fill;->solid:I

    return-void
.end method

.method public a(Landroid/graphics/Canvas;Landroid/graphics/RectF;Landroid/graphics/drawable/shapes/Shape;)V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/library/card/property/Fill;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/twitter/library/card/property/Fill;->type:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/card/property/Fill;->b(Landroid/graphics/Canvas;Landroid/graphics/RectF;Landroid/graphics/drawable/shapes/Shape;)V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/twitter/library/card/property/Fill;->type:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/card/property/Fill;->c(Landroid/graphics/Canvas;Landroid/graphics/RectF;Landroid/graphics/drawable/shapes/Shape;)V

    goto :goto_0
.end method

.method protected b(Landroid/graphics/Canvas;Landroid/graphics/RectF;Landroid/graphics/drawable/shapes/Shape;)V
    .locals 5

    new-instance v0, Landroid/graphics/drawable/PaintDrawable;

    iget v1, p0, Lcom/twitter/library/card/property/Fill;->solid:I

    invoke-direct {v0, v1}, Landroid/graphics/drawable/PaintDrawable;-><init>(I)V

    invoke-virtual {v0, p3}, Landroid/graphics/drawable/PaintDrawable;->setShape(Landroid/graphics/drawable/shapes/Shape;)V

    iget v1, p2, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    iget v2, p2, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    iget v3, p2, Landroid/graphics/RectF;->right:F

    float-to-int v3, v3

    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    float-to-int v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/PaintDrawable;->setBounds(IIII)V

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/PaintDrawable;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public b()Z
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget v2, p0, Lcom/twitter/library/card/property/Fill;->type:I

    if-ne v2, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v2, p0, Lcom/twitter/library/card/property/Fill;->type:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    iget v2, p0, Lcom/twitter/library/card/property/Fill;->solid:I

    invoke-static {v2}, Landroid/graphics/Color;->alpha(I)I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    move v2, v1

    :goto_1
    iget v3, p0, Lcom/twitter/library/card/property/Fill;->mCount:I

    if-ge v2, v3, :cond_0

    iget-object v3, p0, Lcom/twitter/library/card/property/Fill;->mColors:[I

    aget v3, v3, v2

    invoke-static {v3}, Landroid/graphics/Color;->alpha(I)I

    move-result v3

    if-eqz v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method protected c(Landroid/graphics/Canvas;Landroid/graphics/RectF;Landroid/graphics/drawable/shapes/Shape;)V
    .locals 6

    const/high16 v3, 0x3f000000    # 0.5f

    iget v0, p0, Lcom/twitter/library/card/property/Fill;->angle:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    double-to-float v0, v0

    iget v1, p0, Lcom/twitter/library/card/property/Fill;->angle:F

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v1

    double-to-float v1, v1

    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v2

    mul-float v4, v2, v3

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v2

    mul-float v5, v2, v3

    mul-float v2, v4, v0

    sub-float v2, v4, v2

    mul-float v3, v5, v1

    sub-float v3, v5, v3

    mul-float/2addr v0, v4

    add-float/2addr v4, v0

    mul-float v0, v5, v1

    add-float/2addr v5, v0

    new-instance v0, Lcom/twitter/library/card/property/b;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/card/property/b;-><init>(Lcom/twitter/library/card/property/Fill;FFFF)V

    new-instance v1, Landroid/graphics/drawable/PaintDrawable;

    const v2, -0xff0100

    invoke-direct {v1, v2}, Landroid/graphics/drawable/PaintDrawable;-><init>(I)V

    invoke-virtual {v1, p3}, Landroid/graphics/drawable/PaintDrawable;->setShape(Landroid/graphics/drawable/shapes/Shape;)V

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/PaintDrawable;->setShaderFactory(Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;)V

    iget v0, p2, Landroid/graphics/RectF;->left:F

    float-to-int v0, v0

    iget v2, p2, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    iget v3, p2, Landroid/graphics/RectF;->right:F

    float-to-int v3, v3

    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    float-to-int v4, v4

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/graphics/drawable/PaintDrawable;->setBounds(IIII)V

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/PaintDrawable;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/library/card/property/Fill;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/twitter/library/card/property/Fill;

    iget v2, p1, Lcom/twitter/library/card/property/Fill;->angle:F

    iget v3, p0, Lcom/twitter/library/card/property/Fill;->angle:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget v2, p0, Lcom/twitter/library/card/property/Fill;->mCount:I

    iget v3, p1, Lcom/twitter/library/card/property/Fill;->mCount:I

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget v2, p0, Lcom/twitter/library/card/property/Fill;->solid:I

    iget v3, p1, Lcom/twitter/library/card/property/Fill;->solid:I

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget v2, p0, Lcom/twitter/library/card/property/Fill;->type:I

    iget v3, p1, Lcom/twitter/library/card/property/Fill;->type:I

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lcom/twitter/library/card/property/Fill;->mColors:[I

    iget-object v3, p1, Lcom/twitter/library/card/property/Fill;->mColors:[I

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p0, Lcom/twitter/library/card/property/Fill;->mStops:[F

    iget-object v3, p1, Lcom/twitter/library/card/property/Fill;->mStops:[F

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([F[F)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget v0, p0, Lcom/twitter/library/card/property/Fill;->angle:F

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/library/card/property/Fill;->angle:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/twitter/library/card/property/Fill;->type:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/twitter/library/card/property/Fill;->solid:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Fill;->mStops:[F

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/card/property/Fill;->mStops:[F

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([F)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/library/card/property/Fill;->mColors:[I

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/library/card/property/Fill;->mColors:[I

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([I)I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/library/card/property/Fill;->mCount:I

    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 3

    const/16 v1, 0x8

    invoke-interface {p1}, Ljava/io/ObjectInput;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/property/Fill;->angle:F

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/property/Fill;->type:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/property/Fill;->solid:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/property/Fill;->mCount:I

    iget v0, p0, Lcom/twitter/library/card/property/Fill;->mCount:I

    if-lez v0, :cond_0

    new-array v0, v1, [F

    iput-object v0, p0, Lcom/twitter/library/card/property/Fill;->mStops:[F

    new-array v0, v1, [I

    iput-object v0, p0, Lcom/twitter/library/card/property/Fill;->mColors:[I

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/twitter/library/card/property/Fill;->mCount:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/card/property/Fill;->mStops:[F

    invoke-interface {p1}, Ljava/io/ObjectInput;->readFloat()F

    move-result v2

    aput v2, v1, v0

    iget-object v1, p0, Lcom/twitter/library/card/property/Fill;->mColors:[I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v2

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 2

    iget v0, p0, Lcom/twitter/library/card/property/Fill;->angle:F

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeFloat(F)V

    iget v0, p0, Lcom/twitter/library/card/property/Fill;->type:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/card/property/Fill;->solid:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/card/property/Fill;->mCount:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/twitter/library/card/property/Fill;->mCount:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/card/property/Fill;->mStops:[F

    aget v1, v1, v0

    invoke-interface {p1, v1}, Ljava/io/ObjectOutput;->writeFloat(F)V

    iget-object v1, p0, Lcom/twitter/library/card/property/Fill;->mColors:[I

    aget v1, v1, v0

    invoke-interface {p1, v1}, Ljava/io/ObjectOutput;->writeInt(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
