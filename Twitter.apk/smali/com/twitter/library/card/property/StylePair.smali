.class public Lcom/twitter/library/card/property/StylePair;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field private static final serialVersionUID:J = 0x435a0bf165ac70L


# instance fields
.field public elementId:I

.field public styleId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/library/card/property/StylePair;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/twitter/library/card/property/StylePair;

    iget v2, p0, Lcom/twitter/library/card/property/StylePair;->elementId:I

    iget v3, p1, Lcom/twitter/library/card/property/StylePair;->elementId:I

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget v2, p0, Lcom/twitter/library/card/property/StylePair;->styleId:I

    iget v3, p1, Lcom/twitter/library/card/property/StylePair;->styleId:I

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/twitter/library/card/property/StylePair;->elementId:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/library/card/property/StylePair;->styleId:I

    add-int/2addr v0, v1

    return v0
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/property/StylePair;->elementId:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/property/StylePair;->styleId:I

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    iget v0, p0, Lcom/twitter/library/card/property/StylePair;->elementId:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/card/property/StylePair;->styleId:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    return-void
.end method
