.class Lcom/twitter/library/card/property/b;
.super Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;
.source "Twttr"


# instance fields
.field final synthetic a:F

.field final synthetic b:F

.field final synthetic c:F

.field final synthetic d:F

.field final synthetic e:Lcom/twitter/library/card/property/Fill;


# direct methods
.method constructor <init>(Lcom/twitter/library/card/property/Fill;FFFF)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/card/property/b;->e:Lcom/twitter/library/card/property/Fill;

    iput p2, p0, Lcom/twitter/library/card/property/b;->a:F

    iput p3, p0, Lcom/twitter/library/card/property/b;->b:F

    iput p4, p0, Lcom/twitter/library/card/property/b;->c:F

    iput p5, p0, Lcom/twitter/library/card/property/b;->d:F

    invoke-direct {p0}, Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;-><init>()V

    return-void
.end method


# virtual methods
.method public resize(II)Landroid/graphics/Shader;
    .locals 8

    iget-object v0, p0, Lcom/twitter/library/card/property/b;->e:Lcom/twitter/library/card/property/Fill;

    invoke-static {v0}, Lcom/twitter/library/card/property/Fill;->a(Lcom/twitter/library/card/property/Fill;)I

    move-result v0

    new-array v6, v0, [F

    iget-object v0, p0, Lcom/twitter/library/card/property/b;->e:Lcom/twitter/library/card/property/Fill;

    invoke-static {v0}, Lcom/twitter/library/card/property/Fill;->a(Lcom/twitter/library/card/property/Fill;)I

    move-result v0

    new-array v5, v0, [I

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/twitter/library/card/property/b;->e:Lcom/twitter/library/card/property/Fill;

    invoke-static {v1}, Lcom/twitter/library/card/property/Fill;->a(Lcom/twitter/library/card/property/Fill;)I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/card/property/b;->e:Lcom/twitter/library/card/property/Fill;

    invoke-static {v1}, Lcom/twitter/library/card/property/Fill;->b(Lcom/twitter/library/card/property/Fill;)[F

    move-result-object v1

    aget v1, v1, v0

    aput v1, v6, v0

    iget-object v1, p0, Lcom/twitter/library/card/property/b;->e:Lcom/twitter/library/card/property/Fill;

    invoke-static {v1}, Lcom/twitter/library/card/property/Fill;->c(Lcom/twitter/library/card/property/Fill;)[I

    move-result-object v1

    aget v1, v1, v0

    aput v1, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/graphics/LinearGradient;

    iget v1, p0, Lcom/twitter/library/card/property/b;->a:F

    iget v2, p0, Lcom/twitter/library/card/property/b;->b:F

    iget v3, p0, Lcom/twitter/library/card/property/b;->c:F

    iget v4, p0, Lcom/twitter/library/card/property/b;->d:F

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    return-object v0
.end method
