.class public Lcom/twitter/library/card/property/Border;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field private static final serialVersionUID:J = -0x1de14884f87b4325L


# instance fields
.field public background:Lcom/twitter/library/card/property/Fill;

.field public width:F


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/library/card/property/Border;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/twitter/library/card/property/Border;

    iget v2, p1, Lcom/twitter/library/card/property/Border;->width:F

    iget v3, p0, Lcom/twitter/library/card/property/Border;->width:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/twitter/library/card/property/Border;->background:Lcom/twitter/library/card/property/Fill;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/card/property/Border;->background:Lcom/twitter/library/card/property/Fill;

    iget-object v3, p1, Lcom/twitter/library/card/property/Border;->background:Lcom/twitter/library/card/property/Fill;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/Fill;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p1, Lcom/twitter/library/card/property/Border;->background:Lcom/twitter/library/card/property/Fill;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget v0, p0, Lcom/twitter/library/card/property/Border;->width:F

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/library/card/property/Border;->width:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/library/card/property/Border;->background:Lcom/twitter/library/card/property/Fill;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/library/card/property/Border;->background:Lcom/twitter/library/card/property/Fill;

    invoke-virtual {v1}, Lcom/twitter/library/card/property/Fill;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1

    invoke-interface {p1}, Ljava/io/ObjectInput;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/property/Border;->width:F

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Fill;

    iput-object v0, p0, Lcom/twitter/library/card/property/Border;->background:Lcom/twitter/library/card/property/Fill;

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    iget v0, p0, Lcom/twitter/library/card/property/Border;->width:F

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeFloat(F)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Border;->background:Lcom/twitter/library/card/property/Fill;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    return-void
.end method
