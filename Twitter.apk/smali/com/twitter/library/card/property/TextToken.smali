.class public Lcom/twitter/library/card/property/TextToken;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field private static final serialVersionUID:J = -0x6fd908f0875deaceL


# instance fields
.field public id:I

.field public longPressActionId:I

.field public pressDownActionId:I

.field public pressUpActionId:I

.field public styleId:I

.field public tapActionId:I

.field public text:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    iget v0, p0, Lcom/twitter/library/card/property/TextToken;->pressDownActionId:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/library/card/property/TextToken;->pressUpActionId:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/library/card/property/TextToken;->tapActionId:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/library/card/property/TextToken;->longPressActionId:I

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/library/card/property/TextToken;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/twitter/library/card/property/TextToken;

    iget v2, p0, Lcom/twitter/library/card/property/TextToken;->id:I

    iget v3, p1, Lcom/twitter/library/card/property/TextToken;->id:I

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget v2, p0, Lcom/twitter/library/card/property/TextToken;->longPressActionId:I

    iget v3, p1, Lcom/twitter/library/card/property/TextToken;->longPressActionId:I

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget v2, p0, Lcom/twitter/library/card/property/TextToken;->pressDownActionId:I

    iget v3, p1, Lcom/twitter/library/card/property/TextToken;->pressDownActionId:I

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget v2, p0, Lcom/twitter/library/card/property/TextToken;->pressUpActionId:I

    iget v3, p1, Lcom/twitter/library/card/property/TextToken;->pressUpActionId:I

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget v2, p0, Lcom/twitter/library/card/property/TextToken;->styleId:I

    iget v3, p1, Lcom/twitter/library/card/property/TextToken;->styleId:I

    if-eq v2, v3, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget v2, p0, Lcom/twitter/library/card/property/TextToken;->tapActionId:I

    iget v3, p1, Lcom/twitter/library/card/property/TextToken;->tapActionId:I

    if-eq v2, v3, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Lcom/twitter/library/card/property/TextToken;->text:Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/twitter/library/card/property/TextToken;->text:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/property/TextToken;->text:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p1, Lcom/twitter/library/card/property/TextToken;->text:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/twitter/library/card/property/TextToken;->id:I

    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/TextToken;->text:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/property/TextToken;->text:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/library/card/property/TextToken;->styleId:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/library/card/property/TextToken;->pressDownActionId:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/library/card/property/TextToken;->pressUpActionId:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/library/card/property/TextToken;->tapActionId:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/library/card/property/TextToken;->longPressActionId:I

    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/property/TextToken;->id:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/property/TextToken;->text:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/property/TextToken;->styleId:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/property/TextToken;->pressDownActionId:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/property/TextToken;->pressUpActionId:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/property/TextToken;->tapActionId:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/property/TextToken;->longPressActionId:I

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    iget v0, p0, Lcom/twitter/library/card/property/TextToken;->id:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/card/property/TextToken;->text:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget v0, p0, Lcom/twitter/library/card/property/TextToken;->styleId:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/card/property/TextToken;->pressDownActionId:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/card/property/TextToken;->pressUpActionId:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/card/property/TextToken;->tapActionId:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/card/property/TextToken;->longPressActionId:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    return-void
.end method
