.class public Lcom/twitter/library/card/property/PurchaseRequest;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field private static final serialVersionUID:J = -0x748ad16a9351be05L


# instance fields
.field public failureActionId:I

.field public parameters:[Lcom/twitter/library/card/property/PurchaseRequestParameter;

.field public successActionId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/library/card/property/PurchaseRequest;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/twitter/library/card/property/PurchaseRequest;

    iget-object v2, p0, Lcom/twitter/library/card/property/PurchaseRequest;->parameters:[Lcom/twitter/library/card/property/PurchaseRequestParameter;

    iget-object v3, p1, Lcom/twitter/library/card/property/PurchaseRequest;->parameters:[Lcom/twitter/library/card/property/PurchaseRequestParameter;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget v2, p0, Lcom/twitter/library/card/property/PurchaseRequest;->successActionId:I

    iget v3, p1, Lcom/twitter/library/card/property/PurchaseRequest;->successActionId:I

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget v2, p0, Lcom/twitter/library/card/property/PurchaseRequest;->failureActionId:I

    iget v3, p1, Lcom/twitter/library/card/property/PurchaseRequest;->failureActionId:I

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/card/property/PurchaseRequest;->parameters:[Lcom/twitter/library/card/property/PurchaseRequestParameter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/property/PurchaseRequest;->parameters:[Lcom/twitter/library/card/property/PurchaseRequestParameter;

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/library/card/property/PurchaseRequest;->successActionId:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/library/card/property/PurchaseRequest;->failureActionId:I

    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1

    const-class v0, [Lcom/twitter/library/card/property/PurchaseRequestParameter;

    invoke-static {v0, p1}, Lcom/twitter/library/util/Util;->a(Ljava/lang/Class;Ljava/io/ObjectInput;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/card/property/PurchaseRequestParameter;

    iput-object v0, p0, Lcom/twitter/library/card/property/PurchaseRequest;->parameters:[Lcom/twitter/library/card/property/PurchaseRequestParameter;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/property/PurchaseRequest;->successActionId:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/property/PurchaseRequest;->failureActionId:I

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/property/PurchaseRequest;->parameters:[Lcom/twitter/library/card/property/PurchaseRequestParameter;

    invoke-static {v0, p1}, Lcom/twitter/library/util/Util;->a([Ljava/lang/Object;Ljava/io/ObjectOutput;)V

    iget v0, p0, Lcom/twitter/library/card/property/PurchaseRequest;->successActionId:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/card/property/PurchaseRequest;->failureActionId:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    return-void
.end method
