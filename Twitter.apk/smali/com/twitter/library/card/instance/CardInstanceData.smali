.class public Lcom/twitter/library/card/instance/CardInstanceData;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field private static final serialVersionUID:J = -0x34096f0b1949d1a4L


# instance fields
.field public audienceBucket:Ljava/lang/String;

.field public audienceName:Ljava/lang/String;

.field public bindingValues:Ljava/util/HashMap;

.field public cardTypeURL:Ljava/lang/String;

.field public forwardAudienceBucket:Ljava/lang/String;

.field public forwardAudienceName:Ljava/lang/String;

.field public forwardBindingValues:Ljava/util/HashMap;

.field public forwardCardTypeURL:Ljava/lang/String;

.field public forwardName:Ljava/lang/String;

.field public forwardUsers:Ljava/util/HashMap;

.field public name:Ljava/lang/String;

.field public url:Ljava/lang/String;

.field public users:Ljava/util/HashMap;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->bindingValues:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->users:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardBindingValues:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardUsers:Ljava/util/HashMap;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->url:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->cardTypeURL:Ljava/lang/String;

    return-void
.end method

.method private e()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->bindingValues:Ljava/util/HashMap;

    const-string/jumbo v1, "amplify_url_vmap"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/instance/BindingValue;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->bindingValues:Ljava/util/HashMap;

    const-string/jumbo v1, "amplify_url"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/instance/BindingValue;

    :cond_0
    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/twitter/library/card/instance/BindingValue;->value:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardCardTypeURL:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardBindingValues:Ljava/util/HashMap;

    const-string/jumbo v1, "_forward_card_height_"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/instance/BindingValue;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/twitter/library/card/instance/BindingValue;->value:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/twitter/library/card/instance/BindingValue;->value:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    float-to-int v0, v0

    :goto_0
    int-to-float v0, v0

    sget v1, Lcom/twitter/library/util/b;->a:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0

    :cond_0
    invoke-static {}, Lcom/twitter/library/featureswitch/a;->ao()I

    move-result v0

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->bindingValues:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/library/card/instance/CardInstanceData;->e()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->bindingValues:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->bindingValues:Ljava/util/HashMap;

    const-string/jumbo v2, "amplify_content_id"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/instance/BindingValue;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/twitter/library/card/instance/BindingValue;->value:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/library/card/instance/CardInstanceData;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/twitter/library/card/instance/CardInstanceData;

    iget-object v2, p0, Lcom/twitter/library/card/instance/CardInstanceData;->audienceBucket:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/card/instance/CardInstanceData;->audienceBucket:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/instance/CardInstanceData;->audienceBucket:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p1, Lcom/twitter/library/card/instance/CardInstanceData;->audienceBucket:Ljava/lang/String;

    if-nez v2, :cond_3

    :cond_5
    iget-object v2, p0, Lcom/twitter/library/card/instance/CardInstanceData;->audienceName:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/twitter/library/card/instance/CardInstanceData;->audienceName:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/instance/CardInstanceData;->audienceName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    :cond_6
    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p1, Lcom/twitter/library/card/instance/CardInstanceData;->audienceName:Ljava/lang/String;

    if-nez v2, :cond_6

    :cond_8
    iget-object v2, p0, Lcom/twitter/library/card/instance/CardInstanceData;->bindingValues:Ljava/util/HashMap;

    iget-object v3, p1, Lcom/twitter/library/card/instance/CardInstanceData;->bindingValues:Ljava/util/HashMap;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p0, Lcom/twitter/library/card/instance/CardInstanceData;->cardTypeURL:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/instance/CardInstanceData;->cardTypeURL:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardAudienceBucket:Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardAudienceBucket:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/instance/CardInstanceData;->forwardAudienceBucket:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    goto :goto_0

    :cond_c
    iget-object v2, p1, Lcom/twitter/library/card/instance/CardInstanceData;->forwardAudienceBucket:Ljava/lang/String;

    if-nez v2, :cond_b

    :cond_d
    iget-object v2, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardAudienceName:Ljava/lang/String;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardAudienceName:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/instance/CardInstanceData;->forwardAudienceName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    :cond_e
    move v0, v1

    goto :goto_0

    :cond_f
    iget-object v2, p1, Lcom/twitter/library/card/instance/CardInstanceData;->forwardAudienceName:Ljava/lang/String;

    if-nez v2, :cond_e

    :cond_10
    iget-object v2, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardBindingValues:Ljava/util/HashMap;

    iget-object v3, p1, Lcom/twitter/library/card/instance/CardInstanceData;->forwardBindingValues:Ljava/util/HashMap;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    move v0, v1

    goto :goto_0

    :cond_11
    iget-object v2, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardCardTypeURL:Ljava/lang/String;

    if-eqz v2, :cond_13

    iget-object v2, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardCardTypeURL:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/instance/CardInstanceData;->forwardCardTypeURL:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_14

    :cond_12
    move v0, v1

    goto/16 :goto_0

    :cond_13
    iget-object v2, p1, Lcom/twitter/library/card/instance/CardInstanceData;->forwardCardTypeURL:Ljava/lang/String;

    if-nez v2, :cond_12

    :cond_14
    iget-object v2, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardName:Ljava/lang/String;

    if-eqz v2, :cond_16

    iget-object v2, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardName:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/instance/CardInstanceData;->forwardName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    :cond_15
    move v0, v1

    goto/16 :goto_0

    :cond_16
    iget-object v2, p1, Lcom/twitter/library/card/instance/CardInstanceData;->forwardName:Ljava/lang/String;

    if-nez v2, :cond_15

    :cond_17
    iget-object v2, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardUsers:Ljava/util/HashMap;

    iget-object v3, p1, Lcom/twitter/library/card/instance/CardInstanceData;->forwardUsers:Ljava/util/HashMap;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    move v0, v1

    goto/16 :goto_0

    :cond_18
    iget-object v2, p0, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_19

    move v0, v1

    goto/16 :goto_0

    :cond_19
    iget-object v2, p0, Lcom/twitter/library/card/instance/CardInstanceData;->url:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/instance/CardInstanceData;->url:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    move v0, v1

    goto/16 :goto_0

    :cond_1a
    iget-object v2, p0, Lcom/twitter/library/card/instance/CardInstanceData;->users:Ljava/util/HashMap;

    iget-object v3, p1, Lcom/twitter/library/card/instance/CardInstanceData;->users:Ljava/util/HashMap;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/library/card/instance/CardInstanceData;->url:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/library/card/instance/CardInstanceData;->cardTypeURL:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/library/card/instance/CardInstanceData;->bindingValues:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/library/card/instance/CardInstanceData;->users:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardName:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardCardTypeURL:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardCardTypeURL:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardBindingValues:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardUsers:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->audienceName:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->audienceName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->audienceBucket:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->audienceBucket:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardAudienceName:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardAudienceName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardAudienceBucket:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardAudienceBucket:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_4
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 2

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->url:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->cardTypeURL:Ljava/lang/String;

    const-class v0, Ljava/lang/String;

    const-class v1, Lcom/twitter/library/card/instance/BindingValue;

    invoke-static {v0, v1, p1}, Lcom/twitter/library/util/Util;->a(Ljava/lang/Class;Ljava/lang/Class;Ljava/io/ObjectInput;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->bindingValues:Ljava/util/HashMap;

    const-class v0, Ljava/lang/String;

    const-class v1, Lcom/twitter/library/api/TwitterUser;

    invoke-static {v0, v1, p1}, Lcom/twitter/library/util/Util;->a(Ljava/lang/Class;Ljava/lang/Class;Ljava/io/ObjectInput;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->users:Ljava/util/HashMap;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardName:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardCardTypeURL:Ljava/lang/String;

    const-class v0, Ljava/lang/String;

    const-class v1, Lcom/twitter/library/card/instance/BindingValue;

    invoke-static {v0, v1, p1}, Lcom/twitter/library/util/Util;->a(Ljava/lang/Class;Ljava/lang/Class;Ljava/io/ObjectInput;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardBindingValues:Ljava/util/HashMap;

    const-class v0, Ljava/lang/String;

    const-class v1, Lcom/twitter/library/api/TwitterUser;

    invoke-static {v0, v1, p1}, Lcom/twitter/library/util/Util;->a(Ljava/lang/Class;Ljava/lang/Class;Ljava/io/ObjectInput;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardUsers:Ljava/util/HashMap;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->audienceName:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->audienceBucket:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardAudienceName:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardAudienceBucket:Ljava/lang/String;

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->url:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->cardTypeURL:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->bindingValues:Ljava/util/HashMap;

    invoke-static {v0, p1}, Lcom/twitter/library/util/Util;->a(Ljava/util/HashMap;Ljava/io/ObjectOutput;)V

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->users:Ljava/util/HashMap;

    invoke-static {v0, p1}, Lcom/twitter/library/util/Util;->a(Ljava/util/HashMap;Ljava/io/ObjectOutput;)V

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardName:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardCardTypeURL:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardBindingValues:Ljava/util/HashMap;

    invoke-static {v0, p1}, Lcom/twitter/library/util/Util;->a(Ljava/util/HashMap;Ljava/io/ObjectOutput;)V

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardUsers:Ljava/util/HashMap;

    invoke-static {v0, p1}, Lcom/twitter/library/util/Util;->a(Ljava/util/HashMap;Ljava/io/ObjectOutput;)V

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->audienceName:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->audienceBucket:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardAudienceName:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardAudienceBucket:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    return-void
.end method
