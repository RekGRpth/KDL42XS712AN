.class Lcom/twitter/library/card/element/g;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/util/ac;


# instance fields
.field private a:Lcom/twitter/library/util/aa;

.field private b:Lcom/twitter/library/util/m;

.field private c:J

.field private d:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Lcom/twitter/library/util/aa;Lcom/twitter/library/util/m;JLcom/twitter/library/card/element/Image;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/card/element/g;->a:Lcom/twitter/library/util/aa;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p5}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/library/card/element/g;->d:Ljava/lang/ref/WeakReference;

    iput-wide p3, p0, Lcom/twitter/library/card/element/g;->c:J

    iput-object p2, p0, Lcom/twitter/library/card/element/g;->b:Lcom/twitter/library/util/m;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/library/card/element/g;->a:Lcom/twitter/library/util/aa;

    invoke-virtual {v0, p0}, Lcom/twitter/library/util/aa;->a(Lcom/twitter/library/util/ac;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/g;->a:Lcom/twitter/library/util/aa;

    iget-wide v1, p0, Lcom/twitter/library/card/element/g;->c:J

    iget-object v3, p0, Lcom/twitter/library/card/element/g;->b:Lcom/twitter/library/util/m;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/util/aa;->a(JLcom/twitter/library/util/m;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/twitter/library/card/element/g;->a:Lcom/twitter/library/util/aa;

    invoke-virtual {v0, p0}, Lcom/twitter/library/util/aa;->b(Lcom/twitter/library/util/ac;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/g;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/element/Image;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/twitter/library/card/element/g;->b:Lcom/twitter/library/util/m;

    invoke-virtual {v0, v2, v1}, Lcom/twitter/library/card/element/Image;->a(Lcom/twitter/library/util/m;Landroid/graphics/Bitmap;)V

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Retrieved Cached Image "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/card/element/g;->b:Lcom/twitter/library/util/m;

    iget-object v1, v1, Lcom/twitter/library/util/m;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/library/card/CardDebugLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/library/util/aa;Ljava/util/HashMap;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/card/element/g;->b:Lcom/twitter/library/util/m;

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ae;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/twitter/library/util/ae;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1, p0}, Lcom/twitter/library/util/aa;->b(Lcom/twitter/library/util/ac;)V

    iget-object v1, p0, Lcom/twitter/library/card/element/g;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/card/element/Image;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/twitter/library/card/element/g;->b:Lcom/twitter/library/util/m;

    iget-object v0, v0, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/card/element/Image;->a(Lcom/twitter/library/util/m;Landroid/graphics/Bitmap;)V

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Fetched Image "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/card/element/g;->b:Lcom/twitter/library/util/m;

    iget-object v1, v1, Lcom/twitter/library/util/m;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/library/card/CardDebugLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method
