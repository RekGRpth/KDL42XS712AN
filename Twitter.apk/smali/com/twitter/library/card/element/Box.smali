.class public Lcom/twitter/library/card/element/Box;
.super Lcom/twitter/library/card/element/Element;
.source "Twttr"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field private static final serialVersionUID:J = -0x45925849f362ce1cL


# instance fields
.field public background:Lcom/twitter/library/card/property/Fill;

.field public border:Lcom/twitter/library/card/property/Border;

.field public cornerRadius:F

.field private mBackgroundShape:Landroid/graphics/drawable/shapes/RoundRectShape;

.field private mBorderShape:Landroid/graphics/drawable/shapes/RoundRectShape;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/library/card/element/Element;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Canvas;)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/library/card/element/Box;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v0, v0, Lcom/twitter/library/card/property/Style;->shadow:Lcom/twitter/library/card/property/Shadow;

    iget-object v1, p0, Lcom/twitter/library/card/element/Box;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v1, v1, Lcom/twitter/library/card/property/Style;->border:Lcom/twitter/library/card/property/Border;

    iget-object v2, p0, Lcom/twitter/library/card/element/Box;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v2, v2, Lcom/twitter/library/card/property/Style;->cornerRadius:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    if-eqz v0, :cond_0

    iget-boolean v3, v0, Lcom/twitter/library/card/property/Shadow;->inset:Z

    if-eqz v3, :cond_0

    const/4 v3, 0x0

    iget v4, v1, Lcom/twitter/library/card/property/Border;->width:F

    sub-float/2addr v2, v4

    invoke-static {v3, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iget-object v3, p0, Lcom/twitter/library/card/element/Box;->mLayoutSize:Lcom/twitter/library/card/property/Vector2F;

    iget v3, v3, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget-object v4, p0, Lcom/twitter/library/card/element/Box;->mLayoutSize:Lcom/twitter/library/card/property/Vector2F;

    iget v4, v4, Lcom/twitter/library/card/property/Vector2F;->y:F

    invoke-virtual {v0, p1, v3, v4, v2}, Lcom/twitter/library/card/property/Shadow;->b(Landroid/graphics/Canvas;FFF)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/card/element/Box;->mBorderShape:Landroid/graphics/drawable/shapes/RoundRectShape;

    if-eqz v0, :cond_1

    iget-object v0, v1, Lcom/twitter/library/card/property/Border;->background:Lcom/twitter/library/card/property/Fill;

    iget-object v1, p0, Lcom/twitter/library/card/element/Box;->mLayoutRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/twitter/library/card/element/Box;->mBorderShape:Landroid/graphics/drawable/shapes/RoundRectShape;

    invoke-virtual {v0, p1, v1, v2}, Lcom/twitter/library/card/property/Fill;->a(Landroid/graphics/Canvas;Landroid/graphics/RectF;Landroid/graphics/drawable/shapes/Shape;)V

    :cond_1
    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Element;->a(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public b(Landroid/graphics/Canvas;)V
    .locals 4

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Element;->b(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Box;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v0, v0, Lcom/twitter/library/card/property/Style;->shadow:Lcom/twitter/library/card/property/Shadow;

    iget-object v1, p0, Lcom/twitter/library/card/element/Box;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v1, v1, Lcom/twitter/library/card/property/Style;->cornerRadius:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    if-eqz v0, :cond_0

    iget-boolean v2, v0, Lcom/twitter/library/card/property/Shadow;->inset:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/twitter/library/card/element/Box;->mLayoutSize:Lcom/twitter/library/card/property/Vector2F;

    iget v2, v2, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget-object v3, p0, Lcom/twitter/library/card/element/Box;->mLayoutSize:Lcom/twitter/library/card/property/Vector2F;

    iget v3, v3, Lcom/twitter/library/card/property/Vector2F;->y:F

    invoke-virtual {v0, p1, v2, v3, v1}, Lcom/twitter/library/card/property/Shadow;->a(Landroid/graphics/Canvas;FFF)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/card/element/Box;->mBackgroundShape:Landroid/graphics/drawable/shapes/RoundRectShape;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/card/element/Box;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v0, v0, Lcom/twitter/library/card/property/Style;->background:Lcom/twitter/library/card/property/Fill;

    iget-object v1, p0, Lcom/twitter/library/card/element/Box;->mLayoutRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/twitter/library/card/element/Box;->mBackgroundShape:Landroid/graphics/drawable/shapes/RoundRectShape;

    invoke-virtual {v0, p1, v1, v2}, Lcom/twitter/library/card/property/Fill;->a(Landroid/graphics/Canvas;Landroid/graphics/RectF;Landroid/graphics/drawable/shapes/Shape;)V

    :cond_1
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/library/card/element/Box;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Element;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/twitter/library/card/element/Box;

    iget v2, p1, Lcom/twitter/library/card/element/Box;->cornerRadius:F

    iget v3, p0, Lcom/twitter/library/card/element/Box;->cornerRadius:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/twitter/library/card/element/Box;->background:Lcom/twitter/library/card/property/Fill;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/twitter/library/card/element/Box;->background:Lcom/twitter/library/card/property/Fill;

    iget-object v3, p1, Lcom/twitter/library/card/element/Box;->background:Lcom/twitter/library/card/property/Fill;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/Fill;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p1, Lcom/twitter/library/card/element/Box;->background:Lcom/twitter/library/card/property/Fill;

    if-nez v2, :cond_5

    :cond_7
    iget-object v2, p0, Lcom/twitter/library/card/element/Box;->border:Lcom/twitter/library/card/property/Border;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/twitter/library/card/element/Box;->border:Lcom/twitter/library/card/property/Border;

    iget-object v3, p1, Lcom/twitter/library/card/element/Box;->border:Lcom/twitter/library/card/property/Border;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/Border;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p1, Lcom/twitter/library/card/element/Box;->border:Lcom/twitter/library/card/property/Border;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 4

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/twitter/library/card/element/Element;->hashCode()I

    move-result v0

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/element/Box;->background:Lcom/twitter/library/card/property/Fill;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/card/element/Box;->background:Lcom/twitter/library/card/property/Fill;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/Fill;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/element/Box;->border:Lcom/twitter/library/card/property/Border;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/card/element/Box;->border:Lcom/twitter/library/card/property/Border;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/Border;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/twitter/library/card/element/Box;->cornerRadius:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    iget v1, p0, Lcom/twitter/library/card/element/Box;->cornerRadius:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method protected p()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/library/card/element/Element;->p()V

    iget-object v0, p0, Lcom/twitter/library/card/element/Box;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v1, p0, Lcom/twitter/library/card/element/Box;->background:Lcom/twitter/library/card/property/Fill;

    iput-object v1, v0, Lcom/twitter/library/card/property/Style;->background:Lcom/twitter/library/card/property/Fill;

    iget-object v0, p0, Lcom/twitter/library/card/element/Box;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v1, p0, Lcom/twitter/library/card/element/Box;->border:Lcom/twitter/library/card/property/Border;

    iput-object v1, v0, Lcom/twitter/library/card/property/Style;->border:Lcom/twitter/library/card/property/Border;

    iget-object v0, p0, Lcom/twitter/library/card/element/Box;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget v1, p0, Lcom/twitter/library/card/element/Box;->cornerRadius:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/card/property/Style;->cornerRadius:Ljava/lang/Float;

    return-void
.end method

.method public q()V
    .locals 11

    const/4 v10, 0x2

    const/4 v7, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v9, 0x0

    iget-object v0, p0, Lcom/twitter/library/card/element/Box;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v3, v0, Lcom/twitter/library/card/property/Style;->background:Lcom/twitter/library/card/property/Fill;

    iget-object v0, p0, Lcom/twitter/library/card/element/Box;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v0, v0, Lcom/twitter/library/card/property/Style;->border:Lcom/twitter/library/card/property/Border;

    iget v4, v0, Lcom/twitter/library/card/property/Border;->width:F

    iget-object v5, p0, Lcom/twitter/library/card/element/Box;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v5, v5, Lcom/twitter/library/card/property/Style;->cornerRadius:Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    sub-float v6, v5, v4

    invoke-static {v7, v6}, Ljava/lang/Math;->max(FF)F

    move-result v6

    cmpl-float v7, v4, v7

    if-lez v7, :cond_0

    iget-object v0, v0, Lcom/twitter/library/card/property/Border;->background:Lcom/twitter/library/card/property/Fill;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/Fill;->b()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const/16 v7, 0x8

    new-array v7, v7, [F

    aput v5, v7, v2

    aput v5, v7, v1

    aput v5, v7, v10

    const/4 v8, 0x3

    aput v5, v7, v8

    const/4 v8, 0x4

    aput v5, v7, v8

    const/4 v8, 0x5

    aput v5, v7, v8

    const/4 v8, 0x6

    aput v5, v7, v8

    const/4 v8, 0x7

    aput v5, v7, v8

    invoke-virtual {v3}, Lcom/twitter/library/card/property/Fill;->b()Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v9, p0, Lcom/twitter/library/card/element/Box;->mBackgroundShape:Landroid/graphics/drawable/shapes/RoundRectShape;

    :goto_1
    if-eqz v0, :cond_2

    const/16 v0, 0x8

    new-array v0, v0, [F

    aput v6, v0, v2

    aput v6, v0, v1

    aput v6, v0, v10

    const/4 v1, 0x3

    aput v6, v0, v1

    const/4 v1, 0x4

    aput v6, v0, v1

    const/4 v1, 0x5

    aput v6, v0, v1

    const/4 v1, 0x6

    aput v6, v0, v1

    const/4 v1, 0x7

    aput v6, v0, v1

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1, v4, v4, v4, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    new-instance v2, Landroid/graphics/drawable/shapes/RoundRectShape;

    invoke-direct {v2, v7, v1, v0}, Landroid/graphics/drawable/shapes/RoundRectShape;-><init>([FLandroid/graphics/RectF;[F)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Box;->mLayoutSize:Lcom/twitter/library/card/property/Vector2F;

    iget v0, v0, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget-object v1, p0, Lcom/twitter/library/card/element/Box;->mLayoutSize:Lcom/twitter/library/card/property/Vector2F;

    iget v1, v1, Lcom/twitter/library/card/property/Vector2F;->y:F

    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/shapes/RoundRectShape;->resize(FF)V

    iput-object v2, p0, Lcom/twitter/library/card/element/Box;->mBorderShape:Landroid/graphics/drawable/shapes/RoundRectShape;

    :goto_2
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    new-instance v3, Landroid/graphics/drawable/shapes/RoundRectShape;

    invoke-direct {v3, v7, v9, v9}, Landroid/graphics/drawable/shapes/RoundRectShape;-><init>([FLandroid/graphics/RectF;[F)V

    iget-object v5, p0, Lcom/twitter/library/card/element/Box;->mLayoutSize:Lcom/twitter/library/card/property/Vector2F;

    iget v5, v5, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget-object v8, p0, Lcom/twitter/library/card/element/Box;->mLayoutSize:Lcom/twitter/library/card/property/Vector2F;

    iget v8, v8, Lcom/twitter/library/card/property/Vector2F;->y:F

    invoke-virtual {v3, v5, v8}, Landroid/graphics/drawable/shapes/RoundRectShape;->resize(FF)V

    iput-object v3, p0, Lcom/twitter/library/card/element/Box;->mBackgroundShape:Landroid/graphics/drawable/shapes/RoundRectShape;

    goto :goto_1

    :cond_2
    iput-object v9, p0, Lcom/twitter/library/card/element/Box;->mBorderShape:Landroid/graphics/drawable/shapes/RoundRectShape;

    goto :goto_2
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Element;->readExternal(Ljava/io/ObjectInput;)V

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Fill;

    iput-object v0, p0, Lcom/twitter/library/card/element/Box;->background:Lcom/twitter/library/card/property/Fill;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Border;

    iput-object v0, p0, Lcom/twitter/library/card/element/Box;->border:Lcom/twitter/library/card/property/Border;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/element/Box;->cornerRadius:F

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Element;->writeExternal(Ljava/io/ObjectOutput;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Box;->background:Lcom/twitter/library/card/property/Fill;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Box;->border:Lcom/twitter/library/card/property/Border;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget v0, p0, Lcom/twitter/library/card/element/Box;->cornerRadius:F

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeFloat(F)V

    return-void
.end method
