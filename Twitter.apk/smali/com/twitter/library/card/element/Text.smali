.class public Lcom/twitter/library/card/element/Text;
.super Lcom/twitter/library/card/element/Element;
.source "Twttr"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field private static final serialVersionUID:J = -0x1296ab7b120b3b28L


# instance fields
.field public alignmentMode:Lcom/twitter/library/card/property/Vector2;

.field public color:I

.field public fontBold:Z

.field public fontItalic:Z

.field public fontSize:F

.field public fontUnderline:Z

.field public lineHeight:F

.field private mCookedText:Landroid/text/SpannableStringBuilder;

.field private mFontName:Ljava/lang/String;

.field private mHasActionSpans:Z

.field private mRawText:Ljava/lang/String;

.field private mTouchedActionSpan:Lcom/twitter/library/card/element/k;

.field private mTypeface:Landroid/graphics/Typeface;

.field public maxLines:I

.field public overflowMode:I

.field public tokenizedTextId:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/library/card/element/Element;-><init>()V

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/card/element/Text;->mCookedText:Landroid/text/SpannableStringBuilder;

    return-void
.end method

.method private a(Lcom/twitter/library/card/property/Style;Landroid/text/SpannableStringBuilder;II)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->background:Lcom/twitter/library/card/property/Fill;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/text/style/BackgroundColorSpan;

    iget-object v1, p1, Lcom/twitter/library/card/property/Style;->background:Lcom/twitter/library/card/property/Fill;

    iget v1, v1, Lcom/twitter/library/card/property/Fill;->solid:I

    invoke-direct {v0, v1}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    invoke-virtual {p2, v0, p3, p4, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_0
    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->fontName:Ljava/lang/String;

    if-eqz v0, :cond_1

    new-instance v0, Landroid/text/style/TypefaceSpan;

    iget-object v1, p1, Lcom/twitter/library/card/property/Style;->fontName:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/text/style/TypefaceSpan;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v0, p3, p4, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_1
    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->fontSize:Ljava/lang/Float;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->fontSize:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    new-instance v1, Landroid/text/style/AbsoluteSizeSpan;

    float-to-int v0, v0

    invoke-direct {v1, v0}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-virtual {p2, v1, p3, p4, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_2
    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->color:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    iget-object v1, p1, Lcom/twitter/library/card/property/Style;->color:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p2, v0, p3, p4, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_3
    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->lineHeight:Ljava/lang/Float;

    if-eqz v0, :cond_4

    new-instance v0, Landroid/text/style/RelativeSizeSpan;

    iget-object v1, p1, Lcom/twitter/library/card/property/Style;->lineHeight:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-direct {v0, v1}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    invoke-virtual {p2, v0, p3, p4, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_4
    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->fontBold:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->fontBold:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {p2, v0, p3, p4, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_5
    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->fontUnderline:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->fontUnderline:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Landroid/text/style/UnderlineSpan;

    invoke-direct {v0}, Landroid/text/style/UnderlineSpan;-><init>()V

    invoke-virtual {p2, v0, p3, p4, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_6
    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->fontItalic:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    iget-object v0, p1, Lcom/twitter/library/card/property/Style;->fontItalic:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7

    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {p2, v0, p3, p4, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_7
    return-void
.end method


# virtual methods
.method public E()Z
    .locals 1

    invoke-super {p0}, Lcom/twitter/library/card/element/Element;->E()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/card/element/Text;->mHasActionSpans:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public J()V
    .locals 14

    const/4 v4, 0x5

    const/16 v13, 0xe

    const/4 v2, 0x3

    const/4 v3, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/library/card/element/Text;->mView:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/library/card/element/Text;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v6, v1, Lcom/twitter/library/card/property/Style;->fontName:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/library/card/element/Text;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v1, v1, Lcom/twitter/library/card/property/Style;->fontSize:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget-object v7, p0, Lcom/twitter/library/card/element/Text;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v7, v7, Lcom/twitter/library/card/property/Style;->color:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iget-object v8, p0, Lcom/twitter/library/card/element/Text;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v8, v8, Lcom/twitter/library/card/property/Style;->lineHeight:Ljava/lang/Float;

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v8

    iget-object v9, p0, Lcom/twitter/library/card/element/Text;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v9, v9, Lcom/twitter/library/card/property/Style;->fontBold:Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    iget-object v10, p0, Lcom/twitter/library/card/element/Text;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v10, v10, Lcom/twitter/library/card/property/Style;->fontItalic:Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    const/4 v11, 0x0

    const v12, 0x3f99999a    # 1.2f

    mul-float/2addr v8, v12

    invoke-virtual {v0, v11, v8}, Landroid/widget/TextView;->setLineSpacing(FF)V

    iget-object v8, p0, Lcom/twitter/library/card/element/Text;->mCookedText:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v5, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setSingleLine(Z)V

    iget v1, p0, Lcom/twitter/library/card/element/Text;->overflowMode:I

    if-ne v1, v2, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v13, :cond_0

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    :cond_0
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setHorizontallyScrolling(Z)V

    iget v1, p0, Lcom/twitter/library/card/element/Text;->maxLines:I

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/twitter/library/card/element/Text;->maxLines:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    :cond_1
    iget-object v1, p0, Lcom/twitter/library/card/element/Text;->mTypeface:Landroid/graphics/Typeface;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/twitter/library/card/element/Text;->mTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/twitter/library/card/element/Text;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    iget v1, v1, Lcom/twitter/library/card/property/Vector2;->x:I

    packed-switch v1, :pswitch_data_0

    move v1, v5

    :goto_1
    iget-object v2, p0, Lcom/twitter/library/card/element/Text;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    iget v2, v2, Lcom/twitter/library/card/property/Vector2;->y:I

    packed-switch v2, :pswitch_data_1

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v1, p0, Lcom/twitter/library/card/element/Text;->maxSizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v1, v1, Lcom/twitter/library/card/property/Vector2;->x:I

    if-ne v1, v3, :cond_3

    iget-object v1, p0, Lcom/twitter/library/card/element/Text;->maxSize:Lcom/twitter/library/card/property/Vector2F;

    iget v1, v1, Lcom/twitter/library/card/property/Vector2F;->x:F

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxWidth(I)V

    :cond_3
    iget-object v1, p0, Lcom/twitter/library/card/element/Text;->maxSizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v1, v1, Lcom/twitter/library/card/property/Vector2;->y:I

    if-ne v1, v3, :cond_4

    iget-object v1, p0, Lcom/twitter/library/card/element/Text;->maxSize:Lcom/twitter/library/card/property/Vector2F;

    iget v1, v1, Lcom/twitter/library/card/property/Vector2F;->y:F

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxHeight(I)V

    :cond_4
    invoke-super {p0}, Lcom/twitter/library/card/element/Element;->J()V

    return-void

    :cond_5
    if-eqz v6, :cond_2

    if-eqz v9, :cond_a

    move v1, v3

    :goto_3
    if-eqz v10, :cond_6

    or-int/lit8 v1, v1, 0x2

    :cond_6
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v7, v13, :cond_7

    invoke-static {v6, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/library/card/element/Text;->a(Landroid/graphics/Typeface;)V

    :goto_4
    iget-object v1, p0, Lcom/twitter/library/card/element/Text;->mTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/twitter/library/card/element/Text;->C()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/twitter/internal/android/widget/ax;->a(Landroid/content/Context;)Lcom/twitter/internal/android/widget/ax;

    move-result-object v6

    invoke-virtual {v6, v1}, Lcom/twitter/internal/android/widget/ax;->a(I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/library/card/element/Text;->a(Landroid/graphics/Typeface;)V

    goto :goto_4

    :pswitch_0
    move v1, v2

    goto :goto_1

    :pswitch_1
    move v1, v3

    goto :goto_1

    :pswitch_2
    move v1, v4

    goto :goto_1

    :pswitch_3
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v13, :cond_9

    iget-object v1, p0, Lcom/twitter/library/card/element/Text;->mCookedText:Landroid/text/SpannableStringBuilder;

    invoke-static {v1}, Lcom/twitter/library/util/Util;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    move v1, v4

    goto :goto_1

    :cond_8
    move v1, v2

    goto :goto_1

    :cond_9
    const v1, 0x800003

    goto :goto_1

    :pswitch_4
    or-int/lit8 v1, v1, 0x30

    goto :goto_2

    :pswitch_5
    or-int/lit8 v1, v1, 0x10

    goto :goto_2

    :pswitch_6
    or-int/lit8 v1, v1, 0x50

    goto :goto_2

    :cond_a
    move v1, v5

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_4
    .end packed-switch
.end method

.method public a(ILcom/twitter/library/card/property/Vector2F;)F
    .locals 11

    const v5, 0xf4240

    const/4 v2, 0x1

    const/4 v10, 0x0

    iget-object v0, p0, Lcom/twitter/library/card/element/Text;->mView:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v4

    const v0, 0x3f99999a    # 1.2f

    :try_start_0
    iget-object v1, p0, Lcom/twitter/library/card/element/Text;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v1, v1, Lcom/twitter/library/card/property/Style;->lineHeight:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float v7, v0, v1

    iget-object v0, p0, Lcom/twitter/library/card/element/Text;->mCookedText:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    if-nez p1, :cond_0

    new-instance v0, Landroid/text/StaticLayout;

    iget-object v1, p0, Lcom/twitter/library/card/element/Text;->mCookedText:Landroid/text/SpannableStringBuilder;

    const/4 v2, 0x0

    const v5, 0xf4240

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-direct/range {v0 .. v9}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/text/StaticLayout;->getLineWidth(I)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-float v0, v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/card/element/Text;->maxSizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v0, v0, Lcom/twitter/library/card/property/Vector2;->x:I

    if-ne v0, v2, :cond_1

    iget v0, p2, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget-object v1, p0, Lcom/twitter/library/card/element/Text;->maxSize:Lcom/twitter/library/card/property/Vector2F;

    iget v1, v1, Lcom/twitter/library/card/property/Vector2F;->x:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    :goto_1
    cmpg-float v1, v0, v10

    if-gtz v1, :cond_3

    :goto_2
    new-instance v0, Landroid/text/StaticLayout;

    iget-object v1, p0, Lcom/twitter/library/card/element/Text;->mCookedText:Landroid/text/SpannableStringBuilder;

    const/4 v2, 0x0

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-direct/range {v0 .. v9}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v0

    iget v2, p0, Lcom/twitter/library/card/element/Text;->maxLines:I

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/twitter/library/card/element/Text;->maxLines:I

    if-le v0, v2, :cond_4

    div-int v0, v1, v0

    iget v1, p0, Lcom/twitter/library/card/element/Text;->maxLines:I

    mul-int/2addr v0, v1

    :goto_3
    int-to-float v0, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/card/element/Text;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v0, v0, Lcom/twitter/library/card/property/Vector2;->x:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    move v0, v10

    goto :goto_1

    :cond_2
    iget v0, p2, Lcom/twitter/library/card/property/Vector2F;->x:F

    goto :goto_1

    :cond_3
    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    double-to-int v5, v0

    goto :goto_2

    :catch_0
    move-exception v0

    move v0, v10

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_3
.end method

.method protected a(Landroid/view/View;Landroid/view/MotionEvent;)Lcom/twitter/library/card/element/k;
    .locals 6

    const/4 v1, 0x0

    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/widget/TextView;->getTotalPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollX()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/widget/TextView;->getTotalPaddingTop()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollY()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {v0, v3}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/text/Layout;->getLineLeft(I)F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v0, v3}, Landroid/text/Layout;->getLineRight(I)F

    move-result v5

    float-to-int v5, v5

    if-lt v2, v4, :cond_1

    if-gt v2, v5, :cond_1

    int-to-float v2, v2

    invoke-virtual {v0, v3, v2}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    move-result v0

    iget-object v2, p0, Lcom/twitter/library/card/element/Text;->mCookedText:Landroid/text/SpannableStringBuilder;

    invoke-static {v2}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v2

    const-class v3, Lcom/twitter/library/card/element/l;

    invoke-interface {v2, v0, v0, v3}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/card/element/l;

    array-length v2, v0

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    aget-object v0, v0, v2

    instance-of v2, v0, Lcom/twitter/library/card/element/k;

    if-eqz v2, :cond_1

    check-cast v0, Lcom/twitter/library/card/element/k;

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/Text;->mFontName:Ljava/lang/String;

    return-object v0
.end method

.method public a(Landroid/graphics/Typeface;)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/card/element/Text;->mFontName:Ljava/lang/String;

    iput-object p1, p0, Lcom/twitter/library/card/element/Text;->mTypeface:Landroid/graphics/Typeface;

    return-void
.end method

.method protected a(Lcom/twitter/library/card/Card;)V
    .locals 12

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/library/card/element/Text;->mCookedText:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->clear()V

    invoke-virtual {p1}, Lcom/twitter/library/card/Card;->g()Lcom/twitter/library/card/property/LocalizedTokenizedText;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, v0, Lcom/twitter/library/card/property/LocalizedTokenizedText;->tokenizedTexts:Lcom/twitter/library/card/property/a;

    iget v1, p0, Lcom/twitter/library/card/element/Text;->tokenizedTextId:I

    invoke-virtual {v0, v1}, Lcom/twitter/library/card/property/a;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/TokenizedText;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/twitter/library/card/property/TokenizedText;->textTokenGroups:[Lcom/twitter/library/card/property/TextTokenGroup;

    if-eqz v1, :cond_0

    iget-object v4, v0, Lcom/twitter/library/card/property/TokenizedText;->textTokenGroups:[Lcom/twitter/library/card/property/TextTokenGroup;

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v0, v4, v3

    iget-boolean v1, v0, Lcom/twitter/library/card/property/TextTokenGroup;->visible:Z

    if-eqz v1, :cond_4

    iget-object v6, v0, Lcom/twitter/library/card/property/TextTokenGroup;->textTokens:[Lcom/twitter/library/card/property/TextToken;

    array-length v7, v6

    move v1, v2

    :goto_1
    if-ge v1, v7, :cond_4

    aget-object v8, v6, v1

    iget-object v0, v8, Lcom/twitter/library/card/property/TextToken;->text:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget v0, v8, Lcom/twitter/library/card/property/TextToken;->styleId:I

    iget-object v9, p0, Lcom/twitter/library/card/element/Text;->mCookedText:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v9}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v9

    iget-object v10, p0, Lcom/twitter/library/card/element/Text;->mCookedText:Landroid/text/SpannableStringBuilder;

    iget-object v11, v8, Lcom/twitter/library/card/property/TextToken;->text:Ljava/lang/String;

    invoke-virtual {v10, v11}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-object v10, p0, Lcom/twitter/library/card/element/Text;->mCookedText:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v10}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v10

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/twitter/library/card/Card;->styles:Lcom/twitter/library/card/property/a;

    iget v11, v8, Lcom/twitter/library/card/property/TextToken;->styleId:I

    invoke-virtual {v0, v11}, Lcom/twitter/library/card/property/a;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Style;

    iget-object v11, p0, Lcom/twitter/library/card/element/Text;->mCookedText:Landroid/text/SpannableStringBuilder;

    invoke-direct {p0, v0, v11, v9, v10}, Lcom/twitter/library/card/element/Text;->a(Lcom/twitter/library/card/property/Style;Landroid/text/SpannableStringBuilder;II)V

    :cond_2
    invoke-virtual {v8}, Lcom/twitter/library/card/property/TextToken;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/twitter/library/card/element/k;

    invoke-direct {v0, v8}, Lcom/twitter/library/card/element/k;-><init>(Lcom/twitter/library/card/property/TextToken;)V

    iget-object v8, p0, Lcom/twitter/library/card/element/Text;->mCookedText:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v8, v0, v9, v10, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/card/element/Text;->mHasActionSpans:Z

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/library/card/element/Text;->mRawText:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/library/card/element/Text;->mCookedText:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->clear()V

    return-void
.end method

.method public b(Landroid/content/Context;)V
    .locals 1

    new-instance v0, Lcom/twitter/library/card/element/TextElementView;

    invoke-direct {v0, p1, p0}, Lcom/twitter/library/card/element/TextElementView;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/Element;)V

    iput-object v0, p0, Lcom/twitter/library/card/element/Text;->mView:Landroid/view/View;

    return-void
.end method

.method public b(Landroid/graphics/Canvas;)V
    .locals 0

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/library/card/element/Text;->mFontName:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/card/element/Text;->mTypeface:Landroid/graphics/Typeface;

    return-void
.end method

.method protected b(I)Z
    .locals 2

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/twitter/library/card/element/Text;->mHasActionSpans:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/twitter/library/card/element/Text;->mTouchedActionSpan:Lcom/twitter/library/card/element/k;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/element/Text;->mTouchedActionSpan:Lcom/twitter/library/card/element/k;

    iget-object v0, v0, Lcom/twitter/library/card/element/k;->a:Lcom/twitter/library/card/property/TextToken;

    packed-switch p1, :pswitch_data_0

    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_0
    iget v0, v0, Lcom/twitter/library/card/property/TextToken;->pressDownActionId:I

    invoke-virtual {p0, p1, v0}, Lcom/twitter/library/card/element/Text;->a(II)V

    goto :goto_1

    :pswitch_1
    iget v0, v0, Lcom/twitter/library/card/property/TextToken;->pressUpActionId:I

    invoke-virtual {p0, p1, v0}, Lcom/twitter/library/card/element/Text;->a(II)V

    goto :goto_1

    :pswitch_2
    iget v0, v0, Lcom/twitter/library/card/property/TextToken;->tapActionId:I

    invoke-virtual {p0, p1, v0}, Lcom/twitter/library/card/element/Text;->a(II)V

    goto :goto_1

    :pswitch_3
    iget v0, v0, Lcom/twitter/library/card/property/TextToken;->longPressActionId:I

    invoke-virtual {p0, p1, v0}, Lcom/twitter/library/card/element/Text;->a(II)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public c(Landroid/graphics/Canvas;)V
    .locals 5

    sget-object v0, Lcom/twitter/library/card/element/Text;->a:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    sget-object v1, Lcom/twitter/library/card/element/Text;->a:Landroid/graphics/Paint;

    iget v2, p0, Lcom/twitter/library/card/element/Text;->color:I

    ushr-int/lit8 v2, v2, 0x2

    const/high16 v3, -0x1000000

    and-int/2addr v2, v3

    iget v3, p0, Lcom/twitter/library/card/element/Text;->color:I

    const v4, 0xffffff

    and-int/2addr v3, v4

    or-int/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/twitter/library/card/element/Text;->mLayoutRect:Landroid/graphics/RectF;

    sget-object v2, Lcom/twitter/library/card/element/Text;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    sget-object v1, Lcom/twitter/library/card/element/Text;->a:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method

.method protected d(Lcom/twitter/library/card/Card;)V
    .locals 2

    iget v0, p0, Lcom/twitter/library/card/element/Text;->tokenizedTextId:I

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/twitter/library/card/element/Text;->a(Lcom/twitter/library/card/Card;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/card/element/Text;->mCookedText:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->clear()V

    iget-object v0, p0, Lcom/twitter/library/card/element/Text;->mCookedText:Landroid/text/SpannableStringBuilder;

    iget-object v1, p0, Lcom/twitter/library/card/element/Text;->mRawText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/library/card/element/Text;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Element;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/twitter/library/card/element/Text;

    iget v2, p0, Lcom/twitter/library/card/element/Text;->color:I

    iget v3, p1, Lcom/twitter/library/card/element/Text;->color:I

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-boolean v2, p0, Lcom/twitter/library/card/element/Text;->fontBold:Z

    iget-boolean v3, p1, Lcom/twitter/library/card/element/Text;->fontBold:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-boolean v2, p0, Lcom/twitter/library/card/element/Text;->fontItalic:Z

    iget-boolean v3, p1, Lcom/twitter/library/card/element/Text;->fontItalic:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget v2, p1, Lcom/twitter/library/card/element/Text;->fontSize:F

    iget v3, p0, Lcom/twitter/library/card/element/Text;->fontSize:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget-boolean v2, p0, Lcom/twitter/library/card/element/Text;->fontUnderline:Z

    iget-boolean v3, p1, Lcom/twitter/library/card/element/Text;->fontUnderline:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget v2, p1, Lcom/twitter/library/card/element/Text;->lineHeight:F

    iget v3, p0, Lcom/twitter/library/card/element/Text;->lineHeight:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    iget v2, p0, Lcom/twitter/library/card/element/Text;->maxLines:I

    iget v3, p1, Lcom/twitter/library/card/element/Text;->maxLines:I

    if-eq v2, v3, :cond_a

    move v0, v1

    goto :goto_0

    :cond_a
    iget v2, p0, Lcom/twitter/library/card/element/Text;->overflowMode:I

    iget v3, p1, Lcom/twitter/library/card/element/Text;->overflowMode:I

    if-eq v2, v3, :cond_b

    move v0, v1

    goto :goto_0

    :cond_b
    iget v2, p0, Lcom/twitter/library/card/element/Text;->tokenizedTextId:I

    iget v3, p1, Lcom/twitter/library/card/element/Text;->tokenizedTextId:I

    if-eq v2, v3, :cond_c

    move v0, v1

    goto :goto_0

    :cond_c
    iget-object v2, p0, Lcom/twitter/library/card/element/Text;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/twitter/library/card/element/Text;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    iget-object v3, p1, Lcom/twitter/library/card/element/Text;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/Vector2;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    goto :goto_0

    :cond_e
    iget-object v2, p1, Lcom/twitter/library/card/element/Text;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    if-nez v2, :cond_d

    :cond_f
    iget-object v2, p0, Lcom/twitter/library/card/element/Text;->mFontName:Ljava/lang/String;

    if-eqz v2, :cond_10

    iget-object v2, p0, Lcom/twitter/library/card/element/Text;->mFontName:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/element/Text;->mFontName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    goto/16 :goto_0

    :cond_10
    iget-object v2, p1, Lcom/twitter/library/card/element/Text;->mFontName:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 5

    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/twitter/library/card/element/Element;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/twitter/library/card/element/Text;->tokenizedTextId:I

    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget v0, p0, Lcom/twitter/library/card/element/Text;->fontSize:F

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/library/card/element/Text;->fontSize:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    :goto_0
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/twitter/library/card/element/Text;->color:I

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/twitter/library/card/element/Text;->maxLines:I

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/twitter/library/card/element/Text;->overflowMode:I

    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget v0, p0, Lcom/twitter/library/card/element/Text;->lineHeight:F

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/twitter/library/card/element/Text;->lineHeight:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/library/card/element/Text;->fontBold:Z

    if-eqz v0, :cond_3

    move v0, v2

    :goto_2
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/library/card/element/Text;->fontUnderline:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_3
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/twitter/library/card/element/Text;->fontItalic:Z

    if-eqz v3, :cond_5

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/element/Text;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/twitter/library/card/element/Text;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/Vector2;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/library/card/element/Text;->mFontName:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/library/card/element/Text;->mFontName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_3

    :cond_5
    move v2, v1

    goto :goto_4

    :cond_6
    move v0, v1

    goto :goto_5
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/library/card/element/Text;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Element;->onDown(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 1

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/twitter/library/card/element/Text;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Element;->onLongPress(Landroid/view/MotionEvent;)V

    :cond_0
    return-void
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0

    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/twitter/library/card/element/Text;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Element;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/library/card/element/Text;->mHasActionSpans:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/card/element/Text;->a(Landroid/view/View;Landroid/view/MotionEvent;)Lcom/twitter/library/card/element/k;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/card/element/Text;->mTouchedActionSpan:Lcom/twitter/library/card/element/k;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/library/card/element/Text;->b(I)Z

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/twitter/library/card/element/Element;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method protected p()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/library/card/element/Element;->p()V

    iget-object v0, p0, Lcom/twitter/library/card/element/Text;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v1, p0, Lcom/twitter/library/card/element/Text;->mFontName:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/library/card/property/Style;->fontName:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/library/card/element/Text;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget v1, p0, Lcom/twitter/library/card/element/Text;->fontSize:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/card/property/Style;->fontSize:Ljava/lang/Float;

    iget-object v0, p0, Lcom/twitter/library/card/element/Text;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget v1, p0, Lcom/twitter/library/card/element/Text;->color:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/card/property/Style;->color:Ljava/lang/Integer;

    iget-object v0, p0, Lcom/twitter/library/card/element/Text;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget v1, p0, Lcom/twitter/library/card/element/Text;->lineHeight:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/card/property/Style;->lineHeight:Ljava/lang/Float;

    iget-object v0, p0, Lcom/twitter/library/card/element/Text;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-boolean v1, p0, Lcom/twitter/library/card/element/Text;->fontBold:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/card/property/Style;->fontBold:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/twitter/library/card/element/Text;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-boolean v1, p0, Lcom/twitter/library/card/element/Text;->fontUnderline:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/card/property/Style;->fontUnderline:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/twitter/library/card/element/Text;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-boolean v1, p0, Lcom/twitter/library/card/element/Text;->fontItalic:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/card/property/Style;->fontItalic:Ljava/lang/Boolean;

    return-void
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Element;->readExternal(Ljava/io/ObjectInput;)V

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/element/Text;->tokenizedTextId:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/element/Text;->fontSize:F

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/element/Text;->color:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/element/Text;->maxLines:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/element/Text;->overflowMode:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/element/Text;->lineHeight:F

    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/card/element/Text;->fontBold:Z

    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/card/element/Text;->fontUnderline:Z

    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/card/element/Text;->fontItalic:Z

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Vector2;

    iput-object v0, p0, Lcom/twitter/library/card/element/Text;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/element/Text;->mFontName:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/element/Text;->mRawText:Ljava/lang/String;

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Element;->writeExternal(Ljava/io/ObjectOutput;)V

    iget v0, p0, Lcom/twitter/library/card/element/Text;->tokenizedTextId:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/card/element/Text;->fontSize:F

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeFloat(F)V

    iget v0, p0, Lcom/twitter/library/card/element/Text;->color:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/card/element/Text;->maxLines:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/card/element/Text;->overflowMode:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/card/element/Text;->lineHeight:F

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeFloat(F)V

    iget-boolean v0, p0, Lcom/twitter/library/card/element/Text;->fontBold:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    iget-boolean v0, p0, Lcom/twitter/library/card/element/Text;->fontUnderline:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    iget-boolean v0, p0, Lcom/twitter/library/card/element/Text;->fontItalic:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Text;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Text;->mFontName:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Text;->mRawText:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    return-void
.end method
