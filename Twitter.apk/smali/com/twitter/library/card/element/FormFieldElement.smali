.class public abstract Lcom/twitter/library/card/element/FormFieldElement;
.super Lcom/twitter/library/card/element/Element;
.source "Twttr"


# static fields
.field private static final serialVersionUID:J = 0x536107eed955b097L


# instance fields
.field public field:Lcom/twitter/library/card/property/FormField;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/library/card/element/Element;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILcom/twitter/library/card/property/Vector2F;)F
    .locals 2

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/element/FormFieldElement;->mView:Landroid/view/View;

    check-cast v0, Lcom/twitter/library/card/element/FormFieldElementView;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/FormFieldElementView;->getViewWidth()I

    move-result v0

    int-to-float v0, v0

    iget v1, p2, Lcom/twitter/library/card/property/Vector2F;->x:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/card/element/FormFieldElement;->mView:Landroid/view/View;

    check-cast v0, Lcom/twitter/library/card/element/FormFieldElementView;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/FormFieldElementView;->getViewHeight()I

    move-result v0

    int-to-float v0, v0

    goto :goto_0
.end method

.method public abstract a()Ljava/lang/String;
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/FormFieldElement;->field:Lcom/twitter/library/card/property/FormField;

    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/card/element/FormFieldElement;->field:Lcom/twitter/library/card/property/FormField;

    iget-object v0, v0, Lcom/twitter/library/card/property/FormField;->name:Ljava/lang/String;

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/library/card/element/FormFieldElement;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Element;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/twitter/library/card/element/FormFieldElement;

    iget-object v2, p0, Lcom/twitter/library/card/element/FormFieldElement;->field:Lcom/twitter/library/card/property/FormField;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/card/element/FormFieldElement;->field:Lcom/twitter/library/card/property/FormField;

    iget-object v3, p1, Lcom/twitter/library/card/element/FormFieldElement;->field:Lcom/twitter/library/card/property/FormField;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/FormField;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p1, Lcom/twitter/library/card/element/FormFieldElement;->field:Lcom/twitter/library/card/property/FormField;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 2

    invoke-super {p0}, Lcom/twitter/library/card/element/Element;->hashCode()I

    move-result v0

    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/element/FormFieldElement;->field:Lcom/twitter/library/card/property/FormField;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/element/FormFieldElement;->field:Lcom/twitter/library/card/property/FormField;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/FormField;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Element;->readExternal(Ljava/io/ObjectInput;)V

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/FormField;

    iput-object v0, p0, Lcom/twitter/library/card/element/FormFieldElement;->field:Lcom/twitter/library/card/property/FormField;

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Element;->writeExternal(Ljava/io/ObjectOutput;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/FormFieldElement;->field:Lcom/twitter/library/card/property/FormField;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    return-void
.end method
