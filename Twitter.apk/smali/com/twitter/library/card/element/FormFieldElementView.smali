.class public abstract Lcom/twitter/library/card/element/FormFieldElementView;
.super Landroid/widget/FrameLayout;
.source "Twttr"


# instance fields
.field protected a:I

.field protected b:I

.field private c:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/card/element/FormFieldElement;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/card/element/FormFieldElementView;->a(Landroid/content/Context;Lcom/twitter/library/card/element/FormFieldElement;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/card/element/FormFieldElementView;->c:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/library/card/element/FormFieldElementView;->setWillNotDraw(Z)V

    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/String;ZZ)Landroid/graphics/Typeface;
    .locals 3

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    if-eqz p3, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-ge v1, v2, :cond_2

    invoke-static {p1, v0}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/library/card/element/FormFieldElementView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/internal/android/widget/ax;->a(Landroid/content/Context;)Lcom/twitter/internal/android/widget/ax;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/internal/android/widget/ax;->a(I)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_0
.end method

.method protected abstract a(Landroid/content/Context;Lcom/twitter/library/card/element/FormFieldElement;)Landroid/view/View;
.end method

.method protected a(ILcom/twitter/library/card/Card;)Ljava/lang/String;
    .locals 1

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    const-string/jumbo v0, ""

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p2}, Lcom/twitter/library/card/Card;->g()Lcom/twitter/library/card/property/LocalizedTokenizedText;

    move-result-object v0

    if-nez v0, :cond_1

    const-string/jumbo v0, ""

    goto :goto_0

    :cond_1
    invoke-virtual {v0, p2, p1}, Lcom/twitter/library/card/property/LocalizedTokenizedText;->a(Lcom/twitter/library/card/Card;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/card/Card;)V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/library/card/element/FormFieldElementView;->b()V

    invoke-virtual {p0}, Lcom/twitter/library/card/element/FormFieldElementView;->c()V

    return-void
.end method

.method protected b()V
    .locals 2

    const/4 v1, -0x2

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/twitter/library/card/element/FormFieldElementView;->c:Landroid/view/View;

    invoke-virtual {p0, v1, v0}, Lcom/twitter/library/card/element/FormFieldElementView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method protected c()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0}, Lcom/twitter/library/card/element/FormFieldElementView;->measure(II)V

    invoke-virtual {p0}, Lcom/twitter/library/card/element/FormFieldElementView;->getMeasuredWidth()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/element/FormFieldElementView;->a:I

    invoke-virtual {p0}, Lcom/twitter/library/card/element/FormFieldElementView;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/element/FormFieldElementView;->b:I

    return-void
.end method

.method public getViewHeight()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/card/element/FormFieldElementView;->b:I

    return v0
.end method

.method public getViewWidth()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/card/element/FormFieldElementView;->a:I

    return v0
.end method
