.class public Lcom/twitter/library/card/element/FormText;
.super Lcom/twitter/library/card/element/FormFieldElement;
.source "Twttr"


# static fields
.field private static final serialVersionUID:J = 0x50135cef5300a929L


# instance fields
.field public alignmentMode:Lcom/twitter/library/card/property/Vector2;

.field public charset:Ljava/lang/String;

.field public color:I

.field public fontBold:Z

.field public fontItalic:Z

.field public fontName:Ljava/lang/String;

.field public fontSize:F

.field public fontUnderline:Z

.field public inputMode:Ljava/lang/String;

.field public maxLength:I

.field public placeHolderTextId:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/twitter/library/card/element/FormFieldElement;-><init>()V

    new-instance v0, Lcom/twitter/library/card/property/Vector2;

    invoke-direct {v0, v1, v1}, Lcom/twitter/library/card/property/Vector2;-><init>(II)V

    iput-object v0, p0, Lcom/twitter/library/card/element/FormText;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/library/card/element/FormText;->placeHolderTextId:I

    const/16 v0, 0xa

    iput v0, p0, Lcom/twitter/library/card/element/FormText;->maxLength:I

    return-void
.end method


# virtual methods
.method public J()V
    .locals 6

    const/4 v3, 0x5

    const/4 v1, 0x3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/twitter/library/card/element/FormText;->mView:Landroid/view/View;

    check-cast v0, Lcom/twitter/library/card/element/FormTextView;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/FormTextView;->getEditText()Landroid/widget/EditText;

    move-result-object v4

    const/4 v0, 0x0

    iget-object v5, p0, Lcom/twitter/library/card/element/FormText;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    iget v5, v5, Lcom/twitter/library/card/property/Vector2;->x:I

    packed-switch v5, :pswitch_data_0

    :goto_0
    iget-object v1, p0, Lcom/twitter/library/card/element/FormText;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    iget v1, v1, Lcom/twitter/library/card/property/Vector2;->y:I

    packed-switch v1, :pswitch_data_1

    :goto_1
    invoke-virtual {v4, v0}, Landroid/widget/EditText;->setGravity(I)V

    iget-object v0, p0, Lcom/twitter/library/card/element/FormText;->maxSizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v0, v0, Lcom/twitter/library/card/property/Vector2;->x:I

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/element/FormText;->maxSize:Lcom/twitter/library/card/property/Vector2F;

    iget v0, v0, Lcom/twitter/library/card/property/Vector2F;->x:F

    float-to-int v0, v0

    invoke-virtual {v4, v0}, Landroid/widget/EditText;->setMaxWidth(I)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/card/element/FormText;->maxSizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v0, v0, Lcom/twitter/library/card/property/Vector2;->y:I

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/twitter/library/card/element/FormText;->maxSize:Lcom/twitter/library/card/property/Vector2F;

    iget v0, v0, Lcom/twitter/library/card/property/Vector2F;->y:F

    float-to-int v0, v0

    invoke-virtual {v4, v0}, Landroid/widget/EditText;->setMaxHeight(I)V

    :cond_1
    invoke-super {p0}, Lcom/twitter/library/card/element/FormFieldElement;->J()V

    return-void

    :pswitch_0
    move v0, v1

    goto :goto_0

    :pswitch_1
    move v0, v2

    goto :goto_0

    :pswitch_2
    move v0, v3

    goto :goto_0

    :pswitch_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xe

    if-ge v0, v5, :cond_3

    const-string/jumbo v0, "HACKHACK"

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v3

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    const v0, 0x800003

    goto :goto_0

    :pswitch_4
    or-int/lit8 v0, v0, 0x30

    goto :goto_1

    :pswitch_5
    or-int/lit8 v0, v0, 0x10

    goto :goto_1

    :pswitch_6
    or-int/lit8 v0, v0, 0x50

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_4
    .end packed-switch
.end method

.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/FormText;->mView:Landroid/view/View;

    check-cast v0, Lcom/twitter/library/card/element/FormTextView;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/FormTextView;->getText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/content/Context;)V
    .locals 1

    new-instance v0, Lcom/twitter/library/card/element/FormTextView;

    invoke-direct {v0, p1, p0}, Lcom/twitter/library/card/element/FormTextView;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/FormText;)V

    iput-object v0, p0, Lcom/twitter/library/card/element/FormText;->mView:Landroid/view/View;

    return-void
.end method

.method protected d(Lcom/twitter/library/card/Card;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/FormText;->mView:Landroid/view/View;

    check-cast v0, Lcom/twitter/library/card/element/FormTextView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/card/element/FormTextView;->a(Lcom/twitter/library/card/Card;)V

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/library/card/element/FormText;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Lcom/twitter/library/card/element/FormFieldElement;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/twitter/library/card/element/FormText;

    iget v2, p0, Lcom/twitter/library/card/element/FormText;->color:I

    iget v3, p1, Lcom/twitter/library/card/element/FormText;->color:I

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-boolean v2, p0, Lcom/twitter/library/card/element/FormText;->fontBold:Z

    iget-boolean v3, p1, Lcom/twitter/library/card/element/FormText;->fontBold:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-boolean v2, p0, Lcom/twitter/library/card/element/FormText;->fontItalic:Z

    iget-boolean v3, p1, Lcom/twitter/library/card/element/FormText;->fontItalic:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget v2, p1, Lcom/twitter/library/card/element/FormText;->fontSize:F

    iget v3, p0, Lcom/twitter/library/card/element/FormText;->fontSize:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget-boolean v2, p0, Lcom/twitter/library/card/element/FormText;->fontUnderline:Z

    iget-boolean v3, p1, Lcom/twitter/library/card/element/FormText;->fontUnderline:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget v2, p0, Lcom/twitter/library/card/element/FormText;->maxLength:I

    iget v3, p1, Lcom/twitter/library/card/element/FormText;->maxLength:I

    if-eq v2, v3, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    iget v2, p0, Lcom/twitter/library/card/element/FormText;->placeHolderTextId:I

    iget v3, p1, Lcom/twitter/library/card/element/FormText;->placeHolderTextId:I

    if-eq v2, v3, :cond_a

    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p0, Lcom/twitter/library/card/element/FormText;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/twitter/library/card/element/FormText;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    iget-object v3, p1, Lcom/twitter/library/card/element/FormText;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/Vector2;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    goto :goto_0

    :cond_c
    iget-object v2, p1, Lcom/twitter/library/card/element/FormText;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    if-nez v2, :cond_b

    :cond_d
    iget-object v2, p0, Lcom/twitter/library/card/element/FormText;->charset:Ljava/lang/String;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/twitter/library/card/element/FormText;->charset:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/element/FormText;->charset:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    :cond_e
    move v0, v1

    goto :goto_0

    :cond_f
    iget-object v2, p1, Lcom/twitter/library/card/element/FormText;->charset:Ljava/lang/String;

    if-nez v2, :cond_e

    :cond_10
    iget-object v2, p0, Lcom/twitter/library/card/element/FormText;->fontName:Ljava/lang/String;

    if-eqz v2, :cond_12

    iget-object v2, p0, Lcom/twitter/library/card/element/FormText;->fontName:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/element/FormText;->fontName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    :cond_11
    move v0, v1

    goto/16 :goto_0

    :cond_12
    iget-object v2, p1, Lcom/twitter/library/card/element/FormText;->fontName:Ljava/lang/String;

    if-nez v2, :cond_11

    :cond_13
    iget-object v2, p0, Lcom/twitter/library/card/element/FormText;->inputMode:Ljava/lang/String;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lcom/twitter/library/card/element/FormText;->inputMode:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/element/FormText;->inputMode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    goto/16 :goto_0

    :cond_14
    iget-object v2, p1, Lcom/twitter/library/card/element/FormText;->inputMode:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/twitter/library/card/element/FormFieldElement;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/twitter/library/card/element/FormText;->maxLength:I

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/twitter/library/card/element/FormText;->placeHolderTextId:I

    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/element/FormText;->charset:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/element/FormText;->charset:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/element/FormText;->inputMode:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/card/element/FormText;->inputMode:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/element/FormText;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/card/element/FormText;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/Vector2;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/element/FormText;->fontName:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/card/element/FormText;->fontName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget v0, p0, Lcom/twitter/library/card/element/FormText;->fontSize:F

    const/4 v4, 0x0

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/twitter/library/card/element/FormText;->fontSize:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    :goto_4
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/library/card/element/FormText;->fontBold:Z

    if-eqz v0, :cond_5

    move v0, v2

    :goto_5
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/library/card/element/FormText;->fontUnderline:Z

    if-eqz v0, :cond_6

    move v0, v2

    :goto_6
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/twitter/library/card/element/FormText;->fontItalic:Z

    if-eqz v3, :cond_7

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/library/card/element/FormText;->color:I

    add-int/2addr v0, v1

    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_3

    :cond_4
    move v0, v1

    goto :goto_4

    :cond_5
    move v0, v1

    goto :goto_5

    :cond_6
    move v0, v1

    goto :goto_6

    :cond_7
    move v2, v1

    goto :goto_7
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/FormFieldElement;->readExternal(Ljava/io/ObjectInput;)V

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/element/FormText;->maxLength:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/element/FormText;->placeHolderTextId:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/element/FormText;->charset:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/element/FormText;->inputMode:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Vector2;

    iput-object v0, p0, Lcom/twitter/library/card/element/FormText;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/element/FormText;->fontName:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/element/FormText;->fontSize:F

    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/card/element/FormText;->fontBold:Z

    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/card/element/FormText;->fontUnderline:Z

    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/card/element/FormText;->fontItalic:Z

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/element/FormText;->color:I

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/FormFieldElement;->writeExternal(Ljava/io/ObjectOutput;)V

    iget v0, p0, Lcom/twitter/library/card/element/FormText;->maxLength:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/card/element/FormText;->placeHolderTextId:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/card/element/FormText;->charset:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/FormText;->inputMode:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/FormText;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/FormText;->fontName:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget v0, p0, Lcom/twitter/library/card/element/FormText;->fontSize:F

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeFloat(F)V

    iget-boolean v0, p0, Lcom/twitter/library/card/element/FormText;->fontBold:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    iget-boolean v0, p0, Lcom/twitter/library/card/element/FormText;->fontUnderline:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    iget-boolean v0, p0, Lcom/twitter/library/card/element/FormText;->fontItalic:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    iget v0, p0, Lcom/twitter/library/card/element/FormText;->color:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    return-void
.end method
