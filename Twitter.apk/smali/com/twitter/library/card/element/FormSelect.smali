.class public Lcom/twitter/library/card/element/FormSelect;
.super Lcom/twitter/library/card/element/FormFieldElement;
.source "Twttr"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field private static final serialVersionUID:J = -0x299c5621d238e5adL


# instance fields
.field public color:I

.field public fontBold:Z

.field public fontItalic:Z

.field public fontName:Ljava/lang/String;

.field public fontSize:F

.field public fontUnderline:Z

.field public options:[Lcom/twitter/library/card/property/FormSelectOption;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/library/card/element/FormFieldElement;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/card/element/FormSelect;->mView:Landroid/view/View;

    check-cast v0, Lcom/twitter/library/card/element/FormSelectView;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/FormSelectView;->getSelectedIndex()I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/library/card/element/FormSelect;->options:[Lcom/twitter/library/card/property/FormSelectOption;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/card/element/FormSelect;->options:[Lcom/twitter/library/card/property/FormSelectOption;

    aget-object v0, v1, v0

    iget-object v0, v0, Lcom/twitter/library/card/property/FormSelectOption;->value:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public b(Landroid/content/Context;)V
    .locals 1

    new-instance v0, Lcom/twitter/library/card/element/FormSelectView;

    invoke-direct {v0, p1, p0}, Lcom/twitter/library/card/element/FormSelectView;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/FormSelect;)V

    iput-object v0, p0, Lcom/twitter/library/card/element/FormSelect;->mView:Landroid/view/View;

    return-void
.end method

.method protected d(Lcom/twitter/library/card/Card;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/FormFieldElement;->d(Lcom/twitter/library/card/Card;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/FormSelect;->mView:Landroid/view/View;

    check-cast v0, Lcom/twitter/library/card/element/FormSelectView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/card/element/FormSelectView;->a(Lcom/twitter/library/card/Card;)V

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/library/card/element/FormSelect;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Lcom/twitter/library/card/element/FormFieldElement;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/twitter/library/card/element/FormSelect;

    iget v2, p0, Lcom/twitter/library/card/element/FormSelect;->color:I

    iget v3, p1, Lcom/twitter/library/card/element/FormSelect;->color:I

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-boolean v2, p0, Lcom/twitter/library/card/element/FormSelect;->fontBold:Z

    iget-boolean v3, p1, Lcom/twitter/library/card/element/FormSelect;->fontBold:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-boolean v2, p0, Lcom/twitter/library/card/element/FormSelect;->fontItalic:Z

    iget-boolean v3, p1, Lcom/twitter/library/card/element/FormSelect;->fontItalic:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget v2, p1, Lcom/twitter/library/card/element/FormSelect;->fontSize:F

    iget v3, p0, Lcom/twitter/library/card/element/FormSelect;->fontSize:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget-boolean v2, p0, Lcom/twitter/library/card/element/FormSelect;->fontUnderline:Z

    iget-boolean v3, p1, Lcom/twitter/library/card/element/FormSelect;->fontUnderline:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Lcom/twitter/library/card/element/FormSelect;->fontName:Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/twitter/library/card/element/FormSelect;->fontName:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/element/FormSelect;->fontName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    :cond_9
    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p1, Lcom/twitter/library/card/element/FormSelect;->fontName:Ljava/lang/String;

    if-nez v2, :cond_9

    :cond_b
    iget-object v2, p0, Lcom/twitter/library/card/element/FormSelect;->options:[Lcom/twitter/library/card/property/FormSelectOption;

    iget-object v3, p1, Lcom/twitter/library/card/element/FormSelect;->options:[Lcom/twitter/library/card/property/FormSelectOption;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/twitter/library/card/element/FormFieldElement;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/twitter/library/card/element/FormSelect;->color:I

    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/element/FormSelect;->fontName:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/card/element/FormSelect;->fontName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget v0, p0, Lcom/twitter/library/card/element/FormSelect;->fontSize:F

    const/4 v4, 0x0

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/twitter/library/card/element/FormSelect;->fontSize:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/library/card/element/FormSelect;->fontBold:Z

    if-eqz v0, :cond_3

    move v0, v2

    :goto_2
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/library/card/element/FormSelect;->fontItalic:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_3
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/twitter/library/card/element/FormSelect;->fontUnderline:Z

    if-eqz v3, :cond_5

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/library/card/element/FormSelect;->options:[Lcom/twitter/library/card/property/FormSelectOption;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/library/card/element/FormSelect;->options:[Lcom/twitter/library/card/property/FormSelectOption;

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_3

    :cond_5
    move v2, v1

    goto :goto_4
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/FormFieldElement;->readExternal(Ljava/io/ObjectInput;)V

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/element/FormSelect;->color:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/element/FormSelect;->fontName:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/element/FormSelect;->fontSize:F

    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/card/element/FormSelect;->fontBold:Z

    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/card/element/FormSelect;->fontItalic:Z

    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/card/element/FormSelect;->fontUnderline:Z

    const-class v0, [Lcom/twitter/library/card/property/FormSelectOption;

    invoke-static {v0, p1}, Lcom/twitter/library/util/Util;->a(Ljava/lang/Class;Ljava/io/ObjectInput;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/card/property/FormSelectOption;

    iput-object v0, p0, Lcom/twitter/library/card/element/FormSelect;->options:[Lcom/twitter/library/card/property/FormSelectOption;

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/FormFieldElement;->writeExternal(Ljava/io/ObjectOutput;)V

    iget v0, p0, Lcom/twitter/library/card/element/FormSelect;->color:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/card/element/FormSelect;->fontName:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget v0, p0, Lcom/twitter/library/card/element/FormSelect;->fontSize:F

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeFloat(F)V

    iget-boolean v0, p0, Lcom/twitter/library/card/element/FormSelect;->fontBold:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    iget-boolean v0, p0, Lcom/twitter/library/card/element/FormSelect;->fontItalic:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    iget-boolean v0, p0, Lcom/twitter/library/card/element/FormSelect;->fontUnderline:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    iget-object v0, p0, Lcom/twitter/library/card/element/FormSelect;->options:[Lcom/twitter/library/card/property/FormSelectOption;

    invoke-static {v0, p1}, Lcom/twitter/library/util/Util;->a([Ljava/lang/Object;Ljava/io/ObjectOutput;)V

    return-void
.end method
