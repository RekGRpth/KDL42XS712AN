.class public Lcom/twitter/library/card/element/PlayerDelegateView;
.super Lcom/twitter/library/card/element/ElementViewGroup;
.source "Twttr"


# instance fields
.field private a:Lcom/twitter/library/card/element/j;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/card/element/Player;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/card/element/ElementViewGroup;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/Element;)V

    new-instance v0, Lcom/twitter/library/card/element/j;

    invoke-direct {v0}, Lcom/twitter/library/card/element/j;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/card/element/PlayerDelegateView;->a:Lcom/twitter/library/card/element/j;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/library/card/element/PlayerDelegateView;->setWillNotDraw(Z)V

    return-void
.end method


# virtual methods
.method protected B_()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/PlayerDelegateView;->a:Lcom/twitter/library/card/element/j;

    invoke-virtual {v0, p0}, Lcom/twitter/library/card/element/j;->a(Landroid/view/ViewGroup;)Z

    move-result v0

    return v0
.end method

.method public a(Landroid/content/Context;)Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/PlayerDelegateView;->a:Lcom/twitter/library/card/element/j;

    invoke-virtual {v0, p0, p1}, Lcom/twitter/library/card/element/j;->a(Landroid/view/ViewGroup;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/library/card/element/PlayerDelegateView;->a:Lcom/twitter/library/card/element/j;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/card/element/j;->a(ZIIII)V

    return-void
.end method
