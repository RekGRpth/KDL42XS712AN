.class public Lcom/twitter/library/card/element/FormCheckboxView;
.super Lcom/twitter/library/card/element/FormFieldElementView;
.source "Twttr"


# instance fields
.field private c:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/card/element/FormFieldElement;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/card/element/FormFieldElementView;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/FormFieldElement;)V

    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;Lcom/twitter/library/card/element/FormFieldElement;)Landroid/view/View;
    .locals 2

    new-instance v0, Landroid/widget/CheckBox;

    invoke-direct {v0, p1}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/library/card/element/FormCheckboxView;->c:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/twitter/library/card/element/FormCheckboxView;->c:Landroid/widget/CheckBox;

    check-cast p2, Lcom/twitter/library/card/element/FormCheckbox;

    iget-boolean v1, p2, Lcom/twitter/library/card/element/FormCheckbox;->checked:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v0, p0, Lcom/twitter/library/card/element/FormCheckboxView;->c:Landroid/widget/CheckBox;

    return-object v0
.end method

.method public a()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/FormCheckboxView;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    return v0
.end method
