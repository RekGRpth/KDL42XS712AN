.class public Lcom/twitter/library/card/element/FollowButtonElement;
.super Lcom/twitter/library/card/element/Element;
.source "Twttr"


# static fields
.field private static final serialVersionUID:J = -0x2daf890d3dc7eaf7L


# instance fields
.field public kind:Lcom/twitter/library/card/element/FollowButtonElement$Kind;

.field protected mDelegate:Lcom/twitter/library/card/element/d;

.field protected mUser:Lcom/twitter/library/api/TwitterUser;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/library/card/element/Element;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILcom/twitter/library/card/property/Vector2F;)F
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/FollowButtonElement;->mDelegate:Lcom/twitter/library/card/element/d;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/card/element/d;->a(ILcom/twitter/library/card/property/Vector2F;)F

    move-result v0

    return v0
.end method

.method public a()Lcom/twitter/library/card/Card;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/FollowButtonElement;->mCard:Lcom/twitter/library/card/Card;

    return-object v0
.end method

.method public a(JLcom/twitter/library/util/aa;Lcom/twitter/library/util/as;)V
    .locals 1

    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/library/card/element/Element;->a(JLcom/twitter/library/util/aa;Lcom/twitter/library/util/as;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/FollowButtonElement;->mDelegate:Lcom/twitter/library/card/element/d;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/twitter/library/card/element/d;->a(JLcom/twitter/library/util/aa;Lcom/twitter/library/util/as;)V

    return-void
.end method

.method public a(Lcom/twitter/library/api/TwitterUser;)V
    .locals 4

    iput-object p1, p0, Lcom/twitter/library/card/element/FollowButtonElement;->mUser:Lcom/twitter/library/api/TwitterUser;

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/library/card/element/FollowButtonElement;->C()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iget-wide v2, p1, Lcom/twitter/library/api/TwitterUser;->userId:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/library/card/element/FollowButtonElement;->e(Z)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/card/element/FollowButtonElement;->mDelegate:Lcom/twitter/library/card/element/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/card/element/FollowButtonElement;->mDelegate:Lcom/twitter/library/card/element/d;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/d;->d()V

    :cond_1
    return-void
.end method

.method public b(Landroid/content/Context;)V
    .locals 1

    sget-object v0, Lcom/twitter/library/card/element/FollowButtonElement;->b:Lcom/twitter/library/card/element/a;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/library/card/element/FollowButtonElement;->b:Lcom/twitter/library/card/element/a;

    invoke-interface {v0, p1, p0}, Lcom/twitter/library/card/element/a;->a(Landroid/content/Context;Lcom/twitter/library/card/element/FollowButtonElement;)Lcom/twitter/library/card/element/d;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/card/element/FollowButtonElement;->mDelegate:Lcom/twitter/library/card/element/d;

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/card/element/FollowButtonElement;->mDelegate:Lcom/twitter/library/card/element/d;

    if-nez v0, :cond_1

    new-instance v0, Lcom/twitter/library/card/element/d;

    invoke-direct {v0, p1, p0}, Lcom/twitter/library/card/element/d;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/FollowButtonElement;)V

    iput-object v0, p0, Lcom/twitter/library/card/element/FollowButtonElement;->mDelegate:Lcom/twitter/library/card/element/d;

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/card/element/FollowButtonElement;->mDelegate:Lcom/twitter/library/card/element/d;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/d;->a()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/card/element/FollowButtonElement;->mView:Landroid/view/View;

    return-void
.end method

.method public b(Landroid/graphics/Canvas;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Element;->b(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/FollowButtonElement;->mDelegate:Lcom/twitter/library/card/element/d;

    sget-object v1, Lcom/twitter/library/card/element/FollowButtonElement;->a:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/twitter/library/card/element/FollowButtonElement;->mLayoutRect:Landroid/graphics/RectF;

    invoke-virtual {v0, p1, v1, v2}, Lcom/twitter/library/card/element/d;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/RectF;)V

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/library/card/element/FollowButtonElement;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Element;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/twitter/library/card/element/FollowButtonElement;

    iget-object v2, p0, Lcom/twitter/library/card/element/FollowButtonElement;->kind:Lcom/twitter/library/card/element/FollowButtonElement$Kind;

    iget-object v3, p1, Lcom/twitter/library/card/element/FollowButtonElement;->kind:Lcom/twitter/library/card/element/FollowButtonElement$Kind;

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    invoke-super {p0}, Lcom/twitter/library/card/element/Element;->hashCode()I

    move-result v0

    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/element/FollowButtonElement;->kind:Lcom/twitter/library/card/element/FollowButtonElement$Kind;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/element/FollowButtonElement;->kind:Lcom/twitter/library/card/element/FollowButtonElement$Kind;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/FollowButtonElement$Kind;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Element;->readExternal(Ljava/io/ObjectInput;)V

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/element/FollowButtonElement$Kind;

    iput-object v0, p0, Lcom/twitter/library/card/element/FollowButtonElement;->kind:Lcom/twitter/library/card/element/FollowButtonElement$Kind;

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Element;->writeExternal(Ljava/io/ObjectOutput;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/FollowButtonElement;->kind:Lcom/twitter/library/card/element/FollowButtonElement$Kind;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    return-void
.end method

.method public y()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/library/card/element/Element;->y()V

    iget-object v0, p0, Lcom/twitter/library/card/element/FollowButtonElement;->mDelegate:Lcom/twitter/library/card/element/d;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/d;->f()V

    return-void
.end method
