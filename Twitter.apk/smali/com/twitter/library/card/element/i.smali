.class public Lcom/twitter/library/card/element/i;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/util/au;


# instance fields
.field private a:Lcom/twitter/library/util/as;

.field private b:Lcom/twitter/library/util/at;

.field private c:J

.field private d:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Lcom/twitter/library/util/as;Lcom/twitter/library/util/at;JLcom/twitter/library/card/element/h;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/card/element/i;->a:Lcom/twitter/library/util/as;

    iput-object p2, p0, Lcom/twitter/library/card/element/i;->b:Lcom/twitter/library/util/at;

    iput-wide p3, p0, Lcom/twitter/library/card/element/i;->c:J

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p5}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/library/card/element/i;->d:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/util/as;Ljava/util/HashMap;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/card/element/i;->b:Lcom/twitter/library/util/at;

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/an;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/twitter/library/util/an;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1, p0}, Lcom/twitter/library/util/as;->b(Lcom/twitter/library/util/au;)V

    iget-object v1, p0, Lcom/twitter/library/card/element/i;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/card/element/h;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/util/an;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/library/card/element/i;->b:Lcom/twitter/library/util/at;

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/card/element/h;->a(Lcom/twitter/library/util/at;Ljava/lang/String;)V

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Fetched Video "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/card/element/i;->b:Lcom/twitter/library/util/at;

    iget-object v1, v1, Lcom/twitter/library/util/at;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/library/card/CardDebugLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public a()Z
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/card/element/i;->a:Lcom/twitter/library/util/as;

    invoke-virtual {v0, p0}, Lcom/twitter/library/util/as;->a(Lcom/twitter/library/util/au;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/i;->a:Lcom/twitter/library/util/as;

    iget-wide v2, p0, Lcom/twitter/library/card/element/i;->c:J

    iget-object v4, p0, Lcom/twitter/library/card/element/i;->b:Lcom/twitter/library/util/at;

    invoke-virtual {v0, v2, v3, v4, v1}, Lcom/twitter/library/util/as;->a(JLcom/twitter/library/util/at;Z)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v0, p0, Lcom/twitter/library/card/element/i;->a:Lcom/twitter/library/util/as;

    invoke-virtual {v0, p0}, Lcom/twitter/library/util/as;->b(Lcom/twitter/library/util/au;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/i;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/element/h;

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/twitter/library/card/element/i;->b:Lcom/twitter/library/util/at;

    invoke-virtual {v0, v3, v2}, Lcom/twitter/library/card/element/h;->a(Lcom/twitter/library/util/at;Ljava/lang/String;)V

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Retrieved Cached Video "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/library/card/element/i;->b:Lcom/twitter/library/util/at;

    iget-object v2, v2, Lcom/twitter/library/util/at;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/twitter/library/card/CardDebugLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
