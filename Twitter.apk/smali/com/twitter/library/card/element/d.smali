.class public Lcom/twitter/library/card/element/d;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field protected b:Landroid/content/Context;

.field protected c:Lcom/twitter/library/card/element/FollowButtonElement;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/card/element/FollowButtonElement;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/card/element/d;->b:Landroid/content/Context;

    iput-object p2, p0, Lcom/twitter/library/card/element/d;->c:Lcom/twitter/library/card/element/FollowButtonElement;

    return-void
.end method


# virtual methods
.method public a(ILcom/twitter/library/card/property/Vector2F;)F
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public a()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public a(JLcom/twitter/library/util/aa;Lcom/twitter/library/util/as;)V
    .locals 0

    return-void
.end method

.method public a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/RectF;)V
    .locals 0

    return-void
.end method

.method public d()V
    .locals 0

    return-void
.end method

.method public f()V
    .locals 0

    return-void
.end method

.method public g()Lcom/twitter/library/api/TwitterUser;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/d;->c:Lcom/twitter/library/card/element/FollowButtonElement;

    iget-object v0, v0, Lcom/twitter/library/card/element/FollowButtonElement;->mUser:Lcom/twitter/library/api/TwitterUser;

    return-object v0
.end method

.method public h()J
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/card/element/d;->c:Lcom/twitter/library/card/element/FollowButtonElement;

    iget-object v0, v0, Lcom/twitter/library/card/element/FollowButtonElement;->mUser:Lcom/twitter/library/api/TwitterUser;

    if-eqz v0, :cond_0

    iget-wide v0, v0, Lcom/twitter/library/api/TwitterUser;->userId:J

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method
