.class public Lcom/twitter/library/card/element/Player;
.super Lcom/twitter/library/card/element/Image;
.source "Twttr"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field private static final serialVersionUID:J = -0x1e88ced61a3f5a94L


# instance fields
.field public htmlUrl:Ljava/lang/String;

.field protected mDelegate:Lcom/twitter/library/card/element/h;

.field public streamContentType:Ljava/lang/String;

.field public streamSize:Lcom/twitter/library/card/property/Vector2F;

.field public streamUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/library/card/element/Image;-><init>()V

    new-instance v0, Lcom/twitter/library/card/property/Vector2F;

    invoke-direct {v0}, Lcom/twitter/library/card/property/Vector2F;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/card/element/Player;->streamSize:Lcom/twitter/library/card/property/Vector2F;

    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/library/card/element/Player;->loadingIndicator:I

    return-void
.end method


# virtual methods
.method protected a()Lcom/twitter/library/card/property/Vector2F;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->mDelegate:Lcom/twitter/library/card/element/h;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/h;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->spec:Lcom/twitter/library/card/property/ImageSpec;

    iget-object v0, v0, Lcom/twitter/library/card/property/ImageSpec;->size:Lcom/twitter/library/card/property/Vector2F;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->streamSize:Lcom/twitter/library/card/property/Vector2F;

    goto :goto_0
.end method

.method public a(JLcom/twitter/library/util/aa;Lcom/twitter/library/util/as;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->mDelegate:Lcom/twitter/library/card/element/h;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/twitter/library/card/element/h;->a(JLcom/twitter/library/util/aa;Lcom/twitter/library/util/as;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/library/card/element/Image;->a(JLcom/twitter/library/util/aa;Lcom/twitter/library/util/as;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/card/element/h;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/card/element/Player;->mDelegate:Lcom/twitter/library/card/element/h;

    return-void
.end method

.method public a([I)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Image;->a([I)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->mDelegate:Lcom/twitter/library/card/element/h;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/h;->d()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->mCardView:Lcom/twitter/library/card/CardView;

    iget-object v0, v0, Lcom/twitter/library/card/CardView;->a:Lcom/twitter/library/card/o;

    iget-object v0, v0, Lcom/twitter/library/card/o;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    goto :goto_0
.end method

.method public b()Lcom/twitter/library/card/element/h;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->mDelegate:Lcom/twitter/library/card/element/h;

    return-object v0
.end method

.method public b(Landroid/content/Context;)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/library/card/element/Player;->c(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->mDelegate:Lcom/twitter/library/card/element/h;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/h;->a()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/card/element/Player;->mView:Landroid/view/View;

    return-void
.end method

.method public b(Landroid/graphics/Canvas;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Image;->b(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->mDelegate:Lcom/twitter/library/card/element/h;

    sget-object v1, Lcom/twitter/library/card/element/Player;->a:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/twitter/library/card/element/Player;->mLayoutRect:Landroid/graphics/RectF;

    invoke-virtual {v0, p1, v1, v2}, Lcom/twitter/library/card/element/h;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/RectF;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->mDelegate:Lcom/twitter/library/card/element/h;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/h;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->mCardView:Lcom/twitter/library/card/CardView;

    iget-object v0, v0, Lcom/twitter/library/card/CardView;->a:Lcom/twitter/library/card/o;

    iget-object v0, v0, Lcom/twitter/library/card/o;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/card/element/Player;->mImageKey:Lcom/twitter/library/util/m;

    iput-object v0, p0, Lcom/twitter/library/card/element/Player;->mLoadedBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method public c(Landroid/content/Context;)V
    .locals 1

    sget-object v0, Lcom/twitter/library/card/element/Player;->b:Lcom/twitter/library/card/element/a;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/library/card/element/Player;->b:Lcom/twitter/library/card/element/a;

    invoke-interface {v0, p1, p0}, Lcom/twitter/library/card/element/a;->a(Landroid/content/Context;Lcom/twitter/library/card/element/Player;)Lcom/twitter/library/card/element/h;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/card/element/Player;->mDelegate:Lcom/twitter/library/card/element/h;

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->mDelegate:Lcom/twitter/library/card/element/h;

    if-nez v0, :cond_1

    new-instance v0, Lcom/twitter/library/card/element/h;

    invoke-direct {v0, p1, p0}, Lcom/twitter/library/card/element/h;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/Player;)V

    iput-object v0, p0, Lcom/twitter/library/card/element/Player;->mDelegate:Lcom/twitter/library/card/element/h;

    :cond_1
    return-void
.end method

.method public c(Landroid/graphics/Canvas;)V
    .locals 0

    return-void
.end method

.method public c(Z)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Image;->c(Z)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->mDelegate:Lcom/twitter/library/card/element/h;

    invoke-virtual {v0, p1}, Lcom/twitter/library/card/element/h;->a(Z)V

    return-void
.end method

.method public d()Lcom/twitter/library/card/Card;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->mCard:Lcom/twitter/library/card/Card;

    return-object v0
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->mCard:Lcom/twitter/library/card/Card;

    invoke-virtual {v0}, Lcom/twitter/library/card/Card;->k()V

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/library/card/element/Player;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Image;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/twitter/library/card/element/Player;

    iget-object v2, p0, Lcom/twitter/library/card/element/Player;->htmlUrl:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/twitter/library/card/element/Player;->htmlUrl:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/element/Player;->htmlUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p1, Lcom/twitter/library/card/element/Player;->htmlUrl:Ljava/lang/String;

    if-nez v2, :cond_4

    :cond_6
    iget-object v2, p0, Lcom/twitter/library/card/element/Player;->streamContentType:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/twitter/library/card/element/Player;->streamContentType:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/element/Player;->streamContentType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p1, Lcom/twitter/library/card/element/Player;->streamContentType:Ljava/lang/String;

    if-nez v2, :cond_7

    :cond_9
    iget-object v2, p0, Lcom/twitter/library/card/element/Player;->streamSize:Lcom/twitter/library/card/property/Vector2F;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/twitter/library/card/element/Player;->streamSize:Lcom/twitter/library/card/property/Vector2F;

    iget-object v3, p1, Lcom/twitter/library/card/element/Player;->streamSize:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/Vector2F;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    goto :goto_0

    :cond_b
    iget-object v2, p1, Lcom/twitter/library/card/element/Player;->streamSize:Lcom/twitter/library/card/property/Vector2F;

    if-nez v2, :cond_a

    :cond_c
    iget-object v2, p0, Lcom/twitter/library/card/element/Player;->streamUrl:Ljava/lang/String;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/twitter/library/card/element/Player;->streamUrl:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/element/Player;->streamUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_d
    iget-object v2, p1, Lcom/twitter/library/card/element/Player;->streamUrl:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/twitter/library/card/element/Image;->hashCode()I

    move-result v0

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->htmlUrl:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->htmlUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->streamUrl:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->streamUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->streamContentType:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->streamContentType:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/library/card/element/Player;->streamSize:Lcom/twitter/library/card/property/Vector2F;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/library/card/element/Player;->streamSize:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v1}, Lcom/twitter/library/card/property/Vector2F;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->mDelegate:Lcom/twitter/library/card/element/h;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/h;->b()Z

    move-result v0

    return v0
.end method

.method public q()V
    .locals 7

    const/high16 v6, 0x40000000    # 2.0f

    invoke-super {p0}, Lcom/twitter/library/card/element/Image;->q()V

    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->mDelegate:Lcom/twitter/library/card/element/h;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/h;->d()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->mCardView:Lcom/twitter/library/card/CardView;

    iget-object v0, v0, Lcom/twitter/library/card/CardView;->a:Lcom/twitter/library/card/o;

    iget-object v0, v0, Lcom/twitter/library/card/o;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/twitter/library/card/element/Player;->mLayoutSize:Lcom/twitter/library/card/property/Vector2F;

    iget v2, v2, Lcom/twitter/library/card/property/Vector2F;->x:F

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/twitter/library/card/element/Player;->mLayoutSize:Lcom/twitter/library/card/property/Vector2F;

    iget v3, v3, Lcom/twitter/library/card/property/Vector2F;->y:F

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    iget-object v3, p0, Lcom/twitter/library/card/element/Player;->mLayoutPosition:Lcom/twitter/library/card/property/Vector2F;

    iget v3, v3, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget-object v4, p0, Lcom/twitter/library/card/element/Player;->mLayoutSize:Lcom/twitter/library/card/property/Vector2F;

    iget v4, v4, Lcom/twitter/library/card/property/Vector2F;->x:F

    sub-float/2addr v4, v1

    div-float/2addr v4, v6

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/twitter/library/card/element/Player;->mLayoutPosition:Lcom/twitter/library/card/property/Vector2F;

    iget v4, v4, Lcom/twitter/library/card/property/Vector2F;->y:F

    iget-object v5, p0, Lcom/twitter/library/card/element/Player;->mLayoutSize:Lcom/twitter/library/card/property/Vector2F;

    iget v5, v5, Lcom/twitter/library/card/property/Vector2F;->y:F

    sub-float v2, v5, v2

    div-float/2addr v2, v6

    add-float/2addr v2, v4

    float-to-int v4, v3

    float-to-int v5, v2

    add-float/2addr v3, v1

    float-to-int v3, v3

    add-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v4, v5, v3, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_0
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Image;->readExternal(Ljava/io/ObjectInput;)V

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/element/Player;->htmlUrl:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/element/Player;->streamUrl:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/element/Player;->streamContentType:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Vector2F;

    iput-object v0, p0, Lcom/twitter/library/card/element/Player;->streamSize:Lcom/twitter/library/card/property/Vector2F;

    return-void
.end method

.method protected s()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/library/card/element/Image;->s()V

    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->mDelegate:Lcom/twitter/library/card/element/h;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/h;->e()V

    invoke-virtual {p0}, Lcom/twitter/library/card/element/Player;->F()V

    return-void
.end method

.method protected t()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/library/card/element/Image;->t()V

    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->mDelegate:Lcom/twitter/library/card/element/h;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/h;->f()V

    return-void
.end method

.method public u()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/library/card/element/Image;->u()V

    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->mDelegate:Lcom/twitter/library/card/element/h;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/h;->h()V

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Image;->writeExternal(Ljava/io/ObjectOutput;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->htmlUrl:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->streamUrl:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->streamContentType:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->streamSize:Lcom/twitter/library/card/property/Vector2F;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    return-void
.end method

.method public x()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/library/card/element/Image;->x()V

    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->mDelegate:Lcom/twitter/library/card/element/h;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/h;->g()V

    return-void
.end method

.method public y()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/library/card/element/Image;->y()V

    iget-object v0, p0, Lcom/twitter/library/card/element/Player;->mDelegate:Lcom/twitter/library/card/element/h;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/h;->c()V

    return-void
.end method
