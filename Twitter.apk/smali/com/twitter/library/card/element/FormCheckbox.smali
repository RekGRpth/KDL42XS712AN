.class public Lcom/twitter/library/card/element/FormCheckbox;
.super Lcom/twitter/library/card/element/FormFieldElement;
.source "Twttr"


# static fields
.field private static final serialVersionUID:J = 0x41dedc996f2f4afL


# instance fields
.field public checked:Z

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/library/card/element/FormFieldElement;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/FormCheckbox;->mView:Landroid/view/View;

    check-cast v0, Lcom/twitter/library/card/element/FormCheckboxView;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/FormCheckboxView;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "true"

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "false"

    goto :goto_0
.end method

.method public b(Landroid/content/Context;)V
    .locals 1

    new-instance v0, Lcom/twitter/library/card/element/FormCheckboxView;

    invoke-direct {v0, p1, p0}, Lcom/twitter/library/card/element/FormCheckboxView;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/FormFieldElement;)V

    iput-object v0, p0, Lcom/twitter/library/card/element/FormCheckbox;->mView:Landroid/view/View;

    return-void
.end method

.method protected d(Lcom/twitter/library/card/Card;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/FormCheckbox;->mView:Landroid/view/View;

    check-cast v0, Lcom/twitter/library/card/element/FormCheckboxView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/card/element/FormCheckboxView;->a(Lcom/twitter/library/card/Card;)V

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/library/card/element/FormCheckbox;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Lcom/twitter/library/card/element/FormFieldElement;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/twitter/library/card/element/FormCheckbox;

    iget-boolean v2, p0, Lcom/twitter/library/card/element/FormCheckbox;->checked:Z

    iget-boolean v3, p1, Lcom/twitter/library/card/element/FormCheckbox;->checked:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/twitter/library/card/element/FormCheckbox;->value:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/twitter/library/card/element/FormCheckbox;->value:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/element/FormCheckbox;->value:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p1, Lcom/twitter/library/card/element/FormCheckbox;->value:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/twitter/library/card/element/FormFieldElement;->hashCode()I

    move-result v0

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/element/FormCheckbox;->value:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/card/element/FormCheckbox;->value:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/twitter/library/card/element/FormCheckbox;->checked:Z

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/FormFieldElement;->readExternal(Ljava/io/ObjectInput;)V

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/element/FormCheckbox;->value:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/card/element/FormCheckbox;->checked:Z

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/FormFieldElement;->writeExternal(Ljava/io/ObjectOutput;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/FormCheckbox;->value:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-boolean v0, p0, Lcom/twitter/library/card/element/FormCheckbox;->checked:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    return-void
.end method
