.class public Lcom/twitter/library/card/element/j;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private a:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ZIIII)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/library/card/element/j;->a:Landroid/widget/ProgressBar;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sub-int v0, p4, p2

    iget-object v1, p0, Lcom/twitter/library/card/element/j;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getMeasuredWidth()I

    move-result v1

    if-lt v0, v1, :cond_0

    iget-object v2, p0, Lcom/twitter/library/card/element/j;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v2}, Landroid/widget/ProgressBar;->getMeasuredHeight()I

    move-result v2

    sub-int v3, p5, p3

    if-lt v3, v2, :cond_0

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    sub-int/2addr v3, v2

    div-int/lit8 v3, v3, 0x2

    iget-object v4, p0, Lcom/twitter/library/card/element/j;->a:Landroid/widget/ProgressBar;

    add-int/2addr v1, v0

    add-int/2addr v2, v3

    invoke-virtual {v4, v0, v3, v1, v2}, Landroid/widget/ProgressBar;->layout(IIII)V

    goto :goto_0
.end method

.method public a(Landroid/view/ViewGroup;)Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/j;->a:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/element/j;->a:Landroid/widget/ProgressBar;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/card/element/j;->a:Landroid/widget/ProgressBar;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/ViewGroup;Landroid/content/Context;)Z
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/card/element/j;->a:Landroid/widget/ProgressBar;

    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    const v2, 0x101007a    # android.R.attr.progressBarStyleLarge

    invoke-direct {v0, p2, v1, v2}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/twitter/library/card/element/j;->a:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/twitter/library/card/element/j;->a:Landroid/widget/ProgressBar;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/j;->a:Landroid/widget/ProgressBar;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->bringChildToFront(Landroid/view/View;)V

    invoke-virtual {p1}, Landroid/view/ViewGroup;->requestLayout()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
