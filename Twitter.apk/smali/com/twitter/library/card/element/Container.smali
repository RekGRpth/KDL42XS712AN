.class public Lcom/twitter/library/card/element/Container;
.super Lcom/twitter/library/card/element/Box;
.source "Twttr"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field private static final serialVersionUID:J = 0x20b7a5746f30fb00L


# instance fields
.field public alignmentMode:Lcom/twitter/library/card/property/Vector2;

.field public children:Ljava/util/ArrayList;

.field public layoutMode:I

.field public overflowMode:I

.field public padding:Lcom/twitter/library/card/property/Spacing;

.field public visibleChildIndex:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/library/card/element/Box;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/library/card/element/Container;->visibleChildIndex:I

    return-void
.end method

.method private a(F)F
    .locals 12

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v0, v0, Lcom/twitter/library/card/property/Style;->border:Lcom/twitter/library/card/property/Border;

    iget-object v1, p0, Lcom/twitter/library/card/element/Container;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v1, v1, Lcom/twitter/library/card/property/Style;->padding:Lcom/twitter/library/card/property/Spacing;

    iget v0, v0, Lcom/twitter/library/card/property/Border;->width:F

    new-instance v3, Lcom/twitter/library/card/property/Vector2F;

    iget-object v4, v1, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    invoke-direct {v3, v4}, Lcom/twitter/library/card/property/Vector2F;-><init>(Lcom/twitter/library/card/property/Vector2F;)V

    invoke-virtual {v3, v0}, Lcom/twitter/library/card/property/Vector2F;->a(F)V

    new-instance v4, Lcom/twitter/library/card/property/Vector2F;

    iget-object v1, v1, Lcom/twitter/library/card/property/Spacing;->end:Lcom/twitter/library/card/property/Vector2F;

    invoke-direct {v4, v1}, Lcom/twitter/library/card/property/Vector2F;-><init>(Lcom/twitter/library/card/property/Vector2F;)V

    invoke-virtual {v4, v0}, Lcom/twitter/library/card/property/Vector2F;->a(F)V

    new-instance v5, Lcom/twitter/library/card/property/Vector2F;

    invoke-direct {v5, v3}, Lcom/twitter/library/card/property/Vector2F;-><init>(Lcom/twitter/library/card/property/Vector2F;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/element/Element;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/Element;->H()Z

    move-result v7

    if-eqz v7, :cond_2

    iget-object v7, v0, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v7, v7, Lcom/twitter/library/card/property/Style;->margin:Lcom/twitter/library/card/property/Spacing;

    sget-object v8, Lcom/twitter/library/card/property/Vector2F;->a:Lcom/twitter/library/card/property/Vector2F;

    invoke-direct {p0, v0, v8}, Lcom/twitter/library/card/element/Container;->a(Lcom/twitter/library/card/element/Element;Lcom/twitter/library/card/property/Vector2F;)F

    move-result v8

    sget-object v9, Lcom/twitter/library/card/property/Vector2F;->a:Lcom/twitter/library/card/property/Vector2F;

    invoke-direct {p0, v0, v9}, Lcom/twitter/library/card/element/Container;->b(Lcom/twitter/library/card/element/Element;Lcom/twitter/library/card/property/Vector2F;)F

    move-result v0

    iget v9, v3, Lcom/twitter/library/card/property/Vector2F;->x:F

    sub-float v9, p1, v9

    iget v10, v4, Lcom/twitter/library/card/property/Vector2F;->x:F

    sub-float/2addr v9, v10

    iget v10, v5, Lcom/twitter/library/card/property/Vector2F;->x:F

    sub-float/2addr v9, v10

    iget-object v10, v7, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    iget v10, v10, Lcom/twitter/library/card/property/Vector2F;->x:F

    add-float/2addr v10, v8

    iget-object v11, v7, Lcom/twitter/library/card/property/Spacing;->end:Lcom/twitter/library/card/property/Vector2F;

    iget v11, v11, Lcom/twitter/library/card/property/Vector2F;->x:F

    add-float/2addr v10, v11

    cmpg-float v9, v9, v10

    if-gez v9, :cond_0

    iput v2, v5, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget v9, v5, Lcom/twitter/library/card/property/Vector2F;->y:F

    add-float/2addr v1, v9

    iput v1, v5, Lcom/twitter/library/card/property/Vector2F;->y:F

    move v1, v2

    :cond_0
    iget-object v9, v7, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    iget v9, v9, Lcom/twitter/library/card/property/Vector2F;->y:F

    add-float/2addr v0, v9

    iget-object v9, v7, Lcom/twitter/library/card/property/Spacing;->end:Lcom/twitter/library/card/property/Vector2F;

    iget v9, v9, Lcom/twitter/library/card/property/Vector2F;->y:F

    add-float/2addr v0, v9

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iget v1, v5, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget-object v9, v7, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    iget v9, v9, Lcom/twitter/library/card/property/Vector2F;->x:F

    add-float/2addr v8, v9

    iget-object v7, v7, Lcom/twitter/library/card/property/Spacing;->end:Lcom/twitter/library/card/property/Vector2F;

    iget v7, v7, Lcom/twitter/library/card/property/Vector2F;->x:F

    add-float/2addr v7, v8

    add-float/2addr v1, v7

    iput v1, v5, Lcom/twitter/library/card/property/Vector2F;->x:F

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    iget v0, v5, Lcom/twitter/library/card/property/Vector2F;->y:F

    add-float/2addr v0, v1

    iget v1, v4, Lcom/twitter/library/card/property/Vector2F;->y:F

    add-float/2addr v0, v1

    return v0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private a(IIF)F
    .locals 16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/card/element/Container;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v2, v2, Lcom/twitter/library/card/property/Style;->border:Lcom/twitter/library/card/property/Border;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/card/element/Container;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v3, v3, Lcom/twitter/library/card/property/Style;->padding:Lcom/twitter/library/card/property/Spacing;

    xor-int/lit8 v11, p2, 0x1

    iget v2, v2, Lcom/twitter/library/card/property/Border;->width:F

    new-instance v12, Lcom/twitter/library/card/property/Vector2F;

    iget-object v4, v3, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    invoke-direct {v12, v4}, Lcom/twitter/library/card/property/Vector2F;-><init>(Lcom/twitter/library/card/property/Vector2F;)V

    invoke-virtual {v12, v2}, Lcom/twitter/library/card/property/Vector2F;->a(F)V

    new-instance v13, Lcom/twitter/library/card/property/Vector2F;

    iget-object v3, v3, Lcom/twitter/library/card/property/Spacing;->end:Lcom/twitter/library/card/property/Vector2F;

    invoke-direct {v13, v3}, Lcom/twitter/library/card/property/Vector2F;-><init>(Lcom/twitter/library/card/property/Vector2F;)V

    invoke-virtual {v13, v2}, Lcom/twitter/library/card/property/Vector2F;->a(F)V

    move/from16 v0, p1

    move/from16 v1, p2

    if-ne v0, v1, :cond_0

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v2

    move/from16 v0, p2

    invoke-virtual {v13, v0}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v3

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v8, v2

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/card/element/Element;

    invoke-virtual {v3}, Lcom/twitter/library/card/element/Element;->H()Z

    move-result v2

    if-eqz v2, :cond_e

    iget-object v2, v3, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v10, v2, Lcom/twitter/library/card/property/Style;->margin:Lcom/twitter/library/card/property/Spacing;

    iget-object v2, v10, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v2, v11}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v2

    sub-float v2, p3, v2

    iget-object v4, v10, Lcom/twitter/library/card/property/Spacing;->end:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v4, v11}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v4

    sub-float v7, v2, v4

    const/4 v6, 0x0

    move-object/from16 v2, p0

    move/from16 v4, p1

    move/from16 v5, p2

    invoke-direct/range {v2 .. v7}, Lcom/twitter/library/card/element/Container;->a(Lcom/twitter/library/card/element/Element;IIFF)F

    move-result v2

    iget-object v3, v10, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v3

    add-float/2addr v2, v3

    iget-object v3, v10, Lcom/twitter/library/card/property/Spacing;->end:Lcom/twitter/library/card/property/Vector2F;

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v3

    add-float/2addr v2, v3

    add-float/2addr v2, v8

    :goto_1
    move v8, v2

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v4, v3

    move v3, v2

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/card/element/Element;

    invoke-virtual {v2}, Lcom/twitter/library/card/element/Element;->H()Z

    move-result v6

    if-eqz v6, :cond_d

    iget-object v6, v2, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v6, v6, Lcom/twitter/library/card/property/Style;->margin:Lcom/twitter/library/card/property/Spacing;

    iget-object v7, v2, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    move/from16 v0, p2

    invoke-virtual {v7, v0}, Lcom/twitter/library/card/property/Vector2;->a(I)I

    move-result v7

    iget-object v8, v6, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    move/from16 v0, p2

    invoke-virtual {v8, v0}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v8

    iget-object v6, v6, Lcom/twitter/library/card/property/Spacing;->end:Lcom/twitter/library/card/property/Vector2F;

    move/from16 v0, p2

    invoke-virtual {v6, v0}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v6

    add-float/2addr v6, v8

    add-float/2addr v4, v6

    const/4 v6, 0x2

    if-ne v7, v6, :cond_2

    iget-object v2, v2, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/twitter/library/card/property/Vector2;->a(I)I

    move-result v2

    int-to-float v2, v2

    const/4 v6, 0x0

    cmpl-float v6, v2, v6

    if-lez v6, :cond_1

    :goto_3
    add-float/2addr v3, v2

    move v2, v3

    move v3, v4

    :goto_4
    move v4, v3

    move v3, v2

    goto :goto_2

    :cond_1
    const/high16 v2, 0x3f800000    # 1.0f

    goto :goto_3

    :cond_2
    sget-object v6, Lcom/twitter/library/card/property/Vector2F;->a:Lcom/twitter/library/card/property/Vector2F;

    move/from16 v0, p2

    invoke-virtual {v2, v0, v6}, Lcom/twitter/library/card/element/Element;->b(ILcom/twitter/library/card/property/Vector2F;)F

    move-result v2

    add-float/2addr v4, v2

    move v2, v3

    move v3, v4

    goto :goto_4

    :cond_3
    const/4 v2, 0x0

    sub-float v4, p3, v4

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v5

    sub-float/2addr v4, v5

    move/from16 v0, p2

    invoke-virtual {v13, v0}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v5

    sub-float/2addr v4, v5

    invoke-static {v2, v4}, Ljava/lang/Math;->max(FF)F

    move-result v14

    const/4 v2, 0x0

    const/4 v4, 0x0

    cmpl-float v4, v3, v4

    if-nez v4, :cond_4

    const/high16 v3, 0x3f800000    # 1.0f

    move v8, v3

    :goto_5
    const/4 v3, 0x0

    sub-float v2, v14, v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    move v9, v3

    :goto_6
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/card/element/Element;

    invoke-virtual {v3}, Lcom/twitter/library/card/element/Element;->H()Z

    move-result v4

    if-eqz v4, :cond_a

    iget-object v4, v3, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    move/from16 v0, p2

    invoke-virtual {v4, v0}, Lcom/twitter/library/card/property/Vector2;->a(I)I

    move-result v4

    int-to-float v4, v4

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-lez v4, :cond_6

    iget-object v4, v3, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    move/from16 v0, p2

    invoke-virtual {v4, v0}, Lcom/twitter/library/card/property/Vector2;->a(I)I

    move-result v4

    int-to-float v4, v4

    :goto_7
    mul-float/2addr v4, v14

    mul-float v6, v4, v8

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v4, v2, v4

    if-ltz v4, :cond_9

    float-to-double v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-float v6, v4

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float/2addr v2, v4

    move v10, v2

    :goto_8
    const/4 v7, 0x0

    move-object/from16 v2, p0

    move v4, v11

    move/from16 v5, p2

    invoke-direct/range {v2 .. v7}, Lcom/twitter/library/card/element/Container;->a(Lcom/twitter/library/card/element/Element;IIFF)F

    move-result v2

    iget-object v3, v3, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v3, v3, Lcom/twitter/library/card/property/Style;->margin:Lcom/twitter/library/card/property/Spacing;

    invoke-virtual {v12, v11}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v4

    add-float/2addr v2, v4

    invoke-virtual {v13, v11}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v4

    add-float/2addr v2, v4

    iget-object v4, v3, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v4, v11}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v4

    add-float/2addr v2, v4

    iget-object v3, v3, Lcom/twitter/library/card/property/Spacing;->end:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v3, v11}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v3

    add-float/2addr v2, v3

    invoke-static {v9, v2}, Ljava/lang/Math;->max(FF)F

    move-result v3

    move v2, v10

    :goto_9
    move v9, v3

    goto :goto_6

    :cond_4
    const/high16 v4, 0x3f800000    # 1.0f

    div-float v6, v4, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v3, v2

    :goto_a
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/card/element/Element;

    invoke-virtual {v2}, Lcom/twitter/library/card/element/Element;->H()Z

    move-result v4

    if-eqz v4, :cond_b

    iget-object v4, v2, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    move/from16 v0, p2

    invoke-virtual {v4, v0}, Lcom/twitter/library/card/property/Vector2;->a(I)I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_b

    iget-object v2, v2, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/twitter/library/card/property/Vector2;->a(I)I

    move-result v2

    int-to-float v2, v2

    const/4 v4, 0x0

    cmpl-float v4, v2, v4

    if-lez v4, :cond_5

    float-to-double v4, v2

    :goto_b
    float-to-double v8, v6

    mul-double/2addr v4, v8

    float-to-double v8, v14

    mul-double/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-float v2, v4

    add-float/2addr v3, v2

    move v2, v3

    :goto_c
    move v3, v2

    goto :goto_a

    :cond_5
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    goto :goto_b

    :cond_6
    const/high16 v4, 0x3f800000    # 1.0f

    goto/16 :goto_7

    :cond_7
    move v8, v9

    :cond_8
    return v8

    :cond_9
    move v10, v2

    goto :goto_8

    :cond_a
    move v3, v9

    goto :goto_9

    :cond_b
    move v2, v3

    goto :goto_c

    :cond_c
    move v8, v6

    move v2, v3

    goto/16 :goto_5

    :cond_d
    move v2, v3

    move v3, v4

    goto/16 :goto_4

    :cond_e
    move v2, v8

    goto/16 :goto_1
.end method

.method private a(Lcom/twitter/library/card/element/Element;IIFF)F
    .locals 2

    if-nez p3, :cond_0

    move v0, p4

    :goto_0
    if-nez p3, :cond_1

    :goto_1
    new-instance v1, Lcom/twitter/library/card/property/Vector2F;

    invoke-direct {v1, v0, p5}, Lcom/twitter/library/card/property/Vector2F;-><init>(FF)V

    if-nez p2, :cond_2

    invoke-direct {p0, p1, v1}, Lcom/twitter/library/card/element/Container;->a(Lcom/twitter/library/card/element/Element;Lcom/twitter/library/card/property/Vector2F;)F

    move-result v0

    :goto_2
    return v0

    :cond_0
    move v0, p5

    goto :goto_0

    :cond_1
    move p5, p4

    goto :goto_1

    :cond_2
    invoke-direct {p0, p1, v1}, Lcom/twitter/library/card/element/Container;->b(Lcom/twitter/library/card/element/Element;Lcom/twitter/library/card/property/Vector2F;)F

    move-result v0

    goto :goto_2
.end method

.method private a(Lcom/twitter/library/card/element/Element;Lcom/twitter/library/card/property/Vector2F;)F
    .locals 5

    const/4 v4, 0x1

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/twitter/library/card/element/Element;->v()I

    move-result v0

    iget-object v1, p1, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v1, v1, Lcom/twitter/library/card/property/Vector2;->x:I

    if-ne v1, v2, :cond_0

    iget v0, p2, Lcom/twitter/library/card/property/Vector2F;->x:F

    :goto_0
    iget v1, p2, Lcom/twitter/library/card/property/Vector2F;->x:F

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/library/card/element/Container;->b(Lcom/twitter/library/card/element/Element;FF)F

    move-result v0

    return v0

    :cond_0
    if-ne v0, v2, :cond_2

    iget-object v0, p1, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v0, v0, Lcom/twitter/library/card/property/Vector2;->y:I

    if-ne v0, v2, :cond_1

    iget v0, p2, Lcom/twitter/library/card/property/Vector2F;->y:F

    :goto_1
    iget v1, p2, Lcom/twitter/library/card/property/Vector2F;->y:F

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/library/card/element/Container;->c(Lcom/twitter/library/card/element/Element;FF)F

    move-result v0

    new-instance v1, Lcom/twitter/library/card/property/Vector2F;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/twitter/library/card/property/Vector2F;-><init>(FF)V

    invoke-virtual {p1, v3, v1}, Lcom/twitter/library/card/element/Element;->b(ILcom/twitter/library/card/property/Vector2F;)F

    move-result v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1, v4, p2}, Lcom/twitter/library/card/element/Element;->b(ILcom/twitter/library/card/property/Vector2F;)F

    move-result v0

    goto :goto_1

    :cond_2
    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    invoke-virtual {p1, v3, p2}, Lcom/twitter/library/card/element/Element;->b(ILcom/twitter/library/card/property/Vector2F;)F

    move-result v0

    invoke-virtual {p1, v4, p2}, Lcom/twitter/library/card/element/Element;->b(ILcom/twitter/library/card/property/Vector2F;)F

    move-result v1

    iget v2, p2, Lcom/twitter/library/card/property/Vector2F;->x:F

    invoke-direct {p0, p1, v0, v2}, Lcom/twitter/library/card/element/Container;->b(Lcom/twitter/library/card/element/Element;FF)F

    move-result v2

    iget v3, p2, Lcom/twitter/library/card/property/Vector2F;->y:F

    invoke-direct {p0, p1, v1, v3}, Lcom/twitter/library/card/element/Container;->c(Lcom/twitter/library/card/element/Element;FF)F

    move-result v3

    invoke-direct {p0, v2, v3, v0, v1}, Lcom/twitter/library/card/element/Container;->a(FFFF)Landroid/graphics/PointF;

    move-result-object v0

    iget v0, v0, Landroid/graphics/PointF;->x:F

    goto :goto_0

    :cond_3
    invoke-virtual {p1, v3, p2}, Lcom/twitter/library/card/element/Element;->b(ILcom/twitter/library/card/property/Vector2F;)F

    move-result v0

    goto :goto_0
.end method

.method private a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/element/Element;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/Element;->H()Z

    move-result v0

    if-eqz v0, :cond_1

    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private a(FFFF)Landroid/graphics/PointF;
    .locals 4

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    cmpl-float v1, p1, p3

    if-nez v1, :cond_0

    cmpl-float v1, p2, p4

    if-eqz v1, :cond_1

    :cond_0
    div-float v1, p3, p4

    div-float v2, p1, p2

    cmpl-float v2, v1, v2

    if-lez v2, :cond_2

    float-to-double v2, p1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-float v2, v2

    iput v2, v0, Landroid/graphics/PointF;->x:F

    div-float v1, p1, v1

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->floor(D)D

    move-result-wide v1

    double-to-float v1, v1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    mul-float/2addr v1, p2

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->floor(D)D

    move-result-wide v1

    double-to-float v1, v1

    iput v1, v0, Landroid/graphics/PointF;->x:F

    float-to-double v1, p2

    invoke-static {v1, v2}, Ljava/lang/Math;->floor(D)D

    move-result-wide v1

    double-to-float v1, v1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0
.end method

.method private a(FFI)V
    .locals 23

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/card/element/Container;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v2, v2, Lcom/twitter/library/card/property/Style;->border:Lcom/twitter/library/card/property/Border;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/card/element/Container;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v3, v3, Lcom/twitter/library/card/property/Style;->padding:Lcom/twitter/library/card/property/Spacing;

    new-instance v12, Lcom/twitter/library/card/property/Vector2F;

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-direct {v12, v0, v1}, Lcom/twitter/library/card/property/Vector2F;-><init>(FF)V

    xor-int/lit8 v13, p3, 0x1

    iget v2, v2, Lcom/twitter/library/card/property/Border;->width:F

    new-instance v14, Lcom/twitter/library/card/property/Vector2F;

    iget-object v4, v3, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    invoke-direct {v14, v4}, Lcom/twitter/library/card/property/Vector2F;-><init>(Lcom/twitter/library/card/property/Vector2F;)V

    invoke-virtual {v14, v2}, Lcom/twitter/library/card/property/Vector2F;->a(F)V

    new-instance v15, Lcom/twitter/library/card/property/Vector2F;

    iget-object v3, v3, Lcom/twitter/library/card/property/Spacing;->end:Lcom/twitter/library/card/property/Vector2F;

    invoke-direct {v15, v3}, Lcom/twitter/library/card/property/Vector2F;-><init>(Lcom/twitter/library/card/property/Vector2F;)V

    invoke-virtual {v15, v2}, Lcom/twitter/library/card/property/Vector2F;->a(F)V

    invoke-direct/range {p0 .. p0}, Lcom/twitter/library/card/element/Container;->a()I

    move-result v2

    if-lez v2, :cond_d

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-virtual {v12, v13}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v2

    invoke-virtual {v14, v13}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v5

    sub-float/2addr v2, v5

    invoke-virtual {v15, v13}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v5

    sub-float v16, v2, v5

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    move v8, v2

    move v9, v3

    move v10, v4

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/card/element/Element;

    iget-object v2, v3, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v0, v2, Lcom/twitter/library/card/property/Style;->margin:Lcom/twitter/library/card/property/Spacing;

    move-object/from16 v17, v0

    invoke-virtual {v3}, Lcom/twitter/library/card/element/Element;->H()Z

    move-result v2

    if-eqz v2, :cond_14

    iget-object v2, v3, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/twitter/library/card/property/Vector2;->a(I)I

    move-result v2

    const/4 v4, 0x2

    if-eq v2, v4, :cond_0

    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v2, v13}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v2

    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/twitter/library/card/property/Spacing;->end:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v4, v13}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v4

    add-float/2addr v2, v4

    sub-float v7, v16, v2

    const/4 v6, 0x0

    move-object/from16 v2, p0

    move/from16 v4, p3

    move/from16 v5, p3

    invoke-direct/range {v2 .. v7}, Lcom/twitter/library/card/element/Container;->a(Lcom/twitter/library/card/element/Element;IIFF)F

    move-result v2

    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v3

    add-float/2addr v2, v3

    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/twitter/library/card/property/Spacing;->end:Lcom/twitter/library/card/property/Vector2F;

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v3

    add-float/2addr v2, v3

    add-float v4, v10, v2

    move v2, v8

    move v3, v9

    :goto_1
    move v8, v2

    move v9, v3

    move v10, v4

    goto :goto_0

    :cond_0
    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v2

    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/twitter/library/card/property/Spacing;->end:Lcom/twitter/library/card/property/Vector2F;

    move/from16 v0, p3

    invoke-virtual {v4, v0}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v4

    add-float/2addr v2, v4

    add-float v4, v10, v2

    add-int/lit8 v9, v9, 0x1

    iget-object v2, v3, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/twitter/library/card/property/Vector2;->a(I)I

    move-result v2

    int-to-float v2, v2

    const/4 v3, 0x0

    cmpl-float v3, v2, v3

    if-lez v3, :cond_1

    :goto_2
    add-float/2addr v8, v2

    move v2, v8

    move v3, v9

    goto :goto_1

    :cond_1
    const/high16 v2, 0x3f800000    # 1.0f

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    const/4 v2, 0x0

    if-lez v9, :cond_13

    move/from16 v0, p3

    invoke-virtual {v12, v0}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v2

    sub-float/2addr v2, v10

    move/from16 v0, p3

    invoke-virtual {v14, v0}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v3

    sub-float/2addr v2, v3

    move/from16 v0, p3

    invoke-virtual {v15, v0}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v3

    sub-float v4, v2, v3

    const/high16 v2, 0x3f800000    # 1.0f

    div-float v5, v2, v8

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v2

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/card/element/Element;

    invoke-virtual {v2}, Lcom/twitter/library/card/element/Element;->H()Z

    move-result v7

    if-eqz v7, :cond_12

    iget-object v7, v2, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v7, v7, Lcom/twitter/library/card/property/Vector2;->y:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_12

    float-to-double v7, v3

    iget-object v3, v2, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Lcom/twitter/library/card/property/Vector2;->a(I)I

    move-result v3

    int-to-float v3, v3

    const/4 v10, 0x0

    cmpl-float v3, v3, v10

    if-lez v3, :cond_3

    iget-object v2, v2, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/twitter/library/card/property/Vector2;->a(I)I

    move-result v2

    int-to-float v2, v2

    :goto_4
    mul-float/2addr v2, v5

    mul-float/2addr v2, v4

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    add-double/2addr v2, v7

    double-to-float v3, v2

    move v2, v3

    :goto_5
    move v3, v2

    goto :goto_3

    :cond_3
    const/high16 v2, 0x3f800000    # 1.0f

    goto :goto_4

    :cond_4
    sub-float v2, v4, v3

    mul-float v3, v4, v5

    move v8, v2

    move v10, v3

    :goto_6
    move/from16 v0, p3

    invoke-virtual {v14, v0}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v11

    if-nez v9, :cond_10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/card/element/Container;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/twitter/library/card/property/Vector2;->a(I)I

    move-result v17

    const/4 v2, 0x3

    move/from16 v0, v17

    if-eq v0, v2, :cond_5

    const/4 v2, 0x2

    move/from16 v0, v17

    if-ne v0, v2, :cond_10

    :cond_5
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    move v9, v2

    :goto_7
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/card/element/Element;

    invoke-virtual {v3}, Lcom/twitter/library/card/element/Element;->H()Z

    move-result v2

    if-eqz v2, :cond_11

    iget-object v2, v3, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v0, v2, Lcom/twitter/library/card/property/Style;->margin:Lcom/twitter/library/card/property/Spacing;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v2, v0, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v2, v13}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v2

    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/twitter/library/card/property/Spacing;->end:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v4, v13}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v4

    add-float/2addr v2, v4

    sub-float v7, v16, v2

    const/4 v6, 0x0

    move-object/from16 v2, p0

    move/from16 v4, p3

    move/from16 v5, p3

    invoke-direct/range {v2 .. v7}, Lcom/twitter/library/card/element/Container;->a(Lcom/twitter/library/card/element/Element;IIFF)F

    move-result v2

    move-object/from16 v0, v19

    iget-object v3, v0, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v3

    add-float/2addr v2, v3

    move-object/from16 v0, v19

    iget-object v3, v0, Lcom/twitter/library/card/property/Spacing;->end:Lcom/twitter/library/card/property/Vector2F;

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v3

    add-float/2addr v2, v3

    add-float/2addr v2, v9

    :goto_8
    move v9, v2

    goto :goto_7

    :cond_6
    const/4 v2, 0x3

    move/from16 v0, v17

    if-ne v0, v2, :cond_7

    move/from16 v0, p3

    invoke-virtual {v12, v0}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v2

    sub-float/2addr v2, v9

    move/from16 v0, p3

    invoke-virtual {v15, v0}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v3

    sub-float/2addr v2, v3

    :goto_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    move/from16 v22, v2

    move v2, v8

    move/from16 v8, v22

    :goto_a
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/card/element/Element;

    invoke-virtual {v3}, Lcom/twitter/library/card/element/Element;->H()Z

    move-result v4

    if-eqz v4, :cond_f

    iget-object v4, v3, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v0, v4, Lcom/twitter/library/card/property/Style;->margin:Lcom/twitter/library/card/property/Spacing;

    move-object/from16 v17, v0

    new-instance v18, Lcom/twitter/library/card/property/Vector2F;

    invoke-direct/range {v18 .. v18}, Lcom/twitter/library/card/property/Vector2F;-><init>()V

    new-instance v19, Lcom/twitter/library/card/property/Vector2F;

    invoke-direct/range {v19 .. v19}, Lcom/twitter/library/card/property/Vector2F;-><init>()V

    iget-object v4, v3, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    move/from16 v0, p3

    invoke-virtual {v4, v0}, Lcom/twitter/library/card/property/Vector2;->a(I)I

    move-result v4

    int-to-float v4, v4

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-lez v4, :cond_8

    iget-object v4, v3, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    move/from16 v0, p3

    invoke-virtual {v4, v0}, Lcom/twitter/library/card/property/Vector2;->a(I)I

    move-result v4

    int-to-float v4, v4

    :goto_b
    mul-float v6, v10, v4

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v4, v2, v4

    if-ltz v4, :cond_e

    float-to-double v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-float v6, v4

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float/2addr v2, v4

    move v9, v2

    :goto_c
    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v2, v13}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v2

    sub-float v2, v16, v2

    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/twitter/library/card/property/Spacing;->end:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v4, v13}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v4

    sub-float v7, v2, v4

    move-object/from16 v2, p0

    move v4, v13

    move/from16 v5, p3

    invoke-direct/range {v2 .. v7}, Lcom/twitter/library/card/element/Container;->a(Lcom/twitter/library/card/element/Element;IIFF)F

    move-result v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v13, v2}, Lcom/twitter/library/card/property/Vector2F;->a(IF)V

    iget-object v4, v3, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    invoke-virtual {v4, v13}, Lcom/twitter/library/card/property/Vector2;->a(I)I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_9

    invoke-virtual {v14, v13}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v4

    move-object/from16 v0, v17

    iget-object v5, v0, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v5, v13}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v5

    add-float/2addr v4, v5

    move-object/from16 v0, v19

    invoke-virtual {v0, v13, v4}, Lcom/twitter/library/card/property/Vector2F;->a(IF)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v13, v2}, Lcom/twitter/library/card/property/Vector2F;->a(IF)V

    :goto_d
    move-object/from16 v2, p0

    move/from16 v4, p3

    move/from16 v5, p3

    invoke-direct/range {v2 .. v7}, Lcom/twitter/library/card/element/Container;->a(Lcom/twitter/library/card/element/Element;IIFF)F

    move-result v2

    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    move/from16 v0, p3

    invoke-virtual {v4, v0}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v4

    add-float/2addr v4, v8

    move-object/from16 v0, v19

    move/from16 v1, p3

    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/card/property/Vector2F;->a(IF)V

    move-object/from16 v0, v18

    move/from16 v1, p3

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/card/property/Vector2F;->a(IF)V

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v3, v0, v1}, Lcom/twitter/library/card/element/Element;->a(Lcom/twitter/library/card/property/Vector2F;Lcom/twitter/library/card/property/Vector2F;)V

    move-object/from16 v0, v18

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v2

    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v3

    add-float/2addr v2, v3

    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/twitter/library/card/property/Spacing;->end:Lcom/twitter/library/card/property/Vector2F;

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v3

    add-float/2addr v2, v3

    add-float/2addr v2, v8

    move v8, v9

    :goto_e
    move/from16 v22, v2

    move v2, v8

    move/from16 v8, v22

    goto/16 :goto_a

    :cond_7
    const/4 v2, 0x2

    move/from16 v0, v17

    if-ne v0, v2, :cond_10

    const/4 v2, 0x0

    const/high16 v3, 0x3f000000    # 0.5f

    move/from16 v0, p3

    invoke-virtual {v12, v0}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v4

    sub-float/2addr v4, v9

    mul-float/2addr v3, v4

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->floor(D)D

    move-result-wide v3

    double-to-float v3, v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    goto/16 :goto_9

    :cond_8
    const/high16 v4, 0x3f800000    # 1.0f

    goto/16 :goto_b

    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/card/element/Container;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    invoke-virtual {v4, v13}, Lcom/twitter/library/card/property/Vector2;->a(I)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_b

    invoke-virtual {v14, v13}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v4

    move-object/from16 v0, v17

    iget-object v5, v0, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v5, v13}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v5

    add-float/2addr v4, v5

    move-object/from16 v0, v19

    invoke-virtual {v0, v13, v4}, Lcom/twitter/library/card/property/Vector2F;->a(IF)V

    :cond_a
    :goto_f
    move-object/from16 v0, v18

    invoke-virtual {v0, v13, v2}, Lcom/twitter/library/card/property/Vector2F;->a(IF)V

    goto/16 :goto_d

    :cond_b
    const/4 v5, 0x2

    if-ne v4, v5, :cond_c

    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v4, v13}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v4

    move-object/from16 v0, v17

    iget-object v5, v0, Lcom/twitter/library/card/property/Spacing;->end:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v5, v13}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v17

    iget-object v5, v0, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v5, v13}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v5

    const/high16 v20, 0x3f000000    # 0.5f

    invoke-virtual {v12, v13}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v21

    sub-float v21, v21, v2

    add-float v4, v4, v21

    mul-float v4, v4, v20

    float-to-double v0, v4

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->floor(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-float v4, v0

    invoke-static {v5, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v13, v4}, Lcom/twitter/library/card/property/Vector2F;->a(IF)V

    goto :goto_f

    :cond_c
    const/4 v5, 0x3

    if-ne v4, v5, :cond_a

    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v4, v13}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v4

    add-float/2addr v4, v2

    move-object/from16 v0, v17

    iget-object v5, v0, Lcom/twitter/library/card/property/Spacing;->end:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v5, v13}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v5

    add-float/2addr v4, v5

    invoke-virtual {v12, v13}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v5

    sub-float v4, v5, v4

    invoke-virtual {v15, v13}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v5

    sub-float/2addr v4, v5

    move-object/from16 v0, v19

    invoke-virtual {v0, v13, v4}, Lcom/twitter/library/card/property/Vector2F;->a(IF)V

    goto :goto_f

    :cond_d
    return-void

    :cond_e
    move v9, v2

    goto/16 :goto_c

    :cond_f
    move/from16 v22, v8

    move v8, v2

    move/from16 v2, v22

    goto/16 :goto_e

    :cond_10
    move v2, v11

    goto/16 :goto_9

    :cond_11
    move v2, v9

    goto/16 :goto_8

    :cond_12
    move v2, v3

    goto/16 :goto_5

    :cond_13
    move v8, v2

    move v10, v3

    goto/16 :goto_6

    :cond_14
    move v2, v8

    move v3, v9

    move v4, v10

    goto/16 :goto_1
.end method

.method private a(Lcom/twitter/library/card/element/Element;FF)V
    .locals 8

    const/4 v7, 0x2

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v0, v0, Lcom/twitter/library/card/property/Style;->border:Lcom/twitter/library/card/property/Border;

    iget v0, v0, Lcom/twitter/library/card/property/Border;->width:F

    iget-object v1, p1, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v2, v1, Lcom/twitter/library/card/property/Style;->margin:Lcom/twitter/library/card/property/Spacing;

    iget-object v1, p1, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v1, v1, Lcom/twitter/library/card/property/Style;->positionX:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget-object v3, p1, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v3, v3, Lcom/twitter/library/card/property/Style;->positionY:Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    new-instance v4, Lcom/twitter/library/card/property/Vector2F;

    iget-object v5, v2, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    invoke-direct {v4, v5}, Lcom/twitter/library/card/property/Vector2F;-><init>(Lcom/twitter/library/card/property/Vector2F;)V

    invoke-virtual {v4, v0}, Lcom/twitter/library/card/property/Vector2F;->a(F)V

    new-instance v5, Lcom/twitter/library/card/property/Vector2F;

    iget-object v2, v2, Lcom/twitter/library/card/property/Spacing;->end:Lcom/twitter/library/card/property/Vector2F;

    invoke-direct {v5, v2}, Lcom/twitter/library/card/property/Vector2F;-><init>(Lcom/twitter/library/card/property/Vector2F;)V

    invoke-virtual {v5, v0}, Lcom/twitter/library/card/property/Vector2F;->a(F)V

    new-instance v2, Lcom/twitter/library/card/property/Vector2F;

    invoke-direct {v2, p2, p3}, Lcom/twitter/library/card/property/Vector2F;-><init>(FF)V

    invoke-virtual {v2, v4}, Lcom/twitter/library/card/property/Vector2F;->a(Lcom/twitter/library/card/property/Vector2F;)V

    invoke-virtual {v2, v5}, Lcom/twitter/library/card/property/Vector2F;->a(Lcom/twitter/library/card/property/Vector2F;)V

    invoke-direct {p0, p1, v2}, Lcom/twitter/library/card/element/Container;->a(Lcom/twitter/library/card/element/Element;Lcom/twitter/library/card/property/Vector2F;)F

    move-result v0

    iget-object v6, p1, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v6, v6, Lcom/twitter/library/card/property/Vector2;->x:I

    if-ne v6, v7, :cond_0

    iget v1, v4, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget v0, v4, Lcom/twitter/library/card/property/Vector2F;->x:F

    sub-float v0, p2, v0

    iget v6, v5, Lcom/twitter/library/card/property/Vector2F;->x:F

    sub-float/2addr v0, v6

    :cond_0
    invoke-direct {p0, p1, v2}, Lcom/twitter/library/card/element/Container;->b(Lcom/twitter/library/card/element/Element;Lcom/twitter/library/card/property/Vector2F;)F

    move-result v2

    iget-object v6, p1, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v6, v6, Lcom/twitter/library/card/property/Vector2;->y:I

    if-ne v6, v7, :cond_1

    iget v3, v4, Lcom/twitter/library/card/property/Vector2F;->y:F

    iget v2, v4, Lcom/twitter/library/card/property/Vector2F;->y:F

    sub-float v2, p3, v2

    iget v4, v5, Lcom/twitter/library/card/property/Vector2F;->y:F

    sub-float/2addr v2, v4

    :cond_1
    new-instance v4, Lcom/twitter/library/card/property/Vector2F;

    invoke-direct {v4, v1, v3}, Lcom/twitter/library/card/property/Vector2F;-><init>(FF)V

    new-instance v1, Lcom/twitter/library/card/property/Vector2F;

    invoke-direct {v1, v0, v2}, Lcom/twitter/library/card/property/Vector2F;-><init>(FF)V

    invoke-virtual {p1, v4, v1}, Lcom/twitter/library/card/element/Element;->a(Lcom/twitter/library/card/property/Vector2F;Lcom/twitter/library/card/property/Vector2F;)V

    return-void
.end method

.method private b(Lcom/twitter/library/card/element/Element;FF)F
    .locals 3

    iget-object v0, p1, Lcom/twitter/library/card/element/Element;->maxSizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v0, v0, Lcom/twitter/library/card/property/Vector2;->x:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    iget-object v0, p1, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v0, v0, Lcom/twitter/library/card/property/Style;->maxSizeX:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget-object v1, p1, Lcom/twitter/library/card/element/Element;->maxSizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v1, v1, Lcom/twitter/library/card/property/Vector2;->x:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-lez v1, :cond_2

    :goto_0
    mul-float/2addr v0, p3

    :cond_0
    :goto_1
    cmpl-float v1, p2, v0

    if-lez v1, :cond_1

    move p2, v0

    :cond_1
    return p2

    :cond_2
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0

    :cond_3
    iget-object v1, p1, Lcom/twitter/library/card/element/Element;->maxSizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v1, v1, Lcom/twitter/library/card/property/Vector2;->x:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    move v0, p2

    goto :goto_1
.end method

.method private b(Lcom/twitter/library/card/element/Element;Lcom/twitter/library/card/property/Vector2F;)F
    .locals 5

    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p1}, Lcom/twitter/library/card/element/Element;->v()I

    move-result v0

    iget-object v1, p1, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v1, v1, Lcom/twitter/library/card/property/Vector2;->y:I

    if-ne v1, v4, :cond_0

    iget v0, p2, Lcom/twitter/library/card/property/Vector2F;->y:F

    :goto_0
    iget v1, p2, Lcom/twitter/library/card/property/Vector2F;->y:F

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/library/card/element/Container;->c(Lcom/twitter/library/card/element/Element;FF)F

    move-result v0

    return v0

    :cond_0
    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    iget-object v0, p1, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v0, v0, Lcom/twitter/library/card/property/Vector2;->x:I

    if-ne v0, v4, :cond_1

    iget v0, p2, Lcom/twitter/library/card/property/Vector2F;->x:F

    :goto_1
    iget v1, p2, Lcom/twitter/library/card/property/Vector2F;->x:F

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/library/card/element/Container;->b(Lcom/twitter/library/card/element/Element;FF)F

    move-result v0

    new-instance v1, Lcom/twitter/library/card/property/Vector2F;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/card/property/Vector2F;-><init>(FF)V

    invoke-virtual {p1, v3, v1}, Lcom/twitter/library/card/element/Element;->b(ILcom/twitter/library/card/property/Vector2F;)F

    move-result v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1, v2, p2}, Lcom/twitter/library/card/element/Element;->b(ILcom/twitter/library/card/property/Vector2F;)F

    move-result v0

    goto :goto_1

    :cond_2
    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    invoke-virtual {p1, v2, p2}, Lcom/twitter/library/card/element/Element;->b(ILcom/twitter/library/card/property/Vector2F;)F

    move-result v0

    invoke-virtual {p1, v3, p2}, Lcom/twitter/library/card/element/Element;->b(ILcom/twitter/library/card/property/Vector2F;)F

    move-result v1

    iget v2, p2, Lcom/twitter/library/card/property/Vector2F;->x:F

    invoke-direct {p0, p1, v0, v2}, Lcom/twitter/library/card/element/Container;->b(Lcom/twitter/library/card/element/Element;FF)F

    move-result v2

    iget v3, p2, Lcom/twitter/library/card/property/Vector2F;->y:F

    invoke-direct {p0, p1, v1, v3}, Lcom/twitter/library/card/element/Container;->c(Lcom/twitter/library/card/element/Element;FF)F

    move-result v3

    invoke-direct {p0, v2, v3, v0, v1}, Lcom/twitter/library/card/element/Container;->a(FFFF)Landroid/graphics/PointF;

    move-result-object v0

    iget v0, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0

    :cond_3
    invoke-virtual {p1, v3, p2}, Lcom/twitter/library/card/element/Element;->b(ILcom/twitter/library/card/property/Vector2F;)F

    move-result v0

    goto :goto_0
.end method

.method private b(F)V
    .locals 12

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v0, v0, Lcom/twitter/library/card/property/Style;->border:Lcom/twitter/library/card/property/Border;

    iget-object v1, p0, Lcom/twitter/library/card/element/Container;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v1, v1, Lcom/twitter/library/card/property/Style;->padding:Lcom/twitter/library/card/property/Spacing;

    iget v0, v0, Lcom/twitter/library/card/property/Border;->width:F

    new-instance v3, Lcom/twitter/library/card/property/Vector2F;

    iget-object v4, v1, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    invoke-direct {v3, v4}, Lcom/twitter/library/card/property/Vector2F;-><init>(Lcom/twitter/library/card/property/Vector2F;)V

    invoke-virtual {v3, v0}, Lcom/twitter/library/card/property/Vector2F;->a(F)V

    new-instance v4, Lcom/twitter/library/card/property/Vector2F;

    iget-object v1, v1, Lcom/twitter/library/card/property/Spacing;->end:Lcom/twitter/library/card/property/Vector2F;

    invoke-direct {v4, v1}, Lcom/twitter/library/card/property/Vector2F;-><init>(Lcom/twitter/library/card/property/Vector2F;)V

    invoke-virtual {v4, v0}, Lcom/twitter/library/card/property/Vector2F;->a(F)V

    new-instance v5, Lcom/twitter/library/card/property/Vector2F;

    invoke-direct {v5, v3}, Lcom/twitter/library/card/property/Vector2F;-><init>(Lcom/twitter/library/card/property/Vector2F;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/element/Element;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/Element;->H()Z

    move-result v7

    if-eqz v7, :cond_1

    iget-object v7, v0, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v7, v7, Lcom/twitter/library/card/property/Style;->margin:Lcom/twitter/library/card/property/Spacing;

    new-instance v8, Lcom/twitter/library/card/property/Vector2F;

    sget-object v9, Lcom/twitter/library/card/property/Vector2F;->a:Lcom/twitter/library/card/property/Vector2F;

    invoke-direct {p0, v0, v9}, Lcom/twitter/library/card/element/Container;->a(Lcom/twitter/library/card/element/Element;Lcom/twitter/library/card/property/Vector2F;)F

    move-result v9

    sget-object v10, Lcom/twitter/library/card/property/Vector2F;->a:Lcom/twitter/library/card/property/Vector2F;

    invoke-direct {p0, v0, v10}, Lcom/twitter/library/card/element/Container;->b(Lcom/twitter/library/card/element/Element;Lcom/twitter/library/card/property/Vector2F;)F

    move-result v10

    invoke-direct {v8, v9, v10}, Lcom/twitter/library/card/property/Vector2F;-><init>(FF)V

    iget v9, v3, Lcom/twitter/library/card/property/Vector2F;->x:F

    sub-float v9, p1, v9

    iget v10, v4, Lcom/twitter/library/card/property/Vector2F;->x:F

    sub-float/2addr v9, v10

    iget v10, v5, Lcom/twitter/library/card/property/Vector2F;->x:F

    sub-float/2addr v9, v10

    iget v10, v8, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget-object v11, v7, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    iget v11, v11, Lcom/twitter/library/card/property/Vector2F;->x:F

    add-float/2addr v10, v11

    iget-object v11, v7, Lcom/twitter/library/card/property/Spacing;->end:Lcom/twitter/library/card/property/Vector2F;

    iget v11, v11, Lcom/twitter/library/card/property/Vector2F;->x:F

    add-float/2addr v10, v11

    cmpg-float v9, v9, v10

    if-gez v9, :cond_0

    iput v2, v5, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget v9, v5, Lcom/twitter/library/card/property/Vector2F;->y:F

    add-float/2addr v1, v9

    iput v1, v5, Lcom/twitter/library/card/property/Vector2F;->y:F

    move v1, v2

    :cond_0
    iget v9, v8, Lcom/twitter/library/card/property/Vector2F;->y:F

    iget-object v10, v7, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    iget v10, v10, Lcom/twitter/library/card/property/Vector2F;->y:F

    add-float/2addr v9, v10

    iget-object v10, v7, Lcom/twitter/library/card/property/Spacing;->end:Lcom/twitter/library/card/property/Vector2F;

    iget v10, v10, Lcom/twitter/library/card/property/Vector2F;->y:F

    add-float/2addr v9, v10

    invoke-static {v1, v9}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iget v9, v5, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget-object v10, v7, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    iget v10, v10, Lcom/twitter/library/card/property/Vector2F;->x:F

    add-float/2addr v9, v10

    iput v9, v5, Lcom/twitter/library/card/property/Vector2F;->x:F

    invoke-virtual {v0, v5, v8}, Lcom/twitter/library/card/element/Element;->a(Lcom/twitter/library/card/property/Vector2F;Lcom/twitter/library/card/property/Vector2F;)V

    iget v0, v5, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget v8, v8, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget-object v7, v7, Lcom/twitter/library/card/property/Spacing;->end:Lcom/twitter/library/card/property/Vector2F;

    iget v7, v7, Lcom/twitter/library/card/property/Vector2F;->x:F

    add-float/2addr v7, v8

    add-float/2addr v0, v7

    iput v0, v5, Lcom/twitter/library/card/property/Vector2F;->x:F

    :cond_1
    move v0, v1

    move v1, v0

    goto :goto_0

    :cond_2
    return-void
.end method

.method private c(Lcom/twitter/library/card/element/Element;FF)F
    .locals 3

    iget-object v0, p1, Lcom/twitter/library/card/element/Element;->maxSizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v0, v0, Lcom/twitter/library/card/property/Vector2;->y:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    iget-object v0, p1, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v0, v0, Lcom/twitter/library/card/property/Style;->maxSizeY:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget-object v1, p1, Lcom/twitter/library/card/element/Element;->maxSizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v1, v1, Lcom/twitter/library/card/property/Vector2;->y:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-lez v1, :cond_2

    :goto_0
    mul-float/2addr v0, p3

    :cond_0
    :goto_1
    cmpl-float v1, p2, v0

    if-lez v1, :cond_1

    move p2, v0

    :cond_1
    return p2

    :cond_2
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0

    :cond_3
    iget-object v1, p1, Lcom/twitter/library/card/element/Element;->maxSizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v1, v1, Lcom/twitter/library/card/property/Vector2;->y:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    move v0, p2

    goto :goto_1
.end method


# virtual methods
.method protected a(ILcom/twitter/library/card/property/Vector2F;)F
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    iget v0, p0, Lcom/twitter/library/card/element/Container;->layoutMode:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    invoke-super {p0, p1, p2}, Lcom/twitter/library/card/element/Box;->a(ILcom/twitter/library/card/property/Vector2F;)F

    move-result v0

    :goto_1
    return v0

    :pswitch_0
    iget v0, p2, Lcom/twitter/library/card/property/Vector2F;->y:F

    invoke-direct {p0, v1, v1, v0}, Lcom/twitter/library/card/element/Container;->a(IIF)F

    move-result v0

    goto :goto_1

    :pswitch_1
    iget v0, p2, Lcom/twitter/library/card/property/Vector2F;->y:F

    invoke-direct {p0, v1, v2, v0}, Lcom/twitter/library/card/element/Container;->a(IIF)F

    move-result v0

    goto :goto_1

    :cond_0
    iget v0, p0, Lcom/twitter/library/card/element/Container;->layoutMode:I

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_2
    iget v0, p2, Lcom/twitter/library/card/property/Vector2F;->x:F

    invoke-direct {p0, v2, v1, v0}, Lcom/twitter/library/card/element/Container;->a(IIF)F

    move-result v0

    goto :goto_1

    :pswitch_3
    iget v0, p2, Lcom/twitter/library/card/property/Vector2F;->x:F

    invoke-direct {p0, v2, v2, v0}, Lcom/twitter/library/card/element/Container;->a(IIF)F

    move-result v0

    goto :goto_1

    :pswitch_4
    iget v0, p2, Lcom/twitter/library/card/property/Vector2F;->x:F

    invoke-direct {p0, v0}, Lcom/twitter/library/card/element/Container;->a(F)F

    move-result v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public a(JLcom/twitter/library/util/aa;Lcom/twitter/library/util/as;)V
    .locals 2

    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/library/card/element/Box;->a(JLcom/twitter/library/util/aa;Lcom/twitter/library/util/as;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/element/Element;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/twitter/library/card/element/Element;->a(JLcom/twitter/library/util/aa;Lcom/twitter/library/util/as;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected a(Landroid/content/Context;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Box;->a(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/element/Element;

    invoke-virtual {v0, p1}, Lcom/twitter/library/card/element/Element;->a(Landroid/content/Context;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected a(Lcom/twitter/library/card/CardView;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Box;->a(Lcom/twitter/library/card/CardView;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/element/Element;

    invoke-virtual {v0, p1}, Lcom/twitter/library/card/element/Element;->a(Lcom/twitter/library/card/CardView;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/card/element/Element;)V
    .locals 1

    if-ne p1, p0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p0, p1, Lcom/twitter/library/card/element/Element;->mParent:Lcom/twitter/library/card/element/Container;

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/card/element/c;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Box;->a(Lcom/twitter/library/card/element/c;)V

    invoke-virtual {p1}, Lcom/twitter/library/card/element/c;->a()V

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/element/Element;

    invoke-virtual {v0, p1}, Lcom/twitter/library/card/element/Element;->a(Lcom/twitter/library/card/element/c;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/twitter/library/card/element/c;->b()V

    return-void
.end method

.method public a(Lcom/twitter/library/card/property/Vector2F;Lcom/twitter/library/card/property/Vector2F;)V
    .locals 4

    invoke-super {p0, p1, p2}, Lcom/twitter/library/card/element/Box;->a(Lcom/twitter/library/card/property/Vector2F;Lcom/twitter/library/card/property/Vector2F;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/element/Element;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/Element;->I()Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p2, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget v3, p2, Lcom/twitter/library/card/property/Vector2F;->y:F

    invoke-direct {p0, v0, v2, v3}, Lcom/twitter/library/card/element/Container;->a(Lcom/twitter/library/card/element/Element;FF)V

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/twitter/library/card/element/Container;->layoutMode:I

    packed-switch v0, :pswitch_data_0

    :goto_1
    return-void

    :pswitch_0
    iget v0, p2, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget v1, p2, Lcom/twitter/library/card/property/Vector2F;->y:F

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/library/card/element/Container;->a(FFI)V

    goto :goto_1

    :pswitch_1
    iget v0, p2, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget v1, p2, Lcom/twitter/library/card/property/Vector2F;->y:F

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/library/card/element/Container;->a(FFI)V

    goto :goto_1

    :pswitch_2
    iget v0, p2, Lcom/twitter/library/card/property/Vector2F;->x:F

    invoke-direct {p0, v0}, Lcom/twitter/library/card/element/Container;->b(F)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a([I)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Box;->a([I)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/element/Element;

    invoke-virtual {v0, p1}, Lcom/twitter/library/card/element/Element;->a([I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public b(Landroid/content/Context;)V
    .locals 1

    new-instance v0, Lcom/twitter/library/card/element/ContainerElementView;

    invoke-direct {v0, p1, p0}, Lcom/twitter/library/card/element/ContainerElementView;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/Container;)V

    iput-object v0, p0, Lcom/twitter/library/card/element/Container;->mView:Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/twitter/library/card/element/Container;->c(Landroid/content/Context;)V

    return-void
.end method

.method protected c(Landroid/content/Context;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/element/Element;

    invoke-virtual {v0, p1}, Lcom/twitter/library/card/element/Element;->b(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/twitter/library/card/element/Container;->mView:Landroid/view/View;

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/Element;->A()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public c(Z)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Box;->c(Z)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/element/Element;

    invoke-virtual {v0, p1}, Lcom/twitter/library/card/element/Element;->c(Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public d(Lcom/twitter/library/card/Card;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Box;->d(Lcom/twitter/library/card/Card;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/element/Element;

    invoke-virtual {v0, p1}, Lcom/twitter/library/card/element/Element;->d(Lcom/twitter/library/card/Card;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/library/card/element/Container;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Box;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/twitter/library/card/element/Container;

    iget v2, p1, Lcom/twitter/library/card/element/Container;->cornerRadius:F

    iget v3, p0, Lcom/twitter/library/card/element/Container;->cornerRadius:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget v2, p0, Lcom/twitter/library/card/element/Container;->layoutMode:I

    iget v3, p1, Lcom/twitter/library/card/element/Container;->layoutMode:I

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget v2, p0, Lcom/twitter/library/card/element/Container;->overflowMode:I

    iget v3, p1, Lcom/twitter/library/card/element/Container;->overflowMode:I

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget v2, p0, Lcom/twitter/library/card/element/Container;->visibleChildIndex:I

    iget v3, p1, Lcom/twitter/library/card/element/Container;->visibleChildIndex:I

    if-eq v2, v3, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p0, Lcom/twitter/library/card/element/Container;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/twitter/library/card/element/Container;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    iget-object v3, p1, Lcom/twitter/library/card/element/Container;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/Vector2;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p1, Lcom/twitter/library/card/element/Container;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    if-nez v2, :cond_8

    :cond_a
    iget-object v2, p0, Lcom/twitter/library/card/element/Container;->background:Lcom/twitter/library/card/property/Fill;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/twitter/library/card/element/Container;->background:Lcom/twitter/library/card/property/Fill;

    iget-object v3, p1, Lcom/twitter/library/card/element/Container;->background:Lcom/twitter/library/card/property/Fill;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/Fill;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    goto :goto_0

    :cond_c
    iget-object v2, p1, Lcom/twitter/library/card/element/Container;->background:Lcom/twitter/library/card/property/Fill;

    if-nez v2, :cond_b

    :cond_d
    iget-object v2, p0, Lcom/twitter/library/card/element/Container;->border:Lcom/twitter/library/card/property/Border;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/twitter/library/card/element/Container;->border:Lcom/twitter/library/card/property/Border;

    iget-object v3, p1, Lcom/twitter/library/card/element/Container;->border:Lcom/twitter/library/card/property/Border;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/Border;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    :cond_e
    move v0, v1

    goto :goto_0

    :cond_f
    iget-object v2, p1, Lcom/twitter/library/card/element/Container;->border:Lcom/twitter/library/card/property/Border;

    if-nez v2, :cond_e

    :cond_10
    iget-object v2, p0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    if-eqz v2, :cond_12

    iget-object v2, p0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    iget-object v3, p1, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    :cond_11
    move v0, v1

    goto :goto_0

    :cond_12
    iget-object v2, p1, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    if-nez v2, :cond_11

    :cond_13
    iget-object v2, p0, Lcom/twitter/library/card/element/Container;->padding:Lcom/twitter/library/card/property/Spacing;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lcom/twitter/library/card/element/Container;->padding:Lcom/twitter/library/card/property/Spacing;

    iget-object v3, p1, Lcom/twitter/library/card/element/Container;->padding:Lcom/twitter/library/card/property/Spacing;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/Spacing;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    goto/16 :goto_0

    :cond_14
    iget-object v2, p1, Lcom/twitter/library/card/element/Container;->padding:Lcom/twitter/library/card/property/Spacing;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 4

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/twitter/library/card/element/Box;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/twitter/library/card/element/Container;->layoutMode:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->background:Lcom/twitter/library/card/property/Fill;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->background:Lcom/twitter/library/card/property/Fill;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/Fill;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->border:Lcom/twitter/library/card/property/Border;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->border:Lcom/twitter/library/card/property/Border;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/Border;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/Vector2;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->padding:Lcom/twitter/library/card/property/Spacing;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->padding:Lcom/twitter/library/card/property/Spacing;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/Spacing;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/twitter/library/card/element/Container;->overflowMode:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget v0, p0, Lcom/twitter/library/card/element/Container;->cornerRadius:F

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/twitter/library/card/element/Container;->cornerRadius:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/library/card/element/Container;->visibleChildIndex:I

    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_4
.end method

.method protected p()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/library/card/element/Box;->p()V

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v1, p0, Lcom/twitter/library/card/element/Container;->padding:Lcom/twitter/library/card/property/Spacing;

    iput-object v1, v0, Lcom/twitter/library/card/property/Style;->padding:Lcom/twitter/library/card/property/Spacing;

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget v1, p0, Lcom/twitter/library/card/element/Container;->visibleChildIndex:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/card/property/Style;->visibleChildIndex:Ljava/lang/Integer;

    return-void
.end method

.method public r()V
    .locals 7

    const/4 v4, 0x1

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/twitter/library/card/element/Box;->r()V

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/element/Element;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/Element;->r()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v0, v0, Lcom/twitter/library/card/property/Style;->visibleChildIndex:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v0, -0x1

    if-ne v5, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/element/Element;

    invoke-virtual {v0, v4}, Lcom/twitter/library/card/element/Element;->f(Z)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/element/Element;

    if-ne v1, v5, :cond_2

    move v3, v4

    :goto_3
    invoke-virtual {v0, v3}, Lcom/twitter/library/card/element/Element;->f(Z)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_2
    move v3, v2

    goto :goto_3

    :cond_3
    return-void
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Box;->readExternal(Ljava/io/ObjectInput;)V

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/element/Container;->layoutMode:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Vector2;

    iput-object v0, p0, Lcom/twitter/library/card/element/Container;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/element/Container;->overflowMode:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Spacing;

    iput-object v0, p0, Lcom/twitter/library/card/element/Container;->padding:Lcom/twitter/library/card/property/Spacing;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/element/Element;

    invoke-virtual {p0, v0}, Lcom/twitter/library/card/element/Container;->a(Lcom/twitter/library/card/element/Element;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/element/Container;->visibleChildIndex:I

    return-void
.end method

.method public s()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/library/card/element/Box;->s()V

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/element/Element;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/Element;->s()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public t()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/library/card/element/Box;->t()V

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/element/Element;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/Element;->t()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public u()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/library/card/element/Box;->u()V

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/element/Element;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/Element;->u()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected v()I
    .locals 4

    const/4 v0, 0x3

    const/4 v1, 0x1

    const/4 v3, 0x5

    iget-object v2, p0, Lcom/twitter/library/card/element/Container;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v2, v2, Lcom/twitter/library/card/property/Vector2;->y:I

    if-ne v2, v1, :cond_3

    iget-object v2, p0, Lcom/twitter/library/card/element/Container;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v2, v2, Lcom/twitter/library/card/property/Vector2;->x:I

    if-ne v2, v0, :cond_3

    iget-object v2, p0, Lcom/twitter/library/card/element/Container;->maxSizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v2, v2, Lcom/twitter/library/card/property/Vector2;->x:I

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lcom/twitter/library/card/element/Container;->maxSizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v2, v2, Lcom/twitter/library/card/property/Vector2;->y:I

    if-eq v2, v3, :cond_1

    const/4 v0, 0x4

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/twitter/library/card/element/Container;->maxSizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v2, v2, Lcom/twitter/library/card/property/Vector2;->x:I

    if-ne v2, v3, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->maxSizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v0, v0, Lcom/twitter/library/card/property/Vector2;->y:I

    if-eq v0, v3, :cond_2

    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    invoke-super {p0}, Lcom/twitter/library/card/element/Box;->v()I

    move-result v0

    goto :goto_0
.end method

.method public w()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/library/card/element/Box;->w()V

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/element/Element;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/Element;->w()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Box;->writeExternal(Ljava/io/ObjectOutput;)V

    iget v0, p0, Lcom/twitter/library/card/element/Container;->layoutMode:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget v0, p0, Lcom/twitter/library/card/element/Container;->overflowMode:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->padding:Lcom/twitter/library/card/property/Spacing;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/element/Element;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/twitter/library/card/element/Container;->visibleChildIndex:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    return-void
.end method

.method public x()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/element/Element;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/Element;->x()V

    goto :goto_0

    :cond_0
    invoke-super {p0}, Lcom/twitter/library/card/element/Box;->x()V

    return-void
.end method

.method public y()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/library/card/element/Box;->y()V

    iget-object v0, p0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/element/Element;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/Element;->y()V

    goto :goto_0

    :cond_0
    return-void
.end method
