.class public Lcom/twitter/library/card/element/ContainerElementView;
.super Lcom/twitter/library/card/element/ElementViewGroup;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/card/element/Container;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/card/element/ElementViewGroup;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/Element;)V

    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 7

    iget-object v0, p0, Lcom/twitter/library/card/element/ContainerElementView;->b:Lcom/twitter/library/card/element/Element;

    check-cast v0, Lcom/twitter/library/card/element/Container;

    iget-object v0, v0, Lcom/twitter/library/card/element/Container;->children:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/element/Element;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/Element;->G()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/twitter/library/card/element/Element;->mLayoutPosition:Lcom/twitter/library/card/property/Vector2F;

    iget v2, v2, Lcom/twitter/library/card/property/Vector2F;->x:F

    float-to-int v2, v2

    iget-object v3, v0, Lcom/twitter/library/card/element/Element;->mLayoutPosition:Lcom/twitter/library/card/property/Vector2F;

    iget v3, v3, Lcom/twitter/library/card/property/Vector2F;->y:F

    float-to-int v3, v3

    iget-object v4, v0, Lcom/twitter/library/card/element/Element;->mLayoutPosition:Lcom/twitter/library/card/property/Vector2F;

    iget v4, v4, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget-object v5, v0, Lcom/twitter/library/card/element/Element;->mLayoutSize:Lcom/twitter/library/card/property/Vector2F;

    iget v5, v5, Lcom/twitter/library/card/property/Vector2F;->x:F

    add-float/2addr v4, v5

    float-to-int v4, v4

    iget-object v5, v0, Lcom/twitter/library/card/element/Element;->mLayoutPosition:Lcom/twitter/library/card/property/Vector2F;

    iget v5, v5, Lcom/twitter/library/card/property/Vector2F;->y:F

    iget-object v6, v0, Lcom/twitter/library/card/element/Element;->mLayoutSize:Lcom/twitter/library/card/property/Vector2F;

    iget v6, v6, Lcom/twitter/library/card/property/Vector2F;->y:F

    add-float/2addr v5, v6

    float-to-int v5, v5

    invoke-virtual {v0}, Lcom/twitter/library/card/element/Element;->A()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/view/View;->layout(IIII)V

    goto :goto_0

    :cond_1
    return-void
.end method
