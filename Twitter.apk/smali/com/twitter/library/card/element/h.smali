.class public Lcom/twitter/library/card/element/h;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field protected a:Landroid/content/Context;

.field protected b:Lcom/twitter/library/card/element/Player;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/card/element/Player;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/card/element/h;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/twitter/library/card/element/h;->b:Lcom/twitter/library/card/element/Player;

    return-void
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 3

    new-instance v0, Lcom/twitter/library/card/element/ElementView;

    iget-object v1, p0, Lcom/twitter/library/card/element/h;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/library/card/element/h;->b:Lcom/twitter/library/card/element/Player;

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/card/element/ElementView;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/Element;)V

    return-object v0
.end method

.method public a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/RectF;)V
    .locals 0

    return-void
.end method

.method protected a(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/h;->b:Lcom/twitter/library/card/element/Player;

    invoke-virtual {v0, p1}, Lcom/twitter/library/card/element/Player;->a(Landroid/view/View;)V

    return-void
.end method

.method public a(Lcom/twitter/library/util/at;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public a(Z)V
    .locals 0

    return-void
.end method

.method public a(JLcom/twitter/library/util/aa;Lcom/twitter/library/util/as;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public b()Z
    .locals 6

    const/4 v0, 0x0

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/twitter/library/card/element/h;->b:Lcom/twitter/library/card/element/Player;

    iget-object v1, v1, Lcom/twitter/library/card/element/Player;->streamUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/twitter/library/card/element/h;->b:Lcom/twitter/library/card/element/Player;

    iget-object v1, v1, Lcom/twitter/library/card/element/Player;->streamContentType:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/twitter/library/card/element/h;->b:Lcom/twitter/library/card/element/Player;

    iget-object v1, v1, Lcom/twitter/library/card/element/Player;->streamContentType:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/library/util/Util;->e(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/library/card/element/h;->b:Lcom/twitter/library/card/element/Player;

    iget-object v2, v1, Lcom/twitter/library/card/element/Player;->streamUrl:Ljava/lang/String;

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lcom/twitter/library/card/element/h;->b:Lcom/twitter/library/card/element/Player;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/Player;->d()Lcom/twitter/library/card/Card;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/twitter/library/card/element/h;->b:Lcom/twitter/library/card/element/Player;

    iget-object v3, v3, Lcom/twitter/library/card/element/Player;->spec:Lcom/twitter/library/card/property/ImageSpec;

    iget-object v3, v3, Lcom/twitter/library/card/property/ImageSpec;->url:Ljava/lang/String;

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/card/Card;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    :cond_0
    const/4 v0, 0x1

    return v0

    :cond_1
    iget-object v1, p0, Lcom/twitter/library/card/element/h;->b:Lcom/twitter/library/card/element/Player;

    iget-object v1, v1, Lcom/twitter/library/card/element/Player;->htmlUrl:Ljava/lang/String;

    move-object v2, v0

    goto :goto_0
.end method

.method public c()V
    .locals 0

    return-void
.end method

.method public d()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public e()V
    .locals 0

    return-void
.end method

.method public f()V
    .locals 0

    return-void
.end method

.method public g()V
    .locals 0

    return-void
.end method

.method public h()V
    .locals 0

    return-void
.end method
