.class public abstract Lcom/twitter/library/card/element/ElementViewGroup;
.super Landroid/view/ViewGroup;
.source "Twttr"


# instance fields
.field protected b:Lcom/twitter/library/card/element/Element;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/card/element/Element;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/twitter/library/card/element/ElementViewGroup;->b:Lcom/twitter/library/card/element/Element;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/library/card/element/ElementViewGroup;->setWillNotDraw(Z)V

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/ElementViewGroup;->b:Lcom/twitter/library/card/element/Element;

    iget v0, v0, Lcom/twitter/library/card/element/Element;->opacity:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/element/ElementViewGroup;->b:Lcom/twitter/library/card/element/Element;

    invoke-virtual {v0, p1}, Lcom/twitter/library/card/element/Element;->a(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/ElementViewGroup;->b:Lcom/twitter/library/card/element/Element;

    iget v0, v0, Lcom/twitter/library/card/element/Element;->opacity:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/element/ElementViewGroup;->b:Lcom/twitter/library/card/element/Element;

    invoke-virtual {v0, p1}, Lcom/twitter/library/card/element/Element;->b(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/card/element/ElementViewGroup;->b:Lcom/twitter/library/card/element/Element;

    iget-object v0, v0, Lcom/twitter/library/card/element/Element;->mLayoutSize:Lcom/twitter/library/card/property/Vector2F;

    iget v0, v0, Lcom/twitter/library/card/property/Vector2F;->x:F

    float-to-int v0, v0

    iget-object v1, p0, Lcom/twitter/library/card/element/ElementViewGroup;->b:Lcom/twitter/library/card/element/Element;

    iget-object v1, v1, Lcom/twitter/library/card/element/Element;->mLayoutSize:Lcom/twitter/library/card/property/Vector2F;

    iget v1, v1, Lcom/twitter/library/card/property/Vector2F;->y:F

    float-to-int v1, v1

    iget-object v2, p0, Lcom/twitter/library/card/element/ElementViewGroup;->b:Lcom/twitter/library/card/element/Element;

    invoke-virtual {v2}, Lcom/twitter/library/card/element/Element;->q()V

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/card/element/ElementViewGroup;->setMeasuredDimension(II)V

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/card/element/ElementViewGroup;->measureChildren(II)V

    return-void
.end method
