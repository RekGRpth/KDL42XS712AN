.class public Lcom/twitter/library/card/element/FormTextView;
.super Lcom/twitter/library/card/element/FormFieldElementView;
.source "Twttr"


# instance fields
.field private c:Landroid/widget/EditText;

.field private d:Landroid/graphics/Typeface;

.field private e:Lcom/twitter/library/card/element/FormText;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/card/element/FormText;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/card/element/FormFieldElementView;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/FormFieldElement;)V

    iput-object p2, p0, Lcom/twitter/library/card/element/FormTextView;->e:Lcom/twitter/library/card/element/FormText;

    return-void
.end method

.method private a(Landroid/widget/TextView;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/card/element/FormTextView;->d:Landroid/graphics/Typeface;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/element/FormTextView;->e:Lcom/twitter/library/card/element/FormText;

    iget-object v0, v0, Lcom/twitter/library/card/element/FormText;->fontName:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/library/card/element/FormTextView;->e:Lcom/twitter/library/card/element/FormText;

    iget-boolean v1, v1, Lcom/twitter/library/card/element/FormText;->fontBold:Z

    iget-object v2, p0, Lcom/twitter/library/card/element/FormTextView;->e:Lcom/twitter/library/card/element/FormText;

    iget-boolean v2, v2, Lcom/twitter/library/card/element/FormText;->fontItalic:Z

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/library/card/element/FormTextView;->a(Ljava/lang/String;ZZ)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/card/element/FormTextView;->d:Landroid/graphics/Typeface;

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/card/element/FormTextView;->d:Landroid/graphics/Typeface;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/FormTextView;->e:Lcom/twitter/library/card/element/FormText;

    iget-boolean v0, v0, Lcom/twitter/library/card/element/FormText;->fontUnderline:Z

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v0

    or-int/lit8 v0, v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setPaintFlags(I)V

    :goto_0
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/library/card/element/FormTextView;->e:Lcom/twitter/library/card/element/FormText;

    iget v1, v1, Lcom/twitter/library/card/element/FormText;->fontSize:F

    invoke-virtual {p1, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v0, p0, Lcom/twitter/library/card/element/FormTextView;->e:Lcom/twitter/library/card/element/FormText;

    iget v0, v0, Lcom/twitter/library/card/element/FormText;->color:I

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v0

    and-int/lit8 v0, v0, -0x9

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setPaintFlags(I)V

    goto :goto_0
.end method

.method private setUpConstrain(Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x1

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/element/FormTextView;->c:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v0, "email"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/card/element/FormTextView;->c:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "numeric"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/card/element/FormTextView;->c:Landroid/widget/EditText;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "phone"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/card/element/FormTextView;->c:Landroid/widget/EditText;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/library/card/element/FormTextView;->c:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    goto :goto_0
.end method


# virtual methods
.method protected a(Landroid/content/Context;Lcom/twitter/library/card/element/FormFieldElement;)Landroid/view/View;
    .locals 4

    move-object v0, p2

    check-cast v0, Lcom/twitter/library/card/element/FormText;

    new-instance v1, Landroid/widget/EditText;

    invoke-direct {v1, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/twitter/library/card/element/FormTextView;->c:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/twitter/library/card/element/FormTextView;->c:Landroid/widget/EditText;

    iget v2, v0, Lcom/twitter/library/card/element/FormText;->maxLength:I

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setEms(I)V

    iget-object v1, p0, Lcom/twitter/library/card/element/FormTextView;->c:Landroid/widget/EditText;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSingleLine(Z)V

    iget-object v1, p0, Lcom/twitter/library/card/element/FormTextView;->c:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHorizontallyScrolling(Z)V

    iget-object v1, p0, Lcom/twitter/library/card/element/FormTextView;->c:Landroid/widget/EditText;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setImeOptions(I)V

    check-cast p2, Lcom/twitter/library/card/element/FormText;

    iget-object v1, p2, Lcom/twitter/library/card/element/FormText;->inputMode:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/twitter/library/card/element/FormTextView;->setUpConstrain(Ljava/lang/String;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    iget v3, v0, Lcom/twitter/library/card/element/FormText;->maxLength:I

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, v0, Lcom/twitter/library/card/element/FormText;->charset:Ljava/lang/String;

    if-eqz v2, :cond_0

    new-instance v2, Lcom/twitter/library/card/element/f;

    iget-object v0, v0, Lcom/twitter/library/card/element/FormText;->charset:Ljava/lang/String;

    invoke-direct {v2, p0, v0}, Lcom/twitter/library/card/element/f;-><init>(Lcom/twitter/library/card/element/FormTextView;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Landroid/text/InputFilter;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/InputFilter;

    iget-object v1, p0, Lcom/twitter/library/card/element/FormTextView;->c:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/FormTextView;->c:Landroid/widget/EditText;

    return-object v0
.end method

.method public a(Lcom/twitter/library/card/Card;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/card/element/FormTextView;->c:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/twitter/library/card/element/FormTextView;->a(Landroid/widget/TextView;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/FormTextView;->e:Lcom/twitter/library/card/element/FormText;

    iget v0, v0, Lcom/twitter/library/card/element/FormText;->placeHolderTextId:I

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/element/FormTextView;->c:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/twitter/library/card/element/FormTextView;->e:Lcom/twitter/library/card/element/FormText;

    iget v1, v1, Lcom/twitter/library/card/element/FormText;->placeHolderTextId:I

    invoke-virtual {p0, v1, p1}, Lcom/twitter/library/card/element/FormTextView;->a(ILcom/twitter/library/card/Card;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/library/card/element/FormFieldElementView;->a(Lcom/twitter/library/card/Card;)V

    return-void
.end method

.method public getEditText()Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/FormTextView;->c:Landroid/widget/EditText;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/FormTextView;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    const/4 v3, 0x0

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    sub-int v0, p4, p2

    sub-int v1, p5, p3

    iget-object v2, p0, Lcom/twitter/library/card/element/FormTextView;->c:Landroid/widget/EditText;

    invoke-virtual {v2, v3, v3, v0, v1}, Landroid/widget/EditText;->layout(IIII)V

    goto :goto_0
.end method
