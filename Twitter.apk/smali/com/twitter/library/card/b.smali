.class Lcom/twitter/library/card/b;
.super Lcom/twitter/library/card/element/c;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/library/card/element/Element;

.field final synthetic b:[I

.field final synthetic c:Ljava/util/ArrayList;

.field final synthetic d:Lcom/twitter/library/card/Card;


# direct methods
.method constructor <init>(Lcom/twitter/library/card/Card;Lcom/twitter/library/card/element/Element;[ILjava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/card/b;->d:Lcom/twitter/library/card/Card;

    iput-object p2, p0, Lcom/twitter/library/card/b;->a:Lcom/twitter/library/card/element/Element;

    iput-object p3, p0, Lcom/twitter/library/card/b;->b:[I

    iput-object p4, p0, Lcom/twitter/library/card/b;->c:Ljava/util/ArrayList;

    invoke-direct {p0}, Lcom/twitter/library/card/element/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/card/element/Element;)V
    .locals 4

    invoke-virtual {p1}, Lcom/twitter/library/card/element/Element;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    instance-of v0, p1, Lcom/twitter/library/card/element/Image;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/twitter/library/card/element/Image;

    invoke-virtual {p1}, Lcom/twitter/library/card/element/Element;->E()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/library/card/b;->a:Lcom/twitter/library/card/element/Element;

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/card/b;->b:[I

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/library/card/b;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    aput v3, v1, v2

    :cond_0
    iget-object v1, p0, Lcom/twitter/library/card/b;->c:Ljava/util/ArrayList;

    iget-object v2, v0, Lcom/twitter/library/card/element/Image;->specFullSize:Lcom/twitter/library/card/property/ImageSpec;

    if-eqz v2, :cond_2

    iget-object v0, v0, Lcom/twitter/library/card/element/Image;->specFullSize:Lcom/twitter/library/card/property/ImageSpec;

    :goto_0
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void

    :cond_2
    iget-object v0, v0, Lcom/twitter/library/card/element/Image;->spec:Lcom/twitter/library/card/property/ImageSpec;

    goto :goto_0
.end method
