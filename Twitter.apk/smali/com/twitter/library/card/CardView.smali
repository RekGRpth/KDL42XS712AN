.class public Lcom/twitter/library/card/CardView;
.super Lcom/twitter/library/card/element/ContainerElementView;
.source "Twttr"


# instance fields
.field public a:Lcom/twitter/library/card/o;

.field private c:I

.field private d:Lcom/twitter/library/card/property/Vector2F;

.field private e:Lcom/twitter/library/card/property/Vector2F;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget v0, Lib;->cardViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/library/card/CardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/card/element/ContainerElementView;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/Container;)V

    new-instance v0, Lcom/twitter/library/card/property/Vector2F;

    invoke-direct {v0}, Lcom/twitter/library/card/property/Vector2F;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/card/CardView;->d:Lcom/twitter/library/card/property/Vector2F;

    new-instance v0, Lcom/twitter/library/card/property/Vector2F;

    invoke-direct {v0}, Lcom/twitter/library/card/property/Vector2F;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/card/CardView;->e:Lcom/twitter/library/card/property/Vector2F;

    new-instance v0, Lcom/twitter/library/card/o;

    invoke-direct {v0, p1, p2}, Lcom/twitter/library/card/o;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/twitter/library/card/CardView;->a:Lcom/twitter/library/card/o;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/library/card/CardView;->setWillNotDraw(Z)V

    return-void
.end method

.method private static a(IFF)F
    .locals 4

    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    if-ne v2, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-static {p1, p2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    const/high16 v3, -0x80000000

    if-ne v2, v3, :cond_1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    iget v0, p0, Lcom/twitter/library/card/CardView;->c:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/library/card/CardView;->c:I

    invoke-virtual {p0}, Lcom/twitter/library/card/CardView;->refreshDrawableState()V

    return-void
.end method

.method public b()V
    .locals 1

    iget v0, p0, Lcom/twitter/library/card/CardView;->c:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/twitter/library/card/CardView;->c:I

    invoke-virtual {p0}, Lcom/twitter/library/card/CardView;->refreshDrawableState()V

    return-void
.end method

.method protected drawableStateChanged()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/library/card/element/ContainerElementView;->drawableStateChanged()V

    iget-object v0, p0, Lcom/twitter/library/card/CardView;->b:Lcom/twitter/library/card/element/Element;

    check-cast v0, Lcom/twitter/library/card/Card;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/card/CardView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/card/Card;->a([I)V

    :cond_0
    return-void
.end method

.method public getCard()Lcom/twitter/library/card/Card;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/CardView;->b:Lcom/twitter/library/card/element/Element;

    check-cast v0, Lcom/twitter/library/card/Card;

    return-object v0
.end method

.method protected onCreateDrawableState(I)[I
    .locals 2

    sget-object v0, Lcom/twitter/library/card/CardView;->PRESSED_ENABLED_STATE_SET:[I

    array-length v0, v0

    add-int/2addr v0, p1

    invoke-super {p0, v0}, Lcom/twitter/library/card/element/ContainerElementView;->onCreateDrawableState(I)[I

    move-result-object v0

    iget v1, p0, Lcom/twitter/library/card/CardView;->c:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/twitter/library/card/CardView;->PRESSED_ENABLED_STATE_SET:[I

    invoke-static {v0, v1}, Lcom/twitter/library/card/CardView;->mergeDrawableStates([I[I)[I

    :cond_0
    return-object v0
.end method

.method protected onMeasure(II)V
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/library/card/CardView;->getSuggestedMinimumWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/twitter/library/card/CardView;->b:Lcom/twitter/library/card/element/Element;

    iget-object v1, v1, Lcom/twitter/library/card/element/Element;->size:Lcom/twitter/library/card/property/Vector2F;

    iget v1, v1, Lcom/twitter/library/card/property/Vector2F;->x:F

    invoke-static {p1, v0, v1}, Lcom/twitter/library/card/CardView;->a(IFF)F

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/library/card/CardView;->getSuggestedMinimumHeight()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/twitter/library/card/CardView;->b:Lcom/twitter/library/card/element/Element;

    iget-object v2, v2, Lcom/twitter/library/card/element/Element;->size:Lcom/twitter/library/card/property/Vector2F;

    iget v2, v2, Lcom/twitter/library/card/property/Vector2F;->y:F

    invoke-static {p2, v1, v2}, Lcom/twitter/library/card/CardView;->a(IFF)F

    move-result v1

    iget-object v2, p0, Lcom/twitter/library/card/CardView;->d:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/library/card/property/Vector2F;->set(FF)V

    iget-object v0, p0, Lcom/twitter/library/card/CardView;->e:Lcom/twitter/library/card/property/Vector2F;

    iget-object v1, p0, Lcom/twitter/library/card/CardView;->b:Lcom/twitter/library/card/element/Element;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/library/card/CardView;->d:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/card/element/Element;->b(ILcom/twitter/library/card/property/Vector2F;)F

    move-result v1

    iput v1, v0, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget-object v0, p0, Lcom/twitter/library/card/CardView;->e:Lcom/twitter/library/card/property/Vector2F;

    iget-object v1, p0, Lcom/twitter/library/card/CardView;->b:Lcom/twitter/library/card/element/Element;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/twitter/library/card/CardView;->d:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/card/element/Element;->b(ILcom/twitter/library/card/property/Vector2F;)F

    move-result v1

    iput v1, v0, Lcom/twitter/library/card/property/Vector2F;->y:F

    iget-object v0, p0, Lcom/twitter/library/card/CardView;->b:Lcom/twitter/library/card/element/Element;

    sget-object v1, Lcom/twitter/library/card/property/Vector2F;->a:Lcom/twitter/library/card/property/Vector2F;

    iget-object v2, p0, Lcom/twitter/library/card/CardView;->e:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/card/element/Element;->a(Lcom/twitter/library/card/property/Vector2F;Lcom/twitter/library/card/property/Vector2F;)V

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/card/CardView;->measureChildren(II)V

    iget-object v0, p0, Lcom/twitter/library/card/CardView;->b:Lcom/twitter/library/card/element/Element;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/Element;->q()V

    iget-object v0, p0, Lcom/twitter/library/card/CardView;->b:Lcom/twitter/library/card/element/Element;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/Element;->B()Lcom/twitter/library/card/property/Vector2F;

    move-result-object v0

    iget v1, v0, Lcom/twitter/library/card/property/Vector2F;->x:F

    float-to-int v1, v1

    iget v0, v0, Lcom/twitter/library/card/property/Vector2F;->y:F

    float-to-int v0, v0

    invoke-virtual {p0, v1, v0}, Lcom/twitter/library/card/CardView;->setMeasuredDimension(II)V

    return-void
.end method

.method public setCard(Lcom/twitter/library/card/Card;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/card/CardView;->b:Lcom/twitter/library/card/element/Element;

    return-void
.end method
