.class public abstract Lcom/twitter/library/card/l;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method protected static A(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/element/FormText;
    .locals 4

    new-instance v2, Lcom/twitter/library/card/element/FormText;

    invoke-direct {v2}, Lcom/twitter/library/card/element/FormText;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_9

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_9

    sget-object v3, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_0
    const-string/jumbo v1, "form_field"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0}, Lcom/twitter/library/card/l;->w(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/FormField;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/element/FormText;->field:Lcom/twitter/library/card/property/FormField;

    goto :goto_1

    :cond_1
    const-string/jumbo v1, "color"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p0}, Lcom/twitter/library/card/l;->F(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/element/FormText;->color:I

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    const-string/jumbo v1, "placeholder_tokenized_text_id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/element/FormText;->placeHolderTextId:I

    goto :goto_1

    :cond_3
    const-string/jumbo v1, "max_length"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/element/FormText;->maxLength:I

    goto :goto_1

    :cond_4
    const-string/jumbo v1, "font_size"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/twitter/library/card/l;->U(Lcom/fasterxml/jackson/core/JsonParser;)F

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/element/FormText;->fontSize:F

    goto :goto_1

    :pswitch_3
    const-string/jumbo v1, "charset"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/element/FormText;->charset:Ljava/lang/String;

    goto :goto_1

    :cond_5
    const-string/jumbo v1, "input_mode"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/element/FormText;->inputMode:Ljava/lang/String;

    goto :goto_1

    :cond_6
    const-string/jumbo v1, "font_name"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/element/FormText;->fontName:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_4
    const-string/jumbo v1, "font_bold"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->k()Z

    move-result v1

    iput-boolean v1, v2, Lcom/twitter/library/card/element/FormText;->fontBold:Z

    goto/16 :goto_1

    :cond_7
    const-string/jumbo v1, "font_underline"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->k()Z

    move-result v1

    iput-boolean v1, v2, Lcom/twitter/library/card/element/FormText;->fontUnderline:Z

    goto/16 :goto_1

    :cond_8
    const-string/jumbo v1, "font_italic"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->k()Z

    move-result v1

    iput-boolean v1, v2, Lcom/twitter/library/card/element/FormText;->fontItalic:Z

    goto/16 :goto_1

    :pswitch_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_9
    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_5
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method protected static B(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/element/FollowButtonElement;
    .locals 4

    const/4 v0, 0x0

    new-instance v2, Lcom/twitter/library/card/element/FollowButtonElement;

    invoke-direct {v2}, Lcom/twitter/library/card/element/FollowButtonElement;-><init>()V

    iput-object v0, v2, Lcom/twitter/library/card/element/FollowButtonElement;->kind:Lcom/twitter/library/card/element/FollowButtonElement$Kind;

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_3

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_3

    sget-object v3, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "kind"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string/jumbo v3, "small"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v1, Lcom/twitter/library/card/element/FollowButtonElement$Kind;->a:Lcom/twitter/library/card/element/FollowButtonElement$Kind;

    iput-object v1, v2, Lcom/twitter/library/card/element/FollowButtonElement;->kind:Lcom/twitter/library/card/element/FollowButtonElement$Kind;

    goto :goto_1

    :cond_1
    const-string/jumbo v3, "large"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    sget-object v1, Lcom/twitter/library/card/element/FollowButtonElement$Kind;->b:Lcom/twitter/library/card/element/FollowButtonElement$Kind;

    iput-object v1, v2, Lcom/twitter/library/card/element/FollowButtonElement;->kind:Lcom/twitter/library/card/element/FollowButtonElement$Kind;

    goto :goto_1

    :cond_2
    new-instance v0, Lcom/twitter/library/card/CardParserException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unknown kind: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    iget-object v0, v2, Lcom/twitter/library/card/element/FollowButtonElement;->kind:Lcom/twitter/library/card/element/FollowButtonElement$Kind;

    if-nez v0, :cond_4

    new-instance v0, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v1, "invalid follow button"

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static C(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/ImageSpec;
    .locals 4

    const/4 v0, 0x0

    new-instance v2, Lcom/twitter/library/card/property/ImageSpec;

    invoke-direct {v2}, Lcom/twitter/library/card/property/ImageSpec;-><init>()V

    iput-object v0, v2, Lcom/twitter/library/card/property/ImageSpec;->url:Ljava/lang/String;

    invoke-static {}, Lcom/twitter/library/card/property/Vector2F;->b()Lcom/twitter/library/card/property/Vector2F;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/property/ImageSpec;->size:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_2

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_2

    sget-object v3, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_1
    const-string/jumbo v1, "url"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/twitter/library/card/l;->T(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/property/ImageSpec;->url:Ljava/lang/String;

    goto :goto_1

    :pswitch_2
    invoke-static {p0}, Lcom/twitter/library/card/l;->U(Lcom/fasterxml/jackson/core/JsonParser;)F

    move-result v1

    const-string/jumbo v3, "width"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, v2, Lcom/twitter/library/card/property/ImageSpec;->size:Lcom/twitter/library/card/property/Vector2F;

    iput v1, v3, Lcom/twitter/library/card/property/Vector2F;->x:F

    goto :goto_1

    :cond_1
    const-string/jumbo v3, "height"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v2, Lcom/twitter/library/card/property/ImageSpec;->size:Lcom/twitter/library/card/property/Vector2F;

    iput v1, v3, Lcom/twitter/library/card/property/Vector2F;->y:F

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget-object v0, v2, Lcom/twitter/library/card/property/ImageSpec;->url:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, v2, Lcom/twitter/library/card/property/ImageSpec;->size:Lcom/twitter/library/card/property/Vector2F;

    iget v0, v0, Lcom/twitter/library/card/property/Vector2F;->x:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, v2, Lcom/twitter/library/card/property/ImageSpec;->size:Lcom/twitter/library/card/property/Vector2F;

    iget v0, v0, Lcom/twitter/library/card/property/Vector2F;->y:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    new-instance v0, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v1, "invalid image spec"

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected static D(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/Fill;
    .locals 8

    const/4 v7, 0x3

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-instance v5, Lcom/twitter/library/card/property/Fill;

    invoke-direct {v5}, Lcom/twitter/library/card/property/Fill;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_8

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v4, :cond_8

    sget-object v4, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v4, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_0
    const-string/jumbo v1, "solid"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0}, Lcom/twitter/library/card/l;->F(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    invoke-virtual {v5, v1}, Lcom/twitter/library/card/property/Fill;->a(I)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_1
    const-string/jumbo v1, "gradient"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v4, v1

    :goto_2
    if-eqz v4, :cond_2

    move v1, v2

    :goto_3
    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v6, :cond_3

    move v4, v2

    :goto_4
    and-int/2addr v1, v4

    if-eqz v1, :cond_0

    invoke-static {p0, v5}, Lcom/twitter/library/card/l;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/card/property/Fill;)V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v4, v1

    goto :goto_2

    :cond_2
    move v1, v3

    goto :goto_3

    :cond_3
    move v4, v3

    goto :goto_4

    :cond_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v4, "type"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string/jumbo v4, "none"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    iput v2, v5, Lcom/twitter/library/card/property/Fill;->type:I

    goto :goto_1

    :cond_5
    const-string/jumbo v4, "solid"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v1, 0x2

    iput v1, v5, Lcom/twitter/library/card/property/Fill;->type:I

    goto :goto_1

    :cond_6
    const-string/jumbo v4, "gradient"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    iput v7, v5, Lcom/twitter/library/card/property/Fill;->type:I

    goto :goto_1

    :cond_7
    new-instance v0, Lcom/twitter/library/card/CardParserException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unknown fill type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_3
    const-string/jumbo v1, "angle"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->i()F

    move-result v1

    iput v1, v5, Lcom/twitter/library/card/property/Fill;->angle:F

    goto/16 :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_8
    iget v0, v5, Lcom/twitter/library/card/property/Fill;->type:I

    if-ne v0, v7, :cond_9

    invoke-virtual {v5}, Lcom/twitter/library/card/property/Fill;->a()I

    move-result v0

    if-nez v0, :cond_9

    new-instance v0, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v1, "invalid fill"

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    return-object v5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected static E(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/Shadow;
    .locals 7

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-instance v5, Lcom/twitter/library/card/property/Shadow;

    invoke-direct {v5}, Lcom/twitter/library/card/property/Shadow;-><init>()V

    iput v3, v5, Lcom/twitter/library/card/property/Shadow;->color:I

    const/high16 v1, 0x7fc00000    # NaNf

    iput v1, v5, Lcom/twitter/library/card/property/Shadow;->blurRadius:F

    iput-object v0, v5, Lcom/twitter/library/card/property/Shadow;->offset:Lcom/twitter/library/card/property/Vector2F;

    iput-boolean v3, v5, Lcom/twitter/library/card/property/Shadow;->inset:Z

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v4, v1

    move v1, v3

    :goto_0
    if-eqz v4, :cond_3

    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v6, :cond_3

    sget-object v6, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v4}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v4

    aget v4, v6, v4

    packed-switch v4, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    goto :goto_0

    :pswitch_1
    const-string/jumbo v4, "color"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {p0}, Lcom/twitter/library/card/l;->F(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v3

    iput v3, v5, Lcom/twitter/library/card/property/Shadow;->color:I

    move v3, v2

    goto :goto_1

    :cond_1
    const-string/jumbo v4, "offset"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {p0}, Lcom/twitter/library/card/l;->M(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/Vector2F;

    move-result-object v4

    iput-object v4, v5, Lcom/twitter/library/card/property/Shadow;->offset:Lcom/twitter/library/card/property/Vector2F;

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_3
    const-string/jumbo v4, "blur_radius"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {p0}, Lcom/twitter/library/card/l;->U(Lcom/fasterxml/jackson/core/JsonParser;)F

    move-result v4

    iput v4, v5, Lcom/twitter/library/card/property/Shadow;->blurRadius:F

    goto :goto_1

    :pswitch_4
    const-string/jumbo v4, "inset"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->k()Z

    move-result v1

    iput-boolean v1, v5, Lcom/twitter/library/card/property/Shadow;->inset:Z

    move v1, v2

    goto :goto_1

    :pswitch_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    if-eqz v3, :cond_4

    iget v0, v5, Lcom/twitter/library/card/property/Shadow;->blurRadius:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, v5, Lcom/twitter/library/card/property/Shadow;->offset:Lcom/twitter/library/card/property/Vector2F;

    if-eqz v0, :cond_4

    if-nez v1, :cond_5

    :cond_4
    new-instance v0, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v1, "invalid shadow"

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    return-object v5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method protected static F(Lcom/fasterxml/jackson/core/JsonParser;)I
    .locals 7

    const/4 v2, -0x1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    move-object v5, v1

    move v3, v2

    move v4, v2

    move v1, v2

    :goto_0
    if-eqz v5, :cond_4

    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v5, v6, :cond_4

    sget-object v6, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v5}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v5

    aget v5, v6, v5

    packed-switch v5, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v5

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    const-string/jumbo v5, "a"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-static {p0}, Lcom/twitter/library/card/l;->R(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v4

    goto :goto_1

    :cond_1
    const-string/jumbo v5, "r"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-static {p0}, Lcom/twitter/library/card/l;->R(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v3

    goto :goto_1

    :cond_2
    const-string/jumbo v5, "g"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-static {p0}, Lcom/twitter/library/card/l;->R(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v2

    goto :goto_1

    :cond_3
    const-string/jumbo v5, "b"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {p0}, Lcom/twitter/library/card/l;->R(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    if-ltz v4, :cond_5

    if-ltz v3, :cond_5

    if-ltz v2, :cond_5

    if-gez v1, :cond_6

    :cond_5
    new-instance v0, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v1, "invalid color"

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    invoke-static {v4, v3, v2, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected static G(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/Spacing;
    .locals 4

    const/high16 v0, 0x7fc00000    # NaNf

    new-instance v2, Lcom/twitter/library/card/property/Spacing;

    invoke-direct {v2, v0, v0, v0, v0}, Lcom/twitter/library/card/property/Spacing;-><init>(FFFF)V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_4

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_4

    sget-object v3, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-static {p0}, Lcom/twitter/library/card/l;->U(Lcom/fasterxml/jackson/core/JsonParser;)F

    move-result v1

    const-string/jumbo v3, "left"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, v2, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    iput v1, v3, Lcom/twitter/library/card/property/Vector2F;->x:F

    goto :goto_1

    :cond_1
    const-string/jumbo v3, "top"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v2, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    iput v1, v3, Lcom/twitter/library/card/property/Vector2F;->y:F

    goto :goto_1

    :cond_2
    const-string/jumbo v3, "right"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, v2, Lcom/twitter/library/card/property/Spacing;->end:Lcom/twitter/library/card/property/Vector2F;

    iput v1, v3, Lcom/twitter/library/card/property/Vector2F;->x:F

    goto :goto_1

    :cond_3
    const-string/jumbo v3, "bottom"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v2, Lcom/twitter/library/card/property/Spacing;->end:Lcom/twitter/library/card/property/Vector2F;

    iput v1, v3, Lcom/twitter/library/card/property/Vector2F;->y:F

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    iget-object v0, v2, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    iget v0, v0, Lcom/twitter/library/card/property/Vector2F;->x:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, v2, Lcom/twitter/library/card/property/Spacing;->start:Lcom/twitter/library/card/property/Vector2F;

    iget v0, v0, Lcom/twitter/library/card/property/Vector2F;->y:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, v2, Lcom/twitter/library/card/property/Spacing;->end:Lcom/twitter/library/card/property/Vector2F;

    iget v0, v0, Lcom/twitter/library/card/property/Vector2F;->x:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, v2, Lcom/twitter/library/card/property/Spacing;->end:Lcom/twitter/library/card/property/Vector2F;

    iget v0, v0, Lcom/twitter/library/card/property/Vector2F;->y:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    new-instance v0, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v1, "invalid spacing"

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected static H(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/Vector2;
    .locals 4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/card/property/Vector2;

    invoke-direct {v2}, Lcom/twitter/library/card/property/Vector2;-><init>()V

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_2

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_2

    sget-object v3, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    const-string/jumbo v1, "width"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0}, Lcom/twitter/library/card/l;->I(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/property/Vector2;->x:I

    goto :goto_1

    :cond_1
    const-string/jumbo v1, "height"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/twitter/library/card/l;->I(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/property/Vector2;->y:I

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget v0, v2, Lcom/twitter/library/card/property/Vector2;->x:I

    if-eqz v0, :cond_3

    iget v0, v2, Lcom/twitter/library/card/property/Vector2;->y:I

    if-nez v0, :cond_4

    :cond_3
    new-instance v0, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v1, "invalid size mode vector"

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected static I(Lcom/fasterxml/jackson/core/JsonParser;)I
    .locals 4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "fixed"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const-string/jumbo v1, "fill"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const-string/jumbo v1, "fit"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    const-string/jumbo v1, "constrain"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x4

    goto :goto_0

    :cond_3
    const-string/jumbo v1, "none"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v0, 0x5

    goto :goto_0

    :cond_4
    new-instance v1, Lcom/twitter/library/card/CardParserException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unknown size mode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected static J(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/Border;
    .locals 6

    new-instance v3, Lcom/twitter/library/card/property/Border;

    invoke-direct {v3}, Lcom/twitter/library/card/property/Border;-><init>()V

    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, v3, Lcom/twitter/library/card/property/Border;->width:F

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    move-object v5, v1

    move v1, v2

    move-object v2, v5

    :goto_0
    if-eqz v2, :cond_2

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v4, :cond_2

    sget-object v4, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v4, v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    :pswitch_1
    const-string/jumbo v2, "color"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v1, Lcom/twitter/library/card/property/Fill;

    invoke-direct {v1}, Lcom/twitter/library/card/property/Fill;-><init>()V

    iput-object v1, v3, Lcom/twitter/library/card/property/Border;->background:Lcom/twitter/library/card/property/Fill;

    iget-object v1, v3, Lcom/twitter/library/card/property/Border;->background:Lcom/twitter/library/card/property/Fill;

    invoke-static {p0}, Lcom/twitter/library/card/l;->F(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/card/property/Fill;->a(I)V

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_3
    const-string/jumbo v2, "width"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p0}, Lcom/twitter/library/card/l;->U(Lcom/fasterxml/jackson/core/JsonParser;)F

    move-result v2

    iput v2, v3, Lcom/twitter/library/card/property/Border;->width:F

    goto :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget v0, v3, Lcom/twitter/library/card/property/Border;->width:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_3

    if-nez v1, :cond_4

    :cond_3
    new-instance v0, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v1, "invalid border"

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    return-object v3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected static K(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/Vector2;
    .locals 4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/card/property/Vector2;

    invoke-direct {v2}, Lcom/twitter/library/card/property/Vector2;-><init>()V

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_2

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_2

    sget-object v3, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    const-string/jumbo v1, "horizontal"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0}, Lcom/twitter/library/card/l;->L(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/property/Vector2;->x:I

    goto :goto_1

    :cond_1
    const-string/jumbo v1, "vertical"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/twitter/library/card/l;->L(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/property/Vector2;->y:I

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget v0, v2, Lcom/twitter/library/card/property/Vector2;->x:I

    if-eqz v0, :cond_3

    iget v0, v2, Lcom/twitter/library/card/property/Vector2;->y:I

    if-nez v0, :cond_4

    :cond_3
    new-instance v0, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v1, "invalid alignment mode vector"

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected static L(Lcom/fasterxml/jackson/core/JsonParser;)I
    .locals 4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "start"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const-string/jumbo v1, "middle"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const-string/jumbo v1, "end"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    const-string/jumbo v1, "natural"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x4

    goto :goto_0

    :cond_3
    new-instance v1, Lcom/twitter/library/card/CardParserException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unknown alignment mode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected static M(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/Vector2F;
    .locals 4

    invoke-static {}, Lcom/twitter/library/card/property/Vector2F;->b()Lcom/twitter/library/card/property/Vector2F;

    move-result-object v2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_2

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_2

    sget-object v3, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-static {p0}, Lcom/twitter/library/card/l;->U(Lcom/fasterxml/jackson/core/JsonParser;)F

    move-result v1

    const-string/jumbo v3, "x"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput v1, v2, Lcom/twitter/library/card/property/Vector2F;->x:F

    goto :goto_1

    :cond_1
    const-string/jumbo v3, "y"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iput v1, v2, Lcom/twitter/library/card/property/Vector2F;->y:F

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget v0, v2, Lcom/twitter/library/card/property/Vector2F;->x:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_3

    iget v0, v2, Lcom/twitter/library/card/property/Vector2F;->y:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    new-instance v0, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v1, "invalid scaled vector2"

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected static N(Lcom/fasterxml/jackson/core/JsonParser;)I
    .locals 4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "relative"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const-string/jumbo v1, "absolute"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/twitter/library/card/CardParserException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unknown position mode:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected static O(Lcom/fasterxml/jackson/core/JsonParser;)I
    .locals 4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "clip"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const-string/jumbo v1, "show"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const-string/jumbo v1, "ellipsis"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    new-instance v1, Lcom/twitter/library/card/CardParserException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unknown overflowMode mode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected static P(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/a;
    .locals 3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/card/property/a;

    invoke-direct {v1}, Lcom/twitter/library/card/property/a;-><init>()V

    :goto_0
    if-eqz v0, :cond_0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_0

    sget-object v2, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_0
    invoke-static {p0, v1}, Lcom/twitter/library/card/l;->d(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/card/property/a;)V

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_0
    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static final Q(Lcom/fasterxml/jackson/core/JsonParser;)[I
    .locals 4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    if-eqz v0, :cond_0

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-static {p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v3, v0, [I

    const/4 v0, 0x0

    move v1, v0

    :goto_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v3, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_1
    return-object v3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static R(Lcom/fasterxml/jackson/core/JsonParser;)I
    .locals 2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->i()F

    move-result v0

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    if-ltz v0, :cond_0

    const/16 v1, 0xff

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v0, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v1, "invalid color channel"

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return v0
.end method

.method public static S(Lcom/fasterxml/jackson/core/JsonParser;)I
    .locals 2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v1, "invalid id"

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return v0
.end method

.method public static T(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Landroid/webkit/URLUtil;->isValidUrl(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "res://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/net/MalformedURLException;

    invoke-direct {v1, v0}, Ljava/net/MalformedURLException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    return-object v0
.end method

.method protected static U(Lcom/fasterxml/jackson/core/JsonParser;)F
    .locals 2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->i()F

    move-result v0

    sget v1, Lcom/twitter/library/util/b;->a:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public static a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/Card;
    .locals 14

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v6, v1

    move-object v7, v1

    move-object v8, v1

    move-object v9, v1

    move-object v10, v1

    move-object v11, v1

    :goto_0
    if-eqz v0, :cond_9

    sget-object v12, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v12, :cond_9

    sget-object v12, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v12, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    move-object v0, v1

    move v1, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v10

    move-object v10, v11

    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v11

    move-object v13, v0

    move-object v0, v11

    move-object v11, v10

    move-object v10, v9

    move-object v9, v8

    move-object v8, v7

    move-object v7, v6

    move-object v6, v5

    move-object v5, v4

    move-object v4, v3

    move-object v3, v2

    move v2, v1

    move-object v1, v13

    goto :goto_0

    :pswitch_0
    const-string/jumbo v0, "root"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/twitter/library/card/l;->a(Lcom/fasterxml/jackson/core/JsonParser;Z)Lcom/twitter/library/card/element/Element;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/Card;

    move-object v13, v1

    move v1, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v10

    move-object v10, v0

    move-object v0, v13

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v1

    move v1, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v10

    move-object v10, v11

    goto :goto_1

    :pswitch_1
    const-string/jumbo v0, "bindings"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0}, Lcom/twitter/library/card/l;->b(Lcom/fasterxml/jackson/core/JsonParser;)[Lcom/twitter/library/card/property/Binding;

    move-result-object v0

    move-object v7, v8

    move-object v8, v9

    move-object v9, v10

    move-object v10, v11

    move-object v13, v5

    move-object v5, v6

    move-object v6, v0

    move-object v0, v1

    move v1, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v13

    goto :goto_1

    :cond_2
    const-string/jumbo v0, "actions"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0}, Lcom/twitter/library/card/l;->d(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/a;

    move-result-object v0

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v10

    move-object v10, v11

    move-object v13, v0

    move-object v0, v1

    move v1, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v13

    goto :goto_1

    :cond_3
    const-string/jumbo v0, "styles"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p0}, Lcom/twitter/library/card/l;->m(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/a;

    move-result-object v0

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v10

    move-object v10, v11

    move v13, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v0

    move-object v0, v1

    move v1, v13

    goto/16 :goto_1

    :cond_4
    const-string/jumbo v0, "localized_tokenized_text"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {p0}, Lcom/twitter/library/card/l;->n(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;

    move-result-object v0

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v10

    move-object v10, v11

    move-object v13, v0

    move-object v0, v1

    move v1, v2

    move-object v2, v13

    goto/16 :goto_1

    :cond_5
    const-string/jumbo v0, "forms"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {p0}, Lcom/twitter/library/card/l;->P(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/a;

    move-result-object v0

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v10

    move-object v10, v11

    move-object v13, v0

    move-object v0, v1

    move v1, v2

    move-object v2, v3

    move-object v3, v13

    goto/16 :goto_1

    :cond_6
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v1

    move v1, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v10

    move-object v10, v11

    goto/16 :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v12, "uuid"

    invoke-virtual {v12, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_7

    move-object v10, v11

    move v13, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v0

    move-object v0, v1

    move v1, v13

    goto/16 :goto_1

    :cond_7
    const-string/jumbo v12, "name"

    invoke-virtual {v12, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_8

    move-object v9, v10

    move-object v10, v11

    move-object v13, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v0

    move-object v0, v1

    move v1, v2

    move-object v2, v13

    goto/16 :goto_1

    :cond_8
    const-string/jumbo v12, "platform_key"

    invoke-virtual {v12, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_0

    move-object v8, v9

    move-object v9, v10

    move-object v10, v11

    move-object v13, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v0

    move-object v0, v1

    move v1, v2

    move-object v2, v3

    move-object v3, v13

    goto/16 :goto_1

    :pswitch_3
    const-string/jumbo v0, "load_action_id"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v0

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v10

    move-object v10, v11

    move v13, v0

    move-object v0, v1

    move v1, v13

    goto/16 :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    move v1, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v10

    move-object v10, v11

    goto/16 :goto_1

    :cond_9
    if-eqz v11, :cond_a

    if-eqz v10, :cond_a

    if-eqz v9, :cond_a

    if-nez v8, :cond_b

    :cond_a
    new-instance v0, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v1, "invalid card"

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    iput-object v10, v11, Lcom/twitter/library/card/Card;->uuid:Ljava/lang/String;

    iput-object v9, v11, Lcom/twitter/library/card/Card;->name:Ljava/lang/String;

    iput-object v8, v11, Lcom/twitter/library/card/Card;->platformKey:Ljava/lang/String;

    iput-object v7, v11, Lcom/twitter/library/card/Card;->bindings:[Lcom/twitter/library/card/property/Binding;

    iput-object v6, v11, Lcom/twitter/library/card/Card;->actions:Lcom/twitter/library/card/property/a;

    iput-object v5, v11, Lcom/twitter/library/card/Card;->styles:Lcom/twitter/library/card/property/a;

    iput-object v4, v11, Lcom/twitter/library/card/Card;->forms:Lcom/twitter/library/card/property/a;

    iput-object v3, v11, Lcom/twitter/library/card/Card;->localizedTokenizedTexts:Ljava/util/HashMap;

    iput v2, v11, Lcom/twitter/library/card/Card;->loadActionId:I

    return-object v11

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected static a(Lcom/fasterxml/jackson/core/JsonParser;Z)Lcom/twitter/library/card/element/Element;
    .locals 24

    const/16 v21, 0x0

    const/16 v20, 0x0

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    const/16 v18, 0x0

    const/16 v17, 0x0

    const/16 v16, 0x0

    const/4 v15, 0x0

    const/4 v14, 0x0

    const/4 v13, 0x0

    const/high16 v12, 0x7fc00000    # NaNf

    const/4 v11, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    const/4 v2, 0x0

    move-object/from16 v23, v3

    move v3, v4

    move v4, v5

    move-object/from16 v5, v23

    :goto_0
    if-eqz v5, :cond_17

    sget-object v22, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    move-object/from16 v0, v22

    if-eq v5, v0, :cond_17

    sget-object v22, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v5}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v5

    aget v5, v22, v5

    packed-switch v5, :pswitch_data_0

    :cond_0
    move-object v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    move v9, v10

    move-object v10, v11

    move v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move/from16 v19, v20

    move-object/from16 v20, v21

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v21

    move-object/from16 v23, v21

    move-object/from16 v21, v20

    move/from16 v20, v19

    move-object/from16 v19, v18

    move-object/from16 v18, v17

    move-object/from16 v17, v16

    move-object/from16 v16, v15

    move-object v15, v14

    move-object v14, v13

    move-object v13, v12

    move v12, v11

    move-object v11, v10

    move v10, v9

    move v9, v8

    move v8, v7

    move v7, v6

    move-object v6, v5

    move-object/from16 v5, v23

    goto :goto_0

    :pswitch_0
    const-string/jumbo v5, "size_mode"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/card/l;->H(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/Vector2;

    move-result-object v5

    move-object/from16 v18, v19

    move/from16 v19, v20

    move-object/from16 v20, v21

    move/from16 v23, v9

    move v9, v10

    move-object v10, v11

    move v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v5

    move-object v5, v6

    move v6, v7

    move v7, v8

    move/from16 v8, v23

    goto :goto_1

    :cond_1
    const-string/jumbo v5, "max_size_mode"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/card/l;->H(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/Vector2;

    move-result-object v5

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move/from16 v19, v20

    move-object/from16 v20, v21

    move/from16 v23, v10

    move-object v10, v11

    move v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v5

    move-object v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    move/from16 v9, v23

    goto :goto_1

    :cond_2
    const-string/jumbo v5, "position"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/card/l;->M(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/Vector2F;

    move-result-object v5

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move/from16 v19, v20

    move-object/from16 v20, v21

    move-object/from16 v23, v11

    move v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object v15, v5

    move-object v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    move v9, v10

    move-object/from16 v10, v23

    goto/16 :goto_1

    :cond_3
    const-string/jumbo v5, "size"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/card/l;->M(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/Vector2F;

    move-result-object v5

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move/from16 v19, v20

    move-object/from16 v20, v21

    move/from16 v23, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v5

    move-object v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    move v9, v10

    move-object v10, v11

    move/from16 v11, v23

    goto/16 :goto_1

    :cond_4
    const-string/jumbo v5, "max_size"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/card/l;->M(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/Vector2F;

    move-result-object v5

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move/from16 v19, v20

    move-object/from16 v20, v21

    move-object/from16 v23, v13

    move-object v13, v5

    move-object v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    move v9, v10

    move-object v10, v11

    move v11, v12

    move-object/from16 v12, v23

    goto/16 :goto_1

    :cond_5
    const-string/jumbo v5, "margin"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/card/l;->G(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/Spacing;

    move-result-object v5

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move/from16 v19, v20

    move-object/from16 v20, v21

    move-object/from16 v23, v6

    move v6, v7

    move v7, v8

    move v8, v9

    move v9, v10

    move-object v10, v11

    move v11, v12

    move-object v12, v5

    move-object/from16 v5, v23

    goto/16 :goto_1

    :cond_6
    const-string/jumbo v5, "shadow"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/card/l;->E(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/Shadow;

    move-result-object v5

    move v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move/from16 v19, v20

    move-object/from16 v20, v21

    move/from16 v23, v10

    move-object v10, v5

    move-object v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    move/from16 v9, v23

    goto/16 :goto_1

    :cond_7
    const-string/jumbo v5, "container"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-static/range {p0 .. p1}, Lcom/twitter/library/card/l;->b(Lcom/fasterxml/jackson/core/JsonParser;Z)Lcom/twitter/library/card/element/Container;

    move-result-object v5

    move-object/from16 v23, v6

    move v6, v7

    move v7, v8

    move v8, v9

    move v9, v10

    move-object v10, v11

    move v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move/from16 v19, v20

    move-object/from16 v20, v5

    move-object/from16 v5, v23

    goto/16 :goto_1

    :cond_8
    const-string/jumbo v5, "box"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/card/l;->s(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/element/Box;

    move-result-object v5

    move-object/from16 v23, v6

    move v6, v7

    move v7, v8

    move v8, v9

    move v9, v10

    move-object v10, v11

    move v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move/from16 v19, v20

    move-object/from16 v20, v5

    move-object/from16 v5, v23

    goto/16 :goto_1

    :cond_9
    const-string/jumbo v5, "text"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_a

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/card/l;->t(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/element/Text;

    move-result-object v5

    move-object/from16 v23, v6

    move v6, v7

    move v7, v8

    move v8, v9

    move v9, v10

    move-object v10, v11

    move v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move/from16 v19, v20

    move-object/from16 v20, v5

    move-object/from16 v5, v23

    goto/16 :goto_1

    :cond_a
    const-string/jumbo v5, "image"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_b

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/card/l;->u(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/element/Image;

    move-result-object v5

    move-object/from16 v23, v6

    move v6, v7

    move v7, v8

    move v8, v9

    move v9, v10

    move-object v10, v11

    move v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move/from16 v19, v20

    move-object/from16 v20, v5

    move-object/from16 v5, v23

    goto/16 :goto_1

    :cond_b
    const-string/jumbo v5, "player"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_c

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/card/l;->v(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/element/Player;

    move-result-object v5

    move-object/from16 v23, v6

    move v6, v7

    move v7, v8

    move v8, v9

    move v9, v10

    move-object v10, v11

    move v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move/from16 v19, v20

    move-object/from16 v20, v5

    move-object/from16 v5, v23

    goto/16 :goto_1

    :cond_c
    const-string/jumbo v5, "form_checkbox"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_d

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/card/l;->x(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/element/FormCheckbox;

    move-result-object v5

    move-object/from16 v23, v6

    move v6, v7

    move v7, v8

    move v8, v9

    move v9, v10

    move-object v10, v11

    move v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move/from16 v19, v20

    move-object/from16 v20, v5

    move-object/from16 v5, v23

    goto/16 :goto_1

    :cond_d
    const-string/jumbo v5, "form_select"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_e

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/card/l;->z(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/element/FormSelect;

    move-result-object v5

    move-object/from16 v23, v6

    move v6, v7

    move v7, v8

    move v8, v9

    move v9, v10

    move-object v10, v11

    move v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move/from16 v19, v20

    move-object/from16 v20, v5

    move-object/from16 v5, v23

    goto/16 :goto_1

    :cond_e
    const-string/jumbo v5, "form_text"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_f

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/card/l;->A(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/element/FormText;

    move-result-object v5

    move-object/from16 v23, v6

    move v6, v7

    move v7, v8

    move v8, v9

    move v9, v10

    move-object v10, v11

    move v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move/from16 v19, v20

    move-object/from16 v20, v5

    move-object/from16 v5, v23

    goto/16 :goto_1

    :cond_f
    const-string/jumbo v5, "follow_button"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_10

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/card/l;->B(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/element/FollowButtonElement;

    move-result-object v5

    move-object/from16 v23, v6

    move v6, v7

    move v7, v8

    move v8, v9

    move v9, v10

    move-object v10, v11

    move v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move/from16 v19, v20

    move-object/from16 v20, v5

    move-object/from16 v5, v23

    goto/16 :goto_1

    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    move v9, v10

    move-object v10, v11

    move v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move/from16 v19, v20

    move-object/from16 v20, v21

    goto/16 :goto_1

    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    move v9, v10

    move-object v10, v11

    move v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move/from16 v19, v20

    move-object/from16 v20, v21

    goto/16 :goto_1

    :pswitch_2
    const-string/jumbo v5, "position_mode"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_11

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/card/l;->N(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move/from16 v19, v20

    move-object/from16 v20, v21

    move/from16 v23, v8

    move v8, v9

    move v9, v10

    move-object v10, v11

    move v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v5

    move-object v5, v6

    move v6, v7

    move/from16 v7, v23

    goto/16 :goto_1

    :cond_11
    const-string/jumbo v5, "debug_id"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v5

    move v6, v7

    move v7, v8

    move v8, v9

    move v9, v10

    move-object v10, v11

    move v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move/from16 v19, v20

    move-object/from16 v20, v21

    goto/16 :goto_1

    :pswitch_3
    const-string/jumbo v5, "id"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_12

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v5

    move-object/from16 v20, v21

    move/from16 v23, v7

    move v7, v8

    move v8, v9

    move v9, v10

    move-object v10, v11

    move v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move/from16 v19, v5

    move-object v5, v6

    move/from16 v6, v23

    goto/16 :goto_1

    :cond_12
    const-string/jumbo v5, "opacity"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_13

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->i()F

    move-result v5

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move/from16 v19, v20

    move-object/from16 v20, v21

    move/from16 v23, v8

    move v8, v9

    move v9, v10

    move-object v10, v11

    move v11, v5

    move-object v5, v6

    move v6, v7

    move/from16 v7, v23

    goto/16 :goto_1

    :cond_13
    const-string/jumbo v5, "press_down_action_id"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_14

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v5

    move-object v10, v11

    move v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move/from16 v19, v20

    move-object/from16 v20, v21

    move/from16 v23, v7

    move v7, v8

    move v8, v9

    move v9, v5

    move-object v5, v6

    move/from16 v6, v23

    goto/16 :goto_1

    :cond_14
    const-string/jumbo v5, "press_up_action_id"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_15

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v5

    move v9, v10

    move-object v10, v11

    move v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move/from16 v19, v20

    move-object/from16 v20, v21

    move-object/from16 v23, v6

    move v6, v7

    move v7, v8

    move v8, v5

    move-object/from16 v5, v23

    goto/16 :goto_1

    :cond_15
    const-string/jumbo v5, "tap_action_id"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_16

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v5

    move v8, v9

    move v9, v10

    move-object v10, v11

    move v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move/from16 v19, v20

    move-object/from16 v20, v21

    move/from16 v23, v7

    move v7, v5

    move-object v5, v6

    move/from16 v6, v23

    goto/16 :goto_1

    :cond_16
    const-string/jumbo v5, "long_press_action_id"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v5

    move v7, v8

    move v8, v9

    move v9, v10

    move-object v10, v11

    move v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move/from16 v19, v20

    move-object/from16 v20, v21

    move-object/from16 v23, v6

    move v6, v5

    move-object/from16 v5, v23

    goto/16 :goto_1

    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->k()Z

    move-result v5

    const-string/jumbo v22, "visible"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_0

    const/4 v3, 0x1

    move v4, v5

    move-object v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    move v9, v10

    move-object v10, v11

    move v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move/from16 v19, v20

    move-object/from16 v20, v21

    goto/16 :goto_1

    :pswitch_5
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v2

    move-object v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    move v9, v10

    move-object v10, v11

    move v11, v12

    move-object v12, v13

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move/from16 v19, v20

    move-object/from16 v20, v21

    goto/16 :goto_1

    :cond_17
    if-eqz v21, :cond_18

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eqz v2, :cond_18

    if-eqz v18, :cond_18

    if-eqz v17, :cond_18

    if-eqz v16, :cond_18

    if-eqz v15, :cond_18

    if-eqz v14, :cond_18

    if-eqz v13, :cond_18

    invoke-static {v12}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_18

    if-nez v3, :cond_19

    :cond_18
    new-instance v2, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v3, "invalid element"

    invoke-direct {v2, v3}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_19
    move/from16 v0, v20

    move-object/from16 v1, v21

    iput v0, v1, Lcom/twitter/library/card/element/Element;->id:I

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move-object/from16 v0, v21

    iput v2, v0, Lcom/twitter/library/card/element/Element;->positionMode:I

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/twitter/library/card/element/Element;->maxSizeMode:Lcom/twitter/library/card/property/Vector2;

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/twitter/library/card/element/Element;->position:Lcom/twitter/library/card/property/Vector2F;

    move-object/from16 v0, v21

    iput-object v15, v0, Lcom/twitter/library/card/element/Element;->size:Lcom/twitter/library/card/property/Vector2F;

    move-object/from16 v0, v21

    iput-object v14, v0, Lcom/twitter/library/card/element/Element;->maxSize:Lcom/twitter/library/card/property/Vector2F;

    move-object/from16 v0, v21

    iput-object v13, v0, Lcom/twitter/library/card/element/Element;->margin:Lcom/twitter/library/card/property/Spacing;

    move-object/from16 v0, v21

    iput v12, v0, Lcom/twitter/library/card/element/Element;->opacity:F

    move-object/from16 v0, v21

    iput-object v11, v0, Lcom/twitter/library/card/element/Element;->shadow:Lcom/twitter/library/card/property/Shadow;

    move-object/from16 v0, v21

    iput v10, v0, Lcom/twitter/library/card/element/Element;->pressDownActionId:I

    move-object/from16 v0, v21

    iput v9, v0, Lcom/twitter/library/card/element/Element;->pressUpActionId:I

    move-object/from16 v0, v21

    iput v8, v0, Lcom/twitter/library/card/element/Element;->tapActionId:I

    move-object/from16 v0, v21

    iput v7, v0, Lcom/twitter/library/card/element/Element;->longPressActionId:I

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Lcom/twitter/library/card/element/Element;->e(Z)V

    move-object/from16 v0, v21

    iput-object v6, v0, Lcom/twitter/library/card/element/Element;->debugId:Ljava/lang/String;

    return-object v21

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method protected static a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/card/property/Fill;)V
    .locals 6

    const/4 v2, 0x0

    const/high16 v3, 0x7fc00000    # NaNf

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    move-object v4, v1

    move v1, v2

    :goto_0
    if-eqz v4, :cond_2

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v5, :cond_2

    sget-object v5, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v4}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v4

    aget v4, v5, v4

    packed-switch v4, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    goto :goto_0

    :pswitch_1
    const-string/jumbo v4, "color"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {p0}, Lcom/twitter/library/card/l;->F(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v2

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_3
    const-string/jumbo v4, "position"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->i()F

    move-result v3

    goto :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    invoke-static {v3}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_3

    if-nez v1, :cond_4

    :cond_3
    new-instance v0, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v1, "invalid color stop"

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    invoke-virtual {p1, v3, v2}, Lcom/twitter/library/card/property/Fill;->a(FI)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected static a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/card/property/LocalizedTokenizedText;)V
    .locals 2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/card/property/a;

    invoke-direct {v1}, Lcom/twitter/library/card/property/a;-><init>()V

    iput-object v1, p1, Lcom/twitter/library/card/property/LocalizedTokenizedText;->tokenizedTexts:Lcom/twitter/library/card/property/a;

    :goto_0
    if-eqz v0, :cond_0

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_0
    iget-object v0, p1, Lcom/twitter/library/card/property/LocalizedTokenizedText;->tokenizedTexts:Lcom/twitter/library/card/property/a;

    invoke-static {p0, v0}, Lcom/twitter/library/card/l;->c(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/card/property/a;)V

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected static a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/card/property/a;)V
    .locals 5

    const/16 v4, 0xe

    new-instance v2, Lcom/twitter/library/card/property/Action;

    invoke-direct {v2}, Lcom/twitter/library/card/property/Action;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_1c

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_1c

    sget-object v3, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_0
    const-string/jumbo v1, "api_request"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0}, Lcom/twitter/library/card/l;->e(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/ApiRequest;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/property/Action;->apiRequest:Lcom/twitter/library/card/property/ApiRequest;

    goto :goto_1

    :cond_1
    const-string/jumbo v1, "purchase_request"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p0}, Lcom/twitter/library/card/l;->h(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/PurchaseRequest;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/property/Action;->purchaseRequest:Lcom/twitter/library/card/property/PurchaseRequest;

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_1
    const-string/jumbo v1, "style_pairs"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {p0}, Lcom/twitter/library/card/l;->k(Lcom/fasterxml/jackson/core/JsonParser;)[Lcom/twitter/library/card/property/StylePair;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/property/Action;->stylePairs:[Lcom/twitter/library/card/property/StylePair;

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "action_type"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_15

    const-string/jumbo v3, "open_url"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v1, 0x1

    iput v1, v2, Lcom/twitter/library/card/property/Action;->actionType:I

    goto :goto_1

    :cond_4
    const-string/jumbo v3, "open_url_popup_menu"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v1, 0x2

    iput v1, v2, Lcom/twitter/library/card/property/Action;->actionType:I

    goto :goto_1

    :cond_5
    const-string/jumbo v3, "open_photo"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v1, 0x3

    iput v1, v2, Lcom/twitter/library/card/property/Action;->actionType:I

    goto :goto_1

    :cond_6
    const-string/jumbo v3, "open_profile"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    const/4 v1, 0x4

    iput v1, v2, Lcom/twitter/library/card/property/Action;->actionType:I

    goto :goto_1

    :cond_7
    const-string/jumbo v3, "open_store"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    const/4 v1, 0x5

    iput v1, v2, Lcom/twitter/library/card/property/Action;->actionType:I

    goto/16 :goto_1

    :cond_8
    const-string/jumbo v3, "open_app_url"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    const/4 v1, 0x6

    iput v1, v2, Lcom/twitter/library/card/property/Action;->actionType:I

    goto/16 :goto_1

    :cond_9
    const-string/jumbo v3, "apply_styles"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    const/4 v1, 0x7

    iput v1, v2, Lcom/twitter/library/card/property/Action;->actionType:I

    goto/16 :goto_1

    :cond_a
    const-string/jumbo v3, "remove_styles"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    const/16 v1, 0x8

    iput v1, v2, Lcom/twitter/library/card/property/Action;->actionType:I

    goto/16 :goto_1

    :cond_b
    const-string/jumbo v3, "compose_status"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    const/16 v1, 0x9

    iput v1, v2, Lcom/twitter/library/card/property/Action;->actionType:I

    goto/16 :goto_1

    :cond_c
    const-string/jumbo v3, "favorite_status"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_d

    const/16 v1, 0xa

    iput v1, v2, Lcom/twitter/library/card/property/Action;->actionType:I

    goto/16 :goto_1

    :cond_d
    const-string/jumbo v3, "retweet_status"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_e

    const/16 v1, 0xb

    iput v1, v2, Lcom/twitter/library/card/property/Action;->actionType:I

    goto/16 :goto_1

    :cond_e
    const-string/jumbo v3, "share_status"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_f

    const/16 v1, 0xc

    iput v1, v2, Lcom/twitter/library/card/property/Action;->actionType:I

    goto/16 :goto_1

    :cond_f
    const-string/jumbo v3, "dial_phone"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_10

    const/16 v1, 0xd

    iput v1, v2, Lcom/twitter/library/card/property/Action;->actionType:I

    goto/16 :goto_1

    :cond_10
    const-string/jumbo v3, "api_request"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_11

    iput v4, v2, Lcom/twitter/library/card/property/Action;->actionType:I

    goto/16 :goto_1

    :cond_11
    const-string/jumbo v3, "open_tweet_detail"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_12

    const/16 v1, 0xf

    iput v1, v2, Lcom/twitter/library/card/property/Action;->actionType:I

    goto/16 :goto_1

    :cond_12
    const-string/jumbo v3, "purchase_request"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_13

    const/16 v1, 0x10

    iput v1, v2, Lcom/twitter/library/card/property/Action;->actionType:I

    goto/16 :goto_1

    :cond_13
    const-string/jumbo v3, "submit_form"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_14

    const/16 v1, 0x11

    iput v1, v2, Lcom/twitter/library/card/property/Action;->actionType:I

    goto/16 :goto_1

    :cond_14
    new-instance v0, Lcom/twitter/library/card/CardParserException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unknown action_type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_15
    const-string/jumbo v3, "scribe_element"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_16

    iput-object v1, v2, Lcom/twitter/library/card/property/Action;->scribeElement:Ljava/lang/String;

    goto/16 :goto_1

    :cond_16
    const-string/jumbo v3, "scribe_action"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_17

    iput-object v1, v2, Lcom/twitter/library/card/property/Action;->scribeAction:Ljava/lang/String;

    goto/16 :goto_1

    :cond_17
    const-string/jumbo v3, "debug_id"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iput-object v1, v2, Lcom/twitter/library/card/property/Action;->debugId:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_3
    const-string/jumbo v1, "id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_18

    invoke-static {p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/property/Action;->id:I

    goto/16 :goto_1

    :cond_18
    const-string/jumbo v1, "tokenized_text_id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_19

    invoke-static {p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/property/Action;->tokenizedTextId:I

    goto/16 :goto_1

    :cond_19
    const-string/jumbo v1, "success_action_id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1a

    invoke-static {p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/property/Action;->successActionId:I

    goto/16 :goto_1

    :cond_1a
    const-string/jumbo v1, "failure_action_id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1b

    invoke-static {p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/property/Action;->failureActionId:I

    goto/16 :goto_1

    :cond_1b
    const-string/jumbo v1, "form_id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/property/Action;->formId:I

    goto/16 :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_1c
    iget v0, v2, Lcom/twitter/library/card/property/Action;->id:I

    if-eqz v0, :cond_1d

    iget v0, v2, Lcom/twitter/library/card/property/Action;->actionType:I

    if-eqz v0, :cond_1d

    iget v0, v2, Lcom/twitter/library/card/property/Action;->actionType:I

    if-ne v0, v4, :cond_1e

    iget-object v0, v2, Lcom/twitter/library/card/property/Action;->apiRequest:Lcom/twitter/library/card/property/ApiRequest;

    if-nez v0, :cond_1e

    :cond_1d
    new-instance v0, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v1, "invalid action"

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1e
    iget v0, v2, Lcom/twitter/library/card/property/Action;->id:I

    invoke-virtual {p1, v0, v2}, Lcom/twitter/library/card/property/a;->put(ILjava/lang/Object;)V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected static a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;)V
    .locals 5

    const/4 v0, 0x0

    new-instance v3, Lcom/twitter/library/card/property/LocalizedTokenizedText;

    invoke-direct {v3}, Lcom/twitter/library/card/property/LocalizedTokenizedText;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v2, v1

    move-object v1, v0

    :goto_0
    if-eqz v2, :cond_2

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v4, :cond_2

    sget-object v4, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v4, v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    const-string/jumbo v2, "tokenized_texts"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p0, v3}, Lcom/twitter/library/card/l;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/card/property/LocalizedTokenizedText;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v4, "locale"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    move-object v1, v2

    goto :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    if-nez v1, :cond_3

    new-instance v0, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v1, "invalid localized tokenized text"

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-virtual {p1, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method protected static b(Lcom/fasterxml/jackson/core/JsonParser;Z)Lcom/twitter/library/card/element/Container;
    .locals 7

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz p1, :cond_1

    new-instance v0, Lcom/twitter/library/card/Card;

    invoke-direct {v0}, Lcom/twitter/library/card/Card;-><init>()V

    :goto_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    const/4 v1, 0x0

    :goto_1
    if-eqz v2, :cond_f

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v5, :cond_f

    sget-object v5, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v5, v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_1

    :cond_1
    new-instance v0, Lcom/twitter/library/card/element/Container;

    invoke-direct {v0}, Lcom/twitter/library/card/element/Container;-><init>()V

    goto :goto_0

    :pswitch_0
    const-string/jumbo v2, "background"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {p0}, Lcom/twitter/library/card/l;->D(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/Fill;

    move-result-object v2

    iput-object v2, v0, Lcom/twitter/library/card/element/Container;->background:Lcom/twitter/library/card/property/Fill;

    goto :goto_2

    :cond_2
    const-string/jumbo v2, "border"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {p0}, Lcom/twitter/library/card/l;->J(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/Border;

    move-result-object v2

    iput-object v2, v0, Lcom/twitter/library/card/element/Container;->border:Lcom/twitter/library/card/property/Border;

    goto :goto_2

    :cond_3
    const-string/jumbo v2, "padding"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {p0}, Lcom/twitter/library/card/l;->G(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/Spacing;

    move-result-object v2

    iput-object v2, v0, Lcom/twitter/library/card/element/Container;->padding:Lcom/twitter/library/card/property/Spacing;

    goto :goto_2

    :cond_4
    const-string/jumbo v2, "alignment_mode"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {p0}, Lcom/twitter/library/card/l;->K(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/Vector2;

    move-result-object v2

    iput-object v2, v0, Lcom/twitter/library/card/element/Container;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    goto :goto_2

    :cond_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    :pswitch_1
    const-string/jumbo v2, "children"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    move-object v6, v2

    :goto_3
    if-eqz v6, :cond_7

    move v2, v3

    :goto_4
    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v6, v5, :cond_8

    move v5, v3

    :goto_5
    and-int/2addr v2, v5

    if-eqz v2, :cond_0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v6, v2, :cond_9

    invoke-static {p0, v4}, Lcom/twitter/library/card/l;->a(Lcom/fasterxml/jackson/core/JsonParser;Z)Lcom/twitter/library/card/element/Element;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/card/element/Container;->a(Lcom/twitter/library/card/element/Element;)V

    :cond_6
    :goto_6
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    move-object v6, v2

    goto :goto_3

    :cond_7
    move v2, v4

    goto :goto_4

    :cond_8
    move v5, v4

    goto :goto_5

    :cond_9
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v6, v2, :cond_6

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_6

    :cond_a
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_2

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v5, "layout"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_d

    const-string/jumbo v5, "horizontal"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_b

    iput v3, v0, Lcom/twitter/library/card/element/Container;->layoutMode:I

    goto/16 :goto_2

    :cond_b
    const-string/jumbo v5, "vertical"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_c

    const/4 v2, 0x2

    iput v2, v0, Lcom/twitter/library/card/element/Container;->layoutMode:I

    goto/16 :goto_2

    :cond_c
    const-string/jumbo v5, "grid"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x3

    iput v2, v0, Lcom/twitter/library/card/element/Container;->layoutMode:I

    goto/16 :goto_2

    :cond_d
    const-string/jumbo v2, "overflow"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p0}, Lcom/twitter/library/card/l;->O(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v2

    iput v2, v0, Lcom/twitter/library/card/element/Container;->overflowMode:I

    goto/16 :goto_2

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v2

    const-string/jumbo v5, "visible_child_index"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_e

    iput v2, v0, Lcom/twitter/library/card/element/Container;->visibleChildIndex:I

    goto/16 :goto_2

    :cond_e
    const-string/jumbo v2, "corner_radius"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p0}, Lcom/twitter/library/card/l;->U(Lcom/fasterxml/jackson/core/JsonParser;)F

    move-result v2

    iput v2, v0, Lcom/twitter/library/card/element/Container;->cornerRadius:F

    goto/16 :goto_2

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    :cond_f
    iget v1, v0, Lcom/twitter/library/card/element/Container;->layoutMode:I

    if-eqz v1, :cond_10

    iget-object v1, v0, Lcom/twitter/library/card/element/Container;->background:Lcom/twitter/library/card/property/Fill;

    if-eqz v1, :cond_10

    iget-object v1, v0, Lcom/twitter/library/card/element/Container;->border:Lcom/twitter/library/card/property/Border;

    if-eqz v1, :cond_10

    iget-object v1, v0, Lcom/twitter/library/card/element/Container;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    if-eqz v1, :cond_10

    iget-object v1, v0, Lcom/twitter/library/card/element/Container;->padding:Lcom/twitter/library/card/property/Spacing;

    if-eqz v1, :cond_10

    iget v1, v0, Lcom/twitter/library/card/element/Container;->overflowMode:I

    if-nez v1, :cond_11

    :cond_10
    new-instance v0, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v1, "invalid container"

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_11
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected static b(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/card/property/a;)V
    .locals 8

    const/4 v2, 0x0

    new-instance v3, Lcom/twitter/library/card/property/Style;

    invoke-direct {v3}, Lcom/twitter/library/card/property/Style;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    move-object v7, v1

    move v1, v2

    move-object v2, v7

    :goto_0
    if-eqz v2, :cond_15

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v4, :cond_15

    sget-object v4, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v4, v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    :pswitch_0
    const-string/jumbo v2, "margin"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p0}, Lcom/twitter/library/card/l;->G(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/Spacing;

    move-result-object v2

    iput-object v2, v3, Lcom/twitter/library/card/property/Style;->margin:Lcom/twitter/library/card/property/Spacing;

    goto :goto_1

    :cond_1
    const-string/jumbo v2, "shadow"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {p0}, Lcom/twitter/library/card/l;->E(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/Shadow;

    move-result-object v2

    iput-object v2, v3, Lcom/twitter/library/card/property/Style;->shadow:Lcom/twitter/library/card/property/Shadow;

    goto :goto_1

    :cond_2
    const-string/jumbo v2, "background"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {p0}, Lcom/twitter/library/card/l;->D(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/Fill;

    move-result-object v2

    iput-object v2, v3, Lcom/twitter/library/card/property/Style;->background:Lcom/twitter/library/card/property/Fill;

    goto :goto_1

    :cond_3
    const-string/jumbo v2, "border"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {p0}, Lcom/twitter/library/card/l;->J(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/Border;

    move-result-object v2

    iput-object v2, v3, Lcom/twitter/library/card/property/Style;->border:Lcom/twitter/library/card/property/Border;

    goto :goto_1

    :cond_4
    const-string/jumbo v2, "padding"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {p0}, Lcom/twitter/library/card/l;->G(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/Spacing;

    move-result-object v2

    iput-object v2, v3, Lcom/twitter/library/card/property/Style;->padding:Lcom/twitter/library/card/property/Spacing;

    goto :goto_1

    :cond_5
    const-string/jumbo v2, "color"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-static {p0}, Lcom/twitter/library/card/l;->F(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v3, Lcom/twitter/library/card/property/Style;->color:Ljava/lang/Integer;

    goto :goto_1

    :cond_6
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v4, "font_name"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    iput-object v2, v3, Lcom/twitter/library/card/property/Style;->fontName:Ljava/lang/String;

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->i()F

    move-result v4

    invoke-static {p0}, Lcom/twitter/library/card/l;->U(Lcom/fasterxml/jackson/core/JsonParser;)F

    move-result v5

    const-string/jumbo v6, "id"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-static {p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    iput v1, v3, Lcom/twitter/library/card/property/Style;->id:I

    invoke-static {p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    goto/16 :goto_1

    :cond_7
    const-string/jumbo v6, "position_x"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v3, Lcom/twitter/library/card/property/Style;->positionX:Ljava/lang/Float;

    goto/16 :goto_1

    :cond_8
    const-string/jumbo v6, "position_y"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v3, Lcom/twitter/library/card/property/Style;->positionY:Ljava/lang/Float;

    goto/16 :goto_1

    :cond_9
    const-string/jumbo v6, "size_x"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_a

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v3, Lcom/twitter/library/card/property/Style;->sizeX:Ljava/lang/Float;

    goto/16 :goto_1

    :cond_a
    const-string/jumbo v6, "size_y"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_b

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v3, Lcom/twitter/library/card/property/Style;->sizeY:Ljava/lang/Float;

    goto/16 :goto_1

    :cond_b
    const-string/jumbo v6, "max_size_x"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_c

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v3, Lcom/twitter/library/card/property/Style;->maxSizeX:Ljava/lang/Float;

    goto/16 :goto_1

    :cond_c
    const-string/jumbo v6, "max_size_y"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_d

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v3, Lcom/twitter/library/card/property/Style;->maxSizeY:Ljava/lang/Float;

    goto/16 :goto_1

    :cond_d
    const-string/jumbo v6, "opacity"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_e

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v3, Lcom/twitter/library/card/property/Style;->opacity:Ljava/lang/Float;

    goto/16 :goto_1

    :cond_e
    const-string/jumbo v4, "font_size"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v3, Lcom/twitter/library/card/property/Style;->fontSize:Ljava/lang/Float;

    goto/16 :goto_1

    :cond_f
    const-string/jumbo v4, "line_height"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v3, Lcom/twitter/library/card/property/Style;->lineHeight:Ljava/lang/Float;

    goto/16 :goto_1

    :cond_10
    const-string/jumbo v4, "corner_radius"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_11

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v3, Lcom/twitter/library/card/property/Style;->cornerRadius:Ljava/lang/Float;

    goto/16 :goto_1

    :cond_11
    const-string/jumbo v4, "visible_child_index"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v3, Lcom/twitter/library/card/property/Style;->visibleChildIndex:Ljava/lang/Integer;

    goto/16 :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->k()Z

    move-result v2

    const-string/jumbo v4, "visible"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_12

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v3, Lcom/twitter/library/card/property/Style;->visible:Ljava/lang/Boolean;

    goto/16 :goto_1

    :cond_12
    const-string/jumbo v4, "font_bold"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_13

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v3, Lcom/twitter/library/card/property/Style;->fontBold:Ljava/lang/Boolean;

    goto/16 :goto_1

    :cond_13
    const-string/jumbo v4, "font_underline"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_14

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v3, Lcom/twitter/library/card/property/Style;->fontUnderline:Ljava/lang/Boolean;

    goto/16 :goto_1

    :cond_14
    const-string/jumbo v4, "font_italic"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v3, Lcom/twitter/library/card/property/Style;->fontItalic:Ljava/lang/Boolean;

    goto/16 :goto_1

    :pswitch_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_15
    iget v0, v3, Lcom/twitter/library/card/property/Style;->id:I

    if-nez v0, :cond_16

    new-instance v0, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v1, "invalid style"

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_16
    invoke-virtual {p1, v1, v3}, Lcom/twitter/library/card/property/a;->put(ILjava/lang/Object;)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method protected static b(Lcom/fasterxml/jackson/core/JsonParser;)[Lcom/twitter/library/card/property/Binding;
    .locals 3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    if-eqz v0, :cond_0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_0

    sget-object v2, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_0
    invoke-static {p0}, Lcom/twitter/library/card/l;->c(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/Binding;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/twitter/library/card/property/Binding;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/card/property/Binding;

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected static c(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/Binding;
    .locals 9

    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    new-instance v2, Lcom/twitter/library/card/property/Binding;

    invoke-direct {v2}, Lcom/twitter/library/card/property/Binding;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_42

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_42

    sget-object v3, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "source_type"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1c

    const-string/jumbo v3, "string_value"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput v4, v2, Lcom/twitter/library/card/property/Binding;->sourceType:I

    goto :goto_1

    :cond_1
    const-string/jumbo v3, "boolean_value"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    iput v5, v2, Lcom/twitter/library/card/property/Binding;->sourceType:I

    goto :goto_1

    :cond_2
    const-string/jumbo v3, "image_value"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iput v6, v2, Lcom/twitter/library/card/property/Binding;->sourceType:I

    goto :goto_1

    :cond_3
    const-string/jumbo v3, "user_id"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    iput v7, v2, Lcom/twitter/library/card/property/Binding;->sourceType:I

    goto :goto_1

    :cond_4
    const-string/jumbo v3, "user_screen_name"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    iput v8, v2, Lcom/twitter/library/card/property/Binding;->sourceType:I

    goto :goto_1

    :cond_5
    const-string/jumbo v3, "user_profile_image_url"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v1, 0x6

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->sourceType:I

    goto :goto_1

    :cond_6
    const-string/jumbo v3, "user_profile_banner_url"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    const/4 v1, 0x7

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->sourceType:I

    goto :goto_1

    :cond_7
    const-string/jumbo v3, "user_name"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    const/16 v1, 0x8

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->sourceType:I

    goto :goto_1

    :cond_8
    const-string/jumbo v3, "user_location"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    const/16 v1, 0x9

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->sourceType:I

    goto/16 :goto_1

    :cond_9
    const-string/jumbo v3, "user_description"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    const/16 v1, 0xa

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->sourceType:I

    goto/16 :goto_1

    :cond_a
    const-string/jumbo v3, "user_url"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    const/16 v1, 0xb

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->sourceType:I

    goto/16 :goto_1

    :cond_b
    const-string/jumbo v3, "user_protected"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    const/16 v1, 0xc

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->sourceType:I

    goto/16 :goto_1

    :cond_c
    const-string/jumbo v3, "user_verified"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_d

    const/16 v1, 0xd

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->sourceType:I

    goto/16 :goto_1

    :cond_d
    const-string/jumbo v3, "user_following"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_e

    const/16 v1, 0xe

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->sourceType:I

    goto/16 :goto_1

    :cond_e
    const-string/jumbo v3, "user_statuses_count"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_f

    const/16 v1, 0xf

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->sourceType:I

    goto/16 :goto_1

    :cond_f
    const-string/jumbo v3, "user_following_count"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_10

    const/16 v1, 0x10

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->sourceType:I

    goto/16 :goto_1

    :cond_10
    const-string/jumbo v3, "user_followers_count"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_11

    const/16 v1, 0x11

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->sourceType:I

    goto/16 :goto_1

    :cond_11
    const-string/jumbo v3, "status_id"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_12

    const/16 v1, 0x12

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->sourceType:I

    goto/16 :goto_1

    :cond_12
    const-string/jumbo v3, "status_text"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_13

    const/16 v1, 0x13

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->sourceType:I

    goto/16 :goto_1

    :cond_13
    const-string/jumbo v3, "status_date"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_14

    const/16 v1, 0x14

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->sourceType:I

    goto/16 :goto_1

    :cond_14
    const-string/jumbo v3, "status_user_id"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_15

    const/16 v1, 0x15

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->sourceType:I

    goto/16 :goto_1

    :cond_15
    const-string/jumbo v3, "status_in_reply_to_status_id"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_16

    const/16 v1, 0x16

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->sourceType:I

    goto/16 :goto_1

    :cond_16
    const-string/jumbo v3, "status_retweeted_status_id"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_17

    const/16 v1, 0x17

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->sourceType:I

    goto/16 :goto_1

    :cond_17
    const-string/jumbo v3, "status_latitude"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_18

    const/16 v1, 0x18

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->sourceType:I

    goto/16 :goto_1

    :cond_18
    const-string/jumbo v3, "status_longitude"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_19

    const/16 v1, 0x19

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->sourceType:I

    goto/16 :goto_1

    :cond_19
    const-string/jumbo v3, "status_retweet_count"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1a

    const/16 v1, 0x1a

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->sourceType:I

    goto/16 :goto_1

    :cond_1a
    const-string/jumbo v3, "status_favorited"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1b

    const/16 v1, 0x1b

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->sourceType:I

    goto/16 :goto_1

    :cond_1b
    new-instance v0, Lcom/twitter/library/card/CardParserException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unknown source_type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1c
    const-string/jumbo v3, "source_key"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1d

    iput-object v1, v2, Lcom/twitter/library/card/property/Binding;->sourceKey:Ljava/lang/String;

    goto/16 :goto_1

    :cond_1d
    const-string/jumbo v3, "controller_type"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_29

    const-string/jumbo v3, "conditional_exists"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1e

    iput v4, v2, Lcom/twitter/library/card/property/Binding;->controllerType:I

    goto/16 :goto_1

    :cond_1e
    const-string/jumbo v3, "conditional_can_open_url"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1f

    iput v5, v2, Lcom/twitter/library/card/property/Binding;->controllerType:I

    goto/16 :goto_1

    :cond_1f
    const-string/jumbo v3, "conditional_can_dial_phone"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_20

    iput v6, v2, Lcom/twitter/library/card/property/Binding;->controllerType:I

    goto/16 :goto_1

    :cond_20
    const-string/jumbo v3, "conditional_truthy"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_21

    iput v7, v2, Lcom/twitter/library/card/property/Binding;->controllerType:I

    goto/16 :goto_1

    :cond_21
    const-string/jumbo v3, "time_ago"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_22

    iput v8, v2, Lcom/twitter/library/card/property/Binding;->controllerType:I

    goto/16 :goto_1

    :cond_22
    const-string/jumbo v3, "i18n_zero"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_23

    const/4 v1, 0x6

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->controllerType:I

    goto/16 :goto_1

    :cond_23
    const-string/jumbo v3, "i18n_one"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_24

    const/4 v1, 0x7

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->controllerType:I

    goto/16 :goto_1

    :cond_24
    const-string/jumbo v3, "i18n_two"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_25

    const/16 v1, 0x8

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->controllerType:I

    goto/16 :goto_1

    :cond_25
    const-string/jumbo v3, "i18n_few"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_26

    const/16 v1, 0x9

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->controllerType:I

    goto/16 :goto_1

    :cond_26
    const-string/jumbo v3, "i18n_many"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_27

    const/16 v1, 0xa

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->controllerType:I

    goto/16 :goto_1

    :cond_27
    const-string/jumbo v3, "i18n_other"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_28

    const/16 v1, 0xb

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->controllerType:I

    goto/16 :goto_1

    :cond_28
    new-instance v0, Lcom/twitter/library/card/CardParserException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unknown controller type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_29
    const-string/jumbo v3, "dest_type"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string/jumbo v3, "element_visible"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2a

    iput v4, v2, Lcom/twitter/library/card/property/Binding;->destType:I

    goto/16 :goto_1

    :cond_2a
    const-string/jumbo v3, "element_image_spec"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2b

    iput v5, v2, Lcom/twitter/library/card/property/Binding;->destType:I

    goto/16 :goto_1

    :cond_2b
    const-string/jumbo v3, "element_image_spec_full_size"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2c

    iput v6, v2, Lcom/twitter/library/card/property/Binding;->destType:I

    goto/16 :goto_1

    :cond_2c
    const-string/jumbo v3, "element_player_stream_url"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2d

    iput v7, v2, Lcom/twitter/library/card/property/Binding;->destType:I

    goto/16 :goto_1

    :cond_2d
    const-string/jumbo v3, "element_player_stream_content_type"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2e

    iput v8, v2, Lcom/twitter/library/card/property/Binding;->destType:I

    goto/16 :goto_1

    :cond_2e
    const-string/jumbo v3, "element_player_stream_width"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2f

    const/4 v1, 0x6

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->destType:I

    goto/16 :goto_1

    :cond_2f
    const-string/jumbo v3, "element_player_stream_height"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_30

    const/4 v1, 0x7

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->destType:I

    goto/16 :goto_1

    :cond_30
    const-string/jumbo v3, "element_player_html_url"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_31

    const/16 v1, 0x8

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->destType:I

    goto/16 :goto_1

    :cond_31
    const-string/jumbo v3, "element_player_thumbnail_spec"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_32

    const/16 v1, 0x9

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->destType:I

    goto/16 :goto_1

    :cond_32
    const-string/jumbo v3, "text_token_group"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_33

    const/16 v1, 0xb

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->destType:I

    goto/16 :goto_1

    :cond_33
    const-string/jumbo v3, "text_token"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_34

    const/16 v1, 0xc

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->destType:I

    goto/16 :goto_1

    :cond_34
    const-string/jumbo v3, "action_url"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_35

    const/16 v1, 0xd

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->destType:I

    goto/16 :goto_1

    :cond_35
    const-string/jumbo v3, "action_display_url"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_36

    const/16 v1, 0xe

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->destType:I

    goto/16 :goto_1

    :cond_36
    const-string/jumbo v3, "action_user_id"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_37

    const/16 v1, 0xf

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->destType:I

    goto/16 :goto_1

    :cond_37
    const-string/jumbo v3, "action_app_id"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_38

    const/16 v1, 0x10

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->destType:I

    goto/16 :goto_1

    :cond_38
    const-string/jumbo v3, "action_app_url"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_39

    const/16 v1, 0x11

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->destType:I

    goto/16 :goto_1

    :cond_39
    const-string/jumbo v3, "action_display_app_url"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3a

    const/16 v1, 0x12

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->destType:I

    goto/16 :goto_1

    :cond_3a
    const-string/jumbo v3, "action_phone_number_url"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3b

    const/16 v1, 0x13

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->destType:I

    goto/16 :goto_1

    :cond_3b
    const-string/jumbo v3, "action_phone_number"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3c

    const/16 v1, 0x14

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->destType:I

    goto/16 :goto_1

    :cond_3c
    const-string/jumbo v3, "api_parameter_key"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3d

    const/16 v1, 0x15

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->destType:I

    goto/16 :goto_1

    :cond_3d
    const-string/jumbo v3, "api_parameter_value"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3e

    const/16 v1, 0x16

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->destType:I

    goto/16 :goto_1

    :cond_3e
    const-string/jumbo v3, "purchase_parameter_key"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3f

    const/16 v1, 0x17

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->destType:I

    goto/16 :goto_1

    :cond_3f
    const-string/jumbo v3, "purchase_parameter_value"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_40

    const/16 v1, 0x18

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->destType:I

    goto/16 :goto_1

    :cond_40
    const-string/jumbo v3, "element_follow_button_user"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_41

    const/16 v1, 0xa

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->destType:I

    goto/16 :goto_1

    :cond_41
    new-instance v0, Lcom/twitter/library/card/CardParserException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unknown dest_type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_2
    const-string/jumbo v1, "dest_id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/property/Binding;->destId:I

    goto/16 :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->k()Z

    move-result v1

    const-string/jumbo v3, "negate_controller"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iput-boolean v1, v2, Lcom/twitter/library/card/property/Binding;->negateController:Z

    goto/16 :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_42
    iget v0, v2, Lcom/twitter/library/card/property/Binding;->sourceType:I

    if-eqz v0, :cond_43

    iget v0, v2, Lcom/twitter/library/card/property/Binding;->destType:I

    if-eqz v0, :cond_43

    iget v0, v2, Lcom/twitter/library/card/property/Binding;->destId:I

    if-nez v0, :cond_44

    :cond_43
    new-instance v0, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v1, "invalid binding"

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_44
    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method protected static c(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/card/property/a;)V
    .locals 5

    new-instance v3, Lcom/twitter/library/card/property/TokenizedText;

    invoke-direct {v3}, Lcom/twitter/library/card/property/TokenizedText;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    if-eqz v2, :cond_2

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v4, :cond_2

    sget-object v4, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v4, v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    const-string/jumbo v2, "text_token_groups"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p0}, Lcom/twitter/library/card/l;->o(Lcom/fasterxml/jackson/core/JsonParser;)[Lcom/twitter/library/card/property/TextTokenGroup;

    move-result-object v2

    iput-object v2, v3, Lcom/twitter/library/card/property/TokenizedText;->textTokenGroups:[Lcom/twitter/library/card/property/TextTokenGroup;

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_3
    const-string/jumbo v2, "id"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v0

    goto :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_2
    if-nez v0, :cond_3

    new-instance v0, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v1, "invalid tokenized text"

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-virtual {p1, v0, v3}, Lcom/twitter/library/card/property/a;->put(ILjava/lang/Object;)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected static d(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/a;
    .locals 3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/card/property/a;

    invoke-direct {v1}, Lcom/twitter/library/card/property/a;-><init>()V

    :goto_0
    if-eqz v0, :cond_0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_0

    sget-object v2, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_0
    invoke-static {p0, v1}, Lcom/twitter/library/card/l;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/card/property/a;)V

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_0
    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected static d(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/card/property/a;)V
    .locals 4

    new-instance v2, Lcom/twitter/library/card/property/Form;

    invoke-direct {v2}, Lcom/twitter/library/card/property/Form;-><init>()V

    const/4 v0, 0x0

    iput v0, v2, Lcom/twitter/library/card/property/Form;->id:I

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_3

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_3

    sget-object v3, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    const-string/jumbo v1, "element_ids"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0}, Lcom/twitter/library/card/l;->Q(Lcom/fasterxml/jackson/core/JsonParser;)[I

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/property/Form;->elementIds:[I

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_3
    const-string/jumbo v1, "id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/property/Form;->id:I

    goto :goto_1

    :cond_2
    const-string/jumbo v1, "api_request_action"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/property/Form;->apiRequestAction:I

    goto :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    iget v0, v2, Lcom/twitter/library/card/property/Form;->id:I

    if-nez v0, :cond_4

    new-instance v0, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v1, "invalid form"

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iget v0, v2, Lcom/twitter/library/card/property/Form;->id:I

    invoke-virtual {p1, v0, v2}, Lcom/twitter/library/card/property/a;->put(ILjava/lang/Object;)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected static e(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/ApiRequest;
    .locals 4

    new-instance v2, Lcom/twitter/library/card/property/ApiRequest;

    invoke-direct {v2}, Lcom/twitter/library/card/property/ApiRequest;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_6

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_6

    sget-object v3, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_1
    const-string/jumbo v1, "parameters"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0}, Lcom/twitter/library/card/l;->f(Lcom/fasterxml/jackson/core/JsonParser;)[Lcom/twitter/library/card/property/ApiRequestParameter;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/property/ApiRequest;->parameters:[Lcom/twitter/library/card/property/ApiRequestParameter;

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "method"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string/jumbo v3, "get"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v1, 0x1

    iput v1, v2, Lcom/twitter/library/card/property/ApiRequest;->method:I

    goto :goto_1

    :cond_2
    const-string/jumbo v3, "post"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v1, 0x2

    iput v1, v2, Lcom/twitter/library/card/property/ApiRequest;->method:I

    goto :goto_1

    :cond_3
    new-instance v0, Lcom/twitter/library/card/CardParserException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unknown method: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    const-string/jumbo v1, "api_proxy_rule"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/property/ApiRequest;->apiProxyRule:Ljava/lang/String;

    goto :goto_1

    :pswitch_3
    const-string/jumbo v1, "request_success_action_id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/property/ApiRequest;->successActionId:I

    goto :goto_1

    :cond_5
    const-string/jumbo v1, "request_failure_action_id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/property/ApiRequest;->failureActionId:I

    goto/16 :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_6
    iget v0, v2, Lcom/twitter/library/card/property/ApiRequest;->method:I

    if-nez v0, :cond_7

    new-instance v0, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v1, "invalid api_request"

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected static f(Lcom/fasterxml/jackson/core/JsonParser;)[Lcom/twitter/library/card/property/ApiRequestParameter;
    .locals 3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    if-eqz v0, :cond_0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_0

    sget-object v2, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_0
    invoke-static {p0}, Lcom/twitter/library/card/l;->g(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/ApiRequestParameter;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/twitter/library/card/property/ApiRequestParameter;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/card/property/ApiRequestParameter;

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected static g(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/ApiRequestParameter;
    .locals 4

    new-instance v2, Lcom/twitter/library/card/property/ApiRequestParameter;

    invoke-direct {v2}, Lcom/twitter/library/card/property/ApiRequestParameter;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_2

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_2

    sget-object v3, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "key"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v1, v2, Lcom/twitter/library/card/property/ApiRequestParameter;->key:Ljava/lang/String;

    goto :goto_1

    :cond_1
    const-string/jumbo v3, "value"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iput-object v1, v2, Lcom/twitter/library/card/property/ApiRequestParameter;->value:Ljava/lang/String;

    goto :goto_1

    :pswitch_2
    const-string/jumbo v1, "id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/property/ApiRequestParameter;->id:I

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget v0, v2, Lcom/twitter/library/card/property/ApiRequestParameter;->id:I

    if-nez v0, :cond_3

    new-instance v0, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v1, "invalid api request parameter"

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected static h(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/PurchaseRequest;
    .locals 4

    new-instance v2, Lcom/twitter/library/card/property/PurchaseRequest;

    invoke-direct {v2}, Lcom/twitter/library/card/property/PurchaseRequest;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_3

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_3

    sget-object v3, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    const-string/jumbo v1, "parameters"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0}, Lcom/twitter/library/card/l;->i(Lcom/fasterxml/jackson/core/JsonParser;)[Lcom/twitter/library/card/property/PurchaseRequestParameter;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/property/PurchaseRequest;->parameters:[Lcom/twitter/library/card/property/PurchaseRequestParameter;

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_3
    const-string/jumbo v1, "purchase_success_action_id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/property/PurchaseRequest;->successActionId:I

    goto :goto_1

    :cond_2
    const-string/jumbo v1, "purchase_failure_action_id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/property/PurchaseRequest;->failureActionId:I

    goto :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected static i(Lcom/fasterxml/jackson/core/JsonParser;)[Lcom/twitter/library/card/property/PurchaseRequestParameter;
    .locals 3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    if-eqz v0, :cond_0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_0

    sget-object v2, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_0
    invoke-static {p0}, Lcom/twitter/library/card/l;->j(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/PurchaseRequestParameter;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/twitter/library/card/property/PurchaseRequestParameter;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/card/property/PurchaseRequestParameter;

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected static j(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/PurchaseRequestParameter;
    .locals 4

    new-instance v2, Lcom/twitter/library/card/property/PurchaseRequestParameter;

    invoke-direct {v2}, Lcom/twitter/library/card/property/PurchaseRequestParameter;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_2

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_2

    sget-object v3, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "key"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v1, v2, Lcom/twitter/library/card/property/PurchaseRequestParameter;->key:Ljava/lang/String;

    goto :goto_1

    :cond_1
    const-string/jumbo v3, "value"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iput-object v1, v2, Lcom/twitter/library/card/property/PurchaseRequestParameter;->value:Ljava/lang/String;

    goto :goto_1

    :pswitch_2
    const-string/jumbo v1, "id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/property/PurchaseRequestParameter;->id:I

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget v0, v2, Lcom/twitter/library/card/property/PurchaseRequestParameter;->id:I

    if-nez v0, :cond_3

    new-instance v0, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v1, "invalid purchase request parameter"

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected static k(Lcom/fasterxml/jackson/core/JsonParser;)[Lcom/twitter/library/card/property/StylePair;
    .locals 3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    if-eqz v0, :cond_0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_0

    sget-object v2, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_0
    invoke-static {p0}, Lcom/twitter/library/card/l;->l(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/StylePair;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/twitter/library/card/property/StylePair;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/card/property/StylePair;

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected static l(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/StylePair;
    .locals 4

    new-instance v2, Lcom/twitter/library/card/property/StylePair;

    invoke-direct {v2}, Lcom/twitter/library/card/property/StylePair;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_2

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_2

    sget-object v3, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    const-string/jumbo v1, "element_id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/property/StylePair;->elementId:I

    goto :goto_1

    :cond_1
    const-string/jumbo v1, "style_id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/property/StylePair;->styleId:I

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget v0, v2, Lcom/twitter/library/card/property/StylePair;->elementId:I

    if-eqz v0, :cond_3

    iget v0, v2, Lcom/twitter/library/card/property/StylePair;->styleId:I

    if-nez v0, :cond_4

    :cond_3
    new-instance v0, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v1, "invalid style pair"

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected static m(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/a;
    .locals 3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/card/property/a;

    invoke-direct {v1}, Lcom/twitter/library/card/property/a;-><init>()V

    :goto_0
    if-eqz v0, :cond_0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_0

    sget-object v2, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_0
    invoke-static {p0, v1}, Lcom/twitter/library/card/l;->b(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/card/property/a;)V

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_0
    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected static n(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;
    .locals 3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    :goto_0
    if-eqz v0, :cond_0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_0

    sget-object v2, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_0
    invoke-static {p0, v1}, Lcom/twitter/library/card/l;->a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;)V

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_0
    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected static o(Lcom/fasterxml/jackson/core/JsonParser;)[Lcom/twitter/library/card/property/TextTokenGroup;
    .locals 3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    if-eqz v0, :cond_0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_0

    sget-object v2, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_0
    invoke-static {p0}, Lcom/twitter/library/card/l;->p(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/TextTokenGroup;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/twitter/library/card/property/TextTokenGroup;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/card/property/TextTokenGroup;

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected static p(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/TextTokenGroup;
    .locals 4

    new-instance v2, Lcom/twitter/library/card/property/TextTokenGroup;

    invoke-direct {v2}, Lcom/twitter/library/card/property/TextTokenGroup;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_2

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_2

    sget-object v3, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    const-string/jumbo v1, "text_tokens"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0}, Lcom/twitter/library/card/l;->q(Lcom/fasterxml/jackson/core/JsonParser;)[Lcom/twitter/library/card/property/TextToken;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/property/TextTokenGroup;->textTokens:[Lcom/twitter/library/card/property/TextToken;

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_3
    const-string/jumbo v1, "id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/property/TextTokenGroup;->id:I

    goto :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected static q(Lcom/fasterxml/jackson/core/JsonParser;)[Lcom/twitter/library/card/property/TextToken;
    .locals 3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    if-eqz v0, :cond_0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_0

    sget-object v2, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_0
    invoke-static {p0}, Lcom/twitter/library/card/l;->r(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/TextToken;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/twitter/library/card/property/TextToken;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/card/property/TextToken;

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected static r(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/TextToken;
    .locals 4

    new-instance v2, Lcom/twitter/library/card/property/TextToken;

    invoke-direct {v2}, Lcom/twitter/library/card/property/TextToken;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_6

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_6

    sget-object v3, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "text"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iput-object v1, v2, Lcom/twitter/library/card/property/TextToken;->text:Ljava/lang/String;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v1

    const-string/jumbo v3, "id"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/property/TextToken;->id:I

    goto :goto_1

    :cond_1
    const-string/jumbo v3, "style_id"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    iput v1, v2, Lcom/twitter/library/card/property/TextToken;->styleId:I

    goto :goto_1

    :cond_2
    const-string/jumbo v1, "press_down_action_id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/property/TextToken;->pressDownActionId:I

    goto :goto_1

    :cond_3
    const-string/jumbo v1, "press_up_action_id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/property/TextToken;->pressUpActionId:I

    goto :goto_1

    :cond_4
    const-string/jumbo v1, "tap_action_id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/property/TextToken;->tapActionId:I

    goto :goto_1

    :cond_5
    const-string/jumbo v1, "long_press_action_id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/twitter/library/card/l;->S(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/property/TextToken;->longPressActionId:I

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_6
    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected static s(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/element/Box;
    .locals 4

    new-instance v2, Lcom/twitter/library/card/element/Box;

    invoke-direct {v2}, Lcom/twitter/library/card/element/Box;-><init>()V

    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, v2, Lcom/twitter/library/card/element/Box;->cornerRadius:F

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_3

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_3

    sget-object v3, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    const-string/jumbo v1, "background"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0}, Lcom/twitter/library/card/l;->D(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/Fill;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/element/Box;->background:Lcom/twitter/library/card/property/Fill;

    goto :goto_1

    :cond_1
    const-string/jumbo v1, "border"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p0}, Lcom/twitter/library/card/l;->J(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/Border;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/element/Box;->border:Lcom/twitter/library/card/property/Border;

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_3
    const-string/jumbo v1, "corner_radius"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/twitter/library/card/l;->U(Lcom/fasterxml/jackson/core/JsonParser;)F

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/element/Box;->cornerRadius:F

    goto :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    iget-object v0, v2, Lcom/twitter/library/card/element/Box;->background:Lcom/twitter/library/card/property/Fill;

    if-eqz v0, :cond_4

    iget-object v0, v2, Lcom/twitter/library/card/element/Box;->border:Lcom/twitter/library/card/property/Border;

    if-eqz v0, :cond_4

    iget v0, v2, Lcom/twitter/library/card/element/Box;->cornerRadius:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    new-instance v0, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v1, "invalid box"

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected static t(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/element/Text;
    .locals 11

    const/high16 v0, 0x7fc00000    # NaNf

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-instance v8, Lcom/twitter/library/card/element/Text;

    invoke-direct {v8}, Lcom/twitter/library/card/element/Text;-><init>()V

    iput v0, v8, Lcom/twitter/library/card/element/Text;->fontSize:F

    iput v0, v8, Lcom/twitter/library/card/element/Text;->lineHeight:F

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    move-object v7, v1

    move v4, v3

    move v5, v3

    move v6, v3

    move v1, v3

    :goto_0
    if-eqz v7, :cond_a

    sget-object v9, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v7, v9, :cond_a

    sget-object v9, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v7}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v7

    aget v7, v9, v7

    packed-switch v7, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v7

    goto :goto_0

    :pswitch_0
    const-string/jumbo v7, "color"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-static {p0}, Lcom/twitter/library/card/l;->F(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v6

    iput v6, v8, Lcom/twitter/library/card/element/Text;->color:I

    move v6, v2

    goto :goto_1

    :cond_1
    const-string/jumbo v7, "alignment_mode"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-static {p0}, Lcom/twitter/library/card/l;->K(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/Vector2;

    move-result-object v7

    iput-object v7, v8, Lcom/twitter/library/card/element/Text;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v9, "text"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-virtual {v8, v7}, Lcom/twitter/library/card/element/Text;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    const-string/jumbo v9, "font_name"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-virtual {v8, v7}, Lcom/twitter/library/card/element/Text;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    const-string/jumbo v7, "overflow"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-static {p0}, Lcom/twitter/library/card/l;->O(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v7

    iput v7, v8, Lcom/twitter/library/card/element/Text;->overflowMode:I

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v7

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->i()F

    move-result v9

    const-string/jumbo v10, "tokenized_text_id"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    iput v7, v8, Lcom/twitter/library/card/element/Text;->tokenizedTextId:I

    goto :goto_1

    :cond_5
    const-string/jumbo v10, "font_size"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-static {p0}, Lcom/twitter/library/card/l;->U(Lcom/fasterxml/jackson/core/JsonParser;)F

    move-result v7

    iput v7, v8, Lcom/twitter/library/card/element/Text;->fontSize:F

    goto :goto_1

    :cond_6
    const-string/jumbo v10, "max_lines"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    iput v7, v8, Lcom/twitter/library/card/element/Text;->maxLines:I

    move v5, v2

    goto/16 :goto_1

    :cond_7
    const-string/jumbo v7, "line_height"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    iput v9, v8, Lcom/twitter/library/card/element/Text;->lineHeight:F

    goto/16 :goto_1

    :pswitch_4
    const-string/jumbo v7, "font_bold"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->k()Z

    move-result v4

    iput-boolean v4, v8, Lcom/twitter/library/card/element/Text;->fontBold:Z

    move v4, v2

    goto/16 :goto_1

    :cond_8
    const-string/jumbo v7, "font_underline"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->k()Z

    move-result v3

    iput-boolean v3, v8, Lcom/twitter/library/card/element/Text;->fontUnderline:Z

    move v3, v2

    goto/16 :goto_1

    :cond_9
    const-string/jumbo v7, "font_italic"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->k()Z

    move-result v1

    iput-boolean v1, v8, Lcom/twitter/library/card/element/Text;->fontItalic:Z

    move v1, v2

    goto/16 :goto_1

    :pswitch_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_a
    invoke-virtual {v8}, Lcom/twitter/library/card/element/Text;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_b

    iget v0, v8, Lcom/twitter/library/card/element/Text;->fontSize:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_b

    if-eqz v6, :cond_b

    if-eqz v5, :cond_b

    iget v0, v8, Lcom/twitter/library/card/element/Text;->overflowMode:I

    if-eqz v0, :cond_b

    iget v0, v8, Lcom/twitter/library/card/element/Text;->lineHeight:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_b

    if-eqz v4, :cond_b

    if-eqz v3, :cond_b

    if-eqz v1, :cond_b

    iget-object v0, v8, Lcom/twitter/library/card/element/Text;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    if-nez v0, :cond_c

    :cond_b
    new-instance v0, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v1, "invalid text element"

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    return-object v8

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method protected static u(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/element/Image;
    .locals 7

    const/4 v1, 0x0

    const/4 v2, 0x1

    new-instance v5, Lcom/twitter/library/card/element/Image;

    invoke-direct {v5}, Lcom/twitter/library/card/element/Image;-><init>()V

    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, v5, Lcom/twitter/library/card/element/Image;->cornerRadius:F

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    const/4 v0, 0x0

    move-object v4, v3

    move v3, v1

    :goto_0
    if-eqz v4, :cond_5

    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v6, :cond_5

    sget-object v6, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v4}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v4

    aget v4, v6, v4

    packed-switch v4, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    goto :goto_0

    :pswitch_0
    const-string/jumbo v4, "alignment_mode"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {p0}, Lcom/twitter/library/card/l;->K(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/Vector2;

    move-result-object v4

    iput-object v4, v5, Lcom/twitter/library/card/element/Image;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v6, "loading_indicator"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string/jumbo v6, "none"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    iput v2, v5, Lcom/twitter/library/card/element/Image;->loadingIndicator:I

    goto :goto_1

    :cond_2
    const-string/jumbo v6, "placeholder"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    const/4 v4, 0x2

    iput v4, v5, Lcom/twitter/library/card/element/Image;->loadingIndicator:I

    goto :goto_1

    :cond_3
    new-instance v0, Lcom/twitter/library/card/CardParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unknown loading_indicator: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_2
    const-string/jumbo v4, "corner_radius"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {p0}, Lcom/twitter/library/card/l;->U(Lcom/fasterxml/jackson/core/JsonParser;)F

    move-result v4

    iput v4, v5, Lcom/twitter/library/card/element/Image;->cornerRadius:F

    goto :goto_1

    :pswitch_3
    const-string/jumbo v4, "preserve_aspect_ratio"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->k()Z

    move-result v3

    iput-boolean v3, v5, Lcom/twitter/library/card/element/Image;->preserveAspectRatio:Z

    move v3, v2

    goto :goto_1

    :cond_4
    const-string/jumbo v4, "fill_available_space"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->k()Z

    move-result v1

    iput-boolean v1, v5, Lcom/twitter/library/card/element/Image;->fillAvailableSpace:Z

    move v1, v2

    goto/16 :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_5
    iget-object v0, v5, Lcom/twitter/library/card/element/Image;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    if-eqz v0, :cond_6

    iget v0, v5, Lcom/twitter/library/card/element/Image;->loadingIndicator:I

    if-eqz v0, :cond_6

    iget v0, v5, Lcom/twitter/library/card/element/Image;->cornerRadius:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_6

    if-eqz v3, :cond_6

    if-nez v1, :cond_7

    :cond_6
    new-instance v0, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v1, "invalid image"

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    return-object v5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method protected static v(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/element/Player;
    .locals 3

    new-instance v1, Lcom/twitter/library/card/element/Player;

    invoke-direct {v1}, Lcom/twitter/library/card/element/Player;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_0

    sget-object v2, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_0
    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected static w(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/FormField;
    .locals 4

    new-instance v2, Lcom/twitter/library/card/property/FormField;

    invoke-direct {v2}, Lcom/twitter/library/card/property/FormField;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_1

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_1

    sget-object v3, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    const-string/jumbo v1, "name"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/property/FormField;->name:Ljava/lang/String;

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected static x(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/element/FormCheckbox;
    .locals 4

    new-instance v2, Lcom/twitter/library/card/element/FormCheckbox;

    invoke-direct {v2}, Lcom/twitter/library/card/element/FormCheckbox;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_2

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_2

    sget-object v3, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    const-string/jumbo v1, "form_field"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0}, Lcom/twitter/library/card/l;->w(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/FormField;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/element/FormCheckbox;->field:Lcom/twitter/library/card/property/FormField;

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_3
    const-string/jumbo v1, "value"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/element/FormCheckbox;->value:Ljava/lang/String;

    goto :goto_1

    :pswitch_4
    const-string/jumbo v1, "checked"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->k()Z

    move-result v1

    iput-boolean v1, v2, Lcom/twitter/library/card/element/FormCheckbox;->checked:Z

    goto :goto_1

    :pswitch_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method protected static y(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/FormSelectOption;
    .locals 4

    new-instance v2, Lcom/twitter/library/card/property/FormSelectOption;

    invoke-direct {v2}, Lcom/twitter/library/card/property/FormSelectOption;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_1

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_1

    sget-object v3, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    const-string/jumbo v1, "tokenized_text_id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v1

    iput v1, v2, Lcom/twitter/library/card/property/FormSelectOption;->tokenizedTextId:I

    goto :goto_1

    :pswitch_3
    const-string/jumbo v1, "value"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/property/FormSelectOption;->value:Ljava/lang/String;

    goto :goto_1

    :pswitch_4
    const-string/jumbo v1, "selected"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->k()Z

    move-result v1

    iput-boolean v1, v2, Lcom/twitter/library/card/property/FormSelectOption;->selected:Z

    goto :goto_1

    :pswitch_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_5
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method protected static z(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/element/FormSelect;
    .locals 9

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-instance v5, Lcom/twitter/library/card/element/FormSelect;

    invoke-direct {v5}, Lcom/twitter/library/card/element/FormSelect;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    if-eqz v0, :cond_9

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v4, :cond_9

    sget-object v4, Lcom/twitter/library/card/m;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v4, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    move-object v0, v1

    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v8, v0

    move-object v0, v1

    move-object v1, v8

    goto :goto_0

    :pswitch_0
    const-string/jumbo v0, "form_field"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/twitter/library/card/l;->w(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/FormField;

    move-result-object v0

    iput-object v0, v5, Lcom/twitter/library/card/element/FormSelect;->field:Lcom/twitter/library/card/property/FormField;

    move-object v0, v1

    goto :goto_1

    :cond_1
    const-string/jumbo v0, "color"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0}, Lcom/twitter/library/card/l;->F(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v0

    iput v0, v5, Lcom/twitter/library/card/element/FormSelect;->color:I

    move-object v0, v1

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v1

    goto :goto_1

    :pswitch_1
    const-string/jumbo v0, "options"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v4, v0

    :goto_2
    if-eqz v4, :cond_3

    move v0, v2

    :goto_3
    sget-object v7, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v7, :cond_4

    move v4, v2

    :goto_4
    and-int/2addr v0, v4

    if-eqz v0, :cond_5

    invoke-static {p0}, Lcom/twitter/library/card/l;->y(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/FormSelectOption;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v4, v0

    goto :goto_2

    :cond_3
    move v0, v3

    goto :goto_3

    :cond_4
    move v4, v3

    goto :goto_4

    :cond_5
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/twitter/library/card/property/FormSelectOption;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/card/property/FormSelectOption;

    iput-object v0, v5, Lcom/twitter/library/card/element/FormSelect;->options:[Lcom/twitter/library/card/property/FormSelectOption;

    move-object v0, v1

    goto :goto_1

    :cond_6
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v1

    goto :goto_1

    :pswitch_2
    const-string/jumbo v0, "font_size"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/twitter/library/card/l;->U(Lcom/fasterxml/jackson/core/JsonParser;)F

    move-result v0

    iput v0, v5, Lcom/twitter/library/card/element/FormSelect;->fontSize:F

    move-object v0, v1

    goto/16 :goto_1

    :pswitch_3
    const-string/jumbo v0, "font_name"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lcom/twitter/library/card/element/FormSelect;->fontName:Ljava/lang/String;

    move-object v0, v1

    goto/16 :goto_1

    :pswitch_4
    const-string/jumbo v0, "font_bold"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->k()Z

    move-result v0

    iput-boolean v0, v5, Lcom/twitter/library/card/element/FormSelect;->fontBold:Z

    move-object v0, v1

    goto/16 :goto_1

    :cond_7
    const-string/jumbo v0, "font_underline"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->k()Z

    move-result v0

    iput-boolean v0, v5, Lcom/twitter/library/card/element/FormSelect;->fontUnderline:Z

    move-object v0, v1

    goto/16 :goto_1

    :cond_8
    const-string/jumbo v0, "font_italic"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->k()Z

    move-result v0

    iput-boolean v0, v5, Lcom/twitter/library/card/element/FormSelect;->fontItalic:Z

    move-object v0, v1

    goto/16 :goto_1

    :pswitch_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_9
    return-object v5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_5
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method
