.class public abstract Lcom/twitter/library/card/CardDebugLog;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Z

.field private static b:I

.field private static c:I

.field private static d:Ljava/util/ArrayList;

.field private static e:Ljava/util/concurrent/CopyOnWriteArraySet;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Lcom/twitter/library/client/App;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "CARDS"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/library/card/CardDebugLog;->a:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/twitter/library/card/CardDebugLog;->d:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    sput-object v0, Lcom/twitter/library/card/CardDebugLog;->e:Ljava/util/concurrent/CopyOnWriteArraySet;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a()V
    .locals 1

    sget v0, Lcom/twitter/library/card/CardDebugLog;->b:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/twitter/library/card/CardDebugLog;->b:I

    return-void
.end method

.method public static declared-synchronized a(Lcom/twitter/library/card/CardDebugLog$Severity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    const-class v1, Lcom/twitter/library/card/CardDebugLog;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/twitter/library/card/CardDebugLog;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_0
    sget-object v0, Lcom/twitter/library/card/CardDebugLog;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v2, 0x64

    if-le v0, v2, :cond_0

    sget-object v0, Lcom/twitter/library/card/CardDebugLog;->d:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    new-instance v2, Lcom/twitter/library/card/i;

    invoke-direct {v2, p0, p1, p2}, Lcom/twitter/library/card/i;-><init>(Lcom/twitter/library/card/CardDebugLog$Severity;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/twitter/library/card/CardDebugLog;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/twitter/library/card/CardDebugLog;->e:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/h;

    invoke-interface {v0, v2}, Lcom/twitter/library/card/h;->a(Lcom/twitter/library/card/i;)V

    goto :goto_1

    :cond_1
    sget-boolean v0, Lcom/twitter/library/card/CardDebugLog;->a:Z

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/twitter/library/card/CardDebugLog;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/twitter/library/card/g;->a:[I

    invoke-virtual {p0}, Lcom/twitter/library/card/CardDebugLog$Severity;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    const-string/jumbo v0, "CARDS"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    :goto_2
    monitor-exit v1

    return-void

    :pswitch_0
    :try_start_2
    const-string/jumbo v0, "CARDS"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :pswitch_1
    const-string/jumbo v0, "CARDS"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :pswitch_2
    const-string/jumbo v0, "CARDS"

    invoke-static {v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :pswitch_3
    const-string/jumbo v0, "CARDS"

    invoke-static {v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Lcom/twitter/library/card/h;)V
    .locals 1

    sget-object v0, Lcom/twitter/library/card/CardDebugLog;->e:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lcom/twitter/library/card/CardDebugLog$Severity;->a:Lcom/twitter/library/card/CardDebugLog$Severity;

    invoke-static {v0, p0, p1}, Lcom/twitter/library/card/CardDebugLog;->a(Lcom/twitter/library/card/CardDebugLog$Severity;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static b()V
    .locals 1

    sget v0, Lcom/twitter/library/card/CardDebugLog;->b:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/twitter/library/card/CardDebugLog;->b:I

    return-void
.end method

.method public static b(Lcom/twitter/library/card/h;)V
    .locals 1

    sget-object v0, Lcom/twitter/library/card/CardDebugLog;->e:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/CopyOnWriteArraySet;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lcom/twitter/library/card/CardDebugLog$Severity;->b:Lcom/twitter/library/card/CardDebugLog$Severity;

    invoke-static {v0, p0, p1}, Lcom/twitter/library/card/CardDebugLog;->a(Lcom/twitter/library/card/CardDebugLog$Severity;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static c()V
    .locals 1

    sget v0, Lcom/twitter/library/card/CardDebugLog;->c:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/twitter/library/card/CardDebugLog;->c:I

    return-void
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lcom/twitter/library/card/CardDebugLog$Severity;->c:Lcom/twitter/library/card/CardDebugLog$Severity;

    invoke-static {v0, p0, p1}, Lcom/twitter/library/card/CardDebugLog;->a(Lcom/twitter/library/card/CardDebugLog$Severity;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static d()V
    .locals 1

    sget v0, Lcom/twitter/library/card/CardDebugLog;->c:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/twitter/library/card/CardDebugLog;->c:I

    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lcom/twitter/library/card/CardDebugLog$Severity;->d:Lcom/twitter/library/card/CardDebugLog$Severity;

    invoke-static {v0, p0, p1}, Lcom/twitter/library/card/CardDebugLog;->a(Lcom/twitter/library/card/CardDebugLog$Severity;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static e()Z
    .locals 1

    sget v0, Lcom/twitter/library/card/CardDebugLog;->b:I

    if-lez v0, :cond_0

    invoke-static {}, Lcom/twitter/library/client/App;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f()Z
    .locals 1

    sget v0, Lcom/twitter/library/card/CardDebugLog;->c:I

    if-lez v0, :cond_0

    invoke-static {}, Lcom/twitter/library/client/App;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized g()V
    .locals 3

    const-class v1, Lcom/twitter/library/card/CardDebugLog;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/card/CardDebugLog;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    sget-object v0, Lcom/twitter/library/card/CardDebugLog;->e:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/h;

    invoke-interface {v0}, Lcom/twitter/library/card/h;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    monitor-exit v1

    return-void
.end method

.method public static h()Ljava/util/ArrayList;
    .locals 1

    sget-object v0, Lcom/twitter/library/card/CardDebugLog;->d:Ljava/util/ArrayList;

    return-object v0
.end method
