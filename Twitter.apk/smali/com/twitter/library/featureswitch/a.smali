.class public Lcom/twitter/library/featureswitch/a;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Landroid/content/Context;

.field private static b:Z

.field private static final c:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string/jumbo v0, "^https?://\\S+\\/parser$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/featureswitch/a;->c:Ljava/util/regex/Pattern;

    return-void
.end method

.method public static A()Ljava/lang/Boolean;
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "login_challenge_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static B()I
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "login_challenge_polling_interval"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static C()F
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "login_challenge_polling_backoff"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public static D()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "native_password_reset_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static E()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "tweet_inline_actions_counts_all_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static F()Z
    .locals 3

    const/4 v0, 0x0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v1

    const-string/jumbo v2, "tweet_inline_actions_counts_all_enabled"

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v1

    const-string/jumbo v2, "tweet_inline_actions_counts_descendants_enabled"

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static G()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "descendant_reply_count_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static H()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "samsung_spen_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static I()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "public_conversations_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static J()Z
    .locals 3

    const/4 v0, 0x0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->I()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v1

    const-string/jumbo v2, "public_conversations_replies_polling_enabled"

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static K()Ljava/lang/Boolean;
    .locals 3

    const/4 v0, 0x0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->I()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v1

    const-string/jumbo v2, "public_conversations_user_presence_enabled"

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static L()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "custom_timelines_handle_link_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static M()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "custom_timelines_list_entry_point_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static N()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "custom_timelines_create_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static O()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "custom_timelines_destroy_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static P()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "custom_timelines_update_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static Q()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "custom_timelines_curate_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static R()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "custom_timelines_follow_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static S()I
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "custom_timelines_name_max_length"

    const/16 v2, 0x19

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static T()I
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "custom_timelines_description_max_length"

    const/16 v2, 0xa0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static U()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "explore_timeline_middle_tab_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static V()I
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "explore_timeline_refresh_period_minutes"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static W()Z
    .locals 3

    const/4 v0, 0x0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v1

    const-string/jumbo v2, "readability_v2_enabled"

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/twitter/library/featureswitch/a;->c:Ljava/util/regex/Pattern;

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->X()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static X()Ljava/lang/String;
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "readability_v2_proxy_uri"

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static Y()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "signed_out_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static Z()Ljava/lang/String;
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "signed_out_custom_timeline_default"

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a()Lcom/twitter/library/featureswitch/b;
    .locals 2

    sget-boolean v0, Lcom/twitter/library/featureswitch/a;->b:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "FeatureSwitches.initialize() must be called first."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    sget-object v0, Lcom/twitter/library/featureswitch/a;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/featureswitch/b;->a(Landroid/content/Context;)Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "olympic_pivot_cell_title_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "olympic_pivot_cell_title_default"

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    sput-object p0, Lcom/twitter/library/featureswitch/a;->a:Landroid/content/Context;

    const/4 v0, 0x1

    sput-boolean v0, Lcom/twitter/library/featureswitch/a;->b:Z

    return-void
.end method

.method public static declared-synchronized a(Lcom/twitter/library/client/Session;)V
    .locals 4

    const-class v1, Lcom/twitter/library/featureswitch/a;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/featureswitch/b;->c(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Lcom/twitter/library/featureswitch/g;)V
    .locals 1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/library/featureswitch/b;->a(Lcom/twitter/library/featureswitch/g;)V

    return-void
.end method

.method public static a(J)Z
    .locals 2

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "vit_notable_event_setting_for_verified_user_enabled"

    invoke-virtual {v0, p0, p1, v1}, Lcom/twitter/library/featureswitch/b;->a(JLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static aA()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "two_pane_dms_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static aB()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "compose_typeahead_triggering_small_layout_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static aC()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "compose_typeahead_triggering_button_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static aD()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "compose_typeahead_triggering_word_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static aE()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "magic_recs_in_notifications_timeline_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static aF()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "search_features_reverse_bolding_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static aG()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "search_features_tap_ahead_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static aH()Ljava/lang/Boolean;
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "photos_async_upload_enabled"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static aI()Ljava/lang/Boolean;
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "photos_segmented_upload_enabled"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static aJ()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "mute_filters_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static aK()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "action_prompts_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static aL()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "push_landing_pages_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static aM()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "inline_reply_actions_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static aN()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "remove_reply_addressing_from_140_count_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static aO()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "events_bellbird_redesign_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static aP()Ljava/util/ArrayList;
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "events_bellbird_redesign_types"

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public static aQ()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "translate_tweet_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static aR()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "world_cup_nux_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static aS()Z
    .locals 4

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "perftown_default_sampling_rate"

    const/16 v2, 0x64

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;I)I

    move-result v0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v1

    const-string/jumbo v2, "perftown_enabled"

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3, v0}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;II)Z

    move-result v0

    return v0
.end method

.method public static aT()Ljava/lang/String;
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "android_exp_v2_aa_testing_2080"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static aa()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "digits_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static ab()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "dm_event_api_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static ac()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "dm_anyone_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static ad()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "dm_event_api_groups_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static ae()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "poi_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static af()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "poi_info_in_timeline"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static ag()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "poi_autotagging_location"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static ah()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "poi_rich_tweet_detail"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static ai()I
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "poi_top_count"

    const/16 v2, 0x19

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static aj()Ljava/lang/Boolean;
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "extended_image_import_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static ak()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "recommendations_opt_out_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static al()I
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "recommendations_opt_out_max_views"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static am()Ljava/util/ArrayList;
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "recommendations_opt_out_supported_types"

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public static an()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "cards_forward_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static ao()I
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "cards_forward_height"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static ap()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "gallery_bar_experiment_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static aq()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "search_features_timeline_filter_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static ar()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "search_features_timeline_gallery_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static as()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "animated_content_composer_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static at()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "context_reason_in_timeline_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static au()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "animated_content_media_forward_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static av()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "android_network_http_url_connection_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static aw()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "vit_default_filtering_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static ax()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "vit_show_filter_banner_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static ay()Ljava/lang/Boolean;
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "favorite_people_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static az()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "spdy_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static b(Lcom/twitter/library/featureswitch/g;)V
    .locals 1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/library/featureswitch/b;->b(Lcom/twitter/library/featureswitch/g;)V

    return-void
.end method

.method public static b()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "amplify_fullscreen_engagement_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static b(J)Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "vit_notable_event_setting_for_verified_user_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, p0, p1, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(JLjava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static c()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "amplify_promoted_follow_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static c(J)Z
    .locals 2

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "vit_notable_event_setting_enabled"

    invoke-virtual {v0, p0, p1, v1}, Lcom/twitter/library/featureswitch/b;->a(JLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static d()Ljava/lang/String;
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "amplify_video_bitrate_default"

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static d(J)Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "vit_notable_event_setting_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, p0, p1, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(JLjava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static e()Ljava/util/ArrayList;
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "amplify_video_bitrate_buckets"

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public static f()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "promoted_cards_media_forward_appdownload_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static g()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "promoted_cards_media_forward_ctc_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static h()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "promoted_cards_media_forward_leadgen_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static i()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "promoted_cards_media_forward_website_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static j()Ljava/util/ArrayList;
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "amplify_video_hosts"

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public static k()Ljava/util/ArrayList;
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "vine_video_hosts"

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public static l()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "strip_amplify_urls_from_tweet_text_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static m()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "vit_notification_filters_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static n()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "vit_notification_filters_role_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static o()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "oem_launcher_badge_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static p()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "events_see_it_button_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static q()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "olympic_pivot_cell_discover"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static r()Ljava/lang/String;
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "olympic_pivot_cell_event_id"

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static s()Ljava/util/ArrayList;
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "olympic_pivot_cell_locale"

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public static t()Ljava/lang/String;
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "olympic_pivot_cell_query"

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static u()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "search_concierge_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static v()I
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "search_concierge_max_oneclick"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static w()J
    .locals 4

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "search_concierge_oneclick_ttl_hours"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0x36ee80

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public static x()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "multi_photo_render_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static y()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "multi_photo_compose_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static z()Z
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    const-string/jumbo v1, "native_login_verification_enabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
