.class public Lcom/twitter/library/featureswitch/FeatureSwitchesValue$FeatureSwitchesImpression;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x43fac15fafdb007eL


# instance fields
.field private mBucket:Ljava/lang/String;

.field private mExperimentKey:Ljava/lang/String;

.field private mVersion:I


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/featureswitch/FeatureSwitchesValue$FeatureSwitchesImpression;->mExperimentKey:Ljava/lang/String;

    iput p2, p0, Lcom/twitter/library/featureswitch/FeatureSwitchesValue$FeatureSwitchesImpression;->mVersion:I

    iput-object p3, p0, Lcom/twitter/library/featureswitch/FeatureSwitchesValue$FeatureSwitchesImpression;->mBucket:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/featureswitch/FeatureSwitchesValue$FeatureSwitchesImpression;->mExperimentKey:Ljava/lang/String;

    return-object v0
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/featureswitch/FeatureSwitchesValue$FeatureSwitchesImpression;->mVersion:I

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/featureswitch/FeatureSwitchesValue$FeatureSwitchesImpression;->mBucket:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/featureswitch/FeatureSwitchesValue$FeatureSwitchesImpression;->mExperimentKey:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/featureswitch/FeatureSwitchesValue$FeatureSwitchesImpression;->mExperimentKey:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/twitter/library/featureswitch/FeatureSwitchesValue$FeatureSwitchesImpression;->mVersion:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/library/featureswitch/FeatureSwitchesValue$FeatureSwitchesImpression;->mBucket:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/library/featureswitch/FeatureSwitchesValue$FeatureSwitchesImpression;->mBucket:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
