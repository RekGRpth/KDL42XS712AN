.class public Lcom/twitter/library/featureswitch/b;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static b:Lcom/twitter/library/featureswitch/b;


# instance fields
.field public a:Z

.field private c:J

.field private d:I

.field private final e:Ljava/util/concurrent/ConcurrentHashMap;

.field private final f:Ljava/util/HashMap;

.field private final g:Ljava/util/HashMap;

.field private final h:Ljava/util/ArrayList;

.field private final i:Ljava/util/concurrent/ConcurrentHashMap;

.field private j:Lcom/twitter/library/featureswitch/FeatureSwitchesManifest;

.field private k:Landroid/content/Context;

.field private l:Landroid/os/Handler;

.field private m:Z

.field private n:Ljava/lang/Runnable;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/twitter/library/featureswitch/b;->a:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/library/featureswitch/b;->d:I

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/featureswitch/b;->e:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/featureswitch/b;->f:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/featureswitch/b;->g:Ljava/util/HashMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/featureswitch/b;->h:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/featureswitch/b;->i:Ljava/util/concurrent/ConcurrentHashMap;

    iput-boolean v1, p0, Lcom/twitter/library/featureswitch/b;->m:Z

    new-instance v0, Lcom/twitter/library/featureswitch/c;

    invoke-direct {v0, p0}, Lcom/twitter/library/featureswitch/c;-><init>(Lcom/twitter/library/featureswitch/b;)V

    iput-object v0, p0, Lcom/twitter/library/featureswitch/b;->n:Ljava/lang/Runnable;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/library/featureswitch/b;->l:Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/featureswitch/b;->k:Landroid/content/Context;

    new-instance v0, Lcom/twitter/library/featureswitch/d;

    invoke-direct {v0, p0}, Lcom/twitter/library/featureswitch/d;-><init>(Lcom/twitter/library/featureswitch/b;)V

    invoke-static {v0}, Lcom/twitter/library/client/l;->a(Lcom/twitter/library/client/k;)V

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/twitter/library/featureswitch/b;
    .locals 2

    const-class v1, Lcom/twitter/library/featureswitch/b;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/featureswitch/b;->b:Lcom/twitter/library/featureswitch/b;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/library/featureswitch/b;

    invoke-direct {v0, p0}, Lcom/twitter/library/featureswitch/b;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/library/featureswitch/b;->b:Lcom/twitter/library/featureswitch/b;

    :cond_0
    sget-object v0, Lcom/twitter/library/featureswitch/b;->b:Lcom/twitter/library/featureswitch/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(JLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->i:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p3}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->i:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/featureswitch/FeatureSwitchesValue;

    invoke-virtual {v0}, Lcom/twitter/library/featureswitch/FeatureSwitchesValue;->a()Ljava/lang/Object;

    move-result-object p4

    :cond_0
    :goto_0
    return-object p4

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/featureswitch/FeatureSwitchesConfig;

    if-nez v0, :cond_2

    iget-object v1, p0, Lcom/twitter/library/featureswitch/b;->k:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/twitter/library/client/aa;->a(J)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    cmp-long v1, v1, p1

    if-nez v1, :cond_2

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/library/featureswitch/b;->a(JZ)V

    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/featureswitch/FeatureSwitchesConfig;

    :cond_2
    invoke-virtual {v0, p3}, Lcom/twitter/library/featureswitch/FeatureSwitchesConfig;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, p3}, Lcom/twitter/library/featureswitch/FeatureSwitchesConfig;->c(Ljava/lang/String;)Lcom/twitter/library/featureswitch/FeatureSwitchesValue;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/library/featureswitch/b;->a(JLcom/twitter/library/featureswitch/FeatureSwitchesValue;)V

    invoke-virtual {v0}, Lcom/twitter/library/featureswitch/FeatureSwitchesValue;->a()Ljava/lang/Object;

    move-result-object p4

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/library/featureswitch/b;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->f:Ljava/util/HashMap;

    return-object v0
.end method

.method private a(JLcom/twitter/library/featureswitch/FeatureSwitchesValue;)V
    .locals 2

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lcom/twitter/library/featureswitch/FeatureSwitchesValue;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->k:Landroid/content/Context;

    invoke-virtual {p3}, Lcom/twitter/library/featureswitch/FeatureSwitchesValue;->c()Lcom/twitter/library/featureswitch/FeatureSwitchesValue$FeatureSwitchesImpression;

    move-result-object v1

    invoke-static {v0, v1, p1, p2}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/featureswitch/FeatureSwitchesValue$FeatureSwitchesImpression;J)V

    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 2

    invoke-static {}, Lcom/twitter/library/client/App;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "FS"

    invoke-static {v0, p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-boolean v0, p0, Lcom/twitter/library/featureswitch/b;->m:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->l:Landroid/os/Handler;

    new-instance v1, Lcom/twitter/library/featureswitch/f;

    invoke-direct {v1, p0, p1}, Lcom/twitter/library/featureswitch/f;-><init>(Lcom/twitter/library/featureswitch/b;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget-boolean v0, p0, Lcom/twitter/library/featureswitch/b;->m:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/library/featureswitch/b;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Lcom/crashlytics/android/d;->a(Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/library/featureswitch/b;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->k:Landroid/content/Context;

    return-object v0
.end method

.method private d()Z
    .locals 2

    iget v0, p0, Lcom/twitter/library/featureswitch/b;->d:I

    if-gez v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->k:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/network/aa;->a(Landroid/content/Context;)Lcom/twitter/library/network/aa;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/network/aa;->f:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/library/util/Util;->i(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "feature_switches_configs_crashlytics_enabled"

    invoke-static {v0, v1}, Lcom/twitter/library/api/Decider;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/featureswitch/b;->d:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget v0, p0, Lcom/twitter/library/featureswitch/b;->d:I

    if-lez v0, :cond_1

    iget v0, p0, Lcom/twitter/library/featureswitch/b;->d:I

    const/16 v1, 0xa

    if-ge v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private e()Z
    .locals 4

    :try_start_0
    invoke-direct {p0}, Lcom/twitter/library/featureswitch/b;->d()Z

    move-result v0

    iget-wide v1, p0, Lcom/twitter/library/featureswitch/b;->c:J

    const-string/jumbo v3, "feature_switches_configs_crashlytics_enabled"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, v1, v2, v3, v0}, Lcom/twitter/library/featureswitch/b;->a(JLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()J
    .locals 4

    const-string/jumbo v0, "feature_switches_configs_wait_before_kill_minutes"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0xea60

    mul-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public a(JLjava/lang/String;F)F
    .locals 3

    :try_start_0
    invoke-static {p4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/library/featureswitch/b;->a(JLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result p4

    :goto_0
    return p4

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid FeatureSwitch value. Key: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;F)F
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/featureswitch/b;->c:J

    invoke-virtual {p0, v0, v1, p1, p2}, Lcom/twitter/library/featureswitch/b;->a(JLjava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public a(JLjava/lang/String;I)I
    .locals 3

    :try_start_0
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/library/featureswitch/b;->a(JLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result p4

    :goto_0
    return p4

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid FeatureSwitch value. Key: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;I)I
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/featureswitch/b;->c:J

    invoke-virtual {p0, v0, v1, p1, p2}, Lcom/twitter/library/featureswitch/b;->a(JLjava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public declared-synchronized a()Lcom/twitter/library/featureswitch/FeatureSwitchesManifest;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->j:Lcom/twitter/library/featureswitch/FeatureSwitchesManifest;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->k:Landroid/content/Context;

    const-string/jumbo v1, "feature_switch_manifest"

    invoke-static {v0, v1}, Lcom/twitter/library/featureswitch/FeatureSwitchesManifest;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/twitter/library/featureswitch/FeatureSwitchesManifest;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/featureswitch/b;->j:Lcom/twitter/library/featureswitch/FeatureSwitchesManifest;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->j:Lcom/twitter/library/featureswitch/FeatureSwitchesManifest;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    :catch_0
    move-exception v0

    :try_start_3
    const-string/jumbo v1, "Failed to read feature switches manifest."

    invoke-direct {p0, v1, v0}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(JLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    :try_start_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/library/featureswitch/b;->a(JLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    :goto_0
    return-object p4

    :cond_0
    move-object p4, v0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid FeatureSwitch value. Key: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/featureswitch/b;->c:J

    invoke-virtual {p0, v0, v1, p1, p2}, Lcom/twitter/library/featureswitch/b;->a(JLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(JLjava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 3

    :try_start_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/library/featureswitch/b;->a(JLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    :goto_0
    return-object p4

    :cond_0
    move-object p4, v0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid FeatureSwitch value. Key: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/featureswitch/b;->c:J

    invoke-virtual {p0, v0, v1, p1, p2}, Lcom/twitter/library/featureswitch/b;->a(JLjava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public a(JLcom/twitter/library/featureswitch/FeatureSwitchesConfig;)V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/library/featureswitch/b;->a()Lcom/twitter/library/featureswitch/FeatureSwitchesManifest;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/twitter/library/featureswitch/b;->a(JLcom/twitter/library/featureswitch/FeatureSwitchesManifest;Lcom/twitter/library/featureswitch/FeatureSwitchesConfig;)V

    return-void
.end method

.method public declared-synchronized a(JLcom/twitter/library/featureswitch/FeatureSwitchesManifest;Lcom/twitter/library/featureswitch/FeatureSwitchesConfig;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/twitter/library/featureswitch/b;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    if-eqz p3, :cond_1

    if-nez p4, :cond_2

    :cond_1
    :try_start_1
    const-string/jumbo v0, "Manifest or fetchedConfig cannot be null"

    new-instance v1, Ljava/lang/Exception;

    invoke-direct {v1}, Ljava/lang/Exception;-><init>()V

    invoke-direct {p0, v0, v1}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/featureswitch/FeatureSwitchesConfig;

    iget-object v1, p0, Lcom/twitter/library/featureswitch/b;->k:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    cmp-long v1, p1, v1

    if-eqz v1, :cond_3

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/featureswitch/b;->c(J)V

    goto :goto_0

    :cond_3
    if-eqz v0, :cond_5

    invoke-virtual {v0, p4}, Lcom/twitter/library/featureswitch/FeatureSwitchesConfig;->a(Lcom/twitter/library/featureswitch/FeatureSwitchesConfig;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, p3, v0}, Lcom/twitter/library/featureswitch/b;->a(Lcom/twitter/library/featureswitch/FeatureSwitchesManifest;Ljava/util/ArrayList;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/featureswitch/b;->a:Z

    goto :goto_0

    :cond_4
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/library/featureswitch/b;->a(JZ)V

    goto :goto_0

    :cond_5
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/library/featureswitch/b;->a(JZ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized a(JZ)V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p3, :cond_1

    :cond_0
    :try_start_1
    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/featureswitch/b;->g(J)Ljava/io/File;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/featureswitch/b;->k:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/twitter/library/featureswitch/FeatureSwitchesConfig;->a(Landroid/content/Context;Ljava/io/File;)Lcom/twitter/library/featureswitch/FeatureSwitchesConfig;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-wide v1, v0, Lcom/twitter/library/featureswitch/FeatureSwitchesConfig;->mUpdatedAt:J

    invoke-static {}, Lcom/twitter/library/client/App;->i()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-lez v1, :cond_2

    iget-object v1, p0, Lcom/twitter/library/featureswitch/b;->g:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_3

    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->k:Landroid/content/Context;

    sget v2, Lik;->feature_switch_overrides:I

    invoke-static {v0, v2}, Lcom/twitter/library/featureswitch/FeatureSwitchesConfig;->a(Landroid/content/Context;I)Lcom/twitter/library/featureswitch/FeatureSwitchesConfig;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/twitter/library/featureswitch/FeatureSwitchesConfig;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/twitter/library/featureswitch/FeatureSwitchesConfig;->c(Ljava/lang/String;)Lcom/twitter/library/featureswitch/FeatureSwitchesValue;

    move-result-object v4

    invoke-virtual {v1, v0, v4}, Lcom/twitter/library/featureswitch/FeatureSwitchesConfig;->a(Ljava/lang/String;Lcom/twitter/library/featureswitch/FeatureSwitchesValue;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_2
    const-string/jumbo v1, "Failed to load featureSwitches."

    invoke-direct {p0, v1, v0}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :cond_2
    :try_start_3
    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->k:Landroid/content/Context;

    const-string/jumbo v1, "feature_switch_manifest"

    invoke-static {v0, v1}, Lcom/twitter/library/featureswitch/FeatureSwitchesManifest;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/twitter/library/featureswitch/FeatureSwitchesManifest;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/featureswitch/b;->g:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_1

    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/featureswitch/g;

    invoke-interface {v0, p1, p2}, Lcom/twitter/library/featureswitch/g;->a(J)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/twitter/library/featureswitch/g;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(J)Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->g:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->g:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(JLjava/lang/String;)Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/featureswitch/FeatureSwitchesConfig;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p3}, Lcom/twitter/library/featureswitch/FeatureSwitchesConfig;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(JLjava/lang/String;II)Z
    .locals 3

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/featureswitch/b;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/featureswitch/b;->a(JLjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/library/featureswitch/b;->c:J

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, p3, v2}, Lcom/twitter/library/featureswitch/b;->a(JLjava/lang/String;Z)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    if-ne p4, v0, :cond_1

    :goto_1
    invoke-static {p1, p2, p3, p5}, Lcom/twitter/library/api/Decider;->a(JLjava/lang/String;I)Z

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->k:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/util/Util;->i(Landroid/content/Context;)J

    move-result-wide p1

    goto :goto_1
.end method

.method public a(JLjava/lang/String;Z)Z
    .locals 3

    :try_start_0
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/library/featureswitch/b;->a(JLjava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result p4

    :goto_0
    return p4

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid FeatureSwitch value. Key: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/featureswitch/FeatureSwitchesManifest;Ljava/util/ArrayList;)Z
    .locals 3

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1}, Lcom/twitter/library/featureswitch/FeatureSwitchesManifest;->b()Ljava/util/HashSet;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;II)Z
    .locals 6

    iget-wide v1, p0, Lcom/twitter/library/featureswitch/b;->c:J

    move-object v0, p0

    move-object v3, p1

    move v4, p2

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/featureswitch/b;->a(JLjava/lang/String;II)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;Z)Z
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/featureswitch/b;->c:J

    invoke-virtual {p0, v0, v1, p1, p2}, Lcom/twitter/library/featureswitch/b;->a(JLjava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 6

    iget-object v1, p0, Lcom/twitter/library/featureswitch/b;->n:Ljava/lang/Runnable;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/twitter/library/featureswitch/b;->a:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/library/featureswitch/b;->f()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->l:Landroid/os/Handler;

    iget-object v4, p0, Lcom/twitter/library/featureswitch/b;->n:Ljava/lang/Runnable;

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized b(J)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lcom/twitter/library/featureswitch/b;->c:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/featureswitch/b;->a:Z

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/library/featureswitch/b;->a(JZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(JLcom/twitter/library/featureswitch/FeatureSwitchesConfig;)V
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/featureswitch/b;->g(J)Ljava/io/File;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/twitter/library/featureswitch/FeatureSwitchesConfig;->a(Ljava/io/File;)V

    return-void
.end method

.method public b(Lcom/twitter/library/featureswitch/g;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public c()V
    .locals 3

    iget-object v1, p0, Lcom/twitter/library/featureswitch/b;->n:Ljava/lang/Runnable;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->l:Landroid/os/Handler;

    iget-object v2, p0, Lcom/twitter/library/featureswitch/b;->n:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized c(J)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/twitter/library/featureswitch/b;->c:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/library/featureswitch/b;->a(JZ)V

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/featureswitch/b;->d(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    const-string/jumbo v0, "loadFeatureSwitchesForNonActiveUser should not be called for a current user"

    new-instance v1, Ljava/lang/Exception;

    invoke-direct {v1}, Ljava/lang/Exception;-><init>()V

    invoke-direct {p0, v0, v1}, Lcom/twitter/library/featureswitch/b;->a(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d(J)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->f:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/c;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/library/util/c;

    const-wide/32 v1, 0x36ee80

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/util/c;-><init>(J)V

    iget-object v1, p0, Lcom/twitter/library/featureswitch/b;->f:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {v0}, Lcom/twitter/library/util/c;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/featureswitch/b;->e(J)Ljava/lang/String;

    :cond_1
    return-void
.end method

.method public e(J)Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->f:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/c;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/library/util/c;

    const-wide/32 v1, 0x36ee80

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/util/c;-><init>(J)V

    iget-object v1, p0, Lcom/twitter/library/featureswitch/b;->f:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {v0}, Lcom/twitter/library/util/c;->b()V

    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->k:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/client/aa;->a(J)Lcom/twitter/library/client/Session;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/featureswitch/b;->k:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/library/client/w;->a(Landroid/content/Context;)Lcom/twitter/library/client/w;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/api/j;

    iget-object v3, p0, Lcom/twitter/library/featureswitch/b;->k:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lcom/twitter/library/api/j;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Lcom/twitter/library/api/j;->c(Z)Lcom/twitter/library/service/b;

    move-result-object v0

    new-instance v2, Lcom/twitter/library/featureswitch/e;

    invoke-direct {v2, p0, p1, p2}, Lcom/twitter/library/featureswitch/e;-><init>(Lcom/twitter/library/featureswitch/b;J)V

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/client/w;->a(Lcom/twitter/library/service/b;Lcom/twitter/library/service/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f(J)V
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/featureswitch/b;->g(J)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    return-void
.end method

.method public g(J)Ljava/io/File;
    .locals 4

    iget-object v0, p0, Lcom/twitter/library/featureswitch/b;->k:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/util/Util;->d(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "feature_switches_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method
