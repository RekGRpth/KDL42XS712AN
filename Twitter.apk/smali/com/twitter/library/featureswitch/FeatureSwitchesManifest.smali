.class public Lcom/twitter/library/featureswitch/FeatureSwitchesManifest;
.super Lcom/twitter/library/featureswitch/FeatureSwitchesConfig;
.source "Twttr"


# static fields
.field private static final serialVersionUID:J = -0x33bac0dfbb8a784bL


# instance fields
.field private mAvailableExperiments:Ljava/util/HashSet;

.field private mRequiresRestart:Ljava/util/HashSet;


# direct methods
.method public constructor <init>(Ljava/util/HashMap;Ljava/util/HashSet;Ljava/util/HashSet;Ljava/util/HashSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/featureswitch/FeatureSwitchesConfig;-><init>(Ljava/util/HashMap;Ljava/util/HashSet;)V

    iput-object p3, p0, Lcom/twitter/library/featureswitch/FeatureSwitchesManifest;->mRequiresRestart:Ljava/util/HashSet;

    iput-object p4, p0, Lcom/twitter/library/featureswitch/FeatureSwitchesManifest;->mAvailableExperiments:Ljava/util/HashSet;

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Lcom/twitter/library/featureswitch/FeatureSwitchesManifest;
    .locals 3

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string/jumbo v1, "raw"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {p0, v0}, Lcom/twitter/library/featureswitch/FeatureSwitchesManifest;->b(Landroid/content/Context;I)Lcom/twitter/library/featureswitch/FeatureSwitchesManifest;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;I)Lcom/twitter/library/featureswitch/FeatureSwitchesManifest;
    .locals 3

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    sget-object v2, Lcom/twitter/library/featureswitch/FeatureSwitchesManifest;->a:Lcom/fasterxml/jackson/core/JsonFactory;

    invoke-virtual {v2, v0}, Lcom/fasterxml/jackson/core/JsonFactory;->b(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    invoke-static {v1}, Lcom/twitter/library/api/ap;->W(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/featureswitch/FeatureSwitchesManifest;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    throw v0
.end method


# virtual methods
.method public b()Ljava/util/HashSet;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/featureswitch/FeatureSwitchesManifest;->mRequiresRestart:Ljava/util/HashSet;

    return-object v0
.end method
