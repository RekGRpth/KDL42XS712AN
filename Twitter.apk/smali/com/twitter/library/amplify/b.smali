.class Lcom/twitter/library/amplify/b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# instance fields
.field final synthetic a:Lcom/twitter/library/amplify/AmplifyMediaPlayer;


# direct methods
.method constructor <init>(Lcom/twitter/library/amplify/AmplifyMediaPlayer;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/amplify/b;->a:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/twitter/library/amplify/b;->a:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    sget-object v1, Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;->d:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    iput-object v1, v0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->c:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    iget-object v0, p0, Lcom/twitter/library/amplify/b;->a:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    iget-object v0, v0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a:Lcom/twitter/library/amplify/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/b;->a:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    iget-object v0, v0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a:Lcom/twitter/library/amplify/i;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v2

    invoke-interface {v0, v1, v2, v3, v3}, Lcom/twitter/library/amplify/i;->a(IIZZ)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/amplify/b;->a:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    iget-object v0, v0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->d:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    sget-object v1, Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;->e:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/library/amplify/b;->a:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v3}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Lcom/twitter/library/amplify/model/AmplifyVideo;Z)V

    :cond_1
    return-void
.end method
