.class public Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;
.super Landroid/view/SurfaceView;
.source "Twttr"


# instance fields
.field protected a:I

.field protected b:I

.field private c:Lcom/twitter/library/amplify/AmplifyPlayer;

.field private d:Landroid/view/SurfaceHolder$Callback;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/amplify/AmplifyPlayer;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/twitter/library/amplify/l;

    invoke-direct {v0, p0}, Lcom/twitter/library/amplify/l;-><init>(Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->d:Landroid/view/SurfaceHolder$Callback;

    iput-object p2, p0, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-direct {p0}, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->a()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;)Lcom/twitter/library/amplify/AmplifyPlayer;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    return-object v0
.end method

.method private a()V
    .locals 3

    const/4 v2, 0x1

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->a:I

    iput v0, p0, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->b:I

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->d:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    :cond_0
    invoke-virtual {p0, v2}, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->setFocusable(Z)V

    invoke-virtual {p0, v2}, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->setFocusableInTouchMode(Z)V

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->requestFocus()Z

    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 1

    iput p1, p0, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->a:I

    iput p2, p0, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->b:I

    iget v0, p0, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->a:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->b:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->requestLayout()V

    :cond_0
    return-void
.end method

.method public getVideoHeight()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->b:I

    return v0
.end method

.method public getVideoWidth()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->a:I

    return v0
.end method

.method protected onMeasure(II)V
    .locals 4

    iget v0, p0, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->a:I

    invoke-static {v0, p1}, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->getDefaultSize(II)I

    move-result v1

    iget v0, p0, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->b:I

    invoke-static {v0, p2}, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->getDefaultSize(II)I

    move-result v0

    iget v2, p0, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->a:I

    if-lez v2, :cond_0

    iget v2, p0, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->b:I

    if-lez v2, :cond_0

    iget v2, p0, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->a:I

    mul-int/2addr v2, v0

    iget v3, p0, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->b:I

    mul-int/2addr v3, v1

    if-le v2, v3, :cond_1

    iget v0, p0, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->b:I

    mul-int/2addr v0, v1

    iget v2, p0, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->a:I

    div-int/2addr v0, v2

    :cond_0
    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->setMeasuredDimension(II)V

    return-void

    :cond_1
    iget v2, p0, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->a:I

    mul-int/2addr v2, v0

    iget v3, p0, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->b:I

    mul-int/2addr v3, v1

    if-ge v2, v3, :cond_0

    iget v1, p0, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->a:I

    mul-int/2addr v1, v0

    iget v2, p0, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->b:I

    div-int/2addr v1, v2

    goto :goto_0
.end method
