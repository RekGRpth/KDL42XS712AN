.class public Lcom/twitter/library/amplify/AmplifyPlayer;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Lkr;


# static fields
.field private static c:Lcom/twitter/library/amplify/k;


# instance fields
.field a:Lcom/twitter/library/amplify/model/AmplifyInstance;

.field b:Z

.field private final d:Lcom/twitter/library/amplify/model/d;

.field private final e:Lcom/twitter/library/card/element/Player;

.field private final f:Landroid/content/BroadcastReceiver;

.field private g:Ljava/util/HashMap;

.field private h:Lkp;

.field private i:Landroid/content/Context;

.field private j:Ljava/lang/ref/WeakReference;

.field private k:Ljava/lang/ref/WeakReference;

.field private l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

.field private m:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

.field private n:I

.field private o:I

.field private p:Z

.field private q:Landroid/graphics/SurfaceTexture;

.field private r:F

.field private s:Z

.field private t:Lcom/twitter/library/amplify/h;

.field private u:Lcom/twitter/library/provider/Tweet;

.field private v:Z

.field private w:Z

.field private x:I


# direct methods
.method public constructor <init>(Lcom/twitter/library/card/element/Player;)V
    .locals 4

    const/4 v3, 0x0

    const/16 v2, 0x64

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/twitter/library/amplify/f;

    invoke-direct {v0, p0}, Lcom/twitter/library/amplify/f;-><init>(Lcom/twitter/library/amplify/AmplifyPlayer;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->f:Landroid/content/BroadcastReceiver;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->g:Ljava/util/HashMap;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->j:Ljava/lang/ref/WeakReference;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->k:Ljava/lang/ref/WeakReference;

    iput-boolean v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->p:Z

    invoke-static {v2, v2}, Lkp;->a(II)F

    move-result v0

    iput v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->r:F

    iput-boolean v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->s:Z

    iput-object p1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->e:Lcom/twitter/library/card/element/Player;

    new-instance v0, Lcom/twitter/library/amplify/model/d;

    invoke-direct {v0, p0}, Lcom/twitter/library/amplify/model/d;-><init>(Lcom/twitter/library/amplify/AmplifyPlayer;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->d:Lcom/twitter/library/amplify/model/d;

    return-void
.end method

.method public static D()Z
    .locals 1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->c(I)Z

    move-result v0

    return v0
.end method

.method private E()V
    .locals 6

    const-wide/16 v4, 0x0

    const-string/jumbo v0, "open"

    invoke-virtual {p0, v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->l()Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-direct {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->F()V

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->i:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->a:Lcom/twitter/library/amplify/model/AmplifyInstance;

    iget v3, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->n:I

    invoke-virtual {v2, v3}, Lcom/twitter/library/amplify/model/AmplifyInstance;->a(I)Lcom/twitter/library/amplify/model/AmplifyVideo;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Lcom/twitter/library/amplify/model/AmplifyVideo;->a(D)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->a:Lcom/twitter/library/amplify/model/AmplifyInstance;

    invoke-virtual {v3}, Lcom/twitter/library/amplify/model/AmplifyInstance;->b()Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)V

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->o:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->o:I

    iget v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->o:I

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->a:Lcom/twitter/library/amplify/model/AmplifyInstance;

    invoke-virtual {v1}, Lcom/twitter/library/amplify/model/AmplifyInstance;->d()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->l()Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->m:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->m:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->i:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->a:Lcom/twitter/library/amplify/model/AmplifyInstance;

    iget v3, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->o:I

    invoke-virtual {v2, v3}, Lcom/twitter/library/amplify/model/AmplifyInstance;->a(I)Lcom/twitter/library/amplify/model/AmplifyVideo;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Lcom/twitter/library/amplify/model/AmplifyVideo;->a(D)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->a:Lcom/twitter/library/amplify/model/AmplifyInstance;

    invoke-virtual {v3}, Lcom/twitter/library/amplify/model/AmplifyInstance;->b()Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)V

    :cond_0
    return-void
.end method

.method private F()V
    .locals 2

    invoke-static {}, Lcom/twitter/library/util/Util;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-direct {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->I()Landroid/view/Surface;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Landroid/view/Surface;)V

    :goto_0
    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {v0, p0}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->n()Lcom/twitter/library/amplify/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Lcom/twitter/library/amplify/i;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-direct {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->J()Landroid/view/SurfaceHolder;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Landroid/view/SurfaceHolder;)V

    goto :goto_0
.end method

.method private G()Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->b:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "fullscreen"

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "inline"

    goto :goto_0
.end method

.method private H()Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->b:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "inline"

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "fullscreen"

    goto :goto_0
.end method

.method private I()Landroid/view/Surface;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->j:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/Surface;

    return-object v0
.end method

.method private J()Landroid/view/SurfaceHolder;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->k:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceHolder;

    return-object v0
.end method

.method private K()Lcom/twitter/library/amplify/h;
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->t:Lcom/twitter/library/amplify/h;

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->e:Lcom/twitter/library/card/element/Player;

    invoke-virtual {v1}, Lcom/twitter/library/card/element/Player;->d()Lcom/twitter/library/card/Card;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->e:Lcom/twitter/library/card/element/Player;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/Player;->d()Lcom/twitter/library/card/Card;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/card/Card;->e()Lcom/twitter/library/card/j;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/amplify/h;

    :cond_0
    return-object v0
.end method

.method private L()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->d:Lcom/twitter/library/amplify/model/d;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/model/d;->d()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/amplify/AmplifyPlayer;)I
    .locals 1

    iget v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->x:I

    return v0
.end method

.method static synthetic a(Lcom/twitter/library/amplify/AmplifyPlayer;I)I
    .locals 0

    iput p1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->x:I

    return p1
.end method

.method private a(Lcom/twitter/library/amplify/AmplifyPlayer$PlayerStartType;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->d:Lcom/twitter/library/amplify/model/d;

    invoke-virtual {v0, p1}, Lcom/twitter/library/amplify/model/d;->a(Lcom/twitter/library/amplify/AmplifyPlayer$PlayerStartType;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/amplify/AmplifyPlayer;Lcom/twitter/library/amplify/AmplifyPlayer$PlayerStartType;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Lcom/twitter/library/amplify/AmplifyPlayer$PlayerStartType;)V

    return-void
.end method

.method public static a(Lcom/twitter/library/amplify/k;)V
    .locals 0

    sput-object p0, Lcom/twitter/library/amplify/AmplifyPlayer;->c:Lcom/twitter/library/amplify/k;

    return-void
.end method

.method static synthetic b(Lcom/twitter/library/amplify/AmplifyPlayer;)Lcom/twitter/library/amplify/AmplifyMediaPlayer;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/library/amplify/AmplifyPlayer;)F
    .locals 1

    iget v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->r:F

    return v0
.end method

.method static c(I)Z
    .locals 1

    const/16 v0, 0xe

    if-ge p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/twitter/library/amplify/AmplifyPlayer;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->L()V

    return-void
.end method

.method static synthetic e(Lcom/twitter/library/amplify/AmplifyPlayer;)Lcom/twitter/library/amplify/model/d;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->d:Lcom/twitter/library/amplify/model/d;

    return-object v0
.end method


# virtual methods
.method public A()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->d:Lcom/twitter/library/amplify/model/d;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/model/d;->c()V

    return-void
.end method

.method public B()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->d:Lcom/twitter/library/amplify/model/d;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/model/d;->a()V

    return-void
.end method

.method public C()V
    .locals 1

    invoke-static {}, Lcom/twitter/library/amplify/AmplifyPlayer;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->x:I

    if-lez v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->E()V

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->w:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->w:Z

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->n()Lcom/twitter/library/amplify/i;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/twitter/library/amplify/i;->b()V

    :cond_1
    return-void
.end method

.method a(Landroid/content/Context;I)Landroid/view/View;
    .locals 4

    invoke-static {p2}, Lcom/twitter/library/util/Util;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->e:Lcom/twitter/library/card/element/Player;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/Player;ZZ)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/twitter/library/card/element/ElementView;

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->e:Lcom/twitter/library/card/element/Player;

    invoke-direct {v0, p1, v1}, Lcom/twitter/library/card/element/ElementView;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/Element;)V

    goto :goto_0
.end method

.method a(Lcom/twitter/library/amplify/model/AmplifyVideo;)Lcom/twitter/library/amplify/r;
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {v0, p1}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Lcom/twitter/library/amplify/model/AmplifyVideo;)Lcom/twitter/library/amplify/r;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/twitter/library/amplify/r;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/amplify/r;-><init>(IIIII)V

    goto :goto_0
.end method

.method public a()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Lcom/twitter/library/amplify/model/AmplifyVideo;Z)V

    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 3

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->i:Landroid/content/Context;

    new-instance v0, Lkp;

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->i:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lkp;-><init>(Landroid/content/Context;Lkr;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->h:Lkp;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->v:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->i:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->f:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->v:Z

    :cond_0
    return-void
.end method

.method a(Landroid/content/Context;IZ)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->a:Lcom/twitter/library/amplify/model/AmplifyInstance;

    if-nez v0, :cond_0

    if-nez p3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string/jumbo v0, "show"

    invoke-virtual {p0, v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/twitter/library/amplify/AmplifyPlayer;->d(Landroid/content/Context;)Lcom/twitter/library/amplify/model/AmplifyInstance;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->a:Lcom/twitter/library/amplify/model/AmplifyInstance;

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->a:Lcom/twitter/library/amplify/model/AmplifyInstance;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/model/AmplifyInstance;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p2}, Lcom/twitter/library/util/Util;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    invoke-direct {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->E()V

    goto :goto_0

    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->a:Lcom/twitter/library/amplify/model/AmplifyInstance;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/model/AmplifyInstance;->a()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    const-string/jumbo v1, "error"

    invoke-virtual {p0, v1, v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->e:Lcom/twitter/library/card/element/Player;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/Player;->A()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    instance-of v1, v0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;

    if-eqz v1, :cond_3

    check-cast v0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->a:Lcom/twitter/library/amplify/model/AmplifyInstance;

    invoke-virtual {v1}, Lcom/twitter/library/amplify/model/AmplifyInstance;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->setError(I)V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v0, "Error id: %d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->a:Lcom/twitter/library/amplify/model/AmplifyInstance;

    invoke-virtual {v3}, Lcom/twitter/library/amplify/model/AmplifyInstance;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->d:Lcom/twitter/library/amplify/model/d;

    new-instance v1, Lcom/twitter/library/amplify/g;

    invoke-direct {v1, p0, p1}, Lcom/twitter/library/amplify/g;-><init>(Lcom/twitter/library/amplify/AmplifyPlayer;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/model/d;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Z)V
    .locals 1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Landroid/content/Context;IZ)V

    return-void
.end method

.method public a(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->q:Landroid/graphics/SurfaceTexture;

    return-void
.end method

.method public a(Landroid/view/Surface;)V
    .locals 1

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->j:Ljava/lang/ref/WeakReference;

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {v0, p1}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Landroid/view/Surface;)V

    goto :goto_0
.end method

.method public a(Landroid/view/SurfaceHolder;)V
    .locals 1

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->k:Ljava/lang/ref/WeakReference;

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {v0, p1}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Landroid/view/SurfaceHolder;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/amplify/h;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->t:Lcom/twitter/library/amplify/h;

    return-void
.end method

.method public a(Lcom/twitter/library/amplify/i;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    new-instance v0, Lcom/twitter/library/amplify/j;

    invoke-direct {v0, p0, p1}, Lcom/twitter/library/amplify/j;-><init>(Lcom/twitter/library/amplify/AmplifyPlayer;Lcom/twitter/library/amplify/i;)V

    :cond_0
    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->g:Ljava/util/HashMap;

    invoke-virtual {v1, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->n()Lcom/twitter/library/amplify/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Lcom/twitter/library/amplify/i;)V

    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/library/provider/Tweet;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->u:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->k()V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 6

    const/4 v2, 0x0

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/amplify/model/AmplifyVideo;D)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    const/4 v3, 0x0

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/amplify/model/AmplifyVideo;D)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/amplify/model/AmplifyVideo;D)V
    .locals 10

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->i:Landroid/content/Context;

    invoke-direct {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->K()Lcom/twitter/library/amplify/h;

    move-result-object v0

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p3, :cond_5

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->u()Lcom/twitter/library/amplify/model/AmplifyVideo;

    move-result-object v3

    :goto_1
    const-wide/16 v4, 0x0

    cmpg-double v4, p4, v4

    if-gez v4, :cond_4

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->s()Lcom/twitter/library/amplify/r;

    move-result-object v4

    iget v4, v4, Lcom/twitter/library/amplify/r;->a:I

    int-to-double v4, v4

    :goto_2
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    iget v6, v6, Landroid/content/res/Configuration;->orientation:I

    const-string/jumbo v7, "audio"

    invoke-virtual {v1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    if-nez v1, :cond_2

    const/4 v7, 0x1

    :goto_3
    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->e:Lcom/twitter/library/card/element/Player;

    invoke-virtual {v1}, Lcom/twitter/library/card/element/Player;->d()Lcom/twitter/library/card/Card;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/twitter/library/card/Card;->c()Z

    move-result v9

    :goto_4
    iget v2, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->n:I

    move-object v1, p1

    move-object v8, p2

    invoke-interface/range {v0 .. v9}, Lcom/twitter/library/amplify/h;->a(Ljava/lang/String;ILcom/twitter/library/amplify/model/AmplifyVideo;DIZLjava/lang/String;Z)V

    goto :goto_0

    :cond_2
    move v7, v2

    goto :goto_3

    :cond_3
    move v9, v2

    goto :goto_4

    :cond_4
    move-wide v4, p4

    goto :goto_2

    :cond_5
    move-object v3, p3

    goto :goto_1
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->b:Z

    return-void
.end method

.method public a(ZZ)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/twitter/library/amplify/AmplifyPlayer;->d(Z)V

    invoke-direct {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->K()Lcom/twitter/library/amplify/h;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->e:Lcom/twitter/library/card/element/Player;

    invoke-interface {v0, v1, p2}, Lcom/twitter/library/amplify/h;->a(Lcom/twitter/library/card/element/Player;Z)V

    :cond_0
    return-void
.end method

.method a(I)Z
    .locals 2

    const/4 v0, 0x1

    invoke-static {p1}, Lcom/twitter/library/util/Util;->c(I)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->i()V

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->a:Lcom/twitter/library/amplify/model/AmplifyInstance;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->a:Lcom/twitter/library/amplify/model/AmplifyInstance;

    invoke-virtual {v1}, Lcom/twitter/library/amplify/model/AmplifyInstance;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->e:Lcom/twitter/library/card/element/Player;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/Player;->A()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->h()Z

    move-result v0

    goto :goto_0
.end method

.method public b(Landroid/content/Context;)Landroid/view/View;
    .locals 1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {p0, p1, v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->b()V

    :cond_0
    return-void
.end method

.method public b(I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {v0, p1}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(I)V

    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->s:Z

    return-void
.end method

.method public c()V
    .locals 2

    const/16 v0, 0x64

    const/16 v1, 0x32

    invoke-static {v0, v1}, Lkp;->a(II)F

    move-result v0

    iput v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->r:F

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    iget v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->r:F

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(F)V

    :cond_0
    return-void
.end method

.method public c(Landroid/content/Context;)V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Landroid/content/Context;IZ)V

    return-void
.end method

.method public c(Z)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->h:Lkp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->h:Lkp;

    invoke-virtual {v0}, Lkp;->b()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Z)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->m:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->m:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Z)V

    :cond_2
    if-eqz p1, :cond_3

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->x()V

    :cond_3
    invoke-direct {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->K()Lcom/twitter/library/amplify/h;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-interface {v0}, Lcom/twitter/library/amplify/h;->a()V

    :cond_4
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->a:Lcom/twitter/library/amplify/model/AmplifyInstance;

    iput v2, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->x:I

    iget-boolean v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->v:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->i:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->f:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-boolean v2, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->v:Z

    :cond_5
    return-void
.end method

.method d(Landroid/content/Context;)Lcom/twitter/library/amplify/model/AmplifyInstance;
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->e:Lcom/twitter/library/card/element/Player;

    iget-object v0, v0, Lcom/twitter/library/card/element/Player;->streamUrl:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->h()Lcom/twitter/library/provider/Tweet;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/twitter/library/amplify/model/AmplifyInstance;->a(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/provider/Tweet;)Lcom/twitter/library/amplify/model/AmplifyInstance;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 2

    const/16 v0, 0x64

    invoke-static {v0, v0}, Lkp;->a(II)F

    move-result v0

    iput v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->r:F

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    iget v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->r:F

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(F)V

    :cond_0
    return-void
.end method

.method public d(Z)V
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->b:Z

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->b:Z

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->p:Z

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->n()Lcom/twitter/library/amplify/i;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/twitter/library/amplify/i;->e()V

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {v1, v0}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Lcom/twitter/library/amplify/i;)V

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->m()Lcom/twitter/library/amplify/i;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Lcom/twitter/library/amplify/i;->f()V

    :cond_3
    const-string/jumbo v0, "full_screen"

    if-nez p1, :cond_4

    const-string/jumbo v0, "shrink"

    :cond_4
    invoke-virtual {p0, v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public e(Landroid/content/Context;)V
    .locals 2

    invoke-static {}, Lcom/twitter/library/util/Util;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->a:Lcom/twitter/library/amplify/model/AmplifyInstance;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->a:Lcom/twitter/library/amplify/model/AmplifyInstance;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/model/AmplifyInstance;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->a:Lcom/twitter/library/amplify/model/AmplifyInstance;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/model/AmplifyInstance;->a()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method public e(Z)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->d:Lcom/twitter/library/amplify/model/d;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/model/d;->c()V

    const-string/jumbo v0, "play"

    if-eqz p1, :cond_2

    const-string/jumbo v0, "replay"

    :cond_0
    :goto_0
    invoke-virtual {p0, v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->h:Lkp;

    invoke-virtual {v0}, Lkp;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->u()Lcom/twitter/library/amplify/model/AmplifyVideo;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Lcom/twitter/library/amplify/model/AmplifyVideo;Z)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {v1}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v0, "resume"

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->n()Lcom/twitter/library/amplify/i;

    move-result-object v0

    if-eqz v0, :cond_1

    sget v1, Lil;->media_error_audio_focus_rejected:I

    iget-object v2, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->i:Landroid/content/Context;

    sget v3, Lil;->media_error_audio_focus_rejected:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/twitter/library/amplify/i;->a(ILjava/lang/String;)V

    goto :goto_1
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->b:Z

    return v0
.end method

.method public f(Landroid/content/Context;)Lcom/twitter/library/amplify/control/h;
    .locals 1

    sget-object v0, Lcom/twitter/library/amplify/AmplifyPlayer;->c:Lcom/twitter/library/amplify/k;

    invoke-interface {v0, p1}, Lcom/twitter/library/amplify/k;->a(Landroid/content/Context;)Lcom/twitter/library/amplify/control/h;

    move-result-object v0

    return-object v0
.end method

.method public f(Z)V
    .locals 3

    invoke-direct {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->K()Lcom/twitter/library/amplify/h;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->h()Lcom/twitter/library/provider/Tweet;

    move-result-object v2

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->e:Lcom/twitter/library/card/element/Player;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/Player;->d()Lcom/twitter/library/card/Card;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/twitter/library/card/Card;->c()Z

    move-result v0

    :goto_0
    invoke-interface {v1, v2, p1, v0}, Lcom/twitter/library/amplify/h;->a(Lcom/twitter/library/provider/Tweet;ZZ)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->s:Z

    return v0
.end method

.method public g(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->p:Z

    return-void
.end method

.method public g()Z
    .locals 1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(I)Z

    move-result v0

    return v0
.end method

.method public h()Lcom/twitter/library/provider/Tweet;
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->u:Lcom/twitter/library/provider/Tweet;

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->e:Lcom/twitter/library/card/element/Player;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->e:Lcom/twitter/library/card/element/Player;

    invoke-virtual {v1}, Lcom/twitter/library/card/element/Player;->d()Lcom/twitter/library/card/Card;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->e:Lcom/twitter/library/card/element/Player;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/Player;->d()Lcom/twitter/library/card/Card;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/card/Card;->f()Lcom/twitter/library/provider/Tweet;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public h(Z)V
    .locals 3

    const/4 v2, 0x1

    invoke-static {}, Lcom/twitter/library/amplify/AmplifyPlayer;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->u()Lcom/twitter/library/amplify/model/AmplifyVideo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Lcom/twitter/library/amplify/model/AmplifyVideo;)Lcom/twitter/library/amplify/r;

    move-result-object v0

    iget v0, v0, Lcom/twitter/library/amplify/r;->a:I

    iput v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->x:I

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {v0, v2}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Z)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    iput-boolean v2, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->w:Z

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->g(Z)V

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->p()V

    iput-boolean v2, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->w:Z

    :cond_1
    return-void
.end method

.method i()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->e:Lcom/twitter/library/card/element/Player;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/Player;->e()V

    return-void
.end method

.method public j()V
    .locals 0

    return-void
.end method

.method public k()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->n()Lcom/twitter/library/amplify/i;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/twitter/library/amplify/i;->g()V

    :cond_0
    return-void
.end method

.method l()Lcom/twitter/library/amplify/AmplifyMediaPlayer;
    .locals 1

    new-instance v0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-direct {v0}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;-><init>()V

    return-object v0
.end method

.method m()Lcom/twitter/library/amplify/i;
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->g:Ljava/util/HashMap;

    invoke-direct {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->H()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/amplify/i;

    return-object v0
.end method

.method n()Lcom/twitter/library/amplify/i;
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->g:Ljava/util/HashMap;

    invoke-direct {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->G()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/amplify/i;

    return-object v0
.end method

.method public o()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->c(Z)V

    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 8

    const/4 v7, 0x0

    const-wide/16 v5, 0x0

    const/4 v2, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->n()Lcom/twitter/library/amplify/i;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->n()Lcom/twitter/library/amplify/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/amplify/i;->c()V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->y()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->z()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->m:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->m:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    iput-object v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->m:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->m:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {v0, v4}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Lcom/twitter/library/amplify/i;)V

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->m:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {v0, v4}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Landroid/media/MediaPlayer$OnCompletionListener;)V

    invoke-static {}, Lcom/twitter/library/util/Util;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->m:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {v0, v4}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Landroid/view/Surface;)V

    :goto_0
    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->m:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {v0, v2}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Z)V

    iget v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->o:I

    iput v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->n:I

    iget v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->o:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->o:I

    const-string/jumbo v0, "open"

    invoke-virtual {p0, v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->F()V

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    iget v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->r:F

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(F)V

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {v0, v4, v7}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Lcom/twitter/library/amplify/model/AmplifyVideo;Z)V

    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->z()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->o:I

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->a:Lcom/twitter/library/amplify/model/AmplifyInstance;

    invoke-virtual {v1}, Lcom/twitter/library/amplify/model/AmplifyInstance;->d()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->m:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->m:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->i:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->a:Lcom/twitter/library/amplify/model/AmplifyInstance;

    iget v3, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->o:I

    invoke-virtual {v2, v3}, Lcom/twitter/library/amplify/model/AmplifyInstance;->a(I)Lcom/twitter/library/amplify/model/AmplifyVideo;

    move-result-object v2

    invoke-virtual {v2, v5, v6}, Lcom/twitter/library/amplify/model/AmplifyVideo;->a(D)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->a:Lcom/twitter/library/amplify/model/AmplifyInstance;

    invoke-virtual {v3}, Lcom/twitter/library/amplify/model/AmplifyInstance;->b()Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)V

    :cond_2
    return-void

    :cond_3
    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->m:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {v0, v4}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Landroid/view/SurfaceHolder;)V

    goto :goto_0

    :cond_4
    iget v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->o:I

    const-string/jumbo v0, "open"

    invoke-virtual {p0, v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {v0, v4}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Lcom/twitter/library/amplify/i;)V

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {v0, v4}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Landroid/media/MediaPlayer$OnCompletionListener;)V

    invoke-static {}, Lcom/twitter/library/util/Util;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {v0, v4}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Landroid/view/Surface;)V

    :goto_2
    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {v0, v2}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Z)V

    invoke-direct {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->F()V

    invoke-virtual {p0, v2}, Lcom/twitter/library/amplify/AmplifyPlayer;->g(Z)V

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->i:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->a:Lcom/twitter/library/amplify/model/AmplifyInstance;

    iget v3, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->n:I

    invoke-virtual {v2, v3}, Lcom/twitter/library/amplify/model/AmplifyInstance;->a(I)Lcom/twitter/library/amplify/model/AmplifyVideo;

    move-result-object v2

    invoke-virtual {v2, v5, v6}, Lcom/twitter/library/amplify/model/AmplifyVideo;->a(D)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->a:Lcom/twitter/library/amplify/model/AmplifyInstance;

    invoke-virtual {v3}, Lcom/twitter/library/amplify/model/AmplifyInstance;->b()Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)V

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    iget v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->r:F

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(F)V

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {v0, v4, v7}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Lcom/twitter/library/amplify/model/AmplifyVideo;Z)V

    goto/16 :goto_1

    :cond_5
    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {v0, v4}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Landroid/view/SurfaceHolder;)V

    goto :goto_2

    :cond_6
    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->n()Lcom/twitter/library/amplify/i;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->n()Lcom/twitter/library/amplify/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/library/amplify/i;->d()V

    :cond_7
    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->h:Lkp;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->h:Lkp;

    invoke-virtual {v0}, Lkp;->b()V

    goto/16 :goto_1
.end method

.method public p()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "pause"

    invoke-virtual {p0, v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->h:Lkp;

    invoke-virtual {v0}, Lkp;->b()V

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->b()V

    :cond_0
    return-void
.end method

.method public q()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a()Z

    move-result v0

    :cond_0
    return v0
.end method

.method public s()Lcom/twitter/library/amplify/r;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->u()Lcom/twitter/library/amplify/model/AmplifyVideo;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Lcom/twitter/library/amplify/model/AmplifyVideo;)Lcom/twitter/library/amplify/r;

    move-result-object v0

    return-object v0
.end method

.method public t()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->l:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "rewind"

    invoke-virtual {p0, v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public u()Lcom/twitter/library/amplify/model/AmplifyVideo;
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->a:Lcom/twitter/library/amplify/model/AmplifyInstance;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->a:Lcom/twitter/library/amplify/model/AmplifyInstance;

    iget v1, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->n:I

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/model/AmplifyInstance;->a(I)Lcom/twitter/library/amplify/model/AmplifyVideo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public v()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->p:Z

    return v0
.end method

.method public w()Landroid/graphics/SurfaceTexture;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->q:Landroid/graphics/SurfaceTexture;

    return-object v0
.end method

.method public x()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->q:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->q:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->q:Landroid/graphics/SurfaceTexture;

    :cond_0
    return-void
.end method

.method y()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->a:Lcom/twitter/library/amplify/model/AmplifyInstance;

    invoke-virtual {v2}, Lcom/twitter/library/amplify/model/AmplifyInstance;->d()I

    move-result v2

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->z()Z

    move-result v3

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->o:I

    if-lt v3, v2, :cond_0

    iget v3, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->o:I

    add-int/lit8 v3, v3, 0x1

    if-ge v3, v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    iget v3, p0, Lcom/twitter/library/amplify/AmplifyPlayer;->n:I

    add-int/lit8 v3, v3, 0x1

    if-ge v3, v2, :cond_3

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_3
    move v1, v0

    goto :goto_1
.end method

.method z()Z
    .locals 1

    invoke-static {}, Lcom/twitter/library/util/Util;->e()Z

    move-result v0

    return v0
.end method
