.class public Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;
.super Lcom/twitter/library/card/element/PlayerDelegateView;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/amplify/i;


# instance fields
.field private final a:Landroid/view/View$OnClickListener;

.field private final c:Landroid/os/Handler;

.field private d:Lcom/twitter/library/amplify/n;

.field private e:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

.field private final f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/card/element/Player;ZZ)V
    .locals 3

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/card/element/PlayerDelegateView;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/Player;)V

    new-instance v0, Lcom/twitter/library/amplify/p;

    invoke-direct {v0, p0}, Lcom/twitter/library/amplify/p;-><init>(Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->a:Landroid/view/View$OnClickListener;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->c:Landroid/os/Handler;

    iput-boolean p3, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->f:Z

    new-instance v0, Lcom/twitter/library/amplify/n;

    invoke-direct {p0}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->getAmplifyPlayer()Lcom/twitter/library/amplify/AmplifyPlayer;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/twitter/library/amplify/n;-><init>(Landroid/content/Context;Lcom/twitter/library/amplify/AmplifyPlayer;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->d:Lcom/twitter/library/amplify/n;

    new-instance v0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {p0}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->getAmplifyPlayer()Lcom/twitter/library/amplify/AmplifyPlayer;

    move-result-object v2

    invoke-direct {v0, v1, v2, p4}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;-><init>(Landroid/content/Context;Lcom/twitter/library/amplify/AmplifyPlayer;Z)V

    iput-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->e:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->d:Lcom/twitter/library/amplify/n;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/n;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->e:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {p0, v0}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->e:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->a()V

    invoke-direct {p0}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->getAmplifyPlayer()Lcom/twitter/library/amplify/AmplifyPlayer;

    move-result-object v1

    if-eqz p4, :cond_1

    const-string/jumbo v0, "fullscreen"

    :goto_0
    invoke-virtual {v1, p0, v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Lcom/twitter/library/amplify/i;Ljava/lang/String;)V

    if-eqz p4, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void

    :cond_1
    const-string/jumbo v0, "inline"

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;)Lcom/twitter/library/amplify/control/AmplifyVideoControlView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->e:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    return-object v0
.end method

.method private a(Lcom/twitter/library/amplify/AmplifyPlayer;)Z
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {p1}, Lcom/twitter/library/amplify/AmplifyPlayer;->e()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;Lcom/twitter/library/amplify/AmplifyPlayer;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->a(Lcom/twitter/library/amplify/AmplifyPlayer;)Z

    move-result v0

    return v0
.end method

.method private getAmplifyPlayer()Lcom/twitter/library/amplify/AmplifyPlayer;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->b:Lcom/twitter/library/card/element/Element;

    check-cast v0, Lcom/twitter/library/card/element/Player;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/Player;->b()Lcom/twitter/library/card/element/h;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/amplify/o;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/o;->i()Lcom/twitter/library/amplify/AmplifyPlayer;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->d:Lcom/twitter/library/amplify/n;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/library/amplify/n;->a(Lcom/twitter/library/amplify/n;Z)V

    return-void
.end method

.method public a(II)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->d:Lcom/twitter/library/amplify/n;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/amplify/n;->a(II)V

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->e:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->a(II)V

    return-void
.end method

.method public a(IIZZ)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->d:Lcom/twitter/library/amplify/n;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/amplify/n;->a(II)V

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->e:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->a(II)V

    invoke-direct {p0}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->getAmplifyPlayer()Lcom/twitter/library/amplify/AmplifyPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->s()Lcom/twitter/library/amplify/r;

    move-result-object v1

    iget v1, v1, Lcom/twitter/library/amplify/r;->a:I

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->u()Lcom/twitter/library/amplify/model/AmplifyVideo;

    move-result-object v2

    int-to-double v3, v1

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/amplify/model/AmplifyVideo;->e(D)Z

    move-result v1

    iget-object v2, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->e:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v2, v1}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->b(Z)V

    iget-object v1, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->e:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v1}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->b()V

    if-eqz p4, :cond_0

    iget-object v1, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->e:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v1}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->i()V

    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->v()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->e(Z)V

    :cond_1
    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->e:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {p0}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->a(Landroid/content/Context;I)V

    return-void
.end method

.method public a(Lcom/twitter/library/amplify/AmplifyPlayer$PlayerStartType;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->e:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->e()V

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->d:Lcom/twitter/library/amplify/n;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/n;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->d:Lcom/twitter/library/amplify/n;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/n;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->d:Lcom/twitter/library/amplify/n;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/library/amplify/n;->a(Lcom/twitter/library/amplify/n;Z)V

    return-void
.end method

.method public b()V
    .locals 1

    invoke-static {}, Lcom/twitter/library/amplify/AmplifyPlayer;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->e:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->a()V

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->e:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->h()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->e:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->i()V

    goto :goto_0
.end method

.method public b(II)V
    .locals 1

    const/16 v0, 0x2bd

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->e:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->a()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x2be

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->e:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->b()V

    goto :goto_0
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->e:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->d()V

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->e:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->a()V

    return-void
.end method

.method public d()V
    .locals 3

    invoke-direct {p0}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->getAmplifyPlayer()Lcom/twitter/library/amplify/AmplifyPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->s()Lcom/twitter/library/amplify/r;

    move-result-object v1

    iget v1, v1, Lcom/twitter/library/amplify/r;->a:I

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->u()Lcom/twitter/library/amplify/model/AmplifyVideo;

    move-result-object v0

    int-to-double v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/amplify/model/AmplifyVideo;->e(D)Z

    move-result v0

    iget-object v1, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->e:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v1, v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->a(Z)V

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->e:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->b()V

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->d:Lcom/twitter/library/amplify/n;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/library/amplify/n;->a(Lcom/twitter/library/amplify/n;Z)V

    return-void
.end method

.method public e()V
    .locals 3

    invoke-static {}, Lcom/twitter/library/util/Util;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->d:Lcom/twitter/library/amplify/n;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/n;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->removeView(Landroid/view/View;)V

    new-instance v0, Lcom/twitter/library/amplify/n;

    invoke-virtual {p0}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->getAmplifyPlayer()Lcom/twitter/library/amplify/AmplifyPlayer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/amplify/n;-><init>(Landroid/content/Context;Lcom/twitter/library/amplify/AmplifyPlayer;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->d:Lcom/twitter/library/amplify/n;

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->d:Lcom/twitter/library/amplify/n;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/n;->a()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->addView(Landroid/view/View;I)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->d:Lcom/twitter/library/amplify/n;

    invoke-direct {p0}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->getAmplifyPlayer()Lcom/twitter/library/amplify/AmplifyPlayer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->r()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/twitter/library/amplify/n;->a(Lcom/twitter/library/amplify/n;Z)V

    return-void
.end method

.method public f()V
    .locals 2

    invoke-static {}, Lcom/twitter/library/util/Util;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->d:Lcom/twitter/library/amplify/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->d:Lcom/twitter/library/amplify/n;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/n;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->d:Lcom/twitter/library/amplify/n;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/n;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->removeView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->e:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->c()V

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->d:Lcom/twitter/library/amplify/n;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/library/amplify/n;->a(Lcom/twitter/library/amplify/n;Z)V

    return-void
.end method

.method public g()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->e:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->n()V

    return-void
.end method

.method public h()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->e:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->f()Z

    move-result v0

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0}, Lcom/twitter/library/card/element/PlayerDelegateView;->onAttachedToWindow()V

    invoke-direct {p0}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->getAmplifyPlayer()Lcom/twitter/library/amplify/AmplifyPlayer;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v2, :cond_0

    invoke-virtual {v0, v2}, Lcom/twitter/library/amplify/AmplifyPlayer;->b(Z)V

    :cond_0
    iget-object v1, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->c:Landroid/os/Handler;

    new-instance v2, Lcom/twitter/library/amplify/q;

    invoke-direct {v2, p0, v0}, Lcom/twitter/library/amplify/q;-><init>(Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;Lcom/twitter/library/amplify/AmplifyPlayer;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/library/card/element/PlayerDelegateView;->onDetachedFromWindow()V

    invoke-direct {p0}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->getAmplifyPlayer()Lcom/twitter/library/amplify/AmplifyPlayer;

    move-result-object v0

    invoke-direct {p0}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->getAmplifyPlayer()Lcom/twitter/library/amplify/AmplifyPlayer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->r()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->g(Z)V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    invoke-super/range {p0 .. p5}, Lcom/twitter/library/card/element/PlayerDelegateView;->onLayout(ZIIII)V

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->d:Lcom/twitter/library/amplify/n;

    invoke-static {v0, p2, p3, p4, p5}, Lcom/twitter/library/amplify/n;->a(Lcom/twitter/library/amplify/n;IIII)Landroid/graphics/Rect;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->d:Lcom/twitter/library/amplify/n;

    invoke-virtual {v1}, Lcom/twitter/library/amplify/n;->a()Landroid/view/View;

    move-result-object v1

    iget v2, v0, Landroid/graphics/Rect;->left:I

    iget v3, v0, Landroid/graphics/Rect;->top:I

    iget v4, v0, Landroid/graphics/Rect;->right:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/view/View;->layout(IIII)V

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->e:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0, p2, p3, p4, p5}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->layout(IIII)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->f:Z

    if-eqz v0, :cond_0

    invoke-super {p0, p1, p2}, Lcom/twitter/library/card/element/PlayerDelegateView;->onMeasure(II)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->setMeasuredDimension(II)V

    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->measureChildren(II)V

    goto :goto_0
.end method

.method public setError(I)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->e:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {p0}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->a(Landroid/content/Context;I)V

    return-void
.end method

.method public setIsFullScreenToggleAllowed(Z)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->e:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->setIsFullScreenToggleAllowed(Z)V

    return-void
.end method
