.class Lcom/twitter/library/amplify/control/e;
.super Landroid/os/Handler;
.source "Twttr"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;


# direct methods
.method private constructor <init>(Lcom/twitter/library/amplify/control/AmplifyVideoControlView;)V
    .locals 1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/control/e;->a:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/library/amplify/control/AmplifyVideoControlView;Lcom/twitter/library/amplify/control/a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/library/amplify/control/e;-><init>(Lcom/twitter/library/amplify/control/AmplifyVideoControlView;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/amplify/control/e;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->l()I

    move-result v1

    invoke-static {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->c(Lcom/twitter/library/amplify/control/AmplifyVideoControlView;)V

    iget-boolean v2, v0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->c:Z

    if-nez v2, :cond_0

    invoke-static {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->d(Lcom/twitter/library/amplify/control/AmplifyVideoControlView;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, v0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->e:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/library/amplify/control/e;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    rem-int/lit16 v1, v1, 0x3e8

    rsub-int v1, v1, 0x3e8

    int-to-long v1, v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/library/amplify/control/e;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
