.class Lcom/twitter/library/amplify/control/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field final synthetic a:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;


# direct methods
.method constructor <init>(Lcom/twitter/library/amplify/control/AmplifyVideoControlView;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/amplify/control/a;->a:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 10

    const/4 v1, 0x0

    if-nez p3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/amplify/control/a;->a:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    iget-object v0, v0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->e:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->s()Lcom/twitter/library/amplify/r;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/library/amplify/control/a;->a:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    iget-object v0, v0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->e:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->u()Lcom/twitter/library/amplify/model/AmplifyVideo;

    move-result-object v0

    iget v3, v2, Lcom/twitter/library/amplify/r;->c:I

    mul-int/2addr v3, p2

    div-int/lit16 v3, v3, 0x3e8

    int-to-long v3, v3

    invoke-virtual {v0}, Lcom/twitter/library/amplify/model/AmplifyVideo;->b()D

    move-result-wide v5

    const-wide/high16 v7, -0x4010000000000000L    # -1.0

    cmpl-double v5, v5, v7

    if-eqz v5, :cond_3

    invoke-virtual {v0}, Lcom/twitter/library/amplify/model/AmplifyVideo;->b()D

    move-result-wide v5

    double-to-int v0, v5

    :goto_1
    iget-object v5, p0, Lcom/twitter/library/amplify/control/a;->a:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    int-to-long v6, v0

    add-long/2addr v6, v3

    iget v2, v2, Lcom/twitter/library/amplify/r;->a:I

    int-to-long v8, v2

    cmp-long v2, v6, v8

    if-gez v2, :cond_2

    const/4 v1, 0x1

    :cond_2
    iput-boolean v1, v5, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->d:Z

    iget-object v1, p0, Lcom/twitter/library/amplify/control/a;->a:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    iget-object v1, v1, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->e:Lcom/twitter/library/amplify/AmplifyPlayer;

    long-to-int v2, v3

    add-int/2addr v2, v0

    invoke-virtual {v1, v2}, Lcom/twitter/library/amplify/AmplifyPlayer;->b(I)V

    iget-object v1, p0, Lcom/twitter/library/amplify/control/a;->a:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    iget-object v1, v1, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->b:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/amplify/control/a;->a:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    iget-object v1, v1, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->b:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/twitter/library/amplify/control/a;->a:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    long-to-int v3, v3

    sub-int v0, v3, v0

    invoke-static {v2, v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->a(Lcom/twitter/library/amplify/control/AmplifyVideoControlView;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/twitter/library/amplify/control/a;->a:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    iput-boolean v2, v0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->c:Z

    iget-object v0, p0, Lcom/twitter/library/amplify/control/a;->a:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->d:Z

    iget-object v0, p0, Lcom/twitter/library/amplify/control/a;->a:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    iget-object v0, v0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->g:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/twitter/library/amplify/control/a;->a:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    iget-object v0, v0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->e:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->A()V

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/amplify/control/a;->a:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->c:Z

    iget-object v0, p0, Lcom/twitter/library/amplify/control/a;->a:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->l()I

    iget-object v0, p0, Lcom/twitter/library/amplify/control/a;->a:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->k()V

    iget-object v0, p0, Lcom/twitter/library/amplify/control/a;->a:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    iget-object v0, v0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->g:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v0, p0, Lcom/twitter/library/amplify/control/a;->a:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    iget-object v0, v0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->e:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->B()V

    iget-object v0, p0, Lcom/twitter/library/amplify/control/a;->a:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    iget-boolean v0, v0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/control/a;->a:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    iget-object v0, v0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->e:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->t()V

    :cond_0
    return-void
.end method
