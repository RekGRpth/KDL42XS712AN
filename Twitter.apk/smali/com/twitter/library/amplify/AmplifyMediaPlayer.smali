.class public Lcom/twitter/library/amplify/AmplifyMediaPlayer;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final e:Z


# instance fields
.field a:Lcom/twitter/library/amplify/i;

.field b:Landroid/media/MediaPlayer$OnCompletionListener;

.field c:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

.field d:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

.field private final f:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

.field private final g:Landroid/media/MediaPlayer$OnPreparedListener;

.field private final h:Landroid/media/MediaPlayer$OnCompletionListener;

.field private final i:Landroid/media/MediaPlayer$OnInfoListener;

.field private final j:Lcom/twitter/library/amplify/e;

.field private k:Landroid/media/MediaPlayer;

.field private l:Ljava/lang/String;

.field private m:Ljava/util/Map;

.field private n:Ljava/lang/ref/WeakReference;

.field private o:Ljava/lang/ref/WeakReference;

.field private final p:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Lcom/twitter/library/client/App;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "AmplifyMediaPlayer"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->e:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;->b:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->c:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    sget-object v0, Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;->b:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->d:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    new-instance v0, Lcom/twitter/library/amplify/a;

    invoke-direct {v0, p0}, Lcom/twitter/library/amplify/a;-><init>(Lcom/twitter/library/amplify/AmplifyMediaPlayer;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->f:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    new-instance v0, Lcom/twitter/library/amplify/b;

    invoke-direct {v0, p0}, Lcom/twitter/library/amplify/b;-><init>(Lcom/twitter/library/amplify/AmplifyMediaPlayer;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->g:Landroid/media/MediaPlayer$OnPreparedListener;

    new-instance v0, Lcom/twitter/library/amplify/c;

    invoke-direct {v0, p0}, Lcom/twitter/library/amplify/c;-><init>(Lcom/twitter/library/amplify/AmplifyMediaPlayer;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->h:Landroid/media/MediaPlayer$OnCompletionListener;

    new-instance v0, Lcom/twitter/library/amplify/d;

    invoke-direct {v0, p0}, Lcom/twitter/library/amplify/d;-><init>(Lcom/twitter/library/amplify/AmplifyMediaPlayer;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->i:Landroid/media/MediaPlayer$OnInfoListener;

    new-instance v0, Lcom/twitter/library/amplify/e;

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/amplify/e;-><init>(Lcom/twitter/library/amplify/AmplifyMediaPlayer;Lcom/twitter/library/amplify/a;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->j:Lcom/twitter/library/amplify/e;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->n:Ljava/lang/ref/WeakReference;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->o:Ljava/lang/ref/WeakReference;

    const/4 v0, 0x0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->p:[I

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/amplify/AmplifyMediaPlayer;)Landroid/media/MediaPlayer;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->p:[I

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->g:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->f:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->h:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->j:Lcom/twitter/library/amplify/e;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->i:Landroid/media/MediaPlayer$OnInfoListener;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    invoke-direct {p0, p1}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->b(Landroid/content/Context;)V

    invoke-static {}, Lcom/twitter/library/util/Util;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/Surface;

    invoke-virtual {v2, v0}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V

    sget-object v0, Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;->c:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->c:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-void

    :cond_1
    :try_start_3
    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->o:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->o:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceHolder;

    invoke-virtual {v2, v0}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_4
    invoke-direct {p0, v0}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Ljava/lang/Exception;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string/jumbo v0, "Unable to open content %s %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->l:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->e:Z

    if-eqz v1, :cond_0

    const-string/jumbo v1, "AmplifyMediaPlayer"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget-object v1, Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;->a:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    iput-object v1, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->c:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    sget-object v1, Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;->a:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    iput-object v1, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->d:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->j:Lcom/twitter/library/amplify/e;

    iget-object v2, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v2, v4, v3, v0}, Lcom/twitter/library/amplify/e;->a(Landroid/media/MediaPlayer;IILjava/lang/String;)Z

    return-void
.end method

.method private b(Landroid/content/Context;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->l:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->m:Ljava/util/Map;

    invoke-virtual {v0, p1, v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic e()Z
    .locals 1

    sget-boolean v0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->e:Z

    return v0
.end method

.method private f()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->c:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    sget-object v1, Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;->f:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    :cond_0
    return-void
.end method

.method private g()V
    .locals 1

    sget-object v0, Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;->b:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->c:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    sget-object v0, Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;->b:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->d:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/amplify/model/AmplifyVideo;)Lcom/twitter/library/amplify/r;
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/twitter/library/amplify/model/d;->a(Lcom/twitter/library/amplify/model/AmplifyVideo;II)Lcom/twitter/library/amplify/r;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/twitter/library/amplify/r;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/amplify/r;-><init>(IIIII)V

    goto :goto_0
.end method

.method public a(F)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1, p1}, Landroid/media/MediaPlayer;->setVolume(FF)V

    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v0, Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;->g:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->c:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    sget-object v0, Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;->g:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->d:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->b:Landroid/media/MediaPlayer$OnCompletionListener;

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    invoke-interface {v0, v1}, Landroid/media/MediaPlayer$OnCompletionListener;->onCompletion(Landroid/media/MediaPlayer;)V

    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)V
    .locals 0

    iput-object p2, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->l:Ljava/lang/String;

    iput-object p3, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->m:Ljava/util/Map;

    invoke-direct {p0}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->g()V

    invoke-direct {p0, p1}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Landroid/content/Context;)V

    return-void
.end method

.method public a(Landroid/media/MediaPlayer$OnCompletionListener;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->b:Landroid/media/MediaPlayer$OnCompletionListener;

    return-void
.end method

.method public a(Landroid/view/Surface;)V
    .locals 1

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->n:Ljava/lang/ref/WeakReference;

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    invoke-direct {p0}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->f()V

    :cond_0
    return-void
.end method

.method public a(Landroid/view/SurfaceHolder;)V
    .locals 1

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->o:Ljava/lang/ref/WeakReference;

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    invoke-direct {p0}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->f()V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/amplify/i;)V
    .locals 6

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a:Lcom/twitter/library/amplify/i;

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a:Lcom/twitter/library/amplify/i;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->c:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    sget-object v2, Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;->g:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a:Lcom/twitter/library/amplify/i;

    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/twitter/library/amplify/i;->a(II)V

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a:Lcom/twitter/library/amplify/i;

    invoke-interface {v0}, Lcom/twitter/library/amplify/i;->d()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a:Lcom/twitter/library/amplify/i;

    invoke-interface {v0}, Lcom/twitter/library/amplify/i;->e()V

    :cond_1
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a:Lcom/twitter/library/amplify/i;

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v3

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v4

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    iget-object v5, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v5}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v5

    if-gt v0, v5, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-interface {v2, v3, v4, v0, v1}, Lcom/twitter/library/amplify/i;->a(IIZZ)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public a(Lcom/twitter/library/amplify/model/AmplifyVideo;Z)V
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->c:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    if-eqz p2, :cond_1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/amplify/model/AmplifyVideo;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/amplify/model/AmplifyVideo;->b()D

    move-result-wide v3

    double-to-int v0, v3

    const/4 v3, -0x1

    if-eq v0, v3, :cond_4

    :goto_0
    move v1, v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    sget-object v0, Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;->e:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->c:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a:Lcom/twitter/library/amplify/i;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;->f:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    if-eq v2, v0, :cond_2

    sget-object v0, Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;->e:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    if-ne v2, v0, :cond_5

    :cond_2
    sget-object v0, Lcom/twitter/library/amplify/AmplifyPlayer$PlayerStartType;->b:Lcom/twitter/library/amplify/AmplifyPlayer$PlayerStartType;

    :goto_1
    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a:Lcom/twitter/library/amplify/i;

    invoke-interface {v1, v0}, Lcom/twitter/library/amplify/i;->a(Lcom/twitter/library/amplify/AmplifyPlayer$PlayerStartType;)V

    :cond_3
    sget-object v0, Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;->e:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->d:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    return-void

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    if-eqz p2, :cond_6

    sget-object v0, Lcom/twitter/library/amplify/AmplifyPlayer$PlayerStartType;->a:Lcom/twitter/library/amplify/AmplifyPlayer$PlayerStartType;

    goto :goto_1

    :cond_6
    sget-object v0, Lcom/twitter/library/amplify/AmplifyPlayer$PlayerStartType;->c:Lcom/twitter/library/amplify/AmplifyPlayer$PlayerStartType;

    goto :goto_1
.end method

.method public declared-synchronized a(Z)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->p:[I

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_1

    :try_start_2
    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :goto_0
    :try_start_3
    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    sget-object v0, Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;->b:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->c:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    if-eqz p1, :cond_1

    sget-object v0, Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;->b:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->d:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    :cond_1
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public a()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->c:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    sget-object v1, Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;->e:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;->f:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->c:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a:Lcom/twitter/library/amplify/i;

    invoke-interface {v0}, Lcom/twitter/library/amplify/i;->a()V

    :cond_0
    return-void
.end method

.method public c()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->c:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    sget-object v1, Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;->f:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->k:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->c:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    sget-object v1, Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;->a:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->c:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    sget-object v1, Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;->b:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->c:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    sget-object v1, Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;->c:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
