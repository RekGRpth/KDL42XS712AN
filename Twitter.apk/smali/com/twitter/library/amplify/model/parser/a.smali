.class public abstract Lcom/twitter/library/amplify/model/parser/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/internal/network/i;


# instance fields
.field public a:[Lcom/twitter/library/amplify/model/AmplifyVideo;

.field public b:I

.field public c:Z

.field public d:Z

.field protected e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/twitter/library/amplify/model/parser/a;->e:I

    return-void
.end method


# virtual methods
.method public a(ILjava/io/InputStream;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    const/16 v0, 0xc8

    if-ne p1, v0, :cond_2

    :try_start_0
    invoke-virtual {p0, p2}, Lcom/twitter/library/amplify/model/parser/a;->a(Ljava/io/InputStream;)V

    invoke-virtual {p0}, Lcom/twitter/library/amplify/model/parser/a;->a()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/library/amplify/model/parser/a;->c:Z

    if-eqz v0, :cond_1

    sget v0, Lil;->amplify_playback_forbidden_device:I

    iput v0, p0, Lcom/twitter/library/amplify/model/parser/a;->b:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/amplify/model/parser/a;->a:[Lcom/twitter/library/amplify/model/AmplifyVideo;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/amplify/model/parser/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/amplify/model/parser/a;->d:Z

    if-eqz v0, :cond_0

    sget v0, Lil;->amplify_playback_forbidden_device:I

    iput v0, p0, Lcom/twitter/library/amplify/model/parser/a;->b:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/amplify/model/parser/a;->a:[Lcom/twitter/library/amplify/model/AmplifyVideo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iput-object v1, p0, Lcom/twitter/library/amplify/model/parser/a;->a:[Lcom/twitter/library/amplify/model/AmplifyVideo;

    goto :goto_0

    :cond_2
    const/16 v0, 0x193

    if-ne p1, v0, :cond_0

    iput-object v1, p0, Lcom/twitter/library/amplify/model/parser/a;->a:[Lcom/twitter/library/amplify/model/AmplifyVideo;

    sget v0, Lil;->amplify_playback_forbidden:I

    iput v0, p0, Lcom/twitter/library/amplify/model/parser/a;->b:I

    goto :goto_0
.end method

.method public a(Lcom/twitter/internal/network/k;)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/amplify/model/parser/a;->a:[Lcom/twitter/library/amplify/model/AmplifyVideo;

    return-void
.end method

.method protected abstract a(Ljava/io/InputStream;)V
.end method

.method protected a()Z
    .locals 1

    invoke-static {}, Lcom/twitter/library/telephony/TelephonyUtil;->f()Z

    move-result v0

    return v0
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/amplify/model/parser/a;->e:I

    return v0
.end method
