.class public Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;
.super Lcom/twitter/library/amplify/model/parser/a;
.source "Twttr"


# instance fields
.field f:J

.field g:Ljava/lang/String;

.field h:Ljava/lang/String;

.field i:J

.field j:Ljava/lang/String;

.field k:Ljava/lang/String;

.field l:D

.field m:Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser$TwitterVmapVersion;

.field n:Ljava/util/Map;

.field o:Ljava/util/Map;

.field private p:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/amplify/model/parser/a;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method private a(Lorg/w3c/dom/Document;)Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser$TwitterVmapVersion;
    .locals 3

    sget-object v0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser$TwitterVmapVersion;->a:Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser$TwitterVmapVersion;

    const-string/jumbo v1, "http://twitter.com/schema/videoVMapV2.xsd"

    const-string/jumbo v2, "*"

    invoke-interface {p1, v1, v2}, Lorg/w3c/dom/Document;->getElementsByTagNameNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    if-lez v1, :cond_0

    sget-object v0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser$TwitterVmapVersion;->b:Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser$TwitterVmapVersion;

    :cond_0
    return-object v0
.end method

.method private a(Lorg/w3c/dom/Element;Z)Ljava/util/Map;
    .locals 5

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->m:Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser$TwitterVmapVersion;

    sget-object v1, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser$TwitterVmapVersion;->a:Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser$TwitterVmapVersion;

    if-ne v0, v1, :cond_1

    if-eqz p2, :cond_1

    const-string/jumbo v0, "tw:TrackingEvents"

    invoke-interface {p1, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    if-gtz v1, :cond_2

    :cond_0
    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_1
    const-string/jumbo v0, "TrackingEvents"

    invoke-interface {p1, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v0, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    const-string/jumbo v3, "Tracking"

    invoke-interface {v0, v3}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v3

    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v4

    move v0, v2

    :goto_2
    if-ge v0, v4, :cond_3

    invoke-interface {v3, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->a(Ljava/util/Map;Lorg/w3c/dom/Node;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method private a(Ljava/util/Map;Lorg/w3c/dom/Node;)V
    .locals 3

    move-object v0, p2

    check-cast v0, Lorg/w3c/dom/Element;

    const-string/jumbo v1, "event"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method private a(Lorg/w3c/dom/Element;)Z
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->m:Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser$TwitterVmapVersion;

    sget-object v2, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser$TwitterVmapVersion;->a:Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser$TwitterVmapVersion;

    if-eq v0, v2, :cond_1

    const-string/jumbo v0, "http://twitter.com/schema/videoVMapV2.xsd"

    const-string/jumbo v2, "ad"

    invoke-interface {p1, v0, v2}, Lorg/w3c/dom/Element;->getElementsByTagNameNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    if-gtz v2, :cond_2

    :cond_0
    move v0, v1

    :goto_1
    return v0

    :cond_1
    const-string/jumbo v0, "tw:ad"

    invoke-interface {p1, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    const-string/jumbo v2, "ownerId"

    invoke-interface {v0, v2}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    move v0, v1

    goto :goto_1

    :cond_3
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->i:J

    const-string/jumbo v2, "adId"

    invoke-interface {v0, v2}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->j:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->j:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    goto :goto_1

    :cond_4
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private b(Lorg/w3c/dom/Document;)Z
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->m:Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser$TwitterVmapVersion;

    sget-object v3, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser$TwitterVmapVersion;->a:Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser$TwitterVmapVersion;

    if-ne v0, v3, :cond_1

    invoke-interface {p1}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    const-string/jumbo v3, "tw:content"

    invoke-interface {v0, v3}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v3

    if-gtz v3, :cond_2

    :cond_0
    move v0, v1

    :goto_1
    return v0

    :cond_1
    invoke-interface {p1}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    const-string/jumbo v3, "http://twitter.com/schema/videoVMapV2.xsd"

    const-string/jumbo v4, "content"

    invoke-interface {v0, v3, v4}, Lorg/w3c/dom/Element;->getElementsByTagNameNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    const-string/jumbo v3, "stitched"

    invoke-interface {v0, v3}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v0, v1

    goto :goto_1

    :cond_3
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iput-boolean v3, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->p:Z

    const-string/jumbo v3, "ownerId"

    invoke-interface {v0, v3}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    move v0, v1

    goto :goto_1

    :cond_4
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->f:J

    const-string/jumbo v3, "contentId"

    invoke-interface {v0, v3}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->g:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->g:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    move v0, v1

    goto :goto_1

    :cond_5
    invoke-direct {p0, v0}, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->c(Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->h:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->h:Ljava/lang/String;

    if-nez v3, :cond_6

    move v0, v1

    goto :goto_1

    :cond_6
    iget-object v1, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->m:Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser$TwitterVmapVersion;

    sget-object v3, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser$TwitterVmapVersion;->a:Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser$TwitterVmapVersion;

    if-ne v1, v3, :cond_7

    invoke-interface {v0}, Lorg/w3c/dom/Element;->getParentNode()Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    :cond_7
    invoke-direct {p0, v0, v2}, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->a(Lorg/w3c/dom/Element;Z)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->o:Ljava/util/Map;

    move v0, v2

    goto :goto_1
.end method

.method private b(Lorg/w3c/dom/Element;)Z
    .locals 10

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string/jumbo v0, "Duration"

    invoke-interface {p1, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v3

    if-gtz v3, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    invoke-interface {v0}, Lorg/w3c/dom/Element;->getTextContent()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    const-string/jumbo v3, ":"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v3, v0

    const/4 v4, 0x3

    if-eq v3, v4, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    aget-object v3, v0, v2

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x2

    aget-object v0, v0, v4

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    int-to-long v4, v1

    const-wide/32 v6, 0x36ee80

    mul-long/2addr v4, v6

    int-to-long v6, v3

    const-wide/32 v8, 0xea60

    mul-long/2addr v6, v8

    add-long v3, v4, v6

    long-to-float v1, v3

    const/high16 v3, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v3

    add-float/2addr v0, v1

    float-to-double v0, v0

    iput-wide v0, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->l:D

    move v0, v2

    goto :goto_0
.end method

.method private c(Lorg/w3c/dom/Element;)Ljava/lang/String;
    .locals 2

    const-string/jumbo v0, "MediaFile"

    invoke-interface {p1, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    if-gtz v1, :cond_2

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    invoke-interface {v0}, Lorg/w3c/dom/Element;->getTextContent()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private c(Lorg/w3c/dom/Document;)Z
    .locals 4

    const/4 v2, 0x0

    invoke-interface {p1}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    const-string/jumbo v1, "Ad"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    if-gtz v1, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    return v0

    :cond_1
    invoke-interface {v0, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    const-string/jumbo v1, "Extensions"

    invoke-interface {p1, v1}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v3

    if-gtz v3, :cond_3

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    invoke-interface {v1, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v1

    check-cast v1, Lorg/w3c/dom/Element;

    invoke-direct {p0, v1}, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->a(Lorg/w3c/dom/Element;)Z

    move-result v1

    if-nez v1, :cond_4

    move v0, v2

    goto :goto_0

    :cond_4
    iget-boolean v1, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->p:Z

    if-nez v1, :cond_5

    invoke-direct {p0, v0}, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->c(Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->k:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->k:Ljava/lang/String;

    if-nez v1, :cond_6

    move v0, v2

    goto :goto_0

    :cond_5
    invoke-direct {p0, v0}, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->b(Lorg/w3c/dom/Element;)Z

    move-result v1

    if-nez v1, :cond_6

    move v0, v2

    goto :goto_0

    :cond_6
    invoke-direct {p0, v0, v2}, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->a(Lorg/w3c/dom/Element;Z)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->n:Ljava/util/Map;

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected a(Ljava/io/InputStream;)V
    .locals 14

    const/4 v13, 0x0

    const/4 v9, 0x1

    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljavax/xml/parsers/DocumentBuilderFactory;->setNamespaceAware(Z)V

    invoke-virtual {v0}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->a(Lorg/w3c/dom/Document;)Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser$TwitterVmapVersion;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->m:Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser$TwitterVmapVersion;

    invoke-direct {p0, v0}, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->b(Lorg/w3c/dom/Document;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->c(Lorg/w3c/dom/Document;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->p:Z

    if-eqz v0, :cond_1

    new-array v0, v9, [Lcom/twitter/library/amplify/model/AmplifyVideo;

    iput-object v0, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->a:[Lcom/twitter/library/amplify/model/AmplifyVideo;

    iget-object v12, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->a:[Lcom/twitter/library/amplify/model/AmplifyVideo;

    new-instance v0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;

    iget-object v1, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->g:Ljava/lang/String;

    iget-wide v3, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->f:J

    iget-object v5, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->j:Ljava/lang/String;

    iget-wide v6, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->i:J

    iget-wide v8, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->l:D

    iget-object v10, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->o:Ljava/util/Map;

    iget-object v11, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->n:Ljava/util/Map;

    invoke-direct/range {v0 .. v11}, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;JDLjava/util/Map;Ljava/util/Map;)V

    aput-object v0, v12, v13

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/twitter/library/amplify/model/AmplifyVideo;

    iput-object v0, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->a:[Lcom/twitter/library/amplify/model/AmplifyVideo;

    iget-object v8, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->a:[Lcom/twitter/library/amplify/model/AmplifyVideo;

    new-instance v0, Lcom/twitter/library/amplify/model/AmplifyVideo;

    iget-object v1, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->j:Ljava/lang/String;

    const-string/jumbo v2, "ad"

    iget-wide v3, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->i:J

    iget-object v5, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->k:Ljava/lang/String;

    iget-object v7, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->n:Ljava/util/Map;

    move v6, v13

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/amplify/model/AmplifyVideo;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;ZLjava/util/Map;)V

    aput-object v0, v8, v13

    iget-object v8, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->a:[Lcom/twitter/library/amplify/model/AmplifyVideo;

    new-instance v0, Lcom/twitter/library/amplify/model/AmplifyVideo;

    iget-object v1, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->g:Ljava/lang/String;

    const-string/jumbo v2, "video"

    iget-wide v3, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->f:J

    iget-object v5, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->h:Ljava/lang/String;

    iget-object v7, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->o:Ljava/util/Map;

    move v6, v9

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/amplify/model/AmplifyVideo;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;ZLjava/util/Map;)V

    aput-object v0, v8, v9

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->p:Z

    if-nez v0, :cond_0

    new-array v0, v9, [Lcom/twitter/library/amplify/model/AmplifyVideo;

    iput-object v0, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->a:[Lcom/twitter/library/amplify/model/AmplifyVideo;

    iget-object v8, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->a:[Lcom/twitter/library/amplify/model/AmplifyVideo;

    new-instance v0, Lcom/twitter/library/amplify/model/AmplifyVideo;

    iget-object v1, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->g:Ljava/lang/String;

    const-string/jumbo v2, "video"

    iget-wide v3, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->f:J

    iget-object v5, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->h:Ljava/lang/String;

    iget-object v7, p0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;->o:Ljava/util/Map;

    move v6, v9

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/amplify/model/AmplifyVideo;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;ZLjava/util/Map;)V

    aput-object v0, v8, v13

    goto :goto_0
.end method
