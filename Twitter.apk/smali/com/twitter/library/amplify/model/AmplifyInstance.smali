.class public Lcom/twitter/library/amplify/model/AmplifyInstance;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:[Lcom/twitter/library/amplify/model/AmplifyVideo;

.field private final b:I

.field private final c:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/library/amplify/model/a;

    invoke-direct {v0}, Lcom/twitter/library/amplify/model/a;-><init>()V

    sput-object v0, Lcom/twitter/library/amplify/model/AmplifyInstance;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>([Lcom/twitter/library/amplify/model/AmplifyVideo;ILjava/util/Map;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/amplify/model/AmplifyInstance;->a:[Lcom/twitter/library/amplify/model/AmplifyVideo;

    iput p2, p0, Lcom/twitter/library/amplify/model/AmplifyInstance;->b:I

    iput-object p3, p0, Lcom/twitter/library/amplify/model/AmplifyInstance;->c:Ljava/util/Map;

    return-void
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/amplify/model/AmplifyInstanceFactory;Lcom/twitter/library/provider/Tweet;)Lcom/twitter/library/amplify/model/AmplifyInstance;
    .locals 1

    invoke-virtual {p2, p0, p1, p3}, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory;->a(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/provider/Tweet;)Lcom/twitter/library/amplify/model/AmplifyInstance;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/provider/Tweet;)Lcom/twitter/library/amplify/model/AmplifyInstance;
    .locals 1

    invoke-static {}, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory;->a()Lcom/twitter/library/amplify/model/AmplifyInstanceFactory;

    move-result-object v0

    invoke-static {p0, p1, v0, p2}, Lcom/twitter/library/amplify/model/AmplifyInstance;->a(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/amplify/model/AmplifyInstanceFactory;Lcom/twitter/library/provider/Tweet;)Lcom/twitter/library/amplify/model/AmplifyInstance;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/library/amplify/model/AmplifyInstance;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/library/amplify/model/AmplifyInstance;->b:I

    if-nez v0, :cond_0

    sget v0, Lil;->amplify_playlist_download_failed:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/twitter/library/amplify/model/AmplifyInstance;->b:I

    goto :goto_0
.end method

.method public a(I)Lcom/twitter/library/amplify/model/AmplifyVideo;
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/twitter/library/amplify/model/AmplifyInstance;->d()I

    move-result v1

    if-ge p1, v1, :cond_0

    const/4 v1, -0x1

    if-le p1, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyInstance;->a:[Lcom/twitter/library/amplify/model/AmplifyVideo;

    aget-object v0, v0, p1

    :cond_0
    return-object v0
.end method

.method public b()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyInstance;->c:Ljava/util/Map;

    return-object v0
.end method

.method public c()Z
    .locals 1

    iget v0, p0, Lcom/twitter/library/amplify/model/AmplifyInstance;->b:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/amplify/model/AmplifyInstance;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/library/amplify/model/AmplifyInstance;->a:[Lcom/twitter/library/amplify/model/AmplifyVideo;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyInstance;->a:[Lcom/twitter/library/amplify/model/AmplifyVideo;

    array-length v0, v0

    :cond_0
    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected e()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyInstance;->a:[Lcom/twitter/library/amplify/model/AmplifyVideo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyInstance;->a:[Lcom/twitter/library/amplify/model/AmplifyVideo;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/twitter/library/amplify/model/AmplifyInstance;

    iget v2, p0, Lcom/twitter/library/amplify/model/AmplifyInstance;->b:I

    iget v3, p1, Lcom/twitter/library/amplify/model/AmplifyInstance;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/twitter/library/amplify/model/AmplifyInstance;->a:[Lcom/twitter/library/amplify/model/AmplifyVideo;

    iget-object v3, p1, Lcom/twitter/library/amplify/model/AmplifyInstance;->a:[Lcom/twitter/library/amplify/model/AmplifyVideo;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/twitter/library/amplify/model/AmplifyInstance;->c:Ljava/util/Map;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/twitter/library/amplify/model/AmplifyInstance;->c:Ljava/util/Map;

    iget-object v3, p1, Lcom/twitter/library/amplify/model/AmplifyInstance;->c:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p1, Lcom/twitter/library/amplify/model/AmplifyInstance;->c:Ljava/util/Map;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyInstance;->a:[Lcom/twitter/library/amplify/model/AmplifyVideo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyInstance;->a:[Lcom/twitter/library/amplify/model/AmplifyVideo;

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/twitter/library/amplify/model/AmplifyInstance;->b:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/library/amplify/model/AmplifyInstance;->c:Ljava/util/Map;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/library/amplify/model/AmplifyInstance;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyInstance;->a:[Lcom/twitter/library/amplify/model/AmplifyVideo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyInstance;->a:[Lcom/twitter/library/amplify/model/AmplifyVideo;

    array-length v0, v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyInstance;->a:[Lcom/twitter/library/amplify/model/AmplifyVideo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    iget v0, p0, Lcom/twitter/library/amplify/model/AmplifyInstance;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyInstance;->c:Ljava/util/Map;

    invoke-static {v0, p1}, Lky;->a(Ljava/util/Map;Landroid/os/Parcel;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
