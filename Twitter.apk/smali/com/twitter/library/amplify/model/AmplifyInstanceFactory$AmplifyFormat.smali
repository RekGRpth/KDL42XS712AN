.class final enum Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;
.super Ljava/lang/Enum;
.source "Twttr"


# static fields
.field public static final enum a:Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;

.field public static final enum b:Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;

.field public static final enum c:Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;

.field private static final synthetic d:[Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;

    const-string/jumbo v1, "JSON"

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;->a:Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;

    new-instance v0, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;

    const-string/jumbo v1, "VMAP"

    invoke-direct {v0, v1, v3}, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;->b:Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;

    new-instance v0, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;

    const-string/jumbo v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;->c:Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;

    sget-object v1, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;->a:Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;

    aput-object v1, v0, v2

    sget-object v1, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;->b:Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;

    aput-object v1, v0, v3

    sget-object v1, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;->c:Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;

    aput-object v1, v0, v4

    sput-object v0, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;->d:[Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;
    .locals 1

    const-class v0, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;

    return-object v0
.end method

.method public static values()[Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;
    .locals 1

    sget-object v0, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;->d:[Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;

    invoke-virtual {v0}, [Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;

    return-object v0
.end method
