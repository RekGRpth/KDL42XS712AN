.class public Lcom/twitter/library/amplify/model/AmplifyInstanceFactory;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Lcom/twitter/library/amplify/model/AmplifyInstanceFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory;

    invoke-direct {v0}, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory;-><init>()V

    sput-object v0, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory;->a:Lcom/twitter/library/amplify/model/AmplifyInstanceFactory;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/twitter/library/amplify/model/AmplifyInstanceFactory;
    .locals 1

    sget-object v0, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory;->a:Lcom/twitter/library/amplify/model/AmplifyInstanceFactory;

    return-object v0
.end method


# virtual methods
.method protected a(Ljava/lang/String;Ljava/util/Map;)Landroid/net/Uri$Builder;
    .locals 3

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string/jumbo v2, "Detected-Bandwidth"

    const-string/jumbo v0, "Detected-Bandwidth"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string/jumbo v2, "Android-Profile-Main"

    const-string/jumbo v0, "Android-Profile-Main"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string/jumbo v2, "Android-Profile-High"

    const-string/jumbo v0, "Android-Profile-High"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    return-object v1
.end method

.method protected a(Landroid/content/Context;Ljava/util/Map;Lcom/twitter/library/amplify/model/parser/a;Ljava/lang/String;)Lcom/twitter/internal/network/HttpOperation;
    .locals 4

    new-instance v0, Lcom/twitter/library/network/d;

    invoke-direct {v0, p1, p4}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {v0, p3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/internal/network/HttpOperation;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/internal/network/HttpOperation;

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/provider/Tweet;)Lcom/twitter/library/amplify/model/AmplifyInstance;
    .locals 3

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory;->b(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/provider/Tweet;)Lcom/twitter/library/amplify/model/parser/a;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory;->a(Landroid/content/Context;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p0, p2, v1}, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory;->a(Ljava/lang/String;Ljava/util/Map;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v1, v0, v2}, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory;->a(Landroid/content/Context;Ljava/util/Map;Lcom/twitter/library/amplify/model/parser/a;Ljava/lang/String;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {p0, v0, v2, v1}, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory;->a(Lcom/twitter/library/amplify/model/parser/a;Lcom/twitter/internal/network/HttpOperation;Ljava/util/Map;)Lcom/twitter/library/amplify/model/AmplifyInstance;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/amplify/model/parser/a;Lcom/twitter/internal/network/HttpOperation;Ljava/util/Map;)Lcom/twitter/library/amplify/model/AmplifyInstance;
    .locals 3

    invoke-virtual {p2}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p1, Lcom/twitter/library/amplify/model/parser/a;->b:I

    if-nez v0, :cond_0

    sget v0, Lil;->amplify_playlist_download_failed:I

    iput v0, p1, Lcom/twitter/library/amplify/model/parser/a;->b:I

    :cond_0
    new-instance v0, Lcom/twitter/library/amplify/model/AmplifyInstance;

    iget-object v1, p1, Lcom/twitter/library/amplify/model/parser/a;->a:[Lcom/twitter/library/amplify/model/AmplifyVideo;

    iget v2, p1, Lcom/twitter/library/amplify/model/parser/a;->b:I

    invoke-direct {v0, v1, v2, p3}, Lcom/twitter/library/amplify/model/AmplifyInstance;-><init>([Lcom/twitter/library/amplify/model/AmplifyVideo;ILjava/util/Map;)V

    return-object v0
.end method

.method protected a(Ljava/lang/String;Lcom/twitter/library/provider/Tweet;)Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;
    .locals 4

    sget-object v2, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;->c:Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;

    if-eqz p1, :cond_2

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/twitter/library/provider/Tweet;->n()Lcom/twitter/library/api/TwitterStatusCard;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    iget-object v1, v0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    if-eqz v1, :cond_2

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    iget-object v1, v0, Lcom/twitter/library/card/instance/CardInstanceData;->bindingValues:Ljava/util/HashMap;

    const-string/jumbo v0, "amplify_url"

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/instance/BindingValue;

    const-string/jumbo v3, "amplify_url_vmap"

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/card/instance/BindingValue;

    if-eqz v1, :cond_1

    iget-object v1, v1, Lcom/twitter/library/card/instance/BindingValue;->value:Ljava/lang/Object;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v0, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;->b:Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    iget-object v0, v0, Lcom/twitter/library/card/instance/BindingValue;->value:Ljava/lang/Object;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;->a:Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;

    goto :goto_1

    :cond_2
    move-object v0, v2

    goto :goto_1
.end method

.method protected a(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;
    .locals 4

    const-string/jumbo v0, "Twitter-android/%s Android/%d (%s)"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0, p1}, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object p3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;)Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory;->d()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {}, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->a()I

    move-result v2

    if-lt v1, v2, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/telephony/d;)Ljava/lang/String;
    .locals 1

    iget-object v0, p1, Lcom/twitter/library/telephony/d;->a:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    invoke-virtual {p0, v0}, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory;->a(Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/content/Context;)Ljava/util/Map;
    .locals 6

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory;->f()Lcom/twitter/library/amplify/model/b;

    move-result-object v1

    invoke-static {p1}, Lcom/twitter/library/telephony/TelephonyUtil;->d(Landroid/content/Context;)V

    invoke-static {}, Lcom/twitter/library/telephony/TelephonyUtil;->e()Lcom/twitter/library/telephony/d;

    move-result-object v2

    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string/jumbo v4, "video_multi_bitrate_network_type"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    :try_start_0
    invoke-static {v3}, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->valueOf(Ljava/lang/String;)Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    move-result-object v3

    iput-object v3, v2, Lcom/twitter/library/telephony/d;->a:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-virtual {p0, v2}, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory;->a(Lcom/twitter/library/telephony/d;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "Android-Profile-Main"

    iget-boolean v5, v1, Lcom/twitter/library/amplify/model/b;->a:Z

    invoke-static {v5}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v4, "Android-Profile-High"

    iget-boolean v1, v1, Lcom/twitter/library/amplify/model/b;->b:Z

    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "Detected-Bandwidth"

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "Network-Quality-Bucket"

    iget-object v3, v2, Lcom/twitter/library/telephony/d;->a:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    invoke-virtual {v3}, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "Carrier-Name"

    iget-object v3, v2, Lcom/twitter/library/telephony/d;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "Network-Type"

    iget-object v2, v2, Lcom/twitter/library/telephony/d;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "User-Agent"

    invoke-virtual {p0, p1}, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "Twitter-Player"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "X-CDN-DEVICE"

    invoke-virtual {p0}, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0

    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method protected b(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/provider/Tweet;)Lcom/twitter/library/amplify/model/parser/a;
    .locals 2

    invoke-virtual {p0, p2, p3}, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory;->a(Ljava/lang/String;Lcom/twitter/library/provider/Tweet;)Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;->a:Lcom/twitter/library/amplify/model/AmplifyInstanceFactory$AmplifyFormat;

    if-ne v0, v1, :cond_0

    new-instance v0, Lcom/twitter/library/amplify/model/parser/b;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-direct {v0, p1, v1}, Lcom/twitter/library/amplify/model/parser/b;-><init>(Landroid/content/Context;I)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-direct {v0, p1, v1}, Lcom/twitter/library/amplify/model/parser/AmplifyVmapInstanceParser;-><init>(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method protected b()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/twitter/library/telephony/TelephonyUtil;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "phone"

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "tablet"

    goto :goto_0
.end method

.method protected b(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {p0, p1, v0, v1}, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory;->a(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method c()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/library/amplify/model/AmplifyInstanceFactory;->e()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    if-nez v0, :cond_1

    :cond_0
    const-string/jumbo v0, "600"

    :cond_1
    return-object v0
.end method

.method protected c(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method d()Ljava/util/ArrayList;
    .locals 1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->e()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method e()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected f()Lcom/twitter/library/amplify/model/b;
    .locals 10
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    const/4 v1, 0x0

    new-instance v3, Lcom/twitter/library/amplify/model/b;

    invoke-direct {v3}, Lcom/twitter/library/amplify/model/b;-><init>()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v0, v2, :cond_4

    invoke-static {}, Landroid/media/MediaCodecList;->getCodecCount()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_4

    invoke-static {v2}, Landroid/media/MediaCodecList;->getCodecInfoAt(I)Landroid/media/MediaCodecInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaCodecInfo;->isEncoder()Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    :try_start_0
    const-string/jumbo v5, "video/avc"

    invoke-virtual {v0, v5}, Landroid/media/MediaCodecInfo;->getCapabilitiesForType(Ljava/lang/String;)Landroid/media/MediaCodecInfo$CodecCapabilities;

    move-result-object v0

    iget-object v5, v0, Landroid/media/MediaCodecInfo$CodecCapabilities;->profileLevels:[Landroid/media/MediaCodecInfo$CodecProfileLevel;

    array-length v6, v5

    move v0, v1

    :goto_2
    if-ge v0, v6, :cond_0

    aget-object v7, v5, v0

    iget v8, v7, Landroid/media/MediaCodecInfo$CodecProfileLevel;->profile:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_2

    const/4 v8, 0x1

    iput-boolean v8, v3, Lcom/twitter/library/amplify/model/b;->a:Z

    :cond_2
    iget v7, v7, Landroid/media/MediaCodecInfo$CodecProfileLevel;->profile:I

    const/16 v8, 0x8

    if-ne v7, v8, :cond_3

    const/4 v7, 0x1

    iput-boolean v7, v3, Lcom/twitter/library/amplify/model/b;->b:Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    return-object v3

    :catch_0
    move-exception v0

    goto :goto_1
.end method
