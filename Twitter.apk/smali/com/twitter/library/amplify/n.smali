.class public Lcom/twitter/library/amplify/n;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/amplify/AmplifyPlayer;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/twitter/library/util/Util;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;

    invoke-direct {v0, p1, p2}, Lcom/twitter/library/amplify/AmplifyVideoTextureView;-><init>(Landroid/content/Context;Lcom/twitter/library/amplify/AmplifyPlayer;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/n;->a:Landroid/view/View;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;

    invoke-direct {v0, p1, p2}, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;-><init>(Landroid/content/Context;Lcom/twitter/library/amplify/AmplifyPlayer;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/n;->a:Landroid/view/View;

    goto :goto_0
.end method

.method static a(IIII)Landroid/graphics/Point;
    .locals 6

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, p0, p1}, Landroid/graphics/Point;-><init>(II)V

    const/high16 v1, 0x3f800000    # 1.0f

    int-to-float v2, p3

    div-float/2addr v1, v2

    int-to-float v2, p0

    int-to-float v3, p1

    div-float v4, v2, v3

    int-to-float v5, p2

    mul-float/2addr v5, v1

    invoke-static {v5, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v4

    if-eqz v4, :cond_0

    int-to-float v4, p2

    div-float/2addr v2, v4

    mul-float/2addr v1, v3

    invoke-static {v2, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    int-to-float v2, p2

    mul-float/2addr v2, v1

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iput v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v2, p3

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, v0, Landroid/graphics/Point;->y:I

    :cond_0
    return-object v0
.end method

.method public static a(IIIIII)Landroid/graphics/Rect;
    .locals 4

    if-lez p0, :cond_1

    if-lez p1, :cond_1

    sub-int v0, p4, p2

    sub-int v1, p5, p3

    invoke-static {v0, v1, p0, p1}, Lcom/twitter/library/amplify/n;->a(IIII)Landroid/graphics/Point;

    move-result-object v2

    iget v3, v2, Landroid/graphics/Point;->x:I

    if-ge v3, v0, :cond_0

    iget v3, v2, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    add-int/2addr p2, v0

    sub-int/2addr p4, v0

    :cond_0
    iget v0, v2, Landroid/graphics/Point;->y:I

    if-ge v0, v1, :cond_1

    iget v0, v2, Landroid/graphics/Point;->y:I

    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr p3, v0

    sub-int/2addr p5, v0

    :cond_1
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p2, p3, p4, p5}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method

.method public static a(Lcom/twitter/library/amplify/n;IIII)Landroid/graphics/Rect;
    .locals 6

    invoke-virtual {p0}, Lcom/twitter/library/amplify/n;->b()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/library/amplify/n;->c()I

    move-result v1

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/amplify/n;->a(IIIIII)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/twitter/library/amplify/n;Z)V
    .locals 0

    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Lcom/twitter/library/amplify/n;->a(Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/n;->a:Landroid/view/View;

    return-object v0
.end method

.method public a(II)V
    .locals 1

    invoke-static {}, Lcom/twitter/library/util/Util;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/n;->a:Landroid/view/View;

    check-cast v0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->a(II)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/amplify/n;->a:Landroid/view/View;

    check-cast v0, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->a(II)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    invoke-static {}, Lcom/twitter/library/util/Util;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/n;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setKeepScreenOn(Z)V

    :cond_0
    return-void
.end method

.method public b()I
    .locals 1

    invoke-static {}, Lcom/twitter/library/util/Util;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/n;->a:Landroid/view/View;

    check-cast v0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;

    iget v0, v0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->a:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/amplify/n;->a:Landroid/view/View;

    check-cast v0, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;

    iget v0, v0, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->a:I

    goto :goto_0
.end method

.method public c()I
    .locals 1

    invoke-static {}, Lcom/twitter/library/util/Util;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/n;->a:Landroid/view/View;

    check-cast v0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;

    iget v0, v0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->b:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/amplify/n;->a:Landroid/view/View;

    check-cast v0, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;

    iget v0, v0, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->b:I

    goto :goto_0
.end method
