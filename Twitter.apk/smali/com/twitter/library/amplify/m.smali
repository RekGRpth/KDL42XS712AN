.class Lcom/twitter/library/amplify/m;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# instance fields
.field final synthetic a:Lcom/twitter/library/amplify/AmplifyVideoTextureView;


# direct methods
.method constructor <init>(Lcom/twitter/library/amplify/AmplifyVideoTextureView;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/amplify/m;->a:Lcom/twitter/library/amplify/AmplifyVideoTextureView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/library/amplify/m;->a:Lcom/twitter/library/amplify/AmplifyVideoTextureView;

    invoke-static {v0}, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->a(Lcom/twitter/library/amplify/AmplifyVideoTextureView;)Lcom/twitter/library/amplify/AmplifyPlayer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Landroid/graphics/SurfaceTexture;)V

    iget-object v0, p0, Lcom/twitter/library/amplify/m;->a:Lcom/twitter/library/amplify/AmplifyVideoTextureView;

    invoke-static {v0}, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->a(Lcom/twitter/library/amplify/AmplifyVideoTextureView;)Lcom/twitter/library/amplify/AmplifyPlayer;

    move-result-object v0

    new-instance v1, Landroid/view/Surface;

    invoke-direct {v1, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Landroid/view/Surface;)V

    iget-object v0, p0, Lcom/twitter/library/amplify/m;->a:Lcom/twitter/library/amplify/AmplifyVideoTextureView;

    invoke-static {v0}, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->a(Lcom/twitter/library/amplify/AmplifyVideoTextureView;)Lcom/twitter/library/amplify/AmplifyPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/m;->a:Lcom/twitter/library/amplify/AmplifyVideoTextureView;

    invoke-static {v0}, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->a(Lcom/twitter/library/amplify/AmplifyVideoTextureView;)Lcom/twitter/library/amplify/AmplifyPlayer;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/library/amplify/AmplifyPlayer;->g(Z)V

    iget-object v0, p0, Lcom/twitter/library/amplify/m;->a:Lcom/twitter/library/amplify/AmplifyVideoTextureView;

    invoke-static {v0}, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->a(Lcom/twitter/library/amplify/AmplifyVideoTextureView;)Lcom/twitter/library/amplify/AmplifyPlayer;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/library/amplify/AmplifyPlayer;->e(Z)V

    :cond_0
    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/amplify/m;->a:Lcom/twitter/library/amplify/AmplifyVideoTextureView;

    invoke-static {v0}, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->a(Lcom/twitter/library/amplify/AmplifyVideoTextureView;)Lcom/twitter/library/amplify/AmplifyPlayer;

    move-result-object v0

    new-instance v1, Landroid/view/Surface;

    invoke-direct {v1, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Landroid/view/Surface;)V

    return-void
.end method
