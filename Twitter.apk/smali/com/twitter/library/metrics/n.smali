.class public Lcom/twitter/library/metrics/n;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/metrics/k;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/metrics/n;->a:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/metrics/e;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-interface {p1}, Lcom/twitter/library/metrics/e;->d()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    invoke-interface {p1}, Lcom/twitter/library/metrics/e;->a()Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    const/4 v1, 0x3

    if-ne v2, v1, :cond_3

    iget-object v1, p0, Lcom/twitter/library/metrics/n;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_1
    :goto_1
    return-void

    :pswitch_0
    invoke-static {}, Lcom/twitter/library/client/App;->a()Z

    move-result v0

    goto :goto_0

    :pswitch_1
    invoke-static {}, Lcom/twitter/library/client/App;->a()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {}, Lcom/twitter/library/client/App;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :pswitch_2
    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/twitter/library/metrics/n;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/twitter/library/scribe/ScribeService;->c(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public hashCode()I
    .locals 1

    const-class v0, Lcom/twitter/library/metrics/n;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
