.class public abstract Lcom/twitter/library/metrics/d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/metrics/e;


# instance fields
.field a:J

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:J

.field private e:Z

.field private final f:Ljava/lang/String;

.field private final g:Lcom/twitter/library/metrics/g;

.field private final h:J

.field private i:Z

.field private final j:Lcom/twitter/library/metrics/f;

.field private final k:I

.field private l:Z


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;JILcom/twitter/library/metrics/g;)V
    .locals 9

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v8}, Lcom/twitter/library/metrics/d;-><init>(Ljava/lang/String;Ljava/lang/String;JILcom/twitter/library/metrics/g;Lcom/twitter/library/metrics/f;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;JILcom/twitter/library/metrics/g;Lcom/twitter/library/metrics/f;Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/twitter/library/metrics/d;->f:Ljava/lang/String;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/metrics/d;->h:J

    iput-object p1, p0, Lcom/twitter/library/metrics/d;->b:Ljava/lang/String;

    iput-object p6, p0, Lcom/twitter/library/metrics/d;->g:Lcom/twitter/library/metrics/g;

    iput-wide p3, p0, Lcom/twitter/library/metrics/d;->a:J

    iput-boolean p8, p0, Lcom/twitter/library/metrics/d;->l:Z

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/metrics/d;->c:Ljava/lang/String;

    iput p5, p0, Lcom/twitter/library/metrics/d;->k:I

    if-eqz p7, :cond_1

    iput-object p7, p0, Lcom/twitter/library/metrics/d;->j:Lcom/twitter/library/metrics/f;

    invoke-interface {p7, p0}, Lcom/twitter/library/metrics/f;->a(Lcom/twitter/library/metrics/d;)V

    :goto_0
    if-eqz p8, :cond_0

    if-eqz p6, :cond_0

    invoke-interface {p6}, Lcom/twitter/library/metrics/g;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/metrics/d;->a(Landroid/content/SharedPreferences;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/metrics/d;->j:Lcom/twitter/library/metrics/f;

    goto :goto_0
.end method


# virtual methods
.method protected a(Landroid/content/SharedPreferences$Editor;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/library/metrics/d;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "starttime"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/library/metrics/d;->d:J

    invoke-interface {p1, v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    return-void
.end method

.method protected a(Landroid/content/SharedPreferences;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/library/metrics/d;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "starttime"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-interface {p1, v0, v1, v2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/metrics/d;->d:J

    iget-object v0, p0, Lcom/twitter/library/metrics/d;->j:Lcom/twitter/library/metrics/f;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/metrics/d;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/metrics/d;->j:Lcom/twitter/library/metrics/f;

    iget-wide v1, p0, Lcom/twitter/library/metrics/d;->d:J

    invoke-interface {v0, v1, v2}, Lcom/twitter/library/metrics/f;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/metrics/d;->i:Z

    :cond_0
    return-void
.end method

.method protected final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/metrics/d;->e:Z

    return-void
.end method

.method public final b(J)V
    .locals 0

    iput-wide p1, p0, Lcom/twitter/library/metrics/d;->a:J

    return-void
.end method

.method protected b(Landroid/content/SharedPreferences$Editor;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/library/metrics/d;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "starttime"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    return-void
.end method

.method public final c()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/metrics/d;->e:Z

    iput-boolean v0, p0, Lcom/twitter/library/metrics/d;->i:Z

    invoke-virtual {p0}, Lcom/twitter/library/metrics/d;->f()V

    return-void
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/metrics/d;->k:I

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/metrics/d;->c:Ljava/lang/String;

    return-object v0
.end method

.method protected f()V
    .locals 0

    return-void
.end method

.method protected g()V
    .locals 0

    return-void
.end method

.method protected h()V
    .locals 0

    return-void
.end method

.method public final declared-synchronized i()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/twitter/library/metrics/d;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/twitter/library/metrics/d;->i:Z

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/metrics/d;->d:J

    iget-object v0, p0, Lcom/twitter/library/metrics/d;->j:Lcom/twitter/library/metrics/f;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/metrics/d;->j:Lcom/twitter/library/metrics/f;

    invoke-interface {v0}, Lcom/twitter/library/metrics/f;->a()V

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/metrics/d;->g()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized j()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/twitter/library/metrics/d;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/twitter/library/metrics/d;->h()V

    iget-object v0, p0, Lcom/twitter/library/metrics/d;->j:Lcom/twitter/library/metrics/f;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/metrics/d;->j:Lcom/twitter/library/metrics/f;

    invoke-interface {v0}, Lcom/twitter/library/metrics/f;->b()V

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/metrics/d;->i:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized k()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/library/metrics/d;->s()V

    iget-object v0, p0, Lcom/twitter/library/metrics/d;->j:Lcom/twitter/library/metrics/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/metrics/d;->j:Lcom/twitter/library/metrics/f;

    invoke-interface {v0}, Lcom/twitter/library/metrics/f;->c()V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/metrics/d;->p()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/metrics/d;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final m()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/metrics/d;->h:J

    return-wide v0
.end method

.method public final n()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/metrics/d;->i:Z

    return v0
.end method

.method public final o()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/metrics/d;->l:Z

    return v0
.end method

.method protected final p()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/metrics/d;->g:Lcom/twitter/library/metrics/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/metrics/d;->g:Lcom/twitter/library/metrics/g;

    invoke-interface {v0, p0}, Lcom/twitter/library/metrics/g;->b(Lcom/twitter/library/metrics/d;)V

    :cond_0
    return-void
.end method

.method protected final q()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/metrics/d;->e:Z

    iget-object v0, p0, Lcom/twitter/library/metrics/d;->g:Lcom/twitter/library/metrics/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/metrics/d;->g:Lcom/twitter/library/metrics/g;

    invoke-interface {v0, p0}, Lcom/twitter/library/metrics/g;->a(Lcom/twitter/library/metrics/d;)V

    :cond_0
    return-void
.end method

.method protected final r()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/metrics/d;->g:Lcom/twitter/library/metrics/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/metrics/d;->g:Lcom/twitter/library/metrics/g;

    invoke-interface {v0, p0}, Lcom/twitter/library/metrics/g;->c(Lcom/twitter/library/metrics/d;)V

    :cond_0
    return-void
.end method

.method protected s()V
    .locals 0

    return-void
.end method
