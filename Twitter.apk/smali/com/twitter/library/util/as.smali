.class public Lcom/twitter/library/util/as;
.super Lcom/twitter/library/util/ag;
.source "Twttr"


# static fields
.field private static final g:[I

.field private static h:Lld;

.field private static i:Z

.field private static final j:[I

.field private static final k:[I

.field private static l:I

.field private static m:Z


# instance fields
.field private final n:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    new-array v0, v1, [I

    sput-object v0, Lcom/twitter/library/util/as;->g:[I

    sput-boolean v1, Lcom/twitter/library/util/as;->i:Z

    const/4 v0, 0x5

    new-array v0, v0, [I

    sput-object v0, Lcom/twitter/library/util/as;->j:[I

    new-array v0, v1, [I

    sput-object v0, Lcom/twitter/library/util/as;->k:[I

    sput v1, Lcom/twitter/library/util/as;->l:I

    sput-boolean v1, Lcom/twitter/library/util/as;->m:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/util/ag;-><init>(Landroid/content/Context;I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/util/as;->n:Ljava/util/ArrayList;

    return-void
.end method

.method private static a(Landroid/content/Context;)Lld;
    .locals 7

    sget-object v1, Lcom/twitter/library/util/as;->g:[I

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/twitter/library/util/as;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :try_start_1
    invoke-static {p0}, Lcom/twitter/library/util/Util;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v3, Ljava/io/File;

    const-string/jumbo v4, "videos"

    invoke-direct {v3, v2, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 v2, 0x1

    const/4 v4, 0x1

    const-wide/32 v5, 0x6400000

    invoke-static {v3, v2, v4, v5, v6}, Lld;->a(Ljava/io/File;IIJ)Lld;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    :goto_0
    :try_start_2
    sput-object v0, Lcom/twitter/library/util/as;->h:Lld;

    const/4 v0, 0x1

    sput-boolean v0, Lcom/twitter/library/util/as;->i:Z

    :cond_0
    sget-object v0, Lcom/twitter/library/util/as;->h:Lld;

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-object v0

    :cond_1
    :try_start_3
    new-instance v2, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v3

    const-string/jumbo v4, "videos"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 v3, 0x1

    const/4 v4, 0x1

    const-wide/32 v5, 0x3200000

    invoke-static {v2, v3, v4, v5, v6}, Lld;->a(Ljava/io/File;IIJ)Lld;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private static a(I)V
    .locals 3

    sget-object v1, Lcom/twitter/library/util/as;->k:[I

    monitor-enter v1

    :try_start_0
    sget v0, Lcom/twitter/library/util/as;->l:I

    rem-int/lit8 v0, v0, 0x5

    sget-object v2, Lcom/twitter/library/util/as;->j:[I

    aput p0, v2, v0

    add-int/lit8 v0, v0, 0x1

    rem-int/lit8 v0, v0, 0x5

    sput v0, Lcom/twitter/library/util/as;->l:I

    sget v0, Lcom/twitter/library/util/as;->l:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    sput-boolean v0, Lcom/twitter/library/util/as;->m:Z

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method protected bridge synthetic a(JLjava/lang/Object;Ljava/lang/String;)Lcom/twitter/library/util/af;
    .locals 1

    check-cast p3, Lcom/twitter/library/util/at;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/twitter/library/util/as;->a(JLcom/twitter/library/util/at;Ljava/lang/String;)Lcom/twitter/library/util/an;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(JLjava/lang/Object;Ljava/lang/String;Ljava/io/InputStream;)Lcom/twitter/library/util/af;
    .locals 6

    move-object v3, p3

    check-cast v3, Lcom/twitter/library/util/at;

    move-object v0, p0

    move-wide v1, p1

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/util/as;->a(JLcom/twitter/library/util/at;Ljava/lang/String;Ljava/io/InputStream;)Lcom/twitter/library/util/an;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Ljava/lang/String;Ljava/io/InputStream;)Lcom/twitter/library/util/af;
    .locals 1

    check-cast p1, Lcom/twitter/library/util/at;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/util/as;->a(Lcom/twitter/library/util/at;Ljava/lang/String;Ljava/io/InputStream;)Lcom/twitter/library/util/an;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)Lcom/twitter/library/util/af;
    .locals 1

    check-cast p1, Lcom/twitter/library/util/at;

    check-cast p3, Ljava/io/File;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/util/as;->a(Lcom/twitter/library/util/at;Ljava/lang/String;Ljava/io/File;)Lcom/twitter/library/util/an;

    move-result-object v0

    return-object v0
.end method

.method protected a(JLcom/twitter/library/util/at;Ljava/lang/String;)Lcom/twitter/library/util/an;
    .locals 6

    const/4 v0, 0x0

    invoke-static {p4}, Lcom/twitter/library/util/Util;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    sget-object v2, Lcom/twitter/library/util/as;->g:[I

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, Lcom/twitter/library/util/as;->c:Landroid/content/Context;

    invoke-static {v3}, Lcom/twitter/library/util/as;->a(Landroid/content/Context;)Lld;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lld;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-nez v4, :cond_2

    :try_start_1
    invoke-virtual {v3, v1}, Lld;->a(Ljava/lang/String;)Lli;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    if-nez v1, :cond_1

    :try_start_2
    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_1
    :try_start_3
    new-instance v3, Ljava/io/File;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lli;->b(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p3, p4, v3}, Lcom/twitter/library/util/as;->a(Lcom/twitter/library/util/at;Ljava/lang/String;Ljava/io/File;)Lcom/twitter/library/util/an;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v0

    :try_start_4
    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    monitor-exit v2

    goto :goto_0

    :catch_0
    move-exception v1

    move-object v1, v0

    :goto_1
    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    :cond_2
    monitor-exit v2

    goto :goto_0

    :catchall_1
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_2
    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_2
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v3

    goto :goto_1
.end method

.method protected a(JLcom/twitter/library/util/at;Ljava/lang/String;Ljava/io/InputStream;)Lcom/twitter/library/util/an;
    .locals 9

    const/4 v0, 0x0

    const/4 v4, 0x0

    invoke-static {p4}, Lcom/twitter/library/util/Util;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0, p3, p4, p5}, Lcom/twitter/library/util/as;->a(Lcom/twitter/library/util/at;Ljava/lang/String;Ljava/io/InputStream;)Lcom/twitter/library/util/an;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v5, Lcom/twitter/library/util/as;->g:[I

    monitor-enter v5

    :try_start_0
    iget-object v2, p0, Lcom/twitter/library/util/as;->c:Landroid/content/Context;

    invoke-static {v2}, Lcom/twitter/library/util/as;->a(Landroid/content/Context;)Lld;

    move-result-object v6

    if-eqz v6, :cond_7

    invoke-virtual {v6}, Lld;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_7

    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {v6, v1}, Lld;->b(Ljava/lang/String;)Llf;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    if-eqz v3, :cond_4

    :try_start_2
    new-instance v1, Ljava/io/BufferedOutputStream;

    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Llf;->a(I)Ljava/io/OutputStream;

    move-result-object v2

    const/16 v7, 0x1000

    invoke-direct {v1, v2, v7}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :try_start_3
    new-instance v2, Lhx;

    invoke-direct {v2, p5, v1}, Lhx;-><init>(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    invoke-virtual {p0, p3, p4, v2}, Lcom/twitter/library/util/as;->a(Lcom/twitter/library/util/at;Ljava/lang/String;Ljava/io/InputStream;)Lcom/twitter/library/util/an;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/twitter/library/util/an;->b()Z

    move-result v7

    if-eqz v7, :cond_1

    const/4 v4, 0x1

    :cond_1
    if-eqz v4, :cond_a

    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->flush()V

    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    :goto_1
    if-eqz v3, :cond_2

    if-eqz v4, :cond_3

    :try_start_4
    invoke-virtual {v3}, Llf;->a()V

    invoke-virtual {v6}, Lld;->b()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_2
    :goto_2
    :try_start_5
    invoke-static {v0}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-object v0, v2

    goto :goto_0

    :cond_3
    :try_start_6
    invoke-virtual {v3}, Llf;->b()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v1

    goto :goto_2

    :cond_4
    :try_start_7
    invoke-virtual {p0, p3, p4, p5}, Lcom/twitter/library/util/as;->a(Lcom/twitter/library/util/at;Ljava/lang/String;Ljava/io/InputStream;)Lcom/twitter/library/util/an;
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    move-result-object v0

    if-eqz v3, :cond_5

    :try_start_8
    invoke-virtual {v3}, Llf;->b()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :cond_5
    :goto_3
    :try_start_9
    invoke-static {v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    monitor-exit v5

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v5
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    throw v0

    :catch_1
    move-exception v1

    move-object v1, v0

    :goto_4
    if-eqz v0, :cond_6

    :try_start_a
    invoke-virtual {v0}, Llf;->b()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :cond_6
    :goto_5
    :try_start_b
    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    :cond_7
    monitor-exit v5
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    invoke-virtual {p0, p3, p4, p5}, Lcom/twitter/library/util/as;->a(Lcom/twitter/library/util/at;Ljava/lang/String;Ljava/io/InputStream;)Lcom/twitter/library/util/an;

    move-result-object v0

    goto :goto_0

    :catchall_1
    move-exception v1

    move-object v3, v0

    move v2, v4

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    :goto_6
    if-eqz v3, :cond_8

    if-eqz v2, :cond_9

    :try_start_c
    invoke-virtual {v3}, Llf;->a()V

    invoke-virtual {v6}, Lld;->b()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :cond_8
    :goto_7
    :try_start_d
    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    throw v0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :cond_9
    :try_start_e
    invoke-virtual {v3}, Llf;->b()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_2
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto :goto_7

    :catch_2
    move-exception v2

    goto :goto_7

    :catchall_2
    move-exception v1

    move v2, v4

    move-object v8, v0

    move-object v0, v1

    move-object v1, v8

    goto :goto_6

    :catchall_3
    move-exception v0

    move v2, v4

    goto :goto_6

    :catch_3
    move-exception v0

    goto :goto_5

    :catch_4
    move-exception v1

    move-object v1, v0

    move-object v0, v3

    goto :goto_4

    :catch_5
    move-exception v0

    move-object v0, v3

    goto :goto_4

    :catch_6
    move-exception v1

    goto :goto_3

    :cond_a
    move-object v0, v1

    goto :goto_1
.end method

.method protected a(Lcom/twitter/library/util/at;Ljava/lang/String;Ljava/io/File;)Lcom/twitter/library/util/an;
    .locals 1

    new-instance v0, Lcom/twitter/library/util/an;

    invoke-direct {v0, p2, p3}, Lcom/twitter/library/util/an;-><init>(Ljava/lang/String;Ljava/io/File;)V

    return-object v0
.end method

.method protected a(Lcom/twitter/library/util/at;Ljava/lang/String;Ljava/io/InputStream;)Lcom/twitter/library/util/an;
    .locals 11

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/util/as;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/util/Util;->d(Landroid/content/Context;)Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {p2}, Lcom/twitter/library/util/Util;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/16 v0, 0xa

    invoke-static {v0}, Lcom/twitter/internal/util/j;->a(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v2, 0x1000

    :try_start_1
    invoke-static {p3, v0, v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/InputStream;Ljava/io/OutputStream;I)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    if-nez v2, :cond_2

    invoke-static {v0}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0

    :cond_2
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v4, v6, v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    if-lez v2, :cond_3

    div-int/lit16 v2, v2, 0x400

    int-to-double v6, v2

    const-wide/16 v8, 0x1

    invoke-static {v8, v9, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    long-to-double v4, v4

    div-double v4, v6, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v2, v4

    invoke-static {v2}, Lcom/twitter/library/util/as;->a(I)V

    :cond_3
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->flush()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-static {v0}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    new-instance v0, Lcom/twitter/library/util/an;

    invoke-direct {v0, p2, v3}, Lcom/twitter/library/util/an;-><init>(Ljava/lang/String;Ljava/io/File;)V

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_1
    invoke-static {v0}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_2
    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    goto :goto_2

    :catch_1
    move-exception v2

    goto :goto_1
.end method

.method public a(JLcom/twitter/library/util/at;Z)Ljava/lang/String;
    .locals 6

    iget-object v4, p3, Lcom/twitter/library/util/at;->a:Ljava/lang/String;

    const/4 v5, 0x1

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/util/as;->a(JLjava/lang/Object;Ljava/lang/String;Z)Lcom/twitter/library/util/af;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/an;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/twitter/library/util/an;->c:Ljava/lang/Object;

    check-cast v0, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/util/au;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/util/as;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected a(Ljava/util/HashMap;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/util/as;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/util/as;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/au;

    invoke-interface {v0, p0, p1}, Lcom/twitter/library/util/au;->a(Lcom/twitter/library/util/as;Ljava/util/HashMap;)V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/library/util/au;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/util/as;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method
