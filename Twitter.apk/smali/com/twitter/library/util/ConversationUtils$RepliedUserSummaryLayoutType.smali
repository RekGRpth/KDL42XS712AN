.class abstract enum Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;
.super Ljava/lang/Enum;
.source "Twttr"


# static fields
.field public static final enum a:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;

.field public static final enum b:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;

.field private static final synthetic c:[Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType$1;

    const-string/jumbo v1, "COMPACT"

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType$1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;->a:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;

    new-instance v0, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType$2;

    const-string/jumbo v1, "CONTENT"

    invoke-direct {v0, v1, v3}, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType$2;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;->b:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;

    sget-object v1, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;->a:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;->b:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;->c:[Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/twitter/library/util/d;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;
    .locals 1

    const-class v0, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;

    return-object v0
.end method

.method public static values()[Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;
    .locals 1

    sget-object v0, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;->c:[Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;

    invoke-virtual {v0}, [Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;

    return-object v0
.end method


# virtual methods
.method public abstract a(Ljava/lang/CharSequence;Landroid/text/TextPaint;III)Landroid/text/StaticLayout;
.end method
