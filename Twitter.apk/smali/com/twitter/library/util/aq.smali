.class Lcom/twitter/library/util/aq;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/twitter/library/util/ao;

.field private final b:Ljava/lang/String;

.field private final c:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Lcom/twitter/library/util/ao;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/util/aq;->a:Lcom/twitter/library/util/ao;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/twitter/library/util/aq;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/twitter/library/util/aq;->c:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/util/aq;->a:Lcom/twitter/library/util/ao;

    invoke-static {v0}, Lcom/twitter/library/util/ao;->a(Lcom/twitter/library/util/ao;)Lcom/twitter/library/util/e;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/twitter/library/util/aq;->a:Lcom/twitter/library/util/ao;

    iget-object v1, v1, Lcom/twitter/library/util/ao;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/library/util/aq;->c:Landroid/net/Uri;

    invoke-static {v1, v2}, Lkw;->a(Landroid/content/Context;Landroid/net/Uri;)Lkw;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/util/aq;->a:Lcom/twitter/library/util/ao;

    invoke-static {v2}, Lcom/twitter/library/util/ao;->b(Lcom/twitter/library/util/ao;)I

    move-result v2

    invoke-virtual {v1, v2}, Lkw;->a(I)Lkw;

    move-result-object v1

    invoke-virtual {v1}, Lkw;->b()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/twitter/library/util/aq;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Lcom/twitter/library/util/e;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/util/aq;->c:Landroid/net/Uri;

    invoke-static {v0}, Lcom/twitter/library/util/n;->b(Landroid/net/Uri;)Z

    goto :goto_0
.end method
