.class public Lcom/twitter/library/util/UrlNetworkStats;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Ljava/util/LinkedList;

.field private static b:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

.field private static c:J

.field private static d:I

.field private static e:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-wide/16 v1, 0x0

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/twitter/library/util/UrlNetworkStats;->a:Ljava/util/LinkedList;

    sget-object v0, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;->d:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    sput-object v0, Lcom/twitter/library/util/UrlNetworkStats;->b:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    sput-wide v1, Lcom/twitter/library/util/UrlNetworkStats;->c:J

    const/4 v0, 0x0

    sput v0, Lcom/twitter/library/util/UrlNetworkStats;->d:I

    sput-wide v1, Lcom/twitter/library/util/UrlNetworkStats;->e:J

    return-void
.end method

.method public static a(II)I
    .locals 9

    const/4 v0, 0x0

    if-nez p1, :cond_0

    :goto_0
    return v0

    :cond_0
    int-to-float v1, p0

    int-to-float v2, p1

    div-float/2addr v1, v2

    const/high16 v2, 0x447a0000    # 1000.0f

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/twitter/library/util/UrlNetworkStats;->a(IJ)V

    sget v4, Lcom/twitter/library/util/UrlNetworkStats;->d:I

    if-eqz v4, :cond_1

    if-eqz v1, :cond_1

    sget-wide v5, Lcom/twitter/library/util/UrlNetworkStats;->e:J

    sub-long v5, v2, v5

    const-wide/16 v7, 0x7530

    cmp-long v5, v5, v7

    if-gez v5, :cond_1

    sub-int v0, v4, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    :cond_1
    sput-wide v2, Lcom/twitter/library/util/UrlNetworkStats;->e:J

    goto :goto_0
.end method

.method public static a()Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/twitter/library/util/UrlNetworkStats;->a(Z)Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    move-result-object v0

    return-object v0
.end method

.method public static a(Z)Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;
    .locals 3

    if-nez p0, :cond_0

    invoke-static {}, Lcom/twitter/library/util/UrlNetworkStats;->b()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/twitter/library/util/UrlNetworkStats;->b:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    :goto_0
    return-object v0

    :cond_0
    sget-object v1, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;->d:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    if-eqz p0, :cond_2

    const-string/jumbo v0, "photo_quality_dynamic"

    :goto_1
    const-string/jumbo v2, "photo_quality_low"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v0, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;->a:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    :goto_2
    sget-object v1, Lcom/twitter/library/util/UrlNetworkStats;->b:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    if-eq v0, v1, :cond_1

    sput-object v0, Lcom/twitter/library/util/UrlNetworkStats;->b:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/twitter/library/util/UrlNetworkStats;->c:J

    :cond_1
    sget-object v0, Lcom/twitter/library/util/UrlNetworkStats;->b:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "photo_quality_1441"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v0, "photo_quality_1441"

    invoke-static {v0}, Lkk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    const-string/jumbo v2, "photo_quality_medium"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v0, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;->b:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    goto :goto_2

    :cond_4
    const-string/jumbo v2, "photo_quality_dynamic"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-static {}, Lcom/twitter/library/util/UrlNetworkStats;->c()I

    move-result v0

    if-eqz v0, :cond_7

    const/16 v1, 0x2800

    if-ge v0, v1, :cond_5

    sget-object v0, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;->a:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    goto :goto_2

    :cond_5
    const v1, 0x19000

    if-le v0, v1, :cond_6

    sget-object v0, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;->c:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    goto :goto_2

    :cond_6
    sget-object v0, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;->b:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    goto :goto_2

    :cond_7
    move-object v0, v1

    goto :goto_2
.end method

.method private static declared-synchronized a(IJ)V
    .locals 5

    const-class v1, Lcom/twitter/library/util/UrlNetworkStats;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/util/UrlNetworkStats;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v2, 0xa

    if-lt v0, v2, :cond_0

    sget-object v0, Lcom/twitter/library/util/UrlNetworkStats;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;

    :cond_0
    sget-object v0, Lcom/twitter/library/util/UrlNetworkStats;->a:Ljava/util/LinkedList;

    new-instance v2, Landroid/util/Pair;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static b()Z
    .locals 4

    sget-object v0, Lcom/twitter/library/util/UrlNetworkStats;->b:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    sget-object v1, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;->d:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    if-eq v0, v1, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sget-wide v2, Lcom/twitter/library/util/UrlNetworkStats;->c:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x1d4c0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static declared-synchronized c()I
    .locals 13

    const/4 v2, 0x0

    const-class v5, Lcom/twitter/library/util/UrlNetworkStats;

    monitor-enter v5

    :try_start_0
    sget-object v0, Lcom/twitter/library/util/UrlNetworkStats;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sget-object v0, Lcom/twitter/library/util/UrlNetworkStats;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v3, v2

    move v4, v2

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    sub-long v9, v6, v9

    const-wide/16 v11, 0x7530

    cmp-long v1, v9, v11

    if-gez v1, :cond_0

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int v1, v4, v0

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v4, v1

    goto :goto_0

    :cond_0
    if-lez v3, :cond_1

    div-int v0, v4, v3

    :goto_1
    sput v0, Lcom/twitter/library/util/UrlNetworkStats;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v5

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0

    :cond_1
    move v0, v2

    goto :goto_1
.end method
