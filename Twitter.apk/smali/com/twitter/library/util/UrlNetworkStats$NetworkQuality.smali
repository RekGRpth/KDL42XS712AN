.class public final enum Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;
.super Ljava/lang/Enum;
.source "Twttr"


# static fields
.field public static final enum a:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

.field public static final enum b:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

.field public static final enum c:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

.field public static final enum d:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

.field private static final synthetic e:[Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;


# instance fields
.field private mVal:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    new-instance v0, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    const-string/jumbo v1, "LOW"

    invoke-direct {v0, v1, v6, v3}, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;->a:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    new-instance v0, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    const-string/jumbo v1, "MEDIUM"

    invoke-direct {v0, v1, v3, v4}, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;->b:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    new-instance v0, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    const-string/jumbo v1, "HIGH"

    invoke-direct {v0, v1, v4, v5}, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;->c:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    new-instance v0, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    const-string/jumbo v1, "UNKNOWN"

    const/16 v2, 0x64

    invoke-direct {v0, v1, v5, v2}, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;->d:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    sget-object v1, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;->a:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    aput-object v1, v0, v6

    sget-object v1, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;->b:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    aput-object v1, v0, v3

    sget-object v1, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;->c:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    aput-object v1, v0, v4

    sget-object v1, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;->d:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    aput-object v1, v0, v5

    sput-object v0, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;->e:[Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;->mVal:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;
    .locals 1

    const-class v0, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    return-object v0
.end method

.method public static values()[Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;
    .locals 1

    sget-object v0, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;->e:[Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    invoke-virtual {v0}, [Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 1

    sget-object v0, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;->d:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;->c:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    sget-object v0, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;->a:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    sget-object v0, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;->b:Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
