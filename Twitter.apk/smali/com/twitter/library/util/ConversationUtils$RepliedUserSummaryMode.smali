.class public abstract enum Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;
.super Ljava/lang/Enum;
.source "Twttr"


# static fields
.field public static final enum a:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;

.field public static final enum b:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;

.field public static final enum c:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;

.field public static final enum d:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;

.field private static final synthetic e:[Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;


# instance fields
.field private mLayoutType:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode$1;

    const-string/jumbo v1, "DEFAULT"

    sget-object v2, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;->a:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;

    invoke-direct {v0, v1, v3, v2}, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode$1;-><init>(Ljava/lang/String;ILcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;)V

    sput-object v0, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;->a:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;

    new-instance v0, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode$2;

    const-string/jumbo v1, "IN_REPLY_TO"

    sget-object v2, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;->a:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;

    invoke-direct {v0, v1, v4, v2}, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode$2;-><init>(Ljava/lang/String;ILcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;)V

    sput-object v0, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;->b:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;

    new-instance v0, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode$3;

    const-string/jumbo v1, "NOTIFICATION"

    sget-object v2, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;->b:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;

    invoke-direct {v0, v1, v5, v2}, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode$3;-><init>(Ljava/lang/String;ILcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;)V

    sput-object v0, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;->c:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;

    new-instance v0, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode$4;

    const-string/jumbo v1, "OFF"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v6, v2}, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode$4;-><init>(Ljava/lang/String;ILcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;)V

    sput-object v0, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;->d:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;

    sget-object v1, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;->a:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;->b:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;->c:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;->d:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;

    aput-object v1, v0, v6

    sput-object v0, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;->e:[Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;->mLayoutType:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;Lcom/twitter/library/util/d;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;-><init>(Ljava/lang/String;ILcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;
    .locals 1

    const-class v0, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;

    return-object v0
.end method

.method public static values()[Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;
    .locals 1

    sget-object v0, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;->e:[Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;

    invoke-virtual {v0}, [Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/CharSequence;Landroid/text/TextPaint;III)Landroid/text/StaticLayout;
    .locals 6

    iget-object v0, p0, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;->mLayoutType:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;III)Landroid/text/StaticLayout;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(Landroid/content/res/Resources;Ljava/util/ArrayList;J)Ljava/lang/String;
.end method
