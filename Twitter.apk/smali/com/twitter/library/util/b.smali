.class public abstract Lcom/twitter/library/util/b;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static a:F

.field public static b:F

.field public static c:F

.field public static d:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/high16 v0, 0x3f800000    # 1.0f

    sput v0, Lcom/twitter/library/util/b;->a:F

    sput v0, Lcom/twitter/library/util/b;->b:F

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    sput v1, Lcom/twitter/library/util/b;->a:F

    iget v0, v0, Landroid/util/DisplayMetrics;->scaledDensity:F

    sput v0, Lcom/twitter/library/util/b;->b:F

    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    int-to-float v0, v0

    sput v0, Lcom/twitter/library/util/b;->c:F

    sget v0, Lcom/twitter/library/util/b;->c:F

    sget v1, Lcom/twitter/library/util/b;->c:F

    mul-float/2addr v0, v1

    sput v0, Lcom/twitter/library/util/b;->d:F

    invoke-static {p0}, Lcom/twitter/library/telephony/TelephonyUtil;->a(Landroid/content/Context;)V

    return-void
.end method
