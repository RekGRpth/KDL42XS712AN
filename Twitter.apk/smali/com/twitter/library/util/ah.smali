.class Lcom/twitter/library/util/ah;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/twitter/library/util/ag;

.field private final b:Ljava/util/HashMap;

.field private final c:J


# direct methods
.method public constructor <init>(Lcom/twitter/library/util/ag;JLjava/util/HashMap;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/util/ah;->a:Lcom/twitter/library/util/ag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p2, p0, Lcom/twitter/library/util/ah;->c:J

    iput-object p4, p0, Lcom/twitter/library/util/ah;->b:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    const/4 v13, 0x0

    iget-object v0, p0, Lcom/twitter/library/util/ah;->b:Ljava/util/HashMap;

    sget-boolean v1, Lcom/twitter/library/util/ag;->a:Z

    if-eqz v1, :cond_0

    const-string/jumbo v1, "ResourceCache"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Queueing "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v1

    if-lez v1, :cond_9

    new-instance v8, Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v1

    invoke-direct {v8, v1}, Ljava/util/HashMap;-><init>(I)V

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/twitter/library/util/aj;

    iget-object v5, v6, Lcom/twitter/library/util/aj;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/library/util/ah;->a:Lcom/twitter/library/util/ag;

    iget-wide v1, p0, Lcom/twitter/library/util/ah;->c:J

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/twitter/library/util/ag;->a(JLjava/lang/Object;Ljava/lang/String;)Lcom/twitter/library/util/af;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-virtual {v8, v4, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    sget-boolean v0, Lcom/twitter/library/util/ag;->a:Z

    if-eqz v0, :cond_3

    const-string/jumbo v0, "ResourceCache"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Fetch "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    if-eqz v5, :cond_1

    new-instance v0, Lcom/twitter/library/util/al;

    iget-object v1, p0, Lcom/twitter/library/util/ah;->a:Lcom/twitter/library/util/ag;

    iget-wide v2, p0, Lcom/twitter/library/util/ah;->c:J

    iget-boolean v6, v6, Lcom/twitter/library/util/aj;->b:Z

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/util/al;-><init>(Lcom/twitter/library/util/ag;JLjava/lang/Object;Ljava/lang/String;Z)V

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "file"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    const-string/jumbo v3, "content"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_4
    iget-object v2, p0, Lcom/twitter/library/util/ah;->a:Lcom/twitter/library/util/ag;

    iget-object v2, v2, Lcom/twitter/library/util/ag;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    :try_start_0
    invoke-virtual {v2, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/util/al;->a(Ljava/io/InputStream;)Lcom/twitter/library/util/af;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/twitter/library/util/ah;->a:Lcom/twitter/library/util/ag;

    invoke-virtual {v0, v4, v5, v13}, Lcom/twitter/library/util/ag;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)Lcom/twitter/library/util/af;

    move-result-object v0

    :cond_5
    :goto_2
    invoke-virtual {v8, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :catch_0
    move-exception v0

    move-object v0, v7

    goto :goto_1

    :cond_6
    new-instance v1, Lcom/twitter/library/network/d;

    iget-object v2, p0, Lcom/twitter/library/util/ah;->a:Lcom/twitter/library/util/ag;

    iget-object v2, v2, Lcom/twitter/library/util/ag;->c:Landroid/content/Context;

    invoke-direct {v1, v2, v5}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/twitter/library/util/ah;->a:Lcom/twitter/library/util/ag;

    invoke-virtual {v2, v4}, Lcom/twitter/library/util/ag;->a(Ljava/lang/Object;)Lcom/twitter/library/network/a;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-virtual {v0}, Lcom/twitter/library/util/al;->a()Lcom/twitter/library/util/af;

    move-result-object v0

    :goto_3
    if-nez v0, :cond_7

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v1

    iget v1, v1, Lcom/twitter/internal/network/k;->a:I

    if-eqz v1, :cond_5

    iget-object v0, p0, Lcom/twitter/library/util/ah;->a:Lcom/twitter/library/util/ag;

    invoke-virtual {v0, v4, v5, v13}, Lcom/twitter/library/util/ag;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)Lcom/twitter/library/util/af;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const-wide/32 v5, 0xea60

    add-long/2addr v1, v5

    iput-wide v1, v0, Lcom/twitter/library/util/af;->a:J

    goto :goto_2

    :cond_7
    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v1

    iget v2, v1, Lcom/twitter/internal/network/k;->a:I

    if-eqz v2, :cond_5

    iget v2, v1, Lcom/twitter/internal/network/k;->j:I

    int-to-long v2, v2

    long-to-int v6, v2

    iget-wide v10, v1, Lcom/twitter/internal/network/k;->d:J

    long-to-int v7, v10

    invoke-static {v6, v7}, Lcom/twitter/library/util/UrlNetworkStats;->a(II)I

    move-result v6

    iget-object v7, p0, Lcom/twitter/library/util/ah;->a:Lcom/twitter/library/util/ag;

    iget-object v7, v7, Lcom/twitter/library/util/ag;->c:Landroid/content/Context;

    new-instance v10, Lcom/twitter/library/scribe/ScribeLog;

    iget-wide v11, p0, Lcom/twitter/library/util/ah;->c:J

    invoke-direct {v10, v11, v12}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    iget-wide v11, v1, Lcom/twitter/internal/network/k;->d:J

    long-to-int v1, v2

    invoke-static {v5, v1}, Lcom/twitter/library/scribe/ScribeLog;->b(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v11, v12, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(JLjava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-static {v7, v1}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    const/16 v1, 0x400

    if-le v6, v1, :cond_5

    iget-object v1, p0, Lcom/twitter/library/util/ah;->a:Lcom/twitter/library/util/ag;

    iget-object v1, v1, Lcom/twitter/library/util/ag;->c:Landroid/content/Context;

    new-instance v7, Lcom/twitter/library/scribe/ScribeLog;

    iget-wide v10, p0, Lcom/twitter/library/util/ah;->c:J

    invoke-direct {v7, v10, v11}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    int-to-long v10, v6

    long-to-int v2, v2

    invoke-static {v5, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v10, v11, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(JLjava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    goto/16 :goto_2

    :cond_8
    invoke-static {}, Lcom/twitter/library/util/ag;->b()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/util/am;

    iget-object v2, p0, Lcom/twitter/library/util/ah;->a:Lcom/twitter/library/util/ag;

    invoke-direct {v1, v2, v8}, Lcom/twitter/library/util/am;-><init>(Lcom/twitter/library/util/ag;Ljava/util/HashMap;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_9
    return-void

    :cond_a
    move-object v0, v7

    goto :goto_3
.end method
