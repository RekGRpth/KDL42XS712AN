.class public abstract Lcom/twitter/library/util/ag;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field protected static final a:Z

.field protected static final b:Ljava/util/concurrent/ExecutorService;

.field private static final g:Landroid/os/Handler;


# instance fields
.field protected final c:Landroid/content/Context;

.field protected final d:Ljava/util/LinkedHashMap;

.field protected final e:Ljava/util/HashMap;

.field final f:[I

.field private final h:Landroid/support/v4/util/LruCache;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Lcom/twitter/library/client/App;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "ResourceCache"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/library/util/ag;->a:Z

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/util/ag;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/twitter/library/util/ag;->g:Landroid/os/Handler;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 4

    const/16 v3, 0x14

    const/high16 v2, 0x41200000    # 10.0f

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/twitter/library/util/ag;->f:[I

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/util/ag;->c:Landroid/content/Context;

    new-instance v0, Ljava/util/LinkedHashMap;

    const/4 v1, 0x1

    invoke-direct {v0, v3, v2, v1}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    iput-object v0, p0, Lcom/twitter/library/util/ag;->d:Ljava/util/LinkedHashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v3, v2}, Ljava/util/HashMap;-><init>(IF)V

    iput-object v0, p0, Lcom/twitter/library/util/ag;->e:Ljava/util/HashMap;

    if-lez p2, :cond_0

    new-instance v0, Lcom/twitter/library/util/ai;

    invoke-direct {v0, p2}, Lcom/twitter/library/util/ai;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/library/util/ag;->h:Landroid/support/v4/util/LruCache;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/util/ag;->h:Landroid/support/v4/util/LruCache;

    goto :goto_0
.end method

.method static synthetic b()Landroid/os/Handler;
    .locals 1

    sget-object v0, Lcom/twitter/library/util/ag;->g:Landroid/os/Handler;

    return-object v0
.end method

.method private b(JLjava/lang/Object;Ljava/lang/String;Z)V
    .locals 5

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/twitter/library/util/ag;->f:[I

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/util/ag;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p3}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/library/util/aj;

    invoke-direct {v0, p4, p5}, Lcom/twitter/library/util/aj;-><init>(Ljava/lang/String;Z)V

    iget-object v2, p0, Lcom/twitter/library/util/ag;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, p3, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/twitter/library/util/ag;->e:Ljava/util/HashMap;

    invoke-virtual {v2, p3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/util/ag;->g:Landroid/os/Handler;

    new-instance v2, Lcom/twitter/library/util/ak;

    invoke-direct {v2, p0, p1, p2}, Lcom/twitter/library/util/ak;-><init>(Lcom/twitter/library/util/ag;J)V

    const-wide/16 v3, 0x64

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    monitor-exit v1

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method protected a(Ljava/lang/Object;)Lcom/twitter/library/network/a;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected abstract a(JLjava/lang/Object;Ljava/lang/String;)Lcom/twitter/library/util/af;
.end method

.method protected abstract a(JLjava/lang/Object;Ljava/lang/String;Ljava/io/InputStream;)Lcom/twitter/library/util/af;
.end method

.method protected final a(JLjava/lang/Object;Ljava/lang/String;Z)Lcom/twitter/library/util/af;
    .locals 5

    const/4 v0, 0x0

    if-nez p4, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    sget-boolean v1, Lcom/twitter/library/util/ag;->a:Z

    if-eqz v1, :cond_2

    const-string/jumbo v1, "ResourceCache"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Url: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v1, p0, Lcom/twitter/library/util/ag;->h:Landroid/support/v4/util/LruCache;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/twitter/library/util/ag;->h:Landroid/support/v4/util/LruCache;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/util/ag;->h:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0, p3}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/af;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    if-eqz v0, :cond_6

    sget-boolean v1, Lcom/twitter/library/util/ag;->a:Z

    if-eqz v1, :cond_4

    const-string/jumbo v1, "ResourceCache"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Have resource: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/twitter/library/util/af;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v1, v0, Lcom/twitter/library/util/af;->b:Ljava/lang/String;

    invoke-virtual {v1, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-wide v1, v0, Lcom/twitter/library/util/af;->a:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    iget-wide v1, v0, Lcom/twitter/library/util/af;->a:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    :cond_5
    invoke-direct/range {p0 .. p5}, Lcom/twitter/library/util/ag;->b(JLjava/lang/Object;Ljava/lang/String;Z)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_6
    sget-boolean v1, Lcom/twitter/library/util/ag;->a:Z

    if-eqz v1, :cond_7

    const-string/jumbo v1, "ResourceCache"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Looking in persistent storage: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    invoke-direct/range {p0 .. p5}, Lcom/twitter/library/util/ag;->b(JLjava/lang/Object;Ljava/lang/String;Z)V

    goto/16 :goto_0
.end method

.method protected abstract a(Ljava/lang/Object;Ljava/lang/String;Ljava/io/InputStream;)Lcom/twitter/library/util/af;
.end method

.method protected abstract a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)Lcom/twitter/library/util/af;
.end method

.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/util/ag;->h:Landroid/support/v4/util/LruCache;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/library/util/ag;->h:Landroid/support/v4/util/LruCache;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/util/ag;->h:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0}, Landroid/support/v4/util/LruCache;->evictAll()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    iget-object v1, p0, Lcom/twitter/library/util/ag;->f:[I

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lcom/twitter/library/util/ag;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method protected abstract a(Ljava/util/HashMap;)V
.end method

.method public b(Ljava/lang/Object;)Lcom/twitter/library/util/af;
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/util/ag;->h:Landroid/support/v4/util/LruCache;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/library/util/ag;->h:Landroid/support/v4/util/LruCache;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/util/ag;->h:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/af;

    monitor-exit v1

    :goto_0
    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b(Ljava/util/HashMap;)V
    .locals 5

    invoke-virtual {p1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/af;

    iget-object v3, p0, Lcom/twitter/library/util/ag;->h:Landroid/support/v4/util/LruCache;

    if-eqz v3, :cond_1

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/twitter/library/util/ag;->h:Landroid/support/v4/util/LruCache;

    monitor-enter v3

    :try_start_0
    iget-object v4, p0, Lcom/twitter/library/util/ag;->h:Landroid/support/v4/util/LruCache;

    invoke-virtual {v4, v2, v0}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :cond_1
    iget-object v3, p0, Lcom/twitter/library/util/ag;->f:[I

    monitor-enter v3

    :try_start_1
    iget-object v0, p0, Lcom/twitter/library/util/ag;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, v2}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v3

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_2
    invoke-virtual {p0, p1}, Lcom/twitter/library/util/ag;->a(Ljava/util/HashMap;)V

    goto :goto_0
.end method

.method public c(Ljava/lang/Object;)Lcom/twitter/library/util/af;
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/util/ag;->h:Landroid/support/v4/util/LruCache;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/library/util/ag;->h:Landroid/support/v4/util/LruCache;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/util/ag;->h:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/support/v4/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/af;

    monitor-exit v1

    :goto_0
    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
