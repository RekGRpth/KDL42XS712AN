.class Lcom/twitter/library/util/al;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/internal/network/i;


# instance fields
.field final synthetic a:Lcom/twitter/library/util/ag;

.field private final b:J

.field private final c:Ljava/lang/Object;

.field private final d:Ljava/lang/String;

.field private final e:Z

.field private f:Lcom/twitter/library/util/af;


# direct methods
.method public constructor <init>(Lcom/twitter/library/util/ag;JLjava/lang/Object;Ljava/lang/String;Z)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/library/util/al;->a:Lcom/twitter/library/util/ag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/util/al;->f:Lcom/twitter/library/util/af;

    iput-wide p2, p0, Lcom/twitter/library/util/al;->b:J

    iput-object p4, p0, Lcom/twitter/library/util/al;->c:Ljava/lang/Object;

    iput-object p5, p0, Lcom/twitter/library/util/al;->d:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/twitter/library/util/al;->e:Z

    return-void
.end method


# virtual methods
.method public final a()Lcom/twitter/library/util/af;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/util/al;->f:Lcom/twitter/library/util/af;

    return-object v0
.end method

.method public final a(Ljava/io/InputStream;)Lcom/twitter/library/util/af;
    .locals 6

    iget-boolean v0, p0, Lcom/twitter/library/util/al;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/util/al;->a:Lcom/twitter/library/util/ag;

    iget-wide v1, p0, Lcom/twitter/library/util/al;->b:J

    iget-object v3, p0, Lcom/twitter/library/util/al;->c:Ljava/lang/Object;

    iget-object v4, p0, Lcom/twitter/library/util/al;->d:Ljava/lang/String;

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/util/ag;->a(JLjava/lang/Object;Ljava/lang/String;Ljava/io/InputStream;)Lcom/twitter/library/util/af;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/util/al;->a:Lcom/twitter/library/util/ag;

    iget-object v1, p0, Lcom/twitter/library/util/al;->c:Ljava/lang/Object;

    iget-object v2, p0, Lcom/twitter/library/util/al;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p1}, Lcom/twitter/library/util/ag;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/io/InputStream;)Lcom/twitter/library/util/af;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(ILjava/io/InputStream;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p0, p2}, Lcom/twitter/library/util/al;->a(Ljava/io/InputStream;)Lcom/twitter/library/util/af;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/util/al;->f:Lcom/twitter/library/util/af;

    return-void
.end method

.method public a(Lcom/twitter/internal/network/k;)V
    .locals 0

    return-void
.end method
