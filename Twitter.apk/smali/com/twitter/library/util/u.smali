.class public Lcom/twitter/library/util/u;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static varargs a(Landroid/content/Context;I[Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    instance-of v1, v0, Landroid/text/Spanned;

    if-nez v1, :cond_0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Landroid/text/Spanned;

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a(Landroid/text/Spanned;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;I)Ljava/lang/CharSequence;
    .locals 9

    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const-string/jumbo v0, ""

    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v2, :cond_3

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaTag;

    iget-wide v6, v0, Lcom/twitter/library/api/MediaTag;->userId:J

    cmp-long v1, v6, v4

    if-nez v1, :cond_2

    sget v1, Lil;->media_tag_you:I

    :goto_1
    new-array v4, v2, [Ljava/lang/Object;

    iget-object v0, v0, Lcom/twitter/library/api/MediaTag;->name:Ljava/lang/String;

    aput-object v0, v4, v3

    invoke-static {p0, v1, v4}, Lcom/twitter/library/util/u;->a(Landroid/content/Context;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    :goto_2
    if-eqz p2, :cond_a

    new-instance v0, Landroid/text/SpannableStringBuilder;

    const-string/jumbo v4, "  "

    invoke-direct {v0, v4}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v1, Landroid/text/style/ImageSpan;

    invoke-direct {v1, p0, p2, v2}, Landroid/text/style/ImageSpan;-><init>(Landroid/content/Context;II)V

    const/16 v4, 0x12

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    :cond_2
    sget v1, Lil;->media_tag_user_display_name:I

    goto :goto_1

    :cond_3
    invoke-static {p1, v4, v5}, Lcom/twitter/library/util/u;->b(Ljava/util/List;J)Lcom/twitter/library/api/MediaTag;

    move-result-object v0

    if-eqz v0, :cond_4

    move v0, v2

    :goto_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v6, :cond_8

    if-eqz v0, :cond_7

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaTag;

    iget-wide v0, v0, Lcom/twitter/library/api/MediaTag;->userId:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_5

    move v1, v3

    :goto_4
    if-nez v1, :cond_6

    move v0, v2

    :goto_5
    sget v4, Lil;->media_tag_you_two_summary:I

    move v8, v0

    move v0, v1

    move v1, v8

    :goto_6
    new-array v5, v6, [Ljava/lang/Object;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaTag;

    iget-object v0, v0, Lcom/twitter/library/api/MediaTag;->name:Ljava/lang/String;

    aput-object v0, v5, v3

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaTag;

    iget-object v0, v0, Lcom/twitter/library/api/MediaTag;->name:Ljava/lang/String;

    aput-object v0, v5, v2

    invoke-static {p0, v4, v5}, Lcom/twitter/library/util/u;->a(Landroid/content/Context;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_2

    :cond_4
    move v0, v3

    goto :goto_3

    :cond_5
    move v1, v2

    goto :goto_4

    :cond_6
    move v0, v3

    goto :goto_5

    :cond_7
    sget v0, Lil;->media_tag_two_summary:I

    move v1, v2

    move v4, v0

    move v0, v3

    goto :goto_6

    :cond_8
    if-eqz v0, :cond_9

    invoke-static {p1, v4, v5}, Lcom/twitter/library/util/u;->b(Ljava/util/List;J)Lcom/twitter/library/api/MediaTag;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/api/MediaTag;->name:Ljava/lang/String;

    sget v1, Lil;->media_tag_you_multiple_summary:I

    :goto_7
    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v2

    invoke-static {p0, v1, v4}, Lcom/twitter/library/util/u;->a(Landroid/content/Context;I[Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    goto/16 :goto_2

    :cond_9
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaTag;

    iget-object v0, v0, Lcom/twitter/library/api/MediaTag;->name:Ljava/lang/String;

    sget v1, Lil;->media_tag_multiple_summary:I

    goto :goto_7

    :cond_a
    move-object v0, v1

    goto/16 :goto_0
.end method

.method public static a(Ljava/util/List;)Ljava/util/List;
    .locals 3

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    invoke-virtual {v0}, Lcom/twitter/library/api/MediaEntity;->b()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    invoke-virtual {v0}, Lcom/twitter/library/api/MediaEntity;->b()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_2
    invoke-virtual {v1}, Ljava/util/LinkedHashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public static a(Ljava/util/List;Ljava/util/Set;)Ljava/util/List;
    .locals 5

    invoke-static {p0}, Lcom/twitter/library/util/u;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaTag;

    iget-wide v3, v0, Lcom/twitter/library/api/MediaTag;->userId:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public static a(Ljava/util/List;J)Z
    .locals 1

    invoke-static {p0}, Lcom/twitter/library/util/u;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/twitter/library/util/u;->b(Ljava/util/List;J)Lcom/twitter/library/api/MediaTag;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/util/List;J)Lcom/twitter/library/api/MediaTag;
    .locals 4

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaTag;

    iget-wide v2, v0, Lcom/twitter/library/api/MediaTag;->userId:J

    cmp-long v2, v2, p1

    if-nez v2, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/util/List;)[J
    .locals 5

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [J

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, v2

    if-ge v1, v0, :cond_0

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaTag;

    iget-wide v3, v0, Lcom/twitter/library/api/MediaTag;->userId:J

    aput-wide v3, v2, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-object v2
.end method
