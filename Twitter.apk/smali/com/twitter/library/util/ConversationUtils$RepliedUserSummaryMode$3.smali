.class final enum Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode$3;
.super Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;
.source "Twttr"


# direct methods
.method constructor <init>(Ljava/lang/String;ILcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;-><init>(Ljava/lang/String;ILcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;Lcom/twitter/library/util/d;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/res/Resources;Ljava/util/ArrayList;J)Ljava/lang/String;
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v2, v1, -0x2

    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/RepliedUser;

    iget-wide v3, v0, Lcom/twitter/library/api/RepliedUser;->userId:J

    cmp-long v0, v3, p3

    if-nez v0, :cond_2

    if-le v1, v5, :cond_1

    new-array v1, v5, [Ljava/lang/String;

    invoke-virtual {p2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/RepliedUser;

    invoke-virtual {v0}, Lcom/twitter/library/api/RepliedUser;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v6

    new-array v0, v5, [I

    sget v3, Lil;->convo_tweet_you_and_someone:I

    aput v3, v0, v6

    sget v3, Lij;->notification_replied_you_someone_others:I

    invoke-static {p1, v1, v2, v0, v3}, Lcom/twitter/library/util/ConversationUtils;->a(Landroid/content/res/Resources;[Ljava/lang/String;I[II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget v0, Lil;->convo_to_you:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    if-le v1, v5, :cond_3

    invoke-virtual {p2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/RepliedUser;

    iget-wide v3, v0, Lcom/twitter/library/api/RepliedUser;->userId:J

    cmp-long v0, v3, p3

    if-nez v0, :cond_3

    new-array v1, v5, [Ljava/lang/String;

    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/RepliedUser;

    invoke-virtual {v0}, Lcom/twitter/library/api/RepliedUser;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v6

    new-array v0, v5, [I

    sget v3, Lil;->convo_tweet_someone_and_you:I

    aput v3, v0, v6

    sget v3, Lij;->notification_replied_someone_you_others:I

    invoke-static {p1, v1, v2, v0, v3}, Lcom/twitter/library/util/ConversationUtils;->a(Landroid/content/res/Resources;[Ljava/lang/String;I[II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    if-le v1, v5, :cond_4

    new-array v1, v7, [Ljava/lang/String;

    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/RepliedUser;

    invoke-virtual {v0}, Lcom/twitter/library/api/RepliedUser;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v6

    invoke-virtual {p2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/RepliedUser;

    invoke-virtual {v0}, Lcom/twitter/library/api/RepliedUser;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v5

    move-object v0, v1

    :goto_1
    new-array v1, v7, [I

    sget v3, Lil;->social_conversation_tweet:I

    aput v3, v1, v6

    sget v3, Lil;->social_conversation_tweet_two:I

    aput v3, v1, v5

    sget v3, Lij;->social_proof_in_reply_multiple_names_and_count:I

    invoke-static {p1, v0, v2, v1, v3}, Lcom/twitter/library/util/ConversationUtils;->a(Landroid/content/res/Resources;[Ljava/lang/String;I[II)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_4
    new-array v1, v5, [Ljava/lang/String;

    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/RepliedUser;

    invoke-virtual {v0}, Lcom/twitter/library/api/RepliedUser;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v6

    move-object v0, v1

    goto :goto_1
.end method
