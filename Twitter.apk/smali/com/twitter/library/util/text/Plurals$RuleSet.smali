.class final enum Lcom/twitter/library/util/text/Plurals$RuleSet;
.super Ljava/lang/Enum;
.source "Twttr"


# static fields
.field public static final enum a:Lcom/twitter/library/util/text/Plurals$RuleSet;

.field public static final enum b:Lcom/twitter/library/util/text/Plurals$RuleSet;

.field public static final enum c:Lcom/twitter/library/util/text/Plurals$RuleSet;

.field public static final enum d:Lcom/twitter/library/util/text/Plurals$RuleSet;

.field public static final enum e:Lcom/twitter/library/util/text/Plurals$RuleSet;

.field public static final enum f:Lcom/twitter/library/util/text/Plurals$RuleSet;

.field public static final enum g:Lcom/twitter/library/util/text/Plurals$RuleSet;

.field private static final synthetic h:[Lcom/twitter/library/util/text/Plurals$RuleSet;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/twitter/library/util/text/Plurals$RuleSet;

    const-string/jumbo v1, "SET1"

    invoke-direct {v0, v1, v3}, Lcom/twitter/library/util/text/Plurals$RuleSet;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/util/text/Plurals$RuleSet;->a:Lcom/twitter/library/util/text/Plurals$RuleSet;

    new-instance v0, Lcom/twitter/library/util/text/Plurals$RuleSet;

    const-string/jumbo v1, "SET2"

    invoke-direct {v0, v1, v4}, Lcom/twitter/library/util/text/Plurals$RuleSet;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/util/text/Plurals$RuleSet;->b:Lcom/twitter/library/util/text/Plurals$RuleSet;

    new-instance v0, Lcom/twitter/library/util/text/Plurals$RuleSet;

    const-string/jumbo v1, "SET3"

    invoke-direct {v0, v1, v5}, Lcom/twitter/library/util/text/Plurals$RuleSet;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/util/text/Plurals$RuleSet;->c:Lcom/twitter/library/util/text/Plurals$RuleSet;

    new-instance v0, Lcom/twitter/library/util/text/Plurals$RuleSet;

    const-string/jumbo v1, "SET5"

    invoke-direct {v0, v1, v6}, Lcom/twitter/library/util/text/Plurals$RuleSet;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/util/text/Plurals$RuleSet;->d:Lcom/twitter/library/util/text/Plurals$RuleSet;

    new-instance v0, Lcom/twitter/library/util/text/Plurals$RuleSet;

    const-string/jumbo v1, "SET11"

    invoke-direct {v0, v1, v7}, Lcom/twitter/library/util/text/Plurals$RuleSet;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/util/text/Plurals$RuleSet;->e:Lcom/twitter/library/util/text/Plurals$RuleSet;

    new-instance v0, Lcom/twitter/library/util/text/Plurals$RuleSet;

    const-string/jumbo v1, "SET13"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/util/text/Plurals$RuleSet;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/util/text/Plurals$RuleSet;->f:Lcom/twitter/library/util/text/Plurals$RuleSet;

    new-instance v0, Lcom/twitter/library/util/text/Plurals$RuleSet;

    const-string/jumbo v1, "NONE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/util/text/Plurals$RuleSet;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/util/text/Plurals$RuleSet;->g:Lcom/twitter/library/util/text/Plurals$RuleSet;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/twitter/library/util/text/Plurals$RuleSet;

    sget-object v1, Lcom/twitter/library/util/text/Plurals$RuleSet;->a:Lcom/twitter/library/util/text/Plurals$RuleSet;

    aput-object v1, v0, v3

    sget-object v1, Lcom/twitter/library/util/text/Plurals$RuleSet;->b:Lcom/twitter/library/util/text/Plurals$RuleSet;

    aput-object v1, v0, v4

    sget-object v1, Lcom/twitter/library/util/text/Plurals$RuleSet;->c:Lcom/twitter/library/util/text/Plurals$RuleSet;

    aput-object v1, v0, v5

    sget-object v1, Lcom/twitter/library/util/text/Plurals$RuleSet;->d:Lcom/twitter/library/util/text/Plurals$RuleSet;

    aput-object v1, v0, v6

    sget-object v1, Lcom/twitter/library/util/text/Plurals$RuleSet;->e:Lcom/twitter/library/util/text/Plurals$RuleSet;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/twitter/library/util/text/Plurals$RuleSet;->f:Lcom/twitter/library/util/text/Plurals$RuleSet;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/twitter/library/util/text/Plurals$RuleSet;->g:Lcom/twitter/library/util/text/Plurals$RuleSet;

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/library/util/text/Plurals$RuleSet;->h:[Lcom/twitter/library/util/text/Plurals$RuleSet;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/library/util/text/Plurals$RuleSet;
    .locals 1

    const-class v0, Lcom/twitter/library/util/text/Plurals$RuleSet;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/text/Plurals$RuleSet;

    return-object v0
.end method

.method public static values()[Lcom/twitter/library/util/text/Plurals$RuleSet;
    .locals 1

    sget-object v0, Lcom/twitter/library/util/text/Plurals$RuleSet;->h:[Lcom/twitter/library/util/text/Plurals$RuleSet;

    invoke-virtual {v0}, [Lcom/twitter/library/util/text/Plurals$RuleSet;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/util/text/Plurals$RuleSet;

    return-object v0
.end method
