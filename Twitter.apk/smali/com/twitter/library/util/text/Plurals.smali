.class public abstract Lcom/twitter/library/util/text/Plurals;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string/jumbo v1, "ar"

    sget-object v2, Lcom/twitter/library/util/text/Plurals$RuleSet;->a:Lcom/twitter/library/util/text/Plurals$RuleSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "da"

    sget-object v2, Lcom/twitter/library/util/text/Plurals$RuleSet;->c:Lcom/twitter/library/util/text/Plurals$RuleSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "de"

    sget-object v2, Lcom/twitter/library/util/text/Plurals$RuleSet;->c:Lcom/twitter/library/util/text/Plurals$RuleSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "en"

    sget-object v2, Lcom/twitter/library/util/text/Plurals$RuleSet;->c:Lcom/twitter/library/util/text/Plurals$RuleSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "es"

    sget-object v2, Lcom/twitter/library/util/text/Plurals$RuleSet;->c:Lcom/twitter/library/util/text/Plurals$RuleSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "fa"

    sget-object v2, Lcom/twitter/library/util/text/Plurals$RuleSet;->g:Lcom/twitter/library/util/text/Plurals$RuleSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "fi"

    sget-object v2, Lcom/twitter/library/util/text/Plurals$RuleSet;->c:Lcom/twitter/library/util/text/Plurals$RuleSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "fr"

    sget-object v2, Lcom/twitter/library/util/text/Plurals$RuleSet;->d:Lcom/twitter/library/util/text/Plurals$RuleSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "he"

    sget-object v2, Lcom/twitter/library/util/text/Plurals$RuleSet;->b:Lcom/twitter/library/util/text/Plurals$RuleSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "hi"

    sget-object v2, Lcom/twitter/library/util/text/Plurals$RuleSet;->c:Lcom/twitter/library/util/text/Plurals$RuleSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "hu"

    sget-object v2, Lcom/twitter/library/util/text/Plurals$RuleSet;->g:Lcom/twitter/library/util/text/Plurals$RuleSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "in"

    sget-object v2, Lcom/twitter/library/util/text/Plurals$RuleSet;->g:Lcom/twitter/library/util/text/Plurals$RuleSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "it"

    sget-object v2, Lcom/twitter/library/util/text/Plurals$RuleSet;->c:Lcom/twitter/library/util/text/Plurals$RuleSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "iw"

    sget-object v2, Lcom/twitter/library/util/text/Plurals$RuleSet;->b:Lcom/twitter/library/util/text/Plurals$RuleSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "ja"

    sget-object v2, Lcom/twitter/library/util/text/Plurals$RuleSet;->g:Lcom/twitter/library/util/text/Plurals$RuleSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "ko"

    sget-object v2, Lcom/twitter/library/util/text/Plurals$RuleSet;->g:Lcom/twitter/library/util/text/Plurals$RuleSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "ms"

    sget-object v2, Lcom/twitter/library/util/text/Plurals$RuleSet;->g:Lcom/twitter/library/util/text/Plurals$RuleSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "nb"

    sget-object v2, Lcom/twitter/library/util/text/Plurals$RuleSet;->c:Lcom/twitter/library/util/text/Plurals$RuleSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "nl"

    sget-object v2, Lcom/twitter/library/util/text/Plurals$RuleSet;->c:Lcom/twitter/library/util/text/Plurals$RuleSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "pl"

    sget-object v2, Lcom/twitter/library/util/text/Plurals$RuleSet;->f:Lcom/twitter/library/util/text/Plurals$RuleSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "pt"

    sget-object v2, Lcom/twitter/library/util/text/Plurals$RuleSet;->c:Lcom/twitter/library/util/text/Plurals$RuleSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "ru"

    sget-object v2, Lcom/twitter/library/util/text/Plurals$RuleSet;->e:Lcom/twitter/library/util/text/Plurals$RuleSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "sv"

    sget-object v2, Lcom/twitter/library/util/text/Plurals$RuleSet;->c:Lcom/twitter/library/util/text/Plurals$RuleSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "th"

    sget-object v2, Lcom/twitter/library/util/text/Plurals$RuleSet;->g:Lcom/twitter/library/util/text/Plurals$RuleSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "tl"

    sget-object v2, Lcom/twitter/library/util/text/Plurals$RuleSet;->c:Lcom/twitter/library/util/text/Plurals$RuleSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "tr"

    sget-object v2, Lcom/twitter/library/util/text/Plurals$RuleSet;->g:Lcom/twitter/library/util/text/Plurals$RuleSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "zh"

    sget-object v2, Lcom/twitter/library/util/text/Plurals$RuleSet;->g:Lcom/twitter/library/util/text/Plurals$RuleSet;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/util/text/Plurals;->a:Ljava/util/Map;

    return-void
.end method

.method public static a(D)Lcom/twitter/library/util/text/Plurals$Rule;
    .locals 1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lcom/twitter/library/util/text/Plurals;->a(Ljava/lang/String;D)Lcom/twitter/library/util/text/Plurals$Rule;

    move-result-object v0

    return-object v0
.end method

.method public static a(F)Lcom/twitter/library/util/text/Plurals$Rule;
    .locals 2

    float-to-double v0, p0

    invoke-static {v0, v1}, Lcom/twitter/library/util/text/Plurals;->a(D)Lcom/twitter/library/util/text/Plurals$Rule;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lcom/twitter/library/util/text/Plurals$Rule;
    .locals 2

    :try_start_0
    invoke-static {p0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/twitter/library/util/text/Plurals;->a(D)Lcom/twitter/library/util/text/Plurals$Rule;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/twitter/library/util/text/Plurals$Rule;->f:Lcom/twitter/library/util/text/Plurals$Rule;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;D)Lcom/twitter/library/util/text/Plurals$Rule;
    .locals 8

    double-to-long v2, p1

    long-to-double v0, v2

    cmpl-double v0, p1, v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    sget-object v0, Lcom/twitter/library/util/text/Plurals;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/text/Plurals$RuleSet;

    if-nez v0, :cond_0

    sget-object v0, Lcom/twitter/library/util/text/Plurals$RuleSet;->g:Lcom/twitter/library/util/text/Plurals$RuleSet;

    :cond_0
    sget-object v4, Lcom/twitter/library/util/text/a;->a:[I

    invoke-virtual {v0}, Lcom/twitter/library/util/text/Plurals$RuleSet;->ordinal()I

    move-result v0

    aget v0, v4, v0

    packed-switch v0, :pswitch_data_0

    :cond_1
    sget-object v0, Lcom/twitter/library/util/text/Plurals$Rule;->f:Lcom/twitter/library/util/text/Plurals$Rule;

    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    :pswitch_0
    if-eqz v1, :cond_1

    const-wide/16 v0, 0x64

    rem-long v0, v2, v0

    const-wide/16 v4, 0x3

    cmp-long v4, v0, v4

    if-ltz v4, :cond_3

    const-wide/16 v4, 0xa

    cmp-long v4, v0, v4

    if-gtz v4, :cond_3

    sget-object v0, Lcom/twitter/library/util/text/Plurals$Rule;->d:Lcom/twitter/library/util/text/Plurals$Rule;

    goto :goto_1

    :cond_3
    const-wide/16 v4, 0xb

    cmp-long v4, v0, v4

    if-ltz v4, :cond_4

    const-wide/16 v4, 0x63

    cmp-long v0, v0, v4

    if-gtz v0, :cond_4

    sget-object v0, Lcom/twitter/library/util/text/Plurals$Rule;->e:Lcom/twitter/library/util/text/Plurals$Rule;

    goto :goto_1

    :cond_4
    const-wide/16 v0, 0x1

    cmp-long v0, v2, v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/twitter/library/util/text/Plurals$Rule;->b:Lcom/twitter/library/util/text/Plurals$Rule;

    goto :goto_1

    :cond_5
    const-wide/16 v0, 0x2

    cmp-long v0, v2, v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/twitter/library/util/text/Plurals$Rule;->c:Lcom/twitter/library/util/text/Plurals$Rule;

    goto :goto_1

    :cond_6
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/twitter/library/util/text/Plurals$Rule;->a:Lcom/twitter/library/util/text/Plurals$Rule;

    goto :goto_1

    :pswitch_1
    if-eqz v1, :cond_1

    const-wide/16 v0, 0xa

    rem-long v0, v2, v0

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-eqz v4, :cond_7

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-nez v0, :cond_7

    sget-object v0, Lcom/twitter/library/util/text/Plurals$Rule;->e:Lcom/twitter/library/util/text/Plurals$Rule;

    goto :goto_1

    :cond_7
    const-wide/16 v0, 0x1

    cmp-long v0, v2, v0

    if-nez v0, :cond_8

    sget-object v0, Lcom/twitter/library/util/text/Plurals$Rule;->b:Lcom/twitter/library/util/text/Plurals$Rule;

    goto :goto_1

    :cond_8
    const-wide/16 v0, 0x2

    cmp-long v0, v2, v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/twitter/library/util/text/Plurals$Rule;->c:Lcom/twitter/library/util/text/Plurals$Rule;

    goto :goto_1

    :pswitch_2
    if-eqz v1, :cond_1

    const-wide/16 v0, 0x1

    cmp-long v0, v2, v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/twitter/library/util/text/Plurals$Rule;->b:Lcom/twitter/library/util/text/Plurals$Rule;

    goto :goto_1

    :pswitch_3
    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-ltz v0, :cond_1

    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    cmpg-double v0, p1, v0

    if-gez v0, :cond_1

    sget-object v0, Lcom/twitter/library/util/text/Plurals$Rule;->b:Lcom/twitter/library/util/text/Plurals$Rule;

    goto/16 :goto_1

    :pswitch_4
    if-eqz v1, :cond_1

    const-wide/16 v0, 0xa

    rem-long v0, v2, v0

    const-wide/16 v4, 0x64

    rem-long/2addr v2, v4

    const-wide/16 v4, 0x2

    cmp-long v4, v0, v4

    if-ltz v4, :cond_a

    const-wide/16 v4, 0x4

    cmp-long v4, v0, v4

    if-gtz v4, :cond_a

    const-wide/16 v4, 0xc

    cmp-long v4, v2, v4

    if-ltz v4, :cond_9

    const-wide/16 v4, 0xe

    cmp-long v4, v2, v4

    if-lez v4, :cond_a

    :cond_9
    sget-object v0, Lcom/twitter/library/util/text/Plurals$Rule;->d:Lcom/twitter/library/util/text/Plurals$Rule;

    goto/16 :goto_1

    :cond_a
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-eqz v4, :cond_c

    const-wide/16 v4, 0x5

    cmp-long v4, v0, v4

    if-ltz v4, :cond_b

    const-wide/16 v4, 0x9

    cmp-long v4, v0, v4

    if-lez v4, :cond_c

    :cond_b
    const-wide/16 v4, 0xb

    cmp-long v4, v2, v4

    if-ltz v4, :cond_d

    const-wide/16 v4, 0xe

    cmp-long v4, v2, v4

    if-gtz v4, :cond_d

    :cond_c
    sget-object v0, Lcom/twitter/library/util/text/Plurals$Rule;->e:Lcom/twitter/library/util/text/Plurals$Rule;

    goto/16 :goto_1

    :cond_d
    const-wide/16 v4, 0x1

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    const-wide/16 v0, 0xb

    cmp-long v0, v2, v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/twitter/library/util/text/Plurals$Rule;->b:Lcom/twitter/library/util/text/Plurals$Rule;

    goto/16 :goto_1

    :pswitch_5
    if-eqz v1, :cond_1

    const-wide/16 v0, 0xa

    rem-long v0, v2, v0

    const-wide/16 v4, 0x64

    rem-long v4, v2, v4

    const-wide/16 v6, 0x2

    cmp-long v6, v0, v6

    if-ltz v6, :cond_f

    const-wide/16 v6, 0x4

    cmp-long v6, v0, v6

    if-gtz v6, :cond_f

    const-wide/16 v6, 0xc

    cmp-long v6, v4, v6

    if-ltz v6, :cond_e

    const-wide/16 v6, 0xe

    cmp-long v6, v4, v6

    if-lez v6, :cond_f

    :cond_e
    sget-object v0, Lcom/twitter/library/util/text/Plurals$Rule;->d:Lcom/twitter/library/util/text/Plurals$Rule;

    goto/16 :goto_1

    :cond_f
    const-wide/16 v6, 0x1

    cmp-long v6, v2, v6

    if-eqz v6, :cond_10

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-ltz v6, :cond_10

    const-wide/16 v6, 0x1

    cmp-long v6, v0, v6

    if-lez v6, :cond_12

    :cond_10
    const-wide/16 v6, 0x5

    cmp-long v6, v0, v6

    if-ltz v6, :cond_11

    const-wide/16 v6, 0x9

    cmp-long v0, v0, v6

    if-lez v0, :cond_12

    :cond_11
    const-wide/16 v0, 0xc

    cmp-long v0, v4, v0

    if-ltz v0, :cond_13

    const-wide/16 v0, 0xe

    cmp-long v0, v4, v0

    if-gtz v0, :cond_13

    :cond_12
    sget-object v0, Lcom/twitter/library/util/text/Plurals$Rule;->e:Lcom/twitter/library/util/text/Plurals$Rule;

    goto/16 :goto_1

    :cond_13
    const-wide/16 v0, 0x1

    cmp-long v0, v2, v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/twitter/library/util/text/Plurals$Rule;->b:Lcom/twitter/library/util/text/Plurals$Rule;

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
