.class Lcom/twitter/library/util/ak;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/twitter/library/util/ag;

.field private final b:J


# direct methods
.method public constructor <init>(Lcom/twitter/library/util/ag;J)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/util/ak;->a:Lcom/twitter/library/util/ag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p2, p0, Lcom/twitter/library/util/ak;->b:J

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v0, p0, Lcom/twitter/library/util/ak;->a:Lcom/twitter/library/util/ag;

    iget-object v1, v0, Lcom/twitter/library/util/ag;->f:[I

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/util/ak;->a:Lcom/twitter/library/util/ag;

    iget-object v0, v0, Lcom/twitter/library/util/ag;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/util/HashMap;

    iget-object v2, p0, Lcom/twitter/library/util/ak;->a:Lcom/twitter/library/util/ag;

    iget-object v2, v2, Lcom/twitter/library/util/ag;->e:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    iget-object v2, p0, Lcom/twitter/library/util/ak;->a:Lcom/twitter/library/util/ag;

    iget-object v2, v2, Lcom/twitter/library/util/ag;->e:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    iget-object v2, p0, Lcom/twitter/library/util/ak;->a:Lcom/twitter/library/util/ag;

    iget-object v2, v2, Lcom/twitter/library/util/ag;->e:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v1, Lcom/twitter/library/util/ag;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/twitter/library/util/ah;

    iget-object v3, p0, Lcom/twitter/library/util/ak;->a:Lcom/twitter/library/util/ag;

    iget-wide v4, p0, Lcom/twitter/library/util/ak;->b:J

    invoke-direct {v2, v3, v4, v5, v0}, Lcom/twitter/library/util/ah;-><init>(Lcom/twitter/library/util/ag;JLjava/util/HashMap;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
