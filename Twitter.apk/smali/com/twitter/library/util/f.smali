.class public Lcom/twitter/library/util/f;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/util/w;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:J

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;J)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;JLjava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/util/f;->a:Landroid/content/Context;

    iput-wide p2, p0, Lcom/twitter/library/util/f;->b:J

    iput-object p4, p0, Lcom/twitter/library/util/f;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/library/util/f;->a:Landroid/content/Context;

    iget-wide v1, p0, Lcom/twitter/library/util/f;->b:J

    iget-object v3, p0, Lcom/twitter/library/util/f;->c:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, p1}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/util/f;->c:Ljava/lang/String;

    return-void
.end method
