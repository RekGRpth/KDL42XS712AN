.class public Lcom/twitter/library/network/n;
.super Lcom/twitter/library/network/a;
.source "Twttr"


# static fields
.field public static final a:[B

.field public static final b:[B

.field public static final c:Ljava/lang/String;

.field public static final d:Ljava/lang/String;

.field public static final e:[B

.field public static final f:Ljava/lang/String;

.field public static final g:[B

.field public static final h:[B

.field public static final i:Ljava/lang/String;

.field public static final j:Ljava/lang/String;

.field public static final k:[B

.field public static final l:[B

.field public static final m:[B

.field public static final n:[B

.field public static final o:Ljava/lang/String;

.field public static final p:Ljava/lang/String;

.field public static final q:Ljava/lang/String;

.field public static final r:Ljava/lang/String;

.field public static final s:Lcom/twitter/library/network/OAuthToken;

.field private static final t:Ljava/security/SecureRandom;


# instance fields
.field private final u:Lcom/twitter/library/network/OAuthToken;

.field private final v:I

.field private final w:Ljava/lang/String;

.field private final x:Ljava/lang/String;

.field private final y:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v4, 0x2b

    const/4 v3, 0x7

    const/16 v2, 0x16

    const/16 v1, 0x8

    const/16 v0, 0x14

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/twitter/library/network/n;->a:[B

    new-array v0, v4, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/twitter/library/network/n;->b:[B

    sget-object v0, Lcom/twitter/library/network/n;->a:[B

    invoke-static {v2, v0}, Lcom/twitter/library/network/n;->a(B[B)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/network/n;->c:Ljava/lang/String;

    sget-object v0, Lcom/twitter/library/network/n;->b:[B

    invoke-static {v2, v0}, Lcom/twitter/library/network/n;->a(B[B)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/network/n;->d:Ljava/lang/String;

    const/16 v0, 0x6b

    new-array v0, v0, [B

    fill-array-data v0, :array_2

    sput-object v0, Lcom/twitter/library/network/n;->e:[B

    sget-object v0, Lcom/twitter/library/network/n;->e:[B

    invoke-static {v2, v0}, Lcom/twitter/library/network/n;->a(B[B)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/network/n;->f:Ljava/lang/String;

    const/16 v0, 0x29

    new-array v0, v0, [B

    fill-array-data v0, :array_3

    sput-object v0, Lcom/twitter/library/network/n;->g:[B

    new-array v0, v2, [B

    fill-array-data v0, :array_4

    sput-object v0, Lcom/twitter/library/network/n;->h:[B

    sget-object v0, Lcom/twitter/library/network/n;->h:[B

    invoke-static {v3, v0}, Lcom/twitter/library/network/n;->a(B[B)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/network/n;->i:Ljava/lang/String;

    sget-object v0, Lcom/twitter/library/network/n;->g:[B

    invoke-static {v3, v0}, Lcom/twitter/library/network/n;->a(B[B)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/network/n;->j:Ljava/lang/String;

    const/16 v0, 0x13

    new-array v0, v0, [B

    fill-array-data v0, :array_5

    sput-object v0, Lcom/twitter/library/network/n;->k:[B

    const/16 v0, 0x2a

    new-array v0, v0, [B

    fill-array-data v0, :array_6

    sput-object v0, Lcom/twitter/library/network/n;->l:[B

    const/16 v0, 0x32

    new-array v0, v0, [B

    fill-array-data v0, :array_7

    sput-object v0, Lcom/twitter/library/network/n;->m:[B

    new-array v0, v4, [B

    fill-array-data v0, :array_8

    sput-object v0, Lcom/twitter/library/network/n;->n:[B

    sget-object v0, Lcom/twitter/library/network/n;->k:[B

    invoke-static {v1, v0}, Lcom/twitter/library/network/n;->a(B[B)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/network/n;->o:Ljava/lang/String;

    sget-object v0, Lcom/twitter/library/network/n;->l:[B

    invoke-static {v1, v0}, Lcom/twitter/library/network/n;->a(B[B)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/network/n;->p:Ljava/lang/String;

    sget-object v0, Lcom/twitter/library/network/n;->m:[B

    invoke-static {v1, v0}, Lcom/twitter/library/network/n;->a(B[B)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/network/n;->q:Ljava/lang/String;

    sget-object v0, Lcom/twitter/library/network/n;->n:[B

    invoke-static {v1, v0}, Lcom/twitter/library/network/n;->a(B[B)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/network/n;->r:Ljava/lang/String;

    new-instance v0, Lcom/twitter/library/network/OAuthToken;

    sget-object v1, Lcom/twitter/library/network/n;->q:Ljava/lang/String;

    sget-object v2, Lcom/twitter/library/network/n;->r:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/network/OAuthToken;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/twitter/library/network/n;->s:Lcom/twitter/library/network/OAuthToken;

    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    sput-object v0, Lcom/twitter/library/network/n;->t:Ljava/security/SecureRandom;

    return-void

    nop

    :array_0
    .array-data 1
        -0x1dt
        -0x58t
        -0x40t
        -0x5ft
        -0x3dt
        -0x59t
        -0x2ct
        -0x44t
        -0x58t
        -0x62t
        -0x20t
        -0x3ft
        -0x1et
        -0x60t
        -0x64t
        -0x3ft
        -0x62t
        -0x50t
        -0x1ft
        -0x61t
    .end array-data

    :array_1
    .array-data 1
        -0x2ct
        -0x4dt
        -0x5dt
        -0x1ft
        -0x23t
        -0x2ft
        -0x30t
        -0x4ct
        -0x4ct
        -0x5dt
        -0x4et
        -0x30t
        -0x20t
        -0x3dt
        -0x56t
        -0x23t
        -0x38t
        -0x51t
        -0x21t
        -0x1bt
        -0x5dt
        -0x57t
        -0x51t
        -0x3dt
        -0x5et
        -0x41t
        -0x2ft
        -0x31t
        -0x61t
        -0x42t
        -0x42t
        -0x35t
        -0x3dt
        -0x54t
        -0x43t
        -0x60t
        -0x3at
        -0x40t
        -0x5et
        -0x21t
        -0x5bt
        -0x63t
        -0x5dt
    .end array-data

    :array_2
    .array-data 1
        -0x2bt
        -0x2bt
        -0x2bt
        -0x2bt
        -0x2bt
        -0x2bt
        -0x2bt
        -0x2bt
        -0x2bt
        -0x2bt
        -0x2bt
        -0x2bt
        -0x2bt
        -0x2bt
        -0x2bt
        -0x2bt
        -0x2bt
        -0x2bt
        -0x2bt
        -0x2bt
        -0x2bt
        -0x30t
        -0x42t
        -0x64t
        -0x2bt
        -0x61t
        -0x2bt
        -0x2bt
        -0x2bt
        -0x2bt
        -0x2bt
        -0x2bt
        -0x5dt
        -0x4et
        -0x21t
        -0x3bt
        -0x2ft
        -0x56t
        -0x53t
        -0x2bt
        -0xft
        -0x1ct
        -0x30t
        -0x57t
        -0x64t
        -0x4et
        -0x40t
        -0x5bt
        -0x61t
        -0x33t
        -0x5dt
        -0x2ct
        -0x41t
        -0x2bt
        -0x20t
        -0x55t
        -0x57t
        -0x57t
        -0x62t
        -0x3ft
        -0x59t
        -0xft
        -0x1dt
        -0x2et
        -0x3ct
        -0x64t
        -0x61t
        -0x42t
        -0x20t
        -0x40t
        -0x40t
        -0x1ft
        -0x5dt
        -0x1dt
        -0x2ct
        -0x4dt
        -0x42t
        -0x61t
        -0x57t
        -0x5at
        -0x30t
        -0x38t
        -0x5ct
        -0x56t
        -0x42t
        -0x33t
        -0x63t
        -0x5ct
        -0x4ft
        -0x36t
        -0x20t
        -0x2ct
        -0x4dt
        -0x54t
        -0x3et
        -0x33t
        -0x31t
        -0x43t
        -0x3dt
        -0x5ct
        -0x1dt
        -0x2bt
        -0x41t
        -0x56t
        -0x2dt
        -0x3ft
        -0x4dt
    .end array-data

    :array_3
    .array-data 1
        -0x6et
        -0x5dt
        -0x67t
        -0x6ct
        -0x5ct
        -0x2at
        -0x42t
        -0x3at
        -0x72t
        -0x4dt
        -0x4at
        -0x67t
        -0x64t
        -0x63t
        -0x29t
        -0x44t
        -0x49t
        -0x5ft
        -0x53t
        -0x5ft
        -0x5ft
        -0x5bt
        -0x32t
        -0x6et
        -0x6ct
        -0x53t
        -0x2ft
        -0x53t
        -0x6at
        -0x4ft
        -0x68t
        -0x51t
        -0x5ct
        -0x5dt
        -0x3dt
        -0x2ct
        -0x68t
        -0x71t
        -0x42t
        -0x4ft
        -0x68t
    .end array-data

    nop

    :array_4
    .array-data 1
        -0x66t
        -0x32t
        -0x4at
        -0x6ct
        -0x6bt
        -0x6bt
        -0x66t
        -0x43t
        -0x68t
        -0x3at
        -0x47t
        -0x40t
        -0x4bt
        -0x48t
        -0x3at
        -0x62t
        -0x47t
        -0x44t
        -0x5at
        -0x3ct
        -0x31t
        -0x60t
    .end array-data

    nop

    :array_5
    .array-data 1
        -0x4at
        -0x6ft
        -0x51t
        -0x44t
        -0x60t
        -0x70t
        -0x3ft
        -0x52t
        -0x68t
        -0x45t
        -0x69t
        -0x6bt
        -0x4ft
        -0x52t
        -0x3dt
        -0x46t
        -0x3et
        -0x4et
        -0x6ft
    .end array-data

    :array_6
    .array-data 1
        -0x42t
        -0x63t
        -0x30t
        -0x28t
        -0x51t
        -0x4et
        -0x3ft
        -0x69t
        -0x5bt
        -0x2ft
        -0x41t
        -0x72t
        -0x29t
        -0x41t
        -0x3ct
        -0x3dt
        -0x62t
        -0x3bt
        -0x41t
        -0x2et
        -0x70t
        -0x2bt
        -0x3dt
        -0x70t
        -0x45t
        -0x4bt
        -0x3at
        -0x66t
        -0x3ft
        -0x62t
        -0x72t
        -0x3at
        -0x39t
        -0x40t
        -0x2et
        -0x69t
        -0x40t
        -0x5bt
        -0x4ft
        -0x42t
        -0x64t
        -0x67t
    .end array-data

    nop

    :array_7
    .array-data 1
        -0x29t
        -0x2ft
        -0x31t
        -0x2et
        -0x2dt
        -0x2ct
        -0x2dt
        -0x31t
        -0x30t
        -0x25t
        -0x62t
        -0x6bt
        -0x4ft
        -0x6ft
        -0x60t
        -0x3at
        -0x2et
        -0x6bt
        -0x48t
        -0x65t
        -0x5ft
        -0x70t
        -0x2at
        -0x2dt
        -0x40t
        -0x40t
        -0x46t
        -0x42t
        -0x4ft
        -0x47t
        -0x3at
        -0x4bt
        -0x5dt
        -0x69t
        -0x61t
        -0x5dt
        -0x3bt
        -0x64t
        -0x2ct
        -0x4et
        -0x30t
        -0x31t
        -0x2ft
        -0x2ft
        -0x66t
        -0x67t
        -0x49t
        -0x71t
        -0x30t
        -0x44t
    .end array-data

    nop

    :array_8
    .array-data 1
        -0x50t
        -0x5bt
        -0x41t
        -0x68t
        -0x29t
        -0x46t
        -0x3ct
        -0x44t
        -0x4ft
        -0x4dt
        -0x6at
        -0x67t
        -0x28t
        -0x64t
        -0x63t
        -0x66t
        -0x44t
        -0x43t
        -0x44t
        -0x5et
        -0x47t
        -0x4bt
        -0x4ft
        -0x2et
        -0x39t
        -0x46t
        -0x3ft
        -0x41t
        -0x69t
        -0x65t
        -0x31t
        -0x60t
        -0x3ct
        -0x46t
        -0x4bt
        -0x61t
        -0x5ct
        -0x2ct
        -0x61t
        -0x52t
        -0x61t
        -0x6dt
        -0x6bt
    .end array-data
.end method

.method public constructor <init>(I)V
    .locals 6

    const/4 v1, 0x0

    sget-object v4, Lcom/twitter/library/network/n;->c:Ljava/lang/String;

    sget-object v5, Lcom/twitter/library/network/n;->d:Ljava/lang/String;

    move-object v0, p0

    move v2, p1

    move-object v3, v1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/network/OAuthToken;)V
    .locals 6

    const/4 v2, 0x1

    const/4 v3, 0x0

    sget-object v4, Lcom/twitter/library/network/n;->c:Ljava/lang/String;

    sget-object v5, Lcom/twitter/library/network/n;->d:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Lcom/twitter/library/network/OAuthToken;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/library/network/a;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/network/n;->u:Lcom/twitter/library/network/OAuthToken;

    iput p2, p0, Lcom/twitter/library/network/n;->v:I

    iput-object p3, p0, Lcom/twitter/library/network/n;->w:Ljava/lang/String;

    iput-object p4, p0, Lcom/twitter/library/network/n;->x:Ljava/lang/String;

    iput-object p5, p0, Lcom/twitter/library/network/n;->y:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/network/OAuthToken;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    const/4 v2, 0x1

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;Ljava/net/URI;Lorg/apache/http/HttpEntity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    invoke-virtual {p1}, Ljava/net/URI;->getRawQuery()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v0, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    if-eqz p2, :cond_2

    invoke-interface {p2}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string/jumbo v2, "application/x-www-form-urlencoded"

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :try_start_0
    invoke-static {p2}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string/jumbo v2, "&"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/library/network/n;->a(Ljava/lang/String;Z)Ljava/util/TreeMap;

    move-result-object v1

    const-string/jumbo v0, "oauth_consumer_key"

    invoke-virtual {v1, v0, p3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v0, "oauth_nonce"

    invoke-virtual {v1, v0, p5}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v0, "oauth_signature_method"

    const-string/jumbo v2, "HMAC-SHA1"

    invoke-virtual {v1, v0, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v0, "oauth_timestamp"

    invoke-virtual {v1, v0, p6}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v0, "oauth_version"

    const-string/jumbo v2, "1.0"

    invoke-virtual {v1, v0, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p4, :cond_3

    const-string/jumbo v0, "oauth_token"

    invoke-virtual {v1, v0, p4}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/twitter/library/network/n;->a(Ljava/net/URI;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "&"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Lcom/twitter/library/network/n;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "&"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/util/TreeMap;->size()I

    move-result v4

    const/4 v0, 0x0

    invoke-virtual {v1}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v0

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/library/network/n;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/network/n;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v6, "%3D"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/library/network/n;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/network/n;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v2, 0x1

    if-ge v0, v4, :cond_4

    const-string/jumbo v1, "%26"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    move v2, v0

    goto :goto_1

    :cond_5
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    goto/16 :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    if-nez p2, :cond_0

    :try_start_0
    const-string/jumbo p2, ""

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "UTF8"

    invoke-static {p1, v1}, Lcom/twitter/library/util/Util;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "UTF8"

    invoke-static {p2, v1}, Lcom/twitter/library/util/Util;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "UTF8"

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    new-instance v1, Ljavax/crypto/spec/SecretKeySpec;

    const-string/jumbo v2, "HmacSHA1"

    invoke-direct {v1, v0, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    const-string/jumbo v0, "HmacSHA1"

    invoke-static {v0}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V

    const-string/jumbo v1, "UTF8"

    invoke-virtual {p0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljavax/crypto/Mac;->doFinal([B)[B

    move-result-object v0

    new-instance v1, Ljava/lang/String;

    invoke-static {v0}, Llk;->a([B)[B

    move-result-object v0

    const-string/jumbo v2, "UTF8"

    invoke-direct {v1, v0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    const-string/jumbo v0, "UTF8"

    invoke-static {v1, v0}, Lcom/twitter/library/util/Util;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v0, ""

    goto :goto_0

    :catch_1
    move-exception v0

    const-string/jumbo v0, ""

    goto :goto_0

    :catch_2
    move-exception v0

    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    if-nez p4, :cond_0

    const-string/jumbo v0, "OAuth realm=\"%s\", oauth_version=\"%s\", oauth_nonce=\"%s\", oauth_timestamp=\"%s\", oauth_signature=\"%s\", oauth_consumer_key=\"%s\", oauth_signature_method=\"%s\""

    const-string/jumbo v0, "OAuth realm=\"%s\", oauth_version=\"%s\", oauth_nonce=\"%s\", oauth_timestamp=\"%s\", oauth_signature=\"%s\", oauth_consumer_key=\"%s\", oauth_signature_method=\"%s\""

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const-string/jumbo v2, "http://api.twitter.com/"

    aput-object v2, v1, v3

    const-string/jumbo v2, "1.0"

    aput-object v2, v1, v4

    aput-object p2, v1, v5

    aput-object p3, v1, v6

    aput-object p0, v1, v7

    const/4 v2, 0x5

    aput-object p1, v1, v2

    const/4 v2, 0x6

    const-string/jumbo v3, "HMAC-SHA1"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "OAuth realm=\"%s\", oauth_version=\"%s\", oauth_token=\"%s\", oauth_nonce=\"%s\", oauth_timestamp=\"%s\", oauth_signature=\"%s\", oauth_consumer_key=\"%s\", oauth_signature_method=\"%s\""

    const-string/jumbo v0, "OAuth realm=\"%s\", oauth_version=\"%s\", oauth_token=\"%s\", oauth_nonce=\"%s\", oauth_timestamp=\"%s\", oauth_signature=\"%s\", oauth_consumer_key=\"%s\", oauth_signature_method=\"%s\""

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Object;

    const-string/jumbo v2, "http://api.twitter.com/"

    aput-object v2, v1, v3

    const-string/jumbo v2, "1.0"

    aput-object v2, v1, v4

    aput-object p4, v1, v5

    aput-object p2, v1, v6

    aput-object p3, v1, v7

    const/4 v2, 0x5

    aput-object p0, v1, v2

    const/4 v2, 0x6

    aput-object p1, v1, v2

    const/4 v2, 0x7

    const-string/jumbo v3, "HMAC-SHA1"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/net/URI;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Z)Ljava/util/TreeMap;
    .locals 9

    const/4 v8, 0x1

    const/4 v1, 0x0

    new-instance v2, Ljava/util/TreeMap;

    invoke-direct {v2}, Ljava/util/TreeMap;-><init>()V

    if-eqz p0, :cond_4

    const-string/jumbo v0, "&"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_4

    aget-object v5, v3, v0

    const-string/jumbo v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    array-length v6, v5

    const/4 v7, 0x2

    if-ne v6, v7, :cond_2

    if-eqz p1, :cond_1

    aget-object v6, v5, v1

    const-string/jumbo v7, "UTF8"

    invoke-static {v6, v7}, Lcom/twitter/library/util/Util;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aget-object v5, v5, v8

    const-string/jumbo v7, "UTF8"

    invoke-static {v5, v7}, Lcom/twitter/library/util/Util;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v6, v5}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    aget-object v6, v5, v1

    aget-object v5, v5, v8

    invoke-virtual {v2, v6, v5}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    aget-object v6, v5, v1

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    if-eqz p1, :cond_3

    aget-object v5, v5, v1

    const-string/jumbo v6, "UTF8"

    invoke-static {v5, v6}, Lcom/twitter/library/util/Util;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, ""

    invoke-virtual {v2, v5, v6}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    aget-object v5, v5, v1

    const-string/jumbo v6, ""

    invoke-virtual {v2, v5, v6}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_4
    return-object v2
.end method


# virtual methods
.method public synthetic a()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/library/network/n;->b()Lcom/twitter/library/network/OAuthToken;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;Ljava/net/URI;Lorg/apache/http/HttpEntity;J)Ljava/lang/String;
    .locals 8

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/library/network/n;->u:Lcom/twitter/library/network/OAuthToken;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/network/n;->u:Lcom/twitter/library/network/OAuthToken;

    iget-object v4, v0, Lcom/twitter/library/network/OAuthToken;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/library/network/n;->u:Lcom/twitter/library/network/OAuthToken;

    iget-object v0, v0, Lcom/twitter/library/network/OAuthToken;->a:Ljava/lang/String;

    move-object v7, v0

    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/network/n;->t:Ljava/security/SecureRandom;

    invoke-virtual {v1}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Math;->abs(J)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    add-long/2addr v0, p4

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    iget-object v3, p0, Lcom/twitter/library/network/n;->x:Ljava/lang/String;

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    invoke-static/range {v0 .. v6}, Lcom/twitter/library/network/n;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;Ljava/net/URI;Lorg/apache/http/HttpEntity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/network/n;->y:Ljava/lang/String;

    invoke-static {v0, v1, v7}, Lcom/twitter/library/network/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/network/n;->x:Ljava/lang/String;

    invoke-static {v0, v1, v5, v6, v4}, Lcom/twitter/library/network/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v7, v0

    move-object v4, v0

    goto :goto_0
.end method

.method public a(Lcom/twitter/internal/network/HttpOperation;J)V
    .locals 7

    iget v0, p0, Lcom/twitter/library/network/n;->v:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->g()Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    move-result-object v1

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->h()Ljava/net/URI;

    move-result-object v2

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->f()Lorg/apache/http/HttpEntity;

    move-result-object v3

    move-object v0, p0

    move-wide v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/network/n;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;Ljava/net/URI;Lorg/apache/http/HttpEntity;J)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Authorization"

    invoke-virtual {p1, v1, v0}, Lcom/twitter/internal/network/HttpOperation;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/internal/network/HttpOperation;

    goto :goto_0

    :pswitch_1
    iget-object v6, p0, Lcom/twitter/library/network/n;->w:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->g()Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    move-result-object v1

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->h()Ljava/net/URI;

    move-result-object v2

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->f()Lorg/apache/http/HttpEntity;

    move-result-object v3

    move-object v0, p0

    move-wide v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/network/n;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;Ljava/net/URI;Lorg/apache/http/HttpEntity;J)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "X-Auth-Service-Provider"

    invoke-virtual {p1, v1, v6}, Lcom/twitter/internal/network/HttpOperation;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/internal/network/HttpOperation;

    const-string/jumbo v1, "X-Verify-Credentials-Authorization"

    invoke-virtual {p1, v1, v0}, Lcom/twitter/internal/network/HttpOperation;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/internal/network/HttpOperation;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b()Lcom/twitter/library/network/OAuthToken;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/network/n;->u:Lcom/twitter/library/network/OAuthToken;

    return-object v0
.end method
