.class public Lcom/twitter/library/network/s;
.super Lcom/twitter/internal/network/c;
.source "Twttr"


# instance fields
.field protected final b:Ljava/security/KeyStore;


# direct methods
.method public constructor <init>(Ljava/security/KeyStore;Lcom/twitter/internal/network/j;)V
    .locals 0

    invoke-direct {p0, p2}, Lcom/twitter/internal/network/c;-><init>(Lcom/twitter/internal/network/j;)V

    iput-object p1, p0, Lcom/twitter/library/network/s;->b:Ljava/security/KeyStore;

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/internal/network/j;Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)Lorg/apache/http/client/HttpClient;
    .locals 2

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/internal/network/c;->a(Lcom/twitter/internal/network/j;Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)Lorg/apache/http/client/HttpClient;

    move-result-object v0

    check-cast v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    const-class v1, Lorg/apache/http/client/protocol/RequestAddCookies;

    invoke-virtual {v0, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;->removeRequestInterceptorByClass(Ljava/lang/Class;)V

    const-class v1, Lorg/apache/http/client/protocol/ResponseProcessCookies;

    invoke-virtual {v0, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;->removeResponseInterceptorByClass(Ljava/lang/Class;)V

    new-instance v1, Lcom/twitter/library/network/z;

    invoke-direct {v1}, Lcom/twitter/library/network/z;-><init>()V

    invoke-virtual {v0, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;->setKeepAliveStrategy(Lorg/apache/http/conn/ConnectionKeepAliveStrategy;)V

    return-object v0
.end method

.method protected b()Lorg/apache/http/conn/scheme/SchemeRegistry;
    .locals 7

    iget-object v0, p0, Lcom/twitter/library/network/s;->b:Ljava/security/KeyStore;

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/twitter/internal/network/c;->b()Lorg/apache/http/conn/scheme/SchemeRegistry;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    new-instance v6, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v6}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    new-instance v1, Lhu;

    iget-object v0, p0, Lcom/twitter/library/network/s;->b:Ljava/security/KeyStore;

    invoke-direct {v1, v0}, Lhu;-><init>(Ljava/security/KeyStore;)V

    new-instance v0, Lhq;

    sget-object v2, Lku;->b:[Ljava/lang/String;

    invoke-static {}, Lcom/twitter/library/client/App;->i()J

    move-result-wide v3

    sget-object v5, Lku;->c:[Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lhq;-><init>(Lhu;[Ljava/lang/String;J[Ljava/lang/String;)V

    new-instance v1, Lorg/apache/http/conn/scheme/Scheme;

    const-string/jumbo v2, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v3

    const/16 v4, 0x50

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v6, v1}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    new-instance v1, Lorg/apache/http/conn/scheme/Scheme;

    const-string/jumbo v2, "https"

    const/16 v3, 0x1bb

    invoke-direct {v1, v2, v0, v3}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v6, v1}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;
    :try_end_0
    .catch Ljava/security/KeyManagementException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/UnrecoverableKeyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v6

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method
