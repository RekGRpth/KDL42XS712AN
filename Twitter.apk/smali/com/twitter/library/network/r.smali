.class final Lcom/twitter/library/network/r;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/twitter/library/network/OneFactorLoginResponse;
    .locals 1

    new-instance v0, Lcom/twitter/library/network/OneFactorLoginResponse;

    invoke-direct {v0, p1}, Lcom/twitter/library/network/OneFactorLoginResponse;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public a(I)[Lcom/twitter/library/network/OneFactorLoginResponse;
    .locals 1

    new-array v0, p1, [Lcom/twitter/library/network/OneFactorLoginResponse;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/library/network/r;->a(Landroid/os/Parcel;)Lcom/twitter/library/network/OneFactorLoginResponse;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/library/network/r;->a(I)[Lcom/twitter/library/network/OneFactorLoginResponse;

    move-result-object v0

    return-object v0
.end method
