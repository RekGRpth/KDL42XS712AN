.class public abstract Lcom/twitter/library/network/t;
.super Lcom/twitter/internal/network/h;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Lcom/twitter/internal/network/g;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/internal/network/h;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/t;->a:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method protected abstract a(Landroid/content/Context;)Lcom/twitter/internal/network/g;
.end method

.method public declared-synchronized a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;Ljava/net/URI;)Lcom/twitter/internal/network/g;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/network/t;->b:Lcom/twitter/internal/network/g;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/network/t;->a:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/twitter/library/network/t;->a(Landroid/content/Context;)Lcom/twitter/internal/network/g;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/t;->b:Lcom/twitter/internal/network/g;

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/network/t;->b:Lcom/twitter/internal/network/g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/network/t;->b:Lcom/twitter/internal/network/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/network/t;->b:Lcom/twitter/internal/network/g;

    invoke-virtual {v0}, Lcom/twitter/internal/network/g;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/network/t;->b:Lcom/twitter/internal/network/g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
