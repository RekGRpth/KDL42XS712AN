.class public final Lcom/twitter/library/network/d;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:Z

.field private static b:Ljava/util/HashMap;

.field private static c:Lcom/twitter/internal/network/h;


# instance fields
.field private final d:Landroid/content/Context;

.field private e:Ljava/net/URI;

.field private f:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

.field private g:Z

.field private h:Lorg/apache/http/HttpEntity;

.field private i:Lcom/twitter/internal/network/i;

.field private j:Lcom/twitter/library/network/a;

.field private k:Lcom/twitter/internal/network/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/twitter/library/client/App;->m()Z

    move-result v0

    sput-boolean v0, Lcom/twitter/library/network/d;->a:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/twitter/library/network/d;->b:Ljava/util/HashMap;

    sget-boolean v0, Lcom/twitter/library/network/d;->a:Z

    invoke-static {v0}, Lcom/twitter/internal/network/HttpOperation;->a(Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/CharSequence;)V
    .locals 1

    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->b(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/net/URI;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/net/URI;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->a:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    iput-object v0, p0, Lcom/twitter/library/network/d;->f:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/network/d;->g:Z

    iput-object v1, p0, Lcom/twitter/library/network/d;->h:Lorg/apache/http/HttpEntity;

    iput-object v1, p0, Lcom/twitter/library/network/d;->i:Lcom/twitter/internal/network/i;

    iput-object v1, p0, Lcom/twitter/library/network/d;->j:Lcom/twitter/library/network/a;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/d;->d:Landroid/content/Context;

    iput-object p2, p0, Lcom/twitter/library/network/d;->e:Ljava/net/URI;

    return-void
.end method

.method private static a(Ljava/net/URI;)Ljava/net/URI;
    .locals 3

    invoke-static {}, Lcom/twitter/library/client/App;->p()Lcom/twitter/library/client/AppFlavor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/AppFlavor;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object p0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "application_id"

    invoke-static {v1, v2, v0}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->b(Ljava/lang/String;)Ljava/net/URI;

    move-result-object p0

    goto :goto_0
.end method

.method public static declared-synchronized a(Lcom/twitter/internal/network/h;)V
    .locals 4

    const-class v1, Lcom/twitter/library/network/d;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/twitter/library/network/d;->e()V

    sput-object p0, Lcom/twitter/library/network/d;->c:Lcom/twitter/internal/network/h;

    sget-boolean v0, Lcom/twitter/library/network/d;->a:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "HttpOperation"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Default HttpOperationClientFactory set to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Ljava/util/HashMap;)V
    .locals 0

    sput-object p0, Lcom/twitter/library/network/d;->b:Ljava/util/HashMap;

    return-void
.end method

.method private b()Lcom/twitter/internal/network/HttpOperation;
    .locals 5

    invoke-static {}, Lcom/twitter/library/network/d;->c()Lcom/twitter/internal/network/h;

    move-result-object v1

    invoke-direct {p0}, Lcom/twitter/library/network/d;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/network/d;->e:Ljava/net/URI;

    invoke-static {v0}, Lcom/twitter/library/network/d;->a(Ljava/net/URI;)Ljava/net/URI;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/d;->e:Ljava/net/URI;

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/network/d;->e:Ljava/net/URI;

    invoke-direct {p0, v0}, Lcom/twitter/library/network/d;->b(Ljava/net/URI;)Ljava/net/URI;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/d;->e:Ljava/net/URI;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/twitter/library/network/d;->f:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    iget-object v2, p0, Lcom/twitter/library/network/d;->e:Ljava/net/URI;

    iget-object v3, p0, Lcom/twitter/library/network/d;->i:Lcom/twitter/internal/network/i;

    invoke-virtual {v1, v0, v2, v3}, Lcom/twitter/internal/network/h;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;Ljava/net/URI;Lcom/twitter/internal/network/i;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    sget-boolean v2, Lcom/twitter/library/network/d;->a:Z

    if-eqz v2, :cond_1

    const-string/jumbo v2, "HttpOperation"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/library/network/d;->e:Ljava/net/URI;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "] Failed to rewrite host"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    iget-object v2, p0, Lcom/twitter/library/network/d;->f:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    iget-object v3, p0, Lcom/twitter/library/network/d;->e:Ljava/net/URI;

    iget-object v4, p0, Lcom/twitter/library/network/d;->i:Lcom/twitter/internal/network/i;

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/internal/network/h;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;Ljava/net/URI;Lcom/twitter/internal/network/i;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/internal/network/HttpOperation;->a(Ljava/lang/Exception;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Ljava/net/URI;)Ljava/net/URI;
    .locals 2

    sget-object v0, Lcom/twitter/library/network/d;->b:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/library/network/d;->b:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-static {p1, v0}, Lcom/twitter/library/util/Util;->a(Ljava/net/URI;Ljava/lang/String;)Ljava/net/URI;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method private static declared-synchronized c()Lcom/twitter/internal/network/h;
    .locals 3

    const-class v1, Lcom/twitter/library/network/d;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/network/d;->c:Lcom/twitter/internal/network/h;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "A default HttpOperationClientFactory must be set before building a HttpOperation"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    sget-object v0, Lcom/twitter/library/network/d;->c:Lcom/twitter/internal/network/h;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method

.method private d()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/network/d;->j:Lcom/twitter/library/network/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static declared-synchronized e()V
    .locals 3

    const-class v1, Lcom/twitter/library/network/d;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/network/d;->c:Lcom/twitter/internal/network/h;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/twitter/library/network/e;

    sget-object v2, Lcom/twitter/library/network/d;->c:Lcom/twitter/internal/network/h;

    invoke-direct {v0, v2}, Lcom/twitter/library/network/e;-><init>(Lcom/twitter/internal/network/h;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v2}, Lcom/twitter/library/network/e;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const/4 v0, 0x0

    sput-object v0, Lcom/twitter/library/network/d;->c:Lcom/twitter/internal/network/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a()Lcom/twitter/internal/network/HttpOperation;
    .locals 3

    invoke-direct {p0}, Lcom/twitter/library/network/d;->b()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/network/d;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/library/network/aa;->a(Landroid/content/Context;)Lcom/twitter/library/network/aa;

    move-result-object v1

    iget-boolean v2, p0, Lcom/twitter/library/network/d;->g:Z

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/aa;->c(Lcom/twitter/internal/network/HttpOperation;)V

    :goto_0
    iget-object v1, p0, Lcom/twitter/library/network/d;->h:Lorg/apache/http/HttpEntity;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/library/network/d;->f:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->a()Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "The RequestMethod "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/network/d;->f:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " does not allow a request entity."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v1, v0}, Lcom/twitter/library/network/aa;->b(Lcom/twitter/internal/network/HttpOperation;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/twitter/library/network/d;->h:Lorg/apache/http/HttpEntity;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/network/HttpOperation;->a(Lorg/apache/http/HttpEntity;)Lcom/twitter/internal/network/HttpOperation;

    :cond_2
    invoke-direct {p0}, Lcom/twitter/library/network/d;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Lcom/twitter/library/network/p;

    iget-object v2, p0, Lcom/twitter/library/network/d;->j:Lcom/twitter/library/network/a;

    invoke-direct {v1, v2}, Lcom/twitter/library/network/p;-><init>(Lcom/twitter/library/network/a;)V

    invoke-virtual {v0, v1}, Lcom/twitter/internal/network/HttpOperation;->a(Lcom/twitter/internal/network/f;)Lcom/twitter/internal/network/HttpOperation;

    :cond_3
    iget-object v1, p0, Lcom/twitter/library/network/d;->k:Lcom/twitter/internal/network/f;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/twitter/library/network/d;->k:Lcom/twitter/internal/network/f;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/network/HttpOperation;->a(Lcom/twitter/internal/network/f;)Lcom/twitter/internal/network/HttpOperation;

    :goto_1
    return-object v0

    :cond_4
    new-instance v1, Lcom/twitter/library/network/c;

    iget-object v2, p0, Lcom/twitter/library/network/d;->d:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/twitter/library/network/c;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/twitter/internal/network/HttpOperation;->a(Lcom/twitter/internal/network/f;)Lcom/twitter/internal/network/HttpOperation;

    goto :goto_1
.end method

.method public a(J)Lcom/twitter/library/network/d;
    .locals 2

    new-instance v0, Lcom/twitter/library/network/c;

    iget-object v1, p0, Lcom/twitter/library/network/d;->d:Landroid/content/Context;

    invoke-direct {v0, v1, p1, p2}, Lcom/twitter/library/network/c;-><init>(Landroid/content/Context;J)V

    iput-object v0, p0, Lcom/twitter/library/network/d;->k:Lcom/twitter/internal/network/f;

    return-object p0
.end method

.method public a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/network/d;->f:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    return-object p0
.end method

.method public a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/network/d;->i:Lcom/twitter/internal/network/i;

    return-object p0
.end method

.method public a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/network/d;->j:Lcom/twitter/library/network/a;

    return-object p0
.end method

.method public a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;
    .locals 3

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    :try_start_0
    invoke-static {p1}, Lcom/twitter/library/util/Util;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/http/entity/StringEntity;

    const-string/jumbo v2, "UTF-8"

    invoke-direct {v1, v0, v2}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/twitter/library/network/d;->h:Lorg/apache/http/HttpEntity;

    iget-object v0, p0, Lcom/twitter/library/network/d;->h:Lorg/apache/http/HttpEntity;

    check-cast v0, Lorg/apache/http/entity/StringEntity;

    const-string/jumbo v1, "application/x-www-form-urlencoded"

    invoke-virtual {v0, v1}, Lorg/apache/http/entity/StringEntity;->setContentType(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-object p0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public a(Lorg/apache/http/HttpEntity;)Lcom/twitter/library/network/d;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/network/d;->h:Lorg/apache/http/HttpEntity;

    return-object p0
.end method

.method public a(Z)Lcom/twitter/library/network/d;
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/network/d;->g:Z

    return-object p0
.end method

.method public b(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/network/a;->a()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iput-object p1, p0, Lcom/twitter/library/network/d;->j:Lcom/twitter/library/network/a;

    :cond_0
    return-object p0
.end method
