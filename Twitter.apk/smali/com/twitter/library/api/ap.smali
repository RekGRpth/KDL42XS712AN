.class public Lcom/twitter/library/api/ap;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:Lcom/fasterxml/jackson/core/JsonFactory;

.field static final b:Ljava/util/HashMap;

.field private static final c:Ljava/util/HashMap;

.field private static final d:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x3

    const/16 v7, 0xb

    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    new-instance v0, Lcom/fasterxml/jackson/core/JsonFactory;

    invoke-direct {v0}, Lcom/fasterxml/jackson/core/JsonFactory;-><init>()V

    sput-object v0, Lcom/twitter/library/api/ap;->a:Lcom/fasterxml/jackson/core/JsonFactory;

    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x13

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/twitter/library/api/ap;->b:Ljava/util/HashMap;

    sget-object v0, Lcom/twitter/library/api/ap;->b:Ljava/util/HashMap;

    const-string/jumbo v1, "magicrecs_retweet"

    new-instance v2, Lcom/twitter/library/api/ar;

    const/16 v3, 0x14

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/twitter/library/api/ar;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->b:Ljava/util/HashMap;

    const-string/jumbo v1, "magicrecs_follow"

    new-instance v2, Lcom/twitter/library/api/ar;

    const/16 v3, 0x13

    invoke-direct {v2, v3, v4, v4, v6}, Lcom/twitter/library/api/ar;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->b:Ljava/util/HashMap;

    const-string/jumbo v1, "magicrecs_favorite"

    new-instance v2, Lcom/twitter/library/api/ar;

    const/16 v3, 0x12

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/twitter/library/api/ar;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->b:Ljava/util/HashMap;

    const-string/jumbo v1, "favorite"

    new-instance v2, Lcom/twitter/library/api/ar;

    invoke-direct {v2, v4, v4, v5, v6}, Lcom/twitter/library/api/ar;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->b:Ljava/util/HashMap;

    const-string/jumbo v1, "favorited_retweet"

    new-instance v2, Lcom/twitter/library/api/ar;

    const/16 v3, 0xa

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/twitter/library/api/ar;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->b:Ljava/util/HashMap;

    const-string/jumbo v1, "favorited_mention"

    new-instance v2, Lcom/twitter/library/api/ar;

    const/16 v3, 0xc

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/twitter/library/api/ar;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->b:Ljava/util/HashMap;

    const-string/jumbo v1, "retweeted_retweet"

    new-instance v2, Lcom/twitter/library/api/ar;

    const/16 v3, 0x9

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/twitter/library/api/ar;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->b:Ljava/util/HashMap;

    const-string/jumbo v1, "retweeted_mention"

    new-instance v2, Lcom/twitter/library/api/ar;

    invoke-direct {v2, v7, v4, v5, v6}, Lcom/twitter/library/api/ar;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->b:Ljava/util/HashMap;

    const-string/jumbo v1, "mention"

    new-instance v2, Lcom/twitter/library/api/ar;

    invoke-direct {v2, v5, v4, v4, v5}, Lcom/twitter/library/api/ar;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->b:Ljava/util/HashMap;

    const-string/jumbo v1, "reply"

    new-instance v2, Lcom/twitter/library/api/ar;

    invoke-direct {v2, v8, v4, v5, v5}, Lcom/twitter/library/api/ar;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->b:Ljava/util/HashMap;

    const-string/jumbo v1, "retweet"

    new-instance v2, Lcom/twitter/library/api/ar;

    const/4 v3, 0x4

    invoke-direct {v2, v3, v4, v5, v5}, Lcom/twitter/library/api/ar;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->b:Ljava/util/HashMap;

    const-string/jumbo v1, "follow"

    new-instance v2, Lcom/twitter/library/api/ar;

    const/4 v3, 0x5

    invoke-direct {v2, v3, v4, v4, v6}, Lcom/twitter/library/api/ar;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->b:Ljava/util/HashMap;

    const-string/jumbo v1, "joined_twitter"

    new-instance v2, Lcom/twitter/library/api/ar;

    const/16 v3, 0xd

    invoke-direct {v2, v3, v4, v4, v6}, Lcom/twitter/library/api/ar;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->b:Ljava/util/HashMap;

    const-string/jumbo v1, "quote"

    new-instance v2, Lcom/twitter/library/api/ar;

    const/16 v3, 0xe

    invoke-direct {v2, v3, v4, v5, v5}, Lcom/twitter/library/api/ar;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->b:Ljava/util/HashMap;

    const-string/jumbo v1, "list_member_added"

    new-instance v2, Lcom/twitter/library/api/ar;

    const/4 v3, 0x6

    invoke-direct {v2, v3, v4, v4, v8}, Lcom/twitter/library/api/ar;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->b:Ljava/util/HashMap;

    const-string/jumbo v1, "list_created"

    new-instance v2, Lcom/twitter/library/api/ar;

    const/4 v3, 0x7

    invoke-direct {v2, v3, v4, v8, v6}, Lcom/twitter/library/api/ar;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->b:Ljava/util/HashMap;

    const-string/jumbo v1, "media_tagged"

    new-instance v2, Lcom/twitter/library/api/ar;

    const/16 v3, 0xf

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/twitter/library/api/ar;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->b:Ljava/util/HashMap;

    const-string/jumbo v1, "favorited_media_tagged"

    new-instance v2, Lcom/twitter/library/api/ar;

    const/16 v3, 0x10

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/twitter/library/api/ar;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->b:Ljava/util/HashMap;

    const-string/jumbo v1, "retweeted_media_tagged"

    new-instance v2, Lcom/twitter/library/api/ar;

    const/16 v3, 0x11

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/twitter/library/api/ar;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0xc

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/twitter/library/api/ap;->c:Ljava/util/HashMap;

    sget-object v0, Lcom/twitter/library/api/ap;->c:Ljava/util/HashMap;

    const-string/jumbo v1, "both_follow"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->c:Ljava/util/HashMap;

    const-string/jumbo v1, "both_followed_by"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->c:Ljava/util/HashMap;

    const-string/jumbo v1, "follow_and_follow"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->c:Ljava/util/HashMap;

    const-string/jumbo v1, "follower_of_follower"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->c:Ljava/util/HashMap;

    const-string/jumbo v1, "follow_and_retweets"

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->c:Ljava/util/HashMap;

    const-string/jumbo v1, "follower_and_retweets"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->c:Ljava/util/HashMap;

    const-string/jumbo v1, "follow_and_reply"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->c:Ljava/util/HashMap;

    const-string/jumbo v1, "follower_and_reply"

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->c:Ljava/util/HashMap;

    const-string/jumbo v1, "follow_and_fav"

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->c:Ljava/util/HashMap;

    const-string/jumbo v1, "follower_and_fav"

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->c:Ljava/util/HashMap;

    const-string/jumbo v1, "reply_to_follow"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->c:Ljava/util/HashMap;

    const-string/jumbo v1, "reply_to_follower"

    const/16 v2, 0xc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->c:Ljava/util/HashMap;

    const-string/jumbo v1, "popular"

    const/16 v2, 0x1c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->c:Ljava/util/HashMap;

    const-string/jumbo v1, "nearby"

    const/16 v2, 0x1b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->c:Ljava/util/HashMap;

    const-string/jumbo v1, "has_trend"

    const/16 v2, 0x15

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v7}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/twitter/library/api/ap;->d:Ljava/util/HashMap;

    sget-object v0, Lcom/twitter/library/api/ap;->d:Ljava/util/HashMap;

    const-string/jumbo v1, "status"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->d:Ljava/util/HashMap;

    const-string/jumbo v1, "user"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->d:Ljava/util/HashMap;

    const-string/jumbo v1, "news"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->d:Ljava/util/HashMap;

    const-string/jumbo v1, "suggestion"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->d:Ljava/util/HashMap;

    const-string/jumbo v1, "user_gallery"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->d:Ljava/util/HashMap;

    const-string/jumbo v1, "media_gallery"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->d:Ljava/util/HashMap;

    const-string/jumbo v1, "tweet_gallery"

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->d:Ljava/util/HashMap;

    const-string/jumbo v1, "event_summary"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->d:Ljava/util/HashMap;

    const-string/jumbo v1, "event_update"

    const/16 v2, 0xc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->d:Ljava/util/HashMap;

    const-string/jumbo v1, "timeline"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/ap;->d:Ljava/util/HashMap;

    const-string/jumbo v1, "timeline_gallery"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private static A(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterTopic$TvEvent;
    .locals 10

    const/4 v8, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v6, v7

    move v5, v8

    move v4, v8

    move-object v3, v7

    move-object v2, v7

    move-object v1, v7

    :goto_0
    if-eqz v0, :cond_6

    sget-object v9, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v9, :cond_6

    sget-object v9, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v9, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v9, "series_title"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_1
    const-string/jumbo v9, "series_description"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_2
    const-string/jumbo v9, "episode_description"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_3
    const-string/jumbo v9, "episode_title"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    :cond_4
    const-string/jumbo v9, "channel"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v9, "season_number"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v4

    goto :goto_1

    :cond_5
    const-string/jumbo v9, "episode_number"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v5

    goto :goto_1

    :pswitch_3
    const-string/jumbo v0, "is_new"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v8, 0x1

    goto :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    :cond_6
    new-instance v0, Lcom/twitter/library/api/TwitterTopic$TvEvent;

    invoke-direct/range {v0 .. v8}, Lcom/twitter/library/api/TwitterTopic$TvEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Z)V

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static A(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_1

    invoke-static {p0}, Lcom/twitter/library/api/ap;->B(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/Bucket;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_2
    return-object v1
.end method

.method public static B(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/Bucket;
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v4, v0

    move v0, v1

    move-object v1, v2

    move-object v2, v4

    :goto_0
    if-eqz v2, :cond_1

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_1

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    :pswitch_1
    const-string/jumbo v2, "name"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :pswitch_2
    const-string/jumbo v2, "value"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v0

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_1
    new-instance v2, Lcom/twitter/library/api/Bucket;

    invoke-direct {v2, v1, v0}, Lcom/twitter/library/api/Bucket;-><init>(Ljava/lang/String;I)V

    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private static B(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterTopic$LocalEvent;
    .locals 8

    const-wide/16 v5, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-wide v3, v5

    move-wide v1, v5

    :goto_0
    if-eqz v0, :cond_3

    sget-object v7, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v7, :cond_3

    sget-object v7, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v7, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v7, "latitude"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->j()D

    move-result-wide v1

    goto :goto_1

    :cond_1
    const-string/jumbo v7, "longitude"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->j()D

    move-result-wide v3

    goto :goto_1

    :cond_2
    const-string/jumbo v7, "distance"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->j()D

    move-result-wide v5

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_3
    new-instance v0, Lcom/twitter/library/api/TwitterTopic$LocalEvent;

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/api/TwitterTopic$LocalEvent;-><init>(DDD)V

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static C(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterTopic$SportsEvent$Player;
    .locals 7

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v4, v5

    move-object v3, v5

    move-object v2, v5

    move-object v1, v5

    :goto_0
    if-eqz v0, :cond_5

    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v6, :cond_5

    sget-object v6, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v6, v0

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v6, "location"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_1
    const-string/jumbo v6, "name"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_2
    const-string/jumbo v6, "score"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_3
    const-string/jumbo v6, "logo_url"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_4
    const-string/jumbo v6, "abbreviation"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_5
    new-instance v0, Lcom/twitter/library/api/TwitterTopic$SportsEvent$Player;

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/api/TwitterTopic$SportsEvent$Player;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static C(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v0

    move-object v0, v1

    move-object v1, v3

    :goto_0
    if-eqz v1, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_1

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "error"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_1
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static D(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v0

    move-object v0, v1

    move-object v1, v3

    :goto_0
    if-eqz v1, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_1

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "message"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_1
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static D(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_0

    invoke-static {p0, p1}, Lcom/twitter/library/api/ap;->C(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterTopic$SportsEvent$Player;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_1
    return-object v1
.end method

.method public static E(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/Prompt;
    .locals 23

    const-string/jumbo v2, ""

    const-string/jumbo v3, ""

    const-string/jumbo v5, ""

    const-string/jumbo v4, ""

    const-string/jumbo v6, ""

    const-string/jumbo v7, ""

    const-string/jumbo v14, ""

    const-string/jumbo v8, ""

    const-string/jumbo v17, ""

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v9, 0xb4

    const/4 v10, 0x0

    const-wide/16 v15, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_f

    sget-object v11, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v11, :cond_f

    sget-object v11, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v1, v11, :cond_e

    const-string/jumbo v11, "prompt"

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_c

    :goto_1
    if-eqz v1, :cond_b

    sget-object v11, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v11, :cond_b

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v11

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v1

    sget-object v18, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v11}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v19

    aget v18, v18, v19

    packed-switch v18, :pswitch_data_0

    :pswitch_0
    move-object v1, v11

    goto :goto_1

    :pswitch_1
    const-string/jumbo v18, "text"

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v2

    move-object v1, v11

    goto :goto_1

    :cond_0
    const-string/jumbo v18, "header"

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v3

    move-object v1, v11

    goto :goto_1

    :cond_1
    const-string/jumbo v18, "action_text"

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v4

    move-object v1, v11

    goto :goto_1

    :cond_2
    const-string/jumbo v18, "action_url"

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v5

    move-object v1, v11

    goto :goto_1

    :cond_3
    const-string/jumbo v18, "icon"

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v6

    move-object v1, v11

    goto :goto_1

    :cond_4
    const-string/jumbo v18, "format"

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v7

    move-object v1, v11

    goto :goto_1

    :cond_5
    const-string/jumbo v18, "background_image_url"

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v8

    move-object v1, v11

    goto/16 :goto_1

    :cond_6
    const-string/jumbo v18, "template"

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v14

    move-object v1, v11

    goto/16 :goto_1

    :pswitch_2
    const-string/jumbo v18, "prompt_id"

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v10

    move-object v1, v11

    goto/16 :goto_1

    :cond_7
    const-string/jumbo v18, "persistence"

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v9

    move-object v1, v11

    goto/16 :goto_1

    :pswitch_3
    const-string/jumbo v18, "entities"

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_8

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/TweetEntities;->a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/TweetEntities;

    move-result-object v12

    move-object v1, v11

    goto/16 :goto_1

    :cond_8
    const-string/jumbo v18, "header_entities"

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_9

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/TweetEntities;->a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/TweetEntities;

    move-result-object v13

    move-object v1, v11

    goto/16 :goto_1

    :cond_9
    const-string/jumbo v18, "data"

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/ap;->ao(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;

    move-result-object v20

    const-string/jumbo v1, "tweetId"

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    :try_start_0
    const-string/jumbo v1, "tweetId"

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v18

    const-wide/16 v21, 0x0

    cmp-long v1, v18, v21

    if-lez v1, :cond_a

    move-wide/from16 v15, v18

    :cond_a
    :goto_2
    const-string/jumbo v1, "style"

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    const-string/jumbo v1, "style"

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    :goto_3
    move-object/from16 v17, v1

    move-object v1, v11

    goto/16 :goto_1

    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v1, v11

    goto/16 :goto_1

    :cond_b
    new-instance v1, Lcom/twitter/library/api/Prompt;

    const/4 v11, 0x0

    invoke-direct/range {v1 .. v17}, Lcom/twitter/library/api/Prompt;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II[ILcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/TweetEntities;Ljava/lang/String;JLjava/lang/String;)V

    :goto_4
    return-object v1

    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    :cond_d
    :goto_5
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto/16 :goto_0

    :cond_e
    sget-object v11, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v1, v11, :cond_d

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_5

    :cond_f
    const/4 v1, 0x0

    goto :goto_4

    :catch_0
    move-exception v1

    goto :goto_2

    :cond_10
    move-object/from16 v1, v17

    goto :goto_3

    :cond_11
    move-object v1, v11

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private static E(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterTopic$SportsEvent;
    .locals 9

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v5, v6

    move-object v4, v6

    move-object v3, v6

    move-object v2, v6

    move-object v1, v6

    :goto_0
    if-eqz v0, :cond_7

    sget-object v7, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v7, :cond_7

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v8, v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "sports_title"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_1
    const-string/jumbo v0, "game_type"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_2
    const-string/jumbo v0, "channel"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :pswitch_2
    const-string/jumbo v0, "game_info"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_0

    sget-object v7, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v7, :cond_0

    sget-object v7, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v7, v0

    packed-switch v0, :pswitch_data_1

    :cond_3
    :goto_3
    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_2

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v7, "summary"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    :cond_4
    const-string/jumbo v7, "status"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v4

    goto :goto_3

    :pswitch_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :pswitch_6
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :cond_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_7
    const-string/jumbo v0, "players"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {p0, p1}, Lcom/twitter/library/api/ap;->D(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v6

    goto/16 :goto_1

    :cond_6
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    :cond_7
    new-instance v0, Lcom/twitter/library/api/TwitterTopic$SportsEvent;

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/api/TwitterTopic$SportsEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_6
    .end packed-switch
.end method

.method public static F(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/TwitterUserMetadata;
    .locals 6

    const/4 v4, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v0

    move-object v2, v4

    move-object v0, v4

    :goto_0
    if-eqz v3, :cond_3

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v3, v5, :cond_3

    sget-object v5, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v3}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v3

    aget v3, v5, v3

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v5, "result_type"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string/jumbo v1, "top"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_1

    :cond_1
    const-string/jumbo v5, "title"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    const-string/jumbo v3, "social_context"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {p0}, Lcom/twitter/library/api/ap;->H(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/TwitterSocialProof;

    move-result-object v2

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/twitter/library/api/TwitterUserMetadata;

    invoke-direct {v3, v2, v0, v4, v1}, Lcom/twitter/library/api/TwitterUserMetadata;-><init>(Lcom/twitter/library/api/TwitterSocialProof;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static F(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;
    .locals 3

    invoke-static {p0, p1}, Lcom/twitter/library/api/ap;->s(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterStatus;

    invoke-static {v0}, Lcom/twitter/library/api/ap;->a(Lcom/twitter/library/api/TwitterStatus;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    move-object v0, v1

    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static G(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/TwitterSocialProof;
    .locals 10

    const/4 v4, -0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v2, v7

    move v1, v4

    :goto_0
    if-eqz v0, :cond_5

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v5, :cond_5

    sget-object v5, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "type"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/library/api/ap;->c:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_1

    :pswitch_2
    const-string/jumbo v0, "matched_trends"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v9, v2

    move-object v2, v0

    move-object v0, v9

    :goto_2
    if-eqz v2, :cond_7

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v5, :cond_7

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->h:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v2, v5, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    :cond_1
    :goto_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_2

    :cond_2
    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v5, :cond_3

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v2, v5, :cond_1

    :cond_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :cond_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_5
    if-eq v1, v4, :cond_6

    new-instance v0, Lcom/twitter/library/api/TwitterSocialProof;

    move v4, v3

    move v5, v3

    move v6, v3

    move v8, v3

    invoke-direct/range {v0 .. v8}, Lcom/twitter/library/api/TwitterSocialProof;-><init>(ILjava/lang/String;IIIILjava/lang/String;I)V

    :goto_4
    return-object v0

    :cond_6
    move-object v0, v7

    goto :goto_4

    :cond_7
    move-object v2, v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private static G(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/search/b;
    .locals 8

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v6, v0

    move-object v1, v5

    move-object v2, v5

    move-object v0, v5

    :goto_0
    if-eqz v6, :cond_8

    sget-object v7, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v6, v7, :cond_8

    sget-object v7, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v6}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v6

    aget v6, v7, v6

    packed-switch v6, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v6

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "status"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    :goto_2
    if-eqz v3, :cond_b

    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v3, v6, :cond_b

    sget-object v6, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v3}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v3

    aget v3, v6, v3

    packed-switch v3, :pswitch_data_1

    :goto_3
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    goto :goto_2

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v6, "data"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-static {p0, v4, p1}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterStatus;

    move-result-object v2

    goto :goto_3

    :cond_1
    const-string/jumbo v6, "metadata"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {p0, p1}, Lcom/twitter/library/api/ap;->f(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/au;

    move-result-object v1

    goto :goto_3

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :cond_3
    const-string/jumbo v7, "wtf"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v6

    :goto_4
    if-eqz v6, :cond_0

    sget-object v7, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v6, v7, :cond_0

    sget-object v7, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v6}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v6

    aget v6, v7, v6

    packed-switch v6, :pswitch_data_2

    :cond_4
    :goto_5
    :pswitch_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v6

    goto :goto_4

    :pswitch_6
    const-string/jumbo v6, "data"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v6

    :goto_6
    if-eqz v6, :cond_4

    sget-object v7, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v6, v7, :cond_4

    sget-object v7, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v6}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v6

    aget v6, v7, v6

    packed-switch v6, :pswitch_data_3

    :goto_7
    :pswitch_7
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v6

    goto :goto_6

    :pswitch_8
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_7

    :pswitch_9
    const-string/jumbo v6, "users"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-static {p0, p1}, Lcom/twitter/library/api/ap;->u(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_7

    :cond_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_7

    :cond_6
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_5

    :pswitch_a
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_5

    :cond_7
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    :pswitch_b
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    :cond_8
    packed-switch v3, :pswitch_data_4

    :cond_9
    move-object v0, v5

    :goto_8
    return-object v0

    :pswitch_c
    if-eqz v2, :cond_9

    if-eqz v1, :cond_a

    iput-object v1, v2, Lcom/twitter/library/api/TwitterStatus;->v:Lcom/twitter/library/api/au;

    :cond_a
    new-instance v0, Lcom/twitter/library/api/search/b;

    invoke-direct {v0, v3, v2, v5}, Lcom/twitter/library/api/search/b;-><init>(ILcom/twitter/library/api/TwitterStatus;Ljava/util/ArrayList;)V

    goto :goto_8

    :pswitch_d
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_9

    new-instance v1, Lcom/twitter/library/api/search/b;

    invoke-direct {v1, v3, v5, v0}, Lcom/twitter/library/api/search/b;-><init>(ILcom/twitter/library/api/TwitterStatus;Ljava/util/ArrayList;)V

    move-object v0, v1

    goto :goto_8

    :cond_b
    move v3, v4

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_a
        :pswitch_5
        :pswitch_5
        :pswitch_6
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_9
        :pswitch_7
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method private static H(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TimelineScribeContent;
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v0

    move-object v2, v1

    move-object v0, v1

    :goto_0
    if-eqz v3, :cond_3

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v3, v4, :cond_3

    sget-object v4, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v3}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v3

    aget v3, v4, v3

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "source"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    const-string/jumbo v4, "type"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    const-string/jumbo v4, "impression_id"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/twitter/library/api/TimelineScribeContent;

    invoke-direct {v3, v2, v1, v0}, Lcom/twitter/library/api/TimelineScribeContent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v3

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static H(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/TwitterSocialProof;
    .locals 10

    const/4 v4, -0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move v6, v3

    move-object v2, v7

    move v1, v4

    :goto_0
    if-eqz v0, :cond_5

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v5, :cond_5

    sget-object v5, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v5, v0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :sswitch_0
    const-string/jumbo v0, "related_users"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v9, v2

    move v2, v1

    move-object v1, v9

    :goto_2
    if-eqz v0, :cond_9

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v5, :cond_9

    sget-object v5, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move-object v0, v1

    move v1, v2

    :goto_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    move-object v9, v0

    move-object v0, v2

    move v2, v1

    move-object v1, v9

    goto :goto_2

    :pswitch_1
    sget-object v0, Lcom/twitter/library/api/ap;->c:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {p0}, Lcom/twitter/library/api/ap;->ap(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v1

    move v1, v2

    goto :goto_3

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v1

    move v1, v2

    goto :goto_3

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :sswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :sswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v5, "following"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v0, 0x1

    invoke-static {v6, v0}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v6

    const/16 v1, 0x1a

    :cond_3
    :goto_4
    invoke-static {v6}, Lcom/twitter/library/provider/ay;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v6}, Lcom/twitter/library/provider/ay;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v1, 0x19

    goto/16 :goto_1

    :cond_4
    const-string/jumbo v5, "followed_by"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x2

    invoke-static {v6, v0}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v6

    goto :goto_4

    :cond_5
    if-eq v1, v4, :cond_6

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    if-eqz v6, :cond_8

    :cond_7
    new-instance v0, Lcom/twitter/library/api/TwitterSocialProof;

    move v4, v3

    move v5, v3

    move v8, v3

    invoke-direct/range {v0 .. v8}, Lcom/twitter/library/api/TwitterSocialProof;-><init>(ILjava/lang/String;IIIILjava/lang/String;I)V

    :goto_5
    return-object v0

    :cond_8
    move-object v0, v7

    goto :goto_5

    :cond_9
    move-object v9, v1

    move v1, v2

    move-object v2, v9

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x4 -> :sswitch_0
        0x7 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static I(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/DiscoverStoryMetadata;
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v2, v1

    move-object v1, v0

    :goto_0
    if-eqz v2, :cond_2

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_2

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    :pswitch_0
    const-string/jumbo v2, "source"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_1
    const-string/jumbo v2, "type"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    new-instance v2, Lcom/twitter/library/api/DiscoverStoryMetadata;

    invoke-direct {v2, v1, v0}, Lcom/twitter/library/api/DiscoverStoryMetadata;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static I(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/conversations/c;
    .locals 2

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_1

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v1, :cond_0

    invoke-static {p0}, Lcom/twitter/library/api/conversations/c;->a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/conversations/c;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static J(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/conversations/q;
    .locals 2

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_1

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v1, :cond_0

    invoke-static {p0}, Lcom/twitter/library/api/conversations/q;->a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/conversations/q;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static J(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-static {p0, p1}, Lcom/twitter/library/api/ap;->K(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/x;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_1
    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static K(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/af;
    .locals 10

    const-wide/16 v7, 0x0

    const/4 v4, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v4

    move-wide v5, v7

    move-object v1, v4

    :goto_0
    if-eqz v0, :cond_6

    sget-object v9, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v9, :cond_6

    sget-object v9, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v9, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v9, "cluster_id"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_1
    const-string/jumbo v9, "size"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v5

    goto :goto_1

    :cond_2
    const-string/jumbo v9, "timestamp"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v7

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v9, "id"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_3
    const-string/jumbo v9, "title"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_4
    const-string/jumbo v9, "subtitle"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_5
    const-string/jumbo v9, "type"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "PEOPLE"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_6
    new-instance v0, Lcom/twitter/library/api/af;

    invoke-direct/range {v0 .. v8}, Lcom/twitter/library/api/af;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;JJ)V

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method private static K(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/x;
    .locals 5

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/api/z;

    invoke-direct {v1}, Lcom/twitter/library/api/z;-><init>()V

    :goto_0
    if-eqz v0, :cond_3

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_3

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "status"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0, p1}, Lcom/twitter/library/api/ap;->L(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterStatus;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/api/TwitterStatus;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/api/z;->a(Ljava/lang/String;)Lcom/twitter/library/api/z;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/twitter/library/api/z;->a(Lcom/twitter/library/api/TwitterStatus;)Lcom/twitter/library/api/z;

    move-result-object v2

    iget-wide v3, v0, Lcom/twitter/library/api/TwitterStatus;->r:J

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/api/z;->a(J)Lcom/twitter/library/api/z;

    goto :goto_1

    :cond_1
    const-string/jumbo v0, "local_event_summary"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0, p1}, Lcom/twitter/library/api/ap;->M(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterTopic;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/twitter/library/api/TwitterTopic;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/twitter/library/api/z;->a(Ljava/lang/String;)Lcom/twitter/library/api/z;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/twitter/library/api/z;->a(Lcom/twitter/library/api/TwitterTopic;)Lcom/twitter/library/api/z;

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_3
    invoke-virtual {v1}, Lcom/twitter/library/api/z;->b()Lcom/twitter/library/api/x;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static L(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterStatus;
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v0

    move-object v0, v1

    move-object v1, v3

    :goto_0
    if-eqz v1, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_1

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    const-string/jumbo v1, "data"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    invoke-static {p0, v0, p1}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterStatus;

    move-result-object v0

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_1
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static L(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/c;
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v2, v3, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    :goto_0
    if-eqz v2, :cond_1

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_1

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    const-string/jumbo v2, "msg"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_3
    const-string/jumbo v2, "valid"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->k()Z

    move-result v1

    goto :goto_1

    :cond_1
    new-instance v2, Lcom/twitter/library/api/c;

    invoke-direct {v2, v1, v0}, Lcom/twitter/library/api/c;-><init>(ZLjava/lang/String;)V

    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private static M(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterTopic;
    .locals 4

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v1, v0

    move-object v0, v2

    :goto_0
    if-eqz v1, :cond_1

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_1

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    const-string/jumbo v1, "data"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0, v2, v2, p1}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterTopic;

    move-result-object v0

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_1
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static M(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v0

    move-object v0, v1

    move-object v1, v3

    :goto_0
    if-eqz v1, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_1

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    const-string/jumbo v1, "suggestion"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_1
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static N(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_0

    invoke-static {p0}, Lcom/twitter/library/api/ap;->M(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public static O(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/ActivitySummary;
    .locals 8

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    const/4 v0, -0x1

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v2, v3, :cond_a

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v5

    sget-object v6, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v5}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    :cond_1
    :goto_0
    :pswitch_0
    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v5, v6, :cond_0

    move-object v5, v1

    move-object v1, v4

    move-object v4, v2

    move-object v2, v3

    move v3, v0

    :goto_1
    new-instance v0, Lcom/twitter/library/api/ActivitySummary;

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/api/ActivitySummary;-><init>(Ljava/lang/String;Ljava/lang/String;I[J[J)V

    return-object v0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "favoriters_count"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_2
    const-string/jumbo v7, "retweeters_count"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "descendent_reply_count"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v0

    goto :goto_0

    :cond_3
    const-string/jumbo v7, "reply_count"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    if-gez v0, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v0

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "favoriters"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    :goto_2
    if-eqz v5, :cond_5

    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v5, v6, :cond_5

    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->i:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v5, v6, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v5

    goto :goto_2

    :cond_5
    invoke-static {v2}, Lcom/twitter/library/util/Util;->b(Ljava/util/Collection;)[J

    move-result-object v2

    goto/16 :goto_0

    :cond_6
    const-string/jumbo v7, "retweeters"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :goto_3
    if-eqz v5, :cond_8

    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v5, v6, :cond_8

    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->i:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v5, v6, :cond_7

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_7
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v5

    goto :goto_3

    :cond_8
    invoke-static {v1}, Lcom/twitter/library/util/Util;->b(Ljava/util/Collection;)[J

    move-result-object v1

    goto/16 :goto_0

    :cond_9
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_0

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_0

    :cond_a
    move v3, v0

    move-object v5, v1

    move-object v4, v1

    move-object v2, v1

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method

.method public static P(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;
    .locals 12

    const/4 v9, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    if-eqz v0, :cond_a

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_a

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v1, :cond_9

    const-string/jumbo v0, "errors"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v2, v0

    :goto_1
    if-eqz v2, :cond_1

    move v0, v7

    :goto_2
    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v1, :cond_2

    move v1, v7

    :goto_3
    and-int/2addr v0, v1

    if-eqz v0, :cond_9

    sget-object v0, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v2, v0, :cond_8

    const-wide/16 v3, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move v6, v8

    move-object v5, v9

    move-object v2, v9

    move v1, v8

    :goto_4
    if-eqz v0, :cond_6

    sget-object v11, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v11, :cond_6

    sget-object v11, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v11, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_5
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_4

    :cond_1
    move v0, v8

    goto :goto_2

    :cond_2
    move v1, v8

    goto :goto_3

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_5

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v11, "message"

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v2

    goto :goto_5

    :cond_3
    const-string/jumbo v11, "timestamp"

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    goto :goto_5

    :cond_4
    const-string/jumbo v11, "attribute"

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v5

    goto :goto_5

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v11, "code"

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v1

    goto :goto_5

    :cond_5
    const-string/jumbo v11, "sub_error_code"

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v6

    goto :goto_5

    :cond_6
    if-lez v1, :cond_7

    new-instance v0, Lcom/twitter/library/api/al;

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/api/al;-><init>(ILjava/lang/String;JLjava/lang/String;I)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_7
    :goto_6
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v2, v0

    goto/16 :goto_1

    :cond_8
    sget-object v0, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v2, v0, :cond_7

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_6

    :cond_9
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto/16 :goto_0

    :cond_a
    return-object v10

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public static Q(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_4

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_4

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "users"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p0}, Lcom/twitter/library/api/ap;->ar(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string/jumbo v2, "topics"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v0, 0x3

    invoke-static {p0, v0}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;I)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_2
    const-string/jumbo v2, "oneclick"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-static {p0, v0}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;I)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_4
    return-object v1
.end method

.method public static R(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/search/TwitterTypeAheadGroup;
    .locals 6

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v4, v0

    move-object v2, v1

    move-object v3, v1

    move-object v0, v1

    :goto_0
    if-eqz v4, :cond_4

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v5, :cond_4

    sget-object v5, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v4}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v4

    aget v4, v5, v4

    packed-switch v4, :pswitch_data_0

    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "users"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {p0}, Lcom/twitter/library/api/ap;->ar(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v3

    goto :goto_1

    :cond_0
    const-string/jumbo v5, "topics"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v2, 0x3

    invoke-static {p0, v2}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;I)Ljava/util/ArrayList;

    move-result-object v2

    goto :goto_1

    :cond_1
    const-string/jumbo v5, "hashtags"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-static {p0}, Lcom/twitter/library/api/ap;->at(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_1

    :cond_2
    const-string/jumbo v5, "oneclick"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v0, 0x4

    invoke-static {p0, v0}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;I)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_4
    new-instance v4, Lcom/twitter/library/api/search/TwitterTypeAheadGroup;

    invoke-direct {v4, v3, v2, v1, v0}, Lcom/twitter/library/api/search/TwitterTypeAheadGroup;-><init>(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    return-object v4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static S(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/aj;
    .locals 8

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    :goto_0
    if-eqz v1, :cond_5

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v4, :cond_5

    sget-object v4, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v4, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    const-string/jumbo v1, "ids"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v5, v1

    :goto_2
    if-eqz v5, :cond_2

    move v1, v2

    :goto_3
    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v5, v4, :cond_3

    move v4, v2

    :goto_4
    and-int/2addr v1, v4

    if-eqz v1, :cond_0

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->i:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v5, v1, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v5, v1

    goto :goto_2

    :cond_2
    move v1, v3

    goto :goto_3

    :cond_3
    move v4, v3

    goto :goto_4

    :cond_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    const-string/jumbo v1, "next_cursor_str"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_5
    new-instance v1, Lcom/twitter/library/api/aj;

    invoke-direct {v1, v0, v6}, Lcom/twitter/library/api/aj;-><init>(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-object v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static T(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/t;
    .locals 9

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v5, v6

    move-object v4, v6

    move-object v3, v6

    move-object v2, v6

    move-object v1, v6

    :goto_0
    if-eqz v0, :cond_6

    sget-object v7, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v7, :cond_6

    sget-object v7, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v7, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_0

    sget-object v8, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v8, :cond_0

    sget-object v8, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v8, v0

    packed-switch v0, :pswitch_data_1

    :cond_1
    :goto_3
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_2

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :pswitch_4
    const-string/jumbo v0, "fullname"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-nez v1, :cond_2

    sget-object v0, Lcom/twitter/internal/util/a;->a:Lcom/twitter/internal/util/a;

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/internal/util/a;->b(Ljava/lang/String;)Lcom/twitter/internal/util/e;

    move-result-object v0

    iget-object v1, v0, Lcom/twitter/internal/util/e;->a:Ljava/lang/String;

    goto :goto_3

    :cond_2
    const-string/jumbo v0, "screen_name"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    if-nez v3, :cond_3

    sget-object v0, Lcom/twitter/internal/util/a;->a:Lcom/twitter/internal/util/a;

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/internal/util/a;->b(Ljava/lang/String;)Lcom/twitter/internal/util/e;

    move-result-object v0

    iget-object v3, v0, Lcom/twitter/internal/util/e;->a:Ljava/lang/String;

    goto :goto_3

    :cond_3
    const-string/jumbo v0, "password"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    if-nez v4, :cond_4

    sget-object v0, Lcom/twitter/internal/util/a;->a:Lcom/twitter/internal/util/a;

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/twitter/internal/util/a;->b(Ljava/lang/String;)Lcom/twitter/internal/util/e;

    move-result-object v0

    iget-object v4, v0, Lcom/twitter/internal/util/e;->a:Ljava/lang/String;

    goto :goto_3

    :cond_4
    const-string/jumbo v0, "email"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez v2, :cond_1

    sget-object v0, Lcom/twitter/internal/util/a;->a:Lcom/twitter/internal/util/a;

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/internal/util/a;->b(Ljava/lang/String;)Lcom/twitter/internal/util/e;

    move-result-object v0

    iget-object v2, v0, Lcom/twitter/internal/util/e;->a:Ljava/lang/String;

    goto :goto_3

    :pswitch_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    :pswitch_6
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v7, "captcha_token"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :cond_5
    const-string/jumbo v7, "captcha_image_url"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_1

    :cond_6
    new-instance v0, Lcom/twitter/library/api/t;

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/api/t;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_6
        :pswitch_0
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static U(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/h;
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v4, v0

    move-object v0, v1

    move v1, v2

    move-object v2, v4

    :goto_0
    if-eqz v2, :cond_1

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_1

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    :pswitch_1
    const-string/jumbo v2, "build_number"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v1

    goto :goto_1

    :pswitch_2
    const-string/jumbo v2, "checksum"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_1
    new-instance v2, Lcom/twitter/library/api/h;

    invoke-direct {v2, v1, v0}, Lcom/twitter/library/api/h;-><init>(ILjava/lang/String;)V

    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method public static V(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;
    .locals 11

    const-wide/16 v7, 0x0

    const/4 v6, 0x0

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_6

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_6

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v1, :cond_5

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v5, v6

    move-object v4, v6

    move-wide v2, v7

    move-object v1, v6

    :goto_1
    if-eqz v0, :cond_3

    sget-object v10, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v10, :cond_3

    sget-object v10, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v10, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_2
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v10, "name"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_1
    const-string/jumbo v10, "country"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    :cond_2
    const-string/jumbo v10, "countryCode"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    :pswitch_2
    const-string/jumbo v0, "woeid"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v2

    goto :goto_2

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    :cond_3
    if-eqz v1, :cond_4

    cmp-long v0, v2, v7

    if-eqz v0, :cond_4

    new-instance v0, Lcom/twitter/library/api/TwitterLocation;

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/api/TwitterLocation;-><init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    :goto_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_5
    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v1, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :cond_6
    return-object v9

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public static W(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/featureswitch/FeatureSwitchesManifest;
    .locals 6

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v4, v0

    move-object v2, v1

    move-object v3, v1

    move-object v0, v1

    :goto_0
    if-eqz v4, :cond_6

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v5, :cond_6

    sget-object v5, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v4}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v4

    aget v4, v5, v4

    packed-switch v4, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    goto :goto_0

    :pswitch_1
    const-string/jumbo v4, "default"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    :goto_2
    if-eqz v4, :cond_0

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v5, :cond_0

    sget-object v5, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v4}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v4

    aget v4, v5, v4

    packed-switch v4, :pswitch_data_1

    :goto_3
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    goto :goto_2

    :pswitch_3
    const-string/jumbo v4, "config"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {p0}, Lcom/twitter/library/api/ap;->Y(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;

    move-result-object v3

    goto :goto_3

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :pswitch_4
    const-string/jumbo v4, "impressions"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {p0}, Lcom/twitter/library/api/ap;->Z(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashSet;

    move-result-object v2

    goto :goto_3

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :cond_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_5
    const-string/jumbo v4, "requires_restart"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-static {p0}, Lcom/twitter/library/api/ap;->aH(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashSet;

    move-result-object v1

    goto :goto_1

    :cond_4
    const-string/jumbo v4, "experiment_names"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-static {p0}, Lcom/twitter/library/api/ap;->aH(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashSet;

    move-result-object v0

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    :cond_6
    if-nez v3, :cond_7

    new-instance v0, Lcom/twitter/library/util/InvalidDataException;

    const-string/jumbo v1, "\'default\' does not exist in the manifest."

    invoke-direct {v0, v1}, Lcom/twitter/library/util/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    if-nez v1, :cond_8

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    :cond_8
    if-nez v0, :cond_9

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    :cond_9
    new-instance v4, Lcom/twitter/library/featureswitch/FeatureSwitchesManifest;

    invoke-direct {v4, v3, v2, v1, v0}, Lcom/twitter/library/featureswitch/FeatureSwitchesManifest;-><init>(Ljava/util/HashMap;Ljava/util/HashSet;Ljava/util/HashSet;Ljava/util/HashSet;)V

    return-object v4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static X(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/featureswitch/FeatureSwitchesConfig;
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v2, v0

    move-object v0, v1

    :goto_0
    if-eqz v2, :cond_2

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_2

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    :pswitch_1
    const-string/jumbo v2, "config"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p0}, Lcom/twitter/library/api/ap;->Y(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;

    move-result-object v1

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    const-string/jumbo v2, "impressions"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p0}, Lcom/twitter/library/api/ap;->Z(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashSet;

    move-result-object v0

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_2
    if-nez v1, :cond_3

    new-instance v0, Lcom/twitter/library/util/InvalidDataException;

    const-string/jumbo v1, "Invalid feature switch Configs"

    invoke-direct {v0, v1}, Lcom/twitter/library/util/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    new-instance v2, Lcom/twitter/library/featureswitch/FeatureSwitchesConfig;

    invoke-direct {v2, v1, v0}, Lcom/twitter/library/featureswitch/FeatureSwitchesConfig;-><init>(Ljava/util/HashMap;Ljava/util/HashSet;)V

    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static Y(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;
    .locals 3

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_1

    invoke-static {p0}, Lcom/twitter/library/api/ap;->aE(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/featureswitch/FeatureSwitchesValue;

    move-result-object v0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_2
    return-object v1
.end method

.method public static Z(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashSet;
    .locals 7

    const/4 v3, 0x0

    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_5

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_5

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v1, v3

    move-object v2, v3

    move-object v4, v0

    move-object v0, v3

    :goto_1
    if-eqz v4, :cond_2

    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v6, :cond_2

    sget-object v6, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v4}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v4

    aget v4, v6, v4

    packed-switch v4, :pswitch_data_0

    :cond_0
    :goto_2
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    goto :goto_1

    :pswitch_1
    const-string/jumbo v4, "key"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_1
    const-string/jumbo v4, "bucket"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :pswitch_2
    const-string/jumbo v4, "version"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_2

    :cond_2
    if-eqz v2, :cond_3

    if-eqz v1, :cond_3

    if-nez v0, :cond_4

    :cond_3
    new-instance v0, Lcom/twitter/library/util/InvalidDataException;

    const-string/jumbo v1, "Invalid data found for FeatureSwitchesParam"

    invoke-direct {v0, v1}, Lcom/twitter/library/util/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    new-instance v4, Lcom/twitter/library/featureswitch/FeatureSwitchesValue$FeatureSwitchesImpression;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v4, v2, v0, v1}, Lcom/twitter/library/featureswitch/FeatureSwitchesValue$FeatureSwitchesImpression;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_5
    return-object v5

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/ArrayList;)I
    .locals 4

    const/4 v1, -0x1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v2, v0

    move v0, v1

    :goto_0
    if-eqz v2, :cond_3

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_3

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    :pswitch_1
    const-string/jumbo v2, "suggestion_type"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "spelling"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v0, 0x2

    goto :goto_1

    :cond_1
    const-string/jumbo v3, "related"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x3

    goto :goto_1

    :pswitch_2
    const-string/jumbo v2, "suggestions"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {p0, p1}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/ArrayList;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_3
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    move v0, v1

    :cond_5
    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;
    .locals 1

    sget-object v0, Lcom/twitter/library/api/ap;->a:Lcom/fasterxml/jackson/core/JsonFactory;

    invoke-virtual {v0, p0}, Lcom/fasterxml/jackson/core/JsonFactory;->b(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    return-object v0
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/Conversation$Metadata;
    .locals 15

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v7, 0x0

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    const/4 v6, 0x0

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    :goto_0
    if-eqz v1, :cond_5

    sget-object v8, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v8, :cond_5

    sget-object v8, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v8, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :pswitch_0
    move-object v1, v5

    move-object v5, v7

    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v7

    move-object v14, v1

    move-object v1, v7

    move-object v7, v5

    move-object v5, v14

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v8, "participants_count"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v6

    move-object v1, v5

    move-object v5, v7

    goto :goto_1

    :cond_1
    const-string/jumbo v8, "target_count"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v4

    move-object v1, v5

    move-object v5, v7

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v8, "root_user_id"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/TwitterUser;

    if-nez v1, :cond_2

    if-eqz p2, :cond_2

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Root user "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " not in users map"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p2

    invoke-interface {v0, v7}, Lcom/twitter/library/util/w;->a(Ljava/lang/String;)V

    :cond_2
    move-object v14, v5

    move-object v5, v1

    move-object v1, v14

    goto :goto_1

    :cond_3
    const-string/jumbo v8, "target_tweet_id"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    move-object v1, v5

    move-object v5, v7

    goto/16 :goto_1

    :pswitch_3
    const-string/jumbo v1, "participant_ids"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static/range {p0 .. p2}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v1

    move-object v5, v7

    goto/16 :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v1, v5

    move-object v5, v7

    goto/16 :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v1, v5

    move-object v5, v7

    goto/16 :goto_1

    :cond_5
    if-eqz v7, :cond_7

    const/4 v8, 0x0

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_6
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/Conversation$Participant;

    iget-wide v10, v1, Lcom/twitter/library/api/Conversation$Participant;->id:J

    iget-wide v12, v7, Lcom/twitter/library/api/TwitterUser;->userId:J

    cmp-long v1, v10, v12

    if-nez v1, :cond_6

    const/4 v1, 0x1

    :goto_2
    if-nez v1, :cond_7

    const/4 v1, 0x0

    new-instance v8, Lcom/twitter/library/api/Conversation$Participant;

    invoke-direct {v8, v7}, Lcom/twitter/library/api/Conversation$Participant;-><init>(Lcom/twitter/library/api/TwitterUser;)V

    invoke-virtual {v5, v1, v8}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :cond_7
    new-instance v1, Lcom/twitter/library/api/Conversation$Metadata;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-array v7, v7, [Lcom/twitter/library/api/Conversation$Participant;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/twitter/library/api/Conversation$Participant;

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/api/Conversation$Metadata;-><init>(JI[Lcom/twitter/library/api/Conversation$Participant;I)V

    return-object v1

    :cond_8
    move v1, v8

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;I)Lcom/twitter/library/api/TweetClassicCard;
    .locals 21

    const/4 v6, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/4 v15, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    :goto_0
    if-eqz v2, :cond_1c

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_1c

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "players"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    :goto_2
    if-eqz v2, :cond_0

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_0

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v2, v3, :cond_a

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v9

    const/4 v3, 0x0

    const/4 v2, 0x0

    :goto_3
    if-eqz v9, :cond_3

    sget-object v18, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    move-object/from16 v0, v18

    if-eq v9, v0, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v18, "source_url"

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v3

    :cond_1
    :goto_4
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v9

    goto :goto_3

    :cond_2
    const-string/jumbo v18, "source_type"

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    :cond_3
    const-string/jumbo v9, "text/html"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_5

    move-object v13, v3

    :cond_4
    :goto_5
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_2

    :cond_5
    if-eqz v2, :cond_4

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_4

    invoke-static {v2}, Lcom/twitter/library/util/Util;->e(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_6

    const/4 v12, 0x1

    move-object v14, v3

    goto :goto_5

    :cond_6
    const-string/jumbo v9, "audio/mp3"

    invoke-virtual {v2, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_7

    const-string/jumbo v9, "audio/aac"

    invoke-virtual {v2, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_8

    :cond_7
    const/4 v12, 0x2

    move-object v14, v3

    goto :goto_5

    :cond_8
    const-string/jumbo v9, "audio/mp4"

    invoke-virtual {v2, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_9

    const-string/jumbo v9, "mp4a.40.2"

    invoke-virtual {v2, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_9

    const/4 v12, 0x2

    move-object v14, v3

    goto :goto_5

    :cond_9
    const-string/jumbo v9, "audio/ogg"

    invoke-virtual {v2, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    const-string/jumbo v9, "vorbis"

    invoke-virtual {v2, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v12, 0x2

    move-object v14, v3

    goto :goto_5

    :cond_a
    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v2, v3, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_5

    :cond_b
    const-string/jumbo v3, "promotions"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    :goto_6
    if-eqz v2, :cond_0

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_0

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_1

    :cond_c
    :goto_7
    :pswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_6

    :pswitch_3
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v18

    const/4 v9, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    :goto_8
    if-eqz v18, :cond_10

    sget-object v19, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_10

    sget-object v19, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual/range {v18 .. v18}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v18

    aget v18, v19, v18

    packed-switch v18, :pswitch_data_2

    :cond_d
    :goto_9
    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v18

    goto :goto_8

    :pswitch_5
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v18

    const-string/jumbo v19, "promotion_type"

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_e

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v2

    goto :goto_9

    :cond_e
    const-string/jumbo v19, "target_cta"

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_f

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v9

    goto :goto_9

    :cond_f
    const-string/jumbo v19, "target_url"

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_d

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v3

    goto :goto_9

    :pswitch_6
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_9

    :cond_10
    if-eqz v9, :cond_c

    if-eqz v3, :cond_c

    const-string/jumbo v18, "SIGN_UP"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_11

    const-string/jumbo v18, "LEAD_GEN"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    :cond_11
    new-instance v15, Lcom/twitter/library/api/Promotion;

    invoke-direct {v15, v9, v3}, Lcom/twitter/library/api/Promotion;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    :pswitch_7
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_7

    :cond_12
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    :pswitch_8
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "site_user"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_13

    const/4 v2, 0x1

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v16

    goto/16 :goto_1

    :cond_13
    const-string/jumbo v3, "author_user"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_14

    const/4 v2, 0x1

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v17

    goto/16 :goto_1

    :cond_14
    const-string/jumbo v3, "images"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    :goto_a
    if-eqz v2, :cond_0

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_0

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_3

    :goto_b
    :pswitch_9
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_a

    :pswitch_a
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_b

    :pswitch_b
    const-string/jumbo v2, "mobile"

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_18

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    move-object v9, v2

    move v3, v10

    move v2, v11

    move-object/from16 v20, v8

    move-object v8, v7

    move-object/from16 v7, v20

    :goto_c
    if-eqz v9, :cond_1f

    sget-object v10, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v9, v10, :cond_1f

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v10

    sget-object v11, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v9}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v9

    aget v9, v11, v9

    packed-switch v9, :pswitch_data_4

    :cond_15
    :goto_d
    :pswitch_c
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v9

    goto :goto_c

    :pswitch_d
    const-string/jumbo v9, "width"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_16

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v3

    goto :goto_d

    :cond_16
    const-string/jumbo v9, "height"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_15

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v2

    goto :goto_d

    :pswitch_e
    const-string/jumbo v9, "image_url"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_17

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v8

    goto :goto_d

    :cond_17
    const-string/jumbo v9, "image_url_2x"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_15

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v7

    goto :goto_d

    :pswitch_f
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_d

    :cond_18
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_b

    :cond_19
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    :pswitch_10
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "url"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1a

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_1

    :cond_1a
    const-string/jumbo v3, "title"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1b

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    :cond_1b
    const-string/jumbo v3, "description"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :cond_1c
    packed-switch p1, :pswitch_data_5

    :cond_1d
    new-instance v2, Lcom/twitter/library/api/TweetClassicCard;

    const/4 v9, 0x0

    move/from16 v3, p1

    invoke-direct/range {v2 .. v17}, Lcom/twitter/library/api/TweetClassicCard;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Lcom/twitter/library/api/Promotion;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/api/TwitterUser;)V

    :goto_e
    return-object v2

    :pswitch_11
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1d

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1d

    const/4 v2, 0x0

    goto :goto_e

    :pswitch_12
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1d

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1d

    const/4 v2, 0x0

    goto :goto_e

    :pswitch_13
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1d

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1d

    const/4 v2, 0x0

    goto :goto_e

    :pswitch_14
    if-eqz v15, :cond_1e

    iget-object v2, v15, Lcom/twitter/library/api/Promotion;->targetCta:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1e

    iget-object v2, v15, Lcom/twitter/library/api/Promotion;->targetUrl:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1d

    :cond_1e
    const/4 v2, 0x0

    goto :goto_e

    :cond_1f
    move v11, v2

    move v10, v3

    move-object/from16 v20, v7

    move-object v7, v8

    move-object/from16 v8, v20

    goto/16 :goto_b

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_10
        :pswitch_0
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_6
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_9
        :pswitch_b
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_f
        :pswitch_e
        :pswitch_c
        :pswitch_f
        :pswitch_d
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x1
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
    .end packed-switch
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;)Lcom/twitter/library/api/TwitterSocialProof;
    .locals 10

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v4, 0x0

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move v3, v4

    move-object v2, v7

    :cond_0
    :goto_0
    if-eqz v0, :cond_6

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_6

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v6

    aget v1, v1, v6

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_0

    :pswitch_2
    const-string/jumbo v1, "type"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto :goto_0

    :pswitch_3
    const-string/jumbo v1, "users"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move v1, v3

    :goto_1
    if-eqz v0, :cond_b

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v3, :cond_b

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_1

    :cond_1
    :pswitch_4
    move v0, v1

    :goto_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    goto :goto_1

    :pswitch_5
    const-string/jumbo v0, "count"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_2

    :pswitch_6
    const-string/jumbo v0, "ids"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_3
    if-eqz v0, :cond_1

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v3, :cond_1

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->h:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v3, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    if-eqz v0, :cond_2

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_3

    :cond_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_4

    :cond_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move v0, v1

    goto :goto_2

    :pswitch_7
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move v0, v1

    goto :goto_2

    :cond_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_0

    :cond_6
    const-string/jumbo v0, "favorite"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v1, 0x11

    move-object v2, v7

    :goto_5
    new-instance v0, Lcom/twitter/library/api/TwitterSocialProof;

    move v5, v4

    move v6, v4

    move v8, v4

    invoke-direct/range {v0 .. v8}, Lcom/twitter/library/api/TwitterSocialProof;-><init>(ILjava/lang/String;IIIILjava/lang/String;I)V

    return-object v0

    :cond_7
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    sub-int/2addr v3, v1

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v0}, Lcom/twitter/library/api/TwitterUser;->c()Ljava/lang/String;

    move-result-object v2

    if-le v1, v8, :cond_8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v0}, Lcom/twitter/library/api/TwitterUser;->c()Ljava/lang/String;

    move-result-object v7

    :cond_8
    if-nez v3, :cond_9

    const/16 v0, 0x10

    :goto_6
    move v1, v0

    goto :goto_5

    :cond_9
    const/16 v0, 0x21

    goto :goto_6

    :cond_a
    const/4 v1, -0x1

    move-object v2, v7

    goto :goto_5

    :cond_b
    move v3, v1

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_7
    .end packed-switch
.end method

.method public static a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/TwitterStatus;
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    :try_start_0
    invoke-static {p0, v1, v2}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterStatus;
    :try_end_0
    .catch Lcom/twitter/library/util/NullUserException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/api/au;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterStatus;
    .locals 68

    const-wide/16 v4, -0x1

    const/16 v23, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v38, 0x0

    const-wide/16 v11, -0x1

    const-wide/16 v13, -0x1

    const/4 v15, 0x0

    const/16 v18, 0x0

    const/16 v20, 0x0

    const/16 v19, 0x0

    const-wide/16 v36, -0x1

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v21, 0x0

    const/16 v35, 0x0

    const/16 v22, 0x0

    const/16 v31, 0x0

    const/16 v32, -0x1

    const/16 v25, 0x0

    const/4 v10, 0x0

    const/16 v26, 0x0

    const/4 v7, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v34, 0x0

    const/16 v33, -0x1

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    move-object/from16 v28, p2

    :goto_0
    if-eqz v3, :cond_1f

    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v3, v6, :cond_1f

    sget-object v6, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v3}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v3

    aget v3, v6, v3

    packed-switch v3, :pswitch_data_0

    :cond_0
    :pswitch_0
    move-object v3, v7

    move-object/from16 v6, v38

    move-object/from16 v7, v23

    move-wide/from16 v23, v4

    move-object/from16 v5, v35

    move-object v4, v10

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v10

    move-object/from16 v35, v5

    move-object/from16 v38, v6

    move-object/from16 v67, v4

    move-wide/from16 v4, v23

    move-object/from16 v23, v7

    move-object v7, v3

    move-object v3, v10

    move-object/from16 v10, v67

    goto :goto_0

    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v6, "created_at"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v6, v38

    move-wide/from16 v23, v4

    move-object/from16 v5, v35

    move-object v4, v10

    move-object/from16 v67, v3

    move-object v3, v7

    move-object/from16 v7, v67

    goto :goto_1

    :cond_1
    const-string/jumbo v6, "source"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/library/api/ap;->e(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v6

    iget-object v3, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    iget-object v6, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Ljava/lang/String;

    move-object v9, v6

    move-object v8, v3

    move-object/from16 v6, v38

    move-object v3, v7

    move-object/from16 v7, v23

    move-wide/from16 v23, v4

    move-object/from16 v5, v35

    move-object v4, v10

    goto :goto_1

    :cond_2
    const-string/jumbo v6, "text"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v3

    move-object v6, v3

    move-object v3, v7

    move-object/from16 v7, v23

    move-wide/from16 v23, v4

    move-object/from16 v5, v35

    move-object v4, v10

    goto :goto_1

    :cond_3
    const-string/jumbo v6, "retweet_count"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    const-string/jumbo v3, "100+"

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v22, 0x64

    move-object v3, v7

    move-object/from16 v6, v38

    move-object/from16 v7, v23

    move-wide/from16 v23, v4

    move-object/from16 v5, v35

    move-object v4, v10

    goto/16 :goto_1

    :cond_4
    const-string/jumbo v6, "lang"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v34

    move-object v3, v7

    move-object/from16 v6, v38

    move-object/from16 v7, v23

    move-wide/from16 v23, v4

    move-object/from16 v5, v35

    move-object v4, v10

    goto/16 :goto_1

    :pswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v6, "id"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v4

    move-object v3, v7

    move-object/from16 v6, v38

    move-object/from16 v7, v23

    move-wide/from16 v23, v4

    move-object/from16 v5, v35

    move-object v4, v10

    goto/16 :goto_1

    :cond_5
    const-string/jumbo v6, "in_reply_to_user_id"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v11

    move-object v3, v7

    move-object/from16 v6, v38

    move-object/from16 v7, v23

    move-wide/from16 v23, v4

    move-object/from16 v5, v35

    move-object v4, v10

    goto/16 :goto_1

    :cond_6
    const-string/jumbo v6, "in_reply_to_status_id"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v13

    move-object v3, v7

    move-object/from16 v6, v38

    move-object/from16 v7, v23

    move-wide/from16 v23, v4

    move-object/from16 v5, v35

    move-object v4, v10

    goto/16 :goto_1

    :cond_7
    const-string/jumbo v6, "retweet_count"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v22

    move-object v3, v7

    move-object/from16 v6, v38

    move-object/from16 v7, v23

    move-wide/from16 v23, v4

    move-object/from16 v5, v35

    move-object v4, v10

    goto/16 :goto_1

    :cond_8
    const-string/jumbo v6, "favorite_count"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v31

    move-object v3, v7

    move-object/from16 v6, v38

    move-object/from16 v7, v23

    move-wide/from16 v23, v4

    move-object/from16 v5, v35

    move-object v4, v10

    goto/16 :goto_1

    :cond_9
    const-string/jumbo v6, "descendent_reply_count"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v32

    move-object v3, v7

    move-object/from16 v6, v38

    move-object/from16 v7, v23

    move-wide/from16 v23, v4

    move-object/from16 v5, v35

    move-object v4, v10

    goto/16 :goto_1

    :cond_a
    const-string/jumbo v6, "reply_count"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    if-gez v32, :cond_b

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v32

    move-object v3, v7

    move-object/from16 v6, v38

    move-object/from16 v7, v23

    move-wide/from16 v23, v4

    move-object/from16 v5, v35

    move-object v4, v10

    goto/16 :goto_1

    :cond_b
    const-string/jumbo v6, "view_count"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v33

    move-object v3, v7

    move-object/from16 v6, v38

    move-object/from16 v7, v23

    move-wide/from16 v23, v4

    move-object/from16 v5, v35

    move-object v4, v10

    goto/16 :goto_1

    :pswitch_3
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v6, "favorited"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->k()Z

    move-result v15

    move-object v3, v7

    move-object/from16 v6, v38

    move-object/from16 v7, v23

    move-wide/from16 v23, v4

    move-object/from16 v5, v35

    move-object v4, v10

    goto/16 :goto_1

    :cond_c
    const-string/jumbo v6, "possibly_sensitive"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->k()Z

    move-result v29

    move-object v3, v7

    move-object/from16 v6, v38

    move-object/from16 v7, v23

    move-wide/from16 v23, v4

    move-object/from16 v5, v35

    move-object v4, v10

    goto/16 :goto_1

    :cond_d
    const-string/jumbo v6, "is_emergency"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->k()Z

    move-result v30

    move-object v3, v7

    move-object/from16 v6, v38

    move-object/from16 v7, v23

    move-wide/from16 v23, v4

    move-object/from16 v5, v35

    move-object v4, v10

    goto/16 :goto_1

    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v6, "current_user_retweet"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    :goto_2
    if-eqz v3, :cond_0

    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v3, v6, :cond_0

    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->i:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v3, v6, :cond_e

    const-string/jumbo v3, "id"

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v36

    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    goto :goto_2

    :cond_f
    const-string/jumbo v6, "user"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-static {v0, v1, v3}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v20

    move-object v3, v7

    move-object/from16 v6, v38

    move-object/from16 v7, v23

    move-wide/from16 v23, v4

    move-object/from16 v5, v35

    move-object v4, v10

    goto/16 :goto_1

    :cond_10
    const-string/jumbo v6, "coordinates"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_14

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    const/4 v6, 0x0

    :goto_3
    if-eqz v3, :cond_13

    sget-object v24, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    move-object/from16 v0, v24

    if-eq v3, v0, :cond_13

    sget-object v24, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v3}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v3

    aget v3, v24, v3

    packed-switch v3, :pswitch_data_1

    :pswitch_5
    move-object v3, v6

    :goto_4
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v6

    move-object/from16 v67, v3

    move-object v3, v6

    move-object/from16 v6, v67

    goto :goto_3

    :pswitch_6
    const-string/jumbo v3, "coordinates"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/ap;->an(Lcom/fasterxml/jackson/core/JsonParser;)Landroid/util/Pair;

    move-result-object v24

    if-eqz v24, :cond_11

    move-object/from16 v0, v24

    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v24

    iget-object v3, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->toString()Ljava/lang/String;

    move-result-object v17

    :cond_11
    move-object v3, v6

    goto :goto_4

    :cond_12
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v3, v6

    goto :goto_4

    :pswitch_7
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v3

    goto :goto_4

    :pswitch_8
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v3, v6

    goto :goto_4

    :cond_13
    move-object v3, v7

    move-object/from16 v6, v38

    move-object/from16 v7, v23

    move-wide/from16 v23, v4

    move-object/from16 v5, v35

    move-object v4, v10

    goto/16 :goto_1

    :cond_14
    const-string/jumbo v6, "place"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_15

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/ap;->am(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/geo/TwitterPlace;

    move-result-object v18

    move-object v3, v7

    move-object/from16 v6, v38

    move-object/from16 v7, v23

    move-wide/from16 v23, v4

    move-object/from16 v5, v35

    move-object v4, v10

    goto/16 :goto_1

    :cond_15
    const-string/jumbo v6, "retweeted_status"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_17

    :try_start_0
    move-object/from16 v0, p0

    move/from16 v1, p3

    move-object/from16 v2, p4

    invoke-static {v0, v1, v2}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterStatus;
    :try_end_0
    .catch Lcom/twitter/library/util/NullUserException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v19

    move-object v3, v7

    move-object/from16 v6, v38

    move-object/from16 v7, v23

    move-wide/from16 v23, v4

    move-object/from16 v5, v35

    move-object v4, v10

    goto/16 :goto_1

    :catch_0
    move-exception v3

    if-eqz p4, :cond_16

    const-string/jumbo v6, "Received null user for status = %d"

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v27, 0x0

    iget-wide v0, v3, Lcom/twitter/library/util/NullUserException;->statusId:J

    move-wide/from16 v39, v0

    invoke-static/range {v39 .. v40}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v24, v27

    move-object/from16 v0, v24

    invoke-static {v6, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-interface {v0, v3}, Lcom/twitter/library/util/w;->a(Ljava/lang/String;)V

    :cond_16
    move-object v3, v7

    move-object/from16 v6, v38

    move-object/from16 v7, v23

    move-wide/from16 v23, v4

    move-object/from16 v5, v35

    move-object v4, v10

    goto/16 :goto_1

    :cond_17
    const-string/jumbo v6, "entities"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_18

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/TweetEntities;->a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/TweetEntities;

    move-result-object v21

    move-object v3, v7

    move-object/from16 v6, v38

    move-object/from16 v7, v23

    move-wide/from16 v23, v4

    move-object/from16 v5, v35

    move-object v4, v10

    goto/16 :goto_1

    :cond_18
    const-string/jumbo v6, "extended_entities"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_19

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/TweetEntities;->a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/TweetEntities;

    move-result-object v3

    move-object/from16 v6, v38

    move-object/from16 v67, v3

    move-object v3, v7

    move-object/from16 v7, v23

    move-wide/from16 v23, v4

    move-object v4, v10

    move-object/from16 v5, v67

    goto/16 :goto_1

    :cond_19
    if-nez v28, :cond_1a

    const-string/jumbo v6, "metadata"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1a

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/twitter/library/api/ap;->f(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/au;

    move-result-object v28

    move-object v3, v7

    move-object/from16 v6, v38

    move-object/from16 v7, v23

    move-wide/from16 v23, v4

    move-object/from16 v5, v35

    move-object v4, v10

    goto/16 :goto_1

    :cond_1a
    const-string/jumbo v6, "promoted_content"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1b

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/twitter/library/api/PromotedContent;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/PromotedContent;

    move-result-object v25

    move-object v3, v7

    move-object/from16 v6, v38

    move-object/from16 v7, v23

    move-wide/from16 v23, v4

    move-object/from16 v5, v35

    move-object v4, v10

    goto/16 :goto_1

    :cond_1b
    const-string/jumbo v6, "cards"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1c

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/ap;->ag(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v3

    move-object/from16 v6, v38

    move-object/from16 v67, v3

    move-object v3, v7

    move-object/from16 v7, v23

    move-wide/from16 v23, v4

    move-object/from16 v5, v35

    move-object/from16 v4, v67

    goto/16 :goto_1

    :cond_1c
    const-string/jumbo v6, "card"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1d

    new-instance v3, Lcom/twitter/library/api/TwitterStatusCard;

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/twitter/library/card/instance/a;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/card/instance/CardInstanceData;

    move-result-object v6

    invoke-direct {v3, v6}, Lcom/twitter/library/api/TwitterStatusCard;-><init>(Lcom/twitter/library/card/instance/CardInstanceData;)V

    move-object/from16 v6, v38

    move-object/from16 v7, v23

    move-wide/from16 v23, v4

    move-object/from16 v5, v35

    move-object v4, v10

    goto/16 :goto_1

    :cond_1d
    const-string/jumbo v6, "recommended_content"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1e

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/twitter/library/api/RecommendedContent;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/RecommendedContent;

    move-result-object v26

    move-object v3, v7

    move-object/from16 v6, v38

    move-object/from16 v7, v23

    move-wide/from16 v23, v4

    move-object/from16 v5, v35

    move-object v4, v10

    goto/16 :goto_1

    :cond_1e
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v3, v7

    move-object/from16 v6, v38

    move-object/from16 v7, v23

    move-wide/from16 v23, v4

    move-object/from16 v5, v35

    move-object v4, v10

    goto/16 :goto_1

    :cond_1f
    if-nez v20, :cond_20

    new-instance v3, Lcom/twitter/library/util/NullUserException;

    invoke-direct {v3, v4, v5}, Lcom/twitter/library/util/NullUserException;-><init>(J)V

    throw v3

    :cond_20
    if-nez v7, :cond_29

    if-eqz v19, :cond_29

    move-object/from16 v0, v19

    iget-object v7, v0, Lcom/twitter/library/api/TwitterStatus;->m:Lcom/twitter/library/api/TwitterStatusCard;

    move-object/from16 v27, v7

    :goto_5
    if-nez v27, :cond_21

    if-eqz v10, :cond_21

    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_21

    new-instance v27, Lcom/twitter/library/api/TwitterStatusCard;

    const/4 v3, 0x0

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/TweetClassicCard;

    move-object/from16 v0, v27

    invoke-direct {v0, v3}, Lcom/twitter/library/api/TwitterStatusCard;-><init>(Lcom/twitter/library/api/TweetClassicCard;)V

    :cond_21
    if-eqz v25, :cond_26

    invoke-virtual/range {v25 .. v25}, Lcom/twitter/library/api/PromotedContent;->b()Z

    move-result v3

    if-nez v3, :cond_26

    const/4 v3, 0x1

    :goto_6
    sget-object v6, Lcom/twitter/internal/util/k;->b:Ljava/text/SimpleDateFormat;

    move-object/from16 v0, v23

    invoke-static {v6, v0}, Lcom/twitter/library/util/Util;->a(Ljava/text/SimpleDateFormat;Ljava/lang/String;)J

    move-result-wide v6

    if-eqz v28, :cond_22

    const-string/jumbo v10, "popular"

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/twitter/library/api/au;->e:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_23

    :cond_22
    if-eqz v3, :cond_27

    :cond_23
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v23

    :goto_7
    if-eqz v35, :cond_24

    move-object/from16 v0, v35

    iget-object v3, v0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    if-eqz v3, :cond_24

    move-object/from16 v0, v35

    iget-object v3, v0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    move-object/from16 v0, v21

    iput-object v3, v0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    :cond_24
    move-object/from16 v0, v38

    move-object/from16 v1, v21

    move-object/from16 v2, v28

    invoke-static {v0, v1, v2}, Lcom/twitter/library/api/ap;->a(Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/au;)Ljava/lang/String;

    move-result-object v10

    if-eqz v21, :cond_25

    if-eqz v28, :cond_25

    move-object/from16 v0, v28

    iget-object v3, v0, Lcom/twitter/library/api/au;->a:Ljava/util/ArrayList;

    if-eqz v3, :cond_25

    move-object/from16 v0, v28

    iget-object v3, v0, Lcom/twitter/library/api/au;->a:Ljava/util/ArrayList;

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    invoke-static {v3, v0}, Lcom/twitter/library/api/ap;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    :cond_25
    const-wide/16 v38, -0x1

    cmp-long v3, v36, v38

    if-eqz v3, :cond_28

    if-nez v19, :cond_28

    if-eqz p1, :cond_28

    new-instance v3, Lcom/twitter/library/api/TwitterStatus;

    const/16 v19, 0x0

    invoke-direct/range {v3 .. v34}, Lcom/twitter/library/api/TwitterStatus;-><init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJZLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/api/geo/TwitterPlace;Lcom/twitter/library/api/TwitterStatus;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/api/TweetEntities;IJLcom/twitter/library/api/PromotedContent;Lcom/twitter/library/api/RecommendedContent;Lcom/twitter/library/api/TwitterStatusCard;Lcom/twitter/library/api/au;ZZIIILjava/lang/String;)V

    new-instance v35, Lcom/twitter/library/api/TwitterStatus;

    move-wide/from16 v38, v6

    move-object/from16 v40, v8

    move-object/from16 v41, v9

    move-object/from16 v42, v10

    move-wide/from16 v43, v11

    move-wide/from16 v45, v13

    move/from16 v47, v15

    move-object/from16 v48, v16

    move-object/from16 v49, v17

    move-object/from16 v50, v18

    move-object/from16 v51, v3

    move-object/from16 v52, p1

    move-object/from16 v53, v21

    move/from16 v54, v22

    move-wide/from16 v55, v23

    move-object/from16 v57, v25

    move-object/from16 v58, v26

    move-object/from16 v59, v27

    move-object/from16 v60, v28

    move/from16 v61, v29

    move/from16 v62, v30

    move/from16 v63, v31

    move/from16 v64, v32

    move/from16 v65, v33

    move-object/from16 v66, v34

    invoke-direct/range {v35 .. v66}, Lcom/twitter/library/api/TwitterStatus;-><init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJZLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/api/geo/TwitterPlace;Lcom/twitter/library/api/TwitterStatus;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/api/TweetEntities;IJLcom/twitter/library/api/PromotedContent;Lcom/twitter/library/api/RecommendedContent;Lcom/twitter/library/api/TwitterStatusCard;Lcom/twitter/library/api/au;ZZIIILjava/lang/String;)V

    :goto_8
    return-object v35

    :cond_26
    const/4 v3, 0x0

    goto/16 :goto_6

    :cond_27
    move-wide/from16 v23, v6

    goto/16 :goto_7

    :cond_28
    new-instance v3, Lcom/twitter/library/api/TwitterStatus;

    invoke-direct/range {v3 .. v34}, Lcom/twitter/library/api/TwitterStatus;-><init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJZLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/api/geo/TwitterPlace;Lcom/twitter/library/api/TwitterStatus;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/api/TweetEntities;IJLcom/twitter/library/api/PromotedContent;Lcom/twitter/library/api/RecommendedContent;Lcom/twitter/library/api/TwitterStatusCard;Lcom/twitter/library/api/au;ZZIIILjava/lang/String;)V

    move-object/from16 v35, v3

    goto :goto_8

    :cond_29
    move-object/from16 v27, v7

    goto/16 :goto_5

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private static a(Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterStatus;
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterStatus;

    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/twitter/library/api/TwitterStatus;->t:Lcom/twitter/library/api/TwitterUser;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/twitter/library/api/TwitterStatus;->t:Lcom/twitter/library/api/TwitterUser;

    iget-wide v3, v1, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/TwitterUser;

    if-eqz v1, :cond_2

    iput-object v1, v0, Lcom/twitter/library/api/TwitterStatus;->t:Lcom/twitter/library/api/TwitterUser;

    :cond_0
    iget-object v1, v0, Lcom/twitter/library/api/TwitterStatus;->i:Lcom/twitter/library/api/TwitterStatus;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/twitter/library/api/TwitterStatus;->i:Lcom/twitter/library/api/TwitterStatus;

    iget-object v1, v1, Lcom/twitter/library/api/TwitterStatus;->t:Lcom/twitter/library/api/TwitterUser;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/twitter/library/api/TwitterStatus;->i:Lcom/twitter/library/api/TwitterStatus;

    iget-object v1, v1, Lcom/twitter/library/api/TwitterStatus;->t:Lcom/twitter/library/api/TwitterUser;

    iget-wide v3, v1, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/TwitterUser;

    if-eqz v1, :cond_3

    iget-object v2, v0, Lcom/twitter/library/api/TwitterStatus;->i:Lcom/twitter/library/api/TwitterStatus;

    iput-object v1, v2, Lcom/twitter/library/api/TwitterStatus;->t:Lcom/twitter/library/api/TwitterUser;

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    if-eqz p3, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Missing user "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatus;->t:Lcom/twitter/library/api/TwitterUser;

    iget-wide v3, v0, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p3, v0}, Lcom/twitter/library/util/w;->a(Ljava/lang/String;)V

    move-object v0, v2

    goto :goto_0

    :cond_3
    if-eqz p3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Missing original user "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatus;->i:Lcom/twitter/library/api/TwitterStatus;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatus;->t:Lcom/twitter/library/api/TwitterUser;

    iget-wide v3, v0, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p3, v0}, Lcom/twitter/library/util/w;->a(Ljava/lang/String;)V

    move-object v0, v2

    goto :goto_0
.end method

.method public static a(Lcom/fasterxml/jackson/core/JsonParser;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterUser;
    .locals 40

    const/16 v28, 0x0

    const/4 v4, 0x0

    const-wide/16 v26, -0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v12, 0x0

    const/16 v25, 0x0

    const/4 v10, 0x0

    const/16 v19, 0x0

    const/16 v22, 0x0

    const/16 v30, 0x0

    const/4 v11, 0x0

    const/4 v9, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v23, 0x0

    const/16 v18, 0x0

    const-wide/16 v20, 0x0

    const/16 v31, 0x0

    const/16 v24, 0x80

    const/4 v3, 0x0

    const/16 v33, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    move-object/from16 v37, v2

    move-object v2, v3

    move-wide/from16 v38, v26

    move-object/from16 v27, v4

    move-object/from16 v26, v28

    move-wide/from16 v3, v38

    move-object/from16 v28, v37

    :goto_0
    if-eqz v28, :cond_28

    sget-object v29, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    if-eq v0, v1, :cond_28

    sget-object v29, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual/range {v28 .. v28}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v28

    aget v28, v29, v28

    packed-switch v28, :pswitch_data_0

    :cond_0
    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v28

    move-object/from16 v37, v3

    move-object/from16 v38, v4

    move-wide/from16 v3, v25

    move-object/from16 v25, v5

    move-object/from16 v26, v11

    move-object v5, v9

    move-object/from16 v11, v38

    move-object/from16 v9, v37

    goto :goto_0

    :pswitch_0
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v28

    const-string/jumbo v29, "name"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto :goto_1

    :cond_1
    const-string/jumbo v29, "screen_name"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto :goto_1

    :cond_2
    const-string/jumbo v29, "profile_image_url_https"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto :goto_1

    :cond_3
    const-string/jumbo v29, "profile_banner_url"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_4
    const-string/jumbo v29, "description"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_5
    const-string/jumbo v29, "url_https"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_6
    const-string/jumbo v29, "url"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_7
    const-string/jumbo v29, "location"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_8
    const-string/jumbo v29, "created_at"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v20

    :try_start_0
    invoke-static/range {v20 .. v20}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v20

    :goto_2
    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :catch_0
    move-exception v21

    sget-object v21, Lcom/twitter/internal/util/k;->b:Ljava/text/SimpleDateFormat;

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Lcom/twitter/library/util/Util;->a(Ljava/text/SimpleDateFormat;Ljava/lang/String;)J

    move-result-wide v20

    goto :goto_2

    :cond_9
    const-string/jumbo v29, "profile_background_color"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_0

    const/high16 v28, -0x1000000

    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v29

    const/16 v32, 0x10

    move-object/from16 v0, v29

    move/from16 v1, v32

    invoke-static {v0, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v12

    or-int v12, v12, v28

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :catch_1
    move-exception v28

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v28

    const-string/jumbo v29, "id"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_a

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v3

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_a
    const-string/jumbo v29, "followers_count"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_b

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v10

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_b
    const-string/jumbo v29, "friends_count"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_c

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v19

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_c
    const-string/jumbo v29, "statuses_count"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_d

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v22

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_d
    const-string/jumbo v29, "favourites_count"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v30

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :pswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v28

    const-string/jumbo v29, "protected"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_e

    const/4 v14, 0x0

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_e
    const-string/jumbo v29, "geo_enabled"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_f

    const/16 v23, 0x0

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_f
    const-string/jumbo v29, "following"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_10

    const/16 v28, 0x1

    move/from16 v0, v24

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->b(II)I

    move-result v24

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_10
    const-string/jumbo v29, "followed_by"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_11

    const/16 v28, 0x2

    move/from16 v0, v24

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->b(II)I

    move-result v24

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_11
    const-string/jumbo v29, "follow_request_sent"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_0

    const/16 v28, 0x4000

    move/from16 v0, v24

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->b(II)I

    move-result v24

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :pswitch_3
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v28

    const-string/jumbo v29, "protected"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_12

    const/4 v14, 0x1

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_12
    const-string/jumbo v29, "verified"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_13

    const/4 v15, 0x1

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_13
    const-string/jumbo v29, "is_translator"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_14

    const/16 v16, 0x1

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_14
    const-string/jumbo v29, "is_lifeline_institution"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_15

    const/16 v17, 0x1

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_15
    const-string/jumbo v29, "suspended"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_16

    const/4 v13, 0x1

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_16
    const-string/jumbo v29, "geo_enabled"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_17

    const/16 v23, 0x1

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_17
    const-string/jumbo v29, "following"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_18

    const/16 v28, 0x1

    move/from16 v0, v24

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v24

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_18
    const-string/jumbo v29, "follow_request_sent"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_19

    const/16 v28, 0x4000

    move/from16 v0, v24

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v24

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_19
    const-string/jumbo v29, "followed_by"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_1a

    const/16 v28, 0x2

    move/from16 v0, v24

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v24

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_1a
    const-string/jumbo v29, "blocking"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_1b

    const/16 v28, 0x4

    move/from16 v0, v24

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v24

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_1b
    const-string/jumbo v29, "can_dm"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_1c

    const/16 v28, 0x8

    move/from16 v0, v24

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v24

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_1c
    const-string/jumbo v29, "notifications"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_1d

    const/16 v28, 0x10

    move/from16 v0, v24

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v24

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_1d
    const-string/jumbo v29, "lifeline_following"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_1e

    const/16 v28, 0x100

    move/from16 v0, v24

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v24

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_1e
    const-string/jumbo v29, "want_retweets"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_1f

    const/16 v28, 0x200

    move/from16 v0, v24

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v24

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_1f
    const-string/jumbo v29, "favorite_following"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_20

    const/16 v28, 0x800

    move/from16 v0, v24

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v24

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_20
    const-string/jumbo v29, "email_following"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_21

    const/16 v28, 0x1000

    move/from16 v0, v24

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v24

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_21
    const-string/jumbo v29, "needs_phone_verification"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_22

    const/16 v35, 0x1

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_22
    const-string/jumbo v29, "can_media_tag"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_23

    const/16 v28, 0x400

    move/from16 v0, v24

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v24

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_23
    const-string/jumbo v29, "muting"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_24

    const/16 v28, 0x2000

    move/from16 v0, v24

    move/from16 v1, v28

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v24

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_24
    const-string/jumbo v29, "has_custom_timelines"

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_0

    const/16 v36, 0x1

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :pswitch_4
    const-string/jumbo v28, "status"

    move-object/from16 v0, v28

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_25

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/TwitterStatus;

    move-result-object v27

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_25
    const-string/jumbo v28, "promoted_content"

    move-object/from16 v0, v28

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_26

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Lcom/twitter/library/api/PromotedContent;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/PromotedContent;

    move-result-object v31

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_26
    const-string/jumbo v28, "entities"

    move-object/from16 v0, v28

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_27

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/ap;->ae(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/ax;

    move-result-object v2

    iget-object v0, v2, Lcom/twitter/library/api/ax;->b:Lcom/twitter/library/api/TweetEntities;

    move-object/from16 v33, v0

    iget-object v2, v2, Lcom/twitter/library/api/ax;->a:Lcom/twitter/library/api/TweetEntities;

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_27
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :pswitch_5
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :pswitch_6
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v37, v9

    move-object v9, v5

    move-object/from16 v5, v25

    move-object/from16 v38, v11

    move-object/from16 v11, v26

    move-wide/from16 v25, v3

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    goto/16 :goto_1

    :cond_28
    if-nez v11, :cond_29

    move-object v11, v9

    :cond_29
    const-wide/16 v28, 0x0

    cmp-long v9, v3, v28

    if-lez v9, :cond_2a

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2b

    :cond_2a
    if-nez p1, :cond_2e

    :cond_2b
    invoke-static/range {v25 .. v25}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_30

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    sget-object v26, Lcom/twitter/library/util/x;->j:Ljava/util/regex/Pattern;

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v26

    :goto_3
    invoke-virtual/range {v26 .. v26}, Ljava/util/regex/Matcher;->find()Z

    move-result v28

    if-eqz v28, :cond_2c

    new-instance v28, Lcom/twitter/library/api/MentionEntity;

    invoke-direct/range {v28 .. v28}, Lcom/twitter/library/api/MentionEntity;-><init>()V

    invoke-virtual/range {v26 .. v26}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    iput-object v0, v1, Lcom/twitter/library/api/MentionEntity;->screenName:Ljava/lang/String;

    invoke-virtual/range {v26 .. v26}, Ljava/util/regex/Matcher;->start()I

    move-result v29

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput v0, v1, Lcom/twitter/library/api/MentionEntity;->start:I

    invoke-virtual/range {v26 .. v26}, Ljava/util/regex/Matcher;->end()I

    move-result v29

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput v0, v1, Lcom/twitter/library/api/MentionEntity;->end:I

    move-object/from16 v0, v28

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_2c
    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v26

    if-nez v26, :cond_30

    if-nez v2, :cond_2d

    new-instance v2, Lcom/twitter/library/api/TweetEntities;

    invoke-direct {v2}, Lcom/twitter/library/api/TweetEntities;-><init>()V

    :cond_2d
    iput-object v9, v2, Lcom/twitter/library/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    move-object/from16 v32, v2

    :goto_4
    const/4 v2, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, v32

    invoke-static {v0, v1, v2}, Lcom/twitter/library/api/ap;->a(Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/au;)Ljava/lang/String;

    move-result-object v9

    new-instance v2, Lcom/twitter/library/api/TwitterUser;

    const-wide/16 v25, 0x0

    const-wide/16 v28, 0x0

    const/16 v34, 0x0

    invoke-direct/range {v2 .. v36}, Lcom/twitter/library/api/TwitterUser;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IZZZZZLjava/lang/String;IJIZIJLcom/twitter/library/api/TwitterStatus;JILcom/twitter/library/api/PromotedContent;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/TwitterUserMetadata;ZZ)V

    :goto_5
    return-object v2

    :cond_2e
    if-eqz p2, :cond_2f

    const-string/jumbo v2, "Received null user."

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Lcom/twitter/library/util/w;->a(Ljava/lang/String;)V

    :cond_2f
    const/4 v2, 0x0

    goto :goto_5

    :cond_30
    move-object/from16 v32, v2

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Lcom/fasterxml/jackson/core/JsonParser;ILcom/twitter/library/util/w;)Lcom/twitter/library/api/aj;
    .locals 7

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    move-object v0, v3

    move-object v1, v3

    move-object v2, v3

    :goto_0
    if-eqz v4, :cond_2

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v5, :cond_2

    sget-object v5, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v4}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    goto :goto_0

    :pswitch_0
    const-string/jumbo v5, "lists"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-static {p0, p1, p2}, Lcom/twitter/library/api/ap;->c(Lcom/fasterxml/jackson/core/JsonParser;ILcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_1

    :cond_1
    :goto_2
    if-eqz v4, :cond_0

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v5, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    goto :goto_2

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "next_cursor_str"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    if-nez v1, :cond_3

    move-object v0, v3

    :goto_3
    return-object v0

    :cond_3
    new-instance v0, Lcom/twitter/library/api/aj;

    invoke-direct {v0, v2, v1}, Lcom/twitter/library/api/aj;-><init>(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/aj;
    .locals 7

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    move-object v0, v3

    move-object v1, v3

    move-object v2, v3

    :goto_0
    if-eqz v4, :cond_2

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v5, :cond_2

    sget-object v5, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v4}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    goto :goto_0

    :pswitch_0
    const-string/jumbo v5, "users"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-static {p0, p1}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_1

    :cond_1
    :goto_2
    if-eqz v4, :cond_0

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v5, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    goto :goto_2

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "next_cursor_str"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    if-nez v1, :cond_3

    move-object v0, v3

    :goto_3
    return-object v0

    :cond_3
    new-instance v0, Lcom/twitter/library/api/aj;

    invoke-direct {v0, v2, v1}, Lcom/twitter/library/api/aj;-><init>(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;Lcom/twitter/library/api/TwitterUser;)Lcom/twitter/library/api/av;
    .locals 8

    const/4 v1, 0x0

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v5, v0

    move-object v4, v1

    move-object v2, v1

    move-object v0, v1

    :goto_0
    if-eqz v5, :cond_e

    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v5, v6, :cond_e

    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v5, v6, :cond_c

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "twitter_objects"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v5

    :goto_1
    if-eqz v5, :cond_d

    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v5, v6, :cond_d

    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v5, v6, :cond_5

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "tweets"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-static {p0, p2}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/TwitterUser;)Ljava/util/HashMap;

    move-result-object v1

    :cond_0
    :goto_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v5

    goto :goto_1

    :cond_1
    const-string/jumbo v6, "users"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-static {p0}, Lcom/twitter/library/api/ap;->aA(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;

    move-result-object v2

    goto :goto_2

    :cond_2
    const-string/jumbo v6, "event_summaries"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-static {v1, v2, p0}, Lcom/twitter/library/api/ap;->a(Ljava/util/HashMap;Ljava/util/HashMap;Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    goto :goto_2

    :cond_3
    const-string/jumbo v6, "custom_timelines"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-static {p0}, Lcom/twitter/library/api/ap;->aB(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    goto :goto_2

    :cond_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    :cond_5
    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v5, v6, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    :cond_6
    const-string/jumbo v6, "response"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v5

    move-object v6, v0

    move-object v0, v4

    move-object v4, v5

    :goto_3
    if-eqz v4, :cond_f

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v5, :cond_f

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v4, v5, :cond_9

    const-string/jumbo v4, "timeline"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    const/4 v4, 0x0

    move-object v0, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Ljava/util/HashMap;ZLcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v0

    :cond_7
    :goto_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    goto :goto_3

    :cond_8
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_4

    :cond_9
    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v4, v5, :cond_7

    const-string/jumbo v4, "highlight"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-static {p0}, Lcom/twitter/library/api/ap;->az(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;

    move-result-object v6

    goto :goto_4

    :cond_a
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_4

    :cond_b
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v7, v4

    move-object v4, v2

    move-object v2, v1

    move-object v1, v7

    :goto_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v5

    move-object v7, v1

    move-object v1, v2

    move-object v2, v4

    move-object v4, v7

    goto/16 :goto_0

    :cond_c
    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v5, v6, :cond_d

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    :cond_d
    move-object v7, v4

    move-object v4, v2

    move-object v2, v1

    move-object v1, v7

    goto :goto_5

    :cond_e
    new-instance v1, Lcom/twitter/library/api/av;

    invoke-direct {v1, v4, v0}, Lcom/twitter/library/api/av;-><init>(Ljava/util/ArrayList;Ljava/lang/String;)V

    return-object v1

    :cond_f
    move-object v4, v2

    move-object v2, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_5
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;JI)Lcom/twitter/library/api/search/TwitterTypeAhead;
    .locals 18

    const/4 v2, 0x0

    const/4 v12, 0x0

    const/4 v10, 0x0

    const/4 v14, 0x0

    const/4 v11, 0x0

    const/4 v13, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_4

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_4

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :pswitch_0
    move-object v1, v13

    move-object v3, v2

    move v2, v14

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    move-object v13, v1

    move v14, v2

    move-object v1, v4

    move-object v2, v3

    goto :goto_0

    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "topic"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v2

    move-object v1, v13

    move-object v3, v2

    move v2, v14

    goto :goto_1

    :cond_1
    const-string/jumbo v3, "filter"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v12

    move-object v1, v13

    move-object v3, v2

    move v2, v14

    goto :goto_1

    :cond_2
    const-string/jumbo v3, "location"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v10

    move-object v1, v13

    move-object v3, v2

    move v2, v14

    goto :goto_1

    :pswitch_2
    const-string/jumbo v1, "rounded_score"

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v1

    move-object v3, v2

    move v2, v1

    move-object v1, v13

    goto :goto_1

    :pswitch_3
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "follow"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v11, 0x1

    move-object v1, v13

    move-object v3, v2

    move v2, v14

    goto :goto_1

    :pswitch_4
    const-string/jumbo v1, "tokens"

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/ap;->av(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v1

    move-object v3, v2

    move v2, v14

    goto :goto_1

    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v1, v13

    move-object v3, v2

    move v2, v14

    goto/16 :goto_1

    :pswitch_5
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v1, v13

    move-object v3, v2

    move v2, v14

    goto/16 :goto_1

    :cond_4
    if-eqz v2, :cond_5

    if-nez v13, :cond_6

    :cond_5
    const/4 v1, 0x4

    move/from16 v0, p3

    if-eq v0, v1, :cond_6

    const/4 v1, 0x0

    :goto_2
    return-object v1

    :cond_6
    new-instance v15, Lcom/twitter/library/api/search/TwitterTypeAhead;

    const/16 v16, 0x0

    const/16 v17, 0x0

    new-instance v1, Lcom/twitter/library/api/search/TwitterSearchQuery;

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    invoke-static {}, Lcom/twitter/library/util/CollectionsUtil;->a()Ljava/util/ArrayList;

    move-result-object v9

    move-object v3, v2

    move-wide/from16 v4, p1

    invoke-direct/range {v1 .. v12}, Lcom/twitter/library/api/search/TwitterSearchQuery;-><init>(Ljava/lang/String;Ljava/lang/String;JJLcom/twitter/library/api/PromotedContent;Ljava/util/ArrayList;Ljava/lang/String;ZLjava/lang/String;)V

    const/4 v9, 0x0

    move-object v2, v15

    move/from16 v3, p3

    move v4, v14

    move/from16 v5, v16

    move-object v6, v13

    move-object/from16 v7, v17

    move-object v8, v1

    invoke-direct/range {v2 .. v9}, Lcom/twitter/library/api/search/TwitterTypeAhead;-><init>(IIILjava/util/ArrayList;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/api/search/TwitterSearchQuery;Ljava/lang/String;)V

    move-object v1, v15

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_5
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/search/f;
    .locals 10

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v3, -0x1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move v4, v7

    move-object v1, v6

    move-object v2, v6

    :goto_0
    if-eqz v0, :cond_4

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v5, :cond_4

    sget-object v5, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "modules"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0, p1, p2}, Lcom/twitter/library/api/ap;->c(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    const-string/jumbo v0, "metadata"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_0

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v5, :cond_0

    sget-object v5, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_1

    :cond_2
    :goto_3
    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_2

    :pswitch_4
    const-string/jumbo v0, "cursor"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    :pswitch_5
    const-string/jumbo v0, "refresh_interval_in_sec"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->m()I

    move-result v3

    goto :goto_3

    :pswitch_6
    const-string/jumbo v0, "has_events_response"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v4, 0x1

    goto :goto_3

    :pswitch_7
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :cond_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_4
    if-nez v1, :cond_5

    new-instance v0, Lcom/twitter/library/util/InvalidDataException;

    const-string/jumbo v1, "Search did not return results module"

    invoke-direct {v0, v1}, Lcom/twitter/library/util/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v5, v7

    :cond_6
    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/search/g;

    iget-boolean v9, v0, Lcom/twitter/library/api/search/g;->j:Z

    if-eqz v9, :cond_7

    add-int/lit8 v5, v5, 0x1

    :cond_7
    iget-boolean v9, v0, Lcom/twitter/library/api/search/g;->l:Z

    if-eqz v9, :cond_6

    iget-object v9, v0, Lcom/twitter/library/api/search/g;->k:Ljava/util/ArrayList;

    if-eqz v9, :cond_6

    iget-object v9, v0, Lcom/twitter/library/api/search/g;->k:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lez v9, :cond_6

    iget-object v0, v0, Lcom/twitter/library/api/search/g;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterTopic;

    move-object v6, v0

    goto :goto_4

    :cond_8
    new-instance v0, Lcom/twitter/library/api/search/f;

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/api/search/f;-><init>(Ljava/util/ArrayList;Ljava/lang/String;IZILcom/twitter/library/api/TwitterTopic;)V

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_4
        :pswitch_3
        :pswitch_7
        :pswitch_5
        :pswitch_3
        :pswitch_6
    .end packed-switch
.end method

.method public static a(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;
    .locals 8

    const/4 v0, 0x0

    :try_start_0
    new-instance v2, Ljava/io/StringWriter;

    const/16 v1, 0x200

    invoke-direct {v2, v1}, Ljava/io/StringWriter;-><init>(I)V

    sget-object v1, Lcom/twitter/library/api/ap;->a:Lcom/fasterxml/jackson/core/JsonFactory;

    invoke-virtual {v1, v2}, Lcom/fasterxml/jackson/core/JsonFactory;->b(Ljava/io/Writer;)Lcom/fasterxml/jackson/core/JsonGenerator;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    :try_start_1
    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->c()V

    const-string/jumbo v0, "id"

    iget-wide v3, p0, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v1, v0, v3, v4}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    const-string/jumbo v0, "screen_name"

    iget-object v3, p0, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "name"

    iget-object v3, p0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string/jumbo v0, "profile_image_url_https"

    iget-object v3, p0, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileHeaderImageUrl:Ljava/lang/String;

    if-eqz v0, :cond_2

    const-string/jumbo v0, "profile_banner_url"

    iget-object v3, p0, Lcom/twitter/library/api/TwitterUser;->profileHeaderImageUrl:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileDescription:Ljava/lang/String;

    if-eqz v0, :cond_3

    const-string/jumbo v0, "description"

    iget-object v3, p0, Lcom/twitter/library/api/TwitterUser;->profileDescription:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileUrl:Ljava/lang/String;

    if-eqz v0, :cond_4

    const-string/jumbo v0, "url_https"

    iget-object v3, p0, Lcom/twitter/library/api/TwitterUser;->profileUrl:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->location:Ljava/lang/String;

    if-eqz v0, :cond_5

    const-string/jumbo v0, "location"

    iget-object v3, p0, Lcom/twitter/library/api/TwitterUser;->location:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    iget-wide v3, p0, Lcom/twitter/library/api/TwitterUser;->createdAt:J

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-eqz v0, :cond_6

    const-string/jumbo v0, "created_at"

    iget-wide v3, p0, Lcom/twitter/library/api/TwitterUser;->createdAt:J

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    const-string/jumbo v0, "friends_count"

    iget v3, p0, Lcom/twitter/library/api/TwitterUser;->friendsCount:I

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    const-string/jumbo v0, "followers_count"

    iget v3, p0, Lcom/twitter/library/api/TwitterUser;->followersCount:I

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    const-string/jumbo v0, "statuses_count"

    iget v3, p0, Lcom/twitter/library/api/TwitterUser;->statusesCount:I

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    const-string/jumbo v0, "geo_enabled"

    iget-boolean v3, p0, Lcom/twitter/library/api/TwitterUser;->isGeoEnabled:Z

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    const-string/jumbo v0, "protected"

    iget-boolean v3, p0, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    const-string/jumbo v0, "is_lifeline_institution"

    iget-boolean v3, p0, Lcom/twitter/library/api/TwitterUser;->isLifelineInstitution:Z

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    const-string/jumbo v0, "verified"

    iget-boolean v3, p0, Lcom/twitter/library/api/TwitterUser;->verified:Z

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    const-string/jumbo v0, "is_translator"

    iget-boolean v3, p0, Lcom/twitter/library/api/TwitterUser;->isTranslator:Z

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    const-string/jumbo v0, "suspended"

    iget-boolean v3, p0, Lcom/twitter/library/api/TwitterUser;->suspended:Z

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    const-string/jumbo v0, "needs_phone_verification"

    iget-boolean v3, p0, Lcom/twitter/library/api/TwitterUser;->needsPhoneVerification:Z

    invoke-virtual {v1, v0, v3}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->flush()V

    invoke-virtual {v2}, Ljava/io/StringWriter;->getBuffer()Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    move-object v1, v0

    :goto_1
    :try_start_2
    const-string/jumbo v0, ""
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    :goto_2
    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    const-string/jumbo v1, "@"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string/jumbo v1, "[^a-zA-Z0-9_]"

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v2, v1

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    aget-object v0, v1, v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/au;)Ljava/lang/String;
    .locals 3

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/twitter/internal/util/a;->a:Lcom/twitter/internal/util/a;

    invoke-virtual {v0, p0}, Lcom/twitter/internal/util/a;->b(Ljava/lang/String;)Lcom/twitter/internal/util/e;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, v0, Lcom/twitter/internal/util/e;->a:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_1

    iget-object v0, v0, Lcom/twitter/internal/util/e;->b:Ljava/util/ArrayList;

    invoke-static {v0, p1, p2}, Lcom/twitter/library/api/ap;->a(Ljava/util/ArrayList;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/au;)V

    :cond_1
    invoke-static {v1, p1, p2}, Lcom/twitter/library/util/Util;->b(Ljava/lang/StringBuilder;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/au;)V

    invoke-static {v1, p1}, Lcom/twitter/library/util/Util;->a(Ljava/lang/CharSequence;Lcom/twitter/library/api/TweetEntities;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {v1, p1, p2}, Lcom/twitter/library/util/Util;->a(Ljava/lang/StringBuilder;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/au;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;Lcom/twitter/library/api/TwitterUser;ZZ)Ljava/util/ArrayList;
    .locals 18

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    if-eqz p0, :cond_5

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    const-wide/16 v4, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    const/4 v6, 0x0

    move-object v14, v3

    move-wide v15, v4

    move-wide v3, v15

    move-object v5, v14

    :goto_0
    if-eqz v5, :cond_4

    sget-object v9, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v5, v9, :cond_4

    sget-object v9, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v5, v9, :cond_6

    const/4 v5, 0x0

    const/4 v9, 0x1

    :try_start_0
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-static {v0, v1, v5, v9, v2}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/api/au;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterStatus;

    move-result-object v5

    if-eqz p3, :cond_0

    invoke-virtual {v5}, Lcom/twitter/library/api/TwitterStatus;->f()Z

    move-result v9

    if-eqz v9, :cond_0

    if-eqz v6, :cond_0

    iget-wide v9, v6, Lcom/twitter/library/api/TwitterStatus;->r:J

    iput-wide v9, v5, Lcom/twitter/library/api/TwitterStatus;->r:J

    :cond_0
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/twitter/library/util/NullUserException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz p4, :cond_1

    :try_start_1
    invoke-virtual {v5}, Lcom/twitter/library/api/TwitterStatus;->d()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/twitter/library/util/NullUserException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_1
    move-object v14, v5

    move-wide v15, v3

    move-wide v4, v15

    move-object v3, v14

    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v6

    move-object v14, v3

    move-object v15, v6

    move-object v6, v14

    move-wide/from16 v16, v4

    move-wide/from16 v3, v16

    move-object v5, v15

    goto :goto_0

    :cond_2
    :try_start_2
    invoke-virtual {v5}, Lcom/twitter/library/api/TwitterStatus;->f()Z

    move-result v6

    if-nez v6, :cond_1

    iget-wide v9, v5, Lcom/twitter/library/api/TwitterStatus;->r:J

    invoke-static {v3, v4, v9, v10}, Ljava/lang/Math;->max(JJ)J
    :try_end_2
    .catch Lcom/twitter/library/util/NullUserException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-wide v3

    goto :goto_1

    :catch_0
    move-exception v6

    :goto_3
    if-eqz p1, :cond_3

    const-string/jumbo v9, "Received null user for status = %d"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-wide v12, v6, Lcom/twitter/library/util/NullUserException;->statusId:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Lcom/twitter/library/util/w;->a(Ljava/lang/String;)V

    :cond_3
    move-object v14, v5

    move-wide v15, v3

    move-wide v4, v15

    move-object v3, v14

    goto :goto_2

    :cond_4
    if-eqz p4, :cond_5

    const-wide/16 v5, 0x1

    add-long v4, v3, v5

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/TwitterStatus;

    iput-wide v4, v3, Lcom/twitter/library/api/TwitterStatus;->r:J

    goto :goto_4

    :cond_5
    return-object v7

    :catch_1
    move-exception v5

    move-object v14, v5

    move-object v5, v6

    move-object v6, v14

    goto :goto_3

    :cond_6
    move-wide v14, v3

    move-wide v4, v14

    move-object v3, v6

    goto :goto_2
.end method

.method public static a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, p2, p3}, Lcom/twitter/library/api/ap;->a(Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterStatus;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_1
    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Ljava/util/HashMap;ZLcom/twitter/library/util/w;)Ljava/util/ArrayList;
    .locals 6

    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    :cond_0
    if-eqz p5, :cond_1

    const-string/jumbo v0, "Missing tweets map or users map"

    invoke-interface {p5, v0}, Lcom/twitter/library/util/w;->a(Ljava/lang/String;)V

    :cond_1
    invoke-static {}, Lcom/twitter/library/util/CollectionsUtil;->a()Ljava/util/ArrayList;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    :goto_1
    if-eqz v1, :cond_8

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_8

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v1, v2, :cond_7

    invoke-static/range {p0 .. p5}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Ljava/util/HashMap;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/x;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/twitter/library/api/x;->f()Lcom/twitter/library/api/PromotedContent;

    move-result-object v2

    if-eqz v2, :cond_5

    if-eqz v0, :cond_5

    iget-wide v2, v0, Lcom/twitter/library/api/x;->k:J

    iput-wide v2, v1, Lcom/twitter/library/api/x;->k:J

    :cond_3
    :goto_2
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    :cond_4
    :goto_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_1

    :cond_5
    iget v2, v1, Lcom/twitter/library/api/x;->c:I

    if-eqz v2, :cond_3

    if-eqz v0, :cond_6

    iget-wide v2, v0, Lcom/twitter/library/api/x;->k:J

    :goto_4
    iput-wide v2, v1, Lcom/twitter/library/api/x;->k:J

    invoke-virtual {v1}, Lcom/twitter/library/api/x;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v1}, Lcom/twitter/library/api/x;->g()Lcom/twitter/library/api/RecommendedContent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/api/RecommendedContent;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/twitter/library/api/TwitterUser;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/twitter/library/api/RecommendedContent;->socialProof:Ljava/lang/String;

    goto :goto_2

    :cond_6
    const-wide/16 v2, -0x1

    goto :goto_4

    :cond_7
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v1, v2, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :cond_8
    move-object v0, v4

    goto :goto_0
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/TwitterUser;)Ljava/util/HashMap;
    .locals 4

    const/4 v3, 0x0

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {p0, p1, v3, v2, v3}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/api/au;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterStatus;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_2
    return-object v1
.end method

.method private static a(Ljava/util/HashMap;Ljava/util/HashMap;Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;
    .locals 3

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_1

    const/4 v0, 0x0

    invoke-static {p2, p0, p1, v0}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterTopic;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_1
    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_0

    invoke-virtual {p2}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_2
    return-object v1
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/Map;)Ljava/util/Set;
    .locals 3

    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-static {p0, p1}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_1
    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/z;)V
    .locals 4

    new-instance v1, Lcom/twitter/library/api/TweetPivotOptions;

    invoke-direct {v1}, Lcom/twitter/library/api/TweetPivotOptions;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    const-string/jumbo v0, "display_style"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/api/TweetPivotOptions;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p1, v1}, Lcom/twitter/library/api/z;->a(Lcom/twitter/library/api/TweetPivotOptions;)Lcom/twitter/library/api/z;

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Lcom/twitter/library/api/z;Lcom/twitter/library/util/w;)V
    .locals 6

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_4

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_4

    sget-object v1, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move-object v0, v2

    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "ids"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0}, Lcom/twitter/library/api/ap;->aC(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/LinkedHashSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/TwitterTopic;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Missing event "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p3, v0}, Lcom/twitter/library/util/w;->a(Ljava/lang/String;)V

    goto :goto_2

    :cond_0
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    move-object v0, v2

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v2

    goto :goto_1

    :pswitch_2
    const-string/jumbo v0, "highlight"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0, p3}, Lcom/twitter/library/api/ap;->H(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TimelineScribeContent;

    move-result-object v0

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v2

    goto :goto_1

    :cond_4
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterTopic;

    invoke-virtual {p2, v0}, Lcom/twitter/library/api/z;->a(Lcom/twitter/library/api/TwitterTopic;)Lcom/twitter/library/api/z;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/z;->a(I)Lcom/twitter/library/api/z;

    :goto_3
    invoke-virtual {p2, v2}, Lcom/twitter/library/api/z;->a(Lcom/twitter/library/api/TimelineScribeContent;)Lcom/twitter/library/api/z;

    return-void

    :cond_5
    invoke-virtual {p2, v3}, Lcom/twitter/library/api/z;->a(Ljava/util/List;)Lcom/twitter/library/api/z;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/z;->a(I)Lcom/twitter/library/api/z;

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/library/api/z;Lcom/twitter/library/util/w;)V
    .locals 9

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v5, v0

    move-object v4, v1

    move-object v0, v1

    move-object v8, v1

    move v1, v3

    move-object v3, v8

    :goto_0
    if-eqz v5, :cond_3

    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v5, v6, :cond_3

    sget-object v6, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v5}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v5

    aget v5, v6, v5

    packed-switch v5, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v5

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "id"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p1, p2, p4}, Lcom/twitter/library/api/ap;->a(Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterStatus;

    move-result-object v4

    goto :goto_1

    :cond_1
    const-string/jumbo v6, "sort_index"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :pswitch_2
    const-string/jumbo v5, "is_suggestion"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v1, v2

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "highlight"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-static {p0, p4}, Lcom/twitter/library/api/ap;->H(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TimelineScribeContent;

    move-result-object v0

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_3
    if-eqz v4, :cond_5

    invoke-virtual {p3, v4}, Lcom/twitter/library/api/z;->a(Lcom/twitter/library/api/TwitterStatus;)Lcom/twitter/library/api/z;

    move-result-object v5

    iget-wide v6, v4, Lcom/twitter/library/api/TwitterStatus;->r:J

    invoke-virtual {v5, v6, v7}, Lcom/twitter/library/api/z;->a(J)Lcom/twitter/library/api/z;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/twitter/library/api/z;->a(Lcom/twitter/library/api/TimelineScribeContent;)Lcom/twitter/library/api/z;

    if-eqz v3, :cond_4

    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {p3, v5, v6}, Lcom/twitter/library/api/z;->b(J)Lcom/twitter/library/api/z;

    :cond_4
    if-eqz v1, :cond_5

    const/4 v0, 0x3

    invoke-virtual {p3, v0}, Lcom/twitter/library/api/z;->b(I)Lcom/twitter/library/api/z;

    iput-boolean v2, v4, Lcom/twitter/library/api/TwitterStatus;->D:Z

    :cond_5
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/library/api/z;Lcom/twitter/library/util/w;)V
    .locals 16

    const/4 v7, 0x0

    const/4 v12, 0x0

    const/4 v6, 0x0

    const-wide/16 v4, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    :goto_0
    if-eqz v3, :cond_4

    sget-object v8, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v3, v8, :cond_4

    sget-object v8, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v3}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v3

    aget v3, v8, v3

    packed-switch v3, :pswitch_data_0

    :cond_0
    :pswitch_0
    move-wide v13, v4

    move-wide v3, v13

    move-object v5, v6

    move-object v6, v12

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v8

    move-object v12, v6

    move-object v6, v5

    move-wide v13, v3

    move-wide v4, v13

    move-object v3, v8

    goto :goto_0

    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v8, "tweet_id"

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p5

    invoke-static {v3, v0, v1, v2}, Lcom/twitter/library/api/ap;->a(Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterStatus;

    move-result-object v3

    move-object v7, v3

    move-object v13, v6

    move-object v6, v12

    move-wide v14, v4

    move-wide v3, v14

    move-object v5, v13

    goto :goto_1

    :cond_1
    const-string/jumbo v8, "sort_index"

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v3

    move-object v6, v12

    move-object v13, v3

    move-wide v14, v4

    move-wide v3, v14

    move-object v5, v13

    goto :goto_1

    :cond_2
    const-string/jumbo v8, "timeline_id"

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/TwitterTopic;

    move-wide v13, v4

    move-object v5, v6

    move-object v6, v3

    move-wide v3, v13

    goto :goto_1

    :cond_3
    const-string/jumbo v8, "curated_at"

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Lcom/twitter/internal/util/k;->b:Ljava/text/SimpleDateFormat;

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/twitter/library/util/Util;->a(Ljava/text/SimpleDateFormat;Ljava/lang/String;)J

    move-result-wide v3

    move-object v5, v6

    move-object v6, v12

    goto :goto_1

    :pswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-wide v13, v4

    move-wide v3, v13

    move-object v5, v6

    move-object v6, v12

    goto :goto_1

    :cond_4
    if-eqz v7, :cond_7

    if-eqz v12, :cond_7

    const/4 v3, 0x1

    iput-boolean v3, v7, Lcom/twitter/library/api/TwitterStatus;->E:Z

    move-object/from16 v0, p4

    invoke-virtual {v0, v7}, Lcom/twitter/library/api/z;->a(Lcom/twitter/library/api/TwitterStatus;)Lcom/twitter/library/api/z;

    const-wide/16 v7, 0x0

    cmp-long v3, v4, v7

    if-lez v3, :cond_5

    move-object/from16 v0, p4

    invoke-virtual {v0, v4, v5}, Lcom/twitter/library/api/z;->a(J)Lcom/twitter/library/api/z;

    :cond_5
    if-eqz v6, :cond_6

    invoke-static {v6}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    move-object/from16 v0, p4

    invoke-virtual {v0, v3, v4}, Lcom/twitter/library/api/z;->b(J)Lcom/twitter/library/api/z;

    :cond_6
    new-instance v3, Lcom/twitter/library/api/TwitterSocialProof;

    const/16 v4, 0x22

    iget-object v5, v12, Lcom/twitter/library/api/TwitterTopic;->c:Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct/range {v3 .. v11}, Lcom/twitter/library/api/TwitterSocialProof;-><init>(ILjava/lang/String;IIIILjava/lang/String;I)V

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Lcom/twitter/library/api/z;->a(Lcom/twitter/library/api/TwitterSocialProof;)Lcom/twitter/library/api/z;

    const/4 v3, 0x5

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Lcom/twitter/library/api/z;->b(I)Lcom/twitter/library/api/z;

    move-object/from16 v0, p4

    invoke-virtual {v0, v12}, Lcom/twitter/library/api/z;->a(Lcom/twitter/library/api/TwitterTopic;)Lcom/twitter/library/api/z;

    :cond_7
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/Map;Lcom/twitter/library/api/z;Lcom/twitter/library/util/w;)V
    .locals 6

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_4

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_4

    sget-object v1, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move-object v0, v2

    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "users"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0, p1}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/TwitterUser;

    if-eqz v1, :cond_0

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Missing user "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p3, v0}, Lcom/twitter/library/util/w;->a(Ljava/lang/String;)V

    goto :goto_2

    :cond_1
    move-object v0, v2

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v2

    goto :goto_1

    :pswitch_2
    const-string/jumbo v0, "highlight"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0, p3}, Lcom/twitter/library/api/ap;->H(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TimelineScribeContent;

    move-result-object v0

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v2

    goto :goto_1

    :cond_4
    invoke-virtual {p2, v3}, Lcom/twitter/library/api/z;->b(Ljava/util/List;)Lcom/twitter/library/api/z;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/z;->a(I)Lcom/twitter/library/api/z;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/library/api/z;->a(Lcom/twitter/library/api/TimelineScribeContent;)Lcom/twitter/library/api/z;

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Ljava/util/ArrayList;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/au;)V
    .locals 1

    iget-object v0, p1, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-static {v0, p0}, Lcom/twitter/library/api/ap;->b(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    iget-object v0, p1, Lcom/twitter/library/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    invoke-static {v0, p0}, Lcom/twitter/library/api/ap;->b(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    iget-object v0, p1, Lcom/twitter/library/api/TweetEntities;->hashtags:Ljava/util/ArrayList;

    invoke-static {v0, p0}, Lcom/twitter/library/api/ap;->b(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    iget-object v0, p1, Lcom/twitter/library/api/TweetEntities;->cashtags:Ljava/util/ArrayList;

    invoke-static {v0, p0}, Lcom/twitter/library/api/ap;->b(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    if-eqz p2, :cond_0

    iget-object v0, p2, Lcom/twitter/library/api/au;->a:Ljava/util/ArrayList;

    invoke-static {v0, p0}, Lcom/twitter/library/api/ap;->b(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    :cond_0
    return-void
.end method

.method public static a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 9

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v0

    move v2, v0

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/Entity;

    iget v7, v0, Lcom/twitter/library/api/Entity;->start:I

    move v4, v1

    move v3, v2

    move v2, v1

    :goto_1
    if-ge v4, v5, :cond_3

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/UrlEntity;

    iget v8, v1, Lcom/twitter/library/api/UrlEntity;->start:I

    if-ge v8, v7, :cond_2

    iget-object v8, v1, Lcom/twitter/library/api/UrlEntity;->displayUrl:Ljava/lang/String;

    if-eqz v8, :cond_2

    add-int/lit8 v2, v2, 0x1

    iget-object v8, v1, Lcom/twitter/library/api/UrlEntity;->displayUrl:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    iget-object v1, v1, Lcom/twitter/library/api/UrlEntity;->url:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int v1, v8, v1

    add-int/2addr v3, v1

    :cond_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    :cond_3
    iget v1, v0, Lcom/twitter/library/api/Entity;->start:I

    add-int/2addr v1, v3

    iput v1, v0, Lcom/twitter/library/api/Entity;->start:I

    iget v1, v0, Lcom/twitter/library/api/Entity;->end:I

    add-int/2addr v1, v3

    iput v1, v0, Lcom/twitter/library/api/Entity;->end:I

    move v1, v2

    move v2, v3

    goto :goto_0
.end method

.method private static a(Lcom/twitter/library/api/TwitterStatus;)Z
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/twitter/library/api/TwitterStatus;->m:Lcom/twitter/library/api/TwitterStatusCard;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/twitter/library/api/TwitterStatus;->m:Lcom/twitter/library/api/TwitterStatusCard;

    iget-object v2, v2, Lcom/twitter/library/api/TwitterStatusCard;->classicCard:Lcom/twitter/library/api/TweetClassicCard;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/twitter/library/api/TwitterStatus;->m:Lcom/twitter/library/api/TwitterStatusCard;

    iget-object v2, v2, Lcom/twitter/library/api/TwitterStatusCard;->classicCard:Lcom/twitter/library/api/TweetClassicCard;

    iget v2, v2, Lcom/twitter/library/api/TweetClassicCard;->type:I

    if-ne v2, v1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v2, p0, Lcom/twitter/library/api/TwitterStatus;->m:Lcom/twitter/library/api/TwitterStatusCard;

    iget-object v2, v2, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    if-eqz v2, :cond_2

    const-string/jumbo v2, "photo"

    iget-object v3, p0, Lcom/twitter/library/api/TwitterStatus;->m:Lcom/twitter/library/api/TwitterStatusCard;

    iget-object v3, v3, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    iget-object v3, v3, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "gallery"

    iget-object v3, p0, Lcom/twitter/library/api/TwitterStatus;->m:Lcom/twitter/library/api/TwitterStatusCard;

    iget-object v3, v3, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    iget-object v3, v3, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    iget-object v2, p0, Lcom/twitter/library/api/TwitterStatus;->j:Lcom/twitter/library/api/TweetEntities;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/twitter/library/api/TwitterStatus;->j:Lcom/twitter/library/api/TweetEntities;

    invoke-virtual {v2, v1}, Lcom/twitter/library/api/TweetEntities;->a(I)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/twitter/library/api/TwitterStatus;->j:Lcom/twitter/library/api/TweetEntities;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/twitter/library/api/TweetEntities;->a(I)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    move v0, v1

    :cond_4
    move v1, v0

    goto :goto_0

    :cond_5
    move v1, v0

    goto :goto_0
.end method

.method private static aA(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;
    .locals 4

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_2
    return-object v1
.end method

.method private static aB(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;
    .locals 3

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_1

    invoke-static {p0}, Lcom/twitter/library/api/ap;->ay(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/TwitterTopic;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_2
    return-object v1
.end method

.method private static aC(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/LinkedHashSet;
    .locals 3

    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_0

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_0
    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static aD(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_1
    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static aE(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/featureswitch/FeatureSwitchesValue;
    .locals 4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_1

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_1

    const-string/jumbo v1, "value"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Lcom/twitter/library/featureswitch/FeatureSwitchesValue;

    invoke-static {p0}, Lcom/twitter/library/api/ap;->aF(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lcom/twitter/library/featureswitch/FeatureSwitchesValue;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_0
    new-instance v0, Lcom/twitter/library/featureswitch/FeatureSwitchesValue;

    invoke-static {p0}, Lcom/twitter/library/api/ap;->aG(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lcom/twitter/library/featureswitch/FeatureSwitchesValue;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    :cond_1
    if-nez v0, :cond_2

    new-instance v0, Lcom/twitter/library/util/InvalidDataException;

    const-string/jumbo v1, "Default value not found for FeatureSwitchesConfig"

    invoke-direct {v0, v1}, Lcom/twitter/library/util/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private static aF(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;
    .locals 2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    new-instance v0, Lcom/twitter/library/util/InvalidDataException;

    const-string/jumbo v1, "Value not found."

    invoke-direct {v0, v1}, Lcom/twitter/library/util/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->k()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->i()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method private static aG(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_1

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p0}, Lcom/twitter/library/api/ap;->aF(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/twitter/library/util/InvalidDataException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Invalid feature switch array value. token : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/twitter/library/util/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    return-object v1
.end method

.method private static aH(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashSet;
    .locals 4

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_0

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    new-instance v1, Lcom/twitter/library/util/InvalidDataException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Invalid feature switches string array token: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/twitter/library/util/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_0
    return-object v1

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static aa(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v0

    move-object v0, v1

    move-object v1, v3

    :goto_0
    if-eqz v1, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_1

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    const-string/jumbo v1, "access_token"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static ab(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v0

    move-object v0, v1

    move-object v1, v3

    :goto_0
    if-eqz v1, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_1

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    const-string/jumbo v1, "guest_token"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static ac(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/network/OneFactorLoginResponse;
    .locals 8

    const-wide/16 v2, 0x0

    const-string/jumbo v1, ""

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v5, v0

    move-object v0, v1

    move-wide v6, v2

    move-wide v1, v6

    move-object v3, v5

    :goto_0
    if-eqz v3, :cond_1

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v3, v4, :cond_1

    sget-object v4, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v3}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v3

    aget v3, v4, v3

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    goto :goto_0

    :pswitch_1
    const-string/jumbo v3, "login_verification_request_id"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    const-string/jumbo v3, "login_verification_user_id"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v1

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_1
    new-instance v3, Lcom/twitter/library/network/OneFactorLoginResponse;

    invoke-direct {v3, v0, v1, v2}, Lcom/twitter/library/network/OneFactorLoginResponse;-><init>(Ljava/lang/String;J)V

    return-object v3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public static ad(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Boolean;
    .locals 2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_1

    sget-object v1, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v1, v0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :sswitch_0
    const-string/jumbo v0, "is_eligible"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_2
    return-object v0

    :sswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :sswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_1
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x4 -> :sswitch_1
        0x7 -> :sswitch_0
    .end sparse-switch
.end method

.method private static ae(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/ax;
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v2, v0

    move-object v0, v1

    :goto_0
    if-eqz v2, :cond_2

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_2

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "description"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {p0}, Lcom/twitter/library/api/TweetEntities;->a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/TweetEntities;

    move-result-object v1

    goto :goto_1

    :cond_0
    const-string/jumbo v3, "url"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p0}, Lcom/twitter/library/api/TweetEntities;->a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/TweetEntities;

    move-result-object v0

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_2
    new-instance v2, Lcom/twitter/library/api/ax;

    invoke-direct {v2, v1, v0}, Lcom/twitter/library/api/ax;-><init>(Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/TweetEntities;)V

    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static af(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;
    .locals 6

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_1
    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private static ag(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;
    .locals 6

    if-eqz p0, :cond_7

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_6

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_6

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "summaries"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const-string/jumbo v3, "photos"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const-string/jumbo v4, "promotions"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v2, :cond_1

    if-nez v3, :cond_1

    if-nez v4, :cond_1

    const-string/jumbo v5, "players"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_1
    if-eqz v2, :cond_3

    const/4 v0, 0x3

    :goto_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    :goto_3
    if-eqz v2, :cond_0

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_0

    invoke-static {p0, v0}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;I)Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_3

    :cond_3
    if-eqz v3, :cond_4

    const/4 v0, 0x1

    goto :goto_2

    :cond_4
    if-eqz v4, :cond_5

    const/4 v0, 0x4

    goto :goto_2

    :cond_5
    const/4 v0, 0x2

    goto :goto_2

    :cond_6
    move-object v0, v1

    :goto_4
    return-object v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static ah(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/UrlConfiguration;
    .locals 6

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v2, 0x0

    move-object v3, v1

    move-object v1, v0

    :goto_0
    if-eqz v3, :cond_1

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v3, v4, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v3}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v3

    aget v3, v5, v3

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    goto :goto_0

    :pswitch_1
    const-string/jumbo v3, "short_url_length"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v2

    goto :goto_1

    :pswitch_2
    const-string/jumbo v3, "client_event_url"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :pswitch_3
    const-string/jumbo v3, "non_username_paths"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {p0}, Lcom/twitter/library/api/ap;->k(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_1
    new-instance v3, Lcom/twitter/library/api/UrlConfiguration;

    invoke-direct {v3, v2, v1, v0}, Lcom/twitter/library/api/UrlConfiguration;-><init>(ILjava/lang/String;Ljava/util/ArrayList;)V

    return-object v3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method private static ai(Lcom/fasterxml/jackson/core/JsonParser;)I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v1, v2, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_2

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_2

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "following"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    or-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    const-string/jumbo v2, "followed_by"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    or-int/lit8 v0, v0, 0x2

    goto :goto_1

    :cond_2
    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static aj(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/TwitterUser;
    .locals 5

    new-instance v2, Lcom/twitter/library/api/TwitterUser;

    invoke-direct {v2}, Lcom/twitter/library/api/TwitterUser;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v1, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_3

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_3

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_1
    const-string/jumbo v1, "connections"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0}, Lcom/twitter/library/api/ap;->ai(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    iput v1, v2, Lcom/twitter/library/api/TwitterUser;->friendship:I

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    const-string/jumbo v1, "id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v3

    iput-wide v3, v2, Lcom/twitter/library/api/TwitterUser;->userId:J

    goto :goto_1

    :pswitch_3
    const-string/jumbo v1, "name"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    :cond_2
    const-string/jumbo v1, "screen_name"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    goto :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static ak(Lcom/fasterxml/jackson/core/JsonParser;)I
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v0

    move v0, v1

    move-object v1, v3

    :goto_0
    if-eqz v1, :cond_b

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_b

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "following"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v0

    goto :goto_1

    :cond_1
    const-string/jumbo v2, "followed_by"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v0

    goto :goto_1

    :cond_2
    const-string/jumbo v2, "blocking"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v0

    goto :goto_1

    :cond_3
    const-string/jumbo v2, "can_dm"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v0

    goto :goto_1

    :cond_4
    const-string/jumbo v2, "notifications_enabled"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/16 v1, 0x10

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v0

    goto :goto_1

    :cond_5
    const-string/jumbo v2, "lifeline_following"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/16 v1, 0x100

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v0

    goto :goto_1

    :cond_6
    const-string/jumbo v2, "favorite_following"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v1, 0x800

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v0

    goto :goto_1

    :cond_7
    const-string/jumbo v2, "email_following"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v1, 0x1000

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v0

    goto/16 :goto_1

    :cond_8
    const-string/jumbo v2, "want_retweets"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v1, 0x200

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v0

    goto/16 :goto_1

    :cond_9
    const-string/jumbo v2, "can_media_tag"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v1, 0x400

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v0

    goto/16 :goto_1

    :cond_a
    const-string/jumbo v2, "muting"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x2000

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v0

    goto/16 :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    :cond_b
    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static al(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_0

    invoke-static {p0}, Lcom/twitter/library/api/ap;->am(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/geo/TwitterPlace;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method private static am(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/geo/TwitterPlace;
    .locals 15

    const/4 v11, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v10, v11

    move-object v9, v11

    move-object v8, v11

    move-object v7, v11

    move-object v6, v11

    move-object v5, v11

    move-object v4, v11

    move-object v3, v11

    move-object v2, v11

    move-object v1, v11

    :goto_0
    if-eqz v0, :cond_f

    sget-object v12, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v12, :cond_f

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v12

    sget-object v13, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v13, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :pswitch_0
    move-object v0, v2

    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    move-object v14, v2

    move-object v2, v0

    move-object v0, v14

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "full_name"

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v3

    move-object v0, v2

    goto :goto_1

    :cond_1
    const-string/jumbo v0, "name"

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v4

    move-object v0, v2

    goto :goto_1

    :cond_2
    const-string/jumbo v0, "place_type"

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    const-string/jumbo v0, "id"

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    move-object v0, v2

    goto :goto_1

    :cond_4
    const-string/jumbo v0, "country"

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v6

    move-object v0, v2

    goto :goto_1

    :cond_5
    const-string/jumbo v0, "country_code"

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v7

    move-object v0, v2

    goto :goto_1

    :pswitch_2
    const-string/jumbo v0, "contained_within"

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_0

    sget-object v12, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v12, :cond_0

    sget-object v12, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v12, :cond_6

    invoke-static {p0}, Lcom/twitter/library/api/ap;->am(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/geo/TwitterPlace;

    move-result-object v9

    :cond_6
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_2

    :cond_7
    const-string/jumbo v0, "polylines"

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_3
    if-eqz v0, :cond_0

    sget-object v12, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v12, :cond_0

    sget-object v12, Lcom/fasterxml/jackson/core/JsonToken;->h:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v12, :cond_8

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v8

    :cond_8
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_3

    :cond_9
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v2

    goto/16 :goto_1

    :pswitch_3
    const-string/jumbo v0, "bounding_box"

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_4
    if-eqz v0, :cond_b

    sget-object v12, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v12, :cond_b

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v12

    sget-object v13, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v13, v0

    packed-switch v0, :pswitch_data_1

    :goto_5
    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_4

    :pswitch_5
    const-string/jumbo v0, "coordinates"

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {p0}, Lcom/twitter/library/api/ap;->v(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v5

    goto :goto_5

    :cond_a
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_5

    :pswitch_6
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_5

    :cond_b
    move-object v0, v2

    goto/16 :goto_1

    :cond_c
    const-string/jumbo v0, "attributes"

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_6
    if-eqz v0, :cond_0

    sget-object v12, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v12, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v12

    sget-object v13, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v13, v0

    packed-switch v0, :pswitch_data_2

    :goto_7
    :pswitch_7
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_6

    :pswitch_8
    if-nez v10, :cond_d

    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    :cond_d
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v12, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_7

    :pswitch_9
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_7

    :cond_e
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v2

    goto/16 :goto_1

    :cond_f
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_10

    move-object v0, v11

    :goto_8
    return-object v0

    :cond_10
    new-instance v0, Lcom/twitter/library/api/geo/TwitterPlace;

    invoke-static {v2}, Lcom/twitter/library/api/geo/TwitterPlace;->a(Ljava/lang/String;)I

    move-result v2

    invoke-direct/range {v0 .. v10}, Lcom/twitter/library/api/geo/TwitterPlace;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/api/geo/TwitterPlace;Ljava/util/HashMap;)V

    goto :goto_8

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_9
    .end packed-switch
.end method

.method private static an(Lcom/fasterxml/jackson/core/JsonParser;)Landroid/util/Pair;
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    const/4 v2, 0x0

    move-object v1, v3

    move-object v4, v0

    move-object v0, v3

    :goto_0
    if-eqz v4, :cond_2

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v5, :cond_2

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->j:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v5, :cond_0

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->i:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v4, v5, :cond_1

    :cond_0
    packed-switch v2, :pswitch_data_0

    :goto_1
    add-int/lit8 v2, v2, 0x1

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->j()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->j()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    goto :goto_1

    :cond_2
    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, v1, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v2

    :goto_2
    return-object v0

    :cond_3
    move-object v0, v3

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static ao(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;
    .locals 3

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "tooltip_tweet_id"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "tweetId"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    const-string/jumbo v0, "tooltip_style"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "style"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_2
    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static ap(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v0

    move-object v0, v1

    move-object v1, v3

    :goto_0
    if-eqz v1, :cond_4

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_4

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    const-string/jumbo v1, "users"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    :goto_2
    if-eqz v1, :cond_0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_0

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_1

    :cond_1
    :goto_3
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_2

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    :goto_4
    if-eqz v1, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_1

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_2

    :cond_2
    :goto_5
    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_4

    :pswitch_5
    const-string/jumbo v1, "name"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    :pswitch_6
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_5

    :pswitch_7
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :cond_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_8
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_4
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_6
    .end packed-switch
.end method

.method private static aq(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/TwitterSearchFilter;
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v0

    move v0, v2

    :goto_0
    if-eqz v3, :cond_2

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v3, v4, :cond_2

    sget-object v4, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v3}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v3

    aget v3, v4, v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    goto :goto_0

    :sswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "follow"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v2, v1

    goto :goto_1

    :cond_1
    const-string/jumbo v4, "nearby"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    goto :goto_1

    :sswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_2
    new-instance v1, Lcom/twitter/library/api/TwitterSearchFilter;

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/api/TwitterSearchFilter;-><init>(ZZ)V

    return-object v1

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x4 -> :sswitch_1
        0x7 -> :sswitch_0
    .end sparse-switch
.end method

.method private static ar(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-static {p0}, Lcom/twitter/library/api/ap;->as(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/search/TwitterTypeAhead;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_1
    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static as(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/search/TwitterTypeAhead;
    .locals 39

    const-wide/16 v2, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v17, 0x0

    const/16 v38, 0x0

    const/16 v37, 0x0

    const/16 v36, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/16 v33, 0x0

    const/16 v23, 0x400

    :goto_0
    if-eqz v1, :cond_b

    sget-object v7, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v7, :cond_b

    sget-object v7, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v7, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :pswitch_0
    move-object/from16 v1, v36

    move-wide v7, v2

    move/from16 v3, v38

    move/from16 v2, v37

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v9

    move-object/from16 v36, v1

    move/from16 v37, v2

    move/from16 v38, v3

    move-object v1, v9

    move-wide v2, v7

    goto :goto_0

    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v7, "name"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v1, v36

    move-wide v7, v2

    move/from16 v3, v38

    move/from16 v2, v37

    goto :goto_1

    :cond_1
    const-string/jumbo v7, "screen_name"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v1, v36

    move-wide v7, v2

    move/from16 v3, v38

    move/from16 v2, v37

    goto :goto_1

    :cond_2
    const-string/jumbo v7, "profile_image_url_https"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v1, v36

    move-wide v7, v2

    move/from16 v3, v38

    move/from16 v2, v37

    goto :goto_1

    :cond_3
    const-string/jumbo v7, "location"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v1, v36

    move-wide v7, v2

    move/from16 v3, v38

    move/from16 v2, v37

    goto :goto_1

    :pswitch_2
    const-string/jumbo v1, "verified"

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v14, 0x1

    move-object/from16 v1, v36

    move-wide v7, v2

    move/from16 v3, v38

    move/from16 v2, v37

    goto :goto_1

    :cond_4
    const-string/jumbo v1, "is_translator"

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v15, 0x1

    move-object/from16 v1, v36

    move-wide v7, v2

    move/from16 v3, v38

    move/from16 v2, v37

    goto/16 :goto_1

    :cond_5
    const-string/jumbo v1, "is_lifeline_institution"

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v16, 0x1

    move-object/from16 v1, v36

    move-wide v7, v2

    move/from16 v3, v38

    move/from16 v2, v37

    goto/16 :goto_1

    :pswitch_3
    const-string/jumbo v1, "can_media_tag"

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move/from16 v0, v23

    and-int/lit16 v0, v0, -0x401

    move/from16 v23, v0

    move-object/from16 v1, v36

    move-wide v7, v2

    move/from16 v3, v38

    move/from16 v2, v37

    goto/16 :goto_1

    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v7, "id"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v2

    move-object/from16 v1, v36

    move-wide v7, v2

    move/from16 v3, v38

    move/from16 v2, v37

    goto/16 :goto_1

    :cond_6
    const-string/jumbo v7, "rounded_score"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v1

    move-wide v7, v2

    move/from16 v2, v37

    move v3, v1

    move-object/from16 v1, v36

    goto/16 :goto_1

    :cond_7
    const-string/jumbo v7, "rounded_graph_weight"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v1

    move-wide v7, v2

    move v2, v1

    move/from16 v3, v38

    move-object/from16 v1, v36

    goto/16 :goto_1

    :pswitch_5
    const-string/jumbo v1, "tokens"

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/ap;->av(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v1

    move-wide v7, v2

    move/from16 v3, v38

    move/from16 v2, v37

    goto/16 :goto_1

    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object/from16 v1, v36

    move-wide v7, v2

    move/from16 v3, v38

    move/from16 v2, v37

    goto/16 :goto_1

    :pswitch_6
    const-string/jumbo v1, "social_context"

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/ap;->H(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/TwitterSocialProof;

    move-result-object v1

    if-eqz v1, :cond_9

    new-instance v33, Lcom/twitter/library/api/TwitterUserMetadata;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, v33

    invoke-direct {v0, v1, v7, v8, v9}, Lcom/twitter/library/api/TwitterUserMetadata;-><init>(Lcom/twitter/library/api/TwitterSocialProof;Ljava/lang/String;Ljava/lang/String;Z)V

    iget v1, v1, Lcom/twitter/library/api/TwitterSocialProof;->f:I

    or-int v23, v23, v1

    :cond_9
    move-object/from16 v1, v36

    move-wide v7, v2

    move/from16 v3, v38

    move/from16 v2, v37

    goto/16 :goto_1

    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object/from16 v1, v36

    move-wide v7, v2

    move/from16 v3, v38

    move/from16 v2, v37

    goto/16 :goto_1

    :cond_b
    if-nez v36, :cond_c

    const/4 v2, 0x0

    :goto_2
    return-object v2

    :cond_c
    new-instance v1, Lcom/twitter/library/api/TwitterUser;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v18, 0x0

    const-wide/16 v19, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const-wide/16 v24, 0x0

    const/16 v26, 0x0

    const-wide/16 v27, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    invoke-direct/range {v1 .. v35}, Lcom/twitter/library/api/TwitterUser;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IZZZZZLjava/lang/String;IJIZIJLcom/twitter/library/api/TwitterStatus;JILcom/twitter/library/api/PromotedContent;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/TwitterUserMetadata;ZZ)V

    new-instance v2, Lcom/twitter/library/api/search/TwitterTypeAhead;

    const/4 v3, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    move/from16 v4, v38

    move/from16 v5, v37

    move-object/from16 v6, v36

    move-object v7, v1

    invoke-direct/range {v2 .. v9}, Lcom/twitter/library/api/search/TwitterTypeAhead;-><init>(IIILjava/util/ArrayList;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/api/search/TwitterSearchQuery;Ljava/lang/String;)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_1
        :pswitch_0
        :pswitch_6
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private static at(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-static {p0}, Lcom/twitter/library/api/ap;->au(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/search/TwitterTypeAhead;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_1
    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static au(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/search/TwitterTypeAhead;
    .locals 8

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move v2, v3

    move-object v7, v4

    :goto_0
    if-eqz v0, :cond_1

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_1

    sget-object v1, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "hashtag"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    :pswitch_2
    const-string/jumbo v0, "rounded_score"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v2

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_1
    if-nez v7, :cond_2

    :goto_2
    return-object v4

    :cond_2
    new-instance v0, Lcom/twitter/library/api/search/TwitterTypeAhead;

    const/4 v1, 0x2

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/api/search/TwitterTypeAhead;-><init>(IIILjava/util/ArrayList;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/api/search/TwitterSearchQuery;Ljava/lang/String;)V

    move-object v4, v0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private static av(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->h:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_0

    const-string/jumbo v0, "token"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_2
    return-object v1
.end method

.method private static aw(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/ad;
    .locals 14

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const-wide/32 v11, 0x15180

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_b

    sget-object v13, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v13, :cond_b

    sget-object v13, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v13, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v13, "name"

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_1
    const-string/jumbo v13, "key"

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_2
    const-string/jumbo v13, "banner"

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :pswitch_2
    const-string/jumbo v0, "zero_rate"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v4, 0x1

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v13, "expire_seconds"

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v11

    goto :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v13, "host_map"

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    invoke-static {p0}, Lcom/twitter/library/api/ap;->ax(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;

    move-result-object v9

    goto :goto_1

    :cond_3
    const-string/jumbo v13, "display_flags"

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_8

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_0

    sget-object v13, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v13, :cond_0

    sget-object v13, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v13, v0

    sparse-switch v0, :sswitch_data_0

    :cond_4
    :goto_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_2

    :sswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v13, "inline_media_interstitial"

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    const/4 v5, 0x1

    goto :goto_3

    :cond_5
    const-string/jumbo v13, "external_links_interstitial"

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    const/4 v6, 0x1

    goto :goto_3

    :cond_6
    const-string/jumbo v13, "footer_text"

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_7

    const/4 v7, 0x1

    goto :goto_3

    :cond_7
    const-string/jumbo v13, "banner_message"

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v8, 0x1

    goto :goto_3

    :sswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :cond_8
    const-string/jumbo v13, "interstitial"

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_4
    if-eqz v0, :cond_0

    sget-object v13, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v13, :cond_0

    sget-object v13, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v13, v0

    packed-switch v0, :pswitch_data_1

    :cond_9
    :goto_5
    :pswitch_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_4

    :pswitch_6
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v13, "text"

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v10

    goto :goto_5

    :pswitch_7
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_5

    :cond_a
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    :pswitch_8
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    :cond_b
    if-eqz v1, :cond_c

    if-eqz v2, :cond_c

    new-instance v0, Lcom/twitter/library/api/ad;

    invoke-direct/range {v0 .. v12}, Lcom/twitter/library/api/ad;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZZLjava/util/HashMap;Ljava/lang/String;J)V

    :goto_6
    return-object v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x4 -> :sswitch_1
        0x7 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_7
    .end packed-switch
.end method

.method private static ax(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;
    .locals 4

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_0

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v3, :cond_0

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_1

    :cond_1
    :goto_3
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_2

    :pswitch_3
    const-string/jumbo v0, "host"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :pswitch_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_2
    return-object v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method private static ay(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/TwitterTopic;
    .locals 25

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v12

    const/4 v10, 0x0

    const/16 v21, 0x0

    const/16 v19, 0x0

    const/4 v8, 0x0

    const/4 v4, 0x0

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    const/4 v9, 0x6

    const/16 v20, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_e

    sget-object v11, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v11, :cond_e

    sget-object v11, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v11, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :pswitch_0
    move v1, v9

    move-object v11, v10

    move-object/from16 v9, v19

    move-object/from16 v10, v21

    move-object/from16 v22, v4

    move v4, v3

    move/from16 v23, v2

    move-wide v2, v6

    move-object/from16 v7, v22

    move/from16 v6, v23

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v13

    move-object/from16 v19, v9

    move-object/from16 v21, v10

    move v9, v1

    move-object v10, v11

    move-object v1, v13

    move-object/from16 v22, v7

    move/from16 v23, v6

    move-wide v6, v2

    move/from16 v2, v23

    move v3, v4

    move-object/from16 v4, v22

    goto :goto_0

    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v11, "name"

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    move-object v11, v10

    move-object v10, v1

    move v1, v9

    move-object/from16 v9, v19

    move-object/from16 v22, v4

    move v4, v3

    move/from16 v23, v2

    move-wide v2, v6

    move/from16 v6, v23

    move-object/from16 v7, v22

    goto :goto_1

    :cond_1
    const-string/jumbo v11, "description"

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    move-object v11, v10

    move-object/from16 v10, v21

    move/from16 v22, v3

    move/from16 v23, v2

    move-wide v2, v6

    move-object v7, v4

    move/from16 v6, v23

    move/from16 v4, v22

    move-object/from16 v24, v1

    move v1, v9

    move-object/from16 v9, v24

    goto :goto_1

    :cond_2
    const-string/jumbo v11, "user_id"

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    move v4, v3

    move-object v11, v10

    move-object/from16 v10, v21

    move/from16 v22, v2

    move-wide v2, v6

    move-object v7, v1

    move/from16 v6, v22

    move v1, v9

    move-object/from16 v9, v19

    goto :goto_1

    :cond_3
    const-string/jumbo v11, "custom_timeline_url"

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_4

    const-string/jumbo v11, "url"

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_5

    move-object v8, v1

    :cond_5
    move v1, v9

    move-object v11, v10

    move-object/from16 v9, v19

    move-object/from16 v10, v21

    move-object/from16 v22, v4

    move v4, v3

    move/from16 v23, v2

    move-wide v2, v6

    move-object/from16 v7, v22

    move/from16 v6, v23

    goto/16 :goto_1

    :cond_6
    const-string/jumbo v11, "id"

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v10, v21

    move-object v11, v1

    move v1, v9

    move-object/from16 v9, v19

    move-object/from16 v22, v4

    move v4, v3

    move/from16 v23, v2

    move-wide v2, v6

    move-object/from16 v7, v22

    move/from16 v6, v23

    goto/16 :goto_1

    :cond_7
    const-string/jumbo v11, "type"

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v11, "list"

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    const/4 v1, 0x7

    :goto_2
    move-object/from16 v9, v19

    move-object v11, v10

    move-object/from16 v10, v21

    move-object/from16 v22, v4

    move v4, v3

    move-wide/from16 v23, v6

    move v6, v2

    move-object/from16 v7, v22

    move-wide/from16 v2, v23

    goto/16 :goto_1

    :cond_8
    const-string/jumbo v11, "curated"

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    const/4 v1, 0x6

    goto :goto_2

    :pswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v11, "members"

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v2

    move v1, v9

    move-object v11, v10

    move-object/from16 v9, v19

    move-object/from16 v10, v21

    move-object/from16 v22, v4

    move v4, v3

    move/from16 v23, v2

    move-wide v2, v6

    move-object/from16 v7, v22

    move/from16 v6, v23

    goto/16 :goto_1

    :cond_9
    const-string/jumbo v11, "subscribers"

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v3

    move v1, v9

    move-object v11, v10

    move-object/from16 v9, v19

    move-object/from16 v10, v21

    move-object/from16 v22, v4

    move v4, v3

    move/from16 v23, v2

    move-wide v2, v6

    move-object/from16 v7, v22

    move/from16 v6, v23

    goto/16 :goto_1

    :cond_a
    const-string/jumbo v11, "most_recent_tweet_timestamp"

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_b

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v6

    move v1, v9

    move-object v11, v10

    move-object/from16 v9, v19

    move-object/from16 v10, v21

    move-object/from16 v22, v4

    move v4, v3

    move/from16 v23, v2

    move-wide v2, v6

    move-object/from16 v7, v22

    move/from16 v6, v23

    goto/16 :goto_1

    :cond_b
    const-string/jumbo v11, "id"

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v10, v21

    move-object v11, v1

    move v1, v9

    move-object/from16 v9, v19

    move-object/from16 v22, v4

    move v4, v3

    move/from16 v23, v2

    move-wide v2, v6

    move-object/from16 v7, v22

    move/from16 v6, v23

    goto/16 :goto_1

    :pswitch_3
    const-string/jumbo v1, "following"

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->k()Z

    move-result v1

    if-eqz v1, :cond_c

    const/4 v5, 0x1

    :goto_3
    move v1, v9

    move-object v11, v10

    move-object/from16 v9, v19

    move-object/from16 v10, v21

    move-object/from16 v22, v4

    move v4, v3

    move/from16 v23, v2

    move-wide v2, v6

    move-object/from16 v7, v22

    move/from16 v6, v23

    goto/16 :goto_1

    :cond_c
    const/4 v5, 0x2

    goto :goto_3

    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move v1, v9

    move-object v11, v10

    move-object/from16 v9, v19

    move-object/from16 v10, v21

    move-object/from16 v22, v4

    move v4, v3

    move/from16 v23, v2

    move-wide v2, v6

    move-object/from16 v7, v22

    move/from16 v6, v23

    goto/16 :goto_1

    :pswitch_5
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v11, "owner"

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    const/4 v1, 0x1

    const/4 v11, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v1, v11}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v20

    move v1, v9

    move-object v11, v10

    move-object/from16 v9, v19

    move-object/from16 v10, v21

    move-object/from16 v22, v4

    move v4, v3

    move/from16 v23, v2

    move-wide v2, v6

    move-object/from16 v7, v22

    move/from16 v6, v23

    goto/16 :goto_1

    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move v1, v9

    move-object v11, v10

    move-object/from16 v9, v19

    move-object/from16 v10, v21

    move-object/from16 v22, v4

    move v4, v3

    move/from16 v23, v2

    move-wide v2, v6

    move-object/from16 v7, v22

    move/from16 v6, v23

    goto/16 :goto_1

    :cond_e
    if-eqz v20, :cond_f

    invoke-virtual/range {v20 .. v20}, Lcom/twitter/library/api/TwitterUser;->a()J

    move-result-wide v15

    :goto_4
    new-instance v1, Lcom/twitter/library/api/TwitterTopic$TwitterList;

    const/4 v4, 0x0

    const-wide/16 v13, 0x3e8

    mul-long/2addr v6, v13

    invoke-direct/range {v1 .. v7}, Lcom/twitter/library/api/TwitterTopic$TwitterList;-><init>(IIIIJ)V

    if-nez v10, :cond_10

    move-object v6, v12

    :goto_5
    new-instance v2, Lcom/twitter/library/api/TwitterTopic;

    new-instance v3, Lcom/twitter/library/api/TwitterTopic$Metadata;

    const/4 v4, 0x0

    invoke-direct {v3, v9, v6, v4}, Lcom/twitter/library/api/TwitterTopic$Metadata;-><init>(ILjava/lang/String;Z)V

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const-wide/16 v11, 0x0

    const-wide/16 v13, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object/from16 v4, v21

    move-object/from16 v5, v19

    move-object/from16 v19, v1

    invoke-direct/range {v2 .. v20}, Lcom/twitter/library/api/TwitterTopic;-><init>(Lcom/twitter/library/api/TwitterTopic$Metadata;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJLcom/twitter/library/api/PromotedContent;Ljava/util/ArrayList;Lcom/twitter/library/api/TwitterTopic$Data;Lcom/twitter/library/api/TwitterUser;)V

    return-object v2

    :cond_f
    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v15

    goto :goto_4

    :cond_10
    move-object v6, v10

    goto :goto_5

    :cond_11
    move v1, v9

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_5
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private static az(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v0

    move-object v0, v1

    move-object v1, v3

    :goto_0
    if-eqz v1, :cond_5

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_5

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v1, v2, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "ptr"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    :goto_1
    if-eqz v1, :cond_3

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_3

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->h:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "cursor"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    :cond_3
    :goto_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :cond_4
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v1, v2, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :cond_5
    return-object v0
.end method

.method public static b(Lcom/fasterxml/jackson/core/JsonParser;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterStatus;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0, v0, p1, p2}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/api/au;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterStatus;

    move-result-object v0

    return-object v0
.end method

.method static b(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterTopic;
    .locals 23

    const/16 v16, -0x1

    const/4 v3, 0x0

    const-wide/16 v13, 0x0

    const/4 v10, 0x0

    const-wide/16 v11, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v15, 0x0

    const/16 v19, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    :goto_0
    if-eqz v2, :cond_11

    sget-object v20, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    move-object/from16 v0, v20

    if-eq v2, v0, :cond_11

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v20

    sget-object v21, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v21, v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :pswitch_0
    move v2, v15

    move/from16 v15, v16

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v16

    move-object/from16 v22, v16

    move/from16 v16, v15

    move v15, v2

    move-object/from16 v2, v22

    goto :goto_0

    :pswitch_1
    const-string/jumbo v2, "seed_hashtag"

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v7

    move v2, v15

    move/from16 v15, v16

    goto :goto_1

    :cond_1
    const-string/jumbo v2, "image_url"

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v9

    move v2, v15

    move/from16 v15, v16

    goto :goto_1

    :cond_2
    const-string/jumbo v2, "title"

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v4

    move v2, v15

    move/from16 v15, v16

    goto :goto_1

    :cond_3
    const-string/jumbo v2, "subtitle"

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v5

    move v2, v15

    move/from16 v15, v16

    goto :goto_1

    :cond_4
    const-string/jumbo v2, "query"

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v6

    move v2, v15

    move/from16 v15, v16

    goto :goto_1

    :cond_5
    const-string/jumbo v2, "reason"

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v10

    move v2, v15

    move/from16 v15, v16

    goto :goto_1

    :cond_6
    const-string/jumbo v2, "view_url"

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v8

    move v2, v15

    move/from16 v15, v16

    goto/16 :goto_1

    :pswitch_2
    const-string/jumbo v2, "tweet_count"

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v11

    move v2, v15

    move/from16 v15, v16

    goto/16 :goto_1

    :cond_7
    const-string/jumbo v2, "start_time"

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v13

    move v2, v15

    move/from16 v15, v16

    goto/16 :goto_1

    :pswitch_3
    const-string/jumbo v2, "metadata"

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    move-object/from16 v22, v2

    move-object v2, v3

    move/from16 v3, v16

    move-object/from16 v16, v22

    :goto_2
    if-eqz v16, :cond_16

    sget-object v20, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_16

    sget-object v20, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual/range {v16 .. v16}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v16

    aget v16, v20, v16

    packed-switch v16, :pswitch_data_1

    :cond_8
    :goto_3
    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v16

    goto :goto_2

    :pswitch_5
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v16

    const-string/jumbo v20, "id"

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    :cond_9
    const-string/jumbo v20, "type"

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/library/api/TwitterTopic;->c(Ljava/lang/String;)I

    move-result v3

    goto :goto_3

    :pswitch_6
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :cond_a
    const-string/jumbo v2, "tv_data"

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Lcom/twitter/library/api/ap;->A(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterTopic$TvEvent;

    move-result-object v19

    move v2, v15

    move/from16 v15, v16

    goto/16 :goto_1

    :cond_b
    const-string/jumbo v2, "local_data"

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Lcom/twitter/library/api/ap;->B(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterTopic$LocalEvent;

    move-result-object v19

    move v2, v15

    move/from16 v15, v16

    goto/16 :goto_1

    :cond_c
    const-string/jumbo v2, "sports_data"

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Lcom/twitter/library/api/ap;->E(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterTopic$SportsEvent;

    move-result-object v19

    move v2, v15

    move/from16 v15, v16

    goto/16 :goto_1

    :cond_d
    const-string/jumbo v2, "promoted_content"

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Lcom/twitter/library/api/PromotedContent;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/PromotedContent;

    move-result-object v17

    move v2, v15

    move/from16 v15, v16

    goto/16 :goto_1

    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move v2, v15

    move/from16 v15, v16

    goto/16 :goto_1

    :pswitch_7
    const-string/jumbo v2, "tweets"

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    if-nez p1, :cond_f

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Lcom/twitter/library/api/ap;->d(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v18

    move v2, v15

    move/from16 v15, v16

    goto/16 :goto_1

    :cond_f
    invoke-static/range {p0 .. p3}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v18

    move v2, v15

    move/from16 v15, v16

    goto/16 :goto_1

    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move v2, v15

    move/from16 v15, v16

    goto/16 :goto_1

    :pswitch_8
    const-string/jumbo v2, "spiking"

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    move/from16 v15, v16

    goto/16 :goto_1

    :cond_11
    const/4 v2, 0x1

    move/from16 v0, v16

    if-ne v0, v2, :cond_15

    if-nez v3, :cond_15

    invoke-static {v6}, Lcom/twitter/library/api/TwitterTopic;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v20, v3

    :goto_4
    const/4 v2, -0x1

    move/from16 v0, v16

    if-eq v0, v2, :cond_12

    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_12

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_14

    :cond_12
    if-eqz p3, :cond_13

    const-string/jumbo v2, "Failed parsing event; missing required data"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Lcom/twitter/library/util/w;->a(Ljava/lang/String;)V

    :cond_13
    const/4 v2, 0x0

    :goto_5
    return-object v2

    :cond_14
    new-instance v2, Lcom/twitter/library/api/TwitterTopic;

    new-instance v3, Lcom/twitter/library/api/TwitterTopic$Metadata;

    move/from16 v0, v16

    move-object/from16 v1, v20

    invoke-direct {v3, v0, v1, v15}, Lcom/twitter/library/api/TwitterTopic$Metadata;-><init>(ILjava/lang/String;Z)V

    const-wide/16 v15, 0x0

    const/16 v20, 0x0

    invoke-direct/range {v2 .. v20}, Lcom/twitter/library/api/TwitterTopic;-><init>(Lcom/twitter/library/api/TwitterTopic$Metadata;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJLcom/twitter/library/api/PromotedContent;Ljava/util/ArrayList;Lcom/twitter/library/api/TwitterTopic$Data;Lcom/twitter/library/api/TwitterUser;)V

    goto :goto_5

    :cond_15
    move-object/from16 v20, v3

    goto :goto_4

    :cond_16
    move/from16 v22, v15

    move v15, v3

    move-object v3, v2

    move/from16 v2, v22

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_6
    .end packed-switch
.end method

.method public static b(Ljava/lang/String;)Lcom/twitter/library/api/TwitterUser;
    .locals 3

    sget-object v0, Lcom/twitter/library/api/ap;->a:Lcom/fasterxml/jackson/core/JsonFactory;

    invoke-virtual {v0, p0}, Lcom/fasterxml/jackson/core/JsonFactory;->b(Ljava/lang/String;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/account/LvEligibilityResponse;
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v2, v0

    move v0, v1

    :goto_0
    if-eqz v2, :cond_2

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_2

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "enrolled"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->k()Z

    move-result v1

    goto :goto_1

    :cond_1
    const-string/jumbo v3, "enrolled_elsewhere"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->k()Z

    move-result v0

    goto :goto_1

    :cond_2
    new-instance v2, Lcom/twitter/library/api/account/LvEligibilityResponse;

    invoke-direct {v2, v1, v0}, Lcom/twitter/library/api/account/LvEligibilityResponse;-><init>(ZZ)V

    return-object v2

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static b(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;Lcom/twitter/library/api/TwitterUser;)Lcom/twitter/library/api/ah;
    .locals 11

    const/4 v3, 0x0

    invoke-static {}, Lcom/twitter/library/util/CollectionsUtil;->a()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v4, v0

    move-object v5, v3

    move-object v8, v3

    move-object v1, v3

    move-object v2, v3

    move-object v0, v3

    :goto_0
    if-eqz v4, :cond_c

    sget-object v7, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v7, :cond_c

    sget-object v7, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v4, v7, :cond_a

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v7, "objects"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    move-object v7, v4

    move-object v4, v2

    move-object v2, v1

    move-object v1, v8

    :goto_1
    if-eqz v7, :cond_10

    sget-object v8, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v7, v8, :cond_10

    sget-object v8, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v7, v8, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "tweets"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-static {p0, p2}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/TwitterUser;)Ljava/util/HashMap;

    move-result-object v2

    :cond_0
    :goto_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v7

    goto :goto_1

    :cond_1
    const-string/jumbo v8, "users"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-static {p0}, Lcom/twitter/library/api/ap;->aA(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;

    move-result-object v4

    goto :goto_2

    :cond_2
    const-string/jumbo v8, "timelines"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-static {p0}, Lcom/twitter/library/api/ap;->aB(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;

    move-result-object v1

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    :cond_4
    sget-object v8, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v7, v8, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    :cond_5
    const-string/jumbo v7, "response"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    move-object v7, v5

    move-object v9, v0

    move-object v0, v6

    move-object v6, v9

    :goto_3
    if-eqz v4, :cond_f

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v5, :cond_f

    sget-object v5, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v4}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v4

    aget v4, v5, v4

    packed-switch v4, :pswitch_data_0

    :cond_6
    :goto_4
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    goto :goto_3

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "timeline_id"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v6

    goto :goto_4

    :pswitch_2
    const-string/jumbo v4, "timeline"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    const/4 v4, 0x1

    move-object v0, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Ljava/util/HashMap;ZLcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_4

    :cond_7
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_4

    :pswitch_3
    const-string/jumbo v4, "position"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-static {p0, p1}, Lcom/twitter/library/api/ap;->v(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Landroid/util/Pair;

    move-result-object v7

    goto :goto_4

    :cond_8
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_4

    :cond_9
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v4, v8

    move-object v9, v6

    move-object v6, v2

    move-object v2, v9

    move-object v10, v1

    move-object v1, v5

    move-object v5, v10

    :goto_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v7

    move-object v8, v4

    move-object v4, v7

    move-object v9, v2

    move-object v2, v6

    move-object v6, v9

    move-object v10, v5

    move-object v5, v1

    move-object v1, v10

    goto/16 :goto_0

    :cond_a
    sget-object v7, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v4, v7, :cond_b

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    :cond_b
    move-object v4, v8

    move-object v9, v6

    move-object v6, v2

    move-object v2, v9

    move-object v10, v1

    move-object v1, v5

    move-object v5, v10

    goto :goto_5

    :cond_c
    if-eqz v8, :cond_e

    if-eqz v2, :cond_e

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterTopic;

    if-eqz v0, :cond_d

    iget-wide v3, v0, Lcom/twitter/library/api/TwitterTopic;->l:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/TwitterUser;

    move-object v2, v1

    move-object v1, v0

    :goto_6
    new-instance v0, Lcom/twitter/library/api/ah;

    iget-object v4, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    iget-object v5, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Ljava/lang/String;

    move-object v3, v6

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/api/ah;-><init>(Lcom/twitter/library/api/TwitterTopic;Lcom/twitter/library/api/TwitterUser;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_d
    move-object v2, v3

    move-object v1, v0

    goto :goto_6

    :cond_e
    move-object v2, v3

    move-object v1, v3

    goto :goto_6

    :cond_f
    move-object v4, v8

    move-object v5, v1

    move-object v1, v7

    move-object v9, v6

    move-object v6, v2

    move-object v2, v0

    move-object v0, v9

    goto :goto_5

    :cond_10
    move-object v9, v5

    move-object v5, v2

    move-object v2, v6

    move-object v6, v4

    move-object v4, v1

    move-object v1, v9

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static b(Lcom/fasterxml/jackson/core/JsonParser;ILcom/twitter/library/util/w;)Lcom/twitter/library/api/am;
    .locals 13

    const/4 v10, 0x0

    const-wide/16 v1, -0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v12, v0

    move-object v0, v10

    move-object v10, v12

    :goto_0
    if-eqz v10, :cond_8

    sget-object v11, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v10, v11, :cond_8

    sget-object v11, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v10}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v10

    aget v10, v11, v10

    packed-switch v10, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v10

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v10

    const-string/jumbo v11, "name"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_1
    const-string/jumbo v11, "full_name"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_2
    const-string/jumbo v11, "description"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_3
    const-string/jumbo v11, "mode"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v10, "public"

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    const/4 v8, 0x0

    goto :goto_1

    :cond_4
    const/4 v8, 0x1

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v10

    const-string/jumbo v11, "member_count"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v6

    goto :goto_1

    :cond_5
    const-string/jumbo v11, "subscriber_count"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v7

    goto :goto_1

    :cond_6
    const-string/jumbo v11, "id"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v1

    goto :goto_1

    :pswitch_2
    const-string/jumbo v10, "user"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    const/4 v9, 0x1

    invoke-static {p0, v9, p2}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v9

    goto/16 :goto_1

    :cond_7
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_8
    new-instance v0, Lcom/twitter/library/api/am;

    move v10, p1

    invoke-direct/range {v0 .. v10}, Lcom/twitter/library/api/am;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILcom/twitter/library/api/TwitterUser;I)V

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private static b(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/search/g;
    .locals 30

    const/16 v25, 0x0

    const/4 v6, 0x0

    const/16 v23, 0x0

    const/16 v22, 0x0

    const/16 v21, 0x0

    const/16 v20, 0x0

    const/16 v19, 0x0

    const/16 v18, 0x0

    const/4 v14, 0x0

    const/4 v13, 0x0

    const/16 v27, 0x0

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v28

    sget-object v4, Lcom/twitter/library/api/ap;->d:Ljava/util/HashMap;

    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    :goto_0
    if-eqz v4, :cond_13

    sget-object v7, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v7, :cond_13

    sget-object v7, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v4}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v4

    aget v4, v7, v4

    packed-switch v4, :pswitch_data_0

    :cond_0
    :pswitch_0
    move-object v4, v14

    move-object/from16 v7, v19

    move-object/from16 v8, v20

    move-object/from16 v9, v21

    move-object/from16 v10, v22

    move-object/from16 v11, v23

    move-object v12, v6

    move-object/from16 v6, v18

    move-object/from16 v14, v25

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v18

    move-object/from16 v19, v7

    move-object/from16 v20, v8

    move-object/from16 v21, v9

    move-object/from16 v22, v10

    move-object/from16 v23, v11

    move-object/from16 v25, v14

    move-object v14, v4

    move-object/from16 v4, v18

    move-object/from16 v18, v6

    move-object v6, v12

    goto :goto_0

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    const/4 v4, 0x0

    :goto_2
    return-object v4

    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v7, "metadata"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    packed-switch v5, :pswitch_data_1

    :pswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v4, v14

    move-object/from16 v7, v19

    move-object/from16 v8, v20

    move-object/from16 v9, v21

    move-object/from16 v10, v22

    move-object/from16 v11, v23

    move-object v12, v6

    move-object/from16 v6, v18

    move-object/from16 v14, v25

    goto :goto_1

    :pswitch_3
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Lcom/twitter/library/api/ap;->f(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/au;

    move-result-object v7

    if-eqz v7, :cond_2

    const-string/jumbo v4, "top"

    iget-object v8, v7, Lcom/twitter/library/api/au;->e:Ljava/lang/String;

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string/jumbo v4, "popular"

    iput-object v4, v7, Lcom/twitter/library/api/au;->e:Ljava/lang/String;

    :cond_2
    iget-object v4, v7, Lcom/twitter/library/api/au;->g:Lcom/twitter/library/api/af;

    move-object/from16 v8, v20

    move-object/from16 v9, v21

    move-object/from16 v10, v22

    move-object/from16 v11, v23

    move-object v12, v6

    move-object v6, v4

    move-object v4, v14

    move-object v14, v7

    move-object/from16 v7, v19

    goto :goto_1

    :pswitch_4
    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/ap;->F(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/TwitterUserMetadata;

    move-result-object v4

    move-object/from16 v7, v19

    move-object/from16 v8, v20

    move-object/from16 v9, v21

    move-object/from16 v10, v22

    move-object/from16 v11, v23

    move-object v12, v6

    move-object/from16 v14, v25

    move-object/from16 v6, v18

    goto :goto_1

    :cond_3
    const-string/jumbo v7, "data"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    packed-switch v5, :pswitch_data_2

    :pswitch_5
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v4, v14

    move-object/from16 v7, v19

    move-object/from16 v8, v20

    move-object/from16 v9, v21

    move-object/from16 v10, v22

    move-object/from16 v11, v23

    move-object v12, v6

    move-object/from16 v6, v18

    move-object/from16 v14, v25

    goto/16 :goto_1

    :pswitch_6
    const/4 v4, 0x1

    :try_start_0
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v25

    move-object/from16 v3, p2

    invoke-static {v0, v1, v2, v4, v3}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/api/au;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterStatus;
    :try_end_0
    .catch Lcom/twitter/library/util/NullUserException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v24

    :try_start_1
    move-object/from16 v0, v24

    iget-object v4, v0, Lcom/twitter/library/api/TwitterStatus;->v:Lcom/twitter/library/api/au;

    if-eqz v4, :cond_4

    const/4 v4, 0x4

    if-ne v5, v4, :cond_5

    move-object/from16 v0, v24

    iget-object v4, v0, Lcom/twitter/library/api/TwitterStatus;->v:Lcom/twitter/library/api/au;

    const-string/jumbo v6, "news"

    iput-object v6, v4, Lcom/twitter/library/api/au;->e:Ljava/lang/String;

    :cond_4
    :goto_3
    move-object v4, v14

    move-object/from16 v6, v18

    move-object/from16 v7, v19

    move-object/from16 v8, v20

    move-object/from16 v9, v21

    move-object/from16 v10, v22

    move-object/from16 v11, v23

    move-object/from16 v12, v24

    move-object/from16 v14, v25

    goto/16 :goto_1

    :cond_5
    move-object/from16 v0, v24

    iget-object v4, v0, Lcom/twitter/library/api/TwitterStatus;->v:Lcom/twitter/library/api/au;

    iget-object v4, v4, Lcom/twitter/library/api/au;->d:Lcom/twitter/library/api/TwitterSearchHighlight;
    :try_end_1
    .catch Lcom/twitter/library/util/NullUserException; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v4, :cond_4

    const/16 v26, 0x9

    :try_start_2
    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatus;->v:Lcom/twitter/library/api/au;

    move-object/from16 v29, v0

    new-instance v4, Lcom/twitter/library/api/TwitterSocialProof;

    const/16 v5, 0x17

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-direct/range {v4 .. v12}, Lcom/twitter/library/api/TwitterSocialProof;-><init>(ILjava/lang/String;IIIILjava/lang/String;I)V

    move-object/from16 v0, v29

    iput-object v4, v0, Lcom/twitter/library/api/au;->f:Lcom/twitter/library/api/TwitterSocialProof;
    :try_end_2
    .catch Lcom/twitter/library/util/NullUserException; {:try_start_2 .. :try_end_2} :catch_2

    move/from16 v5, v26

    goto :goto_3

    :catch_0
    move-exception v4

    :goto_4
    if-eqz p2, :cond_6

    const-string/jumbo v7, "Received null user for status = %d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-wide v10, v4, Lcom/twitter/library/util/NullUserException;->statusId:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Lcom/twitter/library/util/w;->a(Ljava/lang/String;)V

    :cond_6
    move-object v4, v14

    move-object/from16 v7, v19

    move-object/from16 v8, v20

    move-object/from16 v9, v21

    move-object/from16 v10, v22

    move-object/from16 v11, v23

    move-object v12, v6

    move-object/from16 v6, v18

    move-object/from16 v14, v25

    goto/16 :goto_1

    :pswitch_7
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v4, v1}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v4

    move-object/from16 v7, v19

    move-object/from16 v8, v20

    move-object/from16 v9, v21

    move-object/from16 v10, v22

    move-object v11, v4

    move-object v12, v6

    move-object v4, v14

    move-object/from16 v6, v18

    move-object/from16 v14, v25

    goto/16 :goto_1

    :pswitch_8
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    invoke-static {v0, v8}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/ArrayList;)I

    move-result v7

    const/4 v4, -0x1

    if-eq v7, v4, :cond_0

    packed-switch v7, :pswitch_data_3

    move v5, v7

    move-object v4, v14

    move-object/from16 v8, v20

    move-object/from16 v9, v21

    move-object/from16 v10, v22

    move-object/from16 v11, v23

    move-object v12, v6

    move-object/from16 v7, v19

    move-object/from16 v6, v18

    move-object/from16 v14, v25

    goto/16 :goto_1

    :pswitch_9
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v5, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v8, v4

    :goto_5
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/library/api/TwitterSearchSuggestion;

    iget-object v4, v4, Lcom/twitter/library/api/TwitterSearchSuggestion;->query:Ljava/lang/String;

    aput-object v4, v5, v8

    add-int/lit8 v4, v8, 0x1

    move v8, v4

    goto :goto_5

    :cond_7
    move-object v4, v14

    move-object v8, v5

    move-object/from16 v9, v21

    move-object/from16 v10, v22

    move-object/from16 v11, v23

    move-object v12, v6

    move v5, v7

    move-object/from16 v6, v18

    move-object/from16 v14, v25

    move-object/from16 v7, v19

    goto/16 :goto_1

    :pswitch_a
    const/4 v4, 0x0

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/library/api/TwitterSearchSuggestion;

    move v5, v7

    move-object/from16 v8, v20

    move-object v9, v4

    move-object/from16 v10, v22

    move-object/from16 v11, v23

    move-object v12, v6

    move-object/from16 v7, v19

    move-object v4, v14

    move-object/from16 v6, v18

    move-object/from16 v14, v25

    goto/16 :goto_1

    :pswitch_b
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Lcom/twitter/library/api/ap;->F(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v4

    move-object/from16 v7, v19

    move-object/from16 v8, v20

    move-object/from16 v9, v21

    move-object v10, v4

    move-object/from16 v11, v23

    move-object v12, v6

    move-object v4, v14

    move-object/from16 v6, v18

    move-object/from16 v14, v25

    goto/16 :goto_1

    :pswitch_c
    const-string/jumbo v4, "timeline_gallery"

    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    move-object/from16 v0, p0

    invoke-static {v0, v15}, Lcom/twitter/library/api/ap;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v17

    move-object v4, v14

    move-object/from16 v7, v19

    move-object/from16 v8, v20

    move-object/from16 v9, v21

    move-object/from16 v10, v22

    move-object/from16 v11, v23

    move-object v12, v6

    move-object/from16 v6, v18

    move-object/from16 v14, v25

    goto/16 :goto_1

    :cond_8
    const-string/jumbo v4, "timeline"

    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/ap;->ay(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/TwitterTopic;

    move-result-object v4

    :goto_6
    if-nez v4, :cond_b

    if-eqz p2, :cond_9

    const-string/jumbo v4, "Search with null or empty event"

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Lcom/twitter/library/util/w;->a(Ljava/lang/String;)V

    :cond_9
    const/4 v4, 0x0

    goto/16 :goto_2

    :cond_a
    const/4 v4, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v4, v7, v1}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterTopic;

    move-result-object v4

    goto :goto_6

    :cond_b
    invoke-virtual {v15, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v4, 0xc

    if-ne v5, v4, :cond_c

    const/16 v16, 0x1

    :cond_c
    move-object v4, v14

    move-object/from16 v7, v19

    move-object/from16 v8, v20

    move-object/from16 v9, v21

    move-object/from16 v10, v22

    move-object/from16 v11, v23

    move-object v12, v6

    move-object/from16 v6, v18

    move-object/from16 v14, v25

    goto/16 :goto_1

    :cond_d
    const-string/jumbo v7, "filter"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    const/16 v4, 0xa

    if-ne v5, v4, :cond_e

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/ap;->aq(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/TwitterSearchFilter;

    move-result-object v13

    move-object v4, v14

    move-object/from16 v7, v19

    move-object/from16 v8, v20

    move-object/from16 v9, v21

    move-object/from16 v10, v22

    move-object/from16 v11, v23

    move-object v12, v6

    move-object/from16 v6, v18

    move-object/from16 v14, v25

    goto/16 :goto_1

    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v4, v14

    move-object/from16 v7, v19

    move-object/from16 v8, v20

    move-object/from16 v9, v21

    move-object/from16 v10, v22

    move-object/from16 v11, v23

    move-object v12, v6

    move-object/from16 v6, v18

    move-object/from16 v14, v25

    goto/16 :goto_1

    :cond_f
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v4, v14

    move-object/from16 v7, v19

    move-object/from16 v8, v20

    move-object/from16 v9, v21

    move-object/from16 v10, v22

    move-object/from16 v11, v23

    move-object v12, v6

    move-object/from16 v6, v18

    move-object/from16 v14, v25

    goto/16 :goto_1

    :pswitch_d
    const-string/jumbo v4, "data"

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_12

    const/4 v4, 0x6

    if-ne v5, v4, :cond_10

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/twitter/library/api/ap;->o(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v4

    move-object v7, v4

    move-object/from16 v8, v20

    move-object/from16 v9, v21

    move-object/from16 v10, v22

    move-object/from16 v11, v23

    move-object v12, v6

    move-object v4, v14

    move-object/from16 v6, v18

    move-object/from16 v14, v25

    goto/16 :goto_1

    :cond_10
    const/16 v4, 0xa

    if-ne v5, v4, :cond_11

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Lcom/twitter/library/api/ap;->d(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v4

    move-object/from16 v7, v19

    move-object/from16 v8, v20

    move-object/from16 v9, v21

    move-object v10, v4

    move-object/from16 v11, v23

    move-object v12, v6

    move-object v4, v14

    move-object/from16 v6, v18

    move-object/from16 v14, v25

    goto/16 :goto_1

    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v4, v14

    move-object/from16 v7, v19

    move-object/from16 v8, v20

    move-object/from16 v9, v21

    move-object/from16 v10, v22

    move-object/from16 v11, v23

    move-object v12, v6

    move-object/from16 v6, v18

    move-object/from16 v14, v25

    goto/16 :goto_1

    :cond_12
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v4, v14

    move-object/from16 v7, v19

    move-object/from16 v8, v20

    move-object/from16 v9, v21

    move-object/from16 v10, v22

    move-object/from16 v11, v23

    move-object v12, v6

    move-object/from16 v6, v18

    move-object/from16 v14, v25

    goto/16 :goto_1

    :cond_13
    packed-switch v5, :pswitch_data_4

    :cond_14
    :pswitch_e
    move/from16 v14, v27

    :goto_7
    new-instance v4, Lcom/twitter/library/api/search/g;

    move-object/from16 v7, v23

    move-object/from16 v8, v22

    move-object/from16 v9, v21

    move-object/from16 v10, v20

    move-object/from16 v11, v19

    move-object/from16 v12, v18

    invoke-direct/range {v4 .. v17}, Lcom/twitter/library/api/search/g;-><init>(ILcom/twitter/library/api/TwitterStatus;Lcom/twitter/library/api/TwitterUser;Ljava/util/ArrayList;Lcom/twitter/library/api/TwitterSearchSuggestion;[Ljava/lang/String;Ljava/util/ArrayList;Lcom/twitter/library/api/af;Lcom/twitter/library/api/TwitterSearchFilter;ZLjava/util/ArrayList;ZLjava/lang/String;)V

    goto/16 :goto_2

    :pswitch_f
    if-nez v6, :cond_15

    const/4 v4, 0x0

    goto/16 :goto_2

    :cond_15
    invoke-virtual {v6}, Lcom/twitter/library/api/TwitterStatus;->f()Z

    move-result v14

    goto :goto_7

    :pswitch_10
    if-nez v23, :cond_16

    const/4 v4, 0x0

    goto/16 :goto_2

    :cond_16
    if-eqz v14, :cond_14

    move-object/from16 v0, v23

    iput-object v14, v0, Lcom/twitter/library/api/TwitterUser;->metadata:Lcom/twitter/library/api/TwitterUserMetadata;

    iget-object v4, v14, Lcom/twitter/library/api/TwitterUserMetadata;->a:Lcom/twitter/library/api/TwitterSocialProof;

    if-eqz v4, :cond_14

    move-object/from16 v0, v23

    iget v4, v0, Lcom/twitter/library/api/TwitterUser;->friendship:I

    iget-object v7, v14, Lcom/twitter/library/api/TwitterUserMetadata;->a:Lcom/twitter/library/api/TwitterSocialProof;

    iget v7, v7, Lcom/twitter/library/api/TwitterSocialProof;->f:I

    invoke-static {v4, v7}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v4

    move-object/from16 v0, v23

    iput v4, v0, Lcom/twitter/library/api/TwitterUser;->friendship:I

    move/from16 v14, v27

    goto :goto_7

    :pswitch_11
    if-nez v21, :cond_14

    if-eqz p2, :cond_17

    const-string/jumbo v4, "Search with null spelling suggestions."

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Lcom/twitter/library/util/w;->a(Ljava/lang/String;)V

    :cond_17
    const/4 v4, 0x0

    goto/16 :goto_2

    :pswitch_12
    if-eqz v20, :cond_18

    move-object/from16 v0, v20

    array-length v4, v0

    if-nez v4, :cond_14

    :cond_18
    if-eqz p2, :cond_19

    const-string/jumbo v4, "Search with null or empty related."

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Lcom/twitter/library/util/w;->a(Ljava/lang/String;)V

    :cond_19
    const/4 v4, 0x0

    goto/16 :goto_2

    :pswitch_13
    if-nez v19, :cond_14

    if-eqz p2, :cond_1a

    const-string/jumbo v4, "Search with null or empty user gallery."

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Lcom/twitter/library/util/w;->a(Ljava/lang/String;)V

    :cond_1a
    const/4 v4, 0x0

    goto/16 :goto_2

    :pswitch_14
    if-eqz v22, :cond_1b

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1d

    :cond_1b
    if-eqz p2, :cond_1c

    const-string/jumbo v4, "Search with null or empty tweet/media gallery."

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Lcom/twitter/library/util/w;->a(Ljava/lang/String;)V

    :cond_1c
    const/4 v4, 0x0

    goto/16 :goto_2

    :cond_1d
    const/4 v4, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/library/api/TwitterStatus;

    if-eqz v4, :cond_14

    iget-object v7, v4, Lcom/twitter/library/api/TwitterStatus;->v:Lcom/twitter/library/api/au;

    if-eqz v7, :cond_14

    if-eqz v25, :cond_14

    iget-object v7, v4, Lcom/twitter/library/api/TwitterStatus;->v:Lcom/twitter/library/api/au;

    move-object/from16 v0, v25

    iget-object v8, v0, Lcom/twitter/library/api/au;->i:Ljava/lang/String;

    iput-object v8, v7, Lcom/twitter/library/api/au;->i:Ljava/lang/String;

    iget-object v4, v4, Lcom/twitter/library/api/TwitterStatus;->v:Lcom/twitter/library/api/au;

    move-object/from16 v0, v25

    iget-object v7, v0, Lcom/twitter/library/api/au;->j:Ljava/lang/String;

    iput-object v7, v4, Lcom/twitter/library/api/au;->j:Ljava/lang/String;

    move/from16 v14, v27

    goto/16 :goto_7

    :catch_1
    move-exception v4

    move-object/from16 v6, v24

    goto/16 :goto_4

    :catch_2
    move-exception v4

    move/from16 v5, v26

    move-object/from16 v6, v24

    goto/16 :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
        :pswitch_5
        :pswitch_8
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_b
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_c
        :pswitch_c
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x2
        :pswitch_a
        :pswitch_9
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_f
        :pswitch_e
        :pswitch_13
        :pswitch_14
        :pswitch_e
        :pswitch_f
        :pswitch_14
    .end packed-switch
.end method

.method private static b(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Ljava/util/HashMap;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/x;
    .locals 9

    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x0

    new-instance v4, Lcom/twitter/library/api/z;

    invoke-direct {v4}, Lcom/twitter/library/api/z;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_13

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_13

    sget-object v1, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    move-object v0, v6

    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v6, v0

    move-object v0, v1

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "tweet"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0, p1, p2, v4, p5}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/library/api/z;Lcom/twitter/library/util/w;)V

    move-object v0, v6

    goto :goto_1

    :cond_1
    const-string/jumbo v1, "conversation"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {p0, p1, p2, p5}, Lcom/twitter/library/api/ap;->c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/Conversation;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v1, 0x2

    invoke-virtual {v4, v1}, Lcom/twitter/library/api/z;->a(I)Lcom/twitter/library/api/z;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/api/z;->a(Lcom/twitter/library/api/Conversation;)Lcom/twitter/library/api/z;

    move-result-object v1

    invoke-virtual {v0}, Lcom/twitter/library/api/Conversation;->a()Lcom/twitter/library/api/TwitterStatus;

    move-result-object v0

    iget-wide v2, v0, Lcom/twitter/library/api/TwitterStatus;->r:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/api/z;->a(J)Lcom/twitter/library/api/z;

    :cond_2
    move-object v0, v6

    goto :goto_1

    :cond_3
    const-string/jumbo v1, "discover_status"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {p0, p1, p2, v4, p5}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/library/api/z;Lcom/twitter/library/util/w;)V

    const-wide/16 v0, -0x1

    invoke-virtual {v4, v0, v1}, Lcom/twitter/library/api/z;->a(J)Lcom/twitter/library/api/z;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/twitter/library/api/z;->a(I)Lcom/twitter/library/api/z;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/twitter/library/api/z;->b(I)Lcom/twitter/library/api/z;

    move-object v0, v6

    goto :goto_1

    :cond_4
    const-string/jumbo v1, "event_summary_gallery"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {p0, p3, v4, p5}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Lcom/twitter/library/api/z;Lcom/twitter/library/util/w;)V

    move-object v0, v6

    goto :goto_1

    :cond_5
    const-string/jumbo v1, "user_gallery"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-static {p0, p2, v4, p5}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/Map;Lcom/twitter/library/api/z;Lcom/twitter/library/util/w;)V

    move-object v0, v6

    goto :goto_1

    :cond_6
    const-string/jumbo v1, "topic"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-static {p0, v4}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/z;)V

    move-object v0, v6

    goto :goto_1

    :cond_7
    const-string/jumbo v1, "experiment_values"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-static {p0, v4}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/z;)V

    move-object v0, v6

    goto/16 :goto_1

    :cond_8
    const-string/jumbo v1, "entity_id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_d

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_d

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->h:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_a

    const-string/jumbo v0, "type"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_9
    :goto_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_2

    :cond_a
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_c

    const-string/jumbo v0, "ids"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-static {p0}, Lcom/twitter/library/api/ap;->aC(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/LinkedHashSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    :cond_b
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :cond_c
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_9

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :cond_d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_e
    const-string/jumbo v1, "suggested"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_5
    if-eqz v0, :cond_0

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_1

    :cond_f
    :goto_6
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_5

    :pswitch_2
    const-string/jumbo v0, "suggestion_id"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/twitter/library/api/z;->b(Ljava/lang/String;)Lcom/twitter/library/api/z;

    invoke-virtual {v4, v8}, Lcom/twitter/library/api/z;->b(I)Lcom/twitter/library/api/z;

    goto :goto_6

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_6

    :cond_10
    const-string/jumbo v1, "banner"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    invoke-static {p0, p2}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;)Lcom/twitter/library/api/TwitterSocialProof;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/twitter/library/api/z;->a(Lcom/twitter/library/api/TwitterSocialProof;)Lcom/twitter/library/api/z;

    const/4 v0, 0x4

    invoke-virtual {v4, v0}, Lcom/twitter/library/api/z;->b(I)Lcom/twitter/library/api/z;

    invoke-virtual {v4}, Lcom/twitter/library/api/z;->a()Lcom/twitter/library/api/TwitterStatus;

    move-result-object v0

    iput-boolean v7, v0, Lcom/twitter/library/api/TwitterStatus;->D:Z

    move-object v0, v6

    goto/16 :goto_1

    :cond_11
    const-string/jumbo v1, "curated_tweet"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p5

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/library/api/z;Lcom/twitter/library/util/w;)V

    move-object v0, v6

    goto/16 :goto_1

    :cond_12
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v6

    goto/16 :goto_1

    :cond_13
    if-eqz p4, :cond_14

    invoke-virtual {v4}, Lcom/twitter/library/api/z;->a()Lcom/twitter/library/api/TwitterStatus;

    move-result-object v0

    if-eqz v0, :cond_14

    invoke-virtual {v4}, Lcom/twitter/library/api/z;->a()Lcom/twitter/library/api/TwitterStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/api/TwitterStatus;->b()Ljava/lang/String;

    move-result-object v6

    :cond_14
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_15

    invoke-virtual {v4, v6}, Lcom/twitter/library/api/z;->a(Ljava/lang/String;)Lcom/twitter/library/api/z;

    :cond_15
    invoke-virtual {v4}, Lcom/twitter/library/api/z;->b()Lcom/twitter/library/api/x;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private static b(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/Map;)Ljava/lang/String;
    .locals 13

    const/4 v11, 0x0

    const/4 v7, 0x0

    const/4 v3, -0x1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v10, v7

    move-object v2, v7

    move v1, v3

    :goto_0
    if-eqz v0, :cond_3

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v4, :cond_3

    sget-object v4, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v4, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :pswitch_0
    move-object v0, v10

    move-object v12, v2

    move v2, v1

    move-object v1, v12

    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    move-object v10, v0

    move-object v0, v4

    move-object v12, v1

    move v1, v2

    move-object v2, v12

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v4, "user"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    move-object v12, v2

    move v2, v1

    move-object v1, v12

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v10

    move-object v12, v2

    move v2, v1

    move-object v1, v12

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v4, "connections"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v1, 0x3

    invoke-static {p0}, Lcom/twitter/library/api/ap;->aD(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v0

    move v2, v1

    move-object v1, v0

    move-object v0, v10

    goto :goto_1

    :cond_1
    const-string/jumbo v4, "similar_to_users"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x4

    invoke-static {p0}, Lcom/twitter/library/api/ap;->aD(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v0

    move v2, v1

    move-object v1, v0

    move-object v0, v10

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v10

    move-object v12, v2

    move v2, v1

    move-object v1, v12

    goto :goto_1

    :cond_3
    invoke-interface {p1, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/twitter/library/api/TwitterUser;

    if-eqz v9, :cond_4

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    if-eq v1, v3, :cond_4

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    if-nez v0, :cond_5

    move-object v2, v7

    :goto_2
    new-instance v0, Lcom/twitter/library/api/TwitterSocialProof;

    move v4, v3

    move v5, v3

    move v6, v3

    move v8, v3

    invoke-direct/range {v0 .. v8}, Lcom/twitter/library/api/TwitterSocialProof;-><init>(ILjava/lang/String;IIIILjava/lang/String;I)V

    new-instance v1, Lcom/twitter/library/api/TwitterUserMetadata;

    invoke-direct {v1, v0, v7, v7, v11}, Lcom/twitter/library/api/TwitterUserMetadata;-><init>(Lcom/twitter/library/api/TwitterSocialProof;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v1, v9, Lcom/twitter/library/api/TwitterUser;->metadata:Lcom/twitter/library/api/TwitterUserMetadata;

    :cond_4
    return-object v10

    :cond_5
    iget-object v2, v0, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static b(Lcom/fasterxml/jackson/core/JsonParser;I)Ljava/util/ArrayList;
    .locals 5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v4, :cond_1

    sget-object v4, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v4, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-static {p0, v1, v2, p1}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;JI)Lcom/twitter/library/api/search/TwitterTypeAhead;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_1
    return-object v3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    invoke-static {p0, v0, p1}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method private static b(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    if-eqz v0, :cond_1

    new-instance v2, Lcom/twitter/library/api/Conversation$Participant;

    invoke-direct {v2, v0}, Lcom/twitter/library/api/Conversation$Participant;-><init>(Lcom/twitter/library/api/TwitterUser;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    if-eqz p2, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Participant "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " not in users map"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/twitter/library/util/w;->a(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_2
    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static b(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/z;)V
    .locals 21

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v2, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object/from16 v20, v1

    move-object v1, v2

    move-object/from16 v2, v20

    :goto_0
    if-eqz v2, :cond_6

    sget-object v7, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v7, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v8, v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    move-object/from16 v20, v4

    move-object v4, v3

    move-object/from16 v3, v20

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    move-object/from16 v20, v3

    move-object v3, v4

    move-object/from16 v4, v20

    goto :goto_0

    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object/from16 v20, v4

    move-object v4, v3

    move-object/from16 v3, v20

    goto :goto_1

    :pswitch_2
    invoke-virtual {v7}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/library/api/TwitterTopic;->c(Ljava/lang/String;)I

    move-result v2

    const/4 v8, -0x1

    if-eq v2, v8, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v2, v5

    move-object v5, v1

    move-object v1, v6

    move-object/from16 v20, v4

    move-object v4, v3

    move-object/from16 v3, v20

    :goto_2
    if-eqz v5, :cond_4

    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v5, v6, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v6

    sget-object v8, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v5}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v5

    aget v5, v8, v5

    packed-switch v5, :pswitch_data_1

    :cond_0
    :goto_3
    :pswitch_3
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v5

    goto :goto_2

    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :pswitch_5
    const-string/jumbo v5, "name"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v4

    goto :goto_3

    :cond_1
    const-string/jumbo v5, "query"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    :cond_2
    const-string/jumbo v5, "seed_hashtag"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    :cond_3
    const-string/jumbo v5, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    :cond_4
    move-object v6, v1

    move-object v5, v2

    move-object v1, v7

    goto/16 :goto_1

    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object/from16 v20, v4

    move-object v4, v3

    move-object/from16 v3, v20

    goto/16 :goto_1

    :cond_6
    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/api/TwitterTopic;->c(Ljava/lang/String;)I

    move-result v7

    new-instance v1, Lcom/twitter/library/api/TwitterTopic;

    new-instance v2, Lcom/twitter/library/api/TwitterTopic$Metadata;

    const/4 v8, 0x0

    invoke-direct {v2, v7, v4, v8}, Lcom/twitter/library/api/TwitterTopic$Metadata;-><init>(ILjava/lang/String;Z)V

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, 0x1

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    invoke-direct/range {v1 .. v19}, Lcom/twitter/library/api/TwitterTopic;-><init>(Lcom/twitter/library/api/TwitterTopic$Metadata;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJLcom/twitter/library/api/PromotedContent;Ljava/util/ArrayList;Lcom/twitter/library/api/TwitterTopic$Data;Lcom/twitter/library/api/TwitterUser;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/z;->a(Lcom/twitter/library/api/TwitterTopic;)Lcom/twitter/library/api/z;

    :cond_7
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static b(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/ArrayList;)V
    .locals 8

    const/4 v3, -0x1

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    :goto_0
    if-eqz v1, :cond_8

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_8

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v1, v2, :cond_7

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    :goto_1
    if-eqz v1, :cond_6

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_6

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_2
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_1

    :pswitch_1
    const-string/jumbo v1, "query"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :pswitch_2
    const-string/jumbo v1, "indices"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    :goto_3
    if-eqz v1, :cond_0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_0

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v4

    aget v2, v2, v4

    packed-switch v2, :pswitch_data_1

    :pswitch_3
    move v1, v3

    move v2, v3

    :cond_1
    :goto_4
    if-le v2, v3, :cond_3

    if-ge v2, v1, :cond_3

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-gt v1, v4, :cond_3

    :cond_2
    const/4 v4, 0x2

    new-array v4, v4, [I

    const/4 v6, 0x0

    aput v2, v4, v6

    const/4 v2, 0x1

    aput v1, v4, v2

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_3

    :goto_5
    if-eqz v4, :cond_1

    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v6, :cond_1

    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->i:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v4, v6, :cond_4

    if-ne v2, v3, :cond_5

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v2

    :cond_4
    :goto_6
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    goto :goto_5

    :cond_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v1

    goto :goto_6

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move v1, v3

    move v2, v3

    goto :goto_4

    :pswitch_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    :cond_6
    new-instance v1, Lcom/twitter/library/api/TwitterSearchSuggestion;

    invoke-direct {v1, v0, v5}, Lcom/twitter/library/api/TwitterSearchSuggestion;-><init>(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_7
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto/16 :goto_0

    :cond_8
    return-void

    :pswitch_6
    move v2, v3

    move-object v4, v1

    move v1, v3

    goto :goto_5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static b(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 11

    const/4 v2, 0x0

    if-eqz p0, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v1, v2

    move v3, v2

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/Entity;

    move v6, v3

    move v5, v2

    move v4, v3

    move v3, v1

    :goto_1
    if-ge v6, v7, :cond_3

    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    aget v9, v1, v2

    const/4 v10, 0x1

    aget v1, v1, v10

    sub-int v9, v1, v9

    iget v10, v0, Lcom/twitter/library/api/Entity;->start:I

    if-ge v1, v10, :cond_2

    add-int/2addr v3, v9

    add-int/lit8 v4, v4, 0x1

    move v1, v5

    :goto_2
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    move v5, v1

    goto :goto_1

    :cond_2
    iget v10, v0, Lcom/twitter/library/api/Entity;->end:I

    if-ge v1, v10, :cond_4

    add-int v1, v5, v9

    goto :goto_2

    :cond_3
    iget v1, v0, Lcom/twitter/library/api/Entity;->start:I

    sub-int/2addr v1, v3

    iput v1, v0, Lcom/twitter/library/api/Entity;->start:I

    iget v1, v0, Lcom/twitter/library/api/Entity;->end:I

    add-int/2addr v5, v3

    sub-int/2addr v1, v5

    iput v1, v0, Lcom/twitter/library/api/Entity;->end:I

    move v1, v3

    move v3, v4

    goto :goto_0

    :cond_4
    move v1, v5

    goto :goto_2
.end method

.method public static c(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Landroid/util/Pair;
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_6

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v3, :cond_6

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v3, :cond_5

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v0, "email"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "phone"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "screen_name"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_5

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v4, :cond_5

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v4, :cond_2

    const/4 v0, 0x1

    invoke-static {p0, v0, p1}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_1

    :cond_2
    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->m:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v4, :cond_3

    const-string/jumbo v0, "email"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v4, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    :cond_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    :cond_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_6
    new-instance v0, Landroid/util/Pair;

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method private static c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/Conversation;
    .locals 6

    const/4 v2, 0x0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v1, v2

    :goto_0
    if-eqz v0, :cond_4

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v4, :cond_4

    sget-object v4, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v4, v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move-object v0, v1

    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v4, "context"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0, p2, p3}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/Conversation$Metadata;

    move-result-object v0

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v1

    goto :goto_1

    :pswitch_2
    const-string/jumbo v0, "ids"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0}, Lcom/twitter/library/api/ap;->aC(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/LinkedHashSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, p1, p2, p3}, Lcom/twitter/library/api/ap;->a(Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterStatus;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v1

    goto :goto_1

    :cond_4
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    new-instance v2, Lcom/twitter/library/api/Conversation;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/twitter/library/api/TwitterStatus;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/api/TwitterStatus;

    invoke-direct {v2, v1, v0}, Lcom/twitter/library/api/Conversation;-><init>(Lcom/twitter/library/api/Conversation$Metadata;[Lcom/twitter/library/api/TwitterStatus;)V

    move-object v0, v2

    :goto_3
    return-object v0

    :cond_5
    move-object v0, v2

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static c(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/account/LoginVerificationRequest;
    .locals 8

    const-string/jumbo v1, ""

    const-string/jumbo v2, ""

    const-string/jumbo v3, ""

    const-string/jumbo v4, ""

    const-wide/16 v5, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    sget-object v7, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v7, :cond_4

    sget-object v7, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v7, v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "id"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_1
    const-string/jumbo v0, "challenge"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_2
    const-string/jumbo v0, "geo"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_3
    const-string/jumbo v0, "browser"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :pswitch_2
    const-string/jumbo v0, "createdAt"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v5

    goto :goto_1

    :cond_4
    new-instance v0, Lcom/twitter/library/api/account/LoginVerificationRequest;

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/api/account/LoginVerificationRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static c(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v4, v0

    move-object v0, v1

    move-object v1, v4

    :goto_0
    if-eqz v1, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    const-string/jumbo v1, "default_pivot"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    invoke-static {p0}, Lcom/twitter/library/api/ap;->ay(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/TwitterTopic;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_3
    const-string/jumbo v1, "timeline_items"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_1
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0, v0}, Lcom/twitter/library/api/ap;->a(Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/au;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static c(Lcom/fasterxml/jackson/core/JsonParser;ILcom/twitter/library/util/w;)Ljava/util/ArrayList;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_0

    invoke-static {p0, p1, p2}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;ILcom/twitter/library/util/w;)Lcom/twitter/library/api/am;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method private static c(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_0

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_1

    :cond_1
    :goto_3
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_2

    :pswitch_3
    invoke-static {p0, p1, p2}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/search/g;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :pswitch_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_2
    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static d(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_0

    invoke-static {p0}, Lcom/twitter/library/api/ap;->c(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/account/LoginVerificationRequest;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public static d(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-static {p0, p1, v0, v1, v1}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;Lcom/twitter/library/api/TwitterUser;ZZ)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public static d(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 14

    const/4 v13, 0x2

    const/4 v6, 0x0

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v9

    const/4 v4, 0x0

    :cond_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    const-string/jumbo v1, "errors"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "warnings"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    invoke-virtual {v8, v2}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lorg/json/JSONArray;

    move-object v3, v0

    move v5, v6

    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v5, v1, :cond_0

    invoke-virtual {v3, v5}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/json/JSONObject;

    const-string/jumbo v4, "eventName"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v10, Landroid/util/Pair;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x2

    invoke-virtual {v1, v12}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v10, v11, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_0

    :cond_2
    const-string/jumbo v1, "logs"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v8, v2}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lorg/json/JSONArray;

    move-object v3, v0

    move v5, v6

    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v5, v1, :cond_0

    invoke-virtual {v3, v5}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/json/JSONArray;

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-lt v2, v13, :cond_4

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/json/JSONObject;

    const/4 v10, 0x1

    invoke-virtual {v1, v10}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string/jumbo v10, "client_event"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    const-string/jumbo v4, "eventName"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_3
    :goto_2
    new-instance v10, Landroid/util/Pair;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v11, "]"

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v11, 0x2

    invoke-virtual {v2, v11}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v10, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    move-object v1, v4

    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move-object v4, v1

    goto :goto_1

    :cond_5
    const-string/jumbo v10, "perftown"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "product"

    invoke-virtual {v2, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v10, ":"

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v10, "description"

    invoke-virtual {v2, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    :cond_6
    const-string/jumbo v10, "client_watch_error"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    const-string/jumbo v4, "error"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_2

    :catch_0
    move-exception v1

    :cond_7
    return-object v7
.end method

.method private static e(Ljava/lang/String;)Landroid/util/Pair;
    .locals 5

    const/4 v4, -0x1

    const/4 v0, 0x0

    if-eqz p0, :cond_3

    const-string/jumbo v1, "<a"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x3e

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-eq v1, v4, :cond_0

    const/16 v2, 0x3c

    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v2

    if-eq v2, v4, :cond_0

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "href=\""

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v4, :cond_2

    add-int/lit8 v2, v2, 0x6

    const/16 v3, 0x22

    invoke-virtual {p0, v3, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v3

    if-eq v3, v4, :cond_1

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    move-object p0, v1

    :cond_0
    :goto_0
    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, p0, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_1
    move-object p0, v1

    goto :goto_0

    :cond_2
    move-object p0, v1

    goto :goto_0

    :cond_3
    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, v0, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_1
.end method

.method public static e(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterStatus$Translation;
    .locals 11

    const-wide/16 v8, -0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v7, v1

    move-object v4, v0

    move-object v3, v0

    move-object v6, v0

    move-object v5, v0

    move-wide v1, v8

    :goto_0
    if-eqz v7, :cond_4

    sget-object v10, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v7, v10, :cond_4

    sget-object v10, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v7}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v7

    aget v7, v10, v7

    packed-switch v7, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v7

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v10, "text"

    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_1
    const-string/jumbo v10, "lang"

    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_2
    const-string/jumbo v10, "translated_lang"

    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v10, "id"

    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v1

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v10, "entities"

    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-static {p0}, Lcom/twitter/library/api/TweetEntities;->a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/TweetEntities;

    move-result-object v6

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_4
    cmp-long v7, v1, v8

    if-nez v7, :cond_6

    if-eqz p1, :cond_5

    const-string/jumbo v1, "Received null status while attempting translation."

    invoke-interface {p1, v1}, Lcom/twitter/library/util/w;->a(Ljava/lang/String;)V

    :cond_5
    :goto_2
    return-object v0

    :cond_6
    if-eqz v3, :cond_7

    if-nez v4, :cond_8

    :cond_7
    if-eqz p1, :cond_5

    const-string/jumbo v1, "No language info received about status"

    invoke-interface {p1, v1}, Lcom/twitter/library/util/w;->a(Ljava/lang/String;)V

    goto :goto_2

    :cond_8
    new-instance v0, Lcom/twitter/library/api/TwitterStatus$Translation;

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/api/TwitterStatus$Translation;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public static e(Lcom/fasterxml/jackson/core/JsonParser;)Z
    .locals 2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_3

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_3

    sget-object v1, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "phone"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_0

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v1, v0

    sparse-switch v0, :sswitch_data_0

    :cond_1
    :goto_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_2

    :sswitch_0
    const-string/jumbo v0, "verified"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_4
    return v0

    :sswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x4 -> :sswitch_1
        0x7 -> :sswitch_0
    .end sparse-switch
.end method

.method public static f(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/au;
    .locals 22

    const/16 v16, 0x0

    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    const/4 v15, 0x0

    const/4 v14, 0x0

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    :goto_0
    if-eqz v2, :cond_17

    sget-object v8, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v8, :cond_17

    sget-object v8, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v8, v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :pswitch_0
    move-object v2, v13

    move v8, v14

    move v13, v15

    move-object/from16 v14, v16

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v15

    move-object/from16 v16, v14

    move v14, v8

    move-object/from16 v21, v2

    move-object v2, v15

    move v15, v13

    move-object/from16 v13, v21

    goto :goto_0

    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v8, "result_type"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v2

    move v8, v14

    move-object v14, v2

    move-object v2, v13

    move v13, v15

    goto :goto_1

    :cond_1
    const-string/jumbo v8, "highlight_type"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v3

    move-object v2, v13

    move v8, v14

    move v13, v15

    move-object/from16 v14, v16

    goto :goto_1

    :pswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v8, "auto_expand"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v2, 0x1

    move v8, v14

    move-object/from16 v14, v16

    move/from16 v21, v2

    move-object v2, v13

    move/from16 v13, v21

    goto :goto_1

    :cond_2
    const-string/jumbo v8, "pinned_to_top"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    move v8, v2

    move-object/from16 v14, v16

    move-object v2, v13

    move v13, v15

    goto :goto_1

    :pswitch_3
    const-string/jumbo v2, "hit_highlights"

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    :goto_2
    if-eqz v2, :cond_0

    sget-object v8, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v8, :cond_0

    sget-object v8, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v2, v8, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    :goto_3
    if-eqz v2, :cond_a

    sget-object v8, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v8, :cond_a

    sget-object v8, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v2, v8, :cond_8

    const-string/jumbo v2, "indices"

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v8, -0x1

    const/4 v2, -0x1

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v17

    :goto_4
    if-eqz v17, :cond_6

    sget-object v19, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_6

    sget-object v19, Lcom/fasterxml/jackson/core/JsonToken;->i:Lcom/fasterxml/jackson/core/JsonToken;

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_5

    const/16 v17, -0x1

    move/from16 v0, v17

    if-ne v8, v0, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v8

    :cond_3
    :goto_5
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v17

    goto :goto_4

    :cond_4
    const/16 v17, -0x1

    move/from16 v0, v17

    if-ne v2, v0, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v2

    goto :goto_5

    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_5

    :cond_6
    const/16 v17, -0x1

    move/from16 v0, v17

    if-le v8, v0, :cond_7

    if-ge v8, v2, :cond_7

    new-instance v17, Lcom/twitter/library/api/Entity;

    invoke-direct/range {v17 .. v17}, Lcom/twitter/library/api/Entity;-><init>()V

    move-object/from16 v0, v17

    iput v8, v0, Lcom/twitter/library/api/Entity;->start:I

    move-object/from16 v0, v17

    iput v2, v0, Lcom/twitter/library/api/Entity;->end:I

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_7
    :goto_6
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_3

    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_6

    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto/16 :goto_2

    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v2, v13

    move v8, v14

    move v13, v15

    move-object/from16 v14, v16

    goto/16 :goto_1

    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v8, "social_context"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_c

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/ap;->H(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/TwitterSocialProof;

    move-result-object v2

    move v8, v14

    move v13, v15

    move-object/from16 v14, v16

    goto/16 :goto_1

    :cond_c
    const-string/jumbo v8, "highlight_context"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_d

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/ap;->G(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/TwitterSocialProof;

    move-result-object v2

    move v8, v14

    move v13, v15

    move-object/from16 v14, v16

    goto/16 :goto_1

    :cond_d
    const-string/jumbo v8, "summary_context"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_e

    if-nez v13, :cond_e

    invoke-static/range {p0 .. p1}, Lcom/twitter/library/api/ap;->r(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterSocialProof;

    move-result-object v2

    move v8, v14

    move v13, v15

    move-object/from16 v14, v16

    goto/16 :goto_1

    :cond_e
    const-string/jumbo v8, "reason"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_11

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    :goto_7
    if-eqz v2, :cond_0

    sget-object v8, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v8, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v8

    sget-object v17, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v17, v2

    packed-switch v2, :pswitch_data_1

    :cond_f
    :goto_8
    :pswitch_5
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_7

    :pswitch_6
    const-string/jumbo v2, "text"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v11

    goto :goto_8

    :cond_10
    const-string/jumbo v2, "icon_type"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v12

    goto :goto_8

    :pswitch_7
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_8

    :cond_11
    const-string/jumbo v8, "highlight_time_range"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_14

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    :goto_9
    if-eqz v2, :cond_0

    sget-object v8, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v8, :cond_0

    sget-object v8, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v8, v2

    packed-switch v2, :pswitch_data_2

    :cond_12
    :goto_a
    :pswitch_8
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_9

    :pswitch_9
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v8, "since"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_13

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v4

    goto :goto_a

    :cond_13
    const-string/jumbo v8, "until"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v6

    goto :goto_a

    :pswitch_a
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_a

    :cond_14
    const-string/jumbo v8, "cluster"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_15

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/ap;->K(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/af;

    move-result-object v9

    move-object v2, v13

    move v8, v14

    move v13, v15

    move-object/from16 v14, v16

    goto/16 :goto_1

    :cond_15
    const-string/jumbo v8, "story"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    invoke-static/range {p0 .. p1}, Lcom/twitter/library/api/ap;->I(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/DiscoverStoryMetadata;

    move-result-object v10

    move-object v2, v13

    move v8, v14

    move v13, v15

    move-object/from16 v14, v16

    goto/16 :goto_1

    :cond_16
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v2, v13

    move v8, v14

    move v13, v15

    move-object/from16 v14, v16

    goto/16 :goto_1

    :cond_17
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1a

    const-wide/16 v19, 0x0

    cmp-long v2, v4, v19

    if-eqz v2, :cond_18

    const-wide/16 v19, 0x0

    cmp-long v2, v6, v19

    if-eqz v2, :cond_18

    new-instance v2, Lcom/twitter/library/api/TwitterSearchHighlight;

    invoke-direct/range {v2 .. v7}, Lcom/twitter/library/api/TwitterSearchHighlight;-><init>(Ljava/lang/String;JJ)V

    move-object v8, v2

    :goto_b
    new-instance v2, Lcom/twitter/library/api/au;

    move-object/from16 v3, v16

    move-object/from16 v4, v18

    move v5, v15

    move-object v6, v13

    move v7, v14

    invoke-direct/range {v2 .. v12}, Lcom/twitter/library/api/au;-><init>(Ljava/lang/String;Ljava/util/ArrayList;ZLcom/twitter/library/api/TwitterSocialProof;ZLcom/twitter/library/api/TwitterSearchHighlight;Lcom/twitter/library/api/af;Lcom/twitter/library/api/DiscoverStoryMetadata;Ljava/lang/String;Ljava/lang/String;)V

    return-object v2

    :cond_18
    if-eqz p1, :cond_19

    const-string/jumbo v2, "Search highlight tweet does not have drill down details."

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Lcom/twitter/library/util/w;->a(Ljava/lang/String;)V

    :cond_19
    const/4 v8, 0x0

    goto :goto_b

    :cond_1a
    const/4 v8, 0x0

    goto :goto_b

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_7
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_a
        :pswitch_8
        :pswitch_8
        :pswitch_a
        :pswitch_9
    .end packed-switch
.end method

.method public static f(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v0

    move-object v0, v1

    move-object v1, v3

    :goto_0
    if-eqz v1, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_1

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    const-string/jumbo v1, "token"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_1
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static g(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v0

    move-object v0, v1

    move-object v1, v3

    :goto_0
    if-eqz v1, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_1

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    const-string/jumbo v1, "phone_number"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_1
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static g(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_0

    invoke-static {p0, p1}, Lcom/twitter/library/api/ap;->h(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/ak;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public static h(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/ak;
    .locals 14

    const/4 v4, 0x0

    const-wide/16 v1, -0x1

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_5

    sget-object v10, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v10, :cond_5

    sget-object v10, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v10, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    move v0, v9

    move-object v11, v5

    move-object v5, v4

    move-object v12, v3

    move-wide v3, v1

    move-object v2, v12

    move-object v1, v11

    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v9

    move-object v11, v9

    move v9, v0

    move-object v0, v11

    move-object v12, v1

    move-object v13, v2

    move-wide v1, v3

    move-object v3, v13

    move-object v4, v5

    move-object v5, v12

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v10, "created_at"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    move-object v11, v5

    move-object v5, v4

    move-wide v3, v1

    move-object v2, v0

    move-object v1, v11

    move v0, v9

    goto :goto_1

    :cond_1
    const-string/jumbo v10, "text"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    move-object v5, v4

    move-object v11, v0

    move v0, v9

    move-object v12, v3

    move-wide v3, v1

    move-object v2, v12

    move-object v1, v11

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v10, "id"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v1

    move v0, v9

    move-object v11, v5

    move-object v5, v4

    move-object v12, v3

    move-wide v3, v1

    move-object v2, v12

    move-object v1, v11

    goto :goto_1

    :pswitch_2
    const-string/jumbo v0, "read"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    move-object v11, v5

    move-object v5, v4

    move-object v12, v3

    move-wide v3, v1

    move-object v2, v12

    move-object v1, v11

    goto :goto_1

    :pswitch_3
    const-string/jumbo v0, "sender"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    invoke-static {p0, v0, p1}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v6

    move v0, v9

    move-object v11, v5

    move-object v5, v4

    move-object v12, v3

    move-wide v3, v1

    move-object v2, v12

    move-object v1, v11

    goto :goto_1

    :cond_2
    const-string/jumbo v0, "recipient"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    invoke-static {p0, v0, p1}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v7

    move v0, v9

    move-object v11, v5

    move-object v5, v4

    move-object v12, v3

    move-wide v3, v1

    move-object v2, v12

    move-object v1, v11

    goto/16 :goto_1

    :cond_3
    const-string/jumbo v0, "entities"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p0}, Lcom/twitter/library/api/TweetEntities;->a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/TweetEntities;

    move-result-object v8

    move v0, v9

    move-object v11, v5

    move-object v5, v4

    move-object v12, v3

    move-wide v3, v1

    move-object v2, v12

    move-object v1, v11

    goto/16 :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move v0, v9

    move-object v11, v5

    move-object v5, v4

    move-object v12, v3

    move-wide v3, v1

    move-object v2, v12

    move-object v1, v11

    goto/16 :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move v0, v9

    move-object v11, v5

    move-object v5, v4

    move-object v12, v3

    move-wide v3, v1

    move-object v2, v12

    move-object v1, v11

    goto/16 :goto_1

    :pswitch_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    move-object v11, v5

    move-object v5, v0

    move v0, v9

    move-object v12, v3

    move-wide v3, v1

    move-object v2, v12

    move-object v1, v11

    goto/16 :goto_1

    :cond_5
    new-instance v0, Lcom/twitter/library/api/ak;

    sget-object v4, Lcom/twitter/internal/util/k;->b:Ljava/text/SimpleDateFormat;

    invoke-static {v4, v3}, Lcom/twitter/library/util/Util;->a(Ljava/text/SimpleDateFormat;Ljava/lang/String;)J

    move-result-wide v3

    const/4 v10, 0x0

    invoke-static {v5, v8, v10}, Lcom/twitter/library/api/ap;->a(Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/au;)Ljava/lang/String;

    move-result-object v5

    if-nez v9, :cond_6

    const/4 v9, 0x1

    :goto_2
    invoke-direct/range {v0 .. v9}, Lcom/twitter/library/api/ak;-><init>(JJLjava/lang/String;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/api/TweetEntities;Z)V

    return-object v0

    :cond_6
    const/4 v9, 0x0

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static h(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Long;
    .locals 4

    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v0

    move-object v0, v1

    move-object v1, v3

    :goto_0
    if-eqz v1, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_1

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    const-string/jumbo v1, "media_id_string"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_1
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static i(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/network/LoginResponse;
    .locals 11

    const/4 v7, 0x0

    const-string/jumbo v5, ""

    const-string/jumbo v1, ""

    const-string/jumbo v4, ""

    const-string/jumbo v6, ""

    const-wide/16 v2, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v8, v0

    move-object v0, v1

    move-object v1, v5

    move v5, v7

    :goto_0
    sget-object v9, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v8, v9, :cond_6

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v8}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v8

    aget v8, v10, v8

    packed-switch v8, :pswitch_data_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v8

    goto :goto_0

    :pswitch_1
    const-string/jumbo v8, "oauth_token"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_1
    const-string/jumbo v8, "oauth_token_secret"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    const-string/jumbo v8, "login_verification_request_id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_3
    const-string/jumbo v8, "login_verification_request_url"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    :pswitch_2
    const-string/jumbo v8, "login_verification_user_id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v2

    goto :goto_1

    :cond_4
    const-string/jumbo v8, "login_verification_request_type"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v5

    goto :goto_1

    :cond_5
    const-string/jumbo v8, "login_verification_request_cause"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v7

    goto :goto_1

    :cond_6
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_7

    new-instance v2, Lcom/twitter/library/network/LoginResponse;

    const/4 v3, 0x1

    invoke-direct {v2, v3, v1, v0}, Lcom/twitter/library/network/LoginResponse;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    :goto_2
    return-object v0

    :cond_7
    new-instance v0, Lcom/twitter/library/network/LoginResponse;

    const/4 v1, 0x2

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/network/LoginResponse;-><init>(IJLjava/lang/String;ILjava/lang/String;I)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static i(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;
    .locals 4

    const/4 v0, 0x0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    :goto_0
    if-eqz v2, :cond_1

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_1

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    :pswitch_1
    invoke-static {p0, v0, v0, p1}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;Ljava/util/HashMap;Ljava/util/HashMap;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterTopic;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_2
    return-object v0

    :cond_2
    move-object v0, v1

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static j(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/RateLimit;
    .locals 9

    const/4 v3, 0x0

    const-wide/16 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v4, v0

    move-wide v7, v1

    move-wide v0, v7

    move v2, v3

    :goto_0
    if-eqz v4, :cond_3

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v5, :cond_3

    sget-object v5, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v4}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v4

    aget v4, v5, v4

    packed-switch v4, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "remaining_hits"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v3

    goto :goto_1

    :cond_1
    const-string/jumbo v5, "hourly_limit"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v2

    goto :goto_1

    :cond_2
    const-string/jumbo v5, "reset_time_in_seconds"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v0

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_3
    new-instance v4, Lcom/twitter/library/api/RateLimit;

    const-wide/16 v5, 0x3e8

    mul-long/2addr v0, v5

    invoke-direct {v4, v3, v2, v0, v1}, Lcom/twitter/library/api/RateLimit;-><init>(IIJ)V

    return-object v4

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static j(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;
    .locals 21

    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    const/4 v8, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_5

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_5

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    :goto_2
    if-eqz v1, :cond_4

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_4

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_1

    :cond_1
    :goto_3
    :pswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_2

    :pswitch_3
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "name"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    :cond_2
    const-string/jumbo v2, "image_url"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v8

    goto :goto_3

    :cond_3
    const-string/jumbo v1, "timeline_id"

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v5

    goto :goto_3

    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :cond_4
    if-eqz v3, :cond_0

    if-eqz v5, :cond_0

    new-instance v1, Lcom/twitter/library/api/TwitterTopic;

    new-instance v2, Lcom/twitter/library/api/TwitterTopic$Metadata;

    const/4 v4, 0x5

    const/4 v6, 0x0

    invoke-direct {v2, v4, v5, v6}, Lcom/twitter/library/api/TwitterTopic$Metadata;-><init>(ILjava/lang/String;Z)V

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    invoke-direct/range {v1 .. v19}, Lcom/twitter/library/api/TwitterTopic;-><init>(Lcom/twitter/library/api/TwitterTopic$Metadata;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJLcom/twitter/library/api/PromotedContent;Ljava/util/ArrayList;Lcom/twitter/library/api/TwitterTopic$Data;Lcom/twitter/library/api/TwitterUser;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_5
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_5
    return-object v20

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public static k(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->h:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public static k(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;
    .locals 8

    const/4 v6, 0x0

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v1, :cond_5

    move-object v2, v6

    move-object v1, v6

    :goto_0
    if-eqz v0, :cond_5

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v3, :cond_5

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v3, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v5, v6

    :goto_1
    if-eqz v0, :cond_3

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v3, :cond_3

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_2
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "name"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_1
    const-string/jumbo v3, "slug"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :pswitch_2
    const-string/jumbo v0, "users"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0, p1}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v5

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    :cond_3
    if-eqz v1, :cond_4

    if-eqz v2, :cond_4

    new-instance v0, Lcom/twitter/library/api/search/TwitterSearchQuery;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    int-to-long v3, v3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/api/search/TwitterSearchQuery;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/util/ArrayList;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_5
    return-object v7

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static l(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_0

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_0
    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static l(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    move-object v1, v0

    :goto_0
    if-eqz v2, :cond_2

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_2

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    :pswitch_1
    const-string/jumbo v3, "users"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {p0, p1}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_1

    :cond_1
    :goto_2
    if-eqz v2, :cond_0

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_2

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    return-object v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static m(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/ClientConfiguration;
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v2, v0

    move-object v0, v1

    :goto_0
    if-eqz v2, :cond_2

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_2

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "access"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {p0}, Lcom/twitter/library/api/ap;->aw(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/ad;

    move-result-object v1

    goto :goto_1

    :cond_0
    const-string/jumbo v3, "twitter"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p0}, Lcom/twitter/library/api/ap;->ah(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/UrlConfiguration;

    move-result-object v0

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_2
    new-instance v2, Lcom/twitter/library/api/ClientConfiguration;

    invoke-direct {v2, v0, v1}, Lcom/twitter/library/api/ClientConfiguration;-><init>(Lcom/twitter/library/api/UrlConfiguration;Lcom/twitter/library/api/ad;)V

    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static m(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    invoke-static {p0, p1}, Lcom/twitter/library/api/ap;->n(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/ae;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public static n(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/DeviceOperatorConfiguration;
    .locals 7

    const-string/jumbo v3, ""

    const-string/jumbo v2, ""

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v6, v0

    move v0, v1

    move-object v1, v2

    move-object v2, v3

    move-object v3, v6

    :goto_0
    if-eqz v3, :cond_2

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v3, v4, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v3}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v3

    aget v3, v5, v3

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    goto :goto_0

    :pswitch_1
    const-string/jumbo v3, "shortcode"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    const-string/jumbo v3, "mode"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :pswitch_2
    const-string/jumbo v3, "mt_sms"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_2
    new-instance v3, Lcom/twitter/library/api/DeviceOperatorConfiguration;

    invoke-direct {v3, v2, v0, v1}, Lcom/twitter/library/api/DeviceOperatorConfiguration;-><init>(Ljava/lang/String;ZLjava/lang/String;)V

    return-object v3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static n(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/ae;
    .locals 27

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    const-wide/16 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const-wide/16 v24, -0x1

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    :goto_0
    if-eqz v2, :cond_d

    sget-object v26, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    move-object/from16 v0, v26

    if-eq v2, v0, :cond_d

    sget-object v26, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v26, v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v26, "action"

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-nez v26, :cond_1

    const-string/jumbo v26, "event"

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_2

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v2

    sget-object v26, Lcom/twitter/library/api/ap;->b:Ljava/util/HashMap;

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/ar;

    if-eqz v2, :cond_0

    iget v3, v2, Lcom/twitter/library/api/ar;->a:I

    iget v11, v2, Lcom/twitter/library/api/ar;->b:I

    iget v14, v2, Lcom/twitter/library/api/ar;->c:I

    iget v0, v2, Lcom/twitter/library/api/ar;->d:I

    move/from16 v19, v0

    goto :goto_1

    :cond_2
    const-string/jumbo v26, "created_at"

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_3

    sget-object v2, Lcom/twitter/internal/util/k;->b:Ljava/text/SimpleDateFormat;

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/twitter/library/util/Util;->a(Ljava/text/SimpleDateFormat;Ljava/lang/String;)J

    move-result-wide v4

    goto :goto_1

    :cond_3
    const-string/jumbo v26, "max_position"

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    goto :goto_1

    :cond_4
    const-string/jumbo v26, "min_position"

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    goto :goto_1

    :pswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v26, "sources"

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_6

    const/4 v2, 0x1

    if-ne v2, v11, :cond_5

    invoke-static/range {p0 .. p1}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v12

    goto/16 :goto_1

    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    :cond_6
    const-string/jumbo v26, "enhanced_sources"

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_7

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/ap;->af(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v23

    goto/16 :goto_1

    :cond_7
    const-string/jumbo v26, "targets"

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_8

    packed-switch v14, :pswitch_data_1

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    :pswitch_3
    invoke-static/range {p0 .. p1}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v15

    goto/16 :goto_1

    :pswitch_4
    invoke-static/range {p0 .. p1}, Lcom/twitter/library/api/ap;->d(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v16

    goto/16 :goto_1

    :pswitch_5
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v2, v1}, Lcom/twitter/library/api/ap;->c(Lcom/fasterxml/jackson/core/JsonParser;ILcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v17

    goto/16 :goto_1

    :cond_8
    const-string/jumbo v26, "target_objects"

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    packed-switch v19, :pswitch_data_2

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    :pswitch_6
    invoke-static/range {p0 .. p1}, Lcom/twitter/library/api/ap;->d(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v20

    goto/16 :goto_1

    :pswitch_7
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v2, v1}, Lcom/twitter/library/api/ap;->c(Lcom/fasterxml/jackson/core/JsonParser;ILcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v21

    goto/16 :goto_1

    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    :pswitch_8
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v26, "sources_size"

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_a

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v10

    goto/16 :goto_1

    :cond_a
    const-string/jumbo v26, "targets_size"

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_b

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v13

    goto/16 :goto_1

    :cond_b
    const-string/jumbo v26, "target_objects_size"

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_c

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v18

    goto/16 :goto_1

    :cond_c
    const-string/jumbo v26, "magic_rec_id"

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v24

    goto/16 :goto_1

    :pswitch_9
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    :cond_d
    if-eqz v12, :cond_12

    const/16 v2, 0xe

    if-eq v3, v2, :cond_12

    const/4 v2, 0x1

    if-ne v14, v2, :cond_e

    if-eqz v15, :cond_12

    :cond_e
    const/4 v2, 0x2

    if-ne v14, v2, :cond_f

    if-eqz v16, :cond_12

    :cond_f
    const/4 v2, 0x3

    if-ne v14, v2, :cond_10

    if-eqz v17, :cond_12

    :cond_10
    const/4 v2, 0x2

    move/from16 v0, v19

    if-ne v0, v2, :cond_11

    if-eqz v20, :cond_12

    :cond_11
    const/4 v2, 0x3

    move/from16 v0, v19

    if-ne v0, v2, :cond_13

    if-nez v21, :cond_13

    :cond_12
    const/4 v2, 0x0

    :goto_2
    return-object v2

    :cond_13
    new-instance v2, Lcom/twitter/library/api/ae;

    invoke-direct/range {v2 .. v25}, Lcom/twitter/library/api/ae;-><init>(IJJJIILjava/util/ArrayList;IILjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;IILjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;IJ)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_9
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static o(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/conversations/v;
    .locals 2

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_1

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v1, :cond_0

    invoke-static {p0}, Lcom/twitter/library/api/conversations/v;->a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/conversations/v;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static o(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;
    .locals 6

    const/4 v2, 0x0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_5

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_5

    sget-object v1, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v1, v2

    move-object v3, v0

    move-object v0, v2

    :goto_2
    if-eqz v3, :cond_3

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v3, v5, :cond_3

    sget-object v5, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v3}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v3

    aget v3, v5, v3

    packed-switch v3, :pswitch_data_1

    :goto_3
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    goto :goto_2

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v5, "data"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v1, 0x1

    invoke-static {p0, v1, p1}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v1

    goto :goto_3

    :cond_1
    const-string/jumbo v5, "metadata"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {p0}, Lcom/twitter/library/api/ap;->F(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/TwitterUserMetadata;

    move-result-object v0

    goto :goto_3

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :cond_3
    if-eqz v1, :cond_0

    if-eqz v0, :cond_4

    iput-object v0, v1, Lcom/twitter/library/api/TwitterUser;->metadata:Lcom/twitter/library/api/TwitterUserMetadata;

    iget-object v3, v0, Lcom/twitter/library/api/TwitterUserMetadata;->a:Lcom/twitter/library/api/TwitterSocialProof;

    if-eqz v3, :cond_4

    iget v3, v1, Lcom/twitter/library/api/TwitterUser;->friendship:I

    iget-object v0, v0, Lcom/twitter/library/api/TwitterUserMetadata;->a:Lcom/twitter/library/api/TwitterSocialProof;

    iget v0, v0, Lcom/twitter/library/api/TwitterSocialProof;->f:I

    invoke-static {v3, v0}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v0

    iput v0, v1, Lcom/twitter/library/api/TwitterUser;->friendship:I

    :cond_4
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_5
    return-object v4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static p(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_0

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-static {p0}, Lcom/twitter/library/api/ap;->aj(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static p(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v0

    move-object v0, v1

    move-object v1, v3

    :goto_0
    if-eqz v1, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_1

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    const-string/jumbo v1, "statuses"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0, p1}, Lcom/twitter/library/api/ap;->d(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_1
    if-nez v0, :cond_2

    new-instance v0, Lcom/twitter/library/util/InvalidDataException;

    const-string/jumbo v1, "Search did not return results module"

    invoke-direct {v0, v1}, Lcom/twitter/library/util/InvalidDataException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static q(Lcom/fasterxml/jackson/core/JsonParser;)I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    move-object v4, v1

    move v1, v2

    move-object v2, v4

    :goto_0
    if-eqz v2, :cond_3

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_3

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    :pswitch_0
    const-string/jumbo v2, "relationship"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    :goto_2
    if-eqz v2, :cond_0

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_0

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_1

    :goto_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_2

    :pswitch_1
    const-string/jumbo v2, "source"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p0}, Lcom/twitter/library/api/ap;->ak(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    goto :goto_3

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    return v1

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_3
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static q(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/search/d;
    .locals 5

    const/4 v1, 0x0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v2, v0

    move-object v0, v1

    :goto_0
    if-eqz v2, :cond_6

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v4, :cond_6

    sget-object v4, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v4, v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    :pswitch_1
    const-string/jumbo v2, "modules"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    :goto_2
    if-eqz v2, :cond_0

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v4, :cond_0

    sget-object v4, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v4, v2

    packed-switch v2, :pswitch_data_1

    :cond_1
    :goto_3
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_2

    :pswitch_3
    invoke-static {p0, p1}, Lcom/twitter/library/api/ap;->G(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/search/b;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_5
    const-string/jumbo v2, "metadata"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    :goto_4
    if-eqz v2, :cond_0

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v4, :cond_0

    sget-object v4, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v4, v2

    packed-switch v2, :pswitch_data_2

    :cond_3
    :goto_5
    :pswitch_6
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_4

    :pswitch_7
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_5

    :pswitch_8
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v4, "prev_cursor"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    goto :goto_5

    :cond_4
    const-string/jumbo v4, "next_cursor"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    :cond_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    :cond_6
    new-instance v2, Lcom/twitter/library/api/search/d;

    invoke-direct {v2, v3, v1, v0}, Lcom/twitter/library/api/search/d;-><init>(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_7
        :pswitch_8
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static r(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterSocialProof;
    .locals 12

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v9, -0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v8, v0

    move v4, v5

    move v3, v5

    move-object v1, v7

    move v2, v9

    move-object v0, v7

    :goto_0
    if-eqz v8, :cond_4

    sget-object v10, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v8, v10, :cond_4

    sget-object v10, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v8}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v8

    aget v8, v10, v8

    packed-switch v8, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v8

    goto :goto_0

    :pswitch_1
    const-string/jumbo v8, "user"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-static {p0, v6, p1}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v10, "favorite"

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v3

    goto :goto_1

    :cond_2
    const-string/jumbo v10, "retweet"

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v4

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v10, "trending"

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    const/16 v2, 0x15

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_3
    const-string/jumbo v10, "top"

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    const/16 v2, 0x14

    goto :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_4
    if-lez v3, :cond_7

    move v8, v6

    :goto_2
    if-lez v4, :cond_8

    :goto_3
    if-eqz v0, :cond_d

    iget-object v1, v0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    if-eqz v8, :cond_9

    const/16 v2, 0x10

    move-object v11, v1

    move v1, v2

    move-object v2, v11

    :goto_4
    if-ne v1, v9, :cond_5

    if-eqz v8, :cond_5

    if-eqz v6, :cond_5

    if-le v3, v4, :cond_a

    const/16 v1, 0xe

    :cond_5
    :goto_5
    if-ne v1, v9, :cond_6

    if-eqz v6, :cond_b

    const/16 v1, 0x13

    :cond_6
    :goto_6
    if-eq v1, v9, :cond_c

    new-instance v0, Lcom/twitter/library/api/TwitterSocialProof;

    move v6, v5

    move v8, v5

    invoke-direct/range {v0 .. v8}, Lcom/twitter/library/api/TwitterSocialProof;-><init>(ILjava/lang/String;IIIILjava/lang/String;I)V

    :goto_7
    return-object v0

    :cond_7
    move v8, v5

    goto :goto_2

    :cond_8
    move v6, v5

    goto :goto_3

    :cond_9
    if-eqz v6, :cond_d

    const/16 v2, 0x12

    move-object v11, v1

    move v1, v2

    move-object v2, v11

    goto :goto_4

    :cond_a
    const/16 v1, 0xf

    goto :goto_5

    :cond_b
    if-eqz v8, :cond_6

    const/16 v1, 0x11

    goto :goto_6

    :cond_c
    move-object v0, v7

    goto :goto_7

    :cond_d
    move-object v11, v1

    move v1, v2

    move-object v2, v11

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static r(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v2, v1

    move-object v1, v0

    :goto_0
    if-eqz v2, :cond_3

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_3

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    :pswitch_0
    const-string/jumbo v3, "result"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    :goto_2
    if-eqz v2, :cond_0

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_0

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_1

    :goto_3
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_2

    :pswitch_2
    const-string/jumbo v2, "places"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p0}, Lcom/twitter/library/api/ap;->al(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_3

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    return-object v1

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_4
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public static s(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_1

    :goto_0
    if-eqz v0, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_0

    invoke-static {p0}, Lcom/twitter/library/api/ap;->t(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/search/TwitterSearchQuery;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public static s(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;
    .locals 8

    const/4 v7, 0x1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_7

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_7

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "media_items"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_3
    if-eqz v0, :cond_5

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_5

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_3

    const-string/jumbo v0, "status"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :try_start_0
    invoke-static {p0, v0, p1}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterStatus;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/twitter/library/util/NullUserException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_3

    :catch_0
    move-exception v0

    if-eqz p1, :cond_1

    const-string/jumbo v2, "Received null user for status = %d"

    new-array v3, v7, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-wide v5, v0, Lcom/twitter/library/util/NullUserException;->statusId:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/twitter/library/util/w;->a(Ljava/lang/String;)V

    goto :goto_4

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_4

    :cond_3
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_4

    :cond_4
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_5

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    :cond_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_2

    :cond_6
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_7
    return-object v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static t(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterUser;
    .locals 12

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v9, v7

    move-object v4, v7

    move-object v10, v7

    :goto_0
    if-eqz v0, :cond_3

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_3

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :pswitch_0
    move-object v0, v9

    move-object v2, v4

    move-object v4, v10

    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v5

    move-object v9, v0

    move-object v10, v4

    move-object v4, v2

    move-object v0, v5

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "user"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0, v1, p1}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    move-object v2, v4

    move-object v4, v0

    move-object v0, v9

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v9

    move-object v2, v4

    move-object v4, v10

    goto :goto_1

    :pswitch_2
    const-string/jumbo v0, "connections"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0, p1}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v0

    move-object v2, v0

    move-object v4, v10

    move-object v0, v9

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v9

    move-object v2, v4

    move-object v4, v10

    goto :goto_1

    :pswitch_3
    const-string/jumbo v0, "token"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    move-object v2, v4

    move-object v4, v10

    goto :goto_1

    :cond_3
    if-nez v10, :cond_4

    :goto_2
    return-object v7

    :cond_4
    if-eqz v4, :cond_6

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    new-instance v11, Lcom/twitter/library/api/TwitterUserMetadata;

    new-instance v0, Lcom/twitter/library/api/TwitterSocialProof;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/TwitterUser;

    iget-object v2, v2, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v5, v4, -0x1

    move v4, v3

    move v6, v3

    move v8, v3

    invoke-direct/range {v0 .. v8}, Lcom/twitter/library/api/TwitterSocialProof;-><init>(ILjava/lang/String;IIIILjava/lang/String;I)V

    invoke-direct {v11, v0, v7, v9, v3}, Lcom/twitter/library/api/TwitterUserMetadata;-><init>(Lcom/twitter/library/api/TwitterSocialProof;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v11, v10, Lcom/twitter/library/api/TwitterUser;->metadata:Lcom/twitter/library/api/TwitterUserMetadata;

    :cond_5
    :goto_3
    move-object v7, v10

    goto :goto_2

    :cond_6
    if-eqz v9, :cond_5

    new-instance v0, Lcom/twitter/library/api/TwitterUserMetadata;

    invoke-direct {v0, v7, v7, v9, v3}, Lcom/twitter/library/api/TwitterUserMetadata;-><init>(Lcom/twitter/library/api/TwitterSocialProof;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, v10, Lcom/twitter/library/api/TwitterUser;->metadata:Lcom/twitter/library/api/TwitterUserMetadata;

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static t(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/search/TwitterSearchQuery;
    .locals 10

    const-wide/16 v3, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v2, v7

    move-object v1, v7

    move-wide v5, v3

    :goto_0
    if-eqz v0, :cond_3

    sget-object v8, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v8, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v8

    sget-object v9, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v9, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "id"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v5

    goto :goto_1

    :pswitch_2
    const-string/jumbo v0, "name"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_1
    const-string/jumbo v0, "query"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_2
    const-string/jumbo v0, "created_at"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/internal/util/k;->b:Ljava/text/SimpleDateFormat;

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/twitter/library/util/Util;->a(Ljava/text/SimpleDateFormat;Ljava/lang/String;)J

    move-result-wide v3

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_3
    new-instance v0, Lcom/twitter/library/api/search/TwitterSearchQuery;

    invoke-static {}, Lcom/twitter/library/util/CollectionsUtil;->a()Ljava/util/ArrayList;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lcom/twitter/library/api/search/TwitterSearchQuery;-><init>(Ljava/lang/String;Ljava/lang/String;JJLcom/twitter/library/api/PromotedContent;Ljava/util/ArrayList;)V

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method public static u(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;
    .locals 6

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    if-eqz p0, :cond_2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    :goto_0
    if-eqz v1, :cond_2

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_2

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    :goto_1
    if-eqz v1, :cond_0

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_0

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_1

    :cond_1
    :goto_2
    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_1

    :pswitch_2
    const-string/jumbo v1, "id"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    :cond_2
    return-object v2

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method

.method public static u(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    invoke-static {p0, p1}, Lcom/twitter/library/api/ap;->t(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public static v(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Landroid/util/Pair;
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v2, v0

    move-object v0, v1

    :goto_0
    if-eqz v2, :cond_2

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_2

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "min_position"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_1
    const-string/jumbo v3, "max_position"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_2
    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, v1, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static v(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;
    .locals 3

    const/4 v0, 0x0

    if-eqz p0, :cond_5

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_5

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_5

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v1, v2, :cond_3

    :goto_1
    if-eqz v1, :cond_4

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_4

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v1, v2, :cond_2

    invoke-static {p0}, Lcom/twitter/library/api/ap;->an(Lcom/fasterxml/jackson/core/JsonParser;)Landroid/util/Pair;

    move-result-object v1

    if-eqz v1, :cond_1

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_1

    :cond_2
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    :cond_3
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v1, v2, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    :cond_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :cond_5
    return-object v0
.end method

.method public static w(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/Decider;
    .locals 88

    const/16 v60, 0x0

    const-wide/16 v58, 0x0

    const/16 v57, 0x0

    const/16 v56, 0x0

    const/16 v55, 0x0

    const/16 v54, 0x0

    const-wide/16 v52, 0x0

    const/16 v51, 0x0

    const-wide/16 v49, 0x0

    const/16 v48, 0x0

    const/16 v47, 0x0

    const/16 v46, 0x0

    const/16 v45, 0x0

    const-wide/16 v43, 0x0

    const/16 v42, 0x0

    const-wide/16 v40, 0x0

    const/16 v39, 0x0

    const/16 v38, -0x1

    const/16 v37, 0x0

    const/16 v36, 0x0

    const/16 v35, 0x0

    const/16 v34, 0x0

    const/16 v33, 0x0

    const/16 v32, 0x0

    const/16 v31, 0x0

    const/16 v30, 0x0

    const/16 v29, 0x0

    const/16 v28, -0x1

    const/16 v27, -0x1

    const/16 v26, 0x0

    const/16 v25, 0x0

    const/16 v24, 0x0

    const/16 v23, 0x0

    const/16 v22, 0x0

    const/16 v21, 0x1

    const/16 v20, 0x1

    const/16 v19, 0x1

    const/16 v18, 0x1

    const/16 v17, 0x0

    const/16 v16, 0x0

    const/4 v15, 0x0

    const/4 v14, 0x0

    const/4 v13, 0x1

    const/4 v12, 0x1

    const/4 v11, 0x1

    const/4 v10, 0x1

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    if-eqz p0, :cond_33

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v61

    :goto_0
    if-eqz v61, :cond_32

    sget-object v62, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    move-object/from16 v0, v61

    move-object/from16 v1, v62

    if-eq v0, v1, :cond_32

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v62

    sget-object v63, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual/range {v61 .. v61}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v61

    aget v61, v63, v61

    packed-switch v61, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v61

    goto :goto_0

    :pswitch_1
    const-string/jumbo v61, "web_view_url_whitelist"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_1

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/ap;->k(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v37

    goto :goto_1

    :cond_1
    const-string/jumbo v61, "abdecider_whitelist"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_2

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/api/ap;->l(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v36

    goto :goto_1

    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    const-string/jumbo v61, "scribe_interval"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v58

    const-wide/16 v61, 0x3e8

    mul-long v58, v58, v61

    goto :goto_1

    :cond_3
    const-string/jumbo v61, "scribe_error_sample_size"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v57

    goto :goto_1

    :cond_4
    const-string/jumbo v61, "scribe_crash_sample_size"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v56

    goto :goto_1

    :cond_5
    const-string/jumbo v61, "scribe_api_sample_size"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v55

    goto :goto_1

    :cond_6
    const-string/jumbo v61, "find_friends_interval"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v40

    const-wide/16 v61, 0x3e8

    mul-long v40, v40, v61

    goto :goto_1

    :cond_7
    const-string/jumbo v61, "cache_version"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v38

    goto :goto_1

    :cond_8
    const-string/jumbo v61, "abdecider_enabled"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v35

    goto/16 :goto_1

    :cond_9
    const-string/jumbo v61, "pb_enabled"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_a

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v34

    goto/16 :goto_1

    :cond_a
    const-string/jumbo v61, "typeahead_users_size"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_b

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v54

    goto/16 :goto_1

    :cond_b
    const-string/jumbo v61, "typeahead_users_ttl"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_c

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v52

    const-wide/16 v61, 0x3e8

    mul-long v52, v52, v61

    goto/16 :goto_1

    :cond_c
    const-string/jumbo v61, "typeahead_topics_size"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_d

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v51

    goto/16 :goto_1

    :cond_d
    const-string/jumbo v61, "typeahead_topics_ttl"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_e

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v49

    const-wide/16 v61, 0x3e8

    mul-long v49, v49, v61

    goto/16 :goto_1

    :cond_e
    const-string/jumbo v61, "typeahead_max_recent"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_f

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v48

    goto/16 :goto_1

    :cond_f
    const-string/jumbo v61, "typeahead_max_topic"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_10

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v47

    goto/16 :goto_1

    :cond_10
    const-string/jumbo v61, "typeahead_max_user"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_11

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v46

    goto/16 :goto_1

    :cond_11
    const-string/jumbo v61, "typeahead_max_compose"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_12

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v45

    goto/16 :goto_1

    :cond_12
    const-string/jumbo v61, "typeahead_compose_throttle"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_13

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v43

    goto/16 :goto_1

    :cond_13
    const-string/jumbo v61, "suggestions_max_trend"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_14

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v42

    goto/16 :goto_1

    :cond_14
    const-string/jumbo v61, "geo_data_provider_update_delay"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_15

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v32

    goto/16 :goto_1

    :cond_15
    const-string/jumbo v61, "geo_data_provider_update_duration"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_16

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v31

    goto/16 :goto_1

    :cond_16
    const-string/jumbo v61, "geo_data_provider_update_interval"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_17

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v30

    goto/16 :goto_1

    :cond_17
    const-string/jumbo v61, "tweet_compose_location_hint_impression_limit_three_philosophers_1"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_18

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v29

    goto/16 :goto_1

    :cond_18
    const-string/jumbo v61, "dm_sync_user_fraction"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_19

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v28

    goto/16 :goto_1

    :cond_19
    const-string/jumbo v61, "report_tweet_user_fraction"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_1a

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v27

    goto/16 :goto_1

    :cond_1a
    const-string/jumbo v61, "antispam_connect_tweet_count"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_1b

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v26

    goto/16 :goto_1

    :cond_1b
    const-string/jumbo v61, "antispam_connect_user_count"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_1c

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v25

    goto/16 :goto_1

    :cond_1c
    const-string/jumbo v61, "antispam_query_frequency"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_1d

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v24

    goto/16 :goto_1

    :cond_1d
    const-string/jumbo v61, "scribe_linger_user_fraction"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_1e

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v17

    goto/16 :goto_1

    :cond_1e
    const-string/jumbo v61, "dm_photo_compose_user_fraction"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_1f

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v4

    goto/16 :goto_1

    :cond_1f
    const-string/jumbo v61, "dm_photo_render_user_fraction"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v3

    goto/16 :goto_1

    :pswitch_3
    const-string/jumbo v61, "scribe_status_linger_minimum_threshold"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_20

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->i()F

    move-result v16

    goto/16 :goto_1

    :cond_20
    const-string/jumbo v61, "scribe_status_linger_maximum_threshold"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->i()F

    move-result v15

    goto/16 :goto_1

    :pswitch_4
    const-string/jumbo v61, "scribe_enabled"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_21

    const/16 v60, 0x1

    goto/16 :goto_1

    :cond_21
    const-string/jumbo v61, "geo_data_provider_enabled"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_22

    const/16 v33, 0x1

    goto/16 :goto_1

    :cond_22
    const-string/jumbo v61, "lifeline_alerts_enabled"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_23

    const/16 v23, 0x1

    goto/16 :goto_1

    :cond_23
    const-string/jumbo v61, "rt_fav_settings_enabled"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_24

    const/16 v22, 0x1

    goto/16 :goto_1

    :cond_24
    const-string/jumbo v61, "device_follow_button_enabled"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_25

    const/4 v14, 0x1

    goto/16 :goto_1

    :cond_25
    const-string/jumbo v61, "alerts_activation_enabled"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_26

    const/4 v8, 0x1

    goto/16 :goto_1

    :cond_26
    const-string/jumbo v61, "show_reply_count_with_media"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_0

    const/4 v5, 0x1

    goto/16 :goto_1

    :pswitch_5
    const-string/jumbo v61, "conversations_enabled"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_27

    const/16 v21, 0x0

    goto/16 :goto_1

    :cond_27
    const-string/jumbo v61, "new_connect_types_enabled"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_28

    const/16 v20, 0x0

    goto/16 :goto_1

    :cond_28
    const-string/jumbo v61, "perch_blur_enabled"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_29

    const/16 v19, 0x0

    goto/16 :goto_1

    :cond_29
    const-string/jumbo v61, "media_forward_enabled"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_2a

    const/16 v18, 0x0

    goto/16 :goto_1

    :cond_2a
    const-string/jumbo v61, "inline_follow_enabled"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_2b

    const/4 v13, 0x0

    goto/16 :goto_1

    :cond_2b
    const-string/jumbo v61, "inline_follow_user_fetch_enabled"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_2c

    const/4 v12, 0x0

    goto/16 :goto_1

    :cond_2c
    const-string/jumbo v61, "select_all_invited_friends"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_2d

    const/4 v11, 0x0

    goto/16 :goto_1

    :cond_2d
    const-string/jumbo v61, "media_smart_crop_enabled"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_2e

    const/4 v10, 0x0

    goto/16 :goto_1

    :cond_2e
    const-string/jumbo v61, "amplify_player_enabled"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_2f

    const/4 v9, 0x0

    goto/16 :goto_1

    :cond_2f
    const-string/jumbo v61, "message_compose_suggestions_enabled"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_30

    const/4 v7, 0x0

    goto/16 :goto_1

    :cond_30
    const-string/jumbo v61, "message_compose_retry_enabled"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_31

    const/4 v6, 0x0

    goto/16 :goto_1

    :cond_31
    const-string/jumbo v61, "media_forward_tweet_detail"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_0

    const/4 v2, 0x0

    goto/16 :goto_1

    :pswitch_6
    const-string/jumbo v61, "ihb"

    invoke-virtual/range {v61 .. v62}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v61

    if-eqz v61, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v39

    goto/16 :goto_1

    :pswitch_7
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    :cond_32
    move/from16 v61, v2

    move/from16 v64, v3

    move/from16 v3, v60

    move/from16 v60, v64

    move/from16 v65, v5

    move/from16 v66, v6

    move/from16 v6, v57

    move/from16 v57, v66

    move/from16 v67, v7

    move/from16 v7, v56

    move/from16 v56, v67

    move/from16 v68, v9

    move/from16 v9, v54

    move/from16 v54, v68

    move/from16 v69, v11

    move/from16 v70, v12

    move/from16 v12, v51

    move/from16 v51, v20

    move/from16 v71, v13

    move/from16 v72, v14

    move-wide/from16 v13, v49

    move/from16 v49, v71

    move/from16 v50, v70

    move/from16 v73, v16

    move/from16 v16, v47

    move/from16 v47, v15

    move/from16 v15, v48

    move/from16 v48, v72

    move/from16 v74, v21

    move/from16 v21, v42

    move/from16 v42, v74

    move/from16 v75, v22

    move/from16 v76, v23

    move-wide/from16 v22, v40

    move/from16 v41, v75

    move/from16 v40, v76

    move/from16 v77, v25

    move-object/from16 v25, v37

    move/from16 v37, v26

    move/from16 v26, v35

    move/from16 v35, v28

    move/from16 v28, v34

    move-object/from16 v34, v39

    move/from16 v39, v24

    move/from16 v24, v38

    move/from16 v38, v77

    move/from16 v78, v27

    move-object/from16 v27, v36

    move/from16 v36, v78

    move/from16 v79, v29

    move/from16 v29, v33

    move/from16 v33, v79

    move/from16 v80, v30

    move/from16 v30, v32

    move/from16 v32, v80

    move-wide/from16 v81, v43

    move/from16 v44, v18

    move/from16 v43, v19

    move/from16 v18, v45

    move-wide/from16 v19, v81

    move/from16 v45, v17

    move/from16 v17, v46

    move/from16 v46, v73

    move-wide/from16 v83, v52

    move/from16 v52, v69

    move/from16 v53, v10

    move-wide/from16 v10, v83

    move/from16 v85, v55

    move/from16 v55, v8

    move/from16 v8, v85

    move-wide/from16 v86, v58

    move/from16 v58, v65

    move/from16 v59, v4

    move-wide/from16 v4, v86

    :goto_2
    new-instance v2, Lcom/twitter/library/api/Decider;

    invoke-direct/range {v2 .. v61}, Lcom/twitter/library/api/Decider;-><init>(ZJIIIIJIJIIIIJIJILjava/util/ArrayList;ILjava/util/ArrayList;IZIIIILjava/lang/String;IIIIIZZZZZIFFZZZZZZZZZZZIIZ)V

    return-object v2

    :cond_33
    move/from16 v61, v2

    move/from16 v64, v3

    move/from16 v3, v60

    move/from16 v60, v64

    move/from16 v65, v5

    move/from16 v66, v6

    move/from16 v6, v57

    move/from16 v57, v66

    move/from16 v67, v7

    move/from16 v7, v56

    move/from16 v56, v67

    move/from16 v68, v9

    move/from16 v9, v54

    move/from16 v54, v68

    move/from16 v69, v11

    move/from16 v70, v12

    move/from16 v12, v51

    move/from16 v51, v20

    move/from16 v71, v13

    move/from16 v72, v14

    move-wide/from16 v13, v49

    move/from16 v49, v71

    move/from16 v50, v70

    move/from16 v73, v16

    move/from16 v16, v47

    move/from16 v47, v15

    move/from16 v15, v48

    move/from16 v48, v72

    move/from16 v74, v21

    move/from16 v21, v42

    move/from16 v42, v74

    move/from16 v75, v22

    move/from16 v76, v23

    move-wide/from16 v22, v40

    move/from16 v41, v75

    move/from16 v40, v76

    move/from16 v77, v25

    move-object/from16 v25, v37

    move/from16 v37, v26

    move/from16 v26, v35

    move/from16 v35, v28

    move/from16 v28, v34

    move-object/from16 v34, v39

    move/from16 v39, v24

    move/from16 v24, v38

    move/from16 v38, v77

    move/from16 v78, v27

    move-object/from16 v27, v36

    move/from16 v36, v78

    move/from16 v79, v29

    move/from16 v29, v33

    move/from16 v33, v79

    move/from16 v80, v30

    move/from16 v30, v32

    move/from16 v32, v80

    move-wide/from16 v81, v43

    move/from16 v44, v18

    move/from16 v43, v19

    move/from16 v18, v45

    move-wide/from16 v19, v81

    move/from16 v45, v17

    move/from16 v17, v46

    move/from16 v46, v73

    move-wide/from16 v83, v52

    move/from16 v52, v69

    move/from16 v53, v10

    move-wide/from16 v10, v83

    move/from16 v85, v55

    move/from16 v55, v8

    move/from16 v8, v85

    move-wide/from16 v86, v58

    move/from16 v58, v65

    move/from16 v59, v4

    move-wide/from16 v4, v86

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_2
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public static w(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/ag;
    .locals 7

    const/4 v1, 0x0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v2, v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_0
    if-eqz v1, :cond_11

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v5, :cond_11

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v1, v5, :cond_10

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v5, "objects"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v6, v1

    move-object v1, v2

    move-object v2, v3

    move-object v3, v6

    :goto_1
    if-eqz v3, :cond_e

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v3, v5, :cond_e

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v3, v5, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v5, "users"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-static {p0}, Lcom/twitter/library/api/ap;->aA(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;

    move-result-object v2

    :cond_0
    :goto_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    goto :goto_1

    :cond_1
    const-string/jumbo v5, "timelines"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {p0}, Lcom/twitter/library/api/ap;->aB(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;

    move-result-object v1

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    :cond_3
    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v3, v5, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    :cond_4
    const-string/jumbo v5, "response"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_3
    if-eqz v0, :cond_12

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v5, :cond_12

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v5, :cond_9

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v5, "cursors"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    :goto_4
    if-eqz v1, :cond_7

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v5, :cond_7

    sget-object v5, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v5, v1

    packed-switch v1, :pswitch_data_0

    :cond_5
    :goto_5
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_4

    :pswitch_1
    const-string/jumbo v1, "next_cursor"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_5

    :cond_6
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v1

    :cond_7
    move-object v1, v0

    :cond_8
    :goto_6
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_3

    :cond_9
    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v5, :cond_8

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v5, "results"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_7
    if-eqz v0, :cond_8

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v5, :cond_8

    sget-object v5, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_1

    :cond_a
    :goto_8
    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_7

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_9
    if-eqz v0, :cond_a

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v5, :cond_a

    sget-object v5, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_2

    :cond_b
    :goto_a
    :pswitch_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_9

    :pswitch_6
    const-string/jumbo v0, "timeline_id"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterTopic;

    if-eqz v0, :cond_b

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_a

    :pswitch_7
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_a

    :pswitch_8
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_8

    :cond_c
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_6

    :cond_d
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v1, v2

    move-object v2, v3

    :cond_e
    :goto_b
    move-object v3, v2

    move-object v2, v1

    :cond_f
    :goto_c
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto/16 :goto_0

    :cond_10
    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v1, v5, :cond_f

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_c

    :cond_11
    new-instance v1, Lcom/twitter/library/api/ag;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v1, v4, v2, v0}, Lcom/twitter/library/api/ag;-><init>(Ljava/util/List;Ljava/util/Collection;Ljava/lang/String;)V

    return-object v1

    :cond_12
    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    goto :goto_b

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_8
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_7
    .end packed-switch
.end method

.method public static x(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterTopic;
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v2, v1

    :goto_0
    if-eqz v0, :cond_8

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v3, :cond_8

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v3, :cond_7

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "objects"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v4, v0

    move-object v0, v1

    move-object v1, v4

    :goto_1
    if-eqz v1, :cond_a

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_a

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v1, v3, :cond_2

    const-string/jumbo v1, "timelines"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0}, Lcom/twitter/library/api/ap;->aB(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;

    move-result-object v0

    :cond_0
    :goto_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    :cond_2
    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v1, v3, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    :cond_3
    const-string/jumbo v3, "response"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v4, v0

    move-object v0, v2

    move-object v2, v4

    :goto_3
    if-eqz v2, :cond_9

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_9

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    :cond_4
    :goto_4
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_3

    :pswitch_1
    const-string/jumbo v2, "timeline_id"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterTopic;

    goto :goto_4

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_4

    :cond_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v1

    move-object v1, v2

    :goto_5
    move-object v2, v1

    move-object v1, v0

    :cond_6
    :goto_6
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto/16 :goto_0

    :cond_7
    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v3, :cond_6

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_6

    :cond_8
    return-object v2

    :cond_9
    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_5

    :cond_a
    move-object v1, v2

    goto :goto_5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static x(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v0

    move-object v0, v1

    move-object v1, v3

    :goto_0
    if-eqz v1, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_1

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    const-string/jumbo v1, "experiments"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/twitter/library/api/ap;->y(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;

    move-result-object v0

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_1
    if-nez v0, :cond_2

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    :cond_2
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static y(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)J
    .locals 4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    const-wide/16 v0, -0x1

    :goto_0
    if-eqz v2, :cond_0

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_0

    sget-object v3, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v0

    goto :goto_1

    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    const-string/jumbo v2, "Could not parse timestamp."

    invoke-interface {p1, v2}, Lcom/twitter/library/util/w;->a(Ljava/lang/String;)V

    :cond_1
    return-wide v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static y(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;
    .locals 3

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_1

    invoke-static {p0}, Lcom/twitter/library/api/ap;->z(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/Experiment;

    move-result-object v0

    iget-object v2, v0, Lcom/twitter/library/api/Experiment;->key:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_2
    return-object v1
.end method

.method public static z(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/Experiment;
    .locals 7

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v5, v2

    move-object v4, v2

    move-object v1, v2

    :goto_0
    if-eqz v0, :cond_4

    sget-object v6, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v6, :cond_4

    sget-object v6, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v6, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v6, "experiment_key"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_1
    const-string/jumbo v6, "bucket"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_2
    const-string/jumbo v6, "experiment_type"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :pswitch_2
    const-string/jumbo v0, "buckets"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0}, Lcom/twitter/library/api/ap;->A(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v2

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_4
    const-string/jumbo v0, "version"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v3

    goto :goto_1

    :cond_4
    new-instance v0, Lcom/twitter/library/api/Experiment;

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/api/Experiment;-><init>(Ljava/lang/String;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;)V

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static z(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v3, v0

    move-object v0, v1

    move-object v1, v3

    :goto_0
    if-eqz v1, :cond_1

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v2, :cond_1

    sget-object v2, Lcom/twitter/library/api/aq;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    const-string/jumbo v1, "modules"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0, p1}, Lcom/twitter/library/api/ap;->J(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_1
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
