.class public Lcom/twitter/library/api/au;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Ljava/util/ArrayList;

.field public final b:Z

.field public final c:Z

.field public final d:Lcom/twitter/library/api/TwitterSearchHighlight;

.field public e:Ljava/lang/String;

.field public f:Lcom/twitter/library/api/TwitterSocialProof;

.field public g:Lcom/twitter/library/api/af;

.field public h:Lcom/twitter/library/api/DiscoverStoryMetadata;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/ArrayList;ZLcom/twitter/library/api/TwitterSocialProof;ZLcom/twitter/library/api/TwitterSearchHighlight;Lcom/twitter/library/api/af;Lcom/twitter/library/api/DiscoverStoryMetadata;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/api/au;->e:Ljava/lang/String;

    iput-object p2, p0, Lcom/twitter/library/api/au;->a:Ljava/util/ArrayList;

    iput-boolean p3, p0, Lcom/twitter/library/api/au;->b:Z

    iput-object p4, p0, Lcom/twitter/library/api/au;->f:Lcom/twitter/library/api/TwitterSocialProof;

    iput-boolean p5, p0, Lcom/twitter/library/api/au;->c:Z

    iput-object p6, p0, Lcom/twitter/library/api/au;->d:Lcom/twitter/library/api/TwitterSearchHighlight;

    iput-object p7, p0, Lcom/twitter/library/api/au;->g:Lcom/twitter/library/api/af;

    iput-object p8, p0, Lcom/twitter/library/api/au;->h:Lcom/twitter/library/api/DiscoverStoryMetadata;

    iput-object p9, p0, Lcom/twitter/library/api/au;->i:Ljava/lang/String;

    iput-object p10, p0, Lcom/twitter/library/api/au;->j:Ljava/lang/String;

    return-void
.end method
