.class public Lcom/twitter/library/api/account/j;
.super Lcom/twitter/library/api/account/i;
.source "Twttr"


# instance fields
.field private g:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    const-class v0, Lcom/twitter/library/api/account/j;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/api/account/i;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/twitter/library/api/account/j;->g:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method protected d(Lcom/twitter/library/service/e;)V
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/library/api/account/j;->f()[I

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    const/4 v1, 0x0

    aget v0, v0, v1

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    new-instance v0, Lcom/twitter/library/api/account/r;

    iget-object v1, p0, Lcom/twitter/library/api/account/j;->l:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/twitter/library/api/account/j;->s()Lcom/twitter/library/service/p;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/library/api/account/j;->e:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/api/account/r;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/twitter/library/api/account/j;->a(Lcom/twitter/internal/android/service/a;)Lcom/twitter/library/service/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/service/e;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/api/account/r;->f()Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/account/j;->g:Ljava/lang/Boolean;

    :cond_0
    return-void
.end method

.method public h()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/account/j;->g:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
