.class public Lcom/twitter/library/api/account/o;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    const-class v0, Lcom/twitter/library/api/account/o;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    return-void
.end method


# virtual methods
.method protected b(Lcom/twitter/library/service/e;)V
    .locals 7

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/library/api/account/o;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    iget-object v2, v0, Lcom/twitter/library/service/p;->e:Ljava/lang/String;

    iget-wide v3, v0, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {p0}, Lcom/twitter/library/api/account/o;->w()Lcom/twitter/library/provider/az;

    move-result-object v0

    iget-object v5, p0, Lcom/twitter/library/api/account/o;->l:Landroid/content/Context;

    invoke-static {v5, v2}, Lcom/twitter/library/util/a;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v6, p0, Lcom/twitter/library/api/account/o;->l:Landroid/content/Context;

    invoke-static {v6, v5, v1}, Lcom/twitter/library/platform/PushService;->a(Landroid/content/Context;Landroid/accounts/Account;Z)V

    :cond_0
    iget-object v5, p0, Lcom/twitter/library/api/account/o;->l:Landroid/content/Context;

    invoke-static {v5}, Lcom/twitter/library/platform/PushService;->f(Landroid/content/Context;)Z

    iget-object v5, p0, Lcom/twitter/library/api/account/o;->l:Landroid/content/Context;

    const/4 v6, 0x0

    invoke-static {v5, v3, v4, v2, v6}, Lcom/twitter/library/service/n;->a(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)Ljava/util/List;

    invoke-virtual {v0}, Lcom/twitter/library/provider/az;->d()V

    iget-object v0, p0, Lcom/twitter/library/api/account/o;->l:Landroid/content/Context;

    invoke-static {v0, v3, v4}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;J)V

    iget-object v0, p0, Lcom/twitter/library/api/account/o;->l:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/twitter/library/util/a;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/AccountManagerFuture;

    move-result-object v0

    if-eqz v0, :cond_1

    :try_start_0
    invoke-interface {v0}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v0

    :goto_0
    iget-object v1, p0, Lcom/twitter/library/api/account/o;->l:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/library/provider/f;->a(Landroid/content/Context;)Lcom/twitter/library/provider/f;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/twitter/library/provider/f;->d(Ljava/lang/String;)I

    invoke-virtual {v1, v2}, Lcom/twitter/library/provider/f;->c(Ljava/lang/String;)I

    invoke-virtual {p1, v0}, Lcom/twitter/library/service/e;->a(Z)V

    return-void

    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    move v0, v1

    goto :goto_0

    :catch_2
    move-exception v0

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
