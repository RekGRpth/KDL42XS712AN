.class public Lcom/twitter/library/api/account/q;
.super Lcom/twitter/library/api/account/e;
.source "Twttr"


# instance fields
.field protected final d:Lcom/twitter/library/api/ao;

.field private final e:Ljava/lang/String;

.field private final f:J

.field private final g:Ljava/lang/String;

.field private final n:Ljava/lang/String;

.field private o:Lcom/twitter/library/network/LoginResponse;

.field private p:Lcom/twitter/library/api/TwitterUser;

.field private q:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/client/Session;)V
    .locals 1

    const-class v0, Lcom/twitter/library/api/account/q;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p7}, Lcom/twitter/library/api/account/e;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    const/16 v0, 0x21

    invoke-static {v0}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/account/q;->d:Lcom/twitter/library/api/ao;

    iput-object p2, p0, Lcom/twitter/library/api/account/q;->e:Ljava/lang/String;

    iput-wide p3, p0, Lcom/twitter/library/api/account/q;->f:J

    iput-object p6, p0, Lcom/twitter/library/api/account/q;->n:Ljava/lang/String;

    iput-object p5, p0, Lcom/twitter/library/api/account/q;->g:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/e;Lcom/twitter/library/network/a;)Lcom/twitter/internal/network/HttpOperation;
    .locals 6

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/twitter/library/api/account/q;->m:Lcom/twitter/library/network/aa;

    iget-object v1, v1, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "/auth/one_factor/access_token"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "login_verification_request_id"

    iget-object v4, p0, Lcom/twitter/library/api/account/q;->e:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "login_verification_user_id"

    iget-wide v4, p0, Lcom/twitter/library/api/account/q;->f:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "login_verification_challenge_response"

    iget-object v4, p0, Lcom/twitter/library/api/account/q;->n:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "send_error_codes"

    const-string/jumbo v4, "true"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "x_auth_json_response"

    const-string/jumbo v4, "1"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/twitter/library/network/d;

    iget-object v3, p0, Lcom/twitter/library/api/account/q;->l:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/twitter/library/api/account/q;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    iget-wide v3, v0, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    sget-object v2, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v0, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/account/q;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    const-string/jumbo v1, "Accept"

    const-string/jumbo v2, "application/json"

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Lcom/twitter/internal/network/HttpOperation;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method protected a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)V
    .locals 5

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/account/q;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/network/LoginResponse;

    iput-object v0, p0, Lcom/twitter/library/api/account/q;->o:Lcom/twitter/library/network/LoginResponse;

    new-instance v1, Lcom/twitter/library/api/account/v;

    iget-object v2, p0, Lcom/twitter/library/api/account/q;->l:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/twitter/library/api/account/q;->s()Lcom/twitter/library/service/p;

    move-result-object v3

    new-instance v4, Lcom/twitter/library/network/n;

    iget-object v0, v0, Lcom/twitter/library/network/LoginResponse;->a:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v4, v0}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-direct {v1, v2, v3, v4}, Lcom/twitter/library/api/account/v;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;Lcom/twitter/library/network/n;)V

    invoke-virtual {p0, v1}, Lcom/twitter/library/api/account/q;->a(Lcom/twitter/internal/android/service/a;)Lcom/twitter/library/service/e;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/library/service/e;->a(Lcom/twitter/library/service/e;)V

    invoke-virtual {v1}, Lcom/twitter/library/api/account/v;->e()Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/account/q;->p:Lcom/twitter/library/api/TwitterUser;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/api/account/q;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/twitter/library/api/account/q;->c(Ljava/util/ArrayList;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/account/q;->q:[I

    invoke-virtual {p2, p1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    goto :goto_0
.end method

.method public final e()Lcom/twitter/library/api/TwitterUser;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/account/q;->p:Lcom/twitter/library/api/TwitterUser;

    return-object v0
.end method

.method public final f()[I
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/account/q;->q:[I

    return-object v0
.end method

.method public final g()Lcom/twitter/library/network/LoginResponse;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/account/q;->o:Lcom/twitter/library/network/LoginResponse;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/account/q;->g:Ljava/lang/String;

    return-object v0
.end method
