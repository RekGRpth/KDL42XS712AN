.class public Lcom/twitter/library/api/account/r;
.super Lcom/twitter/library/api/account/e;
.source "Twttr"


# instance fields
.field private final d:Lcom/twitter/library/api/ao;

.field private final e:Ljava/lang/String;

.field private f:Ljava/lang/Boolean;

.field private g:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/p;Ljava/lang/String;)V
    .locals 1

    const-class v0, Lcom/twitter/library/api/account/r;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/account/e;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/p;)V

    const/16 v0, 0x45

    invoke-static {v0}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/account/r;->d:Lcom/twitter/library/api/ao;

    iput-object p3, p0, Lcom/twitter/library/api/account/r;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/e;Lcom/twitter/library/network/a;)Lcom/twitter/internal/network/HttpOperation;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/twitter/library/api/account/r;->m:Lcom/twitter/library/network/aa;

    iget-object v1, v1, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "/auth/one_factor/eligible"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "x_auth_username"

    iget-object v2, p0, Lcom/twitter/library/api/account/r;->e:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "send_error_codes"

    const-string/jumbo v2, "true"

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/twitter/library/network/d;

    iget-object v2, p0, Lcom/twitter/library/api/account/r;->l:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/twitter/library/api/account/r;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    iget-wide v2, v0, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    sget-object v1, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->a:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/account/r;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    const-string/jumbo v1, "Accept"

    const-string/jumbo v2, "application/json"

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Lcom/twitter/internal/network/HttpOperation;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method protected a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)V
    .locals 1

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/account/r;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/twitter/library/api/account/r;->f:Ljava/lang/Boolean;

    :goto_0
    invoke-virtual {p2, p1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/api/account/r;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/twitter/library/api/account/r;->c(Ljava/util/ArrayList;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/account/r;->g:[I

    goto :goto_0
.end method

.method public e()[I
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/account/r;->g:[I

    return-object v0
.end method

.method public f()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/account/r;->f:Ljava/lang/Boolean;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/account/r;->e:Ljava/lang/String;

    return-object v0
.end method
