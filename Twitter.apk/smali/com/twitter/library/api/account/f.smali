.class Lcom/twitter/library/api/account/f;
.super Lcom/twitter/internal/android/service/l;
.source "Twttr"


# instance fields
.field private a:I

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/internal/android/service/l;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/api/account/f;->b:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/internal/android/service/k;)Z
    .locals 3

    invoke-virtual {p1}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->e()Lcom/twitter/internal/network/k;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v1, v0, Lcom/twitter/internal/network/k;->a:I

    const/16 v2, 0x191

    if-eq v1, v2, :cond_0

    iget v0, v0, Lcom/twitter/internal/network/k;->a:I

    const/16 v1, 0x193

    if-ne v0, v1, :cond_1

    :cond_0
    iget v0, p0, Lcom/twitter/library/api/account/f;->a:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/twitter/library/api/account/f;->a:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/api/account/f;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/network/b;->a(Landroid/content/Context;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lcom/twitter/internal/android/service/k;)J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method
