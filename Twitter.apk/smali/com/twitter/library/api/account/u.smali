.class public Lcom/twitter/library/api/account/u;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# instance fields
.field private final d:Lcom/twitter/library/api/ao;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private g:Z

.field private n:Z

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Lcom/twitter/library/network/LoginResponse;

.field private u:Lcom/twitter/library/api/t;

.field private v:Lcom/twitter/library/api/TwitterUser;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-class v0, Lcom/twitter/library/api/account/u;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    const/16 v0, 0x22

    invoke-static {v0}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/account/u;->d:Lcom/twitter/library/api/ao;

    iput-object p3, p0, Lcom/twitter/library/api/account/u;->f:Ljava/lang/String;

    iput-object p4, p0, Lcom/twitter/library/api/account/u;->e:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/api/account/u;->g:Z

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/e;)Lcom/twitter/internal/network/HttpOperation;
    .locals 13

    iget-object v0, p0, Lcom/twitter/library/api/account/u;->s:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/library/api/account/u;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/library/api/account/u;->r:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/library/api/account/u;->e:Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/library/api/account/u;->q:Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/library/api/account/u;->p:Ljava/lang/String;

    iget-object v6, p0, Lcom/twitter/library/api/account/u;->o:Ljava/lang/String;

    iget-boolean v7, p0, Lcom/twitter/library/api/account/u;->g:Z

    iget-boolean v8, p0, Lcom/twitter/library/api/account/u;->n:Z

    iget-object v9, p0, Lcom/twitter/library/api/account/u;->m:Lcom/twitter/library/network/aa;

    iget-object v9, v9, Lcom/twitter/library/network/aa;->c:Ljava/lang/String;

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string/jumbo v12, "signup"

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    if-eqz v0, :cond_0

    new-instance v11, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v12, "fullname"

    invoke-direct {v11, v12, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    if-eqz v1, :cond_1

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v11, "screen_name"

    invoke-direct {v0, v11, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    if-eqz v2, :cond_2

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v1, "email"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    if-eqz v3, :cond_3

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v1, "password"

    invoke-direct {v0, v1, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    if-eqz v6, :cond_4

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v1, "lang"

    invoke-direct {v0, v1, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    if-eqz v7, :cond_5

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v1, "discoverable_by_email"

    const-string/jumbo v2, "true"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    if-eqz v8, :cond_6

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v1, "discoverable_by_mobile_phone"

    const-string/jumbo v2, "true"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    if-eqz v4, :cond_7

    if-eqz v5, :cond_7

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v1, "captcha_token"

    invoke-direct {v0, v1, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v1, "captcha_solution"

    invoke-direct {v0, v1, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_7
    new-instance v0, Lcom/twitter/library/network/d;

    iget-object v1, p0, Lcom/twitter/library/api/account/u;->l:Landroid/content/Context;

    invoke-direct {v0, v1, v9}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/twitter/library/api/account/u;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    iget-wide v1, v1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    sget-object v1, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/n;

    const/4 v2, 0x0

    sget-object v3, Lcom/twitter/library/network/n;->i:Ljava/lang/String;

    sget-object v4, Lcom/twitter/library/network/n;->j:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v10}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/account/u;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/library/api/account/u;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/account/u;->o:Ljava/lang/String;

    return-object p0
.end method

.method public a(Z)Lcom/twitter/library/api/account/u;
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/api/account/u;->g:Z

    return-object p0
.end method

.method protected a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)V
    .locals 6

    const/16 v5, 0xc8

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v0

    iget v0, v0, Lcom/twitter/internal/network/k;->a:I

    if-ne v0, v5, :cond_1

    iget-object v0, p0, Lcom/twitter/library/api/account/u;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/network/OAuthToken;

    new-instance v1, Lcom/twitter/library/network/LoginResponse;

    const/4 v2, 0x1

    iget-object v3, v0, Lcom/twitter/library/network/OAuthToken;->b:Ljava/lang/String;

    iget-object v4, v0, Lcom/twitter/library/network/OAuthToken;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, Lcom/twitter/library/network/LoginResponse;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/twitter/library/api/account/u;->t:Lcom/twitter/library/network/LoginResponse;

    new-instance v1, Lcom/twitter/library/api/account/v;

    iget-object v2, p0, Lcom/twitter/library/api/account/u;->l:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/twitter/library/api/account/u;->s()Lcom/twitter/library/service/p;

    move-result-object v3

    new-instance v4, Lcom/twitter/library/network/n;

    invoke-direct {v4, v0}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-direct {v1, v2, v3, v4}, Lcom/twitter/library/api/account/v;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;Lcom/twitter/library/network/n;)V

    invoke-virtual {p0, v1}, Lcom/twitter/library/api/account/u;->a(Lcom/twitter/internal/android/service/a;)Lcom/twitter/library/service/e;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/library/service/e;->a(Lcom/twitter/library/service/e;)V

    invoke-virtual {v1}, Lcom/twitter/library/api/account/v;->e()Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/account/u;->v:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {p2}, Lcom/twitter/library/service/e;->e()Lcom/twitter/internal/network/k;

    move-result-object v0

    iget v0, v0, Lcom/twitter/internal/network/k;->a:I

    if-eq v0, v5, :cond_0

    const/16 v0, 0x190

    invoke-virtual {p2, v0}, Lcom/twitter/library/service/e;->a(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v1, 0x193

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/twitter/library/api/account/u;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/t;

    iput-object v0, p0, Lcom/twitter/library/api/account/u;->u:Lcom/twitter/library/api/t;

    :cond_2
    :goto_1
    invoke-virtual {p2, p1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    goto :goto_0

    :cond_3
    const/16 v1, 0x19c

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/twitter/library/api/account/u;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/t;

    iput-object v0, p0, Lcom/twitter/library/api/account/u;->u:Lcom/twitter/library/api/t;

    goto :goto_1
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/library/api/account/u;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/account/u;->p:Ljava/lang/String;

    return-object p0
.end method

.method public b(Z)Lcom/twitter/library/api/account/u;
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/api/account/u;->n:Z

    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/library/api/account/u;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/account/u;->s:Ljava/lang/String;

    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/library/api/account/u;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/account/u;->r:Ljava/lang/String;

    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/twitter/library/api/account/u;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/account/u;->q:Ljava/lang/String;

    return-object p0
.end method

.method public e()Lcom/twitter/library/network/LoginResponse;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/account/u;->t:Lcom/twitter/library/network/LoginResponse;

    return-object v0
.end method

.method public f()Lcom/twitter/library/api/t;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/account/u;->u:Lcom/twitter/library/api/t;

    return-object v0
.end method

.method public g()Lcom/twitter/library/api/TwitterUser;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/account/u;->v:Lcom/twitter/library/api/TwitterUser;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/account/u;->f:Ljava/lang/String;

    return-object v0
.end method
