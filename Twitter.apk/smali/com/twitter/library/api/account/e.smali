.class public abstract Lcom/twitter/library/api/account/e;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# instance fields
.field private d:Lcom/twitter/library/network/a;

.field private e:Z


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V
    .locals 1

    new-instance v0, Lcom/twitter/library/service/p;

    invoke-direct {v0, p3}, Lcom/twitter/library/service/p;-><init>(Lcom/twitter/library/client/Session;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/library/api/account/e;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/p;)V

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/p;)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/p;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/api/account/e;->e:Z

    iget-boolean v0, p3, Lcom/twitter/library/service/p;->b:Z

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/library/api/account/f;

    invoke-direct {v0, p1}, Lcom/twitter/library/api/account/f;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/twitter/library/api/account/e;->a(Lcom/twitter/internal/android/service/l;)Lcom/twitter/internal/android/service/a;

    :cond_0
    return-void
.end method


# virtual methods
.method protected final a(Lcom/twitter/library/service/e;)Lcom/twitter/internal/network/HttpOperation;
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/library/api/account/e;->v()Lcom/twitter/internal/android/service/AsyncService;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/api/account/e;->a(Lcom/twitter/internal/android/service/AsyncService;)Lcom/twitter/library/network/a;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    const-string/jumbo v1, "Failed to obtain any auth for this request"

    invoke-virtual {p1, v0, v1}, Lcom/twitter/library/service/e;->a(ILjava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1, v0}, Lcom/twitter/library/api/account/e;->a(Lcom/twitter/library/service/e;Lcom/twitter/library/network/a;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/service/e;Lcom/twitter/library/network/a;)Lcom/twitter/internal/network/HttpOperation;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final a(Lcom/twitter/internal/android/service/AsyncService;)Lcom/twitter/library/network/a;
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/library/api/account/e;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/account/e;->l:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/twitter/library/api/account/e;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/twitter/library/network/b;->a(Landroid/content/Context;Lcom/twitter/internal/android/service/AsyncService;Lcom/twitter/library/service/p;)Lcom/twitter/library/network/a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/account/e;->d:Lcom/twitter/library/network/a;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/api/account/e;->e:Z

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/api/account/e;->d:Lcom/twitter/library/network/a;

    return-object v0
.end method
