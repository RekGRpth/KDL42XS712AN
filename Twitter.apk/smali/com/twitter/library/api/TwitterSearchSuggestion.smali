.class public Lcom/twitter/library/api/TwitterSearchSuggestion;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field private static final serialVersionUID:J = 0x231d4b0a5cff0cbaL


# instance fields
.field public correctionIndices:Ljava/util/ArrayList;

.field public query:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/api/TwitterSearchSuggestion;->query:Ljava/lang/String;

    iput-object p2, p0, Lcom/twitter/library/api/TwitterSearchSuggestion;->correctionIndices:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 9

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne p0, p1, :cond_1

    move v3, v2

    :cond_0
    :goto_0
    return v3

    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-ne v0, v1, :cond_0

    check-cast p1, Lcom/twitter/library/api/TwitterSearchSuggestion;

    iget-object v0, p0, Lcom/twitter/library/api/TwitterSearchSuggestion;->query:Ljava/lang/String;

    if-nez v0, :cond_3

    iget-object v0, p1, Lcom/twitter/library/api/TwitterSearchSuggestion;->query:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v2

    :goto_1
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/TwitterSearchSuggestion;->correctionIndices:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    iget-object v0, p1, Lcom/twitter/library/api/TwitterSearchSuggestion;->correctionIndices:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v6, v0, :cond_0

    move v5, v3

    :goto_2
    if-ge v5, v6, :cond_5

    iget-object v0, p0, Lcom/twitter/library/api/TwitterSearchSuggestion;->correctionIndices:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iget-object v1, p1, Lcom/twitter/library/api/TwitterSearchSuggestion;->correctionIndices:Ljava/util/ArrayList;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    array-length v4, v0

    array-length v7, v1

    if-ne v4, v7, :cond_0

    move v4, v3

    :goto_3
    array-length v7, v0

    if-ge v4, v7, :cond_4

    aget v7, v0, v4

    aget v8, v1, v4

    if-ne v7, v8, :cond_0

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_2
    move v0, v3

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/twitter/library/api/TwitterSearchSuggestion;->query:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/library/api/TwitterSearchSuggestion;->query:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :cond_4
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_2

    :cond_5
    move v3, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/api/TwitterSearchSuggestion;->query:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/TwitterSearchSuggestion;->query:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/library/api/TwitterSearchSuggestion;->correctionIndices:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 7

    const/4 v1, 0x0

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterSearchSuggestion;->query:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    const/4 v4, 0x2

    new-array v4, v4, [I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v5

    aput v5, v4, v1

    const/4 v5, 0x1

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v6

    aput v6, v4, v5

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iput-object v3, p0, Lcom/twitter/library/api/TwitterSearchSuggestion;->correctionIndices:Ljava/util/ArrayList;

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/api/TwitterSearchSuggestion;->query:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterSearchSuggestion;->correctionIndices:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterSearchSuggestion;->correctionIndices:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    const/4 v2, 0x0

    aget v2, v0, v2

    invoke-interface {p1, v2}, Ljava/io/ObjectOutput;->writeInt(I)V

    const/4 v2, 0x1

    aget v0, v0, v2

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    goto :goto_0

    :cond_0
    return-void
.end method
