.class public Lcom/twitter/library/api/TwitterTopic$SportsEvent;
.super Lcom/twitter/library/api/TwitterTopic$Data;
.source "Twttr"


# static fields
.field private static final serialVersionUID:J = -0x4a832d8fba6c96abL


# instance fields
.field public channel:Ljava/lang/String;

.field public gameType:Ljava/lang/String;

.field public players:Ljava/util/ArrayList;

.field public sportsTitle:Ljava/lang/String;

.field public status:Ljava/lang/String;

.field public summary:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/library/api/TwitterTopic$Data;-><init>()V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/library/api/TwitterTopic$Data;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/api/TwitterTopic$SportsEvent;->sportsTitle:Ljava/lang/String;

    iput-object p2, p0, Lcom/twitter/library/api/TwitterTopic$SportsEvent;->gameType:Ljava/lang/String;

    iput-object p3, p0, Lcom/twitter/library/api/TwitterTopic$SportsEvent;->summary:Ljava/lang/String;

    iput-object p4, p0, Lcom/twitter/library/api/TwitterTopic$SportsEvent;->status:Ljava/lang/String;

    iput-object p5, p0, Lcom/twitter/library/api/TwitterTopic$SportsEvent;->channel:Ljava/lang/String;

    iput-object p6, p0, Lcom/twitter/library/api/TwitterTopic$SportsEvent;->players:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterTopic$SportsEvent;->sportsTitle:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterTopic$SportsEvent;->gameType:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterTopic$SportsEvent;->summary:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterTopic$SportsEvent;->status:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterTopic$SportsEvent;->channel:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterTopic$SportsEvent;->players:Ljava/util/ArrayList;

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/TwitterTopic$SportsEvent;->sportsTitle:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterTopic$SportsEvent;->gameType:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterTopic$SportsEvent;->summary:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterTopic$SportsEvent;->status:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterTopic$SportsEvent;->channel:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterTopic$SportsEvent;->players:Ljava/util/ArrayList;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    return-void
.end method
