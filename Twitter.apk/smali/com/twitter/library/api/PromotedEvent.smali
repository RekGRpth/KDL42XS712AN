.class public final enum Lcom/twitter/library/api/PromotedEvent;
.super Ljava/lang/Enum;
.source "Twttr"


# static fields
.field public static final enum a:Lcom/twitter/library/api/PromotedEvent;

.field public static final enum b:Lcom/twitter/library/api/PromotedEvent;

.field public static final enum c:Lcom/twitter/library/api/PromotedEvent;

.field public static final enum d:Lcom/twitter/library/api/PromotedEvent;

.field public static final enum e:Lcom/twitter/library/api/PromotedEvent;

.field public static final enum f:Lcom/twitter/library/api/PromotedEvent;

.field public static final enum g:Lcom/twitter/library/api/PromotedEvent;

.field public static final enum h:Lcom/twitter/library/api/PromotedEvent;

.field public static final enum i:Lcom/twitter/library/api/PromotedEvent;

.field public static final enum j:Lcom/twitter/library/api/PromotedEvent;

.field public static final enum k:Lcom/twitter/library/api/PromotedEvent;

.field public static final enum l:Lcom/twitter/library/api/PromotedEvent;

.field public static final enum m:Lcom/twitter/library/api/PromotedEvent;

.field public static final enum n:Lcom/twitter/library/api/PromotedEvent;

.field public static final enum o:Lcom/twitter/library/api/PromotedEvent;

.field public static final enum p:Lcom/twitter/library/api/PromotedEvent;

.field private static final synthetic q:[Lcom/twitter/library/api/PromotedEvent;


# instance fields
.field private final mEvent:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/twitter/library/api/PromotedEvent;

    const-string/jumbo v1, "IMPRESSION"

    const-string/jumbo v2, "impression"

    invoke-direct {v0, v1, v4, v2}, Lcom/twitter/library/api/PromotedEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/library/api/PromotedEvent;->a:Lcom/twitter/library/api/PromotedEvent;

    new-instance v0, Lcom/twitter/library/api/PromotedEvent;

    const-string/jumbo v1, "URL_CLICK"

    const-string/jumbo v2, "url_click"

    invoke-direct {v0, v1, v5, v2}, Lcom/twitter/library/api/PromotedEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/library/api/PromotedEvent;->b:Lcom/twitter/library/api/PromotedEvent;

    new-instance v0, Lcom/twitter/library/api/PromotedEvent;

    const-string/jumbo v1, "PROFILE_IMAGE_CLICK"

    const-string/jumbo v2, "profile_image_click"

    invoke-direct {v0, v1, v6, v2}, Lcom/twitter/library/api/PromotedEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/library/api/PromotedEvent;->c:Lcom/twitter/library/api/PromotedEvent;

    new-instance v0, Lcom/twitter/library/api/PromotedEvent;

    const-string/jumbo v1, "SCREEN_NAME_CLICK"

    const-string/jumbo v2, "screen_name_click"

    invoke-direct {v0, v1, v7, v2}, Lcom/twitter/library/api/PromotedEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/library/api/PromotedEvent;->d:Lcom/twitter/library/api/PromotedEvent;

    new-instance v0, Lcom/twitter/library/api/PromotedEvent;

    const-string/jumbo v1, "HASHTAG_CLICK"

    const-string/jumbo v2, "hashtag_click"

    invoke-direct {v0, v1, v8, v2}, Lcom/twitter/library/api/PromotedEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/library/api/PromotedEvent;->e:Lcom/twitter/library/api/PromotedEvent;

    new-instance v0, Lcom/twitter/library/api/PromotedEvent;

    const-string/jumbo v1, "USER_MENTION_CLICK"

    const/4 v2, 0x5

    const-string/jumbo v3, "user_mention_click"

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/api/PromotedEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/library/api/PromotedEvent;->f:Lcom/twitter/library/api/PromotedEvent;

    new-instance v0, Lcom/twitter/library/api/PromotedEvent;

    const-string/jumbo v1, "VIEW_DETAILS"

    const/4 v2, 0x6

    const-string/jumbo v3, "view_details"

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/api/PromotedEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/library/api/PromotedEvent;->g:Lcom/twitter/library/api/PromotedEvent;

    new-instance v0, Lcom/twitter/library/api/PromotedEvent;

    const-string/jumbo v1, "PROMOTED_TREND_CLICK"

    const/4 v2, 0x7

    const-string/jumbo v3, "click"

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/api/PromotedEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/library/api/PromotedEvent;->h:Lcom/twitter/library/api/PromotedEvent;

    new-instance v0, Lcom/twitter/library/api/PromotedEvent;

    const-string/jumbo v1, "DISMISS"

    const/16 v2, 0x8

    const-string/jumbo v3, "dismiss"

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/api/PromotedEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/library/api/PromotedEvent;->i:Lcom/twitter/library/api/PromotedEvent;

    new-instance v0, Lcom/twitter/library/api/PromotedEvent;

    const-string/jumbo v1, "FOOTER_PROFILE"

    const/16 v2, 0x9

    const-string/jumbo v3, "footer_profile"

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/api/PromotedEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/library/api/PromotedEvent;->j:Lcom/twitter/library/api/PromotedEvent;

    new-instance v0, Lcom/twitter/library/api/PromotedEvent;

    const-string/jumbo v1, "CARD_URL_CLICK"

    const/16 v2, 0xa

    const-string/jumbo v3, "card_url_click"

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/api/PromotedEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/library/api/PromotedEvent;->k:Lcom/twitter/library/api/PromotedEvent;

    new-instance v0, Lcom/twitter/library/api/PromotedEvent;

    const-string/jumbo v1, "CARD_MEDIA_CLICK"

    const/16 v2, 0xb

    const-string/jumbo v3, "embedded_media"

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/api/PromotedEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/library/api/PromotedEvent;->l:Lcom/twitter/library/api/PromotedEvent;

    new-instance v0, Lcom/twitter/library/api/PromotedEvent;

    const-string/jumbo v1, "CARD_OPEN_APP"

    const/16 v2, 0xc

    const-string/jumbo v3, "open_app"

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/api/PromotedEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/library/api/PromotedEvent;->m:Lcom/twitter/library/api/PromotedEvent;

    new-instance v0, Lcom/twitter/library/api/PromotedEvent;

    const-string/jumbo v1, "CARD_INSTALL_APP"

    const/16 v2, 0xd

    const-string/jumbo v3, "install_app"

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/api/PromotedEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/library/api/PromotedEvent;->n:Lcom/twitter/library/api/PromotedEvent;

    new-instance v0, Lcom/twitter/library/api/PromotedEvent;

    const-string/jumbo v1, "CARD_DIAL_PHONE"

    const/16 v2, 0xe

    const-string/jumbo v3, "dial_phone"

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/api/PromotedEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/library/api/PromotedEvent;->o:Lcom/twitter/library/api/PromotedEvent;

    new-instance v0, Lcom/twitter/library/api/PromotedEvent;

    const-string/jumbo v1, "CASHTAG_CLICK"

    const/16 v2, 0xf

    const-string/jumbo v3, "cashtag_click"

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/api/PromotedEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/library/api/PromotedEvent;->p:Lcom/twitter/library/api/PromotedEvent;

    const/16 v0, 0x10

    new-array v0, v0, [Lcom/twitter/library/api/PromotedEvent;

    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->a:Lcom/twitter/library/api/PromotedEvent;

    aput-object v1, v0, v4

    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->b:Lcom/twitter/library/api/PromotedEvent;

    aput-object v1, v0, v5

    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->c:Lcom/twitter/library/api/PromotedEvent;

    aput-object v1, v0, v6

    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->d:Lcom/twitter/library/api/PromotedEvent;

    aput-object v1, v0, v7

    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->e:Lcom/twitter/library/api/PromotedEvent;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->f:Lcom/twitter/library/api/PromotedEvent;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->g:Lcom/twitter/library/api/PromotedEvent;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->h:Lcom/twitter/library/api/PromotedEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->i:Lcom/twitter/library/api/PromotedEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->j:Lcom/twitter/library/api/PromotedEvent;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->k:Lcom/twitter/library/api/PromotedEvent;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->l:Lcom/twitter/library/api/PromotedEvent;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->m:Lcom/twitter/library/api/PromotedEvent;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->n:Lcom/twitter/library/api/PromotedEvent;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->o:Lcom/twitter/library/api/PromotedEvent;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->p:Lcom/twitter/library/api/PromotedEvent;

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/library/api/PromotedEvent;->q:[Lcom/twitter/library/api/PromotedEvent;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/twitter/library/api/PromotedEvent;->mEvent:Ljava/lang/String;

    return-void
.end method

.method public static a(I)Lcom/twitter/library/api/PromotedEvent;
    .locals 1

    invoke-static {}, Lcom/twitter/library/api/PromotedEvent;->values()[Lcom/twitter/library/api/PromotedEvent;

    move-result-object v0

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/library/api/PromotedEvent;
    .locals 1

    const-class v0, Lcom/twitter/library/api/PromotedEvent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/PromotedEvent;

    return-object v0
.end method

.method public static values()[Lcom/twitter/library/api/PromotedEvent;
    .locals 1

    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->q:[Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {v0}, [Lcom/twitter/library/api/PromotedEvent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/api/PromotedEvent;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/PromotedEvent;->mEvent:Ljava/lang/String;

    return-object v0
.end method
