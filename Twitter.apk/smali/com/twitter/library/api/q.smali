.class public Lcom/twitter/library/api/q;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    const-class v0, Lcom/twitter/library/api/q;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Z)Lcom/twitter/library/service/b;
    .locals 5

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/library/util/a;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/api/q;

    invoke-direct {v1, p0, p1}, Lcom/twitter/library/api/q;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/twitter/library/api/q;->c(I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "user_id"

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "format"

    invoke-virtual {v1, v2, p2}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "account"

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Landroid/os/Parcelable;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "has_unknown_phone_number"

    invoke-virtual {v0, v1, p3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/twitter/library/service/e;Ljava/lang/String;)V
    .locals 8

    invoke-virtual {p0}, Lcom/twitter/library/api/q;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/q;->k:Landroid/os/Bundle;

    const-string/jumbo v2, "user_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    const-string/jumbo v4, "prompt_id"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iget-object v4, p0, Lcom/twitter/library/api/q;->m:Lcom/twitter/library/network/aa;

    iget-object v4, v4, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string/jumbo v7, "1.1"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "prompts"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string/jumbo v7, "record_event"

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ".json"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v7, "user_id"

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v6, v7, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "prompt_id"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v2, "action"

    invoke-direct {v1, v2, p2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/twitter/library/network/d;

    iget-object v2, p0, Lcom/twitter/library/api/q;->l:Landroid/content/Context;

    invoke-direct {v1, v2, v4}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v2, v0, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v1

    sget-object v2, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v1, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/network/n;

    iget-object v0, v0, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v2, v0}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v1, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void
.end method

.method private c(Lcom/twitter/library/service/e;)V
    .locals 13

    const/4 v12, 0x1

    const/4 v11, 0x0

    invoke-virtual {p0}, Lcom/twitter/library/api/q;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/api/q;->l:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/library/api/q;->k:Landroid/os/Bundle;

    const-string/jumbo v0, "phone"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    const-string/jumbo v0, "format"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v0, "lang"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v0, "account"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    const-string/jumbo v7, "tweet_ids"

    invoke-virtual {v3, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v7, p0, Lcom/twitter/library/api/q;->m:Lcom/twitter/library/network/aa;

    iget-object v7, v7, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const-string/jumbo v9, "1.1"

    aput-object v9, v8, v11

    const-string/jumbo v9, "prompts"

    aput-object v9, v8, v12

    const/4 v9, 0x2

    const-string/jumbo v10, "suggest"

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ".json"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "format"

    invoke-static {v7, v8, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v5, "client_namespace"

    const-string/jumbo v8, "native"

    invoke-static {v7, v5, v8}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v5, "force_user_language"

    invoke-static {v7, v5, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v5, "has_unknown_phone_number"

    invoke-static {v7, v5, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v4, "notifications_device"

    invoke-static {v2}, Lcom/twitter/library/platform/PushService;->c(Landroid/content/Context;)Z

    move-result v5

    invoke-static {v7, v4, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v4, "notifications_twitter"

    invoke-static {v2, v0}, Lcom/twitter/library/platform/PushService;->b(Landroid/content/Context;Landroid/accounts/Account;)I

    move-result v5

    invoke-static {v7, v4, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    if-eqz v0, :cond_0

    const-string/jumbo v4, "notifications_app"

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/twitter/library/platform/PushService;->f(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    invoke-static {v7, v4, v0}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_0
    if-eqz v3, :cond_1

    const-string/jumbo v0, "tweet_ids"

    invoke-static {v7, v0, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-static {v2}, Lcom/twitter/library/util/v;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "no_play_store"

    invoke-static {v7, v0, v12}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_2
    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/api/q;->l:Landroid/content/Context;

    const-string/jumbo v3, "debug_prefs"

    invoke-virtual {v0, v3, v11}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v3, "pb_force_campaign_enabled"

    invoke-interface {v0, v3, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string/jumbo v3, "pb_force_campaign_id"

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "pb_force_campaign_cookie"

    const-string/jumbo v5, ""

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "pb_force_campaign_sticky"

    invoke-interface {v0, v5, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    if-eqz v3, :cond_3

    const-string/jumbo v6, "force_campaign"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "targeting_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v7, v6, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v3, "force_fatigue_on_override"

    invoke-static {v7, v3, v12}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v3, "force_cookie"

    invoke-static {v7, v3, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v3, "pb_force_campaign_enabled"

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_3
    const/16 v0, 0x29

    invoke-static {v0}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v0

    new-instance v3, Lcom/twitter/library/network/d;

    invoke-direct {v3, v2, v7}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v4, v1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v3, v4, v5}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/network/n;

    iget-object v1, v1, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v3, v1}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v2, v3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/Prompt;

    iget-object v2, p1, Lcom/twitter/library/service/e;->a:Landroid/os/Bundle;

    const-string/jumbo v3, "prompt"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_4
    invoke-virtual {p1, v1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void
.end method


# virtual methods
.method protected b(Lcom/twitter/library/service/e;)V
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/library/api/q;->r()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Invalid action "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    invoke-direct {p0, p1}, Lcom/twitter/library/api/q;->c(Lcom/twitter/library/service/e;)V

    :goto_0
    return-void

    :pswitch_1
    const-string/jumbo v0, "acted_on"

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/api/q;->a(Lcom/twitter/library/service/e;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    const-string/jumbo v0, "dismissed"

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/api/q;->a(Lcom/twitter/library/service/e;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_3
    const-string/jumbo v0, "shown"

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/api/q;->a(Lcom/twitter/library/service/e;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
