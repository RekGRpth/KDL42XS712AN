.class public Lcom/twitter/library/api/ae;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/api/k;


# instance fields
.field public final a:I

.field public final b:J

.field public final c:J

.field public final d:J

.field public final e:I

.field public final f:I

.field public final g:Ljava/util/ArrayList;

.field public final h:I

.field public final i:I

.field public final j:Ljava/util/ArrayList;

.field public final k:Ljava/util/ArrayList;

.field public final l:Ljava/util/ArrayList;

.field public final m:I

.field public final n:I

.field public final o:Ljava/util/ArrayList;

.field public final p:Ljava/util/ArrayList;

.field public final q:Ljava/util/ArrayList;

.field public final r:I

.field public final s:J


# direct methods
.method public constructor <init>(IJJJIILjava/util/ArrayList;IILjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;IILjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;IJ)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/twitter/library/api/ae;->a:I

    iput-wide p2, p0, Lcom/twitter/library/api/ae;->b:J

    iput-wide p4, p0, Lcom/twitter/library/api/ae;->c:J

    iput-wide p6, p0, Lcom/twitter/library/api/ae;->d:J

    iput p8, p0, Lcom/twitter/library/api/ae;->e:I

    iput p9, p0, Lcom/twitter/library/api/ae;->f:I

    iput-object p10, p0, Lcom/twitter/library/api/ae;->g:Ljava/util/ArrayList;

    iput p11, p0, Lcom/twitter/library/api/ae;->h:I

    iput p12, p0, Lcom/twitter/library/api/ae;->i:I

    iput-object p13, p0, Lcom/twitter/library/api/ae;->j:Ljava/util/ArrayList;

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/twitter/library/api/ae;->k:Ljava/util/ArrayList;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/twitter/library/api/ae;->l:Ljava/util/ArrayList;

    move/from16 v0, p16

    iput v0, p0, Lcom/twitter/library/api/ae;->m:I

    move/from16 v0, p17

    iput v0, p0, Lcom/twitter/library/api/ae;->n:I

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/twitter/library/api/ae;->o:Ljava/util/ArrayList;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/twitter/library/api/ae;->p:Ljava/util/ArrayList;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/twitter/library/api/ae;->q:Ljava/util/ArrayList;

    move/from16 v0, p21

    iput v0, p0, Lcom/twitter/library/api/ae;->r:I

    move-wide/from16 v0, p22

    iput-wide v0, p0, Lcom/twitter/library/api/ae;->s:J

    return-void
.end method

.method private a(Ljava/util/ArrayList;)I
    .locals 6

    const/4 v2, 0x0

    if-nez p1, :cond_0

    :goto_0
    return v2

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/k;

    mul-int/lit8 v1, v1, 0x1f

    if-nez v0, :cond_1

    move v0, v2

    :goto_2
    add-int/2addr v0, v1

    move v1, v0

    goto :goto_1

    :cond_1
    invoke-interface {v0}, Lcom/twitter/library/api/k;->a()J

    move-result-wide v4

    long-to-int v0, v4

    goto :goto_2

    :cond_2
    move v2, v1

    goto :goto_0
.end method

.method private a(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
    .locals 8

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne p1, p2, :cond_1

    move v3, v2

    :cond_0
    :goto_0
    return v3

    :cond_1
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->listIterator()Ljava/util/ListIterator;

    move-result-object v4

    invoke-virtual {p2}, Ljava/util/ArrayList;->listIterator()Ljava/util/ListIterator;

    move-result-object v5

    :cond_2
    invoke-interface {v4}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/k;

    invoke-interface {v5}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/k;

    if-nez v0, :cond_3

    if-nez v1, :cond_0

    :cond_3
    if-eqz v0, :cond_4

    if-eqz v1, :cond_0

    :cond_4
    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/twitter/library/api/k;->a()J

    move-result-wide v6

    invoke-interface {v1}, Lcom/twitter/library/api/k;->a()J

    move-result-wide v0

    cmp-long v0, v6, v0

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_5
    invoke-interface {v4}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-interface {v5}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v2

    :goto_1
    move v3, v0

    goto :goto_0

    :cond_6
    move v0, v3

    goto :goto_1
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/api/ae;->b:J

    return-wide v0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/api/ae;->b:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/twitter/library/api/ae;

    iget-wide v2, p0, Lcom/twitter/library/api/ae;->b:J

    iget-wide v4, p1, Lcom/twitter/library/api/ae;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget v2, p0, Lcom/twitter/library/api/ae;->a:I

    iget v3, p1, Lcom/twitter/library/api/ae;->a:I

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-wide v2, p0, Lcom/twitter/library/api/ae;->c:J

    iget-wide v4, p1, Lcom/twitter/library/api/ae;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-wide v2, p0, Lcom/twitter/library/api/ae;->d:J

    iget-wide v4, p1, Lcom/twitter/library/api/ae;->d:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget v2, p0, Lcom/twitter/library/api/ae;->f:I

    iget v3, p1, Lcom/twitter/library/api/ae;->f:I

    if-eq v2, v3, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget v2, p0, Lcom/twitter/library/api/ae;->e:I

    iget v3, p1, Lcom/twitter/library/api/ae;->e:I

    if-eq v2, v3, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    iget v2, p0, Lcom/twitter/library/api/ae;->n:I

    iget v3, p1, Lcom/twitter/library/api/ae;->n:I

    if-eq v2, v3, :cond_a

    move v0, v1

    goto :goto_0

    :cond_a
    iget v2, p0, Lcom/twitter/library/api/ae;->m:I

    iget v3, p1, Lcom/twitter/library/api/ae;->m:I

    if-eq v2, v3, :cond_b

    move v0, v1

    goto :goto_0

    :cond_b
    iget v2, p0, Lcom/twitter/library/api/ae;->i:I

    iget v3, p1, Lcom/twitter/library/api/ae;->i:I

    if-eq v2, v3, :cond_c

    move v0, v1

    goto :goto_0

    :cond_c
    iget v2, p0, Lcom/twitter/library/api/ae;->h:I

    iget v3, p1, Lcom/twitter/library/api/ae;->h:I

    if-eq v2, v3, :cond_d

    move v0, v1

    goto :goto_0

    :cond_d
    iget-object v2, p0, Lcom/twitter/library/api/ae;->g:Ljava/util/ArrayList;

    iget-object v3, p1, Lcom/twitter/library/api/ae;->g:Ljava/util/ArrayList;

    invoke-direct {p0, v2, v3}, Lcom/twitter/library/api/ae;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    goto :goto_0

    :cond_e
    iget-object v2, p0, Lcom/twitter/library/api/ae;->j:Ljava/util/ArrayList;

    iget-object v3, p1, Lcom/twitter/library/api/ae;->j:Ljava/util/ArrayList;

    invoke-direct {p0, v2, v3}, Lcom/twitter/library/api/ae;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v2

    if-nez v2, :cond_f

    move v0, v1

    goto :goto_0

    :cond_f
    iget-object v2, p0, Lcom/twitter/library/api/ae;->k:Ljava/util/ArrayList;

    iget-object v3, p1, Lcom/twitter/library/api/ae;->k:Ljava/util/ArrayList;

    invoke-direct {p0, v2, v3}, Lcom/twitter/library/api/ae;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v2

    if-nez v2, :cond_10

    move v0, v1

    goto/16 :goto_0

    :cond_10
    iget-object v2, p0, Lcom/twitter/library/api/ae;->l:Ljava/util/ArrayList;

    iget-object v3, p1, Lcom/twitter/library/api/ae;->l:Ljava/util/ArrayList;

    invoke-direct {p0, v2, v3}, Lcom/twitter/library/api/ae;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v2

    if-nez v2, :cond_11

    move v0, v1

    goto/16 :goto_0

    :cond_11
    iget-object v2, p0, Lcom/twitter/library/api/ae;->o:Ljava/util/ArrayList;

    iget-object v3, p1, Lcom/twitter/library/api/ae;->o:Ljava/util/ArrayList;

    invoke-direct {p0, v2, v3}, Lcom/twitter/library/api/ae;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v2

    if-nez v2, :cond_12

    move v0, v1

    goto/16 :goto_0

    :cond_12
    iget-object v2, p0, Lcom/twitter/library/api/ae;->p:Ljava/util/ArrayList;

    iget-object v3, p1, Lcom/twitter/library/api/ae;->p:Ljava/util/ArrayList;

    invoke-direct {p0, v2, v3}, Lcom/twitter/library/api/ae;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v2

    if-nez v2, :cond_13

    move v0, v1

    goto/16 :goto_0

    :cond_13
    iget-object v2, p0, Lcom/twitter/library/api/ae;->q:Ljava/util/ArrayList;

    iget-object v3, p1, Lcom/twitter/library/api/ae;->q:Ljava/util/ArrayList;

    if-eq v2, v3, :cond_15

    iget-object v2, p0, Lcom/twitter/library/api/ae;->q:Ljava/util/ArrayList;

    if-eqz v2, :cond_14

    iget-object v2, p1, Lcom/twitter/library/api/ae;->q:Ljava/util/ArrayList;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lcom/twitter/library/api/ae;->q:Ljava/util/ArrayList;

    iget-object v3, p1, Lcom/twitter/library/api/ae;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_14
    move v0, v1

    goto/16 :goto_0

    :cond_15
    iget v2, p0, Lcom/twitter/library/api/ae;->r:I

    iget v3, p1, Lcom/twitter/library/api/ae;->r:I

    if-eq v2, v3, :cond_16

    move v0, v1

    goto/16 :goto_0

    :cond_16
    iget-wide v2, p0, Lcom/twitter/library/api/ae;->s:J

    iget-wide v4, p1, Lcom/twitter/library/api/ae;->s:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 6

    const/16 v5, 0x20

    iget v0, p0, Lcom/twitter/library/api/ae;->a:I

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/twitter/library/api/ae;->b:J

    iget-wide v3, p0, Lcom/twitter/library/api/ae;->b:J

    ushr-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/twitter/library/api/ae;->c:J

    iget-wide v3, p0, Lcom/twitter/library/api/ae;->c:J

    ushr-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/twitter/library/api/ae;->d:J

    iget-wide v3, p0, Lcom/twitter/library/api/ae;->d:J

    ushr-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/twitter/library/api/ae;->s:J

    iget-wide v3, p0, Lcom/twitter/library/api/ae;->s:J

    ushr-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/library/api/ae;->e:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/library/api/ae;->f:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/library/api/ae;->g:Ljava/util/ArrayList;

    invoke-direct {p0, v1}, Lcom/twitter/library/api/ae;->a(Ljava/util/ArrayList;)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/library/api/ae;->h:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/library/api/ae;->i:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/library/api/ae;->j:Ljava/util/ArrayList;

    invoke-direct {p0, v1}, Lcom/twitter/library/api/ae;->a(Ljava/util/ArrayList;)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/library/api/ae;->k:Ljava/util/ArrayList;

    invoke-direct {p0, v1}, Lcom/twitter/library/api/ae;->a(Ljava/util/ArrayList;)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/library/api/ae;->l:Ljava/util/ArrayList;

    invoke-direct {p0, v1}, Lcom/twitter/library/api/ae;->a(Ljava/util/ArrayList;)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/library/api/ae;->m:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/library/api/ae;->n:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/library/api/ae;->o:Ljava/util/ArrayList;

    invoke-direct {p0, v1}, Lcom/twitter/library/api/ae;->a(Ljava/util/ArrayList;)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/library/api/ae;->p:Ljava/util/ArrayList;

    invoke-direct {p0, v1}, Lcom/twitter/library/api/ae;->a(Ljava/util/ArrayList;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/twitter/library/api/ae;->q:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/api/ae;->q:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    mul-int/lit8 v0, v1, 0x1f

    long-to-int v1, v3

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_0
    move v1, v0

    :cond_1
    mul-int/lit8 v0, v1, 0x1f

    iget v1, p0, Lcom/twitter/library/api/ae;->r:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "event="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/twitter/library/api/ae;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", createdAt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/library/api/ae;->b:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", maxPosition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/library/api/ae;->c:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", minPosition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/library/api/ae;->d:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", sourcesSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/twitter/library/api/ae;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", sourceType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/twitter/library/api/ae;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", targetsSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/twitter/library/api/ae;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", targetType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/twitter/library/api/ae;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", targetObjectsSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/twitter/library/api/ae;->m:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", targetObjectType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/twitter/library/api/ae;->n:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", enhancedSources="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/ae;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", enhancedSourcesSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/twitter/library/api/ae;->r:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", magicRecId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/library/api/ae;->s:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
