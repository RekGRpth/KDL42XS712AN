.class public Lcom/twitter/library/api/ClientConfiguration;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Lcom/twitter/library/api/UrlConfiguration;

.field public final b:Lcom/twitter/library/api/ad;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/library/api/e;

    invoke-direct {v0}, Lcom/twitter/library/api/e;-><init>()V

    sput-object v0, Lcom/twitter/library/api/ClientConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/twitter/library/api/UrlConfiguration;

    invoke-direct {v0, p1}, Lcom/twitter/library/api/UrlConfiguration;-><init>(Landroid/os/Parcel;)V

    iput-object v0, p0, Lcom/twitter/library/api/ClientConfiguration;->a:Lcom/twitter/library/api/UrlConfiguration;

    new-instance v0, Lcom/twitter/library/api/ad;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/library/api/ad;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/library/api/ClientConfiguration;->b:Lcom/twitter/library/api/ad;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/api/UrlConfiguration;Lcom/twitter/library/api/ad;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/api/ClientConfiguration;->a:Lcom/twitter/library/api/UrlConfiguration;

    iput-object p2, p0, Lcom/twitter/library/api/ClientConfiguration;->b:Lcom/twitter/library/api/ad;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/ClientConfiguration;->a:Lcom/twitter/library/api/UrlConfiguration;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/api/UrlConfiguration;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object v0, p0, Lcom/twitter/library/api/ClientConfiguration;->b:Lcom/twitter/library/api/ad;

    invoke-virtual {v0}, Lcom/twitter/library/api/ad;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
