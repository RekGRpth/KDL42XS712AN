.class public Lcom/twitter/library/api/RepliedUser;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field private static final serialVersionUID:J = -0x7ab6eae7a65d30cL


# instance fields
.field public name:Ljava/lang/String;

.field public screenName:Ljava/lang/String;

.field public userId:J


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JLjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/api/RepliedUser;->screenName:Ljava/lang/String;

    iput-wide p2, p0, Lcom/twitter/library/api/RepliedUser;->userId:J

    iput-object p4, p0, Lcom/twitter/library/api/RepliedUser;->name:Ljava/lang/String;

    return-void
.end method

.method public static a(Lcom/twitter/library/api/MentionEntity;)Lcom/twitter/library/api/RepliedUser;
    .locals 5

    new-instance v0, Lcom/twitter/library/api/RepliedUser;

    iget-object v1, p0, Lcom/twitter/library/api/MentionEntity;->screenName:Ljava/lang/String;

    iget-wide v2, p0, Lcom/twitter/library/api/MentionEntity;->userId:J

    iget-object v4, p0, Lcom/twitter/library/api/MentionEntity;->name:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/library/api/RepliedUser;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    return-object v0
.end method

.method public static a(Lcom/twitter/library/provider/ParcelableTweet;)Lcom/twitter/library/api/RepliedUser;
    .locals 5

    new-instance v0, Lcom/twitter/library/api/RepliedUser;

    invoke-interface {p0}, Lcom/twitter/library/provider/ParcelableTweet;->i()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0}, Lcom/twitter/library/provider/ParcelableTweet;->g()J

    move-result-wide v2

    invoke-interface {p0}, Lcom/twitter/library/provider/ParcelableTweet;->j()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/library/api/RepliedUser;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    return-object v0
.end method

.method public static b(Lcom/twitter/library/provider/ParcelableTweet;)Lcom/twitter/library/api/RepliedUser;
    .locals 5

    new-instance v0, Lcom/twitter/library/api/RepliedUser;

    invoke-interface {p0}, Lcom/twitter/library/provider/ParcelableTweet;->h()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0}, Lcom/twitter/library/provider/ParcelableTweet;->f()J

    move-result-wide v2

    invoke-interface {p0}, Lcom/twitter/library/provider/ParcelableTweet;->k()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/library/api/RepliedUser;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/RepliedUser;->name:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/RepliedUser;->screenName:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/api/RepliedUser;->name:Ljava/lang/String;

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/twitter/library/api/RepliedUser;

    iget-wide v2, p0, Lcom/twitter/library/api/RepliedUser;->userId:J

    iget-wide v4, p1, Lcom/twitter/library/api/RepliedUser;->userId:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    iget-wide v0, p0, Lcom/twitter/library/api/RepliedUser;->userId:J

    iget-wide v2, p0, Lcom/twitter/library/api/RepliedUser;->userId:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 2

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/RepliedUser;->screenName:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/api/RepliedUser;->userId:J

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/RepliedUser;->name:Ljava/lang/String;

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/api/RepliedUser;->screenName:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-wide v0, p0, Lcom/twitter/library/api/RepliedUser;->userId:J

    invoke-interface {p1, v0, v1}, Ljava/io/ObjectOutput;->writeLong(J)V

    iget-object v0, p0, Lcom/twitter/library/api/RepliedUser;->name:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    return-void
.end method
