.class public Lcom/twitter/library/api/TwitterStatus;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/api/k;


# instance fields
.field public A:I

.field public B:I

.field public final C:Ljava/lang/String;

.field public D:Z

.field public E:Z

.field public final a:J

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:J

.field public final e:J

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Lcom/twitter/library/api/TwitterStatus;

.field public final j:Lcom/twitter/library/api/TweetEntities;

.field public final k:I

.field public final l:Lcom/twitter/library/api/geo/TwitterPlace;

.field public final m:Lcom/twitter/library/api/TwitterStatusCard;

.field public final n:Ljava/lang/String;

.field public final o:J

.field public p:Lcom/twitter/library/api/PromotedContent;

.field public q:Lcom/twitter/library/api/RecommendedContent;

.field public r:J

.field public s:Z

.field public t:Lcom/twitter/library/api/TwitterUser;

.field public u:Z

.field public v:Lcom/twitter/library/api/au;

.field public final w:Z

.field public final x:Z

.field public final y:Z

.field public z:I


# direct methods
.method public constructor <init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJZLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/api/geo/TwitterPlace;Lcom/twitter/library/api/TwitterStatus;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/api/TweetEntities;IJLcom/twitter/library/api/PromotedContent;Lcom/twitter/library/api/RecommendedContent;Lcom/twitter/library/api/TwitterStatusCard;Lcom/twitter/library/api/au;ZZIIILjava/lang/String;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/twitter/library/api/TwitterStatus;->a:J

    iput-object p5, p0, Lcom/twitter/library/api/TwitterStatus;->b:Ljava/lang/String;

    iput-object p6, p0, Lcom/twitter/library/api/TwitterStatus;->c:Ljava/lang/String;

    iput-wide p3, p0, Lcom/twitter/library/api/TwitterStatus;->o:J

    iput-boolean p12, p0, Lcom/twitter/library/api/TwitterStatus;->s:Z

    iput-wide p8, p0, Lcom/twitter/library/api/TwitterStatus;->d:J

    iput-wide p10, p0, Lcom/twitter/library/api/TwitterStatus;->e:J

    iput-object p7, p0, Lcom/twitter/library/api/TwitterStatus;->f:Ljava/lang/String;

    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/twitter/library/api/TwitterStatus;->g:Ljava/lang/String;

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/twitter/library/api/TwitterStatus;->h:Ljava/lang/String;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/twitter/library/api/TwitterStatus;->i:Lcom/twitter/library/api/TwitterStatus;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/twitter/library/api/TwitterStatus;->t:Lcom/twitter/library/api/TwitterUser;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/twitter/library/api/TwitterStatus;->j:Lcom/twitter/library/api/TweetEntities;

    move/from16 v0, p19

    iput v0, p0, Lcom/twitter/library/api/TwitterStatus;->k:I

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/twitter/library/api/TwitterStatus;->l:Lcom/twitter/library/api/geo/TwitterPlace;

    move-wide/from16 v0, p20

    iput-wide v0, p0, Lcom/twitter/library/api/TwitterStatus;->r:J

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/twitter/library/api/TwitterStatus;->p:Lcom/twitter/library/api/PromotedContent;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/twitter/library/api/TwitterStatus;->q:Lcom/twitter/library/api/RecommendedContent;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/twitter/library/api/TwitterStatus;->m:Lcom/twitter/library/api/TwitterStatusCard;

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/twitter/library/api/TwitterStatus;->u:Z

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/twitter/library/api/TwitterStatus;->v:Lcom/twitter/library/api/au;

    move-object/from16 v0, p18

    invoke-static {p7, v0}, Lcom/twitter/library/util/Util;->a(Ljava/lang/CharSequence;Lcom/twitter/library/api/TweetEntities;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/library/api/TwitterStatus;->w:Z

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, p7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p18

    invoke-static {v2, v0}, Lcom/twitter/library/api/TweetEntities;->a(Ljava/lang/StringBuilder;Lcom/twitter/library/api/TweetEntities;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/library/api/TwitterStatus;->n:Ljava/lang/String;

    move/from16 v0, p26

    iput-boolean v0, p0, Lcom/twitter/library/api/TwitterStatus;->x:Z

    move/from16 v0, p27

    iput-boolean v0, p0, Lcom/twitter/library/api/TwitterStatus;->y:Z

    move/from16 v0, p28

    iput v0, p0, Lcom/twitter/library/api/TwitterStatus;->z:I

    move/from16 v0, p29

    iput v0, p0, Lcom/twitter/library/api/TwitterStatus;->A:I

    move/from16 v0, p30

    iput v0, p0, Lcom/twitter/library/api/TwitterStatus;->B:I

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/twitter/library/api/TwitterStatus;->C:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/api/TwitterStatus;->i:Lcom/twitter/library/api/TwitterStatus;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/TwitterStatus;->i:Lcom/twitter/library/api/TwitterStatus;

    iget-wide v0, v0, Lcom/twitter/library/api/TwitterStatus;->a:J

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/twitter/library/api/TwitterStatus;->a:J

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/library/api/TwitterStatus;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/twitter/library/api/TwitterStatus;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/TwitterStatus;->i:Lcom/twitter/library/api/TwitterStatus;

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/twitter/library/api/TwitterStatus;->i:Lcom/twitter/library/api/TwitterStatus;

    :cond_0
    return-object p0
.end method

.method public d()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/api/TwitterStatus;->v:Lcom/twitter/library/api/au;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "popular"

    iget-object v1, p0, Lcom/twitter/library/api/TwitterStatus;->v:Lcom/twitter/library/api/au;

    iget-object v1, v1, Lcom/twitter/library/api/au;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/api/TwitterStatus;->v:Lcom/twitter/library/api/au;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "news"

    iget-object v1, p0, Lcom/twitter/library/api/TwitterStatus;->v:Lcom/twitter/library/api/au;

    iget-object v1, v1, Lcom/twitter/library/api/au;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/TwitterStatus;->p:Lcom/twitter/library/api/PromotedContent;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/TwitterStatus;->q:Lcom/twitter/library/api/RecommendedContent;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
