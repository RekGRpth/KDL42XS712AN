.class public Lcom/twitter/library/api/TweetClassicCard;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Externalizable;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x7c53b58207cf9dffL


# instance fields
.field private transient a:Lcom/twitter/library/api/aa;

.field public authorUser:Lcom/twitter/library/api/CardUser;

.field private transient b:Lcom/twitter/library/api/aa;

.field private transient c:Lcom/twitter/library/api/aa;

.field public description:Ljava/lang/String;

.field public features:Ljava/util/HashMap;

.field public imageHeight:I

.field public imageUrl:Ljava/lang/String;

.field public imageUrlLarge:Ljava/lang/String;

.field public imageUrlSmall:Ljava/lang/String;

.field public imageWidth:I

.field public playerStreamUrl:Ljava/lang/String;

.field public playerType:I

.field public playerUrl:Ljava/lang/String;

.field public promotion:Lcom/twitter/library/api/Promotion;

.field public siteUser:Lcom/twitter/library/api/CardUser;

.field public title:Ljava/lang/String;

.field public type:I

.field public url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->features:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Lcom/twitter/library/api/Promotion;Lcom/twitter/library/api/CardUser;Lcom/twitter/library/api/CardUser;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/twitter/library/api/TweetClassicCard;->features:Ljava/util/HashMap;

    iput p1, p0, Lcom/twitter/library/api/TweetClassicCard;->type:I

    iput-object p2, p0, Lcom/twitter/library/api/TweetClassicCard;->title:Ljava/lang/String;

    iput-object p3, p0, Lcom/twitter/library/api/TweetClassicCard;->description:Ljava/lang/String;

    iput-object p4, p0, Lcom/twitter/library/api/TweetClassicCard;->url:Ljava/lang/String;

    iput-object p5, p0, Lcom/twitter/library/api/TweetClassicCard;->imageUrl:Ljava/lang/String;

    iput-object p6, p0, Lcom/twitter/library/api/TweetClassicCard;->imageUrlLarge:Ljava/lang/String;

    iput-object p7, p0, Lcom/twitter/library/api/TweetClassicCard;->imageUrlSmall:Ljava/lang/String;

    iput p8, p0, Lcom/twitter/library/api/TweetClassicCard;->imageWidth:I

    iput p9, p0, Lcom/twitter/library/api/TweetClassicCard;->imageHeight:I

    iput p10, p0, Lcom/twitter/library/api/TweetClassicCard;->playerType:I

    iput-object p11, p0, Lcom/twitter/library/api/TweetClassicCard;->playerUrl:Ljava/lang/String;

    iput-object p12, p0, Lcom/twitter/library/api/TweetClassicCard;->playerStreamUrl:Ljava/lang/String;

    iput-object p13, p0, Lcom/twitter/library/api/TweetClassicCard;->promotion:Lcom/twitter/library/api/Promotion;

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->siteUser:Lcom/twitter/library/api/CardUser;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->authorUser:Lcom/twitter/library/api/CardUser;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Lcom/twitter/library/api/Promotion;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/api/TwitterUser;)V
    .locals 18

    if-eqz p14, :cond_0

    new-instance v16, Lcom/twitter/library/api/CardUser;

    move-object/from16 v0, v16

    move-object/from16 v1, p14

    invoke-direct {v0, v1}, Lcom/twitter/library/api/CardUser;-><init>(Lcom/twitter/library/api/TwitterUser;)V

    :goto_0
    if-eqz p15, :cond_1

    new-instance v17, Lcom/twitter/library/api/CardUser;

    move-object/from16 v0, v17

    move-object/from16 v1, p15

    invoke-direct {v0, v1}, Lcom/twitter/library/api/CardUser;-><init>(Lcom/twitter/library/api/TwitterUser;)V

    :goto_1
    move-object/from16 v2, p0

    move/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move/from16 v10, p8

    move/from16 v11, p9

    move/from16 v12, p10

    move-object/from16 v13, p11

    move-object/from16 v14, p12

    move-object/from16 v15, p13

    invoke-direct/range {v2 .. v17}, Lcom/twitter/library/api/TweetClassicCard;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Lcom/twitter/library/api/Promotion;Lcom/twitter/library/api/CardUser;Lcom/twitter/library/api/CardUser;)V

    return-void

    :cond_0
    const/16 v16, 0x0

    goto :goto_0

    :cond_1
    const/16 v17, 0x0

    goto :goto_1
.end method


# virtual methods
.method public a(F)Lcom/twitter/library/api/aa;
    .locals 4

    invoke-static {}, Lcom/twitter/library/util/UrlNetworkStats;->a()Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/TweetClassicCard;->imageUrlLarge:Ljava/lang/String;

    if-eqz v1, :cond_1

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, p1, v1

    if-lez v1, :cond_1

    invoke-virtual {v0}, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->b:Lcom/twitter/library/api/aa;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/library/api/aa;

    iget-object v1, p0, Lcom/twitter/library/api/TweetClassicCard;->imageUrlLarge:Ljava/lang/String;

    iget v2, p0, Lcom/twitter/library/api/TweetClassicCard;->imageWidth:I

    mul-int/lit8 v2, v2, 0x2

    iget v3, p0, Lcom/twitter/library/api/TweetClassicCard;->imageHeight:I

    mul-int/lit8 v3, v3, 0x2

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/api/aa;-><init>(Ljava/lang/String;II)V

    iput-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->b:Lcom/twitter/library/api/aa;

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->b:Lcom/twitter/library/api/aa;

    :goto_0
    return-object v0

    :cond_1
    iget v1, p0, Lcom/twitter/library/api/TweetClassicCard;->type:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    invoke-virtual {v0}, Lcom/twitter/library/util/UrlNetworkStats$NetworkQuality;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->imageUrlSmall:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->c:Lcom/twitter/library/api/aa;

    if-nez v0, :cond_2

    new-instance v0, Lcom/twitter/library/api/aa;

    iget-object v1, p0, Lcom/twitter/library/api/TweetClassicCard;->imageUrlSmall:Ljava/lang/String;

    iget v2, p0, Lcom/twitter/library/api/TweetClassicCard;->imageWidth:I

    iget v3, p0, Lcom/twitter/library/api/TweetClassicCard;->imageHeight:I

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/api/aa;-><init>(Ljava/lang/String;II)V

    iput-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->c:Lcom/twitter/library/api/aa;

    :cond_2
    iget-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->c:Lcom/twitter/library/api/aa;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->a:Lcom/twitter/library/api/aa;

    if-nez v0, :cond_4

    new-instance v0, Lcom/twitter/library/api/aa;

    iget-object v1, p0, Lcom/twitter/library/api/TweetClassicCard;->imageUrl:Ljava/lang/String;

    iget v2, p0, Lcom/twitter/library/api/TweetClassicCard;->imageWidth:I

    iget v3, p0, Lcom/twitter/library/api/TweetClassicCard;->imageHeight:I

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/api/aa;-><init>(Ljava/lang/String;II)V

    iput-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->a:Lcom/twitter/library/api/aa;

    :cond_4
    iget-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->a:Lcom/twitter/library/api/aa;

    goto :goto_0
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/TweetClassicCard;->type:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->title:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->description:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->url:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->imageUrl:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->imageUrlLarge:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/TweetClassicCard;->imageWidth:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/TweetClassicCard;->imageHeight:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/TweetClassicCard;->playerType:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->playerUrl:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->playerStreamUrl:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/twitter/library/api/CardUser;

    invoke-direct {v0}, Lcom/twitter/library/api/CardUser;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->siteUser:Lcom/twitter/library/api/CardUser;

    iget-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->siteUser:Lcom/twitter/library/api/CardUser;

    invoke-virtual {v0, p1}, Lcom/twitter/library/api/CardUser;->readExternal(Ljava/io/ObjectInput;)V

    :cond_0
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/twitter/library/api/CardUser;

    invoke-direct {v0}, Lcom/twitter/library/api/CardUser;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->authorUser:Lcom/twitter/library/api/CardUser;

    iget-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->authorUser:Lcom/twitter/library/api/CardUser;

    invoke-virtual {v0, p1}, Lcom/twitter/library/api/CardUser;->readExternal(Ljava/io/ObjectInput;)V

    :cond_1
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/twitter/library/api/Promotion;

    invoke-direct {v0}, Lcom/twitter/library/api/Promotion;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->promotion:Lcom/twitter/library/api/Promotion;

    iget-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->promotion:Lcom/twitter/library/api/Promotion;

    invoke-virtual {v0, p1}, Lcom/twitter/library/api/Promotion;->readExternal(Ljava/io/ObjectInput;)V

    :cond_2
    :try_start_0
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->features:Ljava/util/HashMap;

    :cond_3
    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->imageUrlSmall:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lcom/twitter/library/api/TweetClassicCard;->type:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->title:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->description:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->url:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->imageUrl:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->imageUrlLarge:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget v0, p0, Lcom/twitter/library/api/TweetClassicCard;->imageWidth:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/api/TweetClassicCard;->imageHeight:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/api/TweetClassicCard;->playerType:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->playerUrl:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->playerStreamUrl:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->siteUser:Lcom/twitter/library/api/CardUser;

    if-eqz v0, :cond_4

    move v0, v1

    :goto_0
    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->siteUser:Lcom/twitter/library/api/CardUser;

    invoke-virtual {v0, p1}, Lcom/twitter/library/api/CardUser;->writeExternal(Ljava/io/ObjectOutput;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->authorUser:Lcom/twitter/library/api/CardUser;

    if-eqz v0, :cond_5

    move v0, v1

    :goto_1
    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->authorUser:Lcom/twitter/library/api/CardUser;

    invoke-virtual {v0, p1}, Lcom/twitter/library/api/CardUser;->writeExternal(Ljava/io/ObjectOutput;)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->promotion:Lcom/twitter/library/api/Promotion;

    if-eqz v0, :cond_6

    move v0, v1

    :goto_2
    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->promotion:Lcom/twitter/library/api/Promotion;

    invoke-virtual {v0, p1}, Lcom/twitter/library/api/Promotion;->writeExternal(Ljava/io/ObjectOutput;)V

    :cond_2
    iget-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->features:Ljava/util/HashMap;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->features:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_7

    :goto_3
    invoke-interface {p1, v1}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->features:Ljava/util/HashMap;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    :cond_3
    iget-object v0, p0, Lcom/twitter/library/api/TweetClassicCard;->imageUrlSmall:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    return-void

    :cond_4
    move v0, v2

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_2

    :cond_7
    move v1, v2

    goto :goto_3
.end method
