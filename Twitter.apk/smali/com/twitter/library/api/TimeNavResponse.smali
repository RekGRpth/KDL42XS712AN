.class public Lcom/twitter/library/api/TimeNavResponse;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public a:[Lcom/twitter/library/api/TimeNavResponse$QueryPeak;

.field public b:[Lcom/twitter/library/api/TimeNavResponse$QueryRange;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/library/api/u;

    invoke-direct {v0}, Lcom/twitter/library/api/u;-><init>()V

    sput-object v0, Lcom/twitter/library/api/TimeNavResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/twitter/library/api/TimeNavResponse$QueryPeak;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/api/TimeNavResponse$QueryPeak;

    iput-object v0, p0, Lcom/twitter/library/api/TimeNavResponse;->a:[Lcom/twitter/library/api/TimeNavResponse$QueryPeak;

    sget-object v0, Lcom/twitter/library/api/TimeNavResponse$QueryRange;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/api/TimeNavResponse$QueryRange;

    iput-object v0, p0, Lcom/twitter/library/api/TimeNavResponse;->b:[Lcom/twitter/library/api/TimeNavResponse$QueryRange;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/twitter/library/api/TimeNavResponse;

    iget-object v2, p0, Lcom/twitter/library/api/TimeNavResponse;->a:[Lcom/twitter/library/api/TimeNavResponse$QueryPeak;

    iget-object v3, p1, Lcom/twitter/library/api/TimeNavResponse;->a:[Lcom/twitter/library/api/TimeNavResponse$QueryPeak;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/twitter/library/api/TimeNavResponse;->b:[Lcom/twitter/library/api/TimeNavResponse$QueryRange;

    iget-object v3, p1, Lcom/twitter/library/api/TimeNavResponse;->b:[Lcom/twitter/library/api/TimeNavResponse$QueryRange;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/api/TimeNavResponse;->a:[Lcom/twitter/library/api/TimeNavResponse$QueryPeak;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/api/TimeNavResponse;->a:[Lcom/twitter/library/api/TimeNavResponse$QueryPeak;

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/library/api/TimeNavResponse;->b:[Lcom/twitter/library/api/TimeNavResponse$QueryRange;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/library/api/TimeNavResponse;->b:[Lcom/twitter/library/api/TimeNavResponse$QueryRange;

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/TimeNavResponse;->a:[Lcom/twitter/library/api/TimeNavResponse$QueryPeak;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/twitter/library/api/TimeNavResponse;->b:[Lcom/twitter/library/api/TimeNavResponse$QueryRange;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    return-void
.end method
