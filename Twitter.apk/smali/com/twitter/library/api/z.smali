.class public Lcom/twitter/library/api/z;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:I

.field private d:J

.field private e:Lcom/twitter/library/api/TwitterStatus;

.field private f:Lcom/twitter/library/api/Conversation;

.field private g:Ljava/util/List;

.field private h:Lcom/twitter/library/api/TwitterTopic;

.field private i:Ljava/util/List;

.field private j:J

.field private k:Lcom/twitter/library/api/TimelineScribeContent;

.field private l:Lcom/twitter/library/api/TweetPivotOptions;

.field private m:Ljava/lang/String;

.field private n:Lcom/twitter/library/api/TwitterSocialProof;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/library/api/z;->b:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/library/api/z;->c:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/library/api/z;->j:J

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/api/z;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/z;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/library/api/z;)Lcom/twitter/library/api/TwitterStatus;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/z;->e:Lcom/twitter/library/api/TwitterStatus;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/library/api/z;)Lcom/twitter/library/api/Conversation;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/z;->f:Lcom/twitter/library/api/Conversation;

    return-object v0
.end method

.method private c()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/api/z;->l:Lcom/twitter/library/api/TweetPivotOptions;

    iget-object v1, p0, Lcom/twitter/library/api/z;->h:Lcom/twitter/library/api/TwitterTopic;

    iget-object v1, v1, Lcom/twitter/library/api/TwitterTopic;->b:Lcom/twitter/library/api/TwitterTopic$Metadata;

    iget-object v1, v1, Lcom/twitter/library/api/TwitterTopic$Metadata;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/library/api/TweetPivotOptions;->pivotEventId:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/library/api/z;->l:Lcom/twitter/library/api/TweetPivotOptions;

    iget-object v1, p0, Lcom/twitter/library/api/z;->h:Lcom/twitter/library/api/TwitterTopic;

    iget-object v1, v1, Lcom/twitter/library/api/TwitterTopic;->f:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/library/api/TweetPivotOptions;->pivotHashtag:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/library/api/z;->l:Lcom/twitter/library/api/TweetPivotOptions;

    iget-object v1, p0, Lcom/twitter/library/api/z;->h:Lcom/twitter/library/api/TwitterTopic;

    iget-object v1, v1, Lcom/twitter/library/api/TwitterTopic;->e:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/library/api/TweetPivotOptions;->pivotQuery:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/library/api/z;->l:Lcom/twitter/library/api/TweetPivotOptions;

    iget-object v1, p0, Lcom/twitter/library/api/z;->h:Lcom/twitter/library/api/TwitterTopic;

    iget-object v1, v1, Lcom/twitter/library/api/TwitterTopic;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/library/api/TweetPivotOptions;->pivotTitle:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/library/api/z;->l:Lcom/twitter/library/api/TweetPivotOptions;

    iget-object v1, p0, Lcom/twitter/library/api/z;->h:Lcom/twitter/library/api/TwitterTopic;

    iget-object v1, v1, Lcom/twitter/library/api/TwitterTopic;->b:Lcom/twitter/library/api/TwitterTopic$Metadata;

    iget v1, v1, Lcom/twitter/library/api/TwitterTopic$Metadata;->type:I

    iput v1, v0, Lcom/twitter/library/api/TweetPivotOptions;->pivotEventType:I

    return-void
.end method

.method static synthetic d(Lcom/twitter/library/api/z;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/z;->g:Ljava/util/List;

    return-object v0
.end method

.method private d()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/twitter/library/api/z;->a:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/twitter/library/api/z;->e:Lcom/twitter/library/api/TwitterStatus;

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/twitter/library/api/z;->f:Lcom/twitter/library/api/Conversation;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/api/z;->h:Lcom/twitter/library/api/TwitterTopic;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/api/z;->g:Ljava/util/List;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/library/api/z;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    iget-object v1, p0, Lcom/twitter/library/api/z;->i:Ljava/util/List;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/twitter/library/api/z;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Lcom/twitter/library/api/z;)Lcom/twitter/library/api/TweetPivotOptions;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/z;->l:Lcom/twitter/library/api/TweetPivotOptions;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/library/api/z;)Lcom/twitter/library/api/TwitterTopic;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/z;->h:Lcom/twitter/library/api/TwitterTopic;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/library/api/z;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/library/api/z;->c()V

    return-void
.end method

.method static synthetic h(Lcom/twitter/library/api/z;)I
    .locals 1

    iget v0, p0, Lcom/twitter/library/api/z;->b:I

    return v0
.end method

.method static synthetic i(Lcom/twitter/library/api/z;)I
    .locals 1

    iget v0, p0, Lcom/twitter/library/api/z;->c:I

    return v0
.end method

.method static synthetic j(Lcom/twitter/library/api/z;)J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/api/z;->d:J

    return-wide v0
.end method

.method static synthetic k(Lcom/twitter/library/api/z;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/z;->i:Ljava/util/List;

    return-object v0
.end method

.method static synthetic l(Lcom/twitter/library/api/z;)J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/api/z;->j:J

    return-wide v0
.end method

.method static synthetic m(Lcom/twitter/library/api/z;)Lcom/twitter/library/api/TimelineScribeContent;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/z;->k:Lcom/twitter/library/api/TimelineScribeContent;

    return-object v0
.end method

.method static synthetic n(Lcom/twitter/library/api/z;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/z;->m:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic o(Lcom/twitter/library/api/z;)Lcom/twitter/library/api/TwitterSocialProof;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/z;->n:Lcom/twitter/library/api/TwitterSocialProof;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/library/api/TwitterStatus;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/z;->e:Lcom/twitter/library/api/TwitterStatus;

    return-object v0
.end method

.method public a(I)Lcom/twitter/library/api/z;
    .locals 0

    iput p1, p0, Lcom/twitter/library/api/z;->b:I

    return-object p0
.end method

.method public a(J)Lcom/twitter/library/api/z;
    .locals 0

    iput-wide p1, p0, Lcom/twitter/library/api/z;->j:J

    return-object p0
.end method

.method public a(Lcom/twitter/library/api/Conversation;)Lcom/twitter/library/api/z;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/z;->f:Lcom/twitter/library/api/Conversation;

    return-object p0
.end method

.method public a(Lcom/twitter/library/api/TimelineScribeContent;)Lcom/twitter/library/api/z;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/z;->k:Lcom/twitter/library/api/TimelineScribeContent;

    return-object p0
.end method

.method public a(Lcom/twitter/library/api/TweetPivotOptions;)Lcom/twitter/library/api/z;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/z;->l:Lcom/twitter/library/api/TweetPivotOptions;

    return-object p0
.end method

.method public a(Lcom/twitter/library/api/TwitterSocialProof;)Lcom/twitter/library/api/z;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/z;->n:Lcom/twitter/library/api/TwitterSocialProof;

    return-object p0
.end method

.method public a(Lcom/twitter/library/api/TwitterStatus;)Lcom/twitter/library/api/z;
    .locals 1

    iput-object p1, p0, Lcom/twitter/library/api/z;->e:Lcom/twitter/library/api/TwitterStatus;

    invoke-virtual {p1}, Lcom/twitter/library/api/TwitterStatus;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/library/api/z;->c:I

    :cond_0
    return-object p0
.end method

.method public a(Lcom/twitter/library/api/TwitterTopic;)Lcom/twitter/library/api/z;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/z;->h:Lcom/twitter/library/api/TwitterTopic;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/library/api/z;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/z;->a:Ljava/lang/String;

    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/twitter/library/api/z;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/z;->g:Ljava/util/List;

    return-object p0
.end method

.method public b()Lcom/twitter/library/api/x;
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/twitter/library/api/z;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/twitter/library/api/x;

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/api/x;-><init>(Lcom/twitter/library/api/z;Lcom/twitter/library/api/y;)V

    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public b(I)Lcom/twitter/library/api/z;
    .locals 0

    iput p1, p0, Lcom/twitter/library/api/z;->c:I

    return-object p0
.end method

.method public b(J)Lcom/twitter/library/api/z;
    .locals 0

    iput-wide p1, p0, Lcom/twitter/library/api/z;->d:J

    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/library/api/z;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/z;->m:Ljava/lang/String;

    return-object p0
.end method

.method public b(Ljava/util/List;)Lcom/twitter/library/api/z;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/z;->i:Ljava/util/List;

    return-object p0
.end method
