.class public Lcom/twitter/library/api/TimeNavResponse$QueryPeak;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Comparable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:F

.field public final b:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/library/api/v;

    invoke-direct {v0}, Lcom/twitter/library/api/v;-><init>()V

    sput-object v0, Lcom/twitter/library/api/TimeNavResponse$QueryPeak;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/TimeNavResponse$QueryPeak;->a:F

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/api/TimeNavResponse$QueryPeak;->b:J

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/api/TimeNavResponse$QueryPeak;)I
    .locals 4

    iget-wide v0, p0, Lcom/twitter/library/api/TimeNavResponse$QueryPeak;->b:J

    iget-wide v2, p1, Lcom/twitter/library/api/TimeNavResponse$QueryPeak;->b:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/twitter/library/api/TimeNavResponse$QueryPeak;->b:J

    iget-wide v2, p1, Lcom/twitter/library/api/TimeNavResponse$QueryPeak;->b:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/twitter/library/api/TimeNavResponse$QueryPeak;

    invoke-virtual {p0, p1}, Lcom/twitter/library/api/TimeNavResponse$QueryPeak;->a(Lcom/twitter/library/api/TimeNavResponse$QueryPeak;)I

    move-result v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/twitter/library/api/TimeNavResponse$QueryPeak;

    iget v2, p1, Lcom/twitter/library/api/TimeNavResponse$QueryPeak;->a:F

    iget v3, p0, Lcom/twitter/library/api/TimeNavResponse$QueryPeak;->a:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-wide v2, p0, Lcom/twitter/library/api/TimeNavResponse$QueryPeak;->b:J

    iget-wide v4, p1, Lcom/twitter/library/api/TimeNavResponse$QueryPeak;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    iget v0, p0, Lcom/twitter/library/api/TimeNavResponse$QueryPeak;->a:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/library/api/TimeNavResponse$QueryPeak;->a:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/twitter/library/api/TimeNavResponse$QueryPeak;->b:J

    iget-wide v3, p0, Lcom/twitter/library/api/TimeNavResponse$QueryPeak;->b:J

    const/16 v5, 0x20

    ushr-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    iget v0, p0, Lcom/twitter/library/api/TimeNavResponse$QueryPeak;->a:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    iget-wide v0, p0, Lcom/twitter/library/api/TimeNavResponse$QueryPeak;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    return-void
.end method
