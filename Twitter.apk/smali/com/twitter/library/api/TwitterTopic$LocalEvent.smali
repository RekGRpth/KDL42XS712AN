.class public Lcom/twitter/library/api/TwitterTopic$LocalEvent;
.super Lcom/twitter/library/api/TwitterTopic$Data;
.source "Twttr"


# static fields
.field private static final serialVersionUID:J = 0x5fc9b8837e0a9df9L


# instance fields
.field public distance:D

.field public latitude:D

.field public longitude:D


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/library/api/TwitterTopic$Data;-><init>()V

    return-void
.end method

.method constructor <init>(DDD)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/library/api/TwitterTopic$Data;-><init>()V

    iput-wide p1, p0, Lcom/twitter/library/api/TwitterTopic$LocalEvent;->latitude:D

    iput-wide p3, p0, Lcom/twitter/library/api/TwitterTopic$LocalEvent;->longitude:D

    iput-wide p5, p0, Lcom/twitter/library/api/TwitterTopic$LocalEvent;->distance:D

    return-void
.end method


# virtual methods
.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 2

    invoke-interface {p1}, Ljava/io/ObjectInput;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/api/TwitterTopic$LocalEvent;->latitude:D

    invoke-interface {p1}, Ljava/io/ObjectInput;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/api/TwitterTopic$LocalEvent;->longitude:D

    invoke-interface {p1}, Ljava/io/ObjectInput;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/api/TwitterTopic$LocalEvent;->distance:D

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/api/TwitterTopic$LocalEvent;->latitude:D

    invoke-interface {p1, v0, v1}, Ljava/io/ObjectOutput;->writeDouble(D)V

    iget-wide v0, p0, Lcom/twitter/library/api/TwitterTopic$LocalEvent;->longitude:D

    invoke-interface {p1, v0, v1}, Ljava/io/ObjectOutput;->writeDouble(D)V

    iget-wide v0, p0, Lcom/twitter/library/api/TwitterTopic$LocalEvent;->distance:D

    invoke-interface {p1, v0, v1}, Ljava/io/ObjectOutput;->writeDouble(D)V

    return-void
.end method
