.class public Lcom/twitter/library/api/TwitterUser;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/twitter/library/api/k;
.implements Ljava/io/Externalizable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final serialVersionUID:J = 0x103946fd9a02a77fL


# instance fields
.field public createdAt:J

.field public descriptionEntities:Lcom/twitter/library/api/TweetEntities;

.field public favoritesCount:I

.field public followersCount:I

.field public friendsCount:I

.field public friendship:I

.field public friendshipTime:J

.field public hasCollections:Z

.field public isGeoEnabled:Z

.field public isLifelineInstitution:Z

.field public isProtected:Z

.field public isTranslator:Z

.field public lastUpdated:J

.field public location:Ljava/lang/String;

.field public metadata:Lcom/twitter/library/api/TwitterUserMetadata;

.field public name:Ljava/lang/String;

.field public needsPhoneVerification:Z

.field public profileBgColor:I

.field public profileDescription:Ljava/lang/String;

.field public profileHeaderImageUrl:Ljava/lang/String;

.field public profileHeaderPath:Ljava/lang/String;

.field public profileImagePath:Ljava/lang/String;

.field public profileImageUrl:Ljava/lang/String;

.field public profileImageUrlChanged:Z

.field public profileUrl:Ljava/lang/String;

.field public promotedContent:Lcom/twitter/library/api/PromotedContent;

.field public status:Lcom/twitter/library/api/TwitterStatus;

.field public statusesCount:I

.field public suspended:Z

.field public urlEntities:Lcom/twitter/library/api/TweetEntities;

.field public userId:J

.field public username:Ljava/lang/String;

.field public verified:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/library/api/aw;

    invoke-direct {v0}, Lcom/twitter/library/api/aw;-><init>()V

    sput-object v0, Lcom/twitter/library/api/TwitterUser;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IZZZZZLjava/lang/String;IJIZIJLcom/twitter/library/api/TwitterStatus;JILcom/twitter/library/api/PromotedContent;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/TwitterUserMetadata;ZZ)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/twitter/library/api/TwitterUser;->userId:J

    iput-object p3, p0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    iput-object p4, p0, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    iput-object p5, p0, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    if-eqz p5, :cond_0

    invoke-static {p5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/library/api/TwitterUser;->profileImagePath:Ljava/lang/String;

    :cond_0
    iput-object p6, p0, Lcom/twitter/library/api/TwitterUser;->profileHeaderImageUrl:Ljava/lang/String;

    if-eqz p6, :cond_1

    invoke-static {p6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/library/api/TwitterUser;->profileHeaderPath:Ljava/lang/String;

    :cond_1
    if-eqz p7, :cond_2

    const-string/jumbo v2, "null"

    invoke-virtual {p7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/twitter/library/api/TwitterUser;->profileDescription:Ljava/lang/String;

    :goto_0
    iput p8, p0, Lcom/twitter/library/api/TwitterUser;->followersCount:I

    if-eqz p9, :cond_3

    const-string/jumbo v2, "null"

    invoke-virtual {p9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/twitter/library/api/TwitterUser;->profileUrl:Ljava/lang/String;

    :goto_1
    iput p10, p0, Lcom/twitter/library/api/TwitterUser;->profileBgColor:I

    iput-boolean p11, p0, Lcom/twitter/library/api/TwitterUser;->suspended:Z

    iput-boolean p12, p0, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    move/from16 v0, p13

    iput-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->verified:Z

    move/from16 v0, p14

    iput-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->isTranslator:Z

    move/from16 v0, p15

    iput-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->isLifelineInstitution:Z

    if-eqz p16, :cond_4

    const-string/jumbo v2, "null"

    move-object/from16 v0, p16

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/twitter/library/api/TwitterUser;->location:Ljava/lang/String;

    :goto_2
    move/from16 v0, p17

    iput v0, p0, Lcom/twitter/library/api/TwitterUser;->friendsCount:I

    move-wide/from16 v0, p18

    iput-wide v0, p0, Lcom/twitter/library/api/TwitterUser;->createdAt:J

    move/from16 v0, p20

    iput v0, p0, Lcom/twitter/library/api/TwitterUser;->statusesCount:I

    move/from16 v0, p21

    iput-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->isGeoEnabled:Z

    move/from16 v0, p22

    iput v0, p0, Lcom/twitter/library/api/TwitterUser;->friendship:I

    move-wide/from16 v0, p23

    iput-wide v0, p0, Lcom/twitter/library/api/TwitterUser;->friendshipTime:J

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->status:Lcom/twitter/library/api/TwitterStatus;

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/twitter/library/api/TwitterUser;->profileImageUrlChanged:Z

    move-wide/from16 v0, p26

    iput-wide v0, p0, Lcom/twitter/library/api/TwitterUser;->lastUpdated:J

    move/from16 v0, p28

    iput v0, p0, Lcom/twitter/library/api/TwitterUser;->favoritesCount:I

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->promotedContent:Lcom/twitter/library/api/PromotedContent;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->descriptionEntities:Lcom/twitter/library/api/TweetEntities;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->urlEntities:Lcom/twitter/library/api/TweetEntities;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->metadata:Lcom/twitter/library/api/TwitterUserMetadata;

    move/from16 v0, p33

    iput-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->needsPhoneVerification:Z

    move/from16 v0, p34

    iput-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->hasCollections:Z

    return-void

    :cond_2
    iput-object p7, p0, Lcom/twitter/library/api/TwitterUser;->profileDescription:Ljava/lang/String;

    goto :goto_0

    :cond_3
    iput-object p9, p0, Lcom/twitter/library/api/TwitterUser;->profileUrl:Ljava/lang/String;

    goto :goto_1

    :cond_4
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->location:Ljava/lang/String;

    goto :goto_2
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileHeaderImageUrl:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileDescription:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/TwitterUser;->followersCount:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileUrl:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/TwitterUser;->profileBgColor:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->suspended:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->verified:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->location:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/TwitterUser;->friendsCount:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/twitter/library/api/TwitterUser;->createdAt:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/TwitterUser;->statusesCount:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->isGeoEnabled:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/TwitterUser;->friendship:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/twitter/library/api/TwitterUser;->friendshipTime:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/twitter/library/api/TwitterUser;->lastUpdated:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/TwitterUser;->favoritesCount:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/PromotedContent;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->promotedContent:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TweetEntities;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->descriptionEntities:Lcom/twitter/library/api/TweetEntities;

    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TweetEntities;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->urlEntities:Lcom/twitter/library/api/TweetEntities;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUserMetadata;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->metadata:Lcom/twitter/library/api/TwitterUserMetadata;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileImagePath:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileHeaderPath:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->isLifelineInstitution:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->isTranslator:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->needsPhoneVerification:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_7

    :goto_7
    iput-boolean v1, p0, Lcom/twitter/library/api/TwitterUser;->hasCollections:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->status:Lcom/twitter/library/api/TwitterStatus;

    iput-boolean v2, p0, Lcom/twitter/library/api/TwitterUser;->profileImageUrlChanged:Z

    return-void

    :cond_0
    move v0, v2

    goto/16 :goto_0

    :cond_1
    move v0, v2

    goto/16 :goto_1

    :cond_2
    move v0, v2

    goto/16 :goto_2

    :cond_3
    move v0, v2

    goto/16 :goto_3

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    move v0, v2

    goto :goto_5

    :cond_6
    move v0, v2

    goto :goto_6

    :cond_7
    move v1, v2

    goto :goto_7
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/api/TwitterUser;->userId:J

    return-wide v0
.end method

.method public a(Lcom/twitter/library/api/TwitterUser;)Z
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileImagePath:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/library/api/TwitterUser;->profileImagePath:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/internal/util/j;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/twitter/library/api/TwitterUser;)Z
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileHeaderPath:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/library/api/TwitterUser;->profileHeaderPath:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/internal/util/j;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    goto :goto_0
.end method

.method public d()Z
    .locals 4

    iget-wide v0, p0, Lcom/twitter/library/api/TwitterUser;->createdAt:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/twitter/library/api/TwitterUser;

    iget-wide v2, p0, Lcom/twitter/library/api/TwitterUser;->createdAt:J

    iget-wide v4, p1, Lcom/twitter/library/api/TwitterUser;->createdAt:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget v2, p0, Lcom/twitter/library/api/TwitterUser;->favoritesCount:I

    iget v3, p1, Lcom/twitter/library/api/TwitterUser;->favoritesCount:I

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget v2, p0, Lcom/twitter/library/api/TwitterUser;->followersCount:I

    iget v3, p1, Lcom/twitter/library/api/TwitterUser;->followersCount:I

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget v2, p0, Lcom/twitter/library/api/TwitterUser;->friendsCount:I

    iget v3, p1, Lcom/twitter/library/api/TwitterUser;->friendsCount:I

    if-eq v2, v3, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget v2, p0, Lcom/twitter/library/api/TwitterUser;->friendship:I

    iget v3, p1, Lcom/twitter/library/api/TwitterUser;->friendship:I

    if-eq v2, v3, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-wide v2, p0, Lcom/twitter/library/api/TwitterUser;->friendshipTime:J

    iget-wide v4, p1, Lcom/twitter/library/api/TwitterUser;->friendshipTime:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    iget-boolean v2, p0, Lcom/twitter/library/api/TwitterUser;->isGeoEnabled:Z

    iget-boolean v3, p1, Lcom/twitter/library/api/TwitterUser;->isGeoEnabled:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    goto :goto_0

    :cond_a
    iget-boolean v2, p0, Lcom/twitter/library/api/TwitterUser;->suspended:Z

    iget-boolean v3, p1, Lcom/twitter/library/api/TwitterUser;->suspended:Z

    if-eq v2, v3, :cond_b

    move v0, v1

    goto :goto_0

    :cond_b
    iget-boolean v2, p0, Lcom/twitter/library/api/TwitterUser;->needsPhoneVerification:Z

    iget-boolean v3, p1, Lcom/twitter/library/api/TwitterUser;->needsPhoneVerification:Z

    if-eq v2, v3, :cond_c

    move v0, v1

    goto :goto_0

    :cond_c
    iget-boolean v2, p0, Lcom/twitter/library/api/TwitterUser;->hasCollections:Z

    iget-boolean v3, p1, Lcom/twitter/library/api/TwitterUser;->hasCollections:Z

    if-eq v2, v3, :cond_d

    move v0, v1

    goto :goto_0

    :cond_d
    iget-boolean v2, p0, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    iget-boolean v3, p1, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    if-eq v2, v3, :cond_e

    move v0, v1

    goto :goto_0

    :cond_e
    iget-boolean v2, p0, Lcom/twitter/library/api/TwitterUser;->profileImageUrlChanged:Z

    iget-boolean v3, p1, Lcom/twitter/library/api/TwitterUser;->profileImageUrlChanged:Z

    if-eq v2, v3, :cond_f

    move v0, v1

    goto :goto_0

    :cond_f
    iget v2, p0, Lcom/twitter/library/api/TwitterUser;->statusesCount:I

    iget v3, p1, Lcom/twitter/library/api/TwitterUser;->statusesCount:I

    if-eq v2, v3, :cond_10

    move v0, v1

    goto :goto_0

    :cond_10
    iget-wide v2, p0, Lcom/twitter/library/api/TwitterUser;->userId:J

    iget-wide v4, p1, Lcom/twitter/library/api/TwitterUser;->userId:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_11

    move v0, v1

    goto/16 :goto_0

    :cond_11
    iget-boolean v2, p0, Lcom/twitter/library/api/TwitterUser;->verified:Z

    iget-boolean v3, p1, Lcom/twitter/library/api/TwitterUser;->verified:Z

    if-eq v2, v3, :cond_12

    move v0, v1

    goto/16 :goto_0

    :cond_12
    iget-boolean v2, p0, Lcom/twitter/library/api/TwitterUser;->isTranslator:Z

    iget-boolean v3, p1, Lcom/twitter/library/api/TwitterUser;->isTranslator:Z

    if-eq v2, v3, :cond_13

    move v0, v1

    goto/16 :goto_0

    :cond_13
    iget-object v2, p0, Lcom/twitter/library/api/TwitterUser;->location:Ljava/lang/String;

    if-eqz v2, :cond_15

    iget-object v2, p0, Lcom/twitter/library/api/TwitterUser;->location:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/api/TwitterUser;->location:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    :cond_14
    move v0, v1

    goto/16 :goto_0

    :cond_15
    iget-object v2, p1, Lcom/twitter/library/api/TwitterUser;->location:Ljava/lang/String;

    if-nez v2, :cond_14

    :cond_16
    iget-object v2, p0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    if-eqz v2, :cond_18

    iget-object v2, p0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_19

    :cond_17
    move v0, v1

    goto/16 :goto_0

    :cond_18
    iget-object v2, p1, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    if-nez v2, :cond_17

    :cond_19
    iget-object v2, p0, Lcom/twitter/library/api/TwitterUser;->profileDescription:Ljava/lang/String;

    if-eqz v2, :cond_1b

    iget-object v2, p0, Lcom/twitter/library/api/TwitterUser;->profileDescription:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/api/TwitterUser;->profileDescription:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1c

    :cond_1a
    move v0, v1

    goto/16 :goto_0

    :cond_1b
    iget-object v2, p1, Lcom/twitter/library/api/TwitterUser;->profileDescription:Ljava/lang/String;

    if-nez v2, :cond_1a

    :cond_1c
    iget-object v2, p0, Lcom/twitter/library/api/TwitterUser;->profileImagePath:Ljava/lang/String;

    if-eqz v2, :cond_1e

    iget-object v2, p0, Lcom/twitter/library/api/TwitterUser;->profileImagePath:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/api/TwitterUser;->profileImagePath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1f

    :cond_1d
    move v0, v1

    goto/16 :goto_0

    :cond_1e
    iget-object v2, p1, Lcom/twitter/library/api/TwitterUser;->profileImagePath:Ljava/lang/String;

    if-nez v2, :cond_1d

    :cond_1f
    iget-object v2, p0, Lcom/twitter/library/api/TwitterUser;->profileHeaderPath:Ljava/lang/String;

    if-eqz v2, :cond_21

    iget-object v2, p0, Lcom/twitter/library/api/TwitterUser;->profileHeaderPath:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/api/TwitterUser;->profileHeaderPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_22

    :cond_20
    move v0, v1

    goto/16 :goto_0

    :cond_21
    iget-object v2, p1, Lcom/twitter/library/api/TwitterUser;->profileHeaderPath:Ljava/lang/String;

    if-nez v2, :cond_20

    :cond_22
    iget-object v2, p0, Lcom/twitter/library/api/TwitterUser;->profileUrl:Ljava/lang/String;

    if-eqz v2, :cond_24

    iget-object v2, p0, Lcom/twitter/library/api/TwitterUser;->profileUrl:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/api/TwitterUser;->profileUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_25

    :cond_23
    move v0, v1

    goto/16 :goto_0

    :cond_24
    iget-object v2, p1, Lcom/twitter/library/api/TwitterUser;->profileUrl:Ljava/lang/String;

    if-nez v2, :cond_23

    :cond_25
    iget v2, p0, Lcom/twitter/library/api/TwitterUser;->profileBgColor:I

    iget v3, p1, Lcom/twitter/library/api/TwitterUser;->profileBgColor:I

    if-eq v2, v3, :cond_26

    move v0, v1

    goto/16 :goto_0

    :cond_26
    iget-object v2, p0, Lcom/twitter/library/api/TwitterUser;->status:Lcom/twitter/library/api/TwitterStatus;

    if-eqz v2, :cond_28

    iget-object v2, p0, Lcom/twitter/library/api/TwitterUser;->status:Lcom/twitter/library/api/TwitterStatus;

    iget-object v3, p1, Lcom/twitter/library/api/TwitterUser;->status:Lcom/twitter/library/api/TwitterStatus;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_29

    :cond_27
    move v0, v1

    goto/16 :goto_0

    :cond_28
    iget-object v2, p1, Lcom/twitter/library/api/TwitterUser;->status:Lcom/twitter/library/api/TwitterStatus;

    if-nez v2, :cond_27

    :cond_29
    iget-object v2, p0, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    if-eqz v2, :cond_2b

    iget-object v2, p0, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2c

    :cond_2a
    move v0, v1

    goto/16 :goto_0

    :cond_2b
    iget-object v2, p1, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    if-nez v2, :cond_2a

    :cond_2c
    iget-object v2, p0, Lcom/twitter/library/api/TwitterUser;->promotedContent:Lcom/twitter/library/api/PromotedContent;

    if-eqz v2, :cond_2e

    iget-object v2, p0, Lcom/twitter/library/api/TwitterUser;->promotedContent:Lcom/twitter/library/api/PromotedContent;

    iget-object v3, p1, Lcom/twitter/library/api/TwitterUser;->promotedContent:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v2, v3}, Lcom/twitter/library/api/PromotedContent;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2f

    :cond_2d
    move v0, v1

    goto/16 :goto_0

    :cond_2e
    iget-object v2, p1, Lcom/twitter/library/api/TwitterUser;->promotedContent:Lcom/twitter/library/api/PromotedContent;

    if-nez v2, :cond_2d

    :cond_2f
    iget-object v2, p0, Lcom/twitter/library/api/TwitterUser;->descriptionEntities:Lcom/twitter/library/api/TweetEntities;

    if-eqz v2, :cond_31

    iget-object v2, p0, Lcom/twitter/library/api/TwitterUser;->descriptionEntities:Lcom/twitter/library/api/TweetEntities;

    iget-object v3, p1, Lcom/twitter/library/api/TwitterUser;->descriptionEntities:Lcom/twitter/library/api/TweetEntities;

    invoke-virtual {v2, v3}, Lcom/twitter/library/api/TweetEntities;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_32

    :cond_30
    move v0, v1

    goto/16 :goto_0

    :cond_31
    iget-object v2, p1, Lcom/twitter/library/api/TwitterUser;->descriptionEntities:Lcom/twitter/library/api/TweetEntities;

    if-nez v2, :cond_30

    :cond_32
    iget-object v2, p0, Lcom/twitter/library/api/TwitterUser;->urlEntities:Lcom/twitter/library/api/TweetEntities;

    if-eqz v2, :cond_34

    iget-object v2, p0, Lcom/twitter/library/api/TwitterUser;->urlEntities:Lcom/twitter/library/api/TweetEntities;

    iget-object v3, p1, Lcom/twitter/library/api/TwitterUser;->urlEntities:Lcom/twitter/library/api/TweetEntities;

    invoke-virtual {v2, v3}, Lcom/twitter/library/api/TweetEntities;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_35

    :cond_33
    move v0, v1

    goto/16 :goto_0

    :cond_34
    iget-object v2, p1, Lcom/twitter/library/api/TwitterUser;->urlEntities:Lcom/twitter/library/api/TweetEntities;

    if-nez v2, :cond_33

    :cond_35
    iget-object v2, p0, Lcom/twitter/library/api/TwitterUser;->metadata:Lcom/twitter/library/api/TwitterUserMetadata;

    if-eqz v2, :cond_37

    iget-object v2, p0, Lcom/twitter/library/api/TwitterUser;->metadata:Lcom/twitter/library/api/TwitterUserMetadata;

    iget-object v3, p1, Lcom/twitter/library/api/TwitterUser;->metadata:Lcom/twitter/library/api/TwitterUserMetadata;

    invoke-virtual {v2, v3}, Lcom/twitter/library/api/TwitterUserMetadata;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_38

    :cond_36
    move v0, v1

    goto/16 :goto_0

    :cond_37
    iget-object v2, p1, Lcom/twitter/library/api/TwitterUser;->metadata:Lcom/twitter/library/api/TwitterUserMetadata;

    if-nez v2, :cond_36

    :cond_38
    iget-boolean v2, p0, Lcom/twitter/library/api/TwitterUser;->isLifelineInstitution:Z

    iget-boolean v3, p1, Lcom/twitter/library/api/TwitterUser;->isLifelineInstitution:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    goto/16 :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->metadata:Lcom/twitter/library/api/TwitterUserMetadata;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->metadata:Lcom/twitter/library/api/TwitterUserMetadata;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterUserMetadata;->c:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    const/16 v7, 0x20

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-wide v3, p0, Lcom/twitter/library/api/TwitterUser;->userId:J

    iget-wide v5, p0, Lcom/twitter/library/api/TwitterUser;->userId:J

    ushr-long/2addr v5, v7

    xor-long/2addr v3, v5

    long-to-int v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileImagePath:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileImagePath:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileHeaderPath:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileHeaderPath:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileDescription:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileDescription:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileUrl:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/twitter/library/api/TwitterUser;->profileBgColor:I

    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->suspended:Z

    if-eqz v0, :cond_6

    move v0, v2

    :goto_6
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->needsPhoneVerification:Z

    if-eqz v0, :cond_7

    move v0, v2

    :goto_7
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->hasCollections:Z

    if-eqz v0, :cond_8

    move v0, v2

    :goto_8
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    if-eqz v0, :cond_9

    move v0, v2

    :goto_9
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->verified:Z

    if-eqz v0, :cond_a

    move v0, v2

    :goto_a
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->isTranslator:Z

    if-eqz v0, :cond_b

    move v0, v2

    :goto_b
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->location:Ljava/lang/String;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->location:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_c
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/twitter/library/api/TwitterUser;->followersCount:I

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/twitter/library/api/TwitterUser;->friendsCount:I

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v3, p0, Lcom/twitter/library/api/TwitterUser;->createdAt:J

    iget-wide v5, p0, Lcom/twitter/library/api/TwitterUser;->createdAt:J

    ushr-long/2addr v5, v7

    xor-long/2addr v3, v5

    long-to-int v3, v3

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/twitter/library/api/TwitterUser;->statusesCount:I

    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->status:Lcom/twitter/library/api/TwitterStatus;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->status:Lcom/twitter/library/api/TwitterStatus;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_d
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->isGeoEnabled:Z

    if-eqz v0, :cond_e

    move v0, v2

    :goto_e
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/twitter/library/api/TwitterUser;->friendship:I

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/twitter/library/api/TwitterUser;->favoritesCount:I

    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->profileImageUrlChanged:Z

    if-eqz v0, :cond_f

    move v0, v2

    :goto_f
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->promotedContent:Lcom/twitter/library/api/PromotedContent;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->promotedContent:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v0}, Lcom/twitter/library/api/PromotedContent;->hashCode()I

    move-result v0

    :goto_10
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->descriptionEntities:Lcom/twitter/library/api/TweetEntities;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->descriptionEntities:Lcom/twitter/library/api/TweetEntities;

    invoke-virtual {v0}, Lcom/twitter/library/api/TweetEntities;->hashCode()I

    move-result v0

    :goto_11
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->urlEntities:Lcom/twitter/library/api/TweetEntities;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->urlEntities:Lcom/twitter/library/api/TweetEntities;

    invoke-virtual {v0}, Lcom/twitter/library/api/TweetEntities;->hashCode()I

    move-result v0

    :goto_12
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->metadata:Lcom/twitter/library/api/TwitterUserMetadata;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->metadata:Lcom/twitter/library/api/TwitterUserMetadata;

    invoke-virtual {v0}, Lcom/twitter/library/api/TwitterUserMetadata;->hashCode()I

    move-result v0

    :goto_13
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/twitter/library/api/TwitterUser;->isLifelineInstitution:Z

    if-eqz v3, :cond_14

    :goto_14
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/twitter/library/api/TwitterUser;->lastUpdated:J

    long-to-int v1, v1

    add-int/2addr v0, v1

    return v0

    :cond_0
    move v0, v1

    goto/16 :goto_0

    :cond_1
    move v0, v1

    goto/16 :goto_1

    :cond_2
    move v0, v1

    goto/16 :goto_2

    :cond_3
    move v0, v1

    goto/16 :goto_3

    :cond_4
    move v0, v1

    goto/16 :goto_4

    :cond_5
    move v0, v1

    goto/16 :goto_5

    :cond_6
    move v0, v1

    goto/16 :goto_6

    :cond_7
    move v0, v1

    goto/16 :goto_7

    :cond_8
    move v0, v1

    goto/16 :goto_8

    :cond_9
    move v0, v1

    goto/16 :goto_9

    :cond_a
    move v0, v1

    goto/16 :goto_a

    :cond_b
    move v0, v1

    goto/16 :goto_b

    :cond_c
    move v0, v1

    goto/16 :goto_c

    :cond_d
    move v0, v1

    goto/16 :goto_d

    :cond_e
    move v0, v1

    goto :goto_e

    :cond_f
    move v0, v1

    goto :goto_f

    :cond_10
    move v0, v1

    goto :goto_10

    :cond_11
    move v0, v1

    goto :goto_11

    :cond_12
    move v0, v1

    goto :goto_12

    :cond_13
    move v0, v1

    goto :goto_13

    :cond_14
    move v2, v1

    goto :goto_14
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 2

    invoke-interface {p1}, Ljava/io/ObjectInput;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileHeaderImageUrl:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileDescription:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/TwitterUser;->followersCount:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileUrl:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/TwitterUser;->profileBgColor:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->verified:Z

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->location:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/TwitterUser;->friendsCount:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/api/TwitterUser;->createdAt:J

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/TwitterUser;->statusesCount:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->isGeoEnabled:Z

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/TwitterUser;->friendship:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/api/TwitterUser;->friendshipTime:J

    invoke-interface {p1}, Ljava/io/ObjectInput;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/api/TwitterUser;->lastUpdated:J

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/TwitterUser;->favoritesCount:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/PromotedContent;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->promotedContent:Lcom/twitter/library/api/PromotedContent;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TweetEntities;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->descriptionEntities:Lcom/twitter/library/api/TweetEntities;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TweetEntities;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->urlEntities:Lcom/twitter/library/api/TweetEntities;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileImagePath:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileHeaderPath:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->isLifelineInstitution:Z

    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->isTranslator:Z

    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->needsPhoneVerification:Z

    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->hasCollections:Z

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-interface {p1, v0, v1}, Ljava/io/ObjectOutput;->writeLong(J)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileHeaderImageUrl:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileDescription:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget v0, p0, Lcom/twitter/library/api/TwitterUser;->followersCount:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileUrl:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget v0, p0, Lcom/twitter/library/api/TwitterUser;->profileBgColor:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    iget-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->verified:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->location:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget v0, p0, Lcom/twitter/library/api/TwitterUser;->friendsCount:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-wide v0, p0, Lcom/twitter/library/api/TwitterUser;->createdAt:J

    invoke-interface {p1, v0, v1}, Ljava/io/ObjectOutput;->writeLong(J)V

    iget v0, p0, Lcom/twitter/library/api/TwitterUser;->statusesCount:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->isGeoEnabled:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    iget v0, p0, Lcom/twitter/library/api/TwitterUser;->friendship:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-wide v0, p0, Lcom/twitter/library/api/TwitterUser;->friendshipTime:J

    invoke-interface {p1, v0, v1}, Ljava/io/ObjectOutput;->writeLong(J)V

    iget-wide v0, p0, Lcom/twitter/library/api/TwitterUser;->lastUpdated:J

    invoke-interface {p1, v0, v1}, Ljava/io/ObjectOutput;->writeLong(J)V

    iget v0, p0, Lcom/twitter/library/api/TwitterUser;->favoritesCount:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->promotedContent:Lcom/twitter/library/api/PromotedContent;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->descriptionEntities:Lcom/twitter/library/api/TweetEntities;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->urlEntities:Lcom/twitter/library/api/TweetEntities;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileImagePath:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileHeaderPath:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->isLifelineInstitution:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    iget-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->isTranslator:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    iget-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->needsPhoneVerification:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    iget-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->hasCollections:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-wide v3, p0, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileHeaderImageUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileDescription:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/twitter/library/api/TwitterUser;->followersCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/twitter/library/api/TwitterUser;->profileBgColor:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->suspended:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->verified:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->location:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/twitter/library/api/TwitterUser;->friendsCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-wide v3, p0, Lcom/twitter/library/api/TwitterUser;->createdAt:J

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    iget v0, p0, Lcom/twitter/library/api/TwitterUser;->statusesCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->isGeoEnabled:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/api/TwitterUser;->friendship:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-wide v3, p0, Lcom/twitter/library/api/TwitterUser;->friendshipTime:J

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v3, p0, Lcom/twitter/library/api/TwitterUser;->lastUpdated:J

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    iget v0, p0, Lcom/twitter/library/api/TwitterUser;->favoritesCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->promotedContent:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->descriptionEntities:Lcom/twitter/library/api/TweetEntities;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->urlEntities:Lcom/twitter/library/api/TweetEntities;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->metadata:Lcom/twitter/library/api/TwitterUserMetadata;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileImagePath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUser;->profileHeaderPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->isLifelineInstitution:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->isTranslator:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->needsPhoneVerification:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->hasCollections:Z

    if-eqz v0, :cond_7

    :goto_7
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    move v0, v2

    goto/16 :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    move v0, v2

    goto :goto_5

    :cond_6
    move v0, v2

    goto :goto_6

    :cond_7
    move v1, v2

    goto :goto_7
.end method
