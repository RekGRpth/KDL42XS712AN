.class public Lcom/twitter/library/api/TwitterSearchHighlight;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field private static final serialVersionUID:J = -0x38db85a7716395eL


# instance fields
.field public timeSince:J

.field public timeUntil:J

.field public type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;JJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/api/TwitterSearchHighlight;->type:Ljava/lang/String;

    iput-wide p2, p0, Lcom/twitter/library/api/TwitterSearchHighlight;->timeSince:J

    iput-wide p4, p0, Lcom/twitter/library/api/TwitterSearchHighlight;->timeUntil:J

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v0, v3, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/twitter/library/api/TwitterSearchHighlight;

    iget-object v0, p0, Lcom/twitter/library/api/TwitterSearchHighlight;->type:Ljava/lang/String;

    if-nez v0, :cond_6

    iget-object v0, p1, Lcom/twitter/library/api/TwitterSearchHighlight;->type:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    iget-wide v3, p0, Lcom/twitter/library/api/TwitterSearchHighlight;->timeSince:J

    iget-wide v5, p1, Lcom/twitter/library/api/TwitterSearchHighlight;->timeSince:J

    cmp-long v0, v3, v5

    if-nez v0, :cond_4

    iget-wide v3, p0, Lcom/twitter/library/api/TwitterSearchHighlight;->timeUntil:J

    iget-wide v5, p1, Lcom/twitter/library/api/TwitterSearchHighlight;->timeUntil:J

    cmp-long v0, v3, v5

    if-eqz v0, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/twitter/library/api/TwitterSearchHighlight;->type:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/api/TwitterSearchHighlight;->type:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/api/TwitterSearchHighlight;->type:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/TwitterSearchHighlight;->type:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/twitter/library/api/TwitterSearchHighlight;->timeSince:J

    long-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/twitter/library/api/TwitterSearchHighlight;->timeUntil:J

    long-to-int v1, v1

    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 2

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterSearchHighlight;->type:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/api/TwitterSearchHighlight;->timeSince:J

    invoke-interface {p1}, Ljava/io/ObjectInput;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/api/TwitterSearchHighlight;->timeUntil:J

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/api/TwitterSearchHighlight;->type:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-wide v0, p0, Lcom/twitter/library/api/TwitterSearchHighlight;->timeSince:J

    invoke-interface {p1, v0, v1}, Ljava/io/ObjectOutput;->writeLong(J)V

    iget-wide v0, p0, Lcom/twitter/library/api/TwitterSearchHighlight;->timeUntil:J

    invoke-interface {p1, v0, v1}, Ljava/io/ObjectOutput;->writeLong(J)V

    return-void
.end method
