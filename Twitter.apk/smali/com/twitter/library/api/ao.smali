.class public Lcom/twitter/library/api/ao;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/internal/network/i;


# instance fields
.field private final a:I

.field private final b:Lcom/twitter/library/api/TwitterUser;

.field private final c:Z

.field private final d:Z

.field private final e:I

.field private final f:Lcom/twitter/library/util/w;

.field private g:Ljava/lang/Object;


# direct methods
.method private constructor <init>(Lcom/twitter/library/api/TwitterUser;ZZIILcom/twitter/library/util/w;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/api/ao;->b:Lcom/twitter/library/api/TwitterUser;

    iput-boolean p2, p0, Lcom/twitter/library/api/ao;->c:Z

    iput-boolean p3, p0, Lcom/twitter/library/api/ao;->d:Z

    iput p4, p0, Lcom/twitter/library/api/ao;->a:I

    iput p5, p0, Lcom/twitter/library/api/ao;->e:I

    iput-object p6, p0, Lcom/twitter/library/api/ao;->f:Lcom/twitter/library/util/w;

    return-void
.end method

.method public static a(I)Lcom/twitter/library/api/ao;
    .locals 7

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/library/api/ao;

    move v3, v2

    move v4, p0

    move v5, v2

    move-object v6, v1

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/api/ao;-><init>(Lcom/twitter/library/api/TwitterUser;ZZIILcom/twitter/library/util/w;)V

    return-object v0
.end method

.method public static a(ILcom/twitter/library/api/TwitterUser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;
    .locals 7

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/library/api/ao;

    move-object v1, p1

    move v3, v2

    move v4, p0

    move v5, v2

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/api/ao;-><init>(Lcom/twitter/library/api/TwitterUser;ZZIILcom/twitter/library/util/w;)V

    return-object v0
.end method

.method public static a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;
    .locals 7

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/library/api/ao;

    const/4 v1, 0x0

    move v3, v2

    move v4, p0

    move v5, v2

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/api/ao;-><init>(Lcom/twitter/library/api/TwitterUser;ZZIILcom/twitter/library/util/w;)V

    return-object v0
.end method

.method public static a(Lcom/twitter/library/api/TwitterUser;ZZLcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;
    .locals 7

    new-instance v0, Lcom/twitter/library/api/ao;

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/api/ao;-><init>(Lcom/twitter/library/api/TwitterUser;ZZIILcom/twitter/library/util/w;)V

    return-object v0
.end method

.method public static a(ZILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;
    .locals 7

    const/4 v2, 0x0

    if-eqz p0, :cond_0

    const/16 v4, 0x8

    :goto_0
    new-instance v0, Lcom/twitter/library/api/ao;

    const/4 v1, 0x0

    move v3, v2

    move v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/api/ao;-><init>(Lcom/twitter/library/api/TwitterUser;ZZIILcom/twitter/library/util/w;)V

    return-object v0

    :cond_0
    const/16 v4, 0x14

    goto :goto_0
.end method

.method private a(Ljava/io/ByteArrayOutputStream;)Lcom/twitter/library/network/OAuthToken;
    .locals 4

    new-instance v0, Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/library/network/n;->a(Ljava/lang/String;Z)Ljava/util/TreeMap;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/network/OAuthToken;

    const-string/jumbo v0, "oauth_token"

    invoke-virtual {v1, v0}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string/jumbo v3, "oauth_token_secret"

    invoke-virtual {v1, v3}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v2, v0, v1}, Lcom/twitter/library/network/OAuthToken;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    return-object v0
.end method

.method public a(ILjava/io/InputStream;ILjava/lang/String;Ljava/lang/String;)V
    .locals 8

    const/16 v4, 0x12c

    const/4 v3, 0x1

    const/4 v6, 0x0

    const/16 v2, 0xc8

    if-nez p4, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/twitter/library/api/ao;->a:I

    const/16 v1, 0x52

    if-ne v0, v1, :cond_2

    if-lt p1, v2, :cond_1

    if-ge p1, v4, :cond_1

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto :goto_0

    :cond_1
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->P(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "application/json"

    invoke-virtual {p4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    iget v0, p0, Lcom/twitter/library/api/ao;->a:I

    const/16 v1, 0x21

    if-eq v0, v1, :cond_3

    iget v0, p0, Lcom/twitter/library/api/ao;->a:I

    const/16 v1, 0x22

    if-eq v0, v1, :cond_3

    iget v0, p0, Lcom/twitter/library/api/ao;->a:I

    const/16 v1, 0x2d

    if-eq v0, v1, :cond_3

    iget v0, p0, Lcom/twitter/library/api/ao;->a:I

    const/16 v1, 0x24

    if-eq v0, v1, :cond_3

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Reader could not validate. content-type=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "], or encoding=["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    if-ne p1, v2, :cond_4

    iget v0, p0, Lcom/twitter/library/api/ao;->a:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move-object v0, v6

    :goto_1
    invoke-static {v0}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :pswitch_1
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/ao;->f:Lcom/twitter/library/util/w;

    iget-object v2, p0, Lcom/twitter/library/api/ao;->b:Lcom/twitter/library/api/TwitterUser;

    iget-boolean v3, p0, Lcom/twitter/library/api/ao;->c:Z

    iget-boolean v4, p0, Lcom/twitter/library/api/ao;->d:Z

    invoke-static {v0, v1, v2, v3, v4}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;Lcom/twitter/library/api/TwitterUser;ZZ)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto :goto_1

    :pswitch_2
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/ao;->f:Lcom/twitter/library/util/w;

    invoke-static {v0, v3, v1}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterStatus;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto :goto_1

    :pswitch_3
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/TwitterStatus;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto :goto_1

    :pswitch_4
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/ao;->f:Lcom/twitter/library/util/w;

    invoke-static {v0, v1}, Lcom/twitter/library/api/ap;->d(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto :goto_1

    :pswitch_5
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->s(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto :goto_1

    :pswitch_6
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/ao;->f:Lcom/twitter/library/util/w;

    invoke-static {v0, v1}, Lcom/twitter/library/api/ap;->k(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto :goto_1

    :pswitch_7
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/ao;->f:Lcom/twitter/library/util/w;

    invoke-static {v0, v1}, Lcom/twitter/library/api/ap;->l(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto :goto_1

    :pswitch_8
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/ao;->f:Lcom/twitter/library/util/w;

    invoke-static {v0, v1}, Lcom/twitter/library/api/ap;->u(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto :goto_1

    :pswitch_9
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    iget v1, p0, Lcom/twitter/library/api/ao;->e:I

    iget-object v2, p0, Lcom/twitter/library/api/ao;->f:Lcom/twitter/library/util/w;

    invoke-static {v0, v1, v2}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;ILcom/twitter/library/util/w;)Lcom/twitter/library/api/am;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto :goto_1

    :pswitch_a
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->u(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_b
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/UserSettings;->a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/UserSettings;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_c
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->t(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/search/TwitterSearchQuery;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_d
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->w(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/Decider;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_e
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->x(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_f
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->L(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/c;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_10
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->O(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/ActivitySummary;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_11
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->S(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/aj;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_12
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->Q(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_13
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->R(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/search/TwitterTypeAheadGroup;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_14
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/ao;->f:Lcom/twitter/library/util/w;

    invoke-static {v0, v3, v1}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_15
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->m(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/ClientConfiguration;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_16
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->n(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/DeviceOperatorConfiguration;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_17
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->j(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/RateLimit;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_18
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    iget v1, p0, Lcom/twitter/library/api/ao;->e:I

    iget-object v2, p0, Lcom/twitter/library/api/ao;->f:Lcom/twitter/library/util/w;

    invoke-static {v0, v1, v2}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;ILcom/twitter/library/util/w;)Lcom/twitter/library/api/aj;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_19
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/ao;->f:Lcom/twitter/library/util/w;

    invoke-static {v0, v1}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/aj;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_1a
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/ao;->f:Lcom/twitter/library/util/w;

    invoke-static {v0, v1}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_1b
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/ao;->f:Lcom/twitter/library/util/w;

    iget-object v2, p0, Lcom/twitter/library/api/ao;->b:Lcom/twitter/library/api/TwitterUser;

    invoke-static {v0, v1, v2}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;Lcom/twitter/library/api/TwitterUser;)Lcom/twitter/library/api/ah;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_1c
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/ao;->f:Lcom/twitter/library/util/w;

    invoke-static {v0, v1}, Lcom/twitter/library/api/ap;->w(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/ag;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_1d
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/ao;->f:Lcom/twitter/library/util/w;

    invoke-static {v0, v1}, Lcom/twitter/library/api/ap;->x(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterTopic;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_1e
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/ao;->f:Lcom/twitter/library/util/w;

    invoke-static {v0, v1}, Lcom/twitter/library/api/ap;->j(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_1f
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/ao;->f:Lcom/twitter/library/util/w;

    invoke-static {v0, v1}, Lcom/twitter/library/api/ap;->z(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_20
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/ao;->f:Lcom/twitter/library/util/w;

    iget-object v2, p0, Lcom/twitter/library/api/ao;->b:Lcom/twitter/library/api/TwitterUser;

    invoke-static {v0, v1, v2}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;Lcom/twitter/library/api/TwitterUser;)Lcom/twitter/library/api/av;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_21
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/ao;->b:Lcom/twitter/library/api/TwitterUser;

    iget-object v2, p0, Lcom/twitter/library/api/ao;->f:Lcom/twitter/library/util/w;

    invoke-static {v0, v1, v2}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/search/f;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_22
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->r(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_23
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->r(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_24
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/ao;->f:Lcom/twitter/library/util/w;

    invoke-static {v0, v1}, Lcom/twitter/library/api/ap;->m(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_25
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/ao;->f:Lcom/twitter/library/util/w;

    invoke-static {v0, v1}, Lcom/twitter/library/api/ap;->q(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/search/d;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_26
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->N(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_27
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->q(Lcom/fasterxml/jackson/core/JsonParser;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_28
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->h(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_29
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/ao;->f:Lcom/twitter/library/util/w;

    invoke-static {v0, v1}, Lcom/twitter/library/api/ap;->h(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/ak;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_2a
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/ao;->f:Lcom/twitter/library/util/w;

    invoke-static {v0, v1}, Lcom/twitter/library/api/ap;->g(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_2b
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->i(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/network/LoginResponse;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_2c
    new-instance v7, Ljava/io/ByteArrayOutputStream;

    const/16 v0, 0x200

    invoke-direct {v7, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v0, Lcom/twitter/internal/network/e;

    invoke-direct {v0, v7, v6}, Lcom/twitter/internal/network/e;-><init>(Ljava/io/OutputStream;Lcom/twitter/internal/network/p;)V

    move v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/internal/network/e;->a(ILjava/io/InputStream;ILjava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v7}, Lcom/twitter/library/api/ao;->a(Ljava/io/ByteArrayOutputStream;)Lcom/twitter/library/network/OAuthToken;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    move-object v0, v6

    goto/16 :goto_1

    :pswitch_2d
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/ao;->f:Lcom/twitter/library/util/w;

    invoke-static {v0, v1}, Lcom/twitter/library/api/ap;->c(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Landroid/util/Pair;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_2e
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->d(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_2f
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->b(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/account/LvEligibilityResponse;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_30
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->e(Lcom/fasterxml/jackson/core/JsonParser;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_31
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->f(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_32
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->g(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_33
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->U(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/h;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_34
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->V(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_35
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->D(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_36
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->E(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/Prompt;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_37
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/ao;->f:Lcom/twitter/library/util/w;

    iget-object v2, p0, Lcom/twitter/library/api/ao;->b:Lcom/twitter/library/api/TwitterUser;

    invoke-static {v0, v1, v2}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;Lcom/twitter/library/api/TwitterUser;)Lcom/twitter/library/api/av;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_38
    new-instance v7, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v7}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v0, Lcom/twitter/internal/network/e;

    invoke-direct {v0, v7, v6}, Lcom/twitter/internal/network/e;-><init>(Ljava/io/OutputStream;Lcom/twitter/internal/network/p;)V

    move v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/internal/network/e;->a(ILjava/io/InputStream;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->d(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    move-object v0, v6

    goto/16 :goto_1

    :pswitch_39
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->p(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_3a
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->o(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/conversations/v;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_3b
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/ao;->f:Lcom/twitter/library/util/w;

    invoke-static {v0, v1}, Lcom/twitter/library/api/ap;->y(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_3c
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->X(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/featureswitch/FeatureSwitchesConfig;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_3d
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->aa(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_3e
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->ab(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_3f
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/ao;->f:Lcom/twitter/library/util/w;

    invoke-static {v0, v1}, Lcom/twitter/library/api/ap;->e(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterStatus$Translation;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_40
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/ao;->f:Lcom/twitter/library/util/w;

    invoke-static {v0, v1}, Lcom/twitter/library/api/ap;->p(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_41
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->ac(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/network/OneFactorLoginResponse;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_42
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->ad(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_43
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->I(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/conversations/c;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_44
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->J(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/conversations/q;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_45
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/conversations/a;->a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/conversations/a;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :cond_4
    iget v0, p0, Lcom/twitter/library/api/ao;->a:I

    sparse-switch v0, :sswitch_data_0

    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->P(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :sswitch_0
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->T(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/t;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :sswitch_1
    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->C(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :sswitch_2
    if-lt p1, v2, :cond_5

    if-ge p1, v4, :cond_5

    invoke-static {p2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/ap;->h(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    goto/16 :goto_1

    :cond_5
    move-object v0, v6

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_14
        :pswitch_15
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_0
        :pswitch_21
        :pswitch_0
        :pswitch_22
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_33
        :pswitch_3
        :pswitch_e
        :pswitch_13
        :pswitch_34
        :pswitch_36
        :pswitch_2e
        :pswitch_0
        :pswitch_37
        :pswitch_38
        :pswitch_2f
        :pswitch_35
        :pswitch_39
        :pswitch_0
        :pswitch_3b
        :pswitch_32
        :pswitch_3c
        :pswitch_3d
        :pswitch_3e
        :pswitch_28
        :pswitch_3f
        :pswitch_1b
        :pswitch_1e
        :pswitch_40
        :pswitch_1f
        :pswitch_16
        :pswitch_23
        :pswitch_1c
        :pswitch_0
        :pswitch_0
        :pswitch_1d
        :pswitch_20
        :pswitch_41
        :pswitch_42
        :pswitch_43
        :pswitch_43
        :pswitch_43
        :pswitch_44
        :pswitch_45
        :pswitch_43
        :pswitch_3a
        :pswitch_43
        :pswitch_0
        :pswitch_0
        :pswitch_30
        :pswitch_31
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_0
        0x31 -> :sswitch_1
        0x37 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Lcom/twitter/internal/network/k;)V
    .locals 2

    iget v0, p0, Lcom/twitter/library/api/ao;->a:I

    const/16 v1, 0x21

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/library/api/ao;->a:I

    const/16 v1, 0x22

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/library/api/ao;->a:I

    const/16 v1, 0x31

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/ao;->g:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/al;

    iget v0, v0, Lcom/twitter/library/api/al;->a:I

    iput v0, p1, Lcom/twitter/internal/network/k;->i:I

    :cond_0
    return-void
.end method
