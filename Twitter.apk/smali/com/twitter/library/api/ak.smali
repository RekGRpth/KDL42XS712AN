.class public Lcom/twitter/library/api/ak;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/api/k;


# instance fields
.field public final a:J

.field public final b:Ljava/lang/String;

.field public final c:J

.field public final d:Lcom/twitter/library/api/TwitterUser;

.field public final e:Lcom/twitter/library/api/TwitterUser;

.field public final f:Lcom/twitter/library/api/TweetEntities;

.field public final g:Z


# direct methods
.method public constructor <init>(JJLjava/lang/String;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/api/TweetEntities;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/twitter/library/api/ak;->a:J

    iput-object p5, p0, Lcom/twitter/library/api/ak;->b:Ljava/lang/String;

    iput-wide p3, p0, Lcom/twitter/library/api/ak;->c:J

    iput-object p6, p0, Lcom/twitter/library/api/ak;->d:Lcom/twitter/library/api/TwitterUser;

    iput-object p7, p0, Lcom/twitter/library/api/ak;->e:Lcom/twitter/library/api/TwitterUser;

    iput-object p8, p0, Lcom/twitter/library/api/ak;->f:Lcom/twitter/library/api/TweetEntities;

    iput-boolean p9, p0, Lcom/twitter/library/api/ak;->g:Z

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/api/ak;->a:J

    return-wide v0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/api/ak;->a:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
