.class public Lcom/twitter/library/api/b;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/library/api/b;->a:Ljava/lang/String;

    :goto_0
    iput-boolean p2, p0, Lcom/twitter/library/api/b;->b:Z

    return-void

    :cond_0
    iput-object p1, p0, Lcom/twitter/library/api/b;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Lcom/twitter/library/api/b;
    .locals 3

    invoke-static {p0}, Lcom/twitter/library/api/b;->b(Landroid/content/Context;)Laf;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/twitter/library/api/b;

    invoke-virtual {v1}, Laf;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Laf;->b()Z

    move-result v1

    invoke-direct {v0, v2, v1}, Lcom/twitter/library/api/b;-><init>(Ljava/lang/String;Z)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;)Laf;
    .locals 3

    const/4 v0, 0x0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    :try_start_0
    invoke-static {p0}, Lae;->a(Landroid/content/Context;)Laf;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/b;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/api/b;->b:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/library/api/b;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/twitter/library/api/b;

    iget-boolean v2, p0, Lcom/twitter/library/api/b;->b:Z

    iget-boolean v3, p1, Lcom/twitter/library/api/b;->b:Z

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/twitter/library/api/b;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/api/b;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/api/b;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/library/api/b;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
