.class public Lcom/twitter/library/api/i;
.super Lcom/twitter/library/api/account/e;
.source "Twttr"


# instance fields
.field private final d:Lcom/twitter/library/api/ao;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    const-class v0, Lcom/twitter/library/api/i;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/account/e;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    const/16 v0, 0x26

    invoke-static {v0}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/i;->d:Lcom/twitter/library/api/ao;

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/e;Lcom/twitter/library/network/a;)Lcom/twitter/internal/network/HttpOperation;
    .locals 4

    iget-object v0, p0, Lcom/twitter/library/api/i;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "1.1"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "help"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "experiments"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/d;

    iget-object v2, p0, Lcom/twitter/library/api/i;->l:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/twitter/library/api/i;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    iget-wide v2, v0, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/library/api/i;->t()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Z)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/i;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)V
    .locals 4

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/i;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/twitter/library/api/i;->l:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/twitter/library/api/i;->s()Lcom/twitter/library/service/p;

    move-result-object v2

    iget-wide v2, v2, Lcom/twitter/library/service/p;->c:J

    invoke-static {v1, v2, v3, v0}, Lju;->a(Landroid/content/Context;JLjava/util/HashMap;)V

    :cond_0
    invoke-virtual {p2, p1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void
.end method
