.class public Lcom/twitter/library/api/d;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# instance fields
.field private final d:Lcom/twitter/library/network/n;

.field private e:Lcom/twitter/library/api/ao;

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 2

    const-class v0, Lcom/twitter/library/api/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Session cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lcom/twitter/library/network/n;

    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->h()Lcom/twitter/library/network/OAuthToken;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    iput-object v0, p0, Lcom/twitter/library/api/d;->d:Lcom/twitter/library/network/n;

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/e;)Lcom/twitter/internal/network/HttpOperation;
    .locals 8

    const/4 v7, 0x1

    iget-object v0, p0, Lcom/twitter/library/api/d;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "1.1"

    aput-object v3, v1, v2

    const-string/jumbo v2, "account"

    aput-object v2, v1, v7

    const/4 v2, 0x2

    const-string/jumbo v3, "verify_credentials"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/library/api/d;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    iget-wide v1, v1, Lcom/twitter/library/service/p;->c:J

    const/16 v3, 0x50

    new-instance v4, Lcom/twitter/library/util/f;

    iget-object v5, p0, Lcom/twitter/library/api/d;->l:Landroid/content/Context;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v1, v2, v6}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v3, v4}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/library/api/d;->e:Lcom/twitter/library/api/ao;

    new-instance v3, Lcom/twitter/library/network/d;

    iget-object v4, p0, Lcom/twitter/library/api/d;->l:Landroid/content/Context;

    invoke-direct {v3, v4, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v1, v2}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/d;->d:Lcom/twitter/library/network/n;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/twitter/library/network/d;->a(Z)Lcom/twitter/library/network/d;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/d;->e:Lcom/twitter/library/api/ao;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)V
    .locals 4

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/d;->e:Lcom/twitter/library/api/ao;

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/api/d;->f:Z

    :cond_0
    new-instance v0, Lcom/twitter/library/util/z;

    iget-object v1, p0, Lcom/twitter/library/api/d;->l:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/twitter/library/util/z;-><init>(Landroid/content/Context;)V

    iget-boolean v1, p0, Lcom/twitter/library/api/d;->f:Z

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/util/z;->a(ZJ)V

    invoke-virtual {p2, p1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/api/d;->f:Z

    return v0
.end method
