.class public Lcom/twitter/library/api/upload/m;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/api/upload/MediaProcessor;


# instance fields
.field private final a:Landroid/net/Uri;

.field private final b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/twitter/library/api/upload/m;->a:Landroid/net/Uri;

    invoke-static {p1, p2}, Lcom/twitter/library/util/Util;->c(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/upload/m;->b:I

    return-void
.end method


# virtual methods
.method public a()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/upload/m;->a:Landroid/net/Uri;

    return-object v0
.end method

.method public a(Z)V
    .locals 0

    return-void
.end method

.method public b()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/upload/m;->a:Landroid/net/Uri;

    return-object v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/api/upload/m;->b:I

    return v0
.end method

.method public d()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public e()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/upload/m;->a:Landroid/net/Uri;

    return-object v0
.end method
