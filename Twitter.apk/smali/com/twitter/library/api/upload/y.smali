.class public Lcom/twitter/library/api/upload/y;
.super Lcom/twitter/library/api/upload/n;
.source "Twttr"


# instance fields
.field private final n:Lcom/twitter/library/api/TwitterUser;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/api/TwitterUser;)V
    .locals 1

    const-class v0, Lcom/twitter/library/api/upload/y;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/upload/n;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    iput-object p3, p0, Lcom/twitter/library/api/upload/y;->n:Lcom/twitter/library/api/TwitterUser;

    return-void
.end method

.method private a(Lcom/twitter/library/api/upload/n;)Lcom/twitter/library/service/e;
    .locals 4

    iget-object v0, p0, Lcom/twitter/library/api/upload/y;->d:Lcom/twitter/library/client/v;

    invoke-virtual {p1, v0}, Lcom/twitter/library/api/upload/n;->a(Lcom/twitter/library/client/v;)Lcom/twitter/library/api/upload/n;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/api/upload/b;

    iget-object v2, p0, Lcom/twitter/library/api/upload/y;->l:Landroid/content/Context;

    const-string/jumbo v3, "tweet::media:upload"

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/api/upload/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/upload/n;->b(Lcom/twitter/library/api/upload/o;)Lcom/twitter/library/api/upload/ad;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/y;->r()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/upload/ad;->c(I)Lcom/twitter/library/service/b;

    invoke-super {p0, p1}, Lcom/twitter/library/api/upload/n;->a(Lcom/twitter/library/api/upload/ad;)Lcom/twitter/library/service/e;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected c(Lcom/twitter/library/service/e;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/library/api/upload/y;->k:Landroid/os/Bundle;

    const-string/jumbo v1, "user"

    iget-object v2, p0, Lcom/twitter/library/api/upload/y;->n:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/y;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/api/upload/y;->d:Lcom/twitter/library/client/v;

    iget-boolean v2, v2, Lcom/twitter/library/client/v;->c:Z

    if-eqz v2, :cond_5

    new-instance v0, Lcom/twitter/library/api/upload/ac;

    iget-object v2, p0, Lcom/twitter/library/api/upload/y;->l:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/library/api/upload/y;->n:Lcom/twitter/library/api/TwitterUser;

    invoke-direct {v0, v2, v1, v3}, Lcom/twitter/library/api/upload/ac;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;Lcom/twitter/library/api/TwitterUser;)V

    invoke-direct {p0, v0}, Lcom/twitter/library/api/upload/y;->a(Lcom/twitter/library/api/upload/n;)Lcom/twitter/library/service/e;

    move-result-object v0

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    iget-object v2, p0, Lcom/twitter/library/api/upload/y;->d:Lcom/twitter/library/client/v;

    iget-object v2, v2, Lcom/twitter/library/client/v;->a:Landroid/net/Uri;

    if-eqz v2, :cond_2

    new-instance v0, Lcom/twitter/library/api/upload/z;

    iget-object v2, p0, Lcom/twitter/library/api/upload/y;->l:Landroid/content/Context;

    invoke-direct {v0, v2, v1}, Lcom/twitter/library/api/upload/z;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;)V

    invoke-direct {p0, v0}, Lcom/twitter/library/api/upload/y;->a(Lcom/twitter/library/api/upload/n;)Lcom/twitter/library/service/e;

    move-result-object v0

    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    iget-object v2, p0, Lcom/twitter/library/api/upload/y;->d:Lcom/twitter/library/client/v;

    iget-boolean v2, v2, Lcom/twitter/library/client/v;->h:Z

    if-eqz v2, :cond_4

    new-instance v0, Lcom/twitter/library/api/upload/aa;

    iget-object v2, p0, Lcom/twitter/library/api/upload/y;->l:Landroid/content/Context;

    invoke-direct {v0, v2, v1}, Lcom/twitter/library/api/upload/aa;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;)V

    invoke-direct {p0, v0}, Lcom/twitter/library/api/upload/y;->a(Lcom/twitter/library/api/upload/n;)Lcom/twitter/library/service/e;

    move-result-object v0

    :cond_4
    if-eqz v0, :cond_6

    invoke-virtual {p1, v0}, Lcom/twitter/library/service/e;->a(Lcom/twitter/library/service/e;)V

    :goto_1
    return-void

    :cond_5
    iget-object v2, p0, Lcom/twitter/library/api/upload/y;->d:Lcom/twitter/library/client/v;

    iget-object v2, v2, Lcom/twitter/library/client/v;->b:Landroid/net/Uri;

    if-eqz v2, :cond_0

    new-instance v0, Lcom/twitter/library/api/upload/ab;

    iget-object v2, p0, Lcom/twitter/library/api/upload/y;->l:Landroid/content/Context;

    invoke-direct {v0, v2, v1}, Lcom/twitter/library/api/upload/ab;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;)V

    invoke-direct {p0, v0}, Lcom/twitter/library/api/upload/y;->a(Lcom/twitter/library/api/upload/n;)Lcom/twitter/library/service/e;

    move-result-object v0

    goto :goto_0

    :cond_6
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/twitter/library/service/e;->a(Z)V

    goto :goto_1
.end method
