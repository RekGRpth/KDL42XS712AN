.class public Lcom/twitter/library/api/upload/p;
.super Lcom/twitter/library/api/upload/c;
.source "Twttr"


# instance fields
.field private n:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    const-class v0, Lcom/twitter/library/api/upload/p;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/upload/c;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    return-void
.end method


# virtual methods
.method public a(J)Lcom/twitter/library/api/upload/p;
    .locals 0

    iput-wide p1, p0, Lcom/twitter/library/api/upload/p;->n:J

    return-object p0
.end method

.method protected a(Lcom/twitter/library/api/upload/w;)Lcom/twitter/library/api/upload/w;
    .locals 2

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Lcom/twitter/library/api/upload/w;->a(Z)Lcom/twitter/library/api/upload/w;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/upload/w;->c(Z)Lcom/twitter/library/service/b;

    return-object p1
.end method

.method protected a(Lcom/twitter/library/api/upload/w;Lcom/twitter/library/service/e;)Z
    .locals 3

    invoke-virtual {p2}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lcom/twitter/library/service/e;->c()I

    move-result v0

    const/16 v1, 0x3eb

    if-ne v0, v1, :cond_0

    const/16 v0, 0x3ec

    invoke-virtual {p2}, Lcom/twitter/library/service/e;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/twitter/library/service/e;->b()Ljava/lang/Exception;

    move-result-object v2

    invoke-virtual {p2, v0, v1, v2}, Lcom/twitter/library/service/e;->a(ILjava/lang/String;Ljava/lang/Exception;)V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected e()Landroid/database/Cursor;
    .locals 8

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/p;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    iget-wide v0, v0, Lcom/twitter/library/service/p;->c:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/twitter/library/api/upload/p;->l:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/provider/ah;->b:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/library/api/upload/p;->n:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string/jumbo v2, "ownerId"

    invoke-virtual {v1, v2, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string/jumbo v2, "limit"

    const-string/jumbo v3, "1"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/api/upload/p;->d:[Ljava/lang/String;

    const-string/jumbo v3, "flags&1=1 AND author_id=? AND _id=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    const/4 v5, 0x1

    iget-wide v6, p0, Lcom/twitter/library/api/upload/p;->n:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
