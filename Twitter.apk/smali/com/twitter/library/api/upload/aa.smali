.class public Lcom/twitter/library/api/upload/aa;
.super Lcom/twitter/library/api/upload/n;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/p;)V
    .locals 1

    const-class v0, Lcom/twitter/library/api/upload/aa;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/upload/n;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/p;)V

    return-void
.end method


# virtual methods
.method protected c(Lcom/twitter/library/service/e;)V
    .locals 6

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/aa;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lcom/twitter/library/api/upload/aa;->d:Lcom/twitter/library/client/v;

    iget-object v2, v2, Lcom/twitter/library/client/v;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "name"

    iget-object v4, p0, Lcom/twitter/library/api/upload/aa;->d:Lcom/twitter/library/client/v;

    iget-object v4, v4, Lcom/twitter/library/client/v;->d:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v2, p0, Lcom/twitter/library/api/upload/aa;->d:Lcom/twitter/library/client/v;

    iget-object v2, v2, Lcom/twitter/library/client/v;->f:Ljava/lang/String;

    if-eqz v2, :cond_1

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "url"

    iget-object v4, p0, Lcom/twitter/library/api/upload/aa;->d:Lcom/twitter/library/client/v;

    iget-object v4, v4, Lcom/twitter/library/client/v;->f:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v2, p0, Lcom/twitter/library/api/upload/aa;->d:Lcom/twitter/library/client/v;

    iget-object v2, v2, Lcom/twitter/library/client/v;->g:Ljava/lang/String;

    if-eqz v2, :cond_2

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "location"

    iget-object v4, p0, Lcom/twitter/library/api/upload/aa;->d:Lcom/twitter/library/client/v;

    iget-object v4, v4, Lcom/twitter/library/client/v;->g:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v2, p0, Lcom/twitter/library/api/upload/aa;->d:Lcom/twitter/library/client/v;

    iget-object v2, v2, Lcom/twitter/library/client/v;->e:Ljava/lang/String;

    if-eqz v2, :cond_3

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "description"

    iget-object v4, p0, Lcom/twitter/library/api/upload/aa;->d:Lcom/twitter/library/client/v;

    iget-object v4, v4, Lcom/twitter/library/client/v;->e:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-object v2, p0, Lcom/twitter/library/api/upload/aa;->m:Lcom/twitter/library/network/aa;

    iget-object v2, v2, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string/jumbo v5, "1.1"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "account"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string/jumbo v5, "update_profile"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/api/upload/ae;

    iget-object v4, p0, Lcom/twitter/library/api/upload/aa;->l:Landroid/content/Context;

    invoke-direct {v3, v4, v0}, Lcom/twitter/library/api/upload/ae;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v4}, Lcom/twitter/library/api/upload/ae;->a(Ljava/lang/StringBuilder;)Lcom/twitter/library/api/upload/ae;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/twitter/library/api/upload/ae;->a(Ljava/util/ArrayList;)Lcom/twitter/library/api/upload/ae;

    move-result-object v1

    iget-wide v3, v0, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v3, v4, v0}, Lcom/twitter/library/api/upload/aa;->a(JLjava/lang/String;)Lcom/twitter/library/api/ao;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/api/upload/ae;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/api/upload/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/upload/aa;->f:Lcom/twitter/library/api/upload/ae;

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/aa;->p()V

    iget-object v0, p0, Lcom/twitter/library/api/upload/aa;->f:Lcom/twitter/library/api/upload/ae;

    iget-object v1, p0, Lcom/twitter/library/api/upload/aa;->f:Lcom/twitter/library/api/upload/ae;

    invoke-virtual {v1}, Lcom/twitter/library/api/upload/ae;->c()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/api/upload/ae;->a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)Lcom/twitter/library/service/e;

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/aa;->q()V

    invoke-virtual {p0, p1}, Lcom/twitter/library/api/upload/aa;->d(Lcom/twitter/library/service/e;)Z

    return-void
.end method
