.class public Lcom/twitter/library/api/upload/j;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field protected static a:Lcom/twitter/library/api/upload/j;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/library/api/upload/j;

    invoke-direct {v0}, Lcom/twitter/library/api/upload/j;-><init>()V

    sput-object v0, Lcom/twitter/library/api/upload/j;->a:Lcom/twitter/library/api/upload/j;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;JLcom/twitter/library/api/MediaEntity;Z)Lcom/twitter/library/api/upload/h;
    .locals 6

    sget-object v0, Lcom/twitter/library/api/upload/j;->a:Lcom/twitter/library/api/upload/j;

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/api/upload/j;->b(Landroid/content/Context;JLcom/twitter/library/api/MediaEntity;Z)Lcom/twitter/library/api/upload/h;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected a()J
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string/jumbo v0, "photo_upload_segment_size_2079"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v0, "photo_upload_segment_size_2079"

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "size_50k"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/32 v0, 0xc800

    :goto_0
    return-wide v0

    :cond_0
    const-string/jumbo v0, "photo_upload_segment_size_2079"

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "size_100k"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-wide/32 v0, 0x19000

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "photo_upload_segment_size_2079"

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "size_256k"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-wide/32 v0, 0x40000

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "photo_upload_segment_size_2079"

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "size_512k"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-wide/32 v0, 0x80000

    goto :goto_0

    :cond_3
    const-string/jumbo v0, "photo_upload_segment_size_2079"

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "size_750k"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-wide/32 v0, 0xbb800

    goto :goto_0

    :cond_4
    const-string/jumbo v0, "photo_upload_segment_size_2079"

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "size_1m"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-wide/32 v0, 0x100000

    goto :goto_0

    :cond_5
    const-string/jumbo v0, "photo_upload_segment_size_2079"

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "dynamic"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/high16 v0, 0x300000

    invoke-static {v0}, Lcom/twitter/library/telephony/TelephonyUtil;->a(I)I

    move-result v0

    int-to-long v0, v0

    goto :goto_0

    :cond_6
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method protected b(Landroid/content/Context;JLcom/twitter/library/api/MediaEntity;Z)Lcom/twitter/library/api/upload/h;
    .locals 7

    if-eqz p5, :cond_0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->aI()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Lcom/twitter/library/api/upload/h;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/twitter/library/api/upload/h;-><init>(Landroid/content/Context;JLcom/twitter/library/api/MediaEntity;)V

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/api/upload/j;->a()J

    move-result-wide v4

    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-lez v0, :cond_2

    new-instance v0, Lcom/twitter/library/api/upload/q;

    move-object v1, p1

    move-wide v2, p2

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/api/upload/q;-><init>(Landroid/content/Context;JJLcom/twitter/library/api/MediaEntity;)V

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/twitter/library/api/upload/h;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/twitter/library/api/upload/h;-><init>(Landroid/content/Context;JLcom/twitter/library/api/MediaEntity;)V

    goto :goto_0
.end method
