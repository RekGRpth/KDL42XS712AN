.class public Lcom/twitter/library/api/upload/x;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Landroid/content/Context;Lcom/twitter/library/service/p;Lcom/twitter/library/api/TweetEntities;)Lcom/twitter/library/api/upload/w;
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    const/4 v2, 0x3

    invoke-virtual {p2, v2}, Lcom/twitter/library/api/TweetEntities;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->aH()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_1

    :goto_1
    if-eqz v0, :cond_2

    new-instance v0, Lcom/twitter/library/api/upload/u;

    invoke-direct {v0, p0, p1}, Lcom/twitter/library/api/upload/u;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;)V

    invoke-virtual {v0, p2}, Lcom/twitter/library/api/upload/u;->a(Lcom/twitter/library/api/TweetEntities;)Lcom/twitter/library/api/upload/w;

    move-result-object v0

    :goto_2
    return-object v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    if-eqz v2, :cond_4

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->aI()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/twitter/library/api/upload/b;

    const-string/jumbo v1, "::async_uploader:upload"

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/api/upload/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    :goto_3
    new-instance v1, Lcom/twitter/library/api/upload/k;

    invoke-direct {v1, p0, p1}, Lcom/twitter/library/api/upload/k;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;)V

    invoke-virtual {v1, v0}, Lcom/twitter/library/api/upload/k;->a(Lcom/twitter/library/api/upload/o;)Lcom/twitter/library/api/upload/k;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/library/api/upload/k;->a(Lcom/twitter/library/api/TweetEntities;)Lcom/twitter/library/api/upload/k;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/upload/k;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/upload/k;

    new-instance v1, Lcom/twitter/library/api/upload/v;

    invoke-direct {v1, p0, p1, v0}, Lcom/twitter/library/api/upload/v;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;Lcom/twitter/library/api/upload/g;)V

    invoke-virtual {v1, p2}, Lcom/twitter/library/api/upload/v;->a(Lcom/twitter/library/api/TweetEntities;)Lcom/twitter/library/api/upload/w;

    move-result-object v0

    goto :goto_2

    :cond_3
    new-instance v0, Lcom/twitter/library/api/upload/b;

    const-string/jumbo v1, "::segmented_uploader:upload"

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/api/upload/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_3

    :cond_4
    new-instance v0, Lcom/twitter/library/api/upload/w;

    const-class v1, Lcom/twitter/library/api/upload/w;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lcom/twitter/library/api/upload/w;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/p;)V

    invoke-virtual {v0, p2}, Lcom/twitter/library/api/upload/w;->a(Lcom/twitter/library/api/TweetEntities;)Lcom/twitter/library/api/upload/w;

    move-result-object v0

    goto :goto_2
.end method
