.class public Lcom/twitter/library/api/upload/d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/api/upload/MediaProcessor;


# instance fields
.field private a:Landroid/content/Context;

.field private b:[I

.field private c:[I

.field private d:I

.field private e:Landroid/net/Uri;

.field private f:J

.field private g:Landroid/net/Uri;

.field private h:I

.field private i:I

.field private j:Lcom/twitter/library/util/l;


# direct methods
.method public constructor <init>(Landroid/content/Context;[I[IILandroid/net/Uri;J)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/upload/d;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/twitter/library/api/upload/d;->b:[I

    iput-object p3, p0, Lcom/twitter/library/api/upload/d;->c:[I

    iput p4, p0, Lcom/twitter/library/api/upload/d;->d:I

    iput-object p5, p0, Lcom/twitter/library/api/upload/d;->e:Landroid/net/Uri;

    iput-wide p6, p0, Lcom/twitter/library/api/upload/d;->f:J

    iput-object p5, p0, Lcom/twitter/library/api/upload/d;->g:Landroid/net/Uri;

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/library/api/upload/d;->h:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/library/api/upload/d;->i:I

    if-eqz p5, :cond_0

    const/4 v0, 0x1

    invoke-static {p1, p5, v0}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;Landroid/net/Uri;Z)Lcom/twitter/library/util/l;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/upload/d;->j:Lcom/twitter/library/util/l;

    :cond_0
    return-void
.end method

.method private a(Landroid/net/Uri;IIZ)Landroid/net/Uri;
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/twitter/library/api/upload/d;->a:Landroid/content/Context;

    invoke-static {v1, p1}, Lkw;->a(Landroid/content/Context;Landroid/net/Uri;)Lkw;

    move-result-object v1

    invoke-virtual {v1, p2, p2}, Lkw;->a(II)Lkw;

    invoke-virtual {v1}, Lkw;->a()Lkw;

    invoke-virtual {v1}, Lkw;->c()Landroid/graphics/Rect;

    move-result-object v2

    if-nez v2, :cond_3

    :cond_1
    :goto_1
    if-ne v0, p1, :cond_2

    const-string/jumbo v1, "resize"

    const-string/jumbo v2, "skip"

    invoke-direct {p0, v1, v2, p2}, Lcom/twitter/library/api/upload/d;->a(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_2
    if-eqz v0, :cond_5

    const-string/jumbo v1, "resize"

    const-string/jumbo v2, "success"

    invoke-direct {p0, v1, v2, p2}, Lcom/twitter/library/api/upload/d;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Lkw;->d()Z

    move-result v2

    if-nez v2, :cond_4

    const/16 v2, 0x64

    if-ge p3, v2, :cond_6

    :cond_4
    invoke-virtual {v1}, Lkw;->b()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/twitter/library/api/upload/d;->a:Landroid/content/Context;

    iget-wide v2, p0, Lcom/twitter/library/api/upload/d;->f:J

    invoke-static {v0, v1, v2, v3, p3}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;Landroid/graphics/Bitmap;JI)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    if-nez p4, :cond_1

    invoke-static {p1}, Lcom/twitter/library/util/n;->b(Landroid/net/Uri;)Z

    goto :goto_1

    :cond_5
    const-string/jumbo v1, "resize"

    const-string/jumbo v2, "failure"

    invoke-direct {p0, v1, v2, p2}, Lcom/twitter/library/api/upload/d;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    :cond_6
    move-object v0, p1

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "::image_processor"

    aput-object v1, v0, v5

    aput-object p1, v0, v6

    aput-object p2, v0, v7

    invoke-static {v0}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/upload/d;->a:Landroid/content/Context;

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    iget-wide v3, p0, Lcom/twitter/library/api/upload/d;->f:J

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v3, v6, [Ljava/lang/String;

    aput-object v0, v3, v5

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/twitter/library/scribe/ScribeLog;->c(I)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "size="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method private f()I
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/api/upload/d;->b:[I

    aget v0, v0, v1

    add-int/lit8 v0, v0, 0x1

    iget-object v2, p0, Lcom/twitter/library/api/upload/d;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/library/api/upload/d;->e:Landroid/net/Uri;

    invoke-static {v2, v3}, Lkw;->a(Landroid/content/Context;Landroid/net/Uri;)Lkw;

    move-result-object v2

    invoke-virtual {v2, v0}, Lkw;->c(I)Lkw;

    invoke-virtual {v2}, Lkw;->a()Lkw;

    invoke-virtual {v2}, Lkw;->c()Landroid/graphics/Rect;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    const/4 v0, 0x1

    :goto_0
    iget-object v3, p0, Lcom/twitter/library/api/upload/d;->b:[I

    array-length v3, v3

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lcom/twitter/library/api/upload/d;->b:[I

    aget v3, v3, v0

    if-le v2, v3, :cond_1

    :cond_0
    return v1

    :cond_1
    add-int/lit8 v1, v0, -0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private g()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/api/upload/d;->e:Landroid/net/Uri;

    iget-object v1, p0, Lcom/twitter/library/api/upload/d;->g:Landroid/net/Uri;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/upload/d;->g:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/upload/d;->g:Landroid/net/Uri;

    invoke-static {v0}, Lcom/twitter/library/util/n;->b(Landroid/net/Uri;)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/api/upload/d;->g:Landroid/net/Uri;

    :cond_0
    return-void
.end method


# virtual methods
.method public a()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/upload/d;->e:Landroid/net/Uri;

    return-object v0
.end method

.method public a(Z)V
    .locals 2

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/library/api/upload/d;->e:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/upload/d;->e:Landroid/net/Uri;

    iget-object v1, p0, Lcom/twitter/library/api/upload/d;->g:Landroid/net/Uri;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/upload/d;->e:Landroid/net/Uri;

    invoke-static {v0}, Lcom/twitter/library/util/n;->b(Landroid/net/Uri;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/twitter/library/api/upload/d;->g()V

    goto :goto_0
.end method

.method public b()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/upload/d;->g:Landroid/net/Uri;

    return-object v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/api/upload/d;->i:I

    return v0
.end method

.method public d()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/api/upload/d;->e:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/library/api/upload/d;->h:I

    iget-object v1, p0, Lcom/twitter/library/api/upload/d;->b:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Landroid/net/Uri;
    .locals 6

    iget v0, p0, Lcom/twitter/library/api/upload/d;->h:I

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/d;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/twitter/library/api/upload/d;->f()I

    move-result v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    :goto_1
    iget-object v1, p0, Lcom/twitter/library/api/upload/d;->b:[I

    array-length v1, v1

    if-ge v0, v1, :cond_5

    :try_start_0
    iget-object v2, p0, Lcom/twitter/library/api/upload/d;->g:Landroid/net/Uri;

    iget-object v1, p0, Lcom/twitter/library/api/upload/d;->b:[I

    aget v3, v1, v0

    iget-object v1, p0, Lcom/twitter/library/api/upload/d;->c:[I

    aget v4, v1, v0

    iget-object v1, p0, Lcom/twitter/library/api/upload/d;->g:Landroid/net/Uri;

    iget-object v5, p0, Lcom/twitter/library/api/upload/d;->e:Landroid/net/Uri;

    if-ne v1, v5, :cond_3

    const/4 v1, 0x1

    :goto_2
    invoke-direct {p0, v2, v3, v4, v1}, Lcom/twitter/library/api/upload/d;->a(Landroid/net/Uri;IIZ)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/upload/d;->g:Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v1, p0, Lcom/twitter/library/api/upload/d;->g:Landroid/net/Uri;

    if-nez v1, :cond_4

    :cond_2
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    :cond_4
    iget-object v1, p0, Lcom/twitter/library/api/upload/d;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/library/api/upload/d;->g:Landroid/net/Uri;

    invoke-static {v1, v2}, Lcom/twitter/library/util/Util;->c(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/api/upload/d;->i:I

    iget v1, p0, Lcom/twitter/library/api/upload/d;->d:I

    iget v2, p0, Lcom/twitter/library/api/upload/d;->i:I

    if-le v1, v2, :cond_2

    :cond_5
    iget-object v1, p0, Lcom/twitter/library/api/upload/d;->g:Landroid/net/Uri;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/twitter/library/api/upload/d;->g:Landroid/net/Uri;

    iget-object v2, p0, Lcom/twitter/library/api/upload/d;->e:Landroid/net/Uri;

    if-eq v1, v2, :cond_6

    iget-object v1, p0, Lcom/twitter/library/api/upload/d;->j:Lcom/twitter/library/util/l;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/twitter/library/api/upload/d;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/library/api/upload/d;->j:Lcom/twitter/library/util/l;

    iget-object v3, p0, Lcom/twitter/library/api/upload/d;->g:Landroid/net/Uri;

    invoke-static {v1, v2, v3}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;Lcom/twitter/library/util/l;Landroid/net/Uri;)V

    :cond_6
    iput v0, p0, Lcom/twitter/library/api/upload/d;->h:I

    iget-object v0, p0, Lcom/twitter/library/api/upload/d;->g:Landroid/net/Uri;

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3
.end method
