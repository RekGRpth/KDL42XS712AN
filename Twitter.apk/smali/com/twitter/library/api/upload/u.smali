.class public Lcom/twitter/library/api/upload/u;
.super Lcom/twitter/library/api/upload/w;
.source "Twttr"


# instance fields
.field private n:F


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/p;)V
    .locals 1

    const-class v0, Lcom/twitter/library/api/upload/u;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/upload/w;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/p;)V

    return-void
.end method


# virtual methods
.method protected d(Lcom/twitter/library/service/e;)V
    .locals 6

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/u;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    iget-wide v4, v0, Lcom/twitter/library/service/p;->c:J

    new-instance v1, Lcom/twitter/library/api/upload/h;

    iget-object v2, p0, Lcom/twitter/library/api/upload/u;->l:Landroid/content/Context;

    iget-object v0, p0, Lcom/twitter/library/api/upload/u;->d:Lcom/twitter/library/api/TweetEntities;

    iget-object v0, v0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    invoke-direct {v1, v2, v4, v5, v0}, Lcom/twitter/library/api/upload/h;-><init>(Landroid/content/Context;JLcom/twitter/library/api/MediaEntity;)V

    iput-object v1, p0, Lcom/twitter/library/api/upload/u;->g:Lcom/twitter/library/api/upload/h;

    iget-object v0, p0, Lcom/twitter/library/api/upload/u;->g:Lcom/twitter/library/api/upload/h;

    invoke-virtual {v0}, Lcom/twitter/library/api/upload/h;->e()Z

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x3ea

    new-instance v1, Lcom/twitter/library/util/MediaException;

    const-string/jumbo v2, "Failed to process a tweet"

    invoke-direct {v1, v2}, Lcom/twitter/library/util/MediaException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0, v1}, Lcom/twitter/library/service/e;->a(ILjava/lang/Exception;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/api/upload/u;->g:Lcom/twitter/library/api/upload/h;

    invoke-virtual {v0}, Lcom/twitter/library/api/upload/h;->g()Lcom/twitter/library/api/MediaEntity;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/library/api/upload/u;->l:Landroid/content/Context;

    iget-object v1, v2, Lcom/twitter/library/api/MediaEntity;->url:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget v2, v2, Lcom/twitter/library/api/MediaEntity;->type:I

    sget-object v3, Lcom/twitter/library/api/upload/MediaProcessor$Action;->a:Lcom/twitter/library/api/upload/MediaProcessor$Action;

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/api/upload/MediaProcessorFactory;->a(Landroid/content/Context;Landroid/net/Uri;ILcom/twitter/library/api/upload/MediaProcessor$Action;J)Lcom/twitter/library/api/upload/MediaProcessor;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/u;->p()V

    iget-object v1, p0, Lcom/twitter/library/api/upload/u;->f:Lcom/twitter/library/api/upload/ae;

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/u;->n()Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/api/upload/ae;->a(Ljava/lang/StringBuilder;)Lcom/twitter/library/api/upload/ae;

    iget-object v1, p0, Lcom/twitter/library/api/upload/u;->g:Lcom/twitter/library/api/upload/h;

    iget-object v2, p0, Lcom/twitter/library/api/upload/u;->f:Lcom/twitter/library/api/upload/ae;

    invoke-virtual {v1, v2}, Lcom/twitter/library/api/upload/h;->a(Lcom/twitter/library/api/upload/ae;)Lcom/twitter/library/api/upload/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/api/upload/h;->a(Lcom/twitter/library/api/upload/MediaProcessor;)Lcom/twitter/library/api/upload/h;

    move-result-object v1

    const-string/jumbo v2, "media[]"

    invoke-virtual {v1, v2}, Lcom/twitter/library/api/upload/h;->a(Ljava/lang/String;)Lcom/twitter/library/api/upload/h;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/twitter/library/api/upload/h;->a(Lcom/twitter/library/service/e;)Lcom/twitter/library/api/upload/h;

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/u;->q()V

    invoke-virtual {p1}, Lcom/twitter/library/service/e;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lcom/twitter/library/api/upload/MediaProcessor;->a()Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/upload/u;->l:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/twitter/library/util/Util;->c(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/twitter/library/api/upload/u;->n:F

    invoke-static {v0}, Lcom/twitter/library/util/n;->b(Landroid/net/Uri;)Z

    goto :goto_0
.end method

.method protected e()Ljava/lang/StringBuilder;
    .locals 4

    iget-object v0, p0, Lcom/twitter/library/api/upload/u;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "1.1"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "statuses"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "update_with_media"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public f()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public g()F
    .locals 1

    iget v0, p0, Lcom/twitter/library/api/upload/u;->n:F

    return v0
.end method
