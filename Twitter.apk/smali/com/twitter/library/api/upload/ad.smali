.class public abstract Lcom/twitter/library/api/upload/ad;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# static fields
.field protected static final e:Z


# instance fields
.field private d:Lcom/twitter/library/api/upload/o;

.field protected f:Lcom/twitter/library/api/upload/ae;

.field protected g:Lcom/twitter/library/api/upload/h;

.field private n:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Lcom/twitter/library/client/App;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "UploadRequest"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/library/api/upload/ad;->e:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/p;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/p;)V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/api/upload/ad;)Lcom/twitter/library/service/e;
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/api/upload/ad;->k:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Lcom/twitter/library/api/upload/ad;->c(Landroid/os/Bundle;)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/api/upload/ad;->a(Lcom/twitter/internal/android/service/a;)Lcom/twitter/library/service/e;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/library/api/upload/ad;->k:Landroid/os/Bundle;

    invoke-virtual {p0, v1}, Lcom/twitter/library/api/upload/ad;->c(Landroid/os/Bundle;)Lcom/twitter/library/service/b;

    return-object v0
.end method

.method public b(Lcom/twitter/library/api/upload/o;)Lcom/twitter/library/api/upload/ad;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/upload/ad;->d:Lcom/twitter/library/api/upload/o;

    return-object p0
.end method

.method protected final b(Lcom/twitter/library/service/e;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/api/upload/ad;->d:Lcom/twitter/library/api/upload/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/upload/ad;->d:Lcom/twitter/library/api/upload/o;

    invoke-virtual {v0, p0}, Lcom/twitter/library/api/upload/o;->a(Lcom/twitter/library/api/upload/ad;)V

    :cond_0
    invoke-virtual {p0, p1}, Lcom/twitter/library/api/upload/ad;->c(Lcom/twitter/library/service/e;)V

    iget-object v0, p0, Lcom/twitter/library/api/upload/ad;->d:Lcom/twitter/library/api/upload/o;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/api/upload/ad;->g:Lcom/twitter/library/api/upload/h;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/api/upload/ad;->d:Lcom/twitter/library/api/upload/o;

    iget-object v1, p0, Lcom/twitter/library/api/upload/ad;->g:Lcom/twitter/library/api/upload/h;

    invoke-virtual {v0, p0, p1, v1}, Lcom/twitter/library/api/upload/o;->a(Lcom/twitter/library/api/upload/ad;Lcom/twitter/library/service/e;Lcom/twitter/library/api/upload/h;)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/api/upload/ad;->d:Lcom/twitter/library/api/upload/o;

    invoke-virtual {v0, p0, p1}, Lcom/twitter/library/api/upload/o;->a(Lcom/twitter/library/api/upload/ad;Lcom/twitter/library/service/e;)V

    :cond_2
    return-void
.end method

.method protected abstract c(Lcom/twitter/library/service/e;)V
.end method

.method public o()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/api/upload/ad;->n:J

    return-wide v0
.end method

.method protected p()V
    .locals 2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/api/upload/ad;->n:J

    return-void
.end method

.method protected q()V
    .locals 4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/twitter/library/api/upload/ad;->n:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/twitter/library/api/upload/ad;->n:J

    return-void
.end method
