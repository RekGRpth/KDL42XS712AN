.class public Lcom/twitter/library/api/upload/z;
.super Lcom/twitter/library/api/upload/n;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/p;)V
    .locals 1

    const-class v0, Lcom/twitter/library/api/upload/z;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/upload/n;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/p;)V

    return-void
.end method


# virtual methods
.method protected c(Lcom/twitter/library/service/e;)V
    .locals 8

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/twitter/library/api/upload/z;->d:Lcom/twitter/library/client/v;

    iget-object v0, v0, Lcom/twitter/library/client/v;->a:Landroid/net/Uri;

    if-nez v0, :cond_1

    const/16 v0, 0x19d

    invoke-virtual {p1, v0}, Lcom/twitter/library/service/e;->a(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/api/upload/z;->s()Lcom/twitter/library/service/p;

    move-result-object v6

    iget-wide v4, v6, Lcom/twitter/library/service/p;->c:J

    iget-object v0, p0, Lcom/twitter/library/api/upload/z;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string/jumbo v7, "1.1"

    aput-object v7, v1, v3

    const-string/jumbo v3, "account"

    aput-object v3, v1, v2

    const/4 v3, 0x2

    const-string/jumbo v7, "update_profile_image"

    aput-object v7, v1, v3

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v0, p0, Lcom/twitter/library/api/upload/z;->l:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/library/api/upload/z;->d:Lcom/twitter/library/client/v;

    iget-object v1, v1, Lcom/twitter/library/client/v;->a:Landroid/net/Uri;

    sget-object v3, Lcom/twitter/library/api/upload/MediaProcessor$Action;->b:Lcom/twitter/library/api/upload/MediaProcessor$Action;

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/api/upload/MediaProcessorFactory;->a(Landroid/content/Context;Landroid/net/Uri;ILcom/twitter/library/api/upload/MediaProcessor$Action;J)Lcom/twitter/library/api/upload/MediaProcessor;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/api/upload/ae;

    iget-object v2, p0, Lcom/twitter/library/api/upload/z;->l:Landroid/content/Context;

    invoke-direct {v1, v2, v6}, Lcom/twitter/library/api/upload/ae;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;)V

    invoke-virtual {v1, v7}, Lcom/twitter/library/api/upload/ae;->a(Ljava/lang/StringBuilder;)Lcom/twitter/library/api/upload/ae;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v4, v5, v2}, Lcom/twitter/library/api/upload/z;->a(JLjava/lang/String;)Lcom/twitter/library/api/ao;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/api/upload/ae;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/api/upload/ae;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/upload/z;->f:Lcom/twitter/library/api/upload/ae;

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/z;->p()V

    new-instance v1, Lcom/twitter/library/api/upload/h;

    iget-object v2, p0, Lcom/twitter/library/api/upload/z;->l:Landroid/content/Context;

    invoke-direct {v1, v2, v4, v5}, Lcom/twitter/library/api/upload/h;-><init>(Landroid/content/Context;J)V

    iget-object v2, p0, Lcom/twitter/library/api/upload/z;->f:Lcom/twitter/library/api/upload/ae;

    invoke-virtual {v1, v2}, Lcom/twitter/library/api/upload/h;->a(Lcom/twitter/library/api/upload/ae;)Lcom/twitter/library/api/upload/h;

    move-result-object v1

    const-string/jumbo v2, "image"

    invoke-virtual {v1, v2}, Lcom/twitter/library/api/upload/h;->a(Ljava/lang/String;)Lcom/twitter/library/api/upload/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/api/upload/h;->a(Lcom/twitter/library/api/upload/MediaProcessor;)Lcom/twitter/library/api/upload/h;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/twitter/library/api/upload/h;->a(Lcom/twitter/library/service/e;)Lcom/twitter/library/api/upload/h;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/upload/z;->g:Lcom/twitter/library/api/upload/h;

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/z;->q()V

    invoke-virtual {p0, p1}, Lcom/twitter/library/api/upload/z;->d(Lcom/twitter/library/service/e;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/api/upload/z;->k:Landroid/os/Bundle;

    const-string/jumbo v2, "avatar_uri"

    invoke-interface {v0}, Lcom/twitter/library/api/upload/MediaProcessor;->b()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto/16 :goto_0
.end method
