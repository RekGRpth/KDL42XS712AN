.class public abstract Lcom/twitter/library/api/upload/c;
.super Lcom/twitter/library/api/upload/ad;
.source "Twttr"


# static fields
.field protected static final d:[Ljava/lang/String;


# instance fields
.field private n:Lcom/twitter/library/api/upload/o;

.field private o:Lcom/twitter/library/api/upload/w;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "content"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "in_r_status_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "url_entities"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "mention_entities"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "media_entities"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "hashtag_entities"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "pc"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/library/api/upload/c;->d:[Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/api/upload/ad;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/api/upload/o;)Lcom/twitter/library/api/upload/c;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/upload/c;->n:Lcom/twitter/library/api/upload/o;

    return-object p0
.end method

.method protected abstract a(Lcom/twitter/library/api/upload/w;)Lcom/twitter/library/api/upload/w;
.end method

.method protected abstract a(Lcom/twitter/library/api/upload/w;Lcom/twitter/library/service/e;)Z
.end method

.method protected c(Lcom/twitter/library/service/e;)V
    .locals 12

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/c;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    iget-wide v2, v1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/c;->e()Landroid/database/Cursor;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/library/api/upload/c;->l:Landroid/content/Context;

    invoke-static {v0, v2, v3}, Lcom/twitter/library/provider/c;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/c;

    move-result-object v2

    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v0

    new-array v3, v0, [J

    :cond_0
    const/4 v0, 0x0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    const/4 v0, 0x1

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v0, 0x2

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    new-instance v0, Lcom/twitter/library/api/TweetEntities;

    invoke-direct {v0}, Lcom/twitter/library/api/TweetEntities;-><init>()V

    const/4 v10, 0x3

    invoke-interface {v4, v10}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v10

    invoke-virtual {v0, v10}, Lcom/twitter/library/api/TweetEntities;->a([B)Lcom/twitter/library/api/TweetEntities;

    move-result-object v0

    const/4 v10, 0x4

    invoke-interface {v4, v10}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v10

    invoke-virtual {v0, v10}, Lcom/twitter/library/api/TweetEntities;->b([B)Lcom/twitter/library/api/TweetEntities;

    move-result-object v0

    const/4 v10, 0x5

    invoke-interface {v4, v10}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v10

    invoke-virtual {v0, v10}, Lcom/twitter/library/api/TweetEntities;->c([B)Lcom/twitter/library/api/TweetEntities;

    move-result-object v0

    const/4 v10, 0x6

    invoke-interface {v4, v10}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v10

    invoke-virtual {v0, v10}, Lcom/twitter/library/api/TweetEntities;->d([B)Lcom/twitter/library/api/TweetEntities;

    move-result-object v10

    const/4 v0, 0x7

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/PromotedContent;

    iget-object v11, p0, Lcom/twitter/library/api/upload/c;->l:Landroid/content/Context;

    invoke-static {v11, v1, v10}, Lcom/twitter/library/api/upload/x;->a(Landroid/content/Context;Lcom/twitter/library/service/p;Lcom/twitter/library/api/TweetEntities;)Lcom/twitter/library/api/upload/w;

    move-result-object v10

    invoke-virtual {v10, v5, v6}, Lcom/twitter/library/api/upload/w;->a(J)Lcom/twitter/library/api/upload/w;

    move-result-object v10

    invoke-virtual {v10, v7}, Lcom/twitter/library/api/upload/w;->a(Ljava/lang/String;)Lcom/twitter/library/api/upload/w;

    move-result-object v7

    invoke-virtual {v7, v0}, Lcom/twitter/library/api/upload/w;->a(Lcom/twitter/library/api/PromotedContent;)Lcom/twitter/library/api/upload/w;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Lcom/twitter/library/api/upload/w;->b(J)Lcom/twitter/library/api/upload/w;

    move-result-object v0

    iget-object v7, p0, Lcom/twitter/library/api/upload/c;->n:Lcom/twitter/library/api/upload/o;

    invoke-virtual {v0, v7}, Lcom/twitter/library/api/upload/w;->b(Lcom/twitter/library/api/upload/o;)Lcom/twitter/library/api/upload/ad;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/c;->r()I

    move-result v7

    invoke-virtual {v0, v7}, Lcom/twitter/library/api/upload/ad;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/upload/w;

    iput-object v0, p0, Lcom/twitter/library/api/upload/c;->o:Lcom/twitter/library/api/upload/w;

    iget-object v0, p0, Lcom/twitter/library/api/upload/c;->o:Lcom/twitter/library/api/upload/w;

    invoke-virtual {p0, v0}, Lcom/twitter/library/api/upload/c;->a(Lcom/twitter/library/api/upload/w;)Lcom/twitter/library/api/upload/w;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/api/upload/c;->a(Lcom/twitter/library/api/upload/ad;)Lcom/twitter/library/service/e;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/library/service/e;->a(Lcom/twitter/library/service/e;)V

    invoke-virtual {p1}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/api/upload/c;->o:Lcom/twitter/library/api/upload/w;

    invoke-virtual {v0}, Lcom/twitter/library/api/upload/w;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    invoke-interface {v4}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    aput-wide v5, v3, v0

    :cond_2
    iget-object v0, p0, Lcom/twitter/library/api/upload/c;->o:Lcom/twitter/library/api/upload/w;

    invoke-virtual {p0, v0, p1}, Lcom/twitter/library/api/upload/c;->a(Lcom/twitter/library/api/upload/w;Lcom/twitter/library/service/e;)Z

    move-result v0

    if-nez v0, :cond_4

    :goto_0
    invoke-virtual {v2, v3}, Lcom/twitter/library/provider/c;->a([J)I

    :goto_1
    if-eqz v4, :cond_3

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    :cond_3
    return-void

    :cond_4
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_5
    const/16 v0, 0x130

    invoke-virtual {p1, v0}, Lcom/twitter/library/service/e;->a(I)V

    goto :goto_1
.end method

.method protected abstract e()Landroid/database/Cursor;
.end method

.method public f()Lcom/twitter/library/api/upload/w;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/upload/c;->o:Lcom/twitter/library/api/upload/w;

    return-object v0
.end method
