.class public Lcom/twitter/library/api/upload/b;
.super Lcom/twitter/library/api/upload/o;
.source "Twttr"


# instance fields
.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/library/api/upload/o;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/twitter/library/api/upload/b;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/api/upload/ad;Lcom/twitter/library/service/e;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/api/upload/ad;Lcom/twitter/library/service/e;Lcom/twitter/library/api/upload/h;)V
    .locals 7

    const/4 v6, 0x1

    invoke-virtual {p2}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/library/api/upload/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":failure"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p3}, Lcom/twitter/library/api/upload/h;->d()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/util/q;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/api/upload/b;->a:Landroid/content/Context;

    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p1}, Lcom/twitter/library/api/upload/ad;->s()Lcom/twitter/library/service/p;

    move-result-object v4

    iget-wide v4, v4, Lcom/twitter/library/service/p;->c:J

    invoke-direct {v3, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v4, v6, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Lcom/twitter/library/scribe/ScribeLog;->c(I)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    return-void

    :cond_0
    invoke-virtual {p3}, Lcom/twitter/library/api/upload/h;->c()I

    move-result v0

    if-le v0, v6, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/library/api/upload/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":retry"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/library/api/upload/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":success"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
