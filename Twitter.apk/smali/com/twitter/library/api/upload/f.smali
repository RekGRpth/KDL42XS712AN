.class public Lcom/twitter/library/api/upload/f;
.super Lcom/twitter/library/api/upload/ad;
.source "Twttr"


# instance fields
.field private d:Lcom/twitter/library/api/ao;

.field private n:J

.field private o:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/p;)V
    .locals 1

    const-class v0, Lcom/twitter/library/api/upload/f;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/upload/ad;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/p;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/api/upload/h;)Lcom/twitter/library/api/upload/f;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/upload/f;->g:Lcom/twitter/library/api/upload/h;

    return-object p0
.end method

.method protected c(Lcom/twitter/library/service/e;)V
    .locals 7

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/f;->f()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/api/upload/f;->g:Lcom/twitter/library/api/upload/h;

    invoke-virtual {v0}, Lcom/twitter/library/api/upload/h;->e()Z

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x3ea

    new-instance v1, Lcom/twitter/library/util/MediaException;

    const-string/jumbo v2, "Failed to process media"

    invoke-direct {v1, v2}, Lcom/twitter/library/util/MediaException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0, v1}, Lcom/twitter/library/service/e;->a(ILjava/lang/Exception;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/api/upload/f;->g:Lcom/twitter/library/api/upload/h;

    invoke-virtual {v0}, Lcom/twitter/library/api/upload/h;->g()Lcom/twitter/library/api/MediaEntity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/f;->s()Lcom/twitter/library/service/p;

    move-result-object v6

    iget-object v0, p0, Lcom/twitter/library/api/upload/f;->l:Landroid/content/Context;

    iget-object v1, v2, Lcom/twitter/library/api/MediaEntity;->url:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget v2, v2, Lcom/twitter/library/api/MediaEntity;->type:I

    sget-object v3, Lcom/twitter/library/api/upload/MediaProcessor$Action;->a:Lcom/twitter/library/api/upload/MediaProcessor$Action;

    iget-wide v4, v6, Lcom/twitter/library/service/p;->c:J

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/api/upload/MediaProcessorFactory;->a(Landroid/content/Context;Landroid/net/Uri;ILcom/twitter/library/api/upload/MediaProcessor$Action;J)Lcom/twitter/library/api/upload/MediaProcessor;

    move-result-object v1

    const/16 v0, 0x37

    invoke-static {v0}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/upload/f;->d:Lcom/twitter/library/api/ao;

    new-instance v0, Lcom/twitter/library/api/upload/ae;

    iget-object v2, p0, Lcom/twitter/library/api/upload/f;->l:Landroid/content/Context;

    invoke-direct {v0, v2, v6}, Lcom/twitter/library/api/upload/ae;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;)V

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/f;->h()Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/api/upload/ae;->a(Ljava/lang/StringBuilder;)Lcom/twitter/library/api/upload/ae;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/f;->t()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/api/upload/ae;->a(Z)Lcom/twitter/library/api/upload/ae;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/library/api/upload/f;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {v0, v2}, Lcom/twitter/library/api/upload/ae;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/api/upload/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/upload/f;->f:Lcom/twitter/library/api/upload/ae;

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/f;->p()V

    iget-object v0, p0, Lcom/twitter/library/api/upload/f;->g:Lcom/twitter/library/api/upload/h;

    iget-object v2, p0, Lcom/twitter/library/api/upload/f;->f:Lcom/twitter/library/api/upload/ae;

    invoke-virtual {v0, v2}, Lcom/twitter/library/api/upload/h;->a(Lcom/twitter/library/api/upload/ae;)Lcom/twitter/library/api/upload/h;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/upload/h;->a(Lcom/twitter/library/api/upload/MediaProcessor;)Lcom/twitter/library/api/upload/h;

    move-result-object v0

    const-string/jumbo v2, "media"

    invoke-virtual {v0, v2}, Lcom/twitter/library/api/upload/h;->a(Ljava/lang/String;)Lcom/twitter/library/api/upload/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/library/api/upload/h;->a(Lcom/twitter/library/service/e;)Lcom/twitter/library/api/upload/h;

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/f;->q()V

    invoke-virtual {p1}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/api/upload/f;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/library/api/upload/f;->n:J

    invoke-interface {v1}, Lcom/twitter/library/api/upload/MediaProcessor;->b()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/upload/f;->o:Landroid/net/Uri;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/library/api/upload/f;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    const-string/jumbo v1, "custom_errors"

    invoke-static {v0}, Lcom/twitter/library/api/upload/f;->c(Ljava/util/ArrayList;)[I

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/twitter/library/api/upload/f;->a(Ljava/lang/String;[I)Lcom/twitter/library/service/b;

    goto/16 :goto_0
.end method

.method public e()J
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/api/upload/f;->g:Lcom/twitter/library/api/upload/h;

    invoke-virtual {v0}, Lcom/twitter/library/api/upload/h;->b()J

    move-result-wide v0

    return-wide v0
.end method

.method public f()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/upload/f;->g:Lcom/twitter/library/api/upload/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/upload/f;->g:Lcom/twitter/library/api/upload/h;

    invoke-virtual {v0}, Lcom/twitter/library/api/upload/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/api/upload/f;->n:J

    return-wide v0
.end method

.method protected h()Ljava/lang/StringBuilder;
    .locals 4

    iget-object v0, p0, Lcom/twitter/library/api/upload/f;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->d:Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "1.1"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "media"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "upload"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    return-object v0
.end method
