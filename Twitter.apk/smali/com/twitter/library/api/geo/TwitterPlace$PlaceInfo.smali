.class public Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Externalizable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public attributes:Ljava/util/HashMap;

.field public boundingCoordinates:Ljava/util/ArrayList;

.field public country:Ljava/lang/String;

.field public countryCode:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public parent:Lcom/twitter/library/api/geo/TwitterPlace;

.field public polyline:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/library/api/geo/e;

    invoke-direct {v0}, Lcom/twitter/library/api/geo/e;-><init>()V

    sput-object v0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->name:Ljava/lang/String;

    const-class v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->boundingCoordinates:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->country:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->countryCode:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->polyline:Ljava/lang/String;

    const-class v0, Lcom/twitter/library/api/geo/TwitterPlace;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/geo/TwitterPlace;

    iput-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->parent:Lcom/twitter/library/api/geo/TwitterPlace;

    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->attributes:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/api/geo/TwitterPlace;Ljava/util/HashMap;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->name:Ljava/lang/String;

    iput-object p2, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->boundingCoordinates:Ljava/util/ArrayList;

    iput-object p3, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->country:Ljava/lang/String;

    iput-object p4, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->countryCode:Ljava/lang/String;

    iput-object p5, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->polyline:Ljava/lang/String;

    iput-object p6, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->parent:Lcom/twitter/library/api/geo/TwitterPlace;

    iput-object p7, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->attributes:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 2

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->name:Ljava/lang/String;

    const-class v0, [Ljava/lang/Double;

    invoke-static {v0, p1}, Lcom/twitter/library/util/Util;->a(Ljava/lang/Class;Ljava/io/ObjectInput;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Double;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->boundingCoordinates:Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->country:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->countryCode:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->polyline:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/geo/TwitterPlace;

    iput-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->parent:Lcom/twitter/library/api/geo/TwitterPlace;

    const-class v0, Ljava/lang/String;

    const-class v1, Ljava/lang/String;

    invoke-static {v0, v1, p1}, Lcom/twitter/library/util/Util;->a(Ljava/lang/Class;Ljava/lang/Class;Ljava/io/ObjectInput;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->attributes:Ljava/util/HashMap;

    return-void

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->name:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->boundingCoordinates:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0, p1}, Lcom/twitter/library/util/Util;->a([Ljava/lang/Object;Ljava/io/ObjectOutput;)V

    iget-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->country:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->countryCode:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->polyline:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->parent:Lcom/twitter/library/api/geo/TwitterPlace;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->attributes:Ljava/util/HashMap;

    invoke-static {v0, p1}, Lcom/twitter/library/util/Util;->a(Ljava/util/HashMap;Ljava/io/ObjectOutput;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->boundingCoordinates:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->boundingCoordinates:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->country:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->countryCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->polyline:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->parent:Lcom/twitter/library/api/geo/TwitterPlace;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->attributes:Ljava/util/HashMap;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    return-void
.end method
