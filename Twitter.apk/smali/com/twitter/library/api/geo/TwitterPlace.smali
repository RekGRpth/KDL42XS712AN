.class public Lcom/twitter/library/api/geo/TwitterPlace;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Externalizable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final a:Ljava/util/HashMap;

.field private static final serialVersionUID:J = -0x1d8b403b1e255469L


# instance fields
.field public fullName:Ljava/lang/String;

.field public placeId:Ljava/lang/String;

.field public placeInfo:Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;

.field public placeType:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x5

    new-instance v0, Lcom/twitter/library/api/geo/d;

    invoke-direct {v0}, Lcom/twitter/library/api/geo/d;-><init>()V

    sput-object v0, Lcom/twitter/library/api/geo/TwitterPlace;->CREATOR:Landroid/os/Parcelable$Creator;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v3}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/twitter/library/api/geo/TwitterPlace;->a:Ljava/util/HashMap;

    sget-object v0, Lcom/twitter/library/api/geo/TwitterPlace;->a:Ljava/util/HashMap;

    const-string/jumbo v1, "poi"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/geo/TwitterPlace;->a:Ljava/util/HashMap;

    const-string/jumbo v1, "neighborhood"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/geo/TwitterPlace;->a:Ljava/util/HashMap;

    const-string/jumbo v1, "city"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/geo/TwitterPlace;->a:Ljava/util/HashMap;

    const-string/jumbo v1, "admin"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/api/geo/TwitterPlace;->a:Ljava/util/HashMap;

    const-string/jumbo v1, "country"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace;->placeId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/geo/TwitterPlace;->placeType:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace;->fullName:Ljava/lang/String;

    const-class v0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;

    iput-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace;->placeInfo:Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/api/geo/TwitterPlace;->placeId:Ljava/lang/String;

    iput p2, p0, Lcom/twitter/library/api/geo/TwitterPlace;->placeType:I

    iput-object p3, p0, Lcom/twitter/library/api/geo/TwitterPlace;->fullName:Ljava/lang/String;

    iput-object p4, p0, Lcom/twitter/library/api/geo/TwitterPlace;->placeInfo:Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/api/geo/TwitterPlace;Ljava/util/HashMap;)V
    .locals 8

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/api/geo/TwitterPlace;->placeId:Ljava/lang/String;

    iput p2, p0, Lcom/twitter/library/api/geo/TwitterPlace;->placeType:I

    iput-object p3, p0, Lcom/twitter/library/api/geo/TwitterPlace;->fullName:Ljava/lang/String;

    new-instance v0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;

    move-object v1, p4

    move-object v2, p5

    move-object v3, p6

    move-object v4, p7

    move-object/from16 v5, p8

    move-object/from16 v6, p9

    move-object/from16 v7, p10

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;-><init>(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/api/geo/TwitterPlace;Ljava/util/HashMap;)V

    iput-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace;->placeInfo:Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;

    return-void
.end method

.method public static a(Ljava/lang/String;)I
    .locals 1

    sget-object v0, Lcom/twitter/library/api/geo/TwitterPlace;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/library/api/geo/TwitterPlace;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Lcom/twitter/library/api/geo/TwitterPlace;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    check-cast p1, Lcom/twitter/library/api/geo/TwitterPlace;

    iget-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace;->placeId:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/library/api/geo/TwitterPlace;->placeId:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace;->placeId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace;->placeId:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/geo/TwitterPlace;->placeType:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace;->fullName:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;

    iput-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace;->placeInfo:Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace;->placeId:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget v0, p0, Lcom/twitter/library/api/geo/TwitterPlace;->placeType:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace;->fullName:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace;->placeInfo:Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace;->placeId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/twitter/library/api/geo/TwitterPlace;->placeType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace;->fullName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/api/geo/TwitterPlace;->placeInfo:Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
