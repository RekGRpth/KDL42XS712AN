.class public Lcom/twitter/library/api/search/j;
.super Lcom/twitter/library/api/l;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/library/api/l;-><init>()V

    return-void
.end method

.method private d(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/search/i;
    .locals 9

    const/4 v4, 0x0

    const-wide/16 v1, -0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_3

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v5, :cond_3

    sget-object v5, Lcom/twitter/library/api/search/k;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :pswitch_0
    move v0, v3

    move-object v3, v4

    :goto_1
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    move-object v6, v4

    move-object v4, v3

    move v3, v0

    move-object v0, v6

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "personalized"

    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    move-object v3, v4

    goto :goto_1

    :pswitch_2
    const-string/jumbo v0, "location"

    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v6, v0

    move-wide v7, v1

    move-wide v0, v7

    move-object v2, v4

    move-object v4, v6

    :goto_2
    if-eqz v4, :cond_4

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v5, :cond_4

    sget-object v5, Lcom/twitter/library/api/search/k;->a:[I

    invoke-virtual {v4}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v4

    aget v4, v5, v4

    packed-switch v4, :pswitch_data_1

    :cond_1
    :goto_3
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    goto :goto_2

    :pswitch_3
    const-string/jumbo v4, "name"

    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    :pswitch_4
    const-string/jumbo v4, "woeid"

    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v0

    goto :goto_3

    :pswitch_5
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :cond_2
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move v0, v3

    move-object v3, v4

    goto :goto_1

    :pswitch_6
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move v0, v3

    move-object v3, v4

    goto :goto_1

    :cond_3
    new-instance v0, Lcom/twitter/library/api/search/i;

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/twitter/library/api/search/i;-><init>(Ljava/lang/String;JZ)V

    return-object v0

    :cond_4
    move v6, v3

    move-object v3, v2

    move-wide v7, v0

    move-wide v1, v7

    move v0, v6

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method protected synthetic b(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/library/api/search/j;->c(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/search/h;

    move-result-object v0

    return-object v0
.end method

.method protected c(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/search/h;
    .locals 4

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v2, v1

    move-object v1, v0

    :goto_0
    if-eqz v2, :cond_3

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_3

    sget-object v3, Lcom/twitter/library/api/search/k;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    :goto_1
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    :pswitch_0
    const-string/jumbo v2, "data"

    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/api/search/j;->b()Lcom/twitter/library/util/w;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/twitter/library/api/ap;->i(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_1

    :cond_0
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_1
    const-string/jumbo v2, "metadata"

    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0, p1}, Lcom/twitter/library/api/search/j;->d(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/search/i;

    move-result-object v1

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_2
    move-object v1, v0

    :cond_3
    new-instance v2, Lcom/twitter/library/api/search/h;

    invoke-direct {v2, v1, v0}, Lcom/twitter/library/api/search/h;-><init>(Lcom/twitter/library/api/search/i;Ljava/util/ArrayList;)V

    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
