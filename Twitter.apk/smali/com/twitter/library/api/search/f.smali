.class public Lcom/twitter/library/api/search/f;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Ljava/util/ArrayList;

.field public final b:Ljava/lang/String;

.field public final c:Z

.field public d:I

.field public e:J

.field public f:Lcom/twitter/library/api/TwitterTopic;


# direct methods
.method public constructor <init>(Ljava/util/ArrayList;Ljava/lang/String;IZILcom/twitter/library/api/TwitterTopic;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/api/search/f;->a:Ljava/util/ArrayList;

    iput-object p2, p0, Lcom/twitter/library/api/search/f;->b:Ljava/lang/String;

    if-lez p3, :cond_0

    int-to-long v0, p3

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    :goto_0
    iput-wide v0, p0, Lcom/twitter/library/api/search/f;->e:J

    iput-boolean p4, p0, Lcom/twitter/library/api/search/f;->c:Z

    iput p5, p0, Lcom/twitter/library/api/search/f;->d:I

    iput-object p6, p0, Lcom/twitter/library/api/search/f;->f:Lcom/twitter/library/api/TwitterTopic;

    return-void

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method
