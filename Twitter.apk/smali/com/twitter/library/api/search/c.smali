.class public Lcom/twitter/library/api/search/c;
.super Lcom/twitter/library/api/account/e;
.source "Twttr"


# instance fields
.field private A:Ljava/lang/String;

.field private B:I

.field private C:I

.field private D:J

.field private E:Ljava/lang/String;

.field private F:Ljava/lang/String;

.field private G:Z

.field private final d:J

.field private final e:Ljava/lang/String;

.field private final f:I

.field private final g:Ljava/lang/String;

.field private final n:Ljava/lang/String;

.field private final o:I

.field private final p:I

.field private final q:Ljava/lang/String;

.field private final r:Lcom/twitter/library/api/TwitterUser;

.field private s:Lcom/twitter/library/api/ao;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:J

.field private y:J

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;)V
    .locals 1

    const-class v0, Lcom/twitter/library/api/search/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/account/e;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    iput-wide p3, p0, Lcom/twitter/library/api/search/c;->d:J

    iput-object p5, p0, Lcom/twitter/library/api/search/c;->e:Ljava/lang/String;

    iput p6, p0, Lcom/twitter/library/api/search/c;->f:I

    iput-object p7, p0, Lcom/twitter/library/api/search/c;->n:Ljava/lang/String;

    iput-object p8, p0, Lcom/twitter/library/api/search/c;->g:Ljava/lang/String;

    iput p9, p0, Lcom/twitter/library/api/search/c;->o:I

    iput p10, p0, Lcom/twitter/library/api/search/c;->p:I

    iput-object p11, p0, Lcom/twitter/library/api/search/c;->q:Ljava/lang/String;

    invoke-static {}, Lcom/twitter/library/api/search/c;->m()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/search/c;->t:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/search/c;->r:Lcom/twitter/library/api/TwitterUser;

    return-void
.end method

.method private static l()Z
    .locals 4

    const/4 v0, 0x1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->ar()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const-string/jumbo v1, "android_timeline_gallery_module_1938"

    invoke-static {v1}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v1, "android_timeline_gallery_module_1938"

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "timeline_gallery"

    aput-object v3, v0, v2

    invoke-static {v1, v0}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private static m()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/twitter/library/api/search/c;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "tweet,user_gallery,news,media_gallery,suggestion,event,tweet_gallery,follows_tweet_gallery,nearby_tweet_gallery,timeline"

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "tweet,user_gallery,news,media_gallery,suggestion,event,tweet_gallery,follows_tweet_gallery,nearby_tweet_gallery"

    goto :goto_0
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/e;Lcom/twitter/library/network/a;)Lcom/twitter/internal/network/HttpOperation;
    .locals 23

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/api/search/c;->k:Landroid/os/Bundle;

    const-string/jumbo v3, "lat"

    const-wide/high16 v4, 0x7ff8000000000000L    # NaN

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;D)D

    move-result-wide v16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/api/search/c;->k:Landroid/os/Bundle;

    const-string/jumbo v3, "long"

    const-wide/high16 v4, 0x7ff8000000000000L    # NaN

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;D)D

    move-result-wide v18

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/search/c;->w()Lcom/twitter/library/provider/az;

    move-result-object v20

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/search/c;->s()Lcom/twitter/library/service/p;

    move-result-object v2

    iget-wide v0, v2, Lcom/twitter/library/service/p;->c:J

    move-wide/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/api/search/c;->z:Ljava/lang/String;

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/api/search/c;->A:Ljava/lang/String;

    if-nez v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/search/c;->t()Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "taoc"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/api/search/c;->n:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "timeline"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/api/search/c;->n:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Lcom/twitter/library/api/search/TwitterSearchQuery;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/api/search/c;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/api/search/c;->e:Ljava/lang/String;

    const-wide/16 v5, 0x0

    const-wide/16 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {}, Lcom/twitter/library/util/CollectionsUtil;->a()Ljava/util/ArrayList;

    move-result-object v15

    invoke-direct/range {v2 .. v15}, Lcom/twitter/library/api/search/TwitterSearchQuery;-><init>(Ljava/lang/String;Ljava/lang/String;JJLjava/lang/Float;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/String;Ljava/util/ArrayList;Lcom/twitter/library/api/PromotedContent;Ljava/util/ArrayList;)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/api/search/c;->f:I

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/provider/az;->b(Lcom/twitter/library/api/search/TwitterSearchQuery;I)J

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/api/search/c;->m:Lcom/twitter/library/network/aa;

    iget-object v2, v2, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string/jumbo v5, "1.1"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "search"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string/jumbo v5, "universal"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v2, "q"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/api/search/c;->e:Ljava/lang/String;

    invoke-static {v9, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/api/search/c;->n:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    const-string/jumbo v2, "query_source"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/api/search/c;->n:Ljava/lang/String;

    invoke-static {v9, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-static/range {v16 .. v17}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static/range {v18 .. v19}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-nez v2, :cond_1

    const-string/jumbo v2, "near"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "%.7f"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static/range {v16 .. v17}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x2c

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "%.7f"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static/range {v18 .. v19}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v9, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/api/search/c;->u:Ljava/lang/String;

    if-eqz v2, :cond_2

    const-string/jumbo v2, "result_type"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/api/search/c;->u:Ljava/lang/String;

    invoke-static {v9, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/api/search/c;->v:Ljava/lang/String;

    if-eqz v2, :cond_3

    const-string/jumbo v2, "filter"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/api/search/c;->v:Ljava/lang/String;

    invoke-static {v9, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/api/search/c;->w:Ljava/lang/String;

    if-eqz v2, :cond_4

    const-string/jumbo v2, "timeline_type"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/api/search/c;->w:Ljava/lang/String;

    invoke-static {v9, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    const-string/jumbo v2, "modules"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/api/search/c;->t:Ljava/lang/String;

    invoke-static {v9, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/api/search/c;->q:Ljava/lang/String;

    if-eqz v2, :cond_5

    const-string/jumbo v2, "experiments"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/api/search/c;->q:Ljava/lang/String;

    invoke-static {v9, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/twitter/library/api/search/c;->x:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_8

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/twitter/library/api/search/c;->x:J

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/twitter/library/api/search/c;->y:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_8

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_6

    const-string/jumbo v2, "since_time"

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/twitter/library/api/search/c;->x:J

    invoke-static {v9, v2, v3, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    const-string/jumbo v2, "until_time"

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/twitter/library/api/search/c;->y:J

    invoke-static {v9, v2, v3, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/api/search/c;->z:Ljava/lang/String;

    if-eqz v2, :cond_9

    const-string/jumbo v2, "cluster_id"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/api/search/c;->z:Ljava/lang/String;

    invoke-static {v9, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/library/api/search/c;->p:I

    if-eqz v2, :cond_b

    const/4 v3, 0x7

    const/16 v4, 0xd

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/twitter/library/api/search/c;->d:J

    move-object/from16 v2, v20

    move-wide/from16 v5, v21

    invoke-virtual/range {v2 .. v8}, Lcom/twitter/library/provider/az;->a(IIJJ)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_a

    const/4 v2, 0x0

    :goto_3
    return-object v2

    :cond_7
    const-string/jumbo v2, "query_source"

    const-string/jumbo v3, "unknown"

    invoke-static {v9, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    const/4 v2, 0x0

    goto :goto_1

    :cond_9
    const-string/jumbo v2, "get_clusters"

    const/4 v3, 0x1

    invoke-static {v9, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    goto :goto_2

    :cond_a
    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/api/search/c;->p:I

    packed-switch v3, :pswitch_data_0

    :cond_b
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/api/search/c;->A:Ljava/lang/String;

    if-eqz v2, :cond_c

    const-string/jumbo v2, "event_id"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/api/search/c;->A:Ljava/lang/String;

    invoke-static {v9, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/api/search/c;->k:Landroid/os/Bundle;

    const-string/jumbo v3, "lang"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_d

    const-string/jumbo v3, "ui_lang"

    invoke-static {v9, v3, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    const-string/jumbo v2, "pc"

    const-string/jumbo v3, "true"

    invoke-static {v9, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "include_media_features"

    const/4 v3, 0x1

    invoke-static {v9, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v2, "include_cards"

    const/4 v3, 0x1

    invoke-static {v9, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v2, "timezone"

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-static {v9, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/api/search/c;->m:Lcom/twitter/library/network/aa;

    invoke-virtual {v2, v9}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/api/search/c;->k:Landroid/os/Bundle;

    invoke-static {v2}, Ljs;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_e

    const/4 v3, 0x1

    invoke-static {v9, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/search/c;->s()Lcom/twitter/library/service/p;

    move-result-object v2

    const/16 v3, 0x18

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/api/search/c;->r:Lcom/twitter/library/api/TwitterUser;

    new-instance v5, Lcom/twitter/library/util/f;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/library/api/search/c;->l:Landroid/content/Context;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-wide/from16 v0, v21

    invoke-direct {v5, v6, v0, v1, v7}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v3, v4, v5}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/api/TwitterUser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/twitter/library/api/search/c;->s:Lcom/twitter/library/api/ao;

    new-instance v3, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/api/search/c;->l:Landroid/content/Context;

    invoke-direct {v3, v4, v9}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v4, v2, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v3, v4, v5}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/search/c;->t()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/twitter/library/network/d;->a(Z)Lcom/twitter/library/network/d;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/api/search/c;->s:Lcom/twitter/library/api/ao;

    invoke-virtual {v2, v3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_3

    :pswitch_0
    const-string/jumbo v3, "next_cursor"

    invoke-static {v9, v3, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    :pswitch_1
    const-string/jumbo v3, "prev_cursor"

    invoke-static {v9, v3, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(IZZZ)Lcom/twitter/library/api/search/c;
    .locals 1

    packed-switch p1, :pswitch_data_0

    :goto_0
    if-eqz p2, :cond_1

    const-string/jumbo v0, "follows"

    iput-object v0, p0, Lcom/twitter/library/api/search/c;->u:Ljava/lang/String;

    :cond_0
    :goto_1
    return-object p0

    :pswitch_0
    const-string/jumbo v0, "tweet"

    iput-object v0, p0, Lcom/twitter/library/api/search/c;->t:Ljava/lang/String;

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "user"

    iput-object v0, p0, Lcom/twitter/library/api/search/c;->t:Ljava/lang/String;

    goto :goto_0

    :pswitch_2
    const-string/jumbo v0, "tweet"

    iput-object v0, p0, Lcom/twitter/library/api/search/c;->t:Ljava/lang/String;

    const-string/jumbo v0, "images"

    iput-object v0, p0, Lcom/twitter/library/api/search/c;->v:Ljava/lang/String;

    goto :goto_0

    :pswitch_3
    const-string/jumbo v0, "tweet"

    iput-object v0, p0, Lcom/twitter/library/api/search/c;->t:Ljava/lang/String;

    const-string/jumbo v0, "videos"

    iput-object v0, p0, Lcom/twitter/library/api/search/c;->v:Ljava/lang/String;

    goto :goto_0

    :pswitch_4
    const-string/jumbo v0, "tweet"

    iput-object v0, p0, Lcom/twitter/library/api/search/c;->t:Ljava/lang/String;

    const-string/jumbo v0, "vine"

    iput-object v0, p0, Lcom/twitter/library/api/search/c;->v:Ljava/lang/String;

    goto :goto_0

    :pswitch_5
    const-string/jumbo v0, "tweet"

    iput-object v0, p0, Lcom/twitter/library/api/search/c;->t:Ljava/lang/String;

    const-string/jumbo v0, "news"

    iput-object v0, p0, Lcom/twitter/library/api/search/c;->v:Ljava/lang/String;

    goto :goto_0

    :pswitch_6
    const-string/jumbo v0, "timeline"

    iput-object v0, p0, Lcom/twitter/library/api/search/c;->t:Ljava/lang/String;

    const-string/jumbo v0, "curated"

    iput-object v0, p0, Lcom/twitter/library/api/search/c;->w:Ljava/lang/String;

    goto :goto_0

    :pswitch_7
    const-string/jumbo v0, "timeline"

    iput-object v0, p0, Lcom/twitter/library/api/search/c;->t:Ljava/lang/String;

    const-string/jumbo v0, "list"

    iput-object v0, p0, Lcom/twitter/library/api/search/c;->w:Ljava/lang/String;

    goto :goto_0

    :pswitch_8
    const-string/jumbo v0, "tweet"

    iput-object v0, p0, Lcom/twitter/library/api/search/c;->t:Ljava/lang/String;

    const-string/jumbo v0, "media"

    iput-object v0, p0, Lcom/twitter/library/api/search/c;->v:Ljava/lang/String;

    goto :goto_0

    :cond_1
    if-eqz p3, :cond_2

    const-string/jumbo v0, "recent"

    iput-object v0, p0, Lcom/twitter/library/api/search/c;->u:Ljava/lang/String;

    goto :goto_1

    :cond_2
    if-eqz p4, :cond_0

    const-string/jumbo v0, "realtime"

    iput-object v0, p0, Lcom/twitter/library/api/search/c;->u:Ljava/lang/String;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public a(JJ)Lcom/twitter/library/api/search/c;
    .locals 0

    iput-wide p1, p0, Lcom/twitter/library/api/search/c;->x:J

    iput-wide p3, p0, Lcom/twitter/library/api/search/c;->y:J

    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/api/search/c;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/search/c;->A:Ljava/lang/String;

    iput-object p2, p0, Lcom/twitter/library/api/search/c;->z:Ljava/lang/String;

    return-object p0
.end method

.method protected a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)V
    .locals 19

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/internal/network/HttpOperation;->k()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/search/c;->w()Lcom/twitter/library/provider/az;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/api/search/c;->s:Lcom/twitter/library/api/ao;

    invoke-virtual {v3}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/twitter/library/api/search/f;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/api/search/c;->p:I

    packed-switch v3, :pswitch_data_0

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/twitter/library/api/search/c;->d:J

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/provider/az;->e(J)I

    const/4 v13, 0x0

    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/search/c;->s()Lcom/twitter/library/service/p;

    move-result-object v3

    iget-wide v5, v3, Lcom/twitter/library/service/p;->c:J

    iget-object v3, v10, Lcom/twitter/library/api/search/f;->b:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v3, 0x7

    const/16 v4, 0xd

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/twitter/library/api/search/c;->d:J

    iget-object v9, v10, Lcom/twitter/library/api/search/f;->b:Ljava/lang/String;

    invoke-virtual/range {v2 .. v9}, Lcom/twitter/library/provider/az;->a(IIJJLjava/lang/String;)V

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/search/c;->t()Z

    move-result v16

    const/4 v3, 0x0

    iget-object v4, v10, Lcom/twitter/library/api/search/f;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v2, 0x0

    move-object/from16 v18, v3

    move v3, v2

    move-object/from16 v2, v18

    :goto_1
    move-object/from16 v0, p0

    iput v3, v0, Lcom/twitter/library/api/search/c;->B:I

    iget v4, v10, Lcom/twitter/library/api/search/f;->d:I

    sub-int/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Lcom/twitter/library/api/search/c;->C:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/api/search/c;->C:I

    if-gez v3, :cond_1

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/twitter/library/api/search/c;->C:I

    :cond_1
    iget-wide v3, v10, Lcom/twitter/library/api/search/f;->e:J

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/twitter/library/api/search/c;->D:J

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/library/api/search/c;->E:Ljava/lang/String;

    :cond_2
    iget-object v2, v10, Lcom/twitter/library/api/search/f;->f:Lcom/twitter/library/api/TwitterTopic;

    if-eqz v2, :cond_3

    iget-object v2, v10, Lcom/twitter/library/api/search/f;->f:Lcom/twitter/library/api/TwitterTopic;

    const-class v3, Lcom/twitter/library/api/TwitterTopic$SportsEvent;

    invoke-virtual {v2, v3}, Lcom/twitter/library/api/TwitterTopic;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/TwitterTopic$SportsEvent;

    if-eqz v2, :cond_3

    iget-object v2, v2, Lcom/twitter/library/api/TwitterTopic$SportsEvent;->summary:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/library/api/search/c;->F:Ljava/lang/String;

    :cond_3
    iget-boolean v2, v10, Lcom/twitter/library/api/search/f;->c:Z

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/twitter/library/api/search/c;->G:Z

    :cond_4
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void

    :pswitch_0
    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/api/search/c;->o:I

    iget-object v4, v10, Lcom/twitter/library/api/search/f;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    sub-int v13, v3, v4

    goto/16 :goto_0

    :pswitch_1
    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/api/search/c;->o:I

    add-int/lit8 v13, v3, 0x1

    goto/16 :goto_0

    :cond_5
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/twitter/library/api/search/c;->d:J

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/api/search/c;->p:I

    const/4 v7, 0x1

    if-ne v4, v7, :cond_8

    const/4 v14, 0x1

    :goto_2
    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/api/search/c;->p:I

    if-nez v4, :cond_9

    const/4 v15, 0x1

    :goto_3
    if-eqz v16, :cond_6

    iget-object v4, v10, Lcom/twitter/library/api/search/f;->f:Lcom/twitter/library/api/TwitterTopic;

    if-eqz v4, :cond_a

    :cond_6
    const/16 v17, 0x1

    :goto_4
    move-object v7, v2

    move-wide v11, v5

    invoke-virtual/range {v7 .. v17}, Lcom/twitter/library/provider/az;->a(JLcom/twitter/library/api/search/f;JIZZZZ)I

    move-result v4

    iget-object v2, v10, Lcom/twitter/library/api/search/f;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_7
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/search/g;

    iget-object v6, v2, Lcom/twitter/library/api/search/g;->m:Ljava/lang/String;

    if-eqz v6, :cond_7

    iget-object v2, v2, Lcom/twitter/library/api/search/g;->m:Ljava/lang/String;

    move v3, v4

    goto/16 :goto_1

    :cond_8
    const/4 v14, 0x0

    goto :goto_2

    :cond_9
    const/4 v15, 0x0

    goto :goto_3

    :cond_a
    const/16 v17, 0x0

    goto :goto_4

    :cond_b
    move-object v2, v3

    move v3, v4

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/search/c;->w:Ljava/lang/String;

    return-void
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/api/search/c;->B:I

    return v0
.end method

.method public f()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/api/search/c;->C:I

    return v0
.end method

.method public g()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/api/search/c;->D:J

    return-wide v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/search/c;->E:Ljava/lang/String;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/search/c;->A:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/search/c;->F:Ljava/lang/String;

    return-object v0
.end method

.method public k()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/api/search/c;->G:Z

    return v0
.end method
