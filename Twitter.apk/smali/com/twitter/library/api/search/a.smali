.class public Lcom/twitter/library/api/search/a;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# instance fields
.field private final d:Lcom/twitter/library/api/TwitterUser;

.field private final e:[J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/p;Lcom/twitter/library/api/TwitterUser;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/library/api/search/a;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;Lcom/twitter/library/api/TwitterUser;[J)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/p;Lcom/twitter/library/api/TwitterUser;[J)V
    .locals 1

    const-class v0, Lcom/twitter/library/api/search/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/p;)V

    iput-object p3, p0, Lcom/twitter/library/api/search/a;->d:Lcom/twitter/library/api/TwitterUser;

    iput-object p4, p0, Lcom/twitter/library/api/search/a;->e:[J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/p;[J)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/twitter/library/api/search/a;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;Lcom/twitter/library/api/TwitterUser;[J)V

    return-void
.end method


# virtual methods
.method protected b(Lcom/twitter/library/service/e;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/library/api/search/a;->d:Lcom/twitter/library/api/TwitterUser;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/library/api/search/a;->w()Lcom/twitter/library/provider/az;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/library/api/search/a;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    iget-wide v1, v1, Lcom/twitter/library/service/p;->c:J

    iget-object v3, p0, Lcom/twitter/library/api/search/a;->d:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/provider/az;->a(JLcom/twitter/library/api/TwitterUser;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/api/search/a;->e:[J

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/api/search/a;->w()Lcom/twitter/library/provider/az;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/library/api/search/a;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    iget-wide v1, v1, Lcom/twitter/library/service/p;->c:J

    iget-object v3, p0, Lcom/twitter/library/api/search/a;->e:[J

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/provider/az;->a(J[J)V

    goto :goto_0
.end method
