.class public Lcom/twitter/library/api/conversations/ag;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# instance fields
.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V
    .locals 1

    const-class v0, Lcom/twitter/library/api/conversations/ag;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    iput-object p3, p0, Lcom/twitter/library/api/conversations/ag;->d:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/e;)Lcom/twitter/internal/network/HttpOperation;
    .locals 6

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/ag;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    const-string/jumbo v1, "https://api-staging1.smf1.twitter.com"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string/jumbo v4, "1.1"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "dm"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "destroy"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v4, "id"

    iget-object v5, p0, Lcom/twitter/library/api/conversations/ag;->d:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lcom/twitter/library/network/d;

    iget-object v4, p0, Lcom/twitter/library/api/conversations/ag;->l:Landroid/content/Context;

    invoke-direct {v3, v4, v1}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v4, v0, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v3, v4, v5}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v1

    sget-object v3, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v1, v3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v1

    new-instance v3, Lcom/twitter/library/network/n;

    iget-object v0, v0, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v3, v0}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v1, v3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)V
    .locals 2

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/ag;->w()Lcom/twitter/library/provider/az;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/conversations/ag;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/az;->h(Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-virtual {p2, p1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v0

    iget v0, v0, Lcom/twitter/internal/network/k;->a:I

    const/16 v1, 0x194

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/ag;->w()Lcom/twitter/library/provider/az;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/conversations/ag;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/az;->h(Ljava/lang/String;)V

    goto :goto_0
.end method
