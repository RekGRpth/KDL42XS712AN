.class public Lcom/twitter/library/api/conversations/ac;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# instance fields
.field private d:Lcom/twitter/library/api/ao;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    const-class v0, Lcom/twitter/library/api/conversations/ac;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const-string/jumbo v0, "https://api-staging1.smf1.twitter.com"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "1.1"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "dm"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "user_updates"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    if-eqz p0, :cond_0

    const-string/jumbo v1, "since_id"

    invoke-static {v0, v1, p0}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Lcom/twitter/library/client/f;

    invoke-direct {v0, p0, p1}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v1, "most_recent_event_id"

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/f;->c(Ljava/lang/String;)Lcom/twitter/library/client/f;

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Lcom/twitter/library/client/f;

    iget-object v1, p0, Lcom/twitter/library/api/conversations/ac;->l:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/ac;->s()Lcom/twitter/library/service/p;

    move-result-object v2

    iget-object v2, v2, Lcom/twitter/library/service/p;->e:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v1, "most_recent_event_id"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/client/f;

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V

    return-void
.end method

.method private e()Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/f;

    iget-object v1, p0, Lcom/twitter/library/api/conversations/ac;->l:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/ac;->s()Lcom/twitter/library/service/p;

    move-result-object v2

    iget-object v2, v2, Lcom/twitter/library/service/p;->e:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const-string/jumbo v1, "most_recent_event_id"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/e;)Lcom/twitter/internal/network/HttpOperation;
    .locals 7

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/ac;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    iget-wide v1, v0, Lcom/twitter/library/service/p;->c:J

    invoke-direct {p0}, Lcom/twitter/library/api/conversations/ac;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/library/api/conversations/ac;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x4b

    new-instance v5, Lcom/twitter/library/util/f;

    iget-object v6, p0, Lcom/twitter/library/api/conversations/ac;->l:Landroid/content/Context;

    invoke-direct {v5, v6, v1, v2, v3}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v4, v5}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/library/api/conversations/ac;->d:Lcom/twitter/library/api/ao;

    new-instance v4, Lcom/twitter/library/network/d;

    iget-object v5, p0, Lcom/twitter/library/api/conversations/ac;->l:Landroid/content/Context;

    invoke-direct {v4, v5, v3}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v1, v2}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/network/n;

    iget-object v0, v0, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v2, v0}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v1, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/conversations/ac;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)V
    .locals 3

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/conversations/ac;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/conversations/c;

    if-eqz v0, :cond_0

    sget-object v1, Lcom/twitter/library/api/conversations/ad;->a:[I

    iget-object v2, v0, Lcom/twitter/library/api/conversations/c;->a:Lcom/twitter/library/api/conversations/DMResponseSource;

    invoke-virtual {v2}, Lcom/twitter/library/api/conversations/DMResponseSource;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    iget-object v1, v0, Lcom/twitter/library/api/conversations/c;->f:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/twitter/library/api/conversations/c;->f:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/twitter/library/api/conversations/ac;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p2, p1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/ac;->w()Lcom/twitter/library/provider/az;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/provider/az;->a(Lcom/twitter/library/api/conversations/c;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/ac;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/library/service/p;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/ac;->w()Lcom/twitter/library/provider/az;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/twitter/library/provider/az;->a(Lcom/twitter/library/api/conversations/c;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
