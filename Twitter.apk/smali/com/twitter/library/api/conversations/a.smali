.class public Lcom/twitter/library/api/conversations/a;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Lcom/twitter/library/api/conversations/f;

.field public final b:Ljava/util/List;


# direct methods
.method protected constructor <init>(Lcom/twitter/library/api/conversations/f;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/api/conversations/a;->a:Lcom/twitter/library/api/conversations/f;

    iput-object p2, p0, Lcom/twitter/library/api/conversations/a;->b:Ljava/util/List;

    return-void
.end method

.method public static a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/conversations/a;
    .locals 6

    const/4 v3, 0x0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p0, :cond_6

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->c()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v4, v1

    move-object v2, v3

    move-object v1, v3

    :goto_0
    if-eqz v4, :cond_4

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v5, :cond_4

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v4, v5, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    :goto_1
    if-eqz v4, :cond_3

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v5, :cond_3

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->f:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v4, v5, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v2

    :cond_0
    :goto_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    goto :goto_1

    :cond_1
    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v4, v5, :cond_0

    const-string/jumbo v4, "conversation"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {p0, v3}, Lcom/twitter/library/api/conversations/f;->a(Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/String;)Lcom/twitter/library/api/conversations/f;

    move-result-object v1

    goto :goto_2

    :cond_2
    const-string/jumbo v4, "users"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {p0}, Lcom/twitter/library/api/conversations/c;->b(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/List;

    move-result-object v0

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    goto :goto_0

    :cond_4
    move-object v2, v1

    move-object v1, v0

    :goto_3
    if-eqz v2, :cond_5

    new-instance v0, Lcom/twitter/library/api/conversations/a;

    invoke-direct {v0, v2, v1}, Lcom/twitter/library/api/conversations/a;-><init>(Lcom/twitter/library/api/conversations/f;Ljava/util/List;)V

    :goto_4
    return-object v0

    :cond_5
    move-object v0, v3

    goto :goto_4

    :cond_6
    move-object v1, v0

    move-object v2, v3

    goto :goto_3
.end method
