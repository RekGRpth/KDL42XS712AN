.class public Lcom/twitter/library/api/conversations/t;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Z

.field public final b:Z

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;


# direct methods
.method protected constructor <init>(ZZLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/twitter/library/api/conversations/t;->a:Z

    iput-boolean p2, p0, Lcom/twitter/library/api/conversations/t;->b:Z

    iput-object p3, p0, Lcom/twitter/library/api/conversations/t;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/twitter/library/api/conversations/t;->d:Ljava/lang/String;

    return-void
.end method

.method protected static a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/conversations/t;
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v4, v0

    move v3, v2

    move-object v0, v1

    :goto_0
    if-eqz v4, :cond_3

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v4, v5, :cond_3

    sget-object v5, Lcom/twitter/library/api/conversations/u;->a:[I

    invoke-virtual {v4}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v4

    aget v4, v5, v4

    packed-switch v4, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "can_dm"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->k()Z

    move-result v3

    goto :goto_1

    :cond_1
    const-string/jumbo v5, "full_permission"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->k()Z

    move-result v2

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "can_dm_failure"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_2
    const-string/jumbo v5, "full_permission_failure"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_3
    new-instance v4, Lcom/twitter/library/api/conversations/t;

    invoke-direct {v4, v3, v2, v1, v0}, Lcom/twitter/library/api/conversations/t;-><init>(ZZLjava/lang/String;Ljava/lang/String;)V

    return-object v4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
