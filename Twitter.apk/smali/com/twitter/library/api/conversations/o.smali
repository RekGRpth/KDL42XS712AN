.class public Lcom/twitter/library/api/conversations/o;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x17

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "entry_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "conversation_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "type"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "user_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "s_name"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "s_username"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "s_profile_image_url"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "created"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "data"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "username_1"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "name_1"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "avatar_1"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "username_2"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "name_2"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "avatar_2"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "username_3"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "name_3"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "avatar_3"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "username_4"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "name_4"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "avatar_4"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/library/api/conversations/o;->a:[Ljava/lang/String;

    return-void
.end method
