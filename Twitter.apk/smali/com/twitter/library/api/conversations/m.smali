.class public Lcom/twitter/library/api/conversations/m;
.super Lcom/twitter/library/api/conversations/e;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/api/conversations/s;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V
    .locals 1

    const-class v0, Lcom/twitter/library/api/conversations/m;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/twitter/library/api/conversations/e;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected e()Ljava/lang/String;
    .locals 4

    const-string/jumbo v0, "https://api-staging1.smf1.twitter.com"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "1.1"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "dm"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "conversation"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/twitter/library/api/conversations/m;->d:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/m;->w()Lcom/twitter/library/provider/az;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/api/conversations/m;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/twitter/library/provider/az;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string/jumbo v2, "max_id"

    invoke-static {v0, v2, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Z
    .locals 2

    sget-object v0, Lcom/twitter/library/api/conversations/DMPaginationStatus;->b:Lcom/twitter/library/api/conversations/DMPaginationStatus;

    iget-object v1, p0, Lcom/twitter/library/api/conversations/m;->e:Lcom/twitter/library/api/conversations/DMPaginationStatus;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
