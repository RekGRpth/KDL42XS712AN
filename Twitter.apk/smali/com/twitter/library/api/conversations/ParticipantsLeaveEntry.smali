.class public Lcom/twitter/library/api/conversations/ParticipantsLeaveEntry;
.super Lcom/twitter/library/api/conversations/ParticipantsEntry;
.source "Twttr"


# static fields
.field private static final serialVersionUID:J = 0x43b28537a8250076L


# direct methods
.method public constructor <init>(Lcom/twitter/library/api/conversations/am;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/library/api/conversations/ParticipantsEntry;-><init>(Lcom/twitter/library/api/conversations/am;)V

    return-void
.end method

.method public static c(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/conversations/ParticipantsLeaveEntry;
    .locals 2

    new-instance v0, Lcom/twitter/library/api/conversations/ParticipantsLeaveEntry;

    invoke-static {p0}, Lcom/twitter/library/api/conversations/ParticipantsEntry;->b(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/conversations/am;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/library/api/conversations/ParticipantsLeaveEntry;-><init>(Lcom/twitter/library/api/conversations/am;)V

    return-object v0
.end method


# virtual methods
.method public a(Landroid/database/sqlite/SQLiteDatabase;J)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/library/api/conversations/ParticipantsLeaveEntry;->participants:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/library/api/conversations/ParticipantsLeaveEntry;->conversationId:Ljava/lang/String;

    aput-object v5, v0, v4

    const/4 v4, 0x1

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    const-string/jumbo v2, "conversation_participants"

    const-string/jumbo v3, "conversation_id=? AND user_id=?"

    invoke-virtual {p1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/library/api/conversations/ParticipantsEntry;->a(Landroid/database/sqlite/SQLiteDatabase;J)V

    return-void
.end method

.method protected d()I
    .locals 1

    const/16 v0, 0xb

    return v0
.end method
