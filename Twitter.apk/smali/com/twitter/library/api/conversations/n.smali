.class public Lcom/twitter/library/api/conversations/n;
.super Lcom/twitter/library/service/o;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/api/conversations/s;


# instance fields
.field private final d:Lcom/twitter/library/api/ao;

.field private e:Lcom/twitter/library/api/conversations/DMPaginationStatus;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    const-class v0, Lcom/twitter/library/api/conversations/n;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    const/16 v0, 0x47

    invoke-static {v0}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/conversations/n;->d:Lcom/twitter/library/api/ao;

    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const-string/jumbo v0, "https://api-staging1.smf1.twitter.com"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "1.1"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "dm"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "user_inbox"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    if-eqz p0, :cond_0

    const-string/jumbo v1, "max_id"

    invoke-static {v0, v1, p0}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private e()V
    .locals 3

    new-instance v0, Lcom/twitter/library/client/f;

    iget-object v1, p0, Lcom/twitter/library/api/conversations/n;->l:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/n;->s()Lcom/twitter/library/service/p;

    move-result-object v2

    iget-object v2, v2, Lcom/twitter/library/service/p;->e:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v1, "more_conversations_available"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;Z)Lcom/twitter/library/client/f;

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/e;)Lcom/twitter/internal/network/HttpOperation;
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/n;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/n;->w()Lcom/twitter/library/provider/az;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/provider/az;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/api/conversations/n;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/network/d;

    iget-object v3, p0, Lcom/twitter/library/api/conversations/n;->l:Landroid/content/Context;

    invoke-direct {v2, v3, v1}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/twitter/library/network/n;

    iget-object v0, v0, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v1, v0}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v2, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/conversations/n;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)V
    .locals 2

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/conversations/n;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/conversations/c;

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/n;->w()Lcom/twitter/library/provider/az;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/provider/az;->a(Lcom/twitter/library/api/conversations/c;)V

    iget-object v0, v0, Lcom/twitter/library/api/conversations/c;->e:Lcom/twitter/library/api/conversations/DMPaginationStatus;

    iput-object v0, p0, Lcom/twitter/library/api/conversations/n;->e:Lcom/twitter/library/api/conversations/DMPaginationStatus;

    sget-object v0, Lcom/twitter/library/api/conversations/DMPaginationStatus;->a:Lcom/twitter/library/api/conversations/DMPaginationStatus;

    iget-object v1, p0, Lcom/twitter/library/api/conversations/n;->e:Lcom/twitter/library/api/conversations/DMPaginationStatus;

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/twitter/library/api/conversations/n;->e()V

    :cond_0
    invoke-virtual {p2, p1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void
.end method

.method protected b(Lcom/twitter/library/service/e;)V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/n;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Lcom/twitter/library/service/o;->b(Lcom/twitter/library/service/e;)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/twitter/library/api/conversations/DMPaginationStatus;->a:Lcom/twitter/library/api/conversations/DMPaginationStatus;

    iput-object v0, p0, Lcom/twitter/library/api/conversations/n;->e:Lcom/twitter/library/api/conversations/DMPaginationStatus;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/twitter/library/service/e;->a(Z)V

    goto :goto_0
.end method

.method public f()Z
    .locals 4

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/twitter/library/api/conversations/n;->e:Lcom/twitter/library/api/conversations/DMPaginationStatus;

    if-nez v1, :cond_1

    new-instance v1, Lcom/twitter/library/client/f;

    iget-object v2, p0, Lcom/twitter/library/api/conversations/n;->l:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/n;->s()Lcom/twitter/library/service/p;

    move-result-object v3

    iget-object v3, v3, Lcom/twitter/library/service/p;->e:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const-string/jumbo v2, "more_conversations_available"

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;Z)Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v1, Lcom/twitter/library/api/conversations/DMPaginationStatus;->b:Lcom/twitter/library/api/conversations/DMPaginationStatus;

    iget-object v2, p0, Lcom/twitter/library/api/conversations/n;->e:Lcom/twitter/library/api/conversations/DMPaginationStatus;

    if-eq v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method
