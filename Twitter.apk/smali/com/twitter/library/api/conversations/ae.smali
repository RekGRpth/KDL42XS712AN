.class public Lcom/twitter/library/api/conversations/ae;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Ljava/lang/StringBuilder;Lcom/twitter/library/api/conversations/DMMessage;)V
    .locals 5

    iget-object v0, p1, Lcom/twitter/library/api/conversations/DMMessage;->attachment:Lcom/twitter/library/api/conversations/k;

    check-cast v0, Lcom/twitter/library/api/conversations/DMPhoto;

    iget-object v1, p1, Lcom/twitter/library/api/conversations/DMMessage;->entities:Lcom/twitter/library/api/TweetEntities;

    iget-object v1, v1, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/UrlEntity;

    iget v3, v1, Lcom/twitter/library/api/UrlEntity;->start:I

    iget v4, v0, Lcom/twitter/library/api/conversations/DMPhoto;->start:I

    if-ne v3, v4, :cond_0

    iget v3, v1, Lcom/twitter/library/api/UrlEntity;->end:I

    iget v4, v0, Lcom/twitter/library/api/conversations/DMPhoto;->end:I

    if-ne v3, v4, :cond_0

    iget-object v0, p1, Lcom/twitter/library/api/conversations/DMMessage;->entities:Lcom/twitter/library/api/TweetEntities;

    iget-object v0, v0, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget v0, v1, Lcom/twitter/library/api/UrlEntity;->start:I

    iget v1, v1, Lcom/twitter/library/api/UrlEntity;->end:I

    invoke-virtual {p0, v0, v1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    :cond_1
    return-void
.end method

.method public static a()Z
    .locals 1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->ac()Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 3

    const/4 v0, 0x0

    const-string/jumbo v1, "dms"

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "dm_event_api"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->ab()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0}, Lcom/twitter/library/api/conversations/ae;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 3

    const/4 v0, 0x0

    const-string/jumbo v1, "dms"

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "group_dms"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->ad()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method
