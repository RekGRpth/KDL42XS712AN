.class public Lcom/twitter/library/api/conversations/i;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field private final e:J

.field private final f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/twitter/library/api/conversations/i;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/api/conversations/i;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JZ)V
    .locals 1

    sget-object v0, Lcom/twitter/library/api/conversations/i;->d:Ljava/lang/String;

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    iput-wide p3, p0, Lcom/twitter/library/api/conversations/i;->e:J

    iput-boolean p5, p0, Lcom/twitter/library/api/conversations/i;->f:Z

    return-void
.end method

.method private static a(JZ)Ljava/lang/String;
    .locals 5

    if-eqz p2, :cond_0

    const-string/jumbo v0, "accept"

    :goto_0
    const-string/jumbo v1, "https://api-staging1.smf1.twitter.com"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string/jumbo v4, "1.1"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "dm"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v0, "ignore"

    goto :goto_0
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/e;)Lcom/twitter/internal/network/HttpOperation;
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/i;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/library/api/conversations/i;->e:J

    iget-boolean v3, p0, Lcom/twitter/library/api/conversations/i;->f:Z

    invoke-static {v1, v2, v3}, Lcom/twitter/library/api/conversations/i;->a(JZ)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/network/d;

    iget-object v3, p0, Lcom/twitter/library/api/conversations/i;->l:Landroid/content/Context;

    invoke-direct {v2, v3, v1}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    sget-object v1, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v2, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v1

    iget-wide v2, v0, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/network/n;

    iget-object v0, v0, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v2, v0}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v1, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)V
    .locals 3

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/i;->w()Lcom/twitter/library/provider/az;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/library/api/conversations/i;->e:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/provider/az;->j(J)V

    :cond_0
    invoke-virtual {p2, p1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void
.end method
