.class public Lcom/twitter/library/api/conversations/ai;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# instance fields
.field private final d:Ljava/lang/String;

.field private final e:[J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;[J)V
    .locals 1

    const-class v0, Lcom/twitter/library/api/conversations/ai;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    iput-object p3, p0, Lcom/twitter/library/api/conversations/ai;->d:Ljava/lang/String;

    iput-object p4, p0, Lcom/twitter/library/api/conversations/ai;->e:[J

    return-void
.end method


# virtual methods
.method protected b(Lcom/twitter/library/service/e;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/api/conversations/ai;->e:[J

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/conversations/ai;->e:[J

    array-length v0, v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/ai;->w()Lcom/twitter/library/provider/az;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/conversations/ai;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/library/api/conversations/ai;->e:[J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/provider/az;->a(Ljava/lang/String;[J)V

    :cond_0
    return-void
.end method
