.class public Lcom/twitter/library/api/conversations/z;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# instance fields
.field private final d:Lcom/twitter/library/api/ao;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    const-class v0, Lcom/twitter/library/api/conversations/z;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    const/16 v0, 0x4d

    invoke-static {v0}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/conversations/z;->d:Lcom/twitter/library/api/ao;

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/e;)Lcom/twitter/internal/network/HttpOperation;
    .locals 5

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/z;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    const-string/jumbo v1, "https://api-staging1.smf1.twitter.com"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string/jumbo v4, "1.1"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "dm"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "requests"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/network/d;

    iget-object v3, p0, Lcom/twitter/library/api/conversations/z;->l:Landroid/content/Context;

    invoke-direct {v2, v3, v1}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/twitter/library/network/n;

    iget-object v0, v0, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v1, v0}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v2, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/conversations/z;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)V
    .locals 2

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/z;->w()Lcom/twitter/library/provider/az;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/library/api/conversations/z;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/conversations/c;

    invoke-virtual {v1, v0}, Lcom/twitter/library/provider/az;->a(Lcom/twitter/library/api/conversations/c;)V

    :cond_0
    invoke-virtual {p2, p1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void
.end method
