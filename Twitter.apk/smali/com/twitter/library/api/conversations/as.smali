.class public abstract Lcom/twitter/library/api/conversations/as;
.super Lcom/twitter/library/api/upload/ad;
.source "Twttr"


# instance fields
.field private d:Z

.field private n:Ljava/lang/String;

.field private o:[I

.field private p:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/api/upload/ad;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    return-void
.end method

.method private a(Lcom/twitter/library/api/MediaEntity;Lcom/twitter/library/api/TweetEntities;Z)J
    .locals 8

    const/4 v7, 0x2

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/as;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    iget-wide v3, v0, Lcom/twitter/library/service/p;->c:J

    new-instance v0, Lcom/twitter/library/api/upload/a;

    iget-object v1, p0, Lcom/twitter/library/api/conversations/as;->l:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/twitter/library/api/upload/a;-><init>(Landroid/content/Context;)V

    iget-boolean v1, p0, Lcom/twitter/library/api/conversations/as;->d:Z

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/upload/a;->b(Z)Lcom/twitter/library/api/upload/a;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/twitter/library/api/upload/a;->a(Z)Lcom/twitter/library/api/upload/a;

    move-result-object v0

    if-eqz p1, :cond_2

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/api/conversations/as;->a(Lcom/twitter/library/api/MediaEntity;Lcom/twitter/library/api/upload/a;)J

    move-result-wide v0

    move-wide v1, v0

    :goto_0
    const-wide/16 v5, 0x0

    cmp-long v0, v1, v5

    if-nez v0, :cond_1

    new-instance v5, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v5, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v0, 0x3

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v4, "app:twitter_service:direct_messages"

    aput-object v4, v3, v0

    const/4 v4, 0x1

    if-eqz p3, :cond_3

    const-string/jumbo v0, "retry_dm"

    :goto_1
    aput-object v0, v3, v4

    const-string/jumbo v0, "failure"

    aput-object v0, v3, v7

    invoke-virtual {v5, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    const-string/jumbo v3, "has_media"

    invoke-virtual {v0, v3}, Lcom/twitter/library/scribe/ScribeLog;->h(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    const/4 v3, 0x6

    invoke-virtual {v0, v3}, Lcom/twitter/library/scribe/ScribeLog;->b(I)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    iget-boolean v0, p0, Lcom/twitter/library/api/conversations/as;->d:Z

    if-eqz v0, :cond_0

    invoke-virtual {v3, v7}, Lcom/twitter/library/scribe/ScribeLog;->c(I)Lcom/twitter/library/scribe/ScribeLog;

    :cond_0
    const-string/jumbo v0, "Upload Media Failed"

    invoke-virtual {v3, v0}, Lcom/twitter/library/scribe/ScribeLog;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    iget-object v0, p0, Lcom/twitter/library/api/conversations/as;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/telephony/TelephonyUtil;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string/jumbo v0, "connected"

    :goto_2
    invoke-virtual {v3, v0}, Lcom/twitter/library/scribe/ScribeLog;->d(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    iget-object v0, p0, Lcom/twitter/library/api/conversations/as;->l:Landroid/content/Context;

    invoke-static {v0, v3}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_1
    return-wide v1

    :cond_2
    invoke-direct {p0, p2, v0}, Lcom/twitter/library/api/conversations/as;->a(Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/upload/a;)J

    move-result-wide v0

    move-wide v1, v0

    goto :goto_0

    :cond_3
    const-string/jumbo v0, "send_dm"

    goto :goto_1

    :cond_4
    const-string/jumbo v0, "disconnected"

    goto :goto_2
.end method

.method private a(Lcom/twitter/library/api/MediaEntity;Lcom/twitter/library/api/upload/a;)J
    .locals 5

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/as;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/conversations/as;->l:Landroid/content/Context;

    iget-wide v2, v0, Lcom/twitter/library/service/p;->c:J

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, p1, v4}, Lcom/twitter/library/api/upload/j;->a(Landroid/content/Context;JLcom/twitter/library/api/MediaEntity;Z)Lcom/twitter/library/api/upload/h;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/api/upload/f;

    iget-object v3, p0, Lcom/twitter/library/api/conversations/as;->l:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lcom/twitter/library/api/upload/f;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;)V

    invoke-virtual {v2, v1}, Lcom/twitter/library/api/upload/f;->a(Lcom/twitter/library/api/upload/h;)Lcom/twitter/library/api/upload/f;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/library/api/upload/f;->b(Lcom/twitter/library/api/upload/o;)Lcom/twitter/library/api/upload/ad;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/upload/ad;->c(I)Lcom/twitter/library/service/b;

    invoke-virtual {p0, v2}, Lcom/twitter/library/api/conversations/as;->a(Lcom/twitter/library/api/upload/ad;)Lcom/twitter/library/service/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lcom/twitter/library/api/upload/f;->g()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/upload/a;)J
    .locals 4

    new-instance v2, Lcom/twitter/library/api/upload/k;

    iget-object v0, p0, Lcom/twitter/library/api/conversations/as;->l:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/as;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    const/4 v3, 0x1

    invoke-direct {v2, v0, v1, v3}, Lcom/twitter/library/api/upload/k;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;I)V

    invoke-virtual {v2, p1}, Lcom/twitter/library/api/upload/k;->a(Lcom/twitter/library/api/TweetEntities;)Lcom/twitter/library/api/upload/k;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/library/api/upload/k;->a(Lcom/twitter/library/api/upload/o;)Lcom/twitter/library/api/upload/k;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/upload/k;->c(I)Lcom/twitter/library/service/b;

    invoke-virtual {p0, v2}, Lcom/twitter/library/api/conversations/as;->a(Lcom/twitter/library/api/upload/ad;)Lcom/twitter/library/service/e;

    move-result-object v3

    const-wide/16 v0, 0x0

    invoke-virtual {v3}, Lcom/twitter/library/service/e;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/twitter/library/api/upload/k;->b()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :cond_0
    return-wide v0
.end method

.method private a(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;
    .locals 5

    const/4 v4, 0x1

    const-string/jumbo v0, "https://api-staging1.smf1.twitter.com"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "1.1"

    aput-object v3, v1, v2

    const-string/jumbo v2, "dm"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    const-string/jumbo v3, "conversation"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "new"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "participant_ids"

    const-string/jumbo v4, ","

    invoke-static {v4, p2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v2, 0x4a

    invoke-static {v2}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/network/d;

    iget-object v4, p0, Lcom/twitter/library/api/conversations/as;->l:Landroid/content/Context;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    sget-object v0, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v3, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v0

    new-instance v3, Lcom/twitter/library/network/n;

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/as;->s()Lcom/twitter/library/service/p;

    move-result-object v4

    iget-object v4, v4, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v3, v4}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v0, v3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/conversations/a;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/twitter/library/api/conversations/a;->a:Lcom/twitter/library/api/conversations/f;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/twitter/library/api/conversations/a;->a:Lcom/twitter/library/api/conversations/f;

    iget-object v1, v1, Lcom/twitter/library/api/conversations/f;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/as;->w()Lcom/twitter/library/provider/az;

    move-result-object v1

    iget-object v2, v0, Lcom/twitter/library/api/conversations/a;->a:Lcom/twitter/library/api/conversations/f;

    iget-object v2, v2, Lcom/twitter/library/api/conversations/f;->d:Ljava/lang/String;

    invoke-virtual {v1, p1, v2}, Lcom/twitter/library/provider/az;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v0, Lcom/twitter/library/api/conversations/a;->a:Lcom/twitter/library/api/conversations/f;

    iget-object v0, v0, Lcom/twitter/library/api/conversations/f;->d:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/twitter/library/scribe/ScribeLog;Lcom/twitter/internal/network/k;Z)V
    .locals 1

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/library/api/conversations/as;->d:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/twitter/library/scribe/ScribeLog;->c(I)Lcom/twitter/library/scribe/ScribeLog;

    :cond_1
    if-eqz p3, :cond_2

    const-string/jumbo v0, "has_media"

    :goto_1
    invoke-virtual {p1, v0}, Lcom/twitter/library/scribe/ScribeLog;->h(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    invoke-static {p2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/internal/network/k;)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/twitter/library/scribe/ScribeLog;->b(I)Lcom/twitter/library/scribe/ScribeLog;

    iget-object v0, p0, Lcom/twitter/library/api/conversations/as;->l:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;Lcom/twitter/internal/network/k;)V

    iget-object v0, p0, Lcom/twitter/library/api/conversations/as;->l:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "no_media"

    goto :goto_1
.end method


# virtual methods
.method protected a(Lcom/twitter/library/api/conversations/DMLocalMessage;Lcom/twitter/library/api/MediaEntity;Lcom/twitter/library/service/e;)V
    .locals 14

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/as;->s()Lcom/twitter/library/service/p;

    move-result-object v11

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/as;->w()Lcom/twitter/library/provider/az;

    move-result-object v1

    const-string/jumbo v2, "https://api-staging1.smf1.twitter.com"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string/jumbo v5, "1.1"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "dm"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string/jumbo v5, "new"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    const/4 v2, 0x2

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p1}, Lcom/twitter/library/api/conversations/DMLocalMessage;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v5, "conversation_id"

    iget-object v6, p1, Lcom/twitter/library/api/conversations/DMLocalMessage;->conversationId:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v5, "text"

    iget-object v6, p1, Lcom/twitter/library/api/conversations/DMLocalMessage;->text:Ljava/lang/String;

    invoke-direct {v2, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz p2, :cond_3

    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/as;->e()Z

    move-result v5

    move-object/from16 v0, p2

    invoke-direct {p0, v0, v2, v5}, Lcom/twitter/library/api/conversations/as;->a(Lcom/twitter/library/api/MediaEntity;Lcom/twitter/library/api/TweetEntities;Z)J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v2, v5, v7

    if-nez v2, :cond_4

    const/4 v2, 0x2

    invoke-virtual {v1, p1, v2}, Lcom/twitter/library/provider/az;->a(Lcom/twitter/library/api/conversations/DMLocalMessage;I)V

    iget-object v1, p1, Lcom/twitter/library/api/conversations/DMLocalMessage;->id:Ljava/lang/String;

    iput-object v1, p0, Lcom/twitter/library/api/conversations/as;->p:Ljava/lang/String;

    const/4 v1, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/e;->a(Z)V

    :goto_2
    return-void

    :cond_0
    iget-object v2, p1, Lcom/twitter/library/api/conversations/DMLocalMessage;->conversationId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/twitter/library/provider/az;->d(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v6, "user_id"

    const/4 v7, 0x0

    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v6, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v5, p1, Lcom/twitter/library/api/conversations/DMLocalMessage;->conversationId:Ljava/lang/String;

    invoke-direct {p0, v5, v2}, Lcom/twitter/library/api/conversations/as;->a(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    const/4 v2, 0x2

    invoke-virtual {v1, p1, v2}, Lcom/twitter/library/provider/az;->a(Lcom/twitter/library/api/conversations/DMLocalMessage;I)V

    iget-object v1, p1, Lcom/twitter/library/api/conversations/DMLocalMessage;->id:Ljava/lang/String;

    iput-object v1, p0, Lcom/twitter/library/api/conversations/as;->p:Ljava/lang/String;

    const/4 v1, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/e;->a(Z)V

    goto :goto_2

    :cond_2
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v6, "conversation_id"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v6, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    :cond_4
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v7, "media_id"

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v7, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    const/16 v2, 0x49

    invoke-static {v2}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v2

    new-instance v5, Lcom/twitter/library/network/d;

    iget-object v6, p0, Lcom/twitter/library/api/conversations/as;->l:Landroid/content/Context;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v5, v6, v3}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    sget-object v3, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v5, v3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v3

    new-instance v5, Lcom/twitter/library/network/n;

    iget-object v6, v11, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v5, v6}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v3, v5}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v3

    invoke-virtual {v3, v4}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v13

    invoke-virtual {v13}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-virtual {v2}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/conversations/q;

    iget-object v3, p1, Lcom/twitter/library/api/conversations/DMLocalMessage;->id:Ljava/lang/String;

    iget-object v4, v2, Lcom/twitter/library/api/conversations/q;->a:Lcom/twitter/library/api/conversations/DMMessage;

    invoke-virtual {v1, v3, v4}, Lcom/twitter/library/provider/az;->a(Ljava/lang/String;Lcom/twitter/library/api/conversations/DMMessage;)V

    iget-object v3, v2, Lcom/twitter/library/api/conversations/q;->a:Lcom/twitter/library/api/conversations/DMMessage;

    iget-object v3, v3, Lcom/twitter/library/api/conversations/DMMessage;->conversationId:Ljava/lang/String;

    iput-object v3, p0, Lcom/twitter/library/api/conversations/as;->n:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_6

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/twitter/library/api/MediaEntity;->mediaUrl:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/library/util/n;->b(Landroid/net/Uri;)Z

    :cond_6
    iget-object v3, v2, Lcom/twitter/library/api/conversations/q;->b:Ljava/util/List;

    if-eqz v3, :cond_8

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iget-object v2, v2, Lcom/twitter/library/api/conversations/q;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/TwitterUser;

    iget-wide v5, v2, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_7
    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    const-wide/16 v3, -0x1

    const/4 v5, -0x1

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x1

    invoke-virtual/range {v1 .. v10}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    :cond_8
    :goto_4
    invoke-virtual {v13}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v2

    if-eqz v2, :cond_9

    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    iget-wide v4, v11, Lcom/twitter/library/service/p;->c:J

    invoke-direct {v3, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v1, 0x3

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v5, "app:twitter_service:direct_messages"

    aput-object v5, v4, v1

    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/as;->e()Z

    move-result v1

    if-eqz v1, :cond_b

    const-string/jumbo v1, "retry_dm"

    :goto_5
    aput-object v1, v4, v5

    const/4 v5, 0x2

    iget v1, v2, Lcom/twitter/internal/network/k;->a:I

    const/16 v6, 0xc8

    if-ne v1, v6, :cond_c

    const-string/jumbo v1, "success"

    :goto_6
    aput-object v1, v4, v5

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v12}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-direct {p0, v1, v2, v3}, Lcom/twitter/library/api/conversations/as;->a(Lcom/twitter/library/scribe/ScribeLog;Lcom/twitter/internal/network/k;Z)V

    :cond_9
    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    goto/16 :goto_2

    :cond_a
    const/4 v3, 0x2

    invoke-virtual {v1, p1, v3}, Lcom/twitter/library/provider/az;->a(Lcom/twitter/library/api/conversations/DMLocalMessage;I)V

    invoke-virtual {v2}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/twitter/library/api/conversations/as;->c(Ljava/util/ArrayList;)[I

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/conversations/as;->o:[I

    iget-object v1, p1, Lcom/twitter/library/api/conversations/DMLocalMessage;->id:Ljava/lang/String;

    iput-object v1, p0, Lcom/twitter/library/api/conversations/as;->p:Ljava/lang/String;

    goto :goto_4

    :cond_b
    const-string/jumbo v1, "send_dm"

    goto :goto_5

    :cond_c
    const-string/jumbo v1, "failure"

    goto :goto_6
.end method

.method protected a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/api/conversations/as;->d:Z

    return-void
.end method

.method public abstract e()Z
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/conversations/as;->n:Ljava/lang/String;

    return-object v0
.end method

.method public g()[I
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/conversations/as;->o:[I

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/conversations/as;->p:Ljava/lang/String;

    return-object v0
.end method
