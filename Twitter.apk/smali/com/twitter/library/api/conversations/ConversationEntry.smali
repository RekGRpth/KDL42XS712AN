.class public abstract Lcom/twitter/library/api/conversations/ConversationEntry;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/api/k;
.implements Ljava/io/Externalizable;


# static fields
.field private static final serialVersionUID:J = 0x331b42a931cea289L


# instance fields
.field public conversationId:Ljava/lang/String;

.field public date:J

.field public id:Ljava/lang/String;

.field public senderId:J


# direct methods
.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 7

    const-wide/16 v5, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/api/conversations/ConversationEntry;-><init>(Ljava/lang/String;Ljava/lang/String;JJ)V

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/api/conversations/ConversationEntry;->id:Ljava/lang/String;

    iput-object p2, p0, Lcom/twitter/library/api/conversations/ConversationEntry;->conversationId:Ljava/lang/String;

    iput-wide p3, p0, Lcom/twitter/library/api/conversations/ConversationEntry;->date:J

    iput-wide p5, p0, Lcom/twitter/library/api/conversations/ConversationEntry;->senderId:J

    return-void
.end method

.method protected static a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/conversations/ConversationEntry;
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v2, v0

    move-object v0, v1

    :goto_0
    if-eqz v2, :cond_9

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_9

    sget-object v3, Lcom/twitter/library/api/conversations/b;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    :pswitch_0
    const-string/jumbo v2, "message"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p0}, Lcom/twitter/library/api/conversations/DMMessage;->b(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/conversations/DMMessage;

    move-result-object v0

    :goto_2
    return-object v0

    :cond_0
    const-string/jumbo v2, "conversation_create"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p0}, Lcom/twitter/library/api/conversations/CreateConversationEntry;->b(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/conversations/CreateConversationEntry;

    move-result-object v0

    goto :goto_2

    :cond_1
    const-string/jumbo v2, "request_received"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {p0}, Lcom/twitter/library/api/conversations/DMRequest;->b(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/conversations/DMRequest;

    move-result-object v0

    goto :goto_2

    :cond_2
    const-string/jumbo v2, "user_typing"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {p0}, Lcom/twitter/library/api/conversations/UserTypingEvent;->b(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/conversations/UserTypingEvent;

    move-result-object v0

    goto :goto_2

    :cond_3
    const-string/jumbo v2, "remove_conversation"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {p0}, Lcom/twitter/library/api/conversations/RemoveConversation;->b(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/conversations/RemoveConversation;

    move-result-object v0

    goto :goto_2

    :cond_4
    const-string/jumbo v2, "message_delete"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {p0}, Lcom/twitter/library/api/conversations/DeleteMessage;->b(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/conversations/DeleteMessage;

    move-result-object v0

    goto :goto_2

    :cond_5
    const-string/jumbo v2, "conversation_name_update"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-static {p0}, Lcom/twitter/library/api/conversations/ConversationNameUpdatedEntry;->b(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/conversations/ConversationNameUpdatedEntry;

    move-result-object v0

    goto :goto_2

    :cond_6
    const-string/jumbo v2, "participants_join"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-static {p0}, Lcom/twitter/library/api/conversations/ParticipantsJoinEntry;->c(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/conversations/ParticipantsJoinEntry;

    move-result-object v0

    goto :goto_2

    :cond_7
    const-string/jumbo v2, "participants_leave"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-static {p0}, Lcom/twitter/library/api/conversations/ParticipantsLeaveEntry;->c(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/conversations/ParticipantsLeaveEntry;

    move-result-object v0

    goto :goto_2

    :cond_8
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    :cond_9
    move-object v0, v1

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public a()J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public a(Landroid/database/sqlite/SQLiteDatabase;J)V
    .locals 5

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v1, "entry_id"

    iget-object v2, p0, Lcom/twitter/library/api/conversations/ConversationEntry;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "conversation_id"

    iget-object v2, p0, Lcom/twitter/library/api/conversations/ConversationEntry;->conversationId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "created"

    iget-wide v2, p0, Lcom/twitter/library/api/conversations/ConversationEntry;->date:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v1, "entry_type"

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/ConversationEntry;->d()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v1, "data"

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/ConversationEntry;->c()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-wide/16 v1, -0x1

    iget-wide v3, p0, Lcom/twitter/library/api/conversations/ConversationEntry;->senderId:J

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    const-string/jumbo v1, "user_id"

    iget-wide v2, p0, Lcom/twitter/library/api/conversations/ConversationEntry;->senderId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_0
    const-string/jumbo v1, "conversation_entries"

    const-string/jumbo v2, "entry_id"

    invoke-virtual {p1, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/conversations/ConversationEntry;->id:Ljava/lang/String;

    return-object v0
.end method

.method public c()[B
    .locals 1

    invoke-static {p0}, Lcom/twitter/library/util/Util;->a(Ljava/lang/Object;)[B

    move-result-object v0

    return-object v0
.end method

.method protected abstract d()I
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 0

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 0

    return-void
.end method
