.class public Lcom/twitter/library/api/conversations/DMPhoto;
.super Lcom/twitter/library/api/conversations/k;
.source "Twttr"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field private static final serialVersionUID:J = 0x1cfcd40880bd999cL


# instance fields
.field public end:I

.field public height:I

.field public mediaEntity:Lcom/twitter/library/api/MediaEntity;

.field public mediaUrl:Ljava/lang/String;

.field public start:I

.field public width:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/twitter/library/api/conversations/k;-><init>()V

    iput v0, p0, Lcom/twitter/library/api/conversations/DMPhoto;->start:I

    iput v0, p0, Lcom/twitter/library/api/conversations/DMPhoto;->end:I

    return-void
.end method

.method public static a(Lcom/twitter/library/api/MediaEntity;)Lcom/twitter/library/api/conversations/DMPhoto;
    .locals 2

    new-instance v0, Lcom/twitter/library/api/conversations/DMPhoto;

    invoke-direct {v0}, Lcom/twitter/library/api/conversations/DMPhoto;-><init>()V

    iget-object v1, p0, Lcom/twitter/library/api/MediaEntity;->mediaUrl:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/library/api/conversations/DMPhoto;->mediaUrl:Ljava/lang/String;

    iget v1, p0, Lcom/twitter/library/api/MediaEntity;->start:I

    iput v1, v0, Lcom/twitter/library/api/conversations/DMPhoto;->start:I

    iget v1, p0, Lcom/twitter/library/api/MediaEntity;->end:I

    iput v1, v0, Lcom/twitter/library/api/conversations/DMPhoto;->end:I

    iget v1, p0, Lcom/twitter/library/api/MediaEntity;->width:I

    iput v1, v0, Lcom/twitter/library/api/conversations/DMPhoto;->width:I

    iget v1, p0, Lcom/twitter/library/api/MediaEntity;->height:I

    iput v1, v0, Lcom/twitter/library/api/conversations/DMPhoto;->height:I

    iput-object p0, v0, Lcom/twitter/library/api/conversations/DMPhoto;->mediaEntity:Lcom/twitter/library/api/MediaEntity;

    return-object v0
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/conversations/DMPhoto;)V
    .locals 3

    const/4 v2, -0x1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_3

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->i:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v0

    iget v1, p1, Lcom/twitter/library/api/conversations/DMPhoto;->start:I

    if-ne v1, v2, :cond_1

    iput v0, p1, Lcom/twitter/library/api/conversations/DMPhoto;->start:I

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget v1, p1, Lcom/twitter/library/api/conversations/DMPhoto;->end:I

    if-ne v1, v2, :cond_0

    iput v0, p1, Lcom/twitter/library/api/conversations/DMPhoto;->end:I

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_3
    return-void
.end method

.method protected static b(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/conversations/DMPhoto;
    .locals 4

    new-instance v2, Lcom/twitter/library/api/conversations/DMPhoto;

    invoke-direct {v2}, Lcom/twitter/library/api/conversations/DMPhoto;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_3

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_3

    sget-object v3, Lcom/twitter/library/api/conversations/x;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "media_url_https"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/api/conversations/DMPhoto;->mediaUrl:Ljava/lang/String;

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    const-string/jumbo v1, "sizes"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0, v2}, Lcom/twitter/library/api/conversations/DMPhoto;->b(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/conversations/DMPhoto;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_3
    const-string/jumbo v1, "indices"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p0, v2}, Lcom/twitter/library/api/conversations/DMPhoto;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/conversations/DMPhoto;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_3
    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static b(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/conversations/DMPhoto;)V
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_8

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_8

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v1, :cond_7

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "large"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move v1, v2

    move-object v3, v0

    move v0, v2

    :goto_1
    if-eqz v3, :cond_4

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v3, v4, :cond_4

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->i:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v3, v4, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "w"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v1

    :cond_0
    :goto_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    goto :goto_1

    :cond_1
    const-string/jumbo v4, "h"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v0

    goto :goto_2

    :cond_2
    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v3, v4, :cond_3

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v3, v4, :cond_0

    :cond_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    :cond_4
    if-lez v1, :cond_5

    if-lez v0, :cond_5

    iput v1, p1, Lcom/twitter/library/api/conversations/DMPhoto;->width:I

    iput v0, p1, Lcom/twitter/library/api/conversations/DMPhoto;->height:I

    :cond_5
    :goto_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :cond_7
    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v1, :cond_5

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :cond_8
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "photo"

    return-object v0
.end method

.method public b()Lcom/twitter/library/api/MediaEntity;
    .locals 3

    const/4 v2, 0x1

    new-instance v0, Lcom/twitter/library/api/MediaEntity;

    invoke-direct {v0}, Lcom/twitter/library/api/MediaEntity;-><init>()V

    iput v2, v0, Lcom/twitter/library/api/MediaEntity;->type:I

    iget-object v1, p0, Lcom/twitter/library/api/conversations/DMPhoto;->mediaUrl:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/library/api/MediaEntity;->url:Ljava/lang/String;

    iget v1, p0, Lcom/twitter/library/api/conversations/DMPhoto;->start:I

    iput v1, v0, Lcom/twitter/library/api/MediaEntity;->start:I

    iget v1, p0, Lcom/twitter/library/api/conversations/DMPhoto;->end:I

    iput v1, v0, Lcom/twitter/library/api/MediaEntity;->end:I

    iget-object v1, p0, Lcom/twitter/library/api/conversations/DMPhoto;->mediaUrl:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/library/api/MediaEntity;->displayUrl:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/library/api/conversations/DMPhoto;->mediaUrl:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/library/api/MediaEntity;->mediaUrl:Ljava/lang/String;

    iget v1, p0, Lcom/twitter/library/api/conversations/DMPhoto;->width:I

    iput v1, v0, Lcom/twitter/library/api/MediaEntity;->width:I

    iget v1, p0, Lcom/twitter/library/api/conversations/DMPhoto;->height:I

    iput v1, v0, Lcom/twitter/library/api/MediaEntity;->height:I

    iput-boolean v2, v0, Lcom/twitter/library/api/MediaEntity;->processed:Z

    return-object v0
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/conversations/DMPhoto;->mediaUrl:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/conversations/DMPhoto;->start:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/conversations/DMPhoto;->end:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/conversations/DMPhoto;->width:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/conversations/DMPhoto;->height:I

    :try_start_0
    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    iput-object v0, p0, Lcom/twitter/library/api/conversations/DMPhoto;->mediaEntity:Lcom/twitter/library/api/MediaEntity;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/conversations/DMPhoto;->mediaUrl:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget v0, p0, Lcom/twitter/library/api/conversations/DMPhoto;->start:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/api/conversations/DMPhoto;->end:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/api/conversations/DMPhoto;->width:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/api/conversations/DMPhoto;->height:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/api/conversations/DMPhoto;->mediaEntity:Lcom/twitter/library/api/MediaEntity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/conversations/DMPhoto;->mediaEntity:Lcom/twitter/library/api/MediaEntity;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
