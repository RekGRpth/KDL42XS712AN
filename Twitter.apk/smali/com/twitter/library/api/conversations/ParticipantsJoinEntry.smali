.class public Lcom/twitter/library/api/conversations/ParticipantsJoinEntry;
.super Lcom/twitter/library/api/conversations/ParticipantsEntry;
.source "Twttr"


# static fields
.field private static final serialVersionUID:J = -0x228c20f6aa4c385L


# direct methods
.method public constructor <init>(Lcom/twitter/library/api/conversations/am;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/library/api/conversations/ParticipantsEntry;-><init>(Lcom/twitter/library/api/conversations/am;)V

    return-void
.end method

.method public static c(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/conversations/ParticipantsJoinEntry;
    .locals 2

    new-instance v0, Lcom/twitter/library/api/conversations/ParticipantsJoinEntry;

    invoke-static {p0}, Lcom/twitter/library/api/conversations/ParticipantsEntry;->b(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/conversations/am;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/library/api/conversations/ParticipantsJoinEntry;-><init>(Lcom/twitter/library/api/conversations/am;)V

    return-object v0
.end method


# virtual methods
.method public a(Landroid/database/sqlite/SQLiteDatabase;J)V
    .locals 6

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    iget-object v0, p0, Lcom/twitter/library/api/conversations/ParticipantsJoinEntry;->participants:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v0, v3, p2

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    const-string/jumbo v0, "conversation_id"

    iget-object v5, p0, Lcom/twitter/library/api/conversations/ParticipantsJoinEntry;->conversationId:Ljava/lang/String;

    invoke-virtual {v1, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "user_id"

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string/jumbo v0, "conversation_participants"

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_0

    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/library/api/conversations/ParticipantsEntry;->a(Landroid/database/sqlite/SQLiteDatabase;J)V

    return-void
.end method

.method protected d()I
    .locals 1

    const/16 v0, 0xa

    return v0
.end method
