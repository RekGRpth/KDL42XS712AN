.class public final enum Lcom/twitter/library/api/conversations/DMResponseSource;
.super Ljava/lang/Enum;
.source "Twttr"


# static fields
.field public static final enum a:Lcom/twitter/library/api/conversations/DMResponseSource;

.field public static final enum b:Lcom/twitter/library/api/conversations/DMResponseSource;

.field public static final enum c:Lcom/twitter/library/api/conversations/DMResponseSource;

.field public static final enum d:Lcom/twitter/library/api/conversations/DMResponseSource;

.field private static final synthetic e:[Lcom/twitter/library/api/conversations/DMResponseSource;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/twitter/library/api/conversations/DMResponseSource;

    const-string/jumbo v1, "USER_INBOX"

    const-string/jumbo v2, "user_inbox"

    invoke-direct {v0, v1, v3, v2}, Lcom/twitter/library/api/conversations/DMResponseSource;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/library/api/conversations/DMResponseSource;->a:Lcom/twitter/library/api/conversations/DMResponseSource;

    new-instance v0, Lcom/twitter/library/api/conversations/DMResponseSource;

    const-string/jumbo v1, "USER_EVENTS"

    const-string/jumbo v2, "user_events"

    invoke-direct {v0, v1, v4, v2}, Lcom/twitter/library/api/conversations/DMResponseSource;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/library/api/conversations/DMResponseSource;->b:Lcom/twitter/library/api/conversations/DMResponseSource;

    new-instance v0, Lcom/twitter/library/api/conversations/DMResponseSource;

    const-string/jumbo v1, "CONVERSATION_TIMELINE"

    const-string/jumbo v2, "conversation_timeline"

    invoke-direct {v0, v1, v5, v2}, Lcom/twitter/library/api/conversations/DMResponseSource;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/library/api/conversations/DMResponseSource;->c:Lcom/twitter/library/api/conversations/DMResponseSource;

    new-instance v0, Lcom/twitter/library/api/conversations/DMResponseSource;

    const-string/jumbo v1, "USER_REQUESTS"

    const-string/jumbo v2, "user_requests"

    invoke-direct {v0, v1, v6, v2}, Lcom/twitter/library/api/conversations/DMResponseSource;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/library/api/conversations/DMResponseSource;->d:Lcom/twitter/library/api/conversations/DMResponseSource;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/twitter/library/api/conversations/DMResponseSource;

    sget-object v1, Lcom/twitter/library/api/conversations/DMResponseSource;->a:Lcom/twitter/library/api/conversations/DMResponseSource;

    aput-object v1, v0, v3

    sget-object v1, Lcom/twitter/library/api/conversations/DMResponseSource;->b:Lcom/twitter/library/api/conversations/DMResponseSource;

    aput-object v1, v0, v4

    sget-object v1, Lcom/twitter/library/api/conversations/DMResponseSource;->c:Lcom/twitter/library/api/conversations/DMResponseSource;

    aput-object v1, v0, v5

    sget-object v1, Lcom/twitter/library/api/conversations/DMResponseSource;->d:Lcom/twitter/library/api/conversations/DMResponseSource;

    aput-object v1, v0, v6

    sput-object v0, Lcom/twitter/library/api/conversations/DMResponseSource;->e:[Lcom/twitter/library/api/conversations/DMResponseSource;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/twitter/library/api/conversations/DMResponseSource;->mName:Ljava/lang/String;

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/twitter/library/api/conversations/DMResponseSource;
    .locals 5

    invoke-static {}, Lcom/twitter/library/api/conversations/DMResponseSource;->values()[Lcom/twitter/library/api/conversations/DMResponseSource;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    iget-object v4, v0, Lcom/twitter/library/api/conversations/DMResponseSource;->mName:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/library/api/conversations/DMResponseSource;
    .locals 1

    const-class v0, Lcom/twitter/library/api/conversations/DMResponseSource;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/conversations/DMResponseSource;

    return-object v0
.end method

.method public static values()[Lcom/twitter/library/api/conversations/DMResponseSource;
    .locals 1

    sget-object v0, Lcom/twitter/library/api/conversations/DMResponseSource;->e:[Lcom/twitter/library/api/conversations/DMResponseSource;

    invoke-virtual {v0}, [Lcom/twitter/library/api/conversations/DMResponseSource;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/api/conversations/DMResponseSource;

    return-object v0
.end method
