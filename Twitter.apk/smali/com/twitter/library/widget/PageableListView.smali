.class public Lcom/twitter/library/widget/PageableListView;
.super Landroid/widget/ListView;
.source "Twttr"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:Z

.field private e:Landroid/view/View;

.field private f:Landroid/view/View;

.field private g:Landroid/widget/TextView;

.field private h:Lcom/twitter/library/widget/k;

.field private i:Landroid/view/View;

.field private j:Landroid/view/View;

.field private k:Landroid/support/v4/widget/ExploreByTouchHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/widget/PageableListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget v0, Lib;->pageableListViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/library/widget/PageableListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-object v0, Lim;->PageableListView:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/widget/PageableListView;->a:I

    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/widget/PageableListView;->b:I

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/widget/PageableListView;->c:I

    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/library/widget/PageableListView;->d:Z

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private c()V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/library/widget/PageableListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "This method must be called before setAdapter."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/twitter/library/widget/PageableListView;->c()V

    iget-object v0, p0, Lcom/twitter/library/widget/PageableListView;->j:Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/library/widget/PageableListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, Lcom/twitter/library/widget/PageableListView;->b:I

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/library/widget/PageableListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    iput-object v0, p0, Lcom/twitter/library/widget/PageableListView;->j:Landroid/view/View;

    sget v1, Lig;->header_content:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/widget/PageableListView;->e:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/library/widget/PageableListView;->e:Landroid/view/View;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "loadingHeaderLayout must define a View with @id/header_content."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-boolean v1, p0, Lcom/twitter/library/widget/PageableListView;->d:Z

    if-nez v1, :cond_1

    sget v1, Lig;->divider:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method public a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/widget/PageableListView;->j:Landroid/view/View;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/library/widget/PageableListView;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/widget/PageableListView;->e:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public b()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/twitter/library/widget/PageableListView;->c()V

    iget-object v0, p0, Lcom/twitter/library/widget/PageableListView;->i:Landroid/view/View;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/widget/PageableListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, Lcom/twitter/library/widget/PageableListView;->a:I

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/library/widget/PageableListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    invoke-virtual {p0, v2}, Lcom/twitter/library/widget/PageableListView;->setFooterDividersEnabled(Z)V

    iput-object v0, p0, Lcom/twitter/library/widget/PageableListView;->i:Landroid/view/View;

    sget v1, Lig;->footer_progress_bar:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/widget/PageableListView;->f:Landroid/view/View;

    sget v1, Lig;->footer_label:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/library/widget/PageableListView;->g:Landroid/widget/TextView;

    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/widget/PageableListView;->i:Landroid/view/View;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/twitter/library/widget/PageableListView;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/library/widget/PageableListView;->g:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/PageableListView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/library/widget/PageableListView;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/library/widget/PageableListView;->g:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/PageableListView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    iget-object v0, p0, Lcom/twitter/library/widget/PageableListView;->k:Landroid/support/v4/widget/ExploreByTouchHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/PageableListView;->k:Landroid/support/v4/widget/ExploreByTouchHelper;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/ExploreByTouchHelper;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public getLoadingHeaderHeight()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/PageableListView;->j:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/PageableListView;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/widget/PageableListView;->h:Lcom/twitter/library/widget/k;

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/widget/PageableListView;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    add-int v0, p2, p3

    iget-object v1, p0, Lcom/twitter/library/widget/PageableListView;->j:Landroid/view/View;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/library/widget/PageableListView;->j:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/library/widget/PageableListView;->j:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/twitter/library/widget/PageableListView;->getPositionForView(Landroid/view/View;)I

    move-result v1

    if-lt v0, v1, :cond_2

    iget-object v1, p0, Lcom/twitter/library/widget/PageableListView;->h:Lcom/twitter/library/widget/k;

    invoke-interface {v1, p0}, Lcom/twitter/library/widget/k;->a(Landroid/widget/AbsListView;)V

    :cond_2
    iget-object v1, p0, Lcom/twitter/library/widget/PageableListView;->i:Landroid/view/View;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/twitter/library/widget/PageableListView;->i:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/twitter/library/widget/PageableListView;->i:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/twitter/library/widget/PageableListView;->getPositionForView(Landroid/view/View;)I

    move-result v1

    if-lt v0, v1, :cond_3

    iget-object v0, p0, Lcom/twitter/library/widget/PageableListView;->h:Lcom/twitter/library/widget/k;

    invoke-interface {v0, p0}, Lcom/twitter/library/widget/k;->b(Landroid/widget/AbsListView;)V

    :cond_3
    iget-object v0, p0, Lcom/twitter/library/widget/PageableListView;->h:Lcom/twitter/library/widget/k;

    invoke-interface {v0, p0, p2, p3, p4}, Lcom/twitter/library/widget/k;->onScroll(Landroid/widget/AbsListView;III)V

    goto :goto_0
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/PageableListView;->h:Lcom/twitter/library/widget/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/PageableListView;->h:Lcom/twitter/library/widget/k;

    invoke-interface {v0, p0, p2}, Lcom/twitter/library/widget/k;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    :cond_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 2

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ListView;->onSizeChanged(IIII)V

    const/4 v0, 0x1

    iget v1, p0, Lcom/twitter/library/widget/PageableListView;->c:I

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/widget/PageableListView;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/PageableListView;->setSelection(I)V

    :cond_0
    return-void
.end method

.method public setExploreByTouchHelper(Landroid/support/v4/widget/ExploreByTouchHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/widget/PageableListView;->k:Landroid/support/v4/widget/ExploreByTouchHelper;

    invoke-static {p0, p1}, Landroid/support/v4/view/ViewCompat;->setAccessibilityDelegate(Landroid/view/View;Landroid/support/v4/view/AccessibilityDelegateCompat;)V

    return-void
.end method

.method public setOnPageScrollListener(Lcom/twitter/library/widget/k;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/library/widget/PageableListView;->h:Lcom/twitter/library/widget/k;

    if-eqz p1, :cond_0

    invoke-super {p0, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    goto :goto_0
.end method

.method public setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 0

    return-void
.end method
