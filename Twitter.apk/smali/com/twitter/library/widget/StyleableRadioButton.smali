.class public Lcom/twitter/library/widget/StyleableRadioButton;
.super Landroid/widget/RadioButton;
.source "Twttr"


# instance fields
.field public final a:I

.field public final b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/widget/StyleableRadioButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget v0, Lib;->radioButtonStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/library/widget/StyleableRadioButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/RadioButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget-object v0, Lim;->StyleableRadioButton:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/widget/StyleableRadioButton;->a:I

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/widget/StyleableRadioButton;->b:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method public setChecked(Z)V
    .locals 2

    invoke-super {p0, p1}, Landroid/widget/RadioButton;->setChecked(Z)V

    invoke-virtual {p0}, Lcom/twitter/library/widget/StyleableRadioButton;->getContext()Landroid/content/Context;

    move-result-object v1

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/twitter/library/widget/StyleableRadioButton;->b:I

    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/twitter/library/widget/StyleableRadioButton;->setTextAppearance(Landroid/content/Context;I)V

    return-void

    :cond_0
    iget v0, p0, Lcom/twitter/library/widget/StyleableRadioButton;->a:I

    goto :goto_0
.end method
