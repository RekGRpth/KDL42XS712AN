.class Lcom/twitter/library/widget/al;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/library/widget/TweetView;

.field private b:Landroid/graphics/Rect;

.field private c:Landroid/graphics/RectF;

.field private final d:J

.field private final e:J

.field private f:Ljava/lang/Runnable;

.field private g:Ljava/lang/Runnable;

.field private h:Ljava/lang/Runnable;

.field private i:Ljava/lang/Runnable;

.field private final j:[I


# direct methods
.method constructor <init>(Lcom/twitter/library/widget/TweetView;JJLjava/lang/Runnable;[I)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/widget/al;->a:Lcom/twitter/library/widget/TweetView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p2, p0, Lcom/twitter/library/widget/al;->d:J

    iput-wide p4, p0, Lcom/twitter/library/widget/al;->e:J

    iput-object p6, p0, Lcom/twitter/library/widget/al;->h:Ljava/lang/Runnable;

    iput-object p7, p0, Lcom/twitter/library/widget/al;->j:[I

    return-void
.end method


# virtual methods
.method a()Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/al;->b:Landroid/graphics/Rect;

    return-object v0
.end method

.method a(Landroid/graphics/Rect;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/widget/al;->b:Landroid/graphics/Rect;

    return-void
.end method

.method a(Landroid/graphics/RectF;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/widget/al;->c:Landroid/graphics/RectF;

    return-void
.end method

.method a(Ljava/lang/Runnable;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/widget/al;->i:Ljava/lang/Runnable;

    return-void
.end method

.method a(Z)V
    .locals 5

    const-wide/16 v3, -0x1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/al;->a:Lcom/twitter/library/widget/TweetView;

    iget-wide v1, p0, Lcom/twitter/library/widget/al;->d:J

    xor-long/2addr v1, v3

    invoke-static {v0, v1, v2}, Lcom/twitter/library/widget/TweetView;->b(Lcom/twitter/library/widget/TweetView;J)I

    iget-object v0, p0, Lcom/twitter/library/widget/al;->a:Lcom/twitter/library/widget/TweetView;

    iget-wide v1, p0, Lcom/twitter/library/widget/al;->e:J

    invoke-static {v0, v1, v2}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/widget/TweetView;J)I

    :goto_0
    iget-object v0, p0, Lcom/twitter/library/widget/al;->a:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->refreshDrawableState()V

    iget-object v0, p0, Lcom/twitter/library/widget/al;->a:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->invalidate()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/widget/al;->a:Lcom/twitter/library/widget/TweetView;

    iget-wide v1, p0, Lcom/twitter/library/widget/al;->e:J

    xor-long/2addr v1, v3

    invoke-static {v0, v1, v2}, Lcom/twitter/library/widget/TweetView;->b(Lcom/twitter/library/widget/TweetView;J)I

    goto :goto_0
.end method

.method a([I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/al;->j:[I

    invoke-static {p1, v0}, Lcom/twitter/library/widget/TweetView;->a([I[I)[I

    return-void
.end method

.method a(II)Z
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/widget/al;->b:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/al;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/widget/al;->c:Landroid/graphics/RectF;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/widget/al;->c:Landroid/graphics/RectF;

    int-to-float v1, p1

    int-to-float v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/widget/al;->a:Lcom/twitter/library/widget/TweetView;

    iget-wide v1, p0, Lcom/twitter/library/widget/al;->d:J

    invoke-static {v0, v1, v2}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/widget/TweetView;J)I

    return-void
.end method

.method c()Z
    .locals 4

    iget-object v0, p0, Lcom/twitter/library/widget/al;->a:Lcom/twitter/library/widget/TweetView;

    invoke-static {v0}, Lcom/twitter/library/widget/TweetView;->c(Lcom/twitter/library/widget/TweetView;)I

    move-result v0

    int-to-long v0, v0

    iget-wide v2, p0, Lcom/twitter/library/widget/al;->e:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method d()Z
    .locals 4

    iget-object v0, p0, Lcom/twitter/library/widget/al;->a:Lcom/twitter/library/widget/TweetView;

    invoke-static {v0}, Lcom/twitter/library/widget/TweetView;->c(Lcom/twitter/library/widget/TweetView;)I

    move-result v0

    int-to-long v0, v0

    iget-wide v2, p0, Lcom/twitter/library/widget/al;->d:J

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method e()Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/al;->f:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/library/widget/am;

    invoke-direct {v0, p0}, Lcom/twitter/library/widget/am;-><init>(Lcom/twitter/library/widget/al;)V

    iput-object v0, p0, Lcom/twitter/library/widget/al;->f:Ljava/lang/Runnable;

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/widget/al;->f:Ljava/lang/Runnable;

    return-object v0
.end method

.method f()Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/al;->g:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/library/widget/an;

    invoke-direct {v0, p0}, Lcom/twitter/library/widget/an;-><init>(Lcom/twitter/library/widget/al;)V

    iput-object v0, p0, Lcom/twitter/library/widget/al;->g:Ljava/lang/Runnable;

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/widget/al;->g:Ljava/lang/Runnable;

    return-object v0
.end method

.method g()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/al;->g:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method h()Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/al;->h:Ljava/lang/Runnable;

    return-object v0
.end method

.method i()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/al;->i:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method j()Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/al;->i:Ljava/lang/Runnable;

    return-object v0
.end method

.method k()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/al;->h:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    return-void
.end method

.method l()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/al;->i:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    return-void
.end method

.method m()V
    .locals 5

    iget-object v0, p0, Lcom/twitter/library/widget/al;->f:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/al;->a:Lcom/twitter/library/widget/TweetView;

    iget-wide v1, p0, Lcom/twitter/library/widget/al;->d:J

    const-wide/16 v3, -0x1

    xor-long/2addr v1, v3

    invoke-static {v0, v1, v2}, Lcom/twitter/library/widget/TweetView;->b(Lcom/twitter/library/widget/TweetView;J)I

    iget-object v0, p0, Lcom/twitter/library/widget/al;->a:Lcom/twitter/library/widget/TweetView;

    iget-object v1, p0, Lcom/twitter/library/widget/al;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetView;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method n()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/widget/al;->g:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/al;->a:Lcom/twitter/library/widget/TweetView;

    iget-object v1, p0, Lcom/twitter/library/widget/al;->g:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetView;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method
