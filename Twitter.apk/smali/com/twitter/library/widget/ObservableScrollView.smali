.class public Lcom/twitter/library/widget/ObservableScrollView;
.super Landroid/widget/ScrollView;
.source "Twttr"


# instance fields
.field private a:Lcom/twitter/library/widget/j;

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/widget/ObservableScrollView;->a:Lcom/twitter/library/widget/j;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/widget/ObservableScrollView;->a:Lcom/twitter/library/widget/j;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/widget/ObservableScrollView;->a:Lcom/twitter/library/widget/j;

    return-void
.end method


# virtual methods
.method protected onScrollChanged(IIII)V
    .locals 6

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    iput p1, p0, Lcom/twitter/library/widget/ObservableScrollView;->b:I

    iput p2, p0, Lcom/twitter/library/widget/ObservableScrollView;->c:I

    iget-object v0, p0, Lcom/twitter/library/widget/ObservableScrollView;->a:Lcom/twitter/library/widget/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/ObservableScrollView;->a:Lcom/twitter/library/widget/j;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/twitter/library/widget/j;->a(Lcom/twitter/library/widget/ObservableScrollView;IIII)V

    :cond_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onSizeChanged(IIII)V

    iget-object v0, p0, Lcom/twitter/library/widget/ObservableScrollView;->a:Lcom/twitter/library/widget/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/ObservableScrollView;->a:Lcom/twitter/library/widget/j;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/twitter/library/widget/j;->a(IIII)V

    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    :pswitch_0
    iget v0, p0, Lcom/twitter/library/widget/ObservableScrollView;->b:I

    iput v0, p0, Lcom/twitter/library/widget/ObservableScrollView;->d:I

    iget v0, p0, Lcom/twitter/library/widget/ObservableScrollView;->c:I

    iput v0, p0, Lcom/twitter/library/widget/ObservableScrollView;->e:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/widget/ObservableScrollView;->f:Z

    goto :goto_0

    :pswitch_1
    iget v0, p0, Lcom/twitter/library/widget/ObservableScrollView;->d:I

    iget v1, p0, Lcom/twitter/library/widget/ObservableScrollView;->b:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/twitter/library/widget/ObservableScrollView;->e:I

    iget v1, p0, Lcom/twitter/library/widget/ObservableScrollView;->c:I

    if-eq v0, v1, :cond_0

    :cond_1
    iput-boolean v2, p0, Lcom/twitter/library/widget/ObservableScrollView;->f:Z

    iget-object v0, p0, Lcom/twitter/library/widget/ObservableScrollView;->a:Lcom/twitter/library/widget/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/ObservableScrollView;->a:Lcom/twitter/library/widget/j;

    invoke-interface {v0, p0}, Lcom/twitter/library/widget/j;->a(Lcom/twitter/library/widget/ObservableScrollView;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/twitter/library/widget/ObservableScrollView;->a:Lcom/twitter/library/widget/j;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/widget/ObservableScrollView;->a:Lcom/twitter/library/widget/j;

    invoke-interface {v0}, Lcom/twitter/library/widget/j;->a()V

    iget-boolean v0, p0, Lcom/twitter/library/widget/ObservableScrollView;->f:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/widget/ObservableScrollView;->a:Lcom/twitter/library/widget/j;

    invoke-interface {v0}, Lcom/twitter/library/widget/j;->b()V

    :cond_2
    iput-boolean v2, p0, Lcom/twitter/library/widget/ObservableScrollView;->f:Z

    goto :goto_0

    :pswitch_3
    iput-boolean v2, p0, Lcom/twitter/library/widget/ObservableScrollView;->f:Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public setObservableScrollViewListener(Lcom/twitter/library/widget/j;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/widget/ObservableScrollView;->a:Lcom/twitter/library/widget/j;

    return-void
.end method
