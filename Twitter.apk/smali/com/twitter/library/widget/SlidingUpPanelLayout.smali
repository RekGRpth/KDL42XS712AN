.class public Lcom/twitter/library/widget/SlidingUpPanelLayout;
.super Landroid/view/ViewGroup;
.source "Twttr"


# instance fields
.field private A:F

.field private B:F

.field private C:F

.field private D:Z

.field private E:Z

.field private F:Landroid/view/View;

.field private G:Landroid/view/MotionEvent;

.field private final a:I

.field private final b:I

.field private c:I

.field private d:I

.field private final e:Landroid/graphics/Paint;

.field private f:Landroid/graphics/drawable/Drawable;

.field private g:I

.field private final h:I

.field private i:Z

.field private j:Landroid/view/View;

.field private k:Landroid/view/View;

.field private l:F

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:Z

.field private r:Z

.field private s:Lcom/twitter/library/widget/o;

.field private final t:Landroid/support/v4/widget/ViewDragHelper;

.field private u:Z

.field private final v:Landroid/graphics/Rect;

.field private final w:Landroid/os/Handler;

.field private final x:[I

.field private final y:[I

.field private z:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8

    const/4 v7, 0x2

    const/high16 v6, -0x67000000

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/high16 v3, 0x3f000000    # 0.5f

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v4, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->c:I

    iput v6, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->d:I

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->e:Landroid/graphics/Paint;

    iput-boolean v5, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->u:Z

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->v:Landroid/graphics/Rect;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->w:Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x42300000    # 44.0f

    mul-float/2addr v1, v0

    add-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->g:I

    const/high16 v1, 0x40800000    # 4.0f

    mul-float/2addr v1, v0

    add-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->h:I

    invoke-virtual {p0, v4}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->setWillNotDraw(Z)V

    new-instance v1, Lcom/twitter/library/widget/n;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/twitter/library/widget/n;-><init>(Lcom/twitter/library/widget/SlidingUpPanelLayout;Lcom/twitter/library/widget/m;)V

    invoke-static {p0, v3, v1}, Landroid/support/v4/widget/ViewDragHelper;->create(Landroid/view/ViewGroup;FLandroid/support/v4/widget/ViewDragHelper$Callback;)Landroid/support/v4/widget/ViewDragHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->t:Landroid/support/v4/widget/ViewDragHelper;

    iget-object v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->t:Landroid/support/v4/widget/ViewDragHelper;

    const/high16 v2, 0x43c80000    # 400.0f

    mul-float/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/ViewDragHelper;->setMinVelocity(F)V

    iput-boolean v5, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->i:Z

    iput-boolean v5, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->r:Z

    invoke-virtual {p0, v6}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->setCoveredFadeColor(I)V

    iput v4, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->o:I

    const/4 v0, 0x7

    iput v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->p:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->n:I

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    mul-int/2addr v0, v0

    iput v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->a:I

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->b:I

    new-array v0, v7, [I

    iput-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->x:[I

    new-array v0, v7, [I

    iput-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->y:[I

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/widget/SlidingUpPanelLayout;FI)I
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->b(FI)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/twitter/library/widget/SlidingUpPanelLayout;Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->G:Landroid/view/MotionEvent;

    return-object p1
.end method

.method public static a(Landroid/view/View;ZIII)Landroid/view/View;
    .locals 9

    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p0}, Landroid/view/View;->getScrollX()I

    move-result v3

    invoke-virtual {p0}, Landroid/view/View;->getScrollY()I

    move-result v4

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_0
    if-ltz v2, :cond_2

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    add-int v5, p3, v3

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v6

    if-lt v5, v6, :cond_1

    add-int v5, p3, v3

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v6

    if-ge v5, v6, :cond_1

    add-int v5, p4, v4

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v6

    if-lt v5, v6, :cond_1

    add-int v5, p4, v4

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v6

    if-ge v5, v6, :cond_1

    const/4 v5, 0x1

    add-int v6, p3, v3

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v7

    sub-int/2addr v6, v7

    add-int v7, p4, v4

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-static {v1, v5, p2, v6, v7}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->a(Landroid/view/View;ZIII)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    move-object p0, v1

    :cond_0
    :goto_1
    return-object p0

    :cond_1
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    neg-int v0, p2

    invoke-static {p0, v0}, Landroid/support/v4/view/ViewCompat;->canScrollVertically(Landroid/view/View;I)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_3
    const/4 p0, 0x0

    goto :goto_1
.end method

.method private a(Landroid/view/MotionEvent;)V
    .locals 3

    const/4 v2, 0x0

    invoke-static {p1}, Landroid/support/v4/view/MotionEventCompat;->getActionMasked(Landroid/view/MotionEvent;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->C:F

    :cond_0
    iget v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->C:F

    invoke-virtual {p1, v2, v0}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->t:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/ViewDragHelper;->processTouchEvent(Landroid/view/MotionEvent;)V

    iget v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->C:F

    neg-float v0, v0

    invoke-virtual {p1, v2, v0}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/widget/SlidingUpPanelLayout;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->d(I)V

    return-void
.end method

.method private a(FF)Z
    .locals 5

    const/4 v1, 0x0

    iget v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->A:F

    sub-float v0, p1, v0

    iget v2, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->B:F

    sub-float v2, p2, v2

    iget v3, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->o:I

    and-int/lit8 v3, v3, 0x6

    if-nez v3, :cond_0

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->w:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->removeMessages(I)V

    return v0

    :cond_0
    iget-object v3, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->w:Landroid/os/Handler;

    invoke-virtual {v3, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v3

    if-eqz v3, :cond_1

    iget v3, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->o:I

    if-eqz v3, :cond_1

    mul-float/2addr v0, v0

    mul-float/2addr v2, v2

    add-float/2addr v0, v2

    iget v2, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->a:I

    int-to-float v2, v2

    cmpg-float v0, v0, v2

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->t:Landroid/support/v4/widget/ViewDragHelper;

    iget-object v2, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    float-to-int v3, p1

    float-to-int v4, p2

    invoke-virtual {v0, v2, v3, v4}, Landroid/support/v4/widget/ViewDragHelper;->isViewUnder(Landroid/view/View;II)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->g()Z

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private a(Landroid/view/MotionEvent;III)Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->F:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->F:Landroid/view/View;

    neg-int v2, p2

    invoke-static {v1, v2}, Landroid/support/v4/view/ViewCompat;->canScrollVertically(Landroid/view/View;I)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    invoke-static {v1, v0, p2, p3, p4}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->a(Landroid/view/View;ZIII)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->F:Landroid/view/View;

    :cond_1
    iget-object v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->F:Landroid/view/View;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->t:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v1}, Landroid/support/v4/widget/ViewDragHelper;->getActivePointerId()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    :cond_2
    invoke-direct {p0, p1}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->b(Landroid/view/MotionEvent;)V

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->t:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v0}, Landroid/support/v4/widget/ViewDragHelper;->cancel()V

    const/4 v0, 0x1

    :cond_3
    return v0
.end method

.method static synthetic a(Lcom/twitter/library/widget/SlidingUpPanelLayout;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->q:Z

    return v0
.end method

.method private b(FI)I
    .locals 2

    move v0, p2

    :goto_0
    if-lez v0, :cond_0

    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    iget v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->p:I

    and-int/2addr v1, v0

    if-eqz v1, :cond_1

    :cond_0
    iget v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->p:I

    and-int/2addr v1, v0

    if-eqz v1, :cond_3

    :goto_1
    return v0

    :cond_1
    const/4 v1, 0x0

    cmpl-float v1, p1, v1

    if-lez v1, :cond_2

    shr-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    shl-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->o:I

    goto :goto_1
.end method

.method static synthetic b(Lcom/twitter/library/widget/SlidingUpPanelLayout;I)I
    .locals 0

    iput p1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->o:I

    return p1
.end method

.method static synthetic b(Lcom/twitter/library/widget/SlidingUpPanelLayout;)Landroid/support/v4/widget/ViewDragHelper;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->t:Landroid/support/v4/widget/ViewDragHelper;

    return-object v0
.end method

.method private b(Landroid/view/MotionEvent;)V
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getScrollX()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getScrollY()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    iget-object v2, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    invoke-virtual {v2, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    return-void
.end method

.method private static b(Landroid/view/View;)Z
    .locals 2

    invoke-virtual {p0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/library/widget/SlidingUpPanelLayout;)F
    .locals 1

    iget v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->l:F

    return v0
.end method

.method private c(Landroid/view/MotionEvent;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->t:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v0}, Landroid/support/v4/widget/ViewDragHelper;->getActivePointerId()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->a(Landroid/view/MotionEvent;)V

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->G:Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->G:Landroid/view/MotionEvent;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->setAction(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->F:Landroid/view/View;

    return-void
.end method

.method private c(I)Z
    .locals 1

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->c()Z

    move-result v0

    :goto_0
    return v0

    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->f()Z

    move-result v0

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->e()Z

    move-result v0

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->d()Z

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic d(Lcom/twitter/library/widget/SlidingUpPanelLayout;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    return-object v0
.end method

.method private d(I)V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getMeasuredHeight()I

    move-result v0

    iget v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->n:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->m:I

    if-eqz v1, :cond_0

    sub-int v0, p1, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->m:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    :goto_0
    iput v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->l:F

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->a(Landroid/view/View;)V

    return-void

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method static synthetic e(Lcom/twitter/library/widget/SlidingUpPanelLayout;)Landroid/view/MotionEvent;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->G:Landroid/view/MotionEvent;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/library/widget/SlidingUpPanelLayout;)I
    .locals 1

    iget v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->o:I

    return v0
.end method

.method static synthetic g(Lcom/twitter/library/widget/SlidingUpPanelLayout;)I
    .locals 1

    iget v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->p:I

    return v0
.end method

.method private g()Z
    .locals 2

    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->o:I

    shr-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->b(FI)I

    move-result v0

    iget v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->o:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    invoke-direct {p0, v0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->c(I)Z

    move-result v0

    return v0
.end method

.method static synthetic h(Lcom/twitter/library/widget/SlidingUpPanelLayout;)I
    .locals 1

    iget v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->g:I

    return v0
.end method

.method static synthetic i(Lcom/twitter/library/widget/SlidingUpPanelLayout;)I
    .locals 1

    iget v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->n:I

    return v0
.end method


# virtual methods
.method a()V
    .locals 11

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getPaddingLeft()I

    move-result v5

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getPaddingRight()I

    move-result v2

    sub-int v6, v0, v2

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getPaddingTop()I

    move-result v7

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getPaddingBottom()I

    move-result v2

    sub-int v8, v0, v2

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    invoke-static {v0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->b(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v4

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v3

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    :goto_1
    invoke-virtual {p0, v1}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/View;->getLeft()I

    move-result v10

    invoke-static {v5, v10}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-virtual {v9}, Landroid/view/View;->getTop()I

    move-result v10

    invoke-static {v7, v10}, Ljava/lang/Math;->max(II)I

    move-result v7

    invoke-virtual {v9}, Landroid/view/View;->getRight()I

    move-result v10

    invoke-static {v6, v10}, Ljava/lang/Math;->min(II)I

    move-result v6

    invoke-virtual {v9}, Landroid/view/View;->getBottom()I

    move-result v10

    invoke-static {v8, v10}, Ljava/lang/Math;->min(II)I

    move-result v8

    if-lt v5, v4, :cond_1

    if-lt v7, v2, :cond_1

    if-gt v6, v3, :cond_1

    if-gt v8, v0, :cond_1

    const/4 v1, 0x4

    :cond_1
    invoke-virtual {v9, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public a(I)V
    .locals 1

    iget v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->p:I

    or-int/2addr v0, p1

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->setPossiblePanelStates(I)V

    return-void
.end method

.method a(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->s:Lcom/twitter/library/widget/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->s:Lcom/twitter/library/widget/o;

    iget v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->l:F

    invoke-interface {v0, p1, v1}, Lcom/twitter/library/widget/o;->a(Landroid/view/View;F)V

    :cond_0
    return-void
.end method

.method a(Landroid/view/View;I)V
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->s:Lcom/twitter/library/widget/o;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    packed-switch p2, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_0

    const/16 v0, 0x20

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->sendAccessibilityEvent(I)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->s:Lcom/twitter/library/widget/o;

    invoke-interface {v1, p1}, Lcom/twitter/library/widget/o;->a(Landroid/view/View;)V

    goto :goto_1

    :pswitch_2
    iget-object v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->s:Lcom/twitter/library/widget/o;

    invoke-interface {v1, p1}, Lcom/twitter/library/widget/o;->d(Landroid/view/View;)V

    goto :goto_1

    :pswitch_3
    iget-object v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->s:Lcom/twitter/library/widget/o;

    invoke-interface {v1, p1}, Lcom/twitter/library/widget/o;->c(Landroid/view/View;)V

    goto :goto_1

    :pswitch_4
    iget-object v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->s:Lcom/twitter/library/widget/o;

    invoke-interface {v1, p1}, Lcom/twitter/library/widget/o;->b(Landroid/view/View;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method a(FI)Z
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->i:Z

    if-nez v0, :cond_0

    :goto_0
    return v2

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getMeasuredHeight()I

    move-result v0

    iget v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->n:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->m:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    float-to-int v0, v0

    invoke-virtual {p0, v0, p2}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->a(II)Z

    goto :goto_0
.end method

.method a(II)Z
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->i:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->t:Landroid/support/v4/widget/ViewDragHelper;

    iget-object v2, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    iget-object v3, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v3

    invoke-virtual {v1, v2, v3, p1}, Landroid/support/v4/widget/ViewDragHelper;->smoothSlideViewTo(Landroid/view/View;II)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->b()V

    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method b()V
    .locals 5

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_0

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public b(I)V
    .locals 2

    iget v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->p:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->setPossiblePanelStates(I)V

    return-void
.end method

.method public c()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->o:I

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iput v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->o:I

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->clearFocus()V

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getBottom()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->a(II)Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    instance-of v0, p1, Lcom/twitter/library/widget/SlidingUpPanelLayout$LayoutParams;

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public computeScroll()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->t:Landroid/support/v4/widget/ViewDragHelper;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/ViewDragHelper;->continueSettling(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->i:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->t:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v0}, Landroid/support/v4/widget/ViewDragHelper;->abort()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    goto :goto_0
.end method

.method public d()Z
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->o:I

    if-eq v2, v0, :cond_0

    iget v2, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->p:I

    and-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    iput v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->o:I

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {p0, v2, v1}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->a(FI)Z

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->requestFocus()Z

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->o:I

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->g()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 5

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    iget v2, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->h:I

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    iget-object v3, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v3

    iget-object v4, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v3, v1, v0, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 7

    const/4 v6, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/SlidingUpPanelLayout$LayoutParams;

    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->save(I)I

    move-result v3

    const/4 v1, 0x0

    iget-boolean v4, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->i:Z

    if-eqz v4, :cond_4

    iget-boolean v0, v0, Lcom/twitter/library/widget/SlidingUpPanelLayout$LayoutParams;->a:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->v:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->getClipBounds(Landroid/graphics/Rect;)Z

    iget-boolean v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->r:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->v:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->v:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    iget-object v5, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    iput v4, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->v:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    :cond_0
    iget v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->l:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v1

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->c:I

    if-nez v0, :cond_2

    iget v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->l:F

    invoke-static {v6, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    sub-float v0, v2, v0

    :goto_1
    iget v2, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->d:I

    const/high16 v3, -0x1000000

    and-int/2addr v2, v3

    ushr-int/lit8 v2, v2, 0x18

    int-to-float v2, v2

    mul-float/2addr v0, v2

    float-to-int v0, v0

    shl-int/lit8 v0, v0, 0x18

    iget v2, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->d:I

    const v3, 0xffffff

    and-int/2addr v2, v3

    or-int/2addr v0, v2

    iget-object v2, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->e:Landroid/graphics/Paint;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->v:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_1
    return v1

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getMeasuredHeight()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v3, v0, v6

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    int-to-float v3, v3

    div-float v0, v3, v0

    invoke-static {v6, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    sub-float v0, v2, v0

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public e()Z
    .locals 3

    const/4 v2, 0x2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->o:I

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->p:I

    and-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iput v2, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->o:I

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->a(FI)Z

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->requestFocus()Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public f()Z
    .locals 3

    const/4 v2, 0x4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->o:I

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->p:I

    and-int/lit8 v1, v1, 0x4

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iput v2, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->o:I

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getTop()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->a(II)Z

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->requestFocus()Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    new-instance v0, Lcom/twitter/library/widget/SlidingUpPanelLayout$LayoutParams;

    invoke-direct {v0}, Lcom/twitter/library/widget/SlidingUpPanelLayout$LayoutParams;-><init>()V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    new-instance v0, Lcom/twitter/library/widget/SlidingUpPanelLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/twitter/library/widget/SlidingUpPanelLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/twitter/library/widget/SlidingUpPanelLayout$LayoutParams;

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p1}, Lcom/twitter/library/widget/SlidingUpPanelLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/twitter/library/widget/SlidingUpPanelLayout$LayoutParams;

    invoke-direct {v0, p1}, Lcom/twitter/library/widget/SlidingUpPanelLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public getCoveredFadeColor()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->d:I

    return v0
.end method

.method public getPanelPeekHeight()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->g:I

    return v0
.end method

.method public getPanelPreviewHeight()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->n:I

    return v0
.end method

.method public getPanelState()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->o:I

    return v0
.end method

.method public getPossibleStates()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->p:I

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->u:Z

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->u:Z

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->G:Landroid/view/MotionEvent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->G:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->G:Landroid/view/MotionEvent;

    :cond_0
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p1}, Landroid/support/v4/view/MotionEventCompat;->getActionMasked(Landroid/view/MotionEvent;)I

    move-result v0

    iget-boolean v3, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->i:Z

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->q:Z

    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->t:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v0}, Landroid/support/v4/widget/ViewDragHelper;->cancel()V

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    :cond_1
    :goto_0
    return v2

    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    if-le v3, v1, :cond_3

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->t:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v0}, Landroid/support/v4/widget/ViewDragHelper;->cancel()V

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    packed-switch v0, :pswitch_data_0

    move v0, v2

    :goto_1
    iget-boolean v3, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->D:Z

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->t:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v3, p1}, Landroid/support/v4/widget/ViewDragHelper;->shouldInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    if-eqz v3, :cond_8

    move v3, v1

    :goto_2
    if-nez v0, :cond_4

    if-eqz v3, :cond_1

    :cond_4
    move v2, v1

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->C:F

    iput-boolean v2, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->q:Z

    iput-boolean v2, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->E:Z

    iput v4, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->z:F

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->w:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->o:I

    packed-switch v0, :pswitch_data_1

    :pswitch_1
    iput-boolean v2, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->D:Z

    move v0, v2

    goto :goto_1

    :pswitch_2
    iput-boolean v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->D:Z

    move v0, v1

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->t:Landroid/support/v4/widget/ViewDragHelper;

    iget-object v5, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    float-to-int v6, v3

    float-to-int v7, v4

    invoke-virtual {v0, v5, v6, v7}, Landroid/support/v4/widget/ViewDragHelper;->isViewUnder(Landroid/view/View;II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->D:Z

    iget-boolean v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->D:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->j:Landroid/view/View;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->x:[I

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getLocationOnScreen([I)V

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->j:Landroid/view/View;

    iget-object v5, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->y:[I

    invoke-virtual {v0, v5}, Landroid/view/View;->getLocationOnScreen([I)V

    float-to-int v0, v3

    iget-object v3, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->y:[I

    aget v3, v3, v2

    iget-object v5, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->x:[I

    aget v5, v5, v2

    sub-int/2addr v3, v5

    sub-int/2addr v0, v3

    float-to-int v3, v4

    iget-object v4, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->y:[I

    aget v4, v4, v1

    iget-object v5, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->x:[I

    aget v5, v5, v1

    sub-int/2addr v4, v5

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->t:Landroid/support/v4/widget/ViewDragHelper;

    iget-object v5, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->j:Landroid/view/View;

    invoke-virtual {v4, v5, v0, v3}, Landroid/support/v4/widget/ViewDragHelper;->isViewUnder(Landroid/view/View;II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->D:Z

    :cond_5
    iget-boolean v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->D:Z

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->w:Landroid/os/Handler;

    iget v3, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->b:I

    int-to-long v3, v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    iget v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->o:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_7

    move v0, v1

    goto :goto_1

    :cond_7
    move v0, v2

    goto/16 :goto_1

    :pswitch_4
    invoke-direct {p0, v3, v4}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->a(FF)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->E:Z

    goto/16 :goto_1

    :cond_8
    move v3, v2

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 10

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getPaddingLeft()I

    move-result v4

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getChildCount()I

    move-result v5

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getBottom()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getPaddingBottom()I

    move-result v2

    sub-int v2, v0, v2

    const/4 v0, 0x0

    move v3, v0

    move v0, v2

    move v2, v1

    :goto_0
    if-ge v3, v5, :cond_2

    invoke-virtual {p0, v3}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getVisibility()I

    move-result v7

    const/16 v8, 0x8

    if-ne v7, v8, :cond_0

    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/SlidingUpPanelLayout$LayoutParams;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    iget-boolean v0, v0, Lcom/twitter/library/widget/SlidingUpPanelLayout$LayoutParams;->a:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getMeasuredHeight()I

    move-result v0

    iget v8, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->n:I

    iget v9, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->g:I

    sub-int/2addr v8, v9

    iput v8, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->m:I

    iget v8, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->o:I

    packed-switch v8, :pswitch_data_0

    :goto_2
    :pswitch_0
    add-int/2addr v7, v0

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    add-int/2addr v8, v4

    invoke-virtual {v6, v4, v0, v8, v7}, Landroid/view/View;->layout(IIII)V

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    add-int/2addr v2, v6

    goto :goto_1

    :pswitch_1
    iget v8, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->g:I

    sub-int/2addr v0, v8

    goto :goto_2

    :pswitch_2
    iget v8, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->n:I

    sub-int/2addr v0, v8

    goto :goto_2

    :pswitch_3
    move v0, v1

    goto :goto_2

    :cond_1
    move v0, v2

    goto :goto_2

    :cond_2
    iget-boolean v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->u:Z

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->m:I

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getMeasuredHeight()I

    move-result v1

    iget v2, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->n:I

    sub-int/2addr v1, v2

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->m:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    :goto_3
    iput v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->l:F

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->a()V

    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->u:Z

    return-void

    :cond_4
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected onMeasure(II)V
    .locals 11

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    const/high16 v2, 0x40000000    # 2.0f

    if-eq v0, v2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Width must have an exact value or MATCH_PARENT"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/high16 v0, 0x40000000    # 2.0f

    if-eq v1, v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Height must have an exact value or MATCH_PARENT"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getPaddingTop()I

    move-result v0

    sub-int v0, v6, v0

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getPaddingBottom()I

    move-result v1

    sub-int v2, v0, v1

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getChildCount()I

    move-result v7

    const/4 v0, 0x2

    if-le v7, v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "onMeasure: More than two child views are not supported."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->i:Z

    iget v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->n:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    div-int/lit8 v0, v2, 0x2

    iput v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->n:I

    :cond_3
    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v7, :cond_b

    invoke-virtual {p0, v4}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/SlidingUpPanelLayout$LayoutParams;

    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v3, 0x8

    if-ne v1, v3, :cond_4

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/twitter/library/widget/SlidingUpPanelLayout$LayoutParams;->b:Z

    :goto_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_4
    const/4 v1, 0x1

    if-ne v4, v1, :cond_c

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/twitter/library/widget/SlidingUpPanelLayout$LayoutParams;->a:Z

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/twitter/library/widget/SlidingUpPanelLayout$LayoutParams;->b:Z

    iput-object v8, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->i:Z

    iget v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->p:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_5

    move v1, v2

    :goto_2
    iget v3, v0, Lcom/twitter/library/widget/SlidingUpPanelLayout$LayoutParams;->width:I

    const/4 v9, -0x2

    if-ne v3, v9, :cond_7

    const/high16 v3, -0x80000000

    invoke-static {v5, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    :goto_3
    iget v9, v0, Lcom/twitter/library/widget/SlidingUpPanelLayout$LayoutParams;->height:I

    const/4 v10, -0x2

    if-ne v9, v10, :cond_9

    const/high16 v0, -0x80000000

    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    :goto_4
    invoke-virtual {v8, v3, v0}, Landroid/view/View;->measure(II)V

    goto :goto_1

    :cond_5
    iget v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->p:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_6

    iget v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->n:I

    goto :goto_2

    :cond_6
    iget v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->g:I

    goto :goto_2

    :cond_7
    iget v3, v0, Lcom/twitter/library/widget/SlidingUpPanelLayout$LayoutParams;->width:I

    const/4 v9, -0x1

    if-ne v3, v9, :cond_8

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v5, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    goto :goto_3

    :cond_8
    iget v3, v0, Lcom/twitter/library/widget/SlidingUpPanelLayout$LayoutParams;->width:I

    const/high16 v9, 0x40000000    # 2.0f

    invoke-static {v3, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    goto :goto_3

    :cond_9
    iget v9, v0, Lcom/twitter/library/widget/SlidingUpPanelLayout$LayoutParams;->height:I

    const/4 v10, -0x1

    if-ne v9, v10, :cond_a

    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_4

    :cond_a
    iget v0, v0, Lcom/twitter/library/widget/SlidingUpPanelLayout$LayoutParams;->height:I

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_4

    :cond_b
    invoke-virtual {p0, v5, v6}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->setMeasuredDimension(II)V

    return-void

    :cond_c
    move v1, v2

    goto :goto_2
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    check-cast p1, Lcom/twitter/library/widget/SlidingUpPanelLayout$SavedState;

    invoke-virtual {p1}, Lcom/twitter/library/widget/SlidingUpPanelLayout$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget v0, p1, Lcom/twitter/library/widget/SlidingUpPanelLayout$SavedState;->a:I

    iput v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->o:I

    iget v0, p1, Lcom/twitter/library/widget/SlidingUpPanelLayout$SavedState;->b:I

    iput v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->p:I

    iget-boolean v0, p1, Lcom/twitter/library/widget/SlidingUpPanelLayout$SavedState;->c:Z

    iput-boolean v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->i:Z

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/widget/SlidingUpPanelLayout$SavedState;

    invoke-direct {v1, v0}, Lcom/twitter/library/widget/SlidingUpPanelLayout$SavedState;-><init>(Landroid/os/Parcelable;)V

    iget v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->o:I

    iput v0, v1, Lcom/twitter/library/widget/SlidingUpPanelLayout$SavedState;->a:I

    iget v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->p:I

    iput v0, v1, Lcom/twitter/library/widget/SlidingUpPanelLayout$SavedState;->b:I

    iget-boolean v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->i:Z

    iput-boolean v0, v1, Lcom/twitter/library/widget/SlidingUpPanelLayout$SavedState;->c:Z

    return-object v1
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    if-eq p2, p4, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->u:Z

    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->E:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->i:Z

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->o:I

    if-nez v1, :cond_3

    :cond_2
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->j:Landroid/view/View;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->t:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v1, p1}, Landroid/support/v4/widget/ViewDragHelper;->processTouchEvent(Landroid/view/MotionEvent;)V

    invoke-direct {p0, p1}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->b(Landroid/view/MotionEvent;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    if-le v1, v0, :cond_5

    iget-object v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->t:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v0}, Landroid/support/v4/widget/ViewDragHelper;->cancel()V

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    :cond_5
    invoke-static {p1}, Landroid/support/v4/view/MotionEventCompat;->getActionMasked(Landroid/view/MotionEvent;)I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_6

    if-eq v1, v0, :cond_6

    invoke-direct {p0, p1}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->a(Landroid/view/MotionEvent;)V

    invoke-direct {p0, p1}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->b(Landroid/view/MotionEvent;)V

    :cond_6
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->F:Landroid/view/View;

    iput v3, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->z:F

    iput v2, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->A:F

    iput v3, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->B:F

    goto :goto_0

    :pswitch_1
    iget v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->z:F

    sub-float v1, v3, v1

    iput v3, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->z:F

    iget-boolean v4, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->D:Z

    if-nez v4, :cond_7

    invoke-direct {p0, p1}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->a(Landroid/view/MotionEvent;)V

    goto :goto_0

    :cond_7
    iget-object v4, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getPaddingTop()I

    move-result v5

    if-le v4, v5, :cond_8

    invoke-direct {p0, p1}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->c(Landroid/view/MotionEvent;)V

    goto :goto_0

    :cond_8
    float-to-int v1, v1

    float-to-int v2, v2

    float-to-int v3, v3

    invoke-direct {p0, p1, v1, v2, v3}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->a(Landroid/view/MotionEvent;III)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, p1}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->c(Landroid/view/MotionEvent;)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, v2, v3}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->a(FF)Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->t:Landroid/support/v4/widget/ViewDragHelper;

    invoke-virtual {v1}, Landroid/support/v4/widget/ViewDragHelper;->cancel()V

    const/4 v1, 0x3

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    iget-object v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->k:Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    goto/16 :goto_0

    :cond_9
    invoke-direct {p0, p1}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->a(Landroid/view/MotionEvent;)V

    invoke-direct {p0, p1}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->b(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->w:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .locals 0

    return-void
.end method

.method public setClipChildren(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->r:Z

    return-void
.end method

.method public setCoveredFadeColor(I)V
    .locals 0

    iput p1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->d:I

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->invalidate()V

    return-void
.end method

.method public setDragView(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->j:Landroid/view/View;

    return-void
.end method

.method public setFadeMode(I)V
    .locals 0

    iput p1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->c:I

    return-void
.end method

.method public setPadding(IIII)V
    .locals 1

    const/4 v0, 0x0

    invoke-super {p0, v0, v0, v0, v0}, Landroid/view/ViewGroup;->setPadding(IIII)V

    return-void
.end method

.method public setPaddingRelative(IIII)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    const/4 v0, 0x0

    invoke-super {p0, v0, v0, v0, v0}, Landroid/view/ViewGroup;->setPaddingRelative(IIII)V

    return-void
.end method

.method public setPanelPeekHeight(I)V
    .locals 0

    iput p1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->g:I

    return-void
.end method

.method public setPanelPreviewHeight(I)V
    .locals 0

    iput p1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->n:I

    return-void
.end method

.method public setPanelSlideListener(Lcom/twitter/library/widget/o;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->s:Lcom/twitter/library/widget/o;

    return-void
.end method

.method public setPanelVisiblity(I)V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->requestLayout()V

    goto :goto_0
.end method

.method public setPossiblePanelStates(I)V
    .locals 2

    iput p1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->p:I

    iget v0, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->o:I

    iget v1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->p:I

    and-int/2addr v0, v1

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/widget/SlidingUpPanelLayout;->c()Z

    :cond_0
    return-void
.end method

.method public setShadowDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/widget/SlidingUpPanelLayout;->f:Landroid/graphics/drawable/Drawable;

    return-void
.end method
