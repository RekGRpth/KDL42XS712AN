.class synthetic Lcom/twitter/library/widget/w;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field static final synthetic a:[I

.field static final synthetic b:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/twitter/library/view/TweetActionType;->values()[Lcom/twitter/library/view/TweetActionType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/twitter/library/widget/w;->b:[I

    :try_start_0
    sget-object v0, Lcom/twitter/library/widget/w;->b:[I

    sget-object v1, Lcom/twitter/library/view/TweetActionType;->b:Lcom/twitter/library/view/TweetActionType;

    invoke-virtual {v1}, Lcom/twitter/library/view/TweetActionType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_7

    :goto_0
    :try_start_1
    sget-object v0, Lcom/twitter/library/widget/w;->b:[I

    sget-object v1, Lcom/twitter/library/view/TweetActionType;->c:Lcom/twitter/library/view/TweetActionType;

    invoke-virtual {v1}, Lcom/twitter/library/view/TweetActionType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_6

    :goto_1
    :try_start_2
    sget-object v0, Lcom/twitter/library/widget/w;->b:[I

    sget-object v1, Lcom/twitter/library/view/TweetActionType;->e:Lcom/twitter/library/view/TweetActionType;

    invoke-virtual {v1}, Lcom/twitter/library/view/TweetActionType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_5

    :goto_2
    :try_start_3
    sget-object v0, Lcom/twitter/library/widget/w;->b:[I

    sget-object v1, Lcom/twitter/library/view/TweetActionType;->d:Lcom/twitter/library/view/TweetActionType;

    invoke-virtual {v1}, Lcom/twitter/library/view/TweetActionType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_4

    :goto_3
    invoke-static {}, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->values()[Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/twitter/library/widget/w;->a:[I

    :try_start_4
    sget-object v0, Lcom/twitter/library/widget/w;->a:[I

    sget-object v1, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->d:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    invoke-virtual {v1}, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_3

    :goto_4
    :try_start_5
    sget-object v0, Lcom/twitter/library/widget/w;->a:[I

    sget-object v1, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->c:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    invoke-virtual {v1}, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_2

    :goto_5
    :try_start_6
    sget-object v0, Lcom/twitter/library/widget/w;->a:[I

    sget-object v1, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->b:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    invoke-virtual {v1}, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_1

    :goto_6
    :try_start_7
    sget-object v0, Lcom/twitter/library/widget/w;->a:[I

    sget-object v1, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->a:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    invoke-virtual {v1}, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_0

    :goto_7
    return-void

    :catch_0
    move-exception v0

    goto :goto_7

    :catch_1
    move-exception v0

    goto :goto_6

    :catch_2
    move-exception v0

    goto :goto_5

    :catch_3
    move-exception v0

    goto :goto_4

    :catch_4
    move-exception v0

    goto :goto_3

    :catch_5
    move-exception v0

    goto :goto_2

    :catch_6
    move-exception v0

    goto :goto_1

    :catch_7
    move-exception v0

    goto :goto_0
.end method
