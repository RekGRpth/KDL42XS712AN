.class public Lcom/twitter/library/widget/TweetView;
.super Landroid/view/ViewGroup;
.source "Twttr"

# interfaces
.implements Lcom/twitter/internal/android/widget/p;


# static fields
.field private static final c:[I

.field private static final d:[I

.field private static final e:[I

.field private static final f:[I

.field private static final g:[I

.field private static final h:[I

.field private static final i:[I

.field private static final j:[I

.field private static final k:[I

.field private static final l:[I

.field private static final m:Landroid/text/TextPaint;

.field private static final n:[Lcom/twitter/library/view/TweetActionType;

.field private static final o:[Lcom/twitter/library/view/TweetActionType;


# instance fields
.field private final A:F

.field private final B:I

.field private final C:I

.field private final D:I

.field private final E:I

.field private final F:I

.field private final G:I

.field private final H:I

.field private final I:I

.field private final J:I

.field private final K:I

.field private final L:I

.field private final M:I

.field private final N:I

.field private final O:I

.field private final P:I

.field private final Q:I

.field private final R:I

.field private final S:I

.field private final T:I

.field private final U:I

.field private final V:I

.field private final W:I

.field private final Z:I

.field private final aA:I

.field private final aB:I

.field private final aC:I

.field private final aD:I

.field private final aE:I

.field private final aF:I

.field private final aG:I

.field private final aH:I

.field private final aI:I

.field private final aJ:I

.field private final aK:I

.field private final aL:I

.field private final aM:I

.field private final aN:I

.field private final aO:I

.field private final aP:I

.field private final aQ:I

.field private final aR:I

.field private final aS:I

.field private final aT:I

.field private final aU:I

.field private final aV:I

.field private final aW:I

.field private final aX:I

.field private final aY:I

.field private final aZ:I

.field private final aa:I

.field private final ab:I

.field private final ac:I

.field private final ad:I

.field private final ae:I

.field private final af:I

.field private final ag:I

.field private final ah:I

.field private final ai:I

.field private final aj:I

.field private final ak:I

.field private final al:I

.field private final am:I

.field private final an:I

.field private final ao:I

.field private final ap:I

.field private final aq:I

.field private final ar:I

.field private final as:I

.field private final at:I

.field private final au:I

.field private final av:I

.field private final aw:I

.field private final ax:I

.field private final ay:I

.field private final az:I

.field protected b:Lcom/twitter/library/widget/aa;

.field private final bA:Z

.field private final bB:Z

.field private final bC:Landroid/content/res/ColorStateList;

.field private final bD:Lcom/twitter/internal/android/widget/ax;

.field private final bE:Landroid/graphics/Rect;

.field private final bF:Z

.field private final bG:Landroid/os/Handler;

.field private final bH:Ljava/lang/Runnable;

.field private final bI:Lcom/twitter/library/widget/TweetMediaImagesView;

.field private final bJ:Landroid/graphics/drawable/Drawable;

.field private final bK:I

.field private final bL:I

.field private final bM:I

.field private bN:Lcom/twitter/library/provider/Tweet;

.field private bO:Lcom/twitter/library/widget/ap;

.field private bP:Lcom/twitter/library/util/FriendshipCache;

.field private bQ:Landroid/text/StaticLayout;

.field private bR:I

.field private bS:Landroid/text/StaticLayout;

.field private bT:Landroid/text/StaticLayout;

.field private bU:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;

.field private bV:I

.field private bW:Landroid/text/StaticLayout;

.field private bX:Landroid/text/StaticLayout;

.field private bY:Landroid/text/StaticLayout;

.field private bZ:Landroid/text/StaticLayout;

.field private final ba:I

.field private final bb:I

.field private final bc:I

.field private final bd:I

.field private final be:I

.field private final bf:I

.field private final bg:I

.field private final bh:I

.field private final bi:I

.field private final bj:I

.field private final bk:I

.field private final bl:Landroid/graphics/drawable/Drawable;

.field private final bm:Landroid/graphics/drawable/Drawable;

.field private final bn:Landroid/graphics/drawable/Drawable;

.field private final bo:Landroid/graphics/drawable/Drawable;

.field private final bp:I

.field private final bq:Ljava/util/ArrayList;

.field private final br:I

.field private final bs:I

.field private final bt:I

.field private final bu:Landroid/graphics/drawable/Drawable;

.field private final bv:F

.field private final bw:F

.field private final bx:F

.field private final by:Landroid/graphics/drawable/Drawable;

.field private final bz:Landroid/graphics/drawable/Drawable;

.field private cA:I

.field private cB:I

.field private cC:I

.field private cD:F

.field private cE:Ljava/lang/String;

.field private cF:Ljava/lang/CharSequence;

.field private cG:Ljava/lang/CharSequence;

.field private cH:Ljava/util/List;

.field private cI:F

.field private cJ:I

.field private cK:I

.field private cL:I

.field private cM:I

.field private cN:Landroid/graphics/drawable/Drawable;

.field private cO:Landroid/graphics/drawable/Drawable;

.field private cP:Landroid/graphics/drawable/Drawable;

.field private cQ:Landroid/graphics/drawable/Drawable;

.field private cR:Landroid/graphics/drawable/Drawable;

.field private cS:Landroid/graphics/drawable/Drawable;

.field private cT:Landroid/graphics/drawable/BitmapDrawable;

.field private cU:Landroid/graphics/drawable/Drawable;

.field private cV:Ljava/lang/String;

.field private cW:Ljava/lang/String;

.field private cX:Ljava/lang/String;

.field private cY:Landroid/graphics/Rect;

.field private cZ:Landroid/graphics/RectF;

.field private ca:Landroid/text/StaticLayout;

.field private cb:Landroid/text/StaticLayout;

.field private cc:Landroid/text/StaticLayout;

.field private cd:Landroid/text/StaticLayout;

.field private ce:Landroid/text/StaticLayout;

.field private cf:Landroid/text/StaticLayout;

.field private cg:Landroid/text/StaticLayout;

.field private ch:Landroid/text/StaticLayout;

.field private ci:Landroid/graphics/RectF;

.field private cj:I

.field private ck:Landroid/text/StaticLayout;

.field private cl:I

.field private cm:Landroid/text/StaticLayout;

.field private cn:I

.field private co:Landroid/graphics/RectF;

.field private cp:Lcom/twitter/library/widget/al;

.field private cq:Landroid/graphics/drawable/Drawable;

.field private cr:Landroid/graphics/RectF;

.field private cs:Lcom/twitter/library/widget/al;

.field private ct:Landroid/graphics/drawable/Drawable;

.field private cu:I

.field private cv:I

.field private cw:I

.field private cx:I

.field private cy:I

.field private cz:I

.field private dA:Ljava/lang/String;

.field private dB:Ljava/lang/String;

.field private dC:Ljava/lang/String;

.field private dD:Ljava/lang/String;

.field private dE:I

.field private dF:I

.field private dG:I

.field private dH:I

.field private dI:I

.field private dJ:I

.field private dK:I

.field private dL:I

.field private dM:I

.field private dN:I

.field private dO:I

.field private dP:Z

.field private dQ:I

.field private dR:Z

.field private dS:Z

.field private dT:Z

.field private dU:Z

.field private dV:Z

.field private dW:Z

.field private dX:Z

.field private dY:Z

.field private dZ:Z

.field private da:Landroid/graphics/RectF;

.field private db:Landroid/graphics/Rect;

.field private dc:Landroid/graphics/Rect;

.field private dd:Landroid/graphics/RectF;

.field private de:Landroid/graphics/RectF;

.field private df:Landroid/graphics/Rect;

.field private dg:Lcom/twitter/library/widget/al;

.field private dh:I

.field private di:Landroid/graphics/RectF;

.field private dj:Landroid/graphics/RectF;

.field private dk:Landroid/graphics/RectF;

.field private dl:Landroid/graphics/RectF;

.field private dm:Z

.field private dn:Ljava/util/List;

.field private do:Ljava/util/List;

.field private dp:Z

.field private dq:Z

.field private dr:Z

.field private ds:Z

.field private dt:Z

.field private du:Ljava/lang/String;

.field private dv:Ljava/lang/String;

.field private dw:Ljava/lang/String;

.field private dx:Lcom/twitter/library/view/TweetActionType;

.field private dy:Ljava/util/LinkedHashMap;

.field private dz:Ljava/lang/String;

.field private eA:I

.field private eB:Lcom/twitter/library/view/l;

.field private eC:Lcom/twitter/library/scribe/ScribeItem;

.field private eD:Z

.field private eE:I

.field private eF:Z

.field private eG:Landroid/graphics/Rect;

.field private eH:Lcom/twitter/library/card/k;

.field private eI:Z

.field private eJ:Z

.field private eK:I

.field private eL:Lcom/twitter/library/widget/al;

.field private eM:Lcom/twitter/library/widget/al;

.field private ea:Z

.field private eb:I

.field private ec:Z

.field private ed:Z

.field private ee:Landroid/graphics/Rect;

.field private ef:Landroid/graphics/Rect;

.field private eg:Landroid/graphics/RectF;

.field private eh:Landroid/graphics/RectF;

.field private ei:Z

.field private ej:Z

.field private ek:Z

.field private el:Z

.field private em:Lcom/twitter/library/widget/al;

.field private en:Z

.field private eo:Ljava/lang/String;

.field private ep:Landroid/text/StaticLayout;

.field private eq:Lcom/twitter/library/widget/al;

.field private er:Landroid/content/Intent;

.field private es:I

.field private et:Z

.field private eu:I

.field private ev:I

.field private ew:Ljava/lang/String;

.field private ex:I

.field private ey:Z

.field private ez:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

.field private final p:Landroid/content/res/ColorStateList;

.field private final q:Landroid/content/res/ColorStateList;

.field private final r:Landroid/content/res/ColorStateList;

.field private final s:Landroid/content/res/ColorStateList;

.field private final t:Landroid/content/res/ColorStateList;

.field private final u:Landroid/content/res/ColorStateList;

.field private final v:Landroid/content/res/ColorStateList;

.field private final w:Landroid/content/res/ColorStateList;

.field private final x:Landroid/content/res/ColorStateList;

.field private final y:Landroid/content/res/ColorStateList;

.field private final z:Landroid/content/res/ColorStateList;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v0, v2, [I

    sget v1, Lib;->state_avatar_pressed:I

    aput v1, v0, v3

    sput-object v0, Lcom/twitter/library/widget/TweetView;->c:[I

    new-array v0, v2, [I

    sget v1, Lib;->state_media_pressed:I

    aput v1, v0, v3

    sput-object v0, Lcom/twitter/library/widget/TweetView;->d:[I

    new-array v0, v2, [I

    sget v1, Lib;->state_badge_media:I

    aput v1, v0, v3

    sput-object v0, Lcom/twitter/library/widget/TweetView;->e:[I

    new-array v0, v2, [I

    sget v1, Lib;->state_badge_cluster_pressed:I

    aput v1, v0, v3

    sput-object v0, Lcom/twitter/library/widget/TweetView;->f:[I

    new-array v0, v2, [I

    sget v1, Lib;->state_promoted_action_pressed:I

    aput v1, v0, v3

    sput-object v0, Lcom/twitter/library/widget/TweetView;->g:[I

    new-array v0, v2, [I

    sget v1, Lib;->state_attribution_pressed:I

    aput v1, v0, v3

    sput-object v0, Lcom/twitter/library/widget/TweetView;->h:[I

    new-array v0, v2, [I

    sget v1, Lib;->state_dismiss_pressed:I

    aput v1, v0, v3

    sput-object v0, Lcom/twitter/library/widget/TweetView;->i:[I

    new-array v0, v2, [I

    sget v1, Lib;->state_pivot_pressed:I

    aput v1, v0, v3

    sput-object v0, Lcom/twitter/library/widget/TweetView;->j:[I

    new-array v0, v2, [I

    sget v1, Lib;->state_action_prompt_pressed:I

    aput v1, v0, v3

    sput-object v0, Lcom/twitter/library/widget/TweetView;->k:[I

    new-array v0, v2, [I

    sget v1, Lib;->state_action_prompt_dismiss_pressed:I

    aput v1, v0, v3

    sput-object v0, Lcom/twitter/library/widget/TweetView;->l:[I

    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0, v2}, Landroid/text/TextPaint;-><init>(I)V

    sput-object v0, Lcom/twitter/library/widget/TweetView;->m:Landroid/text/TextPaint;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/twitter/library/view/TweetActionType;

    sget-object v1, Lcom/twitter/library/view/TweetActionType;->d:Lcom/twitter/library/view/TweetActionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/twitter/library/view/TweetActionType;->c:Lcom/twitter/library/view/TweetActionType;

    aput-object v1, v0, v2

    const/4 v1, 0x2

    sget-object v2, Lcom/twitter/library/view/TweetActionType;->b:Lcom/twitter/library/view/TweetActionType;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/twitter/library/view/TweetActionType;->e:Lcom/twitter/library/view/TweetActionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/library/widget/TweetView;->n:[Lcom/twitter/library/view/TweetActionType;

    new-array v0, v3, [Lcom/twitter/library/view/TweetActionType;

    sput-object v0, Lcom/twitter/library/widget/TweetView;->o:[Lcom/twitter/library/view/TweetActionType;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/widget/TweetView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget v0, Lib;->tweetViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/library/widget/TweetView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9

    const/4 v8, 0x2

    const/high16 v6, 0x41400000    # 12.0f

    const/16 v7, 0x3c

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->bq:Ljava/util/ArrayList;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->bE:Landroid/graphics/Rect;

    new-instance v0, Lcom/twitter/library/widget/ao;

    invoke-direct {v0, p0}, Lcom/twitter/library/widget/ao;-><init>(Lcom/twitter/library/widget/TweetView;)V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->bG:Landroid/os/Handler;

    new-instance v0, Lcom/twitter/library/widget/r;

    invoke-direct {v0, p0}, Lcom/twitter/library/widget/r;-><init>(Lcom/twitter/library/widget/TweetView;)V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->bH:Ljava/lang/Runnable;

    sget-object v0, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;->a:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->bU:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->df:Landroid/graphics/Rect;

    const v0, 0x7fffffff

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->dQ:I

    sget-object v0, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->a:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->ez:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    iput v2, p0, Lcom/twitter/library/widget/TweetView;->eE:I

    invoke-virtual {p0, v2}, Lcom/twitter/library/widget/TweetView;->setWillNotDraw(Z)V

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget-object v0, Lim;->TweetView:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v4

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->E()Z

    move-result v0

    if-eqz v0, :cond_0

    iput v1, p0, Lcom/twitter/library/widget/TweetView;->eE:I

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/twitter/library/widget/TweetView;->dP:Z

    :cond_0
    const/16 v0, 0x8

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->p:Landroid/content/res/ColorStateList;

    const/16 v0, 0x9

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->v:Landroid/content/res/ColorStateList;

    invoke-static {p1}, Lcom/twitter/library/util/Util;->e(Landroid/content/Context;)F

    move-result v0

    invoke-virtual {v4, v8, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->cI:F

    const/4 v0, 0x6

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v4, v0, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->A:F

    const/4 v0, 0x7

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->C:I

    const/16 v0, 0xa

    const/high16 v5, 0x41800000    # 16.0f

    invoke-virtual {v4, v0, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->bv:F

    invoke-static {p1}, Lcom/twitter/library/util/Util;->f(Landroid/content/Context;)F

    move-result v0

    invoke-virtual {v4, v1, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->cD:F

    const/16 v0, 0xb

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->q:Landroid/content/res/ColorStateList;

    const/16 v0, 0xc

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->r:Landroid/content/res/ColorStateList;

    const/16 v0, 0xd

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->s:Landroid/content/res/ColorStateList;

    const/16 v0, 0xe

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->y:Landroid/content/res/ColorStateList;

    const/16 v0, 0xf

    invoke-virtual {v4, v0, v6}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->bx:F

    const/16 v0, 0x63

    invoke-virtual {v4, v0, v6}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->bw:F

    const/16 v0, 0x64

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->z:Landroid/content/res/ColorStateList;

    const/16 v0, 0x2d

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->W:I

    const/16 v0, 0x2e

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->Z:I

    const/16 v0, 0x2f

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->aa:I

    const/16 v0, 0x10

    const/4 v5, 0x4

    invoke-virtual {v4, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->D:I

    const/16 v0, 0x31

    sget v5, Lie;->user_image_size:I

    invoke-virtual {v4, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->ab:I

    const/16 v0, 0x32

    sget v5, Lie;->user_image_size:I

    invoke-virtual {v4, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->ac:I

    const/16 v0, 0x11

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_5

    iget v5, p0, Lcom/twitter/library/widget/TweetView;->ab:I

    iget v6, p0, Lcom/twitter/library/widget/TweetView;->ac:I

    invoke-virtual {v0, v2, v2, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->bu:Landroid/graphics/drawable/Drawable;

    :goto_0
    const/16 v0, 0x12

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->bJ:Landroid/graphics/drawable/Drawable;

    const/16 v0, 0x4a

    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->bK:I

    const/16 v0, 0x49

    const v5, -0x333334

    invoke-virtual {v4, v0, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->bL:I

    const/16 v0, 0x4b

    const v5, -0xbbbbbc

    invoke-virtual {v4, v0, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->bM:I

    const/16 v0, 0x34

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->ad:I

    const/16 v0, 0x35

    invoke-virtual {v4, v0, v8}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->ae:I

    const/16 v0, 0x33

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->by:Landroid/graphics/drawable/Drawable;

    const/16 v0, 0x33

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->bz:Landroid/graphics/drawable/Drawable;

    const/16 v0, 0x30

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v5, p0, Lcom/twitter/library/widget/TweetView;->ab:I

    iget v6, p0, Lcom/twitter/library/widget/TweetView;->ac:I

    invoke-virtual {v0, v2, v2, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->cS:Landroid/graphics/drawable/Drawable;

    :cond_1
    const/16 v0, 0x13

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->E:I

    const/4 v0, 0x5

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->F:I

    const/4 v0, 0x4

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->G:I

    invoke-virtual {v4, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->H:I

    const/16 v0, 0x14

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->S:I

    const/16 v0, 0x15

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->T:I

    const/16 v0, 0x16

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->U:I

    const/16 v0, 0x17

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->V:I

    const/16 v0, 0x18

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->I:I

    const/16 v0, 0x19

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->J:I

    const/16 v0, 0x1a

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->K:I

    const/16 v0, 0x1b

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->L:I

    const/16 v0, 0x1c

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->M:I

    const/16 v0, 0x1d

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->N:I

    const/16 v0, 0x1e

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->O:I

    const/16 v0, 0x1f

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->P:I

    const/16 v0, 0x21

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->Q:I

    const/16 v0, 0x20

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->R:I

    const/16 v0, 0x36

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->af:I

    const/16 v0, 0x36

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->ag:I

    const/16 v0, 0x62

    const/4 v5, 0x7

    invoke-virtual {v4, v0, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->br:I

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->br:I

    if-eqz v0, :cond_6

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->bA:Z

    const/16 v0, 0x38

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->t:Landroid/content/res/ColorStateList;

    const/16 v0, 0x55

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->ah:I

    const/16 v0, 0x5a

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->ai:I

    const/16 v0, 0x5b

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->aj:I

    const/16 v0, 0x5c

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->ak:I

    const/16 v0, 0x5d

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iget v5, p0, Lcom/twitter/library/widget/TweetView;->ai:I

    sub-int/2addr v0, v5

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->al:I

    const/16 v0, 0x3a

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->az:I

    const/16 v0, 0x3b

    invoke-virtual {v4, v0, v7}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->aA:I

    invoke-virtual {v4, v7, v7}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->aB:I

    const/16 v0, 0x40

    invoke-virtual {v4, v0, v7}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->aC:I

    const/16 v0, 0x41

    invoke-virtual {v4, v0, v7}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->aD:I

    const/16 v0, 0x3d

    const/16 v5, 0x10

    invoke-virtual {v4, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->aE:I

    const/16 v0, 0x3e

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->aF:I

    const/16 v0, 0x3f

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->aG:I

    const/16 v0, 0x42

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->aH:I

    const/16 v0, 0x43

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->aI:I

    const/16 v0, 0x46

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->aJ:I

    const/16 v0, 0x44

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->w:Landroid/content/res/ColorStateList;

    const/16 v0, 0x45

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->x:Landroid/content/res/ColorStateList;

    const/16 v0, 0x47

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->u:Landroid/content/res/ColorStateList;

    const/4 v0, 0x3

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->am:I

    const/16 v0, 0x58

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->an:I

    const/16 v0, 0x59

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->ao:I

    const/16 v0, 0x61

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->aK:I

    sget v0, Lid;->placeholder_bg:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->B:I

    const/16 v0, 0x48

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->bt:I

    const/16 v0, 0x4c

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->ap:I

    const/16 v0, 0x4d

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->aq:I

    const/16 v0, 0x50

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->ar:I

    const/16 v0, 0x4e

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->as:I

    iput-boolean v1, p0, Lcom/twitter/library/widget/TweetView;->dW:Z

    const/16 v0, 0x4f

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->at:I

    const/16 v0, 0x51

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->au:I

    const/16 v0, 0x52

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->av:I

    const/16 v0, 0x53

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->aw:I

    const/16 v0, 0x54

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->ax:I

    const/16 v0, 0x65

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->ay:I

    const/16 v0, 0x5f

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->bB:Z

    const/16 v0, 0x60

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->bC:Landroid/content/res/ColorStateList;

    const/16 v0, 0x22

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->aL:I

    const/16 v0, 0x23

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->aM:I

    const/16 v0, 0x24

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->aN:I

    const/16 v0, 0x25

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->aO:I

    const/16 v0, 0x26

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->aP:I

    const/16 v0, 0x27

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->aQ:I

    const/16 v0, 0x28

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->aR:I

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->eE:I

    if-ne v0, v1, :cond_7

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->aR:I

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->dM:I

    :goto_2
    const/16 v0, 0x2a

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->aS:I

    const/16 v0, 0x2b

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->aT:I

    const/16 v0, 0x66

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->bs:I

    const/16 v0, 0x67

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->bd:I

    const/16 v0, 0x68

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->bb:I

    const/16 v0, 0x69

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->bc:I

    const/16 v0, 0x6a

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->bf:I

    const/16 v0, 0x6b

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->bg:I

    const/16 v0, 0x6c

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->bh:I

    const/16 v0, 0x6d

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->bi:I

    const/16 v0, 0x6e

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->bj:I

    const/16 v0, 0x6f

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->aU:I

    const/16 v0, 0x70

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->aV:I

    const/16 v0, 0x71

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->be:I

    const/16 v0, 0x72

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->aW:I

    const/16 v0, 0x73

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->aX:I

    const/16 v0, 0x74

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->aY:I

    const/16 v0, 0x75

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->aZ:I

    const/16 v0, 0x76

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->ba:I

    const/16 v0, 0x78

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->bl:Landroid/graphics/drawable/Drawable;

    const/16 v0, 0x79

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->bm:Landroid/graphics/drawable/Drawable;

    const/16 v0, 0x7a

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->bk:I

    const/16 v0, 0x7b

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->bn:Landroid/graphics/drawable/Drawable;

    const/16 v0, 0x7c

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->bo:Landroid/graphics/drawable/Drawable;

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->bp:I

    invoke-static {p1}, Lcom/twitter/internal/android/widget/ax;->a(Landroid/content/Context;)Lcom/twitter/internal/android/widget/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->bD:Lcom/twitter/internal/android/widget/ax;

    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->D()V

    iput-boolean v2, p0, Lcom/twitter/library/widget/TweetView;->ea:Z

    iput-boolean v2, p0, Lcom/twitter/library/widget/TweetView;->ek:Z

    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->x()V

    const-string/jumbo v0, "android_poi_compose_1922"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v0, "android_poi_compose_1922"

    new-array v3, v1, [Ljava/lang/String;

    const-string/jumbo v5, "geotag"

    aput-object v5, v3, v2

    invoke-static {v0, v3}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "android_poi_compose_1922"

    new-array v3, v1, [Ljava/lang/String;

    const-string/jumbo v5, "geotag_no_rich_view"

    aput-object v5, v3, v2

    invoke-static {v0, v3}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_2
    move v0, v1

    :goto_3
    invoke-static {}, Lcom/twitter/library/featureswitch/a;->ae()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->af()Z

    move-result v3

    if-nez v3, :cond_4

    :cond_3
    if-eqz v0, :cond_9

    :cond_4
    :goto_4
    iput-boolean v1, p0, Lcom/twitter/library/widget/TweetView;->bF:Z

    new-instance v0, Lcom/twitter/library/widget/TweetMediaImagesView;

    invoke-direct {v0, p1}, Lcom/twitter/library/widget/TweetMediaImagesView;-><init>(Landroid/content/Context;)V

    const/16 v1, 0x5e

    invoke-virtual {v4, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetMediaImagesView;->setMediaDividerSize(I)V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->bI:Lcom/twitter/library/widget/TweetMediaImagesView;

    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->bu:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    :cond_6
    move v0, v2

    goto/16 :goto_1

    :cond_7
    iget v0, p0, Lcom/twitter/library/widget/TweetView;->aP:I

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->dM:I

    goto/16 :goto_2

    :cond_8
    move v0, v2

    goto :goto_3

    :cond_9
    move v1, v2

    goto :goto_4
.end method

.method private A()Z
    .locals 2

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->es:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->es:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private B()V
    .locals 3

    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->F()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/al;

    invoke-virtual {v0}, Lcom/twitter/library/widget/al;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/widget/al;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/al;->a(Z)V

    invoke-virtual {v0}, Lcom/twitter/library/widget/al;->n()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private C()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    invoke-direct {p0, v1}, Lcom/twitter/library/widget/TweetView;->d(Lcom/twitter/library/provider/Tweet;)Z

    move-result v1

    if-nez v1, :cond_0

    iget v1, p0, Lcom/twitter/library/widget/TweetView;->eb:I

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return v0

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->q()Z

    move-result v0

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->r()Z

    move-result v0

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v1}, Lcom/twitter/library/provider/Tweet;->q()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v1}, Lcom/twitter/library/provider/Tweet;->r()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private D()V
    .locals 10

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->cY:Landroid/graphics/Rect;

    new-instance v0, Lcom/twitter/library/widget/al;

    const-wide/16 v2, 0x2

    const-wide/16 v4, 0x1

    new-instance v6, Lcom/twitter/library/widget/ae;

    invoke-direct {v6, p0}, Lcom/twitter/library/widget/ae;-><init>(Lcom/twitter/library/widget/TweetView;)V

    sget-object v7, Lcom/twitter/library/widget/TweetView;->c:[I

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/widget/al;-><init>(Lcom/twitter/library/widget/TweetView;JJLjava/lang/Runnable;[I)V

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->cY:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/al;->a(Landroid/graphics/Rect;)V

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/twitter/library/widget/TweetView;->db:Landroid/graphics/Rect;

    new-instance v1, Lcom/twitter/library/widget/al;

    const-wide/16 v3, 0x8

    const-wide/16 v5, 0x4

    new-instance v7, Lcom/twitter/library/widget/ai;

    invoke-direct {v7, p0}, Lcom/twitter/library/widget/ai;-><init>(Lcom/twitter/library/widget/TweetView;)V

    sget-object v8, Lcom/twitter/library/widget/TweetView;->d:[I

    move-object v2, p0

    invoke-direct/range {v1 .. v8}, Lcom/twitter/library/widget/al;-><init>(Lcom/twitter/library/widget/TweetView;JJLjava/lang/Runnable;[I)V

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->db:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/al;->a(Landroid/graphics/Rect;)V

    new-instance v2, Lcom/twitter/library/widget/z;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/twitter/library/widget/z;-><init>(Lcom/twitter/library/widget/TweetView;Lcom/twitter/library/widget/r;)V

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/al;->a(Ljava/lang/Runnable;)V

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/twitter/library/widget/TweetView;->dc:Landroid/graphics/Rect;

    new-instance v2, Lcom/twitter/library/widget/al;

    const-wide/32 v4, 0x400000

    const-wide/32 v6, 0x200000

    new-instance v8, Lcom/twitter/library/widget/aj;

    invoke-direct {v8, p0}, Lcom/twitter/library/widget/aj;-><init>(Lcom/twitter/library/widget/TweetView;)V

    sget-object v9, Lcom/twitter/library/widget/TweetView;->d:[I

    move-object v3, p0

    invoke-direct/range {v2 .. v9}, Lcom/twitter/library/widget/al;-><init>(Lcom/twitter/library/widget/TweetView;JJLjava/lang/Runnable;[I)V

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->dc:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Lcom/twitter/library/widget/al;->a(Landroid/graphics/Rect;)V

    const/4 v3, 0x3

    new-array v3, v3, [Lcom/twitter/library/widget/al;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v1, v3, v0

    const/4 v0, 0x2

    aput-object v2, v3, v0

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->do:Ljava/util/List;

    return-void
.end method

.method private E()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->do:Ljava/util/List;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->D()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->do:Ljava/util/List;

    return-object v0
.end method

.method private F()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dn:Ljava/util/List;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->E()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dn:Ljava/util/List;

    goto :goto_0
.end method

.method private G()V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->eJ:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->eH:Lcom/twitter/library/card/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->eH:Lcom/twitter/library/card/k;

    invoke-interface {v0}, Lcom/twitter/library/card/k;->y()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->eJ:Z

    :cond_0
    return-void
.end method

.method private H()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->eH:Lcom/twitter/library/card/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->eH:Lcom/twitter/library/card/k;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/twitter/library/widget/TweetView;->eH:Lcom/twitter/library/card/k;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/twitter/library/widget/TweetView;->eI:Z

    invoke-interface {v0}, Lcom/twitter/library/card/k;->z()V

    invoke-interface {v0}, Lcom/twitter/library/card/k;->p()Lcom/twitter/library/card/Card;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/card/Card;->A()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/TweetView;->removeView(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method private a(Landroid/text/Layout;Ljava/lang/String;Landroid/graphics/Paint;)I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->bE:Landroid/graphics/Rect;

    invoke-virtual {p3, p2, v0, v1, v2}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->bE:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->bE:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1, v0}, Landroid/text/Layout;->getLineAscent(I)I

    move-result v0

    sub-int v0, v1, v0

    goto :goto_0
.end method

.method private a(Lcom/twitter/library/provider/Tweet;II)I
    .locals 16

    move-object/from16 v0, p0

    iget v12, v0, Lcom/twitter/library/widget/TweetView;->aC:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/twitter/library/widget/TweetView;->aD:I

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getPaddingLeft()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getPaddingTop()I

    move-result v14

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->l()Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v5

    sget-object v3, Lcom/twitter/library/widget/TweetView;->m:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v2, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getLeft()I

    move-result v2

    add-int/2addr v1, v2

    move v9, v1

    move v10, v1

    :goto_0
    iget-object v1, v5, Lcom/twitter/library/api/TweetClassicCard;->siteUser:Lcom/twitter/library/api/CardUser;

    iget-object v1, v5, Lcom/twitter/library/api/TweetClassicCard;->imageUrl:Ljava/lang/String;

    if-eqz v1, :cond_9

    const/4 v1, 0x1

    move v11, v1

    :goto_1
    move-object/from16 v0, p0

    iget v15, v0, Lcom/twitter/library/widget/TweetView;->az:I

    if-eqz v11, :cond_a

    sub-int v1, p2, v12

    mul-int/lit8 v2, v15, 0x2

    sub-int/2addr v1, v2

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v4

    :goto_2
    move-object/from16 v0, p0

    iget v1, v0, Lcom/twitter/library/widget/TweetView;->bv:F

    invoke-virtual {v3, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    const/4 v2, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->N()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->V()Lcom/twitter/library/card/instance/CardInstanceData;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/library/card/instance/CardInstanceData;->bindingValues:Ljava/util/HashMap;

    const-string/jumbo v5, "app_name"

    invoke-virtual {v1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/card/instance/BindingValue;

    if-eqz v1, :cond_10

    iget-object v1, v1, Lcom/twitter/library/card/instance/BindingValue;->value:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    :goto_3
    move-object v2, v1

    :goto_4
    new-instance v1, Landroid/text/StaticLayout;

    if-eqz v2, :cond_c

    :goto_5
    move/from16 v0, p2

    int-to-double v5, v0

    const-wide v7, 0x3fe4cccccccccccdL    # 0.65

    mul-double/2addr v5, v7

    double-to-int v5, v5

    invoke-static {v2, v5}, Lcom/twitter/library/util/Util;->b(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/twitter/library/widget/TweetView;->A:F

    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/library/widget/TweetView;->C:I

    int-to-float v7, v7

    const/4 v8, 0x0

    invoke-direct/range {v1 .. v8}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/twitter/library/widget/TweetView;->cd:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/widget/TweetView;->cd:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    if-eqz v11, :cond_0

    invoke-static {v13, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    :cond_0
    add-int v2, v14, p3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->aH:I

    add-int/2addr v2, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->dd:Landroid/graphics/RectF;

    if-nez v4, :cond_1

    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->dd:Landroid/graphics/RectF;

    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->de:Landroid/graphics/RectF;

    if-nez v4, :cond_2

    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->de:Landroid/graphics/RectF;

    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->dd:Landroid/graphics/RectF;

    int-to-float v5, v10

    int-to-float v6, v2

    add-int v7, v10, p2

    int-to-float v7, v7

    add-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {v4, v5, v6, v7, v1}, Landroid/graphics/RectF;->set(FFFF)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/widget/TweetView;->de:Landroid/graphics/RectF;

    iget v2, v4, Landroid/graphics/RectF;->left:F

    int-to-float v5, v15

    sub-float/2addr v2, v5

    iget v5, v4, Landroid/graphics/RectF;->top:F

    int-to-float v6, v15

    sub-float/2addr v5, v6

    iget v6, v4, Landroid/graphics/RectF;->right:F

    int-to-float v7, v15

    add-float/2addr v6, v7

    iget v7, v4, Landroid/graphics/RectF;->bottom:F

    int-to-float v8, v15

    add-float/2addr v7, v8

    invoke-virtual {v1, v2, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    if-eqz v11, :cond_d

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    if-nez v1, :cond_3

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    :cond_3
    iget v1, v4, Landroid/graphics/RectF;->top:F

    float-to-int v1, v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    add-int v4, v9, v12

    add-int v5, v1, v13

    invoke-virtual {v2, v9, v1, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    :cond_4
    :goto_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/widget/TweetView;->dk:Landroid/graphics/RectF;

    if-nez v1, :cond_5

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/twitter/library/widget/TweetView;->dk:Landroid/graphics/RectF;

    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/widget/TweetView;->dl:Landroid/graphics/RectF;

    if-nez v1, :cond_6

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/twitter/library/widget/TweetView;->dl:Landroid/graphics/RectF;

    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->aa()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_e

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lil;->app_launch_title:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    :goto_7
    new-instance v1, Landroid/text/StaticLayout;

    const/16 v4, 0x3a

    invoke-static {v2, v4}, Lcom/twitter/library/util/Util;->b(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/twitter/library/widget/TweetView;->A:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->C:I

    int-to-float v7, v4

    const/4 v8, 0x0

    move/from16 v4, p2

    invoke-direct/range {v1 .. v8}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/twitter/library/widget/TweetView;->ce:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/widget/TweetView;->de:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    float-to-int v2, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/widget/TweetView;->ce:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    add-int/2addr v1, v2

    add-int v3, v1, v15

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/widget/TweetView;->dd:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    float-to-int v4, v1

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v1, :cond_f

    sub-int v1, v4, p2

    :goto_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->dk:Landroid/graphics/RectF;

    int-to-float v4, v4

    int-to-float v2, v2

    int-to-float v1, v1

    int-to-float v3, v3

    invoke-virtual {v5, v4, v2, v1, v3}, Landroid/graphics/RectF;->set(FFFF)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/widget/TweetView;->dl:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->de:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->de:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->de:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->dk:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/twitter/library/widget/TweetView;->aJ:I

    int-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/widget/TweetView;->eM:Lcom/twitter/library/widget/al;

    if-eqz v1, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/widget/TweetView;->eM:Lcom/twitter/library/widget/al;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->dl:Landroid/graphics/RectF;

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/al;->a(Landroid/graphics/RectF;)V

    :cond_7
    move-object/from16 v0, p0

    iget v1, v0, Lcom/twitter/library/widget/TweetView;->aH:I

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/TweetView;->dl:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    add-float/2addr v1, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/library/widget/TweetView;->aI:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    return v1

    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getLeft()I

    move-result v2

    add-int/2addr v1, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/library/widget/TweetView;->ab:I

    add-int/2addr v1, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/library/widget/TweetView;->aa:I

    add-int/2addr v2, v1

    add-int v1, v2, p2

    sub-int/2addr v1, v12

    move v9, v1

    move v10, v2

    goto/16 :goto_0

    :cond_9
    const/4 v1, 0x0

    move v11, v1

    goto/16 :goto_1

    :cond_a
    move/from16 v4, p2

    goto/16 :goto_2

    :cond_b
    iget-object v1, v5, Lcom/twitter/library/api/TweetClassicCard;->title:Ljava/lang/String;

    move-object v2, v1

    goto/16 :goto_4

    :cond_c
    const-string/jumbo v2, ""

    goto/16 :goto_5

    :cond_d
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    if-eqz v1, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->setEmpty()V

    goto/16 :goto_6

    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lil;->app_download_title:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto/16 :goto_7

    :cond_f
    add-int v1, v4, p2

    goto/16 :goto_8

    :cond_10
    move-object v1, v2

    goto/16 :goto_3
.end method

.method private a(Lcom/twitter/library/provider/Tweet;III)I
    .locals 13

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->W()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->X()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    sget-object v3, Lcom/twitter/library/widget/TweetView;->m:Landroid/text/TextPaint;

    invoke-direct {p0, p1}, Lcom/twitter/library/widget/TweetView;->k(Lcom/twitter/library/provider/Tweet;)Ljava/lang/String;

    move-result-object v2

    new-instance v1, Landroid/text/StaticLayout;

    const/16 v4, 0x3a

    invoke-static {v2, v4}, Lcom/twitter/library/util/Util;->b(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    move/from16 v0, p4

    int-to-double v4, v0

    const-wide v6, 0x3fe4cccccccccccdL    # 0.65

    mul-double/2addr v4, v6

    double-to-int v4, v4

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    iget v6, p0, Lcom/twitter/library/widget/TweetView;->A:F

    iget v7, p0, Lcom/twitter/library/widget/TweetView;->C:I

    int-to-float v7, v7

    const/4 v8, 0x0

    invoke-direct/range {v1 .. v8}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v1, p0, Lcom/twitter/library/widget/TweetView;->cd:Landroid/text/StaticLayout;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->cd:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v10

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getPaddingTop()I

    move-result v1

    add-int/2addr v1, p2

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->aH:I

    add-int v11, v1, v2

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->di:Landroid/graphics/RectF;

    if-nez v1, :cond_1

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/twitter/library/widget/TweetView;->di:Landroid/graphics/RectF;

    :cond_1
    new-instance v12, Landroid/graphics/RectF;

    invoke-direct {v12}, Landroid/graphics/RectF;-><init>()V

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->dj:Landroid/graphics/RectF;

    if-nez v1, :cond_2

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/twitter/library/widget/TweetView;->dj:Landroid/graphics/RectF;

    :cond_2
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->dk:Landroid/graphics/RectF;

    if-nez v1, :cond_3

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/twitter/library/widget/TweetView;->dk:Landroid/graphics/RectF;

    :cond_3
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->dl:Landroid/graphics/RectF;

    if-nez v1, :cond_4

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/twitter/library/widget/TweetView;->dl:Landroid/graphics/RectF;

    :cond_4
    iget-boolean v1, p0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v1, :cond_8

    move/from16 v0, p4

    int-to-double v1, v0

    const-wide v4, 0x3fe4cccccccccccdL    # 0.65

    mul-double/2addr v1, v4

    double-to-int v1, v1

    sub-int v1, p3, v1

    :goto_1
    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->di:Landroid/graphics/RectF;

    move/from16 v0, p3

    int-to-float v4, v0

    int-to-float v5, v11

    int-to-float v1, v1

    add-int v6, v11, v10

    int-to-float v6, v6

    invoke-virtual {v2, v4, v5, v1, v6}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->di:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->aJ:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->di:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    iget v4, p0, Lcom/twitter/library/widget/TweetView;->aJ:I

    int-to-float v4, v4

    sub-float/2addr v2, v4

    iget-object v4, p0, Lcom/twitter/library/widget/TweetView;->di:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    iget v5, p0, Lcom/twitter/library/widget/TweetView;->aJ:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    iget-object v5, p0, Lcom/twitter/library/widget/TweetView;->di:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    iget v6, p0, Lcom/twitter/library/widget/TweetView;->aJ:I

    int-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v12, v1, v2, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getPaddingLeft()I

    move-result v1

    iget-boolean v2, p0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->di:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    float-to-int v2, v2

    mul-int/lit8 v1, v1, 0x3

    sub-int v1, v2, v1

    :goto_2
    iget-object v4, p0, Lcom/twitter/library/widget/TweetView;->dj:Landroid/graphics/RectF;

    int-to-float v2, v2

    int-to-float v5, v11

    int-to-float v1, v1

    add-int v6, v11, v10

    int-to-float v6, v6

    invoke-virtual {v4, v2, v5, v1, v6}, Landroid/graphics/RectF;->set(FFFF)V

    iget-boolean v1, p0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->dj:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    move v9, v1

    :goto_3
    move/from16 v0, p4

    int-to-float v1, v0

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->dj:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    iget-object v4, p0, Lcom/twitter/library/widget/TweetView;->di:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    add-float/2addr v2, v4

    sub-float/2addr v1, v2

    float-to-int v4, v1

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->W()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->V()Lcom/twitter/library/card/instance/CardInstanceData;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/library/card/instance/CardInstanceData;->bindingValues:Ljava/util/HashMap;

    const-string/jumbo v2, "promotion_cta"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/card/instance/BindingValue;

    if-eqz v1, :cond_b

    iget-object v1, v1, Lcom/twitter/library/card/instance/BindingValue;->value:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    :goto_4
    move-object v2, v1

    :goto_5
    new-instance v1, Landroid/text/StaticLayout;

    const/16 v5, 0x8

    invoke-static {v2, v5}, Lcom/twitter/library/util/Util;->b(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    iget v6, p0, Lcom/twitter/library/widget/TweetView;->A:F

    iget v7, p0, Lcom/twitter/library/widget/TweetView;->C:I

    int-to-float v7, v7

    const/4 v8, 0x0

    invoke-direct/range {v1 .. v8}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v1, p0, Lcom/twitter/library/widget/TweetView;->ce:Landroid/text/StaticLayout;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->ce:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    invoke-static {v10, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int v2, v11, v1

    iget-boolean v1, p0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v1, :cond_d

    sub-int v1, v9, v4

    :goto_6
    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->dk:Landroid/graphics/RectF;

    int-to-float v4, v9

    int-to-float v5, v11

    int-to-float v1, v1

    int-to-float v6, v2

    invoke-virtual {v3, v4, v5, v1, v6}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->di:Landroid/graphics/RectF;

    int-to-float v2, v2

    iput v2, v1, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->dl:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->dk:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget v3, p0, Lcom/twitter/library/widget/TweetView;->aJ:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->dk:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget v4, p0, Lcom/twitter/library/widget/TweetView;->aJ:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/twitter/library/widget/TweetView;->dk:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    iget v5, p0, Lcom/twitter/library/widget/TweetView;->aJ:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    iget-object v5, p0, Lcom/twitter/library/widget/TweetView;->dk:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    iget v6, p0, Lcom/twitter/library/widget/TweetView;->aJ:I

    int-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->dj:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->dl:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    iput v2, v1, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->eM:Lcom/twitter/library/widget/al;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->eh:Landroid/graphics/RectF;

    if-nez v1, :cond_5

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/twitter/library/widget/TweetView;->eh:Landroid/graphics/RectF;

    :cond_5
    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->eh:Landroid/graphics/RectF;

    iget v3, v12, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    if-nez v1, :cond_e

    :cond_6
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->dl:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    :goto_7
    iget-object v4, p0, Lcom/twitter/library/widget/TweetView;->dl:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    iget-object v5, p0, Lcom/twitter/library/widget/TweetView;->dl:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->eM:Lcom/twitter/library/widget/al;

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->eh:Landroid/graphics/RectF;

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/al;->a(Landroid/graphics/RectF;)V

    :cond_7
    iget v1, p0, Lcom/twitter/library/widget/TweetView;->aH:I

    int-to-float v1, v1

    invoke-virtual {v12}, Landroid/graphics/RectF;->height()F

    move-result v2

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->dl:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    add-float/2addr v1, v2

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->aI:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    goto/16 :goto_0

    :cond_8
    move/from16 v0, p4

    int-to-double v1, v0

    const-wide v4, 0x3fe4cccccccccccdL    # 0.65

    mul-double/2addr v1, v4

    double-to-int v1, v1

    add-int v1, v1, p3

    goto/16 :goto_1

    :cond_9
    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->di:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    float-to-int v2, v2

    mul-int/lit8 v1, v1, 0x3

    add-int/2addr v1, v2

    goto/16 :goto_2

    :cond_a
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->dj:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    float-to-int v1, v1

    move v9, v1

    goto/16 :goto_3

    :cond_b
    const-string/jumbo v1, ""

    goto/16 :goto_4

    :cond_c
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lil;->call_now:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto/16 :goto_5

    :cond_d
    add-int v1, v9, v4

    goto/16 :goto_6

    :cond_e
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    goto :goto_7
.end method

.method private a(Lcom/twitter/library/provider/Tweet;Landroid/text/TextPaint;IIIIII)I
    .locals 10

    sget-object v0, Lcom/twitter/library/view/TweetActionType;->a:Lcom/twitter/library/view/TweetActionType;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->dx:Lcom/twitter/library/view/TweetActionType;

    invoke-virtual {v0, v1}, Lcom/twitter/library/view/TweetActionType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->ds:Z

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->du:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->dt:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dw:Ljava/lang/String;

    move-object v8, v0

    :goto_1
    iget v0, p0, Lcom/twitter/library/widget/TweetView;->bj:I

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->bf:I

    add-int/2addr v0, v2

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->bg:I

    add-int/2addr v0, v2

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->bb:I

    mul-int/lit8 v2, v2, 0x2

    add-int v9, v0, v2

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->bi:I

    sub-int v3, p4, v0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ck:Landroid/text/StaticLayout;

    if-nez v0, :cond_1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->cI:F

    invoke-virtual {p2, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bD:Lcom/twitter/internal/android/widget/ax;

    iget-object v0, v0, Lcom/twitter/internal/android/widget/ax;->c:Landroid/graphics/Typeface;

    invoke-virtual {p2, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    new-instance v0, Landroid/text/StaticLayout;

    sget-boolean v2, Lcom/twitter/library/util/Util;->c:Z

    if-eqz v2, :cond_a

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->C()Z

    move-result v2

    if-eqz v2, :cond_a

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    :goto_2
    iget v5, p0, Lcom/twitter/library/widget/TweetView;->A:F

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, p2

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->ck:Landroid/text/StaticLayout;

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->cm:Landroid/text/StaticLayout;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->cm:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->cI:F

    invoke-virtual {p2, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bD:Lcom/twitter/internal/android/widget/ax;

    iget-object v0, v0, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    invoke-virtual {p2, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    new-instance v0, Landroid/text/StaticLayout;

    sget-boolean v1, Lcom/twitter/library/util/Util;->c:Z

    if-eqz v1, :cond_b

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->C()Z

    move-result v1

    if-eqz v1, :cond_b

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    :goto_3
    iget v5, p0, Lcom/twitter/library/widget/TweetView;->A:F

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, v8

    move-object v2, p2

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->cm:Landroid/text/StaticLayout;

    :cond_3
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ci:Landroid/graphics/RectF;

    if-nez v0, :cond_4

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->ci:Landroid/graphics/RectF;

    :cond_4
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->cm:Landroid/text/StaticLayout;

    if-eqz v0, :cond_c

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->be:I

    :goto_4
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->ck:Landroid/text/StaticLayout;

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->ck:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    :goto_5
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->cm:Landroid/text/StaticLayout;

    if-eqz v1, :cond_e

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->cm:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    :goto_6
    add-int/2addr v0, v9

    add-int v8, v0, v1

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->bf:I

    add-int v0, v0, p6

    iget v1, p0, Lcom/twitter/library/widget/TweetView;->bg:I

    add-int v1, v1, p6

    add-int/2addr v0, p5

    add-int/2addr v0, v1

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->dF:I

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ci:Landroid/graphics/RectF;

    const/4 v1, 0x0

    const/4 v2, 0x0

    int-to-float v3, p3

    int-to-float v4, v8

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->cq:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->cq:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v9

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->cq:Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    sub-int v0, p3, v0

    iget v1, p0, Lcom/twitter/library/widget/TweetView;->bk:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->dI:I

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->cr:Landroid/graphics/RectF;

    if-nez v0, :cond_5

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->cr:Landroid/graphics/RectF;

    :cond_5
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->cs:Lcom/twitter/library/widget/al;

    if-nez v0, :cond_6

    new-instance v0, Lcom/twitter/library/widget/al;

    const-wide/32 v2, 0x1000000

    const-wide/32 v4, 0x800000

    new-instance v6, Lcom/twitter/library/widget/ac;

    invoke-direct {v6, p0}, Lcom/twitter/library/widget/ac;-><init>(Lcom/twitter/library/widget/TweetView;)V

    sget-object v7, Lcom/twitter/library/widget/TweetView;->l:[I

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/widget/al;-><init>(Lcom/twitter/library/widget/TweetView;JJLjava/lang/Runnable;[I)V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->cs:Lcom/twitter/library/widget/al;

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->cs:Lcom/twitter/library/widget/al;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->cr:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/al;->a(Landroid/graphics/RectF;)V

    :cond_6
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dn:Ljava/util/List;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->cs:Lcom/twitter/library/widget/al;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->co:Landroid/graphics/RectF;

    if-nez v0, :cond_7

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->co:Landroid/graphics/RectF;

    :cond_7
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->cp:Lcom/twitter/library/widget/al;

    if-nez v0, :cond_8

    new-instance v0, Lcom/twitter/library/widget/al;

    const-wide/32 v2, 0x4000000

    const-wide/32 v4, 0x2000000

    new-instance v6, Lcom/twitter/library/widget/ab;

    invoke-direct {v6, p0}, Lcom/twitter/library/widget/ab;-><init>(Lcom/twitter/library/widget/TweetView;)V

    sget-object v7, Lcom/twitter/library/widget/TweetView;->k:[I

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/widget/al;-><init>(Lcom/twitter/library/widget/TweetView;JJLjava/lang/Runnable;[I)V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->cp:Lcom/twitter/library/widget/al;

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->cp:Lcom/twitter/library/widget/al;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->co:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/al;->a(Landroid/graphics/RectF;)V

    :cond_8
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dn:Ljava/util/List;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->cp:Lcom/twitter/library/widget/al;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int v0, p7, p8

    iget v1, p0, Lcom/twitter/library/widget/TweetView;->bd:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->dF:I

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->cr:Landroid/graphics/RectF;

    iget v1, p0, Lcom/twitter/library/widget/TweetView;->dI:I

    int-to-float v1, v1

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->dF:I

    int-to-float v2, v2

    int-to-float v3, p3

    iget v4, p0, Lcom/twitter/library/widget/TweetView;->dF:I

    add-int/2addr v4, v9

    iget v5, p0, Lcom/twitter/library/widget/TweetView;->bk:I

    mul-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    int-to-float v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->co:Landroid/graphics/RectF;

    const/4 v1, 0x0

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->dF:I

    int-to-float v2, v2

    int-to-float v3, p3

    iget v4, p0, Lcom/twitter/library/widget/TweetView;->dF:I

    int-to-float v4, v4

    iget-object v5, p0, Lcom/twitter/library/widget/TweetView;->ci:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->bj:I

    add-int/2addr v0, v8

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dv:Ljava/lang/String;

    move-object v8, v0

    goto/16 :goto_1

    :cond_a
    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    goto/16 :goto_2

    :cond_b
    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    goto/16 :goto_3

    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_4

    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_5

    :cond_e
    const/4 v1, 0x0

    goto/16 :goto_6
.end method

.method private a(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/widget/TweetView$InlineAction$State;)I
    .locals 1

    sget-object v0, Lcom/twitter/library/view/TweetActionType;->b:Lcom/twitter/library/view/TweetActionType;

    if-ne p1, v0, :cond_1

    sget-object v0, Lcom/twitter/library/widget/TweetView$InlineAction$State;->a:Lcom/twitter/library/widget/TweetView$InlineAction$State;

    if-ne p2, v0, :cond_0

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->I:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/twitter/library/widget/TweetView;->J:I

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/twitter/library/view/TweetActionType;->c:Lcom/twitter/library/view/TweetActionType;

    if-ne p1, v0, :cond_4

    sget-object v0, Lcom/twitter/library/widget/TweetView$InlineAction$State;->c:Lcom/twitter/library/widget/TweetView$InlineAction$State;

    if-ne p2, v0, :cond_2

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->M:I

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/twitter/library/widget/TweetView$InlineAction$State;->a:Lcom/twitter/library/widget/TweetView$InlineAction$State;

    if-ne p2, v0, :cond_3

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->K:I

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/twitter/library/widget/TweetView;->L:I

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/twitter/library/view/TweetActionType;->d:Lcom/twitter/library/view/TweetActionType;

    if-ne p1, v0, :cond_5

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->N:I

    goto :goto_0

    :cond_5
    sget-object v0, Lcom/twitter/library/view/TweetActionType;->e:Lcom/twitter/library/view/TweetActionType;

    if-ne p1, v0, :cond_7

    sget-object v0, Lcom/twitter/library/widget/TweetView$InlineAction$State;->a:Lcom/twitter/library/widget/TweetView$InlineAction$State;

    if-ne p2, v0, :cond_6

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->dN:I

    goto :goto_0

    :cond_6
    iget v0, p0, Lcom/twitter/library/widget/TweetView;->dO:I

    goto :goto_0

    :cond_7
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/library/widget/TweetView;J)I
    .locals 2

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->dh:I

    int-to-long v0, v0

    or-long/2addr v0, p1

    long-to-int v0, v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->dh:I

    return v0
.end method

.method private a(Ljava/lang/String;Landroid/graphics/Paint;)I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->bE:Landroid/graphics/Rect;

    invoke-virtual {p2, p1, v0, v1, v2}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bE:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    return v0
.end method

.method private a(Lcom/twitter/library/widget/TweetView$InlineAction;FFJ)Landroid/animation/ValueAnimator;
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput p2, v0, v1

    const/4 v1, 0x1

    aput p3, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p4, p5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/twitter/library/widget/y;

    invoke-direct {v1, p0, p1}, Lcom/twitter/library/widget/y;-><init>(Lcom/twitter/library/widget/TweetView;Lcom/twitter/library/widget/TweetView$InlineAction;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    return-object v0
.end method

.method private a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;
    .locals 4

    const/4 v3, 0x0

    if-lez p2, :cond_0

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Landroid/text/TextPaint;)Landroid/text/StaticLayout;
    .locals 8

    new-instance v0, Landroid/text/StaticLayout;

    invoke-static {p1, p2}, Lhl;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v3

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    return-object v0
.end method

.method private a(Landroid/content/Context;Ljava/util/List;)Ljava/lang/CharSequence;
    .locals 2

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, ""

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p2}, Lcom/twitter/library/util/u;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v0, ""

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/twitter/library/widget/TweetView;->ay:I

    invoke-static {p1, v0, v1}, Lcom/twitter/library/util/u;->a(Landroid/content/Context;Ljava/util/List;I)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;IIIJ)Ljava/lang/String;
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/4 p2, 0x0

    :goto_0
    :pswitch_1
    return-object p2

    :pswitch_2
    sget v0, Lil;->magic_recs_retweet_notif:I

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :pswitch_3
    sget v0, Lil;->magic_recs_favorite_notif:I

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :pswitch_4
    sget v0, Lil;->social_both_follow:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :pswitch_5
    sget v0, Lil;->social_both_followed_by:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :pswitch_6
    sget v0, Lil;->social_follow_and_follow:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :pswitch_7
    sget v0, Lil;->social_follower_of_follower:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :pswitch_8
    sget v0, Lil;->tweets_retweeted:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :pswitch_9
    sget v0, Lil;->social_follower_and_retweets:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :pswitch_a
    sget v0, Lil;->social_follow_and_reply:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :pswitch_b
    sget v0, Lil;->social_follower_and_reply:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :pswitch_c
    sget v0, Lil;->social_follow_and_fav:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :pswitch_d
    sget v0, Lil;->social_follower_and_fav:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :pswitch_e
    sget v0, Lil;->social_reply_to_follow:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :pswitch_f
    sget v0, Lil;->social_reply_to_follower:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :pswitch_10
    sget v0, Lil;->tweets_retweeted:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :pswitch_11
    sget v0, Lij;->social_fav_and_retweets_count:I

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {p0, p4}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {p0, p5}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, p5, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :pswitch_12
    if-ne p5, v3, :cond_0

    sget v0, Lil;->social_retweet_and_fav_count:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :cond_0
    sget v0, Lij;->social_retweet_and_favs_count:I

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {p0, p5}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {p0, p4}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, p4, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :pswitch_13
    if-eqz p3, :cond_1

    sget v0, Lil;->social_fav_with_two_user:I

    new-array v1, v5, [Ljava/lang/Object;

    aput-object p2, v1, v4

    aput-object p3, v1, v3

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :cond_1
    sget v0, Lil;->social_fav_with_user:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :pswitch_14
    sget v0, Lij;->social_fav_count_with_user:I

    new-array v1, v5, [Ljava/lang/Object;

    aput-object p2, v1, v4

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, p4, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :pswitch_15
    sget v0, Lij;->social_fav_count:I

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p0, p4}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, p4, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :pswitch_16
    sget v0, Lil;->social_retweet_with_user:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :pswitch_17
    sget v0, Lij;->social_retweet_count:I

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p0, p5}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, p5, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :pswitch_18
    sget v0, Lil;->social_top_news:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :pswitch_19
    sget v0, Lil;->top_tweet:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :pswitch_1a
    sget v0, Lil;->social_trending_topic:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :pswitch_1b
    invoke-static {p0, p7, p8}, Lcom/twitter/library/util/Util;->c(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    sget v1, Lil;->highlight_tweet:I

    new-array v2, v3, [Ljava/lang/Object;

    aput-object v0, v2, v4

    invoke-virtual {p0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :cond_2
    sget v0, Lil;->highlight_tweet_today:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :pswitch_1c
    if-nez p6, :cond_4

    if-nez p3, :cond_3

    sget v0, Lil;->social_conversation_tweet:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :cond_3
    sget v0, Lil;->social_conversation_tweet_two:I

    new-array v1, v5, [Ljava/lang/Object;

    aput-object p2, v1, v4

    aput-object p3, v1, v3

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :cond_4
    if-nez p3, :cond_5

    sget v0, Lij;->social_proof_in_reply_name_and_count:I

    new-array v1, v5, [Ljava/lang/Object;

    aput-object p2, v1, v4

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, p6, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :cond_5
    sget v0, Lij;->social_proof_in_reply_multiple_names_and_count:I

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v4

    aput-object p3, v1, v3

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {p0, v0, p6, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :pswitch_1d
    sget v0, Lil;->social_context_mutual_follow:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :pswitch_1e
    sget v0, Lil;->social_following:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :pswitch_1f
    sget v0, Lil;->highlight_context_nearby:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :pswitch_20
    sget v0, Lil;->highlight_context_popular:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :pswitch_21
    sget v0, Lil;->social_who_to_follow:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_1a
        :pswitch_19
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_14
        :pswitch_1
    .end packed-switch
.end method

.method private a(Lcom/twitter/library/api/RecommendedContent;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 4

    iget-object v0, p1, Lcom/twitter/library/api/RecommendedContent;->socialProof:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget v0, Lil;->social_follow_and_follow:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p1, Lcom/twitter/library/api/RecommendedContent;->socialProof:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method private a(Lcom/twitter/library/provider/Tweet;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->eE:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget v0, p1, Lcom/twitter/library/provider/Tweet;->V:I

    if-lez v0, :cond_0

    iget v0, p1, Lcom/twitter/library/provider/Tweet;->V:I

    invoke-static {p2, v0}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/twitter/library/provider/Tweet;Landroid/content/res/Resources;J)Ljava/lang/String;
    .locals 5

    iget v0, p1, Lcom/twitter/library/provider/Tweet;->ah:I

    iget-wide v1, p1, Lcom/twitter/library/provider/Tweet;->q:J

    cmp-long v1, v1, p3

    if-nez v1, :cond_0

    invoke-virtual {p1, p3, p4}, Lcom/twitter/library/provider/Tweet;->a(J)Z

    move-result v1

    if-nez v1, :cond_0

    if-lez v0, :cond_0

    iget v1, p0, Lcom/twitter/library/widget/TweetView;->dQ:I

    if-lt v0, v1, :cond_0

    sget v1, Lij;->social_view_count:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2, v0}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p2, v1, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/twitter/library/provider/Tweet;Z)Ljava/lang/String;
    .locals 10

    const/4 v1, 0x1

    const/4 v3, 0x0

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->es:I

    invoke-direct {p0, v0}, Lcom/twitter/library/widget/TweetView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v2, p0, Lcom/twitter/library/widget/TweetView;->ec:Z

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->W()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->X()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v4, v1

    :goto_0
    move-object v0, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/provider/Tweet;->a(ZZZZZ)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_1
    move v4, v3

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/twitter/library/widget/TweetView;->es:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_3

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->es:I

    const/4 v2, 0x5

    if-eq v0, v2, :cond_3

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->es:I

    const/4 v2, 0x4

    if-eq v0, v2, :cond_3

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->es:I

    const/4 v2, 0x6

    if-ne v0, v2, :cond_4

    :cond_3
    iget-boolean v7, p0, Lcom/twitter/library/widget/TweetView;->ed:Z

    move-object v4, p1

    move v5, v1

    move v6, v3

    move v8, v3

    move v9, v3

    invoke-virtual/range {v4 .. v9}, Lcom/twitter/library/provider/Tweet;->a(ZZZZZ)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->z()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method static synthetic a(Lcom/twitter/library/widget/TweetView;)Ljava/util/LinkedHashMap;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dy:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method private a(IIII)V
    .locals 17

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/widget/TweetView;->dy:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move/from16 v2, p1

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/widget/TweetView$InlineAction;

    iget-object v3, v1, Lcom/twitter/library/widget/TweetView$InlineAction;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v10

    iget-object v3, v1, Lcom/twitter/library/widget/TweetView$InlineAction;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v8

    iget-object v3, v1, Lcom/twitter/library/widget/TweetView$InlineAction;->f:Lcom/twitter/library/view/TweetActionType;

    sget-object v4, Lcom/twitter/library/view/TweetActionType;->e:Lcom/twitter/library/view/TweetActionType;

    if-ne v3, v4, :cond_1

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v3, :cond_0

    sub-int v3, p1, p2

    :goto_1
    div-int/lit8 v4, p4, 0x2

    add-int v4, v4, p3

    div-int/lit8 v5, v10, 0x2

    sub-int v11, v4, v5

    iget-object v4, v1, Lcom/twitter/library/widget/TweetView$InlineAction;->g:Landroid/text/StaticLayout;

    if-eqz v4, :cond_3

    iget-object v4, v1, Lcom/twitter/library/widget/TweetView$InlineAction;->g:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v12

    iget-object v4, v1, Lcom/twitter/library/widget/TweetView$InlineAction;->g:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getWidth()I

    move-result v13

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->aQ:I

    add-int/2addr v4, v8

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->dL:I

    invoke-static {v13, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    add-int v6, v4, v5

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->aQ:I

    add-int/2addr v4, v13

    add-int v7, v8, v4

    div-int/lit8 v4, p4, 0x2

    add-int v4, v4, p3

    div-int/lit8 v5, v12, 0x2

    sub-int v14, v4, v5

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->aQ:I

    add-int/2addr v4, v13

    sub-int/2addr v3, v4

    add-int v4, v3, v13

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->aQ:I

    add-int/2addr v4, v5

    move v5, v3

    :goto_2
    iget-object v15, v1, Lcom/twitter/library/widget/TweetView$InlineAction;->h:Landroid/graphics/Rect;

    add-int/2addr v13, v3

    add-int/2addr v12, v14

    invoke-virtual {v15, v3, v14, v13, v12}, Landroid/graphics/Rect;->set(IIII)V

    move v3, v6

    move v6, v5

    move v5, v7

    :goto_3
    iget-object v7, v1, Lcom/twitter/library/widget/TweetView$InlineAction;->b:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget v12, v0, Lcom/twitter/library/widget/TweetView;->bp:I

    sub-int v12, v6, v12

    move-object/from16 v0, p0

    iget v13, v0, Lcom/twitter/library/widget/TweetView;->bp:I

    sub-int v13, p3, v13

    add-int/2addr v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/twitter/library/widget/TweetView;->bp:I

    add-int/2addr v5, v6

    add-int v6, p3, p4

    move-object/from16 v0, p0

    iget v14, v0, Lcom/twitter/library/widget/TweetView;->bp:I

    add-int/2addr v6, v14

    invoke-virtual {v7, v12, v13, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v5, v1, Lcom/twitter/library/widget/TweetView$InlineAction;->c:Landroid/graphics/Rect;

    add-int v6, v4, v8

    add-int v7, v11, v10

    invoke-virtual {v5, v4, v11, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v1, v1, Lcom/twitter/library/widget/TweetView$InlineAction;->f:Lcom/twitter/library/view/TweetActionType;

    sget-object v4, Lcom/twitter/library/view/TweetActionType;->e:Lcom/twitter/library/view/TweetActionType;

    if-eq v1, v4, :cond_6

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v1, :cond_4

    move-object/from16 v0, p0

    iget v1, v0, Lcom/twitter/library/widget/TweetView;->dM:I

    add-int/2addr v1, v3

    sub-int v1, v2, v1

    :goto_4
    move v2, v1

    goto/16 :goto_0

    :cond_0
    add-int v3, p1, p2

    sub-int/2addr v3, v8

    goto/16 :goto_1

    :cond_1
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v3, :cond_7

    sub-int v3, v2, v8

    goto/16 :goto_1

    :cond_2
    add-int v4, v3, v8

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->aQ:I

    add-int/2addr v4, v5

    move v5, v3

    move/from16 v16, v3

    move v3, v4

    move/from16 v4, v16

    goto :goto_2

    :cond_3
    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->aQ:I

    add-int/2addr v4, v8

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->dL:I

    add-int/2addr v4, v5

    move v5, v8

    move v6, v3

    move/from16 v16, v3

    move v3, v4

    move/from16 v4, v16

    goto :goto_3

    :cond_4
    move-object/from16 v0, p0

    iget v1, v0, Lcom/twitter/library/widget/TweetView;->dM:I

    add-int/2addr v1, v3

    add-int/2addr v1, v2

    goto :goto_4

    :cond_5
    return-void

    :cond_6
    move v1, v2

    goto :goto_4

    :cond_7
    move v3, v2

    goto/16 :goto_1
.end method

.method private a(Landroid/content/res/Resources;ILjava/lang/String;IILjava/lang/String;I)V
    .locals 9

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->eE:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/16 v0, 0xe

    if-eq p2, v0, :cond_1

    const/16 v0, 0xf

    if-eq p2, v0, :cond_1

    const/16 v0, 0x11

    if-eq p2, v0, :cond_1

    const/16 v0, 0x13

    if-eq p2, v0, :cond_1

    :cond_0
    const/16 v0, 0x18

    if-ne p2, v0, :cond_2

    invoke-static {}, Lkn;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->dV:Z

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    iget-wide v7, v0, Lcom/twitter/library/provider/Tweet;->h:J

    move-object v0, p1

    move v1, p2

    move-object v2, p3

    move-object v3, p6

    move v4, p4

    move v5, p5

    move/from16 v6, p7

    invoke-static/range {v0 .. v8}, Lcom/twitter/library/widget/TweetView;->a(Landroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;IIIJ)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->cW:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    iget-wide v7, v0, Lcom/twitter/library/provider/Tweet;->h:J

    move-object v0, p1

    move v1, p2

    move-object v2, p3

    move-object v3, p6

    move v4, p4

    move v5, p5

    move/from16 v6, p7

    invoke-static/range {v0 .. v8}, Lcom/twitter/library/widget/TweetView;->b(Landroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;IIIJ)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->cX:Ljava/lang/String;

    packed-switch p2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    iget v0, p0, Lcom/twitter/library/widget/TweetView;->ar:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->cP:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :pswitch_2
    iget v0, p0, Lcom/twitter/library/widget/TweetView;->ap:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->cP:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :pswitch_3
    iget v0, p0, Lcom/twitter/library/widget/TweetView;->as:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->cP:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :pswitch_4
    iget v0, p0, Lcom/twitter/library/widget/TweetView;->at:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->cP:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :pswitch_5
    iget v0, p0, Lcom/twitter/library/widget/TweetView;->aq:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->cP:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :pswitch_6
    iget v0, p0, Lcom/twitter/library/widget/TweetView;->E:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->cP:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :pswitch_7
    iget v0, p0, Lcom/twitter/library/widget/TweetView;->au:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->cP:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :pswitch_8
    iget v0, p0, Lcom/twitter/library/widget/TweetView;->av:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->cP:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :pswitch_9
    iget v0, p0, Lcom/twitter/library/widget/TweetView;->aw:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->cP:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :pswitch_a
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->cP:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :pswitch_b
    iget v0, p0, Lcom/twitter/library/widget/TweetView;->ax:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->cP:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_5
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_5
        :pswitch_2
        :pswitch_5
        :pswitch_5
        :pswitch_2
        :pswitch_2
        :pswitch_6
        :pswitch_a
        :pswitch_6
        :pswitch_6
        :pswitch_4
        :pswitch_1
        :pswitch_1
        :pswitch_7
        :pswitch_8
        :pswitch_1
        :pswitch_9
        :pswitch_0
        :pswitch_9
        :pswitch_5
        :pswitch_b
    .end packed-switch
.end method

.method private a(Landroid/graphics/Canvas;II)V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->dJ:I

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ca:Landroid/text/StaticLayout;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ca:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getWidth()I

    move-result v0

    sub-int p2, p3, v0

    :cond_0
    int-to-float v0, p2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ca:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method private a(Landroid/graphics/Canvas;ILandroid/text/TextPaint;)V
    .locals 2

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iget v1, p0, Lcom/twitter/library/widget/TweetView;->dK:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->bx:F

    invoke-virtual {p3, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    invoke-virtual {p3, p2}, Landroid/text/TextPaint;->setColor(I)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->cf:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method private a(Landroid/graphics/Canvas;Landroid/graphics/Paint;I)V
    .locals 6

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->ds:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ck:Landroid/text/StaticLayout;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->cm:Landroid/text/StaticLayout;

    if-eqz v0, :cond_4

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->dF:I

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->cp:Lcom/twitter/library/widget/al;

    invoke-virtual {v0}, Lcom/twitter/library/widget/al;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->dt:Z

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->ba:I

    :goto_0
    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->dG:I

    int-to-float v2, v2

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->dG:I

    iget v3, p0, Lcom/twitter/library/widget/TweetView;->dH:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget v3, p0, Lcom/twitter/library/widget/TweetView;->bj:I

    rsub-int/lit8 v3, v3, 0x0

    int-to-float v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->dG:I

    iget v3, p0, Lcom/twitter/library/widget/TweetView;->dH:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->ci:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->ci:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->ci:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->ci:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->cj:I

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->bb:I

    int-to-float v0, v0

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->bc:I

    rsub-int/lit8 v2, v2, 0x0

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->dG:I

    int-to-float v2, v2

    iget v3, p0, Lcom/twitter/library/widget/TweetView;->bc:I

    rsub-int/lit8 v3, v3, 0x0

    int-to-float v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->dG:I

    iget v3, p0, Lcom/twitter/library/widget/TweetView;->dH:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget v3, p0, Lcom/twitter/library/widget/TweetView;->bj:I

    rsub-int/lit8 v3, v3, 0x0

    iget v4, p0, Lcom/twitter/library/widget/TweetView;->bc:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->dG:I

    iget v3, p0, Lcom/twitter/library/widget/TweetView;->dH:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget v3, p0, Lcom/twitter/library/widget/TweetView;->bc:I

    rsub-int/lit8 v3, v3, 0x0

    int-to-float v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->ci:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    iget v3, p0, Lcom/twitter/library/widget/TweetView;->bc:I

    rsub-int/lit8 v3, v3, 0x0

    int-to-float v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->bc:I

    rsub-int/lit8 v2, v2, 0x0

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->dF:I

    int-to-float v2, v0

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->dG:I

    int-to-float v3, v0

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->dF:I

    int-to-float v4, v0

    move-object v0, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->dF:I

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->bb:I

    add-int/2addr v0, v2

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->bf:I

    add-int/2addr v0, v2

    int-to-float v2, p3

    int-to-float v0, v0

    invoke-virtual {p1, v2, v0}, Landroid/graphics/Canvas;->translate(FF)V

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->cl:I

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ck:Landroid/text/StaticLayout;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->cI:F

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->cl:I

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bD:Lcom/twitter/internal/android/widget/ax;

    iget-object v0, v0, Lcom/twitter/internal/android/widget/ax;->c:Landroid/graphics/Typeface;

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ck:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ck:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->be:I

    add-int/2addr v0, v2

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->cm:Landroid/text/StaticLayout;

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->cI:F

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->cn:I

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bD:Lcom/twitter/internal/android/widget/ax;

    iget-object v0, v0, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->cm:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->dI:I

    int-to-float v0, v0

    iget v1, p0, Lcom/twitter/library/widget/TweetView;->dF:I

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->bk:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->cq:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ct:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ct:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    sub-int v0, p3, v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->ci:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->dF:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->ct:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v1, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v1, v3

    add-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ct:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_4
    return-void

    :cond_5
    iget v0, p0, Lcom/twitter/library/widget/TweetView;->cj:I

    goto/16 :goto_0
.end method

.method private a(Landroid/graphics/Canvas;Landroid/graphics/RectF;ILandroid/graphics/Paint;)V
    .locals 1

    const/high16 v0, 0x40400000    # 3.0f

    if-eqz p3, :cond_0

    invoke-virtual {p4, p3}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p1, p2, v0, v0, p4}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method private a(Landroid/text/StaticLayout;Landroid/graphics/Canvas;ILandroid/graphics/Paint;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bD:Lcom/twitter/internal/android/widget/ax;

    iget-object v0, v0, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    invoke-virtual {p4, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->cD:F

    invoke-virtual {p4, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    invoke-virtual {p4, p3}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p1, p2}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method private a(Landroid/text/TextPaint;I)V
    .locals 7

    const/4 v4, 0x0

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->dO:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    sub-int v1, p2, v1

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->dy:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v1

    move v1, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/TweetView$InlineAction;

    sget-object v3, Lcom/twitter/library/widget/w;->b:[I

    iget-object v6, v0, Lcom/twitter/library/widget/TweetView$InlineAction;->f:Lcom/twitter/library/view/TweetActionType;

    invoke-virtual {v6}, Lcom/twitter/library/view/TweetActionType;->ordinal()I

    move-result v6

    aget v3, v3, v6

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    move-object v3, v4

    :goto_1
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-direct {p0, v3, p1}, Lcom/twitter/library/widget/TweetView;->a(Ljava/lang/String;Landroid/text/TextPaint;)Landroid/text/StaticLayout;

    move-result-object v3

    iput-object v3, v0, Lcom/twitter/library/widget/TweetView$InlineAction;->g:Landroid/text/StaticLayout;

    :goto_2
    invoke-static {}, Lcom/twitter/library/featureswitch/a;->E()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView$InlineAction;->f:Lcom/twitter/library/view/TweetActionType;

    sget-object v6, Lcom/twitter/library/view/TweetActionType;->e:Lcom/twitter/library/view/TweetActionType;

    if-eq v3, v6, :cond_3

    iget-object v0, v0, Lcom/twitter/library/widget/TweetView$InlineAction;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    sub-int v0, v2, v0

    add-int/lit8 v1, v1, 0x1

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->aQ:I

    iget v3, p0, Lcom/twitter/library/widget/TweetView;->dM:I

    add-int/2addr v2, v3

    sub-int v2, v0, v2

    move v0, v1

    move v1, v2

    :goto_3
    move v2, v1

    move v1, v0

    goto :goto_0

    :pswitch_1
    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->dB:Ljava/lang/String;

    goto :goto_1

    :pswitch_2
    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->dA:Ljava/lang/String;

    goto :goto_1

    :pswitch_3
    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->dC:Ljava/lang/String;

    goto :goto_1

    :cond_0
    iput-object v4, v0, Lcom/twitter/library/widget/TweetView$InlineAction;->g:Landroid/text/StaticLayout;

    goto :goto_2

    :cond_1
    if-lez v1, :cond_2

    div-int v0, v2, v1

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->dL:I

    :cond_2
    return-void

    :cond_3
    move v0, v1

    move v1, v2

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private a(Lcom/twitter/library/provider/Tweet;Landroid/graphics/Canvas;)V
    .locals 10

    const/high16 v3, 0x40000000    # 2.0f

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->W()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->X()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->cd:Landroid/text/StaticLayout;

    if-eqz v0, :cond_3

    sget-object v5, Lcom/twitter/library/widget/TweetView;->m:Landroid/text/TextPaint;

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->di:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getPaddingRight()I

    move-result v1

    int-to-float v1, v1

    sub-float v1, v0, v1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dk:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move v6, v0

    move v0, v1

    :goto_0
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->dk:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->dk:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v1, v2

    div-float v7, v1, v3

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->dj:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->dj:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    add-float/2addr v1, v2

    div-float/2addr v1, v3

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    if-eqz v2, :cond_2

    invoke-virtual {p2}, Landroid/graphics/Canvas;->save()I

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->eh:Landroid/graphics/RectF;

    if-nez v2, :cond_1

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iput-object v2, p0, Lcom/twitter/library/widget/TweetView;->eh:Landroid/graphics/RectF;

    :cond_1
    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->eh:Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    iget-object v8, p0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    int-to-float v8, v8

    iget-object v9, p0, Lcom/twitter/library/widget/TweetView;->dl:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v2, v3, v4, v8, v9}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->eh:Landroid/graphics/RectF;

    iget v3, p0, Lcom/twitter/library/widget/TweetView;->bt:I

    invoke-direct {p0, p2, v2, v3, v5}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Canvas;Landroid/graphics/RectF;ILandroid/graphics/Paint;)V

    invoke-virtual {p2}, Landroid/graphics/Canvas;->restore()V

    :cond_2
    invoke-virtual {p2}, Landroid/graphics/Canvas;->save()I

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->W()Z

    move-result v2

    if-eqz v2, :cond_5

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->cM:I

    invoke-virtual {v5, v2}, Landroid/text/TextPaint;->setColor(I)V

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->cD:F

    invoke-virtual {v5, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    :goto_1
    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->di:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    invoke-virtual {p2, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->cd:Landroid/text/StaticLayout;

    invoke-virtual {v0, p2}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p2}, Landroid/graphics/Canvas;->restore()V

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->W()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p2}, Landroid/graphics/Canvas;->save()I

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->cA:I

    invoke-virtual {v5, v0}, Landroid/text/TextPaint;->setColor(I)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dj:Landroid/graphics/RectF;

    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dj:Landroid/graphics/RectF;

    iget v4, v0, Landroid/graphics/RectF;->top:F

    move-object v0, p2

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    invoke-virtual {p2}, Landroid/graphics/Canvas;->restore()V

    invoke-virtual {p2}, Landroid/graphics/Canvas;->save()I

    invoke-virtual {p2, v6, v7}, Landroid/graphics/Canvas;->translate(FF)V

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->cM:I

    invoke-virtual {v5, v0}, Landroid/text/TextPaint;->setColor(I)V

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->cD:F

    invoke-virtual {v5, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ce:Landroid/text/StaticLayout;

    invoke-virtual {v0, p2}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p2}, Landroid/graphics/Canvas;->restore()V

    :cond_3
    return-void

    :cond_4
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->di:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v1, v0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dk:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move v6, v0

    move v0, v1

    goto/16 :goto_0

    :cond_5
    iget v2, p0, Lcom/twitter/library/widget/TweetView;->cL:I

    invoke-virtual {v5, v2}, Landroid/text/TextPaint;->setColor(I)V

    goto :goto_1
.end method

.method private a(Lcom/twitter/library/widget/TweetView$InlineAction;Z)V
    .locals 11
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    const v7, 0x3fcccccd    # 1.6f

    const v3, 0x3fc66666    # 1.55f

    const/high16 v2, 0x3f800000    # 1.0f

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_1

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->b:Lcom/twitter/library/widget/aa;

    iget-object v1, p1, Lcom/twitter/library/widget/TweetView$InlineAction;->f:Lcom/twitter/library/view/TweetActionType;

    invoke-interface {v0, v1, p0}, Lcom/twitter/library/widget/aa;->b(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/widget/TweetView;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p1, Lcom/twitter/library/widget/TweetView$InlineAction;->d:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_0

    const-wide/16 v4, 0x23

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/widget/TweetView$InlineAction;FFJ)Landroid/animation/ValueAnimator;

    move-result-object v0

    const-wide/16 v8, 0x32

    move-object v4, p0

    move-object v5, p1

    move v6, v3

    invoke-direct/range {v4 .. v9}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/widget/TweetView$InlineAction;FFJ)Landroid/animation/ValueAnimator;

    move-result-object v1

    const-wide/16 v9, 0xa5

    move-object v5, p0

    move-object v6, p1

    move v8, v2

    invoke-direct/range {v5 .. v10}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/widget/TweetView$InlineAction;FFJ)Landroid/animation/ValueAnimator;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/widget/v;

    invoke-direct {v3, p0, p2, p1}, Lcom/twitter/library/widget/v;-><init>(Lcom/twitter/library/widget/TweetView;ZLcom/twitter/library/widget/TweetView$InlineAction;)V

    invoke-virtual {v2, v3}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    const/4 v4, 0x3

    new-array v4, v4, [Landroid/animation/Animator;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    aput-object v2, v4, v0

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/library/widget/TweetView;Lcom/twitter/library/widget/TweetView$InlineAction;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/widget/TweetView;->b(Lcom/twitter/library/widget/TweetView$InlineAction;Z)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/widget/TweetView;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/library/widget/TweetView;->b(Z)V

    return-void
.end method

.method private a(Lcom/twitter/library/provider/Tweet;J)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->bP:Lcom/twitter/library/util/FriendshipCache;

    if-eqz v2, :cond_0

    iget-wide v2, p1, Lcom/twitter/library/provider/Tweet;->n:J

    cmp-long v2, v2, p2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->bP:Lcom/twitter/library/util/FriendshipCache;

    iget-wide v3, p1, Lcom/twitter/library/provider/Tweet;->n:J

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/util/FriendshipCache;->a(J)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->bP:Lcom/twitter/library/util/FriendshipCache;

    iget-wide v3, p1, Lcom/twitter/library/provider/Tweet;->n:J

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/util/FriendshipCache;->h(J)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :goto_1
    and-int/lit8 v3, v2, 0x1

    if-eqz v3, :cond_4

    move v3, v1

    :goto_2
    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_5

    move v2, v1

    :goto_3
    if-eqz v3, :cond_2

    if-nez v2, :cond_2

    invoke-direct {p0, p1}, Lcom/twitter/library/widget/TweetView;->c(Lcom/twitter/library/provider/Tweet;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1

    :cond_4
    move v3, v0

    goto :goto_2

    :cond_5
    move v2, v0

    goto :goto_3
.end method

.method private a(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/provider/Tweet;)Z
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->bO:Lcom/twitter/library/widget/ap;

    invoke-interface {v2}, Lcom/twitter/library/widget/ap;->a()J

    move-result-wide v2

    invoke-direct {p0, p2}, Lcom/twitter/library/widget/TweetView;->c(Lcom/twitter/library/provider/Tweet;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-direct {p0, p2, v2, v3}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/provider/Tweet;J)Z

    move-result v4

    if-eqz v4, :cond_2

    sget-object v2, Lcom/twitter/library/view/TweetActionType;->e:Lcom/twitter/library/view/TweetActionType;

    if-ne p1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->v()Z

    move-result v4

    if-eqz v4, :cond_3

    sget-object v2, Lcom/twitter/library/view/TweetActionType;->e:Lcom/twitter/library/view/TweetActionType;

    if-eq p1, v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    sget-object v1, Lcom/twitter/library/view/TweetActionType;->e:Lcom/twitter/library/view/TweetActionType;

    if-ne p1, v1, :cond_0

    invoke-direct {p0, p2, v2, v3}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/provider/Tweet;J)Z

    move-result v0

    goto :goto_0
.end method

.method private a(Lcom/twitter/library/view/TweetActionType;)[I
    .locals 2

    sget-object v0, Lcom/twitter/library/widget/w;->b:[I

    invoke-virtual {p1}, Lcom/twitter/library/view/TweetActionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    invoke-static {}, Lcom/twitter/library/widget/TweetView$InlineAction;->e()[I

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    invoke-static {}, Lcom/twitter/library/widget/TweetView$InlineAction;->a()[I

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-static {}, Lcom/twitter/library/widget/TweetView$InlineAction;->b()[I

    move-result-object v0

    goto :goto_0

    :pswitch_2
    invoke-static {}, Lcom/twitter/library/widget/TweetView$InlineAction;->c()[I

    move-result-object v0

    goto :goto_0

    :pswitch_3
    invoke-static {}, Lcom/twitter/library/widget/TweetView$InlineAction;->d()[I

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic a([I[I)[I
    .locals 1

    invoke-static {p0, p1}, Lcom/twitter/library/widget/TweetView;->mergeDrawableStates([I[I)[I

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/library/widget/TweetView;J)I
    .locals 2

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->dh:I

    int-to-long v0, v0

    and-long/2addr v0, p1

    long-to-int v0, v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->dh:I

    return v0
.end method

.method static synthetic b(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/library/provider/Tweet;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    return-object v0
.end method

.method private b(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/provider/Tweet;)Lcom/twitter/library/widget/TweetView$InlineAction$State;
    .locals 4

    sget-object v0, Lcom/twitter/library/view/TweetActionType;->b:Lcom/twitter/library/view/TweetActionType;

    if-ne p1, v0, :cond_0

    iget-boolean v0, p2, Lcom/twitter/library/provider/Tweet;->l:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/library/widget/TweetView$InlineAction$State;->a:Lcom/twitter/library/widget/TweetView$InlineAction$State;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/twitter/library/view/TweetActionType;->c:Lcom/twitter/library/view/TweetActionType;

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bO:Lcom/twitter/library/widget/ap;

    invoke-interface {v0}, Lcom/twitter/library/widget/ap;->a()J

    move-result-wide v0

    sget-object v2, Lcom/twitter/library/view/TweetActionType;->c:Lcom/twitter/library/view/TweetActionType;

    if-ne p1, v2, :cond_2

    iget-wide v2, p2, Lcom/twitter/library/provider/Tweet;->n:J

    cmp-long v2, v2, v0

    if-eqz v2, :cond_1

    iget-boolean v2, p2, Lcom/twitter/library/provider/Tweet;->m:Z

    if-eqz v2, :cond_2

    :cond_1
    sget-object v0, Lcom/twitter/library/widget/TweetView$InlineAction$State;->c:Lcom/twitter/library/widget/TweetView$InlineAction$State;

    goto :goto_0

    :cond_2
    invoke-virtual {p2, v0, v1}, Lcom/twitter/library/provider/Tweet;->a(J)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/twitter/library/widget/TweetView$InlineAction$State;->a:Lcom/twitter/library/widget/TweetView$InlineAction$State;

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/twitter/library/view/TweetActionType;->e:Lcom/twitter/library/view/TweetActionType;

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bP:Lcom/twitter/library/util/FriendshipCache;

    iget-wide v1, p2, Lcom/twitter/library/provider/Tweet;->n:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/util/FriendshipCache;->i(J)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/twitter/library/widget/TweetView$InlineAction$State;->a:Lcom/twitter/library/widget/TweetView$InlineAction$State;

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/twitter/library/widget/TweetView$InlineAction$State;->b:Lcom/twitter/library/widget/TweetView$InlineAction$State;

    goto :goto_0
.end method

.method public static b(Landroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;IIIJ)Ljava/lang/String;
    .locals 4

    const/4 v1, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    sparse-switch p1, :sswitch_data_0

    invoke-static/range {p0 .. p8}, Lcom/twitter/library/widget/TweetView;->a(Landroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;IIIJ)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :sswitch_0
    sget v0, Lil;->social_follow_and_follow_accessibility_description:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_1
    sget v0, Lil;->tweets_retweeted_accessibility_description:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_2
    sget v0, Lil;->social_follower_and_retweets_accessibility_description:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_3
    sget v0, Lil;->social_follow_and_reply_accessibility_description:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_4
    sget v0, Lil;->social_follower_and_reply_accessibility_description:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_5
    sget v0, Lil;->social_follow_and_fav_accessibility_description:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_6
    sget v0, Lil;->social_follower_and_fav_accessibility_description:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_7
    sget v0, Lil;->tweets_retweeted_accessibility_description:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_8
    if-eqz p3, :cond_0

    sget v0, Lil;->social_fav_with_two_user_accessibility_description:I

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v2

    aput-object p3, v1, v3

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    sget v0, Lil;->social_fav_with_user_accessibility_description:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_9
    sget v0, Lij;->social_fav_count_with_user_accessibility_description:I

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, p4, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :sswitch_a
    sget v0, Lil;->social_retweet_with_user_accessibility_description:I

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x5 -> :sswitch_1
        0x6 -> :sswitch_2
        0x7 -> :sswitch_3
        0x8 -> :sswitch_4
        0x9 -> :sswitch_5
        0xa -> :sswitch_6
        0xd -> :sswitch_7
        0x10 -> :sswitch_8
        0x12 -> :sswitch_a
        0x21 -> :sswitch_9
    .end sparse-switch
.end method

.method private b(Lcom/twitter/library/provider/Tweet;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->eE:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget v0, p1, Lcom/twitter/library/provider/Tweet;->ae:I

    if-lez v0, :cond_0

    iget v0, p1, Lcom/twitter/library/provider/Tweet;->ae:I

    invoke-static {p2, v0}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/graphics/Canvas;)V
    .locals 7

    sget-object v5, Lcom/twitter/library/widget/TweetView;->m:Landroid/text/TextPaint;

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->de:Landroid/graphics/RectF;

    iget v1, p0, Lcom/twitter/library/widget/TweetView;->cu:I

    invoke-direct {p0, p1, v0, v1, v5}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Canvas;Landroid/graphics/RectF;ILandroid/graphics/Paint;)V

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dd:Landroid/graphics/RectF;

    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->aC:I

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->aJ:I

    add-int/2addr v0, v2

    :goto_0
    int-to-float v0, v0

    add-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    :goto_1
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->eh:Landroid/graphics/RectF;

    if-nez v1, :cond_0

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/twitter/library/widget/TweetView;->eh:Landroid/graphics/RectF;

    :cond_0
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->eh:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->dk:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    int-to-float v4, v4

    iget-object v6, p0, Lcom/twitter/library/widget/TweetView;->dl:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v1, v2, v3, v4, v6}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->eh:Landroid/graphics/RectF;

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->bt:I

    invoke-direct {p0, p1, v1, v2, v5}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Canvas;Landroid/graphics/RectF;ILandroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->dd:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->aJ:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->cL:I

    invoke-virtual {v5, v0}, Landroid/text/TextPaint;->setColor(I)V

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->bv:F

    invoke-virtual {v5, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->cd:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dd:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    float-to-int v1, v0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    :goto_2
    iget v2, p0, Lcom/twitter/library/widget/TweetView;->cA:I

    invoke-virtual {v5, v2}, Landroid/text/TextPaint;->setColor(I)V

    int-to-float v1, v1

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    int-to-float v3, v0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dk:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    :goto_3
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->dk:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->dk:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getPaddingBottom()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->cz:I

    invoke-virtual {v5, v0}, Landroid/text/TextPaint;->setColor(I)V

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->cD:F

    invoke-virtual {v5, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ce:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void

    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dd:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    goto/16 :goto_1

    :cond_4
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dd:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    float-to-int v1, v0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dk:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    goto :goto_3
.end method

.method private b(Landroid/graphics/Canvas;II)V
    .locals 4

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    const/4 v0, 0x0

    iget v1, p0, Lcom/twitter/library/widget/TweetView;->dE:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ch:Landroid/text/StaticLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->cU:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->ch:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    sub-int v3, v0, v1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->cU:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iget v1, p0, Lcom/twitter/library/widget/TweetView;->D:I

    add-int/2addr v0, v1

    iget-boolean v1, p0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v1, :cond_1

    sub-int p2, p3, v0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ch:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getWidth()I

    move-result v0

    sub-int v0, p2, v0

    move v2, v0

    :goto_0
    if-lez v3, :cond_2

    const/4 v1, 0x0

    div-int/lit8 v0, v3, 0x2

    :goto_1
    int-to-float v3, p2

    int-to-float v1, v1

    invoke-virtual {p1, v3, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->cU:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    int-to-float v1, v2

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ch:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void

    :cond_1
    move v2, v0

    goto :goto_0

    :cond_2
    neg-int v0, v3

    div-int/lit8 v1, v0, 0x2

    neg-int v0, v1

    goto :goto_1
.end method

.method private b(Landroid/text/TextPaint;I)V
    .locals 12

    const/4 v2, 0x0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->ca:Landroid/text/StaticLayout;

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dz:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dz:Ljava/lang/String;

    invoke-static {v0, p1}, Lhl;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v5

    if-lt p2, v5, :cond_0

    new-instance v0, Landroid/text/StaticLayout;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->dz:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->dz:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    iget v7, p0, Lcom/twitter/library/widget/TweetView;->A:F

    iget v4, p0, Lcom/twitter/library/widget/TweetView;->C:I

    int-to-float v8, v4

    sget-object v10, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object v4, p1

    move v9, v2

    move v11, p2

    invoke-direct/range {v0 .. v11}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->ca:Landroid/text/StaticLayout;

    :cond_0
    return-void
.end method

.method private b(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/widget/TweetView$InlineAction$State;)V
    .locals 11

    const/4 v10, 0x0

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/widget/TweetView$InlineAction$State;)I

    move-result v9

    if-lez v9, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dy:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v8, Lcom/twitter/library/widget/TweetView$InlineAction;

    invoke-direct {v8}, Lcom/twitter/library/widget/TweetView$InlineAction;-><init>()V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dy:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    mul-int/lit8 v1, v0, 0x2

    new-instance v0, Lcom/twitter/library/widget/al;

    const-wide/16 v2, 0x100

    shl-long/2addr v2, v1

    const-wide/16 v4, 0x80

    shl-long/2addr v4, v1

    new-instance v6, Lcom/twitter/library/widget/ah;

    invoke-direct {v6, p0, v8}, Lcom/twitter/library/widget/ah;-><init>(Lcom/twitter/library/widget/TweetView;Lcom/twitter/library/widget/TweetView$InlineAction;)V

    invoke-direct {p0, p1}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/view/TweetActionType;)[I

    move-result-object v7

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/widget/al;-><init>(Lcom/twitter/library/widget/TweetView;JJLjava/lang/Runnable;[I)V

    iget-object v1, v8, Lcom/twitter/library/widget/TweetView$InlineAction;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/al;->a(Landroid/graphics/Rect;)V

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->dn:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dy:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1, v8}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v8

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v1, v10, v10, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iput-object v1, v0, Lcom/twitter/library/widget/TweetView$InlineAction;->a:Landroid/graphics/drawable/Drawable;

    iput-object p1, v0, Lcom/twitter/library/widget/TweetView$InlineAction;->f:Lcom/twitter/library/view/TweetActionType;

    iput-object p2, v0, Lcom/twitter/library/widget/TweetView$InlineAction;->e:Lcom/twitter/library/widget/TweetView$InlineAction$State;

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dy:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/TweetView$InlineAction;

    goto :goto_0
.end method

.method private b(Lcom/twitter/library/widget/TweetView$InlineAction;Z)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    iget-object v1, p1, Lcom/twitter/library/widget/TweetView$InlineAction;->f:Lcom/twitter/library/view/TweetActionType;

    invoke-direct {p0, v1, v0}, Lcom/twitter/library/widget/TweetView;->b(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/provider/Tweet;)Lcom/twitter/library/widget/TweetView$InlineAction$State;

    move-result-object v0

    iget-object v2, p1, Lcom/twitter/library/widget/TweetView$InlineAction;->d:Landroid/animation/ValueAnimator;

    if-nez v2, :cond_3

    iget-object v2, p1, Lcom/twitter/library/widget/TweetView$InlineAction;->e:Lcom/twitter/library/widget/TweetView$InlineAction$State;

    if-ne v0, v2, :cond_3

    sget-object v2, Lcom/twitter/library/widget/TweetView$InlineAction$State;->c:Lcom/twitter/library/widget/TweetView$InlineAction$State;

    if-eq v0, v2, :cond_3

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->b:Lcom/twitter/library/widget/aa;

    invoke-interface {v0, v1, p0}, Lcom/twitter/library/widget/aa;->a(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/widget/TweetView;)V

    sget-object v0, Lcom/twitter/library/view/TweetActionType;->b:Lcom/twitter/library/view/TweetActionType;

    if-ne v1, v0, :cond_4

    iget-object v0, p1, Lcom/twitter/library/widget/TweetView$InlineAction;->e:Lcom/twitter/library/widget/TweetView$InlineAction$State;

    sget-object v2, Lcom/twitter/library/widget/TweetView$InlineAction$State;->b:Lcom/twitter/library/widget/TweetView$InlineAction$State;

    if-ne v0, v2, :cond_4

    if-eqz p2, :cond_4

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/widget/TweetView$InlineAction;Z)V

    :goto_0
    sget-object v0, Lcom/twitter/library/view/TweetActionType;->b:Lcom/twitter/library/view/TweetActionType;

    if-eq v1, v0, :cond_0

    sget-object v0, Lcom/twitter/library/view/TweetActionType;->e:Lcom/twitter/library/view/TweetActionType;

    if-ne v1, v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->dX:Z

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/twitter/library/widget/TweetView$InlineAction;->e:Lcom/twitter/library/widget/TweetView$InlineAction$State;

    sget-object v2, Lcom/twitter/library/widget/TweetView$InlineAction$State;->a:Lcom/twitter/library/widget/TweetView$InlineAction$State;

    if-ne v0, v2, :cond_6

    sget-object v0, Lcom/twitter/library/widget/TweetView$InlineAction$State;->b:Lcom/twitter/library/widget/TweetView$InlineAction$State;

    :goto_1
    invoke-direct {p0, v1, v0}, Lcom/twitter/library/widget/TweetView;->b(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/widget/TweetView$InlineAction$State;)V

    :cond_1
    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->ds:Z

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/twitter/library/widget/TweetView$InlineAction;->f:Lcom/twitter/library/view/TweetActionType;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->dx:Lcom/twitter/library/view/TweetActionType;

    invoke-virtual {v0, v1}, Lcom/twitter/library/view/TweetActionType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/TweetView;->a(I)V

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->invalidate()V

    :cond_3
    return-void

    :cond_4
    sget-object v0, Lcom/twitter/library/view/TweetActionType;->c:Lcom/twitter/library/view/TweetActionType;

    if-ne v1, v0, :cond_5

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/widget/TweetView$InlineAction;Z)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->b:Lcom/twitter/library/widget/aa;

    invoke-interface {v0, v1, p0}, Lcom/twitter/library/widget/aa;->b(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/widget/TweetView;)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->b:Lcom/twitter/library/widget/aa;

    invoke-interface {v0, v1, p0}, Lcom/twitter/library/widget/aa;->b(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/widget/TweetView;)V

    goto :goto_0

    :cond_6
    sget-object v0, Lcom/twitter/library/widget/TweetView$InlineAction$State;->a:Lcom/twitter/library/widget/TweetView$InlineAction$State;

    goto :goto_1
.end method

.method private b(Z)V
    .locals 2

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->eL:Lcom/twitter/library/widget/al;

    invoke-virtual {v0}, Lcom/twitter/library/widget/al;->h()Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/TweetView;->post(Ljava/lang/Runnable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->eL:Lcom/twitter/library/widget/al;

    invoke-virtual {v0}, Lcom/twitter/library/widget/al;->k()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->eL:Lcom/twitter/library/widget/al;

    invoke-virtual {v0}, Lcom/twitter/library/widget/al;->f()Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/TweetView;->post(Ljava/lang/Runnable;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->eL:Lcom/twitter/library/widget/al;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/al;->a(Z)V

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->eL:Lcom/twitter/library/widget/al;

    return-void
.end method

.method private b(I)Z
    .locals 2

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v1, 0x3

    if-ne p1, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/library/widget/TweetView;)I
    .locals 1

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->dh:I

    return v0
.end method

.method private c(Lcom/twitter/library/provider/Tweet;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->eE:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0, p1}, Lcom/twitter/library/widget/TweetView;->l(Lcom/twitter/library/provider/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p1, Lcom/twitter/library/provider/Tweet;->ag:I

    invoke-static {p2, v0}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Landroid/graphics/Canvas;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bI:Lcom/twitter/library/widget/TweetMediaImagesView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetMediaImagesView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bI:Lcom/twitter/library/widget/TweetMediaImagesView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/widget/TweetMediaImagesView;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_0
    return-void
.end method

.method private c(Landroid/text/TextPaint;I)V
    .locals 12

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dD:Ljava/lang/String;

    invoke-static {v0, p1}, Lhl;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v5

    new-instance v0, Landroid/text/StaticLayout;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->dD:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->dD:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    iget v7, p0, Lcom/twitter/library/widget/TweetView;->A:F

    iget v4, p0, Lcom/twitter/library/widget/TweetView;->C:I

    int-to-float v8, v4

    sget-object v10, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object v4, p1

    move v9, v2

    move v11, p2

    invoke-direct/range {v0 .. v11}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->cf:Landroid/text/StaticLayout;

    return-void
.end method

.method private c(Lcom/twitter/library/provider/Tweet;)Z
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->ab()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(Landroid/text/TextPaint;I)I
    .locals 9

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/twitter/library/widget/TweetView;->dO:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    sub-int v4, p2, v0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dy:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    move v3, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/TweetView$InlineAction;

    iget-object v6, v0, Lcom/twitter/library/widget/TweetView$InlineAction;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v7

    invoke-static {v3, v7}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget-object v7, v0, Lcom/twitter/library/widget/TweetView$InlineAction;->f:Lcom/twitter/library/view/TweetActionType;

    sget-object v8, Lcom/twitter/library/view/TweetActionType;->e:Lcom/twitter/library/view/TweetActionType;

    if-eq v7, v8, :cond_4

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    iget v7, p0, Lcom/twitter/library/widget/TweetView;->aQ:I

    add-int/2addr v6, v7

    iget v7, p0, Lcom/twitter/library/widget/TweetView;->dM:I

    add-int/2addr v6, v7

    add-int/2addr v1, v6

    iget-object v6, v0, Lcom/twitter/library/widget/TweetView$InlineAction;->g:Landroid/text/StaticLayout;

    if-eqz v6, :cond_0

    iget-object v0, v0, Lcom/twitter/library/widget/TweetView$InlineAction;->g:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getWidth()I

    move-result v0

    :goto_1
    iget v6, p0, Lcom/twitter/library/widget/TweetView;->dL:I

    invoke-static {v0, v6}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/2addr v0, v1

    :goto_2
    move v1, v0

    goto :goto_0

    :cond_0
    move v0, v2

    goto :goto_1

    :cond_1
    if-le v1, v4, :cond_2

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dy:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/TweetView$InlineAction;

    const/4 v4, 0x0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView$InlineAction;->g:Landroid/text/StaticLayout;

    goto :goto_3

    :cond_2
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dy:Ljava/util/LinkedHashMap;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dy:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    if-lez v0, :cond_3

    const-string/jumbo v0, ""

    invoke-direct {p0, v0, p1}, Lcom/twitter/library/widget/TweetView;->a(Ljava/lang/String;Landroid/text/TextPaint;)Landroid/text/StaticLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    :cond_3
    return v2

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method private d(Lcom/twitter/library/provider/Tweet;)Z
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->ac()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(Lcom/twitter/library/provider/Tweet;)Z
    .locals 1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->Z()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f(Lcom/twitter/library/provider/Tweet;)Z
    .locals 1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->W()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g(Lcom/twitter/library/provider/Tweet;)Z
    .locals 1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->X()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getInlineActionTypes()[Lcom/twitter/library/view/TweetActionType;
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->dq:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/twitter/library/util/Util;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/twitter/library/widget/TweetView;->o:[Lcom/twitter/library/view/TweetActionType;

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/twitter/library/widget/TweetView;->n:[Lcom/twitter/library/view/TweetActionType;

    goto :goto_0
.end method

.method private getStatsTextHeight()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ca:Landroid/text/StaticLayout;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h(Lcom/twitter/library/provider/Tweet;)Z
    .locals 1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->Y()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->y()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i(Lcom/twitter/library/provider/Tweet;)Z
    .locals 1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->au()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->R()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j(Lcom/twitter/library/provider/Tweet;)Ljava/lang/String;
    .locals 2

    iget-object v0, p1, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    iget-object v1, p1, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/twitter/library/api/PromotedContent;->advertiserName:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p1, Lcom/twitter/library/provider/Tweet;->f:Ljava/lang/String;

    goto :goto_0
.end method

.method private k(Lcom/twitter/library/provider/Tweet;)Ljava/lang/String;
    .locals 6

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->W()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->V()Lcom/twitter/library/card/instance/CardInstanceData;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/card/instance/CardInstanceData;->bindingValues:Ljava/util/HashMap;

    const-string/jumbo v1, "promotion_cta"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/instance/BindingValue;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/twitter/library/card/instance/BindingValue;->value:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->X()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->V()Lcom/twitter/library/card/instance/CardInstanceData;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, v0, Lcom/twitter/library/card/instance/CardInstanceData;->bindingValues:Ljava/util/HashMap;

    const-string/jumbo v1, "phone_number_display"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/instance/BindingValue;

    if-nez v0, :cond_2

    const-string/jumbo v0, ""

    :goto_1
    iget-object v1, p1, Lcom/twitter/library/provider/Tweet;->g:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lil;->call_now_title:I

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const/4 v1, 0x1

    aput-object v0, v4, v1

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, v0, Lcom/twitter/library/card/instance/BindingValue;->value:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    goto :goto_1

    :cond_3
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method private l(Lcom/twitter/library/provider/Tweet;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->dP:Z

    if-eqz v0, :cond_0

    iget v0, p1, Lcom/twitter/library/provider/Tweet;->ag:I

    if-lez v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->J()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->K()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private m(Lcom/twitter/library/provider/Tweet;)V
    .locals 7

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->getInlineActionTypes()[Lcom/twitter/library/view/TweetActionType;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-direct {p0, v4, p1}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/provider/Tweet;)Z

    move-result v5

    iget-object v6, p0, Lcom/twitter/library/widget/TweetView;->dy:Ljava/util/LinkedHashMap;

    invoke-virtual {v6, v4}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eq v5, v4, :cond_2

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->dy:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->clear()V

    :cond_0
    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->getInlineActionTypes()[Lcom/twitter/library/view/TweetActionType;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    invoke-direct {p0, v4, p1}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/provider/Tweet;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-direct {p0, v4, p1}, Lcom/twitter/library/widget/TweetView;->b(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/provider/Tweet;)Lcom/twitter/library/widget/TweetView$InlineAction$State;

    move-result-object v0

    invoke-direct {p0, v4, v0}, Lcom/twitter/library/widget/TweetView;->b(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/widget/TweetView$InlineAction$State;)V

    const/4 v0, 0x1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->refreshDrawableState()V

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->invalidate()V

    :cond_4
    return-void
.end method

.method private u()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->ab()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private v()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->L()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private w()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->ei:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->ej:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private x()V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getDrawableState()[I

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->p:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->p:Landroid/content/res/ColorStateList;

    invoke-virtual {v1, v0, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/widget/TweetView;->cL:I

    :cond_0
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->v:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->v:Landroid/content/res/ColorStateList;

    invoke-virtual {v1, v0, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/widget/TweetView;->cM:I

    :cond_1
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->q:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->q:Landroid/content/res/ColorStateList;

    invoke-virtual {v1, v0, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/widget/TweetView;->cy:I

    :cond_2
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->r:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->r:Landroid/content/res/ColorStateList;

    invoke-virtual {v1, v0, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/widget/TweetView;->cx:I

    :cond_3
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->s:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->s:Landroid/content/res/ColorStateList;

    invoke-virtual {v1, v0, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/widget/TweetView;->cw:I

    :cond_4
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->t:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->t:Landroid/content/res/ColorStateList;

    invoke-virtual {v1, v0, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/widget/TweetView;->cv:I

    :cond_5
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->u:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->u:Landroid/content/res/ColorStateList;

    invoke-virtual {v1, v0, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/widget/TweetView;->cu:I

    :cond_6
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->bC:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->bC:Landroid/content/res/ColorStateList;

    invoke-virtual {v1, v0, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/widget/TweetView;->eA:I

    :cond_7
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->w:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->w:Landroid/content/res/ColorStateList;

    invoke-virtual {v1, v0, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/widget/TweetView;->cz:I

    :cond_8
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->x:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->x:Landroid/content/res/ColorStateList;

    invoke-virtual {v1, v0, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/widget/TweetView;->cA:I

    :cond_9
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->y:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->y:Landroid/content/res/ColorStateList;

    invoke-virtual {v1, v0, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/widget/TweetView;->cB:I

    :cond_a
    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->z:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->z:Landroid/content/res/ColorStateList;

    invoke-virtual {v1, v0, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->cC:I

    :cond_b
    return-void
.end method

.method private y()Z
    .locals 2

    const-string/jumbo v0, "android_promoted_website_card_1832"

    invoke-static {v0}, Lkk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "media_forward_website_card"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private z()Z
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/widget/ListView;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->cN:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->cT:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->invalidate()V

    goto :goto_0
.end method

.method a(I)V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->ds:Z

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->b:Lcom/twitter/library/widget/aa;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    invoke-interface {v0, v1, p0, p1}, Lcom/twitter/library/widget/aa;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/widget/TweetView;I)V

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->requestLayout()V

    return-void
.end method

.method public a(Landroid/graphics/Bitmap;Z)V
    .locals 4

    const/4 v1, 0x0

    if-eqz p1, :cond_2

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v0, v2, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->ab:I

    iget v3, p0, Lcom/twitter/library/widget/TweetView;->ac:I

    invoke-virtual {v0, v1, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :goto_0
    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->cN:Landroid/graphics/drawable/Drawable;

    if-eq v2, v0, :cond_0

    const/4 v1, 0x1

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->cN:Landroid/graphics/drawable/Drawable;

    :cond_0
    move v0, v1

    if-eqz p2, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->invalidate()V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Landroid/graphics/Canvas;)V
    .locals 3

    sget-object v0, Lcom/twitter/library/widget/TweetView;->m:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->B:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->eG:Landroid/graphics/Rect;

    invoke-virtual {p1, v2, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method

.method a(Lcom/twitter/library/provider/Tweet;)V
    .locals 8

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->X()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v6, Lcom/twitter/library/widget/af;

    invoke-direct {v6, p0}, Lcom/twitter/library/widget/af;-><init>(Lcom/twitter/library/widget/TweetView;)V

    :goto_0
    if-eqz v6, :cond_0

    new-instance v0, Lcom/twitter/library/widget/al;

    const-wide/16 v2, 0x40

    const-wide/16 v4, 0x20

    sget-object v7, Lcom/twitter/library/widget/TweetView;->g:[I

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/widget/al;-><init>(Lcom/twitter/library/widget/TweetView;JJLjava/lang/Runnable;[I)V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->eM:Lcom/twitter/library/widget/al;

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dn:Ljava/util/List;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->eM:Lcom/twitter/library/widget/al;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->Z()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v6, Lcom/twitter/library/widget/ad;

    invoke-direct {v6, p0}, Lcom/twitter/library/widget/ad;-><init>(Lcom/twitter/library/widget/TweetView;)V

    goto :goto_0

    :cond_2
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/provider/Tweet;ZLcom/twitter/library/card/k;)V
    .locals 21

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bO:Lcom/twitter/library/widget/ap;

    if-nez v4, :cond_0

    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v5, "You must call setProvider before calling setTweet"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getContext()Landroid/content/Context;

    move-result-object v9

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->dy:Ljava/util/LinkedHashMap;

    if-eqz v4, :cond_5d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->dy:Ljava/util/LinkedHashMap;

    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5c

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/library/widget/TweetView$InlineAction;

    iget-object v7, v4, Lcom/twitter/library/widget/TweetView$InlineAction;->f:Lcom/twitter/library/view/TweetActionType;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v7, v1}, Lcom/twitter/library/widget/TweetView;->b(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/provider/Tweet;)Lcom/twitter/library/widget/TweetView$InlineAction$State;

    move-result-object v7

    iget-object v4, v4, Lcom/twitter/library/widget/TweetView$InlineAction;->e:Lcom/twitter/library/widget/TweetView$InlineAction$State;

    if-eq v7, v4, :cond_1

    const/4 v4, 0x1

    :goto_0
    sget-object v5, Lcom/twitter/library/view/TweetActionType;->e:Lcom/twitter/library/view/TweetActionType;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v5, v1}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/provider/Tweet;)Z

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/library/widget/TweetView;->dy:Ljava/util/LinkedHashMap;

    sget-object v7, Lcom/twitter/library/view/TweetActionType;->e:Lcom/twitter/library/view/TweetActionType;

    invoke-virtual {v6, v7}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eq v5, v6, :cond_2

    const/4 v4, 0x1

    :cond_2
    :goto_1
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/twitter/library/widget/TweetView;->eD:Z

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/twitter/library/provider/Tweet;->a(Lcom/twitter/library/provider/Tweet;)Z

    move-result v5

    if-eqz v5, :cond_3

    if-eqz v4, :cond_b

    :cond_3
    const/4 v4, 0x1

    :goto_2
    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->dm:Z

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->dm:Z

    if-eqz v4, :cond_55

    move-object/from16 v0, p0

    iget v13, v0, Lcom/twitter/library/widget/TweetView;->dh:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cF:Ljava/lang/CharSequence;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->bW:Landroid/text/StaticLayout;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cG:Ljava/lang/CharSequence;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cg:Landroid/text/StaticLayout;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->eB:Lcom/twitter/library/view/l;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->bS:Landroid/text/StaticLayout;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->bT:Landroid/text/StaticLayout;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->bQ:Landroid/text/StaticLayout;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->bX:Landroid/text/StaticLayout;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cQ:Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cR:Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->bZ:Landroid/text/StaticLayout;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->bY:Landroid/text/StaticLayout;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cP:Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cO:Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cW:Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cX:Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cV:Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->dV:Z

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cN:Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/library/widget/TweetView;->dh:I

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cc:Landroid/text/StaticLayout;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cb:Landroid/text/StaticLayout;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cT:Landroid/graphics/drawable/BitmapDrawable;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->ch:Landroid/text/StaticLayout;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->ck:Landroid/text/StaticLayout;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cm:Landroid/text/StaticLayout;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cU:Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/library/widget/TweetView;->es:I

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->ei:Z

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->ej:Z

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->ed:Z

    new-instance v4, Ljava/util/LinkedHashMap;

    invoke-direct {v4}, Ljava/util/LinkedHashMap;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->dy:Ljava/util/LinkedHashMap;

    new-instance v4, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->do:Ljava/util/List;

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->dn:Ljava/util/List;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->dA:Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->dB:Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->dC:Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->dD:Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cf:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bI:Lcom/twitter/library/widget/TweetMediaImagesView;

    invoke-virtual {v4}, Lcom/twitter/library/widget/TweetMediaImagesView;->a()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bI:Lcom/twitter/library/widget/TweetMediaImagesView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/twitter/library/widget/TweetMediaImagesView;->setVisibility(I)V

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/library/widget/TweetView;->eK:I

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bO:Lcom/twitter/library/widget/ap;

    invoke-interface {v4}, Lcom/twitter/library/widget/ap;->a()J

    move-result-wide v15

    move-object/from16 v0, p1

    move-wide v1, v15

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/provider/Tweet;->a(J)Z

    move-result v10

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/twitter/library/provider/Tweet;->k:Ljava/lang/String;

    if-eqz v4, :cond_4

    if-eqz p2, :cond_c

    const/4 v4, 0x0

    :goto_3
    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Bitmap;Z)V

    :cond_4
    invoke-direct/range {p0 .. p1}, Lcom/twitter/library/widget/TweetView;->c(Lcom/twitter/library/provider/Tweet;)Z

    move-result v4

    if-nez v4, :cond_5

    invoke-direct/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->v()Z

    move-result v4

    if-eqz v4, :cond_d

    :cond_5
    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->Q:I

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/library/widget/TweetView;->dN:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->R:I

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/library/widget/TweetView;->dO:I

    :goto_4
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->l()Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v11

    invoke-direct/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->C()Z

    move-result v12

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->bA:Z

    if-eqz v4, :cond_e

    if-nez v12, :cond_6

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->dZ:Z

    if-nez v4, :cond_6

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->dY:Z

    if-nez v4, :cond_6

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->u()Z

    move-result v4

    if-eqz v4, :cond_e

    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v4}, Lcom/twitter/library/provider/Tweet;->H()Z

    move-result v4

    if-eqz v4, :cond_7

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->ey:Z

    if-eqz v4, :cond_e

    :cond_7
    const/4 v4, 0x1

    :goto_5
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->V()Lcom/twitter/library/card/instance/CardInstanceData;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/card/instance/CardInstanceData;)Z

    move-result v8

    if-eqz v8, :cond_f

    if-eqz p3, :cond_f

    const/4 v8, 0x1

    :goto_6
    if-eqz v8, :cond_10

    const/4 v4, 0x6

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/library/widget/TweetView;->es:I

    :cond_8
    :goto_7
    if-eqz v6, :cond_2b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bI:Lcom/twitter/library/widget/TweetMediaImagesView;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/twitter/library/widget/TweetView;->ah:I

    invoke-virtual {v4, v6}, Lcom/twitter/library/widget/TweetMediaImagesView;->setMediaPlaceholder(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bI:Lcom/twitter/library/widget/TweetMediaImagesView;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Lcom/twitter/library/widget/TweetMediaImagesView;->setBackgroundResource(I)V

    :goto_8
    if-eqz v7, :cond_9

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bI:Lcom/twitter/library/widget/TweetMediaImagesView;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/twitter/library/widget/TweetView;->am:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/twitter/library/widget/TweetMediaImagesView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    :cond_9
    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/twitter/library/provider/Tweet;->h:J

    const-wide/16 v17, 0x0

    cmp-long v4, v6, v17

    if-gtz v4, :cond_2c

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cE:Ljava/lang/String;

    :goto_9
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v12}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/provider/Tweet;Z)Ljava/lang/String;

    move-result-object v12

    const/4 v4, 0x0

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/twitter/library/provider/Tweet;->T:[Lcom/twitter/library/api/Entity;

    if-eqz v6, :cond_2d

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/twitter/library/provider/Tweet;->T:[Lcom/twitter/library/api/Entity;

    array-length v6, v6

    if-lez v6, :cond_2d

    new-instance v4, Landroid/text/SpannableString;

    invoke-direct {v4, v12}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v4}, Landroid/text/SpannableString;->length()I

    move-result v7

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/twitter/library/provider/Tweet;->T:[Lcom/twitter/library/api/Entity;

    array-length v11, v8

    const/4 v6, 0x0

    :goto_a
    if-ge v6, v11, :cond_2d

    aget-object v17, v8, v6

    move-object/from16 v0, v17

    iget v0, v0, Lcom/twitter/library/api/Entity;->start:I

    move/from16 v18, v0

    if-ltz v18, :cond_a

    move-object/from16 v0, v17

    iget v0, v0, Lcom/twitter/library/api/Entity;->end:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-gt v0, v7, :cond_a

    new-instance v18, Lcom/twitter/internal/android/widget/TypefacesSpan;

    const/16 v19, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v0, v9, v1}, Lcom/twitter/internal/android/widget/TypefacesSpan;-><init>(Landroid/content/Context;I)V

    move-object/from16 v0, v17

    iget v0, v0, Lcom/twitter/library/api/Entity;->start:I

    move/from16 v19, v0

    move-object/from16 v0, v17

    iget v0, v0, Lcom/twitter/library/api/Entity;->end:I

    move/from16 v17, v0

    const/16 v20, 0x21

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v17

    move/from16 v3, v20

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    :cond_a
    add-int/lit8 v6, v6, 0x1

    goto :goto_a

    :cond_b
    const/4 v4, 0x0

    goto/16 :goto_2

    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bO:Lcom/twitter/library/widget/ap;

    move-object/from16 v0, p1

    iget v6, v0, Lcom/twitter/library/provider/Tweet;->v:I

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/twitter/library/provider/Tweet;->k:Ljava/lang/String;

    invoke-interface {v4, v6, v7}, Lcom/twitter/library/widget/ap;->a(ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    goto/16 :goto_3

    :cond_d
    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->O:I

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/library/widget/TweetView;->dN:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->P:I

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/library/widget/TweetView;->dO:I

    goto/16 :goto_4

    :cond_e
    const/4 v4, 0x0

    goto/16 :goto_5

    :cond_f
    const/4 v8, 0x0

    goto/16 :goto_6

    :cond_10
    if-eqz v4, :cond_29

    if-eqz v11, :cond_29

    iget v4, v11, Lcom/twitter/library/api/TweetClassicCard;->type:I

    const/4 v8, 0x3

    if-ne v4, v8, :cond_15

    if-eqz v17, :cond_15

    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    invoke-static {v4}, Lcom/twitter/library/api/TwitterStatusCard;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_15

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->dZ:Z

    if-nez v4, :cond_11

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->dY:Z

    if-eqz v4, :cond_15

    :cond_11
    const/4 v4, 0x1

    :goto_b
    iget v8, v11, Lcom/twitter/library/api/TweetClassicCard;->type:I

    const/16 v17, 0x1

    move/from16 v0, v17

    if-ne v8, v0, :cond_17

    move-object/from16 v0, p0

    iget v8, v0, Lcom/twitter/library/widget/TweetView;->br:I

    and-int/lit8 v8, v8, 0x1

    if-eqz v8, :cond_17

    if-eqz v11, :cond_16

    iget v4, v11, Lcom/twitter/library/api/TweetClassicCard;->type:I

    const/4 v8, 0x1

    if-ne v4, v8, :cond_16

    iget-object v4, v11, Lcom/twitter/library/api/TweetClassicCard;->siteUser:Lcom/twitter/library/api/CardUser;

    if-nez v4, :cond_16

    const/4 v4, 0x1

    :goto_c
    if-nez v12, :cond_12

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/twitter/library/widget/TweetView;->dZ:Z

    if-eqz v8, :cond_5b

    if-nez v4, :cond_12

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->ez:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    sget-object v8, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->a:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    if-eq v4, v8, :cond_5b

    :cond_12
    iget v4, v11, Lcom/twitter/library/api/TweetClassicCard;->imageWidth:I

    const/16 v8, 0x64

    if-le v4, v8, :cond_5b

    iget v4, v11, Lcom/twitter/library/api/TweetClassicCard;->imageHeight:I

    const/16 v8, 0x64

    if-le v4, v8, :cond_5b

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/library/widget/TweetView;->es:I

    const/4 v4, 0x1

    if-eqz v12, :cond_13

    iget-object v6, v11, Lcom/twitter/library/api/TweetClassicCard;->siteUser:Lcom/twitter/library/api/CardUser;

    if-eqz v6, :cond_13

    iget-object v6, v11, Lcom/twitter/library/api/TweetClassicCard;->siteUser:Lcom/twitter/library/api/CardUser;

    iget-object v6, v6, Lcom/twitter/library/api/CardUser;->fullName:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/twitter/library/widget/TweetView;->dD:Ljava/lang/String;

    :cond_13
    :goto_d
    move v6, v4

    move v4, v7

    :cond_14
    :goto_e
    move v7, v4

    goto/16 :goto_7

    :cond_15
    const/4 v4, 0x0

    goto :goto_b

    :cond_16
    const/4 v4, 0x0

    goto :goto_c

    :cond_17
    iget v8, v11, Lcom/twitter/library/api/TweetClassicCard;->type:I

    const/16 v17, 0x2

    move/from16 v0, v17

    if-ne v8, v0, :cond_1b

    move-object/from16 v0, p0

    iget v8, v0, Lcom/twitter/library/widget/TweetView;->br:I

    and-int/lit8 v8, v8, 0x4

    if-eqz v8, :cond_1b

    if-eqz v12, :cond_18

    iget v4, v11, Lcom/twitter/library/api/TweetClassicCard;->playerType:I

    const/4 v8, 0x1

    if-eq v4, v8, :cond_1a

    :cond_18
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->dZ:Z

    if-eqz v4, :cond_5a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v4}, Lcom/twitter/library/provider/Tweet;->O()Z

    move-result v4

    if-nez v4, :cond_1a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->ez:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    sget-object v8, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->a:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    if-ne v4, v8, :cond_1a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v4}, Lcom/twitter/library/provider/Tweet;->Q()Z

    move-result v4

    if-eqz v4, :cond_19

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->eF:Z

    if-nez v4, :cond_1a

    :cond_19
    invoke-direct/range {p0 .. p1}, Lcom/twitter/library/widget/TweetView;->i(Lcom/twitter/library/provider/Tweet;)Z

    move-result v4

    if-nez v4, :cond_1a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v4}, Lcom/twitter/library/provider/Tweet;->P()Z

    move-result v4

    if-eqz v4, :cond_5a

    :cond_1a
    const/4 v4, 0x3

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/library/widget/TweetView;->es:I

    const/4 v6, 0x1

    const/4 v4, 0x1

    if-eqz v12, :cond_14

    iget-object v7, v11, Lcom/twitter/library/api/TweetClassicCard;->siteUser:Lcom/twitter/library/api/CardUser;

    if-eqz v7, :cond_14

    iget-object v7, v11, Lcom/twitter/library/api/TweetClassicCard;->siteUser:Lcom/twitter/library/api/CardUser;

    iget-object v7, v7, Lcom/twitter/library/api/CardUser;->fullName:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/twitter/library/widget/TweetView;->dD:Ljava/lang/String;

    goto :goto_e

    :cond_1b
    iget v8, v11, Lcom/twitter/library/api/TweetClassicCard;->type:I

    const/16 v17, 0x3

    move/from16 v0, v17

    if-ne v8, v0, :cond_20

    move-object/from16 v0, p0

    iget v8, v0, Lcom/twitter/library/widget/TweetView;->br:I

    and-int/lit8 v8, v8, 0x2

    if-eqz v8, :cond_1c

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/twitter/library/widget/TweetView;->dY:Z

    if-nez v8, :cond_1d

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->u()Z

    move-result v8

    if-nez v8, :cond_1d

    :cond_1c
    if-eqz v4, :cond_20

    :cond_1d
    iget-object v4, v11, Lcom/twitter/library/api/TweetClassicCard;->imageUrl:Ljava/lang/String;

    if-eqz v4, :cond_20

    const/4 v4, 0x2

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/library/widget/TweetView;->es:I

    iget-object v4, v11, Lcom/twitter/library/api/TweetClassicCard;->siteUser:Lcom/twitter/library/api/CardUser;

    if-eqz v4, :cond_1e

    iget-object v4, v11, Lcom/twitter/library/api/TweetClassicCard;->siteUser:Lcom/twitter/library/api/CardUser;

    iget-object v4, v4, Lcom/twitter/library/api/CardUser;->profileImageUrl:Ljava/lang/String;

    if-eqz v4, :cond_1e

    const/4 v4, 0x1

    :goto_f
    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->ei:Z

    invoke-direct/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->w()Z

    move-result v4

    if-eqz v4, :cond_5a

    if-eqz p2, :cond_1f

    const/4 v4, 0x0

    :goto_10
    const/4 v8, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v8}, Lcom/twitter/library/widget/TweetView;->b(Landroid/graphics/Bitmap;Z)V

    move v4, v7

    goto/16 :goto_e

    :cond_1e
    const/4 v4, 0x0

    goto :goto_f

    :cond_1f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bO:Lcom/twitter/library/widget/ap;

    move-object/from16 v0, p1

    iget v8, v0, Lcom/twitter/library/provider/Tweet;->v:I

    iget-object v11, v11, Lcom/twitter/library/api/TweetClassicCard;->siteUser:Lcom/twitter/library/api/CardUser;

    iget-object v11, v11, Lcom/twitter/library/api/CardUser;->profileImageUrl:Ljava/lang/String;

    invoke-interface {v4, v8, v11}, Lcom/twitter/library/widget/ap;->a(ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    goto :goto_10

    :cond_20
    iget v4, v11, Lcom/twitter/library/api/TweetClassicCard;->type:I

    const/4 v8, 0x4

    if-ne v4, v8, :cond_5a

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->ea:Z

    if-eqz v4, :cond_5a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v4}, Lcom/twitter/library/provider/Tweet;->N()Z

    move-result v4

    if-eqz v4, :cond_24

    invoke-direct/range {p0 .. p1}, Lcom/twitter/library/widget/TweetView;->e(Lcom/twitter/library/provider/Tweet;)Z

    move-result v4

    if-eqz v4, :cond_22

    const/4 v4, 0x4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/library/widget/TweetView;->es:I

    :cond_21
    :goto_11
    const/4 v6, 0x1

    const/4 v4, 0x1

    goto/16 :goto_e

    :cond_22
    invoke-direct/range {p0 .. p1}, Lcom/twitter/library/widget/TweetView;->g(Lcom/twitter/library/provider/Tweet;)Z

    move-result v4

    if-eqz v4, :cond_23

    const/4 v4, 0x3

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/library/widget/TweetView;->es:I

    goto :goto_11

    :cond_23
    invoke-direct/range {p0 .. p1}, Lcom/twitter/library/widget/TweetView;->f(Lcom/twitter/library/provider/Tweet;)Z

    move-result v4

    if-eqz v4, :cond_21

    const/4 v4, 0x3

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/library/widget/TweetView;->es:I

    goto :goto_11

    :cond_24
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v4}, Lcom/twitter/library/provider/Tweet;->M()Z

    move-result v4

    if-eqz v4, :cond_5a

    invoke-direct/range {p0 .. p1}, Lcom/twitter/library/widget/TweetView;->e(Lcom/twitter/library/provider/Tweet;)Z

    move-result v4

    if-eqz v4, :cond_26

    const/4 v4, 0x4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/library/widget/TweetView;->es:I

    :cond_25
    :goto_12
    const/4 v6, 0x1

    move v4, v7

    goto/16 :goto_e

    :cond_26
    invoke-direct/range {p0 .. p1}, Lcom/twitter/library/widget/TweetView;->g(Lcom/twitter/library/provider/Tweet;)Z

    move-result v4

    if-eqz v4, :cond_27

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/library/widget/TweetView;->es:I

    goto :goto_12

    :cond_27
    invoke-direct/range {p0 .. p1}, Lcom/twitter/library/widget/TweetView;->f(Lcom/twitter/library/provider/Tweet;)Z

    move-result v4

    if-eqz v4, :cond_28

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/library/widget/TweetView;->es:I

    goto :goto_12

    :cond_28
    invoke-direct/range {p0 .. p1}, Lcom/twitter/library/widget/TweetView;->h(Lcom/twitter/library/provider/Tweet;)Z

    move-result v4

    if-eqz v4, :cond_25

    const/4 v4, 0x5

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/library/widget/TweetView;->es:I

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->ej:Z

    goto :goto_12

    :cond_29
    if-eqz v4, :cond_8

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->br:I

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_8

    if-nez v12, :cond_2a

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->dZ:Z

    if-eqz v4, :cond_8

    :cond_2a
    const/16 v4, 0x64

    const/16 v8, 0x64

    move-object/from16 v0, p1

    invoke-static {v0, v4, v8}, Lcom/twitter/library/util/s;->c(Lcom/twitter/library/provider/Tweet;II)Z

    move-result v4

    if-eqz v4, :cond_8

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/library/widget/TweetView;->es:I

    const/4 v6, 0x1

    goto/16 :goto_7

    :cond_2b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bI:Lcom/twitter/library/widget/TweetMediaImagesView;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/twitter/library/widget/TweetView;->B:I

    invoke-virtual {v4, v6}, Lcom/twitter/library/widget/TweetMediaImagesView;->setBackgroundColor(I)V

    goto/16 :goto_8

    :cond_2c
    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/twitter/library/provider/Tweet;->h:J

    invoke-static {v5, v6, v7}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cE:Ljava/lang/String;

    goto/16 :goto_9

    :cond_2d
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/twitter/library/widget/TweetView;->bB:Z

    if-eqz v6, :cond_30

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    if-eqz v6, :cond_30

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v6, v7, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    if-eqz v6, :cond_30

    iget-object v6, v7, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_30

    if-nez v4, :cond_59

    new-instance v4, Landroid/text/SpannableString;

    invoke-direct {v4, v12}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    move-object v6, v4

    :goto_13
    invoke-virtual {v6}, Landroid/text/SpannableString;->length()I

    move-result v8

    iget-object v4, v7, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2e
    :goto_14
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2f

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/library/api/UrlEntity;

    iget v11, v4, Lcom/twitter/library/api/UrlEntity;->displayStart:I

    iget v0, v4, Lcom/twitter/library/api/UrlEntity;->displayEnd:I

    move/from16 v17, v0

    if-ltz v11, :cond_2e

    move/from16 v0, v17

    if-le v0, v11, :cond_2e

    move/from16 v0, v17

    if-gt v0, v8, :cond_2e

    new-instance v18, Lcom/twitter/library/widget/t;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/twitter/library/widget/TweetView;->aK:I

    move/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move/from16 v2, v19

    move-object/from16 v3, p1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/library/widget/t;-><init>(Lcom/twitter/library/widget/TweetView;ILcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/UrlEntity;)V

    const/16 v4, 0x21

    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v6, v0, v11, v1, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_14

    :cond_2f
    move-object v4, v6

    :cond_30
    if-eqz v4, :cond_35

    :goto_15
    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cF:Ljava/lang/CharSequence;

    const/16 v4, 0x64

    const/16 v6, 0x64

    move-object/from16 v0, p1

    invoke-static {v0, v4, v6}, Lcom/twitter/library/util/s;->a(Lcom/twitter/library/provider/Tweet;II)Ljava/util/List;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cH:Ljava/util/List;

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v4}, Lcom/twitter/library/widget/TweetView;->a(Landroid/content/Context;Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cG:Ljava/lang/CharSequence;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->j()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_31

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0x20

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_31
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/twitter/library/provider/Tweet;->p:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_32

    const-string/jumbo v6, "@"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/twitter/library/provider/Tweet;->p:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0x20

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_32
    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/twitter/library/widget/TweetView;->setContentDescription(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->ew:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_36

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->ex:I

    if-eqz v4, :cond_36

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->dV:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->ew:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cW:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->ex:I

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cP:Landroid/graphics/drawable/Drawable;

    :cond_33
    :goto_16
    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->es:I

    if-nez v4, :cond_4a

    const/4 v4, 0x1

    :goto_17
    if-eqz v4, :cond_4b

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->q()Z

    move-result v6

    if-eqz v6, :cond_4b

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->S:I

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v4}, Lcom/twitter/library/widget/TweetView;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cQ:Landroid/graphics/drawable/Drawable;

    :cond_34
    :goto_18
    invoke-direct/range {p0 .. p1}, Lcom/twitter/library/widget/TweetView;->m(Lcom/twitter/library/provider/Tweet;)V

    invoke-virtual/range {p0 .. p1}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/provider/Tweet;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide v2, v15

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/provider/Tweet;Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->dz:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/provider/Tweet;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->dA:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Lcom/twitter/library/widget/TweetView;->b(Lcom/twitter/library/provider/Tweet;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->dB:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Lcom/twitter/library/widget/TweetView;->c(Lcom/twitter/library/provider/Tweet;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->dC:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bq:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bq:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/library/widget/TweetView;->cQ:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bq:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/library/widget/TweetView;->cP:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bq:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/library/widget/TweetView;->cO:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bq:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/library/widget/TweetView;->cS:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->dy:Ljava/util/LinkedHashMap;

    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_19
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4e

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/library/widget/TweetView$InlineAction;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/widget/TweetView;->bq:Ljava/util/ArrayList;

    iget-object v4, v4, Lcom/twitter/library/widget/TweetView$InlineAction;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_19

    :cond_35
    move-object v4, v12

    goto/16 :goto_15

    :cond_36
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->F()Z

    move-result v4

    if-eqz v4, :cond_38

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cV:Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cO:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/twitter/library/provider/Tweet;->q:J

    cmp-long v4, v15, v6

    if-nez v4, :cond_37

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->dU:Z

    if-eqz v4, :cond_33

    :cond_37
    move-object/from16 v0, p1

    iget-boolean v4, v0, Lcom/twitter/library/provider/Tweet;->r:Z

    if-eqz v4, :cond_33

    const/16 v6, 0xd

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->k()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v11}, Lcom/twitter/library/widget/TweetView;->a(Landroid/content/res/Resources;ILjava/lang/String;IILjava/lang/String;I)V

    goto/16 :goto_16

    :cond_38
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->E()Z

    move-result v4

    if-eqz v4, :cond_39

    if-nez v10, :cond_39

    sget v4, Lil;->promoted_by:I

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-direct/range {p0 .. p1}, Lcom/twitter/library/widget/TweetView;->j(Lcom/twitter/library/provider/Tweet;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v5, v4, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cV:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->G:I

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cO:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v4, :cond_33

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    iget-object v7, v4, Lcom/twitter/library/api/PromotedContent;->socialContext:Ljava/lang/String;

    if-eqz v7, :cond_33

    const/4 v6, 0x3

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v11}, Lcom/twitter/library/widget/TweetView;->a(Landroid/content/res/Resources;ILjava/lang/String;IILjava/lang/String;I)V

    goto/16 :goto_16

    :cond_39
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->v()Z

    move-result v4

    if-eqz v4, :cond_3b

    if-nez v10, :cond_3b

    invoke-direct/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->u()Z

    move-result v4

    if-eqz v4, :cond_3a

    sget v4, Lil;->promoted_without_advertiser:I

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cV:Ljava/lang/String;

    :goto_1a
    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->F:I

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cO:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v4, :cond_33

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    iget-object v7, v4, Lcom/twitter/library/api/PromotedContent;->socialContext:Ljava/lang/String;

    if-eqz v7, :cond_33

    const/4 v6, 0x3

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v11}, Lcom/twitter/library/widget/TweetView;->a(Landroid/content/res/Resources;ILjava/lang/String;IILjava/lang/String;I)V

    goto/16 :goto_16

    :cond_3a
    sget v4, Lil;->promoted_by:I

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-direct/range {p0 .. p1}, Lcom/twitter/library/widget/TweetView;->j(Lcom/twitter/library/provider/Tweet;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v5, v4, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cV:Ljava/lang/String;

    goto :goto_1a

    :cond_3b
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->I()Z

    move-result v4

    if-eqz v4, :cond_3d

    if-nez v10, :cond_3d

    move-object/from16 v0, p1

    iget-boolean v4, v0, Lcom/twitter/library/provider/Tweet;->r:Z

    if-eqz v4, :cond_3c

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/twitter/library/provider/Tweet;->g:Ljava/lang/String;

    :goto_1b
    sget v6, Lil;->lifeline_alert:I

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v4, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cV:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->H:I

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cO:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_16

    :cond_3c
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/twitter/library/provider/Tweet;->f:Ljava/lang/String;

    goto :goto_1b

    :cond_3d
    move-object/from16 v0, p1

    iget-boolean v4, v0, Lcom/twitter/library/provider/Tweet;->B:Z

    if-eqz v4, :cond_3e

    move-object/from16 v0, p1

    iget v4, v0, Lcom/twitter/library/provider/Tweet;->P:I

    const/16 v6, 0x17

    if-eq v4, v6, :cond_3e

    const/16 v6, 0x16

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v11}, Lcom/twitter/library/widget/TweetView;->a(Landroid/content/res/Resources;ILjava/lang/String;IILjava/lang/String;I)V

    goto/16 :goto_16

    :cond_3e
    move-object/from16 v0, p1

    iget-boolean v4, v0, Lcom/twitter/library/provider/Tweet;->W:Z

    if-eqz v4, :cond_3f

    const/16 v6, 0x14

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v11}, Lcom/twitter/library/widget/TweetView;->a(Landroid/content/res/Resources;ILjava/lang/String;IILjava/lang/String;I)V

    goto/16 :goto_16

    :cond_3f
    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/twitter/library/provider/Tweet;->q:J

    cmp-long v4, v15, v6

    if-nez v4, :cond_40

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->dU:Z

    if-eqz v4, :cond_41

    :cond_40
    move-object/from16 v0, p1

    iget-boolean v4, v0, Lcom/twitter/library/provider/Tweet;->r:Z

    if-eqz v4, :cond_41

    const/16 v6, 0xd

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->k()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v11}, Lcom/twitter/library/widget/TweetView;->a(Landroid/content/res/Resources;ILjava/lang/String;IILjava/lang/String;I)V

    goto/16 :goto_16

    :cond_41
    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/twitter/library/provider/Tweet;->q:J

    cmp-long v4, v15, v6

    if-nez v4, :cond_42

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->D()Z

    move-result v4

    if-eqz v4, :cond_42

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/twitter/library/provider/Tweet;->Q:Ljava/lang/String;

    if-eqz v4, :cond_42

    const/16 v6, 0x18

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/twitter/library/provider/Tweet;->Q:Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v11}, Lcom/twitter/library/widget/TweetView;->a(Landroid/content/res/Resources;ILjava/lang/String;IILjava/lang/String;I)V

    goto/16 :goto_16

    :cond_42
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->dW:Z

    if-eqz v4, :cond_43

    move-object/from16 v0, p1

    iget v4, v0, Lcom/twitter/library/provider/Tweet;->P:I

    if-lez v4, :cond_43

    move-object/from16 v0, p1

    iget v6, v0, Lcom/twitter/library/provider/Tweet;->P:I

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/twitter/library/provider/Tweet;->Q:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v8, v0, Lcom/twitter/library/provider/Tweet;->R:I

    move-object/from16 v0, p1

    iget v9, v0, Lcom/twitter/library/provider/Tweet;->S:I

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/twitter/library/provider/Tweet;->aa:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v11, v0, Lcom/twitter/library/provider/Tweet;->ab:I

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v11}, Lcom/twitter/library/widget/TweetView;->a(Landroid/content/res/Resources;ILjava/lang/String;IILjava/lang/String;I)V

    goto/16 :goto_16

    :cond_43
    invoke-direct/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->v()Z

    move-result v4

    if-eqz v4, :cond_48

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/twitter/library/provider/Tweet;->K:Lcom/twitter/library/api/RecommendedContent;

    invoke-virtual {v4}, Lcom/twitter/library/api/RecommendedContent;->b()I

    move-result v6

    if-nez v6, :cond_45

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/api/RecommendedContent;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cV:Ljava/lang/String;

    :cond_44
    :goto_1c
    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->F:I

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cO:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_16

    :cond_45
    const/4 v6, 0x1

    invoke-virtual {v4}, Lcom/twitter/library/api/RecommendedContent;->b()I

    move-result v7

    if-ne v6, v7, :cond_47

    iget-object v6, v4, Lcom/twitter/library/api/RecommendedContent;->socialProof:Ljava/lang/String;

    if-eqz v6, :cond_46

    const/4 v6, 0x3

    iget-object v7, v4, Lcom/twitter/library/api/RecommendedContent;->socialProof:Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v11}, Lcom/twitter/library/widget/TweetView;->a(Landroid/content/res/Resources;ILjava/lang/String;IILjava/lang/String;I)V

    :cond_46
    sget v4, Lil;->recommended_default:I

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cV:Ljava/lang/String;

    goto :goto_1c

    :cond_47
    const/4 v6, 0x2

    invoke-virtual {v4}, Lcom/twitter/library/api/RecommendedContent;->b()I

    move-result v7

    if-ne v6, v7, :cond_44

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/api/RecommendedContent;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cV:Ljava/lang/String;

    const/16 v6, 0x1d

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v11}, Lcom/twitter/library/widget/TweetView;->a(Landroid/content/res/Resources;ILjava/lang/String;IILjava/lang/String;I)V

    goto :goto_1c

    :cond_48
    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->ev:I

    if-lez v4, :cond_49

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->ev:I

    packed-switch v4, :pswitch_data_0

    const/4 v6, -0x1

    :goto_1d
    if-lez v6, :cond_33

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget v11, v0, Lcom/twitter/library/widget/TweetView;->eu:I

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v11}, Lcom/twitter/library/widget/TweetView;->a(Landroid/content/res/Resources;ILjava/lang/String;IILjava/lang/String;I)V

    goto/16 :goto_16

    :pswitch_0
    const/16 v6, 0x1e

    goto :goto_1d

    :pswitch_1
    const/16 v6, 0x1f

    goto :goto_1d

    :pswitch_2
    const/16 v6, 0x20

    goto :goto_1d

    :cond_49
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cV:Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cO:Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cW:Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cX:Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cP:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_16

    :cond_4a
    const/4 v4, 0x0

    goto/16 :goto_17

    :cond_4b
    if-eqz v4, :cond_4c

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->r()Z

    move-result v6

    if-eqz v6, :cond_4c

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->T:I

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v4}, Lcom/twitter/library/widget/TweetView;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cQ:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_18

    :cond_4c
    if-eqz v4, :cond_4d

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->s()Z

    move-result v6

    if-eqz v6, :cond_4d

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->U:I

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v4}, Lcom/twitter/library/widget/TweetView;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cQ:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_18

    :cond_4d
    if-eqz v4, :cond_34

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->t()Z

    move-result v4

    if-eqz v4, :cond_34

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->U:I

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v4}, Lcom/twitter/library/widget/TweetView;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cQ:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_18

    :cond_4e
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->ek:Z

    if-eqz v4, :cond_4f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->V:I

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v4}, Lcom/twitter/library/widget/TweetView;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cR:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bq:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/library/widget/TweetView;->cR:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4f
    move-object/from16 v0, p1

    iget-boolean v4, v0, Lcom/twitter/library/provider/Tweet;->z:Z

    if-eqz v4, :cond_50

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->bF:Z

    if-eqz v4, :cond_50

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->dR:Z

    if-nez v4, :cond_50

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->bs:I

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v4}, Lcom/twitter/library/widget/TweetView;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cU:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bq:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/library/widget/TweetView;->cU:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_50
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->dp:Z

    if-eqz v4, :cond_56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->dh:I

    int-to-long v6, v4

    const-wide/16 v8, 0x10

    or-long/2addr v6, v8

    long-to-int v4, v6

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/library/widget/TweetView;->dh:I

    :goto_1e
    if-nez v13, :cond_51

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->dh:I

    if-eqz v4, :cond_52

    :cond_51
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->refreshDrawableState()V

    :cond_52
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v4}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/provider/Tweet;)Z

    move-result v4

    if-nez v4, :cond_53

    invoke-direct/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->H()V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->eJ:Z

    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/library/widget/TweetView;->eH:Lcom/twitter/library/card/k;

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->eD:Z

    if-nez v4, :cond_53

    invoke-direct/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->G()V

    :cond_53
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->dV:Z

    if-eqz v4, :cond_57

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->cX:Ljava/lang/String;

    if-eqz v4, :cond_57

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->cX:Ljava/lang/String;

    :goto_1f
    if-nez v4, :cond_54

    const-string/jumbo v4, ""

    :cond_54
    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/twitter/library/provider/Tweet;->h:J

    invoke-static {v5, v6, v7}, Lcom/twitter/library/util/Util;->b(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_58

    const-string/jumbo v5, ""

    :goto_20
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lil;->timeline_tweet_format:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    iget-object v9, v9, Lcom/twitter/library/provider/Tweet;->g:Ljava/lang/String;

    aput-object v9, v7, v8

    const/4 v8, 0x1

    aput-object v12, v7, v8

    const/4 v8, 0x2

    aput-object v5, v7, v8

    const/4 v5, 0x3

    aput-object v4, v7, v5

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/twitter/library/widget/TweetView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->requestLayout()V

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->invalidate()V

    :cond_55
    return-void

    :cond_56
    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->dh:I

    int-to-long v6, v4

    const-wide/16 v8, -0x11

    and-long/2addr v6, v8

    long-to-int v4, v6

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/library/widget/TweetView;->dh:I

    goto/16 :goto_1e

    :cond_57
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->cW:Ljava/lang/String;

    goto :goto_1f

    :cond_58
    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    goto :goto_20

    :cond_59
    move-object v6, v4

    goto/16 :goto_13

    :cond_5a
    move v4, v7

    goto/16 :goto_e

    :cond_5b
    move v4, v6

    goto/16 :goto_d

    :cond_5c
    move v4, v5

    goto/16 :goto_0

    :cond_5d
    move v4, v5

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x12
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Lcom/twitter/library/view/TweetActionType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/twitter/library/widget/TweetView;->dx:Lcom/twitter/library/view/TweetActionType;

    iput-object p3, p0, Lcom/twitter/library/widget/TweetView;->du:Ljava/lang/String;

    iput-object p4, p0, Lcom/twitter/library/widget/TweetView;->dv:Ljava/lang/String;

    iput-object p5, p0, Lcom/twitter/library/widget/TweetView;->dw:Ljava/lang/String;

    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->dt:Z

    sget-object v0, Lcom/twitter/library/view/TweetActionType;->b:Lcom/twitter/library/view/TweetActionType;

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bn:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->ct:Landroid/graphics/drawable/Drawable;

    :cond_0
    :goto_1
    const-string/jumbo v0, "blue"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->aY:I

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->cj:I

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->aV:I

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->cl:I

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->aX:I

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->cn:I

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bm:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->cq:Landroid/graphics/drawable/Drawable;

    :goto_2
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ct:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ct:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->ct:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->ct:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v0, v1, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/twitter/library/view/TweetActionType;->d:Lcom/twitter/library/view/TweetActionType;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bo:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->ct:Landroid/graphics/drawable/Drawable;

    goto :goto_1

    :cond_4
    iget v0, p0, Lcom/twitter/library/widget/TweetView;->aZ:I

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->cj:I

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->aU:I

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->cl:I

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->aW:I

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->cn:I

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bl:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->cq:Landroid/graphics/drawable/Drawable;

    goto :goto_2
.end method

.method public a(Ljava/lang/String;)V
    .locals 10

    const/16 v9, 0x21

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    iget-object v0, v0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    iget-object v0, v0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v0, v0, Lcom/twitter/library/api/TweetEntities;->hashtags:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v2, Landroid/text/SpannableString;

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->cF:Ljava/lang/CharSequence;

    invoke-direct {v2, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Landroid/text/SpannableString;->length()I

    move-result v3

    sget v4, Lid;->light_blue:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    iget-object v0, v0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v0, v0, Lcom/twitter/library/api/TweetEntities;->hashtags:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/HashtagEntity;

    iget-object v6, v0, Lcom/twitter/library/api/HashtagEntity;->text:Ljava/lang/String;

    invoke-virtual {v6, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget v6, v0, Lcom/twitter/library/api/HashtagEntity;->start:I

    iget v0, v0, Lcom/twitter/library/api/HashtagEntity;->end:I

    if-ltz v6, :cond_0

    if-le v0, v6, :cond_0

    if-gt v0, v3, :cond_0

    new-instance v7, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v7, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    add-int/lit8 v8, v6, 0x1

    invoke-virtual {v2, v7, v6, v8, v9}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    new-instance v7, Lcom/twitter/library/widget/s;

    iget v8, p0, Lcom/twitter/library/widget/TweetView;->aK:I

    invoke-direct {v7, p0, v8}, Lcom/twitter/library/widget/s;-><init>(Lcom/twitter/library/widget/TweetView;I)V

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v2, v7, v6, v0, v9}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    :cond_1
    iput-object v2, p0, Lcom/twitter/library/widget/TweetView;->cF:Ljava/lang/CharSequence;

    :cond_2
    return-void
.end method

.method public a(Z)V
    .locals 8

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bJ:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->el:Z

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->el:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->em:Lcom/twitter/library/widget/al;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/library/widget/al;

    const-wide/32 v2, 0x40000

    const-wide/32 v4, 0x20000

    new-instance v6, Lcom/twitter/library/widget/ag;

    invoke-direct {v6, p0}, Lcom/twitter/library/widget/ag;-><init>(Lcom/twitter/library/widget/TweetView;)V

    sget-object v7, Lcom/twitter/library/widget/TweetView;->i:[I

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/widget/al;-><init>(Lcom/twitter/library/widget/TweetView;JJLjava/lang/Runnable;[I)V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->em:Lcom/twitter/library/widget/al;

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->em:Lcom/twitter/library/widget/al;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/al;->a(Landroid/graphics/Rect;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dn:Ljava/util/List;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->em:Lcom/twitter/library/widget/al;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->em:Lcom/twitter/library/widget/al;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dn:Ljava/util/List;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->em:Lcom/twitter/library/widget/al;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public a(ZLjava/lang/String;)V
    .locals 8

    iput-object p2, p0, Lcom/twitter/library/widget/TweetView;->eo:Ljava/lang/String;

    if-eqz p1, :cond_2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->en:Z

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->en:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->eq:Lcom/twitter/library/widget/al;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/library/widget/al;

    const-wide/32 v2, 0x100000

    const-wide/32 v4, 0x80000

    new-instance v6, Lcom/twitter/library/widget/ak;

    invoke-direct {v6, p0}, Lcom/twitter/library/widget/ak;-><init>(Lcom/twitter/library/widget/TweetView;)V

    sget-object v7, Lcom/twitter/library/widget/TweetView;->j:[I

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/widget/al;-><init>(Lcom/twitter/library/widget/TweetView;JJLjava/lang/Runnable;[I)V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->eq:Lcom/twitter/library/widget/al;

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->eq:Lcom/twitter/library/widget/al;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/al;->a(Landroid/graphics/Rect;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dn:Ljava/util/List;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->eq:Lcom/twitter/library/widget/al;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->eq:Lcom/twitter/library/widget/al;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dn:Ljava/util/List;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->eq:Lcom/twitter/library/widget/al;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public a(ZZ)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/widget/TweetView;->dS:Z

    iput-boolean p2, p0, Lcom/twitter/library/widget/TweetView;->dT:Z

    return-void
.end method

.method public a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/card/instance/CardInstanceData;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->bA:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->dZ:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->dY:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->H()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->ey:Z

    if-eqz v0, :cond_2

    :cond_1
    invoke-static {}, Lcom/twitter/library/featureswitch/a;->an()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/twitter/library/card/instance/CardInstanceData;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/provider/Tweet;)Z
    .locals 5

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->b()J

    move-result-wide v1

    invoke-virtual {p2}, Lcom/twitter/library/provider/Tweet;->b()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->V()Lcom/twitter/library/card/instance/CardInstanceData;

    move-result-object v1

    invoke-virtual {p2}, Lcom/twitter/library/provider/Tweet;->V()Lcom/twitter/library/card/instance/CardInstanceData;

    move-result-object v2

    if-nez v1, :cond_2

    if-nez v2, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    iget-object v0, v1, Lcom/twitter/library/card/instance/CardInstanceData;->forwardCardTypeURL:Ljava/lang/String;

    iget-object v1, v2, Lcom/twitter/library/card/instance/CardInstanceData;->forwardCardTypeURL:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Ljava/util/HashMap;Z)Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bI:Lcom/twitter/library/widget/TweetMediaImagesView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/widget/TweetMediaImagesView;->a(Ljava/util/Map;)Z

    move-result v0

    if-eqz p2, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->b()V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->refreshDrawableState()V

    return v0
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/TweetView;->invalidate(Landroid/graphics/Rect;)V

    :cond_0
    return-void
.end method

.method public b(Landroid/graphics/Bitmap;Z)V
    .locals 4

    const/4 v1, 0x0

    if-eqz p1, :cond_2

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v0, v2, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->aE:I

    iget v3, p0, Lcom/twitter/library/widget/TweetView;->aE:I

    invoke-virtual {v0, v1, v1, v2, v3}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    :goto_0
    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->cT:Landroid/graphics/drawable/BitmapDrawable;

    if-eq v2, v0, :cond_0

    const/4 v1, 0x1

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->cT:Landroid/graphics/drawable/BitmapDrawable;

    :cond_0
    move v0, v1

    if-eqz p2, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->invalidate()V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b(Lcom/twitter/library/provider/Tweet;)V
    .locals 8

    new-instance v6, Lcom/twitter/library/widget/u;

    invoke-direct {v6, p0}, Lcom/twitter/library/widget/u;-><init>(Lcom/twitter/library/widget/TweetView;)V

    new-instance v0, Lcom/twitter/library/widget/al;

    const-wide/32 v2, 0x10000

    const-wide/32 v4, 0x8000

    sget-object v7, Lcom/twitter/library/widget/TweetView;->h:[I

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/widget/al;-><init>(Lcom/twitter/library/widget/TweetView;JJLjava/lang/Runnable;[I)V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->dg:Lcom/twitter/library/widget/al;

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dg:Lcom/twitter/library/widget/al;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->df:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/al;->a(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dn:Ljava/util/List;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->dg:Lcom/twitter/library/widget/al;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method c()V
    .locals 8

    iget-object v6, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->b:Lcom/twitter/library/widget/aa;

    iget-wide v1, v6, Lcom/twitter/library/provider/Tweet;->o:J

    iget-wide v3, v6, Lcom/twitter/library/provider/Tweet;->n:J

    iget-object v5, v6, Lcom/twitter/library/provider/Tweet;->p:Ljava/lang/String;

    iget-object v6, v6, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    move-object v7, p0

    invoke-interface/range {v0 .. v7}, Lcom/twitter/library/widget/aa;->a(JJLjava/lang/String;Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/widget/TweetView;)V

    return-void
.end method

.method d()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->l()Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->b:Lcom/twitter/library/widget/aa;

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    invoke-interface {v1, v2, v0, p0}, Lcom/twitter/library/widget/aa;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/TweetClassicCard;Lcom/twitter/library/widget/TweetView;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bI:Lcom/twitter/library/widget/TweetMediaImagesView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetMediaImagesView;->getLastTouchedMedia()Lcom/twitter/library/api/MediaEntity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->b:Lcom/twitter/library/widget/aa;

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    invoke-interface {v1, v2, v0, p0}, Lcom/twitter/library/widget/aa;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/MediaEntity;Lcom/twitter/library/widget/TweetView;)V

    goto :goto_0
.end method

.method protected drawableStateChanged()V
    .locals 4

    invoke-super {p0}, Landroid/view/ViewGroup;->drawableStateChanged()V

    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->x()V

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getDrawableState()[I

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bq:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method e()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->b:Lcom/twitter/library/widget/aa;

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->T()Lcom/twitter/library/api/MediaEntity;

    move-result-object v2

    iget-wide v2, v2, Lcom/twitter/library/api/MediaEntity;->id:J

    invoke-interface {v1, v0, v2, v3, p0}, Lcom/twitter/library/widget/aa;->a(Lcom/twitter/library/provider/Tweet;JLcom/twitter/library/widget/TweetView;)V

    return-void
.end method

.method f()V
    .locals 5

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->b:Lcom/twitter/library/widget/aa;

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface {v1, v2, v0, v3, v4}, Lcom/twitter/library/widget/aa;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/card/Card;I)V

    return-void
.end method

.method g()V
    .locals 5

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->b:Lcom/twitter/library/widget/aa;

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-interface {v1, v2, v0, v3, v4}, Lcom/twitter/library/widget/aa;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/card/Card;I)V

    return-void
.end method

.method getFavoriteLabel()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dB:Ljava/lang/String;

    return-object v0
.end method

.method protected getForwardCard()Lcom/twitter/library/card/Card;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->eH:Lcom/twitter/library/card/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->eH:Lcom/twitter/library/card/k;

    invoke-interface {v0}, Lcom/twitter/library/card/k;->p()Lcom/twitter/library/card/Card;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFriendshipCache()Lcom/twitter/library/util/FriendshipCache;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bP:Lcom/twitter/library/util/FriendshipCache;

    return-object v0
.end method

.method public getPivotIntent()Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->er:Landroid/content/Intent;

    return-object v0
.end method

.method getReplyLabel()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dC:Ljava/lang/String;

    return-object v0
.end method

.method getRetweetLabel()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dA:Ljava/lang/String;

    return-object v0
.end method

.method public getScribeItem()Lcom/twitter/library/scribe/ScribeItem;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->eC:Lcom/twitter/library/scribe/ScribeItem;

    return-object v0
.end method

.method public getSocialContextCount()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->eu:I

    return v0
.end method

.method public getSocialContextType()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->ev:I

    return v0
.end method

.method public getSummaryImageKey()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->ei:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->l()Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/api/TweetClassicCard;->siteUser:Lcom/twitter/library/api/CardUser;

    invoke-virtual {v0}, Lcom/twitter/library/api/CardUser;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSummaryUserId()J
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->ei:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->l()Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/api/TweetClassicCard;->siteUser:Lcom/twitter/library/api/CardUser;

    iget-wide v0, v0, Lcom/twitter/library/api/CardUser;->userId:J

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getTweet()Lcom/twitter/library/provider/Tweet;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    return-object v0
.end method

.method public getUserId()J
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    iget-wide v0, v0, Lcom/twitter/library/provider/Tweet;->n:J

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getUserImageKey()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->y()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method h()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    iget-object v0, v0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v0, v0, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    iget-object v1, v1, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v1, v1, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/UrlEntity;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->b:Lcom/twitter/library/widget/aa;

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    invoke-interface {v1, v2, v0}, Lcom/twitter/library/widget/aa;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/UrlEntity;)V

    return-void
.end method

.method i()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->b:Lcom/twitter/library/widget/aa;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    invoke-interface {v0, v1, p0}, Lcom/twitter/library/widget/aa;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/widget/TweetView;)V

    return-void
.end method

.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->cS:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->invalidate()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method j()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->b:Lcom/twitter/library/widget/aa;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    invoke-interface {v0, v1, p0}, Lcom/twitter/library/widget/aa;->b(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/widget/TweetView;)V

    return-void
.end method

.method k()V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->dt:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->dt:Z

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->requestLayout()V

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->invalidate()V

    :cond_0
    return-void
.end method

.method l()V
    .locals 1

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/TweetView;->a(I)V

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->invalidate()V

    return-void
.end method

.method public m()V
    .locals 7

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->o()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    if-eqz v0, :cond_4

    invoke-virtual {v3}, Lcom/twitter/library/provider/Tweet;->q()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v3}, Lcom/twitter/library/provider/Tweet;->r()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    move v0, v1

    :goto_0
    iget-boolean v4, p0, Lcom/twitter/library/widget/TweetView;->dZ:Z

    if-nez v4, :cond_1

    iget-boolean v4, p0, Lcom/twitter/library/widget/TweetView;->dY:Z

    if-nez v4, :cond_1

    invoke-virtual {v3}, Lcom/twitter/library/provider/Tweet;->u()Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    move v2, v1

    :cond_2
    if-eqz v0, :cond_6

    if-eqz v2, :cond_6

    iget-boolean v0, v3, Lcom/twitter/library/provider/Tweet;->l:Z

    if-eqz v0, :cond_5

    const/4 v4, 0x0

    :goto_1
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iget v2, p0, Lcom/twitter/library/widget/TweetView;->an:I

    iget v3, p0, Lcom/twitter/library/widget/TweetView;->ao:I

    iget-object v5, p0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->centerX()I

    move-result v5

    iget-object v6, p0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->centerY()I

    move-result v6

    invoke-static/range {v0 .. v6}, Ljw;->a(Landroid/content/Context;Landroid/view/ViewGroup;IILjava/lang/Runnable;II)V

    :cond_3
    :goto_2
    return-void

    :cond_4
    move v0, v2

    goto :goto_0

    :cond_5
    iget-object v4, p0, Lcom/twitter/library/widget/TweetView;->bH:Ljava/lang/Runnable;

    goto :goto_1

    :cond_6
    iget-boolean v0, v3, Lcom/twitter/library/provider/Tweet;->l:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dy:Ljava/util/LinkedHashMap;

    sget-object v2, Lcom/twitter/library/view/TweetActionType;->b:Lcom/twitter/library/view/TweetActionType;

    invoke-virtual {v0, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/TweetView$InlineAction;

    invoke-direct {p0, v0, v1}, Lcom/twitter/library/widget/TweetView;->b(Lcom/twitter/library/widget/TweetView$InlineAction;Z)V

    goto :goto_2
.end method

.method public n()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    iget-boolean v0, v0, Lcom/twitter/library/provider/Tweet;->l:Z

    return v0
.end method

.method public o()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dy:Ljava/util/LinkedHashMap;

    sget-object v1, Lcom/twitter/library/view/TweetActionType;->b:Lcom/twitter/library/view/TweetActionType;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected onCreateDrawableState(I)[I
    .locals 4

    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->F()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v1, p1

    add-int/lit8 v1, v1, 0x2

    invoke-super {p0, v1}, Landroid/view/ViewGroup;->onCreateDrawableState(I)[I

    move-result-object v1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/al;

    invoke-virtual {v0}, Lcom/twitter/library/widget/al;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/al;->a([I)V

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/twitter/library/widget/TweetView;->es:I

    invoke-direct {p0, v0}, Lcom/twitter/library/widget/TweetView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bI:Lcom/twitter/library/widget/TweetMediaImagesView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetMediaImagesView;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/twitter/library/widget/TweetView;->e:[I

    invoke-static {v1, v0}, Lcom/twitter/library/widget/TweetView;->mergeDrawableStates([I[I)[I

    :cond_2
    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->dp:Z

    if-eqz v0, :cond_3

    sget-object v0, Lcom/twitter/library/widget/TweetView;->a:[I

    invoke-static {v1, v0}, Lcom/twitter/library/widget/TweetView;->mergeDrawableStates([I[I)[I

    :cond_3
    return-object v1
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->B()V

    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->H()V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bI:Lcom/twitter/library/widget/TweetMediaImagesView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetMediaImagesView;->b()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 31

    invoke-super/range {p0 .. p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v8, Lcom/twitter/library/widget/TweetView;->m:Landroid/text/TextPaint;

    invoke-virtual {v8}, Landroid/text/TextPaint;->getColor()I

    move-result v17

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getWidth()I

    move-result v18

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getScrollX()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getPaddingTop()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getScrollY()I

    move-result v5

    add-int v7, v5, v4

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getPaddingLeft()I

    move-result v4

    add-int v13, v3, v4

    add-int v3, v3, v18

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getPaddingRight()I

    move-result v4

    sub-int v9, v3, v4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/twitter/library/widget/TweetView;->cL:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/twitter/library/widget/TweetView;->cy:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->es:I

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/twitter/library/widget/TweetView;->b(I)Z

    move-result v21

    if-eqz v21, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->eg:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->cv:I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4, v8}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Canvas;Landroid/graphics/RectF;ILandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->dD:Ljava/lang/String;

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->cB:I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v8}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Canvas;ILandroid/text/TextPaint;)V

    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v3, v1}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/provider/Tweet;Landroid/graphics/Canvas;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->cN:Landroid/graphics/drawable/Drawable;

    if-nez v3, :cond_1a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->bu:Landroid/graphics/drawable/Drawable;

    :goto_1
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v22

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/twitter/library/widget/TweetView;->el:Z

    if-eqz v5, :cond_3

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bJ:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v5, :cond_1b

    int-to-float v5, v13

    int-to-float v6, v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    add-int/2addr v4, v13

    :goto_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->bJ:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    :cond_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->cW:Ljava/lang/String;

    if-eqz v5, :cond_1c

    const/4 v5, 0x1

    :goto_3
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/library/widget/TweetView;->cP:Landroid/graphics/drawable/Drawable;

    if-eqz v6, :cond_1d

    if-eqz v5, :cond_1d

    const/4 v6, 0x1

    :goto_4
    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/twitter/library/widget/TweetView;->dV:Z

    if-eqz v10, :cond_5

    if-nez v6, :cond_4

    if-eqz v5, :cond_5

    :cond_4
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    if-eqz v6, :cond_1f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->cP:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v5

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v6

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v4, :cond_1e

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->aa:I

    sub-int v4, v9, v4

    sub-int v4, v4, v22

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->bZ:Landroid/text/StaticLayout;

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getWidth()I

    move-result v5

    move-object/from16 v0, p0

    iget v10, v0, Lcom/twitter/library/widget/TweetView;->D:I

    add-int/2addr v5, v10

    neg-int v5, v5

    :goto_5
    int-to-float v4, v4

    int-to-float v10, v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, Landroid/graphics/Canvas;->translate(FF)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->cP:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bZ:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v4

    sub-int v4, v6, v4

    div-int/lit8 v4, v4, 0x2

    :goto_6
    int-to-float v5, v5

    int-to-float v4, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, Landroid/graphics/Canvas;->translate(FF)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bZ:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v20

    invoke-direct {v0, v4, v1, v2, v8}, Lcom/twitter/library/widget/TweetView;->a(Landroid/text/StaticLayout;Landroid/graphics/Canvas;ILandroid/graphics/Paint;)V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->af:I

    add-int/2addr v4, v6

    add-int/2addr v7, v4

    const/4 v4, 0x0

    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/library/widget/TweetView;->cS:Landroid/graphics/drawable/Drawable;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/library/widget/TweetView;->bS:Landroid/text/StaticLayout;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/library/widget/TweetView;->bQ:Landroid/text/StaticLayout;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/twitter/library/widget/TweetView;->cI:F

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/library/widget/TweetView;->bX:Landroid/text/StaticLayout;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/library/widget/TweetView;->cQ:Landroid/graphics/drawable/Drawable;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/library/widget/TweetView;->cR:Landroid/graphics/drawable/Drawable;

    move-object/from16 v28, v0

    if-eqz v24, :cond_22

    invoke-virtual/range {v24 .. v24}, Landroid/text/StaticLayout;->getHeight()I

    move-result v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/twitter/library/widget/TweetView;->bV:I

    sub-int/2addr v5, v6

    int-to-float v6, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->ez:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    sget-object v10, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->d:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    if-ne v5, v10, :cond_21

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->ac:I

    int-to-float v5, v5

    const/high16 v10, 0x40000000    # 2.0f

    mul-float/2addr v10, v6

    sub-float/2addr v5, v10

    float-to-int v5, v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v5, v7

    move v14, v5

    move v15, v6

    :goto_7
    if-eqz v25, :cond_24

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->ez:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    sget-object v6, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->d:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    if-ne v5, v6, :cond_23

    invoke-static {v15}, Ljava/lang/Math;->round(F)I

    move-result v5

    add-int/2addr v5, v14

    :goto_8
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v6, :cond_2c

    move-object/from16 v0, p0

    iget v6, v0, Lcom/twitter/library/widget/TweetView;->aa:I

    add-int v6, v6, v22

    sub-int v12, v9, v6

    if-eqz v25, :cond_25

    invoke-virtual/range {v25 .. v25}, Landroid/text/StaticLayout;->getEllipsizedWidth()I

    move-result v6

    move-object/from16 v0, p0

    iget v9, v0, Lcom/twitter/library/widget/TweetView;->W:I

    add-int/2addr v6, v9

    sub-int v10, v12, v6

    move v6, v10

    :goto_9
    if-eqz v24, :cond_27

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/library/widget/TweetView;->ez:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    sget-object v11, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->d:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    if-ne v9, v11, :cond_26

    const/4 v9, -0x1

    if-le v10, v9, :cond_26

    move v11, v10

    :goto_a
    if-eqz v26, :cond_2b

    add-int v9, v13, v4

    if-eqz v27, :cond_29

    invoke-virtual/range {v26 .. v26}, Landroid/text/StaticLayout;->getWidth()I

    move-result v4

    move-object/from16 v0, p0

    iget v6, v0, Lcom/twitter/library/widget/TweetView;->D:I

    add-int/2addr v6, v4

    if-eqz v28, :cond_28

    invoke-virtual/range {v27 .. v27}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/twitter/library/widget/TweetView;->D:I

    move/from16 v29, v0

    add-int v4, v4, v29

    :goto_b
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    int-to-float v0, v12

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/twitter/library/widget/TweetView;->Z:I

    move/from16 v30, v0

    add-int v30, v30, v7

    move/from16 v0, v30

    int-to-float v0, v0

    move/from16 v30, v0

    move-object/from16 v0, p1

    move/from16 v1, v29

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    if-eqz v23, :cond_6

    move-object/from16 v0, v23

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_6
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->ad:I

    sub-int v3, v22, v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/library/widget/TweetView;->by:Landroid/graphics/drawable/Drawable;

    move-object/from16 v22, v0

    if-eqz v22, :cond_7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/twitter/library/widget/TweetView;->dS:Z

    move/from16 v22, v0

    if-eqz v22, :cond_7

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    int-to-float v0, v3

    move/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/library/widget/TweetView;->by:Landroid/graphics/drawable/Drawable;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/library/widget/TweetView;->bz:Landroid/graphics/drawable/Drawable;

    move-object/from16 v22, v0

    if-eqz v22, :cond_8

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/twitter/library/widget/TweetView;->dT:Z

    move/from16 v22, v0

    if-eqz v22, :cond_8

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/twitter/library/widget/TweetView;->Z:I

    move/from16 v22, v0

    add-int v22, v22, v7

    move-object/from16 v0, p0

    iget v0, v0, Lcom/twitter/library/widget/TweetView;->ac:I

    move/from16 v23, v0

    add-int v22, v22, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/twitter/library/widget/TweetView;->ae:I

    move/from16 v23, v0

    add-int v22, v22, v23

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v3, v1}, Landroid/graphics/Canvas;->translate(FF)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->bz:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    :cond_8
    if-eqz v24, :cond_9

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    int-to-float v3, v11

    move-object/from16 v0, p0

    iget v0, v0, Lcom/twitter/library/widget/TweetView;->bV:I

    move/from16 v22, v0

    sub-int v14, v14, v22

    int-to-float v14, v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, Landroid/graphics/Canvas;->translate(FF)V

    move/from16 v0, v16

    invoke-virtual {v8, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->bD:Lcom/twitter/internal/android/widget/ax;

    iget-object v3, v3, Lcom/twitter/internal/android/widget/ax;->c:Landroid/graphics/Typeface;

    invoke-virtual {v8, v3}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move/from16 v0, v19

    invoke-virtual {v8, v0}, Landroid/text/TextPaint;->setColor(I)V

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->bD:Lcom/twitter/internal/android/widget/ax;

    iget-object v3, v3, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    invoke-virtual {v8, v3}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    if-eqz v25, :cond_a

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    int-to-float v3, v10

    move-object/from16 v0, p0

    iget v10, v0, Lcom/twitter/library/widget/TweetView;->bR:I

    sub-int/2addr v5, v10

    int-to-float v5, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, Landroid/graphics/Canvas;->translate(FF)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->cD:F

    invoke-virtual {v8, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    if-eqz v21, :cond_34

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->cx:I

    invoke-virtual {v8, v3}, Landroid/text/TextPaint;->setColor(I)V

    :goto_c
    move-object/from16 v0, v25

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    :cond_a
    if-eqz v26, :cond_d

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    int-to-float v3, v9

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->bR:I

    sub-int v5, v7, v5

    int-to-float v5, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, Landroid/graphics/Canvas;->translate(FF)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->cD:F

    invoke-virtual {v8, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->cx:I

    invoke-virtual {v8, v3}, Landroid/text/TextPaint;->setColor(I)V

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    if-eqz v27, :cond_b

    int-to-float v3, v6

    invoke-virtual/range {v26 .. v26}, Landroid/text/StaticLayout;->getHeight()I

    move-result v5

    invoke-virtual/range {v27 .. v27}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, Landroid/graphics/Canvas;->translate(FF)V

    move-object/from16 v0, v27

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_b
    if-eqz v28, :cond_c

    if-eqz v27, :cond_35

    int-to-float v3, v4

    invoke-virtual/range {v27 .. v27}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    invoke-virtual/range {v28 .. v28}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    :goto_d
    move-object/from16 v0, v28

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_c
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->ez:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    sget-object v4, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->d:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    if-ne v3, v4, :cond_36

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->ac:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->ai:I

    add-int/2addr v3, v4

    add-int/2addr v3, v7

    move v13, v12

    :goto_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bT:Landroid/text/StaticLayout;

    if-eqz v4, :cond_e

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    int-to-float v4, v13

    int-to-float v5, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bD:Lcom/twitter/internal/android/widget/ax;

    iget-object v4, v4, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    invoke-virtual {v8, v4}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->cD:F

    invoke-virtual {v8, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->cy:I

    invoke-virtual {v8, v4}, Landroid/text/TextPaint;->setColor(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bT:Landroid/text/StaticLayout;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bT:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bW:Landroid/text/StaticLayout;

    if-eqz v4, :cond_f

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    int-to-float v4, v13

    int-to-float v5, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    move/from16 v0, v16

    invoke-virtual {v8, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    move/from16 v0, v19

    invoke-virtual {v8, v0}, Landroid/text/TextPaint;->setColor(I)V

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->eA:I

    iput v4, v8, Landroid/text/TextPaint;->linkColor:I

    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bW:Landroid/text/StaticLayout;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_f
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bW:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    :cond_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->cg:Landroid/text/StaticLayout;

    if-eqz v4, :cond_49

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->ak:I

    add-int/2addr v3, v4

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    int-to-float v4, v13

    int-to-float v5, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->bw:F

    invoke-virtual {v8, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->cC:I

    invoke-virtual {v8, v4}, Landroid/text/TextPaint;->setColor(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->cg:Landroid/text/StaticLayout;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->cg:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->al:I

    add-int/2addr v4, v5

    add-int/2addr v3, v4

    move v9, v3

    :goto_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->bD:Lcom/twitter/internal/android/widget/ax;

    iget-object v3, v3, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    invoke-virtual {v8, v3}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->cD:F

    invoke-virtual {v8, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->cy:I

    invoke-virtual {v8, v3}, Landroid/text/TextPaint;->setColor(I)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->W:I

    sub-int v3, v12, v3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v13, v3}, Lcom/twitter/library/widget/TweetView;->b(Landroid/graphics/Canvas;II)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->es:I

    const/4 v4, 0x2

    if-eq v3, v4, :cond_10

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->es:I

    const/4 v4, 0x5

    if-ne v3, v4, :cond_3c

    :cond_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->da:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->cu:I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4, v8}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Canvas;Landroid/graphics/RectF;ILandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v3, :cond_39

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->cZ:Landroid/graphics/RectF;

    iget v4, v3, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    if-eqz v3, :cond_37

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_37

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->aA:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->az:I

    add-int/2addr v3, v5

    :goto_11
    int-to-float v3, v3

    add-float v5, v4, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->cZ:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->aE:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    sub-float v4, v3, v5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->cc:Landroid/text/StaticLayout;

    if-eqz v3, :cond_38

    invoke-direct/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->w()Z

    move-result v3

    if-eqz v3, :cond_38

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->cc:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getWidth()I

    move-result v3

    move-object/from16 v0, p0

    iget v6, v0, Lcom/twitter/library/widget/TweetView;->D:I

    add-int/2addr v3, v6

    neg-int v3, v3

    move v10, v3

    move v3, v4

    move v4, v5

    :goto_12
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->cZ:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->cM:I

    invoke-virtual {v8, v4}, Landroid/text/TextPaint;->setColor(I)V

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->bv:F

    invoke-virtual {v8, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->cb:Landroid/text/StaticLayout;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->cD:F

    invoke-virtual {v8, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->cc:Landroid/text/StaticLayout;

    if-eqz v4, :cond_12

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->cb:Landroid/text/StaticLayout;

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getHeight()I

    move-result v5

    int-to-float v5, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-direct/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->w()Z

    move-result v4

    if-eqz v4, :cond_11

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->af:I

    int-to-float v4, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->cT:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v3, :cond_3b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->cT:Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    :cond_11
    :goto_13
    int-to-float v3, v10

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->aE:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->af:I

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->cc:Landroid/text/StaticLayout;

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getHeight()I

    move-result v5

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->cy:I

    invoke-virtual {v8, v3}, Landroid/text/TextPaint;->setColor(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->bD:Lcom/twitter/internal/android/widget/ax;

    iget-object v3, v3, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    invoke-virtual {v8, v3}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->cc:Landroid/text/StaticLayout;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    :cond_12
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    :cond_13
    :goto_14
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v8, v13}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;I)V

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Landroid/text/TextPaint;->setColor(I)V

    invoke-direct/range {p0 .. p1}, Lcom/twitter/library/widget/TweetView;->c(Landroid/graphics/Canvas;)V

    invoke-direct/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->v()Z

    move-result v10

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->cO:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_3e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->cV:Ljava/lang/String;

    if-eqz v3, :cond_3e

    const/4 v3, 0x1

    :goto_15
    if-eqz v3, :cond_19

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->eK:I

    add-int/2addr v3, v9

    invoke-direct/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->u()Z

    move-result v4

    if-eqz v4, :cond_14

    if-nez v24, :cond_15

    :cond_14
    if-eqz v10, :cond_17

    if-eqz v24, :cond_17

    :cond_15
    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->ag:I

    mul-int/lit8 v4, v4, 0x2

    add-int v9, v3, v4

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->cA:I

    invoke-virtual {v8, v3}, Landroid/text/TextPaint;->setColor(I)V

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v3, :cond_3f

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getLeft()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getPaddingLeft()I

    move-result v4

    add-int/2addr v3, v4

    :goto_16
    int-to-float v4, v11

    int-to-float v5, v9

    int-to-float v6, v3

    int-to-float v7, v9

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->ag:I

    mul-int/lit8 v3, v3, 0x2

    add-int v5, v9, v3

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    int-to-float v3, v11

    int-to-float v4, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    move/from16 v0, v16

    invoke-virtual {v8, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->bD:Lcom/twitter/internal/android/widget/ax;

    iget-object v3, v3, Lcom/twitter/internal/android/widget/ax;->c:Landroid/graphics/Typeface;

    invoke-virtual {v8, v3}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move/from16 v0, v19

    invoke-virtual {v8, v0}, Landroid/text/TextPaint;->setColor(I)V

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->dy:Ljava/util/LinkedHashMap;

    sget-object v4, Lcom/twitter/library/view/TweetActionType;->e:Lcom/twitter/library/view/TweetActionType;

    invoke-virtual {v3, v4}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/widget/TweetView$InlineAction;

    if-eqz v3, :cond_16

    if-eqz v10, :cond_40

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->ag:I

    :goto_17
    move-object/from16 v0, p0

    iget v6, v0, Lcom/twitter/library/widget/TweetView;->cK:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/library/widget/TweetView;->cJ:I

    add-int/2addr v4, v5

    iget-object v3, v3, Lcom/twitter/library/widget/TweetView$InlineAction;->c:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7, v4, v3}, Lcom/twitter/library/widget/TweetView;->a(IIII)V

    :cond_16
    invoke-virtual/range {v24 .. v24}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    add-int/2addr v3, v5

    :cond_17
    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->ag:I

    add-int v5, v3, v4

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->cO:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v6

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v3, :cond_42

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->W:I

    sub-int v3, v12, v3

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v4

    sub-int v4, v3, v4

    if-eqz v10, :cond_41

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->bY:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getWidth()I

    move-result v3

    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/library/widget/TweetView;->D:I

    add-int/2addr v3, v7

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v7

    add-int/2addr v3, v7

    neg-int v3, v3

    :goto_18
    int-to-float v4, v4

    int-to-float v7, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7}, Landroid/graphics/Canvas;->translate(FF)V

    if-nez v10, :cond_18

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->cO:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_18
    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->Z:I

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/library/widget/TweetView;->bY:Landroid/text/StaticLayout;

    invoke-virtual {v9}, Landroid/text/StaticLayout;->getHeight()I

    move-result v9

    sub-int/2addr v7, v9

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v4, v7

    int-to-float v4, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->bY:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v20

    invoke-direct {v0, v3, v1, v2, v8}, Lcom/twitter/library/widget/TweetView;->a(Landroid/text/StaticLayout;Landroid/graphics/Canvas;ILandroid/graphics/Paint;)V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v5

    :cond_19
    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->W:I

    sub-int v3, v12, v3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v13, v3}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Canvas;II)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->dy:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_19
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_46

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/widget/TweetView$InlineAction;

    iget-object v5, v3, Lcom/twitter/library/widget/TweetView$InlineAction;->f:Lcom/twitter/library/view/TweetActionType;

    sget-object v6, Lcom/twitter/library/view/TweetActionType;->b:Lcom/twitter/library/view/TweetActionType;

    if-ne v5, v6, :cond_44

    iget-object v5, v3, Lcom/twitter/library/widget/TweetView$InlineAction;->e:Lcom/twitter/library/widget/TweetView$InlineAction$State;

    sget-object v6, Lcom/twitter/library/widget/TweetView$InlineAction$State;->a:Lcom/twitter/library/widget/TweetView$InlineAction$State;

    if-ne v5, v6, :cond_44

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->aS:I

    invoke-virtual {v8, v5}, Landroid/text/TextPaint;->setColor(I)V

    :goto_1a
    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/twitter/library/widget/TweetView$InlineAction;->a(Landroid/graphics/Canvas;)V

    goto :goto_19

    :cond_1a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->cN:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_1

    :cond_1b
    sub-int v5, v9, v4

    int-to-float v5, v5

    int-to-float v6, v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    add-int v4, v4, v18

    sub-int/2addr v4, v9

    goto/16 :goto_2

    :cond_1c
    const/4 v5, 0x0

    goto/16 :goto_3

    :cond_1d
    const/4 v6, 0x0

    goto/16 :goto_4

    :cond_1e
    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->W:I

    add-int/2addr v4, v13

    add-int v4, v4, v22

    sub-int/2addr v4, v5

    move-object/from16 v0, p0

    iget v10, v0, Lcom/twitter/library/widget/TweetView;->aa:I

    add-int/2addr v5, v10

    goto/16 :goto_5

    :cond_1f
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v4, :cond_20

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->aa:I

    sub-int v4, v9, v4

    sub-int v4, v4, v22

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->bZ:Landroid/text/StaticLayout;

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getWidth()I

    move-result v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/twitter/library/widget/TweetView;->D:I

    add-int/2addr v5, v6

    sub-int/2addr v4, v5

    :goto_1b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->bZ:Landroid/text/StaticLayout;

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getHeight()I

    move-result v5

    move v6, v5

    move v5, v4

    move v4, v7

    goto/16 :goto_6

    :cond_20
    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->W:I

    add-int/2addr v4, v13

    add-int v4, v4, v22

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->aa:I

    add-int/2addr v4, v5

    goto :goto_1b

    :cond_21
    move v14, v7

    move v15, v6

    goto/16 :goto_7

    :cond_22
    const/4 v5, -0x1

    move v14, v5

    move/from16 v15, v16

    goto/16 :goto_7

    :cond_23
    move v5, v7

    goto/16 :goto_8

    :cond_24
    const/4 v5, -0x1

    goto/16 :goto_8

    :cond_25
    const/4 v10, -0x1

    move v6, v12

    goto/16 :goto_9

    :cond_26
    invoke-virtual/range {v24 .. v24}, Landroid/text/StaticLayout;->getWidth()I

    move-result v9

    sub-int/2addr v6, v9

    move-object/from16 v0, p0

    iget v9, v0, Lcom/twitter/library/widget/TweetView;->D:I

    sub-int v11, v6, v9

    goto/16 :goto_a

    :cond_27
    const/4 v11, -0x1

    goto/16 :goto_a

    :cond_28
    const/4 v4, -0x1

    goto/16 :goto_b

    :cond_29
    const/4 v6, -0x1

    if-eqz v28, :cond_2a

    invoke-virtual/range {v26 .. v26}, Landroid/text/StaticLayout;->getWidth()I

    move-result v4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/twitter/library/widget/TweetView;->D:I

    move/from16 v29, v0

    add-int v4, v4, v29

    goto/16 :goto_b

    :cond_2a
    const/4 v4, -0x1

    goto/16 :goto_b

    :cond_2b
    const/4 v4, -0x1

    const/4 v9, -0x1

    const/4 v6, -0x1

    goto/16 :goto_b

    :cond_2c
    move-object/from16 v0, p0

    iget v6, v0, Lcom/twitter/library/widget/TweetView;->W:I

    add-int v12, v13, v6

    if-eqz v24, :cond_2d

    invoke-virtual/range {v24 .. v24}, Landroid/text/StaticLayout;->getWidth()I

    move-result v6

    move-object/from16 v0, p0

    iget v10, v0, Lcom/twitter/library/widget/TweetView;->aa:I

    add-int v10, v10, v22

    add-int v11, v12, v10

    move v13, v11

    :goto_1c
    if-eqz v25, :cond_30

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/library/widget/TweetView;->ez:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    sget-object v29, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->d:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    move-object/from16 v0, v29

    if-ne v10, v0, :cond_2e

    const/4 v10, -0x1

    if-le v11, v10, :cond_2e

    move v10, v11

    :goto_1d
    if-eqz v26, :cond_32

    invoke-virtual/range {v26 .. v26}, Landroid/text/StaticLayout;->getWidth()I

    move-result v6

    sub-int v6, v9, v6

    sub-int v9, v6, v4

    if-eqz v27, :cond_31

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->D:I

    invoke-virtual/range {v27 .. v27}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    add-int/2addr v4, v6

    neg-int v6, v4

    :goto_1e
    if-eqz v28, :cond_33

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->D:I

    invoke-virtual/range {v28 .. v28}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Landroid/graphics/Rect;->width()I

    move-result v29

    add-int v4, v4, v29

    neg-int v4, v4

    goto/16 :goto_b

    :cond_2d
    const/4 v6, 0x0

    const/4 v11, -0x1

    move v13, v12

    goto :goto_1c

    :cond_2e
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/library/widget/TweetView;->ez:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    sget-object v29, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->d:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    move-object/from16 v0, v29

    if-ne v10, v0, :cond_2f

    move v10, v13

    goto :goto_1d

    :cond_2f
    add-int/2addr v6, v13

    move-object/from16 v0, p0

    iget v10, v0, Lcom/twitter/library/widget/TweetView;->D:I

    add-int/2addr v10, v6

    goto :goto_1d

    :cond_30
    const/4 v10, -0x1

    goto :goto_1d

    :cond_31
    const/4 v6, -0x1

    goto :goto_1e

    :cond_32
    const/4 v9, -0x1

    const/4 v6, -0x1

    goto :goto_1e

    :cond_33
    const/4 v4, -0x1

    goto/16 :goto_b

    :cond_34
    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->cw:I

    invoke-virtual {v8, v3}, Landroid/text/TextPaint;->setColor(I)V

    goto/16 :goto_c

    :cond_35
    int-to-float v3, v4

    invoke-virtual/range {v26 .. v26}, Landroid/text/StaticLayout;->getHeight()I

    move-result v4

    invoke-virtual/range {v28 .. v28}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    goto/16 :goto_d

    :cond_36
    int-to-float v3, v7

    add-float/2addr v3, v15

    float-to-int v3, v3

    goto/16 :goto_e

    :cond_37
    const/4 v3, 0x0

    goto/16 :goto_11

    :cond_38
    const/4 v3, 0x0

    move v10, v3

    move v3, v4

    move v4, v5

    goto/16 :goto_12

    :cond_39
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->cZ:Landroid/graphics/RectF;

    iget v5, v3, Landroid/graphics/RectF;->left:F

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->cc:Landroid/text/StaticLayout;

    if-eqz v3, :cond_3a

    invoke-direct/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->w()Z

    move-result v3

    if-eqz v3, :cond_3a

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->aE:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/twitter/library/widget/TweetView;->D:I

    add-int/2addr v3, v6

    move v10, v3

    move v3, v4

    move v4, v5

    goto/16 :goto_12

    :cond_3a
    const/4 v3, 0x0

    move v10, v3

    move v3, v4

    move v4, v5

    goto/16 :goto_12

    :cond_3b
    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->B:I

    invoke-virtual {v8, v3}, Landroid/text/TextPaint;->setColor(I)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->aE:I

    int-to-float v6, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->aE:I

    int-to-float v7, v3

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_13

    :cond_3c
    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->es:I

    const/4 v4, 0x4

    if-ne v3, v4, :cond_3d

    invoke-direct/range {p0 .. p1}, Lcom/twitter/library/widget/TweetView;->b(Landroid/graphics/Canvas;)V

    goto/16 :goto_14

    :cond_3d
    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->es:I

    const/4 v4, 0x6

    if-ne v3, v4, :cond_13

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->t()Z

    move-result v3

    if-nez v3, :cond_13

    invoke-virtual/range {p0 .. p1}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Canvas;)V

    goto/16 :goto_14

    :cond_3e
    const/4 v3, 0x0

    goto/16 :goto_15

    :cond_3f
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getRight()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    goto/16 :goto_16

    :cond_40
    const/4 v4, 0x0

    goto/16 :goto_17

    :cond_41
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->bY:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getWidth()I

    move-result v3

    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/library/widget/TweetView;->D:I

    add-int/2addr v3, v7

    neg-int v3, v3

    goto/16 :goto_18

    :cond_42
    if-eqz v10, :cond_43

    const/4 v3, 0x0

    move v4, v13

    goto/16 :goto_18

    :cond_43
    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->D:I

    add-int/2addr v3, v4

    move v4, v13

    goto/16 :goto_18

    :cond_44
    iget-object v5, v3, Lcom/twitter/library/widget/TweetView$InlineAction;->f:Lcom/twitter/library/view/TweetActionType;

    sget-object v6, Lcom/twitter/library/view/TweetActionType;->c:Lcom/twitter/library/view/TweetActionType;

    if-ne v5, v6, :cond_45

    iget-object v5, v3, Lcom/twitter/library/widget/TweetView$InlineAction;->e:Lcom/twitter/library/widget/TweetView$InlineAction$State;

    sget-object v6, Lcom/twitter/library/widget/TweetView$InlineAction$State;->a:Lcom/twitter/library/widget/TweetView$InlineAction$State;

    if-ne v5, v6, :cond_45

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->aT:I

    invoke-virtual {v8, v5}, Landroid/text/TextPaint;->setColor(I)V

    goto/16 :goto_1a

    :cond_45
    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->cy:I

    invoke-virtual {v8, v5}, Landroid/text/TextPaint;->setColor(I)V

    goto/16 :goto_1a

    :cond_46
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/twitter/library/widget/TweetView;->en:Z

    if-eqz v3, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->eq:Lcom/twitter/library/widget/al;

    invoke-virtual {v3}, Lcom/twitter/library/widget/al;->c()Z

    move-result v3

    if-eqz v3, :cond_47

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->aK:I

    invoke-virtual {v8, v3}, Landroid/text/TextPaint;->setColor(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->eq:Lcom/twitter/library/widget/al;

    invoke-virtual {v3}, Lcom/twitter/library/widget/al;->a()Landroid/graphics/Rect;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_47
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getPaddingBottom()I

    move-result v9

    int-to-float v3, v13

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getHeight()I

    move-result v4

    mul-int/lit8 v5, v9, 0x2

    sub-int/2addr v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->ep:Landroid/text/StaticLayout;

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getHeight()I

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v4, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->bL:I

    invoke-virtual {v8, v3}, Landroid/text/TextPaint;->setColor(I)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->bK:I

    int-to-float v3, v3

    invoke-virtual {v8, v3}, Landroid/text/TextPaint;->setStrokeWidth(F)V

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v3, :cond_48

    neg-int v3, v13

    int-to-float v4, v3

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->cK:I

    sub-int/2addr v3, v13

    int-to-float v6, v3

    const/4 v7, 0x0

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :goto_1f
    move/from16 v0, v16

    invoke-virtual {v8, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/TweetView;->bM:I

    invoke-virtual {v8, v3}, Landroid/text/TextPaint;->setColor(I)V

    const/4 v3, 0x0

    int-to-float v4, v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/widget/TweetView;->ep:Landroid/text/StaticLayout;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    :cond_48
    const/4 v4, 0x0

    const/4 v5, 0x0

    move/from16 v0, v18

    int-to-float v6, v0

    const/4 v7, 0x0

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_1f

    :catch_0
    move-exception v4

    goto/16 :goto_f

    :cond_49
    move v9, v3

    goto/16 :goto_10
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    const/high16 v5, 0x40000000    # 2.0f

    const/4 v1, 0x0

    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->dm:Z

    if-eqz v0, :cond_3

    :cond_0
    iput-boolean v1, p0, Lcom/twitter/library/widget/TweetView;->dm:Z

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->bI:Lcom/twitter/library/widget/TweetMediaImagesView;

    if-eqz v2, :cond_8

    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->A()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->bA:Z

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->dY:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->dZ:Z

    if-nez v0, :cond_1

    invoke-virtual {v2}, Lcom/twitter/library/provider/Tweet;->u()Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_1
    invoke-virtual {v3}, Lcom/twitter/library/widget/TweetMediaImagesView;->c()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ez:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    sget-object v4, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->c:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    if-eq v0, v4, :cond_2

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ez:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    sget-object v4, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->d:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    if-ne v0, v4, :cond_5

    :cond_2
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/twitter/library/widget/TweetMediaImagesView;->b(Z)V

    invoke-virtual {v2}, Lcom/twitter/library/provider/Tweet;->l()Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v3, v0}, Lcom/twitter/library/widget/TweetMediaImagesView;->setClassicCard(Lcom/twitter/library/api/TweetClassicCard;)V

    invoke-virtual {v3, v1}, Lcom/twitter/library/widget/TweetMediaImagesView;->setVisibility(I)V

    :goto_1
    invoke-virtual {v3}, Lcom/twitter/library/widget/TweetMediaImagesView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v3, v1, v2}, Lcom/twitter/library/widget/TweetMediaImagesView;->measure(II)V

    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget v2, v0, Landroid/graphics/Rect;->top:I

    iget v4, v0, Landroid/graphics/Rect;->right:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v3, v1, v2, v4, v0}, Lcom/twitter/library/widget/TweetMediaImagesView;->layout(IIII)V

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->eD:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bO:Lcom/twitter/library/widget/ap;

    invoke-virtual {v3, v0}, Lcom/twitter/library/widget/TweetMediaImagesView;->a(Lcom/twitter/library/widget/ap;)Z

    :cond_3
    :goto_2
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getForwardCard()Lcom/twitter/library/card/Card;

    move-result-object v0

    iget v1, p0, Lcom/twitter/library/widget/TweetView;->es:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_4

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/twitter/library/card/Card;->A()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->eG:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->eG:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/twitter/library/widget/TweetView;->eG:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lcom/twitter/library/widget/TweetView;->eG:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    :cond_4
    return-void

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->cH:Ljava/util/List;

    invoke-virtual {v3, v0}, Lcom/twitter/library/widget/TweetMediaImagesView;->setMediaEntities(Ljava/util/List;)V

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v1, 0x8

    :cond_7
    invoke-virtual {v3, v1}, Lcom/twitter/library/widget/TweetMediaImagesView;->setVisibility(I)V

    goto :goto_1

    :cond_8
    invoke-virtual {v3}, Lcom/twitter/library/widget/TweetMediaImagesView;->b()V

    goto :goto_2
.end method

.method protected onMeasure(II)V
    .locals 48

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    move-object/from16 v32, v0

    if-nez v32, :cond_1

    invoke-super/range {p0 .. p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v22

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v33

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v30

    const/high16 v4, 0x40000000    # 2.0f

    if-ne v5, v4, :cond_19

    move/from16 v21, v22

    :goto_1
    sget-object v6, Lcom/twitter/library/widget/TweetView;->m:Landroid/text/TextPaint;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getPaddingLeft()I

    move-result v34

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getPaddingRight()I

    move-result v35

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getPaddingTop()I

    move-result v36

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getPaddingBottom()I

    move-result v37

    sub-int v4, v21, v34

    sub-int v28, v4, v35

    move-object/from16 v0, p0

    iget v0, v0, Lcom/twitter/library/widget/TweetView;->ab:I

    move/from16 v38, v0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->W:I

    add-int v4, v4, v38

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->aa:I

    add-int v24, v4, v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->ez:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    sget-object v5, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->d:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    if-ne v4, v5, :cond_1a

    const/4 v4, 0x0

    move/from16 v0, v28

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    move/from16 v23, v4

    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/library/widget/TweetView;->dy:Ljava/util/LinkedHashMap;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->cD:F

    invoke-virtual {v6, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bD:Lcom/twitter/internal/android/widget/ax;

    iget-object v4, v4, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    invoke-virtual {v6, v4}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-direct {v0, v6, v1}, Lcom/twitter/library/widget/TweetView;->a(Landroid/text/TextPaint;I)V

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-direct {v0, v6, v1}, Lcom/twitter/library/widget/TweetView;->d(Landroid/text/TextPaint;I)I

    move-result v40

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-direct {v0, v6, v1}, Lcom/twitter/library/widget/TweetView;->b(Landroid/text/TextPaint;I)V

    invoke-direct/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getStatsTextHeight()I

    move-result v41

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->ez:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    sget-object v5, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->d:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    if-ne v4, v5, :cond_1b

    const/4 v4, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v5, :cond_1c

    sub-int v5, v21, v35

    sub-int v4, v5, v4

    move/from16 v25, v4

    :goto_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bX:Landroid/text/StaticLayout;

    if-nez v4, :cond_5a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->cE:Ljava/lang/String;

    if-eqz v4, :cond_5a

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->cD:F

    invoke-virtual {v6, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bD:Lcom/twitter/internal/android/widget/ax;

    iget-object v4, v4, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    invoke-virtual {v6, v4}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    new-instance v4, Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->cE:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/widget/TweetView;->cE:Ljava/lang/String;

    invoke-static {v7, v6}, Lhl;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v7

    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct/range {v4 .. v11}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->bX:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bX:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getWidth()I

    move-result v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->D:I

    add-int/2addr v4, v5

    sub-int v4, v23, v4

    :goto_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->cQ:Landroid/graphics/drawable/Drawable;

    if-eqz v5, :cond_2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->cQ:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/library/widget/TweetView;->D:I

    add-int/2addr v5, v7

    sub-int/2addr v4, v5

    :cond_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->cR:Landroid/graphics/drawable/Drawable;

    if-eqz v5, :cond_59

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->cR:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/library/widget/TweetView;->D:I

    add-int/2addr v5, v7

    sub-int v18, v4, v5

    :goto_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bS:Landroid/text/StaticLayout;

    if-nez v4, :cond_3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->cI:F

    invoke-virtual {v6, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bD:Lcom/twitter/internal/android/widget/ax;

    iget-object v4, v4, Lcom/twitter/internal/android/widget/ax;->c:Landroid/graphics/Typeface;

    invoke-virtual {v6, v4}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    invoke-virtual/range {v32 .. v32}, Lcom/twitter/library/provider/Tweet;->j()Ljava/lang/String;

    move-result-object v8

    const/4 v4, 0x0

    if-eqz v8, :cond_58

    new-instance v7, Landroid/text/StaticLayout;

    const/4 v9, 0x0

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    invoke-static {v8, v6}, Lhl;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v12

    sget-object v13, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/twitter/library/widget/TweetView;->A:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->C:I

    int-to-float v15, v4

    const/16 v16, 0x0

    sget-object v17, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object v11, v6

    invoke-direct/range {v7 .. v18}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/twitter/library/widget/TweetView;->bS:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bS:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getWidth()I

    move-result v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->D:I

    add-int/2addr v4, v5

    sub-int v18, v18, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v6}, Lcom/twitter/library/widget/TweetView;->a(Ljava/lang/String;Landroid/graphics/Paint;)I

    move-result v4

    move/from16 v5, v18

    :goto_7
    move-object/from16 v0, v32

    iget-object v7, v0, Lcom/twitter/library/provider/Tweet;->p:Ljava/lang/String;

    if-nez v7, :cond_1d

    const-string/jumbo v10, ""

    :goto_8
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/library/widget/TweetView;->bQ:Landroid/text/StaticLayout;

    if-nez v9, :cond_57

    if-lez v5, :cond_57

    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/library/widget/TweetView;->cD:F

    invoke-virtual {v6, v7}, Landroid/text/TextPaint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/widget/TweetView;->bD:Lcom/twitter/internal/android/widget/ax;

    iget-object v7, v7, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    invoke-virtual {v6, v7}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    invoke-static {v10, v6}, Lhl;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v14

    new-instance v9, Landroid/text/StaticLayout;

    const/4 v11, 0x0

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v12

    sget-object v15, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v16, 0x3f800000    # 1.0f

    const/16 v17, 0x0

    const/16 v18, 0x0

    sget-object v19, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    if-le v14, v5, :cond_1e

    move/from16 v20, v5

    :goto_9
    move-object v13, v6

    invoke-direct/range {v9 .. v20}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/twitter/library/widget/TweetView;->bQ:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v6}, Lcom/twitter/library/widget/TweetView;->a(Ljava/lang/String;Landroid/graphics/Paint;)I

    move-result v5

    :goto_a
    if-lt v4, v5, :cond_1f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bS:Landroid/text/StaticLayout;

    if-eqz v4, :cond_1f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->cI:F

    invoke-virtual {v6, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bD:Lcom/twitter/internal/android/widget/ax;

    iget-object v4, v4, Lcom/twitter/internal/android/widget/ax;->c:Landroid/graphics/Typeface;

    invoke-virtual {v6, v4}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bS:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v8, v6}, Lcom/twitter/library/widget/TweetView;->a(Landroid/text/Layout;Ljava/lang/String;Landroid/graphics/Paint;)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/library/widget/TweetView;->bV:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bQ:Landroid/text/StaticLayout;

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bQ:Landroid/text/StaticLayout;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/text/StaticLayout;->getLineBaseline(I)I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->bS:Landroid/text/StaticLayout;

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Landroid/text/StaticLayout;->getLineBaseline(I)I

    move-result v5

    sub-int/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->bV:I

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/library/widget/TweetView;->bR:I

    :cond_3
    :goto_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bT:Landroid/text/StaticLayout;

    if-nez v4, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v4}, Lcom/twitter/library/provider/Tweet;->w()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bU:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v7}, Lcom/twitter/library/provider/Tweet;->w()Ljava/util/ArrayList;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/library/widget/TweetView;->bO:Lcom/twitter/library/widget/ap;

    invoke-interface {v8}, Lcom/twitter/library/widget/ap;->a()J

    move-result-wide v8

    invoke-virtual {v4, v5, v7, v8, v9}, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;->a(Landroid/content/res/Resources;Ljava/util/ArrayList;J)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->cD:F

    invoke-virtual {v6, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bD:Lcom/twitter/internal/android/widget/ax;

    iget-object v4, v4, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    invoke-virtual {v6, v4}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    invoke-static {v5, v6}, Lhl;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v7

    move/from16 v0, v23

    invoke-static {v7, v0}, Ljava/lang/Math;->min(II)I

    move-result v9

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bU:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/twitter/library/widget/TweetView;->C:I

    invoke-virtual/range {v4 .. v9}, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;III)Landroid/text/StaticLayout;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->bT:Landroid/text/StaticLayout;

    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bW:Landroid/text/StaticLayout;

    if-nez v4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->cF:Ljava/lang/CharSequence;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->cI:F

    invoke-virtual {v6, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bD:Lcom/twitter/internal/android/widget/ax;

    iget-object v4, v4, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    invoke-virtual {v6, v4}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    new-instance v4, Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->cF:Ljava/lang/CharSequence;

    sget-boolean v7, Lcom/twitter/library/util/Util;->c:Z

    if-eqz v7, :cond_20

    invoke-virtual/range {v32 .. v32}, Lcom/twitter/library/provider/Tweet;->C()Z

    move-result v7

    if-eqz v7, :cond_20

    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    :goto_c
    move-object/from16 v0, p0

    iget v9, v0, Lcom/twitter/library/widget/TweetView;->A:F

    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/library/widget/TweetView;->C:I

    int-to-float v10, v7

    const/4 v11, 0x0

    move/from16 v7, v23

    invoke-direct/range {v4 .. v11}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->bW:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->cF:Ljava/lang/CharSequence;

    instance-of v4, v4, Landroid/text/Spanned;

    if-eqz v4, :cond_5

    new-instance v4, Lcom/twitter/library/view/l;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->bW:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5}, Lcom/twitter/library/view/l;-><init>(Landroid/view/View;Landroid/text/Layout;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->eB:Lcom/twitter/library/view/l;

    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->cg:Landroid/text/StaticLayout;

    if-nez v4, :cond_6

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->dr:Z

    if-nez v4, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->cG:Ljava/lang/CharSequence;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->bw:F

    invoke-virtual {v6, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bD:Lcom/twitter/internal/android/widget/ax;

    iget-object v4, v4, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    invoke-virtual {v6, v4}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    new-instance v7, Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/library/widget/TweetView;->cG:Ljava/lang/CharSequence;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->cG:Ljava/lang/CharSequence;

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v10

    sget-boolean v4, Lcom/twitter/library/util/Util;->c:Z

    if-eqz v4, :cond_21

    invoke-virtual/range {v32 .. v32}, Lcom/twitter/library/provider/Tweet;->C()Z

    move-result v4

    if-eqz v4, :cond_21

    sget-object v13, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    :goto_d
    move-object/from16 v0, p0

    iget v14, v0, Lcom/twitter/library/widget/TweetView;->A:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->C:I

    int-to-float v15, v4

    const/16 v16, 0x0

    sget-object v17, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object v11, v6

    move/from16 v12, v23

    move/from16 v18, v23

    invoke-direct/range {v7 .. v18}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/twitter/library/widget/TweetView;->cg:Landroid/text/StaticLayout;

    :cond_6
    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->cD:F

    invoke-virtual {v6, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/library/widget/TweetView;->cW:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/library/widget/TweetView;->cV:Ljava/lang/String;

    move-object/from16 v42, v0

    if-eqz v8, :cond_22

    const/4 v4, 0x1

    :goto_e
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->cP:Landroid/graphics/drawable/Drawable;

    if-eqz v5, :cond_23

    if-eqz v4, :cond_23

    const/4 v5, 0x1

    :goto_f
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/widget/TweetView;->cO:Landroid/graphics/drawable/Drawable;

    if-eqz v7, :cond_24

    if-eqz v42, :cond_24

    const/4 v7, 0x1

    move/from16 v31, v7

    :goto_10
    if-eqz v5, :cond_25

    new-instance v7, Landroid/text/StaticLayout;

    const/4 v9, 0x0

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    invoke-static {v8, v6}, Lhl;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v12

    sget-object v13, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/twitter/library/widget/TweetView;->A:F

    move-object/from16 v0, p0

    iget v11, v0, Lcom/twitter/library/widget/TweetView;->C:I

    int-to-float v15, v11

    const/16 v16, 0x0

    sget-object v17, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object v11, v6

    move/from16 v18, v23

    invoke-direct/range {v7 .. v18}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/twitter/library/widget/TweetView;->bZ:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/widget/TweetView;->cP:Landroid/graphics/drawable/Drawable;

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/library/widget/TweetView;->cP:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/library/widget/TweetView;->cP:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v11}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v11

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :cond_7
    :goto_11
    if-nez v41, :cond_8

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/twitter/library/widget/TweetView;->bF:Z

    if-eqz v7, :cond_8

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/twitter/library/widget/TweetView;->dR:Z

    if-nez v7, :cond_8

    if-nez v31, :cond_8

    move-object/from16 v0, v32

    iget-boolean v7, v0, Lcom/twitter/library/provider/Tweet;->z:Z

    if-eqz v7, :cond_8

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/widget/TweetView;->ch:Landroid/text/StaticLayout;

    if-nez v7, :cond_8

    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/library/widget/TweetView;->cD:F

    invoke-virtual {v6, v7}, Landroid/text/TextPaint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/widget/TweetView;->bD:Lcom/twitter/internal/android/widget/ax;

    iget-object v7, v7, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    invoke-virtual {v6, v7}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-object/from16 v0, v32

    iget-object v7, v0, Lcom/twitter/library/provider/Tweet;->E:Lcom/twitter/library/api/geo/TwitterPlace;

    iget-object v8, v7, Lcom/twitter/library/api/geo/TwitterPlace;->fullName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/widget/TweetView;->cU:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    sub-int v7, v23, v7

    move-object/from16 v0, p0

    iget v9, v0, Lcom/twitter/library/widget/TweetView;->D:I

    sub-int v18, v7, v9

    new-instance v7, Landroid/text/StaticLayout;

    const/4 v9, 0x0

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    invoke-static {v8, v6}, Lhl;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v12

    sget-object v13, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v14, 0x3f800000    # 1.0f

    const/4 v15, 0x0

    const/16 v16, 0x0

    sget-object v17, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object v11, v6

    invoke-direct/range {v7 .. v18}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/twitter/library/widget/TweetView;->ch:Landroid/text/StaticLayout;

    :cond_8
    const/4 v7, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/library/widget/TweetView;->bS:Landroid/text/StaticLayout;

    if-eqz v8, :cond_9

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/library/widget/TweetView;->ez:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    sget-object v10, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->d:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    if-eq v8, v10, :cond_9

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/library/widget/TweetView;->bS:Landroid/text/StaticLayout;

    invoke-virtual {v8}, Landroid/text/StaticLayout;->getHeight()I

    move-result v8

    move-object/from16 v0, p0

    iget v10, v0, Lcom/twitter/library/widget/TweetView;->bV:I

    sub-int/2addr v8, v10

    add-int/2addr v7, v8

    :cond_9
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/library/widget/TweetView;->bT:Landroid/text/StaticLayout;

    if-eqz v8, :cond_a

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/library/widget/TweetView;->bT:Landroid/text/StaticLayout;

    invoke-virtual {v8}, Landroid/text/StaticLayout;->getHeight()I

    move-result v8

    add-int/2addr v7, v8

    :cond_a
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/library/widget/TweetView;->bW:Landroid/text/StaticLayout;

    if-eqz v8, :cond_b

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/library/widget/TweetView;->bW:Landroid/text/StaticLayout;

    invoke-virtual {v8}, Landroid/text/StaticLayout;->getHeight()I

    move-result v8

    add-int/2addr v7, v8

    :cond_b
    move-object/from16 v0, p0

    iget v8, v0, Lcom/twitter/library/widget/TweetView;->ac:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/library/widget/TweetView;->ez:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    sget-object v11, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->d:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    if-ne v10, v11, :cond_c

    move-object/from16 v0, p0

    iget v10, v0, Lcom/twitter/library/widget/TweetView;->ai:I

    add-int/2addr v10, v8

    add-int/2addr v7, v10

    :cond_c
    if-eqz v5, :cond_26

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->af:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->cP:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    add-int/2addr v5, v4

    add-int v4, v8, v5

    move/from16 v19, v4

    move/from16 v20, v5

    :goto_12
    add-int v7, v7, v20

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->ef:Landroid/graphics/Rect;

    if-nez v4, :cond_d

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->ef:Landroid/graphics/Rect;

    :cond_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->cg:Landroid/text/StaticLayout;

    if-eqz v4, :cond_28

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->cg:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getWidth()I

    move-result v4

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v5, :cond_27

    sub-int v4, v25, v4

    move v5, v4

    move/from16 v4, v25

    :goto_13
    add-int v8, v36, v7

    move-object/from16 v0, p0

    iget v9, v0, Lcom/twitter/library/widget/TweetView;->ak:I

    add-int/2addr v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/library/widget/TweetView;->cg:Landroid/text/StaticLayout;

    invoke-virtual {v9}, Landroid/text/StaticLayout;->getHeight()I

    move-result v9

    add-int/2addr v9, v8

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/library/widget/TweetView;->ef:Landroid/graphics/Rect;

    invoke-virtual {v10, v5, v8, v4, v9}, Landroid/graphics/Rect;->set(IIII)V

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->ak:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->ef:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->al:I

    add-int/2addr v4, v5

    add-int/2addr v4, v7

    move/from16 v26, v4

    :goto_14
    const/4 v4, -0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->ch:Landroid/text/StaticLayout;

    if-eqz v5, :cond_55

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->cU:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->ch:Landroid/text/StaticLayout;

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getHeight()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    move/from16 v27, v4

    :goto_15
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/library/widget/TweetView;->eK:I

    invoke-virtual/range {v32 .. v32}, Lcom/twitter/library/provider/Tweet;->l()Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v43

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->es:I

    packed-switch v4, :pswitch_data_0

    move/from16 v4, v26

    :goto_16
    if-eqz v31, :cond_e

    invoke-direct/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->v()Z

    move-result v5

    if-nez v5, :cond_54

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->cO:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    sub-int v18, v23, v5

    :goto_17
    new-instance v7, Landroid/text/StaticLayout;

    const/4 v9, 0x0

    invoke-virtual/range {v42 .. v42}, Ljava/lang/String;->length()I

    move-result v10

    move-object/from16 v0, v42

    invoke-static {v0, v6}, Lhl;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v12

    sget-object v13, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/twitter/library/widget/TweetView;->A:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->C:I

    int-to-float v15, v5

    const/16 v16, 0x0

    sget-object v17, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v8, v42

    move-object v11, v6

    invoke-direct/range {v7 .. v18}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/twitter/library/widget/TweetView;->bY:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->cO:Landroid/graphics/drawable/Drawable;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/library/widget/TweetView;->cO:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/library/widget/TweetView;->cO:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v10

    invoke-virtual {v5, v7, v8, v9, v10}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->ag:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/widget/TweetView;->cO:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v7

    add-int/2addr v5, v7

    add-int/2addr v4, v5

    :cond_e
    const/4 v7, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->u()Z

    move-result v5

    if-nez v5, :cond_f

    invoke-direct/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->v()Z

    move-result v5

    if-eqz v5, :cond_3f

    :cond_f
    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->ag:I

    mul-int/lit8 v8, v5, 0x4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->bS:Landroid/text/StaticLayout;

    if-eqz v5, :cond_3e

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->bS:Landroid/text/StaticLayout;

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getHeight()I

    move-result v5

    :goto_18
    add-int/2addr v5, v8

    add-int/2addr v4, v5

    move v5, v4

    move v4, v7

    :goto_19
    move/from16 v0, v19

    invoke-static {v5, v0}, Ljava/lang/Math;->max(II)I

    move-result v9

    const/high16 v5, 0x40000000    # 2.0f

    move/from16 v0, v33

    if-ne v0, v5, :cond_43

    move/from16 v13, v30

    move v11, v4

    :goto_1a
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->ds:Z

    if-eqz v4, :cond_10

    move-object/from16 v4, p0

    move-object/from16 v5, v32

    move/from16 v7, v21

    move/from16 v8, v23

    move/from16 v10, v36

    move/from16 v12, v40

    invoke-direct/range {v4 .. v12}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/provider/Tweet;Landroid/text/TextPaint;IIIIII)I

    move-result v4

    add-int/2addr v13, v4

    :cond_10
    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->es:I

    packed-switch v4, :pswitch_data_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->db:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->setEmpty()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->dc:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->setEmpty()V

    :goto_1b
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v4, :cond_4b

    sub-int v4, v21, v35

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->aa:I

    sub-int/2addr v4, v5

    sub-int v4, v4, v38

    :goto_1c
    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->Z:I

    add-int v5, v5, v36

    add-int v5, v5, v20

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/widget/TweetView;->cY:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/twitter/library/widget/TweetView;->W:I

    sub-int v8, v4, v8

    move-object/from16 v0, p0

    iget v9, v0, Lcom/twitter/library/widget/TweetView;->Z:I

    sub-int v9, v5, v9

    add-int v4, v4, v38

    move-object/from16 v0, p0

    iget v10, v0, Lcom/twitter/library/widget/TweetView;->aa:I

    add-int/2addr v4, v10

    add-int v10, v5, v19

    move-object/from16 v0, p0

    iget v12, v0, Lcom/twitter/library/widget/TweetView;->Z:I

    add-int/2addr v10, v12

    invoke-virtual {v7, v8, v9, v4, v10}, Landroid/graphics/Rect;->set(IIII)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->by:Landroid/graphics/drawable/Drawable;

    if-eqz v4, :cond_11

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->by:Landroid/graphics/drawable/Drawable;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget v9, v0, Lcom/twitter/library/widget/TweetView;->ad:I

    move-object/from16 v0, p0

    iget v10, v0, Lcom/twitter/library/widget/TweetView;->ae:I

    sub-int v10, v5, v10

    invoke-virtual {v4, v7, v8, v9, v10}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :cond_11
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bz:Landroid/graphics/drawable/Drawable;

    if-eqz v4, :cond_13

    sub-int v4, v13, v5

    sub-int v4, v4, v19

    add-int v4, v4, v20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->ae:I

    sub-int/2addr v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    if-eqz v5, :cond_12

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->ai:I

    add-int/2addr v4, v5

    :cond_12
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->bz:Landroid/graphics/drawable/Drawable;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget v9, v0, Lcom/twitter/library/widget/TweetView;->ad:I

    invoke-virtual {v5, v7, v8, v9, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :cond_13
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->eB:Lcom/twitter/library/view/l;

    if-eqz v4, :cond_14

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v4, :cond_4c

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getLeft()I

    move-result v4

    add-int v4, v4, v34

    :goto_1d
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->bS:Landroid/text/StaticLayout;

    if-eqz v5, :cond_4d

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->bS:Landroid/text/StaticLayout;

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getHeight()I

    move-result v5

    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/library/widget/TweetView;->bV:I

    sub-int/2addr v5, v7

    :goto_1e
    add-int v7, v36, v5

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/twitter/library/widget/TweetView;->dV:Z

    if-eqz v5, :cond_4e

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->bZ:Landroid/text/StaticLayout;

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getHeight()I

    move-result v5

    :goto_1f
    add-int/2addr v5, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/widget/TweetView;->eB:Lcom/twitter/library/view/l;

    int-to-float v4, v4

    int-to-float v5, v5

    invoke-virtual {v7, v4, v5}, Lcom/twitter/library/view/l;->a(FF)V

    :cond_14
    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v23

    move/from16 v3, v40

    invoke-direct {v0, v1, v2, v11, v3}, Lcom/twitter/library/widget/TweetView;->a(IIII)V

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->el:Z

    if-eqz v4, :cond_15

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bJ:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->bJ:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/widget/TweetView;->bJ:Landroid/graphics/drawable/Drawable;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v7, :cond_4f

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/widget/TweetView;->em:Lcom/twitter/library/widget/al;

    invoke-virtual {v7}, Lcom/twitter/library/widget/al;->a()Landroid/graphics/Rect;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    mul-int/lit8 v10, v34, 0x2

    add-int/2addr v4, v10

    mul-int/lit8 v10, v36, 0x2

    add-int/2addr v5, v10

    invoke-virtual {v7, v8, v9, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    :cond_15
    :goto_20
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->en:Z

    if-eqz v4, :cond_16

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->cI:F

    invoke-virtual {v6, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bD:Lcom/twitter/internal/android/widget/ax;

    iget-object v4, v4, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    invoke-virtual {v6, v4}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    new-instance v4, Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->eo:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/widget/TweetView;->eo:Ljava/lang/String;

    invoke-static {v7, v6}, Lhl;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v7

    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct/range {v4 .. v11}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->ep:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->ep:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v4

    mul-int/lit8 v5, v37, 0x2

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->eq:Lcom/twitter/library/widget/al;

    invoke-virtual {v5}, Lcom/twitter/library/widget/al;->a()Landroid/graphics/Rect;

    move-result-object v5

    const/4 v6, 0x0

    add-int v7, v13, v4

    move/from16 v0, v21

    invoke-virtual {v5, v6, v13, v0, v7}, Landroid/graphics/Rect;->set(IIII)V

    add-int/2addr v13, v4

    :cond_16
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->ck:Landroid/text/StaticLayout;

    if-nez v4, :cond_17

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->cm:Landroid/text/StaticLayout;

    if-eqz v4, :cond_18

    :cond_17
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->dx:Lcom/twitter/library/view/TweetActionType;

    if-eqz v4, :cond_18

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->dx:Lcom/twitter/library/view/TweetActionType;

    move-object/from16 v0, v39

    invoke-virtual {v0, v4}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/library/widget/TweetView$InlineAction;

    if-eqz v4, :cond_50

    iget-object v5, v4, Lcom/twitter/library/widget/TweetView$InlineAction;->c:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    iget-object v4, v4, Lcom/twitter/library/widget/TweetView$InlineAction;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->bj:I

    sub-int/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/library/widget/TweetView;->dG:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->bj:I

    mul-int/lit8 v4, v4, 0x2

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/library/widget/TweetView;->dH:I

    :cond_18
    :goto_21
    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/twitter/library/widget/TweetView;->cK:I

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/twitter/library/widget/TweetView;->cJ:I

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1, v13}, Lcom/twitter/library/widget/TweetView;->setMeasuredDimension(II)V

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getForwardCard()Lcom/twitter/library/card/Card;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->es:I

    const/4 v6, 0x6

    if-ne v5, v6, :cond_0

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/twitter/library/card/Card;->A()Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_0

    const/high16 v5, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/library/widget/TweetView;->eG:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    or-int/2addr v5, v6

    move/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Landroid/view/View;->measure(II)V

    goto/16 :goto_0

    :cond_19
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getSuggestedMinimumWidth()I

    move-result v4

    const/high16 v6, -0x80000000

    if-ne v5, v6, :cond_5b

    move/from16 v0, v22

    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v4

    move/from16 v21, v4

    goto/16 :goto_1

    :cond_1a
    sub-int v4, v28, v24

    const/4 v5, 0x0

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    move/from16 v23, v4

    goto/16 :goto_2

    :cond_1b
    move/from16 v4, v24

    goto/16 :goto_3

    :cond_1c
    add-int v4, v4, v34

    move/from16 v25, v4

    goto/16 :goto_4

    :cond_1d
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "@"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v32

    iget-object v9, v0, Lcom/twitter/library/provider/Tweet;->p:Ljava/lang/String;

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_8

    :cond_1e
    move/from16 v20, v14

    goto/16 :goto_9

    :cond_1f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bQ:Landroid/text/StaticLayout;

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->cD:F

    invoke-virtual {v6, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bD:Lcom/twitter/internal/android/widget/ax;

    iget-object v4, v4, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    invoke-virtual {v6, v4}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bQ:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v10, v6}, Lcom/twitter/library/widget/TweetView;->a(Landroid/text/Layout;Ljava/lang/String;Landroid/graphics/Paint;)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/library/widget/TweetView;->bR:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bS:Landroid/text/StaticLayout;

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bS:Landroid/text/StaticLayout;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/text/StaticLayout;->getLineBaseline(I)I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->bQ:Landroid/text/StaticLayout;

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Landroid/text/StaticLayout;->getLineBaseline(I)I

    move-result v5

    sub-int/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->bR:I

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/library/widget/TweetView;->bV:I

    goto/16 :goto_b

    :cond_20
    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    goto/16 :goto_c

    :cond_21
    sget-object v13, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    goto/16 :goto_d

    :cond_22
    const/4 v4, 0x0

    goto/16 :goto_e

    :cond_23
    const/4 v5, 0x0

    goto/16 :goto_f

    :cond_24
    const/4 v7, 0x0

    move/from16 v31, v7

    goto/16 :goto_10

    :cond_25
    if-eqz v4, :cond_7

    new-instance v7, Landroid/text/StaticLayout;

    const/4 v9, 0x0

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    invoke-static {v8, v6}, Lhl;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v12

    sget-object v13, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/twitter/library/widget/TweetView;->A:F

    move-object/from16 v0, p0

    iget v11, v0, Lcom/twitter/library/widget/TweetView;->C:I

    int-to-float v15, v11

    const/16 v16, 0x0

    sget-object v17, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object v11, v6

    move/from16 v18, v23

    invoke-direct/range {v7 .. v18}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/twitter/library/widget/TweetView;->bZ:Landroid/text/StaticLayout;

    goto/16 :goto_11

    :cond_26
    if-eqz v4, :cond_56

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->af:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->bZ:Landroid/text/StaticLayout;

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getHeight()I

    move-result v5

    add-int/2addr v4, v5

    move/from16 v19, v8

    move/from16 v20, v4

    goto/16 :goto_12

    :cond_27
    add-int v4, v4, v25

    move/from16 v5, v25

    goto/16 :goto_13

    :cond_28
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->ef:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->setEmpty()V

    move/from16 v26, v7

    goto/16 :goto_14

    :pswitch_0
    sget-object v4, Lcom/twitter/library/widget/w;->a:[I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->ez:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    invoke-virtual {v5}, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_2

    sub-int v4, v28, v24

    :goto_22
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->ez:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    sget-object v7, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->c:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    if-eq v5, v7, :cond_29

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->ez:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    sget-object v7, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->d:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    if-eq v5, v7, :cond_29

    invoke-virtual/range {v32 .. v32}, Lcom/twitter/library/provider/Tweet;->X()Z

    move-result v5

    if-nez v5, :cond_29

    invoke-virtual/range {v32 .. v32}, Lcom/twitter/library/provider/Tweet;->W()Z

    move-result v5

    if-eqz v5, :cond_2d

    :cond_29
    move-object/from16 v0, v43

    iget v5, v0, Lcom/twitter/library/api/TweetClassicCard;->imageWidth:I

    int-to-double v7, v5

    move-object/from16 v0, v43

    iget v5, v0, Lcom/twitter/library/api/TweetClassicCard;->imageHeight:I

    int-to-double v9, v5

    div-double/2addr v7, v9

    :goto_23
    int-to-double v9, v4

    div-double v7, v9, v7

    invoke-static {v7, v8}, Ljava/lang/Math;->rint(D)D

    move-result-wide v7

    double-to-int v7, v7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    if-nez v5, :cond_2a

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    :cond_2a
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v5, :cond_2e

    sub-int v4, v25, v4

    move v5, v4

    move/from16 v4, v25

    :goto_24
    move-object/from16 v0, p0

    iget v8, v0, Lcom/twitter/library/widget/TweetView;->ai:I

    add-int v8, v8, v26

    add-int v8, v8, v36

    add-int v9, v8, v7

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    invoke-virtual {v10, v5, v8, v4, v9}, Landroid/graphics/Rect;->set(IIII)V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/library/widget/TweetView;->eg:Landroid/graphics/RectF;

    if-nez v8, :cond_2b

    new-instance v8, Landroid/graphics/RectF;

    invoke-direct {v8}, Landroid/graphics/RectF;-><init>()V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/twitter/library/widget/TweetView;->eg:Landroid/graphics/RectF;

    :cond_2b
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/library/widget/TweetView;->eg:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v10, v0, Lcom/twitter/library/widget/TweetView;->az:I

    add-int/2addr v10, v5

    int-to-float v10, v10

    move-object/from16 v0, p0

    iget v11, v0, Lcom/twitter/library/widget/TweetView;->az:I

    sub-int v11, v7, v11

    int-to-float v11, v11

    move-object/from16 v0, p0

    iget v12, v0, Lcom/twitter/library/widget/TweetView;->az:I

    sub-int v12, v4, v12

    int-to-float v12, v12

    move-object/from16 v0, p0

    iget v13, v0, Lcom/twitter/library/widget/TweetView;->az:I

    sub-int/2addr v7, v13

    int-to-float v7, v7

    invoke-virtual {v8, v10, v11, v12, v7}, Landroid/graphics/RectF;->set(FFFF)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/widget/TweetView;->dD:Ljava/lang/String;

    if-eqz v7, :cond_2c

    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/library/widget/TweetView;->aj:I

    add-int/2addr v7, v9

    move-object/from16 v0, p0

    iput v7, v0, Lcom/twitter/library/widget/TweetView;->dK:I

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-direct {v0, v6, v1}, Lcom/twitter/library/widget/TweetView;->c(Landroid/text/TextPaint;I)V

    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/library/widget/TweetView;->aj:I

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/library/widget/TweetView;->cf:Landroid/text/StaticLayout;

    invoke-virtual {v8}, Landroid/text/StaticLayout;->getHeight()I

    move-result v8

    add-int/2addr v7, v8

    add-int v26, v26, v7

    new-instance v8, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/twitter/library/widget/TweetView;->dK:I

    move-object/from16 v0, p0

    iget v10, v0, Lcom/twitter/library/widget/TweetView;->dK:I

    add-int/2addr v7, v10

    invoke-direct {v8, v5, v9, v4, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/twitter/library/widget/TweetView;->df:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetView;->b(Lcom/twitter/library/provider/Tweet;)V

    :cond_2c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->ai:I

    add-int/2addr v5, v4

    add-int v4, v26, v5

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    move/from16 v2, v25

    move/from16 v3, v23

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/provider/Tweet;III)I

    move-result v7

    add-int/2addr v4, v7

    add-int/2addr v5, v7

    move-object/from16 v0, p0

    iput v5, v0, Lcom/twitter/library/widget/TweetView;->eK:I

    goto/16 :goto_16

    :pswitch_1
    move/from16 v4, v28

    goto/16 :goto_22

    :cond_2d
    const-wide v7, 0x3ffc71c71c71c71cL    # 1.7777777777777777

    goto/16 :goto_23

    :cond_2e
    add-int v4, v4, v25

    move/from16 v5, v25

    goto/16 :goto_24

    :pswitch_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/twitter/library/widget/TweetView;->aA:I

    move/from16 v44, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/twitter/library/widget/TweetView;->aB:I

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/library/widget/TweetView;->et:Z

    if-eqz v4, :cond_37

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getLeft()I

    move-result v4

    add-int v4, v4, v34

    move/from16 v22, v4

    move/from16 v24, v4

    :goto_25
    move-object/from16 v0, v43

    iget-object v5, v0, Lcom/twitter/library/api/TweetClassicCard;->siteUser:Lcom/twitter/library/api/CardUser;

    move-object/from16 v0, v43

    iget-object v4, v0, Lcom/twitter/library/api/TweetClassicCard;->imageUrl:Ljava/lang/String;

    if-eqz v4, :cond_38

    const/4 v4, 0x1

    move/from16 v28, v4

    :goto_26
    move-object/from16 v0, p0

    iget v0, v0, Lcom/twitter/library/widget/TweetView;->az:I

    move/from16 v46, v0

    if-eqz v28, :cond_39

    sub-int v4, v23, v44

    sub-int v4, v4, v46

    const/4 v7, 0x0

    invoke-static {v4, v7}, Ljava/lang/Math;->max(II)I

    move-result v4

    move/from16 v29, v4

    :goto_27
    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->cD:F

    invoke-virtual {v6, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    if-eqz v5, :cond_30

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->bD:Lcom/twitter/internal/android/widget/ax;

    iget-object v4, v4, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    invoke-virtual {v6, v4}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v5, Lcom/twitter/library/api/CardUser;->fullName:Ljava/lang/String;

    if-eqz v4, :cond_2f

    iget-object v4, v5, Lcom/twitter/library/api/CardUser;->fullName:Ljava/lang/String;

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2f
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_30

    invoke-direct/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->w()Z

    move-result v4

    if-eqz v4, :cond_3a

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->aE:I

    sub-int v4, v29, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->D:I

    sub-int/2addr v4, v5

    const/4 v5, 0x0

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v18

    :goto_28
    new-instance v7, Landroid/text/StaticLayout;

    const/4 v9, 0x0

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v10

    invoke-static {v8, v6}, Lhl;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v12

    sget-object v13, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/twitter/library/widget/TweetView;->A:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->C:I

    int-to-float v15, v4

    const/16 v16, 0x0

    sget-object v17, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object v11, v6

    invoke-direct/range {v7 .. v18}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/twitter/library/widget/TweetView;->cc:Landroid/text/StaticLayout;

    :cond_30
    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->bv:F

    invoke-virtual {v6, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    new-instance v4, Landroid/text/StaticLayout;

    move-object/from16 v0, v43

    iget-object v5, v0, Lcom/twitter/library/api/TweetClassicCard;->title:Ljava/lang/String;

    if-eqz v5, :cond_3b

    move-object/from16 v0, v43

    iget-object v5, v0, Lcom/twitter/library/api/TweetClassicCard;->title:Ljava/lang/String;

    :goto_29
    const/16 v7, 0x3a

    invoke-static {v5, v7}, Lcom/twitter/library/util/Util;->b(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/twitter/library/widget/TweetView;->A:F

    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/library/widget/TweetView;->C:I

    int-to-float v10, v7

    const/4 v11, 0x0

    move/from16 v7, v29

    invoke-direct/range {v4 .. v11}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->cb:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->cb:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->cc:Landroid/text/StaticLayout;

    if-eqz v5, :cond_31

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->af:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/widget/TweetView;->cc:Landroid/text/StaticLayout;

    invoke-virtual {v7}, Landroid/text/StaticLayout;->getHeight()I

    move-result v7

    add-int/2addr v5, v7

    add-int/2addr v4, v5

    :cond_31
    if-eqz v28, :cond_32

    move/from16 v0, v45

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    :cond_32
    add-int v5, v36, v26

    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/library/widget/TweetView;->aF:I

    add-int/2addr v5, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/widget/TweetView;->cZ:Landroid/graphics/RectF;

    if-nez v7, :cond_33

    new-instance v7, Landroid/graphics/RectF;

    invoke-direct {v7}, Landroid/graphics/RectF;-><init>()V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/twitter/library/widget/TweetView;->cZ:Landroid/graphics/RectF;

    :cond_33
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/widget/TweetView;->da:Landroid/graphics/RectF;

    if-nez v7, :cond_34

    new-instance v7, Landroid/graphics/RectF;

    invoke-direct {v7}, Landroid/graphics/RectF;-><init>()V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/twitter/library/widget/TweetView;->da:Landroid/graphics/RectF;

    :cond_34
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/widget/TweetView;->cZ:Landroid/graphics/RectF;

    move/from16 v0, v24

    int-to-float v8, v0

    int-to-float v9, v5

    add-int v10, v24, v23

    int-to-float v10, v10

    add-int/2addr v4, v5

    int-to-float v4, v4

    invoke-virtual {v7, v8, v9, v10, v4}, Landroid/graphics/RectF;->set(FFFF)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->da:Landroid/graphics/RectF;

    iget v5, v7, Landroid/graphics/RectF;->left:F

    move/from16 v0, v46

    int-to-float v8, v0

    sub-float/2addr v5, v8

    iget v8, v7, Landroid/graphics/RectF;->top:F

    move/from16 v0, v46

    int-to-float v9, v0

    sub-float/2addr v8, v9

    iget v9, v7, Landroid/graphics/RectF;->right:F

    move/from16 v0, v46

    int-to-float v10, v0

    add-float/2addr v9, v10

    iget v10, v7, Landroid/graphics/RectF;->bottom:F

    move/from16 v0, v46

    int-to-float v11, v0

    add-float/2addr v10, v11

    invoke-virtual {v4, v5, v8, v9, v10}, Landroid/graphics/RectF;->set(FFFF)V

    if-eqz v28, :cond_3c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    if-nez v4, :cond_35

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    :cond_35
    iget v4, v7, Landroid/graphics/RectF;->top:F

    float-to-int v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    add-int v7, v22, v44

    add-int v8, v4, v45

    move/from16 v0, v22

    invoke-virtual {v5, v0, v4, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    :cond_36
    :goto_2a
    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->aF:I

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->cZ:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    add-float/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->aG:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    float-to-int v4, v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/library/widget/TweetView;->eK:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->eK:I

    add-int v4, v4, v26

    goto/16 :goto_16

    :cond_37
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getLeft()I

    move-result v4

    add-int v4, v4, v34

    add-int v4, v4, v38

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->aa:I

    add-int/2addr v5, v4

    add-int v4, v5, v23

    sub-int v4, v4, v44

    move/from16 v22, v4

    move/from16 v24, v5

    goto/16 :goto_25

    :cond_38
    const/4 v4, 0x0

    move/from16 v28, v4

    goto/16 :goto_26

    :cond_39
    move/from16 v29, v23

    goto/16 :goto_27

    :cond_3a
    move/from16 v18, v29

    goto/16 :goto_28

    :cond_3b
    const-string/jumbo v5, ""

    goto/16 :goto_29

    :cond_3c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    if-eqz v4, :cond_36

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->setEmpty()V

    goto :goto_2a

    :pswitch_3
    move-object/from16 v0, p0

    move-object/from16 v1, v32

    move/from16 v2, v23

    move/from16 v3, v26

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/provider/Tweet;II)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/library/widget/TweetView;->eK:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->eK:I

    add-int v4, v4, v26

    goto/16 :goto_16

    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->t()Z

    sub-int v4, v22, v35

    sub-int v5, v4, v25

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v5}, Lcom/twitter/library/provider/Tweet;->V()Lcom/twitter/library/card/instance/CardInstanceData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/library/card/instance/CardInstanceData;->b()I

    move-result v5

    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/library/widget/TweetView;->ai:I

    add-int v7, v7, v26

    add-int v7, v7, v36

    add-int v8, v7, v5

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/library/widget/TweetView;->eG:Landroid/graphics/Rect;

    if-eqz v9, :cond_3d

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/library/widget/TweetView;->eG:Landroid/graphics/Rect;

    move/from16 v0, v25

    invoke-virtual {v9, v0, v7, v4, v8}, Landroid/graphics/Rect;->set(IIII)V

    :goto_2b
    move-object/from16 v0, p0

    iput v5, v0, Lcom/twitter/library/widget/TweetView;->eK:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/TweetView;->eK:I

    add-int v4, v4, v26

    goto/16 :goto_16

    :cond_3d
    new-instance v9, Landroid/graphics/Rect;

    move/from16 v0, v25

    invoke-direct {v9, v0, v7, v4, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/twitter/library/widget/TweetView;->eG:Landroid/graphics/Rect;

    goto :goto_2b

    :cond_3e
    const/4 v5, 0x0

    goto/16 :goto_18

    :cond_3f
    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->es:I

    if-nez v5, :cond_53

    if-eqz v31, :cond_40

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->ag:I

    add-int/2addr v4, v5

    :cond_40
    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->aL:I

    add-int v5, v5, v36

    if-lez v27, :cond_41

    add-int v8, v4, v5

    move-object/from16 v0, p0

    iput v8, v0, Lcom/twitter/library/widget/TweetView;->dE:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/twitter/library/widget/TweetView;->aL:I

    add-int v8, v8, v27

    add-int/2addr v4, v8

    :cond_41
    if-lez v41, :cond_42

    add-int v8, v4, v5

    move-object/from16 v0, p0

    iput v8, v0, Lcom/twitter/library/widget/TweetView;->dJ:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/twitter/library/widget/TweetView;->aN:I

    add-int v8, v8, v41

    add-int/2addr v4, v8

    :cond_42
    invoke-virtual/range {v39 .. v39}, Ljava/util/LinkedHashMap;->size()I

    move-result v8

    if-lez v8, :cond_53

    add-int/2addr v5, v4

    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/library/widget/TweetView;->aN:I

    add-int v7, v7, v40

    add-int/2addr v4, v7

    move/from16 v47, v5

    move v5, v4

    move/from16 v4, v47

    goto/16 :goto_19

    :cond_43
    add-int v5, v9, v36

    add-int v5, v5, v37

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getSuggestedMinimumHeight()I

    move-result v7

    invoke-static {v5, v7}, Ljava/lang/Math;->max(II)I

    move-result v5

    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/library/widget/TweetView;->es:I

    if-eqz v7, :cond_52

    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/library/widget/TweetView;->aM:I

    sub-int v8, v7, v37

    if-lez v27, :cond_44

    add-int v7, v5, v8

    move-object/from16 v0, p0

    iput v7, v0, Lcom/twitter/library/widget/TweetView;->dE:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/library/widget/TweetView;->aL:I

    add-int v7, v7, v27

    add-int/2addr v5, v7

    :cond_44
    if-lez v41, :cond_45

    add-int v7, v5, v8

    move-object/from16 v0, p0

    iput v7, v0, Lcom/twitter/library/widget/TweetView;->dJ:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/library/widget/TweetView;->aN:I

    add-int v7, v7, v41

    add-int/2addr v5, v7

    :cond_45
    invoke-direct/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->v()Z

    move-result v7

    if-nez v7, :cond_46

    invoke-direct/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->u()Z

    move-result v7

    if-eqz v7, :cond_48

    :cond_46
    const/4 v7, 0x1

    :goto_2c
    invoke-virtual/range {v39 .. v39}, Ljava/util/LinkedHashMap;->size()I

    move-result v10

    if-lez v10, :cond_52

    if-nez v7, :cond_52

    add-int v4, v5, v8

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/widget/TweetView;->ez:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    sget-object v8, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->d:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    if-ne v7, v8, :cond_47

    add-int v7, v34, v38

    :cond_47
    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/library/widget/TweetView;->aO:I

    add-int v7, v7, v40

    add-int/2addr v5, v7

    move v11, v4

    move v4, v5

    :goto_2d
    const/high16 v5, -0x80000000

    move/from16 v0, v33

    if-ne v0, v5, :cond_51

    move/from16 v0, v30

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    move v13, v4

    goto/16 :goto_1a

    :cond_48
    const/4 v7, 0x0

    goto :goto_2c

    :pswitch_5
    invoke-virtual/range {v32 .. v32}, Lcom/twitter/library/provider/Tweet;->W()Z

    move-result v4

    if-nez v4, :cond_49

    invoke-virtual/range {v32 .. v32}, Lcom/twitter/library/provider/Tweet;->X()Z

    move-result v4

    if-eqz v4, :cond_4a

    :cond_49
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->db:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->setEmpty()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->dc:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->setEmpty()V

    goto/16 :goto_1b

    :cond_4a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->db:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->dc:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->ef:Landroid/graphics/Rect;

    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto/16 :goto_1b

    :pswitch_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->db:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->dc:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->ef:Landroid/graphics/Rect;

    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto/16 :goto_1b

    :pswitch_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->db:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/widget/TweetView;->cZ:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    float-to-int v5, v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/widget/TweetView;->cZ:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    float-to-int v7, v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/library/widget/TweetView;->cZ:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->right:F

    float-to-int v8, v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/library/widget/TweetView;->cZ:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->bottom:F

    float-to-int v9, v9

    invoke-virtual {v4, v5, v7, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_1b

    :pswitch_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->db:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->setEmpty()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->dc:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->setEmpty()V

    goto/16 :goto_1b

    :cond_4b
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/TweetView;->getLeft()I

    move-result v4

    add-int v4, v4, v34

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/TweetView;->W:I

    add-int/2addr v4, v5

    goto/16 :goto_1c

    :cond_4c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/widget/TweetView;->cY:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    goto/16 :goto_1d

    :cond_4d
    const/4 v5, 0x0

    goto/16 :goto_1e

    :cond_4e
    const/4 v5, 0x0

    goto/16 :goto_1f

    :cond_4f
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/widget/TweetView;->em:Lcom/twitter/library/widget/al;

    invoke-virtual {v7}, Lcom/twitter/library/widget/al;->a()Landroid/graphics/Rect;

    move-result-object v7

    mul-int/lit8 v8, v35, 0x2

    sub-int v8, v21, v8

    sub-int v4, v8, v4

    const/4 v8, 0x0

    mul-int/lit8 v9, v36, 0x2

    add-int/2addr v5, v9

    move/from16 v0, v21

    invoke-virtual {v7, v4, v8, v0, v5}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_20

    :cond_50
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/library/widget/TweetView;->dx:Lcom/twitter/library/view/TweetActionType;

    goto/16 :goto_21

    :cond_51
    move v13, v4

    goto/16 :goto_1a

    :cond_52
    move v11, v4

    move v4, v5

    goto/16 :goto_2d

    :cond_53
    move v5, v4

    move v4, v7

    goto/16 :goto_19

    :cond_54
    move/from16 v18, v23

    goto/16 :goto_17

    :cond_55
    move/from16 v27, v4

    goto/16 :goto_15

    :cond_56
    move/from16 v19, v8

    move/from16 v20, v9

    goto/16 :goto_12

    :cond_57
    move v5, v7

    goto/16 :goto_a

    :cond_58
    move/from16 v5, v18

    goto/16 :goto_7

    :cond_59
    move/from16 v18, v4

    goto/16 :goto_6

    :cond_5a
    move/from16 v4, v23

    goto/16 :goto_5

    :cond_5b
    move/from16 v21, v4

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_7
        :pswitch_6
        :pswitch_8
        :pswitch_7
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch
.end method

.method public onStartTemporaryDetach()V
    .locals 0

    invoke-super {p0}, Landroid/view/ViewGroup;->onStartTemporaryDetach()V

    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->B()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11

    const/4 v10, 0x0

    const/4 v9, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->b:Lcom/twitter/library/widget/aa;

    if-eqz v0, :cond_7

    invoke-static {}, Lcom/twitter/library/util/Util;->f()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->F()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->isEnabled()Z

    move-result v4

    if-nez v4, :cond_3

    if-ne v1, v3, :cond_1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/al;

    invoke-virtual {v0}, Lcom/twitter/library/widget/al;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/al;->a(Z)V

    goto :goto_0

    :cond_1
    move v3, v2

    :cond_2
    :goto_1
    return v3

    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    if-nez v6, :cond_4

    iget-object v6, p0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/twitter/library/widget/TweetView;->ee:Landroid/graphics/Rect;

    invoke-virtual {v6, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/twitter/library/widget/TweetView;->bI:Lcom/twitter/library/widget/TweetMediaImagesView;

    invoke-virtual {v6}, Lcom/twitter/library/widget/TweetMediaImagesView;->getVisibility()I

    move-result v6

    if-nez v6, :cond_4

    iget-object v6, p0, Lcom/twitter/library/widget/TweetView;->bI:Lcom/twitter/library/widget/TweetMediaImagesView;

    invoke-virtual {v6}, Lcom/twitter/library/widget/TweetMediaImagesView;->getLeft()I

    move-result v6

    sub-int v6, v4, v6

    iget-object v7, p0, Lcom/twitter/library/widget/TweetView;->bI:Lcom/twitter/library/widget/TweetMediaImagesView;

    invoke-virtual {v7}, Lcom/twitter/library/widget/TweetMediaImagesView;->getTop()I

    move-result v7

    sub-int v7, v5, v7

    iget-object v8, p0, Lcom/twitter/library/widget/TweetView;->bI:Lcom/twitter/library/widget/TweetMediaImagesView;

    invoke-virtual {v8, v6, v7}, Lcom/twitter/library/widget/TweetMediaImagesView;->a(II)V

    :cond_4
    iget-object v6, p0, Lcom/twitter/library/widget/TweetView;->eB:Lcom/twitter/library/view/l;

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/twitter/library/widget/TweetView;->eB:Lcom/twitter/library/view/l;

    invoke-virtual {v6, p1}, Lcom/twitter/library/view/l;->a(Landroid/view/MotionEvent;)Z

    move-result v6

    if-nez v6, :cond_2

    :cond_5
    iget-object v6, p0, Lcom/twitter/library/widget/TweetView;->eL:Lcom/twitter/library/widget/al;

    if-eqz v6, :cond_6

    packed-switch v1, :pswitch_data_0

    :cond_6
    :goto_2
    :pswitch_0
    packed-switch v1, :pswitch_data_1

    :cond_7
    :goto_3
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    goto :goto_1

    :pswitch_1
    iget-object v6, p0, Lcom/twitter/library/widget/TweetView;->bG:Landroid/os/Handler;

    invoke-virtual {v6, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v6, p0, Lcom/twitter/library/widget/TweetView;->eL:Lcom/twitter/library/widget/al;

    invoke-virtual {v6, v4, v5}, Lcom/twitter/library/widget/al;->a(II)Z

    move-result v6

    if-eqz v6, :cond_8

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bG:Landroid/os/Handler;

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v9, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    :cond_8
    iput-object v10, p0, Lcom/twitter/library/widget/TweetView;->eL:Lcom/twitter/library/widget/al;

    goto :goto_2

    :pswitch_2
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bG:Landroid/os/Handler;

    invoke-virtual {v0, v9}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->eL:Lcom/twitter/library/widget/al;

    invoke-virtual {v0}, Lcom/twitter/library/widget/al;->j()Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/TweetView;->post(Ljava/lang/Runnable;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->eL:Lcom/twitter/library/widget/al;

    invoke-virtual {v0}, Lcom/twitter/library/widget/al;->l()V

    :cond_9
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->eL:Lcom/twitter/library/widget/al;

    invoke-virtual {v0}, Lcom/twitter/library/widget/al;->f()Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/TweetView;->post(Ljava/lang/Runnable;)Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->eL:Lcom/twitter/library/widget/al;

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/al;->a(Z)V

    :cond_a
    iput-object v10, p0, Lcom/twitter/library/widget/TweetView;->eL:Lcom/twitter/library/widget/al;

    goto/16 :goto_1

    :pswitch_3
    iget-object v6, p0, Lcom/twitter/library/widget/TweetView;->bG:Landroid/os/Handler;

    invoke-virtual {v6, v9}, Landroid/os/Handler;->removeMessages(I)V

    iput-object v10, p0, Lcom/twitter/library/widget/TweetView;->eL:Lcom/twitter/library/widget/al;

    goto :goto_2

    :pswitch_4
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/al;

    invoke-virtual {v0, v4, v5}, Lcom/twitter/library/widget/al;->a(II)Z

    move-result v6

    if-eqz v6, :cond_d

    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->z()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-virtual {v0}, Lcom/twitter/library/widget/al;->b()V

    invoke-virtual {v0}, Lcom/twitter/library/widget/al;->e()Ljava/lang/Runnable;

    move-result-object v0

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/library/widget/TweetView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_1

    :cond_c
    invoke-virtual {v0, v3}, Lcom/twitter/library/widget/al;->a(Z)V

    goto/16 :goto_1

    :cond_d
    invoke-virtual {v0}, Lcom/twitter/library/widget/al;->c()Z

    move-result v6

    if-eqz v6, :cond_b

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/al;->a(Z)V

    goto/16 :goto_3

    :pswitch_5
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/al;

    invoke-virtual {v0}, Lcom/twitter/library/widget/al;->d()Z

    move-result v4

    invoke-virtual {v0}, Lcom/twitter/library/widget/al;->c()Z

    move-result v5

    if-nez v5, :cond_f

    if-eqz v4, :cond_e

    :cond_f
    if-eqz v4, :cond_10

    invoke-virtual {v0, v3}, Lcom/twitter/library/widget/al;->a(Z)V

    :cond_10
    invoke-static {}, Ljy;->b()Z

    move-result v1

    if-eqz v1, :cond_12

    invoke-virtual {v0}, Lcom/twitter/library/widget/al;->i()Z

    move-result v1

    if-eqz v1, :cond_12

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView;->eL:Lcom/twitter/library/widget/al;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->bG:Landroid/os/Handler;

    invoke-static {}, Ljy;->d()I

    move-result v2

    int-to-long v4, v2

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_11
    :goto_4
    invoke-virtual {v0}, Lcom/twitter/library/widget/al;->m()V

    goto/16 :goto_1

    :cond_12
    invoke-virtual {v0}, Lcom/twitter/library/widget/al;->h()Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/library/widget/TweetView;->post(Ljava/lang/Runnable;)Z

    move-result v1

    if-nez v1, :cond_13

    invoke-virtual {v0}, Lcom/twitter/library/widget/al;->k()V

    :cond_13
    if-eqz v4, :cond_14

    invoke-virtual {v0}, Lcom/twitter/library/widget/al;->f()Ljava/lang/Runnable;

    move-result-object v1

    invoke-static {}, Landroid/view/ViewConfiguration;->getPressedStateDuration()I

    move-result v2

    int-to-long v4, v2

    invoke-virtual {p0, v1, v4, v5}, Lcom/twitter/library/widget/TweetView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_4

    :cond_14
    invoke-virtual {v0}, Lcom/twitter/library/widget/al;->f()Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/library/widget/TweetView;->post(Ljava/lang/Runnable;)Z

    move-result v1

    if-nez v1, :cond_11

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/al;->a(Z)V

    goto :goto_4

    :pswitch_6
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    :goto_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/al;

    invoke-virtual {v0, v4, v5}, Lcom/twitter/library/widget/al;->a(II)Z

    move-result v7

    if-nez v7, :cond_17

    invoke-virtual {v0}, Lcom/twitter/library/widget/al;->m()V

    invoke-virtual {v0}, Lcom/twitter/library/widget/al;->c()Z

    move-result v1

    if-eqz v1, :cond_15

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/al;->a(Z)V

    :cond_15
    move v0, v3

    :goto_6
    move v1, v0

    goto :goto_5

    :cond_16
    if-eqz v1, :cond_7

    goto/16 :goto_1

    :pswitch_7
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/al;

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/al;->a(Z)V

    invoke-virtual {v0}, Lcom/twitter/library/widget/al;->m()V

    goto :goto_7

    :cond_17
    move v0, v1

    goto :goto_6

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public onWindowFocusChanged(Z)V
    .locals 3

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onWindowFocusChanged(Z)V

    if-nez p1, :cond_1

    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->F()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/al;

    invoke-virtual {v0}, Lcom/twitter/library/widget/al;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/al;->a(Z)V

    :cond_0
    invoke-virtual {v0}, Lcom/twitter/library/widget/al;->m()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public p()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    invoke-direct {p0, v0}, Lcom/twitter/library/widget/TweetView;->m(Lcom/twitter/library/provider/Tweet;)V

    return-void
.end method

.method public q()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->dy:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/TweetView$InlineAction;

    iget-object v0, v0, Lcom/twitter/library/widget/TweetView$InlineAction;->g:Landroid/text/StaticLayout;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()V
    .locals 5

    const/4 v4, 0x1

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->eD:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->eD:Z

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bN:Lcom/twitter/library/provider/Tweet;

    iget-object v1, v0, Lcom/twitter/library/provider/Tweet;->k:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->bO:Lcom/twitter/library/widget/ap;

    iget v2, v0, Lcom/twitter/library/provider/Tweet;->v:I

    iget-object v3, v0, Lcom/twitter/library/provider/Tweet;->k:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/twitter/library/widget/ap;->a(ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p0, v1, v4}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Bitmap;Z)V

    :cond_0
    iget-boolean v1, p0, Lcom/twitter/library/widget/TweetView;->ei:Z

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->l()Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/widget/TweetView;->bO:Lcom/twitter/library/widget/ap;

    iget v0, v0, Lcom/twitter/library/provider/Tweet;->v:I

    iget-object v1, v1, Lcom/twitter/library/api/TweetClassicCard;->siteUser:Lcom/twitter/library/api/CardUser;

    iget-object v1, v1, Lcom/twitter/library/api/CardUser;->profileImageUrl:Ljava/lang/String;

    invoke-interface {v2, v0, v1}, Lcom/twitter/library/widget/ap;->a(ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0, v0, v4}, Lcom/twitter/library/widget/TweetView;->b(Landroid/graphics/Bitmap;Z)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bI:Lcom/twitter/library/widget/TweetMediaImagesView;

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView;->bO:Lcom/twitter/library/widget/ap;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetMediaImagesView;->a(Lcom/twitter/library/widget/ap;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->refreshDrawableState()V

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->b()V

    :cond_2
    invoke-direct {p0}, Lcom/twitter/library/widget/TweetView;->G()V

    :cond_3
    return-void
.end method

.method public s()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->eH:Lcom/twitter/library/card/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->eH:Lcom/twitter/library/card/k;

    invoke-interface {v0}, Lcom/twitter/library/card/k;->p()Lcom/twitter/library/card/Card;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/card/Card;->A()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/TweetView;->addView(Landroid/view/View;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->eI:Z

    :cond_0
    return-void
.end method

.method public setActionPrompt(Lcom/twitter/library/api/Prompt;)V
    .locals 6

    invoke-virtual {p1}, Lcom/twitter/library/api/Prompt;->d()Lcom/twitter/library/view/TweetActionType;

    move-result-object v1

    iget-object v2, p1, Lcom/twitter/library/api/Prompt;->o:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/api/Prompt;->d:Ljava/lang/String;

    iget-object v4, p1, Lcom/twitter/library/api/Prompt;->a:Ljava/lang/String;

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/view/TweetActionType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setAlwaysExpand(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->dY:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/twitter/library/widget/TweetView;->dY:Z

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->requestLayout()V

    :cond_0
    return-void
.end method

.method public setAlwaysExpandMedia(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->dZ:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/twitter/library/widget/TweetView;->dZ:Z

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->requestLayout()V

    :cond_0
    return-void
.end method

.method public setAlwaysExpandPromotedCardsMedia(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->ea:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/twitter/library/widget/TweetView;->ea:Z

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->requestLayout()V

    :cond_0
    return-void
.end method

.method public setAmplifyEnabled(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/widget/TweetView;->eF:Z

    return-void
.end method

.method public setContentSize(F)V
    .locals 1

    iput p1, p0, Lcom/twitter/library/widget/TweetView;->cI:F

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;F)F

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->cD:F

    return-void
.end method

.method public setDisplaySensitiveMedia(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/widget/TweetView;->ey:Z

    return-void
.end method

.method public setDt2fPressed(Z)V
    .locals 0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setPressed(Z)V

    return-void
.end method

.method public setExpandCardMediaType(I)V
    .locals 1

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->eb:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/twitter/library/widget/TweetView;->eb:I

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->requestLayout()V

    :cond_0
    return-void
.end method

.method public setFriendshipCache(Lcom/twitter/library/util/FriendshipCache;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/widget/TweetView;->bP:Lcom/twitter/library/util/FriendshipCache;

    return-void
.end method

.method public setFullPreview(Lcom/twitter/library/widget/TweetView$FullPreviewMode;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/widget/TweetView;->ez:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    return-void
.end method

.method public setHideGeoInfo(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/widget/TweetView;->dR:Z

    return-void
.end method

.method public setHideInlineActions(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/widget/TweetView;->dq:Z

    return-void
.end method

.method public setHideMediaTagSummary(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/widget/TweetView;->dr:Z

    return-void
.end method

.method public setHighlighted(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->dp:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/twitter/library/widget/TweetView;->dp:Z

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->refreshDrawableState()V

    :cond_0
    return-void
.end method

.method public setOnProfileImageClickListener(Lcom/twitter/library/widget/aa;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/widget/TweetView;->b:Lcom/twitter/library/widget/aa;

    return-void
.end method

.method public setPivotIntent(Landroid/content/Intent;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/widget/TweetView;->er:Landroid/content/Intent;

    return-void
.end method

.method public setPressed(Z)V
    .locals 1

    invoke-static {}, Ljy;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->o()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setPressed(Z)V

    :cond_1
    return-void
.end method

.method public setProvider(Lcom/twitter/library/widget/ap;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/widget/TweetView;->bO:Lcom/twitter/library/widget/ap;

    return-void
.end method

.method public setReason(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/widget/TweetView;->ew:Ljava/lang/String;

    return-void
.end method

.method public setReasonIconResId(I)V
    .locals 0

    iput p1, p0, Lcom/twitter/library/widget/TweetView;->ex:I

    return-void
.end method

.method public setRenderRTL(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/widget/TweetView;->et:Z

    return-void
.end method

.method public setRepliedUserSummaryMode(Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/widget/TweetView;->bU:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;

    return-void
.end method

.method public setScribeItem(Lcom/twitter/library/scribe/ScribeItem;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/widget/TweetView;->eC:Lcom/twitter/library/scribe/ScribeItem;

    return-void
.end method

.method public setShouldSimulateInlineActions(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->dX:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/twitter/library/widget/TweetView;->dX:Z

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->requestLayout()V

    :cond_0
    return-void
.end method

.method public setShowActionPrompt(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/widget/TweetView;->ds:Z

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->requestLayout()V

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->invalidate()V

    return-void
.end method

.method public setShowRetweetSocialProofToOwner(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/widget/TweetView;->dU:Z

    return-void
.end method

.method public setShowSocialBadge(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/widget/TweetView;->dW:Z

    return-void
.end method

.method public setSimplifyUrls(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->ec:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/twitter/library/widget/TweetView;->ec:Z

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->requestLayout()V

    :cond_0
    return-void
.end method

.method public setSmartCrop(Z)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView;->bI:Lcom/twitter/library/widget/TweetMediaImagesView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/widget/TweetMediaImagesView;->a(Z)V

    return-void
.end method

.method public setSocialContextCount(I)V
    .locals 0

    iput p1, p0, Lcom/twitter/library/widget/TweetView;->eu:I

    return-void
.end method

.method public setSocialContextType(I)V
    .locals 0

    iput p1, p0, Lcom/twitter/library/widget/TweetView;->ev:I

    return-void
.end method

.method public setTweet(Lcom/twitter/library/provider/Tweet;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/provider/Tweet;ZLcom/twitter/library/card/k;)V

    return-void
.end method

.method public setTweetCountMode(I)V
    .locals 2

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->eE:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/twitter/library/widget/TweetView;->eE:I

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->eE:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->aR:I

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->dM:I

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->requestLayout()V

    :cond_0
    return-void

    :cond_1
    iget v0, p0, Lcom/twitter/library/widget/TweetView;->aP:I

    iput v0, p0, Lcom/twitter/library/widget/TweetView;->dM:I

    goto :goto_0
.end method

.method public setViewCountThreshold(I)V
    .locals 1

    iget v0, p0, Lcom/twitter/library/widget/TweetView;->dQ:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/twitter/library/widget/TweetView;->dQ:I

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetView;->requestLayout()V

    :cond_0
    return-void
.end method

.method public setWillTranslate(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/widget/TweetView;->ek:Z

    return-void
.end method

.method protected t()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetView;->eI:Z

    return v0
.end method
