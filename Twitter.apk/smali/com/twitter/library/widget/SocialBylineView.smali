.class public Lcom/twitter/library/widget/SocialBylineView;
.super Landroid/view/View;
.source "Twttr"


# static fields
.field private static final a:Landroid/text/TextPaint;


# instance fields
.field private final b:I

.field private final c:I

.field private final d:Landroid/content/res/ColorStateList;

.field private e:I

.field private f:Ljava/lang/CharSequence;

.field private g:F

.field private h:Landroid/graphics/drawable/Drawable;

.field private i:I

.field private j:Landroid/text/StaticLayout;

.field private k:Z

.field private final l:Lcom/twitter/internal/android/widget/ax;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    sput-object v0, Lcom/twitter/library/widget/SocialBylineView;->a:Landroid/text/TextPaint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/widget/SocialBylineView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget v0, Lib;->socialBylineViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/library/widget/SocialBylineView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-object v0, Lim;->SocialBylineView:[I

    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/widget/SocialBylineView;->c:I

    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/widget/SocialBylineView;->b:I

    const/4 v1, 0x2

    invoke-static {p1}, Lcom/twitter/library/util/Util;->f(Landroid/content/Context;)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/twitter/library/widget/SocialBylineView;->g:F

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/widget/SocialBylineView;->d:Landroid/content/res/ColorStateList;

    iget-object v1, p0, Lcom/twitter/library/widget/SocialBylineView;->d:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Lcom/twitter/library/widget/SocialBylineView;->getDrawableState()[I

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/widget/SocialBylineView;->e:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-static {p1}, Lcom/twitter/internal/android/widget/ax;->a(Landroid/content/Context;)Lcom/twitter/internal/android/widget/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/SocialBylineView;->l:Lcom/twitter/internal/android/widget/ax;

    return-void
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 3

    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    iget-object v0, p0, Lcom/twitter/library/widget/SocialBylineView;->d:Landroid/content/res/ColorStateList;

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/SocialBylineView;->d:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Lcom/twitter/library/widget/SocialBylineView;->getDrawableState()[I

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    iget v1, p0, Lcom/twitter/library/widget/SocialBylineView;->e:I

    if-eq v1, v0, :cond_0

    iput v0, p0, Lcom/twitter/library/widget/SocialBylineView;->e:I

    invoke-virtual {p0}, Lcom/twitter/library/widget/SocialBylineView;->invalidate()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/widget/SocialBylineView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/widget/SocialBylineView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/widget/SocialBylineView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/twitter/library/widget/SocialBylineView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    :cond_1
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    const/4 v0, 0x0

    sget-object v4, Lcom/twitter/library/widget/SocialBylineView;->a:Landroid/text/TextPaint;

    iget-object v2, p0, Lcom/twitter/library/widget/SocialBylineView;->h:Landroid/graphics/drawable/Drawable;

    iget-object v5, p0, Lcom/twitter/library/widget/SocialBylineView;->j:Landroid/text/StaticLayout;

    if-eqz v2, :cond_2

    const/4 v1, 0x1

    move v3, v1

    :goto_0
    if-eqz v3, :cond_3

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v2

    :goto_1
    if-eqz v5, :cond_1

    iget-boolean v6, p0, Lcom/twitter/library/widget/SocialBylineView;->k:Z

    if-eqz v6, :cond_5

    if-eqz v3, :cond_4

    iget v0, v1, Landroid/graphics/Rect;->left:I

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/twitter/library/widget/SocialBylineView;->b:I

    sub-int/2addr v0, v1

    :cond_0
    :goto_2
    invoke-virtual {v5}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/twitter/library/widget/SocialBylineView;->getPaddingTop()I

    move-result v3

    sub-int v1, v2, v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v3

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/twitter/library/widget/SocialBylineView;->l:Lcom/twitter/internal/android/widget/ax;

    iget-object v0, v0, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    invoke-virtual {v4, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget v0, p0, Lcom/twitter/library/widget/SocialBylineView;->g:F

    invoke-virtual {v4, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    iget v0, p0, Lcom/twitter/library/widget/SocialBylineView;->e:I

    invoke-virtual {v4, v0}, Landroid/text/TextPaint;->setColor(I)V

    invoke-virtual {v5, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_1
    return-void

    :cond_2
    move v3, v0

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    move v2, v0

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/twitter/library/widget/SocialBylineView;->getWidth()I

    move-result v0

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/twitter/library/widget/SocialBylineView;->b:I

    sub-int/2addr v0, v1

    goto :goto_2

    :cond_5
    if-eqz v3, :cond_0

    iget v0, v1, Landroid/graphics/Rect;->right:I

    iget v1, p0, Lcom/twitter/library/widget/SocialBylineView;->b:I

    add-int/2addr v0, v1

    goto :goto_2
.end method

.method protected onMeasure(II)V
    .locals 19

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v6

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    sget-object v3, Lcom/twitter/library/widget/SocialBylineView;->a:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/twitter/library/widget/SocialBylineView;->h:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/widget/SocialBylineView;->f:Ljava/lang/CharSequence;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/SocialBylineView;->getPaddingLeft()I

    move-result v16

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/SocialBylineView;->getPaddingTop()I

    move-result v17

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/SocialBylineView;->getPaddingRight()I

    move-result v18

    if-eqz v15, :cond_1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/twitter/library/widget/SocialBylineView;->c:I

    invoke-virtual {v15}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    invoke-static {v1, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-virtual {v15}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    move v13, v1

    move v14, v5

    :goto_0
    sparse-switch v6, :sswitch_data_0

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/SocialBylineView;->getSuggestedMinimumWidth()I

    move-result v4

    sub-int v1, v4, v16

    sub-int v1, v1, v18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/SocialBylineView;->b:I

    sub-int/2addr v1, v5

    sub-int/2addr v1, v14

    move v11, v1

    move v12, v4

    :goto_1
    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/widget/SocialBylineView;->l:Lcom/twitter/internal/android/widget/ax;

    iget-object v1, v1, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    invoke-virtual {v3, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-object/from16 v0, p0

    iget v1, v0, Lcom/twitter/library/widget/SocialBylineView;->g:F

    invoke-virtual {v3, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    if-nez v6, :cond_2

    new-instance v1, Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/library/widget/SocialBylineView;->i:I

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v1 .. v8}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    :goto_2
    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    :goto_3
    invoke-static {v2, v13}, Ljava/lang/Math;->max(II)I

    move-result v3

    if-eqz v15, :cond_0

    invoke-virtual {v15}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/twitter/library/widget/SocialBylineView;->k:Z

    if-eqz v2, :cond_4

    sub-int v2, v12, v18

    sub-int/2addr v2, v14

    :goto_4
    sub-int v5, v3, v13

    div-int/lit8 v5, v5, 0x2

    add-int v5, v5, v17

    add-int/2addr v4, v2

    add-int v6, v5, v13

    invoke-virtual {v15, v2, v5, v4, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :cond_0
    move-object/from16 v0, p0

    iput v11, v0, Lcom/twitter/library/widget/SocialBylineView;->i:I

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/twitter/library/widget/SocialBylineView;->j:Landroid/text/StaticLayout;

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    if-ne v1, v2, :cond_5

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    :goto_5
    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v1}, Lcom/twitter/library/widget/SocialBylineView;->setMeasuredDimension(II)V

    return-void

    :cond_1
    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/SocialBylineView;->c:I

    const/4 v1, 0x0

    move v13, v1

    move v14, v5

    goto :goto_0

    :sswitch_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/widget/SocialBylineView;->f:Ljava/lang/CharSequence;

    invoke-static {v1, v3}, Lhl;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v1

    add-int v4, v16, v18

    add-int/2addr v4, v14

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/SocialBylineView;->b:I

    add-int/2addr v4, v5

    add-int/2addr v4, v1

    move v11, v1

    move v12, v4

    goto :goto_1

    :sswitch_1
    sub-int v1, v4, v16

    sub-int v1, v1, v18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/library/widget/SocialBylineView;->b:I

    sub-int/2addr v1, v5

    sub-int/2addr v1, v14

    move v11, v1

    move v12, v4

    goto/16 :goto_1

    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/widget/SocialBylineView;->f:Ljava/lang/CharSequence;

    invoke-static {v1, v3}, Lhl;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/library/widget/SocialBylineView;->i:I

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v6

    new-instance v1, Landroid/text/StaticLayout;

    const/4 v3, 0x0

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v4

    sget-object v5, Lcom/twitter/library/widget/SocialBylineView;->a:Landroid/text/TextPaint;

    sget-object v7, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct/range {v1 .. v10}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    goto/16 :goto_2

    :cond_3
    const/4 v2, 0x0

    const/4 v1, 0x0

    goto/16 :goto_3

    :cond_4
    add-int v2, v16, v14

    sub-int/2addr v2, v4

    goto :goto_4

    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/SocialBylineView;->getSuggestedMinimumHeight()I

    move-result v1

    add-int v2, v3, v17

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/widget/SocialBylineView;->getPaddingBottom()I

    move-result v3

    add-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_5

    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_0
        0x0 -> :sswitch_0
        0x40000000 -> :sswitch_1
    .end sparse-switch
.end method

.method public setIcon(I)V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/library/widget/SocialBylineView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/SocialBylineView;->setIconDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setIconDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/SocialBylineView;->h:Landroid/graphics/drawable/Drawable;

    if-ne v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/twitter/library/widget/SocialBylineView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/widget/SocialBylineView;->requestLayout()V

    goto :goto_0
.end method

.method public setLabel(Ljava/lang/CharSequence;)V
    .locals 1

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/SocialBylineView;->f:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    :cond_0
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/twitter/library/widget/SocialBylineView;->f:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iput-object p1, p0, Lcom/twitter/library/widget/SocialBylineView;->f:Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/twitter/library/widget/SocialBylineView;->requestLayout()V

    goto :goto_0
.end method

.method public setLabelSize(F)V
    .locals 0

    iput p1, p0, Lcom/twitter/library/widget/SocialBylineView;->g:F

    return-void
.end method

.method public setRenderRTL(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/widget/SocialBylineView;->k:Z

    return-void
.end method
