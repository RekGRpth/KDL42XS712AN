.class public Lcom/twitter/library/widget/ComposerBar$LayoutParams;
.super Lcom/twitter/internal/android/widget/RectLayoutParams;
.source "Twttr"


# instance fields
.field public e:Z

.field public f:I


# direct methods
.method public constructor <init>(II)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Lcom/twitter/internal/android/widget/RectLayoutParams;-><init>(II)V

    iput-boolean v0, p0, Lcom/twitter/library/widget/ComposerBar$LayoutParams;->e:Z

    iput v0, p0, Lcom/twitter/library/widget/ComposerBar$LayoutParams;->f:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Lcom/twitter/internal/android/widget/RectLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v2, p0, Lcom/twitter/library/widget/ComposerBar$LayoutParams;->e:Z

    iput v2, p0, Lcom/twitter/library/widget/ComposerBar$LayoutParams;->f:I

    sget-object v0, Lim;->ComposerBar_Layout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/library/widget/ComposerBar$LayoutParams;->e:Z

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/library/widget/ComposerBar$LayoutParams;->e:Z

    const/4 v1, 0x1

    iget v2, p0, Lcom/twitter/library/widget/ComposerBar$LayoutParams;->f:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/widget/ComposerBar$LayoutParams;->f:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lcom/twitter/internal/android/widget/RectLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    iput-boolean v0, p0, Lcom/twitter/library/widget/ComposerBar$LayoutParams;->e:Z

    iput v0, p0, Lcom/twitter/library/widget/ComposerBar$LayoutParams;->f:I

    return-void
.end method
