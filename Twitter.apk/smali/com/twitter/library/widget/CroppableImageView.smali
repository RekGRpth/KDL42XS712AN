.class public Lcom/twitter/library/widget/CroppableImageView;
.super Lcom/twitter/library/widget/MultiTouchImageView;
.source "Twttr"


# instance fields
.field private final e:Landroid/graphics/Bitmap;

.field private final f:Landroid/graphics/Bitmap;

.field private final g:Landroid/graphics/Bitmap;

.field private final h:Landroid/graphics/Bitmap;

.field private final i:Landroid/graphics/Paint;

.field private final j:Landroid/graphics/Paint;

.field private final k:Landroid/graphics/Paint;

.field private final l:Landroid/graphics/RectF;

.field private final m:Landroid/graphics/PointF;

.field private final n:Z

.field private final o:Z

.field private p:Lcom/twitter/library/widget/e;

.field private q:Z

.field private r:I

.field private s:I

.field private t:I

.field private u:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/widget/CroppableImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget v0, Lib;->croppableImageViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/library/widget/CroppableImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9

    const/high16 v8, 0x42b40000    # 90.0f

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/widget/MultiTouchImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/widget/CroppableImageView;->i:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/widget/CroppableImageView;->j:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/widget/CroppableImageView;->k:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/widget/CroppableImageView;->l:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/widget/CroppableImageView;->m:Landroid/graphics/PointF;

    invoke-virtual {p0}, Lcom/twitter/library/widget/CroppableImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget-object v2, Lim;->CroppableImageView:[I

    invoke-virtual {p1, p2, v2, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    invoke-virtual {v2, v6, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    iput v3, p0, Lcom/twitter/library/widget/CroppableImageView;->t:I

    invoke-virtual {v2, v1, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    iput v3, p0, Lcom/twitter/library/widget/CroppableImageView;->u:I

    const/4 v3, 0x5

    invoke-virtual {v2, v3, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    iput-boolean v3, p0, Lcom/twitter/library/widget/CroppableImageView;->o:Z

    const/4 v3, 0x6

    invoke-virtual {v2, v3, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    iput-boolean v3, p0, Lcom/twitter/library/widget/CroppableImageView;->n:Z

    iget-object v3, p0, Lcom/twitter/library/widget/CroppableImageView;->i:Landroid/graphics/Paint;

    const/4 v4, 0x2

    sget v5, Lid;->white:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/4 v4, 0x3

    const/high16 v5, 0x40000000    # 2.0f

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v3, p0, Lcom/twitter/library/widget/CroppableImageView;->j:Landroid/graphics/Paint;

    const/4 v4, 0x7

    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v3, p0, Lcom/twitter/library/widget/CroppableImageView;->k:Landroid/graphics/Paint;

    const/4 v4, 0x4

    sget v5, Lid;->dark_transparent_black:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v4, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    iget-boolean v0, p0, Lcom/twitter/library/widget/CroppableImageView;->o:Z

    if-eqz v0, :cond_0

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/library/widget/CroppableImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lif;->ic_filters_crop_corner:I

    invoke-static {v0, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/CroppableImageView;->e:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/twitter/library/widget/CroppableImageView;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v0, p0, Lcom/twitter/library/widget/CroppableImageView;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v5, v8}, Landroid/graphics/Matrix;->postRotate(F)Z

    iget-object v0, p0, Lcom/twitter/library/widget/CroppableImageView;->e:Landroid/graphics/Bitmap;

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/CroppableImageView;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v5, v8}, Landroid/graphics/Matrix;->postRotate(F)Z

    iget-object v0, p0, Lcom/twitter/library/widget/CroppableImageView;->e:Landroid/graphics/Bitmap;

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/CroppableImageView;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v5, v8}, Landroid/graphics/Matrix;->postRotate(F)Z

    iget-object v0, p0, Lcom/twitter/library/widget/CroppableImageView;->e:Landroid/graphics/Bitmap;

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/CroppableImageView;->g:Landroid/graphics/Bitmap;

    iput v3, p0, Lcom/twitter/library/widget/CroppableImageView;->s:I

    :goto_0
    return-void

    :cond_0
    iput-object v7, p0, Lcom/twitter/library/widget/CroppableImageView;->e:Landroid/graphics/Bitmap;

    iput-object v7, p0, Lcom/twitter/library/widget/CroppableImageView;->f:Landroid/graphics/Bitmap;

    iput-object v7, p0, Lcom/twitter/library/widget/CroppableImageView;->h:Landroid/graphics/Bitmap;

    iput-object v7, p0, Lcom/twitter/library/widget/CroppableImageView;->g:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private static a(Landroid/graphics/PointF;FF)F
    .locals 4

    iget v0, p0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, p1

    float-to-double v0, v0

    iget v2, p0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, p2

    float-to-double v2, v2

    mul-double/2addr v0, v0

    mul-double/2addr v2, v2

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method private a(Landroid/graphics/PointF;)I
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/widget/CroppableImageView;->c:Landroid/graphics/RectF;

    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget v2, v0, Landroid/graphics/RectF;->top:F

    invoke-static {p1, v1, v2}, Lcom/twitter/library/widget/CroppableImageView;->a(Landroid/graphics/PointF;FF)F

    move-result v1

    iget v2, p0, Lcom/twitter/library/widget/CroppableImageView;->s:I

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget v1, v0, Landroid/graphics/RectF;->right:F

    iget v2, v0, Landroid/graphics/RectF;->top:F

    invoke-static {p1, v1, v2}, Lcom/twitter/library/widget/CroppableImageView;->a(Landroid/graphics/PointF;FF)F

    move-result v1

    iget v2, p0, Lcom/twitter/library/widget/CroppableImageView;->s:I

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    iget v1, v0, Landroid/graphics/RectF;->right:F

    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    invoke-static {p1, v1, v2}, Lcom/twitter/library/widget/CroppableImageView;->a(Landroid/graphics/PointF;FF)F

    move-result v1

    iget v2, p0, Lcom/twitter/library/widget/CroppableImageView;->s:I

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    invoke-static {p1, v1, v0}, Lcom/twitter/library/widget/CroppableImageView;->a(Landroid/graphics/PointF;FF)F

    move-result v0

    iget v1, p0, Lcom/twitter/library/widget/CroppableImageView;->s:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_3

    const/4 v0, 0x4

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(IFF)V
    .locals 8

    invoke-virtual {p0}, Lcom/twitter/library/widget/CroppableImageView;->getImageRect()Landroid/graphics/RectF;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/widget/CroppableImageView;->c:Landroid/graphics/RectF;

    iget v2, p0, Lcom/twitter/library/widget/CroppableImageView;->s:I

    int-to-float v2, v2

    packed-switch p1, :pswitch_data_0

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/library/widget/CroppableImageView;->invalidate()V

    return-void

    :pswitch_0
    iget v3, v1, Landroid/graphics/RectF;->left:F

    iget v4, v1, Landroid/graphics/RectF;->left:F

    iget v5, v1, Landroid/graphics/RectF;->left:F

    iget v6, v0, Landroid/graphics/RectF;->left:F

    iget v7, v1, Landroid/graphics/RectF;->right:F

    sub-float/2addr v7, v2

    invoke-static {p2, v4, v5, v6, v7}, Lcom/twitter/library/widget/CroppableImageView;->a(FFFFF)F

    move-result v4

    add-float/2addr v3, v4

    iput v3, v1, Landroid/graphics/RectF;->left:F

    iget v3, v1, Landroid/graphics/RectF;->top:F

    iget v4, v1, Landroid/graphics/RectF;->top:F

    iget v5, v1, Landroid/graphics/RectF;->top:F

    iget v0, v0, Landroid/graphics/RectF;->top:F

    iget v6, v1, Landroid/graphics/RectF;->bottom:F

    sub-float v2, v6, v2

    invoke-static {p3, v4, v5, v0, v2}, Lcom/twitter/library/widget/CroppableImageView;->a(FFFFF)F

    move-result v0

    add-float/2addr v0, v3

    iput v0, v1, Landroid/graphics/RectF;->top:F

    goto :goto_0

    :pswitch_1
    iget v3, v1, Landroid/graphics/RectF;->right:F

    iget v4, v1, Landroid/graphics/RectF;->right:F

    iget v5, v1, Landroid/graphics/RectF;->right:F

    iget v6, v1, Landroid/graphics/RectF;->left:F

    add-float/2addr v6, v2

    iget v7, v0, Landroid/graphics/RectF;->right:F

    invoke-static {p2, v4, v5, v6, v7}, Lcom/twitter/library/widget/CroppableImageView;->a(FFFFF)F

    move-result v4

    add-float/2addr v3, v4

    iput v3, v1, Landroid/graphics/RectF;->right:F

    iget v3, v1, Landroid/graphics/RectF;->top:F

    iget v4, v1, Landroid/graphics/RectF;->top:F

    iget v5, v1, Landroid/graphics/RectF;->top:F

    iget v0, v0, Landroid/graphics/RectF;->top:F

    iget v6, v1, Landroid/graphics/RectF;->bottom:F

    sub-float v2, v6, v2

    invoke-static {p3, v4, v5, v0, v2}, Lcom/twitter/library/widget/CroppableImageView;->a(FFFFF)F

    move-result v0

    add-float/2addr v0, v3

    iput v0, v1, Landroid/graphics/RectF;->top:F

    goto :goto_0

    :pswitch_2
    iget v3, v1, Landroid/graphics/RectF;->right:F

    iget v4, v1, Landroid/graphics/RectF;->right:F

    iget v5, v1, Landroid/graphics/RectF;->right:F

    iget v6, v1, Landroid/graphics/RectF;->left:F

    add-float/2addr v6, v2

    iget v7, v0, Landroid/graphics/RectF;->right:F

    invoke-static {p2, v4, v5, v6, v7}, Lcom/twitter/library/widget/CroppableImageView;->a(FFFFF)F

    move-result v4

    add-float/2addr v3, v4

    iput v3, v1, Landroid/graphics/RectF;->right:F

    iget v3, v1, Landroid/graphics/RectF;->bottom:F

    iget v4, v1, Landroid/graphics/RectF;->bottom:F

    iget v5, v1, Landroid/graphics/RectF;->bottom:F

    iget v6, v1, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v6

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    invoke-static {p3, v4, v5, v2, v0}, Lcom/twitter/library/widget/CroppableImageView;->a(FFFFF)F

    move-result v0

    add-float/2addr v0, v3

    iput v0, v1, Landroid/graphics/RectF;->bottom:F

    goto :goto_0

    :pswitch_3
    iget v3, v1, Landroid/graphics/RectF;->left:F

    iget v4, v1, Landroid/graphics/RectF;->left:F

    iget v5, v1, Landroid/graphics/RectF;->left:F

    iget v6, v0, Landroid/graphics/RectF;->left:F

    iget v7, v1, Landroid/graphics/RectF;->right:F

    sub-float/2addr v7, v2

    invoke-static {p2, v4, v5, v6, v7}, Lcom/twitter/library/widget/CroppableImageView;->a(FFFFF)F

    move-result v4

    add-float/2addr v3, v4

    iput v3, v1, Landroid/graphics/RectF;->left:F

    iget v3, v1, Landroid/graphics/RectF;->bottom:F

    iget v4, v1, Landroid/graphics/RectF;->bottom:F

    iget v5, v1, Landroid/graphics/RectF;->bottom:F

    iget v6, v1, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v6

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    invoke-static {p3, v4, v5, v2, v0}, Lcom/twitter/library/widget/CroppableImageView;->a(FFFFF)F

    move-result v0

    add-float/2addr v0, v3

    iput v0, v1, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic a(Lcom/twitter/library/widget/CroppableImageView;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/library/widget/CroppableImageView;->setAnimating(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 14

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/twitter/library/widget/CroppableImageView;->c:Landroid/graphics/RectF;

    invoke-direct {p0}, Lcom/twitter/library/widget/CroppableImageView;->getPaddedViewRect()Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    sub-float v10, v3, v2

    sub-float v11, v5, v4

    const/4 v6, 0x1

    invoke-static {v0, v1, v6}, Lcom/twitter/library/widget/CroppableImageView;->a(Landroid/graphics/RectF;Landroid/graphics/RectF;Z)F

    move-result v6

    cmpl-float v0, v10, v7

    if-nez v0, :cond_0

    cmpl-float v0, v11, v7

    if-nez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, v6, v0

    if-eqz v0, :cond_1

    :cond_0
    if-eqz p1, :cond_2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_2

    new-instance v0, Lcom/twitter/library/widget/g;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/widget/g;-><init>(Lcom/twitter/library/widget/CroppableImageView;FFFFF)V

    invoke-virtual {v0}, Lcom/twitter/library/widget/g;->a()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v13, 0x0

    move-object v7, p0

    move v8, v2

    move v9, v4

    move v12, v6

    invoke-virtual/range {v7 .. v13}, Lcom/twitter/library/widget/CroppableImageView;->a(FFFFFI)V

    invoke-virtual {p0, v10, v11, v6}, Lcom/twitter/library/widget/CroppableImageView;->a(FFF)V

    goto :goto_0
.end method

.method private getPaddedViewRect()Landroid/graphics/RectF;
    .locals 3

    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/twitter/library/widget/CroppableImageView;->b:Landroid/graphics/RectF;

    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iget v1, v0, Landroid/graphics/RectF;->top:F

    iget v2, p0, Lcom/twitter/library/widget/CroppableImageView;->u:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    iget v1, p0, Lcom/twitter/library/widget/CroppableImageView;->t:I

    int-to-float v1, v1

    iget v2, p0, Lcom/twitter/library/widget/CroppableImageView;->t:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->inset(FF)V

    return-object v0
.end method

.method private setAnimating(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/widget/CroppableImageView;->q:Z

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/library/widget/CroppableImageView;->a(Z)V

    return-void
.end method

.method public a(IZ)V
    .locals 7

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/twitter/library/widget/CroppableImageView;->q:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/twitter/library/widget/CroppableImageView;->d:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/twitter/library/widget/CroppableImageView;->d:I

    if-eqz p2, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/widget/CroppableImageView;->c:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    iget-object v0, p0, Lcom/twitter/library/widget/CroppableImageView;->c:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    const/high16 v5, 0x3f800000    # 1.0f

    move-object v0, p0

    move v4, v3

    move v6, p1

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/library/widget/CroppableImageView;->a(FFFFFI)V

    invoke-virtual {p0}, Lcom/twitter/library/widget/CroppableImageView;->d()V

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/twitter/library/widget/f;

    invoke-direct {v0, p0, p1}, Lcom/twitter/library/widget/f;-><init>(Lcom/twitter/library/widget/CroppableImageView;I)V

    invoke-virtual {v0}, Lcom/twitter/library/widget/f;->a()V

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 13

    const v12, 0x3f2aaaab

    const v11, 0x3eaaaaab

    const/4 v10, 0x0

    const/4 v9, 0x0

    invoke-super {p0, p1}, Lcom/twitter/library/widget/MultiTouchImageView;->onDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    iget-object v6, p0, Lcom/twitter/library/widget/CroppableImageView;->c:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/twitter/library/widget/CroppableImageView;->l:Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/twitter/library/widget/CroppableImageView;->k:Landroid/graphics/Paint;

    iget v4, v6, Landroid/graphics/RectF;->top:F

    iget v5, v6, Landroid/graphics/RectF;->left:F

    iget v7, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v2, v9, v4, v5, v7}, Landroid/graphics/RectF;->set(FFFF)V

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    int-to-float v4, v0

    iget v5, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v2, v9, v9, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    iget v4, v6, Landroid/graphics/RectF;->right:F

    iget v5, v6, Landroid/graphics/RectF;->top:F

    int-to-float v7, v0

    iget v8, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v2, v4, v5, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    iget v4, v6, Landroid/graphics/RectF;->bottom:F

    int-to-float v0, v0

    int-to-float v1, v1

    invoke-virtual {v2, v9, v4, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/twitter/library/widget/CroppableImageView;->c:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/twitter/library/widget/CroppableImageView;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    iget-boolean v0, p0, Lcom/twitter/library/widget/CroppableImageView;->n:Z

    if-eqz v0, :cond_0

    iget v0, v6, Landroid/graphics/RectF;->left:F

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v1

    mul-float/2addr v1, v11

    add-float/2addr v1, v0

    iget v0, v6, Landroid/graphics/RectF;->left:F

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v2

    mul-float/2addr v2, v12

    add-float v7, v0, v2

    iget v0, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v2

    mul-float/2addr v2, v11

    add-float v8, v0, v2

    iget v0, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v2

    mul-float/2addr v2, v12

    add-float v9, v0, v2

    iget v2, v6, Landroid/graphics/RectF;->top:F

    iget v4, v6, Landroid/graphics/RectF;->bottom:F

    iget-object v5, p0, Lcom/twitter/library/widget/CroppableImageView;->j:Landroid/graphics/Paint;

    move-object v0, p1

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget v2, v6, Landroid/graphics/RectF;->top:F

    iget v4, v6, Landroid/graphics/RectF;->bottom:F

    iget-object v5, p0, Lcom/twitter/library/widget/CroppableImageView;->j:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, v7

    move v3, v7

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v3, v6, Landroid/graphics/RectF;->right:F

    iget-object v5, p0, Lcom/twitter/library/widget/CroppableImageView;->j:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v8

    move v4, v8

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v3, v6, Landroid/graphics/RectF;->right:F

    iget-object v5, p0, Lcom/twitter/library/widget/CroppableImageView;->j:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v9

    move v4, v9

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/twitter/library/widget/CroppableImageView;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v1, v0

    const v2, 0x3e0ba2e9

    mul-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/twitter/library/widget/CroppableImageView;->e:Landroid/graphics/Bitmap;

    iget v3, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v3, v1

    iget v4, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v4, v1

    invoke-virtual {p1, v2, v3, v4, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    iget-object v2, p0, Lcom/twitter/library/widget/CroppableImageView;->f:Landroid/graphics/Bitmap;

    iget v3, v6, Landroid/graphics/RectF;->right:F

    int-to-float v4, v0

    sub-float/2addr v3, v4

    add-float/2addr v3, v1

    iget v4, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v4, v1

    invoke-virtual {p1, v2, v3, v4, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    iget-object v2, p0, Lcom/twitter/library/widget/CroppableImageView;->h:Landroid/graphics/Bitmap;

    iget v3, v6, Landroid/graphics/RectF;->right:F

    int-to-float v4, v0

    sub-float/2addr v3, v4

    add-float/2addr v3, v1

    iget v4, v6, Landroid/graphics/RectF;->bottom:F

    int-to-float v5, v0

    sub-float/2addr v4, v5

    add-float/2addr v4, v1

    invoke-virtual {p1, v2, v3, v4, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    iget-object v2, p0, Lcom/twitter/library/widget/CroppableImageView;->g:Landroid/graphics/Bitmap;

    iget v3, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v3, v1

    iget v4, v6, Landroid/graphics/RectF;->bottom:F

    int-to-float v0, v0

    sub-float v0, v4, v0

    add-float/2addr v0, v1

    invoke-virtual {p1, v2, v3, v0, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v6, 0x0

    iget-boolean v2, p0, Lcom/twitter/library/widget/CroppableImageView;->q:Z

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/widget/CroppableImageView;->c()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    iget-boolean v2, p0, Lcom/twitter/library/widget/CroppableImageView;->o:Z

    if-nez v2, :cond_2

    invoke-super {p0, p1}, Lcom/twitter/library/widget/MultiTouchImageView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    packed-switch v2, :pswitch_data_0

    :cond_3
    invoke-super {p0, p1}, Lcom/twitter/library/widget/MultiTouchImageView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/twitter/library/widget/CroppableImageView;->m:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/PointF;->set(FF)V

    iget-object v1, p0, Lcom/twitter/library/widget/CroppableImageView;->m:Landroid/graphics/PointF;

    invoke-direct {p0, v1}, Lcom/twitter/library/widget/CroppableImageView;->a(Landroid/graphics/PointF;)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/widget/CroppableImageView;->r:I

    iget v1, p0, Lcom/twitter/library/widget/CroppableImageView;->r:I

    if-eqz v1, :cond_3

    goto :goto_0

    :pswitch_1
    iget v2, p0, Lcom/twitter/library/widget/CroppableImageView;->r:I

    if-eqz v2, :cond_3

    new-instance v2, Landroid/graphics/PointF;

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    invoke-direct {v2, v3, v1}, Landroid/graphics/PointF;-><init>(FF)V

    iget v1, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/twitter/library/widget/CroppableImageView;->b:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    iget-object v4, p0, Lcom/twitter/library/widget/CroppableImageView;->b:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    invoke-static {v1, v6, v6, v3, v4}, Lcom/twitter/library/widget/CroppableImageView;->a(FFFFF)F

    move-result v1

    iget v3, v2, Landroid/graphics/PointF;->y:F

    iget-object v4, p0, Lcom/twitter/library/widget/CroppableImageView;->b:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    iget-object v5, p0, Lcom/twitter/library/widget/CroppableImageView;->b:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    invoke-static {v3, v6, v6, v4, v5}, Lcom/twitter/library/widget/CroppableImageView;->a(FFFFF)F

    move-result v3

    invoke-virtual {v2, v1, v3}, Landroid/graphics/PointF;->set(FF)V

    iget v1, p0, Lcom/twitter/library/widget/CroppableImageView;->r:I

    iget v3, v2, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lcom/twitter/library/widget/CroppableImageView;->m:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    iget v4, v2, Landroid/graphics/PointF;->y:F

    iget-object v5, p0, Lcom/twitter/library/widget/CroppableImageView;->m:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    sub-float/2addr v4, v5

    invoke-direct {p0, v1, v3, v4}, Lcom/twitter/library/widget/CroppableImageView;->a(IFF)V

    iget-object v1, p0, Lcom/twitter/library/widget/CroppableImageView;->p:Lcom/twitter/library/widget/e;

    invoke-interface {v1}, Lcom/twitter/library/widget/e;->b()V

    iget-object v1, p0, Lcom/twitter/library/widget/CroppableImageView;->m:Landroid/graphics/PointF;

    invoke-virtual {v1, v2}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    goto :goto_0

    :pswitch_2
    iget v2, p0, Lcom/twitter/library/widget/CroppableImageView;->r:I

    if-eqz v2, :cond_3

    iput v1, p0, Lcom/twitter/library/widget/CroppableImageView;->r:I

    invoke-direct {p0, v0}, Lcom/twitter/library/widget/CroppableImageView;->a(Z)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setCropAspectRatio(F)V
    .locals 6

    const/4 v1, 0x1

    const/high16 v5, 0x40000000    # 2.0f

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/twitter/library/widget/CroppableImageView;->getImageRect()Landroid/graphics/RectF;

    move-result-object v2

    invoke-direct {p0}, Lcom/twitter/library/widget/CroppableImageView;->getPaddedViewRect()Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/graphics/RectF;->intersect(Landroid/graphics/RectF;)Z

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v0

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v3

    div-float/2addr v0, v3

    cmpl-float v0, v0, p1

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v0

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v3

    mul-float/2addr v3, p1

    sub-float/2addr v0, v3

    div-float/2addr v0, v5

    invoke-virtual {v2, v4, v0}, Landroid/graphics/RectF;->inset(FF)V

    :goto_1
    iget-object v0, p0, Lcom/twitter/library/widget/CroppableImageView;->c:Landroid/graphics/RectF;

    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    invoke-direct {p0, v1}, Lcom/twitter/library/widget/CroppableImageView;->a(Z)V

    invoke-virtual {p0}, Lcom/twitter/library/widget/CroppableImageView;->invalidate()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v0

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v3

    div-float/2addr v3, p1

    sub-float/2addr v0, v3

    div-float/2addr v0, v5

    invoke-virtual {v2, v0, v4}, Landroid/graphics/RectF;->inset(FF)V

    goto :goto_1
.end method

.method public setCropListener(Lcom/twitter/library/widget/e;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/widget/CroppableImageView;->p:Lcom/twitter/library/widget/e;

    return-void
.end method
