.class public final enum Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;
.super Ljava/lang/Enum;
.source "Twttr"


# static fields
.field public static final enum a:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

.field public static final enum b:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

.field public static final enum c:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

.field public static final enum d:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

.field public static final enum e:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

.field public static final enum f:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

.field public static final enum g:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

.field public static final enum h:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

.field private static final synthetic i:[Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;


# instance fields
.field private mIndex:I

.field private mThreshold:F


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    new-instance v0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    const-string/jumbo v1, "UNKNOWN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v5, v5, v2}, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;-><init>(Ljava/lang/String;IIF)V

    sput-object v0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->a:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    new-instance v0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    const-string/jumbo v1, "VERY_LOW"

    const/high16 v2, 0x41480000    # 12.5f

    invoke-direct {v0, v1, v6, v6, v2}, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;-><init>(Ljava/lang/String;IIF)V

    sput-object v0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->b:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    new-instance v0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    const-string/jumbo v1, "LOW"

    const/high16 v2, 0x41480000    # 12.5f

    invoke-direct {v0, v1, v7, v7, v2}, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;-><init>(Ljava/lang/String;IIF)V

    sput-object v0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->c:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    new-instance v0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    const-string/jumbo v1, "MEDIUM_LOW"

    const/high16 v2, 0x42480000    # 50.0f

    invoke-direct {v0, v1, v8, v8, v2}, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;-><init>(Ljava/lang/String;IIF)V

    sput-object v0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->d:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    new-instance v0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    const-string/jumbo v1, "MEDIUM"

    const/high16 v2, 0x42af0000    # 87.5f

    invoke-direct {v0, v1, v9, v9, v2}, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;-><init>(Ljava/lang/String;IIF)V

    sput-object v0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->e:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    new-instance v0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    const-string/jumbo v1, "MEDIUM_HIGH"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const/high16 v4, 0x437a0000    # 250.0f

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;-><init>(Ljava/lang/String;IIF)V

    sput-object v0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->f:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    new-instance v0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    const-string/jumbo v1, "HIGH"

    const/4 v2, 0x6

    const/4 v3, 0x6

    const v4, 0x449c4000    # 1250.0f

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;-><init>(Ljava/lang/String;IIF)V

    sput-object v0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->g:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    new-instance v0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    const-string/jumbo v1, "VERY_HIGH"

    const/4 v2, 0x7

    const/4 v3, 0x7

    const v4, 0x451c4000    # 2500.0f

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;-><init>(Ljava/lang/String;IIF)V

    sput-object v0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->h:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    sget-object v1, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->a:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    aput-object v1, v0, v5

    sget-object v1, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->b:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    aput-object v1, v0, v6

    sget-object v1, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->c:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    aput-object v1, v0, v7

    sget-object v1, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->d:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    aput-object v1, v0, v8

    sget-object v1, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->e:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->f:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->g:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->h:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->i:[Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIF)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->mIndex:I

    iput p4, p0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->mThreshold:F

    return-void
.end method

.method public static final a()I
    .locals 1

    sget-object v0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->h:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    invoke-virtual {v0}, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;
    .locals 1

    const-class v0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    return-object v0
.end method

.method public static values()[Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;
    .locals 1

    sget-object v0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->i:[Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    invoke-virtual {v0}, [Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    return-object v0
.end method


# virtual methods
.method public b()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->mIndex:I

    return v0
.end method

.method public c()F
    .locals 1

    iget v0, p0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->mThreshold:F

    return v0
.end method
