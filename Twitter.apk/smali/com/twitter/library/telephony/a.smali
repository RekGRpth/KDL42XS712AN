.class public Lcom/twitter/library/telephony/a;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Lcom/twitter/library/telephony/a;


# instance fields
.field private b:Landroid/content/Context;

.field private c:I

.field private final d:Lcom/twitter/library/telephony/c;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x63

    iput v0, p0, Lcom/twitter/library/telephony/a;->c:I

    new-instance v0, Lcom/twitter/library/telephony/c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/telephony/c;-><init>(Lcom/twitter/library/telephony/a;Lcom/twitter/library/telephony/b;)V

    iput-object v0, p0, Lcom/twitter/library/telephony/a;->d:Lcom/twitter/library/telephony/c;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/telephony/a;->b:Landroid/content/Context;

    iget-object v0, p0, Lcom/twitter/library/telephony/a;->b:Landroid/content/Context;

    const-string/jumbo v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/twitter/library/telephony/a;->d:Lcom/twitter/library/telephony/c;

    const/16 v2, 0x100

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/telephony/a;I)I
    .locals 0

    iput p1, p0, Lcom/twitter/library/telephony/a;->c:I

    return p1
.end method

.method public static declared-synchronized a()Lcom/twitter/library/telephony/a;
    .locals 3

    const-class v1, Lcom/twitter/library/telephony/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/telephony/a;->a:Lcom/twitter/library/telephony/a;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "Initialize must be called first from the main looper thread"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    sget-object v0, Lcom/twitter/library/telephony/a;->a:Lcom/twitter/library/telephony/a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    sget-object v0, Lcom/twitter/library/telephony/a;->a:Lcom/twitter/library/telephony/a;

    if-nez v0, :cond_1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "initialize() must be called from main thread"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lcom/twitter/library/telephony/a;

    invoke-direct {v0, p0}, Lcom/twitter/library/telephony/a;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/library/telephony/a;->a:Lcom/twitter/library/telephony/a;

    :cond_1
    return-void
.end method


# virtual methods
.method public b()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/telephony/a;->c:I

    return v0
.end method
