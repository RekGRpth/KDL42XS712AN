.class public Lcom/twitter/library/scribe/ScribeItem;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public A:Ljava/lang/String;

.field public B:Ljava/lang/String;

.field public C:Ljava/util/HashMap;

.field public D:J

.field public E:J

.field public F:Ljava/lang/String;

.field public G:J

.field public H:Ljava/lang/String;

.field public I:I

.field public J:I

.field public K:Ljava/lang/String;

.field public L:Ljava/lang/String;

.field public M:Ljava/lang/String;

.field public N:Lcom/twitter/library/scribe/f;

.field public a:J

.field public b:Ljava/lang/String;

.field public c:I

.field public d:J

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:I

.field public h:I

.field public i:I

.field public j:Z

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:I

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;

.field public s:Z

.field public t:Ljava/lang/String;

.field public u:Ljava/lang/String;

.field public v:Ljava/lang/String;

.field public w:Ljava/lang/String;

.field public x:Ljava/lang/String;

.field public y:I

.field public z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/library/scribe/b;

    invoke-direct {v0}, Lcom/twitter/library/scribe/b;-><init>()V

    sput-object v0, Lcom/twitter/library/scribe/ScribeItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, -0x1

    const/4 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v2, p0, Lcom/twitter/library/scribe/ScribeItem;->a:J

    iput v1, p0, Lcom/twitter/library/scribe/ScribeItem;->c:I

    iput-wide v2, p0, Lcom/twitter/library/scribe/ScribeItem;->d:J

    iput v1, p0, Lcom/twitter/library/scribe/ScribeItem;->g:I

    iput v1, p0, Lcom/twitter/library/scribe/ScribeItem;->h:I

    iput v1, p0, Lcom/twitter/library/scribe/ScribeItem;->i:I

    iput v1, p0, Lcom/twitter/library/scribe/ScribeItem;->m:I

    iput v1, p0, Lcom/twitter/library/scribe/ScribeItem;->y:I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->C:Ljava/util/HashMap;

    iput-wide v2, p0, Lcom/twitter/library/scribe/ScribeItem;->D:J

    iput-wide v2, p0, Lcom/twitter/library/scribe/ScribeItem;->E:J

    iput v1, p0, Lcom/twitter/library/scribe/ScribeItem;->I:I

    iput v1, p0, Lcom/twitter/library/scribe/ScribeItem;->J:I

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    const/4 v2, 0x0

    const-wide/16 v4, -0x1

    const/4 v1, 0x1

    const/4 v3, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v4, p0, Lcom/twitter/library/scribe/ScribeItem;->a:J

    iput v3, p0, Lcom/twitter/library/scribe/ScribeItem;->c:I

    iput-wide v4, p0, Lcom/twitter/library/scribe/ScribeItem;->d:J

    iput v3, p0, Lcom/twitter/library/scribe/ScribeItem;->g:I

    iput v3, p0, Lcom/twitter/library/scribe/ScribeItem;->h:I

    iput v3, p0, Lcom/twitter/library/scribe/ScribeItem;->i:I

    iput v3, p0, Lcom/twitter/library/scribe/ScribeItem;->m:I

    iput v3, p0, Lcom/twitter/library/scribe/ScribeItem;->y:I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->C:Ljava/util/HashMap;

    iput-wide v4, p0, Lcom/twitter/library/scribe/ScribeItem;->D:J

    iput-wide v4, p0, Lcom/twitter/library/scribe/ScribeItem;->E:J

    iput v3, p0, Lcom/twitter/library/scribe/ScribeItem;->I:I

    iput v3, p0, Lcom/twitter/library/scribe/ScribeItem;->J:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/twitter/library/scribe/ScribeItem;->a:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/scribe/ScribeItem;->c:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/twitter/library/scribe/ScribeItem;->d:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->e:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->f:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/scribe/ScribeItem;->g:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/scribe/ScribeItem;->h:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/scribe/ScribeItem;->i:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/library/scribe/ScribeItem;->j:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->k:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->l:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/scribe/ScribeItem;->m:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->n:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->o:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->p:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->q:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->r:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/twitter/library/scribe/ScribeItem;->s:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->t:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->u:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->v:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->w:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->x:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/scribe/ScribeItem;->y:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->A:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->B:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->z:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    :goto_2
    if-ge v2, v0, :cond_2

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeItem;->C:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_0
    move v0, v2

    goto/16 :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/scribe/ScribeItem;->D:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/scribe/ScribeItem;->E:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/scribe/ScribeItem;->I:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->F:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->H:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/scribe/ScribeItem;->G:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/scribe/ScribeItem;->J:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->K:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->L:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->M:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/twitter/library/scribe/f;

    invoke-direct {v0, p1}, Lcom/twitter/library/scribe/f;-><init>(Landroid/os/Parcel;)V

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->N:Lcom/twitter/library/scribe/f;

    :cond_3
    return-void
.end method

.method public static a(IJJ)Lcom/twitter/library/scribe/ScribeItem;
    .locals 5

    const-wide/16 v3, 0x0

    const/4 v0, 0x0

    new-instance v2, Lcom/twitter/library/scribe/ScribeItem;

    invoke-direct {v2}, Lcom/twitter/library/scribe/ScribeItem;-><init>()V

    packed-switch p0, :pswitch_data_0

    const/4 v1, 0x0

    const/4 v0, -0x1

    :goto_0
    if-eqz v1, :cond_0

    iput-object v1, v2, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    :cond_0
    cmp-long v1, p1, v3

    if-lez v1, :cond_1

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/scribe/ScribeItem;->z:Ljava/lang/String;

    :cond_1
    cmp-long v1, p3, v3

    if-lez v1, :cond_2

    iput-wide p3, v2, Lcom/twitter/library/scribe/ScribeItem;->a:J

    :cond_2
    if-lez v0, :cond_3

    iput v0, v2, Lcom/twitter/library/scribe/ScribeItem;->c:I

    :cond_3
    sget-object v0, Lcom/twitter/library/provider/x;->b:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/twitter/library/scribe/ScribeItem;->x:Ljava/lang/String;

    return-object v2

    :pswitch_0
    const-string/jumbo v1, "magic_rec_tweet"

    goto :goto_0

    :pswitch_1
    const-string/jumbo v1, "magic_rec_tweet"

    goto :goto_0

    :pswitch_2
    const-string/jumbo v1, "magic_rec_user"

    const/4 v0, 0x3

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x12
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;
    .locals 6

    const/4 v5, -0x1

    move-wide v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/scribe/ScribeItem;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Ljava/lang/String;I)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    return-object v0
.end method

.method public static a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Ljava/lang/String;I)Lcom/twitter/library/scribe/ScribeItem;
    .locals 2

    new-instance v0, Lcom/twitter/library/scribe/ScribeItem;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeItem;-><init>()V

    iput-wide p0, v0, Lcom/twitter/library/scribe/ScribeItem;->a:J

    const/4 v1, 0x3

    iput v1, v0, Lcom/twitter/library/scribe/ScribeItem;->c:I

    iput-object p4, v0, Lcom/twitter/library/scribe/ScribeItem;->v:Ljava/lang/String;

    iput p5, v0, Lcom/twitter/library/scribe/ScribeItem;->g:I

    if-eqz p2, :cond_0

    iget-object v1, p2, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/library/scribe/ScribeItem;->e:Ljava/lang/String;

    iget-object v1, p2, Lcom/twitter/library/api/PromotedContent;->disclosureType:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/library/scribe/ScribeItem;->f:Ljava/lang/String;

    :cond_0
    iput-object p3, v0, Lcom/twitter/library/scribe/ScribeItem;->k:Ljava/lang/String;

    return-object v0
.end method

.method public static a(JLjava/lang/String;II)Lcom/twitter/library/scribe/ScribeItem;
    .locals 1

    new-instance v0, Lcom/twitter/library/scribe/ScribeItem;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeItem;-><init>()V

    iput-wide p0, v0, Lcom/twitter/library/scribe/ScribeItem;->a:J

    iput-object p2, v0, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    iput p3, v0, Lcom/twitter/library/scribe/ScribeItem;->c:I

    iput p4, v0, Lcom/twitter/library/scribe/ScribeItem;->g:I

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/provider/ParcelableTweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;
    .locals 8

    const/4 v6, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v7, -0x1

    new-instance v4, Lcom/twitter/library/scribe/ScribeItem;

    invoke-direct {v4}, Lcom/twitter/library/scribe/ScribeItem;-><init>()V

    invoke-interface {p1}, Lcom/twitter/library/provider/ParcelableTweet;->d()J

    move-result-wide v0

    iput-wide v0, v4, Lcom/twitter/library/scribe/ScribeItem;->a:J

    iput v2, v4, Lcom/twitter/library/scribe/ScribeItem;->c:I

    iput-object p3, v4, Lcom/twitter/library/scribe/ScribeItem;->v:Ljava/lang/String;

    new-instance v0, Lcom/twitter/library/scribe/f;

    invoke-direct {v0, p1}, Lcom/twitter/library/scribe/f;-><init>(Lcom/twitter/library/provider/ParcelableTweet;)V

    iput-object v0, v4, Lcom/twitter/library/scribe/ScribeItem;->N:Lcom/twitter/library/scribe/f;

    invoke-interface {p1}, Lcom/twitter/library/provider/ParcelableTweet;->n()Lcom/twitter/library/api/TwitterStatusCard;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v1, v0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    if-eqz v1, :cond_4

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    const/4 v1, 0x6

    iput v1, v4, Lcom/twitter/library/scribe/ScribeItem;->i:I

    invoke-static {p0}, Lcom/twitter/library/network/aa;->a(Landroid/content/Context;)Lcom/twitter/library/network/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/network/aa;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v4, Lcom/twitter/library/scribe/ScribeItem;->o:Ljava/lang/String;

    iget-object v1, v0, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    iput-object v1, v4, Lcom/twitter/library/scribe/ScribeItem;->l:Ljava/lang/String;

    iget-object v1, v0, Lcom/twitter/library/card/instance/CardInstanceData;->audienceName:Ljava/lang/String;

    iput-object v1, v4, Lcom/twitter/library/scribe/ScribeItem;->p:Ljava/lang/String;

    iget-object v1, v0, Lcom/twitter/library/card/instance/CardInstanceData;->audienceBucket:Ljava/lang/String;

    iput-object v1, v4, Lcom/twitter/library/scribe/ScribeItem;->q:Ljava/lang/String;

    iget-object v1, v0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardName:Ljava/lang/String;

    iput-object v1, v4, Lcom/twitter/library/scribe/ScribeItem;->r:Ljava/lang/String;

    iget-object v1, v0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardAudienceName:Ljava/lang/String;

    iput-object v1, v4, Lcom/twitter/library/scribe/ScribeItem;->t:Ljava/lang/String;

    iget-object v1, v0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardAudienceBucket:Ljava/lang/String;

    iput-object v1, v4, Lcom/twitter/library/scribe/ScribeItem;->u:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/twitter/library/card/instance/CardInstanceData;->a()Z

    move-result v1

    iput-boolean v1, v4, Lcom/twitter/library/scribe/ScribeItem;->s:Z

    iget-object v1, v0, Lcom/twitter/library/card/instance/CardInstanceData;->bindingValues:Ljava/util/HashMap;

    if-eqz v1, :cond_5

    const-string/jumbo v0, "app_id"

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/instance/BindingValue;

    invoke-static {v0}, Lcom/twitter/library/card/instance/BindingValue;->a(Lcom/twitter/library/card/instance/BindingValue;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v4, Lcom/twitter/library/scribe/ScribeItem;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    if-eqz p0, :cond_0

    invoke-static {p0, v0}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iput v6, v4, Lcom/twitter/library/scribe/ScribeItem;->m:I

    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/instance/BindingValue;

    iget-object v1, v0, Lcom/twitter/library/card/instance/BindingValue;->scribeKey:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/twitter/library/card/instance/BindingValue;->value:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, v4, Lcom/twitter/library/scribe/ScribeItem;->C:Ljava/util/HashMap;

    iget-object v6, v0, Lcom/twitter/library/card/instance/BindingValue;->scribeKey:Ljava/lang/String;

    iget-object v0, v0, Lcom/twitter/library/card/instance/BindingValue;->value:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    iput v3, v4, Lcom/twitter/library/scribe/ScribeItem;->m:I

    goto :goto_0

    :cond_3
    iget-object v1, v0, Lcom/twitter/library/card/instance/BindingValue;->value:Ljava/lang/Object;

    instance-of v1, v1, Lcom/twitter/library/card/instance/UserValue;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/twitter/library/card/instance/BindingValue;->value:Ljava/lang/Object;

    check-cast v1, Lcom/twitter/library/card/instance/UserValue;

    iget-object v1, v1, Lcom/twitter/library/card/instance/UserValue;->id:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v6, v4, Lcom/twitter/library/scribe/ScribeItem;->C:Ljava/util/HashMap;

    iget-object v0, v0, Lcom/twitter/library/card/instance/BindingValue;->scribeKey:Ljava/lang/String;

    invoke-virtual {v6, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_4
    invoke-interface {p1}, Lcom/twitter/library/provider/ParcelableTweet;->q()Z

    move-result v0

    if-eqz v0, :cond_9

    iput v6, v4, Lcom/twitter/library/scribe/ScribeItem;->i:I

    :cond_5
    :goto_2
    invoke-interface {p1}, Lcom/twitter/library/provider/ParcelableTweet;->u()Z

    move-result v0

    if-nez v0, :cond_6

    iget v0, v4, Lcom/twitter/library/scribe/ScribeItem;->i:I

    if-eq v0, v7, :cond_d

    if-eqz p2, :cond_d

    invoke-virtual {p2}, Lcom/twitter/library/scribe/ScribeAssociation;->a()Z

    move-result v0

    if-eqz v0, :cond_d

    :cond_6
    move v0, v3

    :goto_3
    iput-boolean v0, v4, Lcom/twitter/library/scribe/ScribeItem;->j:Z

    invoke-interface {p1}, Lcom/twitter/library/provider/ParcelableTweet;->e()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {p1}, Lcom/twitter/library/provider/ParcelableTweet;->c()J

    move-result-wide v0

    iput-wide v0, v4, Lcom/twitter/library/scribe/ScribeItem;->d:J

    :cond_7
    invoke-interface {p1}, Lcom/twitter/library/provider/ParcelableTweet;->v()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {p1}, Lcom/twitter/library/provider/ParcelableTweet;->m()Lcom/twitter/library/api/PromotedContent;

    move-result-object v0

    iget-object v1, v0, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    iput-object v1, v4, Lcom/twitter/library/scribe/ScribeItem;->e:Ljava/lang/String;

    iget-object v0, v0, Lcom/twitter/library/api/PromotedContent;->disclosureType:Ljava/lang/String;

    iput-object v0, v4, Lcom/twitter/library/scribe/ScribeItem;->f:Ljava/lang/String;

    :cond_8
    return-object v4

    :cond_9
    invoke-interface {p1}, Lcom/twitter/library/provider/ParcelableTweet;->r()Z

    move-result v0

    if-eqz v0, :cond_a

    const/4 v0, 0x3

    iput v0, v4, Lcom/twitter/library/scribe/ScribeItem;->i:I

    goto :goto_2

    :cond_a
    invoke-interface {p1}, Lcom/twitter/library/provider/ParcelableTweet;->s()Z

    move-result v0

    if-eqz v0, :cond_b

    const/4 v0, 0x4

    iput v0, v4, Lcom/twitter/library/scribe/ScribeItem;->i:I

    goto :goto_2

    :cond_b
    invoke-interface {p1}, Lcom/twitter/library/provider/ParcelableTweet;->t()Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x5

    iput v0, v4, Lcom/twitter/library/scribe/ScribeItem;->i:I

    goto :goto_2

    :cond_c
    iput v7, v4, Lcom/twitter/library/scribe/ScribeItem;->i:I

    goto :goto_2

    :cond_d
    move v0, v2

    goto :goto_3
.end method

.method public static a(Lcom/twitter/library/api/DiscoverStoryMetadata;)Lcom/twitter/library/scribe/ScribeItem;
    .locals 2

    new-instance v0, Lcom/twitter/library/scribe/ScribeItem;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeItem;-><init>()V

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/api/DiscoverStoryMetadata;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/scribe/ScribeItem;->B:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/library/api/DiscoverStoryMetadata;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/scribe/ScribeItem;->A:Ljava/lang/String;

    :cond_0
    return-object v0
.end method

.method public static a(Lcom/twitter/library/api/TimelineScribeContent;Ljava/lang/String;JILjava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;
    .locals 3

    new-instance v0, Lcom/twitter/library/scribe/ScribeItem;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeItem;-><init>()V

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/api/TimelineScribeContent;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/scribe/ScribeItem;->z:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/library/api/TimelineScribeContent;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/scribe/ScribeItem;->B:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/library/api/TimelineScribeContent;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/scribe/ScribeItem;->A:Ljava/lang/String;

    :cond_0
    if-eqz p1, :cond_1

    iput-object p1, v0, Lcom/twitter/library/scribe/ScribeItem;->x:Ljava/lang/String;

    :cond_1
    iput p4, v0, Lcom/twitter/library/scribe/ScribeItem;->c:I

    if-eqz p5, :cond_2

    iput-object p5, v0, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    :cond_2
    const-wide/16 v1, 0x0

    cmp-long v1, p2, v1

    if-lez v1, :cond_3

    iput-wide p2, v0, Lcom/twitter/library/scribe/ScribeItem;->a:J

    :cond_3
    return-object v0
.end method

.method public static a(Lcom/twitter/library/api/af;)Lcom/twitter/library/scribe/ScribeItem;
    .locals 1

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/api/af;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/library/scribe/ScribeItem;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;
    .locals 2

    new-instance v0, Lcom/twitter/library/scribe/ScribeItem;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeItem;-><init>()V

    iput-object p0, v0, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    const/4 v1, 0x3

    iput v1, v0, Lcom/twitter/library/scribe/ScribeItem;->c:I

    return-object v0
.end method

.method public static a(Ljava/lang/String;I)Lcom/twitter/library/scribe/ScribeItem;
    .locals 2

    new-instance v0, Lcom/twitter/library/scribe/ScribeItem;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeItem;-><init>()V

    const/16 v1, 0x11

    iput v1, v0, Lcom/twitter/library/scribe/ScribeItem;->c:I

    iput-object p0, v0, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    const/4 v1, -0x1

    if-eq p1, v1, :cond_0

    add-int/lit8 v1, p1, 0x1

    iput v1, v0, Lcom/twitter/library/scribe/ScribeItem;->g:I

    :cond_0
    return-object v0
.end method

.method public static a(Ljava/lang/String;II)Lcom/twitter/library/scribe/ScribeItem;
    .locals 2

    const-wide/16 v0, -0x1

    invoke-static {v0, v1, p0, p1, p2}, Lcom/twitter/library/scribe/ScribeItem;->a(JLjava/lang/String;II)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;
    .locals 2

    new-instance v0, Lcom/twitter/library/scribe/ScribeItem;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeItem;-><init>()V

    iput-object p0, v0, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    const/16 v1, 0xc

    iput v1, v0, Lcom/twitter/library/scribe/ScribeItem;->c:I

    iput-object p1, v0, Lcom/twitter/library/scribe/ScribeItem;->v:Ljava/lang/String;

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;III)Lcom/twitter/library/scribe/ScribeItem;
    .locals 1

    new-instance v0, Lcom/twitter/library/scribe/ScribeItem;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeItem;-><init>()V

    iput-object p0, v0, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    iput-object p1, v0, Lcom/twitter/library/scribe/ScribeItem;->w:Ljava/lang/String;

    iput p2, v0, Lcom/twitter/library/scribe/ScribeItem;->c:I

    iput p3, v0, Lcom/twitter/library/scribe/ScribeItem;->g:I

    iput p4, v0, Lcom/twitter/library/scribe/ScribeItem;->h:I

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;
    .locals 1

    new-instance v0, Lcom/twitter/library/scribe/ScribeItem;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeItem;-><init>()V

    iput-object p0, v0, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    return-object v0
.end method

.method public static c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;
    .locals 1

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/twitter/library/scribe/ScribeItem;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeItem;-><init>()V

    iput-object p0, v0, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public static d(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;
    .locals 8

    const/4 v2, 0x0

    const/4 v3, 0x4

    const/4 v7, 0x1

    const/4 v1, 0x0

    const/4 v4, -0x1

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v2

    :cond_0
    const-string/jumbo v0, ","

    invoke-virtual {p0, v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v5

    array-length v0, v5

    if-ge v0, v3, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Expecting: \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\' to contain "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " or more: \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\'."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    aget-object v0, v5, v1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    aget-object v0, v5, v1

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    :goto_1
    array-length v3, v5

    add-int/lit8 v6, v3, -0x4

    aget-object v3, v5, v7

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    new-instance v3, Ljava/lang/StringBuilder;

    aget-object v2, v5, v7

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x2

    :goto_2
    add-int/lit8 v7, v6, 0x1

    if-gt v2, v7, :cond_3

    const-string/jumbo v7, ","

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v7, v5, v2

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    const-wide/16 v0, -0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_4
    add-int/lit8 v3, v6, 0x2

    aget-object v3, v5, v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    add-int/lit8 v3, v6, 0x2

    aget-object v3, v5, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    :goto_3
    add-int/lit8 v7, v6, 0x3

    aget-object v7, v5, v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    add-int/lit8 v4, v6, 0x3

    aget-object v4, v5, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    :cond_5
    invoke-static {v0, v1, v2, v3, v4}, Lcom/twitter/library/scribe/ScribeItem;->a(JLjava/lang/String;II)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v2

    goto/16 :goto_0

    :cond_6
    move v3, v4

    goto :goto_3
.end method


# virtual methods
.method public a(Lcom/fasterxml/jackson/core/JsonGenerator;)V
    .locals 8

    const/4 v2, 0x1

    const-wide/16 v6, -0x1

    const/4 v5, -0x1

    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->c()V

    iget-wide v0, p0, Lcom/twitter/library/scribe/ScribeItem;->a:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_0

    const-string/jumbo v0, "id"

    iget-wide v3, p0, Lcom/twitter/library/scribe/ScribeItem;->a:J

    invoke-virtual {p1, v0, v3, v4}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string/jumbo v0, "name"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget v0, p0, Lcom/twitter/library/scribe/ScribeItem;->c:I

    if-eq v0, v5, :cond_2

    const-string/jumbo v0, "item_type"

    iget v1, p0, Lcom/twitter/library/scribe/ScribeItem;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    :cond_2
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    const-string/jumbo v0, "promoted_id"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeItem;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    const-string/jumbo v0, "disclosure_type"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeItem;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-wide v0, p0, Lcom/twitter/library/scribe/ScribeItem;->d:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_5

    const-string/jumbo v0, "retweeting_tweet_id"

    iget-wide v3, p0, Lcom/twitter/library/scribe/ScribeItem;->d:J

    invoke-virtual {p1, v0, v3, v4}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    :cond_5
    iget v0, p0, Lcom/twitter/library/scribe/ScribeItem;->g:I

    if-eq v0, v5, :cond_6

    const-string/jumbo v0, "position"

    iget v1, p0, Lcom/twitter/library/scribe/ScribeItem;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    :cond_6
    iget v0, p0, Lcom/twitter/library/scribe/ScribeItem;->h:I

    if-eq v0, v5, :cond_7

    const-string/jumbo v0, "cursor"

    iget v1, p0, Lcom/twitter/library/scribe/ScribeItem;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    :cond_7
    iget v0, p0, Lcom/twitter/library/scribe/ScribeItem;->i:I

    if-eq v0, v5, :cond_8

    const-string/jumbo v0, "card_type"

    iget v1, p0, Lcom/twitter/library/scribe/ScribeItem;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    const-string/jumbo v0, "pre_expanded"

    iget-boolean v1, p0, Lcom/twitter/library/scribe/ScribeItem;->j:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    const-string/jumbo v0, "forward_card_pre_expanded"

    iget-boolean v1, p0, Lcom/twitter/library/scribe/ScribeItem;->s:Z

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    :cond_8
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->k:Ljava/lang/String;

    if-eqz v0, :cond_9

    const-string/jumbo v0, "token"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeItem;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->l:Ljava/lang/String;

    if-eqz v0, :cond_a

    const-string/jumbo v0, "card_name"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeItem;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    iget v0, p0, Lcom/twitter/library/scribe/ScribeItem;->m:I

    if-eq v0, v5, :cond_b

    const-string/jumbo v0, "publisher_app_install_status"

    iget v1, p0, Lcom/twitter/library/scribe/ScribeItem;->m:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    :cond_b
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->n:Ljava/lang/String;

    if-eqz v0, :cond_c

    const-string/jumbo v0, "publisher_app_id"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeItem;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->o:Ljava/lang/String;

    if-eqz v0, :cond_d

    const-string/jumbo v0, "card_platform_key"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeItem;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->p:Ljava/lang/String;

    if-eqz v0, :cond_e

    const-string/jumbo v0, "audience_name"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeItem;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->q:Ljava/lang/String;

    if-eqz v0, :cond_e

    const-string/jumbo v0, "audience_bucket"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeItem;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_e
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->r:Ljava/lang/String;

    if-eqz v0, :cond_f

    const-string/jumbo v0, "forward_card_name"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeItem;->r:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_f
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->t:Ljava/lang/String;

    if-eqz v0, :cond_10

    const-string/jumbo v0, "forward_card_audience_name"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeItem;->t:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->u:Ljava/lang/String;

    if-eqz v0, :cond_10

    const-string/jumbo v0, "forward_card_audience_bucket"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeItem;->u:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_10
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->v:Ljava/lang/String;

    if-eqz v0, :cond_11

    const-string/jumbo v0, "description"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeItem;->v:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_11
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->w:Ljava/lang/String;

    if-eqz v0, :cond_12

    const-string/jumbo v0, "item_query"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeItem;->w:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_12
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->x:Ljava/lang/String;

    if-eqz v0, :cond_13

    const-string/jumbo v0, "entity_type"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeItem;->x:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_13
    iget v0, p0, Lcom/twitter/library/scribe/ScribeItem;->y:I

    if-eq v0, v5, :cond_14

    const-string/jumbo v0, "tweet_count"

    iget v1, p0, Lcom/twitter/library/scribe/ScribeItem;->y:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    :cond_14
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->B:Ljava/lang/String;

    if-eqz v0, :cond_15

    const-string/jumbo v0, "story_source"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeItem;->B:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_15
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->A:Ljava/lang/String;

    if-eqz v0, :cond_16

    const-string/jumbo v0, "story_type"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeItem;->A:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_16
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->z:Ljava/lang/String;

    if-eqz v0, :cond_17

    const-string/jumbo v0, "impression_id"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeItem;->z:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_17
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->C:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_18
    iget-wide v0, p0, Lcom/twitter/library/scribe/ScribeItem;->D:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_19

    const-string/jumbo v0, "visibility_start"

    iget-wide v3, p0, Lcom/twitter/library/scribe/ScribeItem;->D:J

    invoke-virtual {p1, v0, v3, v4}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    :cond_19
    iget-wide v0, p0, Lcom/twitter/library/scribe/ScribeItem;->E:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_1a

    const-string/jumbo v0, "visibility_end"

    iget-wide v3, p0, Lcom/twitter/library/scribe/ScribeItem;->E:J

    invoke-virtual {p1, v0, v3, v4}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    :cond_1a
    iget v0, p0, Lcom/twitter/library/scribe/ScribeItem;->I:I

    if-eq v0, v5, :cond_1b

    const-string/jumbo v0, "video_index"

    iget v1, p0, Lcom/twitter/library/scribe/ScribeItem;->I:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    :cond_1b
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->F:Ljava/lang/String;

    if-eqz v0, :cond_1c

    const-string/jumbo v0, "video_uuid"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeItem;->F:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1c
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->H:Ljava/lang/String;

    if-eqz v0, :cond_1d

    const-string/jumbo v0, "video_type"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeItem;->H:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1d
    iget-wide v0, p0, Lcom/twitter/library/scribe/ScribeItem;->G:J

    const-wide/16 v3, 0x0

    cmp-long v0, v0, v3

    if-eqz v0, :cond_1e

    const-string/jumbo v0, "video_owner_id"

    iget-wide v3, p0, Lcom/twitter/library/scribe/ScribeItem;->G:J

    invoke-virtual {p1, v0, v3, v4}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    :cond_1e
    iget v0, p0, Lcom/twitter/library/scribe/ScribeItem;->J:I

    if-eq v0, v5, :cond_1f

    const-string/jumbo v1, "video_is_muted"

    iget v0, p0, Lcom/twitter/library/scribe/ScribeItem;->J:I

    if-ne v0, v2, :cond_24

    move v0, v2

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Z)V

    :cond_1f
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->K:Ljava/lang/String;

    if-eqz v0, :cond_20

    const-string/jumbo v0, "error_message"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeItem;->K:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_20
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->L:Ljava/lang/String;

    if-eqz v0, :cond_21

    const-string/jumbo v0, "content_id"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeItem;->L:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_21
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->M:Ljava/lang/String;

    if-eqz v0, :cond_22

    const-string/jumbo v0, "playlist_url"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeItem;->M:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_22
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->N:Lcom/twitter/library/scribe/f;

    if-eqz v0, :cond_23

    const-string/jumbo v0, "mediaDetails"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->N:Lcom/twitter/library/scribe/f;

    invoke-virtual {v0, p1}, Lcom/twitter/library/scribe/f;->a(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    :cond_23
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    return-void

    :cond_24
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    const/4 v5, -0x1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v1, p0, Lcom/twitter/library/scribe/ScribeItem;->a:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    iget-wide v1, p0, Lcom/twitter/library/scribe/ScribeItem;->a:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    :cond_0
    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/twitter/library/scribe/ScribeItem;->c:I

    if-eq v1, v5, :cond_2

    iget v1, p0, Lcom/twitter/library/scribe/ScribeItem;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_2
    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/twitter/library/scribe/ScribeItem;->g:I

    if-eq v1, v5, :cond_3

    iget v1, p0, Lcom/twitter/library/scribe/ScribeItem;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-wide v0, p0, Lcom/twitter/library/scribe/ScribeItem;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/twitter/library/scribe/ScribeItem;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-wide v0, p0, Lcom/twitter/library/scribe/ScribeItem;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/twitter/library/scribe/ScribeItem;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/scribe/ScribeItem;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/scribe/ScribeItem;->i:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/twitter/library/scribe/ScribeItem;->j:Z

    if-eqz v0, :cond_0

    move v0, v2

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/twitter/library/scribe/ScribeItem;->m:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->r:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/twitter/library/scribe/ScribeItem;->s:Z

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->t:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->u:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->v:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->w:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->x:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/twitter/library/scribe/ScribeItem;->y:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->A:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->B:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->z:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->C:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->C:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_2

    :cond_0
    move v0, v3

    goto/16 :goto_0

    :cond_1
    move v0, v3

    goto :goto_1

    :cond_2
    iget-wide v0, p0, Lcom/twitter/library/scribe/ScribeItem;->D:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Lcom/twitter/library/scribe/ScribeItem;->E:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget v0, p0, Lcom/twitter/library/scribe/ScribeItem;->I:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->F:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->H:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/twitter/library/scribe/ScribeItem;->G:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget v0, p0, Lcom/twitter/library/scribe/ScribeItem;->J:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->K:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->L:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->M:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->N:Lcom/twitter/library/scribe/f;

    if-eqz v0, :cond_3

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeByte(B)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeItem;->N:Lcom/twitter/library/scribe/f;

    invoke-virtual {v0, p1}, Lcom/twitter/library/scribe/f;->a(Landroid/os/Parcel;)V

    :goto_3
    return-void

    :cond_3
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeByte(B)V

    goto :goto_3
.end method
