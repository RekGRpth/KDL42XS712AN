.class Lcom/twitter/library/scribe/g;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field a:Ljava/lang/String;

.field b:I

.field c:Ljava/lang/String;

.field d:Z

.field e:Ljava/lang/Long;

.field f:Ljava/lang/Long;

.field private g:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Long;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/scribe/g;->a:Ljava/lang/String;

    iput p2, p0, Lcom/twitter/library/scribe/g;->b:I

    iput-object p3, p0, Lcom/twitter/library/scribe/g;->c:Ljava/lang/String;

    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/g;->f:Ljava/lang/Long;

    iput-object p4, p0, Lcom/twitter/library/scribe/g;->e:Ljava/lang/Long;

    invoke-static {}, Lcom/twitter/library/scribe/ScribeService;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/scribe/g;->d:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/scribe/g;->g:Z

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/scribe/g;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/scribe/g;->g:Z

    return v0
.end method


# virtual methods
.method public a(ILjava/lang/String;J)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/scribe/g;->g:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/library/scribe/g;->b:I

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/scribe/g;->c:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/library/scribe/g;->g:Z

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/g;->e:Ljava/lang/Long;

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(J)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/scribe/g;->g:Z

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/g;->f:Ljava/lang/Long;

    return-void
.end method
