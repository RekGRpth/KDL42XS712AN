.class public Lcom/twitter/library/scribe/f;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:I


# direct methods
.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/scribe/f;->a:I

    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/provider/ParcelableTweet;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/twitter/library/util/s;->a(Lcom/twitter/library/provider/ParcelableTweet;)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/scribe/f;->a:I

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)V
    .locals 1

    iget v0, p0, Lcom/twitter/library/scribe/f;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public a(Lcom/fasterxml/jackson/core/JsonGenerator;)V
    .locals 1

    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->c()V

    const-string/jumbo v0, "photoCount"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;)V

    iget v0, p0, Lcom/twitter/library/scribe/f;->a:I

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->b(I)V

    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    return-void
.end method
