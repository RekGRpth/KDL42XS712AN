.class public Lcom/twitter/library/scribe/ScribeLog;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static a:Ljava/lang/String;

.field private static final b:[I

.field private static final c:[Ljava/lang/String;

.field private static final d:[F

.field private static final e:[Ljava/lang/String;


# instance fields
.field private A:I

.field private B:[B

.field private C:Ljava/lang/String;

.field private D:I

.field private E:Ljava/lang/String;

.field private F:Ljava/lang/String;

.field private G:I

.field private H:I

.field private I:Ljava/lang/String;

.field private J:Ljava/lang/String;

.field private K:Ljava/lang/String;

.field private L:Ljava/lang/String;

.field private M:Ljava/lang/String;

.field private N:Ljava/lang/String;

.field private O:Ljava/lang/String;

.field private P:Ljava/lang/String;

.field private Q:Ljava/lang/String;

.field private R:I

.field private S:I

.field private T:Ljava/util/ArrayList;

.field private U:Ljava/util/ArrayList;

.field private V:Ljava/util/HashMap;

.field private W:I

.field private X:Ljava/lang/String;

.field private Y:Ljava/lang/String;

.field private Z:Ljava/lang/String;

.field private aa:Ljava/lang/String;

.field private ab:Ljava/lang/String;

.field private ac:Ljava/lang/String;

.field private final f:J

.field private g:J

.field private h:I

.field private i:I

.field private j:Ljava/lang/String;

.field private k:I

.field private l:I

.field private m:Ljava/lang/String;

.field private final n:J

.field private o:I

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Lcom/twitter/library/scribe/ScribeLog$SearchDetails;

.field private v:I

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x6

    new-instance v0, Lcom/twitter/library/scribe/c;

    invoke-direct {v0}, Lcom/twitter/library/scribe/c;-><init>()V

    sput-object v0, Lcom/twitter/library/scribe/ScribeLog;->CREATOR:Landroid/os/Parcelable$Creator;

    const-string/jumbo v0, "android"

    sput-object v0, Lcom/twitter/library/scribe/ScribeLog;->a:Ljava/lang/String;

    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/twitter/library/scribe/ScribeLog;->b:[I

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "48x48"

    aput-object v1, v0, v4

    const-string/jumbo v1, "96x96"

    aput-object v1, v0, v5

    const-string/jumbo v1, "192x192"

    aput-object v1, v0, v6

    const-string/jumbo v1, "384x384"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string/jumbo v2, "576x576"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "768x768"

    aput-object v2, v0, v1

    const-string/jumbo v1, "960x960"

    aput-object v1, v0, v3

    const/4 v1, 0x7

    const-string/jumbo v2, "1152x1152"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "1152x1152+"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/library/scribe/ScribeLog;->c:[Ljava/lang/String;

    new-array v0, v3, [F

    fill-array-data v0, :array_1

    sput-object v0, Lcom/twitter/library/scribe/ScribeLog;->d:[F

    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, "0-1mb"

    aput-object v1, v0, v4

    const-string/jumbo v1, "1-2mb"

    aput-object v1, v0, v5

    const-string/jumbo v1, "2-3mb"

    aput-object v1, v0, v6

    const-string/jumbo v1, "3-4mb"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string/jumbo v2, "4-5mb"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "5mb+"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/library/scribe/ScribeLog;->e:[Ljava/lang/String;

    return-void

    :array_0
    .array-data 4
        0x900
        0x2400
        0x9000
        0x24000
        0x51000
        0x90000
        0xe1000
        0x144000
        0x7fffffff
    .end array-data

    :array_1
    .array-data 4
        0x44800000    # 1024.0f
        0x45000000    # 2048.0f
        0x45400000    # 3072.0f
        0x45800000    # 4096.0f
        0x45a00000    # 5120.0f
        0x4f000000
    .end array-data
.end method

.method public constructor <init>(J)V
    .locals 3

    const/4 v2, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/library/scribe/ScribeLog;->g:J

    iput v2, p0, Lcom/twitter/library/scribe/ScribeLog;->h:I

    iput v2, p0, Lcom/twitter/library/scribe/ScribeLog;->i:I

    iput v2, p0, Lcom/twitter/library/scribe/ScribeLog;->v:I

    iput v2, p0, Lcom/twitter/library/scribe/ScribeLog;->G:I

    const v0, 0x7fffffff

    iput v0, p0, Lcom/twitter/library/scribe/ScribeLog;->H:I

    iput v2, p0, Lcom/twitter/library/scribe/ScribeLog;->R:I

    iput v2, p0, Lcom/twitter/library/scribe/ScribeLog;->S:I

    iput v2, p0, Lcom/twitter/library/scribe/ScribeLog;->W:I

    iput-wide p1, p0, Lcom/twitter/library/scribe/ScribeLog;->n:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/scribe/ScribeLog;->f:J

    iput v2, p0, Lcom/twitter/library/scribe/ScribeLog;->o:I

    const-string/jumbo v0, "client_event"

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->w:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    const/4 v2, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/library/scribe/ScribeLog;->g:J

    iput v2, p0, Lcom/twitter/library/scribe/ScribeLog;->h:I

    iput v2, p0, Lcom/twitter/library/scribe/ScribeLog;->i:I

    iput v2, p0, Lcom/twitter/library/scribe/ScribeLog;->v:I

    iput v2, p0, Lcom/twitter/library/scribe/ScribeLog;->G:I

    const v0, 0x7fffffff

    iput v0, p0, Lcom/twitter/library/scribe/ScribeLog;->H:I

    iput v2, p0, Lcom/twitter/library/scribe/ScribeLog;->R:I

    iput v2, p0, Lcom/twitter/library/scribe/ScribeLog;->S:I

    iput v2, p0, Lcom/twitter/library/scribe/ScribeLog;->W:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/scribe/ScribeLog;->n:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->w:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->O:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->P:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/scribe/ScribeLog;->f:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->m:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->Q:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->j:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/scribe/ScribeLog;->k:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/scribe/ScribeLog;->l:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/scribe/ScribeLog;->g:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/scribe/ScribeLog;->h:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/scribe/ScribeLog;->i:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/scribe/ScribeLog;->o:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->p:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->q:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->r:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->t:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/scribe/ScribeLog;->v:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->x:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->y:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->z:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/scribe/ScribeLog;->A:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eq v0, v2, :cond_0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->B:[B

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->B:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->F:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/scribe/ScribeLog;->G:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/scribe/ScribeLog;->H:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->I:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->J:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->K:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->L:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->M:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->N:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->s:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->T:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->T:Ljava/util/ArrayList;

    const-class v1, Lcom/twitter/library/scribe/ScribeItem;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->U:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->U:Ljava/util/ArrayList;

    const-class v1, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->C:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/scribe/ScribeLog;->D:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->E:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/scribe/ScribeLog;->W:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->ac:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->X:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->Y:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->Z:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->aa:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->ab:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/scribe/ScribeLog;->R:I

    const-class v0, Lcom/twitter/library/scribe/ScribeLog$SearchDetails;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/scribe/ScribeLog$SearchDetails;

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->u:Lcom/twitter/library/scribe/ScribeLog$SearchDetails;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/scribe/ScribeLog;->S:I

    return-void
.end method

.method public static a(Lcom/twitter/internal/network/k;)I
    .locals 2

    if-nez p0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/twitter/internal/network/k;->a:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/internal/network/k;->a:I

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/internal/network/k;->c:Ljava/lang/Exception;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/internal/network/k;->c:Ljava/lang/Exception;

    instance-of v0, v0, Ljava/net/SocketTimeoutException;

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    const/4 v0, 0x4

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(F)Ljava/lang/String;
    .locals 4

    const-string/jumbo v0, "image:upload:%s::%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {}, Lcom/twitter/library/telephony/TelephonyUtil;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p0}, Lcom/twitter/library/scribe/ScribeLog;->b(F)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    if-eqz p0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/library/scribe/ScribeAssociation;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "tweet"

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/library/scribe/ScribeAssociation;->c()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    const-string/jumbo v1, ""

    :goto_1
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    aput-object v1, v2, v0

    const/4 v0, 0x2

    aput-object p1, v2, v0

    const/4 v0, 0x3

    aput-object p2, v2, v0

    const/4 v0, 0x4

    aput-object p3, v2, v0

    invoke-static {v2}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/scribe/ScribeAssociation;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/scribe/ScribeAssociation;->c()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_2
    const-string/jumbo v0, "tweet"

    const-string/jumbo v1, ""

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;I)Ljava/lang/String;
    .locals 5

    invoke-static {p0}, Lcom/twitter/library/util/Util;->b(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "twimg"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "o.twimg.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "twimg"

    :goto_0
    const-string/jumbo v1, "image:predictor:%s:%s:%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Lcom/twitter/library/telephony/TelephonyUtil;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    const/4 v0, 0x2

    invoke-static {p1}, Lcom/twitter/library/scribe/ScribeLog;->h(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v0, "other"

    goto :goto_0
.end method

.method public static varargs a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    array-length v1, p1

    if-nez v1, :cond_1

    :cond_0
    const-string/jumbo v0, ""

    :goto_0
    return-object v0

    :cond_1
    array-length v1, p1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    aget-object v0, p1, v0

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    array-length v1, p1

    :goto_1
    if-ge v0, v1, :cond_5

    aget-object v2, p1, v0

    if-nez v2, :cond_4

    const-string/jumbo v2, ""

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_2
    add-int/lit8 v2, v1, -0x1

    if-ge v0, v2, :cond_3

    const/16 v2, 0x3a

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    aget-object v2, p1, v0

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_5
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static varargs a([Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_1

    :cond_0
    const-string/jumbo v0, ""

    :goto_0
    return-object v0

    :cond_1
    array-length v0, p0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    const/4 v0, 0x0

    aget-object v0, p0, v0

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0, p0}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;Lcom/twitter/internal/network/k;)V
    .locals 1

    iget-object v0, p2, Lcom/twitter/internal/network/k;->c:Ljava/lang/Exception;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/twitter/internal/network/k;->c:Ljava/lang/Exception;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/library/scribe/ScribeLog;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_0
    iget v0, p2, Lcom/twitter/internal/network/k;->a:I

    invoke-virtual {p1, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(I)Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p1, p2}, Lcom/twitter/library/scribe/ScribeLog;->b(Lcom/twitter/internal/network/k;)Lcom/twitter/library/scribe/ScribeLog;

    invoke-static {p0}, Lcom/twitter/library/telephony/TelephonyUtil;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "connected"

    :goto_0
    invoke-virtual {p1, v0}, Lcom/twitter/library/scribe/ScribeLog;->d(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    return-void

    :cond_1
    const-string/jumbo v0, "disconnected"

    goto :goto_0
.end method

.method private a(Lcom/fasterxml/jackson/core/JsonGenerator;)V
    .locals 5

    const/4 v4, -0x1

    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->c()V

    const-string/jumbo v0, "_category_"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->w:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "format_version"

    const/4 v1, 0x2

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "client_version"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string/jumbo v0, "client_event"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "app_download_client_event"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2e

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->P:Ljava/lang/String;

    if-eqz v0, :cond_2

    const-string/jumbo v0, "referring_event"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/twitter/library/scribe/ScribeLog;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/scribe/ScribeLog;->P:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->O:Ljava/lang/String;

    if-eqz v0, :cond_3

    const-string/jumbo v0, "event_name"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/twitter/library/scribe/ScribeLog;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/scribe/ScribeLog;->O:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->Q:Ljava/lang/String;

    if-eqz v0, :cond_4

    const-string/jumbo v0, "limit_ad_tracking"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->Q:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    :cond_4
    const-string/jumbo v0, "ts"

    iget-wide v1, p0, Lcom/twitter/library/scribe/ScribeLog;->f:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->j:Ljava/lang/String;

    if-eqz v0, :cond_5

    const-string/jumbo v0, "protocol"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    iget v0, p0, Lcom/twitter/library/scribe/ScribeLog;->k:I

    if-ltz v0, :cond_6

    const-string/jumbo v0, "stream_id"

    iget v1, p0, Lcom/twitter/library/scribe/ScribeLog;->k:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    :cond_6
    iget v0, p0, Lcom/twitter/library/scribe/ScribeLog;->l:I

    if-ltz v0, :cond_7

    const-string/jumbo v0, "content_length"

    iget v1, p0, Lcom/twitter/library/scribe/ScribeLog;->l:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    :cond_7
    iget-wide v0, p0, Lcom/twitter/library/scribe/ScribeLog;->g:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_8

    const-string/jumbo v0, "duration_ms"

    iget-wide v1, p0, Lcom/twitter/library/scribe/ScribeLog;->g:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    :cond_8
    iget v0, p0, Lcom/twitter/library/scribe/ScribeLog;->h:I

    if-eq v0, v4, :cond_9

    const-string/jumbo v0, "status_code"

    iget v1, p0, Lcom/twitter/library/scribe/ScribeLog;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    :cond_9
    iget v0, p0, Lcom/twitter/library/scribe/ScribeLog;->i:I

    if-eq v0, v4, :cond_a

    const-string/jumbo v0, "failure_type"

    iget v1, p0, Lcom/twitter/library/scribe/ScribeLog;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    :cond_a
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->q:Ljava/lang/String;

    if-eqz v0, :cond_b

    const-string/jumbo v0, "message"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->r:Ljava/lang/String;

    if-eqz v0, :cond_c

    const-string/jumbo v0, "event_info"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->r:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->s:Ljava/lang/String;

    if-eqz v0, :cond_d

    const-string/jumbo v0, "event_value"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->s:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->m:Ljava/lang/String;

    if-eqz v0, :cond_e

    const-string/jumbo v0, "url"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_e
    iget v0, p0, Lcom/twitter/library/scribe/ScribeLog;->o:I

    if-eq v0, v4, :cond_f

    const-string/jumbo v0, "event_initiator"

    iget v1, p0, Lcom/twitter/library/scribe/ScribeLog;->o:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    :cond_f
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->t:Ljava/lang/String;

    if-eqz v0, :cond_10

    const-string/jumbo v0, "query"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->t:Ljava/lang/String;

    const-string/jumbo v2, "UTF8"

    invoke-static {v1, v2}, Lcom/twitter/library/util/Util;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_10
    iget v0, p0, Lcom/twitter/library/scribe/ScribeLog;->v:I

    if-eq v0, v4, :cond_11

    const-string/jumbo v0, "position"

    iget v1, p0, Lcom/twitter/library/scribe/ScribeLog;->v:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    :cond_11
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->x:Ljava/lang/String;

    if-eqz v0, :cond_12

    const-string/jumbo v0, "context"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->x:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_12
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->y:Ljava/lang/String;

    if-eqz v0, :cond_13

    const-string/jumbo v0, "profile_id"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->y:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_13
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->F:Ljava/lang/String;

    if-eqz v0, :cond_14

    const-string/jumbo v0, "orientation"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->F:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_14
    iget v0, p0, Lcom/twitter/library/scribe/ScribeLog;->G:I

    if-eq v0, v4, :cond_15

    const-string/jumbo v0, "network_status"

    iget v1, p0, Lcom/twitter/library/scribe/ScribeLog;->G:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    :cond_15
    iget v0, p0, Lcom/twitter/library/scribe/ScribeLog;->H:I

    const v1, 0x7fffffff

    if-eq v0, v1, :cond_16

    const-string/jumbo v0, "signal_strength"

    iget v1, p0, Lcom/twitter/library/scribe/ScribeLog;->H:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    :cond_16
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->I:Ljava/lang/String;

    if-eqz v0, :cond_17

    const-string/jumbo v0, "mobile_network_operator_iso_country_code"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->I:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_17
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->J:Ljava/lang/String;

    if-eqz v0, :cond_18

    const-string/jumbo v0, "mobile_network_operator_code"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->J:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_18
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->K:Ljava/lang/String;

    if-eqz v0, :cond_19

    const-string/jumbo v0, "mobile_network_operator_name"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->K:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_19
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->L:Ljava/lang/String;

    if-eqz v0, :cond_1a

    const-string/jumbo v0, "mobile_sim_provider_iso_country_code"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->L:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1a
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->M:Ljava/lang/String;

    if-eqz v0, :cond_1b

    const-string/jumbo v0, "mobile_sim_provider_code"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->M:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1b
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->N:Ljava/lang/String;

    if-eqz v0, :cond_1c

    const-string/jumbo v0, "mobile_sim_provider_name"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->N:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1c
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->u:Lcom/twitter/library/scribe/ScribeLog$SearchDetails;

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->u:Lcom/twitter/library/scribe/ScribeLog$SearchDetails;

    invoke-virtual {v0, p1}, Lcom/twitter/library/scribe/ScribeLog$SearchDetails;->a(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    :cond_1d
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->T:Ljava/util/ArrayList;

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->T:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1f

    const-string/jumbo v0, "items"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->T:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/scribe/ScribeItem;

    invoke-virtual {v0, p1}, Lcom/twitter/library/scribe/ScribeItem;->a(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    goto :goto_0

    :cond_1e
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->b()V

    :cond_1f
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->U:Ljava/util/ArrayList;

    if-eqz v0, :cond_21

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->U:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_21

    const-string/jumbo v0, "associations"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->U:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, p1}, Lcom/twitter/library/scribe/ScribeAssociation;->a(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    goto :goto_1

    :cond_20
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    :cond_21
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->C:Ljava/lang/String;

    if-eqz v0, :cond_22

    const-string/jumbo v0, "experiment_key"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->C:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "version"

    iget v1, p0, Lcom/twitter/library/scribe/ScribeLog;->D:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    const-string/jumbo v0, "bucket"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->E:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_22
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->V:Ljava/util/HashMap;

    if-eqz v0, :cond_24

    const-string/jumbo v0, "external_ids"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->V:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_23

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_23
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    :cond_24
    iget v0, p0, Lcom/twitter/library/scribe/ScribeLog;->W:I

    if-eq v0, v4, :cond_25

    const-string/jumbo v0, "referral_type"

    iget v1, p0, Lcom/twitter/library/scribe/ScribeLog;->W:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    :cond_25
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->X:Ljava/lang/String;

    if-eqz v0, :cond_26

    const-string/jumbo v0, "medium"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->X:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_26
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->Y:Ljava/lang/String;

    if-eqz v0, :cond_27

    const-string/jumbo v0, "campaign"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->Y:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_27
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->Z:Ljava/lang/String;

    if-eqz v0, :cond_28

    const-string/jumbo v0, "query_term"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->Z:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_28
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->aa:Ljava/lang/String;

    if-eqz v0, :cond_29

    const-string/jumbo v0, "campaign_content"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->aa:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_29
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->ab:Ljava/lang/String;

    if-eqz v0, :cond_2a

    const-string/jumbo v0, "gclid"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->ab:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2a
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->ac:Ljava/lang/String;

    if-eqz v0, :cond_2b

    const-string/jumbo v0, "source"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->ac:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "external_referer"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->ac:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2b
    iget v0, p0, Lcom/twitter/library/scribe/ScribeLog;->R:I

    if-eq v0, v4, :cond_2c

    const-string/jumbo v0, "cursor_or_page"

    iget v1, p0, Lcom/twitter/library/scribe/ScribeLog;->R:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    :cond_2c
    iget v0, p0, Lcom/twitter/library/scribe/ScribeLog;->S:I

    if-eq v0, v4, :cond_2d

    const-string/jumbo v0, "item_count"

    iget v1, p0, Lcom/twitter/library/scribe/ScribeLog;->S:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    :cond_2d
    :goto_3
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    return-void

    :cond_2e
    const-string/jumbo v0, "client_watch_error"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2f

    iget v0, p0, Lcom/twitter/library/scribe/ScribeLog;->A:I

    if-lez v0, :cond_2f

    const-string/jumbo v0, "product_name"

    sget-object v1, Lcom/twitter/library/scribe/ScribeLog;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lcom/twitter/library/scribe/ScribeLog;->A:I

    packed-switch v0, :pswitch_data_0

    :goto_4
    const-string/jumbo v0, "error"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->z:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->B:[B

    if-eqz v0, :cond_2d

    const-string/jumbo v0, "error_details"

    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/library/scribe/ScribeLog;->B:[B

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :pswitch_0
    const-string/jumbo v0, "type"

    const-string/jumbo v1, "crash"

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :pswitch_1
    const-string/jumbo v0, "type"

    const-string/jumbo v1, "error"

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :cond_2f
    const-string/jumbo v0, "perftown"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2d

    const-string/jumbo v0, "product"

    sget-object v1, Lcom/twitter/library/scribe/ScribeLog;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "protocol"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "stream_id"

    iget v1, p0, Lcom/twitter/library/scribe/ScribeLog;->k:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    const-string/jumbo v0, "content_length"

    iget v1, p0, Lcom/twitter/library/scribe/ScribeLog;->l:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    const-string/jumbo v0, "duration_ms"

    iget-wide v1, p0, Lcom/twitter/library/scribe/ScribeLog;->g:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;J)V

    const-string/jumbo v0, "description"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->t:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lcom/twitter/library/client/AppFlavor;)V
    .locals 2

    sget-object v0, Lcom/twitter/library/scribe/d;->a:[I

    invoke-virtual {p0}, Lcom/twitter/library/client/AppFlavor;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string/jumbo v0, "android"

    sput-object v0, Lcom/twitter/library/scribe/ScribeLog;->a:Ljava/lang/String;

    :goto_0
    return-void

    :pswitch_0
    const-string/jumbo v0, "android_lite"

    sput-object v0, Lcom/twitter/library/scribe/ScribeLog;->a:Ljava/lang/String;

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "android_tablet"

    sput-object v0, Lcom/twitter/library/scribe/ScribeLog;->a:Ljava/lang/String;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeItem;)V
    .locals 2

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->n()Lcom/twitter/library/api/TwitterStatusCard;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    invoke-virtual {v1}, Lcom/twitter/library/card/instance/CardInstanceData;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, Lcom/twitter/library/scribe/ScribeItem;->M:Ljava/lang/String;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    invoke-virtual {v0}, Lcom/twitter/library/card/instance/CardInstanceData;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/twitter/library/scribe/ScribeItem;->L:Ljava/lang/String;

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(F)Ljava/lang/String;
    .locals 4

    const/4 v1, 0x0

    sget-object v0, Lcom/twitter/library/scribe/ScribeLog;->d:[F

    array-length v2, v0

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_1

    sget-object v3, Lcom/twitter/library/scribe/ScribeLog;->d:[F

    aget v3, v3, v0

    cmpg-float v3, p0, v3

    if-gtz v3, :cond_0

    :goto_1
    sget-object v1, Lcom/twitter/library/scribe/ScribeLog;->e:[Ljava/lang/String;

    aget-object v0, v1, v0

    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public static b(Ljava/lang/String;I)Ljava/lang/String;
    .locals 5

    invoke-static {p0}, Lcom/twitter/library/util/Util;->b(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "twimg"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "o.twimg.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "twimg"

    :goto_0
    const-string/jumbo v1, "image:download:%s:%s:%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Lcom/twitter/library/telephony/TelephonyUtil;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    const/4 v0, 0x2

    invoke-static {p1}, Lcom/twitter/library/scribe/ScribeLog;->h(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v0, "other"

    goto :goto_0
.end method

.method private static h(I)Ljava/lang/String;
    .locals 3

    sget-object v0, Lcom/twitter/library/scribe/ScribeLog;->c:[Ljava/lang/String;

    array-length v1, v0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    sget-object v2, Lcom/twitter/library/scribe/ScribeLog;->b:[I

    aget v2, v2, v0

    if-gt p0, v2, :cond_0

    sget-object v1, Lcom/twitter/library/scribe/ScribeLog;->c:[Ljava/lang/String;

    aget-object v0, v1, v0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/twitter/library/scribe/ScribeLog;->c:[Ljava/lang/String;

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    goto :goto_1
.end method


# virtual methods
.method public a(I)Lcom/twitter/library/scribe/ScribeLog;
    .locals 0

    iput p1, p0, Lcom/twitter/library/scribe/ScribeLog;->h:I

    return-object p0
.end method

.method public a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p1, p2, p3, p4, v0}, Lcom/twitter/library/scribe/ScribeItem;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    return-object p0
.end method

.method public a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;I)Lcom/twitter/library/scribe/ScribeLog;
    .locals 6

    const/4 v4, 0x0

    move-wide v0, p1

    move-object v2, p3

    move-object v3, p4

    move v5, p5

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/scribe/ScribeItem;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Ljava/lang/String;I)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    return-object p0
.end method

.method public a(JLjava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 1

    const-string/jumbo v0, "perftown"

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->w:Ljava/lang/String;

    iput-wide p1, p0, Lcom/twitter/library/scribe/ScribeLog;->g:J

    iput-object p3, p0, Lcom/twitter/library/scribe/ScribeLog;->t:Ljava/lang/String;

    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/library/scribe/ScribeLog;->o:I

    return-object p0
.end method

.method public a(Landroid/content/Context;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 2

    invoke-static {}, Lcom/twitter/library/telephony/TelephonyUtil;->a()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "2g"

    if-eq v0, v1, :cond_0

    const-string/jumbo v1, "cellular"

    if-ne v0, v1, :cond_1

    :cond_0
    invoke-static {}, Lcom/twitter/library/telephony/a;->a()Lcom/twitter/library/telephony/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/telephony/a;->b()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/scribe/ScribeLog;->H:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/library/scribe/ScribeLog;->G:I

    :goto_0
    return-object p0

    :cond_1
    const-string/jumbo v1, "wifi"

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/library/scribe/ScribeLog;->G:I

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/library/scribe/ScribeLog;->G:I

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 1

    if-eqz p2, :cond_0

    invoke-static {p1, p2, p3, p4}, Lcom/twitter/library/scribe/ScribeItem;->a(Landroid/content/Context;Lcom/twitter/library/provider/ParcelableTweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_0
    return-object p0
.end method

.method public a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;JZLjava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 2

    if-eqz p2, :cond_1

    invoke-static {p1, p2, p3, p4}, Lcom/twitter/library/scribe/ScribeItem;->a(Landroid/content/Context;Lcom/twitter/library/provider/ParcelableTweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    :goto_0
    iput p5, v0, Lcom/twitter/library/scribe/ScribeItem;->I:I

    iput-object p6, v0, Lcom/twitter/library/scribe/ScribeItem;->F:Ljava/lang/String;

    iput-object p7, v0, Lcom/twitter/library/scribe/ScribeItem;->H:Ljava/lang/String;

    iput-wide p8, v0, Lcom/twitter/library/scribe/ScribeItem;->G:J

    if-eqz p10, :cond_2

    const/4 v1, 0x1

    :goto_1
    iput v1, v0, Lcom/twitter/library/scribe/ScribeItem;->J:I

    invoke-static {p11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p11, 0x0

    :cond_0
    iput-object p11, v0, Lcom/twitter/library/scribe/ScribeItem;->K:Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeItem;)V

    invoke-virtual {p0, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    return-object p0

    :cond_1
    new-instance v0, Lcom/twitter/library/scribe/ScribeItem;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeItem;-><init>()V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public a(Lcom/twitter/internal/network/HttpOperation;Z)Lcom/twitter/library/scribe/ScribeLog;
    .locals 3

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v0

    iget v1, v0, Lcom/twitter/internal/network/k;->a:I

    const/16 v2, 0xc8

    if-eq v1, v2, :cond_0

    iget v1, v0, Lcom/twitter/internal/network/k;->a:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->r:Ljava/lang/String;

    :goto_0
    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->h()Ljava/net/URI;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Lcom/twitter/internal/network/k;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/twitter/library/network/aa;->d(Lcom/twitter/internal/network/HttpOperation;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "polling"

    iput-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->r:Ljava/lang/String;

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    const-string/jumbo v1, "non-polling-foreground"

    iput-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->r:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string/jumbo v1, "non-polling-background"

    iput-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->r:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 1

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->U:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->U:Ljava/util/ArrayList;

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->U:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    return-object p0
.end method

.method public a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 1

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->T:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->T:Ljava/util/ArrayList;

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->T:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/scribe/ScribeLog;->m:Ljava/lang/String;

    return-object p0
.end method

.method public a(Ljava/lang/String;ILjava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/scribe/ScribeLog;->C:Ljava/lang/String;

    iput p2, p0, Lcom/twitter/library/scribe/ScribeLog;->D:I

    iput-object p3, p0, Lcom/twitter/library/scribe/ScribeLog;->E:Ljava/lang/String;

    return-object p0
.end method

.method public a(Ljava/lang/String;Lcom/twitter/internal/network/k;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 2

    iput-object p1, p0, Lcom/twitter/library/scribe/ScribeLog;->m:Ljava/lang/String;

    iget v0, p2, Lcom/twitter/internal/network/k;->a:I

    iput v0, p0, Lcom/twitter/library/scribe/ScribeLog;->h:I

    iget-wide v0, p2, Lcom/twitter/internal/network/k;->d:J

    iput-wide v0, p0, Lcom/twitter/library/scribe/ScribeLog;->g:J

    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/library/scribe/ScribeLog;->o:I

    invoke-virtual {p0, p2}, Lcom/twitter/library/scribe/ScribeLog;->b(Lcom/twitter/internal/network/k;)Lcom/twitter/library/scribe/ScribeLog;

    iget v0, p2, Lcom/twitter/internal/network/k;->k:I

    iput v0, p0, Lcom/twitter/library/scribe/ScribeLog;->k:I

    iget v0, p2, Lcom/twitter/internal/network/k;->j:I

    iput v0, p0, Lcom/twitter/library/scribe/ScribeLog;->l:I

    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->V:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->V:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->V:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 1

    const/4 v0, 0x3

    iput v0, p0, Lcom/twitter/library/scribe/ScribeLog;->W:I

    iput-object p1, p0, Lcom/twitter/library/scribe/ScribeLog;->ac:Ljava/lang/String;

    iput-object p2, p0, Lcom/twitter/library/scribe/ScribeLog;->X:Ljava/lang/String;

    iput-object p3, p0, Lcom/twitter/library/scribe/ScribeLog;->Y:Ljava/lang/String;

    iput-object p4, p0, Lcom/twitter/library/scribe/ScribeLog;->Z:Ljava/lang/String;

    iput-object p5, p0, Lcom/twitter/library/scribe/ScribeLog;->aa:Ljava/lang/String;

    iput-object p6, p0, Lcom/twitter/library/scribe/ScribeLog;->ab:Ljava/lang/String;

    iput-object p7, p0, Lcom/twitter/library/scribe/ScribeLog;->q:Ljava/lang/String;

    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/library/scribe/ScribeLog;
    .locals 1

    new-instance v0, Lcom/twitter/library/scribe/ScribeLog$SearchDetails;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/twitter/library/scribe/ScribeLog$SearchDetails;-><init>(Ljava/lang/String;Ljava/lang/String;ZZ)V

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->u:Lcom/twitter/library/scribe/ScribeLog$SearchDetails;

    return-object p0
.end method

.method public a(Ljava/lang/Throwable;ILjava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 1

    const-string/jumbo v0, "client_watch_error"

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->w:Ljava/lang/String;

    iput p2, p0, Lcom/twitter/library/scribe/ScribeLog;->A:I

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->z:Ljava/lang/String;

    invoke-static {p1}, Lcom/twitter/library/util/Util;->a(Ljava/lang/Throwable;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->B:[B

    iput-object p3, p0, Lcom/twitter/library/scribe/ScribeLog;->r:Ljava/lang/String;

    iput-object p4, p0, Lcom/twitter/library/scribe/ScribeLog;->p:Ljava/lang/String;

    return-object p0
.end method

.method public a(Ljava/util/ArrayList;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 2

    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/library/scribe/ScribeLog;->o:I

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/scribe/ScribeItem;

    invoke-virtual {p0, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public a(Z)Lcom/twitter/library/scribe/ScribeLog;
    .locals 1

    if-eqz p1, :cond_0

    const-string/jumbo v0, "1"

    :goto_0
    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->Q:Ljava/lang/String;

    return-object p0

    :cond_0
    const-string/jumbo v0, "0"

    goto :goto_0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->O:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/io/OutputStream;)V
    .locals 6

    const/4 v1, 0x0

    :try_start_0
    new-instance v0, Ljava/io/OutputStreamWriter;

    invoke-direct {v0, p1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    sget-object v2, Lcom/twitter/library/api/ap;->a:Lcom/fasterxml/jackson/core/JsonFactory;

    invoke-virtual {v2, v0}, Lcom/fasterxml/jackson/core/JsonFactory;->b(Ljava/io/Writer;)Lcom/fasterxml/jackson/core/JsonGenerator;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    :try_start_2
    invoke-direct {p0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/fasterxml/jackson/core/JsonGenerator;)V

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->flush()V

    sget-boolean v2, Lcom/twitter/library/scribe/ScribeService;->b:Z

    if-eqz v2, :cond_0

    const-string/jumbo v2, "ScribeService"

    new-instance v3, Lorg/json/JSONObject;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :cond_0
    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-static {v0}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_1
    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-static {v0}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v0, v1

    :goto_2
    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-static {v0}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_3
    invoke-static {v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v2

    move-object v5, v2

    move-object v2, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_3

    :catchall_2
    move-exception v2

    move-object v5, v2

    move-object v2, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_3

    :catch_2
    move-exception v2

    goto :goto_2

    :catch_3
    move-exception v2

    goto :goto_1
.end method

.method public b()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/scribe/ScribeLog;->n:J

    return-wide v0
.end method

.method public b(I)Lcom/twitter/library/scribe/ScribeLog;
    .locals 0

    iput p1, p0, Lcom/twitter/library/scribe/ScribeLog;->i:I

    return-object p0
.end method

.method public b(Landroid/content/Context;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 2

    const-string/jumbo v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->I:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->J:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->K:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->L:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/scribe/ScribeLog;->M:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperatorName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->N:Ljava/lang/String;

    return-object p0
.end method

.method public b(Lcom/twitter/internal/network/k;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p1, Lcom/twitter/internal/network/k;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/internal/network/k;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->j:Ljava/lang/String;

    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/scribe/ScribeLog;->p:Ljava/lang/String;

    return-object p0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/scribe/ScribeLog;->m:Ljava/lang/String;

    iput-object p2, p0, Lcom/twitter/library/scribe/ScribeLog;->q:Ljava/lang/String;

    return-object p0
.end method

.method public varargs b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 1

    invoke-static {p1}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->O:Ljava/lang/String;

    return-object p0
.end method

.method public c()Lcom/twitter/library/scribe/ScribeItem;
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->T:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->T:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->T:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/scribe/ScribeItem;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(F)Lcom/twitter/library/scribe/ScribeLog;
    .locals 1

    invoke-static {p1}, Lcom/twitter/library/scribe/ScribeLog;->b(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->q:Ljava/lang/String;

    return-object p0
.end method

.method public c(I)Lcom/twitter/library/scribe/ScribeLog;
    .locals 0

    iput p1, p0, Lcom/twitter/library/scribe/ScribeLog;->o:I

    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/scribe/ScribeLog;->q:Ljava/lang/String;

    return-object p0
.end method

.method public varargs c([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 1

    invoke-static {p1}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->P:Ljava/lang/String;

    return-object p0
.end method

.method public d(I)Lcom/twitter/library/scribe/ScribeLog;
    .locals 0

    iput p1, p0, Lcom/twitter/library/scribe/ScribeLog;->v:I

    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/scribe/ScribeLog;->r:Ljava/lang/String;

    return-object p0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e(I)Lcom/twitter/library/scribe/ScribeLog;
    .locals 0

    iput p1, p0, Lcom/twitter/library/scribe/ScribeLog;->R:I

    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/scribe/ScribeLog;->s:Ljava/lang/String;

    return-object p0
.end method

.method public f(I)Lcom/twitter/library/scribe/ScribeLog;
    .locals 0

    iput p1, p0, Lcom/twitter/library/scribe/ScribeLog;->S:I

    return-object p0
.end method

.method public f(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/scribe/ScribeLog;->t:Ljava/lang/String;

    return-object p0
.end method

.method public g(I)Lcom/twitter/library/scribe/ScribeLog;
    .locals 2

    int-to-long v0, p1

    iput-wide v0, p0, Lcom/twitter/library/scribe/ScribeLog;->g:J

    return-object p0
.end method

.method public g(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/scribe/ScribeLog;->w:Ljava/lang/String;

    return-object p0
.end method

.method public h(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/scribe/ScribeLog;->x:Ljava/lang/String;

    return-object p0
.end method

.method public i(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/scribe/ScribeLog;->y:Ljava/lang/String;

    return-object p0
.end method

.method public j(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/scribe/ScribeLog;->F:Ljava/lang/String;

    return-object p0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    iget-wide v0, p0, Lcom/twitter/library/scribe/ScribeLog;->n:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->w:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->O:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->P:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/twitter/library/scribe/ScribeLog;->f:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->Q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/twitter/library/scribe/ScribeLog;->k:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/scribe/ScribeLog;->l:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-wide v0, p0, Lcom/twitter/library/scribe/ScribeLog;->g:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget v0, p0, Lcom/twitter/library/scribe/ScribeLog;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/scribe/ScribeLog;->i:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/scribe/ScribeLog;->o:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->r:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->t:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/twitter/library/scribe/ScribeLog;->v:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->x:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->y:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->z:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/twitter/library/scribe/ScribeLog;->A:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->B:[B

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->B:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->B:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    :goto_0
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->F:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/twitter/library/scribe/ScribeLog;->G:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/scribe/ScribeLog;->H:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->I:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->J:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->K:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->L:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->M:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->N:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->s:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->T:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->U:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->C:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/twitter/library/scribe/ScribeLog;->D:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->E:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->V:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->V:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->V:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    :cond_2
    iget v0, p0, Lcom/twitter/library/scribe/ScribeLog;->W:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->ac:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->X:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->Y:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->Z:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->aa:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->ab:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/twitter/library/scribe/ScribeLog;->R:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeLog;->u:Lcom/twitter/library/scribe/ScribeLog$SearchDetails;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget v0, p0, Lcom/twitter/library/scribe/ScribeLog;->S:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
