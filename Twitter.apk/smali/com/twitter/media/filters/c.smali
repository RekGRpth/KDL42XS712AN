.class public Lcom/twitter/media/filters/c;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Z

.field private static b:Z


# direct methods
.method public static a(I)Z
    .locals 1

    if-nez p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Z
    .locals 8

    const/4 v1, 0x0

    const/4 v0, 0x1

    const-class v3, Lcom/twitter/media/filters/c;

    monitor-enter v3

    :try_start_0
    sget-boolean v2, Lcom/twitter/media/filters/c;->a:Z

    if-eqz v2, :cond_1

    sget-boolean v0, Lcom/twitter/media/filters/c;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :cond_0
    :goto_0
    monitor-exit v3

    return v0

    :cond_1
    const/4 v2, 0x0

    :try_start_1
    invoke-static {}, Lcom/twitter/media/NativeInit;->b()Z

    move-result v4

    if-eqz v4, :cond_6

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x8

    if-lt v4, v5, :cond_6

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/twitter/media/b;->filter_resources:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->openRawResourceFd(I)Landroid/content/res/AssetFileDescriptor;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    if-eqz v2, :cond_2

    :try_start_2
    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_2

    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Ljava/io/FileDescriptor;->valid()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    sput-boolean v4, Lcom/twitter/media/filters/c;->b:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v1, 0x1

    :try_start_3
    sput-boolean v1, Lcom/twitter/media/filters/c;->a:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v2, :cond_0

    :try_start_4
    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    :cond_2
    move-object v0, v2

    :goto_1
    const/4 v2, 0x0

    :try_start_5
    sput-boolean v2, Lcom/twitter/media/filters/c;->b:Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_6
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    const/4 v2, 0x1

    :try_start_6
    sput-boolean v2, Lcom/twitter/media/filters/c;->a:Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    if-eqz v0, :cond_3

    :try_start_7
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :cond_3
    :goto_2
    move v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v0, v2

    :goto_3
    const/4 v2, 0x0

    :try_start_8
    sput-boolean v2, Lcom/twitter/media/filters/c;->b:Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    const/4 v2, 0x1

    :try_start_9
    sput-boolean v2, Lcom/twitter/media/filters/c;->a:Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    if-eqz v0, :cond_4

    :try_start_a
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :cond_4
    :goto_4
    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_5
    const/4 v1, 0x1

    :try_start_b
    sput-boolean v1, Lcom/twitter/media/filters/c;->a:Z
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    if-eqz v2, :cond_5

    :try_start_c
    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_4
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    :cond_5
    :goto_6
    :try_start_d
    throw v0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v3

    throw v0

    :catch_2
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v0

    goto :goto_4

    :catch_4
    move-exception v1

    goto :goto_6

    :catchall_2
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_5

    :catch_5
    move-exception v0

    move-object v0, v2

    goto :goto_3

    :catch_6
    move-exception v2

    goto :goto_3

    :cond_6
    move-object v0, v2

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)Z
    .locals 2

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-static {p0, p1}, Lcom/twitter/media/MediaUtils;->a(Landroid/content/Context;Landroid/net/Uri;)Lcom/twitter/media/ImageInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;Landroid/graphics/Bitmap;IZ)Z
    .locals 7

    const/high16 v5, 0x3f800000    # 1.0f

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v6, v5

    invoke-static/range {v0 .. v6}, Lcom/twitter/media/filters/c;->a(Landroid/content/Context;Landroid/net/Uri;Landroid/graphics/Bitmap;IZFF)Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;Landroid/graphics/Bitmap;IZFF)Z
    .locals 6

    const/4 v1, 0x0

    if-ltz p3, :cond_0

    const/16 v0, 0x9

    if-le p3, v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    new-instance v0, Lcom/twitter/media/filters/Filters;

    invoke-direct {v0}, Lcom/twitter/media/filters/Filters;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/twitter/media/b;->filter_resources:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->openRawResourceFd(I)Landroid/content/res/AssetFileDescriptor;

    move-result-object v2

    invoke-virtual {v0, p0, v2}, Lcom/twitter/media/filters/Filters;->a(Landroid/content/Context;Landroid/content/res/AssetFileDescriptor;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {v0, p1, v2, v3, p4}, Lcom/twitter/media/filters/Filters;->a(Landroid/net/Uri;IIZ)I

    move-result v2

    if-lez v2, :cond_2

    move v1, p3

    move-object v3, p2

    move v4, p5

    move v5, p6

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/media/filters/Filters;->a(IILandroid/graphics/Bitmap;FF)Z

    move-result v1

    :cond_2
    invoke-virtual {v0}, Lcom/twitter/media/filters/Filters;->a()V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;Ljava/io/File;IIIZ)Z
    .locals 9

    const/high16 v7, 0x3f800000    # 1.0f

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move v8, v7

    invoke-static/range {v0 .. v8}, Lcom/twitter/media/filters/c;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/io/File;IIIZFF)Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;Ljava/io/File;IIIZFF)Z
    .locals 6

    const/4 v1, 0x0

    if-ltz p5, :cond_0

    const/16 v0, 0x9

    if-le p5, v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    new-instance v0, Lcom/twitter/media/filters/Filters;

    invoke-direct {v0}, Lcom/twitter/media/filters/Filters;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/twitter/media/b;->filter_resources:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->openRawResourceFd(I)Landroid/content/res/AssetFileDescriptor;

    move-result-object v2

    invoke-virtual {v0, p0, v2}, Lcom/twitter/media/filters/Filters;->a(Landroid/content/Context;Landroid/content/res/AssetFileDescriptor;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0, p1, p3, p4, p6}, Lcom/twitter/media/filters/Filters;->a(Landroid/net/Uri;IIZ)I

    move-result v2

    if-lez v2, :cond_2

    move v1, p5

    move-object v3, p2

    move v4, p7

    move v5, p8

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/media/filters/Filters;->a(IILjava/io/File;FF)Z

    move-result v1

    :cond_2
    invoke-virtual {v0}, Lcom/twitter/media/filters/Filters;->a()V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/io/File;Ljava/io/File;IIZ)Z
    .locals 8

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/twitter/media/b;->filter_resources:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->openRawResourceFd(I)Landroid/content/res/AssetFileDescriptor;

    move-result-object v1

    const/high16 v7, 0x3f800000    # 1.0f

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-static/range {v0 .. v7}, Lcom/twitter/media/filters/Filters;->a(Landroid/content/Context;Landroid/content/res/AssetFileDescriptor;Ljava/io/File;Ljava/io/File;IIZF)Z

    move-result v0

    return v0
.end method
