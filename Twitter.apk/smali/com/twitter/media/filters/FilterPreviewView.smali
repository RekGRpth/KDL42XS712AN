.class public Lcom/twitter/media/filters/FilterPreviewView;
.super Landroid/opengl/GLSurfaceView;
.source "Twttr"


# instance fields
.field private a:Landroid/content/Context;

.field private b:I

.field private c:Lcom/twitter/media/filters/b;

.field private d:F

.field private e:F

.field private f:F

.field private g:F

.field private h:Z

.field private i:Z

.field private j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/twitter/media/filters/FilterPreviewView;->a:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object p1, p0, Lcom/twitter/media/filters/FilterPreviewView;->a:Landroid/content/Context;

    return-void
.end method

.method private a(Landroid/view/MotionEvent;)F
    .locals 2

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    add-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method private b(Landroid/view/MotionEvent;)F
    .locals 2

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    add-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(I)V
    .locals 0

    return-void
.end method

.method public getFilterIntensity()F
    .locals 1

    iget-object v0, p0, Lcom/twitter/media/filters/FilterPreviewView;->c:Lcom/twitter/media/filters/b;

    iget v0, v0, Lcom/twitter/media/filters/b;->b:F

    return v0
.end method

.method public getVignetteSize()F
    .locals 1

    iget-object v0, p0, Lcom/twitter/media/filters/FilterPreviewView;->c:Lcom/twitter/media/filters/b;

    iget v0, v0, Lcom/twitter/media/filters/b;->a:F

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12

    const/4 v2, -0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v4, 0x1

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v5

    const/4 v6, 0x5

    if-ne v5, v6, :cond_1

    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/media/filters/FilterPreviewView;->a(Landroid/view/MotionEvent;)F

    move-result v0

    iput v0, p0, Lcom/twitter/media/filters/FilterPreviewView;->d:F

    invoke-direct {p0, p1}, Lcom/twitter/media/filters/FilterPreviewView;->b(Landroid/view/MotionEvent;)F

    move-result v0

    iput v0, p0, Lcom/twitter/media/filters/FilterPreviewView;->e:F

    iput v1, p0, Lcom/twitter/media/filters/FilterPreviewView;->f:F

    iput v1, p0, Lcom/twitter/media/filters/FilterPreviewView;->g:F

    iput-boolean v3, p0, Lcom/twitter/media/filters/FilterPreviewView;->h:Z

    iput-boolean v3, p0, Lcom/twitter/media/filters/FilterPreviewView;->i:Z

    iput-boolean v3, p0, Lcom/twitter/media/filters/FilterPreviewView;->j:Z

    move v0, v4

    :goto_0
    return v0

    :cond_1
    iget-boolean v5, p0, Lcom/twitter/media/filters/FilterPreviewView;->j:Z

    if-eqz v5, :cond_2

    move v0, v3

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lcom/twitter/media/filters/FilterPreviewView;->a(Landroid/view/MotionEvent;)F

    move-result v5

    invoke-direct {p0, p1}, Lcom/twitter/media/filters/FilterPreviewView;->b(Landroid/view/MotionEvent;)F

    move-result v6

    iget v3, p0, Lcom/twitter/media/filters/FilterPreviewView;->f:F

    iget v7, p0, Lcom/twitter/media/filters/FilterPreviewView;->d:F

    sub-float/2addr v7, v5

    add-float/2addr v3, v7

    iput v3, p0, Lcom/twitter/media/filters/FilterPreviewView;->f:F

    iget v3, p0, Lcom/twitter/media/filters/FilterPreviewView;->g:F

    iget v7, p0, Lcom/twitter/media/filters/FilterPreviewView;->e:F

    sub-float/2addr v7, v6

    add-float/2addr v3, v7

    iput v3, p0, Lcom/twitter/media/filters/FilterPreviewView;->g:F

    iget-boolean v3, p0, Lcom/twitter/media/filters/FilterPreviewView;->h:Z

    if-nez v3, :cond_5

    iget-boolean v3, p0, Lcom/twitter/media/filters/FilterPreviewView;->i:Z

    if-nez v3, :cond_5

    iget v0, p0, Lcom/twitter/media/filters/FilterPreviewView;->f:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/twitter/media/filters/FilterPreviewView;->g:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4

    iget v0, p0, Lcom/twitter/media/filters/FilterPreviewView;->f:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x40e00000    # 7.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4

    iput-boolean v4, p0, Lcom/twitter/media/filters/FilterPreviewView;->i:Z

    :cond_3
    :goto_1
    iput v5, p0, Lcom/twitter/media/filters/FilterPreviewView;->d:F

    iput v6, p0, Lcom/twitter/media/filters/FilterPreviewView;->e:F

    move v0, v4

    goto :goto_0

    :cond_4
    iget v0, p0, Lcom/twitter/media/filters/FilterPreviewView;->g:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/twitter/media/filters/FilterPreviewView;->f:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    iget v0, p0, Lcom/twitter/media/filters/FilterPreviewView;->g:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x40e00000    # 7.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    iput-boolean v4, p0, Lcom/twitter/media/filters/FilterPreviewView;->h:Z

    goto :goto_1

    :cond_5
    iget-boolean v3, p0, Lcom/twitter/media/filters/FilterPreviewView;->h:Z

    if-eqz v3, :cond_b

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    if-ne v2, v4, :cond_8

    iget-object v1, p0, Lcom/twitter/media/filters/FilterPreviewView;->c:Lcom/twitter/media/filters/b;

    iget v1, v1, Lcom/twitter/media/filters/b;->b:F

    iget v2, p0, Lcom/twitter/media/filters/FilterPreviewView;->e:F

    sub-float v2, v6, v2

    const v3, 0x3b449ba6    # 0.003f

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    const v2, 0x3e99999a    # 0.3f

    cmpg-float v2, v1, v2

    if-gez v2, :cond_7

    const v0, 0x3e99999a    # 0.3f

    :cond_6
    :goto_2
    iget-object v1, p0, Lcom/twitter/media/filters/FilterPreviewView;->c:Lcom/twitter/media/filters/b;

    iput v0, v1, Lcom/twitter/media/filters/b;->b:F

    goto :goto_1

    :cond_7
    cmpl-float v2, v1, v0

    if-gtz v2, :cond_6

    move v0, v1

    goto :goto_2

    :cond_8
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/twitter/media/filters/FilterPreviewView;->c:Lcom/twitter/media/filters/b;

    iget v2, v2, Lcom/twitter/media/filters/b;->a:F

    iget v3, p0, Lcom/twitter/media/filters/FilterPreviewView;->e:F

    sub-float v3, v6, v3

    const v7, 0x3b449ba6    # 0.003f

    mul-float/2addr v3, v7

    sub-float/2addr v2, v3

    cmpg-float v3, v2, v1

    if-gez v3, :cond_a

    move v0, v1

    :cond_9
    :goto_3
    iget-object v1, p0, Lcom/twitter/media/filters/FilterPreviewView;->c:Lcom/twitter/media/filters/b;

    iput v0, v1, Lcom/twitter/media/filters/b;->a:F

    goto :goto_1

    :cond_a
    cmpl-float v1, v2, v0

    if-gtz v1, :cond_9

    move v0, v2

    goto :goto_3

    :cond_b
    iget-boolean v3, p0, Lcom/twitter/media/filters/FilterPreviewView;->i:Z

    if-eqz v3, :cond_3

    iget v3, p0, Lcom/twitter/media/filters/FilterPreviewView;->b:I

    iget v7, p0, Lcom/twitter/media/filters/FilterPreviewView;->f:F

    cmpl-float v1, v7, v1

    if-ltz v1, :cond_e

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    if-eq v1, v4, :cond_c

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    const/4 v7, 0x6

    if-ne v1, v7, :cond_d

    :cond_c
    iget v1, p0, Lcom/twitter/media/filters/FilterPreviewView;->f:F

    iget-object v7, p0, Lcom/twitter/media/filters/FilterPreviewView;->c:Lcom/twitter/media/filters/b;

    invoke-virtual {v7}, Lcom/twitter/media/filters/b;->a()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v1, v7

    float-to-double v7, v1

    const-wide/high16 v9, 0x3fe0000000000000L    # 0.5

    cmpl-double v1, v7, v9

    if-lez v1, :cond_13

    iget v1, p0, Lcom/twitter/media/filters/FilterPreviewView;->b:I

    add-int/lit8 v1, v1, 0x1

    rem-int/lit8 v1, v1, 0x9

    iput v1, p0, Lcom/twitter/media/filters/FilterPreviewView;->b:I

    iget v1, p0, Lcom/twitter/media/filters/FilterPreviewView;->b:I

    iget v3, p0, Lcom/twitter/media/filters/FilterPreviewView;->b:I

    invoke-virtual {p0, v3}, Lcom/twitter/media/filters/FilterPreviewView;->a(I)V

    move v11, v2

    move v2, v1

    move v1, v11

    :goto_4
    iget-object v3, p0, Lcom/twitter/media/filters/FilterPreviewView;->c:Lcom/twitter/media/filters/b;

    invoke-virtual {v3, v2}, Lcom/twitter/media/filters/b;->a(I)V

    iget-object v2, p0, Lcom/twitter/media/filters/FilterPreviewView;->c:Lcom/twitter/media/filters/b;

    invoke-virtual {v2, v1}, Lcom/twitter/media/filters/b;->b(I)V

    iget-object v1, p0, Lcom/twitter/media/filters/FilterPreviewView;->c:Lcom/twitter/media/filters/b;

    invoke-virtual {v1, v0}, Lcom/twitter/media/filters/b;->a(F)V

    goto/16 :goto_1

    :cond_d
    iget v1, p0, Lcom/twitter/media/filters/FilterPreviewView;->b:I

    add-int/lit8 v1, v1, 0x1

    rem-int/lit8 v2, v1, 0x9

    iget v1, p0, Lcom/twitter/media/filters/FilterPreviewView;->b:I

    iget v3, p0, Lcom/twitter/media/filters/FilterPreviewView;->f:F

    iget-object v7, p0, Lcom/twitter/media/filters/FilterPreviewView;->c:Lcom/twitter/media/filters/b;

    invoke-virtual {v7}, Lcom/twitter/media/filters/b;->a()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v3, v7

    sub-float/2addr v0, v3

    goto :goto_4

    :cond_e
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    if-eq v1, v4, :cond_f

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    const/4 v7, 0x6

    if-ne v1, v7, :cond_11

    :cond_f
    iget v1, p0, Lcom/twitter/media/filters/FilterPreviewView;->f:F

    iget-object v7, p0, Lcom/twitter/media/filters/FilterPreviewView;->c:Lcom/twitter/media/filters/b;

    invoke-virtual {v7}, Lcom/twitter/media/filters/b;->a()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v1, v7

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    float-to-double v7, v1

    const-wide/high16 v9, 0x3fe0000000000000L    # 0.5

    cmpl-double v1, v7, v9

    if-lez v1, :cond_13

    iget v1, p0, Lcom/twitter/media/filters/FilterPreviewView;->b:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/twitter/media/filters/FilterPreviewView;->b:I

    iget v1, p0, Lcom/twitter/media/filters/FilterPreviewView;->b:I

    if-gez v1, :cond_10

    iget v1, p0, Lcom/twitter/media/filters/FilterPreviewView;->b:I

    add-int/lit8 v1, v1, 0x9

    iput v1, p0, Lcom/twitter/media/filters/FilterPreviewView;->b:I

    :cond_10
    iget v1, p0, Lcom/twitter/media/filters/FilterPreviewView;->b:I

    iget v3, p0, Lcom/twitter/media/filters/FilterPreviewView;->b:I

    invoke-virtual {p0, v3}, Lcom/twitter/media/filters/FilterPreviewView;->a(I)V

    move v11, v2

    move v2, v1

    move v1, v11

    goto :goto_4

    :cond_11
    iget v2, p0, Lcom/twitter/media/filters/FilterPreviewView;->b:I

    iget v0, p0, Lcom/twitter/media/filters/FilterPreviewView;->b:I

    add-int/lit8 v0, v0, -0x1

    rem-int/lit8 v0, v0, 0x9

    if-gez v0, :cond_12

    add-int/lit8 v0, v0, 0x9

    :cond_12
    iget v1, p0, Lcom/twitter/media/filters/FilterPreviewView;->f:F

    iget-object v3, p0, Lcom/twitter/media/filters/FilterPreviewView;->c:Lcom/twitter/media/filters/b;

    invoke-virtual {v3}, Lcom/twitter/media/filters/b;->a()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    move v11, v1

    move v1, v0

    move v0, v11

    goto :goto_4

    :cond_13
    move v1, v2

    move v2, v3

    goto/16 :goto_4
.end method

.method public setFilterIndex(I)V
    .locals 2

    iput p1, p0, Lcom/twitter/media/filters/FilterPreviewView;->b:I

    iget-object v0, p0, Lcom/twitter/media/filters/FilterPreviewView;->c:Lcom/twitter/media/filters/b;

    invoke-virtual {v0, p1}, Lcom/twitter/media/filters/b;->a(I)V

    iget-object v0, p0, Lcom/twitter/media/filters/FilterPreviewView;->c:Lcom/twitter/media/filters/b;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/twitter/media/filters/b;->b(I)V

    return-void
.end method
