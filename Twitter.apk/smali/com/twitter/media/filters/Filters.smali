.class public Lcom/twitter/media/filters/Filters;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:I

.field private static b:Lcom/twitter/media/filters/a;

.field private static c:Ljava/util/concurrent/Semaphore;


# instance fields
.field private d:Landroid/content/ContentResolver;

.field private e:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x0

    sput v0, Lcom/twitter/media/filters/Filters;->a:I

    const/4 v0, 0x0

    sput-object v0, Lcom/twitter/media/filters/Filters;->c:Ljava/util/concurrent/Semaphore;

    invoke-static {}, Lcom/twitter/media/NativeInit;->a()V

    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    sput-object v0, Lcom/twitter/media/filters/Filters;->c:Ljava/util/concurrent/Semaphore;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/media/filters/Filters;->e:Ljava/util/ArrayList;

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/res/AssetFileDescriptor;Ljava/io/File;Ljava/io/File;IIZF)Z
    .locals 12

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lcom/twitter/media/filters/Filters;->b()Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Lcom/twitter/media/filters/Filters;->b:Lcom/twitter/media/filters/a;

    if-nez v3, :cond_0

    new-instance v3, Lcom/twitter/media/filters/a;

    invoke-direct {v3}, Lcom/twitter/media/filters/a;-><init>()V

    sput-object v3, Lcom/twitter/media/filters/Filters;->b:Lcom/twitter/media/filters/a;

    sget-object v3, Lcom/twitter/media/filters/Filters;->b:Lcom/twitter/media/filters/a;

    invoke-virtual {v3}, Lcom/twitter/media/filters/a;->a()Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v3, 0x0

    sput-object v3, Lcom/twitter/media/filters/Filters;->b:Lcom/twitter/media/filters/a;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x0

    sget-object v3, Lcom/twitter/media/filters/Filters;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    invoke-static {v2}, Lcom/twitter/media/MediaUtils;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/twitter/media/MediaUtils;->a(Ljava/io/Closeable;)V

    :goto_0
    return v0

    :cond_0
    :try_start_1
    sget-object v3, Lcom/twitter/media/filters/Filters;->b:Lcom/twitter/media/filters/a;

    invoke-virtual {v3}, Lcom/twitter/media/filters/a;->b()Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    const/4 v9, 0x1

    :try_start_2
    new-instance v11, Ljava/io/FileInputStream;

    invoke-direct {v11, p2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    new-instance v10, Ljava/io/FileOutputStream;

    invoke-direct {v10, p3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :try_start_4
    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v3

    invoke-virtual {v10}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    invoke-static/range {v0 .. v8}, Lcom/twitter/media/filters/Filters;->nativeBlur(Landroid/content/Context;Landroid/content/res/AssetFileDescriptor;Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;IIZF)Z
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_7
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    move-result v0

    sget-object v1, Lcom/twitter/media/filters/Filters;->b:Lcom/twitter/media/filters/a;

    invoke-virtual {v1}, Lcom/twitter/media/filters/a;->c()Z

    sget-object v1, Lcom/twitter/media/filters/Filters;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    invoke-static {v11}, Lcom/twitter/media/MediaUtils;->a(Ljava/io/Closeable;)V

    invoke-static {v10}, Lcom/twitter/media/MediaUtils;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    sget-object v3, Lcom/twitter/media/filters/Filters;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    invoke-static {v2}, Lcom/twitter/media/MediaUtils;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/twitter/media/MediaUtils;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catch_0
    move-exception v3

    move-object v3, v2

    move-object v2, v1

    move v1, v0

    :goto_1
    const/4 v0, 0x0

    if-eqz v1, :cond_2

    sget-object v1, Lcom/twitter/media/filters/Filters;->b:Lcom/twitter/media/filters/a;

    invoke-virtual {v1}, Lcom/twitter/media/filters/a;->c()Z

    :cond_2
    sget-object v1, Lcom/twitter/media/filters/Filters;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    invoke-static {v3}, Lcom/twitter/media/MediaUtils;->a(Ljava/io/Closeable;)V

    invoke-static {v2}, Lcom/twitter/media/MediaUtils;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catch_1
    move-exception v3

    move v9, v0

    :goto_2
    const/4 v0, 0x0

    if-eqz v9, :cond_3

    sget-object v3, Lcom/twitter/media/filters/Filters;->b:Lcom/twitter/media/filters/a;

    invoke-virtual {v3}, Lcom/twitter/media/filters/a;->c()Z

    :cond_3
    sget-object v3, Lcom/twitter/media/filters/Filters;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    invoke-static {v2}, Lcom/twitter/media/MediaUtils;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/twitter/media/MediaUtils;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v3

    move v9, v0

    move-object v0, v3

    :goto_3
    if-eqz v9, :cond_4

    sget-object v3, Lcom/twitter/media/filters/Filters;->b:Lcom/twitter/media/filters/a;

    invoke-virtual {v3}, Lcom/twitter/media/filters/a;->c()Z

    :cond_4
    sget-object v3, Lcom/twitter/media/filters/Filters;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    invoke-static {v2}, Lcom/twitter/media/MediaUtils;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/twitter/media/MediaUtils;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3

    :catchall_2
    move-exception v0

    move-object v2, v11

    goto :goto_3

    :catchall_3
    move-exception v0

    move-object v1, v10

    move-object v2, v11

    goto :goto_3

    :catch_2
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v0

    move-object v2, v11

    goto :goto_2

    :catch_4
    move-exception v0

    move-object v1, v10

    move-object v2, v11

    goto :goto_2

    :catch_5
    move-exception v0

    move-object v3, v2

    move-object v2, v1

    move v1, v9

    goto :goto_1

    :catch_6
    move-exception v0

    move-object v2, v1

    move-object v3, v11

    move v1, v9

    goto :goto_1

    :catch_7
    move-exception v0

    move v1, v9

    move-object v2, v10

    move-object v3, v11

    goto :goto_1
.end method

.method private static b()Z
    .locals 1

    :try_start_0
    sget-object v0, Lcom/twitter/media/filters/Filters;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquire()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static native nativeBlur(Landroid/content/Context;Landroid/content/res/AssetFileDescriptor;Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;IIZF)Z
.end method

.method private static native nativeCreateDynamicImageSource(III)I
.end method

.method private static native nativeCreateStaticImageSource(ILandroid/content/res/AssetFileDescriptor;IIZIIII)I
.end method

.method private static native nativeDispose(I)V
.end method

.method private static native nativeDisposeImageSource(II)V
.end method

.method private static native nativeFilter(IIILandroid/graphics/Bitmap;FF)Z
.end method

.method private static native nativeFilterToFile(IIILjava/io/FileDescriptor;FF)Z
.end method

.method private static native nativeGetFilterIdentifier(II)Ljava/lang/String;
.end method

.method private static native nativeGetFilterName(II)Ljava/lang/String;
.end method

.method private static native nativeGetImageSourceTexId(II)I
.end method

.method private static native nativeGetNumFilters(I)I
.end method

.method private static native nativeInit(Landroid/content/Context;Landroid/content/res/AssetFileDescriptor;Ljava/lang/String;)I
.end method

.method private static native nativeRenderFilterPreview(IIIFFIF)Z
.end method


# virtual methods
.method public a(Landroid/net/Uri;IIZ)I
    .locals 9

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v6, v5

    move v7, v5

    move v8, v5

    invoke-virtual/range {v0 .. v8}, Lcom/twitter/media/filters/Filters;->a(Landroid/net/Uri;IIZIIII)I

    move-result v0

    return v0
.end method

.method public a(Landroid/net/Uri;IIZIIII)I
    .locals 10

    const/4 v9, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/media/filters/Filters;->d:Landroid/content/ContentResolver;

    const-string/jumbo v1, "r"

    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    invoke-static {}, Lcom/twitter/media/filters/Filters;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    sget v0, Lcom/twitter/media/filters/Filters;->a:I

    if-lez v0, :cond_1

    sget-object v0, Lcom/twitter/media/filters/Filters;->b:Lcom/twitter/media/filters/a;

    invoke-virtual {v0}, Lcom/twitter/media/filters/a;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :try_start_1
    sget v0, Lcom/twitter/media/filters/Filters;->a:I

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-static/range {v0 .. v8}, Lcom/twitter/media/filters/Filters;->nativeCreateStaticImageSource(ILandroid/content/res/AssetFileDescriptor;IIZIIII)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    :goto_0
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/twitter/media/filters/Filters;->e:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    sget-object v2, Lcom/twitter/media/filters/Filters;->b:Lcom/twitter/media/filters/a;

    invoke-virtual {v2}, Lcom/twitter/media/filters/a;->c()Z

    :goto_1
    sget-object v2, Lcom/twitter/media/filters/Filters;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->release()V

    :goto_2
    invoke-static {v1}, Lcom/twitter/media/MediaUtils;->a(Landroid/content/res/AssetFileDescriptor;)V

    :goto_3
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_3

    :catch_1
    move-exception v0

    move v0, v9

    goto :goto_0

    :cond_1
    move v0, v9

    goto :goto_1

    :cond_2
    move v0, v9

    goto :goto_2
.end method

.method public declared-synchronized a()V
    .locals 2

    monitor-enter p0

    :try_start_0
    sget v0, Lcom/twitter/media/filters/Filters;->a:I

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/media/filters/Filters;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/media/filters/Filters;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/twitter/media/filters/Filters;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    monitor-exit p0

    return-void
.end method

.method public a(I)V
    .locals 1

    invoke-static {}, Lcom/twitter/media/filters/Filters;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Lcom/twitter/media/filters/Filters;->a:I

    if-lez v0, :cond_0

    sget-object v0, Lcom/twitter/media/filters/Filters;->b:Lcom/twitter/media/filters/a;

    invoke-virtual {v0}, Lcom/twitter/media/filters/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    sget v0, Lcom/twitter/media/filters/Filters;->a:I

    invoke-static {v0, p1}, Lcom/twitter/media/filters/Filters;->nativeDisposeImageSource(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    sget-object v0, Lcom/twitter/media/filters/Filters;->b:Lcom/twitter/media/filters/a;

    invoke-virtual {v0}, Lcom/twitter/media/filters/a;->c()Z

    :cond_0
    sget-object v0, Lcom/twitter/media/filters/Filters;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    :cond_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public a(IIFFIF)Z
    .locals 8

    const/4 v7, 0x0

    invoke-static {}, Lcom/twitter/media/filters/Filters;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Lcom/twitter/media/filters/Filters;->a:I

    if-lez v0, :cond_0

    :try_start_0
    sget v0, Lcom/twitter/media/filters/Filters;->a:I

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-static/range {v0 .. v6}, Lcom/twitter/media/filters/Filters;->nativeRenderFilterPreview(IIIFFIF)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    sget-object v1, Lcom/twitter/media/filters/Filters;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    :goto_1
    return v0

    :catch_0
    move-exception v0

    move v0, v7

    goto :goto_0

    :cond_0
    move v0, v7

    goto :goto_0

    :cond_1
    move v0, v7

    goto :goto_1
.end method

.method public a(IILandroid/graphics/Bitmap;)Z
    .locals 6

    const/high16 v4, 0x3f800000    # 1.0f

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/media/filters/Filters;->a(IILandroid/graphics/Bitmap;FF)Z

    move-result v0

    return v0
.end method

.method public a(IILandroid/graphics/Bitmap;FF)Z
    .locals 7

    const/4 v6, 0x0

    invoke-static {}, Lcom/twitter/media/filters/Filters;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Lcom/twitter/media/filters/Filters;->a:I

    if-lez v0, :cond_0

    sget-object v0, Lcom/twitter/media/filters/Filters;->b:Lcom/twitter/media/filters/a;

    invoke-virtual {v0}, Lcom/twitter/media/filters/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    sget v0, Lcom/twitter/media/filters/Filters;->a:I

    move v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-static/range {v0 .. v5}, Lcom/twitter/media/filters/Filters;->nativeFilter(IIILandroid/graphics/Bitmap;FF)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    sget-object v1, Lcom/twitter/media/filters/Filters;->b:Lcom/twitter/media/filters/a;

    invoke-virtual {v1}, Lcom/twitter/media/filters/a;->c()Z

    move v6, v0

    :cond_0
    sget-object v0, Lcom/twitter/media/filters/Filters;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    :cond_1
    return v6

    :catch_0
    move-exception v0

    move v0, v6

    goto :goto_0
.end method

.method public a(IILjava/io/File;FF)Z
    .locals 8

    const/4 v6, 0x0

    const/4 v0, 0x0

    :try_start_0
    new-instance v7, Ljava/io/FileOutputStream;

    invoke-direct {v7, p3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v3

    invoke-static {}, Lcom/twitter/media/filters/Filters;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Lcom/twitter/media/filters/Filters;->a:I

    if-lez v0, :cond_0

    sget-object v0, Lcom/twitter/media/filters/Filters;->b:Lcom/twitter/media/filters/a;

    invoke-virtual {v0}, Lcom/twitter/media/filters/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_2
    sget v0, Lcom/twitter/media/filters/Filters;->a:I

    move v1, p1

    move v2, p2

    move v4, p4

    move v5, p5

    invoke-static/range {v0 .. v5}, Lcom/twitter/media/filters/Filters;->nativeFilterToFile(IIILjava/io/FileDescriptor;FF)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result v0

    :goto_0
    sget-object v1, Lcom/twitter/media/filters/Filters;->b:Lcom/twitter/media/filters/a;

    invoke-virtual {v1}, Lcom/twitter/media/filters/a;->c()Z

    move v6, v0

    :cond_0
    sget-object v0, Lcom/twitter/media/filters/Filters;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    :cond_1
    invoke-static {v7}, Lcom/twitter/media/MediaUtils;->a(Ljava/io/Closeable;)V

    :goto_1
    return v6

    :catch_0
    move-exception v1

    :goto_2
    invoke-static {v0}, Lcom/twitter/media/MediaUtils;->a(Ljava/io/Closeable;)V

    goto :goto_1

    :catch_1
    move-exception v0

    move v0, v6

    goto :goto_0

    :catch_2
    move-exception v0

    move-object v0, v7

    goto :goto_2
.end method

.method public a(Landroid/content/Context;Landroid/content/res/AssetFileDescriptor;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/media/filters/Filters;->d:Landroid/content/ContentResolver;

    invoke-static {}, Lcom/twitter/media/filters/Filters;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    :try_start_0
    sget v2, Lcom/twitter/media/filters/Filters;->a:I

    if-nez v2, :cond_4

    sget-object v2, Lcom/twitter/media/filters/Filters;->b:Lcom/twitter/media/filters/a;

    if-nez v2, :cond_1

    new-instance v2, Lcom/twitter/media/filters/a;

    invoke-direct {v2}, Lcom/twitter/media/filters/a;-><init>()V

    sput-object v2, Lcom/twitter/media/filters/Filters;->b:Lcom/twitter/media/filters/a;

    sget-object v2, Lcom/twitter/media/filters/Filters;->b:Lcom/twitter/media/filters/a;

    invoke-virtual {v2}, Lcom/twitter/media/filters/a;->a()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v1, 0x0

    sput-object v1, Lcom/twitter/media/filters/Filters;->b:Lcom/twitter/media/filters/a;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v1, Lcom/twitter/media/filters/Filters;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    :cond_0
    :goto_0
    return v0

    :cond_1
    :try_start_1
    sget-object v2, Lcom/twitter/media/filters/Filters;->b:Lcom/twitter/media/filters/a;

    invoke-virtual {v2}, Lcom/twitter/media/filters/a;->b()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-eqz v2, :cond_7

    :try_start_2
    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {p1, p2, v2}, Lcom/twitter/media/filters/Filters;->nativeInit(Landroid/content/Context;Landroid/content/res/AssetFileDescriptor;Ljava/lang/String;)I

    move-result v2

    sput v2, Lcom/twitter/media/filters/Filters;->a:I

    sget v2, Lcom/twitter/media/filters/Filters;->a:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-lez v2, :cond_2

    move v0, v1

    :cond_2
    :goto_1
    if-eqz v1, :cond_3

    sget-object v1, Lcom/twitter/media/filters/Filters;->b:Lcom/twitter/media/filters/a;

    invoke-virtual {v1}, Lcom/twitter/media/filters/a;->c()Z

    :cond_3
    sget-object v1, Lcom/twitter/media/filters/Filters;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    :cond_4
    move v3, v0

    move v0, v1

    move v1, v3

    goto :goto_1

    :catch_0
    move-exception v1

    move v1, v0

    :goto_2
    if-eqz v1, :cond_5

    sget-object v1, Lcom/twitter/media/filters/Filters;->b:Lcom/twitter/media/filters/a;

    invoke-virtual {v1}, Lcom/twitter/media/filters/a;->c()Z

    :cond_5
    sget-object v1, Lcom/twitter/media/filters/Filters;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v3, v1

    move v1, v0

    move-object v0, v3

    :goto_3
    if-eqz v1, :cond_6

    sget-object v1, Lcom/twitter/media/filters/Filters;->b:Lcom/twitter/media/filters/a;

    invoke-virtual {v1}, Lcom/twitter/media/filters/a;->c()Z

    :cond_6
    sget-object v1, Lcom/twitter/media/filters/Filters;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v2

    goto :goto_2

    :cond_7
    move v1, v0

    goto :goto_1
.end method

.method public b(I)Ljava/lang/String;
    .locals 2

    invoke-static {}, Lcom/twitter/media/filters/Filters;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Lcom/twitter/media/filters/Filters;->a:I

    if-lez v0, :cond_0

    sget v0, Lcom/twitter/media/filters/Filters;->a:I

    invoke-static {v0, p1}, Lcom/twitter/media/filters/Filters;->nativeGetFilterIdentifier(II)Ljava/lang/String;

    move-result-object v0

    :goto_0
    sget-object v1, Lcom/twitter/media/filters/Filters;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    :goto_1
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0

    :cond_1
    const-string/jumbo v0, ""

    goto :goto_1
.end method
