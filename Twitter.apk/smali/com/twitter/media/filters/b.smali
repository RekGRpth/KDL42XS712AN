.class public Lcom/twitter/media/filters/b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;


# instance fields
.field public a:F

.field public b:F

.field private c:Landroid/net/Uri;

.field private d:Z

.field private e:Z

.field private f:I

.field private g:I

.field private h:F

.field private i:I

.field private j:Lcom/twitter/media/filters/Filters;

.field private k:I

.field private l:I


# direct methods
.method private b()V
    .locals 9

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/media/filters/b;->c:Landroid/net/Uri;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/media/filters/b;->j:Lcom/twitter/media/filters/Filters;

    iget-object v1, p0, Lcom/twitter/media/filters/b;->c:Landroid/net/Uri;

    iget-boolean v4, p0, Lcom/twitter/media/filters/b;->d:Z

    move v3, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    invoke-virtual/range {v0 .. v8}, Lcom/twitter/media/filters/Filters;->a(Landroid/net/Uri;IIZIIII)I

    move-result v0

    iput v0, p0, Lcom/twitter/media/filters/b;->i:I

    iget v0, p0, Lcom/twitter/media/filters/b;->i:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/media/filters/b;->e:Z

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/twitter/media/filters/b;->k:I

    return v0
.end method

.method public a(F)V
    .locals 0

    iput p1, p0, Lcom/twitter/media/filters/b;->h:F

    return-void
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/twitter/media/filters/b;->f:I

    return-void
.end method

.method public b(I)V
    .locals 0

    iput p1, p0, Lcom/twitter/media/filters/b;->g:I

    return-void
.end method

.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 7

    iget-object v0, p0, Lcom/twitter/media/filters/b;->j:Lcom/twitter/media/filters/Filters;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/media/filters/b;->e:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/twitter/media/filters/b;->i:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/media/filters/b;->j:Lcom/twitter/media/filters/Filters;

    iget v1, p0, Lcom/twitter/media/filters/b;->i:I

    invoke-virtual {v0, v1}, Lcom/twitter/media/filters/Filters;->a(I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/media/filters/b;->i:I

    :cond_1
    invoke-direct {p0}, Lcom/twitter/media/filters/b;->b()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/media/filters/b;->j:Lcom/twitter/media/filters/Filters;

    iget v1, p0, Lcom/twitter/media/filters/b;->f:I

    iget v2, p0, Lcom/twitter/media/filters/b;->i:I

    iget v3, p0, Lcom/twitter/media/filters/b;->a:F

    iget v4, p0, Lcom/twitter/media/filters/b;->b:F

    iget v5, p0, Lcom/twitter/media/filters/b;->g:I

    iget v6, p0, Lcom/twitter/media/filters/b;->h:F

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/media/filters/Filters;->a(IIFFIF)Z

    goto :goto_0
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 3

    const/4 v2, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    invoke-static {v0, v0, v1, v1}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    const/16 v0, 0x4000

    invoke-static {v0}, Landroid/opengl/GLES20;->glClear(I)V

    invoke-static {v2, v2, p2, p3}, Landroid/opengl/GLES20;->glViewport(IIII)V

    iput p2, p0, Lcom/twitter/media/filters/b;->k:I

    iput p3, p0, Lcom/twitter/media/filters/b;->l:I

    return-void
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 2

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    invoke-static {v0, v0, v1, v1}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    const/16 v0, 0x4000

    invoke-static {v0}, Landroid/opengl/GLES20;->glClear(I)V

    const/16 v0, 0xb71

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    const/16 v0, 0xb44

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    return-void
.end method
