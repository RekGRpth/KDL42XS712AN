.class public Lcom/twitter/android/DialogFragmentActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Lcom/twitter/android/ru;
.implements Lcom/twitter/android/widget/ce;


# static fields
.field public static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/twitter/android/client/NotificationService;->c:Ljava/lang/String;

    sput-object v0, Lcom/twitter/android/DialogFragmentActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    const/4 v0, 0x1

    invoke-static {v0, p0}, Lcom/twitter/android/widget/PhoneVerificationDialog;->a(ILandroid/content/Context;)Lcom/twitter/android/widget/PhoneVerificationDialog;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/PhoneVerificationDialog;->a(Lcom/twitter/android/widget/ce;)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/content/DialogInterface$OnCancelListener;)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/DialogFragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    return-void
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 7

    const/4 v0, 0x0

    const-string/jumbo v1, "sb_account_name"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const-string/jumbo v3, "notif_service_intent"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Landroid/content/Intent;

    const-string/jumbo v3, "tweet"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/provider/ParcelableTweet;

    const/4 v5, 0x1

    move v4, v0

    invoke-static/range {v0 .. v6}, Lcom/twitter/android/widget/NotifyRetweetDialogFragment;->a(IJLcom/twitter/library/provider/ParcelableTweet;ZZLandroid/content/Intent;)Lcom/twitter/android/widget/NotifyRetweetDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/DialogFragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/NotifyRetweetDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    return-void
.end method


# virtual methods
.method public a(IJLcom/twitter/library/provider/ParcelableTweet;Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/DialogFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/android/client/NotificationService;->a(Landroid/content/Context;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/DialogFragmentActivity;->finish()V

    return-void
.end method

.method public a(IJLcom/twitter/library/provider/ParcelableTweet;ZLjava/lang/String;)V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/DialogFragmentActivity;->finish()V

    return-void
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/DialogFragmentActivity;->finish()V

    return-void
.end method

.method public b(IJLcom/twitter/library/provider/ParcelableTweet;Z)V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/DialogFragmentActivity;->finish()V

    return-void
.end method

.method public c(IJLcom/twitter/library/provider/ParcelableTweet;Z)V
    .locals 0

    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/DialogFragmentActivity;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/DialogFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/DialogFragmentActivity;->a:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/DialogFragmentActivity;->a(Landroid/os/Bundle;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string/jumbo v0, "blocked_needs_phone_verification"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/twitter/android/DialogFragmentActivity;->a()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/DialogFragmentActivity;->finish()V

    goto :goto_0
.end method
