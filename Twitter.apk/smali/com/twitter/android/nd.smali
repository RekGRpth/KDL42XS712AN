.class public Lcom/twitter/android/nd;
.super Landroid/widget/BaseAdapter;
.source "Twttr"


# instance fields
.field private a:Lcom/twitter/android/client/c;

.field private b:Landroid/support/v4/widget/CursorAdapter;

.field private c:Ljava/util/ArrayList;

.field private d:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/support/v4/widget/CursorAdapter;Lcom/twitter/android/client/c;)V
    .locals 2

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/nd;->d:Ljava/util/ArrayList;

    iput-object p2, p0, Lcom/twitter/android/nd;->a:Lcom/twitter/android/client/c;

    iput-object p1, p0, Lcom/twitter/android/nd;->b:Landroid/support/v4/widget/CursorAdapter;

    iget-object v0, p0, Lcom/twitter/android/nd;->b:Landroid/support/v4/widget/CursorAdapter;

    new-instance v1, Lcom/twitter/android/ne;

    invoke-direct {v1, p0}, Lcom/twitter/android/ne;-><init>(Lcom/twitter/android/nd;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/CursorAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    invoke-direct {p0}, Lcom/twitter/android/nd;->a()V

    return-void
.end method

.method private a(I)I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/nd;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/nd;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/nd;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/nd;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/nd;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ng;

    iget v0, v0, Lcom/twitter/android/ng;->b:I

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/maps/model/LatLng;DD)Lcom/google/android/gms/maps/model/LatLngBounds;
    .locals 6

    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v1, p1, Lcom/google/android/gms/maps/model/LatLng;->a:D

    add-double/2addr v1, p2

    iget-wide v3, p1, Lcom/google/android/gms/maps/model/LatLng;->b:D

    add-double/2addr v3, p4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, p1, Lcom/google/android/gms/maps/model/LatLng;->a:D

    sub-double/2addr v2, p2

    iget-wide v4, p1, Lcom/google/android/gms/maps/model/LatLng;->b:D

    sub-double/2addr v4, p4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    new-instance v2, Lcom/google/android/gms/maps/model/LatLngBounds;

    invoke-direct {v2, v1, v0}, Lcom/google/android/gms/maps/model/LatLngBounds;-><init>(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLng;)V

    return-object v2
.end method

.method private a()V
    .locals 15

    const/16 v14, 0xd

    const/16 v13, 0xc

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/nd;->b:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCount()I

    move-result v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_3

    iget-object v0, p0, Lcom/twitter/android/nd;->b:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    sget v7, Lcom/twitter/library/provider/cc;->g:I

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    const/4 v8, 0x4

    invoke-static {v7}, Lcom/twitter/library/provider/au;->e(I)Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v0, v13}, Landroid/database/Cursor;->isNull(I)Z

    move-result v9

    if-nez v9, :cond_2

    invoke-interface {v0, v14}, Landroid/database/Cursor;->isNull(I)Z

    move-result v9

    if-nez v9, :cond_2

    new-instance v7, Lcom/google/android/gms/maps/model/LatLng;

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v9

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v11

    invoke-direct {v7, v9, v10, v11, v12}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    const/16 v9, 0x1e

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/twitter/library/provider/ay;->b(I)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/twitter/android/ng;

    invoke-direct {v0, p0, v7, v1, v8}, Lcom/twitter/android/ng;-><init>(Lcom/twitter/android/nd;Lcom/google/android/gms/maps/model/LatLng;II)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/twitter/android/ng;

    invoke-direct {v0, p0, v7, v1, v2}, Lcom/twitter/android/ng;-><init>(Lcom/twitter/android/nd;Lcom/google/android/gms/maps/model/LatLng;II)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-static {v7}, Lcom/twitter/library/provider/au;->q(I)Z

    move-result v7

    if-eqz v7, :cond_0

    sget v7, Lcom/twitter/library/provider/cc;->M:I

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterTopic$LocalEvent;

    new-instance v7, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v8, v0, Lcom/twitter/library/api/TwitterTopic$LocalEvent;->latitude:D

    iget-wide v10, v0, Lcom/twitter/library/api/TwitterTopic$LocalEvent;->longitude:D

    invoke-direct {v7, v8, v9, v10, v11}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    const/4 v0, 0x2

    new-instance v8, Lcom/twitter/android/ng;

    invoke-direct {v8, p0, v7, v1, v0}, Lcom/twitter/android/ng;-><init>(Lcom/twitter/android/nd;Lcom/google/android/gms/maps/model/LatLng;II)V

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/nd;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/twitter/android/nd;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/twitter/android/nd;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/twitter/android/nd;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0}, Lcom/twitter/android/nd;->notifyDataSetChanged()V

    return-void
.end method

.method private a(ILjava/lang/String;Lcom/google/android/gms/maps/model/LatLng;IDDLandroid/content/res/Resources;Ljava/util/HashMap;Ljava/util/HashMap;Ljava/util/HashMap;)V
    .locals 9

    const/4 v2, 0x0

    const/4 v1, 0x0

    if-nez p1, :cond_3

    iget-object v3, p0, Lcom/twitter/android/nd;->a:Lcom/twitter/android/client/c;

    invoke-virtual {v3, p2}, Lcom/twitter/android/client/c;->g(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v3

    if-eqz v3, :cond_2

    move-object/from16 v0, p9

    invoke-static {v0, v3}, Lcom/twitter/android/nf;->a(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/maps/model/b;->a(Landroid/graphics/Bitmap;)Lcom/google/android/gms/maps/model/a;

    move-result-object v2

    move v7, v1

    move-object v1, v2

    :goto_0
    if-nez v1, :cond_0

    const/4 v1, 0x0

    move-object/from16 v0, p9

    invoke-static {v0, p1, v1}, Lcom/twitter/android/nf;->a(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/maps/model/b;->a(Landroid/graphics/Bitmap;)Lcom/google/android/gms/maps/model/a;

    move-result-object v1

    :cond_0
    if-eqz v1, :cond_1

    new-instance v2, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v2}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    invoke-virtual {v2, p3}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/a;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v1

    const/high16 v2, 0x3f000000    # 0.5f

    const/high16 v3, 0x3f000000    # 0.5f

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(FF)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Z)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/model/MarkerOptions;->b(Z)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v8

    move-object v1, p0

    move-object v2, p3

    move-wide v3, p5

    move-wide/from16 v5, p7

    invoke-direct/range {v1 .. v6}, Lcom/twitter/android/nd;->a(Lcom/google/android/gms/maps/model/LatLng;DD)Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v1

    move-object/from16 v0, p10

    invoke-virtual {v0, v1, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p11

    invoke-virtual {v0, v8, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v7, :cond_1

    move-object/from16 v0, p12

    invoke-virtual {v0, v8, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void

    :cond_2
    const/4 v1, 0x1

    move v7, v1

    move-object v1, v2

    goto :goto_0

    :cond_3
    move v7, v1

    move-object v1, v2

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/nd;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/nd;->a()V

    return-void
.end method


# virtual methods
.method public a(DDLandroid/content/res/Resources;Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 23

    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    const/4 v6, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/nd;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v6, v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/nd;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/ng;

    iget-object v5, v2, Lcom/twitter/android/ng;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget v3, v2, Lcom/twitter/android/ng;->c:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/nd;->b:Landroid/support/v4/widget/CursorAdapter;

    iget v2, v2, Lcom/twitter/android/ng;->b:I

    invoke-virtual {v4, v2}, Landroid/support/v4/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/database/Cursor;

    const/16 v4, 0x9

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v12}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v2, p0

    move-wide/from16 v7, p1

    move-wide/from16 v9, p3

    move-object/from16 v11, p5

    move-object/from16 v14, p6

    invoke-direct/range {v2 .. v14}, Lcom/twitter/android/nd;->a(ILjava/lang/String;Lcom/google/android/gms/maps/model/LatLng;IDDLandroid/content/res/Resources;Ljava/util/HashMap;Ljava/util/HashMap;Ljava/util/HashMap;)V

    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_0
    const v9, 0x7f7fffff    # Float.MAX_VALUE

    const/4 v2, 0x3

    new-array v0, v2, [F

    move-object/from16 v22, v0

    const/4 v8, 0x0

    invoke-virtual {v12}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_1
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/gms/maps/model/LatLngBounds;

    invoke-virtual {v7, v5}, Lcom/google/android/gms/maps/model/LatLngBounds;->a(Lcom/google/android/gms/maps/model/LatLng;)Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v2}, Lcom/google/android/gms/maps/model/MarkerOptions;->c()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v2

    iget-wide v14, v5, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-wide v0, v5, Lcom/google/android/gms/maps/model/LatLng;->b:D

    move-wide/from16 v16, v0

    iget-wide v0, v2, Lcom/google/android/gms/maps/model/LatLng;->a:D

    move-wide/from16 v18, v0

    iget-wide v0, v2, Lcom/google/android/gms/maps/model/LatLng;->b:D

    move-wide/from16 v20, v0

    invoke-static/range {v14 .. v22}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    const/4 v2, 0x0

    aget v2, v22, v2

    cmpg-float v2, v2, v9

    if-gez v2, :cond_9

    const/4 v2, 0x0

    aget v2, v22, v2

    :goto_3
    move-object v8, v7

    move v9, v2

    goto :goto_2

    :cond_2
    if-nez v8, :cond_3

    move-object/from16 v2, p0

    move-wide/from16 v7, p1

    move-wide/from16 v9, p3

    move-object/from16 v11, p5

    move-object/from16 v14, p6

    invoke-direct/range {v2 .. v14}, Lcom/twitter/android/nd;->a(ILjava/lang/String;Lcom/google/android/gms/maps/model/LatLng;IDDLandroid/content/res/Resources;Ljava/util/HashMap;Ljava/util/HashMap;Ljava/util/HashMap;)V

    goto :goto_1

    :cond_3
    invoke-virtual {v12, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v13, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/nd;->d:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/android/ng;

    const/4 v5, 0x0

    iget v7, v4, Lcom/twitter/android/ng;->c:I

    if-nez v7, :cond_7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/nd;->b:Landroid/support/v4/widget/CursorAdapter;

    iget v4, v4, Lcom/twitter/android/ng;->b:I

    invoke-virtual {v7, v4}, Landroid/support/v4/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/database/Cursor;

    const/16 v7, 0x9

    invoke-interface {v4, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/nd;->a:Lcom/twitter/android/client/c;

    invoke-virtual {v7, v4}, Lcom/twitter/android/client/c;->g(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v7

    if-eqz v7, :cond_6

    const/4 v4, 0x1

    move-object/from16 v0, p5

    invoke-static {v0, v7, v4}, Lcom/twitter/android/nf;->a(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/maps/model/b;->a(Landroid/graphics/Bitmap;)Lcom/google/android/gms/maps/model/a;

    move-result-object v4

    :goto_4
    if-nez v4, :cond_4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/nd;->d:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/android/ng;

    iget v4, v4, Lcom/twitter/android/ng;->c:I

    const/4 v5, 0x1

    move-object/from16 v0, p5

    invoke-static {v0, v4, v5}, Lcom/twitter/android/nf;->a(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/maps/model/b;->a(Landroid/graphics/Bitmap;)Lcom/google/android/gms/maps/model/a;

    move-result-object v4

    :cond_4
    invoke-virtual {v2, v4}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/a;)Lcom/google/android/gms/maps/model/MarkerOptions;

    :cond_5
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_6
    move-object/from16 v0, p6

    invoke-virtual {v0, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_7
    move-object v4, v5

    goto :goto_4

    :cond_8
    return-object v13

    :cond_9
    move-object v7, v8

    move v2, v9

    goto/16 :goto_3
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/nd;->c:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/twitter/android/nd;->notifyDataSetChanged()V

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/nd;->c:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/nd;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/nd;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/nd;->b:Landroid/support/v4/widget/CursorAdapter;

    invoke-direct {p0, p1}, Lcom/twitter/android/nd;->a(I)I

    move-result v1

    invoke-virtual {v0, v1, p2, p3}, Landroid/support/v4/widget/CursorAdapter;->getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/nd;->b:Landroid/support/v4/widget/CursorAdapter;

    invoke-direct {p0, p1}, Lcom/twitter/android/nd;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/nd;->b:Landroid/support/v4/widget/CursorAdapter;

    invoke-direct {p0, p1}, Lcom/twitter/android/nd;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/CursorAdapter;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/nd;->b:Landroid/support/v4/widget/CursorAdapter;

    invoke-direct {p0, p1}, Lcom/twitter/android/nd;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/CursorAdapter;->getItemViewType(I)I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/nd;->b:Landroid/support/v4/widget/CursorAdapter;

    invoke-direct {p0, p1}, Lcom/twitter/android/nd;->a(I)I

    move-result v1

    invoke-virtual {v0, v1, p2, p3}, Landroid/support/v4/widget/CursorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getViewTypeCount()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/nd;->b:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getViewTypeCount()I

    move-result v0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/nd;->b:Landroid/support/v4/widget/CursorAdapter;

    invoke-direct {p0, p1}, Lcom/twitter/android/nd;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/CursorAdapter;->isEnabled(I)Z

    move-result v0

    return v0
.end method
