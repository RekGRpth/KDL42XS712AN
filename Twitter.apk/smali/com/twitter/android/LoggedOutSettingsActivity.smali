.class public Lcom/twitter/android/LoggedOutSettingsActivity;
.super Lcom/twitter/android/client/BasePreferenceActivity;
.source "Twttr"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BasePreferenceActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/twitter/android/client/BasePreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f06000d    # com.twitter.android.R.xml.logged_out_preferences

    invoke-virtual {p0, v0}, Lcom/twitter/android/LoggedOutSettingsActivity;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/twitter/android/LoggedOutSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "advanced_proxy"

    invoke-virtual {p0, v1}, Lcom/twitter/android/LoggedOutSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/LoggedOutSettingsActivity;->d()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-static {p0, v1, v2}, Lju;->d(Landroid/content/Context;J)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    new-instance v1, Landroid/preference/Preference;

    invoke-direct {v1, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    const-string/jumbo v2, "advanced_experiments"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    const v2, 0x7f0f03df    # com.twitter.android.R.string.settings_experiment_title

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(I)V

    const v2, 0x7f0f03de    # com.twitter.android.R.string.settings_experiment_summary

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(I)V

    new-instance v2, Lcom/twitter/android/jc;

    invoke-direct {v2, p0}, Lcom/twitter/android/jc;-><init>(Lcom/twitter/android/LoggedOutSettingsActivity;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    :cond_1
    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f060008    # com.twitter.android.R.xml.debug_preferences

    invoke-virtual {p0, v0}, Lcom/twitter/android/LoggedOutSettingsActivity;->addPreferencesFromResource(I)V

    :cond_2
    return-void
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "advanced_proxy"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/ProxySettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/LoggedOutSettingsActivity;->startActivity(Landroid/content/Intent;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
