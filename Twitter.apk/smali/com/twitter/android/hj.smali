.class Lcom/twitter/android/hj;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/GalleryActivity;

.field private b:F


# direct methods
.method private constructor <init>(Lcom/twitter/android/GalleryActivity;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/hj;->a:Lcom/twitter/android/GalleryActivity;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/twitter/android/hj;->b:F

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/GalleryActivity;Lcom/twitter/android/gt;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/hj;-><init>(Lcom/twitter/android/GalleryActivity;)V

    return-void
.end method

.method private a(Landroid/view/ScaleGestureDetector;)F
    .locals 2

    iget v0, p0, Lcom/twitter/android/hj;->b:F

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v1

    mul-float/2addr v0, v1

    return v0
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 5

    invoke-direct {p0, p1}, Lcom/twitter/android/hj;->a(Landroid/view/ScaleGestureDetector;)F

    move-result v0

    float-to-double v1, v0

    const-wide v3, 0x3ff199999999999aL    # 1.1

    cmpl-double v1, v1, v3

    if-lez v1, :cond_0

    iget v1, p0, Lcom/twitter/android/hj;->b:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/hj;->a:Lcom/twitter/android/GalleryActivity;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/android/GalleryActivity;->a(I)V

    :goto_0
    invoke-super {p0, p1}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;->onScale(Landroid/view/ScaleGestureDetector;)Z

    move-result v0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/hj;->a:Lcom/twitter/android/GalleryActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/GalleryActivity;->a(I)V

    goto :goto_0
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 6

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-direct {p0, p1}, Lcom/twitter/android/hj;->a(Landroid/view/ScaleGestureDetector;)F

    move-result v0

    iget v1, p0, Lcom/twitter/android/hj;->b:F

    cmpg-float v1, v1, v5

    if-gtz v1, :cond_0

    float-to-double v1, v0

    const-wide v3, 0x3fe6666666666666L    # 0.7

    cmpg-double v1, v1, v3

    if-gtz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/hj;->a:Lcom/twitter/android/GalleryActivity;

    invoke-virtual {v1}, Lcom/twitter/android/GalleryActivity;->finish()V

    :cond_0
    invoke-static {v5, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/twitter/android/hj;->b:F

    invoke-super {p0, p1}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;->onScaleEnd(Landroid/view/ScaleGestureDetector;)V

    return-void
.end method
