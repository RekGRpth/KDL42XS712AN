.class Lcom/twitter/android/dr;
.super Landroid/support/v4/widget/CursorAdapter;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/DMConversationFragment;

.field private final b:I


# direct methods
.method public constructor <init>(Lcom/twitter/android/DMConversationFragment;)V
    .locals 3

    iput-object p1, p0, Lcom/twitter/android/dr;->a:Lcom/twitter/android/DMConversationFragment;

    invoke-virtual {p1}, Lcom/twitter/android/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {p0, v0, v1, v2}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    invoke-virtual {p1}, Lcom/twitter/android/DMConversationFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0092    # com.twitter.android.R.dimen.message_content_max_width

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/dr;->b:I

    return-void
.end method

.method private a(Landroid/database/Cursor;)I
    .locals 2

    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/dr;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/database/Cursor;Lcom/twitter/android/dv;Landroid/content/res/Resources;)V
    .locals 12

    const/4 v7, 0x5

    const/4 v11, 0x3

    const/4 v10, 0x0

    const/4 v9, 0x4

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    iget-object v5, p0, Lcom/twitter/android/dr;->a:Lcom/twitter/android/DMConversationFragment;

    invoke-virtual {v5, p1}, Lcom/twitter/android/DMConversationFragment;->b(Landroid/database/Cursor;)Z

    move-result v5

    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_2

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    sub-long v3, v7, v3

    invoke-virtual {v6, v3, v4}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v3

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    cmp-long v6, v0, v6

    if-nez v6, :cond_1

    const-wide/16 v6, 0x3c

    cmp-long v6, v3, v6

    if-gtz v6, :cond_1

    iget-object v6, p2, Lcom/twitter/android/dv;->c:Landroid/widget/TextView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v6, p2, Lcom/twitter/android/dv;->h:Lcom/twitter/android/widget/Divot;

    invoke-virtual {v6, v9}, Lcom/twitter/android/widget/Divot;->setVisibility(I)V

    if-eqz v5, :cond_0

    iget-object v5, p2, Lcom/twitter/android/dv;->i:Lcom/twitter/android/widget/Divot;

    invoke-virtual {v5, v9}, Lcom/twitter/android/widget/Divot;->setVisibility(I)V

    :cond_0
    invoke-direct {p0, v0, v1}, Lcom/twitter/android/dr;->a(J)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p2, Lcom/twitter/android/dv;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_1
    iget-object v0, p2, Lcom/twitter/android/dv;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    const v1, 0x7f0c005d    # com.twitter.android.R.dimen.dm_timestamp_margin_top

    invoke-virtual {p3, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    const-wide/16 v6, 0xe10

    cmp-long v1, v3, v6

    if-ltz v1, :cond_3

    const v1, 0x7f0c005b    # com.twitter.android.R.dimen.dm_timestamp_big_margin

    invoke-virtual {p3, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    :goto_0
    invoke-virtual {v0, v10, v5, v10, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v1, p2, Lcom/twitter/android/dv;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    invoke-interface {p1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    return-void

    :cond_3
    const v1, 0x7f0c005c    # com.twitter.android.R.dimen.dm_timestamp_margin_bottom

    invoke-virtual {p3, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    goto :goto_0
.end method

.method private a(Landroid/widget/ImageView;Lcom/twitter/library/api/conversations/DMPhoto;)V
    .locals 4

    invoke-virtual {p1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p0, Lcom/twitter/android/dr;->b:I

    iget v2, p2, Lcom/twitter/library/api/conversations/DMPhoto;->width:I

    int-to-float v2, v2

    iget v3, p2, Lcom/twitter/library/api/conversations/DMPhoto;->height:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    const/high16 v3, 0x3f400000    # 0.75f

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    const/high16 v3, 0x40400000    # 3.0f

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    int-to-float v3, v1

    div-float v2, v3, v2

    float-to-int v2, v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private a(Lcom/twitter/android/dv;Ljava/lang/StringBuilder;Lcom/twitter/library/api/conversations/DMMessage;I)V
    .locals 8

    const/4 v2, 0x0

    const/4 v7, 0x0

    const/16 v6, 0x8

    iget-object v0, p0, Lcom/twitter/android/dr;->a:Lcom/twitter/android/DMConversationFragment;

    invoke-static {v0}, Lcom/twitter/android/DMConversationFragment;->j(Lcom/twitter/android/DMConversationFragment;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/android/dv;->e:Lcom/twitter/library/util/m;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v2, p1, Lcom/twitter/android/dv;->e:Lcom/twitter/library/util/m;

    const-string/jumbo v0, "photo"

    invoke-virtual {p3, v0}, Lcom/twitter/library/api/conversations/DMMessage;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p3, Lcom/twitter/library/api/conversations/DMMessage;->attachment:Lcom/twitter/library/api/conversations/k;

    check-cast v0, Lcom/twitter/library/api/conversations/DMPhoto;

    iget-object v1, p0, Lcom/twitter/android/dr;->a:Lcom/twitter/android/DMConversationFragment;

    invoke-static {v1}, Lcom/twitter/android/DMConversationFragment;->k(Lcom/twitter/android/DMConversationFragment;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, v0, Lcom/twitter/library/api/conversations/DMPhoto;->mediaUrl:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/dr;->a:Lcom/twitter/android/DMConversationFragment;

    invoke-static {v1}, Lcom/twitter/android/DMConversationFragment;->k(Lcom/twitter/android/DMConversationFragment;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, v0, Lcom/twitter/library/api/conversations/DMPhoto;->mediaUrl:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/m;

    iput-object v1, p1, Lcom/twitter/android/dv;->e:Lcom/twitter/library/util/m;

    :goto_0
    iget-object v1, p1, Lcom/twitter/android/dv;->d:Landroid/widget/ImageView;

    invoke-direct {p0, v1, v0}, Lcom/twitter/android/dr;->a(Landroid/widget/ImageView;Lcom/twitter/library/api/conversations/DMPhoto;)V

    iget-object v1, p0, Lcom/twitter/android/dr;->a:Lcom/twitter/android/DMConversationFragment;

    invoke-static {v1}, Lcom/twitter/android/DMConversationFragment;->l(Lcom/twitter/android/DMConversationFragment;)Lcom/twitter/android/client/c;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    iget-object v2, p1, Lcom/twitter/android/dv;->e:Lcom/twitter/library/util/m;

    invoke-interface {v1, v2}, Lcom/twitter/library/widget/ap;->a(Lcom/twitter/library/util/m;)Landroid/graphics/Bitmap;

    move-result-object v1

    iget-object v2, p1, Lcom/twitter/android/dv;->d:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v1, p1, Lcom/twitter/android/dv;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/twitter/android/dr;->a:Lcom/twitter/android/DMConversationFragment;

    invoke-static {v1}, Lcom/twitter/android/DMConversationFragment;->j(Lcom/twitter/android/DMConversationFragment;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, p1, Lcom/twitter/android/dv;->e:Lcom/twitter/library/util/m;

    iget-object v3, p1, Lcom/twitter/android/dv;->d:Landroid/widget/ImageView;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2, p3}, Lcom/twitter/library/api/conversations/ae;->a(Ljava/lang/StringBuilder;Lcom/twitter/library/api/conversations/DMMessage;)V

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p1, Lcom/twitter/android/dv;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p1, Lcom/twitter/android/dv;->f:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    iget-object v1, p1, Lcom/twitter/android/dv;->d:Landroid/widget/ImageView;

    new-instance v2, Lcom/twitter/android/dt;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/dt;-><init>(Lcom/twitter/android/dr;Lcom/twitter/library/api/conversations/DMPhoto;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p1, Lcom/twitter/android/dv;->d:Landroid/widget/ImageView;

    new-instance v1, Lcom/twitter/android/du;

    invoke-direct {v1, p0, p4}, Lcom/twitter/android/du;-><init>(Lcom/twitter/android/dr;I)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    :goto_2
    return-void

    :cond_0
    new-instance v1, Lcom/twitter/library/util/m;

    iget-object v2, v0, Lcom/twitter/library/api/conversations/DMPhoto;->mediaUrl:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/android/dv;->d:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getWidth()I

    move-result v3

    iget v4, v0, Lcom/twitter/library/api/conversations/DMPhoto;->height:I

    const/4 v5, 0x1

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;IIZ)V

    iput-object v1, p1, Lcom/twitter/android/dv;->e:Lcom/twitter/library/util/m;

    goto :goto_0

    :cond_1
    iget-object v1, p1, Lcom/twitter/android/dv;->f:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_2
    iget-object v0, p1, Lcom/twitter/android/dv;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p1, Lcom/twitter/android/dv;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p1, Lcom/twitter/android/dv;->f:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method private a(J)Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/dr;->a:Lcom/twitter/android/DMConversationFragment;

    invoke-static {v0}, Lcom/twitter/android/DMConversationFragment;->n(Lcom/twitter/android/DMConversationFragment;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 14

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/twitter/android/dv;

    const/4 v1, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const/4 v1, 0x6

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v3}, Lcom/twitter/android/dr;->a(J)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    move v11, v1

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/dr;->a:Lcom/twitter/android/DMConversationFragment;

    invoke-static {v1}, Lcom/twitter/android/DMConversationFragment;->i(Lcom/twitter/android/DMConversationFragment;)Lcom/twitter/android/client/c;

    move-result-object v5

    if-eqz v11, :cond_4

    const/4 v1, 0x0

    if-eqz v4, :cond_0

    invoke-virtual {v5, v4}, Lcom/twitter/android/client/c;->g(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    :cond_0
    if-eqz v1, :cond_3

    iget-object v4, v9, Lcom/twitter/android/dv;->a:Landroid/widget/ImageView;

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_1
    iget-object v1, v9, Lcom/twitter/android/dv;->a:Landroid/widget/ImageView;

    new-instance v4, Lcom/twitter/android/ds;

    invoke-direct {v4, p0, v2, v3}, Lcom/twitter/android/ds;-><init>(Lcom/twitter/android/dr;J)V

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v9, Lcom/twitter/android/dv;->a:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_2
    const/4 v1, 0x7

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/twitter/library/api/conversations/DMMessage;

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    iget-object v1, v9, Lcom/twitter/android/dv;->b:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v10, Lcom/twitter/library/api/conversations/DMMessage;->text:Ljava/lang/String;

    if-nez v1, :cond_5

    const-string/jumbo v1, ""

    :goto_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    invoke-direct {p0, v9, v2, v10, v1}, Lcom/twitter/android/dr;->a(Lcom/twitter/android/dv;Ljava/lang/StringBuilder;Lcom/twitter/library/api/conversations/DMMessage;I)V

    iget-object v1, v9, Lcom/twitter/android/dv;->b:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v5}, Lcom/twitter/android/client/c;->V()F

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v13, v9, Lcom/twitter/android/dv;->b:Landroid/widget/TextView;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v10, Lcom/twitter/library/api/conversations/DMMessage;->entities:Lcom/twitter/library/api/TweetEntities;

    iget-object v3, p0, Lcom/twitter/android/dr;->a:Lcom/twitter/android/DMConversationFragment;

    const v4, 0x7f0b006b    # com.twitter.android.R.color.prefix

    invoke-virtual {v12, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    if-nez v11, :cond_6

    const/4 v7, 0x1

    :goto_4
    if-nez v11, :cond_7

    const/4 v8, 0x1

    :goto_5
    invoke-static/range {v1 .. v8}, Lcom/twitter/library/view/d;->a(Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/view/c;IIIZZ)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v13, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v9, Lcom/twitter/android/dv;->b:Landroid/widget/TextView;

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    if-nez v11, :cond_1

    iget-object v1, v9, Lcom/twitter/android/dv;->g:Landroid/widget/LinearLayout;

    const v2, 0x7f0b004b    # com.twitter.android.R.color.dm_sent_bg

    invoke-virtual {v12, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    iget-object v1, v9, Lcom/twitter/android/dv;->b:Landroid/widget/TextView;

    const v2, 0x7f0b004c    # com.twitter.android.R.color.dm_sent_content

    invoke-virtual {v12, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_1
    iget-object v1, v9, Lcom/twitter/android/dv;->h:Lcom/twitter/android/widget/Divot;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/Divot;->setVisibility(I)V

    iget-object v1, v9, Lcom/twitter/android/dv;->i:Lcom/twitter/android/widget/Divot;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/Divot;->setVisibility(I)V

    iget-object v1, v9, Lcom/twitter/android/dv;->c:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v9, Lcom/twitter/android/dv;->c:Landroid/widget/TextView;

    const v2, 0x7f0b0074    # com.twitter.android.R.color.secondary_text

    invoke-virtual {v12, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/twitter/android/dr;->a:Lcom/twitter/android/DMConversationFragment;

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Lcom/twitter/android/DMConversationFragment;->b(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_a

    check-cast v10, Lcom/twitter/library/api/conversations/DMLocalMessage;

    iget v1, v10, Lcom/twitter/library/api/conversations/DMLocalMessage;->status:I

    if-nez v1, :cond_8

    iget-object v1, v9, Lcom/twitter/android/dv;->c:Landroid/widget/TextView;

    const v2, 0x7f0f010e    # com.twitter.android.R.string.direct_message_sending

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    :goto_6
    move-object/from16 v0, p3

    invoke-direct {p0, v0, v9, v12}, Lcom/twitter/android/dr;->a(Landroid/database/Cursor;Lcom/twitter/android/dv;Landroid/content/res/Resources;)V

    return-void

    :cond_2
    const/4 v1, 0x0

    move v11, v1

    goto/16 :goto_0

    :cond_3
    iget-object v1, v9, Lcom/twitter/android/dv;->a:Landroid/widget/ImageView;

    const v4, 0x7f02003a    # com.twitter.android.R.drawable.bg_no_profile_photo_md

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    :cond_4
    iget-object v1, v9, Lcom/twitter/android/dv;->a:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    :cond_5
    iget-object v1, v10, Lcom/twitter/library/api/conversations/DMMessage;->text:Ljava/lang/String;

    goto/16 :goto_3

    :cond_6
    const/4 v7, 0x0

    goto/16 :goto_4

    :cond_7
    const/4 v8, 0x0

    goto/16 :goto_5

    :cond_8
    const/4 v2, 0x1

    if-ne v1, v2, :cond_9

    iget-object v1, v9, Lcom/twitter/android/dv;->c:Landroid/widget/TextView;

    const v2, 0x7f0f010e    # com.twitter.android.R.string.direct_message_sending

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    :goto_7
    iget-object v1, v9, Lcom/twitter/android/dv;->b:Landroid/widget/TextView;

    const v2, 0x7f0b0048    # com.twitter.android.R.color.dm_error_content

    invoke-virtual {v12, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, v9, Lcom/twitter/android/dv;->g:Landroid/widget/LinearLayout;

    const v2, 0x7f0b0047    # com.twitter.android.R.color.dm_error_bg

    invoke-virtual {v12, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    iget-object v1, v9, Lcom/twitter/android/dv;->h:Lcom/twitter/android/widget/Divot;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/Divot;->setVisibility(I)V

    iget-object v1, v9, Lcom/twitter/android/dv;->i:Lcom/twitter/android/widget/Divot;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/Divot;->setVisibility(I)V

    iget-object v1, v9, Lcom/twitter/android/dv;->c:Landroid/widget/TextView;

    const v2, 0x7f0b0045    # com.twitter.android.R.color.deep_red

    invoke-virtual {v12, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_6

    :cond_9
    iget-object v1, v9, Lcom/twitter/android/dv;->c:Landroid/widget/TextView;

    const v2, 0x7f0f010d    # com.twitter.android.R.string.direct_message_not_sent

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_7

    :cond_a
    const/4 v1, 0x5

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v2

    if-eqz v2, :cond_b

    iget-object v2, v9, Lcom/twitter/android/dv;->c:Landroid/widget/TextView;

    invoke-static {}, Lcom/twitter/android/DMConversationFragment;->q()Ljava/text/SimpleDateFormat;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    :cond_b
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    add-long/2addr v2, v4

    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v2

    if-eqz v2, :cond_c

    iget-object v2, v9, Lcom/twitter/android/dv;->c:Landroid/widget/TextView;

    const v3, 0x7f0f0572    # com.twitter.android.R.string.yesterday_with_time

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {}, Lcom/twitter/android/DMConversationFragment;->q()Ljava/text/SimpleDateFormat;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-virtual {v12, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    :cond_c
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/twitter/library/util/Util;->a(J)Z

    move-result v2

    if-eqz v2, :cond_d

    iget-object v2, v9, Lcom/twitter/android/dv;->c:Landroid/widget/TextView;

    invoke-static {}, Lcom/twitter/android/DMConversationFragment;->r()Ljava/text/SimpleDateFormat;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    :cond_d
    iget-object v2, v9, Lcom/twitter/android/dv;->c:Landroid/widget/TextView;

    invoke-static {}, Lcom/twitter/android/DMConversationFragment;->s()Ljava/text/SimpleDateFormat;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6
.end method

.method public getItemViewType(I)I
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/dr;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-direct {p0, v0}, Lcom/twitter/android/dr;->a(Landroid/database/Cursor;)I

    move-result v0

    return v0
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x3

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/dr;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030055    # com.twitter.android.R.layout.dm_thread_row_sent_view

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    move-object v1, v0

    :goto_0
    new-instance v2, Lcom/twitter/android/dv;

    invoke-direct {v2}, Lcom/twitter/android/dv;-><init>()V

    const v0, 0x7f09012d    # com.twitter.android.R.id.profile_image

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/twitter/android/dv;->a:Landroid/widget/ImageView;

    const v0, 0x7f090036    # com.twitter.android.R.id.content

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/twitter/android/dv;->b:Landroid/widget/TextView;

    iget-object v0, v2, Lcom/twitter/android/dv;->b:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/twitter/library/view/l;->a(Landroid/widget/TextView;)V

    const v0, 0x7f0900b6    # com.twitter.android.R.id.timestamp

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/twitter/android/dv;->c:Landroid/widget/TextView;

    const v0, 0x7f090077    # com.twitter.android.R.id.image

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/twitter/android/dv;->d:Landroid/widget/ImageView;

    const v0, 0x7f09012e    # com.twitter.android.R.id.image_border

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v2, Lcom/twitter/android/dv;->f:Landroid/view/View;

    const v0, 0x7f09012a    # com.twitter.android.R.id.bubble

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v2, Lcom/twitter/android/dv;->g:Landroid/widget/LinearLayout;

    const v0, 0x7f09012b    # com.twitter.android.R.id.bubble_divot

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/Divot;

    iput-object v0, v2, Lcom/twitter/android/dv;->h:Lcom/twitter/android/widget/Divot;

    const v0, 0x7f09012c    # com.twitter.android.R.id.bubble_divot_error

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/Divot;

    iput-object v0, v2, Lcom/twitter/android/dv;->i:Lcom/twitter/android/widget/Divot;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    return-object v1

    :cond_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030054    # com.twitter.android.R.layout.dm_thread_row_received_view

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    move-object v1, v0

    goto :goto_0
.end method
