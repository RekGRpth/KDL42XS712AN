.class public Lcom/twitter/android/ac;
.super Lcom/twitter/android/bl;
.source "Twttr"


# instance fields
.field private final a:J

.field private final b:I

.field private final c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;JII)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/twitter/android/bl;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    iput-wide p7, p0, Lcom/twitter/android/ac;->a:J

    iput p9, p0, Lcom/twitter/android/ac;->b:I

    iput p10, p0, Lcom/twitter/android/ac;->c:I

    return-void
.end method


# virtual methods
.method public a()Landroid/database/Cursor;
    .locals 7

    invoke-super {p0}, Lcom/twitter/android/bl;->a()Landroid/database/Cursor;

    move-result-object v1

    new-instance v0, Lcom/twitter/android/ActivityCursor;

    iget-wide v2, p0, Lcom/twitter/android/ac;->a:J

    invoke-virtual {p0}, Lcom/twitter/android/ac;->getContext()Landroid/content/Context;

    move-result-object v4

    iget v5, p0, Lcom/twitter/android/ac;->b:I

    iget v6, p0, Lcom/twitter/android/ac;->c:I

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ActivityCursor;-><init>(Landroid/database/Cursor;JLandroid/content/Context;II)V

    return-object v0
.end method

.method public synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ac;->a()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
