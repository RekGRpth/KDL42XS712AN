.class Lcom/twitter/android/vj;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/TrendsActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/TrendsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/vj;->a:Lcom/twitter/android/TrendsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6

    const/4 v3, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/vj;->a:Lcom/twitter/android/TrendsActivity;

    invoke-static {v0}, Lcom/twitter/android/TrendsActivity;->a(Lcom/twitter/android/TrendsActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/vj;->a:Lcom/twitter/android/TrendsActivity;

    invoke-virtual {v0}, Lcom/twitter/android/TrendsActivity;->f()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/vj;->a:Lcom/twitter/android/TrendsActivity;

    invoke-static {v0}, Lcom/twitter/android/TrendsActivity;->a(Lcom/twitter/android/TrendsActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    if-nez p3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/vj;->a:Lcom/twitter/android/TrendsActivity;

    invoke-static {v0}, Lcom/twitter/android/TrendsActivity;->b(Lcom/twitter/android/TrendsActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/vj;->a:Lcom/twitter/android/TrendsActivity;

    invoke-static {v0}, Lcom/twitter/android/TrendsActivity;->d(Lcom/twitter/android/TrendsActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/vj;->a:Lcom/twitter/android/TrendsActivity;

    invoke-static {v1}, Lcom/twitter/android/TrendsActivity;->c(Lcom/twitter/android/TrendsActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "trends:detail_view::detail_view:click"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/vj;->a:Lcom/twitter/android/TrendsActivity;

    invoke-static {v0}, Lcom/twitter/android/TrendsActivity;->f(Lcom/twitter/android/TrendsActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/vj;->a:Lcom/twitter/android/TrendsActivity;

    invoke-static {v1}, Lcom/twitter/android/TrendsActivity;->e(Lcom/twitter/android/TrendsActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "trends:list_view::list_view:click"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0
.end method
