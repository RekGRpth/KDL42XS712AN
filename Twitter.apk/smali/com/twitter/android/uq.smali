.class final Lcom/twitter/android/uq;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Lcom/twitter/android/widget/TextSwitcherView;

.field private final b:J

.field private final c:Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/twitter/android/widget/TextSwitcherView;JLjava/util/Map;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/uq;->a:Lcom/twitter/android/widget/TextSwitcherView;

    iput-wide p2, p0, Lcom/twitter/android/uq;->b:J

    iput-object p4, p0, Lcom/twitter/android/uq;->c:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/uq;->a:Lcom/twitter/android/widget/TextSwitcherView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/TextSwitcherView;->showNext()V

    iget-object v0, p0, Lcom/twitter/android/uq;->c:Ljava/util/Map;

    iget-wide v1, p0, Lcom/twitter/android/uq;->b:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
