.class Lcom/twitter/android/tp;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final c:Landroid/widget/TextView;

.field public final d:Landroid/widget/TextView;

.field public final e:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7f090090    # com.twitter.android.R.id.title

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/tp;->c:Landroid/widget/TextView;

    const v0, 0x7f0900da    # com.twitter.android.R.id.subtitle

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/tp;->d:Landroid/widget/TextView;

    const v0, 0x7f0900f4    # com.twitter.android.R.id.count

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/tp;->e:Landroid/widget/TextView;

    return-void
.end method

.method public static a(ILandroid/view/View;Landroid/view/ViewGroup;Lcom/twitter/android/to;F)Landroid/view/View;
    .locals 0

    if-nez p1, :cond_0

    invoke-static {p0, p2, p4}, Lcom/twitter/android/tp;->a(ILandroid/view/ViewGroup;F)Landroid/view/View;

    move-result-object p1

    :cond_0
    invoke-static {p1, p3}, Lcom/twitter/android/tp;->a(Landroid/view/View;Lcom/twitter/android/to;)V

    return-object p1
.end method

.method public static a(ILandroid/view/ViewGroup;F)Landroid/view/View;
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v0, p0, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/tp;

    invoke-direct {v1, v0}, Lcom/twitter/android/tp;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    const/4 v2, 0x0

    cmpl-float v2, p2, v2

    if-lez v2, :cond_0

    iget-object v1, v1, Lcom/twitter/android/tp;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v3, p2}, Landroid/widget/TextView;->setTextSize(IF)V

    :cond_0
    return-object v0
.end method

.method public static a(Landroid/view/View;Lcom/twitter/android/to;)V
    .locals 3

    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/tp;

    iget-object v1, v0, Lcom/twitter/android/tp;->c:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/twitter/android/to;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/twitter/android/tp;->e:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget v1, p1, Lcom/twitter/android/to;->a:I

    if-lez v1, :cond_1

    iget-object v2, v0, Lcom/twitter/android/tp;->e:Landroid/widget/TextView;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v0, Lcom/twitter/android/tp;->e:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, v0, Lcom/twitter/android/tp;->e:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
