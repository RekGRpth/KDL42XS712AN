.class public Lcom/twitter/android/TrendLocationsActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->c(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a(Z)V

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 4

    if-nez p1, :cond_0

    new-instance v0, Lcom/twitter/android/TrendLocationsFragment;

    invoke-direct {v0}, Lcom/twitter/android/TrendLocationsFragment;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/TrendLocationsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/twitter/android/TrendLocationsFragment;->a(Landroid/content/Intent;Z)Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "empty_desc"

    const v3, 0x7f0f03a9    # com.twitter.android.R.string.search_no_results

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/TrendLocationsFragment;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/TrendLocationsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_0
    return-void
.end method
