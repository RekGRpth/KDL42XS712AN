.class public Lcom/twitter/android/ChangePasswordActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field private a:Landroid/widget/EditText;

.field private b:Landroid/widget/EditText;

.field private c:Landroid/widget/EditText;

.field private d:Landroid/widget/Button;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:I

.field private h:I

.field private i:Lcom/twitter/library/client/Session;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic a([I)I
    .locals 1

    invoke-static {p0}, Lcom/twitter/android/ChangePasswordActivity;->b([I)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/ChangePasswordActivity;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->b:Landroid/widget/EditText;

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/ChangePasswordActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ChangePasswordActivity;->i:Lcom/twitter/library/client/Session;

    invoke-virtual {v0, v1, p2, p1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    return-void
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1

    const-string/jumbo v0, " "

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lcom/twitter/android/ChangePasswordActivity;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/ChangePasswordActivity;->b:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/twitter/android/ChangePasswordActivity;->e:Landroid/widget/TextView;

    const v3, 0x7f0f0443    # com.twitter.android.R.string.signup_error_password

    invoke-virtual {p0, v1, v2, v3}, Lcom/twitter/android/ChangePasswordActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;I)V

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/ChangePasswordActivity;->c:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/twitter/android/ChangePasswordActivity;->f:Landroid/widget/TextView;

    const v3, 0x7f0f02f0    # com.twitter.android.R.string.password_mismatch

    invoke-virtual {p0, v1, v2, v3}, Lcom/twitter/android/ChangePasswordActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;I)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/ChangePasswordActivity;->b:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/twitter/android/ChangePasswordActivity;->e:Landroid/widget/TextView;

    const v3, 0x7f0f0299    # com.twitter.android.R.string.new_password_same_as_old

    invoke-virtual {p0, v1, v2, v3}, Lcom/twitter/android/ChangePasswordActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;I)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static b([I)I
    .locals 2

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    array-length v1, p0

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    aget v0, p0, v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/ChangePasswordActivity;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->a:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/ChangePasswordActivity;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->c:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/ChangePasswordActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ChangePasswordActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method private f()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/ChangePasswordActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    const/4 v1, 0x6

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ChangePasswordActivity;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->c:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ChangePasswordActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ChangePasswordActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2, v0}, Lcom/twitter/android/ChangePasswordActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0, v1, v0}, Lcom/twitter/android/ChangePasswordActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const v1, 0x7f030027    # com.twitter.android.R.layout.change_password

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a(Z)V

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 6

    invoke-virtual {p0}, Lcom/twitter/android/ChangePasswordActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "account_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/ChangePasswordActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->i:Lcom/twitter/library/client/Session;

    new-instance v0, Lcom/twitter/android/cl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/cl;-><init>(Lcom/twitter/android/ChangePasswordActivity;Lcom/twitter/android/ck;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/ChangePasswordActivity;->a(Lcom/twitter/library/client/j;)V

    invoke-virtual {p0}, Lcom/twitter/android/ChangePasswordActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ChangePasswordActivity;->i:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "settings:change_password:::impression"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/ChangePasswordActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x106000c    # android.R.color.black

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/ChangePasswordActivity;->h:I

    const v1, 0x7f0b0056    # com.twitter.android.R.color.form_error

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ChangePasswordActivity;->g:I

    const v0, 0x7f0900cb    # com.twitter.android.R.id.old_password

    invoke-virtual {p0, v0}, Lcom/twitter/android/ChangePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->a:Landroid/widget/EditText;

    const v0, 0x7f0900cc    # com.twitter.android.R.id.new_password

    invoke-virtual {p0, v0}, Lcom/twitter/android/ChangePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->b:Landroid/widget/EditText;

    const v0, 0x7f0900ce    # com.twitter.android.R.id.new_password_confirm

    invoke-virtual {p0, v0}, Lcom/twitter/android/ChangePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->c:Landroid/widget/EditText;

    const v0, 0x7f0900d0    # com.twitter.android.R.id.update_password

    invoke-virtual {p0, v0}, Lcom/twitter/android/ChangePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->d:Landroid/widget/Button;

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->d:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    const v0, 0x7f0900cd    # com.twitter.android.R.id.new_password_err

    invoke-virtual {p0, v0}, Lcom/twitter/android/ChangePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->e:Landroid/widget/TextView;

    const v0, 0x7f0900cf    # com.twitter.android.R.id.new_password_confirm_err

    invoke-virtual {p0, v0}, Lcom/twitter/android/ChangePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->f:Landroid/widget/TextView;

    const v0, 0x7f0900d1    # com.twitter.android.R.id.password_reset

    invoke-virtual {p0, v0}, Lcom/twitter/android/ChangePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method a(Landroid/widget/EditText;Landroid/widget/TextView;I)V
    .locals 1

    if-eqz p3, :cond_0

    iget v0, p0, Lcom/twitter/android/ChangePasswordActivity;->g:I

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setTextColor(I)V

    invoke-virtual {p0, p3}, Lcom/twitter/android/ChangePasswordActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/twitter/android/ChangePasswordActivity;->h:I

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setTextColor(I)V

    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4

    const v3, 0x7f0f0443    # com.twitter.android.R.string.signup_error_password

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->b:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/twitter/android/ChangePasswordActivity;->e:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/ChangePasswordActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;I)V

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ChangePasswordActivity;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->b:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/twitter/android/ChangePasswordActivity;->e:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v1, v3}, Lcom/twitter/android/ChangePasswordActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;I)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->d:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/twitter/android/ChangePasswordActivity;->f()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->c:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/twitter/android/ChangePasswordActivity;->f:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/ChangePasswordActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;I)V

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ChangePasswordActivity;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->c:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/twitter/android/ChangePasswordActivity;->f:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v1, v3}, Lcom/twitter/android/ChangePasswordActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;I)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    const/4 v3, 0x1

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0900d0    # com.twitter.android.R.id.update_password

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/ChangePasswordActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ChangePasswordActivity;->i:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "settings:change_password::change_password:click"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/twitter/android/ChangePasswordActivity;->g()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v1, 0x7f0900d1    # com.twitter.android.R.id.password_reset

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ChangePasswordActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ChangePasswordActivity;->i:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "settings:change_password::forgot_password:click"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    const-string/jumbo v0, ""

    const v1, 0x7f0f02f4    # com.twitter.android.R.string.password_reset_url_refsrc_link

    invoke-static {p0, v0, v1}, Lcom/twitter/android/util/y;->a(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 4

    const v3, 0x7f0f0443    # com.twitter.android.R.string.signup_error_password

    const/4 v2, 0x6

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0900ce    # com.twitter.android.R.id.new_password_confirm

    if-ne v0, v1, :cond_2

    if-nez p2, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ChangePasswordActivity;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-ge v0, v2, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->c:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/twitter/android/ChangePasswordActivity;->f:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v1, v3}, Lcom/twitter/android/ChangePasswordActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const v1, 0x7f0900cc    # com.twitter.android.R.id.new_password

    if-ne v0, v1, :cond_1

    if-nez p2, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ChangePasswordActivity;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-ge v0, v2, :cond_1

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/ChangePasswordActivity;->b:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/twitter/android/ChangePasswordActivity;->e:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v1, v3}, Lcom/twitter/android/ChangePasswordActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;I)V

    goto :goto_0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
