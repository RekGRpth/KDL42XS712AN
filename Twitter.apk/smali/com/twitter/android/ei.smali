.class public Lcom/twitter/android/ei;
.super Lcom/twitter/android/ip;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/android/client/c;Lcom/twitter/library/widget/a;Ljava/util/Map;)V
    .locals 6

    const/4 v2, 0x2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/ip;-><init>(Landroid/content/Context;ILcom/twitter/android/client/c;Lcom/twitter/library/widget/a;Ljava/util/Map;)V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/widget/BaseUserView;Landroid/database/Cursor;J)V
    .locals 18

    const/4 v1, 0x7

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v1, 0x5

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v1, 0x4

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v1, 0x6

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v1, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-long v11, v1

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getPosition()I

    move-result v17

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-wide/from16 v3, p3

    invoke-virtual/range {v1 .. v17}, Lcom/twitter/android/ei;->a(Lcom/twitter/library/widget/BaseUserView;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/PromotedContent;JLjava/lang/String;IZZI)V

    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x3

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    check-cast p1, Lcom/twitter/library/widget/UserApprovalView;

    invoke-virtual {p0, p1, p3, v0, v1}, Lcom/twitter/android/ei;->a(Lcom/twitter/library/widget/BaseUserView;Landroid/database/Cursor;J)V

    iget-wide v2, p0, Lcom/twitter/android/ei;->j:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_0

    invoke-virtual {p1, v4}, Lcom/twitter/library/widget/UserApprovalView;->setState(I)V

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserApprovalView;->e()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/twitter/library/widget/UserApprovalView;->c()V

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ei;->a(J)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p1, v4}, Lcom/twitter/library/widget/UserApprovalView;->setState(I)V

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserApprovalView;->c()V

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/UserApprovalView;->setState(I)V

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserApprovalView;->e()V

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/UserApprovalView;->setState(I)V

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserApprovalView;->e()V

    goto :goto_0

    :cond_1
    invoke-virtual {p1, v4}, Lcom/twitter/library/widget/UserApprovalView;->setState(I)V

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserApprovalView;->c()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
