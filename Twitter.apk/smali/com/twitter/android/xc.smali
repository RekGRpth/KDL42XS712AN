.class Lcom/twitter/android/xc;
.super Lcom/twitter/android/yb;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/TweetFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/TweetFragment;Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/xc;->a:Lcom/twitter/android/TweetFragment;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/twitter/android/yb;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/MediaEntity;Lcom/twitter/library/widget/TweetView;)V
    .locals 2

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/yb;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/MediaEntity;Lcom/twitter/library/widget/TweetView;)V

    iget-object v0, p0, Lcom/twitter/android/xc;->c:Lcom/twitter/android/client/c;

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->S()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xc;->a:Lcom/twitter/android/TweetFragment;

    const-string/jumbo v1, "click"

    invoke-static {v0, v1, p1}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/android/TweetFragment;Ljava/lang/String;Lcom/twitter/library/provider/Tweet;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/TweetClassicCard;Lcom/twitter/library/widget/TweetView;)V
    .locals 2

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/yb;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/TweetClassicCard;Lcom/twitter/library/widget/TweetView;)V

    iget-object v0, p0, Lcom/twitter/android/xc;->c:Lcom/twitter/android/client/c;

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->S()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xc;->a:Lcom/twitter/android/TweetFragment;

    const-string/jumbo v1, "click"

    invoke-static {v0, v1, p1}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/android/TweetFragment;Ljava/lang/String;Lcom/twitter/library/provider/Tweet;)V

    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/widget/TweetView;)V
    .locals 4

    sget-object v0, Lcom/twitter/library/view/TweetActionType;->d:Lcom/twitter/library/view/TweetActionType;

    if-ne p1, v0, :cond_0

    invoke-static {}, Lkl;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xc;->a:Lcom/twitter/android/TweetFragment;

    const-string/jumbo v1, "tweet"

    const-string/jumbo v2, "non_focused_tweet"

    const-string/jumbo v3, "reply"

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/android/TweetFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/xc;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {p2}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/library/provider/Tweet;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/library/provider/Tweet;

    iget-object v0, p0, Lcom/twitter/android/xc;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/xl;

    move-result-object v0

    invoke-virtual {p2}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/library/provider/Tweet;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/xl;->a(Lcom/twitter/library/provider/Tweet;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/twitter/android/yb;->b(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/widget/TweetView;)V

    goto :goto_0
.end method
