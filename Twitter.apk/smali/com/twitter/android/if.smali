.class public Lcom/twitter/android/if;
.super Lcom/twitter/android/va;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/HomeTimelineFragment;


# direct methods
.method protected constructor <init>(Lcom/twitter/android/HomeTimelineFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/if;->a:Lcom/twitter/android/HomeTimelineFragment;

    invoke-direct {p0, p1}, Lcom/twitter/android/va;-><init>(Lcom/twitter/android/TimelineFragment;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;Lcom/twitter/library/api/TwitterUser;)V
    .locals 7

    const v6, 0x7f0f052d    # com.twitter.android.R.string.users_device_follow_success

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/if;->a:Lcom/twitter/android/HomeTimelineFragment;

    invoke-virtual {v0}, Lcom/twitter/android/HomeTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/twitter/library/platform/PushService;->f(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz p5, :cond_0

    const/16 v5, 0xc8

    if-ne p3, v5, :cond_0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/if;->a:Lcom/twitter/android/HomeTimelineFragment;

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p5}, Lcom/twitter/library/api/TwitterUser;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-virtual {v0, v6, v1}, Lcom/twitter/android/HomeTimelineFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    if-eqz p5, :cond_3

    const/16 v5, 0x3e9

    if-eq p3, v5, :cond_1

    if-nez v0, :cond_3

    :cond_1
    invoke-static {v3}, Lcom/twitter/android/client/aw;->a(Landroid/content/Context;)Lcom/twitter/android/client/aw;

    move-result-object v5

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v5, v4, v0}, Lcom/twitter/android/client/aw;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/twitter/android/if;->a:Lcom/twitter/android/HomeTimelineFragment;

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p5}, Lcom/twitter/library/api/TwitterUser;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-virtual {v0, v6, v1}, Lcom/twitter/android/HomeTimelineFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/if;->a:Lcom/twitter/android/HomeTimelineFragment;

    invoke-virtual {v0}, Lcom/twitter/android/HomeTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v2, 0x7f0f00ee    # com.twitter.android.R.string.default_error_message

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;I[ILjava/lang/String;J)V
    .locals 2

    invoke-super/range {p0 .. p7}, Lcom/twitter/android/va;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;I[ILjava/lang/String;J)V

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/if;->a:Lcom/twitter/android/HomeTimelineFragment;

    iget v1, v0, Lcom/twitter/android/HomeTimelineFragment;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/twitter/android/HomeTimelineFragment;->d:I

    iget-object v0, p0, Lcom/twitter/android/if;->a:Lcom/twitter/android/HomeTimelineFragment;

    invoke-static {v0}, Lcom/twitter/android/HomeTimelineFragment;->b(Lcom/twitter/android/HomeTimelineFragment;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;I[ILjava/lang/String;Lcom/twitter/library/api/TwitterUser;)V
    .locals 1

    invoke-super/range {p0 .. p6}, Lcom/twitter/android/va;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;I[ILjava/lang/String;Lcom/twitter/library/api/TwitterUser;)V

    if-eqz p6, :cond_0

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/if;->a:Lcom/twitter/android/HomeTimelineFragment;

    invoke-static {v0}, Lcom/twitter/android/HomeTimelineFragment;->b(Lcom/twitter/android/HomeTimelineFragment;)V

    :cond_0
    return-void
.end method

.method public c(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 2

    invoke-super/range {p0 .. p6}, Lcom/twitter/android/va;->c(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/if;->a:Lcom/twitter/android/HomeTimelineFragment;

    iget v1, v0, Lcom/twitter/android/HomeTimelineFragment;->d:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/twitter/android/HomeTimelineFragment;->d:I

    iget-object v0, p0, Lcom/twitter/android/if;->a:Lcom/twitter/android/HomeTimelineFragment;

    invoke-static {v0}, Lcom/twitter/android/HomeTimelineFragment;->b(Lcom/twitter/android/HomeTimelineFragment;)V

    :cond_0
    return-void
.end method
