.class public abstract Lcom/twitter/android/NotificationsBaseTimelineActivity;
.super Lcom/twitter/android/ListFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;
.implements Lcom/twitter/android/client/ag;


# instance fields
.field a:Lcom/twitter/library/widget/SlidingPanel;

.field b:Lcom/twitter/android/widget/dt;

.field c:Lcom/twitter/android/oa;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/ListFragmentActivity;-><init>()V

    return-void
.end method

.method protected static a(Landroid/content/Context;Lcom/twitter/library/client/aa;)I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/util/ah;->a(Lcom/twitter/library/api/TwitterUser;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0}, Lcom/twitter/android/util/ah;->a(Landroid/content/Context;)I

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {}, Lgo;->a()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/twitter/library/client/f;

    invoke-virtual {p1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const-string/jumbo v2, "notifications_follow_only"

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const v1, 0x7f0300d2    # com.twitter.android.R.layout.notification_activity

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    return-object v0
.end method

.method protected a(Landroid/content/Intent;Lcom/twitter/library/client/e;)Lcom/twitter/android/iu;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected a(Landroid/content/Intent;)Ljava/lang/CharSequence;
    .locals 1

    const v0, 0x7f0f029f    # com.twitter.android.R.string.notif_center_title

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationsBaseTimelineActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 2

    const v0, 0x7f0901f5    # com.twitter.android.R.id.sliding_notification_panel

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationsBaseTimelineActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/SlidingPanel;

    new-instance v1, Lcom/twitter/android/oa;

    invoke-direct {v1, v0, p0}, Lcom/twitter/android/oa;-><init>(Lcom/twitter/library/widget/SlidingPanel;Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    iput-object v1, p0, Lcom/twitter/android/NotificationsBaseTimelineActivity;->c:Lcom/twitter/android/oa;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SlidingPanel;->b(I)V

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    iput-object v0, p0, Lcom/twitter/android/NotificationsBaseTimelineActivity;->a:Lcom/twitter/library/widget/SlidingPanel;

    invoke-super {p0, p1, p2}, Lcom/twitter/android/ListFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V

    return-void
.end method

.method protected a(Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/android/ListFragmentActivity;->a(Lcom/twitter/internal/android/widget/ToolBar;)Z

    const/4 v0, 0x1

    return v0
.end method

.method protected a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 4

    invoke-super {p0, p1, p2}, Lcom/twitter/android/ListFragmentActivity;->a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z

    invoke-virtual {p0}, Lcom/twitter/android/NotificationsBaseTimelineActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/util/ah;->a(Lcom/twitter/library/api/TwitterUser;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f11001a    # com.twitter.android.R.menu.notification_results

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    invoke-static {p0}, Lcom/twitter/android/widget/dt;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/NotificationsBaseTimelineActivity;->b:Lcom/twitter/android/widget/dt;

    if-nez v0, :cond_0

    const v0, 0x7f090326    # com.twitter.android.R.id.menu_notification_filter_slideup

    invoke-virtual {p2, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/widget/dt;

    invoke-virtual {p0}, Lcom/twitter/android/NotificationsBaseTimelineActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v2

    invoke-virtual {v0}, Lhn;->d()Landroid/view/View;

    move-result-object v3

    invoke-direct {v1, v2, p0, v3}, Lcom/twitter/android/widget/dt;-><init>(Lcom/twitter/android/client/c;Lcom/twitter/android/NotificationsBaseTimelineActivity;Landroid/view/View;)V

    iput-object v1, p0, Lcom/twitter/android/NotificationsBaseTimelineActivity;->b:Lcom/twitter/android/widget/dt;

    iget-object v1, p0, Lcom/twitter/android/NotificationsBaseTimelineActivity;->b:Lcom/twitter/android/widget/dt;

    new-instance v2, Lcom/twitter/android/nz;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/nz;-><init>(Lcom/twitter/android/NotificationsBaseTimelineActivity;Lhn;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/dt;->a(Lcom/twitter/android/widget/dz;)V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/NotificationsBaseTimelineActivity;->f()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationsBaseTimelineActivity;->e(I)V

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public a(Lhn;)Z
    .locals 7

    const/4 v0, 0x1

    invoke-virtual {p1}, Lhn;->a()I

    move-result v1

    const v2, 0x7f090326    # com.twitter.android.R.id.menu_notification_filter_slideup

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/twitter/android/NotificationsBaseTimelineActivity;->b:Lcom/twitter/android/widget/dt;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/NotificationsBaseTimelineActivity;->b:Lcom/twitter/android/widget/dt;

    invoke-virtual {v1}, Lcom/twitter/android/widget/dt;->a()V

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/NotificationsBaseTimelineActivity;->a:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v1}, Lcom/twitter/library/widget/SlidingPanel;->e()Z

    invoke-virtual {p0}, Lcom/twitter/android/NotificationsBaseTimelineActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/NotificationsBaseTimelineActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v6, "notification:universal:filter_sheet::impression"

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Lcom/twitter/android/ListFragmentActivity;->a(Lhn;)Z

    move-result v0

    goto :goto_0
.end method

.method protected c()V
    .locals 6

    invoke-virtual {p0}, Lcom/twitter/android/NotificationsBaseTimelineActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/NotificationsBaseTimelineActivity;->a(Landroid/content/Intent;Lcom/twitter/library/client/e;)Lcom/twitter/android/iu;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/NotificationsBaseTimelineActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    const/16 v3, 0x1003

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentTransaction;->setTransition(I)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    const v3, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    iget-object v4, v0, Lcom/twitter/android/iu;->a:Lcom/twitter/android/client/BaseListFragment;

    iget-object v5, v0, Lcom/twitter/android/iu;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    iget-object v0, v0, Lcom/twitter/android/iu;->a:Lcom/twitter/android/client/BaseListFragment;

    invoke-virtual {v0}, Lcom/twitter/android/client/BaseListFragment;->Z()V

    iget-object v0, p0, Lcom/twitter/android/NotificationsBaseTimelineActivity;->a:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->c()Z

    return-void
.end method

.method public d()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/NotificationsBaseTimelineActivity;->b:Lcom/twitter/android/widget/dt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/NotificationsBaseTimelineActivity;->b:Lcom/twitter/android/widget/dt;

    const-wide/16 v1, 0x5dc

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/widget/dt;->a(J)V

    :cond_0
    return-void
.end method

.method protected f()I
    .locals 1

    invoke-static {p0}, Lcom/twitter/android/util/ah;->a(Landroid/content/Context;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const v0, 0x7f0f02c3    # com.twitter.android.R.string.notification_filter_none

    :goto_0
    return v0

    :pswitch_0
    const v0, 0x7f0f02c0    # com.twitter.android.R.string.notification_filter_filtered

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0f02c1    # com.twitter.android.R.string.notification_filter_follow_only

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0f02c5    # com.twitter.android.R.string.notification_filter_verified

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 2

    const v0, 0x7f0901f9    # com.twitter.android.R.id.notification_filter_filtered

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {p0, v0}, Lcom/twitter/android/util/ah;->a(Landroid/content/Context;I)V

    iget-object v1, p0, Lcom/twitter/android/NotificationsBaseTimelineActivity;->c:Lcom/twitter/android/oa;

    invoke-virtual {v1, v0}, Lcom/twitter/android/oa;->a(I)V

    invoke-virtual {p0}, Lcom/twitter/android/NotificationsBaseTimelineActivity;->f()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationsBaseTimelineActivity;->e(I)V

    invoke-virtual {p0}, Lcom/twitter/android/NotificationsBaseTimelineActivity;->c()V

    return-void

    :cond_0
    const v0, 0x7f0901fa    # com.twitter.android.R.id.notification_filter_follow_only

    if-ne p2, v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const v0, 0x7f0901fb    # com.twitter.android.R.id.notification_filter_verified

    if-ne p2, v0, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onGlobalLayout()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/NotificationsBaseTimelineActivity;->a:Lcom/twitter/library/widget/SlidingPanel;

    const v1, 0x7f0901fc    # com.twitter.android.R.id.notification_filter_header

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    const v2, 0x7f0901f6    # com.twitter.android.R.id.notification_filter_content

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SlidingPanel;->setPanelPreviewHeight(I)V

    return-void
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/ListFragmentActivity;->onResume()V

    invoke-static {p0}, Lcom/twitter/android/util/ah;->a(Landroid/content/Context;)I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/NotificationsBaseTimelineActivity;->c:Lcom/twitter/android/oa;

    invoke-virtual {v1, v0}, Lcom/twitter/android/oa;->a(I)V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/ListFragmentActivity;->onStart()V

    invoke-static {p0}, Lcom/twitter/android/client/aw;->a(Landroid/content/Context;)Lcom/twitter/android/client/aw;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/NotificationsBaseTimelineActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/aw;->c(Ljava/lang/String;)V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 3

    invoke-super {p0, p1}, Lcom/twitter/android/ListFragmentActivity;->onWindowFocusChanged(Z)V

    iget-object v0, p0, Lcom/twitter/android/NotificationsBaseTimelineActivity;->b:Lcom/twitter/android/widget/dt;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/NotificationsBaseTimelineActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/twitter/android/widget/dt;->b(J)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/NotificationsBaseTimelineActivity;->b:Lcom/twitter/android/widget/dt;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/dt;->a(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/NotificationsBaseTimelineActivity;->b:Lcom/twitter/android/widget/dt;

    const-wide/16 v1, 0x1388

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/widget/dt;->a(J)V

    goto :goto_0
.end method
