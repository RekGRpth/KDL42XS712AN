.class public Lcom/twitter/android/tq;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:J

.field private b:I

.field private c:Landroid/content/Context;

.field private d:Lcom/twitter/library/scribe/ScribeItem;

.field private e:J


# direct methods
.method public constructor <init>(Landroid/content/Context;JILcom/twitter/library/scribe/ScribeItem;J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/tq;->c:Landroid/content/Context;

    iput-wide p2, p0, Lcom/twitter/android/tq;->a:J

    iput p4, p0, Lcom/twitter/android/tq;->b:I

    iput-object p5, p0, Lcom/twitter/android/tq;->d:Lcom/twitter/library/scribe/ScribeItem;

    iput-wide p6, p0, Lcom/twitter/android/tq;->e:J

    return-void
.end method


# virtual methods
.method public a(JILcom/twitter/library/scribe/ScribeItem;J)V
    .locals 0

    iput-wide p1, p0, Lcom/twitter/android/tq;->a:J

    iput p3, p0, Lcom/twitter/android/tq;->b:I

    iput-object p4, p0, Lcom/twitter/android/tq;->d:Lcom/twitter/library/scribe/ScribeItem;

    iput-wide p5, p0, Lcom/twitter/android/tq;->e:J

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    const/4 v1, 0x0

    const/4 v3, 0x0

    iget v0, p0, Lcom/twitter/android/tq;->b:I

    packed-switch v0, :pswitch_data_0

    move-object v0, v1

    move v2, v3

    :goto_0
    if-lez v2, :cond_0

    iget-wide v4, p0, Lcom/twitter/android/tq;->a:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    new-instance v4, Landroid/content/Intent;

    iget-object v5, p0, Lcom/twitter/android/tq;->c:Landroid/content/Context;

    const-class v6, Lcom/twitter/android/ActivityDetailActivity;

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v5, "event_type"

    iget v6, p0, Lcom/twitter/android/tq;->b:I

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v4

    const-string/jumbo v5, "user_tag"

    iget-wide v6, p0, Lcom/twitter/android/tq;->a:J

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v4

    const-string/jumbo v5, "hide_action_button"

    invoke-virtual {v4, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v4

    const-string/jumbo v5, "title_res_id"

    invoke-virtual {v4, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v4, "status_tag"

    iget-wide v5, p0, Lcom/twitter/android/tq;->a:J

    invoke-virtual {v2, v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v4, "magic_rec_id"

    iget-wide v5, p0, Lcom/twitter/android/tq;->e:J

    invoke-virtual {v2, v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v2

    iget-object v4, p0, Lcom/twitter/android/tq;->c:Landroid/content/Context;

    invoke-static {v4}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v4

    new-instance v5, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v6, p0, Lcom/twitter/android/tq;->c:Landroid/content/Context;

    invoke-static {v6}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/String;

    aput-object v0, v6, v3

    const/4 v0, 0x1

    aput-object v1, v6, v0

    const/4 v0, 0x2

    const-string/jumbo v3, "magic_rec_bar"

    aput-object v3, v6, v0

    const/4 v0, 0x3

    aput-object v1, v6, v0

    const/4 v0, 0x4

    const-string/jumbo v1, "click"

    aput-object v1, v6, v0

    invoke-virtual {v5, v6}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/tq;->d:Lcom/twitter/library/scribe/ScribeItem;

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    iget-object v0, p0, Lcom/twitter/android/tq;->c:Landroid/content/Context;

    invoke-virtual {v0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void

    :pswitch_0
    const v2, 0x7f0f0014    # com.twitter.android.R.string.activity_retweeted

    const-string/jumbo v0, "tweet"

    goto/16 :goto_0

    :pswitch_1
    const v2, 0x7f0f0011    # com.twitter.android.R.string.activity_favorited

    const-string/jumbo v0, "tweet"

    goto/16 :goto_0

    :pswitch_2
    const v2, 0x7f0f0013    # com.twitter.android.R.string.activity_followed

    const-string/jumbo v0, "profile"

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x12
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
