.class public Lcom/twitter/android/ii;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:I

.field private c:Z

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/ii;->b:I

    iput-boolean v1, p0, Lcom/twitter/android/ii;->c:Z

    iput-boolean v1, p0, Lcom/twitter/android/ii;->d:Z

    iput-object p1, p0, Lcom/twitter/android/ii;->a:Landroid/content/Context;

    return-void
.end method

.method private a(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/twitter/android/ii;->c()I

    move-result v0

    iget v1, p0, Lcom/twitter/android/ii;->b:I

    if-ne v0, v1, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    iput v0, p0, Lcom/twitter/android/ii;->b:I

    iget v0, p0, Lcom/twitter/android/ii;->b:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Lcom/twitter/library/network/u;

    iget-object v1, p0, Lcom/twitter/android/ii;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/twitter/library/network/u;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/h;)V

    :cond_1
    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/twitter/android/ii;->d()Lcom/twitter/internal/network/h;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/h;)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/twitter/library/network/v;

    iget-object v1, p0, Lcom/twitter/android/ii;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/twitter/library/network/v;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/h;)V

    goto :goto_0

    :pswitch_2
    new-instance v0, Lcom/twitter/library/network/w;

    iget-object v1, p0, Lcom/twitter/android/ii;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/twitter/library/network/w;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/h;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Lcom/twitter/android/ii;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/ii;->c:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/android/ii;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/ii;->d:Z

    return p1
.end method

.method private c()I
    .locals 1

    invoke-static {}, Lcom/twitter/library/client/App;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/ii;->d:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lcom/twitter/android/ii;->c:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/android/ii;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/ii;->a(Z)V

    return-void
.end method

.method private d()Lcom/twitter/internal/network/h;
    .locals 4

    :try_start_0
    const-string/jumbo v0, "com.twitter.library.network.debug.DebugHttpOperationClientFactory"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Landroid/content/Context;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/ii;->a:Landroid/content/Context;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/network/h;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Could not initialize com.twitter.library.network.debug.DebugHttpOperationClientFactory"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/ii;->a(Z)V

    new-instance v0, Lcom/twitter/android/ij;

    invoke-direct {v0, p0}, Lcom/twitter/android/ij;-><init>(Lcom/twitter/android/ii;)V

    invoke-static {v0}, Lcom/twitter/library/featureswitch/a;->a(Lcom/twitter/library/featureswitch/g;)V

    return-void
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/android/ii;->a(Z)V

    return-void
.end method
