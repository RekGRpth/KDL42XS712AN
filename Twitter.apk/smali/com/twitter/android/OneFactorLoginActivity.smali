.class public Lcom/twitter/android/OneFactorLoginActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field a:Lcom/twitter/android/of;

.field private b:Landroid/widget/Button;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field private f:Ljava/lang/String;

.field private g:Lcom/twitter/library/client/Session;

.field private h:Lcom/twitter/library/network/OneFactorLoginResponse;

.field private i:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

.field private j:Ljava/lang/String;

.field private k:Lcom/twitter/android/oe;

.field private l:Landroid/os/Handler;

.field private m:Ljava/lang/Runnable;

.field private n:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    new-instance v0, Lcom/twitter/android/of;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/of;-><init>(Lcom/twitter/android/OneFactorLoginActivity;Lcom/twitter/android/od;)V

    iput-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->a:Lcom/twitter/android/of;

    new-instance v0, Lcom/twitter/android/oe;

    invoke-direct {v0, p0}, Lcom/twitter/android/oe;-><init>(Lcom/twitter/android/OneFactorLoginActivity;)V

    iput-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->k:Lcom/twitter/android/oe;

    new-instance v0, Lcom/twitter/android/od;

    invoke-direct {v0, p0}, Lcom/twitter/android/od;-><init>(Lcom/twitter/android/OneFactorLoginActivity;)V

    iput-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->m:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->g:Lcom/twitter/library/client/Session;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/OneFactorLoginActivity;Lcom/twitter/library/client/Session;)Lcom/twitter/library/client/Session;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/OneFactorLoginActivity;->g:Lcom/twitter/library/client/Session;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/OneFactorLoginActivity;Lcom/twitter/library/network/OneFactorLoginResponse;)Lcom/twitter/library/network/OneFactorLoginResponse;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/OneFactorLoginActivity;->h:Lcom/twitter/library/network/OneFactorLoginResponse;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/OneFactorLoginActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/OneFactorLoginActivity;->j:Ljava/lang/String;

    return-object p1
.end method

.method private a(IIZZZ)V
    .locals 4

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->n:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->b:Landroid/widget/Button;

    invoke-virtual {v0, p2}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->b:Landroid/widget/Button;

    invoke-virtual {v0, p3}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v3, p0, Lcom/twitter/android/OneFactorLoginActivity;->d:Landroid/view/View;

    if-eqz p5, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    if-eqz p5, :cond_1

    iget-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    iget-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->c:Landroid/widget/TextView;

    const/4 v3, 0x1

    invoke-static {p0, v0, v3}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;Landroid/view/View;Z)V

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->e:Landroid/view/View;

    if-eqz p4, :cond_2

    :goto_2
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->c:Landroid/widget/TextView;

    invoke-static {p0, v0, v1}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;Landroid/view/View;Z)V

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method private a(Lcom/twitter/android/OneFactorLoginActivity$LoginState;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/OneFactorLoginActivity;->i:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    invoke-virtual {p1, p0}, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->a(Lcom/twitter/android/OneFactorLoginActivity;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/OneFactorLoginActivity;IIZZZ)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/twitter/android/OneFactorLoginActivity;->a(IIZZZ)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/OneFactorLoginActivity;Lcom/twitter/android/OneFactorLoginActivity$LoginState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/OneFactorLoginActivity;->a(Lcom/twitter/android/OneFactorLoginActivity$LoginState;)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/OneFactorLoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/android/OneFactorLoginActivity$LoginState;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->i:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/OneFactorLoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/OneFactorLoginActivity;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->m:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/OneFactorLoginActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->l:Landroid/os/Handler;

    return-object v0
.end method

.method private f()V
    .locals 8

    sget-object v0, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->e:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    invoke-direct {p0, v0}, Lcom/twitter/android/OneFactorLoginActivity;->a(Lcom/twitter/android/OneFactorLoginActivity$LoginState;)V

    iget-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/twitter/android/OneFactorLoginActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/OneFactorLoginActivity;->g:Lcom/twitter/library/client/Session;

    iget-object v2, p0, Lcom/twitter/android/OneFactorLoginActivity;->h:Lcom/twitter/library/network/OneFactorLoginResponse;

    iget-wide v2, v2, Lcom/twitter/library/network/OneFactorLoginResponse;->b:J

    iget-object v4, p0, Lcom/twitter/android/OneFactorLoginActivity;->f:Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/android/OneFactorLoginActivity;->h:Lcom/twitter/library/network/OneFactorLoginResponse;

    iget-object v5, v5, Lcom/twitter/library/network/OneFactorLoginResponse;->a:Ljava/lang/String;

    iget-object v7, p0, Lcom/twitter/android/OneFactorLoginActivity;->k:Lcom/twitter/android/oe;

    invoke-virtual/range {v0 .. v7}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/Session;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/client/ag;)Ljava/lang/String;

    return-void
.end method

.method static synthetic g(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/library/network/OneFactorLoginResponse;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->h:Lcom/twitter/library/network/OneFactorLoginResponse;

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/OneFactorLoginActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/android/oe;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->k:Lcom/twitter/android/oe;

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/library/client/aa;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/OneFactorLoginActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/OneFactorLoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic l(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/OneFactorLoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic m(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/OneFactorLoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic n(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/OneFactorLoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic o(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/OneFactorLoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic p(Lcom/twitter/android/OneFactorLoginActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/OneFactorLoginActivity;->f()V

    return-void
.end method

.method static synthetic q(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/OneFactorLoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic r(Lcom/twitter/android/OneFactorLoginActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->c:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic s(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/OneFactorLoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const v1, 0x7f0300d9    # com.twitter.android.R.layout.one_factor_login

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->c(Z)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->a(Z)V

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/library/client/e;)V
    .locals 4

    const v0, 0x7f0f0237    # com.twitter.android.R.string.login_signing_in

    invoke-virtual {p0, v0}, Lcom/twitter/android/OneFactorLoginActivity;->setTitle(I)V

    invoke-virtual {p0}, Lcom/twitter/android/OneFactorLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "username"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->f:Ljava/lang/String;

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/twitter/android/OneFactorLoginActivity;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->l:Landroid/os/Handler;

    const v0, 0x7f090204    # com.twitter.android.R.id.manual_entry

    invoke-virtual {p0, v0}, Lcom/twitter/android/OneFactorLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->d:Landroid/view/View;

    const v0, 0x7f09017f    # com.twitter.android.R.id.spinner

    invoke-virtual {p0, v0}, Lcom/twitter/android/OneFactorLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->e:Landroid/view/View;

    const v0, 0x7f090206    # com.twitter.android.R.id.progress_text

    invoke-virtual {p0, v0}, Lcom/twitter/android/OneFactorLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->n:Landroid/widget/TextView;

    const v0, 0x7f09002c    # com.twitter.android.R.id.action_button

    invoke-virtual {p0, v0}, Lcom/twitter/android/OneFactorLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->b:Landroid/widget/Button;

    iget-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->b:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090205    # com.twitter.android.R.id.code

    invoke-virtual {p0, v0}, Lcom/twitter/android/OneFactorLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v1, "android.provider.Telephony.SMS_RECEIVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/OneFactorLoginActivity;->a:Lcom/twitter/android/of;

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/OneFactorLoginActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    if-nez p1, :cond_0

    sget-object v0, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->a:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    iput-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->i:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    invoke-virtual {p0}, Lcom/twitter/android/OneFactorLoginActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/OneFactorLoginActivity;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->a(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->g:Lcom/twitter/library/client/Session;

    invoke-virtual {p0}, Lcom/twitter/android/OneFactorLoginActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/OneFactorLoginActivity;->g:Lcom/twitter/library/client/Session;

    iget-object v2, p0, Lcom/twitter/android/OneFactorLoginActivity;->f:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/OneFactorLoginActivity;->k:Lcom/twitter/android/oe;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;Lcom/twitter/library/client/ag;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->j:Ljava/lang/String;

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->i:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    invoke-virtual {v0, p0}, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->a(Lcom/twitter/android/OneFactorLoginActivity;)V

    return-void

    :cond_0
    const-string/jumbo v0, "session"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/Session;

    iput-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->g:Lcom/twitter/library/client/Session;

    const-string/jumbo v0, "response"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/network/OneFactorLoginResponse;

    iput-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->h:Lcom/twitter/library/network/OneFactorLoginResponse;

    const-string/jumbo v0, "state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    invoke-direct {p0, v0}, Lcom/twitter/android/OneFactorLoginActivity;->a(Lcom/twitter/android/OneFactorLoginActivity$LoginState;)V

    const-string/jumbo v0, "reqId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->j:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/OneFactorLoginActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/OneFactorLoginActivity;->j:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/OneFactorLoginActivity;->k:Lcom/twitter/android/oe;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/aa;->a(Ljava/lang/String;Lcom/twitter/library/client/ah;)V

    goto :goto_0
.end method

.method public a(Lhn;)Z
    .locals 2

    invoke-virtual {p1}, Lhn;->a()I

    move-result v0

    const v1, 0x7f09031f    # com.twitter.android.R.id.menu_about

    if-ne v0, v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/AboutActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/OneFactorLoginActivity;->startActivity(Landroid/content/Intent;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhn;)Z

    move-result v0

    goto :goto_0
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->b:Landroid/widget/Button;

    iget-object v1, p0, Lcom/twitter/android/OneFactorLoginActivity;->i:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    invoke-virtual {v1, p0}, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->c(Lcom/twitter/android/OneFactorLoginActivity;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onBackPressed()V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/OneFactorLoginActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/OneFactorLoginActivity;->g:Lcom/twitter/library/client/Session;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/Session;)Ljava/lang/String;

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onBackPressed()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f09002c    # com.twitter.android.R.id.action_button

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->i:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    invoke-virtual {v0, p0}, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->b(Lcom/twitter/android/OneFactorLoginActivity;)V

    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/OneFactorLoginActivity;->a:Lcom/twitter/android/of;

    invoke-virtual {p0, v0}, Lcom/twitter/android/OneFactorLoginActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onDestroy()V

    return-void
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onResume()V

    sget-object v0, Lcom/twitter/library/client/Session$LoginStatus;->c:Lcom/twitter/library/client/Session$LoginStatus;

    iget-object v1, p0, Lcom/twitter/android/OneFactorLoginActivity;->g:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->b()Lcom/twitter/library/client/Session$LoginStatus;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const-string/jumbo v0, "android.intent.action.MAIN"

    invoke-virtual {p0}, Lcom/twitter/android/OneFactorLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v1, "session"

    iget-object v2, p0, Lcom/twitter/android/OneFactorLoginActivity;->g:Lcom/twitter/library/client/Session;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string/jumbo v1, "session"

    iget-object v2, p0, Lcom/twitter/android/OneFactorLoginActivity;->g:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/library/api/ap;->a(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/OneFactorLoginActivity;->setResult(ILandroid/content/Intent;)V

    sget-object v0, Lcom/twitter/android/MainActivity;->a:Landroid/net/Uri;

    invoke-static {p0, v0}, Lcom/twitter/android/MainActivity;->a(Landroid/app/Activity;Landroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "state"

    iget-object v1, p0, Lcom/twitter/android/OneFactorLoginActivity;->i:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string/jumbo v0, "session"

    iget-object v1, p0, Lcom/twitter/android/OneFactorLoginActivity;->g:Lcom/twitter/library/client/Session;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v0, "response"

    iget-object v1, p0, Lcom/twitter/android/OneFactorLoginActivity;->h:Lcom/twitter/library/network/OneFactorLoginResponse;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v0, "reqId"

    iget-object v1, p0, Lcom/twitter/android/OneFactorLoginActivity;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
