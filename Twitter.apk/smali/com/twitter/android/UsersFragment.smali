.class public Lcom/twitter/android/UsersFragment;
.super Lcom/twitter/android/client/BaseListFragment;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/android/cu;
.implements Lcom/twitter/android/cy;
.implements Lcom/twitter/android/ob;
.implements Lcom/twitter/android/ty;
.implements Lcom/twitter/android/tz;
.implements Lcom/twitter/android/widget/ce;
.implements Lcom/twitter/library/widget/a;


# static fields
.field private static final y:Ljava/util/Set;


# instance fields
.field private A:Landroid/net/Uri;

.field private B:[J

.field private C:Z

.field private D:Lcom/twitter/android/FollowFlowController;

.field private E:Z

.field private F:[Ljava/lang/String;

.field private G:Ljava/lang/String;

.field private H:Ljava/lang/String;

.field private I:Ljava/lang/String;

.field private J:Ljava/util/ArrayList;

.field private K:Ljava/util/HashMap;

.field private L:Ljava/util/HashSet;

.field private U:Ljava/util/HashSet;

.field private V:I

.field private W:I

.field private X:I

.field private Y:I

.field private Z:Z

.field protected a:Z

.field private aa:Z

.field private ab:Z

.field private ac:Z

.field private ad:Z

.field private ae:Z

.field private af:Landroid/view/View;

.field private ag:Z

.field private ah:I

.field private ai:Z

.field private aj:Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

.field private ak:Lcom/twitter/android/ClusterFollowAdapterData;

.field private al:Lcom/twitter/library/view/c;

.field private am:I

.field private an:I

.field private ao:Z

.field private ap:Z

.field private aq:Z

.field private ar:Z

.field private as:Z

.field private at:Z

.field private au:Ljava/lang/String;

.field private av:Z

.field private aw:Z

.field final b:Ljava/util/HashMap;

.field c:J

.field d:Lcom/twitter/library/api/PromotedContent;

.field e:J

.field f:Ljava/lang/Integer;

.field g:Ljava/util/HashSet;

.field h:Ljava/util/ArrayList;

.field i:Lcom/twitter/library/util/FriendshipCache;

.field j:Ljava/util/HashMap;

.field k:Ljava/util/HashMap;

.field l:Lcom/twitter/android/aag;

.field m:Lcom/twitter/android/aaf;

.field n:Ljava/lang/String;

.field o:Lcom/twitter/android/widget/cg;

.field p:Lcom/twitter/android/tn;

.field q:Lcom/twitter/library/api/TwitterUser;

.field r:I

.field s:I

.field t:I

.field u:I

.field v:I

.field w:Z

.field x:Z

.field private final z:Ljava/util/HashSet;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/twitter/android/UsersFragment;->y:Ljava/util/Set;

    sget-object v0, Lcom/twitter/android/UsersFragment;->y:Ljava/util/Set;

    const-wide/32 v1, 0x124215c8

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/twitter/android/UsersFragment;->y:Ljava/util/Set;

    const-wide/32 v1, 0x5a6dfdf5

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseListFragment;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->a:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->b:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->g:Ljava/util/HashSet;

    new-instance v0, Lcom/twitter/android/aah;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/twitter/android/aah;-><init>(Lcom/twitter/android/aad;)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->m:Lcom/twitter/android/aaf;

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/UsersFragment;->r:I

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->z:Ljava/util/HashSet;

    iput v2, p0, Lcom/twitter/android/UsersFragment;->Y:I

    iput-boolean v2, p0, Lcom/twitter/android/UsersFragment;->Z:Z

    iput v2, p0, Lcom/twitter/android/UsersFragment;->am:I

    iput v2, p0, Lcom/twitter/android/UsersFragment;->an:I

    iput-boolean v2, p0, Lcom/twitter/android/UsersFragment;->ar:Z

    iput-boolean v2, p0, Lcom/twitter/android/UsersFragment;->as:Z

    iput-boolean v2, p0, Lcom/twitter/android/UsersFragment;->at:Z

    return-void
.end method

.method static synthetic A(Lcom/twitter/android/UsersFragment;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->I:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic B(Lcom/twitter/android/UsersFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic C(Lcom/twitter/android/UsersFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->av:Z

    return v0
.end method

.method static synthetic D(Lcom/twitter/android/UsersFragment;)J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/android/UsersFragment;->Q:J

    return-wide v0
.end method

.method static synthetic E(Lcom/twitter/android/UsersFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method static synthetic F(Lcom/twitter/android/UsersFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method static synthetic G(Lcom/twitter/android/UsersFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method private G()Z
    .locals 2

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v1, 0x9

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v1, 0x13

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v1, 0x1b

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v1, 0x15

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v1, 0x16

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v1, 0x18

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ai:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic H(Lcom/twitter/android/UsersFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method private H()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/cx;->b(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ao:Z

    return-void
.end method

.method private I()Z
    .locals 2

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v1, 0x15

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v1, 0x16

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private J()V
    .locals 7

    const v6, 0x7f09014d    # com.twitter.android.R.id.find_friends_cta

    const v5, 0x7f090120    # com.twitter.android.R.id.scan_contacts_view

    const v4, 0x7f090037    # com.twitter.android.R.id.divider

    const/4 v3, 0x0

    const/16 v2, 0x8

    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->I()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->af:Landroid/view/View;

    iget-boolean v1, p0, Lcom/twitter/android/UsersFragment;->ap:Z

    if-eqz v1, :cond_2

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget-boolean v1, p0, Lcom/twitter/android/UsersFragment;->ao:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private K()V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f090267    # com.twitter.android.R.id.friend_count

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0263    # com.twitter.android.R.string.matched_contacts_format

    new-array v5, v1, [Ljava/lang/Object;

    iget v6, p0, Lcom/twitter/android/UsersFragment;->X:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v0, p0, Lcom/twitter/android/UsersFragment;->X:I

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f090268    # com.twitter.android.R.id.select_all_checkbox

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->g:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v3

    iget v4, p0, Lcom/twitter/android/UsersFragment;->X:I

    if-ne v3, v4, :cond_1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_0
    return-void

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method private L()V
    .locals 5

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/UsersFragment;->y:Ljava/util/Set;

    invoke-static {v1}, Lcom/twitter/library/util/Util;->b(Ljava/util/Collection;)[J

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a([J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->au:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->au:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersFragment;->d(Ljava/lang/String;)V

    sget-object v0, Lcom/twitter/android/UsersFragment;->y:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iget-object v2, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/util/FriendshipCache;->b(J)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->as:Z

    return-void
.end method

.method private M()V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    :cond_1
    const/4 v2, 0x2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const/4 v5, 0x7

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    or-int/lit8 v5, v5, 0x1

    invoke-virtual {v1, v3, v4, v5}, Lcom/twitter/library/util/FriendshipCache;->b(JI)V

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->g:Ljava/util/HashSet;

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetChanged()V

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->m:Lcom/twitter/android/aaf;

    invoke-interface {v0}, Lcom/twitter/android/aaf;->b()V

    goto :goto_0
.end method

.method private N()Z
    .locals 2

    iget v0, p0, Lcom/twitter/android/UsersFragment;->am:I

    iget v1, p0, Lcom/twitter/android/UsersFragment;->an:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private O()V
    .locals 6

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/4 v2, 0x7

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/android/UsersFragment;->D:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v5}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "follow_friends:::follow_all"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private Q()V
    .locals 7

    const/4 v4, 0x1

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/UsersFragment;->s:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v4, [Ljava/lang/String;

    const-string/jumbo v4, "following::::impression"

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v4, [Ljava/lang/String;

    const-string/jumbo v4, "followers::::impression"

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v4, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/twitter/android/UsersFragment;->D:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v5}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ":discover:::impression"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v4, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->aD()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ":impression"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0

    :pswitch_5
    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v4, [Ljava/lang/String;

    const-string/jumbo v4, "friend_suggestions::::impression"

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_6
    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v4, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/twitter/android/UsersFragment;->D:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v5}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ":follow_friends:::impression"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_7
    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v2, v4, [Ljava/lang/String;

    const-string/jumbo v3, "events:people:::impression"

    aput-object v3, v2, v6

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "event_name"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/library/util/Util;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->f(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto/16 :goto_0

    :pswitch_8
    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v4, [Ljava/lang/String;

    const-string/jumbo v4, "similar_to::::impression"

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_9
    const-string/jumbo v0, "category"

    invoke-direct {p0, v0}, Lcom/twitter/android/UsersFragment;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_a
    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v4, [Ljava/lang/String;

    const-string/jumbo v4, "favorite_people::::impression"

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_6
        :pswitch_0
        :pswitch_3
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_7
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private R()V
    .locals 5

    const/4 v0, 0x0

    iget v1, p0, Lcom/twitter/android/UsersFragment;->s:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_0

    new-instance v2, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v2}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeAssociation;->a(I)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v2

    iget-wide v3, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeAssociation;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/scribe/ScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersFragment;->b(Lcom/twitter/library/scribe/ScribeAssociation;)V

    :cond_0
    return-void

    :pswitch_1
    const-string/jumbo v1, "following"

    goto :goto_0

    :pswitch_2
    const-string/jumbo v1, "follower"

    goto :goto_0

    :pswitch_3
    const-string/jumbo v1, "blocked"

    goto :goto_0

    :pswitch_4
    const-string/jumbo v1, "list"

    const-string/jumbo v0, "users"

    goto :goto_0

    :pswitch_5
    const-string/jumbo v1, "category"

    goto :goto_0

    :pswitch_6
    const-string/jumbo v1, "matches"

    goto :goto_0

    :pswitch_7
    const-string/jumbo v1, "who_to_follow"

    goto :goto_0

    :pswitch_8
    const-string/jumbo v1, "similar_to"

    goto :goto_0

    :pswitch_9
    const-string/jumbo v1, "favorited_by"

    goto :goto_0

    :pswitch_a
    const-string/jumbo v1, "retweeted_by"

    goto :goto_0

    :pswitch_b
    const-string/jumbo v1, "events"

    const-string/jumbo v0, "users"

    goto :goto_0

    :pswitch_c
    const-string/jumbo v1, "friendships"

    goto :goto_0

    :pswitch_d
    const-string/jumbo v1, "following_favorite_people"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_d
        :pswitch_b
        :pswitch_c
        :pswitch_7
        :pswitch_0
        :pswitch_7
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method private S()Lcom/twitter/android/aaa;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/aaa;

    return-object v0
.end method

.method private T()Z
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->aq:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v1, 0x13

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/4 v1, 0x7

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v1, 0x1b

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/UsersFragment;I)I
    .locals 0

    iput p1, p0, Lcom/twitter/android/UsersFragment;->X:I

    return p1
.end method

.method private a(Landroid/os/Bundle;I)Lcom/twitter/android/aaa;
    .locals 12

    const/4 v3, 0x2

    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v4

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ai:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->aj:Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

    invoke-direct {v0}, Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->aj:Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

    :cond_0
    new-instance v0, Lcom/twitter/android/ClusterFollowAdapterData;

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    iget-object v5, p0, Lcom/twitter/android/UsersFragment;->aj:Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

    invoke-direct {v0, v2, v1, p0, v5}, Lcom/twitter/android/ClusterFollowAdapterData;-><init>(Landroid/content/Context;Lcom/twitter/library/util/FriendshipCache;Lcom/twitter/android/cu;Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->ak:Lcom/twitter/android/ClusterFollowAdapterData;

    new-instance v0, Lcom/twitter/android/ct;

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->ak:Lcom/twitter/android/ClusterFollowAdapterData;

    iget-object v7, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    const-string/jumbo v5, "follow_all_title"

    invoke-virtual {p1, v5, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    const-string/jumbo v5, "follow_all_subtitle"

    invoke-virtual {p1, v5, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v9

    const-string/jumbo v5, "show_follow_all_button"

    invoke-virtual {p1, v5, v10}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    const-string/jumbo v5, "user_checkbox"

    invoke-virtual {p1, v5, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v11

    move v5, p2

    move-object v6, p0

    invoke-direct/range {v0 .. v11}, Lcom/twitter/android/ct;-><init>(Lcom/twitter/android/ClusterFollowAdapterData;Landroid/content/Context;ILcom/twitter/android/client/c;ILcom/twitter/library/widget/a;Lcom/twitter/library/util/FriendshipCache;IIZZ)V

    :goto_0
    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->T()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/aaa;->c(Z)V

    return-object v0

    :cond_1
    new-instance v1, Lcom/twitter/android/aaa;

    iget-object v6, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    const-string/jumbo v0, "follow_all_title"

    invoke-virtual {p1, v0, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    const-string/jumbo v0, "follow_all_subtitle"

    invoke-virtual {p1, v0, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    const-string/jumbo v0, "show_follow_all_button"

    invoke-virtual {p1, v0, v10}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    const-string/jumbo v0, "user_checkbox"

    invoke-virtual {p1, v0, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    move v4, p2

    move-object v5, p0

    invoke-direct/range {v1 .. v10}, Lcom/twitter/android/aaa;-><init>(Landroid/content/Context;IILcom/twitter/library/widget/a;Lcom/twitter/library/util/FriendshipCache;IIZZ)V

    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/UsersFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/os/Bundle;IZ)Lcom/twitter/android/rh;
    .locals 10

    const/4 v3, 0x2

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v4

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ai:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->aj:Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

    invoke-direct {v0}, Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->aj:Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

    :cond_0
    new-instance v0, Lcom/twitter/android/ClusterFollowAdapterData;

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    iget-object v6, p0, Lcom/twitter/android/UsersFragment;->aj:Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

    invoke-direct {v0, v2, v1, p0, v6}, Lcom/twitter/android/ClusterFollowAdapterData;-><init>(Landroid/content/Context;Lcom/twitter/library/util/FriendshipCache;Lcom/twitter/android/cu;Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->ak:Lcom/twitter/android/ClusterFollowAdapterData;

    new-instance v0, Lcom/twitter/android/cs;

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->ak:Lcom/twitter/android/ClusterFollowAdapterData;

    iget v6, p0, Lcom/twitter/android/UsersFragment;->R:I

    if-eqz v6, :cond_1

    const/4 v5, 0x1

    :cond_1
    iget-object v8, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    move v6, p2

    move-object v7, p0

    move v9, p3

    invoke-direct/range {v0 .. v9}, Lcom/twitter/android/cs;-><init>(Lcom/twitter/android/ClusterFollowAdapterData;Landroid/content/Context;ILcom/twitter/android/client/c;ZILcom/twitter/library/widget/a;Lcom/twitter/library/util/FriendshipCache;Z)V

    :goto_0
    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->T()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/rh;->c(Z)V

    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->aC()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/rh;->a(Z)V

    return-object v0

    :cond_2
    new-instance v1, Lcom/twitter/android/rh;

    iget-object v7, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    const-string/jumbo v0, "follow_all_subtitle"

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    move v5, p2

    move-object v6, p0

    move v9, p3

    invoke-direct/range {v1 .. v9}, Lcom/twitter/android/rh;-><init>(Landroid/content/Context;ILcom/twitter/android/client/c;ILcom/twitter/library/widget/a;Lcom/twitter/library/util/FriendshipCache;IZ)V

    move-object v0, v1

    goto :goto_0
.end method

.method private a(JLcom/twitter/library/api/PromotedContent;Lcom/twitter/android/zx;)V
    .locals 7

    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/UsersFragment;->av:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v1, p1, p2}, Lcom/twitter/library/util/FriendshipCache;->i(J)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v1, p1, p2}, Lcom/twitter/library/util/FriendshipCache;->g(J)V

    :cond_0
    new-instance v1, Lin;

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lin;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {v1, v4}, Lin;->c(I)Lcom/twitter/library/service/b;

    move-result-object v2

    const-string/jumbo v3, "user_id"

    invoke-virtual {v2, v3, p1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    invoke-virtual {p0, v1, v4, v5}, Lcom/twitter/android/UsersFragment;->a(Lcom/twitter/library/service/b;II)Z

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "following_favorite_people:following:::unfavorite_user"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iput-boolean v6, p0, Lcom/twitter/android/UsersFragment;->x:Z

    return-void

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v1, p1, p2}, Lcom/twitter/library/util/FriendshipCache;->i(J)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v1, p1, p2}, Lcom/twitter/library/util/FriendshipCache;->f(J)V

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v1, v2, v3}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/twitter/library/provider/az;->a(J)Lcom/twitter/library/api/TwitterUser;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->au()Lcom/twitter/library/client/aa;

    move-result-object v2

    iget-wide v3, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/client/aa;->a(J)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v0, v2, v1, v5}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/TwitterUser;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/UsersFragment;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 5

    const/4 v1, 0x0

    if-eqz p6, :cond_2

    const-string/jumbo v0, "cluster_follow"

    :goto_0
    iget v2, p0, Lcom/twitter/android/UsersFragment;->s:I

    packed-switch v2, :pswitch_data_0

    :goto_1
    :pswitch_0
    if-eqz v1, :cond_1

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v1, 0x1

    aput-object v0, v3, v1

    const/4 v0, 0x2

    aput-object p5, v3, v0

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/twitter/library/scribe/ScribeLog;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v2, 0x11

    if-ne v1, v2, :cond_4

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "event_name"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/util/Util;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->f(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_0
    :goto_2
    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_1
    return-void

    :cond_2
    const-string/jumbo v0, "user"

    goto :goto_0

    :pswitch_1
    const-string/jumbo v1, "following:following:"

    goto :goto_1

    :pswitch_2
    const-string/jumbo v1, "followers:followers:"

    goto :goto_1

    :pswitch_3
    const-string/jumbo v1, "events:people:"

    goto :goto_1

    :pswitch_4
    const-string/jumbo v1, "favorited_by::"

    goto :goto_1

    :pswitch_5
    const-string/jumbo v1, "retweeted_by::"

    goto :goto_1

    :pswitch_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/twitter/android/UsersFragment;->D:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v2}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ":follow_friends:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :pswitch_7
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/twitter/android/UsersFragment;->D:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v2}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ":who_to_follow:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    :pswitch_8
    const-string/jumbo v1, "who_to_follow::"

    goto/16 :goto_1

    :pswitch_9
    const-string/jumbo v1, "friend_suggestions::"

    goto/16 :goto_1

    :pswitch_a
    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->D:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v1}, Lcom/twitter/android/FollowFlowController;->c()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string/jumbo v1, "welcome:category:"

    goto/16 :goto_1

    :cond_3
    const-string/jumbo v1, "category::"

    goto/16 :goto_1

    :pswitch_b
    const-string/jumbo v1, "similar_to::"

    goto/16 :goto_1

    :pswitch_c
    const-string/jumbo v1, "following_favorite_people:following:"

    goto/16 :goto_1

    :cond_4
    iget v1, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_0

    iget-wide v1, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->i(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_6
        :pswitch_0
        :pswitch_8
        :pswitch_b
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_3
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method private a(Landroid/view/View;JLjava/lang/String;)V
    .locals 9

    const/4 v8, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->at:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v2

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "user_id"

    invoke-virtual {v0, v3, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "type"

    iget v4, p0, Lcom/twitter/android/UsersFragment;->s:I

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v3, "override_home"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "override_home"

    invoke-virtual {v7, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    if-eqz v0, :cond_2

    const-string/jumbo v0, "association"

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v7, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/zx;

    iget-object v0, v0, Lcom/twitter/android/zx;->b:Lcom/twitter/library/widget/BaseUserView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/BaseUserView;->getUserName()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-interface {v3, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v4, 0x40

    if-ne v0, v4, :cond_6

    move v0, v6

    :goto_1
    const-string/jumbo v4, "screen_name"

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v5

    invoke-interface {v3, v0, v5}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v7, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    :cond_3
    const/16 v0, 0x12

    iget v3, p0, Lcom/twitter/android/UsersFragment;->s:I

    if-ne v0, v3, :cond_8

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->j:Ljava/util/HashMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_4
    :goto_2
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/zx;

    iget-object v0, v0, Lcom/twitter/android/zx;->b:Lcom/twitter/library/widget/BaseUserView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/BaseUserView;->getPromotedContent()Lcom/twitter/library/api/PromotedContent;

    move-result-object v4

    if-eqz v4, :cond_5

    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->d:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {v2, v0, v4}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V

    const-string/jumbo v0, "pc"

    invoke-virtual {v7, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_5
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/zx;

    iget-object v5, v0, Lcom/twitter/android/zx;->f:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p4

    move-wide v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/UsersFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;)V

    invoke-virtual {p0, v7, v6}, Lcom/twitter/android/UsersFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_6
    move v0, v1

    goto :goto_1

    :pswitch_0
    const-string/jumbo v0, "friendship"

    invoke-virtual {v7, v0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_2

    :pswitch_1
    const-string/jumbo v0, "friendship"

    const/4 v1, 0x3

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_2

    :pswitch_2
    const-string/jumbo v0, "friendship"

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_2

    :cond_7
    const-string/jumbo v0, "friendship"

    const/16 v1, 0x20

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_2

    :cond_8
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/library/util/FriendshipCache;->h(J)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_9

    const-string/jumbo v1, "friendship"

    invoke-virtual {v7, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_2

    :cond_9
    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    if-ne v8, v0, :cond_4

    const-string/jumbo v0, "friendship"

    const/4 v1, 0x4

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lcom/twitter/android/UsersFragment;JLcom/twitter/library/api/PromotedContent;Lcom/twitter/android/zx;Z)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/twitter/android/UsersFragment;->b(JLcom/twitter/library/api/PromotedContent;Lcom/twitter/android/zx;Z)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    const-string/jumbo v0, ":click"

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    sparse-switch v0, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_0
    return-void

    :sswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "welcome:who_to_follow::"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":click"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "who_to_follow:contacts::"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":click"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "friend_suggestions:contacts::"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":click"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_1
        0x13 -> :sswitch_0
        0x15 -> :sswitch_1
        0x16 -> :sswitch_2
        0x1b -> :sswitch_0
    .end sparse-switch
.end method

.method private a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;)V
    .locals 4

    const/4 v0, 0x0

    iget v1, p0, Lcom/twitter/android/UsersFragment;->s:I

    sparse-switch v1, :sswitch_data_0

    :goto_0
    if-eqz v0, :cond_1

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, p2, p3, p4, p5}, Lcom/twitter/library/scribe/ScribeLog;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v2, 0x11

    if-ne v1, v2, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "event_name"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/util/Util;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->f(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_1
    return-void

    :sswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "events:people::"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":profile_click"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "category:::"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":profile_click"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "welcome:who_to_follow::"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":profile_click"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :sswitch_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "who_to_follow:::"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":profile_click"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :sswitch_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "friend_suggestions:::"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":profile_click"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :sswitch_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "similar_to:::"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":profile_click"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_2
    iget v1, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_0

    iget-wide v1, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->i(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_1
        0x9 -> :sswitch_3
        0xa -> :sswitch_5
        0x11 -> :sswitch_0
        0x13 -> :sswitch_2
        0x15 -> :sswitch_3
        0x16 -> :sswitch_4
        0x18 -> :sswitch_1
        0x1b -> :sswitch_2
    .end sparse-switch
.end method

.method static synthetic a(Lcom/twitter/android/UsersFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ae:Z

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/UsersFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/UsersFragment;->ae:Z

    return p1
.end method

.method private aC()Z
    .locals 2

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v1, 0x13

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v1, 0x1b

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aD()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->D:Lcom/twitter/android/FollowFlowController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->D:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v0}, Lcom/twitter/android/FollowFlowController;->a()Lcom/twitter/android/FollowFlowController$Initiator;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/FollowFlowController$Initiator;->b:Lcom/twitter/android/FollowFlowController$Initiator;

    if-ne v0, v1, :cond_1

    :cond_0
    const-string/jumbo v0, "who_to_follow:::"

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->D:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v1}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":who_to_follow::"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/UsersFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method private b(Landroid/os/Bundle;I)Lcom/twitter/android/widget/g;
    .locals 7

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance v0, Lcom/twitter/android/widget/g;

    const/4 v2, 0x2

    iget-object v5, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    const-string/jumbo v3, "follow_all_subtitle"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    move v3, p2

    move-object v4, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/widget/g;-><init>(Landroid/content/Context;IILcom/twitter/library/widget/a;Lcom/twitter/library/util/FriendshipCache;I)V

    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->T()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/g;->c(Z)V

    return-object v0
.end method

.method private b(JLcom/twitter/library/api/PromotedContent;Lcom/twitter/android/zx;)V
    .locals 9

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/UsersFragment;->av:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v1, p1, p2}, Lcom/twitter/library/util/FriendshipCache;->i(J)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v1, p1, p2}, Lcom/twitter/library/util/FriendshipCache;->d(J)V

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->g:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->g:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-wide v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/c;->a(JZLcom/twitter/library/api/PromotedContent;ZZ)Ljava/lang/String;

    :cond_1
    new-instance v1, Lin;

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lin;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lin;->c(I)Lcom/twitter/library/service/b;

    move-result-object v2

    const-string/jumbo v3, "user_id"

    invoke-virtual {v2, v3, p1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    invoke-static {}, Lgl;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    const-string/jumbo v2, "device_follow"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lin;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    :cond_2
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v2, v3}, Lcom/twitter/android/UsersFragment;->a(Lcom/twitter/library/service/b;II)Z

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "following_list_favorite_follow_dialog_shown"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-static {}, Lgl;->g()I

    move-result v2

    const/4 v3, 0x4

    invoke-static {v3}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v3

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f0546    # com.twitter.android.R.string.users_tweet_notifications_dialog_title

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p4, Lcom/twitter/android/zx;->h:Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Ljava/lang/String;)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v2

    const v3, 0x7f0f02d5    # com.twitter.android.R.string.ok

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, p0, v3}, Lcom/twitter/android/widget/PromptDialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "following_list_favorite_follow_dialog_shown"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-wide v1, p0, Lcom/twitter/android/UsersFragment;->Q:J

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "following_favorite_people::confirm_favorite_dialog_listing::show"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_3
    :goto_0
    iget-wide v1, p0, Lcom/twitter/android/UsersFragment;->Q:J

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "following_favorite_people:following:::favorite_user"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->x:Z

    return-void

    :cond_4
    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v1, p1, p2}, Lcom/twitter/library/util/FriendshipCache;->i(J)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v1, p1, p2}, Lcom/twitter/library/util/FriendshipCache;->c(J)V

    :cond_5
    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->g:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->g:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-wide v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/c;->a(JZLcom/twitter/library/api/PromotedContent;ZZ)Ljava/lang/String;

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v1, v2, v3}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/twitter/library/provider/az;->a(J)Lcom/twitter/library/api/TwitterUser;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->au()Lcom/twitter/library/client/aa;

    move-result-object v2

    iget-wide v3, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/client/aa;->a(J)Lcom/twitter/library/client/Session;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v1, v3}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/TwitterUser;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/UsersFragment;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private b(JLcom/twitter/library/api/PromotedContent;Lcom/twitter/android/zx;Z)V
    .locals 13

    move-object/from16 v0, p4

    iget-wide v10, v0, Lcom/twitter/android/zx;->c:J

    move-object/from16 v0, p4

    iget-object v5, v0, Lcom/twitter/android/zx;->f:Ljava/lang/String;

    move-object/from16 v0, p4

    iget v8, v0, Lcom/twitter/android/zx;->e:I

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v12

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->r()Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz p3, :cond_4

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->r()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->b:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    const/4 v1, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v12, p1, p2, v1, v0}, Lcom/twitter/android/client/c;->a(JZLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/UsersFragment;->d(Ljava/lang/String;)V

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v1, p1, p2}, Lcom/twitter/library/util/FriendshipCache;->b(J)V

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->m:Lcom/twitter/android/aaf;

    invoke-interface {v1}, Lcom/twitter/android/aaf;->b()V

    const-string/jumbo v6, "follow"

    move-object v1, p0

    move-wide v2, p1

    move-object/from16 v4, p3

    move/from16 v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/twitter/android/UsersFragment;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {v8}, Lcom/twitter/library/provider/ay;->c(I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v6, "follow_back"

    move-object v1, p0

    move-wide v2, p1

    move-object/from16 v4, p3

    move/from16 v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/twitter/android/UsersFragment;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Ljava/lang/String;Z)V

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget v2, p0, Lcom/twitter/android/UsersFragment;->ah:I

    invoke-static {v1, v2}, Lcom/twitter/android/util/c;->b(Landroid/content/Context;I)V

    iget-boolean v1, p0, Lcom/twitter/android/UsersFragment;->ai:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->ak:Lcom/twitter/android/ClusterFollowAdapterData;

    invoke-virtual {v1, v10, v11}, Lcom/twitter/android/ClusterFollowAdapterData;->a(J)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v4, 0x3

    iget v9, p0, Lcom/twitter/android/UsersFragment;->t:I

    :goto_1
    iget v2, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/twitter/android/UsersFragment;->f(Z)[J

    move-result-object v3

    move-object v1, v12

    move-wide v5, p1

    move-wide v7, v10

    invoke-virtual/range {v1 .. v9}, Lcom/twitter/android/client/c;->a(I[JIJJI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/UsersFragment;->d(Ljava/lang/String;)V

    :cond_3
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/twitter/android/UsersFragment;->x:Z

    return-void

    :cond_4
    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->g:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    const/4 v4, 0x1

    iget v9, p0, Lcom/twitter/android/UsersFragment;->u:I

    goto :goto_1
.end method

.method private final b(Landroid/database/Cursor;)V
    .locals 5

    const/4 v4, 0x3

    const/4 v3, -0x1

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/4 v1, 0x7

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v1, 0x11

    if-ne v0, v1, :cond_1

    :cond_0
    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->w:Z

    if-nez v0, :cond_2

    :cond_1
    invoke-virtual {p0, v4}, Lcom/twitter/android/UsersFragment;->d(I)Z

    :goto_0
    return-void

    :cond_2
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/UsersFragment;->X:I

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->l:Lcom/twitter/android/aag;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->l:Lcom/twitter/android/aag;

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v3, v3, v2}, Lcom/twitter/android/aag;->a(IIILjava/util/ArrayList;)V

    :cond_3
    invoke-virtual {p0, v4}, Lcom/twitter/android/UsersFragment;->b(I)V

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f090268    # com.twitter.android.R.id.select_all_checkbox

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->M()V

    :cond_4
    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->K()V

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "impression"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeItem;

    invoke-direct {v1}, Lcom/twitter/library/scribe/ScribeItem;-><init>()V

    iget-wide v2, p0, Lcom/twitter/android/UsersFragment;->Q:J

    iput-wide v2, v1, Lcom/twitter/library/scribe/ScribeItem;->a:J

    iget-object v2, p0, Lcom/twitter/android/UsersFragment;->H:Ljava/lang/String;

    iput-object v2, v1, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    iget v2, p0, Lcom/twitter/android/UsersFragment;->W:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/twitter/library/scribe/ScribeItem;->g:I

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/UsersFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->aa:Z

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/UsersFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/UsersFragment;->Z:Z

    return p1
.end method

.method static synthetic c(Lcom/twitter/android/UsersFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/UsersFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method private c(J)V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-wide/32 v0, 0x124215c8

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "twitter_photos"

    :goto_0
    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "welcome"

    aput-object v2, v1, v4

    const-string/jumbo v2, "who_to_follow"

    aput-object v2, v1, v5

    const/4 v2, 0x2

    const-string/jumbo v3, "autofollow"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object v0, v1, v2

    const/4 v0, 0x4

    const-string/jumbo v2, "unfollow"

    aput-object v2, v1, v0

    invoke-static {v1}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v2, v5, [Ljava/lang/String;

    aput-object v0, v2, v4

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, p1, p2, v6, v6}, Lcom/twitter/library/scribe/ScribeLog;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_0
    return-void

    :cond_1
    const-wide/32 v0, 0x5a6dfdf5

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "twitter_video"

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)V
    .locals 6

    const/4 v0, 0x0

    iget v1, p0, Lcom/twitter/android/UsersFragment;->s:I

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object p1, v4, v0

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_1
    return-void

    :sswitch_0
    const-string/jumbo v0, "followers:::"

    goto :goto_0

    :sswitch_1
    const-string/jumbo v0, "following:::"

    goto :goto_0

    :sswitch_2
    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->aD()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_3
    iget-boolean v1, p0, Lcom/twitter/android/UsersFragment;->ao:Z

    if-eqz v1, :cond_0

    const-string/jumbo v0, "friend_suggestions:::"

    goto :goto_0

    :sswitch_4
    const-string/jumbo v0, "category:::user"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x1 -> :sswitch_0
        0x6 -> :sswitch_4
        0x15 -> :sswitch_2
        0x16 -> :sswitch_3
        0x18 -> :sswitch_4
    .end sparse-switch
.end method

.method static synthetic d(Lcom/twitter/android/UsersFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/UsersFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/UsersFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/UsersFragment;->c(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic f(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/UsersFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/UsersFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ag:Z

    return v0
.end method

.method private f(Z)[J
    .locals 4

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v1}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_1
    iget-boolean v1, p0, Lcom/twitter/android/UsersFragment;->ai:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->ak:Lcom/twitter/android/ClusterFollowAdapterData;

    invoke-virtual {v1}, Lcom/twitter/android/ClusterFollowAdapterData;->b()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    :cond_2
    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->g:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    iget-boolean v1, p0, Lcom/twitter/android/UsersFragment;->ar:Z

    if-eqz v1, :cond_3

    sget-object v1, Lcom/twitter/android/UsersFragment;->y:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    :cond_3
    invoke-static {v0}, Lcom/twitter/library/util/Util;->b(Ljava/util/Collection;)[J

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/UsersFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/UsersFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/UsersFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/UsersFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/UsersFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/UsersFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->K()V

    return-void
.end method

.method static synthetic j(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/UsersFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/UsersFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->M()V

    return-void
.end method

.method static synthetic k(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/UsersFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/android/UsersFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ai:Z

    return v0
.end method

.method private l(I)I
    .locals 3

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid fetch type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v1}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    :pswitch_1
    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic l(Lcom/twitter/android/UsersFragment;)Lcom/twitter/android/ClusterFollowAdapterData;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->ak:Lcom/twitter/android/ClusterFollowAdapterData;

    return-object v0
.end method

.method static synthetic l(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/twitter/android/UsersFragment;->d(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic m(Lcom/twitter/android/UsersFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method static synthetic m(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/UsersFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic n(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/UsersFragment;->au:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic n(Lcom/twitter/android/UsersFragment;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->o()Z

    move-result v0

    return v0
.end method

.method static synthetic o(Lcom/twitter/android/UsersFragment;)Lcom/twitter/android/FollowFlowController;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->D:Lcom/twitter/android/FollowFlowController;

    return-object v0
.end method

.method static synthetic o(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/twitter/android/UsersFragment;->d(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic p(Lcom/twitter/android/UsersFragment;)I
    .locals 2

    iget v0, p0, Lcom/twitter/android/UsersFragment;->Y:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/twitter/android/UsersFragment;->Y:I

    return v0
.end method

.method static synthetic p(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/UsersFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic q(Lcom/twitter/android/UsersFragment;)I
    .locals 1

    iget v0, p0, Lcom/twitter/android/UsersFragment;->Y:I

    return v0
.end method

.method static synthetic q(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/UsersFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic r(Lcom/twitter/android/UsersFragment;)I
    .locals 2

    iget v0, p0, Lcom/twitter/android/UsersFragment;->am:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/twitter/android/UsersFragment;->am:I

    return v0
.end method

.method static synthetic r(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/UsersFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic s(Lcom/twitter/android/UsersFragment;)Z
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->N()Z

    move-result v0

    return v0
.end method

.method static synthetic t(Lcom/twitter/android/UsersFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method static synthetic u(Lcom/twitter/android/UsersFragment;)Lcom/twitter/android/aaa;
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->S()Lcom/twitter/android/aaa;

    move-result-object v0

    return-object v0
.end method

.method static synthetic v(Lcom/twitter/android/UsersFragment;)J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/android/UsersFragment;->Q:J

    return-wide v0
.end method

.method static synthetic w(Lcom/twitter/android/UsersFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic x(Lcom/twitter/android/UsersFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ar:Z

    return v0
.end method

.method static synthetic y(Lcom/twitter/android/UsersFragment;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->au:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic z(Lcom/twitter/android/UsersFragment;)J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/android/UsersFragment;->Q:J

    return-wide v0
.end method


# virtual methods
.method public C()Z
    .locals 1

    iget v0, p0, Lcom/twitter/android/UsersFragment;->an:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public D()I
    .locals 1

    iget v0, p0, Lcom/twitter/android/UsersFragment;->am:I

    return v0
.end method

.method protected F()V
    .locals 11

    const/4 v2, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v3

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->J:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    sparse-switch v0, :sswitch_data_0

    move-object v0, v2

    :goto_0
    if-eqz v0, :cond_0

    new-instance v4, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v5, v10, [Ljava/lang/String;

    aput-object v0, v5, v9

    invoke-virtual {v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/util/ArrayList;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-wide v4, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/twitter/library/scribe/ScribeLog;->i(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    :cond_1
    iget-object v4, p0, Lcom/twitter/android/UsersFragment;->K:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iget v1, p0, Lcom/twitter/android/UsersFragment;->s:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_2

    new-instance v6, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v7

    invoke-virtual {v7}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v7

    invoke-direct {v6, v7, v8}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v7, v10, [Ljava/lang/String;

    aput-object v1, v7, v9

    invoke-virtual {v6, v7}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/util/ArrayList;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto :goto_1

    :sswitch_0
    const-string/jumbo v0, "similar_to::stream::results"

    goto :goto_0

    :sswitch_1
    const-string/jumbo v0, "category:who_to_follow:::results"

    goto :goto_0

    :sswitch_2
    const-string/jumbo v0, "who_to_follow::stream::results"

    goto :goto_0

    :sswitch_3
    const-string/jumbo v0, "friend_suggestions::stream::results"

    goto :goto_0

    :sswitch_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/twitter/android/UsersFragment;->D:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v4}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v4, ":who_to_follow:stream::results"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_1
    const-string/jumbo v1, "similar_to:user_similarities_list:stream::results"

    goto :goto_2

    :pswitch_2
    const-string/jumbo v1, "who_to_follow:user_similarities_list:stream::results"

    goto :goto_2

    :pswitch_3
    const-string/jumbo v1, "matches:user_similarities_list:stream::results"

    goto :goto_2

    :pswitch_4
    const-string/jumbo v1, "category:user_similarities_list:stream::results"

    goto :goto_2

    :pswitch_5
    const-string/jumbo v1, "followers:user_similarities_list:stream::results"

    goto :goto_2

    :pswitch_6
    const-string/jumbo v1, "following:user_similarities_list:stream::results"

    goto :goto_2

    :pswitch_7
    const-string/jumbo v1, "favorited_by:user_similarities_list:stream::results"

    goto :goto_2

    :pswitch_8
    const-string/jumbo v1, "retweeted_by:user_similarities_list:stream::results"

    goto :goto_2

    :pswitch_9
    const-string/jumbo v1, "following_favorite_people:user_similarities_list:stream::results"

    goto :goto_2

    :cond_3
    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    :cond_4
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_1
        0x9 -> :sswitch_2
        0xa -> :sswitch_0
        0x13 -> :sswitch_4
        0x15 -> :sswitch_2
        0x16 -> :sswitch_3
        0x18 -> :sswitch_1
        0x1b -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected a(IIZ)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->aa:Z

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    if-nez p2, :cond_1

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->b(I)V

    :cond_1
    return-void
.end method

.method a(JLcom/twitter/library/api/PromotedContent;Lcom/twitter/android/zx;Z)V
    .locals 7

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ar:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/android/UsersFragment;->y:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/UsersFragment;->c(J)V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->r()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/UsersFragment;->b(J)Ljava/lang/Long;

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/android/client/c;->a(JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersFragment;->d(Ljava/lang/String;)V

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/util/FriendshipCache;->e(J)V

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->m:Lcom/twitter/android/aaf;

    invoke-interface {v0}, Lcom/twitter/android/aaf;->b()V

    iget-object v4, p4, Lcom/twitter/android/zx;->f:Ljava/lang/String;

    const-string/jumbo v5, "unfollow"

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/UsersFragment;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Ljava/lang/String;Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->x:Z

    return-void

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->g:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/android/client/c;->a(JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersFragment;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Landroid/content/Context;IILcom/twitter/library/service/b;)V
    .locals 11

    const v10, 0x7f0f00ee    # com.twitter.android.R.string.default_error_message

    const/4 v1, 0x1

    iget-object v0, p4, Lcom/twitter/library/service/b;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    invoke-virtual {p4}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v2, p4, Lcom/twitter/library/service/b;->k:Landroid/os/Bundle;

    const-string/jumbo v3, "user_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    const-string/jumbo v5, "friendship"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v6

    invoke-static {p1, v6}, Lcom/twitter/library/platform/PushService;->f(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v7

    iget-object v8, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v8, v3, v4}, Lcom/twitter/library/util/FriendshipCache;->i(J)Z

    move-result v8

    iget-object v9, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v9, v3, v4, v5}, Lcom/twitter/library/util/FriendshipCache;->b(JI)V

    if-eqz v8, :cond_1

    iget-object v5, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v5, v3, v4}, Lcom/twitter/library/util/FriendshipCache;->b(J)V

    :cond_1
    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    if-nez v7, :cond_0

    :cond_2
    const-string/jumbo v0, "notification_not_enabled"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    if-nez v7, :cond_5

    :cond_3
    invoke-static {p1}, Lcom/twitter/android/client/aw;->a(Landroid/content/Context;)Lcom/twitter/android/client/aw;

    move-result-object v2

    if-nez v7, :cond_4

    move v0, v1

    :goto_1
    invoke-virtual {v2, v6, v0}, Lcom/twitter/android/client/aw;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    invoke-static {p1, v10, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetChanged()V

    goto :goto_0

    :pswitch_1
    iget-object v2, p4, Lcom/twitter/library/service/b;->k:Landroid/os/Bundle;

    const-string/jumbo v3, "user_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    const-string/jumbo v5, "friendship"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iget-object v5, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v5, v3, v4, v2}, Lcom/twitter/library/util/FriendshipCache;->b(JI)V

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1, v10, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v0, -0x1

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-ne p3, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/twitter/android/UsersFragment;->e:J

    iget-wide v4, p0, Lcom/twitter/android/UsersFragment;->c:J

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/client/c;->b(IJJ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/UsersFragment;->d(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v7, [Ljava/lang/String;

    const-string/jumbo v4, "me:lists:list:people:remove"

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    if-ne p3, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    :pswitch_2
    if-ne p3, v0, :cond_0

    new-instance v0, Lcom/twitter/android/FollowFlowController;

    invoke-direct {v0}, Lcom/twitter/android/FollowFlowController;-><init>()V

    sget-object v1, Lcom/twitter/android/FollowFlowController$Initiator;->b:Lcom/twitter/android/FollowFlowController$Initiator;

    invoke-virtual {v0, v1}, Lcom/twitter/android/FollowFlowController;->a(Lcom/twitter/android/FollowFlowController$Initiator;)Lcom/twitter/android/FollowFlowController;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/twitter/android/FollowFlowController;->a(Z)Lcom/twitter/android/FollowFlowController;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "follow_friends"

    aput-object v2, v1, v6

    const-string/jumbo v2, "nux_tag_invite"

    aput-object v2, v1, v7

    invoke-virtual {v0, v1}, Lcom/twitter/android/FollowFlowController;->a([Ljava/lang/String;)Lcom/twitter/android/FollowFlowController;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/FollowFlowController;->b(Landroid/app/Activity;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected a(Landroid/database/Cursor;)V
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/16 v1, 0x190

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v2}, Lcom/twitter/android/UsersFragment;->d(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "get_older"

    invoke-direct {p0, v0}, Lcom/twitter/android/UsersFragment;->c(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v1, -0x1

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseListFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    invoke-direct {p0, p2}, Lcom/twitter/android/UsersFragment;->b(Landroid/database/Cursor;)V

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-nez p2, :cond_1

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->l:Lcom/twitter/android/aag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->l:Lcom/twitter/android/aag;

    invoke-interface {v0, v5, v1, v1, v2}, Lcom/twitter/android/aag;->a(IIILjava/util/ArrayList;)V

    goto :goto_0

    :cond_1
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->k:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {p2, v0, v1, v2, p0}, Lcom/twitter/android/cx;->a(Landroid/database/Cursor;Ljava/util/Locale;Ljava/util/HashMap;Landroid/content/Context;Lcom/twitter/android/cy;)V

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v2, v4, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/twitter/android/UsersFragment;->D:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v4}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ":follow_friends:::resolvable"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->e(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->l:Lcom/twitter/android/aag;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->l:Lcom/twitter/android/aag;

    invoke-interface {v0, v5, v1, v1, v2}, Lcom/twitter/android/aag;->a(IIILjava/util/ArrayList;)V

    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersFragment;->b(I)V

    goto :goto_0

    :pswitch_1
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->p:Lcom/twitter/android/tn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->p:Lcom/twitter/android/tn;

    iget v1, p0, Lcom/twitter/android/UsersFragment;->r:I

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/tn;->a(II)V

    goto/16 :goto_0

    :pswitch_2
    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->D:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v3}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    const-string/jumbo v3, "follow_friends:not_followed::followable"

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->e(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto/16 :goto_0

    :pswitch_3
    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ao:Z

    if-eqz v0, :cond_0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseListFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    invoke-direct {p0, p2}, Lcom/twitter/android/UsersFragment;->b(Landroid/database/Cursor;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public bridge synthetic a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V
    .locals 0

    check-cast p1, Lcom/twitter/library/widget/BaseUserView;

    check-cast p2, Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/UsersFragment;->a(Lcom/twitter/library/widget/BaseUserView;Lcom/twitter/library/api/PromotedContent;Landroid/os/Bundle;)V

    return-void
.end method

.method public a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 3

    invoke-virtual {p1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    sub-int v1, p3, v0

    iget-object v2, p0, Lcom/twitter/android/UsersFragment;->o:Lcom/twitter/android/widget/cg;

    invoke-virtual {v2, v1}, Lcom/twitter/android/widget/cg;->b(I)I

    move-result v1

    iget v2, p0, Lcom/twitter/android/UsersFragment;->r:I

    if-ne v1, v2, :cond_1

    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersFragment;->startActivity(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-lez v0, :cond_2

    add-int/lit8 v0, v0, -0x1

    if-gt p3, v0, :cond_2

    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v0

    if-lez v0, :cond_3

    invoke-virtual {p1}, Landroid/widget/ListView;->getCount()I

    move-result v1

    sub-int v0, v1, v0

    add-int/lit8 v0, v0, -0x1

    if-le p3, v0, :cond_3

    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_3
    instance-of v0, p2, Lcom/twitter/library/widget/UserView;

    if-eqz v0, :cond_4

    move-object v0, p2

    check-cast v0, Lcom/twitter/library/widget/UserView;

    :goto_1
    if-eqz v0, :cond_5

    iget-object v1, v0, Lcom/twitter/library/widget/UserView;->o:Landroid/widget/CheckBox;

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "user_checkbox"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, v0, Lcom/twitter/library/widget/UserView;->o:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->toggle()V

    invoke-virtual {p0, v0, p4, p5}, Lcom/twitter/android/UsersFragment;->c(Lcom/twitter/library/widget/UserView;J)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    const-string/jumbo v0, "user"

    invoke-direct {p0, p2, p4, p5, v0}, Lcom/twitter/android/UsersFragment;->a(Landroid/view/View;JLjava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/aaf;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/UsersFragment;->m:Lcom/twitter/android/aaf;

    return-void
.end method

.method a(Lcom/twitter/android/aag;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/UsersFragment;->l:Lcom/twitter/android/aag;

    return-void
.end method

.method protected a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;Z)V
    .locals 3

    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->S()Lcom/twitter/android/aaa;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/aaa;->a()I

    move-result v1

    iget v2, p1, Lcom/twitter/library/util/ao;->g:I

    if-ne v1, v2, :cond_0

    invoke-virtual {v0, p2}, Lcom/twitter/android/aaa;->a(Ljava/util/HashMap;)V

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ai:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->ak:Lcom/twitter/android/ClusterFollowAdapterData;

    invoke-virtual {v0, p2}, Lcom/twitter/android/ClusterFollowAdapterData;->a(Ljava/util/HashMap;)V

    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/library/widget/BaseUserView;Lcom/twitter/library/api/PromotedContent;Landroid/os/Bundle;)V
    .locals 5

    invoke-virtual {p1}, Lcom/twitter/library/widget/BaseUserView;->getUserId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->G()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->U:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/twitter/library/widget/BaseUserView;->getPromotedContent()Lcom/twitter/library/api/PromotedContent;

    move-result-object v3

    invoke-virtual {p1}, Lcom/twitter/library/widget/BaseUserView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/zx;

    iget-object v0, v0, Lcom/twitter/android/zx;->f:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v0, v4}, Lcom/twitter/library/scribe/ScribeItem;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    const-string/jumbo v1, "position"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/twitter/library/scribe/ScribeItem;->g:I

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->J:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->z:Ljava/util/HashSet;

    iget-object v1, p2, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->a:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {v0, v1, p2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V

    :cond_1
    instance-of v0, p1, Lcom/twitter/library/widget/UserView;

    if-eqz v0, :cond_3

    move-object v0, p1

    check-cast v0, Lcom/twitter/library/widget/UserView;

    iget-boolean v1, p0, Lcom/twitter/android/UsersFragment;->at:Z

    if-eqz v1, :cond_2

    const v1, 0x7f0202b9    # com.twitter.android.R.drawable.list_item_disabled_bg

    invoke-virtual {p1, v1}, Lcom/twitter/library/widget/BaseUserView;->setBackgroundResource(I)V

    :cond_2
    iget-object v1, v0, Lcom/twitter/library/widget/UserView;->o:Landroid/widget/CheckBox;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/twitter/library/widget/UserView;->o:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/twitter/android/UsersFragment;->g:Ljava/util/HashSet;

    invoke-virtual {v0}, Lcom/twitter/library/widget/UserView;->getUserId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_3
    return-void
.end method

.method public a(Lcom/twitter/library/widget/UserView;J)V
    .locals 3

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getUserId()J

    move-result-wide v0

    const-string/jumbo v2, "cluster_follow"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/twitter/android/UsersFragment;->a(Landroid/view/View;JLjava/lang/String;)V

    return-void
.end method

.method public a(Lcom/twitter/library/widget/UserView;JZ)V
    .locals 6

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iput-wide p2, p0, Lcom/twitter/android/UsersFragment;->c:J

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f053a    # com.twitter.android.R.string.users_remove_list_member

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0539    # com.twitter.android.R.string.users_remove_from_list_question

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0571    # com.twitter.android.R.string.yes

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f029d    # com.twitter.android.R.string.no

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/android/zx;

    iget-object v0, p1, Lcom/twitter/library/widget/UserView;->m:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0}, Lcom/twitter/library/widget/ActionButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getPromotedContent()Lcom/twitter/library/api/PromotedContent;

    move-result-object v3

    move-object v0, p0

    move-wide v1, p2

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/UsersFragment;->a(JLcom/twitter/library/api/PromotedContent;Lcom/twitter/android/zx;Z)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getPromotedContent()Lcom/twitter/library/api/PromotedContent;

    move-result-object v3

    move-object v0, p0

    move-wide v1, p2

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/UsersFragment;->b(JLcom/twitter/library/api/PromotedContent;Lcom/twitter/android/zx;Z)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/widget/UserView;Lcom/twitter/library/api/PromotedContent;I)V
    .locals 7

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/zx;

    iget-wide v2, v0, Lcom/twitter/android/zx;->c:J

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getUserId()J

    move-result-wide v4

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->L:Ljava/util/HashSet;

    new-instance v6, Lcom/twitter/android/cq;

    invoke-direct {v6, v2, v3, v4, v5}, Lcom/twitter/android/cq;-><init>(JJ)V

    invoke-virtual {v1, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v6, p0, Lcom/twitter/android/UsersFragment;->K:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v6, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v0, v0, Lcom/twitter/android/zx;->f:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v4, v5, p2, v0, v2}, Lcom/twitter/library/scribe/ScribeItem;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    const-string/jumbo v1, "position"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/UsersFragment;->a(Lcom/twitter/library/widget/BaseUserView;Lcom/twitter/library/api/PromotedContent;Landroid/os/Bundle;)V

    return-void
.end method

.method protected a(Z)V
    .locals 2

    const/4 v1, 0x3

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->a(Z)V

    if-eqz p1, :cond_1

    invoke-virtual {p0, v1}, Lcom/twitter/android/UsersFragment;->a_(I)V

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->P()Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {p0, v1}, Lcom/twitter/android/UsersFragment;->a_(I)V

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->p()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/twitter/android/UsersFragment;->d(I)Z

    goto :goto_0
.end method

.method public a([Ljava/lang/String;[Ljava/lang/String;II)V
    .locals 10

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const-wide/16 v5, -0x1

    iget-boolean v7, p0, Lcom/twitter/android/UsersFragment;->E:Z

    move-object v1, p1

    move-object v2, p2

    move v8, p3

    move v9, p4

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/android/client/c;->a([Ljava/lang/String;[Ljava/lang/String;[JIJZII)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/UsersFragment;->a(Ljava/lang/String;I)V

    return-void
.end method

.method b(J)Ljava/lang/Long;
    .locals 2

    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->S()Lcom/twitter/android/aaa;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/aaa;->c(J)Ljava/lang/Long;

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->b:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method

.method public b(Lcom/twitter/library/widget/UserView;J)V
    .locals 3

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getUserId()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/twitter/android/UsersFragment;->a(Lcom/twitter/library/widget/UserView;JZ)V

    return-void
.end method

.method public c(Lcom/twitter/library/widget/UserView;J)V
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v2, 0x7f090268    # com.twitter.android.R.id.select_all_checkbox

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iget-object v2, p1, Lcom/twitter/library/widget/UserView;->o:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/twitter/android/UsersFragment;->g:Ljava/util/HashSet;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v2, p2, p3}, Lcom/twitter/library/util/FriendshipCache;->b(J)V

    iget-object v2, p0, Lcom/twitter/android/UsersFragment;->g:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v2

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v3}, Landroid/support/v4/widget/CursorAdapter;->getCount()I

    move-result v3

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->m:Lcom/twitter/android/aaf;

    invoke-interface {v0}, Lcom/twitter/android/aaf;->b()V

    return-void

    :cond_1
    iget-object v2, p0, Lcom/twitter/android/UsersFragment;->g:Ljava/util/HashSet;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v2, p2, p3}, Lcom/twitter/library/util/FriendshipCache;->e(J)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method

.method public c(Z)V
    .locals 1

    if-eqz p1, :cond_0

    const-string/jumbo v0, "get_newer"

    invoke-direct {p0, v0}, Lcom/twitter/android/UsersFragment;->c(Ljava/lang/String;)V

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->c(Z)V

    return-void
.end method

.method public d(Lcom/twitter/library/widget/UserView;J)V
    .locals 2

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/zx;

    iget-object v1, p1, Lcom/twitter/library/widget/UserView;->n:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v1}, Lcom/twitter/library/widget/ActionButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getPromotedContent()Lcom/twitter/library/api/PromotedContent;

    move-result-object v1

    invoke-direct {p0, p2, p3, v1, v0}, Lcom/twitter/android/UsersFragment;->a(JLcom/twitter/library/api/PromotedContent;Lcom/twitter/android/zx;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getPromotedContent()Lcom/twitter/library/api/PromotedContent;

    move-result-object v1

    invoke-direct {p0, p2, p3, v1, v0}, Lcom/twitter/android/UsersFragment;->b(JLcom/twitter/library/api/PromotedContent;Lcom/twitter/android/zx;)V

    goto :goto_0
.end method

.method protected d(I)Z
    .locals 10

    invoke-virtual {p0, p1}, Lcom/twitter/android/UsersFragment;->i(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget v2, p0, Lcom/twitter/android/UsersFragment;->s:I

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    iget-wide v1, p0, Lcom/twitter/android/UsersFragment;->Q:J

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->I:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/twitter/android/UsersFragment;->l(I)I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/client/c;->a(JLjava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {p0, p1}, Lcom/twitter/android/UsersFragment;->a_(I)V

    invoke-virtual {p0, v0, p1}, Lcom/twitter/android/UsersFragment;->a(Ljava/lang/String;I)V

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_1
    iget-wide v1, p0, Lcom/twitter/android/UsersFragment;->Q:J

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->I:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/twitter/android/UsersFragment;->l(I)I

    move-result v4

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/client/c;->a(JLjava/lang/String;II)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/twitter/android/UsersFragment;->l(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/c;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersFragment;->d(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_1

    :pswitch_2
    iget-wide v1, p0, Lcom/twitter/android/UsersFragment;->Q:J

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->H:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->b(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_3
    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->H:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/twitter/android/UsersFragment;->l(I)I

    move-result v2

    iget v3, p0, Lcom/twitter/android/UsersFragment;->s:I

    iget-wide v4, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/client/c;->a(Ljava/lang/String;IIJ)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_4
    iget v1, p0, Lcom/twitter/android/UsersFragment;->s:I

    iget-wide v2, p0, Lcom/twitter/android/UsersFragment;->Q:J

    iget-wide v4, p0, Lcom/twitter/android/UsersFragment;->e:J

    invoke-direct {p0, p1}, Lcom/twitter/android/UsersFragment;->l(I)I

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/c;->a(IJJI)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_5
    iget v1, p0, Lcom/twitter/android/UsersFragment;->s:I

    iget-wide v2, p0, Lcom/twitter/android/UsersFragment;->Q:J

    iget-wide v4, p0, Lcom/twitter/android/UsersFragment;->e:J

    invoke-direct {p0, p1}, Lcom/twitter/android/UsersFragment;->l(I)I

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/c;->a(IJJI)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_6
    const/4 v2, 0x1

    invoke-direct {p0, p1}, Lcom/twitter/android/UsersFragment;->l(I)I

    move-result v3

    iget v4, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/4 v1, 0x1

    if-ne p1, v1, :cond_3

    const/4 v1, 0x1

    :goto_2
    invoke-direct {p0, v1}, Lcom/twitter/android/UsersFragment;->f(Z)[J

    move-result-object v1

    invoke-virtual {v0, v2, v3, v4, v1}, Lcom/twitter/android/client/c;->a(ZII[J)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    :pswitch_7
    iget-boolean v2, p0, Lcom/twitter/android/UsersFragment;->ao:Z

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    invoke-direct {p0, p1}, Lcom/twitter/android/UsersFragment;->l(I)I

    move-result v3

    iget v4, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/4 v1, 0x1

    if-ne p1, v1, :cond_4

    const/4 v1, 0x1

    :goto_3
    invoke-direct {p0, v1}, Lcom/twitter/android/UsersFragment;->f(Z)[J

    move-result-object v1

    invoke-virtual {v0, v2, v3, v4, v1}, Lcom/twitter/android/client/c;->a(ZII[J)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_4
    const/4 v1, 0x0

    goto :goto_3

    :pswitch_8
    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v3, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/4 v4, 0x6

    iget-wide v5, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/c;->a(ZIIIJ)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :pswitch_9
    const/4 v0, 0x3

    if-ne p1, v0, :cond_7

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/twitter/android/UsersFragment;->ab:Z

    if-eqz v1, :cond_5

    iget v1, p0, Lcom/twitter/android/UsersFragment;->v:I

    and-int/lit8 v1, v1, 0x1

    if-nez v1, :cond_5

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    iget v1, p0, Lcom/twitter/android/UsersFragment;->v:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/twitter/android/UsersFragment;->v:I

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->D:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v1}, Lcom/twitter/android/FollowFlowController;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;)Lcom/twitter/android/util/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/twitter/android/util/d;->g()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->u()V

    goto/16 :goto_1

    :cond_5
    iget-boolean v1, p0, Lcom/twitter/android/UsersFragment;->ab:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->D:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v1}, Lcom/twitter/android/FollowFlowController;->b()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->u()V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/twitter/android/UsersFragment;->g(I)V

    goto/16 :goto_1

    :cond_6
    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->D:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v1}, Lcom/twitter/android/FollowFlowController;->b()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/twitter/android/UsersFragment;->b(I)V

    goto/16 :goto_1

    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_0

    :pswitch_a
    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->B:[J

    iget v4, p0, Lcom/twitter/android/UsersFragment;->s:I

    iget-wide v5, p0, Lcom/twitter/android/UsersFragment;->e:J

    const/4 v7, 0x0

    const/4 v8, -0x1

    const/4 v9, -0x1

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/android/client/c;->a([Ljava/lang/String;[Ljava/lang/String;[JIJZII)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :pswitch_b
    invoke-direct {p0, p1}, Lcom/twitter/android/UsersFragment;->l(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :pswitch_c
    iget-wide v1, p0, Lcom/twitter/android/UsersFragment;->Q:J

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->I:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/twitter/android/UsersFragment;->l(I)I

    move-result v4

    const/16 v5, 0x10

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/client/c;->a(JLjava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_8
    move-object v0, v1

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_2
        :pswitch_9
        :pswitch_0
        :pswitch_6
        :pswitch_8
        :pswitch_a
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_0
        :pswitch_b
        :pswitch_6
        :pswitch_8
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public e()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ac:Z

    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->Q()V

    return-void
.end method

.method public e(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/UsersFragment;->ad:Z

    return-void
.end method

.method protected g()V
    .locals 0

    return-void
.end method

.method public g(I)V
    .locals 0

    iput p1, p0, Lcom/twitter/android/UsersFragment;->an:I

    return-void
.end method

.method public i()V
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersFragment;->d(I)Z

    return-void
.end method

.method public n_()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ac:Z

    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 15

    invoke-super/range {p0 .. p1}, Lcom/twitter/android/client/BaseListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->o:Lcom/twitter/android/widget/cg;

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v3

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->d()Z

    move-result v6

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v7

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    sget-object v4, Lcom/twitter/library/provider/cm;->a:[Ljava/lang/String;

    invoke-virtual {v0}, Landroid/widget/ListView;->getChoiceMode()I

    move-result v0

    if-eqz v0, :cond_5

    const-string/jumbo v0, "LOWER(username) ASC"

    :goto_0
    const/4 v2, 0x0

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    iget v10, p0, Lcom/twitter/android/UsersFragment;->s:I

    packed-switch v10, :pswitch_data_0

    :pswitch_0
    sget-object v10, Lcom/twitter/library/provider/ax;->i:Landroid/net/Uri;

    iget-wide v11, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v10, v11, v12}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v10

    iput-object v10, p0, Lcom/twitter/android/UsersFragment;->A:Landroid/net/Uri;

    :cond_0
    move v14, v2

    move-object v2, v0

    move v0, v14

    :goto_1
    iput-object v4, p0, Lcom/twitter/android/UsersFragment;->F:[Ljava/lang/String;

    iput-object v2, p0, Lcom/twitter/android/UsersFragment;->G:Ljava/lang/String;

    iget v2, p0, Lcom/twitter/android/UsersFragment;->V:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_1

    iget-object v2, p0, Lcom/twitter/android/UsersFragment;->A:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string/jumbo v4, "limit"

    iget v10, p0, Lcom/twitter/android/UsersFragment;->V:I

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v4, v10}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/UsersFragment;->A:Landroid/net/Uri;

    :cond_1
    if-eqz v6, :cond_2

    const-string/jumbo v2, "follow"

    const/4 v4, 0x0

    invoke-virtual {v7, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    const v0, 0x7f020085    # com.twitter.android.R.drawable.btn_follow_action

    :cond_2
    iget v2, p0, Lcom/twitter/android/UsersFragment;->s:I

    packed-switch v2, :pswitch_data_1

    :pswitch_1
    invoke-direct {p0, v7, v0}, Lcom/twitter/android/UsersFragment;->a(Landroid/os/Bundle;I)Lcom/twitter/android/aaa;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/aaa;->a(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, p0}, Lcom/twitter/android/aaa;->a(Lcom/twitter/android/ob;)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    new-instance v0, Lcom/twitter/android/widget/cg;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/widget/BaseAdapter;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/widget/cg;-><init>([Landroid/widget/BaseAdapter;I)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->o:Lcom/twitter/android/widget/cg;

    :cond_3
    :goto_2
    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->o:Lcom/twitter/android/widget/cg;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v0, Lcom/twitter/android/aai;

    invoke-direct {v0, p0}, Lcom/twitter/android/aai;-><init>(Lcom/twitter/android/UsersFragment;)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->O:Lcom/twitter/library/client/j;

    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->S()Lcom/twitter/android/aaa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/aaa;->a()I

    move-result v0

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/UsersFragment;->a(ILcom/twitter/library/util/ar;)V

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ai:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x2

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/UsersFragment;->a(ILcom/twitter/library/util/ar;)V

    :cond_4
    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->K()V

    return-void

    :cond_5
    const-string/jumbo v0, "_id ASC"

    goto/16 :goto_0

    :pswitch_2
    sget-object v4, Lcom/twitter/library/provider/ax;->g:Landroid/net/Uri;

    iget-wide v10, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v4, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/UsersFragment;->A:Landroid/net/Uri;

    sget-object v4, Lcom/twitter/library/provider/cm;->b:[Ljava/lang/String;

    move v14, v2

    move-object v2, v0

    move v0, v14

    goto/16 :goto_1

    :pswitch_3
    sget-object v4, Lcom/twitter/library/provider/ax;->h:Landroid/net/Uri;

    iget-wide v10, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v4, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/UsersFragment;->A:Landroid/net/Uri;

    sget-object v4, Lcom/twitter/library/provider/cm;->b:[Ljava/lang/String;

    move v14, v2

    move-object v2, v0

    move v0, v14

    goto/16 :goto_1

    :pswitch_4
    sget-object v10, Lcom/twitter/library/provider/ax;->f:Landroid/net/Uri;

    iget-wide v11, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v10, v11, v12}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v10

    iput-object v10, p0, Lcom/twitter/android/UsersFragment;->A:Landroid/net/Uri;

    move v14, v2

    move-object v2, v0

    move v0, v14

    goto/16 :goto_1

    :pswitch_5
    sget-object v4, Lcom/twitter/library/provider/ax;->y:Landroid/net/Uri;

    iget-wide v10, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v4, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/UsersFragment;->A:Landroid/net/Uri;

    sget-object v4, Lcom/twitter/library/provider/cm;->b:[Ljava/lang/String;

    move v14, v2

    move-object v2, v0

    move v0, v14

    goto/16 :goto_1

    :pswitch_6
    iget-wide v10, p0, Lcom/twitter/android/UsersFragment;->e:J

    const-wide/16 v12, 0x0

    cmp-long v10, v10, v12

    if-lez v10, :cond_6

    iget-wide v10, p0, Lcom/twitter/android/UsersFragment;->Q:J

    const-wide/16 v12, 0x0

    cmp-long v10, v10, v12

    if-lez v10, :cond_6

    sget-object v10, Lcom/twitter/library/provider/ax;->c:Landroid/net/Uri;

    iget-wide v11, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v10, v11, v12}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v10

    iput-object v10, p0, Lcom/twitter/android/UsersFragment;->A:Landroid/net/Uri;

    :cond_6
    if-eqz v6, :cond_0

    iget-wide v10, p0, Lcom/twitter/android/UsersFragment;->Q:J

    cmp-long v10, v10, v8

    if-nez v10, :cond_0

    const v2, 0x7f020162    # com.twitter.android.R.drawable.ic_deny_default

    move v14, v2

    move-object v2, v0

    move v0, v14

    goto/16 :goto_1

    :pswitch_7
    sget-object v10, Lcom/twitter/library/provider/ax;->m:Landroid/net/Uri;

    iget-wide v11, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v10, v11, v12}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v10

    iput-object v10, p0, Lcom/twitter/android/UsersFragment;->A:Landroid/net/Uri;

    move v14, v2

    move-object v2, v0

    move v0, v14

    goto/16 :goto_1

    :pswitch_8
    sget-object v10, Lcom/twitter/library/provider/ax;->b:Landroid/net/Uri;

    iget-wide v11, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v10, v11, v12}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v10

    iput-object v10, p0, Lcom/twitter/android/UsersFragment;->A:Landroid/net/Uri;

    move v14, v2

    move-object v2, v0

    move v0, v14

    goto/16 :goto_1

    :pswitch_9
    sget-object v0, Lcom/twitter/library/provider/ax;->j:Landroid/net/Uri;

    iget-wide v10, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v0, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->A:Landroid/net/Uri;

    const-string/jumbo v0, "(friendship & 1) ASC, LOWER(name) ASC"

    move v14, v2

    move-object v2, v0

    move v0, v14

    goto/16 :goto_1

    :pswitch_a
    sget-object v4, Lcom/twitter/library/provider/ag;->b:Landroid/net/Uri;

    iget-wide v10, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v4, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/UsersFragment;->A:Landroid/net/Uri;

    sget-object v4, Lcom/twitter/library/provider/cm;->d:[Ljava/lang/String;

    move v14, v2

    move-object v2, v0

    move v0, v14

    goto/16 :goto_1

    :pswitch_b
    sget-object v4, Lcom/twitter/library/provider/ax;->w:Landroid/net/Uri;

    iget-wide v10, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v4, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/UsersFragment;->A:Landroid/net/Uri;

    sget-object v4, Lcom/twitter/library/provider/cm;->b:[Ljava/lang/String;

    move v14, v2

    move-object v2, v0

    move v0, v14

    goto/16 :goto_1

    :pswitch_c
    sget-object v4, Lcom/twitter/library/provider/ax;->x:Landroid/net/Uri;

    iget-wide v10, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v4, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/UsersFragment;->A:Landroid/net/Uri;

    sget-object v4, Lcom/twitter/library/provider/cm;->b:[Ljava/lang/String;

    move v14, v2

    move-object v2, v0

    move v0, v14

    goto/16 :goto_1

    :pswitch_d
    sget-object v0, Lcom/twitter/library/provider/at;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v4, "limit"

    const-string/jumbo v10, "20"

    invoke-virtual {v0, v4, v10}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->A:Landroid/net/Uri;

    const-string/jumbo v0, "updated_at DESC, _id ASC"

    sget-object v4, Lcom/twitter/library/provider/cm;->c:[Ljava/lang/String;

    move v14, v2

    move-object v2, v0

    move v0, v14

    goto/16 :goto_1

    :pswitch_e
    iget-boolean v4, p0, Lcom/twitter/android/UsersFragment;->ar:Z

    if-eqz v4, :cond_8

    sget-object v0, Lcom/twitter/library/provider/ax;->v:Landroid/net/Uri;

    iget-wide v10, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v0, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->A:Landroid/net/Uri;

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->A:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    sget-object v0, Lcom/twitter/android/UsersFragment;->y:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    const-string/jumbo v0, "autofollowIds"

    invoke-static {v11, v12}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v0, v11}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_3

    :cond_7
    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->A:Landroid/net/Uri;

    sget-object v0, Lcom/twitter/android/UsersFragment;->y:Ljava/util/Set;

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/provider/ax;->a(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v0

    :goto_4
    sget-object v4, Lcom/twitter/library/provider/cm;->b:[Ljava/lang/String;

    move v14, v2

    move-object v2, v0

    move v0, v14

    goto/16 :goto_1

    :cond_8
    sget-object v4, Lcom/twitter/library/provider/ax;->u:Landroid/net/Uri;

    iget-wide v10, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v4, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/UsersFragment;->A:Landroid/net/Uri;

    goto :goto_4

    :pswitch_f
    sget-object v10, Lcom/twitter/library/provider/ax;->n:Landroid/net/Uri;

    iget-wide v11, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v10, v11, v12}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v10

    iput-object v10, p0, Lcom/twitter/android/UsersFragment;->A:Landroid/net/Uri;

    move v14, v2

    move-object v2, v0

    move v0, v14

    goto/16 :goto_1

    :pswitch_10
    sget-object v10, Lcom/twitter/library/provider/ax;->o:Landroid/net/Uri;

    iget-wide v11, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v10, v11, v12}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v10

    iput-object v10, p0, Lcom/twitter/android/UsersFragment;->A:Landroid/net/Uri;

    move v14, v2

    move-object v2, v0

    move v0, v14

    goto/16 :goto_1

    :pswitch_11
    sget-object v10, Lcom/twitter/library/provider/ax;->e:Landroid/net/Uri;

    iput-object v10, p0, Lcom/twitter/android/UsersFragment;->A:Landroid/net/Uri;

    move v14, v2

    move-object v2, v0

    move v0, v14

    goto/16 :goto_1

    :pswitch_12
    sget-object v10, Lcom/twitter/library/provider/ax;->p:Landroid/net/Uri;

    iget-wide v11, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v10, v11, v12}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v10

    iput-object v10, p0, Lcom/twitter/android/UsersFragment;->A:Landroid/net/Uri;

    move v14, v2

    move-object v2, v0

    move v0, v14

    goto/16 :goto_1

    :pswitch_13
    sget-object v10, Lcom/twitter/library/provider/ax;->q:Landroid/net/Uri;

    iget-wide v11, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v10, v11, v12}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v10

    iput-object v10, p0, Lcom/twitter/android/UsersFragment;->A:Landroid/net/Uri;

    move v14, v2

    move-object v2, v0

    move v0, v14

    goto/16 :goto_1

    :pswitch_14
    sget-object v10, Lcom/twitter/library/provider/ax;->s:Landroid/net/Uri;

    iget-wide v11, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v10, v11, v12}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v10

    iput-object v10, p0, Lcom/twitter/android/UsersFragment;->A:Landroid/net/Uri;

    if-eqz v6, :cond_0

    iget-wide v10, p0, Lcom/twitter/android/UsersFragment;->Q:J

    cmp-long v10, v10, v8

    if-nez v10, :cond_0

    const v2, 0x7f020085    # com.twitter.android.R.drawable.btn_follow_action

    move v14, v2

    move-object v2, v0

    move v0, v14

    goto/16 :goto_1

    :pswitch_15
    sget-object v4, Lcom/twitter/library/provider/ax;->z:Landroid/net/Uri;

    iget-wide v10, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v4, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/UsersFragment;->A:Landroid/net/Uri;

    sget-object v4, Lcom/twitter/library/provider/cm;->b:[Ljava/lang/String;

    move v14, v2

    move-object v2, v0

    move v0, v14

    goto/16 :goto_1

    :pswitch_16
    sget-object v0, Lcom/twitter/library/provider/ax;->l:Landroid/net/Uri;

    iget-wide v10, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v0, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->A:Landroid/net/Uri;

    sget-object v4, Lcom/twitter/library/provider/cm;->b:[Ljava/lang/String;

    const-string/jumbo v0, "friendship_time DESC"

    move v14, v2

    move-object v2, v0

    move v0, v14

    goto/16 :goto_1

    :pswitch_17
    const/4 v1, 0x0

    invoke-direct {p0, v7, v0, v1}, Lcom/twitter/android/UsersFragment;->a(Landroid/os/Bundle;IZ)Lcom/twitter/android/rh;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/UsersFragment;->Q:J

    cmp-long v1, v1, v8

    if-nez v1, :cond_a

    iget-boolean v1, p0, Lcom/twitter/android/UsersFragment;->av:Z

    if-eqz v1, :cond_9

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/rh;->d(Z)V

    :cond_9
    iget-boolean v1, p0, Lcom/twitter/android/UsersFragment;->aw:Z

    if-eqz v1, :cond_a

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/rh;->e(Z)V

    :cond_a
    invoke-virtual {v0, p0}, Lcom/twitter/android/rh;->a(Lcom/twitter/android/ob;)V

    invoke-virtual {v0, p0}, Lcom/twitter/android/rh;->a(Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    new-instance v0, Lcom/twitter/android/widget/cg;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/widget/BaseAdapter;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/widget/cg;-><init>([Landroid/widget/BaseAdapter;I)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->o:Lcom/twitter/android/widget/cg;

    goto/16 :goto_2

    :pswitch_18
    const/4 v1, 0x1

    invoke-direct {p0, v7, v0, v1}, Lcom/twitter/android/UsersFragment;->a(Landroid/os/Bundle;IZ)Lcom/twitter/android/rh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/rh;->a(Lcom/twitter/android/ob;)V

    invoke-virtual {v0, p0}, Lcom/twitter/android/rh;->a(Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    new-instance v0, Lcom/twitter/android/widget/cg;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/widget/BaseAdapter;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/widget/cg;-><init>([Landroid/widget/BaseAdapter;I)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->o:Lcom/twitter/android/widget/cg;

    goto/16 :goto_2

    :pswitch_19
    invoke-direct {p0, v7, v0}, Lcom/twitter/android/UsersFragment;->b(Landroid/os/Bundle;I)Lcom/twitter/android/widget/g;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/g;->a(Lcom/twitter/android/ob;)V

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/g;->a(Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    new-instance v0, Lcom/twitter/android/widget/cg;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/widget/BaseAdapter;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/widget/cg;-><init>([Landroid/widget/BaseAdapter;I)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->o:Lcom/twitter/android/widget/cg;

    goto/16 :goto_2

    :pswitch_1a
    new-instance v0, Lcom/twitter/android/ip;

    const/4 v2, 0x2

    new-instance v4, Lcom/twitter/android/aae;

    invoke-direct {v4, p0}, Lcom/twitter/android/aae;-><init>(Lcom/twitter/android/UsersFragment;)V

    iget-object v5, p0, Lcom/twitter/android/UsersFragment;->j:Ljava/util/HashMap;

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/ip;-><init>(Landroid/content/Context;ILcom/twitter/android/client/c;Lcom/twitter/library/widget/a;Ljava/util/Map;)V

    invoke-virtual {v0, p0}, Lcom/twitter/android/ip;->a(Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    new-instance v0, Lcom/twitter/android/widget/cg;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/widget/BaseAdapter;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/widget/cg;-><init>([Landroid/widget/BaseAdapter;I)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->o:Lcom/twitter/android/widget/cg;

    goto/16 :goto_2

    :pswitch_1b
    const/4 v2, 0x0

    invoke-direct {p0, v7, v0, v2}, Lcom/twitter/android/UsersFragment;->a(Landroid/os/Bundle;IZ)Lcom/twitter/android/rh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/aaa;->a(Lcom/twitter/android/ob;)V

    invoke-virtual {v0, p0}, Lcom/twitter/android/aaa;->a(Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v2

    iget-wide v4, v2, Lcom/twitter/library/api/TwitterUser;->userId:J

    iget-wide v6, p0, Lcom/twitter/android/UsersFragment;->Q:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_b

    iget-boolean v4, p0, Lcom/twitter/android/UsersFragment;->aw:Z

    if-eqz v4, :cond_b

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/twitter/android/aaa;->e(Z)V

    :cond_b
    iget-wide v4, v2, Lcom/twitter/library/api/TwitterUser;->userId:J

    iget-wide v6, p0, Lcom/twitter/android/UsersFragment;->Q:J

    cmp-long v0, v4, v6

    if-nez v0, :cond_c

    iget-boolean v0, v2, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    if-eqz v0, :cond_c

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/UsersActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "type"

    const/16 v4, 0x12

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    new-instance v2, Lcom/twitter/android/tn;

    new-instance v4, Lcom/twitter/android/to;

    const v5, 0x7f0f01a3    # com.twitter.android.R.string.follow_requests_title

    invoke-virtual {v1, v5}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1, v0}, Lcom/twitter/android/to;-><init>(Ljava/lang/String;Landroid/content/Intent;)V

    invoke-direct {v2, v3, v4}, Lcom/twitter/android/tn;-><init>(Lcom/twitter/android/client/c;Lcom/twitter/android/to;)V

    new-instance v0, Lcom/twitter/android/widget/cg;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/widget/BaseAdapter;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    aput-object v4, v1, v3

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/twitter/android/widget/cg;-><init>([Landroid/widget/BaseAdapter;I)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->o:Lcom/twitter/android/widget/cg;

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/UsersFragment;->r:I

    iput-object v2, p0, Lcom/twitter/android/UsersFragment;->p:Lcom/twitter/android/tn;

    goto/16 :goto_2

    :cond_c
    new-instance v0, Lcom/twitter/android/widget/cg;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/widget/BaseAdapter;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/widget/cg;-><init>([Landroid/widget/BaseAdapter;I)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->o:Lcom/twitter/android/widget/cg;

    goto/16 :goto_2

    :pswitch_1c
    invoke-direct {p0, v7, v0}, Lcom/twitter/android/UsersFragment;->a(Landroid/os/Bundle;I)Lcom/twitter/android/aaa;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/aaa;->b(Z)V

    invoke-virtual {v0, p0}, Lcom/twitter/android/aaa;->a(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, p0}, Lcom/twitter/android/aaa;->a(Lcom/twitter/android/ob;)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    new-instance v0, Lcom/twitter/android/widget/cg;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/widget/BaseAdapter;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/widget/cg;-><init>([Landroid/widget/BaseAdapter;I)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->o:Lcom/twitter/android/widget/cg;

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_6
        :pswitch_8
        :pswitch_7
        :pswitch_9
        :pswitch_f
        :pswitch_a
        :pswitch_5
        :pswitch_12
        :pswitch_13
        :pswitch_10
        :pswitch_0
        :pswitch_0
        :pswitch_16
        :pswitch_11
        :pswitch_14
        :pswitch_e
        :pswitch_15
        :pswitch_b
        :pswitch_c
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_d
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_17
        :pswitch_1b
        :pswitch_1
        :pswitch_1
        :pswitch_1c
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_18
        :pswitch_18
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_17
        :pswitch_1
        :pswitch_1a
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_19
    .end packed-switch
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 9

    const-wide/16 v7, 0x0

    const/4 v6, 0x3

    const/4 v3, 0x0

    const/4 v5, 0x1

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const/4 v0, -0x1

    if-ne v0, p2, :cond_0

    if-eqz p3, :cond_0

    const-string/jumbo v0, "user_id"

    invoke-virtual {p3, v0, v7, v8}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    cmp-long v0, v1, v7

    if-lez v0, :cond_0

    const-string/jumbo v0, "friendship"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "friendship"

    invoke-virtual {p3, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const/16 v0, 0x12

    iget v4, p0, Lcom/twitter/android/UsersFragment;->s:I

    if-ne v0, v4, :cond_4

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->j:Ljava/util/HashMap;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    :pswitch_1
    goto :goto_0

    :pswitch_2
    invoke-static {v3}, Lcom/twitter/library/provider/ay;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->j:Ljava/util/HashMap;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetChanged()V

    goto :goto_0

    :pswitch_3
    invoke-static {v3}, Lcom/twitter/library/provider/ay;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->j:Ljava/util/HashMap;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetChanged()V

    goto :goto_0

    :cond_1
    invoke-static {v3}, Lcom/twitter/library/provider/ay;->c(I)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {v3}, Lcom/twitter/library/provider/ay;->b(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->j:Ljava/util/HashMap;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->j:Ljava/util/HashMap;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0

    :cond_3
    invoke-static {v3}, Lcom/twitter/library/provider/ay;->i(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->j:Ljava/util/HashMap;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/util/FriendshipCache;->a(JI)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/util/FriendshipCache;->b(JI)V

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetChanged()V

    iput-boolean v5, p0, Lcom/twitter/android/UsersFragment;->x:Z

    goto/16 :goto_0

    :pswitch_4
    if-nez p2, :cond_0

    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "who_to_follow:contacts::find_friends:cancel"

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto/16 :goto_0

    :pswitch_5
    if-nez p2, :cond_0

    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "friend_suggestions:contacts::find_friends:cancel"

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onAttach(Landroid/app/Activity;)V

    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->H()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    const v2, 0x7f090124    # com.twitter.android.R.id.import_contacts_btn

    const v1, 0x7f090120    # com.twitter.android.R.id.scan_contacts_view

    const/4 v4, 0x3

    const/4 v7, 0x0

    const/4 v6, 0x2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-eq v0, v2, :cond_0

    if-ne v0, v1, :cond_6

    :cond_0
    if-ne v0, v2, :cond_3

    const-string/jumbo v0, "find_friends"

    invoke-direct {p0, v0}, Lcom/twitter/android/UsersFragment;->a(Ljava/lang/String;)V

    :cond_1
    :goto_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/DialogActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "originating_activity"

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "originating_activity"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "ff"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v2, 0x15

    if-ne v1, v2, :cond_4

    invoke-virtual {p0, v0, v6}, Lcom/twitter/android/UsersFragment;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ap:Z

    :cond_2
    :goto_2
    return-void

    :cond_3
    if-ne v0, v1, :cond_1

    const-string/jumbo v0, "find_more_friends"

    invoke-direct {p0, v0}, Lcom/twitter/android/UsersFragment;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    iget v1, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v2, 0x16

    if-ne v1, v2, :cond_5

    invoke-virtual {p0, v0, v4}, Lcom/twitter/android/UsersFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1

    :cond_5
    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    :cond_6
    const v1, 0x7f090184    # com.twitter.android.R.id.follow_all

    if-ne v0, v1, :cond_9

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    :cond_7
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const/4 v5, 0x7

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    or-int/lit8 v5, v5, 0x1

    invoke-virtual {v1, v3, v4, v5}, Lcom/twitter/library/util/FriendshipCache;->b(JI)V

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->g:Ljava/util/HashSet;

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetChanged()V

    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->O()V

    :cond_8
    invoke-virtual {p1, v7}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_2

    :cond_9
    const v1, 0x7f090268    # com.twitter.android.R.id.select_all_checkbox

    if-eq v0, v1, :cond_a

    const v1, 0x7f090269    # com.twitter.android.R.id.select_all_label

    if-ne v0, v1, :cond_e

    :cond_a
    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    if-eqz v1, :cond_2

    const v1, 0x7f090269    # com.twitter.android.R.id.select_all_label

    if-ne v0, v1, :cond_c

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f090268    # com.twitter.android.R.id.select_all_checkbox

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    :goto_3
    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v1}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_b
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_d

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/twitter/library/util/FriendshipCache;->b(J)V

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->g:Ljava/util/HashSet;

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :goto_4
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_b

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->m:Lcom/twitter/android/aaf;

    invoke-interface {v0}, Lcom/twitter/android/aaf;->b()V

    goto/16 :goto_2

    :cond_c
    check-cast p1, Landroid/widget/CheckBox;

    move-object v0, p1

    goto :goto_3

    :cond_d
    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/twitter/library/util/FriendshipCache;->e(J)V

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->g:Ljava/util/HashSet;

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_e
    const v1, 0x7f090121    # com.twitter.android.R.id.scan_contacts

    if-ne v0, v1, :cond_2

    invoke-static {v4}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f038e    # com.twitter.android.R.string.scan_contacts_confirm_title

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f038c    # com.twitter.android.R.string.scan_contacts_confirm_message

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f02d5    # com.twitter.android.R.string.ok

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0089    # com.twitter.android.R.string.cancel

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {v0, p0, v7}, Lcom/twitter/android/widget/PromptDialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto/16 :goto_2
.end method

.method public bridge synthetic onClick(Lcom/twitter/library/widget/BaseUserView;JI)V
    .locals 0

    check-cast p1, Lcom/twitter/library/widget/UserView;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/twitter/android/UsersFragment;->onClick(Lcom/twitter/library/widget/UserView;JI)V

    return-void
.end method

.method public onClick(Lcom/twitter/library/widget/UserView;JI)V
    .locals 1

    const v0, 0x7f09002c    # com.twitter.android.R.id.action_button

    if-ne p4, v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/twitter/android/UsersFragment;->a(Lcom/twitter/library/widget/UserView;JZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v0, 0x7f09005d    # com.twitter.android.R.id.user_checkbox

    if-ne p4, v0, :cond_2

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/UsersFragment;->c(Lcom/twitter/library/widget/UserView;J)V

    goto :goto_0

    :cond_2
    const v0, 0x7f09003d    # com.twitter.android.R.id.favorite_action_button

    if-ne p4, v0, :cond_0

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/UsersFragment;->d(Lcom/twitter/library/widget/UserView;J)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    const/4 v7, 0x7

    const/4 v6, 0x1

    const/4 v5, -0x1

    const/4 v4, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v0, "type"

    invoke-virtual {v1, v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->R()V

    new-instance v0, Lcom/twitter/android/widget/cd;

    iget-object v2, p0, Lcom/twitter/android/UsersFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v0, p0, v2}, Lcom/twitter/android/widget/cd;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->al:Lcom/twitter/library/view/c;

    const-string/jumbo v0, "category"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->H:Ljava/lang/String;

    const-string/jumbo v0, "tag"

    const-wide/16 v2, -0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/android/UsersFragment;->e:J

    const-string/jumbo v0, "wtf_page"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "wtf_page"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->f:Ljava/lang/Integer;

    :cond_0
    const-string/jumbo v0, "user_ids"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v0

    if-eqz v0, :cond_1

    array-length v2, v0

    if-lez v2, :cond_1

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->B:[J

    :cond_1
    const-string/jumbo v0, "preselect_all"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->C:Z

    const-string/jumbo v0, "map_contacts"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->E:Z

    const-string/jumbo v0, "owner_name"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->I:Ljava/lang/String;

    const-string/jumbo v0, "category_position"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/UsersFragment;->W:I

    const-string/jumbo v0, "limit"

    invoke-virtual {v1, v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/UsersFragment;->V:I

    const-string/jumbo v0, "cluster_follow"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ai:Z

    const-string/jumbo v0, "cluster_follow_experiment"

    invoke-virtual {v1, v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/UsersFragment;->ah:I

    const-string/jumbo v0, "cluster_follow_type"

    invoke-virtual {v1, v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/UsersFragment;->t:I

    const-string/jumbo v0, "cluster_follow_replenish_type"

    invoke-virtual {v1, v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/UsersFragment;->u:I

    const-string/jumbo v0, "fetch_always"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->aa:Z

    const-string/jumbo v0, "upload_contacts"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ab:Z

    const-string/jumbo v0, "hide_bio"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->aq:Z

    const-string/jumbo v0, "in_autofollow_bucket"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ar:Z

    if-eqz p1, :cond_e

    const-string/jumbo v0, "state_flow_controller"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/FollowFlowController;

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->D:Lcom/twitter/android/FollowFlowController;

    const-string/jumbo v0, "state_load_flags"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/UsersFragment;->v:I

    const-string/jumbo v0, "state_dialog_user"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/android/UsersFragment;->c:J

    const-string/jumbo v0, "state_dialog_pc"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/PromotedContent;

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->d:Lcom/twitter/library/api/PromotedContent;

    const-string/jumbo v0, "state_checked_users"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->h:Ljava/util/ArrayList;

    const-string/jumbo v0, "state_pending_follows"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->g:Ljava/util/HashSet;

    const-string/jumbo v0, "state_sync_follow_state"

    invoke-virtual {p1, v0, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ad:Z

    const-string/jumbo v0, "state_lookup_complete_pages"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/UsersFragment;->Y:I

    const-string/jumbo v0, "state_completed_components"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/UsersFragment;->am:I

    const-string/jumbo v0, "state_total_progress_components"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/UsersFragment;->an:I

    const-string/jumbo v0, "state_user_ids"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->B:[J

    const-string/jumbo v0, "state_mediator"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->q:Lcom/twitter/library/api/TwitterUser;

    const-string/jumbo v0, "state_friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    const-string/jumbo v0, "state_friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/FriendshipCache;

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    :goto_0
    const-string/jumbo v0, "state_incoming_friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    const-string/jumbo v0, "state_incoming_friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->j:Ljava/util/HashMap;

    :goto_1
    const-string/jumbo v0, "state_search_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->n:Ljava/lang/String;

    const-string/jumbo v0, "state_total_users"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/UsersFragment;->X:I

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    if-ne v0, v7, :cond_2

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->E:Z

    if-eqz v0, :cond_2

    const-string/jumbo v0, "contact_name_map"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    const-string/jumbo v0, "contact_name_map"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->k:Ljava/util/HashMap;

    :cond_2
    :goto_2
    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/4 v2, 0x6

    if-ne v0, v2, :cond_3

    const-string/jumbo v0, "state_fetched_category_users"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v0, "state_fetched_category_users"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->w:Z

    :cond_3
    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ai:Z

    if-eqz v0, :cond_4

    const-string/jumbo v0, "state_cluster_recommendations"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string/jumbo v0, "state_cluster_recommendations"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->aj:Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

    :cond_4
    const-string/jumbo v0, "state_loader_initialized"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string/jumbo v0, "state_loader_initialized"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ae:Z

    :cond_5
    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ar:Z

    if-eqz v0, :cond_7

    const-string/jumbo v0, "state_autofollow_create_all_friendships_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string/jumbo v0, "state_autofollow_create_all_friendships_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->au:Ljava/lang/String;

    :cond_6
    const-string/jumbo v0, "state_autofollow_has_followed_accounts"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string/jumbo v0, "state_autofollow_has_followed_accounts"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->as:Z

    :cond_7
    const-string/jumbo v0, "state_hide_contacts_import_cta"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ap:Z

    const-string/jumbo v0, "is_hidden"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ac:Z

    :goto_3
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->D:Lcom/twitter/android/FollowFlowController;

    if-nez v0, :cond_8

    new-instance v0, Lcom/twitter/android/FollowFlowController;

    invoke-direct {v0}, Lcom/twitter/android/FollowFlowController;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->D:Lcom/twitter/android/FollowFlowController;

    :cond_8
    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->G()Z

    move-result v0

    if-eqz v0, :cond_9

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->J:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->U:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->K:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->L:Ljava/util/HashSet;

    :cond_9
    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->ae()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ag:Z

    const-string/jumbo v0, "should_ignore_user_view_click"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->at:Z

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ar:Z

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->as:Z

    if-nez v0, :cond_a

    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->L()V

    :cond_a
    invoke-static {}, Lgl;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->av:Z

    invoke-static {}, Lcom/twitter/android/util/af;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->aw:Z

    return-void

    :cond_b
    new-instance v0, Lcom/twitter/library/util/FriendshipCache;

    invoke-direct {v0}, Lcom/twitter/library/util/FriendshipCache;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    goto/16 :goto_0

    :cond_c
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->j:Ljava/util/HashMap;

    goto/16 :goto_1

    :cond_d
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->k:Ljava/util/HashMap;

    goto/16 :goto_2

    :cond_e
    const-string/jumbo v0, "flow_controller"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/FollowFlowController;

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->D:Lcom/twitter/android/FollowFlowController;

    iput v4, p0, Lcom/twitter/android/UsersFragment;->v:I

    const-string/jumbo v0, "friendship_cache"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    const-string/jumbo v0, "friendship_cache"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/FriendshipCache;

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    :goto_4
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->g:Ljava/util/HashSet;

    const-string/jumbo v0, "sync_follow_state"

    invoke-virtual {v1, v0, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ad:Z

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    if-ne v0, v7, :cond_11

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->E:Z

    if-eqz v0, :cond_11

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->k:Ljava/util/HashMap;

    :cond_f
    :goto_5
    const-string/jumbo v0, "hide_contacts_import_cta"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ap:Z

    const-string/jumbo v0, "is_hidden"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ac:Z

    goto/16 :goto_3

    :cond_10
    new-instance v0, Lcom/twitter/library/util/FriendshipCache;

    invoke-direct {v0}, Lcom/twitter/library/util/FriendshipCache;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    goto :goto_4

    :cond_11
    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v2, 0x12

    if-ne v0, v2, :cond_f

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->j:Ljava/util/HashMap;

    goto :goto_5
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 11

    const-wide/16 v1, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v10, 0x0

    packed-switch p1, :pswitch_data_0

    iget-wide v7, p0, Lcom/twitter/android/UsersFragment;->e:J

    cmp-long v0, v7, v1

    if-lez v0, :cond_0

    const-string/jumbo v4, "type=? AND tag=?"

    new-array v5, v3, [Ljava/lang/String;

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v10

    iget-wide v7, p0, Lcom/twitter/android/UsersFragment;->e:J

    invoke-static {v7, v8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    :goto_0
    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v3, 0x1b

    if-ne v0, v3, :cond_7

    move-wide v2, v1

    :goto_1
    new-instance v0, Lcom/twitter/android/bl;

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v6, p0, Lcom/twitter/android/UsersFragment;->A:Landroid/net/Uri;

    invoke-static {v6, v2, v3}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->F:[Ljava/lang/String;

    iget-object v6, p0, Lcom/twitter/android/UsersFragment;->G:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/bl;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    return-object v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/cx;->a(Landroid/content/Context;)Landroid/support/v4/content/Loader;

    move-result-object v0

    goto :goto_2

    :pswitch_1
    sget-object v0, Lcom/twitter/library/provider/ax;->s:Landroid/net/Uri;

    iget-wide v1, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v0, v1, v2}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lcom/twitter/library/provider/cm;->a:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :pswitch_2
    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "session_owner_id"

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    new-instance v0, Lcom/twitter/android/bl;

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v4, p0, Lcom/twitter/android/UsersFragment;->A:Landroid/net/Uri;

    invoke-static {v4, v2, v3}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->F:[Ljava/lang/String;

    const-string/jumbo v4, "(friendship IS NULL OR (friendship & 1 == 0)) AND user_id!=?"

    new-array v5, v6, [Ljava/lang/String;

    iget-wide v6, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v10

    iget-object v6, p0, Lcom/twitter/android/UsersFragment;->G:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/bl;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->B:[J

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/twitter/android/UsersFragment;->B:[J

    array-length v4, v3

    new-array v5, v4, [Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "user_id"

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v7, " IN (?"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-wide v8, v3, v10

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v10

    move v0, v6

    :goto_3
    if-ge v0, v4, :cond_1

    const-string/jumbo v6, ", ?"

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-wide v8, v3, v0

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_1
    const-string/jumbo v0, ")"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    const-string/jumbo v4, "d_page=?"

    new-array v5, v6, [Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v10

    goto/16 :goto_0

    :cond_3
    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    sparse-switch v0, :sswitch_data_0

    move-object v5, v4

    goto/16 :goto_0

    :sswitch_0
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0}, Lcom/twitter/library/util/FriendshipCache;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ar:Z

    if-nez v0, :cond_4

    const-string/jumbo v4, "(friendship IS NULL OR (friendship & 1 == 0)) AND user_id!=?"

    new-array v5, v6, [Ljava/lang/String;

    iget-wide v6, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v10

    goto/16 :goto_0

    :cond_4
    move-object v5, v4

    goto/16 :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->D:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v0}, Lcom/twitter/android/FollowFlowController;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string/jumbo v0, "(friendship IS NULL OR (friendship & 1 == 0)) AND user_id!=?"

    :goto_4
    new-array v4, v6, [Ljava/lang/String;

    iget-wide v5, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v10

    move-object v5, v4

    move-object v4, v0

    goto/16 :goto_0

    :cond_5
    const-string/jumbo v0, "user_id!=?"

    goto :goto_4

    :sswitch_2
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0}, Lcom/twitter/library/util/FriendshipCache;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    const-string/jumbo v4, "(u_friendship IS NULL OR (u_friendship & 1 == 0)) AND u_user_id!=?"

    new-array v5, v6, [Ljava/lang/String;

    iget-wide v6, p0, Lcom/twitter/android/UsersFragment;->Q:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v10

    goto/16 :goto_0

    :cond_6
    move-object v5, v4

    goto/16 :goto_0

    :sswitch_3
    const-string/jumbo v4, "type=? AND tag=?"

    new-array v5, v3, [Ljava/lang/String;

    const/16 v0, 0x11

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v10

    const-string/jumbo v0, "-1"

    aput-object v0, v5, v6

    goto/16 :goto_0

    :sswitch_4
    const-string/jumbo v4, "type=? AND tag=?"

    new-array v5, v3, [Ljava/lang/String;

    const/4 v0, 0x6

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v10

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->H:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    goto/16 :goto_0

    :sswitch_5
    const-string/jumbo v4, "type=? AND tag=?"

    new-array v5, v3, [Ljava/lang/String;

    const/16 v0, 0x18

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v10

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->H:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    goto/16 :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "session_owner_id"

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    move-wide v2, v0

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_4
        0x7 -> :sswitch_1
        0x9 -> :sswitch_2
        0x11 -> :sswitch_3
        0x13 -> :sswitch_0
        0x18 -> :sswitch_5
    .end sparse-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v1, 0x16

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ao:Z

    if-nez v0, :cond_0

    const v0, 0x7f030181    # com.twitter.android.R.layout.users_pymk_wtf_fragment

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/android/UsersFragment;->a(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/client/BaseListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onDestroy()V

    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->S()Lcom/twitter/android/aaa;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/aaa;->a()I

    move-result v0

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/UsersFragment;->b(ILcom/twitter/library/util/ar;)V

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ai:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/UsersFragment;->b(ILcom/twitter/library/util/ar;)V

    :cond_0
    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/UsersFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onResume()V
    .locals 5

    const/4 v2, 0x3

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onResume()V

    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->H()V

    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->J()V

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->A:Landroid/net/Uri;

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->aa:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ae:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0, v2}, Lcom/twitter/android/UsersFragment;->a_(I)V

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->p()V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-wide v1, v0, Lcom/twitter/library/api/TwitterUser;->userId:J

    iget-wide v3, p0, Lcom/twitter/android/UsersFragment;->Q:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_1

    iget-boolean v0, v0, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_1
    invoke-static {}, Lgl;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/UsersFragment;->av:Z

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ac:Z

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->Q()V

    :cond_2
    return-void

    :cond_3
    invoke-virtual {p0, v2}, Lcom/twitter/android/UsersFragment;->d(I)Z

    goto :goto_0

    :cond_4
    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v1, 0x11

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_6

    :cond_5
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->n:Ljava/lang/String;

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->q:Lcom/twitter/library/api/TwitterUser;

    if-eqz v0, :cond_7

    :cond_6
    invoke-virtual {p0, v2}, Lcom/twitter/android/UsersFragment;->a_(I)V

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->p()V

    goto :goto_0

    :cond_7
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v2}, Lcom/twitter/android/UsersFragment;->d(I)Z

    goto :goto_0

    :cond_8
    invoke-virtual {p0, v2}, Lcom/twitter/android/UsersFragment;->b(I)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "state_dialog_user"

    iget-wide v1, p0, Lcom/twitter/android/UsersFragment;->c:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->d:Lcom/twitter/library/api/PromotedContent;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "state_dialog_pc"

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->d:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    const-string/jumbo v0, "state_checked_users"

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->h:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->g:Ljava/util/HashSet;

    if-eqz v0, :cond_2

    const-string/jumbo v0, "state_pending_follows"

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->g:Ljava/util/HashSet;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->B:[J

    if-eqz v0, :cond_3

    const-string/jumbo v0, "state_user_ids"

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->B:[J

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0}, Lcom/twitter/library/util/FriendshipCache;->a()Z

    move-result v0

    if-nez v0, :cond_4

    const-string/jumbo v0, "state_friendship_cache"

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->j:Ljava/util/HashMap;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    const-string/jumbo v0, "state_incoming_friendship_cache"

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->j:Ljava/util/HashMap;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_5
    const-string/jumbo v0, "state_search_id"

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "state_mediator"

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->q:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->k:Ljava/util/HashMap;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->k:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    const-string/jumbo v0, "contact_name_map"

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->k:Ljava/util/HashMap;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_6
    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_7

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v1, 0x18

    if-ne v0, v1, :cond_8

    :cond_7
    const-string/jumbo v0, "state_fetched_category_users"

    iget-boolean v1, p0, Lcom/twitter/android/UsersFragment;->w:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_8
    const-string/jumbo v0, "state_load_flags"

    iget v1, p0, Lcom/twitter/android/UsersFragment;->v:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->aa:Z

    if-eqz v0, :cond_9

    const-string/jumbo v0, "state_loader_initialized"

    iget-boolean v1, p0, Lcom/twitter/android/UsersFragment;->ae:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_9
    const-string/jumbo v0, "state_lookup_complete_pages"

    iget v1, p0, Lcom/twitter/android/UsersFragment;->Y:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "state_completed_components"

    iget v1, p0, Lcom/twitter/android/UsersFragment;->am:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "state_total_progress_components"

    iget v1, p0, Lcom/twitter/android/UsersFragment;->an:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "state_sync_follow_state"

    iget-boolean v1, p0, Lcom/twitter/android/UsersFragment;->ad:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "state_flow_controller"

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->D:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v0, "state_total_users"

    iget v1, p0, Lcom/twitter/android/UsersFragment;->X:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ai:Z

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->ak:Lcom/twitter/android/ClusterFollowAdapterData;

    if-eqz v0, :cond_a

    const-string/jumbo v0, "state_cluster_recommendations"

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->ak:Lcom/twitter/android/ClusterFollowAdapterData;

    invoke-virtual {v1}, Lcom/twitter/android/ClusterFollowAdapterData;->c()Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_a
    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ar:Z

    if-eqz v0, :cond_b

    const-string/jumbo v0, "state_autofollow_create_all_friendships_req_id"

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->au:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "state_autofollow_has_followed_accounts"

    iget-boolean v1, p0, Lcom/twitter/android/UsersFragment;->as:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_b
    const-string/jumbo v0, "state_hide_contacts_import_cta"

    iget-boolean v1, p0, Lcom/twitter/android/UsersFragment;->ap:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "is_hidden"

    iget-boolean v1, p0, Lcom/twitter/android/UsersFragment;->ac:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public onStop()V
    .locals 11

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/UsersFragment;->b:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/twitter/android/UsersFragment;->b:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v7, v2

    check-cast v7, Ljava/lang/Long;

    iget-wide v2, p0, Lcom/twitter/android/UsersFragment;->Q:J

    iget v4, p0, Lcom/twitter/android/UsersFragment;->s:I

    iget-wide v5, p0, Lcom/twitter/android/UsersFragment;->e:J

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    iget-object v9, p0, Lcom/twitter/android/UsersFragment;->f:Ljava/lang/Integer;

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;JIJJLjava/lang/Integer;)Ljava/lang/String;

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->b:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->S()Lcom/twitter/android/aaa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/aaa;->b()V

    iget-boolean v1, p0, Lcom/twitter/android/UsersFragment;->ad:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->g:Ljava/util/HashSet;

    invoke-static {v1}, Lcom/twitter/library/util/Util;->b(Ljava/util/Collection;)[J

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a([J)Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/UsersFragment;->g:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->clear()V

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/android/UsersFragment;->D:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v5}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "follow_friends:::follow_many"

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    array-length v1, v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/twitter/library/scribe/ScribeLog;->e(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_2
    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->G()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->F()V

    :cond_3
    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onStop()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 10

    const v9, 0x7f090268    # com.twitter.android.R.id.select_all_checkbox

    const v8, 0x7f09014d    # com.twitter.android.R.id.find_friends_cta

    const v7, 0x7f090124    # com.twitter.android.R.id.import_contacts_btn

    const/4 v6, 0x0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->V()Landroid/widget/ListView;

    move-result-object v2

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f030180    # com.twitter.android.R.layout.users_fragment_header

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const v3, 0x7f090120    # com.twitter.android.R.id.scan_contacts_view

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget v4, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v5, 0x16

    if-ne v4, v5, :cond_0

    iget-boolean v4, p0, Lcom/twitter/android/UsersFragment;->ao:Z

    if-nez v4, :cond_0

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;->I()Z

    move-result v4

    if-eqz v4, :cond_3

    iget-boolean v4, p0, Lcom/twitter/android/UsersFragment;->ao:Z

    if-nez v4, :cond_3

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    :goto_0
    invoke-virtual {v2, v6}, Landroid/widget/ListView;->setHeaderDividersEnabled(Z)V

    iput-object v0, p0, Lcom/twitter/android/UsersFragment;->af:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    const-string/jumbo v2, "check_all_header"

    invoke-virtual {v1, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-lez v1, :cond_2

    const v2, 0x7f090181    # com.twitter.android.R.id.follow_all_check_header

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f090182    # com.twitter.android.R.id.check_header_desc

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iget-boolean v1, p0, Lcom/twitter/android/UsersFragment;->C:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090269    # com.twitter.android.R.id.select_all_label

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ab:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersFragment;->g(I)V

    :cond_2
    return-void

    :cond_3
    const-string/jumbo v4, "find_friends"

    invoke-virtual {v1, v4, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected p()V
    .locals 2

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v1, 0x16

    if-ne v0, v1, :cond_0

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersFragment;->h(I)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->p()V

    goto :goto_0
.end method

.method public q()Lcom/twitter/library/util/FriendshipCache;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    return-object v0
.end method

.method r()Z
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->ai:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v1, 0x14

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v1, 0x16

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v1, 0x13

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public s()Ljava/util/ArrayList;
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->k:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->k:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v1, p0, Lcom/twitter/android/UsersFragment;->k:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/client/c;->F()Ljava/util/regex/Pattern;

    move-result-object v2

    iget-boolean v3, p0, Lcom/twitter/android/UsersFragment;->ag:Z

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/cx;->a(Ljava/util/List;Ljava/util/Map;Ljava/util/regex/Pattern;Z)Ljava/util/ArrayList;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public u()V
    .locals 4

    const/4 v3, 0x7

    iget-boolean v0, p0, Lcom/twitter/android/UsersFragment;->Z:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, v3}, Lcom/twitter/android/UsersFragment;->i(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/UsersFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    const-wide/16 v1, -0x1

    invoke-virtual {v0, v3, v1, v2}, Lcom/twitter/android/client/c;->a(IJ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v3}, Lcom/twitter/android/UsersFragment;->a(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method public u_()V
    .locals 0

    return-void
.end method

.method public v()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->g:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    return v0
.end method

.method y()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/UsersFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->requery()Z

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public z()I
    .locals 1

    iget v0, p0, Lcom/twitter/android/UsersFragment;->an:I

    return v0
.end method
