.class public Lcom/twitter/android/EventResultsFragment;
.super Lcom/twitter/android/SearchResultsFragment;
.source "Twttr"


# instance fields
.field private J:Landroid/view/View;

.field private K:I

.field private L:I

.field private U:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/SearchResultsFragment;-><init>()V

    return-void
.end method

.method private l(I)V
    .locals 3

    iget v0, p0, Lcom/twitter/android/EventResultsFragment;->L:I

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/EventResultsFragment;->J:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/EventResultsFragment;->J:Landroid/view/View;

    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v2, p1}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    iput p1, p0, Lcom/twitter/android/EventResultsFragment;->L:I

    :cond_1
    return-void
.end method


# virtual methods
.method protected a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    const v0, 0x7f030068    # com.twitter.android.R.layout.event_list_header_fragment

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/android/EventResultsFragment;->a(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(II)V
    .locals 5

    const/4 v1, 0x1

    invoke-direct {p0, p1}, Lcom/twitter/android/EventResultsFragment;->l(I)V

    invoke-virtual {p0}, Lcom/twitter/android/EventResultsFragment;->V()Landroid/widget/ListView;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    add-int v0, p2, p1

    iget v3, p0, Lcom/twitter/android/EventResultsFragment;->K:I

    if-le v0, v3, :cond_1

    add-int v0, p2, p1

    invoke-virtual {v2, v1, v0}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-lt v0, v1, :cond_2

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v1

    :goto_1
    invoke-virtual {v2, v0, v1}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/twitter/android/EventResultsFragment;->K:I

    move v4, v1

    move v1, v0

    move v0, v4

    goto :goto_1
.end method

.method protected a(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/EventResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/twitter/android/EventSearchActivity;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/twitter/android/EventSearchActivity;

    invoke-virtual {v0, p1}, Lcom/twitter/android/EventSearchActivity;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a(Landroid/widget/AbsListView;III)Z
    .locals 3

    const/4 v0, 0x0

    if-lez p3, :cond_1

    iget v1, p0, Lcom/twitter/android/EventResultsFragment;->L:I

    if-lez v1, :cond_1

    if-nez p2, :cond_2

    invoke-virtual {p1, v0}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    neg-int v0, v0

    :cond_0
    :goto_0
    iget v1, p0, Lcom/twitter/android/EventResultsFragment;->L:I

    iget v2, p0, Lcom/twitter/android/EventResultsFragment;->K:I

    sub-int/2addr v1, v2

    neg-int v0, v0

    neg-int v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {p0}, Lcom/twitter/android/EventResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v2, v0, Lcom/twitter/android/EventSearchActivity;

    if-eqz v2, :cond_1

    check-cast v0, Lcom/twitter/android/EventSearchActivity;

    iget v2, p0, Lcom/twitter/android/EventResultsFragment;->U:I

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/EventSearchActivity;->a(II)V

    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/android/SearchResultsFragment;->a(Landroid/widget/AbsListView;III)Z

    move-result v0

    return v0

    :cond_2
    iget v0, p0, Lcom/twitter/android/EventResultsFragment;->L:I

    goto :goto_0
.end method

.method protected a_(I)V
    .locals 0

    return-void
.end method

.method protected b(I)V
    .locals 1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/EventResultsFragment;->p:Lcom/twitter/android/se;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/EventResultsFragment;->p:Lcom/twitter/android/se;

    invoke-interface {v0}, Lcom/twitter/android/se;->a()V

    :cond_0
    return-void
.end method

.method public e()V
    .locals 4

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/EventResultsFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/twitter/android/EventResultsFragment;->K:I

    invoke-virtual {v0, v3, v1}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    :cond_0
    iget v0, p0, Lcom/twitter/android/EventResultsFragment;->t:I

    if-lez v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/EventResultsFragment;->v()V

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/EventResultsFragment;->h:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-virtual {p0}, Lcom/twitter/android/EventResultsFragment;->M()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string/jumbo v2, "new_tweet_prompt"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const/4 v2, 0x0

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "click"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/EventResultsFragment;->b(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method protected k()V
    .locals 0

    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/EventResultsFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/EventResultsFragment;->J:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    iput-boolean v3, p0, Lcom/twitter/android/EventResultsFragment;->x:Z

    invoke-super {p0, p1}, Lcom/twitter/android/SearchResultsFragment;->onActivityCreated(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, -0x1

    invoke-super {p0, p1}, Lcom/twitter/android/SearchResultsFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/EventResultsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c009d    # com.twitter.android.R.dimen.nav_bar_height

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/EventResultsFragment;->K:I

    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Lcom/twitter/android/EventResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/EventResultsFragment;->J:Landroid/view/View;

    iget-object v0, p0, Lcom/twitter/android/EventResultsFragment;->J:Landroid/view/View;

    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    iget v2, p0, Lcom/twitter/android/EventResultsFragment;->K:I

    iget v3, p0, Lcom/twitter/android/EventResultsFragment;->L:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-direct {v1, v4, v2, v4}, Landroid/widget/AbsListView$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, p0}, Lcom/twitter/android/EventResultsFragment;->a(Lcom/twitter/android/client/ah;)V

    invoke-virtual {p0}, Lcom/twitter/android/EventResultsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "fragment_page_number"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/EventResultsFragment;->U:I

    return-void
.end method
