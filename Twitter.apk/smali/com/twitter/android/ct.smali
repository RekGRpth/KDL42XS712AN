.class public Lcom/twitter/android/ct;
.super Lcom/twitter/android/aaa;
.source "Twttr"


# instance fields
.field private a:Lcom/twitter/android/ClusterFollowAdapterData;


# direct methods
.method public constructor <init>(Lcom/twitter/android/ClusterFollowAdapterData;Landroid/content/Context;ILcom/twitter/android/client/c;ILcom/twitter/library/widget/a;Lcom/twitter/library/util/FriendshipCache;IIZZ)V
    .locals 10

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move v3, p5

    move-object/from16 v4, p6

    move-object/from16 v5, p7

    move/from16 v6, p8

    move/from16 v7, p9

    move/from16 v8, p10

    move/from16 v9, p11

    invoke-direct/range {v0 .. v9}, Lcom/twitter/android/aaa;-><init>(Landroid/content/Context;IILcom/twitter/library/widget/a;Lcom/twitter/library/util/FriendshipCache;IIZZ)V

    iput-object p1, p0, Lcom/twitter/android/ct;->a:Lcom/twitter/android/ClusterFollowAdapterData;

    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ct;->a:Lcom/twitter/android/ClusterFollowAdapterData;

    invoke-virtual {v0, p3}, Lcom/twitter/android/ClusterFollowAdapterData;->a(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ct;->a:Lcom/twitter/android/ClusterFollowAdapterData;

    invoke-virtual {v0, p1}, Lcom/twitter/android/ClusterFollowAdapterData;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    invoke-super {p0, v0, p2, p3}, Lcom/twitter/android/aaa;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/android/ct;->a:Lcom/twitter/android/ClusterFollowAdapterData;

    invoke-virtual {v0, p1, p3}, Lcom/twitter/android/ClusterFollowAdapterData;->a(Landroid/view/View;Landroid/database/Cursor;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/aaa;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 3

    invoke-super {p0, p1}, Lcom/twitter/android/aaa;->getItemViewType(I)I

    move-result v1

    invoke-virtual {p0, p1}, Lcom/twitter/android/ct;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/twitter/android/ct;->a:Lcom/twitter/android/ClusterFollowAdapterData;

    invoke-virtual {v2, v0}, Lcom/twitter/android/ClusterFollowAdapterData;->a(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0}, Lcom/twitter/android/aaa;->getViewTypeCount()I

    move-result v0

    add-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/aaa;->getViewTypeCount()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/aaa;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ct;->a:Lcom/twitter/android/ClusterFollowAdapterData;

    invoke-virtual {v1, v0, p1, p2, p3}, Lcom/twitter/android/ClusterFollowAdapterData;->a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
