.class public Lcom/twitter/android/FollowActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/android/aaf;
.implements Lcom/twitter/android/aag;


# static fields
.field private static final a:Z


# instance fields
.field private b:Lcom/twitter/android/FollowFlowController;

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Ljava/util/ArrayList;

.field private g:I

.field private h:I

.field private i:Z

.field private j:Lcom/twitter/android/UsersFragment;

.field private k:Lcom/twitter/android/util/d;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Lcom/twitter/library/client/App;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "digits"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/FollowActivity;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/FollowActivity;->e:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/FollowActivity;->f:Ljava/util/ArrayList;

    iput v1, p0, Lcom/twitter/android/FollowActivity;->g:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/FollowActivity;->h:I

    iput-boolean v1, p0, Lcom/twitter/android/FollowActivity;->i:Z

    return-void
.end method

.method private a(Landroid/content/Intent;II)Landroid/os/Bundle;
    .locals 6

    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-static {p1, v4}, Lcom/twitter/android/UsersFragment;->a(Landroid/content/Intent;Z)Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v0, "override_home"

    invoke-virtual {v1, v0, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "follow"

    invoke-virtual {v1, v0, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    const-string/jumbo v2, "follow_friends"

    invoke-virtual {v0, v2}, Lcom/twitter/android/FollowFlowController;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string/jumbo v0, "preselect_friends"

    invoke-virtual {p1, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    const-string/jumbo v2, "preselect_all"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v2, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v2}, Lcom/twitter/android/FollowFlowController;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    const-string/jumbo v2, "follow_all_title"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v2, "follow_all_subtitle"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-boolean v2, p0, Lcom/twitter/android/FollowActivity;->d:Z

    if-eqz v2, :cond_2

    const-string/jumbo v2, "check_all_header"

    if-eqz v0, :cond_1

    const v0, 0x7f0f0198    # com.twitter.android.R.string.find_friends_precheck_header

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :goto_1
    const-string/jumbo v0, "upload_contacts"

    iget-boolean v2, p0, Lcom/twitter/android/FollowActivity;->d:Z

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "show_follow_all_button"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "user_checkbox"

    invoke-virtual {v1, v0, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "sync_follow_state"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :goto_2
    const-string/jumbo v0, "flow_controller"

    iget-object v2, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v0, "type"

    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "refresh"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "empty_desc"

    const v2, 0x7f0f014b    # com.twitter.android.R.string.empty_find_friends

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v0}, Lcom/twitter/android/FollowFlowController;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    const-string/jumbo v0, "cluster_follow"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    :goto_3
    return-object v1

    :cond_1
    const v0, 0x7f0f019a    # com.twitter.android.R.string.find_friends_uncheck_header

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "check_all_header"

    const v2, 0x7f0f0197    # com.twitter.android.R.string.find_friends_opt_out_header

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_1

    :cond_3
    const-string/jumbo v0, "follow_all_title"

    if-lez p2, :cond_4

    :goto_4
    invoke-virtual {v1, v0, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "follow_all_subtitle"

    if-lez p3, :cond_5

    :goto_5
    invoke-virtual {v1, v0, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "show_follow_all_button"

    invoke-virtual {v1, v0, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "upload_contacts"

    invoke-virtual {v1, v0, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "user_checkbox"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "sync_follow_state"

    invoke-virtual {v1, v0, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_2

    :cond_4
    const p2, 0x7f0f0263    # com.twitter.android.R.string.matched_contacts_format

    goto :goto_4

    :cond_5
    const p3, 0x7f0f0392    # com.twitter.android.R.string.scanned_contacts_subtitle

    goto :goto_5

    :cond_6
    const-string/jumbo v0, "cluster_follow"

    invoke-static {p0, v4}, Lcom/twitter/android/util/c;->a(Landroid/content/Context;I)Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "cluster_follow_experiment"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_3

    :cond_7
    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    const-string/jumbo v2, "follow_recommendations"

    invoke-virtual {v0, v2}, Lcom/twitter/android/FollowFlowController;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    const-string/jumbo v0, "follow_all_title"

    if-lez p2, :cond_8

    :goto_6
    invoke-virtual {v1, v0, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "follow_all_subtitle"

    if-lez p3, :cond_9

    :goto_7
    invoke-virtual {v1, v0, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-static {p0}, Lcom/twitter/android/SignedOutActivity;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string/jumbo v0, "type"

    const/16 v2, 0x1b

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :goto_8
    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v0}, Lcom/twitter/android/FollowFlowController;->b()Z

    move-result v0

    if-eqz v0, :cond_b

    const-string/jumbo v0, "follow_all_subtitle"

    const v2, 0x7f0f010b    # com.twitter.android.R.string.digits_wtf_header

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :goto_9
    const-string/jumbo v0, "cluster_follow"

    invoke-static {p0, v3}, Lcom/twitter/android/util/c;->a(Landroid/content/Context;I)Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "cluster_follow_experiment"

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_3

    :cond_8
    const p2, 0x7f0f048a    # com.twitter.android.R.string.suggestions_for_you

    goto :goto_6

    :cond_9
    const p3, 0x7f0f048c    # com.twitter.android.R.string.suggestions_for_you_subtitle

    goto :goto_7

    :cond_a
    const-string/jumbo v0, "type"

    const/16 v2, 0x13

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_8

    :cond_b
    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->k:Lcom/twitter/android/util/d;

    invoke-interface {v0}, Lcom/twitter/android/util/d;->h()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-direct {p0, v1}, Lcom/twitter/android/FollowActivity;->d(Landroid/os/Bundle;)V

    :cond_c
    const-string/jumbo v0, "empty_desc"

    const v2, 0x7f0f014c    # com.twitter.android.R.string.empty_find_friends_and_wtf

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_9

    :cond_d
    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    const-string/jumbo v2, "follow_interest"

    invoke-virtual {v0, v2}, Lcom/twitter/android/FollowFlowController;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "type"

    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "tag"

    iget-object v2, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v2}, Lcom/twitter/android/FollowFlowController;->d()Lcom/twitter/android/util/x;

    move-result-object v2

    iget-wide v2, v2, Lcom/twitter/android/util/x;->c:J

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v0, "check_all_header"

    const v2, 0x7f0f01a1    # com.twitter.android.R.string.follow_interest_check_header

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "show_follow_all_button"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "user_checkbox"

    invoke-virtual {v1, v0, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "preselect_all"

    invoke-virtual {v1, v0, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "sync_follow_state"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "refresh"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "flow_controller"

    iget-object v2, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto/16 :goto_3
.end method

.method static synthetic a(Lcom/twitter/android/FollowActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/FollowActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/FollowActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/FollowActivity;->i:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/android/FollowActivity;)Lcom/twitter/android/UsersFragment;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->j:Lcom/twitter/android/UsersFragment;

    return-object v0
.end method

.method private c(Landroid/os/Bundle;)Lcom/twitter/android/UsersFragment;
    .locals 2

    new-instance v0, Lcom/twitter/android/UsersFragment;

    invoke-direct {v0}, Lcom/twitter/android/UsersFragment;-><init>()V

    invoke-virtual {v0, p1}, Lcom/twitter/android/UsersFragment;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {v0, p0}, Lcom/twitter/android/UsersFragment;->a(Lcom/twitter/android/aag;)V

    iget-object v1, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v1}, Lcom/twitter/android/FollowFlowController;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, p0}, Lcom/twitter/android/UsersFragment;->a(Lcom/twitter/android/aaf;)V

    :cond_0
    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/FollowActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/FollowActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c()Z
    .locals 1

    sget-boolean v0, Lcom/twitter/android/FollowActivity;->a:Z

    return v0
.end method

.method private d(Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x1

    const-string/jumbo v0, "android_incubation_autofollow_1588"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v0, "android_incubation_autofollow_1588"

    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "autofollow"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "in_autofollow_bucket"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "follow_all_subtitle"

    const v1, 0x7f0f048b    # com.twitter.android.R.string.suggestions_for_you_autofollow_subtitle

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    const-string/jumbo v0, "should_ignore_user_view_click"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic d(Lcom/twitter/android/FollowActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/FollowActivity;->i:Z

    return v0
.end method

.method static synthetic e(Lcom/twitter/android/FollowActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/FollowActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/FollowActivity;)Lcom/twitter/android/FollowFlowController;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    return-object v0
.end method

.method private f()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v0}, Lcom/twitter/android/FollowFlowController;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->k:Lcom/twitter/android/util/d;

    invoke-interface {v0}, Lcom/twitter/android/util/d;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const-string/jumbo v0, "android_edit_profile_nux_1267"

    invoke-static {v0}, Lkk;->c(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private g()V
    .locals 7

    iget-boolean v0, p0, Lcom/twitter/android/FollowActivity;->e:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/twitter/android/FollowActivity;->h:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->j:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0}, Lcom/twitter/android/UsersFragment;->C()Z

    move-result v0

    if-nez v0, :cond_1

    const v0, 0x7f0f0224    # com.twitter.android.R.string.loading

    invoke-virtual {p0, v0}, Lcom/twitter/android/FollowActivity;->setTitle(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/FollowActivity;->h()F

    move-result v0

    const v1, 0x7f0f0226    # com.twitter.android.R.string.loading_progress

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Ljava/text/NumberFormat;->getPercentInstance()Ljava/text/NumberFormat;

    move-result-object v4

    float-to-double v5, v0

    invoke-virtual {v4, v5, v6}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/twitter/android/FollowActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/FollowActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    const-string/jumbo v1, "follow_friends"

    invoke-virtual {v0, v1}, Lcom/twitter/android/FollowFlowController;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f0f0199    # com.twitter.android.R.string.find_friends_title

    invoke-virtual {p0, v0}, Lcom/twitter/android/FollowActivity;->setTitle(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    const-string/jumbo v1, "follow_recommendations"

    invoke-virtual {v0, v1}, Lcom/twitter/android/FollowFlowController;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v0}, Lcom/twitter/android/FollowFlowController;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x7f0f0560    # com.twitter.android.R.string.who_to_follow_title

    invoke-virtual {p0, v0}, Lcom/twitter/android/FollowActivity;->setTitle(I)V

    goto :goto_0

    :cond_4
    const v0, 0x7f0f048d    # com.twitter.android.R.string.suggestions_title

    invoke-virtual {p0, v0}, Lcom/twitter/android/FollowActivity;->setTitle(I)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    const-string/jumbo v1, "follow_interest"

    invoke-virtual {v0, v1}, Lcom/twitter/android/FollowFlowController;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v0}, Lcom/twitter/android/FollowFlowController;->d()Lcom/twitter/android/util/x;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/android/util/x;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/android/FollowActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private h()F
    .locals 4

    iget v0, p0, Lcom/twitter/android/FollowActivity;->h:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/twitter/android/FollowActivity;->g:I

    int-to-float v0, v0

    iget v1, p0, Lcom/twitter/android/FollowActivity;->h:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/FollowActivity;->j:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v1}, Lcom/twitter/android/UsersFragment;->C()Z

    move-result v1

    if-nez v1, :cond_1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/FollowActivity;->j:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v1}, Lcom/twitter/android/UsersFragment;->z()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/FollowActivity;->j:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v2}, Lcom/twitter/android/UsersFragment;->D()I

    move-result v2

    int-to-float v2, v2

    int-to-float v3, v1

    div-float/2addr v2, v3

    int-to-float v1, v1

    div-float/2addr v0, v1

    add-float/2addr v0, v2

    goto :goto_1
.end method

.method private k()Z
    .locals 2

    invoke-direct {p0}, Lcom/twitter/android/FollowActivity;->h()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private l()V
    .locals 2

    const v0, 0x7f09011d    # com.twitter.android.R.id.next

    invoke-virtual {p0, v0}, Lcom/twitter/android/FollowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v1}, Lcom/twitter/android/FollowFlowController;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0f019e    # com.twitter.android.R.string.finish

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    return-void

    :cond_0
    const v1, 0x7f0f029c    # com.twitter.android.R.string.next

    goto :goto_0
.end method

.method private m()V
    .locals 5

    const/4 v4, -0x2

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300d8    # com.twitter.android.R.layout.nux_progress_indicator

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f09000b    # com.twitter.android.R.id.indicator

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v2, p0}, Lcom/twitter/android/FollowFlowController;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/twitter/android/FollowActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    new-instance v2, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;

    const/4 v3, 0x5

    invoke-direct {v2, v4, v4, v3}, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/internal/android/widget/ToolBar;->a(Landroid/view/View;Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;)V

    return-void
.end method

.method private n()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->j:Lcom/twitter/android/UsersFragment;

    iget v0, v0, Lcom/twitter/android/UsersFragment;->s:I

    sparse-switch v0, :sswitch_data_0

    const-string/jumbo v0, ""

    :goto_0
    return-object v0

    :sswitch_0
    const-string/jumbo v0, "follow_friends"

    goto :goto_0

    :sswitch_1
    const-string/jumbo v0, "people"

    goto :goto_0

    :sswitch_2
    const-string/jumbo v0, "who_to_follow"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_0
        0x11 -> :sswitch_1
        0x13 -> :sswitch_2
        0x1b -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 4

    const/4 v3, 0x0

    new-instance v1, Lcom/twitter/android/client/z;

    invoke-direct {v1, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    invoke-virtual {p0}, Lcom/twitter/android/FollowActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-nez p1, :cond_1

    const-string/jumbo v2, "flow_controller"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    :goto_0
    check-cast v0, Lcom/twitter/android/FollowFlowController;

    check-cast v0, Lcom/twitter/android/FollowFlowController;

    iput-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/FollowFlowController;

    invoke-direct {v0}, Lcom/twitter/android/FollowFlowController;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v0}, Lcom/twitter/android/FollowFlowController;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f03004a    # com.twitter.android.R.layout.digits_nux_activity

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/z;->d(I)V

    const/16 v0, 0xa

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/z;->c(I)V

    invoke-virtual {v1, v3}, Lcom/twitter/android/client/z;->a(Z)V

    :goto_1
    return-object v1

    :cond_1
    const-string/jumbo v0, "flow_controller"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {v1, v3}, Lcom/twitter/android/client/z;->a(Z)V

    invoke-virtual {v1, v3}, Lcom/twitter/android/client/z;->a(I)V

    goto :goto_1
.end method

.method public a(IIILjava/util/ArrayList;)V
    .locals 7

    const/4 v3, 0x1

    const/4 v6, 0x0

    const/4 v1, -0x1

    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_0
    if-eq p3, v1, :cond_1

    iput p3, p0, Lcom/twitter/android/FollowActivity;->h:I

    if-eq p2, v1, :cond_1

    iget v0, p0, Lcom/twitter/android/FollowActivity;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/android/FollowActivity;->g:I

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->j:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0}, Lcom/twitter/android/UsersFragment;->C()Z

    move-result v0

    if-nez v0, :cond_2

    iget v0, p0, Lcom/twitter/android/FollowActivity;->h:I

    if-eq v0, v1, :cond_3

    :cond_2
    invoke-direct {p0}, Lcom/twitter/android/FollowActivity;->k()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    iput-boolean v6, p0, Lcom/twitter/android/FollowActivity;->e:Z

    :cond_4
    invoke-direct {p0}, Lcom/twitter/android/FollowActivity;->g()V

    invoke-virtual {p0}, Lcom/twitter/android/FollowActivity;->V()V

    iget v0, p0, Lcom/twitter/android/FollowActivity;->g:I

    if-ne v0, v3, :cond_5

    invoke-virtual {p0}, Lcom/twitter/android/FollowActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/FollowActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v5}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ":follow_friends:stream::results"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_5
    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->j:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0}, Lcom/twitter/android/UsersFragment;->aq()I

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    const-string/jumbo v1, "follow_friends"

    invoke-virtual {v0, v1}, Lcom/twitter/android/FollowFlowController;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-direct {p0}, Lcom/twitter/android/FollowActivity;->k()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/twitter/android/FollowActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/FollowActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/FollowActivity;->a(Lcom/twitter/android/client/c;J)V

    :cond_6
    return-void
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 9

    const v8, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    const/4 v7, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/FollowActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-static {p0}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;)Lcom/twitter/android/util/d;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/FollowActivity;->k:Lcom/twitter/android/util/d;

    const-string/jumbo v0, "register_device"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/FollowActivity;->c:Z

    const-string/jumbo v0, "may_upload_contacts"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/FollowActivity;->d:Z

    invoke-virtual {p0}, Lcom/twitter/android/FollowActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v3

    if-nez p1, :cond_7

    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v0}, Lcom/twitter/android/FollowFlowController;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/FollowActivity;->c:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/twitter/android/FollowActivity;->a:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "digits"

    const-string/jumbo v4, "Attempting to register new device..."

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v4, "device_registration_normalized_phone_number"

    const-string/jumbo v5, ""

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v4, "device_registration_sms_text"

    const-string/jumbo v5, ""

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    new-instance v0, Lcom/twitter/android/gp;

    invoke-direct {v0, p0}, Lcom/twitter/android/gp;-><init>(Lcom/twitter/android/FollowActivity;)V

    invoke-virtual {v3, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/j;)V

    const-string/jumbo v0, "device_to_register"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    const-string/jumbo v0, "device_to_register"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v4, p0, Lcom/twitter/android/FollowActivity;->k:Lcom/twitter/android/util/d;

    invoke-interface {v4}, Lcom/twitter/android/util/d;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lcom/twitter/android/client/c;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    const-string/jumbo v4, "follow_interest"

    invoke-virtual {v0, v4}, Lcom/twitter/android/FollowFlowController;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v0}, Lcom/twitter/android/FollowFlowController;->d()Lcom/twitter/android/util/x;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/twitter/android/FollowActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    iget-object v6, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v6}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v1

    const-string/jumbo v6, "follow_interest:::impression"

    aput-object v6, v0, v7

    invoke-virtual {v3, v4, v5, v0}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_2
    :goto_1
    invoke-direct {p0, v2, v1, v1}, Lcom/twitter/android/FollowActivity;->a(Landroid/content/Intent;II)Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/FollowActivity;->c(Landroid/os/Bundle;)Lcom/twitter/android/UsersFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/FollowActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2, v8, v0}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/twitter/android/FollowActivity;->f:Ljava/util/ArrayList;

    :goto_2
    iput-object v0, p0, Lcom/twitter/android/FollowActivity;->j:Lcom/twitter/android/UsersFragment;

    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v0}, Lcom/twitter/android/FollowFlowController;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f09011b    # com.twitter.android.R.id.skip

    invoke-virtual {p0, v0}, Lcom/twitter/android/FollowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09011d    # com.twitter.android.R.id.next

    invoke-virtual {p0, v0}, Lcom/twitter/android/FollowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09011c    # com.twitter.android.R.id.digits_cta

    invoke-virtual {p0, v0}, Lcom/twitter/android/FollowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/twitter/android/FollowActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0f01a2    # com.twitter.android.R.string.follow_n

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/twitter/android/FollowActivity;->j:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v5}, Lcom/twitter/android/UsersFragment;->v()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/twitter/android/FollowActivity;->b()V

    invoke-direct {p0}, Lcom/twitter/android/FollowActivity;->l()V

    invoke-direct {p0}, Lcom/twitter/android/FollowActivity;->m()V

    :cond_3
    invoke-direct {p0}, Lcom/twitter/android/FollowActivity;->g()V

    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->k:Lcom/twitter/android/util/d;

    invoke-interface {v0}, Lcom/twitter/android/util/d;->h()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/twitter/android/FollowActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    const-string/jumbo v0, "follow_friends_next_btn_timeout_1192"

    invoke-static {p0, v4, v5, v0}, Lju;->b(Landroid/content/Context;JLjava/lang/String;)V

    const-string/jumbo v0, "follow_friends_next_btn_timeout_1192"

    invoke-static {p0, v4, v5, v0}, Lju;->a(Landroid/content/Context;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "5_second"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v0, 0x1388

    :goto_3
    if-lez v0, :cond_4

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/twitter/android/go;

    invoke-direct {v2, p0, v3}, Lcom/twitter/android/go;-><init>(Lcom/twitter/android/FollowActivity;Lcom/twitter/android/client/c;)V

    int-to-long v3, v0

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_4
    return-void

    :cond_5
    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->k:Lcom/twitter/android/util/d;

    invoke-interface {v0}, Lcom/twitter/android/util/d;->d()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v0, p0}, Lcom/twitter/android/FollowFlowController;->a(Landroid/app/Activity;)V

    invoke-virtual {p0}, Lcom/twitter/android/FollowActivity;->finish()V

    goto/16 :goto_1

    :cond_7
    const-string/jumbo v0, "contact_list"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/FollowActivity;->f:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/twitter/android/FollowActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0, p0}, Lcom/twitter/android/UsersFragment;->a(Lcom/twitter/android/aag;)V

    iget-object v2, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v2}, Lcom/twitter/android/FollowFlowController;->b()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {v0, p0}, Lcom/twitter/android/UsersFragment;->a(Lcom/twitter/android/aaf;)V

    :cond_8
    const-string/jumbo v2, "page_count"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/twitter/android/FollowActivity;->g:I

    const-string/jumbo v2, "page_total"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/twitter/android/FollowActivity;->h:I

    const-string/jumbo v2, "loading"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/android/FollowActivity;->e:Z

    goto/16 :goto_2

    :cond_9
    const-string/jumbo v2, "10_second"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    const/16 v0, 0x2710

    goto :goto_3

    :cond_a
    move v0, v1

    goto :goto_3
.end method

.method public a(Lcom/twitter/android/client/c;J)V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    iget-object v1, p0, Lcom/twitter/android/FollowActivity;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/twitter/android/FollowFlowController;->a(Ljava/util/ArrayList;)Lcom/twitter/android/FollowFlowController;

    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v0, p0}, Lcom/twitter/android/FollowFlowController;->a(Landroid/app/Activity;)V

    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v0}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v1, p2, p3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    aput-object v0, v2, v4

    invoke-direct {p0}, Lcom/twitter/android/FollowActivity;->n()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    const-string/jumbo v3, ""

    aput-object v3, v2, v6

    const-string/jumbo v3, ""

    aput-object v3, v2, v7

    const-string/jumbo v3, "followable"

    aput-object v3, v2, v8

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/FollowActivity;->j:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v2}, Lcom/twitter/android/UsersFragment;->aq()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->e(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v1, p2, p3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    aput-object v0, v2, v4

    invoke-direct {p0}, Lcom/twitter/android/FollowActivity;->n()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    const-string/jumbo v0, ""

    aput-object v0, v2, v6

    const-string/jumbo v0, ""

    aput-object v0, v2, v7

    const-string/jumbo v0, "resolved"

    aput-object v0, v2, v8

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/FollowActivity;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/FollowActivity;->j:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v2}, Lcom/twitter/android/UsersFragment;->aq()I

    move-result v2

    add-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->e(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method protected a(Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 6

    const v5, 0x7f090328    # com.twitter.android.R.id.menu_finish

    const v4, 0x7f090327    # com.twitter.android.R.id.menu_next

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lcom/twitter/internal/android/widget/ToolBar;)Z

    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v0}, Lcom/twitter/android/FollowFlowController;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return v2

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    iget-boolean v1, p0, Lcom/twitter/android/FollowActivity;->e:Z

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    iget-boolean v1, p0, Lcom/twitter/android/FollowActivity;->i:Z

    if-nez v1, :cond_2

    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v0}, Lcom/twitter/android/FollowFlowController;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, v5}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    invoke-virtual {v0, v3}, Lhn;->b(Z)Lhn;

    goto :goto_0

    :cond_1
    invoke-virtual {p1, v4}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    invoke-virtual {v0, v3}, Lhn;->b(Z)Lhn;

    goto :goto_0

    :cond_2
    iput-boolean v2, p0, Lcom/twitter/android/FollowActivity;->i:Z

    iget-object v1, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v1}, Lcom/twitter/android/FollowFlowController;->e()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lcom/twitter/android/FollowActivity;->f()Z

    move-result v1

    if-nez v1, :cond_3

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/twitter/android/FollowActivity;->e:Z

    if-eqz v0, :cond_4

    :cond_3
    invoke-virtual {p1, v4}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    invoke-virtual {v0, v2}, Lhn;->b(Z)Lhn;

    goto :goto_0

    :cond_4
    invoke-virtual {p1, v5}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    invoke-virtual {v0, v2}, Lhn;->b(Z)Lhn;

    goto :goto_0
.end method

.method protected a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z

    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v0}, Lcom/twitter/android/FollowFlowController;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f11001b    # com.twitter.android.R.menu.onboarding

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public a(Lhn;)Z
    .locals 12

    const/4 v11, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1}, Lhn;->a()I

    move-result v0

    iget-object v2, p0, Lcom/twitter/android/FollowActivity;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {p0}, Lcom/twitter/android/FollowActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v4

    const v5, 0x7f090327    # com.twitter.android.R.id.menu_next

    if-eq v0, v5, :cond_0

    const v5, 0x7f090328    # com.twitter.android.R.id.menu_finish

    if-ne v0, v5, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->j:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0}, Lcom/twitter/android/UsersFragment;->s()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v0, v2}, Lcom/twitter/android/FollowFlowController;->a(Ljava/util/ArrayList;)Lcom/twitter/android/FollowFlowController;

    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v0, p0}, Lcom/twitter/android/FollowFlowController;->a(Landroid/app/Activity;)V

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v2}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/FollowActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    new-instance v7, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v7, v5, v6}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v8, v1, [Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ":follow_friends:::followable"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-virtual {v7, v8}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v7

    iget-object v8, p0, Lcom/twitter/android/FollowActivity;->j:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v8}, Lcom/twitter/android/UsersFragment;->aq()I

    move-result v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/twitter/library/scribe/ScribeLog;->e(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    new-instance v7, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v7, v5, v6}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v1, v1, [Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v5, ":follow_friends:::resolved"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v11

    invoke-virtual {v7, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/FollowActivity;->j:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v2}, Lcom/twitter/android/UsersFragment;->aq()I

    move-result v2

    add-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->e(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return v0

    :cond_1
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhn;)Z

    move-result v0

    goto :goto_0
.end method

.method public b()V
    .locals 8

    const/16 v7, 0x8

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->j:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0}, Lcom/twitter/android/UsersFragment;->v()I

    move-result v3

    const v0, 0x7f09011c    # com.twitter.android.R.id.digits_cta

    invoke-virtual {p0, v0}, Lcom/twitter/android/FollowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f09011d    # com.twitter.android.R.id.next

    invoke-virtual {p0, v1}, Lcom/twitter/android/FollowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    const v2, 0x7f09011b    # com.twitter.android.R.id.skip

    invoke-virtual {p0, v2}, Lcom/twitter/android/FollowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    if-lez v3, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/FollowActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f01a2    # com.twitter.android.R.string.follow_n

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {v2, v6}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {v2, v7}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 6

    invoke-virtual {p0}, Lcom/twitter/android/FollowActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/FollowActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v5}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-direct {p0}, Lcom/twitter/android/FollowActivity;->n()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string/jumbo v5, ""

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string/jumbo v5, "back_button:click"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-static {p0}, Lcom/twitter/android/SignedOutActivity;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/android/MainActivity;->a:Landroid/net/Uri;

    invoke-static {p0, v0}, Lcom/twitter/android/MainActivity;->a(Landroid/app/Activity;Landroid/net/Uri;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 11

    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/FollowActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->f:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/twitter/android/FollowActivity;->j:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v2}, Lcom/twitter/android/UsersFragment;->s()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0}, Lcom/twitter/android/FollowActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v4, 0x7f09011c    # com.twitter.android.R.id.digits_cta

    if-ne v0, v4, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/FollowActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v4, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    invoke-virtual {v0, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0, v6}, Lcom/twitter/android/UsersFragment;->e(Z)V

    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v0, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v5}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-direct {p0}, Lcom/twitter/android/FollowActivity;->n()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    const-string/jumbo v5, ""

    aput-object v5, v4, v8

    const-string/jumbo v5, ""

    aput-object v5, v4, v9

    const-string/jumbo v5, "remove"

    aput-object v5, v4, v10

    invoke-virtual {v0, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v4, p0, Lcom/twitter/android/FollowActivity;->j:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v4}, Lcom/twitter/android/UsersFragment;->aq()I

    move-result v4

    iget-object v5, p0, Lcom/twitter/android/FollowActivity;->j:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v5}, Lcom/twitter/android/UsersFragment;->v()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/twitter/library/scribe/ScribeLog;->e(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_0
    :goto_0
    invoke-virtual {p0, v1, v2, v3}, Lcom/twitter/android/FollowActivity;->a(Lcom/twitter/android/client/c;J)V

    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v4, 0x7f09011b    # com.twitter.android.R.id.skip

    if-ne v0, v4, :cond_2

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v4}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v7

    invoke-direct {p0}, Lcom/twitter/android/FollowActivity;->n()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v6

    const-string/jumbo v4, ""

    aput-object v4, v0, v8

    const-string/jumbo v4, ""

    aput-object v4, v0, v9

    const-string/jumbo v4, "skip"

    aput-object v4, v0, v10

    invoke-virtual {v1, v2, v3, v0}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v4, 0x7f09011d    # com.twitter.android.R.id.next

    if-ne v0, v4, :cond_0

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v4}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v7

    invoke-direct {p0}, Lcom/twitter/android/FollowActivity;->n()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v6

    const-string/jumbo v4, ""

    aput-object v4, v0, v8

    const-string/jumbo v4, ""

    aput-object v4, v0, v9

    const-string/jumbo v4, "next"

    aput-object v4, v0, v10

    invoke-virtual {v1, v2, v3, v0}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/FollowActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/twitter/android/FollowFlowController;->e(Landroid/app/Activity;)V

    :goto_0
    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onPause()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v0, p0}, Lcom/twitter/android/FollowFlowController;->d(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onResume()V

    invoke-static {p0}, Lcom/twitter/android/FollowFlowController;->e(Landroid/app/Activity;)V

    iget-object v0, p0, Lcom/twitter/android/FollowActivity;->k:Lcom/twitter/android/util/d;

    invoke-interface {v0}, Lcom/twitter/android/util/d;->h()Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "follow_friends_next_btn_timeout_1192"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "flow_controller"

    iget-object v1, p0, Lcom/twitter/android/FollowActivity;->b:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v0, "contact_list"

    iget-object v1, p0, Lcom/twitter/android/FollowActivity;->f:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string/jumbo v0, "page_count"

    iget v1, p0, Lcom/twitter/android/FollowActivity;->g:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "page_total"

    iget v1, p0, Lcom/twitter/android/FollowActivity;->h:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "loading"

    iget-boolean v1, p0, Lcom/twitter/android/FollowActivity;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
