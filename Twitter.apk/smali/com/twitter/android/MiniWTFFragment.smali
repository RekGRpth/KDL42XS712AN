.class public Lcom/twitter/android/MiniWTFFragment;
.super Lcom/twitter/android/UsersFragment;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Lcom/twitter/android/UsersFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "follow"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v1, "type"

    const/16 v2, 0x13

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v1, "empty_desc"

    const v2, 0x7f0f014c    # com.twitter.android.R.string.empty_find_friends_and_wtf

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v1, "limit"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/twitter/android/MiniWTFFragment;->a:Z

    invoke-virtual {p0, v0}, Lcom/twitter/android/MiniWTFFragment;->setArguments(Landroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const v0, 0x7f0300cd    # com.twitter.android.R.layout.mini_wtf_fragment

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/android/MiniWTFFragment;->a(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/twitter/android/UsersFragment;->onResume()V

    invoke-virtual {p0}, Lcom/twitter/android/MiniWTFFragment;->i()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 7

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/UsersFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/MiniWTFFragment;->V()Landroid/widget/ListView;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-virtual {v0, v3}, Lcom/twitter/refresh/widget/RefreshableListView;->setRefreshListener(Lcom/twitter/refresh/widget/d;)V

    invoke-virtual {p0}, Lcom/twitter/android/MiniWTFFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f03019b    # com.twitter.android.R.layout.wtf_view_all

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1, v5}, Landroid/widget/ListView;->setFooterDividersEnabled(Z)V

    new-instance v2, Lcom/twitter/android/FollowFlowController;

    invoke-direct {v2}, Lcom/twitter/android/FollowFlowController;-><init>()V

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "follow_recommendations"

    aput-object v4, v3, v5

    const-string/jumbo v4, "nux_tag_invite"

    aput-object v4, v3, v6

    invoke-virtual {v2, v3}, Lcom/twitter/android/FollowFlowController;->a([Ljava/lang/String;)Lcom/twitter/android/FollowFlowController;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/MiniWTFFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const-class v5, Lcom/twitter/android/FollowActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v4, "flow_controller"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v0, v2, v6}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    return-void
.end method
