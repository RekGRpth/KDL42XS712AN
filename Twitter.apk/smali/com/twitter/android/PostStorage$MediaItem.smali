.class public final Lcom/twitter/android/PostStorage$MediaItem;
.super Lcom/twitter/android/pz;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Landroid/net/Uri;

.field public b:Landroid/net/Uri;

.field public c:I

.field public d:Ljava/util/ArrayList;

.field public transient e:Landroid/graphics/Bitmap;

.field public transient f:Ljava/lang/String;

.field public transient g:Ljava/lang/String;

.field public transient h:I

.field public transient i:Z

.field public transient j:Z

.field public transient k:Z

.field private m:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/android/py;

    invoke-direct {v0}, Lcom/twitter/android/py;-><init>()V

    sput-object v0, Lcom/twitter/android/PostStorage$MediaItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/twitter/android/pz;-><init>(Landroid/os/Parcel;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->d:Ljava/util/ArrayList;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->a:Landroid/net/Uri;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->b:Landroid/net/Uri;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->m:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->c:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->d:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/api/MediaEntity;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/pz;-><init>(Ljava/lang/String;I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->d:Ljava/util/ArrayList;

    iget-object v0, p1, Lcom/twitter/library/api/MediaEntity;->url:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->b:Landroid/net/Uri;

    iget-object v0, p1, Lcom/twitter/library/api/MediaEntity;->composerSourceUrl:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->b:Landroid/net/Uri;

    :goto_0
    iput-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->a:Landroid/net/Uri;

    iget-boolean v0, p1, Lcom/twitter/library/api/MediaEntity;->enhanced:Z

    iput-boolean v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->i:Z

    iget v0, p1, Lcom/twitter/library/api/MediaEntity;->effect:I

    iput v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->h:I

    iget-boolean v0, p1, Lcom/twitter/library/api/MediaEntity;->processed:Z

    iput-boolean v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->j:Z

    iget-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->d:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/twitter/library/api/MediaEntity;->b()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p1, Lcom/twitter/library/api/MediaEntity;->displayUrl:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostStorage$MediaItem;->a(Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v0, p1, Lcom/twitter/library/api/MediaEntity;->composerSourceUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/pz;-><init>(Ljava/lang/String;I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->d:Ljava/util/ArrayList;

    iput-object p2, p0, Lcom/twitter/android/PostStorage$MediaItem;->a:Landroid/net/Uri;

    iput-object p3, p0, Lcom/twitter/android/PostStorage$MediaItem;->b:Landroid/net/Uri;

    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->c:I

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->m:Ljava/lang/String;

    return-object v0
.end method

.method a(Lcom/twitter/android/PostStorage$MediaItem;)V
    .locals 3

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->e:Landroid/graphics/Bitmap;

    iget-object v1, p1, Lcom/twitter/android/PostStorage$MediaItem;->e:Landroid/graphics/Bitmap;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->e:Landroid/graphics/Bitmap;

    :cond_2
    iget-object v0, p1, Lcom/twitter/android/PostStorage$MediaItem;->a:Landroid/net/Uri;

    iget-object v1, p1, Lcom/twitter/android/PostStorage$MediaItem;->b:Landroid/net/Uri;

    iget-object v2, p0, Lcom/twitter/android/PostStorage$MediaItem;->b:Landroid/net/Uri;

    invoke-virtual {v2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/twitter/android/PostStorage$MediaItem;->b:Landroid/net/Uri;

    invoke-virtual {v2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/twitter/android/PostStorage$MediaItem;->b:Landroid/net/Uri;

    invoke-static {v2}, Lcom/twitter/library/util/n;->b(Landroid/net/Uri;)Z

    :cond_3
    iget-object v2, p0, Lcom/twitter/android/PostStorage$MediaItem;->a:Landroid/net/Uri;

    invoke-virtual {v2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->a:Landroid/net/Uri;

    invoke-static {v0}, Lcom/twitter/library/util/n;->b(Landroid/net/Uri;)Z

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/PostStorage$MediaItem;->m:Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostStorage$MediaItem;->b(Ljava/lang/String;)V

    return-void
.end method

.method public b()Lcom/twitter/library/api/MediaEntity;
    .locals 4

    new-instance v1, Lcom/twitter/library/api/MediaEntity;

    invoke-direct {v1}, Lcom/twitter/library/api/MediaEntity;-><init>()V

    const-wide/16 v2, 0x0

    iput-wide v2, v1, Lcom/twitter/library/api/MediaEntity;->id:J

    iget-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/twitter/library/api/MediaEntity;->url:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/twitter/library/api/MediaEntity;->mediaUrl:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/PostStorage$MediaItem;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/twitter/library/api/MediaEntity;->displayUrl:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/twitter/library/api/MediaEntity;->composerSourceUrl:Ljava/lang/String;

    iget v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->h:I

    iput v0, v1, Lcom/twitter/library/api/MediaEntity;->effect:I

    iget-boolean v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->i:Z

    iput-boolean v0, v1, Lcom/twitter/library/api/MediaEntity;->enhanced:Z

    iget-boolean v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->j:Z

    iput-boolean v0, v1, Lcom/twitter/library/api/MediaEntity;->processed:Z

    iget-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->e:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, v1, Lcom/twitter/library/api/MediaEntity;->height:I

    iget-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, v1, Lcom/twitter/library/api/MediaEntity;->width:I

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Lcom/twitter/library/api/MediaEntity;->a(Ljava/util/List;)V

    iget-boolean v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->k:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    :goto_0
    iput v0, v1, Lcom/twitter/library/api/MediaEntity;->type:I

    return-object v1

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method c()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->e:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->e:Landroid/graphics/Bitmap;

    :cond_0
    return-void
.end method

.method d()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/PostStorage$MediaItem;->c()V

    iget-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->b:Landroid/net/Uri;

    invoke-static {v0}, Lcom/twitter/library/util/n;->b(Landroid/net/Uri;)Z

    iget-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->a:Landroid/net/Uri;

    invoke-static {v0}, Lcom/twitter/library/util/n;->b(Landroid/net/Uri;)Z

    return-void
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/android/pz;->a(Landroid/os/Parcel;)V

    iget-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->a:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/android/PostStorage$MediaItem;->d:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    return-void
.end method
