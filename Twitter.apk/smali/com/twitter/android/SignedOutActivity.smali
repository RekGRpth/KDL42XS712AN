.class public Lcom/twitter/android/SignedOutActivity;
.super Lcom/twitter/android/TimelineActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/zf;


# static fields
.field private static a:Z

.field private static e:Z


# instance fields
.field private f:Lcom/twitter/android/SignedOutFragment;

.field private g:Lcom/twitter/android/ze;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/TimelineActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p0}, Lcom/twitter/android/SignedOutActivity;->c(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->Y()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "android_signed_out_1960"

    new-array v3, v0, [Ljava/lang/String;

    const-string/jumbo v4, "signed_out_enhancement_bucket"

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 1

    invoke-static {p0}, Lcom/twitter/android/SignedOutActivity;->c(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private static c(Landroid/content/Context;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-static {}, Lcom/twitter/library/client/App;->a()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/twitter/library/client/App;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-boolean v1, Lcom/twitter/android/SignedOutActivity;->a:Z

    if-nez v1, :cond_2

    invoke-static {p0}, Lcom/twitter/android/SignedOutActivity;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    const-string/jumbo v2, "in"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    sput-boolean v0, Lcom/twitter/android/SignedOutActivity;->e:Z

    :goto_1
    sput-boolean v0, Lcom/twitter/android/SignedOutActivity;->a:Z

    :cond_2
    sget-boolean v0, Lcom/twitter/android/SignedOutActivity;->e:Z

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    sput-boolean v1, Lcom/twitter/android/SignedOutActivity;->e:Z

    goto :goto_1
.end method

.method private static d(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    const/4 v3, 0x2

    :try_start_0
    const-string/jumbo v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-ne v2, v3, :cond_0

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v1

    if-eq v1, v3, :cond_1

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v1, v3, :cond_1

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/TimelineActivity;->a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->c(Z)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a(I)V

    return-object v0
.end method

.method protected a(Landroid/content/Intent;Lcom/twitter/library/client/e;)Lcom/twitter/android/iu;
    .locals 3

    check-cast p2, Lcom/twitter/android/ug;

    new-instance v0, Lcom/twitter/android/SignedOutFragment;

    invoke-direct {v0}, Lcom/twitter/android/SignedOutFragment;-><init>()V

    invoke-virtual {v0, p1}, Lcom/twitter/android/SignedOutFragment;->a(Landroid/content/Intent;)Lcom/twitter/android/client/BaseListFragment;

    move-result-object v1

    iget-boolean v2, p2, Lcom/twitter/android/ug;->a:Z

    invoke-virtual {v1, v2}, Lcom/twitter/android/client/BaseListFragment;->j(Z)Lcom/twitter/android/client/BaseListFragment;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/twitter/android/client/BaseListFragment;->k(Z)V

    iput-object v0, p0, Lcom/twitter/android/SignedOutActivity;->f:Lcom/twitter/android/SignedOutFragment;

    new-instance v1, Lcom/twitter/android/iu;

    invoke-direct {v1, v0}, Lcom/twitter/android/iu;-><init>(Lcom/twitter/android/client/BaseListFragment;)V

    return-object v1
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 6

    const-wide/16 v1, 0x0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/TimelineActivity;->a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V

    iget-object v0, p0, Lcom/twitter/android/SignedOutActivity;->g:Lcom/twitter/android/ze;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/ze;

    invoke-virtual {p0}, Lcom/twitter/android/SignedOutActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {v0, p0, v3, p0, v4}, Lcom/twitter/android/ze;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;Lcom/twitter/android/zf;I)V

    iput-object v0, p0, Lcom/twitter/android/SignedOutActivity;->g:Lcom/twitter/android/ze;

    iget-object v0, p0, Lcom/twitter/android/SignedOutActivity;->g:Lcom/twitter/android/ze;

    const/4 v3, 0x0

    move-wide v4, v1

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/ze;->a(JLjava/lang/String;J)V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;J)V
    .locals 0

    iput-wide p2, p0, Lcom/twitter/android/SignedOutActivity;->b:J

    iput-object p1, p0, Lcom/twitter/android/SignedOutActivity;->c:Ljava/lang/String;

    return-void
.end method

.method protected a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b(Lcom/twitter/library/api/TwitterUser;)V
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/android/SignedOutActivity;->d:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/SignedOutActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/api/TwitterUser;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/SignedOutActivity;->f:Lcom/twitter/android/SignedOutFragment;

    iget-object v1, p1, Lcom/twitter/library/api/TwitterUser;->profileDescription:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/SignedOutFragment;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public f()Lcom/twitter/android/ze;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SignedOutActivity;->g:Lcom/twitter/android/ze;

    return-object v0
.end method

.method public onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SignedOutActivity;->f:Lcom/twitter/android/SignedOutFragment;

    if-nez v0, :cond_0

    check-cast p1, Lcom/twitter/android/SignedOutFragment;

    iput-object p1, p0, Lcom/twitter/android/SignedOutActivity;->f:Lcom/twitter/android/SignedOutFragment;

    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/SignedOutActivity;->finish()V

    return-void
.end method
