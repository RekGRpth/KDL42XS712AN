.class public Lcom/twitter/android/yl;
.super Landroid/support/v4/content/AsyncTaskLoader;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:[J

.field private c:Ljava/util/List;

.field private d:J


# direct methods
.method public constructor <init>(Landroid/content/Context;[J)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/support/v4/content/AsyncTaskLoader;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/twitter/android/yl;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/twitter/android/yl;->b:[J

    return-void
.end method

.method private a([JLcom/twitter/library/provider/az;Lcom/twitter/library/client/Session;)Ljava/util/List;
    .locals 10

    const/4 v5, 0x0

    const/16 v4, 0x64

    const/4 v9, 0x1

    array-length v0, p1

    if-le v0, v4, :cond_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Lookup users through API for more than one page is not supported in TwitterUsersLoader yet. Lookup requested: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " Page limit: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/yl;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/network/aa;->a(Landroid/content/Context;)Lcom/twitter/library/network/aa;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const-string/jumbo v2, "1.1"

    aput-object v2, v1, v5

    const-string/jumbo v2, "users"

    aput-object v2, v1, v9

    const/4 v2, 0x2

    const-string/jumbo v3, "lookup"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v0, "user_id"

    invoke-static {v1, v0, p1, v5, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;[JII)I

    const-string/jumbo v0, "include_user_entities"

    invoke-static {v1, v0, v9}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const/16 v0, 0x16

    new-instance v2, Lcom/twitter/library/util/f;

    iget-object v3, p0, Lcom/twitter/android/yl;->a:Landroid/content/Context;

    iget-wide v4, p0, Lcom/twitter/android/yl;->d:J

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v0, v2}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v0

    new-instance v2, Lcom/twitter/library/network/d;

    iget-object v3, p0, Lcom/twitter/android/yl;->a:Landroid/content/Context;

    invoke-direct {v2, v3, v1}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v3, p0, Lcom/twitter/android/yl;->d:J

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/network/n;

    invoke-virtual {p3}, Lcom/twitter/library/client/Session;->h()Lcom/twitter/library/network/OAuthToken;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v1, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    iget-wide v2, p0, Lcom/twitter/android/yl;->d:J

    const/4 v4, -0x1

    const-wide/16 v5, -0x1

    const-string/jumbo v7, "-1"

    const/4 v8, 0x0

    move-object v0, p2

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    :goto_0
    return-object v1

    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 11

    iget-object v0, p0, Lcom/twitter/android/yl;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/android/yl;->d:J

    iget-object v0, p0, Lcom/twitter/android/yl;->a:Landroid/content/Context;

    iget-wide v2, p0, Lcom/twitter/android/yl;->d:J

    invoke-static {v0, v2, v3}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/android/yl;->b:[J

    invoke-virtual {v2, v0}, Lcom/twitter/library/provider/az;->a([J)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    iget-object v4, p0, Lcom/twitter/android/yl;->b:[J

    array-length v4, v4

    if-eq v0, v4, :cond_3

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v0}, Lcom/twitter/library/api/TwitterUser;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iget-object v6, p0, Lcom/twitter/android/yl;->b:[J

    array-length v7, v6

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v7, :cond_2

    aget-wide v8, v6, v0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v4, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-static {v5}, Lcom/twitter/library/util/Util;->b(Ljava/util/Collection;)[J

    move-result-object v0

    invoke-direct {p0, v0, v2, v1}, Lcom/twitter/android/yl;->a([JLcom/twitter/library/provider/az;Lcom/twitter/library/client/Session;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_3
    return-object v3
.end method

.method public a(Ljava/util/List;)V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/yl;->isReset()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/yl;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object p1, p0, Lcom/twitter/android/yl;->c:Ljava/util/List;

    invoke-super {p0, p1}, Landroid/support/v4/content/AsyncTaskLoader;->deliverResult(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public synthetic deliverResult(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/twitter/android/yl;->a(Ljava/util/List;)V

    return-void
.end method

.method public synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/yl;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public onReset()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/content/AsyncTaskLoader;->onReset()V

    invoke-virtual {p0}, Lcom/twitter/android/yl;->onStopLoading()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/yl;->c:Ljava/util/List;

    return-void
.end method

.method protected onStartLoading()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/yl;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/android/yl;->d:J

    iget-object v2, p0, Lcom/twitter/android/yl;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/yl;->forceLoad()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/yl;->c:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/twitter/android/yl;->a(Ljava/util/List;)V

    goto :goto_0
.end method

.method protected onStopLoading()V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/yl;->cancelLoad()Z

    return-void
.end method
