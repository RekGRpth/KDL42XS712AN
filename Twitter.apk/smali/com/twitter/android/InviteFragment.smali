.class public Lcom/twitter/android/InviteFragment;
.super Lcom/twitter/android/client/BaseListFragment;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Lcom/twitter/android/cy;


# instance fields
.field private a:Lcom/twitter/android/iq;

.field private b:Ljava/util/ArrayList;

.field private c:Ljava/util/HashMap;

.field private d:Lcom/twitter/android/FollowFlowController;

.field private e:Z

.field private f:Z

.field private g:Lcom/twitter/android/aag;

.field private h:Lcom/twitter/android/ir;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/client/BaseListFragment;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/InviteFragment;->e:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/InviteFragment;->f:Z

    new-instance v0, Lcom/twitter/android/is;

    invoke-direct {v0}, Lcom/twitter/android/is;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/InviteFragment;->h:Lcom/twitter/android/ir;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/InviteFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/InviteFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/InviteFragment;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/InviteFragment;->b:Ljava/util/ArrayList;

    return-object v0
.end method

.method private a(Landroid/widget/ListView;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const/high16 v0, 0x3000000

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setFastScrollAlwaysVisible(Z)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setVerticalScrollBarEnabled(Z)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/InviteFragment;I)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/InviteFragment;->c_(I)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/InviteFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/InviteFragment;->e:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/android/InviteFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/InviteFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/InviteFragment;)Lcom/twitter/android/iq;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/InviteFragment;->a:Lcom/twitter/android/iq;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/InviteFragment;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/InviteFragment;->c:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/InviteFragment;)Lcom/twitter/android/aag;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/InviteFragment;->g:Lcom/twitter/android/aag;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/InviteFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/InviteFragment;->s()V

    return-void
.end method

.method static synthetic g(Lcom/twitter/android/InviteFragment;)Lcom/twitter/android/FollowFlowController;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/InviteFragment;->d:Lcom/twitter/android/FollowFlowController;

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/InviteFragment;)Lcom/twitter/android/ir;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/InviteFragment;->h:Lcom/twitter/android/ir;

    return-object v0
.end method

.method private s()V
    .locals 2

    const/4 v1, 0x3

    iget-boolean v0, p0, Lcom/twitter/android/InviteFragment;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/InviteFragment;->a:Lcom/twitter/android/iq;

    invoke-virtual {v0}, Lcom/twitter/android/iq;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/twitter/android/InviteFragment;->a_(I)V

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/InviteFragment;->a:Lcom/twitter/android/iq;

    iget-boolean v0, p0, Lcom/twitter/android/InviteFragment;->e:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/twitter/android/iq;->a(Z)V

    return-void

    :cond_0
    invoke-virtual {p0, v1}, Lcom/twitter/android/InviteFragment;->b(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, -0x1

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/InviteFragment;->g:Lcom/twitter/android/aag;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/InviteFragment;->g:Lcom/twitter/android/aag;

    const/4 v1, 0x0

    invoke-interface {v0, v3, v2, v2, v1}, Lcom/twitter/android/aag;->a(IIILjava/util/ArrayList;)V

    :cond_1
    iput-boolean v3, p0, Lcom/twitter/android/InviteFragment;->e:Z

    invoke-direct {p0}, Lcom/twitter/android/InviteFragment;->s()V

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/InviteFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iget-object v1, p0, Lcom/twitter/android/InviteFragment;->c:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/twitter/android/InviteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {p2, v0, v1, v2, p0}, Lcom/twitter/android/cx;->a(Landroid/database/Cursor;Ljava/util/Locale;Ljava/util/HashMap;Landroid/content/Context;Lcom/twitter/android/cy;)V

    goto :goto_0
.end method

.method a(Lcom/twitter/android/aag;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/InviteFragment;->g:Lcom/twitter/android/aag;

    return-void
.end method

.method public a(Lcom/twitter/android/ir;)V
    .locals 1

    if-nez p1, :cond_0

    new-instance p1, Lcom/twitter/android/is;

    invoke-direct {p1}, Lcom/twitter/android/is;-><init>()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/InviteFragment;->a:Lcom/twitter/android/iq;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/InviteFragment;->a:Lcom/twitter/android/iq;

    invoke-virtual {v0, p1}, Lcom/twitter/android/iq;->a(Lcom/twitter/android/ir;)V

    :cond_1
    iput-object p1, p0, Lcom/twitter/android/InviteFragment;->h:Lcom/twitter/android/ir;

    return-void
.end method

.method public a([Ljava/lang/String;[Ljava/lang/String;II)V
    .locals 10

    invoke-virtual {p0}, Lcom/twitter/android/InviteFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const-wide/16 v5, -0x1

    const/4 v7, 0x1

    move-object v1, p1

    move-object v2, p2

    move v8, p3

    move v9, p4

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/android/client/c;->a([Ljava/lang/String;[Ljava/lang/String;[JIJZII)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/InviteFragment;->a(Ljava/lang/String;I)V

    return-void
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/InviteFragment;->a:Lcom/twitter/android/iq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/InviteFragment;->a:Lcom/twitter/android/iq;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/iq;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected b()V
    .locals 0

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->b()V

    invoke-direct {p0}, Lcom/twitter/android/InviteFragment;->s()V

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public e()Ljava/util/ArrayList;
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/twitter/android/InviteFragment;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterContact;

    iget-boolean v3, v0, Lcom/twitter/library/api/TwitterContact;->c:Z

    if-eqz v3, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 10

    const/4 v9, 0x1

    const/4 v8, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/InviteFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {p0}, Lcom/twitter/android/InviteFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v7

    iget-object v0, p0, Lcom/twitter/android/InviteFragment;->a:Lcom/twitter/android/iq;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/InviteFragment;->b:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    new-instance v0, Lcom/twitter/android/iq;

    invoke-virtual {p0}, Lcom/twitter/android/InviteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string/jumbo v2, "select_all_title"

    invoke-virtual {v3, v2, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    const-string/jumbo v4, "select_all_subtitle"

    invoke-virtual {v3, v4, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iget-object v4, p0, Lcom/twitter/android/InviteFragment;->b:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/twitter/android/InviteFragment;->d:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v5}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/InviteFragment;->d:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v6}, Lcom/twitter/android/FollowFlowController;->b()Z

    move-result v6

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/iq;-><init>(Landroid/content/Context;IILjava/util/ArrayList;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/twitter/android/InviteFragment;->a:Lcom/twitter/android/iq;

    iget-object v0, p0, Lcom/twitter/android/InviteFragment;->a:Lcom/twitter/android/iq;

    iget-object v1, p0, Lcom/twitter/android/InviteFragment;->h:Lcom/twitter/android/ir;

    invoke-virtual {v0, v1}, Lcom/twitter/android/iq;->a(Lcom/twitter/android/ir;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/InviteFragment;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/InviteFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/InviteFragment;->d:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v3}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    const-string/jumbo v3, "invite_contacts"

    aput-object v3, v2, v9

    const/4 v3, 0x2

    const-string/jumbo v4, "stream"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string/jumbo v4, ""

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "results"

    aput-object v4, v2, v3

    invoke-virtual {v7, v0, v1, v2}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/InviteFragment;->h:Lcom/twitter/android/ir;

    invoke-interface {v0}, Lcom/twitter/android/ir;->b()V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/InviteFragment;->a:Lcom/twitter/android/iq;

    invoke-virtual {v0}, Lcom/twitter/android/iq;->a()V

    invoke-virtual {p0}, Lcom/twitter/android/InviteFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/InviteFragment;->a:Lcom/twitter/android/iq;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {v0, v9}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    invoke-direct {p0, v0}, Lcom/twitter/android/InviteFragment;->a(Landroid/widget/ListView;)V

    new-instance v0, Lcom/twitter/android/it;

    invoke-direct {v0, p0}, Lcom/twitter/android/it;-><init>(Lcom/twitter/android/InviteFragment;)V

    iput-object v0, p0, Lcom/twitter/android/InviteFragment;->O:Lcom/twitter/library/client/j;

    invoke-direct {p0}, Lcom/twitter/android/InviteFragment;->s()V

    if-nez p1, :cond_2

    iget-boolean v0, p0, Lcom/twitter/android/InviteFragment;->e:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/InviteFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v9, v1, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_2
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    const/4 v6, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/InviteFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    if-eqz p1, :cond_1

    const-string/jumbo v0, "flow_controller"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/FollowFlowController;

    iput-object v0, p0, Lcom/twitter/android/InviteFragment;->d:Lcom/twitter/android/FollowFlowController;

    const-string/jumbo v0, "state_loading"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/InviteFragment;->e:Z

    const-string/jumbo v0, "contact_list"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/InviteFragment;->b:Ljava/util/ArrayList;

    const-string/jumbo v0, "contact_name_map"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lcom/twitter/android/InviteFragment;->c:Ljava/util/HashMap;

    const-string/jumbo v0, "select_all_without_user_input"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/InviteFragment;->f:Z

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/InviteFragment;->d:Lcom/twitter/android/FollowFlowController;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/FollowFlowController;

    invoke-direct {v0}, Lcom/twitter/android/FollowFlowController;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/InviteFragment;->d:Lcom/twitter/android/FollowFlowController;

    :cond_0
    return-void

    :cond_1
    const-string/jumbo v0, "flow_controller"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/FollowFlowController;

    iput-object v0, p0, Lcom/twitter/android/InviteFragment;->d:Lcom/twitter/android/FollowFlowController;

    const-string/jumbo v0, "load_contacts"

    invoke-virtual {v1, v0, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/InviteFragment;->e:Z

    const-string/jumbo v0, "contacts"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/InviteFragment;->b:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/twitter/android/InviteFragment;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/InviteFragment;->b:Ljava/util/ArrayList;

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/InviteFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/InviteFragment;->d:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v1}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/InviteFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v5, ":invite_contacts:::impression"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v6

    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/InviteFragment;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->ae()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/InviteFragment;->f:Z

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/InviteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/cx;->a(Landroid/content/Context;)Landroid/support/v4/content/Loader;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f03003e    # com.twitter.android.R.layout.contact_list_fragment

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/android/InviteFragment;->a(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f09010d    # com.twitter.android.R.id.contact_filter_query

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-object v1
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/InviteFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "flow_controller"

    iget-object v1, p0, Lcom/twitter/android/InviteFragment;->d:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v0, "state_loading"

    iget-boolean v1, p0, Lcom/twitter/android/InviteFragment;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "contact_list"

    iget-object v1, p0, Lcom/twitter/android/InviteFragment;->b:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string/jumbo v0, "contact_name_map"

    iget-object v1, p0, Lcom/twitter/android/InviteFragment;->c:Ljava/util/HashMap;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string/jumbo v0, "select_all_without_user_input"

    invoke-virtual {p0}, Lcom/twitter/android/InviteFragment;->r()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public q()[Lcom/twitter/library/api/TwitterContact;
    .locals 12

    const/4 v11, 0x1

    const/4 v10, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/InviteFragment;->e()Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/InviteFragment;->b:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/InviteFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/InviteFragment;->d:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v3}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/twitter/android/InviteFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    new-instance v6, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v6, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v7, v11, [Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ":invite_contacts:::invitable"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-virtual {v6, v7}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v6

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Lcom/twitter/library/scribe/ScribeLog;->e(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {v2, v6}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v1, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v4, v11, [Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, ":invite_contacts:::invited"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v10

    invoke-virtual {v1, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/twitter/library/scribe/ScribeLog;->e(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {v2, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lcom/twitter/library/api/TwitterContact;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/api/TwitterContact;

    return-object v0
.end method

.method public r()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/InviteFragment;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/InviteFragment;->a:Lcom/twitter/android/iq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/InviteFragment;->a:Lcom/twitter/android/iq;

    invoke-virtual {v0}, Lcom/twitter/android/iq;->getCount()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/InviteFragment;->a:Lcom/twitter/android/iq;

    invoke-virtual {v0}, Lcom/twitter/android/iq;->d()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
