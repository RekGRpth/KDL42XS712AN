.class Lcom/twitter/android/tb;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/SecuritySettingsActivity;


# direct methods
.method private constructor <init>(Lcom/twitter/android/SecuritySettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/SecuritySettingsActivity;Lcom/twitter/android/sq;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/tb;-><init>(Lcom/twitter/android/SecuritySettingsActivity;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;ILjava/lang/String;[I)V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-virtual {v0}, Lcom/twitter/android/SecuritySettingsActivity;->a()V

    const/16 v0, 0xc8

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/SecuritySettingsActivity;->z(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v1}, Lcom/twitter/android/SecuritySettingsActivity;->y(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "settings:login_verification:unenroll::success"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-virtual {v0}, Lcom/twitter/android/SecuritySettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v1}, Lcom/twitter/android/SecuritySettingsActivity;->c(Lcom/twitter/android/SecuritySettingsActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/library/api/account/k;->b(Landroid/content/Context;Ljava/lang/String;)Z

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    const-string/jumbo v1, "login_verification"

    invoke-virtual {v0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v0, v5}, Lcom/twitter/android/SecuritySettingsActivity;->c(Lcom/twitter/android/SecuritySettingsActivity;Z)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    const-string/jumbo v1, "unenroll"

    invoke-static {v0, v1, p2, p4}, Lcom/twitter/android/SecuritySettingsActivity;->a(Lcom/twitter/android/SecuritySettingsActivity;Ljava/lang/String;I[I)V

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    const v1, 0x7f0f0242    # com.twitter.android.R.string.login_verification_currently_unavailable

    invoke-static {v0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->a(Lcom/twitter/android/SecuritySettingsActivity;I)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/client/Session;ILjava/lang/String;[ILcom/twitter/library/api/account/LvEligibilityResponse;)V
    .locals 8

    const/16 v7, 0xb

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-static {p4}, Lcom/twitter/android/SecuritySettingsActivity;->a([I)I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v1}, Lcom/twitter/android/SecuritySettingsActivity;->q(Lcom/twitter/android/SecuritySettingsActivity;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-virtual {v1}, Lcom/twitter/android/SecuritySettingsActivity;->a()V

    :cond_0
    const/16 v1, 0xc8

    if-eq p2, v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    const-string/jumbo v2, "eligibility"

    invoke-static {v1, v2, p2, p4}, Lcom/twitter/android/SecuritySettingsActivity;->a(Lcom/twitter/android/SecuritySettingsActivity;Ljava/lang/String;I[I)V

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-virtual {v0}, Lcom/twitter/android/SecuritySettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v1}, Lcom/twitter/android/SecuritySettingsActivity;->c(Lcom/twitter/android/SecuritySettingsActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/library/api/account/k;->f(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->showDialog(I)V

    :cond_1
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->showDialog(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-virtual {v0, v7}, Lcom/twitter/android/SecuritySettingsActivity;->showDialog(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->showDialog(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p5}, Lcom/twitter/library/api/account/LvEligibilityResponse;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/SecuritySettingsActivity;->q(Lcom/twitter/android/SecuritySettingsActivity;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v0, v5}, Lcom/twitter/android/SecuritySettingsActivity;->b(Lcom/twitter/android/SecuritySettingsActivity;Z)Z

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-virtual {v0}, Lcom/twitter/android/SecuritySettingsActivity;->a()V

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/SecuritySettingsActivity;->q(Lcom/twitter/android/SecuritySettingsActivity;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    const-string/jumbo v1, "login_verification"

    invoke-virtual {v0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p5}, Lcom/twitter/library/api/account/LvEligibilityResponse;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/SecuritySettingsActivity;->s(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v1}, Lcom/twitter/android/SecuritySettingsActivity;->r(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "settings:login_verification:eligibility::success"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p5}, Lcom/twitter/library/api/account/LvEligibilityResponse;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v0, v6}, Lcom/twitter/android/SecuritySettingsActivity;->c(Lcom/twitter/android/SecuritySettingsActivity;Z)Z

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-virtual {v0, v7}, Lcom/twitter/android/SecuritySettingsActivity;->showDialog(I)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v0, v5}, Lcom/twitter/android/SecuritySettingsActivity;->c(Lcom/twitter/android/SecuritySettingsActivity;Z)Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0xe8
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public b(Lcom/twitter/library/client/Session;ILjava/lang/String;[I)V
    .locals 8

    const v7, 0x7f0f0245    # com.twitter.android.R.string.login_verification_enabled_success

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v0, v5}, Lcom/twitter/android/SecuritySettingsActivity;->b(Lcom/twitter/android/SecuritySettingsActivity;Z)Z

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-virtual {v0}, Lcom/twitter/android/SecuritySettingsActivity;->a()V

    invoke-static {p4}, Lcom/twitter/android/SecuritySettingsActivity;->a([I)I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v1}, Lcom/twitter/android/SecuritySettingsActivity;->t(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/16 v3, 0xc8

    if-ne p2, v3, :cond_0

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/SecuritySettingsActivity;->u(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "settings:login_verification:set_enabled_for::success"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/SecuritySettingsActivity;->v(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "settings:login_verification:enroll::success"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v0, v7}, Lcom/twitter/android/SecuritySettingsActivity;->a(Lcom/twitter/android/SecuritySettingsActivity;I)V

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    const-string/jumbo v1, "login_verification"

    invoke-virtual {v0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v6}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v0, v5}, Lcom/twitter/android/SecuritySettingsActivity;->c(Lcom/twitter/android/SecuritySettingsActivity;Z)Z

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    const-class v3, Lcom/twitter/android/BackupCodeActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "bc_account_name"

    iget-object v3, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v3}, Lcom/twitter/android/SecuritySettingsActivity;->c(Lcom/twitter/android/SecuritySettingsActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "show_welcome"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    const/16 v3, 0x190

    if-ne p2, v3, :cond_1

    const/16 v3, 0xf7

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/SecuritySettingsActivity;->w(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "settings:login_verification:set_enabled_for::failure"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/SecuritySettingsActivity;->x(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "settings:login_verification:enroll::success"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v0, v7}, Lcom/twitter/android/SecuritySettingsActivity;->a(Lcom/twitter/android/SecuritySettingsActivity;I)V

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    const-string/jumbo v1, "login_verification"

    invoke-virtual {v0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v6}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v0, v5}, Lcom/twitter/android/SecuritySettingsActivity;->c(Lcom/twitter/android/SecuritySettingsActivity;Z)Z

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->showDialog(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-virtual {v0}, Lcom/twitter/android/SecuritySettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v1}, Lcom/twitter/android/SecuritySettingsActivity;->c(Lcom/twitter/android/SecuritySettingsActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/library/api/account/k;->b(Landroid/content/Context;Ljava/lang/String;)Z

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    const-string/jumbo v1, "enroll"

    invoke-static {v0, v1, p2, p4}, Lcom/twitter/android/SecuritySettingsActivity;->a(Lcom/twitter/android/SecuritySettingsActivity;Ljava/lang/String;I[I)V

    iget-object v0, p0, Lcom/twitter/android/tb;->a:Lcom/twitter/android/SecuritySettingsActivity;

    const v1, 0x7f0f0242    # com.twitter.android.R.string.login_verification_currently_unavailable

    invoke-static {v0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->a(Lcom/twitter/android/SecuritySettingsActivity;I)V

    goto :goto_0
.end method
