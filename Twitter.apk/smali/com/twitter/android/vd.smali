.class public Lcom/twitter/android/vd;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/ob;


# instance fields
.field final synthetic a:Lcom/twitter/android/TimelineFragment;

.field private final b:Lcom/twitter/android/client/c;

.field private final c:Ljava/util/ArrayList;

.field private final d:Landroid/widget/ListView;

.field private final e:Lcom/twitter/internal/android/widget/DockLayout;


# direct methods
.method public constructor <init>(Lcom/twitter/android/TimelineFragment;Lcom/twitter/android/client/c;Lcom/twitter/internal/android/widget/DockLayout;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/vd;->a:Lcom/twitter/android/TimelineFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/twitter/android/vd;->b:Lcom/twitter/android/client/c;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/vd;->c:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/twitter/android/TimelineFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/vd;->d:Landroid/widget/ListView;

    iput-object p3, p0, Lcom/twitter/android/vd;->e:Lcom/twitter/internal/android/widget/DockLayout;

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;Ljava/lang/Long;Landroid/os/Bundle;)V
    .locals 7

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/twitter/android/ur;

    iget v1, v6, Lcom/twitter/android/ur;->j:I

    iget-object v0, v6, Lcom/twitter/android/ur;->l:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/twitter/android/vd;->a:Lcom/twitter/android/TimelineFragment;

    invoke-static {v2}, Lcom/twitter/android/TimelineFragment;->i(Lcom/twitter/android/TimelineFragment;)Ljava/util/HashSet;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v6, Lcom/twitter/android/ur;->l:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    move-object v1, v0

    :cond_0
    :goto_0
    if-eqz v1, :cond_2

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/vd;->a:Lcom/twitter/android/TimelineFragment;

    invoke-virtual {v3}, Lcom/twitter/android/TimelineFragment;->l()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/twitter/android/vd;->a:Lcom/twitter/android/TimelineFragment;

    invoke-static {v3}, Lcom/twitter/android/TimelineFragment;->j(Lcom/twitter/android/TimelineFragment;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x2

    iget-object v3, v6, Lcom/twitter/android/ur;->p:Ljava/lang/String;

    aput-object v3, v0, v2

    const/4 v2, 0x3

    iget-object v3, v6, Lcom/twitter/android/ur;->o:Ljava/lang/String;

    aput-object v3, v0, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "results"

    aput-object v3, v0, v2

    invoke-static {v0}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/android/vd;->a:Lcom/twitter/android/TimelineFragment;

    invoke-static {v0}, Lcom/twitter/android/TimelineFragment;->k(Lcom/twitter/android/TimelineFragment;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :cond_1
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/twitter/android/vd;->a:Lcom/twitter/android/TimelineFragment;

    invoke-static {v1}, Lcom/twitter/android/TimelineFragment;->k(Lcom/twitter/android/TimelineFragment;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/vd;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/vd;->b:Lcom/twitter/android/client/c;

    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->a:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;J)V

    iget-object v0, p0, Lcom/twitter/android/vd;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/vd;->d:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/vd;->d:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/twitter/android/vd;->d:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getCount()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/vd;->d:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/twitter/android/vd;->e:Lcom/twitter/internal/android/widget/DockLayout;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/android/vd;->a:Lcom/twitter/android/TimelineFragment;

    iget-object v3, p0, Lcom/twitter/android/vd;->e:Lcom/twitter/internal/android/widget/DockLayout;

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_1
    invoke-static {v2, v3, v0}, Lcom/twitter/android/TimelineFragment;->a(Lcom/twitter/android/TimelineFragment;Lcom/twitter/internal/android/widget/DockLayout;Z)V

    :cond_4
    return-void

    :pswitch_1
    iget v0, v6, Lcom/twitter/android/ur;->n:I

    const/16 v1, 0x11

    if-ne v0, v1, :cond_5

    iget-object v0, v6, Lcom/twitter/android/ur;->l:Ljava/lang/String;

    const-string/jumbo v1, "position"

    const/4 v2, -0x1

    invoke-virtual {p3, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/twitter/library/scribe/ScribeItem;->a(Ljava/lang/String;I)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_0

    :cond_5
    iget-object v0, v6, Lcom/twitter/android/ur;->i:Lcom/twitter/library/api/TimelineScribeContent;

    iget-object v1, v6, Lcom/twitter/android/ur;->p:Ljava/lang/String;

    const-wide/16 v2, -0x1

    iget v4, v6, Lcom/twitter/android/ur;->n:I

    iget-object v5, v6, Lcom/twitter/android/ur;->l:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/scribe/ScribeItem;->a(Lcom/twitter/library/api/TimelineScribeContent;Ljava/lang/String;JILjava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    const/4 v1, 0x0

    iput v1, v0, Lcom/twitter/library/scribe/ScribeItem;->h:I

    const-string/jumbo v1, "single"

    iput-object v1, v0, Lcom/twitter/library/scribe/ScribeItem;->v:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/vd;->a:Lcom/twitter/android/TimelineFragment;

    iget v1, v1, Lcom/twitter/android/TimelineFragment;->F:I

    const/16 v2, 0x10

    if-ne v1, v2, :cond_8

    const-string/jumbo v1, "tweet_count"

    const/4 v2, -0x1

    invoke-virtual {p3, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lcom/twitter/library/scribe/ScribeItem;->y:I

    const/4 v1, 0x0

    iput-object v1, v6, Lcom/twitter/android/ur;->p:Ljava/lang/String;

    move-object v1, v0

    goto/16 :goto_0

    :pswitch_2
    iget-object v5, v6, Lcom/twitter/android/ur;->c:Lcom/twitter/android/um;

    iget-object v0, v6, Lcom/twitter/android/ur;->i:Lcom/twitter/library/api/TimelineScribeContent;

    iget-object v1, v5, Lcom/twitter/android/um;->d:Ljava/lang/String;

    iget-wide v2, v6, Lcom/twitter/android/ur;->m:J

    iget v4, v6, Lcom/twitter/android/ur;->n:I

    iget-object v5, v5, Lcom/twitter/android/um;->f:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/scribe/ScribeItem;->a(Lcom/twitter/library/api/TimelineScribeContent;Ljava/lang/String;JILjava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    const/4 v1, 0x0

    iput v1, v0, Lcom/twitter/library/scribe/ScribeItem;->h:I

    const-string/jumbo v1, "multipage"

    iput-object v1, v0, Lcom/twitter/library/scribe/ScribeItem;->v:Ljava/lang/String;

    move-object v1, v0

    goto/16 :goto_0

    :pswitch_3
    iget-object v2, v6, Lcom/twitter/android/ur;->b:Lcom/twitter/android/un;

    iget-object v0, v6, Lcom/twitter/android/ur;->i:Lcom/twitter/library/api/TimelineScribeContent;

    iget-object v1, v6, Lcom/twitter/android/ur;->p:Ljava/lang/String;

    iget-wide v2, v2, Lcom/twitter/android/un;->d:J

    iget v4, v6, Lcom/twitter/android/ur;->n:I

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/scribe/ScribeItem;->a(Lcom/twitter/library/api/TimelineScribeContent;Ljava/lang/String;JILjava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_0

    :pswitch_4
    new-instance v1, Lcom/twitter/library/scribe/ScribeItem;

    invoke-direct {v1}, Lcom/twitter/library/scribe/ScribeItem;-><init>()V

    const-string/jumbo v2, "trend_cursor"

    const/4 v3, -0x1

    invoke-virtual {p3, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, v1, Lcom/twitter/library/scribe/ScribeItem;->h:I

    const-string/jumbo v2, "entity_type"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/twitter/library/scribe/ScribeItem;->x:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/library/api/TwitterTopic;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v0, v6, Lcom/twitter/android/ur;->d:Lcom/twitter/android/us;

    iget-object v0, v0, Lcom/twitter/android/us;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_6
    iput-object v0, v1, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    const-string/jumbo v0, "tweet_count"

    const/4 v2, -0x1

    invoke-virtual {p3, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, v1, Lcom/twitter/library/scribe/ScribeItem;->y:I

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/twitter/library/scribe/ScribeItem;->e:Ljava/lang/String;

    goto/16 :goto_0

    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_8
    move-object v1, v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V
    .locals 0

    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/vd;->a(Landroid/view/View;Ljava/lang/Long;Landroid/os/Bundle;)V

    return-void
.end method
