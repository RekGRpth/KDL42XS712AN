.class public Lcom/twitter/android/AccountSettingsActivity;
.super Lcom/twitter/android/client/BasePreferenceActivity;
.source "Twttr"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field a:Z

.field b:Ljava/lang/String;

.field c:I

.field d:Z

.field e:Landroid/preference/CheckBoxPreference;

.field f:Landroid/preference/Preference;

.field private g:Lcom/twitter/android/b;

.field private h:Landroid/content/SharedPreferences;

.field private i:Lcom/twitter/library/util/z;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BasePreferenceActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/AccountSettingsActivity;)Lcom/twitter/library/util/z;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/AccountSettingsActivity;->i:Lcom/twitter/library/util/z;

    return-object v0
.end method

.method private a(Ljava/lang/String;)V
    .locals 6

    const v0, 0x7f0f027d    # com.twitter.android.R.string.media_tagging_setting_value_all

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "settings:who_can_tag_me::from_anyone:select"

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/AccountSettingsActivity;->Q:Lcom/twitter/android/client/c;

    invoke-virtual {p0}, Lcom/twitter/android/AccountSettingsActivity;->d()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const v0, 0x7f0f027e    # com.twitter.android.R.string.media_tagging_setting_value_following

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "settings:who_can_tag_me::from_people_you_follow:select"

    goto :goto_0

    :cond_2
    const v0, 0x7f0f027f    # com.twitter.android.R.string.media_tagging_setting_value_none

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "settings:who_can_tag_me:::deselect"

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/AccountSettingsActivity;)Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/AccountSettingsActivity;->h:Landroid/content/SharedPreferences;

    return-object v0
.end method


# virtual methods
.method a(Z)V
    .locals 2

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/AccountSettingsActivity;->e:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f0f0429    # com.twitter.android.R.string.settings_sync_data_summary_on

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    :goto_0
    iget-boolean v0, p0, Lcom/twitter/android/AccountSettingsActivity;->d:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/twitter/android/AccountSettingsActivity;->b(Z)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/AccountSettingsActivity;->e:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f0f0428    # com.twitter.android.R.string.settings_sync_data_summary_off

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    goto :goto_0
.end method

.method b(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/AccountSettingsActivity;->f:Landroid/preference/Preference;

    const v1, 0x7f0f0414    # com.twitter.android.R.string.settings_notifications_on

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/AccountSettingsActivity;->f:Landroid/preference/Preference;

    const v1, 0x7f0f0413    # com.twitter.android.R.string.settings_notifications_off

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(I)V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/client/BasePreferenceActivity;->onActivityResult(IILandroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    const-string/jumbo v0, "is_last"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/twitter/android/StartActivity;->a(Landroid/app/Activity;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/AccountSettingsActivity;->finish()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BasePreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/twitter/library/platform/PushService;->c(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/AccountSettingsActivity;->d:Z

    iget-boolean v0, p0, Lcom/twitter/android/AccountSettingsActivity;->d:Z

    if-eqz v0, :cond_1

    const v0, 0x7f060003    # com.twitter.android.R.xml.account_prefs_gcm

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountSettingsActivity;->addPreferencesFromResource(I)V

    new-instance v0, Lcom/twitter/android/b;

    invoke-direct {v0, p0, v3}, Lcom/twitter/android/b;-><init>(Lcom/twitter/android/AccountSettingsActivity;Lcom/twitter/android/a;)V

    iput-object v0, p0, Lcom/twitter/android/AccountSettingsActivity;->g:Lcom/twitter/android/b;

    iget-object v0, p0, Lcom/twitter/android/AccountSettingsActivity;->g:Lcom/twitter/android/b;

    sget-object v1, Lcom/twitter/android/GCMChangeReceiver;->b:Landroid/content/IntentFilter;

    sget-object v2, Lcom/twitter/library/provider/w;->a:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/twitter/android/AccountSettingsActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/AccountSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "account_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/AccountSettingsActivity;->b:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/AccountSettingsActivity;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountSettingsActivity;->setTitle(Ljava/lang/CharSequence;)V

    new-instance v0, Lcom/twitter/library/util/z;

    invoke-virtual {p0}, Lcom/twitter/android/AccountSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/library/util/z;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/AccountSettingsActivity;->i:Lcom/twitter/library/util/z;

    const-string/jumbo v0, "notif"

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/NotificationSettingsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "account_name"

    iget-object v3, p0, Lcom/twitter/android/AccountSettingsActivity;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/twitter/android/AccountSettingsActivity;->f:Landroid/preference/Preference;

    const-string/jumbo v0, "notifications_timeline"

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/NotificationsTimelineSettingsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "account_name"

    iget-object v3, p0, Lcom/twitter/android/AccountSettingsActivity;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    const-string/jumbo v0, "sync_data"

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/twitter/android/AccountSettingsActivity;->e:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v0, "polling_interval"

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "remove_account"

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    const-string/jumbo v0, "protected"

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "discoverable_by_email"

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "discoverable_by_mobile_phone"

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/AccountSettingsActivity;->i:Lcom/twitter/library/util/z;

    invoke-virtual {v0}, Lcom/twitter/library/util/z;->c()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/AccountSettingsActivity;->i:Lcom/twitter/library/util/z;

    invoke-virtual {v0}, Lcom/twitter/library/util/z;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :goto_1
    const-string/jumbo v0, "display_sensitive_media"

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "allow_media_tagging"

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "security"

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/SecuritySettingsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "account_name"

    iget-object v3, p0, Lcom/twitter/android/AccountSettingsActivity;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    const-string/jumbo v0, "change_password"

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/ChangePasswordActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "account_name"

    iget-object v3, p0, Lcom/twitter/android/AccountSettingsActivity;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    const-string/jumbo v0, "use_dm_event_api"

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :goto_2
    const-string/jumbo v0, "enable_group_dms"

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :goto_3
    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f060002    # com.twitter.android.R.xml.account_prefs_debug

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountSettingsActivity;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "change_screen_name"

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/ChangeScreenNameActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "account_name"

    iget-object v3, p0, Lcom/twitter/android/AccountSettingsActivity;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    const-string/jumbo v0, "change_email"

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/ChangeEmailActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "account_name"

    iget-object v3, p0, Lcom/twitter/android/AccountSettingsActivity;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    :cond_0
    const-string/jumbo v0, "dms"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/AccountSettingsActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/AccountSettingsActivity;->h:Landroid/content/SharedPreferences;

    return-void

    :cond_1
    const v0, 0x7f060001    # com.twitter.android.R.xml.account_prefs

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountSettingsActivity;->addPreferencesFromResource(I)V

    goto/16 :goto_0

    :cond_2
    const-string/jumbo v0, "settings_other"

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_1

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/AccountSettingsActivity;->i:Lcom/twitter/library/util/z;

    invoke-virtual {v0}, Lcom/twitter/library/util/z;->a()V

    invoke-virtual {p0}, Lcom/twitter/android/AccountSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;)Lcom/twitter/android/util/d;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/a;

    invoke-direct {v1, p0}, Lcom/twitter/android/a;-><init>(Lcom/twitter/android/AccountSettingsActivity;)V

    invoke-interface {v0, v1}, Lcom/twitter/android/util/d;->a(Lcom/twitter/android/util/e;)V

    goto/16 :goto_1

    :cond_4
    const-string/jumbo v0, "settings_other"

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_2

    :cond_5
    const-string/jumbo v0, "settings_other"

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_3
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BasePreferenceActivity;->onDestroy()V

    iget-object v0, p0, Lcom/twitter/android/AccountSettingsActivity;->g:Lcom/twitter/android/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/AccountSettingsActivity;->g:Lcom/twitter/android/b;

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountSettingsActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "sync_data"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountSettingsActivity;->a(Z)V

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string/jumbo v3, "polling_interval"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    check-cast p1, Landroid/preference/ListPreference;

    check-cast p2, Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/twitter/library/util/Util;->a(Landroid/preference/ListPreference;Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_2
    const-string/jumbo v3, "protected"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/AccountSettingsActivity;->c()Lcom/twitter/library/client/aa;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/AccountSettingsActivity;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/library/api/UserSettings;

    move-result-object v3

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    iput-boolean v4, v3, Lcom/twitter/library/api/UserSettings;->j:Z

    iget-object v4, p0, Lcom/twitter/android/AccountSettingsActivity;->Q:Lcom/twitter/android/client/c;

    invoke-virtual {v4, v2, v3, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/UserSettings;Z)Ljava/lang/String;

    move v0, v1

    goto :goto_0

    :cond_3
    const-string/jumbo v3, "discoverable_by_email"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p0}, Lcom/twitter/android/AccountSettingsActivity;->c()Lcom/twitter/library/client/aa;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/AccountSettingsActivity;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/library/api/UserSettings;

    move-result-object v3

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    iput-boolean v4, v3, Lcom/twitter/library/api/UserSettings;->i:Z

    iget-object v4, p0, Lcom/twitter/android/AccountSettingsActivity;->Q:Lcom/twitter/android/client/c;

    invoke-virtual {v4, v2, v3, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/UserSettings;Z)Ljava/lang/String;

    move v0, v1

    goto :goto_0

    :cond_4
    const-string/jumbo v3, "discoverable_by_mobile_phone"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Lcom/twitter/android/AccountSettingsActivity;->c()Lcom/twitter/library/client/aa;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/AccountSettingsActivity;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/library/api/UserSettings;

    move-result-object v3

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    iput-boolean v4, v3, Lcom/twitter/library/api/UserSettings;->l:Z

    iget-object v4, p0, Lcom/twitter/android/AccountSettingsActivity;->Q:Lcom/twitter/android/client/c;

    invoke-virtual {v4, v2, v3, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/UserSettings;Z)Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    :cond_5
    const-string/jumbo v2, "display_sensitive_media"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {p0}, Lcom/twitter/android/AccountSettingsActivity;->c()Lcom/twitter/library/client/aa;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/AccountSettingsActivity;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/library/api/UserSettings;

    move-result-object v3

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    iput-boolean v4, v3, Lcom/twitter/library/api/UserSettings;->k:Z

    iget-object v4, p0, Lcom/twitter/android/AccountSettingsActivity;->Q:Lcom/twitter/android/client/c;

    invoke-virtual {v4, v2, v3, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/UserSettings;Z)Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    :cond_6
    const-string/jumbo v2, "use_dm_event_api"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    move-object v0, p2

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iget-object v0, p0, Lcom/twitter/android/AccountSettingsActivity;->h:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string/jumbo v0, "dm_event_api"

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-interface {v3, v0, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    if-nez v2, :cond_7

    const-string/jumbo v0, "group_dms"

    invoke-interface {v3, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v0, "enable_group_dms"

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_7
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    move v0, v1

    goto/16 :goto_0

    :cond_8
    const-string/jumbo v2, "enable_group_dms"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iget-object v0, p0, Lcom/twitter/android/AccountSettingsActivity;->h:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string/jumbo v0, "group_dms"

    invoke-interface {v3, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    if-eqz v2, :cond_9

    const-string/jumbo v0, "dm_event_api"

    invoke-interface {v3, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v0, "use_dm_event_api"

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_9
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    move v0, v1

    goto/16 :goto_0

    :cond_a
    const-string/jumbo v2, "allow_media_tagging"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/AccountSettingsActivity;->c()Lcom/twitter/library/client/aa;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/AccountSettingsActivity;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/library/api/UserSettings;

    move-result-object v3

    check-cast p2, Ljava/lang/String;

    iput-object p2, v3, Lcom/twitter/library/api/UserSettings;->n:Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/AccountSettingsActivity;->Q:Lcom/twitter/android/client/c;

    invoke-virtual {v4, v2, v3, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/UserSettings;Z)Ljava/lang/String;

    check-cast p1, Landroid/preference/ListPreference;

    invoke-static {p1, p2}, Lcom/twitter/library/util/Util;->a(Landroid/preference/ListPreference;Ljava/lang/String;)V

    invoke-direct {p0, p2}, Lcom/twitter/android/AccountSettingsActivity;->a(Ljava/lang/String;)V

    move v0, v1

    goto/16 :goto_0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "remove_account"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/RemoveAccountDialogActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "account_name"

    iget-object v3, p0, Lcom/twitter/android/AccountSettingsActivity;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/AccountSettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSearchRequested()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onStart()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/client/BasePreferenceActivity;->onStart()V

    iget-object v0, p0, Lcom/twitter/android/AccountSettingsActivity;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/twitter/android/c;

    iget-object v1, p0, Lcom/twitter/android/AccountSettingsActivity;->b:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/c;-><init>(Lcom/twitter/android/AccountSettingsActivity;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/c;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/client/BasePreferenceActivity;->onStop()V

    iget-object v0, p0, Lcom/twitter/android/AccountSettingsActivity;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/twitter/android/d;

    iget-object v1, p0, Lcom/twitter/android/AccountSettingsActivity;->b:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/d;-><init>(Lcom/twitter/android/AccountSettingsActivity;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/d;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    return-void
.end method
