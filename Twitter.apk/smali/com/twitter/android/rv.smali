.class Lcom/twitter/android/rv;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/client/c;

.field final synthetic b:Lcom/twitter/android/SearchActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/SearchActivity;Lcom/twitter/android/client/c;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/rv;->b:Lcom/twitter/android/SearchActivity;

    iput-object p2, p0, Lcom/twitter/android/rv;->a:Lcom/twitter/android/client/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, -0x1

    if-ne v2, p2, :cond_2

    iget-object v2, p0, Lcom/twitter/android/rv;->b:Lcom/twitter/android/SearchActivity;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string/jumbo v3, "location"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v2, p0, Lcom/twitter/android/rv;->a:Lcom/twitter/android/client/c;

    iget-object v3, p0, Lcom/twitter/android/rv;->b:Lcom/twitter/android/SearchActivity;

    invoke-static {v3}, Lcom/twitter/android/SearchActivity;->a(Lcom/twitter/android/SearchActivity;)Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    new-array v5, v1, [Ljava/lang/String;

    const-string/jumbo v6, "location_prompt:::allow:click"

    aput-object v6, v5, v0

    invoke-virtual {v2, v3, v4, v5}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    move v0, v1

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/twitter/android/rv;->b:Lcom/twitter/android/SearchActivity;

    iget-boolean v2, v2, Lcom/twitter/android/SearchActivity;->e:Z

    if-eq v0, v2, :cond_1

    iget-object v2, p0, Lcom/twitter/android/rv;->b:Lcom/twitter/android/SearchActivity;

    iput-boolean v0, v2, Lcom/twitter/android/SearchActivity;->e:Z

    iget-object v0, p0, Lcom/twitter/android/rv;->b:Lcom/twitter/android/SearchActivity;

    iput-boolean v1, v0, Lcom/twitter/android/SearchActivity;->f:Z

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/rv;->b:Lcom/twitter/android/SearchActivity;

    iget-object v0, v0, Lcom/twitter/android/SearchActivity;->b:Lcom/twitter/android/sb;

    iget-object v1, p0, Lcom/twitter/android/rv;->b:Lcom/twitter/android/SearchActivity;

    iget v1, v1, Lcom/twitter/android/SearchActivity;->c:I

    iget-object v2, p0, Lcom/twitter/android/rv;->b:Lcom/twitter/android/SearchActivity;

    iget-boolean v2, v2, Lcom/twitter/android/SearchActivity;->d:Z

    iget-object v3, p0, Lcom/twitter/android/rv;->b:Lcom/twitter/android/SearchActivity;

    iget-boolean v3, v3, Lcom/twitter/android/SearchActivity;->e:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/sb;->a(IZZ)V

    return-void

    :cond_2
    const/4 v2, -0x2

    if-ne v2, p2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/rv;->a:Lcom/twitter/android/client/c;

    iget-object v3, p0, Lcom/twitter/android/rv;->b:Lcom/twitter/android/SearchActivity;

    invoke-static {v3}, Lcom/twitter/android/SearchActivity;->b(Lcom/twitter/android/SearchActivity;)Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    new-array v5, v1, [Ljava/lang/String;

    const-string/jumbo v6, "location_prompt:::deny:click"

    aput-object v6, v5, v0

    invoke-virtual {v2, v3, v4, v5}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0
.end method
