.class Lcom/twitter/android/hf;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/GalleryActivity;

.field final synthetic b:I

.field final synthetic c:Lcom/twitter/android/hd;


# direct methods
.method constructor <init>(Lcom/twitter/android/hd;Lcom/twitter/android/GalleryActivity;I)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/hf;->c:Lcom/twitter/android/hd;

    iput-object p2, p0, Lcom/twitter/android/hf;->a:Lcom/twitter/android/GalleryActivity;

    iput p3, p0, Lcom/twitter/android/hf;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    invoke-static {p2}, Landroid/support/v4/view/MotionEventCompat;->getActionMasked(Landroid/view/MotionEvent;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    :goto_1
    return v0

    :pswitch_1
    iget-object v1, p0, Lcom/twitter/android/hf;->c:Lcom/twitter/android/hd;

    invoke-static {v1, v0}, Lcom/twitter/android/hd;->a(Lcom/twitter/android/hd;F)F

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/twitter/android/hf;->c:Lcom/twitter/android/hd;

    invoke-static {v1}, Lcom/twitter/android/hd;->a(Lcom/twitter/android/hd;)F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/twitter/android/hf;->b:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
