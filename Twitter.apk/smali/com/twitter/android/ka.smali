.class Lcom/twitter/android/ka;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/client/c;

.field final synthetic b:Lcom/twitter/android/MainActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/MainActivity;Lcom/twitter/android/client/c;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/ka;->b:Lcom/twitter/android/MainActivity;

    iput-object p2, p0, Lcom/twitter/android/ka;->a:Lcom/twitter/android/client/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    const/4 v5, 0x0

    const/4 v3, 0x1

    const/4 v0, -0x1

    if-ne v0, p2, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ka;->b:Lcom/twitter/android/MainActivity;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "location"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v0, p0, Lcom/twitter/android/ka;->a:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/ka;->b:Lcom/twitter/android/MainActivity;

    invoke-static {v1}, Lcom/twitter/android/MainActivity;->l(Lcom/twitter/android/MainActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "location_prompt:::allow:click"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/ka;->b:Lcom/twitter/android/MainActivity;

    invoke-static {v0}, Lcom/twitter/android/MainActivity;->m(Lcom/twitter/android/MainActivity;)Lcom/twitter/library/platform/LocationProducer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/platform/LocationProducer;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ka;->b:Lcom/twitter/android/MainActivity;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/twitter/android/MainActivity;->showDialog(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, -0x2

    if-ne v0, p2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ka;->a:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/ka;->b:Lcom/twitter/android/MainActivity;

    invoke-static {v1}, Lcom/twitter/android/MainActivity;->n(Lcom/twitter/android/MainActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "location_prompt:::deny:click"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0
.end method
