.class public abstract Lcom/twitter/android/ListFragmentActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    return-void
.end method

.method private b(Landroid/content/Intent;Lcom/twitter/library/client/e;)Lcom/twitter/android/client/BaseListFragment;
    .locals 4

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/ListFragmentActivity;->a(Landroid/content/Intent;Lcom/twitter/library/client/e;)Lcom/twitter/android/iu;

    move-result-object v0

    iget-object v1, v0, Lcom/twitter/android/iu;->a:Lcom/twitter/android/client/BaseListFragment;

    invoke-virtual {v1, p1}, Lcom/twitter/android/client/BaseListFragment;->a(Landroid/content/Intent;)Lcom/twitter/android/client/BaseListFragment;

    move-result-object v2

    iget-boolean v3, v0, Lcom/twitter/android/iu;->c:Z

    invoke-virtual {v2, v3}, Lcom/twitter/android/client/BaseListFragment;->j(Z)Lcom/twitter/android/client/BaseListFragment;

    invoke-virtual {p0}, Lcom/twitter/android/ListFragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    const v3, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    iget-object v0, v0, Lcom/twitter/android/iu;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v1, v0}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    return-object v1
.end method


# virtual methods
.method protected abstract a(Landroid/content/Intent;Lcom/twitter/library/client/e;)Lcom/twitter/android/iu;
.end method

.method protected abstract a(Landroid/content/Intent;)Ljava/lang/CharSequence;
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/ListFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/ListFragmentActivity;->a(Landroid/content/Intent;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ListFragmentActivity;->setTitle(Ljava/lang/CharSequence;)V

    if-nez p1, :cond_1

    invoke-direct {p0, v1, p2}, Lcom/twitter/android/ListFragmentActivity;->b(Landroid/content/Intent;Lcom/twitter/library/client/e;)Lcom/twitter/android/client/BaseListFragment;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/ListFragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v2, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/BaseListFragment;

    if-nez v0, :cond_0

    invoke-direct {p0, v1, p2}, Lcom/twitter/android/ListFragmentActivity;->b(Landroid/content/Intent;Lcom/twitter/library/client/e;)Lcom/twitter/android/client/BaseListFragment;

    goto :goto_0
.end method
