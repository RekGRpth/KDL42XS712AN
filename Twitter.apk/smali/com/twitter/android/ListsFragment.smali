.class public Lcom/twitter/android/ListsFragment;
.super Lcom/twitter/android/client/BaseListFragment;
.source "Twttr"


# instance fields
.field a:I

.field b:Ljava/lang/String;

.field c:Z

.field private d:Lcom/twitter/android/oc;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseListFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;IILcom/twitter/library/service/b;)V
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/android/client/BaseListFragment;->a(Landroid/content/Context;IILcom/twitter/library/service/b;)V

    invoke-virtual {p4}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    if-ne p2, v2, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ListsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0f0219    # com.twitter.android.R.string.lists_fetch_error

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method protected a(Landroid/database/Cursor;)V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ListsFragment;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/ListsFragment;->c(I)Z

    :cond_0
    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2

    const/4 v1, 0x3

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseListFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/ListsFragment;->c:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/ListsFragment;->c:Z

    invoke-virtual {p0, v1}, Lcom/twitter/android/ListsFragment;->c(I)Z

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, v1}, Lcom/twitter/android/ListsFragment;->b(I)V

    goto :goto_0
.end method

.method public a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 6

    const/4 v3, 0x1

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f090118    # com.twitter.android.R.id.create_new_item

    if-ne v0, v1, :cond_1

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/ListsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/ListCreateEditActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/ListsFragment;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/ListsFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/ListsFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "me:lists:list:new_list:create"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/widget/ListView;->getChoiceMode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/ListsFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/ListsFragment;->d:Lcom/twitter/android/oc;

    if-eqz v0, :cond_0

    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    iget-object v1, p0, Lcom/twitter/android/ListsFragment;->d:Lcom/twitter/android/oc;

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const/4 v4, 0x2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v3, v0}, Lcom/twitter/android/oc;->a(JLjava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method a(Lcom/twitter/android/oc;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/ListsFragment;->d:Lcom/twitter/android/oc;

    return-void
.end method

.method protected a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;Z)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ListsFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/ix;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/ix;->a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;)V

    return-void
.end method

.method protected a(Z)V
    .locals 2

    const/4 v1, 0x3

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->a(Z)V

    if-eqz p1, :cond_1

    invoke-virtual {p0, v1}, Lcom/twitter/android/ListsFragment;->a_(I)V

    invoke-virtual {p0}, Lcom/twitter/android/ListsFragment;->P()Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ListsFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {p0, v1}, Lcom/twitter/android/ListsFragment;->a_(I)V

    invoke-virtual {p0}, Lcom/twitter/android/ListsFragment;->p()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/ListsFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/twitter/android/ListsFragment;->c(I)Z

    goto :goto_0
.end method

.method protected c(I)Z
    .locals 4

    const/4 v0, 0x1

    invoke-virtual {p0, p1}, Lcom/twitter/android/ListsFragment;->c_(I)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v1, p0, Lcom/twitter/android/ListsFragment;->a:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    new-instance v1, Lis;

    invoke-virtual {p0}, Lcom/twitter/android/ListsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/ListsFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lis;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {p0, p1}, Lcom/twitter/android/ListsFragment;->d(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lis;->a(I)Lis;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/ListsFragment;->Q:J

    invoke-virtual {v1, v2, v3}, Lis;->a(J)Lis;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ListsFragment;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lis;->a(Ljava/lang/String;)Lis;

    move-result-object v1

    invoke-virtual {p0, v1, v0, p1}, Lcom/twitter/android/ListsFragment;->a(Lcom/twitter/library/service/b;II)Z

    :goto_1
    invoke-virtual {p0, p1}, Lcom/twitter/android/ListsFragment;->a_(I)V

    goto :goto_0

    :cond_1
    new-instance v1, Lit;

    invoke-virtual {p0}, Lcom/twitter/android/ListsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/ListsFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lit;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    iget v2, p0, Lcom/twitter/android/ListsFragment;->a:I

    invoke-virtual {v1, v2}, Lit;->a(I)Lit;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/twitter/android/ListsFragment;->d(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lit;->b(I)Lit;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ListsFragment;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lit;->a(Ljava/lang/String;)Lit;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/ListsFragment;->Q:J

    invoke-virtual {v1, v2, v3}, Lit;->a(J)Lit;

    move-result-object v1

    invoke-virtual {p0, v1, v0, p1}, Lcom/twitter/android/ListsFragment;->a(Lcom/twitter/library/service/b;II)Z

    goto :goto_1
.end method

.method d(I)I
    .locals 3

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid fetch type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v1, p0, Lcom/twitter/android/ListsFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v1}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    :pswitch_1
    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public i()V
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/android/ListsFragment;->c(I)Z

    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 11

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/ListsFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/ix;

    invoke-virtual {p0}, Lcom/twitter/android/ListsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/twitter/android/ListsFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v3

    iget v4, p0, Lcom/twitter/android/ListsFragment;->R:I

    if-nez v4, :cond_1

    move v4, v5

    :goto_0
    iget-wide v7, p0, Lcom/twitter/android/ListsFragment;->Q:J

    invoke-virtual {p0}, Lcom/twitter/android/ListsFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v9

    invoke-virtual {v9}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v9

    cmp-long v7, v7, v9

    if-nez v7, :cond_2

    :goto_1
    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/ix;-><init>(Landroid/content/Context;ILcom/twitter/android/client/c;ZZ)V

    iput-object v0, p0, Lcom/twitter/android/ListsFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/ListsFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ListsFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void

    :cond_1
    move v4, v6

    goto :goto_0

    :cond_2
    move v5, v6

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, -0x1

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/ListsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/ListsFragment;->a:I

    const-string/jumbo v1, "screen_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/ListsFragment;->b:Ljava/lang/String;

    const-string/jumbo v1, "force_restart"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/ListsFragment;->c:Z

    :goto_0
    return-void

    :cond_0
    iput v2, p0, Lcom/twitter/android/ListsFragment;->a:I

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 11

    const/4 v6, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    sget-object v0, Lcom/twitter/library/provider/ai;->a:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/twitter/android/ListsFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    iget v0, p0, Lcom/twitter/android/ListsFragment;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    new-instance v0, Lcom/twitter/android/bl;

    invoke-virtual {p0}, Lcom/twitter/android/ListsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lcom/twitter/android/iz;->a:[Ljava/lang/String;

    const-string/jumbo v4, "ev_type=7 AND list_mapping_user_id=? AND list_mapping_type IN (0,2)"

    new-array v5, v10, [Ljava/lang/String;

    iget-wide v7, p0, Lcom/twitter/android/ListsFragment;->Q:J

    invoke-static {v7, v8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v9

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/bl;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/twitter/android/bl;

    invoke-virtual {p0}, Lcom/twitter/android/ListsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lcom/twitter/android/iz;->a:[Ljava/lang/String;

    const-string/jumbo v4, "ev_type=7 AND list_mapping_user_id=? AND list_mapping_type=?"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    iget-wide v7, p0, Lcom/twitter/android/ListsFragment;->Q:J

    invoke-static {v7, v8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v9

    iget v7, p0, Lcom/twitter/android/ListsFragment;->a:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v10

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/bl;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/ListsFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onPause()V

    const/4 v0, 0x2

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/ListsFragment;->b(ILcom/twitter/library/util/ar;)V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onResume()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ListsFragment;->a(Z)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/ListsFragment;->a(ILcom/twitter/library/util/ar;)V

    return-void
.end method
