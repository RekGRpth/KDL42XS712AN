.class Lcom/twitter/android/gb;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/FilterActivity;


# direct methods
.method public constructor <init>(Lcom/twitter/android/FilterActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/gb;->a:Lcom/twitter/android/FilterActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/gb;->a:Lcom/twitter/android/FilterActivity;

    iget-object v0, v0, Lcom/twitter/android/FilterActivity;->d:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/gb;->a:Lcom/twitter/android/FilterActivity;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/twitter/android/FilterActivity;->a(Lcom/twitter/android/FilterActivity;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/gb;->a:Lcom/twitter/android/FilterActivity;

    invoke-static {v0}, Lcom/twitter/android/FilterActivity;->h(Lcom/twitter/android/FilterActivity;)Landroid/view/animation/AnimationSet;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/gb;->a:Lcom/twitter/android/FilterActivity;

    invoke-static {v1}, Lcom/twitter/android/FilterActivity;->i(Lcom/twitter/android/FilterActivity;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/gb;->a:Lcom/twitter/android/FilterActivity;

    iget-object v0, v0, Lcom/twitter/android/FilterActivity;->c:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/gb;->a:Lcom/twitter/android/FilterActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/FilterActivity;->a(Lcom/twitter/android/FilterActivity;I)V

    :cond_0
    return-void
.end method
