.class public Lcom/twitter/android/lw;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Ljava/lang/StringBuilder;Lcom/twitter/library/api/TweetEntities;)V
    .locals 5

    iget-object v0, p1, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    iget-object v1, p1, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/UrlEntity;

    iget v3, v1, Lcom/twitter/library/api/UrlEntity;->start:I

    iget v4, v0, Lcom/twitter/library/api/MediaEntity;->start:I

    if-ne v3, v4, :cond_0

    iget v3, v1, Lcom/twitter/library/api/UrlEntity;->end:I

    iget v4, v0, Lcom/twitter/library/api/MediaEntity;->end:I

    if-ne v3, v4, :cond_0

    iget-object v0, p1, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget v0, v1, Lcom/twitter/library/api/UrlEntity;->start:I

    iget v1, v1, Lcom/twitter/library/api/UrlEntity;->end:I

    invoke-virtual {p0, v0, v1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    :cond_1
    return-void
.end method
