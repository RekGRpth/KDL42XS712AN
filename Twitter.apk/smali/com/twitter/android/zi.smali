.class Lcom/twitter/android/zi;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public a:Lcom/twitter/android/widget/AutomatedEllipsisTextView;

.field final synthetic b:Lcom/twitter/android/zg;


# direct methods
.method constructor <init>(Lcom/twitter/android/zg;Landroid/view/View;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/zi;->b:Lcom/twitter/android/zg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7f0902ba    # com.twitter.android.R.id.user_presence_text

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/AutomatedEllipsisTextView;

    iput-object v0, p0, Lcom/twitter/android/zi;->a:Lcom/twitter/android/widget/AutomatedEllipsisTextView;

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 6

    const/4 v5, 0x0

    if-lez p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/zi;->a:Lcom/twitter/android/widget/AutomatedEllipsisTextView;

    iget-object v1, p0, Lcom/twitter/android/zi;->b:Lcom/twitter/android/zg;

    iget-object v1, v1, Lcom/twitter/android/zg;->a:Lcom/twitter/android/UserPresenceFragment;

    invoke-virtual {v1}, Lcom/twitter/android/UserPresenceFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0029    # com.twitter.android.R.plurals.typing_users_count

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, p1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/AutomatedEllipsisTextView;->setTextContent(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/zi;->a:Lcom/twitter/android/widget/AutomatedEllipsisTextView;

    iget-object v1, p0, Lcom/twitter/android/zi;->b:Lcom/twitter/android/zg;

    iget-object v1, v1, Lcom/twitter/android/zg;->a:Lcom/twitter/android/UserPresenceFragment;

    invoke-virtual {v1}, Lcom/twitter/android/UserPresenceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f040007    # com.twitter.android.R.anim.fade_in

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/AutomatedEllipsisTextView;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/twitter/android/zi;->a:Lcom/twitter/android/widget/AutomatedEllipsisTextView;

    invoke-virtual {v0, v5}, Lcom/twitter/android/widget/AutomatedEllipsisTextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/zi;->a:Lcom/twitter/android/widget/AutomatedEllipsisTextView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/AutomatedEllipsisTextView;->a()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/zi;->a:Lcom/twitter/android/widget/AutomatedEllipsisTextView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/AutomatedEllipsisTextView;->b()V

    iget-object v0, p0, Lcom/twitter/android/zi;->b:Lcom/twitter/android/zg;

    iget-object v1, p0, Lcom/twitter/android/zi;->a:Lcom/twitter/android/widget/AutomatedEllipsisTextView;

    invoke-static {v0, v1}, Lcom/twitter/android/zg;->a(Lcom/twitter/android/zg;Landroid/view/View;)V

    iget-object v0, p0, Lcom/twitter/android/zi;->a:Lcom/twitter/android/widget/AutomatedEllipsisTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/AutomatedEllipsisTextView;->setVisibility(I)V

    goto :goto_0
.end method
