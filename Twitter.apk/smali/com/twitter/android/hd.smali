.class Lcom/twitter/android/hd;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lcom/twitter/library/util/ar;
.implements Lcom/twitter/library/widget/a;


# instance fields
.field public final a:Landroid/view/View;

.field final synthetic b:Lcom/twitter/android/GalleryActivity;

.field private c:Lcom/twitter/library/widget/SlidingPanel;

.field private d:Landroid/widget/ListView;

.field private e:Lcom/twitter/android/aaa;

.field private f:Lcom/twitter/library/util/FriendshipCache;

.field private g:Z

.field private h:F

.field private final i:Lcom/twitter/android/hh;


# direct methods
.method public constructor <init>(Lcom/twitter/android/GalleryActivity;Lcom/twitter/library/widget/SlidingPanel;)V
    .locals 10

    const/16 v4, 0x8

    const/4 v2, 0x2

    const/4 v6, 0x0

    iput-object p1, p0, Lcom/twitter/android/hd;->b:Lcom/twitter/android/GalleryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/twitter/android/hh;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/hh;-><init>(Lcom/twitter/android/hd;Lcom/twitter/android/gt;)V

    iput-object v0, p0, Lcom/twitter/android/hd;->i:Lcom/twitter/android/hh;

    iput-object p2, p0, Lcom/twitter/android/hd;->c:Lcom/twitter/library/widget/SlidingPanel;

    iget-object v0, p0, Lcom/twitter/android/hd;->c:Lcom/twitter/library/widget/SlidingPanel;

    const v1, 0x7f090057    # com.twitter.android.R.id.sliding_panel_content

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/android/GalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b0036    # com.twitter.android.R.color.dark_transparent_black

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/twitter/android/hd;->c:Lcom/twitter/library/widget/SlidingPanel;

    const v1, 0x7f090058    # com.twitter.android.R.id.sliding_panel_top_header_divider

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/hd;->c:Lcom/twitter/library/widget/SlidingPanel;

    const v1, 0x7f090056    # com.twitter.android.R.id.sliding_panel_bottom_header_divider

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/hd;->c:Lcom/twitter/library/widget/SlidingPanel;

    const v1, 0x7f0901e2    # com.twitter.android.R.id.done

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/hd;->a:Landroid/view/View;

    iget-object v0, p0, Lcom/twitter/android/hd;->a:Landroid/view/View;

    new-instance v1, Lcom/twitter/android/he;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/he;-><init>(Lcom/twitter/android/hd;Lcom/twitter/android/GalleryActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {p1}, Lcom/twitter/android/GalleryActivity;->j(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0, v2, p0}, Lcom/twitter/android/client/c;->a(ILcom/twitter/library/util/ar;)V

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/hd;->c:Lcom/twitter/library/widget/SlidingPanel;

    new-instance v3, Lcom/twitter/android/hf;

    invoke-direct {v3, p0, p1, v0}, Lcom/twitter/android/hf;-><init>(Lcom/twitter/android/hd;Lcom/twitter/android/GalleryActivity;I)V

    invoke-virtual {v1, v3}, Lcom/twitter/library/widget/SlidingPanel;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/twitter/android/hd;->c:Lcom/twitter/library/widget/SlidingPanel;

    const v1, 0x7f0901e0    # com.twitter.android.R.id.media_tagged_user_list_view

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/twitter/android/hd;->d:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/twitter/android/hd;->d:Landroid/widget/ListView;

    new-instance v1, Lcom/twitter/android/hg;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/hg;-><init>(Lcom/twitter/android/hd;Lcom/twitter/android/GalleryActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    new-instance v0, Lcom/twitter/library/util/FriendshipCache;

    invoke-direct {v0}, Lcom/twitter/library/util/FriendshipCache;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/hd;->f:Lcom/twitter/library/util/FriendshipCache;

    new-instance v0, Lcom/twitter/android/aaa;

    const v3, 0x7f02008e    # com.twitter.android.R.drawable.btn_media_tag_follow_action

    iget-object v5, p0, Lcom/twitter/android/hd;->f:Lcom/twitter/library/util/FriendshipCache;

    move-object v1, p1

    move-object v4, p0

    move v7, v6

    move v8, v6

    move v9, v6

    invoke-direct/range {v0 .. v9}, Lcom/twitter/android/aaa;-><init>(Landroid/content/Context;IILcom/twitter/library/widget/a;Lcom/twitter/library/util/FriendshipCache;IIZZ)V

    iput-object v0, p0, Lcom/twitter/android/hd;->e:Lcom/twitter/android/aaa;

    iget-object v0, p0, Lcom/twitter/android/hd;->d:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/twitter/android/hd;->e:Lcom/twitter/android/aaa;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/hd;)F
    .locals 1

    iget v0, p0, Lcom/twitter/android/hd;->h:F

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/hd;F)F
    .locals 0

    iput p1, p0, Lcom/twitter/android/hd;->h:F

    return p1
.end method

.method static synthetic a(Lcom/twitter/android/hd;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/hd;->g:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/android/hd;)Lcom/twitter/library/widget/SlidingPanel;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/hd;->c:Lcom/twitter/library/widget/SlidingPanel;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/hd;)Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/hd;->d:Landroid/widget/ListView;

    return-object v0
.end method


# virtual methods
.method protected a(IJ)V
    .locals 1

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/hd;->f:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/library/util/FriendshipCache;->e(J)V

    iget-object v0, p0, Lcom/twitter/android/hd;->e:Lcom/twitter/android/aaa;

    invoke-virtual {v0}, Lcom/twitter/android/aaa;->notifyDataSetChanged()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/hd;->f:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/library/util/FriendshipCache;->b(J)V

    iget-object v0, p0, Lcom/twitter/android/hd;->e:Lcom/twitter/android/aaa;

    invoke-virtual {v0}, Lcom/twitter/android/aaa;->notifyDataSetChanged()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected a(JI)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/hd;->f:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/library/util/FriendshipCache;->a(JI)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/hd;->f:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/library/util/FriendshipCache;->b(JI)V

    iget-object v0, p0, Lcom/twitter/android/hd;->e:Lcom/twitter/android/aaa;

    invoke-virtual {v0}, Lcom/twitter/android/aaa;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/hd;->f:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0}, Lcom/twitter/library/util/FriendshipCache;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "friendship_cache"

    iget-object v1, p0, Lcom/twitter/android/hd;->f:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_0
    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Ljava/util/List;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/hd;->e:Lcom/twitter/android/aaa;

    invoke-virtual {v0, p2}, Lcom/twitter/android/aaa;->a(Ljava/util/List;)V

    iget-boolean v0, p0, Lcom/twitter/android/hd;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/hd;->a(Z)V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/hd;->e:Lcom/twitter/android/aaa;

    invoke-virtual {v0, p2}, Lcom/twitter/android/aaa;->a(Ljava/util/HashMap;)V

    return-void
.end method

.method protected a(Lcom/twitter/library/widget/UserView;)V
    .locals 9

    const/4 v8, 0x0

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getUserName()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/hd;->f:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getUserId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/util/FriendshipCache;->h(J)Ljava/lang/Integer;

    move-result-object v6

    iget-object v0, p0, Lcom/twitter/android/hd;->b:Lcom/twitter/android/GalleryActivity;

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getUserId()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getPromotedContent()Lcom/twitter/library/api/PromotedContent;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/hd;->b:Lcom/twitter/android/GalleryActivity;

    invoke-static {v5}, Lcom/twitter/android/GalleryActivity;->l(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v5

    if-nez v6, :cond_1

    const/4 v6, -0x1

    :goto_0
    const/4 v7, 0x0

    invoke-static/range {v0 .. v8}, Lcom/twitter/android/ProfileActivity;->a(Landroid/content/Context;JLjava/lang/String;Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/scribe/ScribeAssociation;ILcom/twitter/library/api/PromotedEvent;Z)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/hd;->b:Lcom/twitter/android/GalleryActivity;

    invoke-virtual {v1, v0, v8}, Lcom/twitter/android/GalleryActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void

    :cond_1
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    goto :goto_0
.end method

.method public a(Ljava/util/List;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "tags"

    invoke-static {p1}, Lcom/twitter/library/util/u;->b(Ljava/util/List;)[J

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    iget-object v1, p0, Lcom/twitter/android/hd;->b:Lcom/twitter/android/GalleryActivity;

    invoke-virtual {v1}, Lcom/twitter/android/GalleryActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method

.method public a(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/twitter/android/hd;->g:Z

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/twitter/android/hd;->b:Lcom/twitter/android/GalleryActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/GalleryActivity;->a(Z)V

    iget-object v0, p0, Lcom/twitter/android/hd;->e:Lcom/twitter/android/aaa;

    invoke-virtual {v0}, Lcom/twitter/android/aaa;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/hd;->b:Lcom/twitter/android/GalleryActivity;

    invoke-static {v0}, Lcom/twitter/android/GalleryActivity;->n(Lcom/twitter/android/GalleryActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/hd;->c:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/hd;->i:Lcom/twitter/android/hh;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    iget-object v0, p0, Lcom/twitter/android/hd;->c:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->f()Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/hd;->c:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->getPanelState()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/hd;->c:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->e()Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/hd;->b:Lcom/twitter/android/GalleryActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/GalleryActivity;->a(Z)V

    iget-object v0, p0, Lcom/twitter/android/hd;->c:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->c()Z

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/hd;->g:Z

    return v0
.end method

.method protected b()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/hd;->b:Lcom/twitter/android/GalleryActivity;

    invoke-static {v0}, Lcom/twitter/android/GalleryActivity;->o(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p0}, Lcom/twitter/android/client/c;->b(ILcom/twitter/library/util/ar;)V

    return-void
.end method

.method protected b(Landroid/os/Bundle;)V
    .locals 1

    const-string/jumbo v0, "friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/FriendshipCache;

    iput-object v0, p0, Lcom/twitter/android/hd;->f:Lcom/twitter/library/util/FriendshipCache;

    :cond_0
    return-void
.end method

.method public bridge synthetic onClick(Lcom/twitter/library/widget/BaseUserView;JI)V
    .locals 0

    check-cast p1, Lcom/twitter/library/widget/UserView;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/twitter/android/hd;->onClick(Lcom/twitter/library/widget/UserView;JI)V

    return-void
.end method

.method public onClick(Lcom/twitter/library/widget/UserView;JI)V
    .locals 10

    const/4 v9, 0x0

    const v0, 0x7f09002c    # com.twitter.android.R.id.action_button

    if-ne p4, v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getPromotedContent()Lcom/twitter/library/api/PromotedContent;

    move-result-object v7

    iget-object v0, p0, Lcom/twitter/android/hd;->b:Lcom/twitter/android/GalleryActivity;

    invoke-static {v0}, Lcom/twitter/android/GalleryActivity;->m(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    iget-object v0, p1, Lcom/twitter/library/widget/UserView;->m:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0}, Lcom/twitter/library/widget/ActionButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v8, p0, Lcom/twitter/android/hd;->b:Lcom/twitter/android/GalleryActivity;

    new-instance v0, Liq;

    iget-object v1, p0, Lcom/twitter/android/hd;->b:Lcom/twitter/android/GalleryActivity;

    iget-object v2, p0, Lcom/twitter/android/hd;->b:Lcom/twitter/android/GalleryActivity;

    invoke-static {v2}, Lcom/twitter/android/GalleryActivity;->m(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/library/client/Session;

    move-result-object v2

    move-wide v5, p2

    invoke-direct/range {v0 .. v6}, Liq;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJ)V

    invoke-virtual {v0, v7}, Liq;->a(Lcom/twitter/library/api/PromotedContent;)Liq;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v8, v0, v1, v9}, Lcom/twitter/android/GalleryActivity;->a(Lcom/twitter/android/GalleryActivity;Lcom/twitter/library/service/b;II)Z

    iget-object v0, p0, Lcom/twitter/android/hd;->f:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/library/util/FriendshipCache;->e(J)V

    const/16 v1, 0x8

    :goto_0
    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getTag()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/twitter/android/zx;

    iget-object v0, p0, Lcom/twitter/android/hd;->b:Lcom/twitter/android/GalleryActivity;

    iget-object v5, v2, Lcom/twitter/android/zx;->f:Ljava/lang/String;

    move-wide v2, p2

    move-object v4, v7

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/GalleryActivity;->a(Lcom/twitter/android/GalleryActivity;IJLcom/twitter/library/api/PromotedContent;Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    iget-object v8, p0, Lcom/twitter/android/hd;->b:Lcom/twitter/android/GalleryActivity;

    new-instance v0, Lip;

    iget-object v1, p0, Lcom/twitter/android/hd;->b:Lcom/twitter/android/GalleryActivity;

    iget-object v2, p0, Lcom/twitter/android/hd;->b:Lcom/twitter/android/GalleryActivity;

    invoke-static {v2}, Lcom/twitter/android/GalleryActivity;->m(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/library/client/Session;

    move-result-object v2

    move-wide v5, p2

    invoke-direct/range {v0 .. v6}, Lip;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJ)V

    invoke-virtual {v0, v7}, Lip;->a(Lcom/twitter/library/api/PromotedContent;)Lip;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v8, v0, v1, v9}, Lcom/twitter/android/GalleryActivity;->b(Lcom/twitter/android/GalleryActivity;Lcom/twitter/library/service/b;II)Z

    iget-object v0, p0, Lcom/twitter/android/hd;->f:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/library/util/FriendshipCache;->b(J)V

    const/4 v1, 0x7

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 3

    const-string/jumbo v0, "tags"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, [J

    check-cast v0, [J

    new-instance v1, Lcom/twitter/android/yl;

    iget-object v2, p0, Lcom/twitter/android/hd;->b:Lcom/twitter/android/GalleryActivity;

    invoke-direct {v1, v2, v0}, Lcom/twitter/android/yl;-><init>(Landroid/content/Context;[J)V

    return-object v1
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Ljava/util/List;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/hd;->a(Landroid/support/v4/content/Loader;Ljava/util/List;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/hd;->e:Lcom/twitter/android/aaa;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/aaa;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    return-void
.end method
