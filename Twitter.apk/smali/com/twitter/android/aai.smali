.class Lcom/twitter/android/aai;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/UsersFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/UsersFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method

.method private a(IIILjava/util/ArrayList;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget-object v0, v0, Lcom/twitter/android/UsersFragment;->l:Lcom/twitter/android/aag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v0}, Lcom/twitter/android/UsersFragment;->c(Lcom/twitter/android/UsersFragment;)Landroid/support/v4/widget/CursorAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v0}, Lcom/twitter/android/UsersFragment;->d(Lcom/twitter/android/UsersFragment;)Landroid/support/v4/widget/CursorAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget-object v0, v0, Lcom/twitter/android/UsersFragment;->k:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v1}, Lcom/twitter/android/UsersFragment;->e(Lcom/twitter/android/UsersFragment;)Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/client/c;->F()Ljava/util/regex/Pattern;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v2}, Lcom/twitter/android/UsersFragment;->f(Lcom/twitter/android/UsersFragment;)Z

    move-result v2

    invoke-static {p4, v0, v1, v2}, Lcom/twitter/android/cx;->a(Ljava/util/List;Ljava/util/Map;Ljava/util/regex/Pattern;Z)Ljava/util/ArrayList;

    move-result-object v0

    if-lez p1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget-object v1, v1, Lcom/twitter/android/UsersFragment;->l:Lcom/twitter/android/aag;

    iget-object v2, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v2}, Lcom/twitter/android/UsersFragment;->g(Lcom/twitter/android/UsersFragment;)Landroid/support/v4/widget/CursorAdapter;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/widget/CursorAdapter;->getCount()I

    move-result v2

    invoke-interface {v1, v2, p2, p3, v0}, Lcom/twitter/android/aag;->a(IIILjava/util/ArrayList;)V

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget-object v1, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v1}, Lcom/twitter/android/UsersFragment;->h(Lcom/twitter/android/UsersFragment;)Landroid/support/v4/widget/CursorAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/widget/CursorAdapter;->getCount()I

    move-result v1

    invoke-static {v0, v1}, Lcom/twitter/android/UsersFragment;->a(Lcom/twitter/android/UsersFragment;I)I

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v0}, Lcom/twitter/android/UsersFragment;->i(Lcom/twitter/android/UsersFragment;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget-object v1, v1, Lcom/twitter/android/UsersFragment;->l:Lcom/twitter/android/aag;

    const/4 v2, 0x0

    invoke-interface {v1, v2, p2, p3, v0}, Lcom/twitter/android/aag;->a(IIILjava/util/ArrayList;)V

    goto :goto_1
.end method

.method private a(I)Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v1}, Lcom/twitter/android/UsersFragment;->a(Lcom/twitter/android/UsersFragment;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v1}, Lcom/twitter/android/UsersFragment;->b(Lcom/twitter/android/UsersFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xc8

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v1, v0}, Lcom/twitter/android/UsersFragment;->a(Lcom/twitter/android/UsersFragment;Z)Z

    iget-object v1, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v1}, Lcom/twitter/android/UsersFragment;->p()V

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;IIILjava/util/ArrayList;[Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 4

    invoke-direct {p0, p3}, Lcom/twitter/android/aai;->a(I)Z

    move-result v1

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/UsersFragment;->f(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0}, Lcom/twitter/android/UsersFragment;->y()V

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0}, Lcom/twitter/android/UsersFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f090268    # com.twitter.android.R.id.select_all_checkbox

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iget-object v3, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v3}, Lcom/twitter/android/UsersFragment;->o(Lcom/twitter/android/UsersFragment;)Lcom/twitter/android/FollowFlowController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/android/FollowFlowController;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v0}, Lcom/twitter/android/UsersFragment;->j(Lcom/twitter/android/UsersFragment;)V

    :cond_0
    invoke-direct {p0, p5, p6, p7, p8}, Lcom/twitter/android/aai;->a(IIILjava/util/ArrayList;)V

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v0}, Lcom/twitter/android/UsersFragment;->p(Lcom/twitter/android/UsersFragment;)I

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v0}, Lcom/twitter/android/UsersFragment;->q(Lcom/twitter/android/UsersFragment;)I

    move-result v0

    if-ne v0, p7, :cond_1

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v0}, Lcom/twitter/android/UsersFragment;->r(Lcom/twitter/android/UsersFragment;)I

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget v2, v2, Lcom/twitter/android/client/PendingRequest;->b:I

    invoke-virtual {v0, v2, p5, v1}, Lcom/twitter/android/UsersFragment;->a(IIZ)V

    if-eqz p9, :cond_2

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget-object v0, v0, Lcom/twitter/android/UsersFragment;->k:Ljava/util/HashMap;

    if-eqz v0, :cond_2

    array-length v1, p9

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_2

    aget-object v2, p9, v0

    iget-object v3, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget-object v3, v3, Lcom/twitter/android/UsersFragment;->k:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v0}, Lcom/twitter/android/UsersFragment;->s(Lcom/twitter/android/UsersFragment;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0}, Lcom/twitter/android/UsersFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x3

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_3
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;I[J)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, -0x1

    invoke-direct {p0, p3}, Lcom/twitter/android/aai;->a(I)Z

    move-result v1

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/UsersFragment;->g(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    if-eqz v0, :cond_2

    if-lez p5, :cond_0

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/twitter/android/UsersFragment;->b(Lcom/twitter/android/UsersFragment;Z)Z

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0}, Lcom/twitter/android/UsersFragment;->y()V

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0}, Lcom/twitter/android/UsersFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v2, 0x7f090268    # com.twitter.android.R.id.select_all_checkbox

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v2}, Lcom/twitter/android/UsersFragment;->o(Lcom/twitter/android/UsersFragment;)Lcom/twitter/android/FollowFlowController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/FollowFlowController;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v0}, Lcom/twitter/android/UsersFragment;->j(Lcom/twitter/android/UsersFragment;)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v0}, Lcom/twitter/android/UsersFragment;->r(Lcom/twitter/android/UsersFragment;)I

    invoke-direct {p0, p5, v3, v3, v5}, Lcom/twitter/android/aai;->a(IIILjava/util/ArrayList;)V

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v0}, Lcom/twitter/android/UsersFragment;->s(Lcom/twitter/android/UsersFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0}, Lcom/twitter/android/UsersFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0, v4, v5, v2}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0, v4, p5, v1}, Lcom/twitter/android/UsersFragment;->a(IIZ)V

    :cond_2
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JII)V
    .locals 6

    const/16 v5, 0xc8

    const/4 v4, -0x1

    invoke-direct {p0, p3}, Lcom/twitter/android/aai;->a(I)Z

    move-result v1

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/UsersFragment;->b(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v2

    if-eqz v2, :cond_1

    if-ne p3, v5, :cond_0

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0}, Lcom/twitter/android/UsersFragment;->y()V

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0}, Lcom/twitter/android/UsersFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f090268    # com.twitter.android.R.id.select_all_checkbox

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v0}, Lcom/twitter/android/UsersFragment;->j(Lcom/twitter/android/UsersFragment;)V

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p8, v4, v4, v0}, Lcom/twitter/android/aai;->a(IIILjava/util/ArrayList;)V

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget v2, v2, Lcom/twitter/android/client/PendingRequest;->b:I

    invoke-virtual {v0, v2, p8, v1}, Lcom/twitter/android/UsersFragment;->a(IIZ)V

    if-eq p3, v5, :cond_1

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0f0531    # com.twitter.android.R.string.users_fetch_error

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JIILjava/util/ArrayList;)V
    .locals 6

    const/16 v5, 0xc8

    const/4 v4, 0x1

    const/4 v3, -0x1

    invoke-direct {p0, p3}, Lcom/twitter/android/aai;->a(I)Z

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v1, p2}, Lcom/twitter/android/UsersFragment;->a(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v1

    if-eqz v1, :cond_1

    if-ne p3, v5, :cond_0

    iget-object v2, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v2}, Lcom/twitter/android/UsersFragment;->y()V

    :cond_0
    const/4 v2, 0x0

    invoke-direct {p0, p8, v3, v3, v2}, Lcom/twitter/android/aai;->a(IIILjava/util/ArrayList;)V

    iget-object v2, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget v1, v1, Lcom/twitter/android/client/PendingRequest;->b:I

    invoke-virtual {v2, v1, p8, v0}, Lcom/twitter/android/UsersFragment;->a(IIZ)V

    const/16 v0, 0x191

    if-ne p3, v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0f0339    # com.twitter.android.R.string.protected_profile

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    if-eq p3, v5, :cond_3

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0f0531    # com.twitter.android.R.string.users_fetch_error

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget-object v0, v0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    if-eqz v0, :cond_1

    invoke-virtual {p9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    iget-object v2, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget-object v2, v2, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0}, Lcom/twitter/library/api/TwitterUser;->a()J

    move-result-wide v3

    iget v0, v0, Lcom/twitter/library/api/TwitterUser;->friendship:I

    invoke-virtual {v2, v3, v4, v0}, Lcom/twitter/library/util/FriendshipCache;->b(JI)V

    goto :goto_1
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JIJJLcom/twitter/library/api/TwitterUser;)V
    .locals 4

    invoke-direct {p0, p3}, Lcom/twitter/android/aai;->a(I)Z

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/UsersFragment;->r(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    if-eqz v0, :cond_2

    if-eqz p12, :cond_3

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v1}, Lcom/twitter/android/UsersFragment;->y()V

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v1}, Lcom/twitter/android/UsersFragment;->u(Lcom/twitter/android/UsersFragment;)Lcom/twitter/android/aaa;

    move-result-object v1

    invoke-virtual {v1, p10, p11}, Lcom/twitter/android/aaa;->b(J)Ljava/lang/Long;

    move-result-object v2

    if-eqz v2, :cond_2

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0}, Lcom/twitter/android/UsersFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/twitter/android/zw;->a(Landroid/widget/ListView;J)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    invoke-virtual {v1, p10, p11}, Lcom/twitter/android/aaa;->c(J)Ljava/lang/Long;

    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JJII[Lcom/twitter/library/api/TwitterUser;)V
    .locals 11

    invoke-direct {p0, p3}, Lcom/twitter/android/aai;->a(I)Z

    move-result v9

    iget-object v3, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v3, p2}, Lcom/twitter/android/UsersFragment;->d(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v10

    if-nez v10, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v3, 0xc8

    if-ne p3, v3, :cond_7

    const-wide/16 v3, 0x0

    cmp-long v3, p5, v3

    if-lez v3, :cond_5

    if-lez p10, :cond_5

    iget-object v3, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v3}, Lcom/twitter/android/UsersFragment;->k(Lcom/twitter/android/UsersFragment;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget-object v3, v3, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    move-wide/from16 v0, p5

    invoke-virtual {v3, v0, v1}, Lcom/twitter/library/util/FriendshipCache;->i(J)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v3}, Lcom/twitter/android/UsersFragment;->l(Lcom/twitter/android/UsersFragment;)Lcom/twitter/android/ClusterFollowAdapterData;

    move-result-object v3

    move-wide/from16 v0, p7

    invoke-virtual {v3, v0, v1}, Lcom/twitter/android/ClusterFollowAdapterData;->a(J)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x0

    aget-object v8, p11, v4

    move-wide/from16 v4, p7

    move-wide/from16 v6, p5

    invoke-virtual/range {v3 .. v8}, Lcom/twitter/android/ClusterFollowAdapterData;->a(JJLcom/twitter/library/api/TwitterUser;)V

    :goto_1
    iget-object v3, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v3}, Lcom/twitter/android/UsersFragment;->m(Lcom/twitter/android/UsersFragment;)Landroid/support/v4/widget/CursorAdapter;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetChanged()V

    :goto_2
    iget-object v3, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v3}, Lcom/twitter/android/UsersFragment;->n(Lcom/twitter/android/UsersFragment;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget v3, v3, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v4, 0x16

    if-eq v3, v4, :cond_2

    iget-object v3, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget v3, v3, Lcom/twitter/android/UsersFragment;->s:I

    const/16 v4, 0x15

    if-ne v3, v4, :cond_3

    :cond_2
    iget-object v3, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    const-string/jumbo v4, "no_results"

    invoke-static {v3, v4}, Lcom/twitter/android/UsersFragment;->e(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)V

    :cond_3
    :goto_3
    const/4 v3, -0x1

    const/4 v4, -0x1

    const/4 v5, 0x0

    move/from16 v0, p10

    invoke-direct {p0, v0, v3, v4, v5}, Lcom/twitter/android/aai;->a(IIILjava/util/ArrayList;)V

    iget-object v3, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget v4, v10, Lcom/twitter/android/client/PendingRequest;->b:I

    move/from16 v0, p10

    invoke-virtual {v3, v4, v0, v9}, Lcom/twitter/android/UsersFragment;->a(IIZ)V

    goto :goto_0

    :cond_4
    move-wide/from16 v0, p7

    move-object/from16 v2, p11

    invoke-virtual {v3, v0, v1, v2}, Lcom/twitter/android/ClusterFollowAdapterData;->a(J[Lcom/twitter/library/api/TwitterUser;)V

    goto :goto_1

    :cond_5
    iget v3, v10, Lcom/twitter/android/client/PendingRequest;->b:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_6

    iget-object v3, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v3}, Lcom/twitter/android/UsersFragment;->k(Lcom/twitter/android/UsersFragment;)Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v3}, Lcom/twitter/android/UsersFragment;->l(Lcom/twitter/android/UsersFragment;)Lcom/twitter/android/ClusterFollowAdapterData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/android/ClusterFollowAdapterData;->a()V

    :cond_6
    iget-object v3, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v3}, Lcom/twitter/android/UsersFragment;->y()V

    goto :goto_2

    :cond_7
    iget-object v3, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v3}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v4, 0x7f0f0531    # com.twitter.android.R.string.users_fetch_error

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_3
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;Lcom/twitter/library/api/TwitterUser;)V
    .locals 5

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/twitter/library/platform/PushService;->f(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    invoke-direct {p0, p3}, Lcom/twitter/android/aai;->a(I)Z

    iget-object v4, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v4, p2}, Lcom/twitter/android/UsersFragment;->q(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v4

    if-eqz v4, :cond_0

    const/16 v4, 0xc8

    if-ne p3, v4, :cond_1

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v4, 0x3e9

    if-eq p3, v4, :cond_2

    if-nez v3, :cond_3

    :cond_2
    invoke-static {v0}, Lcom/twitter/android/client/aw;->a(Landroid/content/Context;)Lcom/twitter/android/client/aw;

    move-result-object v4

    if-nez v3, :cond_4

    move v0, v1

    :goto_1
    invoke-virtual {v4, v2, v0}, Lcom/twitter/android/client/aw;->a(Ljava/lang/String;Z)V

    :cond_3
    invoke-virtual {p5}, Lcom/twitter/library/api/TwitterUser;->a()J

    move-result-wide v2

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget-object v0, v0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/util/FriendshipCache;->i(J)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p5, Lcom/twitter/library/api/TwitterUser;->friendship:I

    invoke-static {v0}, Lcom/twitter/library/provider/ay;->h(I)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget-object v0, v0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/util/FriendshipCache;->c(J)V

    :goto_2
    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v0}, Lcom/twitter/android/UsersFragment;->F(Lcom/twitter/android/UsersFragment;)Landroid/support/v4/widget/CursorAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v2, 0x7f0f00ee    # com.twitter.android.R.string.default_error_message

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget-object v0, v0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/util/FriendshipCache;->f(J)V

    goto :goto_2
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;[J)V
    .locals 4

    invoke-direct {p0, p3}, Lcom/twitter/android/aai;->a(I)Z

    move-result v1

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/UsersFragment;->h(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v2

    if-eqz v2, :cond_0

    if-nez p5, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget v2, v2, Lcom/twitter/android/client/PendingRequest;->b:I

    invoke-virtual {v3, v2, v0, v1}, Lcom/twitter/android/UsersFragment;->a(IIZ)V

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_2

    const/16 v0, 0x12

    iget-object v1, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget v1, v1, Lcom/twitter/android/UsersFragment;->s:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0}, Lcom/twitter/android/UsersFragment;->y()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    array-length v0, p5

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0f01e8    # com.twitter.android.R.string.incoming_friendships_error

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;I[ILjava/lang/String;J)V
    .locals 13

    move/from16 v0, p3

    invoke-direct {p0, v0}, Lcom/twitter/android/aai;->a(I)Z

    iget-object v2, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v2, p2}, Lcom/twitter/android/UsersFragment;->k(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v2

    if-eqz v2, :cond_0

    const/16 v2, 0xc8

    move/from16 v0, p3

    if-eq v0, v2, :cond_1

    iget-object v2, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget-object v2, v2, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    move-wide/from16 v0, p6

    invoke-virtual {v2, v0, v1}, Lcom/twitter/library/util/FriendshipCache;->e(J)V

    iget-object v2, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v2}, Lcom/twitter/android/UsersFragment;->t(Lcom/twitter/android/UsersFragment;)Landroid/support/v4/widget/CursorAdapter;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetChanged()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v2}, Lcom/twitter/android/UsersFragment;->r()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    move-wide/from16 v0, p6

    invoke-virtual {v2, v0, v1}, Lcom/twitter/android/UsersFragment;->b(J)Ljava/lang/Long;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget-boolean v3, v3, Lcom/twitter/android/UsersFragment;->a:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v3}, Lcom/twitter/android/UsersFragment;->u(Lcom/twitter/android/UsersFragment;)Lcom/twitter/android/aaa;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-wide/from16 v0, p6

    invoke-virtual {v3, v0, v1, v4, v5}, Lcom/twitter/android/aaa;->a(JJ)V

    :cond_2
    iget-object v12, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget-object v2, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v2}, Lcom/twitter/android/UsersFragment;->w(Lcom/twitter/android/UsersFragment;)Lcom/twitter/android/client/c;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v3}, Lcom/twitter/android/UsersFragment;->v(Lcom/twitter/android/UsersFragment;)J

    move-result-wide v4

    iget-object v3, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget v6, v3, Lcom/twitter/android/UsersFragment;->s:I

    iget-object v3, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget-wide v7, v3, Lcom/twitter/android/UsersFragment;->e:J

    iget-object v3, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget-object v11, v3, Lcom/twitter/android/UsersFragment;->f:Ljava/lang/Integer;

    move-object v3, p1

    move-wide/from16 v9, p6

    invoke-virtual/range {v2 .. v11}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;JIJJLjava/lang/Integer;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v12, v2}, Lcom/twitter/android/UsersFragment;->l(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;I[ILjava/lang/String;[J)V
    .locals 7

    const/4 v4, 0x0

    invoke-direct {p0, p3}, Lcom/twitter/android/aai;->a(I)Z

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/UsersFragment;->m(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v0}, Lcom/twitter/android/UsersFragment;->x(Lcom/twitter/android/UsersFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v0}, Lcom/twitter/android/UsersFragment;->y(Lcom/twitter/android/UsersFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/UsersFragment;->n(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)Ljava/lang/String;

    iget-object v6, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v0}, Lcom/twitter/android/UsersFragment;->B(Lcom/twitter/android/UsersFragment;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v1}, Lcom/twitter/android/UsersFragment;->z(Lcom/twitter/android/UsersFragment;)J

    move-result-wide v1

    iget-object v3, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v3}, Lcom/twitter/android/UsersFragment;->A(Lcom/twitter/android/UsersFragment;)Ljava/lang/String;

    move-result-object v3

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/client/c;->a(JLjava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lcom/twitter/android/UsersFragment;->o(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;IJJ)V
    .locals 3

    invoke-direct {p0, p3}, Lcom/twitter/android/aai;->a(I)Z

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/UsersFragment;->c(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0}, Lcom/twitter/android/UsersFragment;->y()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0f053b    # com.twitter.android.R.string.users_remove_list_member_error

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public c(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;I)V
    .locals 4

    invoke-direct {p0, p3}, Lcom/twitter/android/aai;->a(I)Z

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v1, p2}, Lcom/twitter/android/UsersFragment;->i(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/twitter/android/UsersFragment;->w:Z

    const/16 v2, 0xc8

    if-ne p3, v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v2}, Lcom/twitter/android/UsersFragment;->y()V

    :cond_0
    iget-object v2, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget v1, v1, Lcom/twitter/android/client/PendingRequest;->b:I

    invoke-virtual {v2, v1, p5, v0}, Lcom/twitter/android/UsersFragment;->a(IIZ)V

    :cond_1
    return-void
.end method

.method public c(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 3

    invoke-direct {p0, p3}, Lcom/twitter/android/aai;->a(I)Z

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/UsersFragment;->p(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget-object v0, v0, Lcom/twitter/android/UsersFragment;->b:Ljava/util/HashMap;

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget-object v0, v0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0, p5, p6}, Lcom/twitter/library/util/FriendshipCache;->b(J)V

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v0}, Lcom/twitter/android/UsersFragment;->C(Lcom/twitter/android/UsersFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0}, Lcom/twitter/android/UsersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v1}, Lcom/twitter/android/UsersFragment;->D(Lcom/twitter/android/UsersFragment;)J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v0

    invoke-virtual {v0, p5, p6}, Lcom/twitter/library/provider/az;->a(J)Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v0, v0, Lcom/twitter/library/api/TwitterUser;->friendship:I

    invoke-static {v0}, Lcom/twitter/library/provider/ay;->l(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget-object v0, v0, Lcom/twitter/android/UsersFragment;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0, p5, p6}, Lcom/twitter/library/util/FriendshipCache;->d(J)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v0}, Lcom/twitter/android/UsersFragment;->E(Lcom/twitter/android/UsersFragment;)Landroid/support/v4/widget/CursorAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetChanged()V

    :cond_1
    return-void
.end method

.method public d(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;I)V
    .locals 4

    invoke-direct {p0, p3}, Lcom/twitter/android/aai;->a(I)Z

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v1, p2}, Lcom/twitter/android/UsersFragment;->j(Lcom/twitter/android/UsersFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/twitter/android/UsersFragment;->w:Z

    const/16 v2, 0xc8

    if-ne p3, v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {v2}, Lcom/twitter/android/UsersFragment;->y()V

    :cond_0
    iget-object v2, p0, Lcom/twitter/android/aai;->a:Lcom/twitter/android/UsersFragment;

    iget v1, v1, Lcom/twitter/android/client/PendingRequest;->b:I

    invoke-virtual {v2, v1, p5, v0}, Lcom/twitter/android/UsersFragment;->a(IIZ)V

    :cond_1
    return-void
.end method
