.class Lcom/twitter/android/fv;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/ExperimentSettingsActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/ExperimentSettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/fv;->a:Lcom/twitter/android/ExperimentSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "experiment_search"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/fv;->a:Lcom/twitter/android/ExperimentSettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/ExperimentSettingsActivity;->i(Lcom/twitter/android/ExperimentSettingsActivity;)V

    iget-object v0, p0, Lcom/twitter/android/fv;->a:Lcom/twitter/android/ExperimentSettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/ExperimentSettingsActivity;->b(Lcom/twitter/android/ExperimentSettingsActivity;)V

    :cond_0
    return-void
.end method
