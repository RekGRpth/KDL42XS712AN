.class public Lcom/twitter/android/FollowFlowController;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:Lcom/twitter/android/FollowFlowController$Initiator;

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Ljava/lang/String;

.field private g:[Ljava/lang/String;

.field private h:I

.field private i:Ljava/util/ArrayList;

.field private j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/android/gq;

    invoke-direct {v0}, Lcom/twitter/android/gq;-><init>()V

    sput-object v0, Lcom/twitter/android/FollowFlowController;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/twitter/android/FollowFlowController$Initiator;->b:Lcom/twitter/android/FollowFlowController$Initiator;

    iput-object v0, p0, Lcom/twitter/android/FollowFlowController;->a:Lcom/twitter/android/FollowFlowController$Initiator;

    iput-boolean v1, p0, Lcom/twitter/android/FollowFlowController;->b:Z

    iput-boolean v1, p0, Lcom/twitter/android/FollowFlowController;->c:Z

    iput-boolean v1, p0, Lcom/twitter/android/FollowFlowController;->d:Z

    iput-boolean v1, p0, Lcom/twitter/android/FollowFlowController;->e:Z

    iput-object v2, p0, Lcom/twitter/android/FollowFlowController;->f:Ljava/lang/String;

    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/FollowFlowController;->g:[Ljava/lang/String;

    iput v1, p0, Lcom/twitter/android/FollowFlowController;->h:I

    iput-object v2, p0, Lcom/twitter/android/FollowFlowController;->i:Ljava/util/ArrayList;

    iput-object v2, p0, Lcom/twitter/android/FollowFlowController;->j:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/twitter/android/FollowFlowController$Initiator;->b:Lcom/twitter/android/FollowFlowController$Initiator;

    iput-object v0, p0, Lcom/twitter/android/FollowFlowController;->a:Lcom/twitter/android/FollowFlowController$Initiator;

    iput-boolean v2, p0, Lcom/twitter/android/FollowFlowController;->b:Z

    iput-boolean v2, p0, Lcom/twitter/android/FollowFlowController;->c:Z

    iput-boolean v2, p0, Lcom/twitter/android/FollowFlowController;->d:Z

    iput-boolean v2, p0, Lcom/twitter/android/FollowFlowController;->e:Z

    iput-object v3, p0, Lcom/twitter/android/FollowFlowController;->f:Ljava/lang/String;

    new-array v0, v2, [Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/FollowFlowController;->g:[Ljava/lang/String;

    iput v2, p0, Lcom/twitter/android/FollowFlowController;->h:I

    iput-object v3, p0, Lcom/twitter/android/FollowFlowController;->i:Ljava/util/ArrayList;

    iput-object v3, p0, Lcom/twitter/android/FollowFlowController;->j:Ljava/lang/String;

    invoke-static {}, Lcom/twitter/android/FollowFlowController$Initiator;->values()[Lcom/twitter/android/FollowFlowController$Initiator;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    aget-object v0, v0, v3

    iput-object v0, p0, Lcom/twitter/android/FollowFlowController;->a:Lcom/twitter/android/FollowFlowController$Initiator;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/FollowFlowController;->b:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    new-array v0, v3, [Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/FollowFlowController;->g:[Ljava/lang/String;

    move v0, v2

    :goto_1
    if-ge v0, v3, :cond_1

    iget-object v4, p0, Lcom/twitter/android/FollowFlowController;->g:[Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/FollowFlowController;->h:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/twitter/android/FollowFlowController;->c:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/twitter/android/FollowFlowController;->e:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/FollowFlowController;->j:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_4

    :goto_4
    iput-boolean v1, p0, Lcom/twitter/android/FollowFlowController;->d:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/FollowFlowController;->f:Ljava/lang/String;

    return-void

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_4
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/twitter/android/gq;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/FollowFlowController;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private a(Landroid/app/Activity;I)V
    .locals 7

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/twitter/android/FollowFlowController;->g:[Ljava/lang/String;

    array-length v0, v0

    if-ge p2, v0, :cond_9

    iget-object v0, p0, Lcom/twitter/android/FollowFlowController;->g:[Ljava/lang/String;

    aget-object v0, v0, p2

    invoke-direct {p0}, Lcom/twitter/android/FollowFlowController;->h()Lcom/twitter/android/FollowFlowController;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/twitter/android/FollowFlowController;->a(I)Lcom/twitter/android/FollowFlowController;

    move-result-object v1

    const-string/jumbo v2, "follow_friends"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/FollowActivity;

    invoke-direct {v0, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "map_contacts"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "register_device"

    iget-boolean v3, p0, Lcom/twitter/android/FollowFlowController;->c:Z

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "may_upload_contacts"

    iget-boolean v3, p0, Lcom/twitter/android/FollowFlowController;->d:Z

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "device_to_register"

    iget-object v3, p0, Lcom/twitter/android/FollowFlowController;->j:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "preselect_friends"

    iget-boolean v3, p0, Lcom/twitter/android/FollowFlowController;->e:Z

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    :goto_0
    const-string/jumbo v2, "flow_controller"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :goto_1
    return-void

    :cond_0
    const-string/jumbo v2, "nux_tag_invite"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/InviteActivity;

    invoke-direct {v0, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lcom/twitter/android/FollowFlowController;->i:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    const-string/jumbo v2, "load_contacts"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/twitter/android/FollowFlowController;->i:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_2

    add-int/lit8 v0, p2, 0x1

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/FollowFlowController;->a(Landroid/app/Activity;I)V

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/twitter/android/FollowFlowController;->i:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/16 v3, 0x3e8

    if-le v2, v3, :cond_3

    const-string/jumbo v2, "load_contacts"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {v1}, Lcom/twitter/android/FollowFlowController;->f()Lcom/twitter/android/FollowFlowController;

    goto :goto_0

    :cond_3
    const-string/jumbo v2, "contacts"

    iget-object v3, p0, Lcom/twitter/android/FollowFlowController;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_0

    :cond_4
    const-string/jumbo v2, "follow_recommendations"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/FollowActivity;

    invoke-direct {v0, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0

    :cond_5
    const-string/jumbo v2, "follow_interest"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v0, p0, Lcom/twitter/android/FollowFlowController;->f:Ljava/lang/String;

    if-nez v0, :cond_6

    add-int/lit8 v0, p2, 0x1

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/FollowFlowController;->a(Landroid/app/Activity;I)V

    goto :goto_1

    :cond_6
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/FollowActivity;

    invoke-direct {v0, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0

    :cond_7
    const-string/jumbo v2, "phone_entry"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/PhoneEntryActivity;

    invoke-direct {v0, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto/16 :goto_0

    :cond_8
    invoke-static {p1}, Lcom/twitter/android/FollowFlowController;->c(Landroid/app/Activity;)V

    goto/16 :goto_1

    :cond_9
    invoke-virtual {p0}, Lcom/twitter/android/FollowFlowController;->c()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {p1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {p1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "::::complete"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v0, v1, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_a
    invoke-static {p1}, Lcom/twitter/android/FollowFlowController;->c(Landroid/app/Activity;)V

    goto/16 :goto_1
.end method

.method public static c(Landroid/app/Activity;)V
    .locals 6

    invoke-static {p0}, Lcom/twitter/android/FollowFlowController;->e(Landroid/app/Activity;)V

    invoke-static {p0}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;)Lcom/twitter/android/util/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/util/d;->a()Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "android_edit_profile_nux_1267"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v0, "android_edit_profile_nux_1267"

    invoke-static {v0}, Lkk;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/EditProfileOnboardingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "welcome:::avatar:egg"

    aput-object v5, v3, v4

    invoke-virtual {v2, v0, v1, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_1
    invoke-static {p0}, Lcom/twitter/android/StartActivity;->a(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method public static e(Landroid/app/Activity;)V
    .locals 2

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "flow_initiator"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "flow_digits_enabled"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "flow_register_device"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "flow_onboard_interest"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "flow_preselect_friends"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "flow_may_upload_contacts"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "flow_flowsteps"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "flow_step"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public static f(Landroid/app/Activity;)Z
    .locals 2

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "flow_flowsteps"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static g(Landroid/app/Activity;)V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x0

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "flow_flowsteps"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string/jumbo v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/FollowFlowController;

    invoke-direct {v2}, Lcom/twitter/android/FollowFlowController;-><init>()V

    invoke-static {}, Lcom/twitter/android/FollowFlowController$Initiator;->values()[Lcom/twitter/android/FollowFlowController$Initiator;

    move-result-object v3

    const-string/jumbo v4, "flow_initiator"

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/android/FollowFlowController;->a(Lcom/twitter/android/FollowFlowController$Initiator;)Lcom/twitter/android/FollowFlowController;

    move-result-object v2

    const-string/jumbo v3, "flow_digits_enabled"

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/FollowFlowController;->a(Z)Lcom/twitter/android/FollowFlowController;

    move-result-object v2

    const-string/jumbo v3, "flow_register_device"

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/FollowFlowController;->b(Z)Lcom/twitter/android/FollowFlowController;

    move-result-object v2

    const-string/jumbo v3, "flow_onboard_interest"

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/FollowFlowController;->b(Ljava/lang/String;)Lcom/twitter/android/FollowFlowController;

    move-result-object v2

    const-string/jumbo v3, "flow_preselect_friends"

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/FollowFlowController;->d(Z)Lcom/twitter/android/FollowFlowController;

    move-result-object v2

    const-string/jumbo v3, "flow_may_upload_contacts"

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/FollowFlowController;->c(Z)Lcom/twitter/android/FollowFlowController;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/twitter/android/FollowFlowController;->a([Ljava/lang/String;)Lcom/twitter/android/FollowFlowController;

    move-result-object v1

    const-string/jumbo v2, "flow_step"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/FollowFlowController;->a(I)Lcom/twitter/android/FollowFlowController;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/FollowFlowController;->b(Landroid/app/Activity;)V

    invoke-static {p0}, Lcom/twitter/android/FollowFlowController;->e(Landroid/app/Activity;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p0, v6}, Lcom/twitter/android/MainActivity;->a(Landroid/app/Activity;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method private h()Lcom/twitter/android/FollowFlowController;
    .locals 2

    new-instance v0, Lcom/twitter/android/FollowFlowController;

    invoke-direct {v0}, Lcom/twitter/android/FollowFlowController;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/FollowFlowController;->g:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/FollowFlowController;->a([Ljava/lang/String;)Lcom/twitter/android/FollowFlowController;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/FollowFlowController;->h:I

    invoke-virtual {v0, v1}, Lcom/twitter/android/FollowFlowController;->a(I)Lcom/twitter/android/FollowFlowController;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/FollowFlowController;->a:Lcom/twitter/android/FollowFlowController$Initiator;

    invoke-virtual {v0, v1}, Lcom/twitter/android/FollowFlowController;->a(Lcom/twitter/android/FollowFlowController$Initiator;)Lcom/twitter/android/FollowFlowController;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/FollowFlowController;->b:Z

    invoke-virtual {v0, v1}, Lcom/twitter/android/FollowFlowController;->a(Z)Lcom/twitter/android/FollowFlowController;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/FollowFlowController;->c:Z

    invoke-virtual {v0, v1}, Lcom/twitter/android/FollowFlowController;->b(Z)Lcom/twitter/android/FollowFlowController;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/FollowFlowController;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/FollowFlowController;->b(Ljava/lang/String;)Lcom/twitter/android/FollowFlowController;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/FollowFlowController;->e:Z

    invoke-virtual {v0, v1}, Lcom/twitter/android/FollowFlowController;->d(Z)Lcom/twitter/android/FollowFlowController;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/FollowFlowController;->d:Z

    invoke-virtual {v0, v1}, Lcom/twitter/android/FollowFlowController;->c(Z)Lcom/twitter/android/FollowFlowController;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/FollowFlowController;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/twitter/android/FollowFlowController;->a(Ljava/util/ArrayList;)Lcom/twitter/android/FollowFlowController;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/FollowFlowController;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/FollowFlowController;->a(Ljava/lang/String;)Lcom/twitter/android/FollowFlowController;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/android/FollowFlowController$Initiator;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/FollowFlowController;->a:Lcom/twitter/android/FollowFlowController$Initiator;

    return-object v0
.end method

.method public a(I)Lcom/twitter/android/FollowFlowController;
    .locals 0

    iput p1, p0, Lcom/twitter/android/FollowFlowController;->h:I

    return-object p0
.end method

.method public a(Lcom/twitter/android/FollowFlowController$Initiator;)Lcom/twitter/android/FollowFlowController;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/FollowFlowController;->a:Lcom/twitter/android/FollowFlowController$Initiator;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/android/FollowFlowController;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/FollowFlowController;->j:Ljava/lang/String;

    return-object p0
.end method

.method public a(Ljava/util/ArrayList;)Lcom/twitter/android/FollowFlowController;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/FollowFlowController;->i:Ljava/util/ArrayList;

    return-object p0
.end method

.method public a(Z)Lcom/twitter/android/FollowFlowController;
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/FollowFlowController;->b:Z

    return-object p0
.end method

.method public a([Ljava/lang/String;)Lcom/twitter/android/FollowFlowController;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/FollowFlowController;->g:[Ljava/lang/String;

    return-object p0
.end method

.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f02d3    # com.twitter.android.R.string.nux_progress

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/twitter/android/FollowFlowController;->h:I

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/twitter/android/FollowFlowController;->g:[Ljava/lang/String;

    array-length v3, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/app/Activity;)V
    .locals 1

    iget v0, p0, Lcom/twitter/android/FollowFlowController;->h:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/FollowFlowController;->a(Landroid/app/Activity;I)V

    return-void
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/android/FollowFlowController;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/FollowFlowController;->f:Ljava/lang/String;

    return-object p0
.end method

.method public b(Z)Lcom/twitter/android/FollowFlowController;
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/FollowFlowController;->c:Z

    return-object p0
.end method

.method public b(Landroid/app/Activity;)V
    .locals 1

    iget v0, p0, Lcom/twitter/android/FollowFlowController;->h:I

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/FollowFlowController;->a(Landroid/app/Activity;I)V

    return-void
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/FollowFlowController;->b:Z

    return v0
.end method

.method public c(Z)Lcom/twitter/android/FollowFlowController;
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/FollowFlowController;->d:Z

    return-object p0
.end method

.method public c()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/FollowFlowController;->a:Lcom/twitter/android/FollowFlowController$Initiator;

    sget-object v1, Lcom/twitter/android/FollowFlowController$Initiator;->a:Lcom/twitter/android/FollowFlowController$Initiator;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/FollowFlowController;->a:Lcom/twitter/android/FollowFlowController$Initiator;

    sget-object v1, Lcom/twitter/android/FollowFlowController$Initiator;->d:Lcom/twitter/android/FollowFlowController$Initiator;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)Z
    .locals 2

    iget v0, p0, Lcom/twitter/android/FollowFlowController;->h:I

    iget-object v1, p0, Lcom/twitter/android/FollowFlowController;->g:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/FollowFlowController;->g:[Ljava/lang/String;

    iget v1, p0, Lcom/twitter/android/FollowFlowController;->h:I

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(Z)Lcom/twitter/android/FollowFlowController;
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/FollowFlowController;->e:Z

    return-object p0
.end method

.method public d()Lcom/twitter/android/util/x;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/FollowFlowController;->f:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/android/util/x;->a(Ljava/lang/String;)Lcom/twitter/android/util/x;

    move-result-object v0

    return-object v0
.end method

.method public d(Landroid/app/Activity;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/FollowFlowController;->g:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Ljava/lang/StringBuffer;

    iget-object v0, p0, Lcom/twitter/android/FollowFlowController;->g:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    invoke-direct {v1, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_1
    iget-object v2, p0, Lcom/twitter/android/FollowFlowController;->g:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/FollowFlowController;->g:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "flow_initiator"

    iget-object v3, p0, Lcom/twitter/android/FollowFlowController;->a:Lcom/twitter/android/FollowFlowController$Initiator;

    invoke-virtual {v3}, Lcom/twitter/android/FollowFlowController$Initiator;->ordinal()I

    move-result v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "flow_digits_enabled"

    iget-boolean v3, p0, Lcom/twitter/android/FollowFlowController;->b:Z

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "flow_register_device"

    iget-boolean v3, p0, Lcom/twitter/android/FollowFlowController;->c:Z

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "flow_onboard_interest"

    iget-object v3, p0, Lcom/twitter/android/FollowFlowController;->f:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "flow_preselect_friends"

    iget-boolean v3, p0, Lcom/twitter/android/FollowFlowController;->e:Z

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "flow_may_upload_contacts"

    iget-boolean v3, p0, Lcom/twitter/android/FollowFlowController;->d:Z

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "flow_flowsteps"

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "flow_step"

    iget v2, p0, Lcom/twitter/android/FollowFlowController;->h:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e()Z
    .locals 2

    iget v0, p0, Lcom/twitter/android/FollowFlowController;->h:I

    iget-object v1, p0, Lcom/twitter/android/FollowFlowController;->g:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Lcom/twitter/android/FollowFlowController;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/FollowFlowController;->a(Ljava/util/ArrayList;)Lcom/twitter/android/FollowFlowController;

    move-result-object v0

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/FollowFlowController;->a:Lcom/twitter/android/FollowFlowController$Initiator;

    sget-object v1, Lcom/twitter/android/FollowFlowController$Initiator;->a:Lcom/twitter/android/FollowFlowController$Initiator;

    if-ne v0, v1, :cond_0

    const-string/jumbo v0, "welcome"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/FollowFlowController;->a:Lcom/twitter/android/FollowFlowController$Initiator;

    sget-object v1, Lcom/twitter/android/FollowFlowController$Initiator;->d:Lcom/twitter/android/FollowFlowController$Initiator;

    if-ne v0, v1, :cond_1

    const-string/jumbo v0, "opt_in"

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/FollowFlowController;->a:Lcom/twitter/android/FollowFlowController$Initiator;

    sget-object v1, Lcom/twitter/android/FollowFlowController$Initiator;->b:Lcom/twitter/android/FollowFlowController$Initiator;

    if-ne v0, v1, :cond_2

    const-string/jumbo v0, "discover"

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/FollowFlowController;->a:Lcom/twitter/android/FollowFlowController$Initiator;

    sget-object v1, Lcom/twitter/android/FollowFlowController$Initiator;->c:Lcom/twitter/android/FollowFlowController$Initiator;

    if-ne v0, v1, :cond_3

    const-string/jumbo v0, "add_phone_prompt"

    goto :goto_0

    :cond_3
    const-string/jumbo v0, "error"

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/FollowFlowController;->a:Lcom/twitter/android/FollowFlowController$Initiator;

    invoke-virtual {v0}, Lcom/twitter/android/FollowFlowController$Initiator;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/twitter/android/FollowFlowController;->b:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/android/FollowFlowController;->g:[Ljava/lang/String;

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    move v0, v2

    :goto_1
    iget-object v3, p0, Lcom/twitter/android/FollowFlowController;->g:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    iget-object v3, p0, Lcom/twitter/android/FollowFlowController;->g:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/twitter/android/FollowFlowController;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/twitter/android/FollowFlowController;->c:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/twitter/android/FollowFlowController;->e:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/android/FollowFlowController;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/twitter/android/FollowFlowController;->d:Z

    if-eqz v0, :cond_4

    :goto_4
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/android/FollowFlowController;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_4
.end method
