.class Lcom/twitter/android/pp;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/client/c;

.field final synthetic b:Lcom/twitter/android/PostActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/PostActivity;Lcom/twitter/android/client/c;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/pp;->b:Lcom/twitter/android/PostActivity;

    iput-object p2, p0, Lcom/twitter/android/pp;->a:Lcom/twitter/android/client/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    const/4 v1, 0x4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/twitter/android/pp;->b:Lcom/twitter/android/PostActivity;

    iget-boolean v0, v0, Lcom/twitter/android/PostActivity;->u:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/pp;->b:Lcom/twitter/android/PostActivity;

    iget-boolean v0, v0, Lcom/twitter/android/PostActivity;->z:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/pp;->b:Lcom/twitter/android/PostActivity;

    iget v0, v0, Lcom/twitter/android/PostActivity;->v:I

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/pp;->b:Lcom/twitter/android/PostActivity;

    invoke-virtual {v0, v1, v3}, Lcom/twitter/android/PostActivity;->a(IZ)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/pp;->a:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/pp;->b:Lcom/twitter/android/PostActivity;

    invoke-static {v1}, Lcom/twitter/android/PostActivity;->d(Lcom/twitter/android/PostActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "compose:poi::poi_tag:click"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_1
    return-void
.end method
