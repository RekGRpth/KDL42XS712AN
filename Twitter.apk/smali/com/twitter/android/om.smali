.class Lcom/twitter/android/om;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/twitter/android/PhotoGridFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/PhotoGridFragment;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/om;->b:Lcom/twitter/android/PhotoGridFragment;

    iput-object p2, p0, Lcom/twitter/android/om;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ok;

    iget-object v1, p0, Lcom/twitter/android/om;->b:Lcom/twitter/android/PhotoGridFragment;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/twitter/android/om;->a:Landroid/content/Context;

    const-class v4, Lcom/twitter/android/GalleryActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v3, p0, Lcom/twitter/android/om;->b:Lcom/twitter/android/PhotoGridFragment;

    invoke-static {v3}, Lcom/twitter/android/PhotoGridFragment;->a(Lcom/twitter/android/PhotoGridFragment;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "prj"

    sget-object v4, Lcom/twitter/library/provider/Tweet;->a:[Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "sel"

    const-string/jumbo v4, "flags&1 != 0"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "orderBy"

    const-string/jumbo v4, "updated_at DESC, _id ASC"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "id"

    iget-object v0, v0, Lcom/twitter/android/ok;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v4, v0, Lcom/twitter/library/provider/Tweet;->u:J

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/PhotoGridFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
