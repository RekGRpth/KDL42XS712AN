.class Lcom/twitter/android/xk;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/in;


# instance fields
.field final synthetic a:Lcom/twitter/android/InReplyToDialogFragment;

.field final synthetic b:Lcom/twitter/android/TweetFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/TweetFragment;Lcom/twitter/android/InReplyToDialogFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/xk;->b:Lcom/twitter/android/TweetFragment;

    iput-object p2, p0, Lcom/twitter/android/xk;->a:Lcom/twitter/android/InReplyToDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public a(J)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/xk;->b:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/TweetFragment;->c(J)V

    iget-object v0, p0, Lcom/twitter/android/xk;->a:Lcom/twitter/android/InReplyToDialogFragment;

    invoke-virtual {v0}, Lcom/twitter/android/InReplyToDialogFragment;->dismiss()V

    iget-object v0, p0, Lcom/twitter/android/xk;->b:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->p(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v2, p0, Lcom/twitter/android/xk;->b:Lcom/twitter/android/TweetFragment;

    invoke-static {v2}, Lcom/twitter/android/TweetFragment;->o(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "tweet::reply_dialog::profile_click"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method public a(Ljava/util/HashSet;)V
    .locals 0

    return-void
.end method
