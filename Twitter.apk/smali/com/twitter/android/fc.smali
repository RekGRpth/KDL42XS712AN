.class Lcom/twitter/android/fc;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Landroid/widget/ImageView;

.field public final b:Landroid/widget/TextView;

.field public final c:Landroid/widget/TextView;

.field public final d:Landroid/widget/TextView;

.field public e:Z

.field public f:J

.field public g:Landroid/content/Intent;

.field public h:Ljava/lang/String;

.field public i:Lcom/twitter/library/scribe/ScribeItem;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/fc;->e:Z

    const v0, 0x7f09001f    # com.twitter.android.R.id.icon

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/fc;->a:Landroid/widget/ImageView;

    const v0, 0x7f09009d    # com.twitter.android.R.id.text_item

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/fc;->b:Landroid/widget/TextView;

    const v0, 0x7f09004d    # com.twitter.android.R.id.promoted

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/fc;->c:Landroid/widget/TextView;

    const v0, 0x7f090291    # com.twitter.android.R.id.sub_title

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/fc;->d:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method a(I)V
    .locals 2

    const v0, 0x7f0202b4    # com.twitter.android.R.drawable.icn_trending_default

    iget-boolean v1, p0, Lcom/twitter/android/fc;->e:Z

    if-eqz v1, :cond_0

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/fc;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void

    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const v0, 0x7f0202b5    # com.twitter.android.R.drawable.icn_tv_default

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0202ad    # com.twitter.android.R.drawable.icn_local_default

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0202b1    # com.twitter.android.R.drawable.icn_sports_default

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method a(IILandroid/content/Context;)V
    .locals 5

    const v0, 0x7f0f0163    # com.twitter.android.R.string.event_subtitle_popular_event

    const/4 v4, 0x0

    iget-boolean v1, p0, Lcom/twitter/android/fc;->e:Z

    if-eqz v1, :cond_0

    if-lez p1, :cond_0

    packed-switch p2, :pswitch_data_0

    :goto_0
    :pswitch_0
    iget-object v1, p0, Lcom/twitter/android/fc;->d:Landroid/widget/TextView;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3, p1}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p3, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/fc;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    return-void

    :pswitch_1
    const v0, 0x7f0f0166    # com.twitter.android.R.string.event_subtitle_popular_trend

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0f0165    # com.twitter.android.R.string.event_subtitle_popular_show

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/fc;->d:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method a(Landroid/content/Context;II)V
    .locals 1

    invoke-virtual {p1, p2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p3}, Lcom/twitter/android/fc;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    return-void
.end method

.method a(Landroid/content/Context;Ljava/lang/CharSequence;I)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/fc;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/fc;->b:Landroid/widget/TextView;

    const/4 v1, 0x0

    int-to-float v2, p3

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    return-void
.end method

.method a(Lcom/twitter/library/api/PromotedContent;Landroid/content/Context;)V
    .locals 5

    const/16 v3, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/fc;->e:Z

    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/twitter/android/fc;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/fc;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/twitter/library/api/PromotedContent;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/fc;->c:Landroid/widget/TextView;

    const v1, 0x7f020124    # com.twitter.android.R.drawable.ic_badge_gov_default

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    :goto_1
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iget-object v0, p1, Lcom/twitter/library/api/PromotedContent;->advertiserName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/fc;->c:Landroid/widget/TextView;

    const v3, 0x7f0f0333    # com.twitter.android.R.string.promoted_by

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p1, Lcom/twitter/library/api/PromotedContent;->advertiserName:Ljava/lang/String;

    aput-object v4, v1, v2

    invoke-virtual {p2, v3, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/fc;->c:Landroid/widget/TextView;

    const v1, 0x7f020125    # com.twitter.android.R.drawable.ic_badge_promoted_default

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/fc;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/fc;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/fc;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method b(I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/fc;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method
