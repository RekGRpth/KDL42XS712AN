.class public Lcom/twitter/android/AuthorizeAppActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/an;


# instance fields
.field private a:Landroid/content/ServiceConnection;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/AuthorizeAppActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/AuthorizeAppActivity;->g()V

    return-void
.end method

.method private f()Ljava/lang/CharSequence;
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/AuthorizeAppActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/AuthorizeAppActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    :try_start_0
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private g()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppActivity;->a:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppActivity;->a:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/twitter/android/AuthorizeAppActivity;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/AuthorizeAppActivity;->a:Landroid/content/ServiceConnection;

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->c(I)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->c(Z)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->b(Z)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->a(Z)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->a(I)V

    return-object v0
.end method

.method public a()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/AuthorizeAppActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/twitter/android/AuthorizeAppActivity;->finish()V

    return-void
.end method

.method public a(I)V
    .locals 1

    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/twitter/android/AuthorizeAppActivity;->finish()V

    return-void
.end method

.method public a(Landroid/accounts/Account;)V
    .locals 4

    new-instance v0, Lcom/twitter/android/al;

    iget-object v1, p0, Lcom/twitter/android/AuthorizeAppActivity;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/AuthorizeAppActivity;->c:Ljava/lang/String;

    new-instance v3, Lcom/twitter/android/am;

    invoke-direct {v3, p0}, Lcom/twitter/android/am;-><init>(Lcom/twitter/android/AuthorizeAppActivity;)V

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/twitter/android/al;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/android/am;Landroid/accounts/Account;)V

    const/4 v1, 0x1

    invoke-virtual {p0, p0, v0, v1}, Lcom/twitter/android/AuthorizeAppActivity;->a(Landroid/content/Context;Landroid/content/ServiceConnection;I)Z

    move-result v1

    if-eqz v1, :cond_0

    iput-object v0, p0, Lcom/twitter/android/AuthorizeAppActivity;->a:Landroid/content/ServiceConnection;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/AuthorizeAppActivity;->a:Landroid/content/ServiceConnection;

    const v0, 0x7f0f000c    # com.twitter.android.R.string.action_error

    invoke-virtual {p0, v0}, Lcom/twitter/android/AuthorizeAppActivity;->a(I)V

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 5

    const v4, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    invoke-virtual {p0}, Lcom/twitter/android/AuthorizeAppActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "ck"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/AuthorizeAppActivity;->b:Ljava/lang/String;

    const-string/jumbo v1, "cs"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/AuthorizeAppActivity;->c:Ljava/lang/String;

    if-nez p1, :cond_1

    new-instance v0, Lcom/twitter/android/AuthorizeAppFragment;

    invoke-direct {v0}, Lcom/twitter/android/AuthorizeAppFragment;-><init>()V

    invoke-direct {p0}, Lcom/twitter/android/AuthorizeAppActivity;->f()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v3, "app_name"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    const-string/jumbo v1, "app_consumer_key"

    iget-object v3, p0, Lcom/twitter/android/AuthorizeAppActivity;->b:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/AuthorizeAppFragment;->setArguments(Landroid/os/Bundle;)V

    :cond_0
    invoke-virtual {v0, p0}, Lcom/twitter/android/AuthorizeAppFragment;->a(Lcom/twitter/android/an;)V

    invoke-virtual {p0}, Lcom/twitter/android/AuthorizeAppActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1, v4, v0}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/AuthorizeAppActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/AuthorizeAppFragment;

    invoke-virtual {v0, p0}, Lcom/twitter/android/AuthorizeAppFragment;->a(Lcom/twitter/android/an;)V

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Landroid/content/ServiceConnection;I)Z
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/library/service/AuthTokenService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v0, p2, p3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/client/BaseFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/AuthorizeAppActivity;->finish()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/AuthorizeAppActivity;->g()V

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onDestroy()V

    return-void
.end method

.method protected onStart()V
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onStart()V

    invoke-virtual {p0}, Lcom/twitter/android/AuthorizeAppActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/LoginActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "add_account"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "authorize_account"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v2}, Lcom/twitter/android/AuthorizeAppActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    return-void
.end method
