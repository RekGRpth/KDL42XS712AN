.class Lcom/twitter/android/ts;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/StartActivity;

.field private final b:Landroid/support/v4/view/ViewPager;

.field private final c:Lgg;

.field private final d:Lcom/twitter/android/widget/PipView;

.field private final e:I

.field private final f:Landroid/os/Handler;

.field private final g:Ljava/lang/Runnable;

.field private h:I

.field private i:Z


# direct methods
.method public constructor <init>(Lcom/twitter/android/StartActivity;Landroid/support/v4/view/ViewPager;Lcom/twitter/android/widget/PipView;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/ts;->a:Lcom/twitter/android/StartActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ts;->f:Landroid/os/Handler;

    new-instance v0, Lcom/twitter/android/tt;

    invoke-direct {v0, p0}, Lcom/twitter/android/tt;-><init>(Lcom/twitter/android/ts;)V

    iput-object v0, p0, Lcom/twitter/android/ts;->g:Ljava/lang/Runnable;

    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/android/ts;->h:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/ts;->i:Z

    iput-object p2, p0, Lcom/twitter/android/ts;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {p2}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    check-cast v0, Lgg;

    iput-object v0, p0, Lcom/twitter/android/ts;->c:Lgg;

    iput-object p3, p0, Lcom/twitter/android/ts;->d:Lcom/twitter/android/widget/PipView;

    const/16 v0, 0x1388

    invoke-direct {p0, v0}, Lcom/twitter/android/ts;->a(I)V

    iget-object v0, p0, Lcom/twitter/android/ts;->c:Lgg;

    invoke-virtual {v0}, Lgg;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/twitter/android/ts;->e:I

    return-void
.end method

.method private a()V
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/android/ts;->i:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/ts;->i:Z

    iget-object v0, p0, Lcom/twitter/android/ts;->f:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/android/ts;->g:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method private a(I)V
    .locals 4

    iget-boolean v0, p0, Lcom/twitter/android/ts;->i:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/ts;->i:Z

    iget-object v0, p0, Lcom/twitter/android/ts;->f:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/android/ts;->g:Ljava/lang/Runnable;

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/ts;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/ts;->a()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/ts;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/ts;->a(I)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/ts;)I
    .locals 1

    iget v0, p0, Lcom/twitter/android/ts;->h:I

    return v0
.end method

.method static synthetic c(Lcom/twitter/android/ts;)Landroid/support/v4/view/ViewPager;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ts;->b:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 6

    const/4 v3, 0x1

    const/4 v5, 0x0

    if-nez p1, :cond_3

    iget v0, p0, Lcom/twitter/android/ts;->h:I

    if-nez v0, :cond_2

    iget v0, p0, Lcom/twitter/android/ts;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/twitter/android/ts;->h:I

    iget-object v0, p0, Lcom/twitter/android/ts;->b:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/twitter/android/ts;->h:I

    invoke-virtual {v0, v1, v5}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    :cond_0
    :goto_0
    const/16 v0, 0x1b58

    invoke-direct {p0, v0}, Lcom/twitter/android/ts;->a(I)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget v0, p0, Lcom/twitter/android/ts;->h:I

    iget v1, p0, Lcom/twitter/android/ts;->e:I

    if-ne v0, v1, :cond_0

    iput v3, p0, Lcom/twitter/android/ts;->h:I

    iget-object v0, p0, Lcom/twitter/android/ts;->b:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/twitter/android/ts;->h:I

    invoke-virtual {v0, v1, v5}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    goto :goto_0

    :cond_3
    if-ne p1, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ts;->a:Lcom/twitter/android/StartActivity;

    invoke-static {v0}, Lcom/twitter/android/StartActivity;->b(Lcom/twitter/android/StartActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ts;->a:Lcom/twitter/android/StartActivity;

    invoke-static {v1}, Lcom/twitter/android/StartActivity;->a(Lcom/twitter/android/StartActivity;)Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "front::::scroll"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/twitter/android/ts;->a()V

    goto :goto_1
.end method

.method public onPageScrolled(IFI)V
    .locals 0

    return-void
.end method

.method public onPageSelected(I)V
    .locals 2

    iput p1, p0, Lcom/twitter/android/ts;->h:I

    iget-object v0, p0, Lcom/twitter/android/ts;->d:Lcom/twitter/android/widget/PipView;

    iget-object v1, p0, Lcom/twitter/android/ts;->c:Lgg;

    invoke-virtual {v1, p1}, Lgg;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PipView;->setPipOnPosition(I)V

    invoke-direct {p0}, Lcom/twitter/android/ts;->a()V

    const/16 v0, 0x1b58

    invoke-direct {p0, v0}, Lcom/twitter/android/ts;->a(I)V

    return-void
.end method
