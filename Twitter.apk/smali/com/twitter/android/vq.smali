.class Lcom/twitter/android/vq;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/library/provider/Tweet;

.field final synthetic b:Lcom/twitter/android/vn;


# direct methods
.method constructor <init>(Lcom/twitter/android/vn;Lcom/twitter/library/provider/Tweet;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/vq;->b:Lcom/twitter/android/vn;

    iput-object p2, p0, Lcom/twitter/android/vq;->a:Lcom/twitter/library/provider/Tweet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/vq;->a:Lcom/twitter/library/provider/Tweet;

    iget-object v0, v0, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    :goto_0
    iget-object v2, p0, Lcom/twitter/android/vq;->b:Lcom/twitter/android/vn;

    iget-object v2, v2, Lcom/twitter/android/vn;->a:Lcom/twitter/android/client/c;

    iget-object v3, p0, Lcom/twitter/android/vq;->a:Lcom/twitter/library/provider/Tweet;

    iget v3, v3, Lcom/twitter/library/provider/Tweet;->N:I

    iget-object v4, p0, Lcom/twitter/android/vq;->a:Lcom/twitter/library/provider/Tweet;

    iget-wide v4, v4, Lcom/twitter/library/provider/Tweet;->L:J

    invoke-virtual {v2, v3, v4, v5, v0}, Lcom/twitter/android/client/c;->a(IJLjava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/vq;->b:Lcom/twitter/android/vn;

    const-string/jumbo v2, "dismiss"

    iget-object v3, p0, Lcom/twitter/android/vq;->a:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0, v2, v3, v1}, Lcom/twitter/android/vn;->a(Ljava/lang/String;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeItem;)V

    return-void

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method
