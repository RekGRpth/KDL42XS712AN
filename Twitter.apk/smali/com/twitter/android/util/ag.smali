.class public Lcom/twitter/android/util/ag;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Lcom/twitter/library/api/TwitterUser;)Z
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->verified:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-static {v0, v1}, Lcom/twitter/library/featureswitch/a;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-static {v0, v1}, Lcom/twitter/library/featureswitch/a;->b(J)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->verified:Z

    if-nez v0, :cond_2

    iget-wide v0, p0, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-static {v0, v1}, Lcom/twitter/library/featureswitch/a;->c(J)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-wide v0, p0, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-static {v0, v1}, Lcom/twitter/library/featureswitch/a;->d(J)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
