.class public Lcom/twitter/android/util/x;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static a:Ljava/util/HashMap;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:J


# direct methods
.method static constructor <clinit>()V
    .locals 6

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/twitter/android/util/x;->a:Ljava/util/HashMap;

    sget-object v0, Lcom/twitter/android/util/x;->a:Ljava/util/HashMap;

    const-string/jumbo v1, "raptor-team"

    new-instance v2, Lcom/twitter/android/util/x;

    const-string/jumbo v3, "Raptor Team"

    const-wide/32 v4, 0x6a1d3dc

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/twitter/android/util/x;-><init>(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/util/x;->a:Ljava/util/HashMap;

    const-string/jumbo v1, "sfgiants"

    new-instance v2, Lcom/twitter/android/util/x;

    const-string/jumbo v3, "SF Giants"

    const-wide/32 v4, 0xb839c4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/twitter/android/util/x;-><init>(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/util/x;->a:Ljava/util/HashMap;

    const-string/jumbo v1, "reds"

    new-instance v2, Lcom/twitter/android/util/x;

    const-string/jumbo v3, "Cincinnati Reds"

    const-wide/32 v4, 0x6e671dd

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/twitter/android/util/x;-><init>(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Long;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/util/x;->b:Ljava/lang/String;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/util/x;->c:J

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/twitter/android/util/x;
    .locals 1

    sget-object v0, Lcom/twitter/android/util/x;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/util/x;

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 1

    sget-object v0, Lcom/twitter/android/util/x;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
