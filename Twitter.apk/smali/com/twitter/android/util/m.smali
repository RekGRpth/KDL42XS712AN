.class Lcom/twitter/android/util/m;
.super Lcom/twitter/library/service/a;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I

.field private final c:Lcom/twitter/android/util/g;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/twitter/android/util/g;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/library/service/a;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/util/m;->a:Landroid/content/Context;

    iput-object p3, p0, Lcom/twitter/android/util/m;->c:Lcom/twitter/android/util/g;

    iput p2, p0, Lcom/twitter/android/util/m;->b:I

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/internal/android/service/a;)V
    .locals 0

    check-cast p1, Lcom/twitter/library/service/b;

    invoke-virtual {p0, p1}, Lcom/twitter/android/util/m;->a(Lcom/twitter/library/service/b;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/b;)V
    .locals 5

    check-cast p1, Lcom/twitter/library/api/d;

    invoke-virtual {p1}, Lcom/twitter/library/api/d;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/util/m;->c:Lcom/twitter/android/util/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/util/m;->c:Lcom/twitter/android/util/g;

    invoke-interface {v0}, Lcom/twitter/android/util/g;->a()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/twitter/android/util/m;->b:I

    if-lez v0, :cond_2

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/twitter/android/util/n;

    iget-object v2, p0, Lcom/twitter/android/util/m;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/android/util/m;->c:Lcom/twitter/android/util/g;

    iget v4, p0, Lcom/twitter/android/util/m;->b:I

    add-int/lit8 v4, v4, -0x1

    invoke-direct {v1, v2, v3, v4}, Lcom/twitter/android/util/n;-><init>(Landroid/content/Context;Lcom/twitter/android/util/g;I)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/util/m;->c:Lcom/twitter/android/util/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/util/m;->c:Lcom/twitter/android/util/g;

    invoke-interface {v0}, Lcom/twitter/android/util/g;->b()V

    goto :goto_0
.end method
