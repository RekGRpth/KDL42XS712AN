.class public Lcom/twitter/android/util/i;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/util/d;


# static fields
.field private static final a:Ljava/util/Set;

.field private static b:Z

.field private static c:Ljava/lang/String;


# instance fields
.field private final d:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "com.whatsapp"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string/jumbo v2, "com.facebook"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/util/i;->a:Ljava/util/Set;

    sput-boolean v3, Lcom/twitter/android/util/i;->b:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/twitter/android/util/i;->c:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/util/i;->d:Landroid/content/Context;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/util/i;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/util/i;->d:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    sput-object p0, Lcom/twitter/android/util/i;->c:Ljava/lang/String;

    return-object p0
.end method

.method private r()Z
    .locals 4

    const-string/jumbo v0, "android_digits_mo_1958"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "mo_guessing"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "mt_only_guessing"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->a()Lcom/google/i18n/phonenumbers/PhoneNumberUtil;

    move-result-object v1

    :try_start_0
    iget-object v2, p0, Lcom/twitter/android/util/i;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->b(Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Lcom/google/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;->a:Lcom/google/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;

    invoke-virtual {v1, v2, v3}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->a(Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;Lcom/google/i18n/phonenumbers/PhoneNumberUtil$PhoneNumberFormat;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/i18n/phonenumbers/NumberParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/util/e;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/util/i;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/util/i;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/library/client/w;->a(Landroid/content/Context;)Lcom/twitter/library/client/w;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/api/d;

    iget-object v3, p0, Lcom/twitter/android/util/i;->d:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lcom/twitter/library/api/d;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    new-instance v0, Lcom/twitter/android/util/k;

    invoke-direct {v0, p1}, Lcom/twitter/android/util/k;-><init>(Lcom/twitter/android/util/e;)V

    invoke-virtual {v2, v0}, Lcom/twitter/library/api/d;->a(Lcom/twitter/internal/android/service/m;)Lcom/twitter/internal/android/service/a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    return-void
.end method

.method public a(Lcom/twitter/android/util/f;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/util/i;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/util/i;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/library/client/w;->a(Landroid/content/Context;)Lcom/twitter/library/client/w;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/api/m;

    iget-object v3, p0, Lcom/twitter/android/util/i;->d:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lcom/twitter/library/api/m;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    new-instance v0, Lcom/twitter/android/util/l;

    invoke-direct {v0, p1}, Lcom/twitter/android/util/l;-><init>(Lcom/twitter/android/util/f;)V

    invoke-virtual {v2, v0}, Lcom/twitter/library/api/m;->a(Lcom/twitter/internal/android/service/m;)Lcom/twitter/internal/android/service/a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    return-void
.end method

.method public a(Lcom/twitter/android/util/g;)V
    .locals 3

    new-instance v0, Lcom/twitter/android/util/n;

    iget-object v1, p0, Lcom/twitter/android/util/i;->d:Landroid/content/Context;

    const/4 v2, 0x5

    invoke-direct {v0, v1, p1, v2}, Lcom/twitter/android/util/n;-><init>(Landroid/content/Context;Lcom/twitter/android/util/g;I)V

    invoke-virtual {v0}, Lcom/twitter/android/util/n;->run()V

    return-void
.end method

.method public a()Z
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/util/i;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/util/i;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/util/i;->h()Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    :goto_1
    return v2

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1
.end method

.method public a(Z)[Ljava/lang/String;
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/util/i;->g()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/util/i;->l()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-array v0, v6, [Ljava/lang/String;

    const-string/jumbo v1, "bogus step representing SignUpActivity"

    aput-object v1, v0, v2

    const-string/jumbo v1, "follow_friends"

    aput-object v1, v0, v3

    const-string/jumbo v1, "nux_tag_invite"

    aput-object v1, v0, v4

    const-string/jumbo v1, "follow_recommendations"

    aput-object v1, v0, v5

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "bogus step representing SignUpActivity"

    aput-object v1, v0, v2

    const-string/jumbo v1, "phone_entry"

    aput-object v1, v0, v3

    const-string/jumbo v1, "follow_friends"

    aput-object v1, v0, v4

    const-string/jumbo v1, "nux_tag_invite"

    aput-object v1, v0, v5

    const-string/jumbo v1, "follow_recommendations"

    aput-object v1, v0, v6

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/util/i;->g()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/util/i;->l()Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, "follow_friends"

    aput-object v1, v0, v2

    const-string/jumbo v1, "nux_tag_invite"

    aput-object v1, v0, v3

    const-string/jumbo v1, "follow_recommendations"

    aput-object v1, v0, v4

    goto :goto_0

    :cond_4
    new-array v0, v6, [Ljava/lang/String;

    const-string/jumbo v1, "phone_entry"

    aput-object v1, v0, v2

    const-string/jumbo v1, "follow_friends"

    aput-object v1, v0, v3

    const-string/jumbo v1, "nux_tag_invite"

    aput-object v1, v0, v4

    const-string/jumbo v1, "follow_recommendations"

    aput-object v1, v0, v5

    goto :goto_0
.end method

.method public b()Z
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/util/i;->d:Landroid/content/Context;

    const-wide/16 v1, 0x0

    const-string/jumbo v3, "digits_android_intl_u2_1931"

    invoke-static {v0, v1, v2, v3}, Lju;->a(Landroid/content/Context;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "precheck_follows"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "precheck_fallback"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/android/util/i;->h()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/util/i;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const-string/jumbo v3, "digits_android_intl_u2_1931"

    invoke-static {v0, v1, v2, v3}, Lju;->b(Landroid/content/Context;JLjava/lang/String;)V

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/twitter/android/util/i;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/twitter/android/util/i;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public e()V
    .locals 5

    const/4 v4, 0x1

    invoke-direct {p0}, Lcom/twitter/android/util/i;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/util/i;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    sget-boolean v2, Lcom/twitter/android/util/i;->b:Z

    if-nez v2, :cond_0

    sget-object v2, Lcom/twitter/android/util/i;->c:Ljava/lang/String;

    if-nez v2, :cond_0

    sput-boolean v4, Lcom/twitter/android/util/i;->b:Z

    new-instance v2, Lcom/twitter/android/util/p;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/twitter/android/util/p;-><init>(Lcom/twitter/android/util/i;Lcom/twitter/android/util/j;)V

    new-array v3, v4, [Ljava/lang/Long;

    const/4 v4, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/android/util/p;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    :goto_0
    return-void

    :cond_1
    sput-boolean v4, Lcom/twitter/android/util/i;->b:Z

    invoke-virtual {p0}, Lcom/twitter/android/util/i;->n()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/util/i;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public f()Z
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    const/4 v1, 0x1

    const/4 v2, 0x0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v0, v3, :cond_0

    const-string/jumbo v0, "airplane_mode_on"

    :goto_0
    iget-object v3, p0, Lcom/twitter/android/util/i;->d:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3, v0, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    return v0

    :cond_0
    const-string/jumbo v0, "airplane_mode_on"

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method public g()Z
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/util/i;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Z
    .locals 1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->aa()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/util/i;->m()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/util/i;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "Welcome to Twitter! You have successfully added your phone."

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/util/i;->d:Landroid/content/Context;

    const v1, 0x7f0f0451    # com.twitter.android.R.string.sms_verification_text

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public j()Z
    .locals 2

    const-string/jumbo v0, "android_suppress_mt_1987"

    invoke-static {v0}, Lkk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "suppress"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public k()Z
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/util/i;->d:Landroid/content/Context;

    const-wide/16 v1, 0x0

    const-string/jumbo v3, "digits_android_intl_u2_1931"

    invoke-static {v0, v1, v2, v3}, Lju;->a(Landroid/content/Context;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "uncheck_follows"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "uncheck_fallback"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Z
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/util/i;->d:Landroid/content/Context;

    const-wide/16 v1, 0x0

    const-string/jumbo v3, "digits_android_intl_u2_1931"

    invoke-static {v0, v1, v2, v3}, Lju;->a(Landroid/content/Context;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "precheck_fallback"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "uncheck_fallback"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()Z
    .locals 6

    const/4 v5, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/util/i;->d:Landroid/content/Context;

    const-string/jumbo v3, "phone"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v3

    const/4 v4, 0x5

    if-ne v3, v4, :cond_1

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v3, v5, :cond_1

    invoke-virtual {v0, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const/16 v3, 0x1b8

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/util/i;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    sget-object v3, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    if-ne v0, v3, :cond_2

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method n()Ljava/lang/String;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/util/i;->d:Landroid/content/Context;

    const-string/jumbo v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/util/i;->a(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method o()Ljava/lang/String;
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    const/4 v6, 0x0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/util/i;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Profile;->CONTENT_URI:Landroid/net/Uri;

    const-string/jumbo v2, "data"

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/util/o;->a:[Ljava/lang/String;

    const-string/jumbo v3, "mimetype = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const-string/jumbo v5, "vnd.android.cursor.item/phone_v2"

    aput-object v5, v4, v6

    const-string/jumbo v5, "is_primary"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/util/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method p()Ljava/lang/String;
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/util/i;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    sget-object v4, Lcom/twitter/android/util/i;->a:Ljava/util/Set;

    iget-object v5, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/android/util/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public q()Z
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/util/i;->j()Z

    move-result v0

    return v0
.end method
