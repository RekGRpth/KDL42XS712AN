.class public Lcom/twitter/android/util/AppMetrics;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:J

.field private static b:Lcom/twitter/android/util/AppMetrics$LaunchType;

.field private static c:Landroid/net/Uri;

.field private static d:Z

.field private static e:Z

.field private static f:Lcom/twitter/android/client/c;

.field private static final g:[Ljava/lang/String;

.field private static final h:[Ljava/lang/String;

.field private static i:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/twitter/android/util/AppMetrics;->a:J

    sget-object v0, Lcom/twitter/android/util/AppMetrics$LaunchType;->a:Lcom/twitter/android/util/AppMetrics$LaunchType;

    sput-object v0, Lcom/twitter/android/util/AppMetrics;->b:Lcom/twitter/android/util/AppMetrics$LaunchType;

    const/4 v0, 0x0

    sput-object v0, Lcom/twitter/android/util/AppMetrics;->c:Landroid/net/Uri;

    sput-boolean v2, Lcom/twitter/android/util/AppMetrics;->d:Z

    sput-boolean v2, Lcom/twitter/android/util/AppMetrics;->e:Z

    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, "home:refresh"

    aput-object v1, v0, v2

    sput-object v0, Lcom/twitter/android/util/AppMetrics;->g:[Ljava/lang/String;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "home:first_tweet_api"

    aput-object v1, v0, v2

    const-string/jumbo v1, "home:first_tweet_cache"

    aput-object v1, v0, v3

    sput-object v0, Lcom/twitter/android/util/AppMetrics;->h:[Ljava/lang/String;

    sput v2, Lcom/twitter/android/util/AppMetrics;->i:I

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 5

    const/4 v1, 0x0

    sget-object v2, Lcom/twitter/android/util/AppMetrics;->g:[Ljava/lang/String;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    invoke-static {p0, v4}, Lcom/twitter/android/util/AppMetrics;->c(Landroid/content/Context;Ljava/lang/String;)Lcom/twitter/library/metrics/o;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/metrics/o;->k()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/twitter/android/util/AppMetrics;->h:[Ljava/lang/String;

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    invoke-static {p0, v4}, Lcom/twitter/android/util/AppMetrics;->b(Landroid/content/Context;Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    sput-boolean v1, Lcom/twitter/android/util/AppMetrics;->d:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/twitter/android/util/AppMetrics;->c:Landroid/net/Uri;

    return-void
.end method

.method public static a(Landroid/content/Context;JI)V
    .locals 2

    sget v0, Lcom/twitter/android/util/AppMetrics;->i:I

    or-int/2addr v0, p3

    sput v0, Lcom/twitter/android/util/AppMetrics;->i:I

    sget v0, Lcom/twitter/android/util/AppMetrics;->i:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    if-eqz p0, :cond_1

    const-string/jumbo v0, "profile:complete"

    invoke-static {p0, v0}, Lcom/twitter/android/util/AppMetrics;->c(Landroid/content/Context;Ljava/lang/String;)Lcom/twitter/library/metrics/o;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/metrics/o;->b(J)V

    invoke-virtual {v0}, Lcom/twitter/library/metrics/o;->j()V

    :goto_0
    const/4 v0, 0x0

    sput v0, Lcom/twitter/android/util/AppMetrics;->i:I

    :cond_0
    return-void

    :cond_1
    const-string/jumbo v0, "profile:complete"

    invoke-static {p0, v0}, Lcom/twitter/android/util/AppMetrics;->c(Landroid/content/Context;Ljava/lang/String;)Lcom/twitter/library/metrics/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/metrics/o;->k()V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;JLjava/lang/String;)V
    .locals 7

    sget-object v0, Lcom/twitter/android/util/AppMetrics;->b:Lcom/twitter/android/util/AppMetrics$LaunchType;

    sget-object v1, Lcom/twitter/android/util/AppMetrics$LaunchType;->a:Lcom/twitter/android/util/AppMetrics$LaunchType;

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v0, Lcom/twitter/android/util/AppMetrics;->b:Lcom/twitter/android/util/AppMetrics$LaunchType;

    sget-object v2, Lcom/twitter/android/util/AppMetrics$LaunchType;->b:Lcom/twitter/android/util/AppMetrics$LaunchType;

    if-ne v0, v2, :cond_4

    const-string/jumbo v0, "_cold"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz p0, :cond_5

    invoke-static {}, Lcom/twitter/android/util/AppMetrics;->a()Z

    move-result v1

    if-eqz v1, :cond_5

    sget-boolean v1, Lcom/twitter/android/util/AppMetrics;->e:Z

    if-eqz v1, :cond_5

    sget-boolean v1, Lcom/twitter/android/util/AppMetrics;->d:Z

    if-eqz v1, :cond_5

    invoke-static {p0, v0}, Lcom/twitter/android/util/AppMetrics;->c(Landroid/content/Context;Ljava/lang/String;)Lcom/twitter/library/metrics/o;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/metrics/o;->b(J)V

    invoke-virtual {v0}, Lcom/twitter/library/metrics/o;->j()V

    :goto_2
    const-string/jumbo v0, "home:first_tweet_api"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "home:first_tweet_cache"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v0, Lcom/twitter/android/util/AppMetrics;->b:Lcom/twitter/android/util/AppMetrics$LaunchType;

    sget-object v2, Lcom/twitter/android/util/AppMetrics$LaunchType;->b:Lcom/twitter/android/util/AppMetrics$LaunchType;

    if-ne v0, v2, :cond_6

    const-string/jumbo v0, "_firstui"

    :goto_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":full"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sget-wide v4, Lcom/twitter/android/util/AppMetrics;->a:J

    sub-long/2addr v2, v4

    if-eqz p0, :cond_3

    invoke-static {}, Lcom/twitter/android/util/AppMetrics;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-boolean v0, Lcom/twitter/android/util/AppMetrics;->e:Z

    if-eqz v0, :cond_3

    sget-boolean v0, Lcom/twitter/android/util/AppMetrics;->d:Z

    if-eqz v0, :cond_3

    sget-object v0, Lcom/twitter/android/util/AppMetrics;->f:Lcom/twitter/android/client/c;

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->a()Lcom/twitter/library/metrics/h;

    move-result-object v6

    new-instance v0, Lcom/twitter/android/util/b;

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/util/b;-><init>(Ljava/lang/String;JJ)V

    invoke-virtual {v6, v0}, Lcom/twitter/library/metrics/h;->a(Lcom/twitter/library/metrics/e;)V

    :cond_3
    const-string/jumbo v0, "home:first_tweet_api"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/twitter/android/util/AppMetrics;->a(Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_4
    const-string/jumbo v0, "_warm"

    goto/16 :goto_1

    :cond_5
    invoke-static {p0, v0}, Lcom/twitter/android/util/AppMetrics;->c(Landroid/content/Context;Ljava/lang/String;)Lcom/twitter/library/metrics/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/metrics/o;->k()V

    goto :goto_2

    :cond_6
    const-string/jumbo v0, "_resume"

    goto :goto_3
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    const/4 v1, 0x1

    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/util/AppMetrics;->f:Lcom/twitter/android/client/c;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v0

    const-string/jumbo v2, "android.intent.category.LAUNCHER"

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "android.intent.action.MAIN"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/util/AppMetrics;->e:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/twitter/android/util/AppMetrics;->c:Landroid/net/Uri;

    sput-boolean v1, Lcom/twitter/android/util/AppMetrics;->d:Z

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sput-wide v0, Lcom/twitter/android/util/AppMetrics;->a:J

    sget-object v0, Lcom/twitter/android/util/AppMetrics;->b:Lcom/twitter/android/util/AppMetrics$LaunchType;

    sget-object v1, Lcom/twitter/android/util/AppMetrics$LaunchType;->a:Lcom/twitter/android/util/AppMetrics$LaunchType;

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/twitter/android/util/AppMetrics$LaunchType;->b:Lcom/twitter/android/util/AppMetrics$LaunchType;

    :goto_1
    sput-object v0, Lcom/twitter/android/util/AppMetrics;->b:Lcom/twitter/android/util/AppMetrics$LaunchType;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/twitter/android/util/AppMetrics$LaunchType;->c:Lcom/twitter/android/util/AppMetrics$LaunchType;

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    sget-object v0, Lcom/twitter/android/util/AppMetrics;->b:Lcom/twitter/android/util/AppMetrics$LaunchType;

    sget-object v1, Lcom/twitter/android/util/AppMetrics$LaunchType;->a:Lcom/twitter/android/util/AppMetrics$LaunchType;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v0, Lcom/twitter/android/util/AppMetrics;->b:Lcom/twitter/android/util/AppMetrics$LaunchType;

    sget-object v2, Lcom/twitter/android/util/AppMetrics$LaunchType;->b:Lcom/twitter/android/util/AppMetrics$LaunchType;

    if-ne v0, v2, :cond_1

    const-string/jumbo v0, "_cold"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/android/util/AppMetrics;->c(Landroid/content/Context;Ljava/lang/String;)Lcom/twitter/library/metrics/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/metrics/o;->i()V

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "_warm"

    goto :goto_1
.end method

.method public static a(Landroid/net/Uri;)V
    .locals 1

    sget-object v0, Lcom/twitter/android/util/AppMetrics;->c:Landroid/net/Uri;

    if-eqz v0, :cond_0

    if-eqz p0, :cond_0

    :goto_0
    return-void

    :cond_0
    sput-object p0, Lcom/twitter/android/util/AppMetrics;->c:Landroid/net/Uri;

    goto :goto_0
.end method

.method private static a()Z
    .locals 2

    sget-object v0, Lcom/twitter/android/util/AppMetrics;->c:Landroid/net/Uri;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/android/util/AppMetrics;->c:Landroid/net/Uri;

    sget-object v1, Lcom/twitter/android/MainActivity;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 1

    const-string/jumbo v0, "profile:complete"

    invoke-static {p0, v0}, Lcom/twitter/android/util/AppMetrics;->c(Landroid/content/Context;Ljava/lang/String;)Lcom/twitter/library/metrics/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/metrics/o;->k()V

    const/4 v0, 0x0

    sput v0, Lcom/twitter/android/util/AppMetrics;->i:I

    const-string/jumbo v0, "profile:complete"

    invoke-static {p0, v0}, Lcom/twitter/android/util/AppMetrics;->c(Landroid/content/Context;Ljava/lang/String;)Lcom/twitter/library/metrics/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/metrics/o;->i()V

    return-void
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    sget-object v0, Lcom/twitter/android/util/AppMetrics;->b:Lcom/twitter/android/util/AppMetrics$LaunchType;

    sget-object v1, Lcom/twitter/android/util/AppMetrics$LaunchType;->a:Lcom/twitter/android/util/AppMetrics$LaunchType;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v0, Lcom/twitter/android/util/AppMetrics;->b:Lcom/twitter/android/util/AppMetrics$LaunchType;

    sget-object v2, Lcom/twitter/android/util/AppMetrics$LaunchType;->b:Lcom/twitter/android/util/AppMetrics$LaunchType;

    if-ne v0, v2, :cond_1

    const-string/jumbo v0, "_cold"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/android/util/AppMetrics;->c(Landroid/content/Context;Ljava/lang/String;)Lcom/twitter/library/metrics/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/metrics/o;->k()V

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "_warm"

    goto :goto_1
.end method

.method private static c(Landroid/content/Context;Ljava/lang/String;)Lcom/twitter/library/metrics/o;
    .locals 1

    sget-object v0, Lcom/twitter/android/util/AppMetrics;->f:Lcom/twitter/android/client/c;

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/util/AppMetrics;->f:Lcom/twitter/android/client/c;

    :cond_0
    sget-object v0, Lcom/twitter/android/util/AppMetrics;->f:Lcom/twitter/android/client/c;

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->a()Lcom/twitter/library/metrics/h;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/twitter/library/metrics/o;->a(Ljava/lang/String;Lcom/twitter/library/metrics/h;)Lcom/twitter/library/metrics/o;

    move-result-object v0

    return-object v0
.end method
