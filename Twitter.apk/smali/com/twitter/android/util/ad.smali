.class public abstract Lcom/twitter/android/util/ad;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Landroid/os/Handler;

.field private b:J

.field private volatile c:Z


# direct methods
.method public constructor <init>(J)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/util/ad;->a:Landroid/os/Handler;

    iput-wide p1, p0, Lcom/twitter/android/util/ad;->b:J

    return-void
.end method


# virtual methods
.method public abstract a()Z
.end method

.method public b()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/util/ad;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/android/util/ad;->a:Landroid/os/Handler;

    iget-wide v1, p0, Lcom/twitter/android/util/ad;->b:J

    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/util/ad;->c:Z

    return-void
.end method

.method public c()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/util/ad;->c:Z

    iget-object v0, p0, Lcom/twitter/android/util/ad;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    return-void
.end method

.method public run()V
    .locals 3

    iget-boolean v0, p0, Lcom/twitter/android/util/ad;->c:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/util/ad;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/util/ad;->a:Landroid/os/Handler;

    iget-wide v1, p0, Lcom/twitter/android/util/ad;->b:J

    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method
