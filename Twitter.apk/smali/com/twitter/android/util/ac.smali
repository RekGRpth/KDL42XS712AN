.class Lcom/twitter/android/util/ac;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/util/aa;

.field private b:I


# direct methods
.method constructor <init>(Lcom/twitter/android/util/aa;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/util/ac;->a:Lcom/twitter/android/util/aa;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/util/ac;->b:I

    return-void
.end method

.method private a()Z
    .locals 2

    iget v0, p0, Lcom/twitter/android/util/ac;->b:I

    iget-object v1, p0, Lcom/twitter/android/util/ac;->a:Lcom/twitter/android/util/aa;

    invoke-static {v1}, Lcom/twitter/android/util/aa;->a(Lcom/twitter/android/util/aa;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Boolean;)Landroid/graphics/Bitmap;
    .locals 10

    const/4 v6, 0x0

    const/4 v9, 0x0

    iget-object v0, p0, Lcom/twitter/android/util/ac;->a:Lcom/twitter/android/util/aa;

    invoke-static {v0}, Lcom/twitter/android/util/aa;->b(Lcom/twitter/android/util/aa;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    invoke-direct {p0}, Lcom/twitter/android/util/ac;->a()Z

    move-result v0

    if-nez v0, :cond_a

    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/util/ac;->a:Lcom/twitter/android/util/aa;

    invoke-static {v0}, Lcom/twitter/android/util/aa;->c(Lcom/twitter/android/util/aa;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/util/ac;->a:Lcom/twitter/android/util/aa;

    invoke-static {v1}, Lcom/twitter/android/util/aa;->b(Lcom/twitter/android/util/aa;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/twitter/library/util/Util;->c(Landroid/content/Context;J)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/android/util/ac;->a:Lcom/twitter/android/util/aa;

    invoke-static {v2}, Lcom/twitter/android/util/aa;->b(Lcom/twitter/android/util/aa;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/twitter/library/util/Util;->d(Landroid/content/Context;J)Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v2}, Lkw;->a(Ljava/io/File;)Lkw;

    move-result-object v0

    invoke-virtual {v0}, Lkw;->b()Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "local_perch_blur_disabled"

    invoke-interface {v8, v1, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->apply()V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "local_perch_blur_disabled"

    invoke-interface {v8, v0, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->apply()V

    move-object v0, v6

    goto :goto_0

    :cond_1
    move-object v7, v6

    :goto_1
    if-eqz v1, :cond_c

    :try_start_2
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_c

    if-eqz v2, :cond_c

    invoke-direct {p0}, Lcom/twitter/android/util/ac;->a()Z

    move-result v0

    if-nez v0, :cond_c

    const-string/jumbo v0, "local_perch_blur_disabled"

    const/4 v3, 0x1

    invoke-interface {v8, v0, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v0, p0, Lcom/twitter/android/util/ac;->a:Lcom/twitter/android/util/aa;

    invoke-static {v0}, Lcom/twitter/android/util/aa;->b(Lcom/twitter/android/util/aa;)Landroid/content/Context;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/util/ac;->a:Lcom/twitter/android/util/aa;

    invoke-static {v3}, Lcom/twitter/android/util/aa;->d(Lcom/twitter/android/util/aa;)I

    move-result v3

    iget-object v4, p0, Lcom/twitter/android/util/ac;->a:Lcom/twitter/android/util/aa;

    invoke-static {v4}, Lcom/twitter/android/util/aa;->e(Lcom/twitter/android/util/aa;)I

    move-result v4

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/twitter/media/filters/c;->a(Landroid/content/Context;Ljava/io/File;Ljava/io/File;IIZ)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-static {v2}, Lkw;->a(Ljava/io/File;)Lkw;

    move-result-object v0

    invoke-virtual {v0}, Lkw;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_2
    if-nez v0, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    :cond_2
    :goto_3
    if-eqz v7, :cond_3

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v7}, Ljava/io/File;->delete()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_3
    const-string/jumbo v1, "local_perch_blur_disabled"

    invoke-interface {v8, v1, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    :cond_4
    :try_start_3
    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    if-nez v0, :cond_5

    move-object v1, v6

    :goto_4
    if-nez v1, :cond_6

    const-string/jumbo v0, "local_perch_blur_disabled"

    invoke-interface {v8, v0, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->apply()V

    move-object v0, v6

    goto :goto_0

    :cond_5
    :try_start_4
    iget-object v0, v0, Lcom/twitter/library/api/TwitterUser;->profileHeaderImageUrl:Ljava/lang/String;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Lcom/twitter/library/util/Util;->a(Ljava/lang/String;F)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lcom/twitter/android/util/ac;->a:Lcom/twitter/android/util/aa;

    invoke-static {v0}, Lcom/twitter/android/util/aa;->b(Lcom/twitter/android/util/aa;)Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x3

    invoke-static {v0, v1, v2}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/io/File;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v2

    if-nez v2, :cond_7

    const-string/jumbo v0, "local_perch_blur_disabled"

    invoke-interface {v8, v0, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->apply()V

    move-object v0, v6

    goto/16 :goto_0

    :cond_7
    :try_start_5
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-static {v2}, Lkw;->a(Ljava/io/File;)Lkw;

    move-result-object v0

    invoke-virtual {v0}, Lkw;->b()Landroid/graphics/Bitmap;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_8

    const-string/jumbo v1, "local_perch_blur_disabled"

    invoke-interface {v8, v1, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_0

    :cond_8
    const/4 v0, 0x0

    :try_start_6
    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result v0

    if-eqz v0, :cond_9

    const-string/jumbo v0, "local_perch_blur_disabled"

    invoke-interface {v8, v0, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->apply()V

    move-object v0, v6

    goto/16 :goto_0

    :cond_9
    :try_start_7
    invoke-static {v1}, Lcom/twitter/library/util/Util;->b(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v1

    if-eqz v1, :cond_e

    invoke-direct {p0}, Lcom/twitter/android/util/ac;->a()Z

    move-result v0

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/twitter/android/util/ac;->a:Lcom/twitter/android/util/aa;

    invoke-static {v0}, Lcom/twitter/android/util/aa;->b(Lcom/twitter/android/util/aa;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->j(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    new-instance v3, Lcom/twitter/library/service/m;

    invoke-direct {v3, v0}, Lcom/twitter/library/service/m;-><init>(Ljava/io/File;)V

    new-instance v4, Lcom/twitter/library/network/d;

    iget-object v5, p0, Lcom/twitter/android/util/ac;->a:Lcom/twitter/android/util/aa;

    invoke-static {v5}, Lcom/twitter/android/util/aa;->b(Lcom/twitter/android/util/aa;)Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5, v1}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/net/URI;)V

    invoke-virtual {v4, v3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->j()Z
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result v1

    if-eqz v1, :cond_d

    move-object v7, v0

    move-object v1, v0

    goto/16 :goto_1

    :catch_0
    move-exception v0

    const-string/jumbo v0, "local_perch_blur_disabled"

    invoke-interface {v8, v0, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_a
    move-object v0, v6

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    const-string/jumbo v1, "local_perch_blur_disabled"

    invoke-interface {v8, v1, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->apply()V

    throw v0

    :cond_b
    move-object v0, v6

    goto/16 :goto_2

    :cond_c
    move-object v0, v6

    goto/16 :goto_3

    :cond_d
    move-object v2, v6

    move-object v7, v0

    move-object v1, v6

    goto/16 :goto_1

    :cond_e
    move-object v2, v6

    move-object v7, v6

    move-object v1, v6

    goto/16 :goto_1
.end method

.method protected a(Landroid/graphics/Bitmap;)V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/util/ac;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/util/ac;->a:Lcom/twitter/android/util/aa;

    invoke-static {v0}, Lcom/twitter/android/util/aa;->f(Lcom/twitter/android/util/aa;)Lcom/twitter/android/util/ab;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/util/ac;->a:Lcom/twitter/android/util/aa;

    invoke-static {v0}, Lcom/twitter/android/util/aa;->f(Lcom/twitter/android/util/aa;)Lcom/twitter/android/util/ab;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/twitter/android/util/ab;->a(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/twitter/android/util/ac;->a([Ljava/lang/Boolean;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/twitter/android/util/ac;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/util/ac;->a:Lcom/twitter/android/util/aa;

    invoke-static {v0}, Lcom/twitter/android/util/aa;->a(Lcom/twitter/android/util/aa;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/util/ac;->b:I

    return-void
.end method
