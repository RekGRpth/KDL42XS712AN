.class Lcom/twitter/android/util/n;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/android/util/g;

.field private final c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/android/util/g;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/util/n;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/twitter/android/util/n;->b:Lcom/twitter/android/util/g;

    iput p3, p0, Lcom/twitter/android/util/n;->c:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/util/n;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/util/n;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/library/client/w;->a(Landroid/content/Context;)Lcom/twitter/library/client/w;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/api/d;

    iget-object v3, p0, Lcom/twitter/android/util/n;->a:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lcom/twitter/library/api/d;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    new-instance v0, Lcom/twitter/android/util/m;

    iget-object v3, p0, Lcom/twitter/android/util/n;->a:Landroid/content/Context;

    iget v4, p0, Lcom/twitter/android/util/n;->c:I

    iget-object v5, p0, Lcom/twitter/android/util/n;->b:Lcom/twitter/android/util/g;

    invoke-direct {v0, v3, v4, v5}, Lcom/twitter/android/util/m;-><init>(Landroid/content/Context;ILcom/twitter/android/util/g;)V

    invoke-virtual {v2, v0}, Lcom/twitter/library/api/d;->a(Lcom/twitter/internal/android/service/m;)Lcom/twitter/internal/android/service/a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    return-void
.end method
