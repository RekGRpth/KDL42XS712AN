.class public Lcom/twitter/android/util/aa;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Ljava/util/concurrent/atomic/AtomicInteger;

.field private b:Landroid/content/Context;

.field private c:Lcom/twitter/library/client/aa;

.field private d:Lcom/twitter/android/util/ab;

.field private e:I

.field private f:I


# direct methods
.method static synthetic a(Lcom/twitter/android/util/aa;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/util/aa;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/util/aa;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/util/aa;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/util/aa;)Lcom/twitter/library/client/aa;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/util/aa;->c:Lcom/twitter/library/client/aa;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/util/aa;)I
    .locals 1

    iget v0, p0, Lcom/twitter/android/util/aa;->e:I

    return v0
.end method

.method static synthetic e(Lcom/twitter/android/util/aa;)I
    .locals 1

    iget v0, p0, Lcom/twitter/android/util/aa;->f:I

    return v0
.end method

.method static synthetic f(Lcom/twitter/android/util/aa;)Lcom/twitter/android/util/ab;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/util/aa;->d:Lcom/twitter/android/util/ab;

    return-object v0
.end method


# virtual methods
.method public a(Z)V
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/android/util/aa;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/twitter/android/util/ac;

    invoke-direct {v0, p0}, Lcom/twitter/android/util/ac;-><init>(Lcom/twitter/android/util/aa;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Boolean;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/android/util/ac;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/util/aa;->d:Lcom/twitter/android/util/ab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/util/aa;->d:Lcom/twitter/android/util/ab;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/twitter/android/util/ab;->a(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public a()Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/util/aa;->b:Landroid/content/Context;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v3, "perch_blur_enabled"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    const-string/jumbo v4, "local_perch_blur_disabled"

    invoke-interface {v2, v4, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v3, :cond_0

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/util/aa;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/twitter/media/filters/c;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
