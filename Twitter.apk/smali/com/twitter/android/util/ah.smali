.class public Lcom/twitter/android/util/ah;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Landroid/content/Context;)I
    .locals 3

    const/4 v0, 0x0

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/android/util/ah;->a(Lcom/twitter/library/api/TwitterUser;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Lcom/twitter/library/client/f;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, p0, v1}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const-string/jumbo v1, "vit_notification_filter_type"

    invoke-virtual {v2, v1, v0}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;I)I

    move-result v0

    :cond_0
    return v0
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 2

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/f;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v1, "vit_notification_filter_type"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;I)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V

    return-void
.end method

.method public static a(Lcom/twitter/library/api/TwitterUser;)Z
    .locals 1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/api/TwitterUser;->verified:Z

    if-nez v0, :cond_1

    :cond_0
    invoke-static {}, Lcom/twitter/library/featureswitch/a;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
