.class Lcom/twitter/android/util/p;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/util/i;


# direct methods
.method private constructor <init>(Lcom/twitter/android/util/i;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/util/p;->a:Lcom/twitter/android/util/i;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/util/i;Lcom/twitter/android/util/j;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/util/p;-><init>(Lcom/twitter/android/util/i;)V

    return-void
.end method

.method private a(JLjava/lang/String;)V
    .locals 6

    const/4 v5, 0x2

    if-nez p3, :cond_0

    const-string/jumbo v0, "unavailable"

    :goto_0
    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v1, p1, p2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    invoke-virtual {v1, v5}, Lcom/twitter/library/scribe/ScribeLog;->c(I)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "app"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, ""

    aput-object v4, v2, v3

    const-string/jumbo v3, "phone_number"

    aput-object v3, v2, v5

    const/4 v3, 0x3

    aput-object p3, v2, v3

    const/4 v3, 0x4

    const-string/jumbo v4, ""

    aput-object v4, v2, v3

    const/4 v3, 0x5

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/util/p;->a:Lcom/twitter/android/util/i;

    invoke-static {v1}, Lcom/twitter/android/util/i;->a(Lcom/twitter/android/util/i;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void

    :cond_0
    const-string/jumbo v0, "available"

    goto :goto_0
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Long;)Ljava/lang/String;
    .locals 5

    const/4 v1, 0x0

    array-length v0, p1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v0, p0, Lcom/twitter/android/util/p;->a:Lcom/twitter/android/util/i;

    invoke-virtual {v0}, Lcom/twitter/android/util/i;->n()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string/jumbo v1, "source_telephonymanager"

    invoke-direct {p0, v2, v3, v1}, Lcom/twitter/android/util/p;->a(JLjava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/util/p;->a:Lcom/twitter/android/util/i;

    invoke-virtual {v0}, Lcom/twitter/android/util/i;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string/jumbo v1, "source_accountmanager"

    invoke-direct {p0, v2, v3, v1}, Lcom/twitter/android/util/p;->a(JLjava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/util/p;->a:Lcom/twitter/android/util/i;

    invoke-virtual {v0}, Lcom/twitter/android/util/i;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string/jumbo v1, "source_google_contact"

    invoke-direct {p0, v2, v3, v1}, Lcom/twitter/android/util/p;->a(JLjava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v2, v3, v1}, Lcom/twitter/android/util/p;->a(JLjava/lang/String;)V

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method protected a(Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/twitter/android/util/i;->b(Ljava/lang/String;)Ljava/lang/String;

    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/twitter/android/util/p;->a([Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/twitter/android/util/p;->a(Ljava/lang/String;)V

    return-void
.end method
