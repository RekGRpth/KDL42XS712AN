.class final Lcom/twitter/android/util/b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/metrics/e;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:J

.field final synthetic c:J


# direct methods
.method constructor <init>(Ljava/lang/String;JJ)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/util/b;->a:Ljava/lang/String;

    iput-wide p2, p0, Lcom/twitter/android/util/b;->b:J

    iput-wide p4, p0, Lcom/twitter/android/util/b;->c:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/library/scribe/ScribeLog;
    .locals 4

    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    iget-wide v1, p0, Lcom/twitter/android/util/b;->c:J

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    iget-wide v1, p0, Lcom/twitter/android/util/b;->b:J

    iget-object v3, p0, Lcom/twitter/android/util/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->a(JLjava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Recorded perf event: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/util/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", duration: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/util/b;->b:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()V
    .locals 0

    return-void
.end method

.method public d()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "AppMetric"

    return-object v0
.end method
