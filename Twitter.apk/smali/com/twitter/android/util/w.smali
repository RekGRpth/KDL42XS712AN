.class public Lcom/twitter/android/util/w;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/util/v;


# instance fields
.field private a:J

.field private b:J

.field private c:J

.field private d:I

.field private e:J

.field private f:Ljava/lang/String;

.field private g:Z

.field private h:Landroid/content/Context;

.field private i:Lcom/twitter/android/client/c;

.field private j:Lcom/twitter/library/client/aa;


# direct methods
.method public constructor <init>(Landroid/content/Context;IJLjava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/util/w;->h:Landroid/content/Context;

    iget-object v0, p0, Lcom/twitter/android/util/w;->h:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/util/w;->i:Lcom/twitter/android/client/c;

    iget-object v0, p0, Lcom/twitter/android/util/w;->h:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/util/w;->j:Lcom/twitter/library/client/aa;

    invoke-virtual {p0, p2, p3, p4, p5}, Lcom/twitter/android/util/w;->a(IJLjava/lang/String;)V

    return-void
.end method

.method public static a(I)I
    .locals 1

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x4

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x17
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private b(J)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/util/w;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/util/w;->h:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/util/w;->f:Ljava/lang/String;

    invoke-static {v0, v1, p1, p2}, Lcom/twitter/library/client/i;->b(Landroid/content/Context;Ljava/lang/String;J)Z

    :cond_0
    return-void
.end method

.method private c(J)V
    .locals 6

    iput-wide p1, p0, Lcom/twitter/android/util/w;->a:J

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/util/w;->b(J)V

    iget-object v0, p0, Lcom/twitter/android/util/w;->i:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/util/w;->j:Lcom/twitter/library/client/aa;

    iget-wide v2, p0, Lcom/twitter/android/util/w;->e:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/client/aa;->a(J)Lcom/twitter/library/client/Session;

    move-result-object v1

    iget v2, p0, Lcom/twitter/android/util/w;->d:I

    iget-boolean v5, p0, Lcom/twitter/android/util/w;->g:Z

    move-wide v3, p1

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;IJZ)V

    return-void
.end method

.method private d()V
    .locals 6

    iget-wide v0, p0, Lcom/twitter/android/util/w;->b:J

    iput-wide v0, p0, Lcom/twitter/android/util/w;->c:J

    iget-object v0, p0, Lcom/twitter/android/util/w;->i:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/util/w;->j:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/util/w;->c:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->c(Lcom/twitter/library/client/Session;J)Ljava/lang/String;

    return-void
.end method

.method private e()J
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/util/w;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/util/w;->h:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/util/w;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/library/client/i;->a(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/android/util/w;->a:J

    return-wide v0
.end method

.method public a(IJLjava/lang/String;)V
    .locals 5

    const-wide/16 v1, 0x0

    iput p1, p0, Lcom/twitter/android/util/w;->d:I

    iput-wide p2, p0, Lcom/twitter/android/util/w;->e:J

    iput-object p4, p0, Lcom/twitter/android/util/w;->f:Ljava/lang/String;

    iput-wide v1, p0, Lcom/twitter/android/util/w;->c:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/util/w;->g:Z

    cmp-long v0, p2, v1

    if-lez v0, :cond_0

    if-eqz p4, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/util/w;->e()J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    cmp-long v4, v2, v0

    if-lez v4, :cond_1

    iget-object v2, p0, Lcom/twitter/android/util/w;->h:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/android/util/w;->f:Ljava/lang/String;

    invoke-static {v2, v3, v0, v1}, Lcom/twitter/library/client/i;->a(Landroid/content/Context;Ljava/lang/String;J)V

    :goto_0
    iput-wide v0, p0, Lcom/twitter/android/util/w;->a:J

    iput-wide v0, p0, Lcom/twitter/android/util/w;->b:J

    :cond_0
    return-void

    :cond_1
    move-wide v0, v2

    goto :goto_0
.end method

.method public a(J)V
    .locals 2

    iget-wide v0, p0, Lcom/twitter/android/util/w;->b:J

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/util/w;->b:J

    return-void
.end method

.method public a(JJ)V
    .locals 2

    iget-wide v0, p0, Lcom/twitter/android/util/w;->e:J

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    iput-wide p3, p0, Lcom/twitter/android/util/w;->c:J

    iget-wide v0, p0, Lcom/twitter/android/util/w;->a:J

    cmp-long v0, p3, v0

    if-lez v0, :cond_0

    invoke-virtual {p0, p3, p4}, Lcom/twitter/android/util/w;->a(J)V

    invoke-direct {p0, p3, p4}, Lcom/twitter/android/util/w;->c(J)V

    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/util/w;->g:Z

    return-void
.end method

.method public c()V
    .locals 4

    iget-wide v0, p0, Lcom/twitter/android/util/w;->b:J

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/util/w;->c(J)V

    iget-wide v0, p0, Lcom/twitter/android/util/w;->b:J

    iget-wide v2, p0, Lcom/twitter/android/util/w;->c:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/util/w;->d()V

    :cond_0
    return-void
.end method
