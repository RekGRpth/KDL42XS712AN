.class public Lcom/twitter/android/util/u;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Lcom/twitter/android/util/u;


# instance fields
.field private final b:Landroid/media/AudioManager;

.field private c:Landroid/media/SoundPool;

.field private d:[I

.field private e:I


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string/jumbo v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/twitter/android/util/u;->b:Landroid/media/AudioManager;

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/util/u;->e:I

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/twitter/android/util/u;
    .locals 4

    const-class v1, Lcom/twitter/android/util/u;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget-object v2, Lcom/twitter/android/util/u;->a:Lcom/twitter/android/util/u;

    if-nez v2, :cond_0

    new-instance v2, Lcom/twitter/android/util/u;

    invoke-direct {v2, v0}, Lcom/twitter/android/util/u;-><init>(Landroid/content/Context;)V

    sput-object v2, Lcom/twitter/android/util/u;->a:Lcom/twitter/android/util/u;

    :cond_0
    sget-object v2, Lcom/twitter/android/util/u;->a:Lcom/twitter/android/util/u;

    iget v3, v2, Lcom/twitter/android/util/u;->e:I

    if-nez v3, :cond_1

    invoke-direct {v2, v0}, Lcom/twitter/android/util/u;->b(Landroid/content/Context;)V

    :cond_1
    iget v0, v2, Lcom/twitter/android/util/u;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v2, Lcom/twitter/android/util/u;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v2

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private b(Landroid/content/Context;)V
    .locals 6

    const/4 v5, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    new-instance v0, Landroid/media/SoundPool;

    const/4 v1, 0x5

    invoke-direct {v0, v5, v1, v3}, Landroid/media/SoundPool;-><init>(III)V

    const/4 v1, 0x4

    new-array v1, v1, [I

    const v2, 0x7f070006    # com.twitter.android.R.raw.psst1

    invoke-virtual {v0, p1, v2, v4}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    aput v2, v1, v3

    const v2, 0x7f070007    # com.twitter.android.R.raw.psst2

    invoke-virtual {v0, p1, v2, v4}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    aput v2, v1, v4

    const v2, 0x7f070005    # com.twitter.android.R.raw.pop

    invoke-virtual {v0, p1, v2, v4}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    aput v2, v1, v5

    const/4 v2, 0x3

    const v3, 0x7f070008    # com.twitter.android.R.raw.tick

    invoke-virtual {v0, p1, v3, v4}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v3

    aput v3, v1, v2

    iput-object v1, p0, Lcom/twitter/android/util/u;->d:[I

    iput-object v0, p0, Lcom/twitter/android/util/u;->c:Landroid/media/SoundPool;

    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 7

    const/4 v4, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    iget-object v0, p0, Lcom/twitter/android/util/u;->c:Landroid/media/SoundPool;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/util/u;->b:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/util/u;->b:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/util/u;->c:Landroid/media/SoundPool;

    iget-object v1, p0, Lcom/twitter/android/util/u;->d:[I

    aget v1, v1, p1

    move v3, v2

    move v5, v4

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    move-result v4

    :cond_1
    return v4
.end method

.method public a()V
    .locals 6

    const/4 v5, 0x0

    iget v0, p0, Lcom/twitter/android/util/u;->e:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/twitter/android/util/u;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/twitter/android/util/u;->e:I

    :cond_0
    iget v0, p0, Lcom/twitter/android/util/u;->e:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/util/u;->c:Landroid/media/SoundPool;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/twitter/android/util/u;->d:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget v3, v1, v0

    iget-object v4, p0, Lcom/twitter/android/util/u;->c:Landroid/media/SoundPool;

    invoke-virtual {v4, v3}, Landroid/media/SoundPool;->unload(I)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iput-object v5, p0, Lcom/twitter/android/util/u;->c:Landroid/media/SoundPool;

    iput-object v5, p0, Lcom/twitter/android/util/u;->d:[I

    :cond_2
    return-void
.end method
