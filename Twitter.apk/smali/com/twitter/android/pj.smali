.class Lcom/twitter/android/pj;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field final synthetic a:Landroid/graphics/Rect;

.field final synthetic b:I

.field final synthetic c:Landroid/view/View;

.field final synthetic d:Lcom/twitter/android/PostActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/PostActivity;Landroid/graphics/Rect;ILandroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/pj;->d:Lcom/twitter/android/PostActivity;

    iput-object p2, p0, Lcom/twitter/android/pj;->a:Landroid/graphics/Rect;

    iput p3, p0, Lcom/twitter/android/pj;->b:I

    iput-object p4, p0, Lcom/twitter/android/pj;->c:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/pj;->d:Lcom/twitter/android/PostActivity;

    invoke-static {v0}, Lcom/twitter/android/PostActivity;->H(Lcom/twitter/android/PostActivity;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/pj;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->getHitRect(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/twitter/android/pj;->a:Landroid/graphics/Rect;

    iget v1, p0, Lcom/twitter/android/pj;->b:I

    neg-int v1, v1

    iget v2, p0, Lcom/twitter/android/pj;->b:I

    neg-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->inset(II)V

    iget-object v0, p0, Lcom/twitter/android/pj;->c:Landroid/view/View;

    new-instance v1, Landroid/view/TouchDelegate;

    iget-object v2, p0, Lcom/twitter/android/pj;->a:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/twitter/android/pj;->d:Lcom/twitter/android/PostActivity;

    invoke-static {v3}, Lcom/twitter/android/PostActivity;->H(Lcom/twitter/android/PostActivity;)Landroid/widget/Button;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    return-void
.end method
