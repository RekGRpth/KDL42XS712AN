.class Lcom/twitter/android/wp;
.super Lcom/twitter/library/service/a;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/TweetActivity;


# direct methods
.method private constructor <init>(Lcom/twitter/android/TweetActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/wp;->a:Lcom/twitter/android/TweetActivity;

    invoke-direct {p0}, Lcom/twitter/library/service/a;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/TweetActivity;Lcom/twitter/android/vx;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/wp;-><init>(Lcom/twitter/android/TweetActivity;)V

    return-void
.end method

.method private a(Lcom/twitter/library/service/b;Lcom/twitter/internal/android/service/k;)V
    .locals 2

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->r()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/wp;->a:Lcom/twitter/android/TweetActivity;

    iget-object v1, p1, Lcom/twitter/library/service/b;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/android/TweetActivity;->a(Lcom/twitter/android/TweetActivity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/wp;->a:Lcom/twitter/android/TweetActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/TweetActivity;->d(Lcom/twitter/android/TweetActivity;Z)Z

    iget-object v0, p0, Lcom/twitter/android/wp;->a:Lcom/twitter/android/TweetActivity;

    iget-object v0, v0, Lcom/twitter/android/TweetActivity;->d:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/wp;->a:Lcom/twitter/android/TweetActivity;

    iget-object v0, v0, Lcom/twitter/android/TweetActivity;->d:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->l()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/wp;->a:Lcom/twitter/android/TweetActivity;

    iget-object v0, v0, Lcom/twitter/android/TweetActivity;->f:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/wp;->a:Lcom/twitter/android/TweetActivity;

    iget-object v1, v0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/android/TweetFragment;

    invoke-virtual {p2}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/TweetFragment;->f(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/internal/android/service/a;)V
    .locals 0

    check-cast p1, Lcom/twitter/library/service/b;

    invoke-virtual {p0, p1}, Lcom/twitter/android/wp;->a(Lcom/twitter/library/service/b;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/b;)V
    .locals 1

    instance-of v0, p1, Lcom/twitter/library/api/upload/ad;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/wp;->a(Lcom/twitter/library/service/b;Lcom/twitter/internal/android/service/k;)V

    :cond_0
    return-void
.end method
