.class public Lcom/twitter/android/lx;
.super Landroid/support/v4/widget/CursorAdapter;
.source "Twttr"


# static fields
.field public static final a:[Ljava/lang/String;

.field private static b:Ljava/util/HashMap;

.field private static c:Ljava/util/HashMap;


# instance fields
.field private final d:Lcom/twitter/android/client/c;

.field private final e:Ljava/util/ArrayList;

.field private final f:Lcom/twitter/library/client/aa;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "msg_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "content"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "recipient_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "r_name"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "r_username"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "r_profile_image_url"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "sender_id"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "s_name"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "s_username"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "s_profile_image_url"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "created"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "entities"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "thread"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "unread_count"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/android/lx;->a:[Ljava/lang/String;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/twitter/android/lx;->b:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/twitter/android/lx;->c:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-direct {p0, p1, v0, v1}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/lx;->e:Ljava/util/ArrayList;

    invoke-static {p1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/lx;->d:Lcom/twitter/android/client/c;

    iget-object v0, p0, Lcom/twitter/android/lx;->d:Lcom/twitter/android/client/c;

    new-instance v1, Lcom/twitter/android/ly;

    invoke-direct {v1, p0}, Lcom/twitter/android/ly;-><init>(Lcom/twitter/android/lx;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/j;)V

    invoke-static {p1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/lx;->f:Lcom/twitter/library/client/aa;

    return-void
.end method

.method private a(Landroid/view/View;)Lcom/twitter/android/mc;
    .locals 2

    new-instance v1, Lcom/twitter/android/mc;

    const/4 v0, 0x0

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/mc;-><init>(Lcom/twitter/android/lx;Lcom/twitter/android/ly;)V

    const v0, 0x7f09012d    # com.twitter.android.R.id.profile_image

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/twitter/android/mc;->b:Landroid/widget/ImageView;

    const v0, 0x7f0901e6    # com.twitter.android.R.id.message_image

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/twitter/android/mc;->i:Landroid/widget/ImageView;

    const v0, 0x7f0901e5    # com.twitter.android.R.id.preview

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/twitter/android/mc;->c:Landroid/widget/TextView;

    const v0, 0x7f0900b6    # com.twitter.android.R.id.timestamp

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/twitter/android/mc;->d:Landroid/widget/TextView;

    const v0, 0x7f090097    # com.twitter.android.R.id.name

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/twitter/android/mc;->e:Landroid/widget/TextView;

    const v0, 0x7f090098    # com.twitter.android.R.id.username

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/twitter/android/mc;->f:Landroid/widget/TextView;

    check-cast p1, Lcom/twitter/internal/android/widget/HighlightedRelativeLayout;

    iput-object p1, v1, Lcom/twitter/android/mc;->a:Lcom/twitter/internal/android/widget/HighlightedRelativeLayout;

    return-object v1
.end method

.method static synthetic a(Lcom/twitter/android/lx;)Lcom/twitter/library/client/aa;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/lx;->f:Lcom/twitter/library/client/aa;

    return-object v0
.end method

.method static synthetic a()Ljava/util/HashMap;
    .locals 1

    sget-object v0, Lcom/twitter/android/lx;->c:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/lx;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/lx;->d:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/lx;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/lx;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Long;)I
    .locals 7

    invoke-virtual {p0}, Lcom/twitter/android/lx;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/android/lx;->f:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    if-eqz v2, :cond_3

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x7

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    cmp-long v5, v0, v3

    if-nez v5, :cond_1

    const/4 v0, 0x3

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v0, v0, v5

    if-nez v0, :cond_2

    invoke-interface {v2}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    :goto_0
    return v0

    :cond_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_3
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public a(Landroid/view/View;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 4

    invoke-direct {p0, p1}, Lcom/twitter/android/lx;->a(Landroid/view/View;)Lcom/twitter/android/mc;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/twitter/android/mc;->a(Ljava/lang/String;)V

    invoke-virtual {v0, p4}, Lcom/twitter/android/mc;->b(Ljava/lang/String;)V

    invoke-virtual {p6}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2, p5}, Lcom/twitter/android/mc;->a(JLjava/lang/String;)V

    invoke-virtual {p6}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/mc;->a(J)V

    iget-object v1, v0, Lcom/twitter/android/mc;->i:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, v0, Lcom/twitter/android/mc;->b:Landroid/widget/ImageView;

    new-instance v2, Lcom/twitter/android/ma;

    invoke-direct {v2, p0, p2, p6}, Lcom/twitter/android/ma;-><init>(Lcom/twitter/android/lx;Landroid/content/Context;Ljava/lang/Long;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Lcom/twitter/android/mc;->c:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/twitter/android/lx;->mContext:Landroid/content/Context;

    const v3, 0x7f1000f2    # com.twitter.android.R.style.TextItalic

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    iget-object v1, v0, Lcom/twitter/android/mc;->c:Landroid/widget/TextView;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0064    # com.twitter.android.R.color.medium_gray

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0298    # com.twitter.android.R.string.new_message_preview

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Lcom/twitter/android/mc;->a(Ljava/lang/String;Landroid/content/Context;Z)V

    return-void
.end method

.method public a(Ljava/util/HashMap;Z)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/lx;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_2

    iget-object v0, p0, Lcom/twitter/android/lx;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/mc;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/lx;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    :goto_1
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lcom/twitter/android/mc;->h:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/util/ae;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v1, v1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/twitter/android/mc;->a(Landroid/graphics/Bitmap;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 7

    const/4 v6, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/mc;

    new-instance v2, Lcom/twitter/android/mb;

    invoke-direct {v2, p0, p3}, Lcom/twitter/android/mb;-><init>(Lcom/twitter/android/lx;Landroid/database/Cursor;)V

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-wide v3, v2, Lcom/twitter/android/mb;->b:J

    iget-object v5, v2, Lcom/twitter/android/mb;->c:Ljava/lang/String;

    invoke-virtual {v0, v3, v4, v5}, Lcom/twitter/android/mc;->a(JLjava/lang/String;)V

    iget-object v3, v0, Lcom/twitter/android/mc;->b:Landroid/widget/ImageView;

    new-instance v4, Lcom/twitter/android/lz;

    invoke-direct {v4, p0, p2, v2}, Lcom/twitter/android/lz;-><init>(Lcom/twitter/android/lx;Landroid/content/Context;Lcom/twitter/android/mb;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, v0, Lcom/twitter/android/mc;->d:Landroid/widget/TextView;

    iget-wide v4, v2, Lcom/twitter/android/mb;->g:J

    invoke-static {v1, v4, v5}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, v2, Lcom/twitter/android/mb;->e:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/twitter/android/mc;->a(Ljava/lang/String;)V

    iget-object v3, v2, Lcom/twitter/android/mb;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/twitter/android/mc;->b(Ljava/lang/String;)V

    iget-wide v3, v2, Lcom/twitter/android/mb;->b:J

    invoke-virtual {v0, v3, v4}, Lcom/twitter/android/mc;->a(J)V

    iget-boolean v3, v2, Lcom/twitter/android/mb;->f:Z

    invoke-virtual {v0, v3, v1}, Lcom/twitter/android/mc;->a(ZLandroid/content/res/Resources;)V

    sget-object v1, Lcom/twitter/android/lx;->c:Ljava/util/HashMap;

    iget-object v3, v0, Lcom/twitter/android/mc;->j:Lcom/twitter/library/util/m;

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v6, v0, Lcom/twitter/android/mc;->j:Lcom/twitter/library/util/m;

    iget-object v1, v2, Lcom/twitter/android/mb;->i:Lcom/twitter/library/api/MediaEntity;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/twitter/android/lx;->b:Ljava/util/HashMap;

    iget-object v3, v2, Lcom/twitter/android/mb;->i:Lcom/twitter/library/api/MediaEntity;

    iget-object v3, v3, Lcom/twitter/library/api/MediaEntity;->mediaUrl:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/twitter/android/lx;->b:Ljava/util/HashMap;

    iget-object v3, v2, Lcom/twitter/android/mb;->i:Lcom/twitter/library/api/MediaEntity;

    iget-object v3, v3, Lcom/twitter/library/api/MediaEntity;->mediaUrl:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/m;

    iput-object v1, v0, Lcom/twitter/android/mc;->j:Lcom/twitter/library/util/m;

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/lx;->d:Lcom/twitter/android/client/c;

    iget-object v1, v1, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    iget-object v3, v0, Lcom/twitter/android/mc;->j:Lcom/twitter/library/util/m;

    invoke-interface {v1, v3}, Lcom/twitter/library/widget/ap;->a(Lcom/twitter/library/util/m;)Landroid/graphics/Bitmap;

    move-result-object v1

    iget-object v3, v0, Lcom/twitter/android/mc;->i:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v1, v0, Lcom/twitter/android/mc;->i:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    sget-object v1, Lcom/twitter/android/lx;->c:Ljava/util/HashMap;

    iget-object v3, v0, Lcom/twitter/android/mc;->j:Lcom/twitter/library/util/m;

    iget-object v4, v0, Lcom/twitter/android/mc;->i:Landroid/widget/ImageView;

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    iget-boolean v1, v2, Lcom/twitter/android/mb;->k:Z

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/twitter/android/mc;->c:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/twitter/android/lx;->mContext:Landroid/content/Context;

    const v4, 0x7f1000f2    # com.twitter.android.R.style.TextItalic

    invoke-virtual {v1, v3, v4}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    :goto_2
    iget-object v1, v2, Lcom/twitter/android/mb;->j:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/twitter/android/lx;->mContext:Landroid/content/Context;

    iget-boolean v2, v2, Lcom/twitter/android/mb;->h:Z

    invoke-virtual {v0, v1, v3, v2}, Lcom/twitter/android/mc;->a(Ljava/lang/String;Landroid/content/Context;Z)V

    return-void

    :cond_0
    new-instance v1, Lcom/twitter/library/util/m;

    iget-object v3, v2, Lcom/twitter/android/mb;->i:Lcom/twitter/library/api/MediaEntity;

    iget-object v3, v3, Lcom/twitter/library/api/MediaEntity;->mediaUrl:Ljava/lang/String;

    iget-object v4, v2, Lcom/twitter/android/mb;->i:Lcom/twitter/library/api/MediaEntity;

    iget v4, v4, Lcom/twitter/library/api/MediaEntity;->width:I

    iget-object v5, v2, Lcom/twitter/android/mb;->i:Lcom/twitter/library/api/MediaEntity;

    iget v5, v5, Lcom/twitter/library/api/MediaEntity;->height:I

    const/4 v6, 0x1

    invoke-direct {v1, v3, v4, v5, v6}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;IIZ)V

    iput-object v1, v0, Lcom/twitter/android/mc;->j:Lcom/twitter/library/util/m;

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lcom/twitter/android/mc;->i:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v1, v0, Lcom/twitter/android/mc;->i:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    :cond_2
    iget-object v1, v0, Lcom/twitter/android/mc;->c:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/twitter/android/lx;->mContext:Landroid/content/Context;

    const v4, 0x7f1000f3    # com.twitter.android.R.style.TextNormal

    invoke-virtual {v1, v3, v4}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    goto :goto_2
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 6

    invoke-super {p0, p1}, Landroid/support/v4/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    new-instance v1, Lcom/twitter/android/mb;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/mb;-><init>(Lcom/twitter/android/lx;Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/twitter/android/lx;->f:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-instance v0, Landroid/content/Intent;

    iget-object v4, p0, Lcom/twitter/android/lx;->mContext:Landroid/content/Context;

    const-class v5, Lcom/twitter/android/MessagesDetailActivity;

    invoke-direct {v0, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v4, "thread_id"

    iget-object v5, v1, Lcom/twitter/android/mb;->a:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v4, "owner_id"

    invoke-virtual {v0, v4, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "user_id"

    iget-wide v3, v1, Lcom/twitter/android/mb;->b:J

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "user_name"

    iget-object v3, v1, Lcom/twitter/android/mb;->d:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "user_fullname"

    iget-object v1, v1, Lcom/twitter/android/mb;->e:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    const/16 v1, 0xd

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300c2    # com.twitter.android.R.layout.message_row_view

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/lx;->a(Landroid/view/View;)Lcom/twitter/android/mc;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/twitter/android/lx;->e:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method
