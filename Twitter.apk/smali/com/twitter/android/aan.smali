.class Lcom/twitter/android/aan;
.super Landroid/webkit/WebViewClient;
.source "Twttr"


# instance fields
.field final synthetic a:Landroid/app/ProgressDialog;

.field final synthetic b:Landroid/content/res/Resources;

.field final synthetic c:Landroid/webkit/WebView;

.field final synthetic d:Lcom/twitter/library/network/OAuthToken;

.field final synthetic e:Ljava/util/HashMap;

.field final synthetic f:Lcom/twitter/android/WebViewActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/WebViewActivity;Landroid/app/ProgressDialog;Landroid/content/res/Resources;Landroid/webkit/WebView;Lcom/twitter/library/network/OAuthToken;Ljava/util/HashMap;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/aan;->f:Lcom/twitter/android/WebViewActivity;

    iput-object p2, p0, Lcom/twitter/android/aan;->a:Landroid/app/ProgressDialog;

    iput-object p3, p0, Lcom/twitter/android/aan;->b:Landroid/content/res/Resources;

    iput-object p4, p0, Lcom/twitter/android/aan;->c:Landroid/webkit/WebView;

    iput-object p5, p0, Lcom/twitter/android/aan;->d:Lcom/twitter/library/network/OAuthToken;

    iput-object p6, p0, Lcom/twitter/android/aan;->e:Ljava/util/HashMap;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p1}, Landroid/webkit/WebView;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->X()Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/aan;->a:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/aan;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/aan;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_0
    return-void
.end method

.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/aan;->a:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/aan;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/aan;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_0
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 6

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/aan;->f:Lcom/twitter/android/WebViewActivity;

    invoke-virtual {v1, v0}, Lcom/twitter/android/WebViewActivity;->a(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v2, p0, Lcom/twitter/android/aan;->b:Landroid/content/res/Resources;

    const v3, 0x7f0f04fd    # com.twitter.android.R.string.twitter_authority

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/twitter/android/aan;->c:Landroid/webkit/WebView;

    iget-object v3, p0, Lcom/twitter/android/aan;->f:Lcom/twitter/android/WebViewActivity;

    iget-object v4, p0, Lcom/twitter/android/aan;->d:Lcom/twitter/library/network/OAuthToken;

    iget-object v5, p0, Lcom/twitter/android/aan;->e:Ljava/util/HashMap;

    invoke-virtual {v3, v4, v0, v1, v5}, Lcom/twitter/android/WebViewActivity;->a(Lcom/twitter/library/network/OAuthToken;Landroid/net/Uri;ZLjava/util/Map;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v2, p2, v0}, Lcom/twitter/library/util/Util;->a(Landroid/webkit/WebView;Ljava/lang/String;Ljava/util/Map;)V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/aan;->f:Lcom/twitter/android/WebViewActivity;

    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/WebViewActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/twitter/android/aan;->f:Lcom/twitter/android/WebViewActivity;

    invoke-virtual {v0}, Lcom/twitter/android/WebViewActivity;->finish()V

    const/4 v0, 0x1

    goto :goto_0
.end method
