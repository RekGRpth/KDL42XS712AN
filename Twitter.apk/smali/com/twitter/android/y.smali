.class Lcom/twitter/android/y;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/ActivityFragment;


# direct methods
.method public constructor <init>(Lcom/twitter/android/ActivityFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/y;->a:Lcom/twitter/android/ActivityFragment;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;JLjava/lang/String;ILjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/y;->a:Lcom/twitter/android/ActivityFragment;

    invoke-static {v0}, Lcom/twitter/android/ActivityFragment;->f(Lcom/twitter/android/ActivityFragment;)V

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;IJ)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/y;->a:Lcom/twitter/android/ActivityFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/ActivityFragment;->b(Lcom/twitter/android/ActivityFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/y;->a:Lcom/twitter/android/ActivityFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/ActivityFragment;->a(Lcom/twitter/android/ActivityFragment;Z)Z

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/y;->a:Lcom/twitter/android/ActivityFragment;

    invoke-static {v0}, Lcom/twitter/android/ActivityFragment;->c(Lcom/twitter/android/ActivityFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/y;->a:Lcom/twitter/android/ActivityFragment;

    invoke-static {v0}, Lcom/twitter/android/ActivityFragment;->e(Lcom/twitter/android/ActivityFragment;)Lcom/twitter/android/util/w;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2, p4, p5}, Lcom/twitter/android/util/w;->a(JJ)V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;IJJIZ)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/y;->a:Lcom/twitter/android/ActivityFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/ActivityFragment;->a(Lcom/twitter/android/ActivityFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/twitter/android/y;->a:Lcom/twitter/android/ActivityFragment;

    invoke-static {v1}, Lcom/twitter/android/ActivityFragment;->g(Lcom/twitter/android/ActivityFragment;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/y;->a:Lcom/twitter/android/ActivityFragment;

    iget v0, v0, Lcom/twitter/android/client/PendingRequest;->b:I

    invoke-virtual {v1, v0}, Lcom/twitter/android/ActivityFragment;->b(I)V

    :cond_0
    const/16 v0, 0xc8

    if-eq p3, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/y;->a:Lcom/twitter/android/ActivityFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0f0012    # com.twitter.android.R.string.activity_fetch_error

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Z)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/y;->a:Lcom/twitter/android/ActivityFragment;

    invoke-static {v0}, Lcom/twitter/android/ActivityFragment;->j(Lcom/twitter/android/ActivityFragment;)Landroid/support/v4/widget/CursorAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/y;->a:Lcom/twitter/android/ActivityFragment;

    invoke-static {v0}, Lcom/twitter/android/ActivityFragment;->k(Lcom/twitter/android/ActivityFragment;)Landroid/support/v4/widget/CursorAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/h;

    invoke-virtual {v0, p2}, Lcom/twitter/android/h;->a(Z)V

    :cond_0
    return-void
.end method

.method public a(Ljava/util/HashMap;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/y;->a:Lcom/twitter/android/ActivityFragment;

    invoke-static {v0}, Lcom/twitter/android/ActivityFragment;->h(Lcom/twitter/android/ActivityFragment;)Landroid/support/v4/widget/CursorAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/y;->a:Lcom/twitter/android/ActivityFragment;

    invoke-static {v0}, Lcom/twitter/android/ActivityFragment;->i(Lcom/twitter/android/ActivityFragment;)Landroid/support/v4/widget/CursorAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/h;

    invoke-virtual {v0, p1}, Lcom/twitter/android/h;->a(Ljava/util/HashMap;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/y;->a:Lcom/twitter/android/ActivityFragment;

    invoke-static {v0, p1}, Lcom/twitter/android/ActivityFragment;->a(Lcom/twitter/android/ActivityFragment;Ljava/util/HashMap;)V

    return-void
.end method
