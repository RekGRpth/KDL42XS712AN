.class Lcom/twitter/android/kj;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/MainActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/MainActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/kj;->a:Lcom/twitter/android/MainActivity;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;JLjava/lang/String;ILjava/lang/String;)V
    .locals 2

    invoke-super/range {p0 .. p6}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;JLjava/lang/String;ILjava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/kj;->a:Lcom/twitter/android/MainActivity;

    invoke-virtual {v0}, Lcom/twitter/android/MainActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-static {}, Lgu;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setDisplayOptions(I)V

    iget-object v0, p0, Lcom/twitter/android/kj;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v0}, Lcom/twitter/android/MainActivity;->w(Lcom/twitter/android/MainActivity;)Lcom/twitter/android/client/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/z;->a()I

    move-result v0

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/kj;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v1}, Lcom/twitter/android/MainActivity;->x(Lcom/twitter/android/MainActivity;)Lcom/twitter/android/client/bn;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/bn;->a(I)V

    return-void

    :cond_0
    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->b(I)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/io/File;)V
    .locals 4

    invoke-static {}, Lcom/twitter/library/client/App;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/kj;->a:Lcom/twitter/android/MainActivity;

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    const-string/jumbo v3, "application/vnd.android.package-archive"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/MainActivity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;I)V
    .locals 2

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/kj;->a:Lcom/twitter/android/MainActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/MainActivity;->a(Lcom/twitter/android/MainActivity;Landroid/graphics/drawable/BitmapDrawable;)Landroid/graphics/drawable/BitmapDrawable;

    iget-object v0, p0, Lcom/twitter/android/kj;->a:Lcom/twitter/android/MainActivity;

    invoke-virtual {v0}, Lcom/twitter/android/MainActivity;->V()V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;IJJIZ)V
    .locals 4

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iget-object v2, p0, Lcom/twitter/android/kj;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v2}, Lcom/twitter/android/MainActivity;->r(Lcom/twitter/android/MainActivity;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p11, :cond_2

    iget-object v0, p0, Lcom/twitter/android/kj;->a:Lcom/twitter/android/MainActivity;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/MainActivity;->a(Lcom/twitter/android/MainActivity;Ljava/lang/String;J)V

    goto :goto_0

    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "activity"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v1, p0, Lcom/twitter/android/kj;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v1, v0}, Lcom/twitter/android/MainActivity;->b(Lcom/twitter/android/MainActivity;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;IZLjava/lang/String;Lcom/twitter/library/api/search/TwitterTypeAheadGroup;)V
    .locals 9

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_1

    if-eqz p6, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-object v0, p0, Lcom/twitter/android/kj;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v0}, Lcom/twitter/android/MainActivity;->t(Lcom/twitter/android/MainActivity;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/twitter/library/client/f;

    iget-object v1, p0, Lcom/twitter/android/kj;->a:Lcom/twitter/android/MainActivity;

    const-string/jumbo v5, "hometab"

    invoke-direct {v2, v1, v0, v5}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x3

    if-ne p5, v0, :cond_2

    invoke-virtual {v2}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v1, "tatt"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iget-object v7, p0, Lcom/twitter/android/kj;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v7}, Lcom/twitter/android/MainActivity;->u(Lcom/twitter/android/MainActivity;)Lcom/twitter/android/client/c;

    move-result-object v7

    invoke-virtual {v7}, Lcom/twitter/android/client/c;->w()J

    move-result-wide v7

    sub-long/2addr v5, v7

    const-wide/32 v7, 0x927c0

    sub-long/2addr v5, v7

    invoke-virtual {v0, v1, v5, v6}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;J)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/kj;->a:Lcom/twitter/android/MainActivity;

    const-wide/32 v5, 0x927c0

    move-object v1, p1

    invoke-static/range {v0 .. v6}, Lcom/twitter/android/MainActivity;->a(Lcom/twitter/android/MainActivity;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/f;JJ)V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x1

    if-ne p5, v0, :cond_3

    invoke-virtual {v2}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v1, "taut"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iget-object v7, p0, Lcom/twitter/android/kj;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v7}, Lcom/twitter/android/MainActivity;->v(Lcom/twitter/android/MainActivity;)Lcom/twitter/android/client/c;

    move-result-object v7

    invoke-virtual {v7}, Lcom/twitter/android/client/c;->u()J

    move-result-wide v7

    sub-long/2addr v5, v7

    const-wide/32 v7, 0x927c0

    sub-long/2addr v5, v7

    invoke-virtual {v0, v1, v5, v6}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;J)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V

    goto :goto_0

    :cond_3
    const/4 v0, 0x4

    if-ne p5, v0, :cond_0

    invoke-virtual {v2}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v1, "taot"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->w()J

    move-result-wide v7

    sub-long/2addr v5, v7

    const-wide/32 v7, 0x927c0

    sub-long/2addr v5, v7

    invoke-virtual {v0, v1, v5, v6}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;J)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 4

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iget-object v2, p0, Lcom/twitter/android/kj;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v2}, Lcom/twitter/android/MainActivity;->s(Lcom/twitter/android/MainActivity;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "discover"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v1, p0, Lcom/twitter/android/kj;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v1, v0}, Lcom/twitter/android/MainActivity;->b(Lcom/twitter/android/MainActivity;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;I[ILjava/lang/String;Lcom/twitter/library/api/TwitterUser;)V
    .locals 4

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_0

    if-eqz p6, :cond_0

    iget-wide v0, p6, Lcom/twitter/library/api/TwitterUser;->userId:J

    iget-object v2, p0, Lcom/twitter/android/kj;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v2}, Lcom/twitter/android/MainActivity;->q(Lcom/twitter/android/MainActivity;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/kj;->a:Lcom/twitter/android/MainActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/MainActivity;->a(Z)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/kj;->a:Lcom/twitter/android/MainActivity;

    iget-object v1, p0, Lcom/twitter/android/kj;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v1}, Lcom/twitter/android/MainActivity;->a(Lcom/twitter/android/MainActivity;)Lcom/twitter/android/kr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/kr;->c()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/MainActivity;->a(Lcom/twitter/android/MainActivity;Landroid/net/Uri;)V

    return-void
.end method
