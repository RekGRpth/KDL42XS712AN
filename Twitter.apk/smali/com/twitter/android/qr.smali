.class Lcom/twitter/android/qr;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Landroid/widget/TextView;

.field public final b:Lcom/twitter/internal/android/widget/ShadowTextView;

.field public final c:Lcom/twitter/internal/android/widget/ShadowTextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7f090230    # com.twitter.android.R.id.requester

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/qr;->a:Landroid/widget/TextView;

    const v0, 0x7f09002c    # com.twitter.android.R.id.action_button

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ShadowTextView;

    iput-object v0, p0, Lcom/twitter/android/qr;->b:Lcom/twitter/internal/android/widget/ShadowTextView;

    const v0, 0x7f09002f    # com.twitter.android.R.id.action_button_deny

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ShadowTextView;

    iput-object v0, p0, Lcom/twitter/android/qr;->c:Lcom/twitter/internal/android/widget/ShadowTextView;

    return-void
.end method
