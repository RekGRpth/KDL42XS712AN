.class Lcom/twitter/android/wb;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/client/c;

.field final synthetic b:J

.field final synthetic c:Lcom/twitter/android/TweetActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/TweetActivity;Lcom/twitter/android/client/c;J)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/wb;->c:Lcom/twitter/android/TweetActivity;

    iput-object p2, p0, Lcom/twitter/android/wb;->a:Lcom/twitter/android/client/c;

    iput-wide p3, p0, Lcom/twitter/android/wb;->b:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    const/4 v9, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/wb;->c:Lcom/twitter/android/TweetActivity;

    invoke-virtual {v0}, Lcom/twitter/android/TweetActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/wb;->c:Lcom/twitter/android/TweetActivity;

    iget-object v1, v1, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    iget-object v2, p0, Lcom/twitter/android/wb;->c:Lcom/twitter/android/TweetActivity;

    iget-object v2, v2, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v2, v2, Lcom/twitter/library/provider/Tweet;->d:Ljava/lang/String;

    const-wide/16 v3, 0x0

    iget-object v6, p0, Lcom/twitter/android/wb;->c:Lcom/twitter/android/TweetActivity;

    invoke-static {v6}, Lcom/twitter/android/TweetActivity;->s(Lcom/twitter/android/TweetActivity;)Lcom/twitter/library/api/TweetEntities;

    move-result-object v8

    move-object v6, v5

    move-object v7, v5

    move-object v10, v5

    invoke-static/range {v0 .. v10}, Lcom/twitter/android/client/cd;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;JLandroid/location/Location;Ljava/lang/String;Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/api/TweetEntities;ZLjava/util/ArrayList;)Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/wb;->a:Lcom/twitter/android/client/c;

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    iget-wide v2, p0, Lcom/twitter/android/wb;->b:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v2, v9, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "welcome:compose:::apply"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    iget-object v0, p0, Lcom/twitter/android/wb;->c:Lcom/twitter/android/TweetActivity;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/wb;->c:Lcom/twitter/android/TweetActivity;

    const-class v3, Lcom/twitter/android/MainActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetActivity;->d(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/twitter/android/wb;->c:Lcom/twitter/android/TweetActivity;

    invoke-virtual {v0}, Lcom/twitter/android/TweetActivity;->finish()V

    return-void
.end method
