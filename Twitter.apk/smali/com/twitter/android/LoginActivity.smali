.class public Lcom/twitter/android/LoginActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/android/widget/ce;


# instance fields
.field a:Z

.field private final b:Lcom/twitter/android/jg;

.field private c:I

.field private d:Z

.field private e:Landroid/widget/EditText;

.field private f:Landroid/widget/EditText;

.field private g:Landroid/widget/Button;

.field private h:Landroid/widget/CheckBox;

.field private i:Landroid/widget/CheckBox;

.field private j:Landroid/view/View;

.field private k:I

.field private l:Lcom/twitter/android/util/d;

.field private m:Ljava/lang/String;

.field private n:Landroid/widget/TextView;

.field private o:Landroid/widget/TextView;

.field private p:Z

.field private q:Z

.field private r:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    new-instance v0, Lcom/twitter/android/jg;

    invoke-direct {v0, p0}, Lcom/twitter/android/jg;-><init>(Lcom/twitter/android/LoginActivity;)V

    iput-object v0, p0, Lcom/twitter/android/LoginActivity;->b:Lcom/twitter/android/jg;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/LoginActivity;I)I
    .locals 0

    iput p1, p0, Lcom/twitter/android/LoginActivity;->c:I

    return p1
.end method

.method static synthetic a(Lcom/twitter/android/LoginActivity;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->e:Landroid/widget/EditText;

    return-object v0
.end method

.method private a(I)V
    .locals 3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/PasswordResetActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "account_id"

    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->n()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p1}, Lcom/twitter/android/util/y;->a(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;Landroid/content/Intent;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/LoginActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "android.intent.extra.INTENT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method private a(Landroid/net/Uri;)V
    .locals 13

    const/4 v5, 0x0

    const/4 v8, 0x1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->D()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string/jumbo v0, "screen_name"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "login_verification_user_id"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "login_verification_request_id"

    invoke-virtual {p1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v2, "login_verification_cause"

    invoke-virtual {p1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    :try_start_0
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v6

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/twitter/library/client/aa;->a(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v7

    if-ne v6, v8, :cond_2

    const-string/jumbo v0, "login_verification_type"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    :try_start_1
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v4

    iput-boolean v8, p0, Lcom/twitter/android/LoginActivity;->q:Z

    iget-object v8, p0, Lcom/twitter/android/LoginActivity;->b:Lcom/twitter/android/jg;

    new-instance v0, Lcom/twitter/library/network/LoginVerificationRequiredResponse;

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/network/LoginVerificationRequiredResponse;-><init>(JLjava/lang/String;ILjava/lang/String;I)V

    invoke-virtual {v8, v7, v0}, Lcom/twitter/android/jg;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/network/LoginVerificationRequiredResponse;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v8}, Lcom/twitter/android/LoginActivity;->showDialog(I)V

    iput-boolean v8, p0, Lcom/twitter/android/LoginActivity;->q:Z

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v6

    iget-object v12, p0, Lcom/twitter/android/LoginActivity;->b:Lcom/twitter/android/jg;

    move-wide v8, v1

    move-object v10, v3

    move-object v11, v5

    invoke-virtual/range {v6 .. v12}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/Session;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/client/ac;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/LoginActivity;->m:Ljava/lang/String;

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/LoginActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/LoginActivity;->q:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/android/LoginActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private b(I)V
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x0

    iput p1, p0, Lcom/twitter/android/LoginActivity;->k:I

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->g:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->f()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget v0, p0, Lcom/twitter/android/LoginActivity;->k:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-direct {p0, v2}, Lcom/twitter/android/LoginActivity;->c(I)V

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->j:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->g:Landroid/widget/Button;

    const v1, 0x7f0f0236    # com.twitter.android.R.string.login_signin

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/twitter/android/LoginActivity;->k:I

    if-nez v0, :cond_0

    invoke-direct {p0, v3}, Lcom/twitter/android/LoginActivity;->c(I)V

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->g:Landroid/widget/Button;

    const v1, 0x7f0f02e4    # com.twitter.android.R.string.one_factor_login_using_password

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->j:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->o:Landroid/widget/TextView;

    const v1, 0x7f0f02df    # com.twitter.android.R.string.one_factor_disclaimer

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/android/LoginActivity;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c(I)V
    .locals 1

    const v0, 0x7f0901c0    # com.twitter.android.R.id.password_login_view

    invoke-virtual {p0, v0}, Lcom/twitter/android/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/twitter/android/LoginActivity;->d(I)V

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/twitter/android/LoginActivity;->d(I)V

    goto :goto_0
.end method

.method private d(I)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->i:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->o:Landroid/widget/TextView;

    const v1, 0x7f0f043d    # com.twitter.android.R.string.signup_digits_discovery_privacy

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->o:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->l:Lcom/twitter/android/util/d;

    invoke-interface {v0}, Lcom/twitter/android/util/d;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->h:Landroid/widget/CheckBox;

    const v1, 0x7f0f0438    # com.twitter.android.R.string.signup_addr_book_and_phone_text

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/LoginActivity;->l:Lcom/twitter/android/util/d;

    invoke-interface {v4}, Lcom/twitter/android/util/d;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/twitter/android/LoginActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->h:Landroid/widget/CheckBox;

    const v1, 0x7f0f0439    # com.twitter.android.R.string.signup_addr_book_no_phone_text

    invoke-virtual {p0, v1}, Lcom/twitter/android/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/twitter/android/LoginActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/LoginActivity;->q:Z

    return v0
.end method

.method static synthetic e(Lcom/twitter/android/LoginActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/LoginActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method private f()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->f:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/LoginActivity;->k:I

    if-nez v0, :cond_2

    :cond_0
    invoke-static {}, Lcom/twitter/library/client/App;->o()Z

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/LoginActivity;->e:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;ZLjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p0}, Lcom/twitter/library/util/Util;->n(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic g(Lcom/twitter/android/LoginActivity;)I
    .locals 2

    iget v0, p0, Lcom/twitter/android/LoginActivity;->c:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/twitter/android/LoginActivity;->c:I

    return v0
.end method

.method private g()V
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->f:Landroid/widget/EditText;

    invoke-static {p0, v0, v7}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;Landroid/view/View;Z)V

    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->n()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/LoginActivity;->f:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    new-array v5, v8, [Ljava/lang/String;

    const-string/jumbo v6, "login:form:::submit"

    aput-object v6, v5, v7

    invoke-virtual {v2, v3, v4, v5}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v2, p0, Lcom/twitter/android/LoginActivity;->r:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v4, v8, [Ljava/lang/String;

    const-string/jumbo v5, "login:form:::submit"

    aput-object v5, v4, v7

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    new-array v4, v8, [Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/android/LoginActivity;->r:Ljava/lang/String;

    aput-object v5, v4, v7

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->c([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/LoginActivity;->b:Lcom/twitter/android/jg;

    invoke-virtual {v2, v0, v1, v3}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/client/ae;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/LoginActivity;->m:Ljava/lang/String;

    :goto_0
    invoke-virtual {p0, v8}, Lcom/twitter/android/LoginActivity;->showDialog(I)V

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/LoginActivity;->b:Lcom/twitter/android/jg;

    invoke-virtual {v2, v0, v1, v3}, Lcom/twitter/library/client/aa;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/client/ae;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/LoginActivity;->m:Ljava/lang/String;

    goto :goto_0
.end method

.method private h()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->f:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;Landroid/view/View;Z)V

    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->n()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/twitter/android/LoginActivity;->p:Z

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/OneFactorLoginActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "username"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic h(Lcom/twitter/android/LoginActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->l()V

    return-void
.end method

.method static synthetic i(Lcom/twitter/android/LoginActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->m()V

    return-void
.end method

.method static synthetic j(Lcom/twitter/android/LoginActivity;)I
    .locals 1

    iget v0, p0, Lcom/twitter/android/LoginActivity;->c:I

    return v0
.end method

.method static synthetic k(Lcom/twitter/android/LoginActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method private k()Z
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/android/LoginActivity;->d:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "android.intent.extra.INTENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->l:Lcom/twitter/android/util/d;

    invoke-interface {v0}, Lcom/twitter/android/util/d;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic l(Lcom/twitter/android/LoginActivity;)I
    .locals 1

    iget v0, p0, Lcom/twitter/android/LoginActivity;->k:I

    return v0
.end method

.method private l()V
    .locals 6

    const/4 v0, 0x3

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0517    # com.twitter.android.R.string.use_a_temporary_password_title

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0516    # com.twitter.android.R.string.use_a_temporary_password_message

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f02d5    # com.twitter.android.R.string.ok

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f01b5    # com.twitter.android.R.string.get_help

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "login::use_temporary_password_prompt::impression"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    return-void
.end method

.method private m()V
    .locals 3

    const v2, 0x7f0f035f    # com.twitter.android.R.string.reset_password

    const/4 v0, 0x4

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0360    # com.twitter.android.R.string.reset_password_message

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f04e1    # com.twitter.android.R.string.tweets_dismiss_positive

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    return-void
.end method

.method private n()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private o()V
    .locals 5

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "login::::success"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    const-string/jumbo v2, "app_download_client_event"

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->g(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->h()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "4"

    invoke-virtual {v1, v3, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->g()Lcom/twitter/library/api/b;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string/jumbo v3, "6"

    invoke-virtual {v2}, Lcom/twitter/library/api/b;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {v2}, Lcom/twitter/library/api/b;->b()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Z)Lcom/twitter/library/scribe/ScribeLog;

    :cond_0
    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const v1, 0x7f0300ae    # com.twitter.android.R.layout.login

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->a(Z)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->c(Z)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->b(Z)V

    return-object v0
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 6

    const/4 v1, -0x1

    const/4 v0, -0x2

    const/4 v3, 0x1

    const/4 v5, 0x0

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-ne p3, v1, :cond_0

    const v0, 0x7f0f02f3    # com.twitter.android.R.string.password_reset_url_refsrc_dialog

    invoke-direct {p0, v0}, Lcom/twitter/android/LoginActivity;->a(I)V

    goto :goto_0

    :pswitch_1
    if-ne p3, v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    const v2, 0x7f0f0254    # com.twitter.android.R.string.login_verification_temp_pw_support_url

    invoke-virtual {p0, v2}, Lcom/twitter/android/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/LoginActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "login::use_temporary_password_prompt:get_help:click"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    if-ne p3, v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    const v2, 0x7f0f0361    # com.twitter.android.R.string.reset_password_url

    invoke-virtual {p0, v2}, Lcom/twitter/android/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/LoginActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_3
    if-ne p3, v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "login::1fa:rescue:continue"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/OneFactorLoginActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "username"

    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "login::1fa:rescue:cancel"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 10

    const v4, 0x7f090140    # com.twitter.android.R.id.phone_number_addr_book_checkbox

    const v1, 0x7f0900d1    # com.twitter.android.R.id.password_reset

    const/4 v9, 0x1

    const/4 v8, 0x0

    const v0, 0x7f0f0236    # com.twitter.android.R.string.login_signin

    invoke-virtual {p0, v0}, Lcom/twitter/android/LoginActivity;->setTitle(I)V

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v0, "add_account"

    invoke-virtual {v2, v0, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/LoginActivity;->a:Z

    const-string/jumbo v0, "authorize_account"

    invoke-virtual {v2, v0, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/LoginActivity;->d:Z

    sget-object v0, Lcom/twitter/android/SignedOutFragment;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/LoginActivity;->r:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v3

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->r:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/twitter/android/SignedOutActivity;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/android/SignedOutFragment;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/LoginActivity;->r:Ljava/lang/String;

    :cond_0
    const v0, 0x7f0901bf    # com.twitter.android.R.id.login_username

    invoke-virtual {p0, v0}, Lcom/twitter/android/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/LoginActivity;->e:Landroid/widget/EditText;

    const v0, 0x7f0901c1    # com.twitter.android.R.id.login_password

    invoke-virtual {p0, v0}, Lcom/twitter/android/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/LoginActivity;->f:Landroid/widget/EditText;

    const v0, 0x7f0901c2    # com.twitter.android.R.id.login_login

    invoke-virtual {p0, v0}, Lcom/twitter/android/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/LoginActivity;->g:Landroid/widget/Button;

    invoke-virtual {p0, v1}, Lcom/twitter/android/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/LoginActivity;->n:Landroid/widget/TextView;

    const v0, 0x7f0901c4    # com.twitter.android.R.id.disclaimer

    invoke-virtual {p0, v0}, Lcom/twitter/android/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/LoginActivity;->o:Landroid/widget/TextView;

    invoke-virtual {p0, v4}, Lcom/twitter/android/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/twitter/android/LoginActivity;->h:Landroid/widget/CheckBox;

    invoke-virtual {p0, v4}, Lcom/twitter/android/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/twitter/android/LoginActivity;->i:Landroid/widget/CheckBox;

    const v0, 0x7f0901c3    # com.twitter.android.R.id.login_one_factor

    invoke-virtual {p0, v0}, Lcom/twitter/android/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/LoginActivity;->j:Landroid/view/View;

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->g:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->e:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->f:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    invoke-static {p0}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;)Lcom/twitter/android/util/d;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/LoginActivity;->l:Lcom/twitter/android/util/d;

    const v0, 0x7f0901c5    # com.twitter.android.R.id.settings_button

    invoke-virtual {p0, v0}, Lcom/twitter/android/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v1}, Lcom/twitter/android/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput v8, p0, Lcom/twitter/android/LoginActivity;->c:I

    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    if-eqz v4, :cond_7

    const-string/jumbo v0, "screen_name"

    invoke-virtual {v4, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v0, "password"

    invoke-virtual {v4, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/twitter/android/LoginActivity;->e:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->f:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    :cond_1
    :goto_1
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string/jumbo v2, "com.google"

    invoke-virtual {v0, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    if-eqz v0, :cond_9

    array-length v2, v0

    if-lez v2, :cond_9

    aget-object v0, v0, v8

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    :goto_2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->b()Z

    move-result v2

    if-nez v2, :cond_b

    const-string/jumbo v2, "prefill_signin_email_qster3_1640"

    invoke-static {p0, v5, v6, v2}, Lju;->b(Landroid/content/Context;JLjava/lang/String;)V

    const-string/jumbo v2, "prefill_signin_email_qster3_1640"

    invoke-static {p0, v5, v6, v2}, Lju;->a(Landroid/content/Context;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v7, "prefill_highlight_username"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    iget-object v2, p0, Lcom/twitter/android/LoginActivity;->e:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    :cond_2
    :goto_3
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c

    new-array v0, v9, [Ljava/lang/String;

    const-string/jumbo v1, "login:::username:prefill_fail"

    aput-object v1, v0, v8

    invoke-virtual {v3, v5, v6, v0}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :goto_4
    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->e:Landroid/widget/EditText;

    new-instance v1, Lcom/twitter/android/jd;

    invoke-direct {v1, p0, v3}, Lcom/twitter/android/jd;-><init>(Lcom/twitter/android/LoginActivity;Lcom/twitter/android/client/c;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->f:Landroid/widget/EditText;

    new-instance v1, Lcom/twitter/android/je;

    invoke-direct {v1, p0, v3, v5, v6}, Lcom/twitter/android/je;-><init>(Lcom/twitter/android/LoginActivity;Lcom/twitter/android/client/c;J)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->b()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-direct {p0, v8}, Lcom/twitter/android/LoginActivity;->b(I)V

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->j:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_5
    invoke-static {}, Lcom/twitter/library/client/App;->o()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0}, Lcom/twitter/library/util/Util;->n(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {p0}, Lcom/twitter/library/util/a;->b(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_3

    const v0, 0x7f0901be    # com.twitter.android.R.id.label_username

    invoke-virtual {p0, v0}, Lcom/twitter/android/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string/jumbo v1, "@twitter.com email"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    if-nez p1, :cond_e

    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v0, v5, v6}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v1, v9, [Ljava/lang/String;

    const-string/jumbo v2, "login::::impression"

    aput-object v2, v1, v8

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/LoginActivity;->r:Ljava/lang/String;

    if-eqz v1, :cond_4

    new-array v1, v9, [Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/LoginActivity;->r:Ljava/lang/String;

    aput-object v2, v1, v8

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->c([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_4
    invoke-virtual {v3, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/twitter/library/client/App;->b()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {}, Lcom/twitter/library/client/App;->c()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {}, Lcom/twitter/library/client/App;->a()Z

    move-result v0

    if-nez v0, :cond_5

    const-wide/16 v0, 0x0

    const-string/jumbo v2, "android_one_factor_signin_1978"

    invoke-static {p0, v0, v1, v2}, Lju;->b(Landroid/content/Context;JLjava/lang/String;)V

    :cond_5
    if-eqz v4, :cond_6

    invoke-direct {p0, v4}, Lcom/twitter/android/LoginActivity;->a(Landroid/net/Uri;)V

    :cond_6
    :goto_6
    return-void

    :cond_7
    const-string/jumbo v0, "screen_name"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v0, "password"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_8
    iget-object v2, p0, Lcom/twitter/android/LoginActivity;->f:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->g:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto/16 :goto_1

    :cond_9
    const-string/jumbo v0, ""

    goto/16 :goto_2

    :cond_a
    const-string/jumbo v7, "prefill_select_password"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/twitter/android/LoginActivity;->e:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->f:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto/16 :goto_3

    :cond_b
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/twitter/android/LoginActivity;->e:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    goto/16 :goto_3

    :cond_c
    new-array v0, v9, [Ljava/lang/String;

    const-string/jumbo v1, "login:::username:prefill"

    aput-object v1, v0, v8

    invoke-virtual {v3, v5, v6, v0}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_d
    invoke-direct {p0, v9}, Lcom/twitter/android/LoginActivity;->b(I)V

    goto/16 :goto_5

    :cond_e
    const-string/jumbo v0, "reqId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/LoginActivity;->m:Ljava/lang/String;

    const-string/jumbo v0, "oneFaInProg"

    invoke-virtual {p1, v0, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/LoginActivity;->p:Z

    const-string/jumbo v0, "passwordResetLogin"

    invoke-virtual {p1, v0, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/LoginActivity;->q:Z

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/LoginActivity;->m:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/LoginActivity;->b:Lcom/twitter/android/jg;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/aa;->a(Ljava/lang/String;Lcom/twitter/library/client/ah;)V

    goto/16 :goto_6
.end method

.method a(Lcom/twitter/library/client/Session;Ljava/lang/String;)V
    .locals 11

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v4

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    iget-boolean v0, p0, Lcom/twitter/android/LoginActivity;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v8, "accountAuthenticatorResponse"

    invoke-virtual {v0, v8}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountAuthenticatorResponse;

    if-eqz v0, :cond_0

    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v9, "authAccount"

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v9, "accountType"

    sget-object v10, Lcom/twitter/library/util/a;->a:Ljava/lang/String;

    invoke-virtual {v8, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v9, "account_user_info"

    invoke-virtual {v8, v9, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v8}, Landroid/accounts/AccountAuthenticatorResponse;->onResult(Landroid/os/Bundle;)V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/util/q;->a(Landroid/content/Context;)Lcom/twitter/android/util/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/util/r;->a()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v0}, Lcom/twitter/android/util/r;->c()V

    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->k()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->l:Lcom/twitter/android/util/d;

    invoke-interface {v0}, Lcom/twitter/android/util/d;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_4

    move v1, v2

    :goto_0
    if-eqz v1, :cond_5

    const-string/jumbo v0, "select"

    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v8

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const-string/jumbo v10, "login:form:phone_number"

    aput-object v10, v9, v3

    const-string/jumbo v10, ""

    aput-object v10, v9, v2

    const/4 v10, 0x2

    aput-object v0, v9, v10

    invoke-virtual {v8, v6, v7, v9}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    new-instance v0, Lcom/twitter/android/FollowFlowController;

    invoke-direct {v0}, Lcom/twitter/android/FollowFlowController;-><init>()V

    sget-object v8, Lcom/twitter/android/FollowFlowController$Initiator;->a:Lcom/twitter/android/FollowFlowController$Initiator;

    invoke-virtual {v0, v8}, Lcom/twitter/android/FollowFlowController;->a(Lcom/twitter/android/FollowFlowController$Initiator;)Lcom/twitter/android/FollowFlowController;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/android/FollowFlowController;->a(Z)Lcom/twitter/android/FollowFlowController;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/android/FollowFlowController;->b(Z)Lcom/twitter/android/FollowFlowController;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/LoginActivity;->h:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/FollowFlowController;->c(Z)Lcom/twitter/android/FollowFlowController;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/LoginActivity;->l:Lcom/twitter/android/util/d;

    invoke-interface {v1}, Lcom/twitter/android/util/d;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/FollowFlowController;->d(Z)Lcom/twitter/android/FollowFlowController;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/LoginActivity;->l:Lcom/twitter/android/util/d;

    invoke-interface {v1, v3}, Lcom/twitter/android/util/d;->a(Z)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/FollowFlowController;->a([Ljava/lang/String;)Lcom/twitter/android/FollowFlowController;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/FollowFlowController;->b(Landroid/app/Activity;)V

    :cond_2
    :goto_2
    const/4 v0, -0x1

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v8, "sb_account_name"

    invoke-virtual {v1, v8, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/LoginActivity;->setResult(ILandroid/content/Intent;)V

    invoke-static {p0, v6, v7}, Lcom/twitter/library/provider/c;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/provider/c;->a()I

    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v0, v6, v7}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v1, v2, [Ljava/lang/String;

    const-string/jumbo v2, "login::::success"

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/LoginActivity;->a:Z

    if-eqz v1, :cond_3

    const-string/jumbo v1, "sso_sdk"

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->h(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_3
    invoke-virtual {v4, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->o()V

    invoke-virtual {v4}, Lcom/twitter/android/client/c;->p()V

    const-string/jumbo v0, "login::::success"

    invoke-virtual {v4, v6, v7, v0}, Lcom/twitter/android/client/c;->c(JLjava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->l:Lcom/twitter/android/util/d;

    invoke-interface {v0}, Lcom/twitter/android/util/d;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {v4, p1}, Lcom/twitter/android/client/c;->c(Lcom/twitter/library/client/Session;)Ljava/lang/String;

    :goto_3
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->finish()V

    return-void

    :cond_4
    move v1, v3

    goto/16 :goto_0

    :cond_5
    const-string/jumbo v0, "skip"

    goto/16 :goto_1

    :cond_6
    const-string/jumbo v0, "android.intent.extra.INTENT"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    if-eqz v0, :cond_7

    invoke-virtual {p0, v0}, Lcom/twitter/android/LoginActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2

    :cond_7
    iget-boolean v0, p0, Lcom/twitter/android/LoginActivity;->q:Z

    if-nez v0, :cond_8

    invoke-static {p0}, Lcom/twitter/android/SignedOutActivity;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_8
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/twitter/android/MainActivity;->a(Landroid/app/Activity;Landroid/net/Uri;)V

    goto :goto_2

    :cond_9
    invoke-virtual {v4, p1}, Lcom/twitter/android/client/c;->d(Lcom/twitter/library/client/Session;)Ljava/lang/String;

    goto :goto_3
.end method

.method protected a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z

    const v0, 0x7f110014    # com.twitter.android.R.menu.login

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    const/4 v0, 0x1

    return v0
.end method

.method public a(Lhn;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p1}, Lhn;->a()I

    move-result v1

    const v2, 0x7f09031e    # com.twitter.android.R.id.menu_sign_up

    if-ne v1, v2, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/SignUpActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "add_account"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return v0

    :cond_0
    const v2, 0x7f09031f    # com.twitter.android.R.id.menu_about

    if-ne v1, v2, :cond_1

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/AboutActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/twitter/android/LoginActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhn;)Z

    move-result v0

    goto :goto_0
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->g:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->f()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->j:Landroid/view/View;

    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->f()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method public b()Z
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {}, Lcom/twitter/library/client/App;->c()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/twitter/library/client/App;->b()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/twitter/library/client/App;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    const-string/jumbo v2, "android_one_factor_signin_1978"

    new-array v3, v1, [Ljava/lang/String;

    const-string/jumbo v4, "enabled"

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string/jumbo v2, "android_one_factor_signin_1978"

    new-array v3, v1, [Ljava/lang/String;

    const-string/jumbo v4, "enabled_and_prefill_email"

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public c()Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "android_one_factor_signin_1978"

    new-array v3, v0, [Ljava/lang/String;

    const-string/jumbo v4, "enabled_and_prefill_email"

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, -0x1

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/client/BaseFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-ne p2, v2, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/LoginActivity;->a:Z

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "android.intent.extra.INTENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    if-nez v0, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/WelcomeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    :cond_1
    invoke-virtual {p0, v0}, Lcom/twitter/android/LoginActivity;->startActivity(Landroid/content/Intent;)V

    :cond_2
    invoke-virtual {p0, v2, p3}, Lcom/twitter/android/LoginActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->finish()V

    goto :goto_0

    :pswitch_1
    if-ne p2, v2, :cond_0

    invoke-static {p0}, Lcom/twitter/android/SignedOutActivity;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v0, "session"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/Session;

    const-string/jumbo v1, "user"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/LoginActivity;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v2, p3}, Lcom/twitter/android/LoginActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->finish()V

    goto :goto_0

    :pswitch_2
    if-ne p2, v2, :cond_5

    const-string/jumbo v0, "session"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/Session;

    const-string/jumbo v1, "user"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/LoginActivity;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    :cond_4
    :goto_1
    invoke-virtual {p0, v3}, Lcom/twitter/android/LoginActivity;->removeDialog(I)V

    goto :goto_0

    :cond_5
    const/4 v0, 0x2

    if-ne p2, v0, :cond_6

    invoke-direct {p0, v3}, Lcom/twitter/android/LoginActivity;->b(I)V

    goto :goto_1

    :cond_6
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->b()Lcom/twitter/library/client/Session$LoginStatus;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/client/Session$LoginStatus;->b:Lcom/twitter/library/client/Session$LoginStatus;

    if-ne v0, v1, :cond_4

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/Session;)Ljava/lang/String;

    goto :goto_1

    :pswitch_3
    if-ne p2, v2, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/LoginActivity;->a(Landroid/net/Uri;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 3

    iget v0, p0, Lcom/twitter/android/LoginActivity;->k:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/LoginActivity;->b(I)V

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/LoginActivity;->a:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "accountAuthenticatorResponse"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountAuthenticatorResponse;

    if-eqz v0, :cond_1

    const/4 v1, 0x4

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountAuthenticatorResponse;->onError(ILjava/lang/String;)V

    :cond_1
    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    const/4 v3, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/LoggedOutSettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/LoginActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :sswitch_1
    const v0, 0x7f0f02f4    # com.twitter.android.R.string.password_reset_url_refsrc_link

    invoke-direct {p0, v0}, Lcom/twitter/android/LoginActivity;->a(I)V

    goto :goto_0

    :sswitch_2
    iget v0, p0, Lcom/twitter/android/LoginActivity;->k:I

    if-ne v0, v3, :cond_1

    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->g()V

    goto :goto_0

    :cond_1
    invoke-direct {p0, v3}, Lcom/twitter/android/LoginActivity;->b(I)V

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->f:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    :sswitch_3
    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "login:form:::submit"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/twitter/android/LoginActivity;->h()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0900d1 -> :sswitch_1    # com.twitter.android.R.id.password_reset
        0x7f0901c2 -> :sswitch_2    # com.twitter.android.R.id.login_login
        0x7f0901c3 -> :sswitch_3    # com.twitter.android.R.id.login_one_factor
        0x7f0901c5 -> :sswitch_0    # com.twitter.android.R.id.settings_button
    .end sparse-switch
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    const v1, 0x7f0f0237    # com.twitter.android.R.string.login_signing_in

    invoke-virtual {p0, v1}, Lcom/twitter/android/LoginActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onDestroy()V

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/LoginActivity;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->e(Ljava/lang/String;)V

    return-void
.end method

.method public onResume()V
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onResume()V

    iget-boolean v0, p0, Lcom/twitter/android/LoginActivity;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/LoginActivity;->l:Lcom/twitter/android/util/d;

    invoke-interface {v0}, Lcom/twitter/android/util/d;->c()V

    :cond_0
    sget-object v0, Lcom/twitter/android/jf;->a:[I

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->b()Lcom/twitter/library/client/Session$LoginStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session$LoginStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_0
    return-void

    :pswitch_0
    const-string/jumbo v0, "android.intent.action.MAIN"

    invoke-virtual {p0}, Lcom/twitter/android/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/LoginActivity;->setResult(I)V

    sget-object v0, Lcom/twitter/android/MainActivity;->a:Landroid/net/Uri;

    invoke-static {p0, v0}, Lcom/twitter/android/MainActivity;->a(Landroid/app/Activity;Landroid/net/Uri;)V

    goto :goto_0

    :pswitch_1
    iget-boolean v0, p0, Lcom/twitter/android/LoginActivity;->p:Z

    if-nez v0, :cond_1

    invoke-virtual {p0, v2}, Lcom/twitter/android/LoginActivity;->showDialog(I)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, v2}, Lcom/twitter/android/LoginActivity;->removeDialog(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "reqId"

    iget-object v1, p0, Lcom/twitter/android/LoginActivity;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "oneFaInProg"

    iget-boolean v1, p0, Lcom/twitter/android/LoginActivity;->p:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "passwordResetLogin"

    iget-boolean v1, p0, Lcom/twitter/android/LoginActivity;->q:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
