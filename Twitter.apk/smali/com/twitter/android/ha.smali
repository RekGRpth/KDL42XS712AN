.class Lcom/twitter/android/ha;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/GalleryActivity;


# direct methods
.method private constructor <init>(Lcom/twitter/android/GalleryActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/ha;->a:Lcom/twitter/android/GalleryActivity;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/GalleryActivity;Lcom/twitter/android/gt;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/ha;-><init>(Lcom/twitter/android/GalleryActivity;)V

    return-void
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/twitter/android/ha;->a:Lcom/twitter/android/GalleryActivity;

    invoke-static {v2}, Lcom/twitter/android/GalleryActivity;->e(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/android/hd;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/hd;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/twitter/android/ha;->a:Lcom/twitter/android/GalleryActivity;

    invoke-static {v2}, Lcom/twitter/android/GalleryActivity;->e(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/android/hd;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/twitter/android/hd;->a(Z)V

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-static {}, Lcom/twitter/library/util/Util;->f()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/ha;->a:Lcom/twitter/android/GalleryActivity;

    iget-object v3, p0, Lcom/twitter/android/ha;->a:Lcom/twitter/android/GalleryActivity;

    invoke-static {v3}, Lcom/twitter/android/GalleryActivity;->g(Lcom/twitter/android/GalleryActivity;)Z

    move-result v3

    if-nez v3, :cond_2

    move v0, v1

    :cond_2
    invoke-virtual {v2, v0}, Lcom/twitter/android/GalleryActivity;->a(Z)V

    goto :goto_0
.end method

.method private b(Landroid/view/MotionEvent;)Z
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/ha;->a:Lcom/twitter/android/GalleryActivity;

    iget-object v0, v0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    iget-boolean v0, v0, Lcom/twitter/library/provider/Tweet;->l:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ha;->a:Lcom/twitter/android/GalleryActivity;

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/twitter/android/ha;->a:Lcom/twitter/android/GalleryActivity;

    iget-object v2, v2, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    invoke-static {v0, v1, v2}, Lcom/twitter/android/GalleryActivity;->a(Lcom/twitter/android/GalleryActivity;ILcom/twitter/library/provider/Tweet;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ha;->a:Lcom/twitter/android/GalleryActivity;

    iget-object v0, v0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    iget-boolean v0, v0, Lcom/twitter/library/provider/Tweet;->l:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/ha;->a:Lcom/twitter/android/GalleryActivity;

    iget-object v2, p0, Lcom/twitter/android/ha;->a:Lcom/twitter/android/GalleryActivity;

    invoke-static {v2}, Lcom/twitter/android/GalleryActivity;->f(Lcom/twitter/android/GalleryActivity;)Landroid/view/ViewGroup;

    move-result-object v2

    const v3, 0x7f0201f2    # com.twitter.android.R.drawable.ic_photo_action_overlay_favorite_on

    const v4, 0x7f040006    # com.twitter.android.R.anim.double_tap_favorite

    invoke-static {v1, v2, v3, v4, v0}, Ljw;->a(Landroid/content/Context;Landroid/view/ViewGroup;IILjava/lang/Runnable;)V

    const/4 v0, 0x1

    return v0

    :cond_1
    new-instance v0, Lcom/twitter/android/gx;

    iget-object v1, p0, Lcom/twitter/android/ha;->a:Lcom/twitter/android/GalleryActivity;

    iget-object v2, p0, Lcom/twitter/android/ha;->a:Lcom/twitter/android/GalleryActivity;

    iget-object v2, v2, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    iget-object v3, p0, Lcom/twitter/android/ha;->a:Lcom/twitter/android/GalleryActivity;

    invoke-static {v3}, Lcom/twitter/android/GalleryActivity;->h(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/android/gx;-><init>(Lcom/twitter/android/GalleryActivity;Lcom/twitter/library/provider/Tweet;Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;)V

    goto :goto_0
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-static {}, Ljy;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/twitter/android/ha;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/android/ha;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-static {}, Ljy;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/twitter/android/ha;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-static {}, Ljy;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/twitter/android/ha;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
