.class public Lcom/twitter/android/ja;
.super Landroid/os/AsyncTask;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:J

.field private c:Lcom/twitter/android/jb;


# direct methods
.method public constructor <init>(Landroid/content/Context;JLcom/twitter/android/jb;)V
    .locals 0

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/ja;->a:Landroid/content/Context;

    iput-wide p2, p0, Lcom/twitter/android/ja;->b:J

    iput-object p4, p0, Lcom/twitter/android/ja;->c:Lcom/twitter/android/jb;

    return-void
.end method


# virtual methods
.method protected varargs a([Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 7

    const/4 v0, 0x0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lcom/twitter/android/ja;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/ContactsContract$Profile;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/provider/ContactsContract$Contacts;->openContactPhotoInputStream(Landroid/content/ContentResolver;Landroid/net/Uri;Z)Ljava/io/InputStream;

    move-result-object v2

    if-nez v2, :cond_1

    iget-object v1, p0, Lcom/twitter/android/ja;->c:Lcom/twitter/android/jb;

    invoke-interface {v1}, Lcom/twitter/android/jb;->a()V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/ja;->a:Landroid/content/Context;

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/twitter/android/ja;->b:J

    invoke-static {v1, v3, v4, v5}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;ZJ)Ljava/io/File;

    move-result-object v3

    if-nez v3, :cond_2

    iget-object v1, p0, Lcom/twitter/android/ja;->c:Lcom/twitter/android/jb;

    invoke-interface {v1}, Lcom/twitter/android/jb;->a()V

    goto :goto_0

    :cond_2
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v4, 0x1000

    :try_start_1
    invoke-static {v2, v1, v4}, Lcom/twitter/internal/util/h;->a(Ljava/io/InputStream;Ljava/io/OutputStream;I)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-static {v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    iget-object v0, p0, Lcom/twitter/android/ja;->c:Lcom/twitter/android/jb;

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/jb;->a(Landroid/net/Uri;)V

    invoke-static {v3}, Lkw;->a(Ljava/io/File;)Lkw;

    move-result-object v0

    invoke-virtual {v0}, Lkw;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    move-object v1, v0

    :goto_1
    :try_start_2
    iget-object v3, p0, Lcom/twitter/android/ja;->c:Lcom/twitter/android/jb;

    invoke-interface {v3}, Lcom/twitter/android/jb;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-static {v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_2
    invoke-static {v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v3

    goto :goto_1
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lcom/twitter/android/ja;->a([Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
