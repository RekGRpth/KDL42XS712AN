.class Lcom/twitter/android/qw;
.super Lcom/twitter/android/yb;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/ProfileFragment;


# direct methods
.method public constructor <init>(Lcom/twitter/android/ProfileFragment;Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/qw;->a:Lcom/twitter/android/ProfileFragment;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/twitter/android/yb;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(JJLjava/lang/String;Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/widget/TweetView;)V
    .locals 8

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/twitter/android/qw;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    if-nez v7, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/qw;->a:Lcom/twitter/android/ProfileFragment;

    iget-wide v0, v0, Lcom/twitter/android/ProfileFragment;->I:J

    cmp-long v0, p3, v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/qw;->c:Lcom/twitter/android/client/c;

    iget-object v0, p0, Lcom/twitter/android/qw;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v1, p0, Lcom/twitter/android/qw;->f:Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/twitter/android/qw;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    move-wide v2, p3

    move-object v4, p6

    invoke-static/range {v0 .. v6}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    new-instance v0, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->a(I)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    const-string/jumbo v1, "profile"

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    const-string/jumbo v1, "tweets"

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v5

    iget-object v0, p0, Lcom/twitter/android/qw;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->ab()Z

    move-result v6

    move-object v0, v7

    move-wide v1, p3

    move-object v3, p5

    move-object v4, p6

    invoke-static/range {v0 .. v6}, Lcom/twitter/android/ProfileActivity;->a(Landroid/content/Context;JLjava/lang/String;Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/scribe/ScribeAssociation;Z)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/qw;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->getView()Landroid/view/View;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/qw;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->M(Lcom/twitter/android/ProfileFragment;)Landroid/view/animation/TranslateAnimation;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {v7}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d0004    # com.twitter.android.R.integer.bounceAnimTime

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    const v3, 0x7f0c0014    # com.twitter.android.R.dimen.bounceTravelDistance

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    neg-float v4, v3

    invoke-direct {v0, v4, v6, v6, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    int-to-long v4, v2

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    new-instance v4, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {v4}, Landroid/view/animation/OvershootInterpolator;-><init>()V

    invoke-virtual {v0, v4}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    new-instance v4, Lcom/twitter/android/qx;

    invoke-direct {v4, p0, v1, v0}, Lcom/twitter/android/qx;-><init>(Lcom/twitter/android/qw;Landroid/view/View;Landroid/view/animation/TranslateAnimation;)V

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    neg-float v3, v3

    invoke-direct {v0, v6, v3, v6, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    new-instance v2, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v2}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v2}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    invoke-virtual {v0, v4}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v2, p0, Lcom/twitter/android/qw;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v2, v0}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/android/ProfileFragment;Landroid/view/animation/TranslateAnimation;)Landroid/view/animation/TranslateAnimation;

    :cond_2
    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0
.end method
