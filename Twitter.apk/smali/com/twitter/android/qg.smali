.class Lcom/twitter/android/qg;
.super Lcom/twitter/library/view/g;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/widget/PipView;

.field final synthetic b:Lcom/twitter/android/ProfileFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/ProfileFragment;Landroid/view/ViewParent;ILcom/twitter/android/widget/PipView;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/qg;->b:Lcom/twitter/android/ProfileFragment;

    iput-object p4, p0, Lcom/twitter/android/qg;->a:Lcom/twitter/android/widget/PipView;

    invoke-direct {p0, p2, p3}, Lcom/twitter/library/view/g;-><init>(Landroid/view/ViewParent;I)V

    return-void
.end method


# virtual methods
.method public onPageScrolled(IFI)V
    .locals 2

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/library/view/g;->onPageScrolled(IFI)V

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qg;->b:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->j:Lcom/twitter/android/widget/ProfileHeader;

    invoke-virtual {v0, p2}, Lcom/twitter/android/widget/ProfileHeader;->setAlphaFloat(F)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/qg;->b:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->j:Lcom/twitter/android/widget/ProfileHeader;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ProfileHeader;->setAlphaFloat(F)V

    goto :goto_0
.end method

.method public onPageSelected(I)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/qg;->b:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/qt;

    invoke-virtual {v0}, Lcom/twitter/android/qt;->getCount()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/twitter/android/qg;->a:Lcom/twitter/android/widget/PipView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/PipView;->setVisibility(I)V

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qg;->a:Lcom/twitter/android/widget/PipView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/PipView;->setPipOnPosition(I)V

    :goto_0
    invoke-super {p0, p1}, Lcom/twitter/library/view/g;->onPageSelected(I)V

    return-void

    :cond_0
    if-ne p1, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/qg;->a:Lcom/twitter/android/widget/PipView;

    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/PipView;->setPipOnPosition(I)V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Number of items is not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/qg;->a:Lcom/twitter/android/widget/PipView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PipView;->setVisibility(I)V

    goto :goto_0
.end method
