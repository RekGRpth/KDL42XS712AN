.class Lcom/twitter/android/xt;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lcom/twitter/android/xr;


# direct methods
.method constructor <init>(Lcom/twitter/android/xr;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/xt;->b:Lcom/twitter/android/xr;

    iput-object p2, p0, Lcom/twitter/android/xt;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/twitter/android/xt;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/twitter/android/yd;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/twitter/android/yd;

    iget-object v0, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xt;->b:Lcom/twitter/android/xr;

    invoke-static {v0, v2}, Lcom/twitter/android/xr;->a(Lcom/twitter/android/xr;Z)Z

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/xt;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    return v2
.end method
