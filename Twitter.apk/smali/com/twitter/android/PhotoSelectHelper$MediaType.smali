.class public final enum Lcom/twitter/android/PhotoSelectHelper$MediaType;
.super Ljava/lang/Enum;
.source "Twttr"


# static fields
.field public static final enum a:Lcom/twitter/android/PhotoSelectHelper$MediaType;

.field public static final enum b:Lcom/twitter/android/PhotoSelectHelper$MediaType;

.field public static final c:Ljava/util/EnumSet;

.field public static final d:Ljava/util/EnumSet;

.field private static final synthetic e:[Lcom/twitter/android/PhotoSelectHelper$MediaType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/android/PhotoSelectHelper$MediaType;

    const-string/jumbo v1, "IMAGE"

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/PhotoSelectHelper$MediaType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/PhotoSelectHelper$MediaType;->a:Lcom/twitter/android/PhotoSelectHelper$MediaType;

    new-instance v0, Lcom/twitter/android/PhotoSelectHelper$MediaType;

    const-string/jumbo v1, "ANIMATED_GIF"

    invoke-direct {v0, v1, v3}, Lcom/twitter/android/PhotoSelectHelper$MediaType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/PhotoSelectHelper$MediaType;->b:Lcom/twitter/android/PhotoSelectHelper$MediaType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/twitter/android/PhotoSelectHelper$MediaType;

    sget-object v1, Lcom/twitter/android/PhotoSelectHelper$MediaType;->a:Lcom/twitter/android/PhotoSelectHelper$MediaType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/twitter/android/PhotoSelectHelper$MediaType;->b:Lcom/twitter/android/PhotoSelectHelper$MediaType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/twitter/android/PhotoSelectHelper$MediaType;->e:[Lcom/twitter/android/PhotoSelectHelper$MediaType;

    sget-object v0, Lcom/twitter/android/PhotoSelectHelper$MediaType;->a:Lcom/twitter/android/PhotoSelectHelper$MediaType;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/PhotoSelectHelper$MediaType;->c:Ljava/util/EnumSet;

    const-class v0, Lcom/twitter/android/PhotoSelectHelper$MediaType;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/PhotoSelectHelper$MediaType;->d:Ljava/util/EnumSet;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/android/PhotoSelectHelper$MediaType;
    .locals 1

    const-class v0, Lcom/twitter/android/PhotoSelectHelper$MediaType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/PhotoSelectHelper$MediaType;

    return-object v0
.end method

.method public static values()[Lcom/twitter/android/PhotoSelectHelper$MediaType;
    .locals 1

    sget-object v0, Lcom/twitter/android/PhotoSelectHelper$MediaType;->e:[Lcom/twitter/android/PhotoSelectHelper$MediaType;

    invoke-virtual {v0}, [Lcom/twitter/android/PhotoSelectHelper$MediaType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/PhotoSelectHelper$MediaType;

    return-object v0
.end method
