.class Lcom/twitter/android/sh;
.super Landroid/widget/BaseAdapter;
.source "Twttr"


# static fields
.field private static final a:Ljava/util/HashMap;

.field private static final b:Ljava/util/HashMap;


# instance fields
.field private A:Z

.field private B:Z

.field private C:J

.field private D:Z

.field private E:I

.field private F:I

.field private G:Ljava/lang/String;

.field private H:Ljava/util/HashSet;

.field private final I:Ljava/util/ArrayList;

.field private final J:Lcom/twitter/library/util/aa;

.field private final K:Ljava/lang/String;

.field private final L:Lcom/twitter/android/ny;

.field private final M:Lcom/twitter/android/zc;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/twitter/android/client/c;

.field private final e:Lcom/twitter/library/client/aa;

.field private final f:Lcom/twitter/library/widget/ap;

.field private final g:Lcom/twitter/android/vs;

.field private final h:Lcom/twitter/library/util/FriendshipCache;

.field private final i:Lcom/twitter/android/ob;

.field private final j:Landroid/widget/AdapterView$OnItemClickListener;

.field private final k:Lcom/twitter/android/zb;

.field private final l:Ljava/util/ArrayList;

.field private final m:Ljava/util/ArrayList;

.field private final n:Ljava/util/ArrayList;

.field private final o:Ljava/util/ArrayList;

.field private final p:Ljava/util/ArrayList;

.field private final q:Ljava/lang/String;

.field private final r:I

.field private final s:Lcom/twitter/library/scribe/ScribeAssociation;

.field private final t:Z

.field private final u:Z

.field private final v:Z

.field private w:Lcom/twitter/library/widget/aa;

.field private x:Lcom/twitter/library/widget/aa;

.field private y:Lcom/twitter/library/widget/aa;

.field private z:Landroid/database/Cursor;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/twitter/android/sh;->a:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/twitter/android/sh;->b:Ljava/util/HashMap;

    sget-object v0, Lcom/twitter/android/sh;->a:Ljava/util/HashMap;

    const-string/jumbo v1, "event_parrot"

    const v2, 0x7f0f0161    # com.twitter.android.R.string.event_parrot_news_proof

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/sh;->b:Ljava/util/HashMap;

    const-string/jumbo v1, "map_pin"

    const v2, 0x7f0201c2    # com.twitter.android.R.drawable.ic_highlight_context_nearby

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/sh;->b:Ljava/util/HashMap;

    const-string/jumbo v1, "newspaper"

    const v2, 0x7f02011a    # com.twitter.android.R.drawable.ic_activity_news_tweet

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/sh;->b:Ljava/util/HashMap;

    const-string/jumbo v1, "speech_bubble"

    const v2, 0x7f02011b    # com.twitter.android.R.drawable.ic_activity_reply_tweet

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/sh;->b:Ljava/util/HashMap;

    const-string/jumbo v1, "head"

    const v2, 0x7f020117    # com.twitter.android.R.drawable.ic_activity_follow_tweet_default

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/sh;->b:Ljava/util/HashMap;

    const-string/jumbo v1, "megaphone"

    const v2, 0x7f02011e    # com.twitter.android.R.drawable.ic_activity_top_tweet

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/twitter/android/client/c;Ljava/lang/String;Lcom/twitter/library/widget/ap;Lcom/twitter/android/vs;Lcom/twitter/library/util/FriendshipCache;Lcom/twitter/android/ob;Landroid/widget/AdapterView$OnItemClickListener;Lcom/twitter/android/zb;Lcom/twitter/android/ny;IZZLjava/lang/String;Lcom/twitter/library/util/aa;ZLjava/util/HashSet;Ljava/lang/String;ZZ)V
    .locals 4

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/sh;->l:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/sh;->m:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/sh;->n:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/sh;->o:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/sh;->p:Ljava/util/ArrayList;

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/twitter/android/sh;->C:J

    new-instance v1, Lcom/twitter/android/sn;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/twitter/android/sn;-><init>(Lcom/twitter/android/si;)V

    iput-object v1, p0, Lcom/twitter/android/sh;->M:Lcom/twitter/android/zc;

    iput-object p1, p0, Lcom/twitter/android/sh;->c:Landroid/content/Context;

    iput-object p2, p0, Lcom/twitter/android/sh;->d:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/sh;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/sh;->e:Lcom/twitter/library/client/aa;

    iput-object p3, p0, Lcom/twitter/android/sh;->q:Ljava/lang/String;

    iput-object p4, p0, Lcom/twitter/android/sh;->f:Lcom/twitter/library/widget/ap;

    iput-object p5, p0, Lcom/twitter/android/sh;->g:Lcom/twitter/android/vs;

    iput-object p6, p0, Lcom/twitter/android/sh;->h:Lcom/twitter/library/util/FriendshipCache;

    iput-object p7, p0, Lcom/twitter/android/sh;->i:Lcom/twitter/android/ob;

    iput-object p8, p0, Lcom/twitter/android/sh;->j:Landroid/widget/AdapterView$OnItemClickListener;

    iput-object p9, p0, Lcom/twitter/android/sh;->k:Lcom/twitter/android/zb;

    iput p11, p0, Lcom/twitter/android/sh;->r:I

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/twitter/android/sh;->K:Ljava/lang/String;

    move/from16 v0, p19

    iput-boolean v0, p0, Lcom/twitter/android/sh;->v:Z

    iget v1, p0, Lcom/twitter/android/sh;->r:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    new-instance v1, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v1}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeAssociation;->a(I)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/sh;->e:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeAssociation;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v1

    const-string/jumbo v2, "search"

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v1

    const-string/jumbo v2, "people"

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/sh;->s:Lcom/twitter/library/scribe/ScribeAssociation;

    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/twitter/android/sh;->z:Landroid/database/Cursor;

    move/from16 v0, p12

    iput-boolean v0, p0, Lcom/twitter/android/sh;->t:Z

    move/from16 v0, p13

    iput-boolean v0, p0, Lcom/twitter/android/sh;->u:Z

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/twitter/android/sh;->G:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/sh;->G:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/sh;->c:Landroid/content/Context;

    instance-of v1, v1, Lcom/twitter/android/EventSearchActivity;

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    iput-boolean v1, p0, Lcom/twitter/android/sh;->D:Z

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/sh;->I:Ljava/util/ArrayList;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/twitter/android/sh;->J:Lcom/twitter/library/util/aa;

    iput-object p10, p0, Lcom/twitter/android/sh;->L:Lcom/twitter/android/ny;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/twitter/android/sh;->H:Ljava/util/HashSet;

    move/from16 v0, p20

    iput-boolean v0, p0, Lcom/twitter/android/sh;->A:Z

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->at()Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/sh;->B:Z

    return-void

    :cond_0
    new-instance v1, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v1}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeAssociation;->a(I)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v1

    const-string/jumbo v2, "search"

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v1

    const-string/jumbo v2, "universal"

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/sh;->s:Lcom/twitter/library/scribe/ScribeAssociation;

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private a(ILandroid/view/View;Landroid/view/ViewGroup;Lcom/twitter/android/sp;Landroid/content/Context;I)Landroid/view/View;
    .locals 4

    if-nez p2, :cond_0

    invoke-static {p5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p6, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    new-instance v3, Lcom/twitter/android/sk;

    const v1, 0x7f090090    # com.twitter.android.R.id.title

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0900da    # com.twitter.android.R.id.subtitle

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/GroupedRowView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-direct {v3, v1, v2}, Lcom/twitter/android/sk;-><init>(Landroid/widget/TextView;Landroid/widget/TextView;)V

    invoke-virtual {v0, v3}, Lcom/twitter/internal/android/widget/GroupedRowView;->setTag(Ljava/lang/Object;)V

    move-object p2, v0

    move-object v0, v3

    :goto_0
    iget-object v1, v0, Lcom/twitter/android/sk;->f:Landroid/widget/TextView;

    iget-object v2, p4, Lcom/twitter/android/sp;->k:Lcom/twitter/library/api/af;

    iget-object v2, v2, Lcom/twitter/library/api/af;->d:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/twitter/android/sh;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    iget-object v1, v0, Lcom/twitter/android/sk;->g:Landroid/widget/TextView;

    iget-object v2, p4, Lcom/twitter/android/sp;->k:Lcom/twitter/library/api/af;

    iget-object v2, v2, Lcom/twitter/library/api/af;->e:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/twitter/android/sh;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    iput-object p4, v0, Lcom/twitter/android/sk;->l:Lcom/twitter/android/sp;

    return-object p2

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/sk;

    goto :goto_0
.end method

.method private a(ILandroid/view/View;Landroid/view/ViewGroup;Lcom/twitter/android/sp;Landroid/content/Context;Landroid/database/Cursor;)Landroid/view/View;
    .locals 6

    const/4 v4, 0x1

    const/4 v3, 0x0

    sget v0, Lcom/twitter/library/provider/bs;->z:I

    invoke-interface {p6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    if-nez p2, :cond_4

    invoke-direct {p0, v5}, Lcom/twitter/android/sh;->c(I)I

    move-result v0

    invoke-static {p5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, v0, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    const v1, 0x7f09015b    # com.twitter.android.R.id.event_view

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/widget/TopicView;

    new-instance v2, Lcom/twitter/android/sk;

    invoke-direct {v2, v1}, Lcom/twitter/android/sk;-><init>(Lcom/twitter/android/widget/TopicView;)V

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/GroupedRowView;->setTag(Ljava/lang/Object;)V

    move-object p2, v0

    move-object v0, v2

    :goto_0
    iput-object p4, v0, Lcom/twitter/android/sk;->l:Lcom/twitter/android/sp;

    invoke-direct {p0, v1, p6}, Lcom/twitter/android/sh;->a(Lcom/twitter/android/widget/TopicView;Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/twitter/android/sh;->p:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x6

    if-eq v5, v0, :cond_0

    const/4 v0, 0x7

    if-ne v5, v0, :cond_6

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/sh;->l:Ljava/util/ArrayList;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/sp;

    iget v0, v0, Lcom/twitter/android/sp;->b:I

    const/16 v1, 0x1f

    if-eq v0, v1, :cond_5

    :cond_1
    move v0, v4

    :goto_1
    move v4, v0

    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/twitter/android/sh;->i:Lcom/twitter/android/ob;

    if-eqz v0, :cond_3

    if-eqz v4, :cond_3

    iget-object v0, p0, Lcom/twitter/android/sh;->i:Lcom/twitter/android/ob;

    const/4 v1, 0x0

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {v0, p2, v1, v2}, Lcom/twitter/android/ob;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    :cond_3
    return-object p2

    :cond_4
    check-cast p2, Lcom/twitter/internal/android/widget/GroupedRowView;

    invoke-virtual {p2}, Lcom/twitter/internal/android/widget/GroupedRowView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/sk;

    iget-object v1, v0, Lcom/twitter/android/sk;->j:Lcom/twitter/android/widget/TopicView;

    goto :goto_0

    :cond_5
    move v0, v3

    goto :goto_1

    :cond_6
    if-nez p4, :cond_2

    move v4, v3

    goto :goto_2
.end method

.method private a(Landroid/content/Context;ILcom/twitter/android/sp;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    if-nez p4, :cond_8

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030096    # com.twitter.android.R.layout.grouped_tweet_row_view

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    new-instance v1, Lcom/twitter/android/sk;

    new-instance v2, Lcom/twitter/android/yd;

    invoke-direct {v2, v0}, Lcom/twitter/android/yd;-><init>(Landroid/view/View;)V

    invoke-direct {v1, v2}, Lcom/twitter/android/sk;-><init>(Lcom/twitter/android/yd;)V

    iget-object v2, v1, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iget-object v2, v2, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v3, p0, Lcom/twitter/android/sh;->f:Lcom/twitter/library/widget/ap;

    invoke-virtual {v2, v3}, Lcom/twitter/library/widget/TweetView;->setProvider(Lcom/twitter/library/widget/ap;)V

    iget v2, p3, Lcom/twitter/android/sp;->b:I

    sparse-switch v2, :sswitch_data_0

    iget-object v2, v1, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iget-object v2, v2, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v3, p0, Lcom/twitter/android/sh;->w:Lcom/twitter/library/widget/aa;

    invoke-virtual {v2, v3}, Lcom/twitter/library/widget/TweetView;->setOnProfileImageClickListener(Lcom/twitter/library/widget/aa;)V

    :goto_0
    iget-object v2, p0, Lcom/twitter/android/sh;->m:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/ref/WeakReference;

    iget-object v4, v1, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    invoke-direct {v3, v4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->setTag(Ljava/lang/Object;)V

    move-object v2, v1

    move-object v1, v0

    :goto_1
    iput-object p3, v2, Lcom/twitter/android/sk;->l:Lcom/twitter/android/sp;

    iget-object v3, p0, Lcom/twitter/android/sh;->z:Landroid/database/Cursor;

    iget v0, p3, Lcom/twitter/android/sp;->d:I

    invoke-interface {v3, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v4, p0, Lcom/twitter/android/sh;->d:Lcom/twitter/android/client/c;

    invoke-virtual {v4}, Lcom/twitter/android/client/c;->af()Z

    move-result v5

    iget v6, p0, Lcom/twitter/android/sh;->r:I

    sget v0, Lcom/twitter/library/provider/bs;->K:I

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    sget-object v0, Lcom/twitter/android/sh;->b:Ljava/util/HashMap;

    sget v8, Lcom/twitter/library/provider/bs;->L:I

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-boolean v8, p0, Lcom/twitter/android/sh;->B:Z

    if-eqz v8, :cond_9

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_9

    if-eqz v0, :cond_9

    iget-object v8, v2, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iget-object v8, v8, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v8, v7}, Lcom/twitter/library/widget/TweetView;->setReason(Ljava/lang/String;)V

    iget-object v8, v2, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iget-object v8, v8, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v8, v0}, Lcom/twitter/library/widget/TweetView;->setReasonIconResId(I)V

    :goto_2
    iget-object v0, v2, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iget-object v0, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v4}, Lcom/twitter/android/client/c;->ad()Z

    move-result v8

    invoke-virtual {v0, v8}, Lcom/twitter/library/widget/TweetView;->setSmartCrop(Z)V

    iget-object v0, v2, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iget-object v0, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v4}, Lcom/twitter/android/client/c;->V()F

    move-result v8

    invoke-virtual {v0, v8}, Lcom/twitter/library/widget/TweetView;->setContentSize(F)V

    iget-object v0, v2, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iget-object v0, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    iget-boolean v8, v4, Lcom/twitter/android/client/c;->f:Z

    invoke-virtual {v0, v8}, Lcom/twitter/library/widget/TweetView;->setRenderRTL(Z)V

    iget-object v0, v2, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iget-object v0, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v4}, Lcom/twitter/android/client/c;->al()Z

    move-result v4

    invoke-virtual {v0, v4}, Lcom/twitter/library/widget/TweetView;->setAmplifyEnabled(Z)V

    new-instance v4, Lcom/twitter/library/provider/Tweet;

    invoke-direct {v4, v3}, Lcom/twitter/library/provider/Tweet;-><init>(Landroid/database/Cursor;)V

    if-nez v5, :cond_0

    invoke-virtual {v4}, Lcom/twitter/library/provider/Tweet;->u()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/sh;->u:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/sh;->t:Z

    if-nez v0, :cond_1

    iget v0, p3, Lcom/twitter/android/sp;->b:I

    const/4 v3, 0x4

    if-eq v0, v3, :cond_1

    const/4 v0, 0x3

    if-eq v6, v0, :cond_1

    const/4 v0, 0x4

    if-eq v6, v0, :cond_1

    const/4 v0, 0x5

    if-eq v6, v0, :cond_1

    const/4 v0, 0x6

    if-ne v6, v0, :cond_a

    :cond_1
    const/4 v0, 0x1

    :goto_3
    iget-object v3, v2, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iget-object v3, v3, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v3, v0}, Lcom/twitter/library/widget/TweetView;->setAlwaysExpand(Z)V

    iget-object v3, v2, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iget-object v3, v3, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v3, v0}, Lcom/twitter/library/widget/TweetView;->setAlwaysExpandMedia(Z)V

    iget-object v0, v2, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iget-object v0, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v3, p0, Lcom/twitter/android/sh;->e:Lcom/twitter/library/client/aa;

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v4, p1, v3}, Lcom/twitter/library/provider/Tweet;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/twitter/library/widget/TweetView;->setWillTranslate(Z)V

    if-eqz v5, :cond_2

    iget v0, v4, Lcom/twitter/library/provider/Tweet;->I:I

    and-int/lit8 v0, v0, -0x9

    iput v0, v4, Lcom/twitter/library/provider/Tweet;->I:I

    :cond_2
    iget v0, p3, Lcom/twitter/android/sp;->b:I

    const/16 v3, 0x18

    if-ne v0, v3, :cond_3

    const/4 v0, 0x0

    iput-object v0, v4, Lcom/twitter/library/provider/Tweet;->X:Ljava/lang/String;

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/sh;->h:Lcom/twitter/library/util/FriendshipCache;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/sh;->h:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0, v4}, Lcom/twitter/library/util/FriendshipCache;->a(Lcom/twitter/library/provider/Tweet;)V

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/sh;->d:Lcom/twitter/android/client/c;

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->ab()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, v2, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iget-object v0, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v3, p0, Lcom/twitter/android/sh;->h:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0, v3}, Lcom/twitter/library/widget/TweetView;->setFriendshipCache(Lcom/twitter/library/util/FriendshipCache;)V

    :cond_5
    iget-object v0, v2, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iget-object v0, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0, v4}, Lcom/twitter/library/widget/TweetView;->setTweet(Lcom/twitter/library/provider/Tweet;)V

    iget-object v0, p0, Lcom/twitter/android/sh;->i:Lcom/twitter/android/ob;

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/twitter/android/sh;->B:Z

    if-eqz v0, :cond_b

    new-instance v0, Landroid/os/Bundle;

    const/4 v3, 0x2

    invoke-direct {v0, v3}, Landroid/os/Bundle;-><init>(I)V

    const-string/jumbo v3, "reason_text"

    invoke-virtual {v0, v3, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :goto_4
    const-string/jumbo v3, "position"

    invoke-virtual {v0, v3, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v3, p0, Lcom/twitter/android/sh;->i:Lcom/twitter/android/ob;

    const/4 v4, 0x0

    invoke-interface {v3, v1, v4, v0}, Lcom/twitter/android/ob;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    :cond_6
    iget-object v0, p0, Lcom/twitter/android/sh;->g:Lcom/twitter/android/vs;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/twitter/android/sh;->g:Lcom/twitter/android/vs;

    iget-object v2, v2, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iget v3, p3, Lcom/twitter/android/sp;->c:I

    int-to-long v3, v3

    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/android/vs;->b(Lcom/twitter/android/yd;J)V

    :cond_7
    return-object v1

    :sswitch_0
    iget-object v2, v1, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iget-object v2, v2, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v3, p0, Lcom/twitter/android/sh;->x:Lcom/twitter/library/widget/aa;

    invoke-virtual {v2, v3}, Lcom/twitter/library/widget/TweetView;->setOnProfileImageClickListener(Lcom/twitter/library/widget/aa;)V

    goto/16 :goto_0

    :sswitch_1
    iget-object v2, v1, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iget-object v2, v2, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v3, p0, Lcom/twitter/android/sh;->y:Lcom/twitter/library/widget/aa;

    invoke-virtual {v2, v3}, Lcom/twitter/library/widget/TweetView;->setOnProfileImageClickListener(Lcom/twitter/library/widget/aa;)V

    goto/16 :goto_0

    :cond_8
    move-object v0, p4

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    invoke-virtual {p4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/sk;

    move-object v2, v1

    move-object v1, v0

    goto/16 :goto_1

    :cond_9
    iget-object v0, v2, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iget-object v0, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    const/4 v8, 0x0

    invoke-virtual {v0, v8}, Lcom/twitter/library/widget/TweetView;->setReason(Ljava/lang/String;)V

    iget-object v0, v2, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iget-object v0, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    const/4 v8, 0x0

    invoke-virtual {v0, v8}, Lcom/twitter/library/widget/TweetView;->setReasonIconResId(I)V

    goto/16 :goto_2

    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_3

    :cond_b
    new-instance v0, Landroid/os/Bundle;

    const/4 v3, 0x1

    invoke-direct {v0, v3}, Landroid/os/Bundle;-><init>(I)V

    goto :goto_4

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x9 -> :sswitch_1
    .end sparse-switch
.end method

.method private a(Landroid/content/Context;ILcom/twitter/android/sp;Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 7

    const/4 v6, 0x1

    if-nez p4, :cond_2

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p6, p5, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    new-instance v2, Lcom/twitter/android/sk;

    const v1, 0x7f0900db    # com.twitter.android.R.id.photo_list

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/internal/android/widget/HorizontalListView;

    invoke-direct {v2, v1}, Lcom/twitter/android/sk;-><init>(Lcom/twitter/internal/android/widget/HorizontalListView;)V

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/GroupedRowView;->setTag(Ljava/lang/Object;)V

    move-object p4, v0

    move-object v1, v2

    :goto_0
    iput-object p3, v1, Lcom/twitter/android/sk;->l:Lcom/twitter/android/sp;

    iget-object v0, v1, Lcom/twitter/android/sk;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ud;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/ud;

    iget-object v2, p0, Lcom/twitter/android/sh;->J:Lcom/twitter/library/util/aa;

    invoke-direct {v0, p1, v6, v2}, Lcom/twitter/android/ud;-><init>(Landroid/content/Context;ZLcom/twitter/library/util/aa;)V

    iget-object v2, p0, Lcom/twitter/android/sh;->I:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, v1, Lcom/twitter/android/sk;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    invoke-virtual {v2, v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v2, v1, Lcom/twitter/android/sk;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    iget-object v3, p0, Lcom/twitter/android/sh;->j:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v2, v3}, Lcom/twitter/internal/android/widget/HorizontalListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    :cond_0
    new-instance v2, Lcom/twitter/library/provider/s;

    iget-object v3, p0, Lcom/twitter/android/sh;->z:Landroid/database/Cursor;

    iget v4, p3, Lcom/twitter/android/sp;->d:I

    iget v5, p3, Lcom/twitter/android/sp;->e:I

    invoke-direct {v2, v3, v4, v5}, Lcom/twitter/library/provider/s;-><init>(Landroid/database/Cursor;II)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/ud;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    iget-object v0, v1, Lcom/twitter/android/sk;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    invoke-virtual {v0, p3}, Lcom/twitter/internal/android/widget/HorizontalListView;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/android/sh;->i:Lcom/twitter/android/ob;

    if-eqz v0, :cond_1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, v6}, Landroid/os/Bundle;-><init>(I)V

    const-string/jumbo v1, "position"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/twitter/android/sh;->i:Lcom/twitter/android/ob;

    const/4 v2, 0x0

    invoke-interface {v1, p4, v2, v0}, Lcom/twitter/android/ob;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    :cond_1
    return-object p4

    :cond_2
    check-cast p4, Lcom/twitter/internal/android/widget/GroupedRowView;

    invoke-virtual {p4}, Lcom/twitter/internal/android/widget/GroupedRowView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/sk;

    move-object v1, v0

    goto :goto_0
.end method

.method private a(Landroid/content/Context;ILcom/twitter/android/sp;Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/String;)Landroid/view/View;
    .locals 3

    if-nez p4, :cond_0

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03008f    # com.twitter.android.R.layout.grouped_more_row_view

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p4

    new-instance v1, Lcom/twitter/android/sk;

    const v0, 0x7f090090    # com.twitter.android.R.id.title

    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f0901a4    # com.twitter.android.R.id.chevron

    invoke-virtual {p4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/twitter/android/sk;-><init>(Landroid/widget/TextView;Landroid/view/View;)V

    invoke-virtual {p4, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    :goto_0
    iput-object p3, v0, Lcom/twitter/android/sk;->l:Lcom/twitter/android/sp;

    iget-object v0, v0, Lcom/twitter/android/sk;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p4

    :cond_0
    invoke-virtual {p4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/sk;

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Lcom/twitter/android/sp;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    if-nez p3, :cond_2

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03013f    # com.twitter.android.R.layout.spelling_corrections_onebox

    invoke-virtual {v0, v1, p4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    new-instance v2, Lcom/twitter/android/sk;

    const v0, 0x7f090272    # com.twitter.android.R.id.spelling_corrections_title

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f090273    # com.twitter.android.R.id.spelling_search_instead

    invoke-virtual {p3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-direct {v2, v0, v1}, Lcom/twitter/android/sk;-><init>(Landroid/widget/TextView;Landroid/widget/TextView;)V

    invoke-virtual {p3, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    :goto_0
    iput-object p2, v1, Lcom/twitter/android/sk;->l:Lcom/twitter/android/sp;

    new-instance v2, Landroid/text/SpannableString;

    iget-object v0, p2, Lcom/twitter/android/sp;->h:Lcom/twitter/library/api/TwitterSearchSuggestion;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterSearchSuggestion;->query:Ljava/lang/String;

    invoke-direct {v2, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    iget-object v0, p2, Lcom/twitter/android/sp;->h:Lcom/twitter/library/api/TwitterSearchSuggestion;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterSearchSuggestion;->correctionIndices:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p2, Lcom/twitter/android/sp;->h:Lcom/twitter/library/api/TwitterSearchSuggestion;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterSearchSuggestion;->correctionIndices:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    new-instance v3, Lcom/twitter/internal/android/widget/TypefacesSpan;

    const/4 v4, 0x3

    invoke-direct {v3, p1, v4}, Lcom/twitter/internal/android/widget/TypefacesSpan;-><init>(Landroid/content/Context;I)V

    aget v4, v0, v5

    aget v0, v0, v6

    invoke-virtual {v2, v3, v4, v0, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    :cond_0
    iget-object v0, v1, Lcom/twitter/android/sk;->f:Landroid/widget/TextView;

    const v3, 0x7f0f047d    # com.twitter.android.R.string.spelling_corrections_title

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v2, v4, v5

    invoke-virtual {p1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v1, Lcom/twitter/android/sk;->g:Landroid/widget/TextView;

    const v1, 0x7f0f047e    # com.twitter.android.R.string.spelling_search_instead

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/android/sh;->q:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/sh;->i:Lcom/twitter/android/ob;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/sh;->i:Lcom/twitter/android/ob;

    const/4 v1, 0x0

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {v0, p3, v1, v2}, Lcom/twitter/android/ob;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    :cond_1
    return-object p3

    :cond_2
    invoke-virtual {p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/sk;

    move-object v1, v0

    goto :goto_0
.end method

.method private a(Lcom/twitter/android/sp;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    if-nez p2, :cond_0

    new-instance p2, Landroid/view/View;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/twitter/android/sk;

    invoke-direct {v0}, Lcom/twitter/android/sk;-><init>()V

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iput-object p1, v0, Lcom/twitter/android/sk;->l:Lcom/twitter/android/sp;

    return-object p2

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/sk;

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/sh;)Lcom/twitter/android/ob;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/sh;->i:Lcom/twitter/android/ob;

    return-object v0
.end method

.method private a(IIIILjava/util/ArrayList;Lcom/twitter/android/sp;ZZLjava/lang/String;Ljava/lang/String;)Lcom/twitter/android/sp;
    .locals 12

    move/from16 v0, p4

    if-lt v0, p3, :cond_0

    invoke-virtual/range {p5 .. p5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    move-object/from16 v8, p6

    :cond_1
    :goto_0
    return-object v8

    :cond_2
    iget-object v11, p0, Lcom/twitter/android/sh;->z:Landroid/database/Cursor;

    iget-boolean v1, p0, Lcom/twitter/android/sh;->t:Z

    if-nez v1, :cond_8

    if-eqz p10, :cond_8

    invoke-interface {v11}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v1

    if-eqz v1, :cond_8

    new-instance v1, Lcom/twitter/library/api/af;

    invoke-direct {v1, v11}, Lcom/twitter/library/api/af;-><init>(Landroid/database/Cursor;)V

    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-object v10, v1

    :goto_1
    if-eqz p10, :cond_3

    const/4 v1, 0x7

    if-ne p1, v1, :cond_9

    const/16 p1, 0x15

    :cond_3
    :goto_2
    if-eqz p1, :cond_4

    const/16 v1, 0xa

    if-ne p1, v1, :cond_10

    :cond_4
    iget-object v1, p0, Lcom/twitter/android/sh;->K:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_10

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->ak()Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->am()Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/sh;->K:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    iget-object v1, p0, Lcom/twitter/android/sh;->c:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/twitter/android/sh;->K:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "_opt_out_count"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->al()I

    move-result v2

    if-ge v1, v2, :cond_10

    const/4 v1, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/sj;

    iget-wide v1, v1, Lcom/twitter/android/sj;->c:J

    iget-wide v3, p0, Lcom/twitter/android/sh;->C:J

    cmp-long v3, v1, v3

    if-eqz v3, :cond_5

    iget-wide v3, p0, Lcom/twitter/android/sh;->C:J

    const-wide/16 v5, -0x1

    cmp-long v3, v3, v5

    if-nez v3, :cond_10

    :cond_5
    iput-wide v1, p0, Lcom/twitter/android/sh;->C:J

    const-wide/16 v2, -0x1

    const/16 v4, 0x1b

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p0

    move v5, p2

    move-object/from16 v8, p6

    move-object/from16 v9, p9

    invoke-direct/range {v1 .. v9}, Lcom/twitter/android/sh;->a(JIIIILcom/twitter/android/sp;Ljava/lang/String;)Lcom/twitter/android/sp;

    move-result-object p6

    move-object/from16 v8, p6

    :goto_3
    if-eqz p7, :cond_6

    if-eqz v10, :cond_6

    invoke-virtual {v10}, Lcom/twitter/library/api/af;->d()Z

    move-result v1

    if-eqz v1, :cond_6

    sparse-switch p1, :sswitch_data_0

    :cond_6
    :goto_4
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/4 v1, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/sj;

    iget-wide v2, v1, Lcom/twitter/android/sj;->a:J

    move-object v1, p0

    move v4, p1

    move v5, p2

    move v6, p3

    move/from16 v7, p4

    invoke-direct/range {v1 .. v8}, Lcom/twitter/android/sh;->a(JIIIILcom/twitter/android/sp;)Lcom/twitter/android/sp;

    move-result-object v8

    iput-object v10, v8, Lcom/twitter/android/sp;->k:Lcom/twitter/library/api/af;

    :cond_7
    :goto_5
    :pswitch_1
    if-eqz p8, :cond_1

    sparse-switch p1, :sswitch_data_1

    goto/16 :goto_0

    :sswitch_0
    const-wide/16 v2, -0x1

    const/16 v4, 0x19

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p0

    move v5, p2

    move-object/from16 v9, p9

    invoke-direct/range {v1 .. v9}, Lcom/twitter/android/sh;->a(JIIIILcom/twitter/android/sp;Ljava/lang/String;)Lcom/twitter/android/sp;

    move-result-object v8

    iput-object v10, v8, Lcom/twitter/android/sp;->k:Lcom/twitter/library/api/af;

    const/4 v1, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/sj;

    iget-object v1, v1, Lcom/twitter/android/sj;->b:[B

    invoke-static {v1}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/TwitterSearchFilter;

    iput-object v1, v8, Lcom/twitter/android/sp;->l:Lcom/twitter/library/api/TwitterSearchFilter;

    goto/16 :goto_0

    :cond_8
    const/4 v1, 0x0

    move-object v10, v1

    goto/16 :goto_1

    :cond_9
    const/4 v1, 0x6

    if-ne p1, v1, :cond_3

    const/16 p1, 0x17

    goto/16 :goto_2

    :sswitch_1
    const-wide/16 v2, -0x1

    const/16 v4, 0x13

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p0

    move v5, p2

    move-object/from16 v9, p9

    invoke-direct/range {v1 .. v9}, Lcom/twitter/android/sh;->a(JIIIILcom/twitter/android/sp;Ljava/lang/String;)Lcom/twitter/android/sp;

    move-result-object v8

    iput-object v10, v8, Lcom/twitter/android/sp;->k:Lcom/twitter/library/api/af;

    goto :goto_4

    :sswitch_2
    const-wide/16 v2, -0x1

    const/16 v4, 0x14

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p0

    move v5, p2

    move-object/from16 v9, p9

    invoke-direct/range {v1 .. v9}, Lcom/twitter/android/sh;->a(JIIIILcom/twitter/android/sp;Ljava/lang/String;)Lcom/twitter/android/sp;

    move-result-object v8

    iput-object v10, v8, Lcom/twitter/android/sp;->k:Lcom/twitter/library/api/af;

    goto :goto_4

    :sswitch_3
    const-wide/16 v2, -0x1

    const/16 v4, 0x14

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p0

    move v5, p2

    move-object/from16 v9, p9

    invoke-direct/range {v1 .. v9}, Lcom/twitter/android/sh;->a(JIIIILcom/twitter/android/sp;Ljava/lang/String;)Lcom/twitter/android/sp;

    move-result-object v8

    iput-object v10, v8, Lcom/twitter/android/sp;->k:Lcom/twitter/library/api/af;

    goto/16 :goto_4

    :pswitch_2
    const/4 v1, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/sj;

    iget-wide v2, v1, Lcom/twitter/android/sj;->a:J

    const/4 v4, 0x6

    move-object v1, p0

    move v5, p2

    move v6, p3

    move/from16 v7, p4

    invoke-direct/range {v1 .. v8}, Lcom/twitter/android/sh;->a(JIIIILcom/twitter/android/sp;)Lcom/twitter/android/sp;

    move-result-object v8

    const/4 v1, 0x0

    invoke-virtual/range {p5 .. p5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v1

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/sj;

    iget-object v1, v1, Lcom/twitter/android/sj;->b:[B

    invoke-static {v1}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_f

    add-int/lit8 v1, v2, 0x1

    :goto_7
    move v2, v1

    goto :goto_6

    :cond_a
    if-eqz v2, :cond_7

    sub-int v1, p4, p3

    add-int/lit8 v1, v1, 0x1

    if-ne v2, v1, :cond_7

    const-wide/16 v2, -0x1

    const/16 v4, 0xd

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p0

    move v5, p2

    invoke-direct/range {v1 .. v8}, Lcom/twitter/android/sh;->a(JIIIILcom/twitter/android/sp;)Lcom/twitter/android/sp;

    move-result-object v8

    goto/16 :goto_5

    :pswitch_3
    const/4 v1, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/sj;

    iget-wide v2, v1, Lcom/twitter/android/sj;->a:J

    const/16 v4, 0x8

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p0

    move v5, p2

    invoke-direct/range {v1 .. v8}, Lcom/twitter/android/sh;->a(JIIIILcom/twitter/android/sp;)Lcom/twitter/android/sp;

    move-result-object v8

    goto/16 :goto_5

    :pswitch_4
    const-wide/16 v2, -0x1

    const/16 v4, 0xf

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p0

    move v5, p2

    invoke-direct/range {v1 .. v8}, Lcom/twitter/android/sh;->a(JIIIILcom/twitter/android/sp;)Lcom/twitter/android/sp;

    move-result-object v8

    invoke-virtual/range {p5 .. p5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_b
    :goto_8
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/sj;

    iget-object v2, v1, Lcom/twitter/android/sj;->b:[B

    if-eqz v2, :cond_b

    iget-object v2, v1, Lcom/twitter/android/sj;->b:[B

    invoke-static {v2}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Ljava/lang/String;

    if-eqz v9, :cond_b

    iget-wide v2, v1, Lcom/twitter/android/sj;->a:J

    const/4 v4, 0x3

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p0

    move v5, p2

    invoke-direct/range {v1 .. v8}, Lcom/twitter/android/sh;->a(JIIIILcom/twitter/android/sp;)Lcom/twitter/android/sp;

    move-result-object v8

    iput-object v9, v8, Lcom/twitter/android/sp;->i:Ljava/lang/String;

    goto :goto_8

    :pswitch_5
    const/4 v1, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/sj;

    iget-object v2, v1, Lcom/twitter/android/sj;->b:[B

    if-eqz v2, :cond_7

    iget-object v2, v1, Lcom/twitter/android/sj;->b:[B

    invoke-static {v2}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Lcom/twitter/library/api/TwitterSearchSuggestion;

    if-eqz v9, :cond_7

    iget-wide v2, v1, Lcom/twitter/android/sj;->a:J

    move-object v1, p0

    move v4, p1

    move v5, p2

    move v6, p3

    move/from16 v7, p4

    invoke-direct/range {v1 .. v8}, Lcom/twitter/android/sh;->a(JIIIILcom/twitter/android/sp;)Lcom/twitter/android/sp;

    move-result-object v8

    iput-object v9, v8, Lcom/twitter/android/sp;->h:Lcom/twitter/library/api/TwitterSearchSuggestion;

    goto/16 :goto_5

    :pswitch_6
    const/4 v1, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/twitter/android/sj;

    iget-wide v2, v9, Lcom/twitter/android/sj;->a:J

    move-object v1, p0

    move v4, p1

    move v5, p2

    move v6, p3

    move/from16 v7, p4

    invoke-direct/range {v1 .. v8}, Lcom/twitter/android/sh;->a(JIIIILcom/twitter/android/sp;)Lcom/twitter/android/sp;

    move-result-object v8

    iget-object v1, v9, Lcom/twitter/android/sj;->b:[B

    if-eqz v1, :cond_7

    iget-object v1, v9, Lcom/twitter/android/sj;->b:[B

    invoke-static {v1}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/twitter/library/api/TwitterSearchHighlight;

    if-eqz v9, :cond_7

    const-wide/16 v2, -0x1

    const/16 v4, 0xe

    move-object v1, p0

    move v5, p2

    move v6, p3

    move/from16 v7, p4

    invoke-direct/range {v1 .. v8}, Lcom/twitter/android/sh;->a(JIIIILcom/twitter/android/sp;)Lcom/twitter/android/sp;

    move-result-object v8

    iput-object v9, v8, Lcom/twitter/android/sp;->j:Lcom/twitter/library/api/TwitterSearchHighlight;

    goto/16 :goto_5

    :pswitch_7
    const/4 v1, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/sj;

    iget-wide v2, v1, Lcom/twitter/android/sj;->a:J

    const/16 v4, 0x18

    move-object v1, p0

    move v5, p2

    move v6, p3

    move v7, p3

    move-object/from16 v9, p9

    invoke-direct/range {v1 .. v9}, Lcom/twitter/android/sh;->a(JIIIILcom/twitter/android/sp;Ljava/lang/String;)Lcom/twitter/android/sp;

    move-result-object v8

    add-int/lit8 v6, p3, 0x1

    :goto_9
    move/from16 v0, p4

    if-gt v6, v0, :cond_7

    sub-int v1, v6, p3

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/sj;

    iget-wide v2, v1, Lcom/twitter/android/sj;->a:J

    const/16 v4, 0x18

    move-object v1, p0

    move v5, p2

    move v7, v6

    move-object/from16 v9, p9

    invoke-direct/range {v1 .. v9}, Lcom/twitter/android/sh;->a(JIIIILcom/twitter/android/sp;Ljava/lang/String;)Lcom/twitter/android/sp;

    move-result-object v8

    add-int/lit8 v6, v6, 0x1

    goto :goto_9

    :pswitch_8
    iget-object v1, p0, Lcom/twitter/android/sh;->d:Lcom/twitter/android/client/c;

    invoke-virtual {v1}, Lcom/twitter/android/client/c;->af()Z

    move-result v1

    if-nez v1, :cond_7

    const/4 v1, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/sj;

    iget-wide v2, v1, Lcom/twitter/android/sj;->a:J

    move-object v1, p0

    move v4, p1

    move v5, p2

    move v6, p3

    move/from16 v7, p4

    move-object/from16 v9, p9

    invoke-direct/range {v1 .. v9}, Lcom/twitter/android/sh;->a(JIIIILcom/twitter/android/sp;Ljava/lang/String;)Lcom/twitter/android/sp;

    move-result-object v8

    const-wide/16 v2, -0x1

    const/16 v4, 0x1a

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p0

    move v5, p2

    invoke-direct/range {v1 .. v8}, Lcom/twitter/android/sh;->a(JIIIILcom/twitter/android/sp;)Lcom/twitter/android/sp;

    move-result-object v8

    goto/16 :goto_5

    :pswitch_9
    iget-object v1, p0, Lcom/twitter/android/sh;->d:Lcom/twitter/android/client/c;

    invoke-virtual {v1}, Lcom/twitter/android/client/c;->af()Z

    move-result v1

    if-nez v1, :cond_7

    const/4 v1, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/sj;

    iget-wide v2, v1, Lcom/twitter/android/sj;->a:J

    move-object v1, p0

    move v4, p1

    move v5, p2

    move v6, p3

    move/from16 v7, p4

    move-object/from16 v9, p9

    invoke-direct/range {v1 .. v9}, Lcom/twitter/android/sh;->a(JIIIILcom/twitter/android/sp;Ljava/lang/String;)Lcom/twitter/android/sp;

    move-result-object v8

    goto/16 :goto_5

    :pswitch_a
    const/4 v1, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/sj;

    iget-wide v2, v1, Lcom/twitter/android/sj;->a:J

    move-object v1, p0

    move v4, p1

    move v5, p2

    move v6, p3

    move/from16 v7, p4

    move-object/from16 v9, p9

    invoke-direct/range {v1 .. v9}, Lcom/twitter/android/sh;->a(JIIIILcom/twitter/android/sp;Ljava/lang/String;)Lcom/twitter/android/sp;

    move-result-object v8

    goto/16 :goto_5

    :pswitch_b
    iget-boolean v1, p0, Lcom/twitter/android/sh;->D:Z

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/twitter/android/sh;->z:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/sh;->z:Landroid/database/Cursor;

    invoke-interface {v2, p3}, Landroid/database/Cursor;->moveToPosition(I)Z

    iget-object v2, p0, Lcom/twitter/android/sh;->z:Landroid/database/Cursor;

    invoke-direct {p0, v2}, Lcom/twitter/android/sh;->b(Landroid/database/Cursor;)Lcom/twitter/android/widget/TopicView$TopicData;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/sh;->z:Landroid/database/Cursor;

    invoke-interface {v3, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    iget-object v1, p0, Lcom/twitter/android/sh;->G:Ljava/lang/String;

    iget-object v3, v2, Lcom/twitter/android/widget/TopicView$TopicData;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    iput p2, p0, Lcom/twitter/android/sh;->E:I

    iget-object v1, p0, Lcom/twitter/android/sh;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iput v1, p0, Lcom/twitter/android/sh;->F:I

    iget-object v1, p0, Lcom/twitter/android/sh;->c:Landroid/content/Context;

    check-cast v1, Lcom/twitter/android/EventSearchActivity;

    invoke-virtual {v1, v2}, Lcom/twitter/android/EventSearchActivity;->a(Lcom/twitter/android/widget/TopicView$TopicData;)V

    goto/16 :goto_5

    :cond_c
    move v6, p3

    :goto_a
    move/from16 v0, p4

    if-gt v6, v0, :cond_e

    iget-object v1, p0, Lcom/twitter/android/sh;->z:Landroid/database/Cursor;

    invoke-interface {v1, v6}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-eqz v1, :cond_d

    sget v1, Lcom/twitter/library/provider/bs;->z:I

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/twitter/android/sp;->a(I)I

    move-result v4

    sub-int v1, v6, p3

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/sj;

    iget-wide v2, v1, Lcom/twitter/android/sj;->a:J

    move-object v1, p0

    move v5, p2

    move v7, v6

    move-object/from16 v9, p9

    invoke-direct/range {v1 .. v9}, Lcom/twitter/android/sh;->a(JIIIILcom/twitter/android/sp;Ljava/lang/String;)Lcom/twitter/android/sp;

    move-result-object v8

    :cond_d
    add-int/lit8 v6, v6, 0x1

    goto :goto_a

    :cond_e
    iget-object v1, p0, Lcom/twitter/android/sh;->z:Landroid/database/Cursor;

    add-int/lit8 v2, p4, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    invoke-virtual/range {p5 .. p5}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_7

    invoke-static {}, Lcom/twitter/android/SearchActivity;->f()Z

    move-result v1

    if-eqz v1, :cond_7

    const-wide/16 v2, -0x1

    const/16 v4, 0x1c

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p0

    move v5, p2

    invoke-direct/range {v1 .. v8}, Lcom/twitter/android/sh;->a(JIIIILcom/twitter/android/sp;)Lcom/twitter/android/sp;

    move-result-object v8

    goto/16 :goto_5

    :cond_f
    move v1, v2

    goto/16 :goto_7

    :cond_10
    move-object/from16 v8, p6

    goto/16 :goto_3

    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
        0x17 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_8
        :pswitch_3
        :pswitch_6
        :pswitch_7
        :pswitch_b
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_a
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        0xa -> :sswitch_0
        0x15 -> :sswitch_0
        0x17 -> :sswitch_0
    .end sparse-switch
.end method

.method private a(JIIIILcom/twitter/android/sp;)Lcom/twitter/android/sp;
    .locals 9

    const/4 v8, 0x0

    move-object v0, p0

    move-wide v1, p1

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/sh;->a(JIIIILcom/twitter/android/sp;Ljava/lang/String;)Lcom/twitter/android/sp;

    move-result-object v0

    return-object v0
.end method

.method private a(JIIIILcom/twitter/android/sp;Ljava/lang/String;)Lcom/twitter/android/sp;
    .locals 8

    new-instance v0, Lcom/twitter/android/sp;

    move-wide v1, p1

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move-object/from16 v7, p8

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/sp;-><init>(JIIIILjava/lang/String;)V

    invoke-static {v0, p7}, Lcom/twitter/android/sh;->a(Lcom/twitter/android/sp;Lcom/twitter/android/sp;)V

    iget-object v1, p0, Lcom/twitter/android/sh;->l:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method private a(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private static a(Lcom/twitter/android/sp;)V
    .locals 2

    if-eqz p0, :cond_0

    iget v0, p0, Lcom/twitter/android/sp;->g:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x4

    iput v0, p0, Lcom/twitter/android/sp;->g:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x3

    iput v0, p0, Lcom/twitter/android/sp;->g:I

    goto :goto_0
.end method

.method private static a(Lcom/twitter/android/sp;Lcom/twitter/android/sp;)V
    .locals 7

    const/4 v6, 0x2

    const/4 v2, -0x1

    iget v3, p0, Lcom/twitter/android/sp;->b:I

    iget-object v4, p0, Lcom/twitter/android/sp;->f:Ljava/lang/String;

    if-eqz p1, :cond_0

    iget v1, p1, Lcom/twitter/android/sp;->b:I

    iget-object v0, p1, Lcom/twitter/android/sp;->f:Ljava/lang/String;

    :goto_0
    const/16 v5, 0x18

    if-ne v3, v5, :cond_1

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/sp;->g:I

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    move v1, v2

    goto :goto_0

    :cond_1
    const/4 v5, 0x7

    if-ne v3, v5, :cond_2

    const/4 v0, 0x5

    iput v0, p0, Lcom/twitter/android/sp;->g:I

    invoke-static {p1}, Lcom/twitter/android/sh;->a(Lcom/twitter/android/sp;)V

    goto :goto_1

    :cond_2
    const/16 v5, 0x1b

    if-ne v1, v5, :cond_3

    iput v6, p0, Lcom/twitter/android/sp;->g:I

    goto :goto_1

    :cond_3
    if-eq v1, v2, :cond_4

    invoke-static {v1, v0, v3, v4}, Lcom/twitter/android/sh;->a(ILjava/lang/String;ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/android/sp;->g:I

    invoke-static {p1}, Lcom/twitter/android/sh;->a(Lcom/twitter/android/sp;)V

    goto :goto_1

    :cond_4
    iput v6, p0, Lcom/twitter/android/sp;->g:I

    goto :goto_1
.end method

.method private a(Lcom/twitter/android/widget/TopicView;Landroid/database/Cursor;)V
    .locals 24

    sget v1, Lcom/twitter/library/provider/bs;->z:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    sget v1, Lcom/twitter/library/provider/bs;->F:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v1, Lcom/twitter/library/provider/bs;->D:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    sget v1, Lcom/twitter/library/provider/bs;->I:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    sget v1, Lcom/twitter/library/provider/bs;->y:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v1, Lcom/twitter/library/provider/bs;->G:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    sget v1, Lcom/twitter/library/provider/bs;->H:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    sget v1, Lcom/twitter/library/provider/bs;->B:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget v1, Lcom/twitter/library/provider/bs;->C:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget v1, Lcom/twitter/library/provider/bs;->J:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v15

    sget v1, Lcom/twitter/library/provider/bs;->A:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    sget v1, Lcom/twitter/library/provider/bs;->E:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    sget v1, Lcom/twitter/library/provider/bs;->f:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    sget v1, Lcom/twitter/library/provider/bs;->h:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    sget v1, Lcom/twitter/library/provider/bs;->i:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    sget v1, Lcom/twitter/library/provider/bs;->g:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/sh;->d:Lcom/twitter/android/client/c;

    iget-object v12, v1, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/twitter/android/sh;->u:Z

    if-nez v1, :cond_0

    const/16 v16, 0x1

    :goto_0
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/twitter/android/sh;->u:Z

    if-nez v1, :cond_1

    const/16 v17, 0x1

    :goto_1
    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v21}, Lcom/twitter/android/widget/TopicView;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLcom/twitter/library/widget/ap;Ljava/lang/String;Ljava/lang/String;[BZZLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const/16 v16, 0x0

    goto :goto_0

    :cond_1
    const/16 v17, 0x0

    goto :goto_1
.end method

.method private a(Lcom/twitter/internal/android/widget/GroupedRowView;I)V
    .locals 2

    const/4 v1, 0x1

    const/4 v0, 0x0

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setSingle(Z)V

    invoke-virtual {p1, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setSingle(Z)V

    invoke-virtual {p1, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    invoke-virtual {p1}, Lcom/twitter/internal/android/widget/GroupedRowView;->a()V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setSingle(Z)V

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setSingle(Z)V

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    goto :goto_0

    :pswitch_4
    invoke-virtual {p1, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->setSingle(Z)V

    goto :goto_0

    :pswitch_5
    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setSingle(Z)V

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method private static a(ILjava/lang/String;ILjava/lang/String;)Z
    .locals 3

    const/4 v0, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/twitter/android/sh;->b(I)I

    move-result v1

    invoke-static {p2}, Lcom/twitter/android/sh;->b(I)I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private static b(I)I
    .locals 0

    packed-switch p0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return p0

    :pswitch_1
    const/4 p0, 0x0

    goto :goto_0

    :pswitch_2
    const/4 p0, 0x6

    goto :goto_0

    :pswitch_3
    const/16 p0, 0x1f

    goto :goto_0

    :pswitch_4
    const/4 p0, 0x3

    goto :goto_0

    :pswitch_5
    const/16 p0, 0x9

    goto :goto_0

    :pswitch_6
    const/16 p0, 0x12

    goto :goto_0

    :pswitch_7
    const/4 p0, 0x7

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private b(Landroid/content/Context;ILcom/twitter/android/sp;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    if-nez p4, :cond_1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030097    # com.twitter.android.R.layout.grouped_user_gallery_row

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/twitter/internal/android/widget/GroupedRowView;

    const v0, 0x7f0900bb    # com.twitter.android.R.id.pager

    invoke-virtual {v6, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/support/v4/view/ViewPager;

    new-instance v0, Lcom/twitter/android/yu;

    iget-object v1, p0, Lcom/twitter/android/sh;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/sh;->h:Lcom/twitter/library/util/FriendshipCache;

    iget-object v4, p0, Lcom/twitter/android/sh;->M:Lcom/twitter/android/zc;

    iget-object v5, p0, Lcom/twitter/android/sh;->k:Lcom/twitter/android/zb;

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/yu;-><init>(Landroid/content/Context;Lcom/twitter/library/util/FriendshipCache;ILcom/twitter/android/zc;Lcom/twitter/android/zb;)V

    invoke-virtual {v8, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    const v1, 0x7f09017d    # com.twitter.android.R.id.pip_layout

    invoke-virtual {v6, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/twitter/android/widget/PipView;

    const/4 v1, 0x0

    invoke-virtual {v7, v1}, Lcom/twitter/android/widget/PipView;->setPipOnPosition(I)V

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v4

    new-instance v1, Lcom/twitter/android/si;

    move-object v2, p0

    move-object v3, p5

    move v5, p2

    invoke-direct/range {v1 .. v7}, Lcom/twitter/android/si;-><init>(Lcom/twitter/android/sh;Landroid/view/ViewParent;IILcom/twitter/internal/android/widget/GroupedRowView;Lcom/twitter/android/widget/PipView;)V

    invoke-virtual {v8, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    new-instance v1, Lcom/twitter/android/sk;

    invoke-direct {v1, v8, v7}, Lcom/twitter/android/sk;-><init>(Landroid/support/v4/view/ViewPager;Lcom/twitter/android/widget/PipView;)V

    iget-object v2, p0, Lcom/twitter/android/sh;->o:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v6, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->setTag(Ljava/lang/Object;)V

    :goto_0
    iput-object p3, v1, Lcom/twitter/android/sk;->l:Lcom/twitter/android/sp;

    new-instance v2, Lcom/twitter/library/provider/s;

    iget-object v3, p0, Lcom/twitter/android/sh;->z:Landroid/database/Cursor;

    iget v4, p3, Lcom/twitter/android/sp;->d:I

    iget v5, p3, Lcom/twitter/android/sp;->e:I

    invoke-direct {v2, v3, v4, v5}, Lcom/twitter/library/provider/s;-><init>(Landroid/database/Cursor;II)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/yu;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    invoke-virtual {v0}, Lcom/twitter/android/yu;->getCount()I

    move-result v0

    const/4 v2, 0x1

    if-le v0, v2, :cond_2

    iget-object v2, v1, Lcom/twitter/android/sk;->e:Lcom/twitter/android/widget/PipView;

    invoke-virtual {v2, v0}, Lcom/twitter/android/widget/PipView;->setPipCount(I)V

    iget-object v0, v1, Lcom/twitter/android/sk;->e:Lcom/twitter/android/widget/PipView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PipView;->setVisibility(I)V

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/sh;->i:Lcom/twitter/android/ob;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    const-string/jumbo v1, "position"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v1, "page"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/twitter/android/sh;->i:Lcom/twitter/android/ob;

    const/4 v2, 0x0

    invoke-interface {v1, v6, v2, v0}, Lcom/twitter/android/ob;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    :cond_0
    return-object v6

    :cond_1
    check-cast p4, Lcom/twitter/internal/android/widget/GroupedRowView;

    invoke-virtual {p4}, Lcom/twitter/internal/android/widget/GroupedRowView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/sk;

    iget-object v1, v0, Lcom/twitter/android/sk;->d:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/yu;

    move-object v6, p4

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    goto :goto_0

    :cond_2
    iget-object v0, v1, Lcom/twitter/android/sk;->e:Lcom/twitter/android/widget/PipView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PipView;->setVisibility(I)V

    goto :goto_1
.end method

.method static synthetic b(Lcom/twitter/android/sh;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/sh;->d:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method private b(Landroid/database/Cursor;)Lcom/twitter/android/widget/TopicView$TopicData;
    .locals 12

    sget v0, Lcom/twitter/library/provider/bs;->y:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v0, Lcom/twitter/library/provider/bs;->z:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    sget v0, Lcom/twitter/library/provider/bs;->B:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v0, Lcom/twitter/library/provider/bs;->C:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v0, Lcom/twitter/library/provider/bs;->F:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget v0, Lcom/twitter/library/provider/bs;->D:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    sget v0, Lcom/twitter/library/provider/bs;->G:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget v0, Lcom/twitter/library/provider/bs;->A:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    sget v0, Lcom/twitter/library/provider/bs;->E:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    sget v0, Lcom/twitter/library/provider/bs;->J:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v11

    sget v0, Lcom/twitter/library/provider/bs;->H:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    new-instance v0, Lcom/twitter/android/widget/TopicView$TopicData;

    invoke-direct/range {v0 .. v11}, Lcom/twitter/android/widget/TopicView$TopicData;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I[B)V

    return-object v0
.end method

.method private c(I)I
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/sh;->u:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    const v0, 0x7f03012f    # com.twitter.android.R.layout.search_sports_landing_view

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f030131    # com.twitter.android.R.layout.search_tv_landing_view

    goto :goto_0

    :cond_1
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const v0, 0x7f03012a    # com.twitter.android.R.layout.search_event_card

    goto :goto_0

    :pswitch_1
    const v0, 0x7f030094    # com.twitter.android.R.layout.grouped_sports_event_row_view

    goto :goto_0

    :pswitch_2
    const v0, 0x7f030128    # com.twitter.android.R.layout.search_collection_card

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic c(Lcom/twitter/android/sh;)I
    .locals 1

    iget v0, p0, Lcom/twitter/android/sh;->r:I

    return v0
.end method

.method private c(Landroid/content/Context;ILcom/twitter/android/sp;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    if-nez p4, :cond_2

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030098    # com.twitter.android.R.layout.grouped_user_social_row_view

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/widget/UserSocialView;

    iget-object v2, v1, Lcom/twitter/library/widget/UserSocialView;->m:Lcom/twitter/library/widget/ActionButton;

    const v3, 0x7f020086    # com.twitter.android.R.drawable.btn_follow_action_bg

    invoke-virtual {v2, v3}, Lcom/twitter/library/widget/ActionButton;->setBackgroundResource(I)V

    new-instance v2, Lcom/twitter/android/sk;

    new-instance v3, Lcom/twitter/android/zx;

    invoke-direct {v3, v1}, Lcom/twitter/android/zx;-><init>(Lcom/twitter/library/widget/BaseUserView;)V

    new-instance v4, Lcom/twitter/android/sl;

    invoke-direct {v4, p0}, Lcom/twitter/android/sl;-><init>(Lcom/twitter/android/sh;)V

    invoke-direct {v2, v3, v4}, Lcom/twitter/android/sk;-><init>(Lcom/twitter/android/zx;Lcom/twitter/android/sl;)V

    iget-object v3, p0, Lcom/twitter/android/sh;->n:Ljava/util/ArrayList;

    new-instance v4, Ljava/lang/ref/WeakReference;

    iget-object v5, v2, Lcom/twitter/android/sk;->b:Lcom/twitter/android/zx;

    invoke-direct {v4, v5}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/GroupedRowView;->setTag(Ljava/lang/Object;)V

    move-object p4, v0

    move-object v0, v1

    :goto_0
    iput-object p3, v2, Lcom/twitter/android/sk;->l:Lcom/twitter/android/sp;

    iget-object v3, p0, Lcom/twitter/android/sh;->z:Landroid/database/Cursor;

    iget v1, p3, Lcom/twitter/android/sp;->d:I

    invoke-interface {v3, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-eqz v1, :cond_1

    sget v1, Lcom/twitter/library/provider/bs;->f:I

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/twitter/library/widget/UserSocialView;->setUserId(J)V

    iget-object v1, v2, Lcom/twitter/android/sk;->b:Lcom/twitter/android/zx;

    iput-wide v4, v1, Lcom/twitter/android/zx;->d:J

    sget v1, Lcom/twitter/library/provider/bs;->i:I

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    iget-object v6, p0, Lcom/twitter/android/sh;->d:Lcom/twitter/android/client/c;

    invoke-virtual {v6, v1}, Lcom/twitter/android/client/c;->g(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/twitter/library/widget/UserSocialView;->setUserImage(Landroid/graphics/Bitmap;)V

    iget-object v6, v2, Lcom/twitter/android/sk;->b:Lcom/twitter/android/zx;

    iput-object v1, v6, Lcom/twitter/android/zx;->g:Ljava/lang/String;

    :goto_1
    sget v1, Lcom/twitter/library/provider/bs;->g:I

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v6, Lcom/twitter/library/provider/bs;->h:I

    invoke-interface {v3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v1, v6}, Lcom/twitter/library/widget/UserSocialView;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget v1, Lcom/twitter/library/provider/bs;->j:I

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    and-int/lit8 v1, v6, 0x1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_2
    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/UserSocialView;->setProtected(Z)V

    and-int/lit8 v1, v6, 0x2

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :goto_3
    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/UserSocialView;->setVerified(Z)V

    sget v1, Lcom/twitter/library/provider/bs;->w:I

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v6, "[^\\S]"

    const-string/jumbo v7, " "

    invoke-virtual {v1, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    sget v1, Lcom/twitter/library/provider/bs;->x:I

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/TweetEntities;

    invoke-virtual {v0, v6, v1}, Lcom/twitter/library/widget/UserSocialView;->a(Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;)V

    sget v1, Lcom/twitter/library/provider/bs;->l:I

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/PromotedContent;

    iget-object v6, p0, Lcom/twitter/android/sh;->d:Lcom/twitter/android/client/c;

    iget-boolean v6, v6, Lcom/twitter/android/client/c;->f:Z

    invoke-virtual {v0, v1, v6}, Lcom/twitter/library/widget/UserSocialView;->a(Lcom/twitter/library/api/PromotedContent;Z)V

    iget-object v6, p0, Lcom/twitter/android/sh;->i:Lcom/twitter/android/ob;

    if-eqz v6, :cond_0

    new-instance v6, Landroid/os/Bundle;

    const/4 v7, 0x1

    invoke-direct {v6, v7}, Landroid/os/Bundle;-><init>(I)V

    const-string/jumbo v7, "position"

    invoke-virtual {v6, v7, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v7, p0, Lcom/twitter/android/sh;->i:Lcom/twitter/android/ob;

    const/4 v8, 0x0

    invoke-interface {v7, p4, v8, v6}, Lcom/twitter/android/ob;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    :cond_0
    iget-object v6, p0, Lcom/twitter/android/sh;->e:Lcom/twitter/library/client/aa;

    invoke-virtual {v6}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    cmp-long v6, v4, v6

    if-nez v6, :cond_6

    iget-object v0, v0, Lcom/twitter/library/widget/UserSocialView;->m:Lcom/twitter/library/widget/ActionButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ActionButton;->setVisibility(I)V

    :cond_1
    :goto_4
    return-object p4

    :cond_2
    check-cast p4, Lcom/twitter/internal/android/widget/GroupedRowView;

    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/UserSocialView;

    invoke-virtual {p4}, Lcom/twitter/internal/android/widget/GroupedRowView;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/sk;

    move-object v2, v1

    goto/16 :goto_0

    :cond_3
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/UserSocialView;->setUserImage(Landroid/graphics/Bitmap;)V

    goto/16 :goto_1

    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_5
    const/4 v1, 0x0

    goto :goto_3

    :cond_6
    iget-object v6, v0, Lcom/twitter/library/widget/UserSocialView;->m:Lcom/twitter/library/widget/ActionButton;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/twitter/library/widget/ActionButton;->setVisibility(I)V

    iget-object v6, v2, Lcom/twitter/android/sk;->c:Lcom/twitter/android/sl;

    invoke-virtual {v6, v1}, Lcom/twitter/android/sl;->a(Lcom/twitter/library/api/PromotedContent;)V

    iget-object v1, v2, Lcom/twitter/android/sk;->c:Lcom/twitter/android/sl;

    invoke-virtual {v1, p2}, Lcom/twitter/android/sl;->a(I)V

    iget-object v1, v0, Lcom/twitter/library/widget/UserSocialView;->m:Lcom/twitter/library/widget/ActionButton;

    const v6, 0x7f020086    # com.twitter.android.R.drawable.btn_follow_action_bg

    invoke-virtual {v1, v6}, Lcom/twitter/library/widget/ActionButton;->setBackgroundResource(I)V

    const v1, 0x7f020085    # com.twitter.android.R.drawable.btn_follow_action

    iget-object v6, v2, Lcom/twitter/android/sk;->c:Lcom/twitter/android/sl;

    invoke-virtual {v0, v1, v6}, Lcom/twitter/library/widget/UserSocialView;->a(ILcom/twitter/library/widget/a;)V

    iget-object v1, p0, Lcom/twitter/android/sh;->h:Lcom/twitter/library/util/FriendshipCache;

    sget v6, Lcom/twitter/library/provider/bs;->k:I

    invoke-interface {v3, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    if-eqz v1, :cond_7

    invoke-virtual {v1, v4, v5}, Lcom/twitter/library/util/FriendshipCache;->a(J)Z

    move-result v7

    if-eqz v7, :cond_8

    iget-object v7, v0, Lcom/twitter/library/widget/UserSocialView;->m:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v1, v4, v5}, Lcom/twitter/library/util/FriendshipCache;->i(J)Z

    move-result v1

    invoke-virtual {v7, v1}, Lcom/twitter/library/widget/ActionButton;->setChecked(Z)V

    :cond_7
    :goto_5
    iget-object v1, v2, Lcom/twitter/android/sk;->b:Lcom/twitter/android/zx;

    iput v6, v1, Lcom/twitter/android/zx;->e:I

    invoke-static {v6}, Lcom/twitter/library/provider/ay;->c(I)Z

    move-result v1

    if-eqz v1, :cond_9

    const v1, 0x7f020117    # com.twitter.android.R.drawable.ic_activity_follow_tweet_default

    iget-object v2, p0, Lcom/twitter/android/sh;->d:Lcom/twitter/android/client/c;

    iget-boolean v2, v2, Lcom/twitter/android/client/c;->f:Z

    invoke-virtual {v0, v1, v6, v2}, Lcom/twitter/library/widget/UserSocialView;->a(IIZ)V

    goto :goto_4

    :cond_8
    iget-object v1, v0, Lcom/twitter/library/widget/UserSocialView;->m:Lcom/twitter/library/widget/ActionButton;

    invoke-static {v6}, Lcom/twitter/library/provider/ay;->b(I)Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/twitter/library/widget/ActionButton;->setChecked(Z)V

    goto :goto_5

    :cond_9
    const/4 v1, 0x1

    const v2, 0x7f020117    # com.twitter.android.R.drawable.ic_activity_follow_tweet_default

    sget v4, Lcom/twitter/library/provider/bs;->u:I

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/android/sh;->d:Lcom/twitter/android/client/c;

    iget-boolean v5, v5, Lcom/twitter/android/client/c;->f:Z

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/widget/UserSocialView;->a(IILjava/lang/String;IZ)V

    goto/16 :goto_4
.end method

.method private d(Landroid/content/Context;ILcom/twitter/android/sp;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    if-nez p4, :cond_1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030093    # com.twitter.android.R.layout.grouped_simple_row_view

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    new-instance v2, Lcom/twitter/android/sk;

    const v1, 0x7f090090    # com.twitter.android.R.id.title

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-direct {v2, v1}, Lcom/twitter/android/sk;-><init>(Landroid/widget/TextView;)V

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/GroupedRowView;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    :goto_0
    iput-object p3, v1, Lcom/twitter/android/sk;->l:Lcom/twitter/android/sp;

    iget-object v1, v1, Lcom/twitter/android/sk;->f:Landroid/widget/TextView;

    iget-object v2, p3, Lcom/twitter/android/sp;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/twitter/android/sh;->i:Lcom/twitter/android/ob;

    if-eqz v1, :cond_0

    new-instance v1, Landroid/os/Bundle;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/os/Bundle;-><init>(I)V

    const-string/jumbo v2, "position"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/twitter/android/sh;->i:Lcom/twitter/android/ob;

    const/4 v3, 0x0

    invoke-interface {v2, v0, v3, v1}, Lcom/twitter/android/ob;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    :cond_0
    return-object v0

    :cond_1
    check-cast p4, Lcom/twitter/internal/android/widget/GroupedRowView;

    invoke-virtual {p4}, Lcom/twitter/internal/android/widget/GroupedRowView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/sk;

    move-object v1, v0

    move-object v0, p4

    goto :goto_0
.end method

.method static synthetic d(Lcom/twitter/android/sh;)Lcom/twitter/library/util/FriendshipCache;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/sh;->h:Lcom/twitter/library/util/FriendshipCache;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/sh;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/sh;->q:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/sh;)Lcom/twitter/library/scribe/ScribeAssociation;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/sh;->s:Lcom/twitter/library/scribe/ScribeAssociation;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/sh;)Lcom/twitter/library/client/aa;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/sh;->e:Lcom/twitter/library/client/aa;

    return-object v0
.end method


# virtual methods
.method public a(J)I
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/sh;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/sp;

    iget v0, v0, Lcom/twitter/android/sp;->c:I

    int-to-long v4, v0

    cmp-long v0, v4, p1

    if-nez v0, :cond_0

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public a(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/sh;->z:Landroid/database/Cursor;

    iput-object p1, p0, Lcom/twitter/android/sh;->z:Landroid/database/Cursor;

    invoke-virtual {p0}, Lcom/twitter/android/sh;->a()V

    return-object v0
.end method

.method public a(I)Lcom/twitter/android/sp;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/sh;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/sp;

    return-object v0
.end method

.method public a()V
    .locals 21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/sh;->z:Landroid/database/Cursor;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/sh;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    const/4 v7, 0x0

    if-eqz v20, :cond_1

    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    const/4 v4, 0x0

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, -0x1

    const/4 v3, -0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v8, 0x1

    move/from16 v18, v1

    :goto_0
    sget v1, Lcom/twitter/library/provider/bs;->c:I

    move-object/from16 v0, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    sget v1, Lcom/twitter/library/provider/bs;->d:I

    move-object/from16 v0, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    sget v1, Lcom/twitter/library/provider/bs;->m:I

    move-object/from16 v0, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    const/16 v1, 0xa

    move/from16 v0, v17

    if-eq v0, v1, :cond_0

    const/4 v1, 0x7

    move/from16 v0, v17

    if-eq v0, v1, :cond_0

    const/4 v1, 0x6

    move/from16 v0, v17

    if-ne v0, v1, :cond_2

    :cond_0
    move-object v15, v14

    :goto_1
    move/from16 v0, v16

    if-eq v3, v0, :cond_4

    move/from16 v0, v17

    invoke-static {v2, v10, v0, v15}, Lcom/twitter/android/sh;->a(ILjava/lang/String;ILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v9, 0x1

    :goto_2
    add-int/lit8 v5, v18, -0x1

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v11}, Lcom/twitter/android/sh;->a(IIIILjava/util/ArrayList;Lcom/twitter/android/sp;ZZLjava/lang/String;Ljava/lang/String;)Lcom/twitter/android/sp;

    move-result-object v19

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    move v13, v9

    move/from16 v4, v18

    :goto_3
    new-instance v7, Lcom/twitter/android/sj;

    const/4 v1, 0x0

    move-object/from16 v0, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    sget v1, Lcom/twitter/library/provider/bs;->e:I

    move-object/from16 v0, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v10

    const/4 v1, 0x1

    move-object/from16 v0, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    invoke-direct/range {v7 .. v12}, Lcom/twitter/android/sj;-><init>(J[BJ)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v18, 0x1

    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_5

    add-int/lit8 v5, v1, -0x1

    const/4 v9, 0x0

    move-object/from16 v1, p0

    move/from16 v2, v17

    move/from16 v3, v16

    move-object/from16 v7, v19

    move v8, v13

    move-object v10, v15

    move-object v11, v14

    invoke-direct/range {v1 .. v11}, Lcom/twitter/android/sh;->a(IIIILjava/util/ArrayList;Lcom/twitter/android/sp;ZZLjava/lang/String;Ljava/lang/String;)Lcom/twitter/android/sp;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/sh;->a(Lcom/twitter/android/sp;)V

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/sh;->notifyDataSetChanged()V

    return-void

    :cond_2
    const/4 v15, 0x0

    goto :goto_1

    :cond_3
    const/4 v9, 0x0

    goto :goto_2

    :cond_4
    move v13, v8

    move-object/from16 v19, v7

    goto :goto_3

    :cond_5
    move v8, v13

    move-object v11, v14

    move-object v10, v15

    move/from16 v3, v16

    move/from16 v2, v17

    move/from16 v18, v1

    move-object/from16 v7, v19

    goto/16 :goto_0
.end method

.method public a(Lcom/twitter/library/util/aa;Ljava/util/HashMap;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/sh;->I:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ud;

    if-nez v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_0
    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/ud;->a(Lcom/twitter/library/util/aa;Ljava/util/HashMap;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/sh;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/TopicView;

    if-nez v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    :cond_2
    invoke-virtual {v0, p2}, Lcom/twitter/android/widget/TopicView;->setEventImages(Ljava/util/HashMap;)V

    goto :goto_1

    :cond_3
    return-void
.end method

.method public a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;Z)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/sh;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/yd;

    if-nez v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v1}, Lcom/twitter/library/widget/TweetView;->getUserImageKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/twitter/library/util/ae;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v1, v1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v3, v1, p3}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Bitmap;Z)V

    :cond_2
    iget-object v1, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v1}, Lcom/twitter/library/widget/TweetView;->getSummaryImageKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/util/ae;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v0, v1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v3, v0, p3}, Lcom/twitter/library/widget/TweetView;->b(Landroid/graphics/Bitmap;Z)V

    goto :goto_0

    :cond_3
    iget v0, p1, Lcom/twitter/library/util/ao;->g:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_b

    iget-object v0, p0, Lcom/twitter/android/sh;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/zx;

    if-nez v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    :cond_5
    invoke-virtual {v0}, Lcom/twitter/android/zx;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/twitter/library/util/ae;->b()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, v0, Lcom/twitter/android/zx;->b:Lcom/twitter/library/widget/BaseUserView;

    iget-object v0, v1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v3, v0}, Lcom/twitter/library/widget/BaseUserView;->setUserImage(Landroid/graphics/Bitmap;)V

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/twitter/android/sh;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/yu;

    if-nez v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    :cond_7
    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/yu;->a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;)V

    goto :goto_2

    :cond_8
    iget-object v0, p0, Lcom/twitter/android/sh;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_9
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/TopicView;

    if-nez v0, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_3

    :cond_a
    invoke-virtual {v0}, Lcom/twitter/android/widget/TopicView;->getUserImageKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/twitter/library/util/ae;->b()Z

    move-result v3

    if-eqz v3, :cond_9

    iget-object v1, v1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/TopicView;->setUserImage(Landroid/graphics/Bitmap;)V

    goto :goto_3

    :cond_b
    return-void
.end method

.method public a(Lcom/twitter/library/widget/aa;Lcom/twitter/library/widget/aa;Lcom/twitter/library/widget/aa;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/sh;->w:Lcom/twitter/library/widget/aa;

    iput-object p2, p0, Lcom/twitter/android/sh;->x:Lcom/twitter/library/widget/aa;

    iput-object p3, p0, Lcom/twitter/android/sh;->y:Lcom/twitter/library/widget/aa;

    return-void
.end method

.method public a(Ljava/util/HashMap;Z)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/sh;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/yd;

    if-nez v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_0
    iget-object v0, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/widget/TweetView;->a(Ljava/util/HashMap;Z)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/sh;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/yu;

    if-nez v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    :cond_2
    invoke-virtual {v0, p1}, Lcom/twitter/android/yu;->a(Ljava/util/HashMap;)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/sh;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/TopicView;

    if-nez v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    :cond_4
    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/TopicView;->setEventImages(Ljava/util/HashMap;)V

    goto :goto_2

    :cond_5
    return-void
.end method

.method public b()Landroid/database/Cursor;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/sh;->z:Landroid/database/Cursor;

    return-object v0
.end method

.method public c()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/sh;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/yd;

    if-nez v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_0
    iget-object v2, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v2}, Lcom/twitter/library/widget/TweetView;->a()V

    iget-object v0, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->b()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public d()I
    .locals 5

    iget-boolean v0, p0, Lcom/twitter/android/sh;->D:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/sh;->F:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/android/sh;->E:I

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Lcom/twitter/android/sh;->l:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7fffffff

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/sp;

    iget v1, v0, Lcom/twitter/android/sp;->c:I

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/sp;

    iget v3, v0, Lcom/twitter/android/sp;->c:I

    const/high16 v4, -0x80000000

    if-eq v3, v4, :cond_2

    iget v0, v0, Lcom/twitter/android/sp;->c:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public e()I
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/sh;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const v0, 0x7fffffff

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/sp;

    iget v0, v0, Lcom/twitter/android/sp;->c:I

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/sh;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/sh;->a(I)Lcom/twitter/android/sp;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/sh;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/sp;

    iget v0, v0, Lcom/twitter/android/sp;->c:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/sh;->a(I)Lcom/twitter/android/sp;

    move-result-object v0

    iget v0, v0, Lcom/twitter/android/sp;->b:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12

    const v2, 0x7f090090    # com.twitter.android.R.id.title

    const v4, 0x7f0f0099    # com.twitter.android.R.string.cluster_footer_more

    const/4 v11, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual {p0, p1}, Lcom/twitter/android/sh;->a(I)Lcom/twitter/android/sp;

    move-result-object v3

    iget-object v1, p0, Lcom/twitter/android/sh;->c:Landroid/content/Context;

    iget v0, v3, Lcom/twitter/android/sp;->b:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    invoke-direct {p0, v3, p2, p3}, Lcom/twitter/android/sh;->a(Lcom/twitter/android/sp;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    :cond_0
    :goto_0
    move-object v0, v1

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    iget v2, v3, Lcom/twitter/android/sp;->g:I

    invoke-direct {p0, v0, v2}, Lcom/twitter/android/sh;->a(Lcom/twitter/internal/android/widget/GroupedRowView;I)V

    return-object v1

    :pswitch_1
    move-object v0, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/sh;->a(Landroid/content/Context;ILcom/twitter/android/sp;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0f03bb    # com.twitter.android.R.string.search_view_more_highlight

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/sh;->a(Landroid/content/Context;ILcom/twitter/android/sp;Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/String;)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :pswitch_3
    move-object v0, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/sh;->b(Landroid/content/Context;ILcom/twitter/android/sp;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :pswitch_4
    move-object v0, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/sh;->c(Landroid/content/Context;ILcom/twitter/android/sp;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :pswitch_5
    const v0, 0x7f0f03bd    # com.twitter.android.R.string.search_view_more_people

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/sh;->a(Landroid/content/Context;ILcom/twitter/android/sp;Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/String;)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :pswitch_6
    const v0, 0x7f0f03b9    # com.twitter.android.R.string.search_view_more_collections

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/sh;->a(Landroid/content/Context;ILcom/twitter/android/sp;Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/String;)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :pswitch_7
    const v0, 0x7f0f03ab    # com.twitter.android.R.string.search_only_users

    new-array v2, v7, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/twitter/android/sh;->q:Ljava/lang/String;

    aput-object v4, v2, v8

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/sh;->a(Landroid/content/Context;ILcom/twitter/android/sp;Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/String;)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :pswitch_8
    const v6, 0x7f03008e    # com.twitter.android.R.layout.grouped_media_thumb_row

    move-object v0, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/sh;->a(Landroid/content/Context;ILcom/twitter/android/sp;Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :pswitch_9
    const v0, 0x7f0f03be    # com.twitter.android.R.string.search_view_more_photo

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/sh;->a(Landroid/content/Context;ILcom/twitter/android/sp;Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/String;)Landroid/view/View;

    move-result-object v1

    goto/16 :goto_0

    :pswitch_a
    const v6, 0x7f03002f    # com.twitter.android.R.layout.cluster_media_thumb_row

    move-object v0, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/sh;->a(Landroid/content/Context;ILcom/twitter/android/sp;Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v1

    goto/16 :goto_0

    :pswitch_b
    move-object v0, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/sh;->b(Landroid/content/Context;ILcom/twitter/android/sp;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto/16 :goto_0

    :pswitch_c
    if-nez p2, :cond_1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030089    # com.twitter.android.R.layout.grouped_header_row

    invoke-virtual {v0, v1, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/GroupedRowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0f034a    # com.twitter.android.R.string.related_queries_header

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    new-instance v1, Lcom/twitter/android/sk;

    invoke-direct {v1}, Lcom/twitter/android/sk;-><init>()V

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->setTag(Ljava/lang/Object;)V

    move-object p2, v0

    move-object v0, v1

    :goto_1
    iput-object v3, v0, Lcom/twitter/android/sk;->l:Lcom/twitter/android/sp;

    move-object v1, p2

    goto/16 :goto_0

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/sk;

    goto :goto_1

    :pswitch_d
    const v10, 0x7f03002d    # com.twitter.android.R.layout.cluster_header_row

    move-object v4, p0

    move v5, p1

    move-object v6, p2

    move-object v7, p3

    move-object v8, v3

    move-object v9, v1

    invoke-direct/range {v4 .. v10}, Lcom/twitter/android/sh;->a(ILandroid/view/View;Landroid/view/ViewGroup;Lcom/twitter/android/sp;Landroid/content/Context;I)Landroid/view/View;

    move-result-object v1

    goto/16 :goto_0

    :pswitch_e
    const v10, 0x7f03002e    # com.twitter.android.R.layout.cluster_media_header_row

    move-object v4, p0

    move v5, p1

    move-object v6, p2

    move-object v7, p3

    move-object v8, v3

    move-object v9, v1

    invoke-direct/range {v4 .. v10}, Lcom/twitter/android/sh;->a(ILandroid/view/View;Landroid/view/ViewGroup;Lcom/twitter/android/sp;Landroid/content/Context;I)Landroid/view/View;

    move-result-object v1

    goto/16 :goto_0

    :pswitch_f
    iget-object v0, v3, Lcom/twitter/android/sp;->k:Lcom/twitter/library/api/af;

    if-eqz v0, :cond_2

    new-array v0, v7, [Ljava/lang/Object;

    iget-object v2, v3, Lcom/twitter/android/sp;->k:Lcom/twitter/library/api/af;

    iget-object v2, v2, Lcom/twitter/library/api/af;->d:Ljava/lang/String;

    aput-object v2, v0, v8

    invoke-virtual {v1, v4, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    :goto_2
    move-object v0, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/sh;->a(Landroid/content/Context;ILcom/twitter/android/sp;Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/String;)Landroid/view/View;

    move-result-object v1

    goto/16 :goto_0

    :cond_2
    iget-object v0, v3, Lcom/twitter/android/sp;->l:Lcom/twitter/library/api/TwitterSearchFilter;

    if-eqz v0, :cond_5

    iget-object v0, v3, Lcom/twitter/android/sp;->l:Lcom/twitter/library/api/TwitterSearchFilter;

    iget-boolean v0, v0, Lcom/twitter/library/api/TwitterSearchFilter;->follow:Z

    if-eqz v0, :cond_3

    const v0, 0x7f0f03ba    # com.twitter.android.R.string.search_view_more_follows

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    :cond_3
    iget-object v0, v3, Lcom/twitter/android/sp;->l:Lcom/twitter/library/api/TwitterSearchFilter;

    iget-boolean v0, v0, Lcom/twitter/library/api/TwitterSearchFilter;->nearby:Z

    if-eqz v0, :cond_4

    const v0, 0x7f0f03bc    # com.twitter.android.R.string.search_view_more_nearby

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    :cond_4
    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    :cond_5
    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    :pswitch_10
    move-object v0, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/sh;->d(Landroid/content/Context;ILcom/twitter/android/sp;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto/16 :goto_0

    :pswitch_11
    invoke-direct {p0, v1, v3, p2, p3}, Lcom/twitter/android/sh;->a(Landroid/content/Context;Lcom/twitter/android/sp;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto/16 :goto_0

    :pswitch_12
    if-nez p2, :cond_6

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030093    # com.twitter.android.R.layout.grouped_simple_row_view

    invoke-virtual {v0, v1, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/GroupedRowView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0f03a9    # com.twitter.android.R.string.search_no_results

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    new-instance v1, Lcom/twitter/android/sk;

    invoke-direct {v1}, Lcom/twitter/android/sk;-><init>()V

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->setTag(Ljava/lang/Object;)V

    move-object p2, v0

    move-object v0, v1

    :goto_3
    iput-object v3, v0, Lcom/twitter/android/sk;->l:Lcom/twitter/android/sp;

    move-object v1, p2

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/sk;

    goto :goto_3

    :pswitch_13
    iget-object v10, p0, Lcom/twitter/android/sh;->z:Landroid/database/Cursor;

    iget v0, v3, Lcom/twitter/android/sp;->d:I

    invoke-interface {v10, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_7

    sget v0, Lcom/twitter/library/provider/bs;->z:I

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    packed-switch v0, :pswitch_data_1

    :cond_7
    :pswitch_14
    move-object v1, p2

    :goto_4
    if-nez v1, :cond_0

    invoke-direct {p0, v3, v11, p3}, Lcom/twitter/android/sh;->a(Lcom/twitter/android/sp;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto/16 :goto_0

    :pswitch_15
    move-object v4, p0

    move v5, p1

    move-object v6, p2

    move-object v7, p3

    move-object v8, v3

    move-object v9, v1

    invoke-direct/range {v4 .. v10}, Lcom/twitter/android/sh;->a(ILandroid/view/View;Landroid/view/ViewGroup;Lcom/twitter/android/sp;Landroid/content/Context;Landroid/database/Cursor;)Landroid/view/View;

    move-result-object p2

    move-object v1, p2

    goto :goto_4

    :pswitch_16
    sget-object v0, Lcom/twitter/android/sh;->a:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/twitter/android/sh;->K:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_9

    move v6, v7

    :goto_5
    if-nez p2, :cond_b

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0300f9    # com.twitter.android.R.layout.push_opt_out_row_view

    invoke-virtual {v1, v2, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/internal/android/widget/GroupedRowView;

    const v2, 0x7f0901a2    # com.twitter.android.R.id.textSwitcher

    invoke-virtual {v1, v2}, Lcom/twitter/internal/android/widget/GroupedRowView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/widget/TextSwitcherView;

    if-nez v0, :cond_a

    const v0, 0x7f0f025f    # com.twitter.android.R.string.manage_notifications_banner

    move v4, v0

    :goto_6
    const v0, 0x7f090233    # com.twitter.android.R.id.second_text

    invoke-virtual {v2, v0}, Lcom/twitter/android/widget/TextSwitcherView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    new-instance v0, Lcom/twitter/android/sk;

    invoke-direct {v0, v2}, Lcom/twitter/android/sk;-><init>(Lcom/twitter/android/widget/TextSwitcherView;)V

    invoke-virtual {v1, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setTag(Ljava/lang/Object;)V

    :goto_7
    iput-object v3, v0, Lcom/twitter/android/sk;->l:Lcom/twitter/android/sp;

    iget-object v0, v0, Lcom/twitter/android/sk;->k:Lcom/twitter/android/widget/TextSwitcherView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/TextSwitcherView;->a()V

    invoke-virtual {v0, v11}, Lcom/twitter/android/widget/TextSwitcherView;->setOutAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {v0, v11}, Lcom/twitter/android/widget/TextSwitcherView;->setInAnimation(Landroid/view/animation/Animation;)V

    iget-boolean v2, p0, Lcom/twitter/android/sh;->A:Z

    if-nez v2, :cond_8

    if-nez v6, :cond_8

    iget-object v2, p0, Lcom/twitter/android/sh;->H:Ljava/util/HashSet;

    iget-wide v4, v3, Lcom/twitter/android/sp;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    iget-boolean v2, p0, Lcom/twitter/android/sh;->v:Z

    if-eqz v2, :cond_c

    :cond_8
    invoke-virtual {v0, v7}, Lcom/twitter/android/widget/TextSwitcherView;->setDisplayedChild(I)V

    :goto_8
    iget-object v0, p0, Lcom/twitter/android/sh;->i:Lcom/twitter/android/ob;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, v7}, Landroid/os/Bundle;-><init>(I)V

    const-string/jumbo v2, "position"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v2, "notification_setting_key"

    iget-object v4, p0, Lcom/twitter/android/sh;->K:Ljava/lang/String;

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/twitter/android/sh;->i:Lcom/twitter/android/ob;

    invoke-interface {v2, v1, v11, v0}, Lcom/twitter/android/ob;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    goto/16 :goto_0

    :cond_9
    move v6, v8

    goto :goto_5

    :cond_a
    const v5, 0x7f0f033b    # com.twitter.android.R.string.push_notif_change_settings

    const v4, 0x7f090232    # com.twitter.android.R.id.first_text

    invoke-virtual {v2, v4}, Lcom/twitter/android/widget/TextSwitcherView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/twitter/android/sh;->L:Lcom/twitter/android/ny;

    invoke-virtual {v2, v0}, Lcom/twitter/android/widget/TextSwitcherView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move v4, v5

    goto :goto_6

    :cond_b
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/sk;

    move-object v1, p2

    goto :goto_7

    :cond_c
    iget-object v2, p0, Lcom/twitter/android/sh;->H:Ljava/util/HashSet;

    iget-wide v4, v3, Lcom/twitter/android/sp;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0, v8}, Lcom/twitter/android/widget/TextSwitcherView;->setDisplayedChild(I)V

    iget-object v2, p0, Lcom/twitter/android/sh;->c:Landroid/content/Context;

    const v4, 0x7f04000a    # com.twitter.android.R.anim.highlight_slide_up

    invoke-virtual {v0, v2, v4}, Lcom/twitter/android/widget/TextSwitcherView;->setInAnimation(Landroid/content/Context;I)V

    iget-object v2, p0, Lcom/twitter/android/sh;->c:Landroid/content/Context;

    const v4, 0x7f040009    # com.twitter.android.R.anim.highlight_slide_out

    invoke-virtual {v0, v2, v4}, Lcom/twitter/android/widget/TextSwitcherView;->setOutAnimation(Landroid/content/Context;I)V

    new-instance v2, Lcom/twitter/android/sm;

    invoke-direct {v2, v0}, Lcom/twitter/android/sm;-><init>(Lcom/twitter/android/widget/TextSwitcherView;)V

    const-wide/16 v4, 0xbb8

    invoke-virtual {v0, v2, v4, v5}, Lcom/twitter/android/widget/TextSwitcherView;->a(Ljava/lang/Runnable;J)V

    goto :goto_8

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_11
        :pswitch_10
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_8
        :pswitch_7
        :pswitch_1
        :pswitch_0
        :pswitch_13
        :pswitch_0
        :pswitch_5
        :pswitch_2
        :pswitch_c
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_d
        :pswitch_e
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_1
        :pswitch_f
        :pswitch_9
        :pswitch_16
        :pswitch_6
        :pswitch_13
        :pswitch_13
        :pswitch_13
        :pswitch_12
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_15
        :pswitch_14
        :pswitch_15
        :pswitch_14
        :pswitch_15
        :pswitch_14
        :pswitch_15
        :pswitch_15
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    const/16 v0, 0x21

    return v0
.end method
