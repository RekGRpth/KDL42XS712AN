.class Lcom/twitter/android/dt;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/library/api/conversations/DMPhoto;

.field final synthetic b:Lcom/twitter/android/dr;


# direct methods
.method constructor <init>(Lcom/twitter/android/dr;Lcom/twitter/library/api/conversations/DMPhoto;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/dt;->b:Lcom/twitter/android/dr;

    iput-object p2, p0, Lcom/twitter/android/dt;->a:Lcom/twitter/library/api/conversations/DMPhoto;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/dt;->b:Lcom/twitter/android/dr;

    iget-object v1, v1, Lcom/twitter/android/dr;->a:Lcom/twitter/android/DMConversationFragment;

    invoke-virtual {v1}, Lcom/twitter/android/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/GalleryActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "dm"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "media"

    iget-object v2, p0, Lcom/twitter/android/dt;->a:Lcom/twitter/library/api/conversations/DMPhoto;

    invoke-virtual {v2}, Lcom/twitter/library/api/conversations/DMPhoto;->b()Lcom/twitter/library/api/MediaEntity;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    iget-object v2, p0, Lcom/twitter/android/dt;->b:Lcom/twitter/android/dr;

    iget-object v2, v2, Lcom/twitter/android/dr;->a:Lcom/twitter/android/DMConversationFragment;

    invoke-static {v2}, Lcom/twitter/android/DMConversationFragment;->m(Lcom/twitter/android/DMConversationFragment;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/dt;->b:Lcom/twitter/android/dr;

    iget-object v1, v1, Lcom/twitter/android/dr;->a:Lcom/twitter/android/DMConversationFragment;

    invoke-virtual {v1, v0}, Lcom/twitter/android/DMConversationFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
