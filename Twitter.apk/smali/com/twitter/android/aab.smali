.class Lcom/twitter/android/aab;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/widget/a;


# instance fields
.field final synthetic a:Lcom/twitter/android/aaa;


# direct methods
.method public constructor <init>(Lcom/twitter/android/aaa;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/aab;->a:Lcom/twitter/android/aaa;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onClick(Lcom/twitter/library/widget/BaseUserView;JI)V
    .locals 0

    check-cast p1, Lcom/twitter/library/widget/UserView;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/twitter/android/aab;->onClick(Lcom/twitter/library/widget/UserView;JI)V

    return-void
.end method

.method public onClick(Lcom/twitter/library/widget/UserView;JI)V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/aab;->a:Lcom/twitter/android/aaa;

    iget-object v0, v0, Lcom/twitter/android/aaa;->f:Lcom/twitter/library/widget/a;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/twitter/library/widget/a;->onClick(Lcom/twitter/library/widget/BaseUserView;JI)V

    iget-object v0, p0, Lcom/twitter/android/aab;->a:Lcom/twitter/android/aaa;

    invoke-static {v0}, Lcom/twitter/android/aaa;->a(Lcom/twitter/android/aaa;)V

    iget-object v0, p0, Lcom/twitter/android/aab;->a:Lcom/twitter/android/aaa;

    invoke-static {v0}, Lcom/twitter/android/aaa;->b(Lcom/twitter/android/aaa;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/aab;->a:Lcom/twitter/android/aaa;

    invoke-static {v0, p2, p3}, Lcom/twitter/android/aaa;->a(Lcom/twitter/android/aaa;J)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/twitter/library/widget/UserView;->n:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/ActionButton;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p1, Lcom/twitter/library/widget/UserView;->m:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0}, Lcom/twitter/library/widget/ActionButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/twitter/library/widget/UserView;->n:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/ActionButton;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p1, Lcom/twitter/library/widget/UserView;->n:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ActionButton;->setVisibility(I)V

    iget-object v0, p1, Lcom/twitter/library/widget/UserView;->n:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ActionButton;->setChecked(Z)V

    goto :goto_0
.end method
