.class public Lcom/twitter/android/uu;
.super Lcom/twitter/android/bl;
.source "Twttr"


# instance fields
.field private a:Lcom/twitter/android/client/c;

.field private b:Lcom/twitter/library/client/aa;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct/range {p0 .. p6}, Lcom/twitter/android/bl;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/uu;->a:Lcom/twitter/android/client/c;

    invoke-static {p1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/uu;->b:Lcom/twitter/library/client/aa;

    return-void
.end method

.method public static a(Lcom/twitter/android/client/c;Landroid/database/Cursor;Lcom/twitter/library/client/aa;)V
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/client/c;->ab()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/client/c;->ac()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    move v0, v1

    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    const/16 v3, 0x190

    if-ge v0, v3, :cond_1

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v3

    const/16 v4, 0x64

    if-ge v3, v4, :cond_1

    const/16 v3, 0x12

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/16 v3, 0x1e

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Lcom/twitter/library/provider/ay;->a(I)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x5

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {p2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-static {v2}, Lcom/twitter/library/util/Util;->b(Ljava/util/Collection;)[J

    move-result-object v2

    invoke-virtual {p0, v0, v2, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;[JZ)Ljava/lang/String;

    :cond_2
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    :cond_3
    return-void
.end method


# virtual methods
.method public a()Landroid/database/Cursor;
    .locals 3

    invoke-super {p0}, Lcom/twitter/android/bl;->a()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/uu;->a:Lcom/twitter/android/client/c;

    iget-object v2, p0, Lcom/twitter/android/uu;->b:Lcom/twitter/library/client/aa;

    invoke-static {v1, v0, v2}, Lcom/twitter/android/uu;->a(Lcom/twitter/android/client/c;Landroid/database/Cursor;Lcom/twitter/library/client/aa;)V

    :cond_0
    return-object v0
.end method

.method public synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/uu;->a()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
