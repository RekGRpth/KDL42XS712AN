.class final enum Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;
.super Ljava/lang/Enum;
.source "Twttr"


# static fields
.field public static final enum a:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

.field public static final enum b:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

.field public static final enum c:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

.field public static final enum d:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

.field public static final enum e:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

.field private static final synthetic f:[Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    const-string/jumbo v1, "DOWN"

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->a:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    new-instance v0, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    const-string/jumbo v1, "UP"

    invoke-direct {v0, v1, v3}, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->b:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    new-instance v0, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    const-string/jumbo v1, "UP_LOCKED"

    invoke-direct {v0, v1, v4}, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->c:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    new-instance v0, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    const-string/jumbo v1, "FULL_SCREEN"

    invoke-direct {v0, v1, v5}, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->d:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    new-instance v0, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    const-string/jumbo v1, "FULL_SCREEN_LOCKED"

    invoke-direct {v0, v1, v6}, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->e:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    sget-object v1, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->a:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->b:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->c:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->d:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->e:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    aput-object v1, v0, v6

    sput-object v0, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->f:[Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;
    .locals 1

    const-class v0, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    return-object v0
.end method

.method public static values()[Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;
    .locals 1

    sget-object v0, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->f:[Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    invoke-virtual {v0}, [Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    return-object v0
.end method
