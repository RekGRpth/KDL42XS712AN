.class Lcom/twitter/android/widget/ak;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public a:Z

.field public b:Z

.field public c:J

.field public d:Ljava/lang/ref/WeakReference;

.field final synthetic e:Lcom/twitter/android/widget/aj;

.field private final f:Lcom/twitter/library/provider/Tweet;


# direct methods
.method public constructor <init>(Lcom/twitter/android/widget/aj;Landroid/view/View;Lcom/twitter/library/provider/Tweet;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/twitter/android/widget/ak;->e:Lcom/twitter/android/widget/aj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/twitter/android/widget/ak;->a:Z

    iput-boolean v0, p0, Lcom/twitter/android/widget/ak;->b:Z

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/widget/ak;->d:Ljava/lang/ref/WeakReference;

    iput-object p3, p0, Lcom/twitter/android/widget/ak;->f:Lcom/twitter/library/provider/Tweet;

    return-void
.end method

.method private a(Landroid/view/View;)Lcom/twitter/library/provider/Tweet;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/ak;->e:Lcom/twitter/android/widget/aj;

    invoke-static {v0}, Lcom/twitter/android/widget/aj;->a(Lcom/twitter/android/widget/aj;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/ak;->e:Lcom/twitter/android/widget/aj;

    invoke-static {v0}, Lcom/twitter/android/widget/aj;->a(Lcom/twitter/android/widget/aj;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/scribe/ScribeAssociation;->d()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/yd;

    iget-object v0, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/library/provider/Tweet;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a()Z
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/ak;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, v0}, Lcom/twitter/android/widget/ak;->a(Landroid/view/View;)Lcom/twitter/library/provider/Tweet;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-wide v2, v2, Lcom/twitter/library/provider/Tweet;->L:J

    iget-object v4, p0, Lcom/twitter/android/widget/ak;->f:Lcom/twitter/library/provider/Tweet;

    iget-wide v4, v4, Lcom/twitter/library/provider/Tweet;->L:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    if-ne v2, v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    add-int/2addr v0, v2

    div-int/lit8 v3, v0, 0x2

    iget-object v0, p0, Lcom/twitter/android/widget/ak;->e:Lcom/twitter/android/widget/aj;

    invoke-static {v0}, Lcom/twitter/android/widget/aj;->b(Lcom/twitter/android/widget/aj;)Landroid/widget/ListView;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/widget/ak;->e:Lcom/twitter/android/widget/aj;

    invoke-static {v0}, Lcom/twitter/android/widget/aj;->b(Lcom/twitter/android/widget/aj;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getTop()I

    move-result v2

    iget-object v0, p0, Lcom/twitter/android/widget/ak;->e:Lcom/twitter/android/widget/aj;

    invoke-static {v0}, Lcom/twitter/android/widget/aj;->b(Lcom/twitter/android/widget/aj;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getBottom()I

    move-result v0

    :goto_1
    if-lt v3, v2, :cond_5

    if-gt v3, v0, :cond_5

    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method private b(J)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/ak;->f:Lcom/twitter/library/provider/Tweet;

    iget-object v1, p0, Lcom/twitter/android/widget/ak;->e:Lcom/twitter/android/widget/aj;

    invoke-static {v1}, Lcom/twitter/android/widget/aj;->a(Lcom/twitter/android/widget/aj;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v1

    invoke-static {v2, v0, v1, v2}, Lcom/twitter/library/scribe/ScribeItem;->a(Landroid/content/Context;Lcom/twitter/library/provider/ParcelableTweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/widget/ak;->c:J

    iput-wide v1, v0, Lcom/twitter/library/scribe/ScribeItem;->D:J

    iput-wide p1, v0, Lcom/twitter/library/scribe/ScribeItem;->E:J

    iget-object v1, p0, Lcom/twitter/android/widget/ak;->e:Lcom/twitter/android/widget/aj;

    invoke-static {v1}, Lcom/twitter/android/widget/aj;->c(Lcom/twitter/android/widget/aj;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method a(J)V
    .locals 6

    const-wide/16 v2, 0x7530

    const/4 v5, 0x0

    const/4 v4, 0x1

    invoke-direct {p0}, Lcom/twitter/android/widget/ak;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/android/widget/ak;->a:Z

    if-nez v0, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/widget/ak;->c:J

    iput-boolean v4, p0, Lcom/twitter/android/widget/ak;->a:Z

    iput-boolean v5, p0, Lcom/twitter/android/widget/ak;->b:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/twitter/android/widget/ak;->b:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/android/widget/ak;->c:J

    sub-long v0, p1, v0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/android/widget/ak;->c:J

    add-long/2addr v0, v2

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/widget/ak;->b(J)V

    iput-boolean v4, p0, Lcom/twitter/android/widget/ak;->b:Z

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/twitter/android/widget/ak;->a:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/android/widget/ak;->c:J

    sub-long v0, p1, v0

    iget-boolean v2, p0, Lcom/twitter/android/widget/ak;->b:Z

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/twitter/android/widget/ak;->e:Lcom/twitter/android/widget/aj;

    invoke-static {v2}, Lcom/twitter/android/widget/aj;->d(Lcom/twitter/android/widget/aj;)I

    move-result v2

    int-to-long v2, v2

    cmp-long v2, v0, v2

    if-lez v2, :cond_3

    iget-object v2, p0, Lcom/twitter/android/widget/ak;->e:Lcom/twitter/android/widget/aj;

    invoke-static {v2}, Lcom/twitter/android/widget/aj;->e(Lcom/twitter/android/widget/aj;)I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_4

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/widget/ak;->b(J)V

    :goto_1
    iput-boolean v4, p0, Lcom/twitter/android/widget/ak;->b:Z

    :cond_3
    iput-boolean v5, p0, Lcom/twitter/android/widget/ak;->a:Z

    goto :goto_0

    :cond_4
    iget-wide v0, p0, Lcom/twitter/android/widget/ak;->c:J

    iget-object v2, p0, Lcom/twitter/android/widget/ak;->e:Lcom/twitter/android/widget/aj;

    invoke-static {v2}, Lcom/twitter/android/widget/aj;->e(Lcom/twitter/android/widget/aj;)I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/widget/ak;->b(J)V

    goto :goto_1
.end method
