.class public Lcom/twitter/android/widget/StagingUrlPreference;
.super Lcom/twitter/android/widget/DebugUrlPreference;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8

    const-string/jumbo v3, "staging_enabled"

    const-string/jumbo v4, "staging_url"

    const-string/jumbo v5, "Enable Staging Server"

    const-string/jumbo v6, "Example: https://api-staging148.smf1.twitter.com/1.1"

    const-string/jumbo v7, "https://api-staging148.smf1.twitter.com/1.1"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/widget/DebugUrlPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method a()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "1.1"

    return-object v0
.end method
