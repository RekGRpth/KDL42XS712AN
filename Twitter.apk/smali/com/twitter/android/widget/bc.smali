.class Lcom/twitter/android/widget/bc;
.super Landroid/widget/MediaController;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/bb;


# instance fields
.field final synthetic a:Lcom/twitter/android/widget/MediaPlayerView;


# direct methods
.method public constructor <init>(Lcom/twitter/android/widget/MediaPlayerView;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/bc;->a:Lcom/twitter/android/widget/MediaPlayerView;

    invoke-direct {p0, p2}, Landroid/widget/MediaController;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/bc;->setAnchorView(Landroid/view/View;)V

    return-void
.end method

.method public hide()V
    .locals 1

    invoke-super {p0}, Landroid/widget/MediaController;->hide()V

    iget-object v0, p0, Lcom/twitter/android/widget/bc;->a:Lcom/twitter/android/widget/MediaPlayerView;

    invoke-static {v0}, Lcom/twitter/android/widget/MediaPlayerView;->d(Lcom/twitter/android/widget/MediaPlayerView;)Lcom/twitter/android/widget/bd;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/bc;->a:Lcom/twitter/android/widget/MediaPlayerView;

    invoke-static {v0}, Lcom/twitter/android/widget/MediaPlayerView;->d(Lcom/twitter/android/widget/MediaPlayerView;)Lcom/twitter/android/widget/bd;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/widget/bd;->d()V

    :cond_0
    return-void
.end method

.method public show(I)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/MediaController;->show(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/bc;->a:Lcom/twitter/android/widget/MediaPlayerView;

    invoke-static {v0}, Lcom/twitter/android/widget/MediaPlayerView;->d(Lcom/twitter/android/widget/MediaPlayerView;)Lcom/twitter/android/widget/bd;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/bc;->a:Lcom/twitter/android/widget/MediaPlayerView;

    invoke-static {v0}, Lcom/twitter/android/widget/MediaPlayerView;->d(Lcom/twitter/android/widget/MediaPlayerView;)Lcom/twitter/android/widget/bd;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/twitter/android/widget/bd;->b(I)V

    :cond_0
    return-void
.end method
