.class Lcom/twitter/android/widget/v;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final a:Ljava/lang/ref/WeakReference;

.field final b:Landroid/net/Uri;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/twitter/android/widget/y;

.field private final e:J

.field private final f:J

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:Landroid/os/Handler;

.field private final k:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Landroid/content/Context;JJILandroid/net/Uri;IILcom/twitter/android/widget/y;Landroid/os/Handler;Lcom/twitter/android/MemoryImageCache;Ljava/lang/ref/WeakReference;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/v;->c:Landroid/content/Context;

    iput-object p10, p0, Lcom/twitter/android/widget/v;->d:Lcom/twitter/android/widget/y;

    iput-wide p2, p0, Lcom/twitter/android/widget/v;->e:J

    iput-wide p4, p0, Lcom/twitter/android/widget/v;->f:J

    iput p6, p0, Lcom/twitter/android/widget/v;->g:I

    iput p8, p0, Lcom/twitter/android/widget/v;->h:I

    iput p9, p0, Lcom/twitter/android/widget/v;->i:I

    iput-object p7, p0, Lcom/twitter/android/widget/v;->b:Landroid/net/Uri;

    iput-object p11, p0, Lcom/twitter/android/widget/v;->j:Landroid/os/Handler;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p12}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/widget/v;->k:Ljava/lang/ref/WeakReference;

    iput-object p13, p0, Lcom/twitter/android/widget/v;->a:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    const/4 v7, 0x1

    iget-object v0, p0, Lcom/twitter/android/widget/v;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/widget/v;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/widget/v;->e:J

    iget-wide v3, p0, Lcom/twitter/android/widget/v;->f:J

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JJILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_4

    iget v1, p0, Lcom/twitter/android/widget/v;->g:I

    if-eq v1, v7, :cond_4

    iget-object v1, p0, Lcom/twitter/android/widget/v;->c:Landroid/content/Context;

    iget v2, p0, Lcom/twitter/android/widget/v;->g:I

    invoke-static {v1, v2, v0}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;ILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, v0

    :goto_2
    iget-object v0, p0, Lcom/twitter/android/widget/v;->k:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/MemoryImageCache;

    if-eqz v0, :cond_2

    new-instance v2, Lcom/twitter/android/lu;

    invoke-direct {v2}, Lcom/twitter/android/lu;-><init>()V

    iput-object v1, v2, Lcom/twitter/android/lu;->a:Landroid/graphics/Bitmap;

    iget-wide v3, p0, Lcom/twitter/android/widget/v;->e:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Lcom/twitter/android/MemoryImageCache;->a(Ljava/lang/String;Lcom/twitter/android/lu;)V

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/widget/v;->d:Lcom/twitter/android/widget/y;

    iput-object v1, v0, Lcom/twitter/android/widget/y;->a:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    iget-object v0, p0, Lcom/twitter/android/widget/v;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/v;->j:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/android/widget/v;->d:Lcom/twitter/android/widget/y;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_3
    :try_start_1
    iget v0, p0, Lcom/twitter/android/widget/v;->h:I

    const/16 v1, 0x200

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget v1, p0, Lcom/twitter/android/widget/v;->i:I

    const/16 v2, 0x180

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/widget/v;->c:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/android/widget/v;->b:Landroid/net/Uri;

    invoke-static {v2, v3}, Lkw;->a(Landroid/content/Context;Landroid/net/Uri;)Lkw;

    move-result-object v2

    invoke-virtual {v2}, Lkw;->a()Lkw;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lkw;->a(II)Lkw;

    move-result-object v0

    invoke-virtual {v0}, Lkw;->b()Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_3

    :cond_4
    move-object v1, v0

    goto :goto_2
.end method
