.class public Lcom/twitter/android/widget/ActivityUserView;
.super Lcom/twitter/library/widget/UserView;
.source "Twttr"

# interfaces
.implements Lcom/twitter/internal/android/widget/p;


# instance fields
.field private p:Z

.field private q:Ljava/lang/String;

.field private r:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/widget/UserView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/widget/ActivityUserView;->p:Z

    return-void
.end method

.method public static a(Lcom/twitter/library/widget/ActionButton;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/util/FriendshipCache;ZJ)V
    .locals 5

    const/4 v1, 0x0

    if-eqz p0, :cond_2

    if-eqz p2, :cond_5

    iget-wide v2, p1, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {p2, v2, v3}, Lcom/twitter/library/util/FriendshipCache;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2, p1}, Lcom/twitter/library/util/FriendshipCache;->a(Lcom/twitter/library/api/TwitterUser;)V

    :cond_0
    iget-wide v2, p1, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {p2, v2, v3}, Lcom/twitter/library/util/FriendshipCache;->h(J)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    and-int/lit8 v0, v2, 0x40

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-static {v2}, Lcom/twitter/library/provider/ay;->b(I)Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/twitter/library/widget/ActionButton;->setChecked(Z)V

    iget-wide v3, p1, Lcom/twitter/library/api/TwitterUser;->userId:J

    cmp-long v3, v3, p4

    if-eqz v3, :cond_1

    if-eqz p3, :cond_4

    if-eqz v2, :cond_4

    if-nez v0, :cond_4

    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/ActionButton;->setVisibility(I)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    invoke-virtual {p0, v1}, Lcom/twitter/library/widget/ActionButton;->setVisibility(I)V

    goto :goto_1

    :cond_5
    invoke-virtual {p0, v1}, Lcom/twitter/library/widget/ActionButton;->setVisibility(I)V

    invoke-virtual {p0, v1}, Lcom/twitter/library/widget/ActionButton;->setChecked(Z)V

    goto :goto_1
.end method


# virtual methods
.method public a(ILandroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/ActivityUserView;->m:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0, p1}, Lcom/twitter/library/widget/ActionButton;->a(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/ActivityUserView;->m:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0, p2}, Lcom/twitter/library/widget/ActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public a(Lcom/twitter/android/client/c;Lcom/twitter/library/util/FriendshipCache;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/view/c;ZJ)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/widget/ActivityUserView;->h:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "@"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p3, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/widget/ActivityUserView;->g:Landroid/widget/TextView;

    iget-object v1, p3, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p3, Lcom/twitter/library/api/TwitterUser;->profileDescription:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/widget/ActivityUserView;->p:Z

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/ActivityUserView;->k:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/ActivityUserView;->k:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p3, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/twitter/android/client/c;->g(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/twitter/android/widget/ActivityUserView;->l:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/widget/ActivityUserView;->m:Lcom/twitter/library/widget/ActionButton;

    move-object v1, p3

    move-object v2, p2

    move v3, p5

    move-wide v4, p6

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/widget/ActivityUserView;->a(Lcom/twitter/library/widget/ActionButton;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/util/FriendshipCache;ZJ)V

    return-void

    :cond_1
    iget-object v0, p3, Lcom/twitter/library/api/TwitterUser;->profileDescription:Ljava/lang/String;

    iget-object v1, p3, Lcom/twitter/library/api/TwitterUser;->descriptionEntities:Lcom/twitter/library/api/TweetEntities;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/widget/ActivityUserView;->a(Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/widget/ActivityUserView;->l:Landroid/widget/ImageView;

    const v1, 0x7f02003a    # com.twitter.android.R.drawable.bg_no_profile_photo_md

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method

.method public a(Lcom/twitter/android/client/c;Lcom/twitter/library/util/FriendshipCache;Lcom/twitter/library/api/TwitterUser;ZJ)V
    .locals 8

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    move-wide v6, p5

    invoke-virtual/range {v0 .. v7}, Lcom/twitter/android/widget/ActivityUserView;->a(Lcom/twitter/android/client/c;Lcom/twitter/library/util/FriendshipCache;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/view/c;ZJ)V

    return-void
.end method

.method public getImageTag()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/ActivityUserView;->l:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/ActivityUserView;->l:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getReason()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/ActivityUserView;->q:Ljava/lang/String;

    return-object v0
.end method

.method public setHighlighted(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/widget/ActivityUserView;->r:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/twitter/android/widget/ActivityUserView;->r:Z

    invoke-virtual {p0}, Lcom/twitter/android/widget/ActivityUserView;->refreshDrawableState()V

    :cond_0
    return-void
.end method

.method public setImageTag(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/ActivityUserView;->l:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/ActivityUserView;->l:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public setReason(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/ActivityUserView;->q:Ljava/lang/String;

    return-void
.end method

.method public setShowBio(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/widget/ActivityUserView;->p:Z

    return-void
.end method
