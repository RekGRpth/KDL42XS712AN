.class public Lcom/twitter/android/widget/PoiItemView;
.super Landroid/view/View;
.source "Twttr"


# static fields
.field private static final a:Landroid/text/TextPaint;


# instance fields
.field private b:Ljava/lang/CharSequence;

.field private c:Landroid/text/StaticLayout;

.field private d:Ljava/lang/CharSequence;

.field private e:Landroid/text/StaticLayout;

.field private f:Ljava/lang/CharSequence;

.field private g:Landroid/text/StaticLayout;

.field private h:Ljava/lang/CharSequence;

.field private i:Landroid/text/StaticLayout;

.field private j:Z

.field private k:Z

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:I

.field private s:I

.field private t:I

.field private u:Lcom/twitter/internal/android/widget/ax;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    sput-object v0, Lcom/twitter/android/widget/PoiItemView;->a:Landroid/text/TextPaint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/widget/PoiItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/widget/PoiItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/widget/PoiItemView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    const/4 v2, 0x0

    invoke-static {p1}, Lcom/twitter/internal/android/widget/ax;->a(Landroid/content/Context;)Lcom/twitter/internal/android/widget/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/PoiItemView;->u:Lcom/twitter/internal/android/widget/ax;

    invoke-virtual {p0}, Lcom/twitter/android/widget/PoiItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c007a    # com.twitter.android.R.dimen.icon_spacing

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/widget/PoiItemView;->n:I

    if-eqz p2, :cond_0

    sget-object v0, Lcom/twitter/android/rg;->PoiItemView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/PoiItemView;->o:I

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/PoiItemView;->p:I

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/PoiItemView;->q:I

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/PoiItemView;->s:I

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/PoiItemView;->l:I

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/PoiItemView;->m:I

    const/4 v1, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/PoiItemView;->t:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    .locals 1

    if-nez p0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    if-eqz p0, :cond_2

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V
    .locals 4

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/twitter/android/widget/PoiItemView;->b:Ljava/lang/CharSequence;

    invoke-static {v2, p1}, Lcom/twitter/android/widget/PoiItemView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iput-object p1, p0, Lcom/twitter/android/widget/PoiItemView;->b:Ljava/lang/CharSequence;

    iput-object v3, p0, Lcom/twitter/android/widget/PoiItemView;->c:Landroid/text/StaticLayout;

    iput-object v3, p0, Lcom/twitter/android/widget/PoiItemView;->e:Landroid/text/StaticLayout;

    move v0, v1

    :cond_0
    iget-object v2, p0, Lcom/twitter/android/widget/PoiItemView;->d:Ljava/lang/CharSequence;

    invoke-static {v2, p2}, Lcom/twitter/android/widget/PoiItemView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iput-object p2, p0, Lcom/twitter/android/widget/PoiItemView;->d:Ljava/lang/CharSequence;

    iput-object v3, p0, Lcom/twitter/android/widget/PoiItemView;->e:Landroid/text/StaticLayout;

    move v0, v1

    :cond_1
    iget-object v2, p0, Lcom/twitter/android/widget/PoiItemView;->f:Ljava/lang/CharSequence;

    invoke-static {v2, p3}, Lcom/twitter/android/widget/PoiItemView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iput-object p3, p0, Lcom/twitter/android/widget/PoiItemView;->f:Ljava/lang/CharSequence;

    iput-object v3, p0, Lcom/twitter/android/widget/PoiItemView;->g:Landroid/text/StaticLayout;

    iput-object v3, p0, Lcom/twitter/android/widget/PoiItemView;->i:Landroid/text/StaticLayout;

    move v0, v1

    :cond_2
    iget-object v2, p0, Lcom/twitter/android/widget/PoiItemView;->h:Ljava/lang/CharSequence;

    invoke-static {v2, p4}, Lcom/twitter/android/widget/PoiItemView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    iput-object p4, p0, Lcom/twitter/android/widget/PoiItemView;->h:Ljava/lang/CharSequence;

    iput-object v3, p0, Lcom/twitter/android/widget/PoiItemView;->i:Landroid/text/StaticLayout;

    move v0, v1

    :cond_3
    iget-boolean v2, p0, Lcom/twitter/android/widget/PoiItemView;->j:Z

    if-eq v2, p5, :cond_5

    iput-boolean p5, p0, Lcom/twitter/android/widget/PoiItemView;->j:Z

    :goto_0
    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/twitter/android/widget/PoiItemView;->requestLayout()V

    :cond_4
    return-void

    :cond_5
    move v1, v0

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 13

    sget-object v5, Lcom/twitter/android/widget/PoiItemView;->a:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lcom/twitter/android/widget/PoiItemView;->getMeasuredHeight()I

    move-result v4

    iget-object v6, p0, Lcom/twitter/android/widget/PoiItemView;->c:Landroid/text/StaticLayout;

    iget-object v7, p0, Lcom/twitter/android/widget/PoiItemView;->e:Landroid/text/StaticLayout;

    iget-object v8, p0, Lcom/twitter/android/widget/PoiItemView;->g:Landroid/text/StaticLayout;

    iget-object v9, p0, Lcom/twitter/android/widget/PoiItemView;->i:Landroid/text/StaticLayout;

    iget v0, p0, Lcom/twitter/android/widget/PoiItemView;->r:I

    sub-int v0, v4, v0

    div-int/lit8 v3, v0, 0x2

    invoke-virtual {p0}, Lcom/twitter/android/widget/PoiItemView;->getPaddingLeft()I

    move-result v2

    if-eqz v6, :cond_5

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    int-to-float v0, v2

    int-to-float v1, v3

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {v6}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    iget-object v0, p0, Lcom/twitter/android/widget/PoiItemView;->u:Lcom/twitter/internal/android/widget/ax;

    iget-object v0, v0, Lcom/twitter/internal/android/widget/ax;->c:Landroid/graphics/Typeface;

    invoke-virtual {v5, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget v0, p0, Lcom/twitter/android/widget/PoiItemView;->l:I

    int-to-float v0, v0

    invoke-virtual {v5, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    iget-boolean v0, p0, Lcom/twitter/android/widget/PoiItemView;->j:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/android/widget/PoiItemView;->q:I

    invoke-virtual {v5, v0}, Landroid/text/TextPaint;->setColor(I)V

    :goto_0
    if-eqz v7, :cond_3

    iget-boolean v0, p0, Lcom/twitter/android/widget/PoiItemView;->j:Z

    if-nez v0, :cond_3

    invoke-virtual {v7}, Landroid/text/StaticLayout;->getHeight()I

    move-result v10

    invoke-static {v1, v10}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {v6}, Landroid/text/StaticLayout;->getWidth()I

    move-result v11

    iget v12, p0, Lcom/twitter/android/widget/PoiItemView;->n:I

    add-int/2addr v11, v12

    if-le v1, v10, :cond_2

    invoke-virtual {v6, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    const/4 v1, 0x0

    invoke-virtual {v6, v1}, Landroid/text/StaticLayout;->getLineBaseline(I)I

    move-result v1

    const/4 v6, 0x0

    invoke-virtual {v7, v6}, Landroid/text/StaticLayout;->getLineBaseline(I)I

    move-result v6

    sub-int/2addr v1, v6

    int-to-float v6, v11

    int-to-float v1, v1

    invoke-virtual {p1, v6, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :goto_1
    iget-object v1, p0, Lcom/twitter/android/widget/PoiItemView;->u:Lcom/twitter/internal/android/widget/ax;

    iget-object v1, v1, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    invoke-virtual {v5, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget v1, p0, Lcom/twitter/android/widget/PoiItemView;->m:I

    int-to-float v1, v1

    invoke-virtual {v5, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    iget v1, p0, Lcom/twitter/android/widget/PoiItemView;->p:I

    invoke-virtual {v5, v1}, Landroid/text/TextPaint;->setColor(I)V

    invoke-virtual {v7, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    :goto_2
    add-int/2addr v0, v3

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    move v1, v0

    :goto_3
    if-eqz v8, :cond_4

    iget-boolean v0, p0, Lcom/twitter/android/widget/PoiItemView;->j:Z

    if-nez v0, :cond_4

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    int-to-float v0, v2

    int-to-float v3, v1

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiItemView;->u:Lcom/twitter/internal/android/widget/ax;

    iget-object v0, v0, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    invoke-virtual {v5, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget v0, p0, Lcom/twitter/android/widget/PoiItemView;->m:I

    int-to-float v0, v0

    invoke-virtual {v5, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    iget v0, p0, Lcom/twitter/android/widget/PoiItemView;->p:I

    invoke-virtual {v5, v0}, Landroid/text/TextPaint;->setColor(I)V

    invoke-virtual {v8, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiItemView;->g:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getEllipsizedWidth()I

    move-result v0

    add-int/2addr v0, v2

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :goto_4
    if-eqz v9, :cond_0

    iget-boolean v2, p0, Lcom/twitter/android/widget/PoiItemView;->j:Z

    if-nez v2, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    int-to-float v0, v0

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiItemView;->u:Lcom/twitter/internal/android/widget/ax;

    iget-object v0, v0, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    invoke-virtual {v5, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget v0, p0, Lcom/twitter/android/widget/PoiItemView;->m:I

    int-to-float v0, v0

    invoke-virtual {v5, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    iget v0, p0, Lcom/twitter/android/widget/PoiItemView;->p:I

    invoke-virtual {v5, v0}, Landroid/text/TextPaint;->setColor(I)V

    invoke-virtual {v9, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_0
    iget v0, p0, Lcom/twitter/android/widget/PoiItemView;->s:I

    invoke-virtual {v5, v0}, Landroid/text/TextPaint;->setColor(I)V

    iget v0, p0, Lcom/twitter/android/widget/PoiItemView;->t:I

    int-to-float v0, v0

    invoke-virtual {v5, v0}, Landroid/text/TextPaint;->setStrokeWidth(F)V

    const/4 v1, 0x0

    int-to-float v2, v4

    invoke-virtual {p0}, Lcom/twitter/android/widget/PoiItemView;->getMeasuredWidth()I

    move-result v0

    int-to-float v3, v0

    int-to-float v4, v4

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    return-void

    :cond_1
    iget v0, p0, Lcom/twitter/android/widget/PoiItemView;->o:I

    invoke-virtual {v5, v0}, Landroid/text/TextPaint;->setColor(I)V

    goto/16 :goto_0

    :cond_2
    const/4 v1, 0x0

    invoke-virtual {v7, v1}, Landroid/text/StaticLayout;->getLineBaseline(I)I

    move-result v1

    const/4 v10, 0x0

    invoke-virtual {v6, v10}, Landroid/text/StaticLayout;->getLineBaseline(I)I

    move-result v10

    sub-int/2addr v1, v10

    const/4 v10, 0x0

    int-to-float v12, v1

    invoke-virtual {p1, v10, v12}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {v6, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    int-to-float v6, v11

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v6, v1}, Landroid/graphics/Canvas;->translate(FF)V

    goto/16 :goto_1

    :cond_3
    invoke-virtual {v6, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    move v0, v1

    goto/16 :goto_2

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    move v1, v3

    goto/16 :goto_3
.end method

.method protected onMeasure(II)V
    .locals 34

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v29

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v30

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v28

    sget-object v6, Lcom/twitter/android/widget/PoiItemView;->a:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/widget/PoiItemView;->b:Ljava/lang/CharSequence;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/PoiItemView;->d:Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v15, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/widget/PoiItemView;->f:Ljava/lang/CharSequence;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/PoiItemView;->h:Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x0

    move-object/from16 v26, v2

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/widget/PoiItemView;->getPaddingLeft()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/widget/PoiItemView;->getPaddingTop()I

    move-result v32

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/widget/PoiItemView;->getPaddingRight()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/widget/PoiItemView;->getPaddingBottom()I

    move-result v33

    sub-int v2, v29, v2

    sub-int v13, v2, v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/PoiItemView;->c:Landroid/text/StaticLayout;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/PoiItemView;->c:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    :goto_2
    if-eqz v3, :cond_10

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/PoiItemView;->c:Landroid/text/StaticLayout;

    if-nez v4, :cond_10

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/android/widget/PoiItemView;->l:I

    int-to-float v2, v2

    invoke-virtual {v6, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/PoiItemView;->u:Lcom/twitter/internal/android/widget/ax;

    iget-object v2, v2, Lcom/twitter/internal/android/widget/ax;->c:Landroid/graphics/Typeface;

    invoke-virtual {v6, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    new-instance v2, Landroid/text/StaticLayout;

    const/4 v4, 0x0

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v5

    invoke-static {v3, v6}, Lhl;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v7

    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    const/4 v11, 0x0

    sget-object v12, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-direct/range {v2 .. v13}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/widget/PoiItemView;->c:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/PoiItemView;->c:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    move/from16 v27, v2

    :goto_3
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/twitter/android/widget/PoiItemView;->j:Z

    if-eqz v2, :cond_5

    const/4 v2, 0x0

    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/widget/PoiItemView;->c:Landroid/text/StaticLayout;

    if-eqz v3, :cond_7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/widget/PoiItemView;->c:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getWidth()I

    move-result v3

    :goto_5
    sub-int v3, v13, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/android/widget/PoiItemView;->n:I

    sub-int v25, v3, v4

    if-eqz v15, :cond_f

    if-lez v25, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/widget/PoiItemView;->e:Landroid/text/StaticLayout;

    if-nez v3, :cond_f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/android/widget/PoiItemView;->m:I

    int-to-float v2, v2

    invoke-virtual {v6, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/PoiItemView;->u:Lcom/twitter/internal/android/widget/ax;

    iget-object v2, v2, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    invoke-virtual {v6, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    new-instance v14, Landroid/text/StaticLayout;

    const/16 v16, 0x0

    invoke-interface {v15}, Ljava/lang/CharSequence;->length()I

    move-result v17

    invoke-static {v15, v6}, Lhl;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v19

    sget-object v20, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v21, 0x3f800000    # 1.0f

    const/16 v22, 0x0

    const/16 v23, 0x0

    sget-object v24, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v18, v6

    invoke-direct/range {v14 .. v25}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/twitter/android/widget/PoiItemView;->e:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/PoiItemView;->e:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    move v14, v2

    :goto_6
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/twitter/android/widget/PoiItemView;->j:Z

    if-eqz v2, :cond_8

    const/4 v2, 0x0

    :goto_7
    if-eqz v26, :cond_e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/widget/PoiItemView;->i:Landroid/text/StaticLayout;

    if-nez v3, :cond_e

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/android/widget/PoiItemView;->m:I

    int-to-float v2, v2

    invoke-virtual {v6, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/PoiItemView;->u:Lcom/twitter/internal/android/widget/ax;

    iget-object v2, v2, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    invoke-virtual {v6, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    new-instance v2, Landroid/text/StaticLayout;

    const/4 v4, 0x0

    invoke-interface/range {v26 .. v26}, Ljava/lang/CharSequence;->length()I

    move-result v5

    move-object/from16 v0, v26

    invoke-static {v0, v6}, Lhl;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v7

    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    const/4 v11, 0x0

    sget-object v12, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v3, v26

    invoke-direct/range {v2 .. v13}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/widget/PoiItemView;->i:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/PoiItemView;->i:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getWidth()I

    move-result v2

    sub-int/2addr v13, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/PoiItemView;->i:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    move v15, v2

    :goto_8
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/twitter/android/widget/PoiItemView;->j:Z

    if-eqz v2, :cond_a

    const/4 v2, 0x0

    :goto_9
    if-eqz v31, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/widget/PoiItemView;->g:Landroid/text/StaticLayout;

    if-nez v3, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/android/widget/PoiItemView;->m:I

    int-to-float v2, v2

    invoke-virtual {v6, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/PoiItemView;->u:Lcom/twitter/internal/android/widget/ax;

    iget-object v2, v2, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    invoke-virtual {v6, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-object/from16 v0, v31

    invoke-static {v0, v6}, Lhl;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)I

    move-result v7

    new-instance v2, Landroid/text/StaticLayout;

    const/4 v4, 0x0

    invoke-interface/range {v31 .. v31}, Ljava/lang/CharSequence;->length()I

    move-result v5

    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    const/4 v11, 0x0

    sget-object v12, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    if-le v7, v13, :cond_c

    :goto_a
    move-object/from16 v3, v31

    invoke-direct/range {v2 .. v13}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/widget/PoiItemView;->g:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/PoiItemView;->g:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    :cond_0
    move/from16 v0, v27

    invoke-static {v0, v14}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-static {v2, v15}, Ljava/lang/Math;->max(II)I

    move-result v2

    add-int/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/android/widget/PoiItemView;->r:I

    const/high16 v3, 0x40000000    # 2.0f

    move/from16 v0, v30

    if-ne v0, v3, :cond_d

    move/from16 v2, v28

    :goto_b
    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/widget/PoiItemView;->setMeasuredDimension(II)V

    return-void

    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "@"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/PoiItemView;->d:Ljava/lang/CharSequence;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_0

    :cond_2
    invoke-static/range {v31 .. v31}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/PoiItemView;->h:Ljava/lang/CharSequence;

    move-object/from16 v26, v2

    goto/16 :goto_1

    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, " \u00b7 "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/PoiItemView;->h:Ljava/lang/CharSequence;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v26, v2

    goto/16 :goto_1

    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/PoiItemView;->e:Landroid/text/StaticLayout;

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/PoiItemView;->e:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    goto/16 :goto_4

    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_4

    :cond_7
    const/4 v3, 0x0

    goto/16 :goto_5

    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/PoiItemView;->i:Landroid/text/StaticLayout;

    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/PoiItemView;->i:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    goto/16 :goto_7

    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_7

    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/PoiItemView;->g:Landroid/text/StaticLayout;

    if-eqz v2, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/PoiItemView;->g:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    goto/16 :goto_9

    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_9

    :cond_c
    move v13, v7

    goto/16 :goto_a

    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/widget/PoiItemView;->getSuggestedMinimumHeight()I

    move-result v3

    add-int v2, v2, v32

    add-int v2, v2, v33

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto/16 :goto_b

    :cond_e
    move v15, v2

    goto/16 :goto_8

    :cond_f
    move v14, v2

    goto/16 :goto_6

    :cond_10
    move/from16 v27, v2

    goto/16 :goto_3
.end method

.method public setHighlightTextColor(I)V
    .locals 0

    iput p1, p0, Lcom/twitter/android/widget/PoiItemView;->q:I

    return-void
.end method

.method public setRenderRTL(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/widget/PoiItemView;->k:Z

    return-void
.end method

.method public setSelected(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/widget/PoiItemView;->j:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/twitter/android/widget/PoiItemView;->j:Z

    invoke-virtual {p0}, Lcom/twitter/android/widget/PoiItemView;->requestLayout()V

    :cond_0
    return-void
.end method
