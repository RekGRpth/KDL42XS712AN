.class Lcom/twitter/android/widget/dx;
.super Landroid/view/animation/ScaleAnimation;
.source "Twttr"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/widget/dt;


# direct methods
.method private constructor <init>(Lcom/twitter/android/widget/dt;I)V
    .locals 9

    const/4 v5, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/twitter/android/widget/dx;->a:Lcom/twitter/android/widget/dt;

    int-to-float v6, p2

    move-object v0, p0

    move v3, v1

    move v4, v2

    move v7, v5

    move v8, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    const-wide/16 v0, 0x64

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/widget/dx;->setDuration(J)V

    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/widget/dx;->setStartOffset(J)V

    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/dx;->setInterpolator(Landroid/view/animation/Interpolator;)V

    invoke-virtual {p0, p0}, Lcom/twitter/android/widget/dx;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    return-void
.end method

.method private constructor <init>(Lcom/twitter/android/widget/dt;Landroid/view/View;)V
    .locals 1

    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/widget/dx;-><init>(Lcom/twitter/android/widget/dt;Landroid/widget/RelativeLayout$LayoutParams;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/widget/dt;Landroid/view/View;Lcom/twitter/android/widget/du;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/widget/dx;-><init>(Lcom/twitter/android/widget/dt;Landroid/view/View;)V

    return-void
.end method

.method private constructor <init>(Lcom/twitter/android/widget/dt;Landroid/widget/RelativeLayout$LayoutParams;)V
    .locals 2

    iget v0, p2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v1, p2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/widget/dx;-><init>(Lcom/twitter/android/widget/dt;I)V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/dx;->a:Lcom/twitter/android/widget/dt;

    invoke-static {v0}, Lcom/twitter/android/widget/dt;->c(Lcom/twitter/android/widget/dt;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method
