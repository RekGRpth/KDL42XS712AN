.class public Lcom/twitter/android/widget/AutomatedEllipsisTextView;
.super Landroid/widget/TextView;
.source "Twttr"


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private b:Ljava/lang/String;

.field private c:I

.field private d:Lcom/twitter/android/widget/c;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "      "

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, " .    "

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, " . .  "

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, " . . ."

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/android/widget/AutomatedEllipsisTextView;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/AutomatedEllipsisTextView;->setSingleLine(Z)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/AutomatedEllipsisTextView;->setHorizontalFadingEdgeEnabled(Z)V

    new-instance v0, Lcom/twitter/android/widget/c;

    const-wide/16 v1, 0x4b0

    invoke-direct {v0, p0, v1, v2}, Lcom/twitter/android/widget/c;-><init>(Lcom/twitter/android/widget/AutomatedEllipsisTextView;J)V

    iput-object v0, p0, Lcom/twitter/android/widget/AutomatedEllipsisTextView;->d:Lcom/twitter/android/widget/c;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/widget/AutomatedEllipsisTextView;->c:I

    iget-object v0, p0, Lcom/twitter/android/widget/AutomatedEllipsisTextView;->d:Lcom/twitter/android/widget/c;

    invoke-virtual {v0}, Lcom/twitter/android/widget/c;->b()V

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/AutomatedEllipsisTextView;->d:Lcom/twitter/android/widget/c;

    invoke-virtual {v0}, Lcom/twitter/android/widget/c;->c()V

    return-void
.end method

.method c()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/widget/AutomatedEllipsisTextView;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/widget/AutomatedEllipsisTextView;->a:[Ljava/lang/String;

    iget v2, p0, Lcom/twitter/android/widget/AutomatedEllipsisTextView;->c:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/AutomatedEllipsisTextView;->setText(Ljava/lang/CharSequence;)V

    iget v0, p0, Lcom/twitter/android/widget/AutomatedEllipsisTextView;->c:I

    add-int/lit8 v0, v0, 0x1

    sget-object v1, Lcom/twitter/android/widget/AutomatedEllipsisTextView;->a:[Ljava/lang/String;

    array-length v1, v1

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/twitter/android/widget/AutomatedEllipsisTextView;->c:I

    return-void
.end method

.method public setTextContent(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/AutomatedEllipsisTextView;->b:Ljava/lang/String;

    return-void
.end method
