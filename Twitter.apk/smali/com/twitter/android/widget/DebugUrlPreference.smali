.class public Lcom/twitter/android/widget/DebugUrlPreference;
.super Landroid/preference/DialogPreference;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# instance fields
.field a:Landroid/widget/LinearLayout;

.field b:Landroid/widget/RadioGroup;

.field c:Landroid/widget/Button;

.field private d:Landroid/widget/EditText;

.field private e:Z

.field private final f:Landroid/content/SharedPreferences;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const v0, 0x7f030067    # com.twitter.android.R.layout.endpoint_dialog_preference

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/DebugUrlPreference;->setDialogLayoutResource(I)V

    iput-boolean v2, p0, Lcom/twitter/android/widget/DebugUrlPreference;->e:Z

    iput-object p3, p0, Lcom/twitter/android/widget/DebugUrlPreference;->g:Ljava/lang/String;

    iput-object p4, p0, Lcom/twitter/android/widget/DebugUrlPreference;->h:Ljava/lang/String;

    iput-object p5, p0, Lcom/twitter/android/widget/DebugUrlPreference;->i:Ljava/lang/String;

    iput-object p6, p0, Lcom/twitter/android/widget/DebugUrlPreference;->j:Ljava/lang/String;

    iput-object p7, p0, Lcom/twitter/android/widget/DebugUrlPreference;->k:Ljava/lang/String;

    const-string/jumbo v0, "debug_prefs"

    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1, p3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "Production"

    invoke-interface {v1, p4, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/DebugUrlPreference;->setSummary(Ljava/lang/CharSequence;)V

    iput-object v1, p0, Lcom/twitter/android/widget/DebugUrlPreference;->f:Landroid/content/SharedPreferences;

    return-void

    :cond_0
    const-string/jumbo v0, "Production"

    goto :goto_0
.end method

.method static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/net/URL;
    .locals 4

    :try_start_0
    const-string/jumbo v0, "://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_0

    const-string/jumbo v0, "%s://%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    :cond_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/net/URL;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_0
    invoke-direct {v0, p1, v2, p2}, Ljava/net/URL;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-object v0

    :cond_1
    move-object p2, v1

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic a(Lcom/twitter/android/widget/DebugUrlPreference;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/widget/DebugUrlPreference;->e:Z

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/widget/DebugUrlPreference;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/DebugUrlPreference;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/widget/DebugUrlPreference;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/DebugUrlPreference;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/widget/DebugUrlPreference;)Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/DebugUrlPreference;->f:Landroid/content/SharedPreferences;

    return-object v0
.end method


# virtual methods
.method a()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, ""

    return-object v0
.end method

.method protected onBindDialogView(Landroid/view/View;)V
    .locals 5

    const/4 v4, 0x1

    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onBindDialogView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/twitter/android/widget/DebugUrlPreference;->f:Landroid/content/SharedPreferences;

    const v0, 0x7f090152    # com.twitter.android.R.id.enable_debug

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/twitter/android/widget/DebugUrlPreference;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f090153    # com.twitter.android.R.id.debug_description

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/twitter/android/widget/DebugUrlPreference;->j:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/twitter/android/widget/DebugUrlPreference;->g:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-interface {v2, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    :goto_0
    const v0, 0x7f090150    # com.twitter.android.R.id.debug_group

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    invoke-virtual {v0, p0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    iput-object v0, p0, Lcom/twitter/android/widget/DebugUrlPreference;->b:Landroid/widget/RadioGroup;

    const v0, 0x7f090155    # com.twitter.android.R.id.validation_progress

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/twitter/android/widget/DebugUrlPreference;->a:Landroid/widget/LinearLayout;

    const v0, 0x7f090154    # com.twitter.android.R.id.debug_endpoint

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iget-object v3, p0, Lcom/twitter/android/widget/DebugUrlPreference;->k:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/twitter/android/widget/DebugUrlPreference;->h:Ljava/lang/String;

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/twitter/android/widget/DebugUrlPreference;->h:Ljava/lang/String;

    const-string/jumbo v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    iput-object v0, p0, Lcom/twitter/android/widget/DebugUrlPreference;->d:Landroid/widget/EditText;

    iput-boolean v1, p0, Lcom/twitter/android/widget/DebugUrlPreference;->e:Z

    return-void

    :cond_1
    const v0, 0x7f090151    # com.twitter.android.R.id.enable_prod

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 2

    const v0, 0x7f090152    # com.twitter.android.R.id.enable_debug

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/widget/DebugUrlPreference;->d:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setEnabled(Z)V

    iput-boolean v0, p0, Lcom/twitter/android/widget/DebugUrlPreference;->e:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    new-instance v0, Lcom/twitter/android/widget/h;

    invoke-direct {v0, p0}, Lcom/twitter/android/widget/h;-><init>(Lcom/twitter/android/widget/DebugUrlPreference;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/widget/DebugUrlPreference;->d:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/h;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method protected showDialog(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->showDialog(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/widget/DebugUrlPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    instance-of v1, v0, Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/DebugUrlPreference;->c:Landroid/widget/Button;

    iget-object v0, p0, Lcom/twitter/android/widget/DebugUrlPreference;->c:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method
