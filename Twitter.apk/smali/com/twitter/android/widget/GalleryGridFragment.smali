.class public Lcom/twitter/android/widget/GalleryGridFragment;
.super Landroid/support/v4/app/Fragment;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/twitter/android/widget/x;


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:[Landroid/view/View;

.field private e:Landroid/widget/GridView;

.field private f:Lcom/twitter/android/widget/t;

.field private g:Lcom/twitter/android/widget/ab;

.field private h:Lcom/twitter/android/MemoryImageCache;

.field private i:Ljava/util/List;

.field private j:Z

.field private k:Z

.field private l:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method public static final a(III)Lcom/twitter/android/widget/GalleryGridFragment;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/twitter/android/widget/GalleryGridFragment;->a(IIIZ)Lcom/twitter/android/widget/GalleryGridFragment;

    move-result-object v0

    return-object v0
.end method

.method public static final a(IIIZ)Lcom/twitter/android/widget/GalleryGridFragment;
    .locals 3

    new-instance v0, Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-direct {v0}, Lcom/twitter/android/widget/GalleryGridFragment;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Landroid/os/Bundle;-><init>(I)V

    const-string/jumbo v2, "color"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v2, "header"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v2, "scroll_header"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v2, "show_expand"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GalleryGridFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/widget/GalleryGridFragment;)Lcom/twitter/android/widget/t;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->f:Lcom/twitter/android/widget/t;

    return-object v0
.end method

.method private a(IIII)V
    .locals 5

    if-lez p3, :cond_0

    if-lez p2, :cond_0

    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v1, p1, p2}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_0

    new-instance v3, Landroid/view/View;

    invoke-direct {v3, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v3, p4}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v4, p0, Lcom/twitter/android/widget/GalleryGridFragment;->f:Lcom/twitter/android/widget/t;

    invoke-virtual {v4, v3}, Lcom/twitter/android/widget/t;->a(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/widget/GalleryGridFragment;IIII)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/widget/GalleryGridFragment;->a(IIII)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/widget/GalleryGridFragment;[Landroid/view/View;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/widget/GalleryGridFragment;->a([Landroid/view/View;I)V

    return-void
.end method

.method private a([Landroid/view/View;I)V
    .locals 5

    if-eqz p1, :cond_0

    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v1, p2, p2}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p1, v0

    invoke-virtual {v3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v4, p0, Lcom/twitter/android/widget/GalleryGridFragment;->f:Lcom/twitter/android/widget/t;

    invoke-virtual {v4, v3}, Lcom/twitter/android/widget/t;->a(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/widget/GalleryGridFragment;)I
    .locals 1

    iget v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->c:I

    return v0
.end method

.method static synthetic c(Lcom/twitter/android/widget/GalleryGridFragment;)I
    .locals 1

    iget v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:I

    return v0
.end method

.method static synthetic d(Lcom/twitter/android/widget/GalleryGridFragment;)[Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->d:[Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->f:Lcom/twitter/android/widget/t;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->i:Ljava/util/List;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->f:Lcom/twitter/android/widget/t;

    invoke-virtual {v0}, Lcom/twitter/android/widget/t;->a()V

    goto :goto_0
.end method

.method public a(F)V
    .locals 9

    const v6, 0x7fffffff

    const/4 v1, 0x0

    new-instance v0, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    float-to-int v4, p1

    const/high16 v7, -0x80000000

    move v2, v1

    move v3, v1

    move v5, v1

    move v8, v6

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    iget-object v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->e:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getFinalY()I

    move-result v2

    neg-int v2, v2

    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-virtual {v0}, Landroid/widget/Scroller;->getDuration()I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/widget/GridView;->smoothScrollBy(II)V

    return-void
.end method

.method public a(Landroid/net/Uri;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->g:Lcom/twitter/android/widget/ab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->g:Lcom/twitter/android/widget/ab;

    invoke-interface {v0, p1}, Lcom/twitter/android/widget/ab;->c(Landroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->f:Lcom/twitter/android/widget/t;

    invoke-virtual {v0, p2}, Lcom/twitter/android/widget/t;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v3, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v4, "composition::photo_gallery::load_finished"

    aput-object v4, v1, v2

    invoke-virtual {v3, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->e(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/android/widget/ab;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->g:Lcom/twitter/android/widget/ab;

    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->f:Lcom/twitter/android/widget/t;

    if-nez v0, :cond_1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->j:Z

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->f:Lcom/twitter/android/widget/t;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/t;->a(Z)V

    goto :goto_1
.end method

.method public a([Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->d:[Landroid/view/View;

    return-void
.end method

.method public b(Landroid/net/Uri;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->f:Lcom/twitter/android/widget/t;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->i:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->i:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->f:Lcom/twitter/android/widget/t;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/t;->a(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public b(Z)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->f:Lcom/twitter/android/widget/t;

    if-nez v0, :cond_0

    iput-boolean p1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->k:Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->f:Lcom/twitter/android/widget/t;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/t;->b(Z)V

    goto :goto_0
.end method

.method public c(Landroid/net/Uri;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->f:Lcom/twitter/android/widget/t;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->f:Lcom/twitter/android/widget/t;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/t;->b(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "color"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:I

    const-string/jumbo v1, "header"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->b:I

    const-string/jumbo v1, "scroll_header"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->c:I

    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/MemoryImageCache;->a(Landroid/support/v4/app/FragmentManager;)Lcom/twitter/android/MemoryImageCache;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->h:Lcom/twitter/android/MemoryImageCache;

    const-string/jumbo v1, "show_expand"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->l:Z

    new-instance v0, Lcom/twitter/android/widget/t;

    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/widget/GalleryGridFragment;->h:Lcom/twitter/android/MemoryImageCache;

    iget-boolean v3, p0, Lcom/twitter/android/widget/GalleryGridFragment;->l:Z

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/twitter/android/widget/t;-><init>(Landroid/content/Context;Lcom/twitter/android/MemoryImageCache;IZ)V

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->f:Lcom/twitter/android/widget/t;

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->i:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iget-object v2, p0, Lcom/twitter/android/widget/GalleryGridFragment;->f:Lcom/twitter/android/widget/t;

    invoke-virtual {v2, v0}, Lcom/twitter/android/widget/t;->a(Landroid/net/Uri;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->i:Ljava/util/List;

    :cond_1
    iget-boolean v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->l:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->f:Lcom/twitter/android/widget/t;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/t;->a(Lcom/twitter/android/widget/x;)V

    :cond_2
    iget-boolean v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->j:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->f:Lcom/twitter/android/widget/t;

    invoke-virtual {v0, v4}, Lcom/twitter/android/widget/t;->a(Z)V

    iput-boolean v4, p0, Lcom/twitter/android/widget/GalleryGridFragment;->j:Z

    :cond_3
    iget-boolean v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->k:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->f:Lcom/twitter/android/widget/t;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/t;->b(Z)V

    iput-boolean v4, p0, Lcom/twitter/android/widget/GalleryGridFragment;->k:Z

    :cond_4
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 2

    const-string/jumbo v0, "_data NOT NULL AND _data != ? AND _size > ?"

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->f:Lcom/twitter/android/widget/t;

    const-string/jumbo v1, "_data NOT NULL AND _data != ? AND _size > ?"

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/t;->a(Ljava/lang/String;)Landroid/support/v4/content/CursorLoader;

    move-result-object v0

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    const/4 v4, 0x0

    const v0, 0x7f03007e    # com.twitter.android.R.layout.gallery_grid

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iget v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->a:I

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    const v0, 0x7f090187    # com.twitter.android.R.id.gallery_header

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget v2, p0, Lcom/twitter/android/widget/GalleryGridFragment;->b:I

    if-lez v2, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v3, p0, Lcom/twitter/android/widget/GalleryGridFragment;->b:I

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_0
    const v0, 0x7f090188    # com.twitter.android.R.id.gallery_grid

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iget-object v2, p0, Lcom/twitter/android/widget/GalleryGridFragment;->f:Lcom/twitter/android/widget/t;

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {v0, p0}, Landroid/widget/GridView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    invoke-virtual {v0, p0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v2, p0, Lcom/twitter/android/widget/GalleryGridFragment;->f:Lcom/twitter/android/widget/t;

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->e:Landroid/widget/GridView;

    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v4, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-object v1

    :cond_0
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->f:Lcom/twitter/android/widget/t;

    invoke-virtual {v0}, Lcom/twitter/android/widget/t;->b()V

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    invoke-static {p2}, Lcom/twitter/android/widget/t;->c(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->g:Lcom/twitter/android/widget/ab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->f:Lcom/twitter/android/widget/t;

    invoke-virtual {v0}, Lcom/twitter/android/widget/t;->c()I

    move-result v0

    if-lt p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->g:Lcom/twitter/android/widget/ab;

    invoke-static {p2}, Lcom/twitter/android/widget/t;->b(Landroid/view/View;)Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/widget/ab;->b(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/widget/GalleryGridFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->f:Lcom/twitter/android/widget/t;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/t;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    return-void
.end method

.method public onPause()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->f:Lcom/twitter/android/widget/t;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/t;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryGridFragment;->e:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->invalidate()V

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryGridFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->g:Lcom/twitter/android/widget/ab;

    if-eqz v1, :cond_1

    invoke-virtual {p1, v0}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/widget/GalleryGridFragment;->g:Lcom/twitter/android/widget/ab;

    if-eqz v1, :cond_0

    if-nez p2, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-interface {v2, v0}, Lcom/twitter/android/widget/ab;->e(Z)V

    :cond_1
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2

    const/4 v0, 0x2

    if-eq p2, v0, :cond_0

    if-nez p2, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/widget/GalleryGridFragment;->f:Lcom/twitter/android/widget/t;

    if-ne p2, v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/t;->c(Z)V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/widget/aa;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/widget/aa;-><init>(Lcom/twitter/android/widget/GalleryGridFragment;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-void
.end method
