.class Lcom/twitter/android/widget/be;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/bb;


# instance fields
.field private a:Lcom/twitter/android/widget/bd;

.field private b:Landroid/widget/MediaController$MediaPlayerControl;

.field private c:Z

.field private d:Lcom/twitter/android/widget/ba;


# direct methods
.method public constructor <init>(Lcom/twitter/android/widget/bd;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/widget/be;->a:Lcom/twitter/android/widget/bd;

    iput-object p1, p0, Lcom/twitter/android/widget/be;->a:Lcom/twitter/android/widget/bd;

    new-instance v0, Lcom/twitter/android/widget/ba;

    invoke-direct {v0, p0}, Lcom/twitter/android/widget/ba;-><init>(Lcom/twitter/android/widget/be;)V

    iput-object v0, p0, Lcom/twitter/android/widget/be;->d:Lcom/twitter/android/widget/ba;

    return-void
.end method

.method private b()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/be;->b:Landroid/widget/MediaController$MediaPlayerControl;

    invoke-interface {v0}, Landroid/widget/MediaController$MediaPlayerControl;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/be;->b:Landroid/widget/MediaController$MediaPlayerControl;

    invoke-interface {v0}, Landroid/widget/MediaController$MediaPlayerControl;->pause()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/be;->b:Landroid/widget/MediaController$MediaPlayerControl;

    invoke-interface {v0}, Landroid/widget/MediaController$MediaPlayerControl;->start()V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/widget/be;->b()V

    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public hide()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/widget/be;->c:Z

    iget-object v0, p0, Lcom/twitter/android/widget/be;->a:Lcom/twitter/android/widget/bd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/be;->a:Lcom/twitter/android/widget/bd;

    invoke-interface {v0}, Lcom/twitter/android/widget/bd;->d()V

    :cond_0
    return-void
.end method

.method public isShowing()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/widget/be;->c:Z

    return v0
.end method

.method public setMediaPlayer(Landroid/widget/MediaController$MediaPlayerControl;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/be;->b:Landroid/widget/MediaController$MediaPlayerControl;

    return-void
.end method

.method public show(I)V
    .locals 4

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/twitter/android/widget/be;->c:Z

    iget-object v0, p0, Lcom/twitter/android/widget/be;->a:Lcom/twitter/android/widget/bd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/be;->a:Lcom/twitter/android/widget/bd;

    invoke-interface {v0, p1}, Lcom/twitter/android/widget/bd;->b(I)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/be;->d:Lcom/twitter/android/widget/ba;

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/ba;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/widget/be;->d:Lcom/twitter/android/widget/ba;

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/ba;->removeMessages(I)V

    iget-object v1, p0, Lcom/twitter/android/widget/be;->d:Lcom/twitter/android/widget/ba;

    int-to-long v2, p1

    invoke-virtual {v1, v0, v2, v3}, Lcom/twitter/android/widget/ba;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_1
    return-void
.end method
