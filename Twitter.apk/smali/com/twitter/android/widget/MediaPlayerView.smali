.class public Lcom/twitter/android/widget/MediaPlayerView;
.super Landroid/view/SurfaceView;
.source "Twttr"

# interfaces
.implements Landroid/media/MediaPlayer$OnBufferingUpdateListener;
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnInfoListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Landroid/media/MediaPlayer$OnVideoSizeChangedListener;
.implements Landroid/view/SurfaceHolder$Callback;
.implements Landroid/widget/MediaController$MediaPlayerControl;


# instance fields
.field private a:Landroid/media/MediaPlayer;

.field private b:Lcom/twitter/android/widget/bb;

.field private final c:Landroid/view/SurfaceHolder;

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:Ljava/util/ArrayList;

.field private j:I

.field private k:Z

.field private l:Z

.field private m:Lcom/twitter/android/widget/bd;

.field private n:Landroid/widget/ProgressBar;

.field private o:Landroid/widget/ImageButton;

.field private p:Landroid/view/View;

.field private final q:Lkp;

.field private final r:Lkr;

.field private s:F

.field private t:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/widget/MediaPlayerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/widget/MediaPlayerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    const/16 v3, 0x64

    const/4 v2, 0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Lcom/twitter/android/widget/ay;

    invoke-direct {v0, p0}, Lcom/twitter/android/widget/ay;-><init>(Lcom/twitter/android/widget/MediaPlayerView;)V

    iput-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->r:Lkr;

    new-instance v0, Lcom/twitter/android/widget/az;

    invoke-direct {v0, p0}, Lcom/twitter/android/widget/az;-><init>(Lcom/twitter/android/widget/MediaPlayerView;)V

    iput-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->t:Landroid/view/View$OnClickListener;

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->g:I

    invoke-virtual {p0}, Lcom/twitter/android/widget/MediaPlayerView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    iput-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->c:Landroid/view/SurfaceHolder;

    invoke-virtual {p0, v2}, Lcom/twitter/android/widget/MediaPlayerView;->setFocusable(Z)V

    invoke-virtual {p0, v2}, Lcom/twitter/android/widget/MediaPlayerView;->setFocusableInTouchMode(Z)V

    invoke-virtual {p0}, Lcom/twitter/android/widget/MediaPlayerView;->requestFocus()Z

    new-instance v0, Lkp;

    iget-object v1, p0, Lcom/twitter/android/widget/MediaPlayerView;->r:Lkr;

    invoke-direct {v0, p1, v1}, Lkp;-><init>(Landroid/content/Context;Lkr;)V

    iput-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->q:Lkp;

    invoke-static {v3, v3}, Lkp;->a(II)F

    move-result v0

    iput v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->s:F

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/widget/MediaPlayerView;)I
    .locals 2

    iget v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->j:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/twitter/android/widget/MediaPlayerView;->j:I

    return v0
.end method

.method private a(I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->m:Lcom/twitter/android/widget/bd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->m:Lcom/twitter/android/widget/bd;

    invoke-interface {v0, p1}, Lcom/twitter/android/widget/bd;->c(I)V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/widget/MediaPlayerView;)Landroid/media/MediaPlayer;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->a:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method private b(I)Z
    .locals 1

    iget v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->g:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(I)V
    .locals 2

    iget v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->g:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->g:I

    return-void
.end method

.method static synthetic c(Lcom/twitter/android/widget/MediaPlayerView;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/widget/MediaPlayerView;->h()V

    return-void
.end method

.method static synthetic d(Lcom/twitter/android/widget/MediaPlayerView;)Lcom/twitter/android/widget/bd;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->m:Lcom/twitter/android/widget/bd;

    return-object v0
.end method

.method private g()Z
    .locals 1

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/MediaPlayerView;->b(I)Z

    move-result v0

    return v0
.end method

.method private h()V
    .locals 2

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->o:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->p:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->o:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->p:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private i()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->o:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->p:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->o:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->p:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private j()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/MediaPlayerView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->b:Lcom/twitter/android/widget/bb;

    invoke-interface {v0}, Lcom/twitter/android/widget/bb;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/widget/MediaPlayerView;->e()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/widget/MediaPlayerView;->d()V

    goto :goto_0
.end method

.method private setState(I)V
    .locals 1

    iget v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->g:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->g:I

    return-void
.end method


# virtual methods
.method a()V
    .locals 3

    const/16 v0, 0x64

    const/16 v1, 0x32

    invoke-static {v0, v1}, Lkp;->a(II)F

    move-result v0

    iput v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->s:F

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->a:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->a:Landroid/media/MediaPlayer;

    iget v1, p0, Lcom/twitter/android/widget/MediaPlayerView;->s:F

    iget v2, p0, Lcom/twitter/android/widget/MediaPlayerView;->s:F

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setVolume(FF)V

    :cond_0
    return-void
.end method

.method public a(Landroid/widget/ImageButton;Landroid/view/View;)V
    .locals 2

    iput-object p1, p0, Lcom/twitter/android/widget/MediaPlayerView;->o:Landroid/widget/ImageButton;

    iput-object p2, p0, Lcom/twitter/android/widget/MediaPlayerView;->p:Landroid/view/View;

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->o:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/twitter/android/widget/MediaPlayerView;->t:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->p:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/widget/MediaPlayerView;->t:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public a(Ljava/util/ArrayList;II)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/MediaPlayerView;->i:Ljava/util/ArrayList;

    iput p2, p0, Lcom/twitter/android/widget/MediaPlayerView;->j:I

    iput p3, p0, Lcom/twitter/android/widget/MediaPlayerView;->h:I

    return-void
.end method

.method b()V
    .locals 3

    const/16 v0, 0x64

    invoke-static {v0, v0}, Lkp;->a(II)F

    move-result v0

    iput v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->s:F

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->a:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->a:Landroid/media/MediaPlayer;

    iget v1, p0, Lcom/twitter/android/widget/MediaPlayerView;->s:F

    iget v2, p0, Lcom/twitter/android/widget/MediaPlayerView;->s:F

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setVolume(FF)V

    :cond_0
    return-void
.end method

.method public c()V
    .locals 4

    invoke-direct {p0}, Lcom/twitter/android/widget/MediaPlayerView;->g()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/widget/MediaPlayerView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/widget/MediaPlayerView;->a:Landroid/media/MediaPlayer;

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->i:Ljava/util/ArrayList;

    iget v2, p0, Lcom/twitter/android/widget/MediaPlayerView;->j:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/MediaDescriptor;

    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/android/widget/MediaPlayerView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, v0, Lcom/twitter/library/util/MediaDescriptor;->a:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v1, p0}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    invoke-virtual {v1, p0}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    invoke-virtual {v1, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    invoke-virtual {v1, p0}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    invoke-virtual {v1, p0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    invoke-virtual {v1, p0}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    iget-object v2, p0, Lcom/twitter/android/widget/MediaPlayerView;->c:Landroid/view/SurfaceHolder;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    iget-boolean v2, p0, Lcom/twitter/android/widget/MediaPlayerView;->k:Z

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setLooping(Z)V

    iget v2, p0, Lcom/twitter/android/widget/MediaPlayerView;->s:F

    iget v3, p0, Lcom/twitter/android/widget/MediaPlayerView;->s:F

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaPlayer;->setVolume(FF)V

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepareAsync()V

    iget-boolean v0, v0, Lcom/twitter/library/util/MediaDescriptor;->b:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->l:Z

    if-eqz v0, :cond_2

    new-instance v0, Lcom/twitter/android/widget/be;

    iget-object v1, p0, Lcom/twitter/android/widget/MediaPlayerView;->m:Lcom/twitter/android/widget/bd;

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/be;-><init>(Lcom/twitter/android/widget/bd;)V

    :goto_1
    invoke-interface {v0, p0}, Lcom/twitter/android/widget/bb;->a(Landroid/view/View;)V

    invoke-interface {v0, p0}, Lcom/twitter/android/widget/bb;->setMediaPlayer(Landroid/widget/MediaController$MediaPlayerControl;)V

    iput-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->b:Lcom/twitter/android/widget/bb;

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/twitter/android/widget/MediaPlayerView;->f()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/MediaPlayerView;->a(I)V

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/twitter/android/widget/bc;

    invoke-virtual {p0}, Lcom/twitter/android/widget/MediaPlayerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/widget/bc;-><init>(Lcom/twitter/android/widget/MediaPlayerView;Landroid/content/Context;)V

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->b:Lcom/twitter/android/widget/bb;

    goto :goto_0
.end method

.method public canPause()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->a:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canSeekBackward()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->a:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/MediaPlayerView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canSeekForward()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->a:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/MediaPlayerView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->b:Lcom/twitter/android/widget/bb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->b:Lcom/twitter/android/widget/bb;

    const/16 v1, 0xbb8

    invoke-interface {v0, v1}, Lcom/twitter/android/widget/bb;->show(I)V

    :cond_0
    return-void
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->b:Lcom/twitter/android/widget/bb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->b:Lcom/twitter/android/widget/bb;

    invoke-interface {v0}, Lcom/twitter/android/widget/bb;->hide()V

    :cond_0
    return-void
.end method

.method public f()V
    .locals 2

    const/16 v1, 0x10

    invoke-direct {p0, v1}, Lcom/twitter/android/widget/MediaPlayerView;->b(I)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->a:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->a:Landroid/media/MediaPlayer;

    :cond_0
    iput v1, p0, Lcom/twitter/android/widget/MediaPlayerView;->g:I

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->q:Lkp;

    invoke-virtual {v0}, Lkp;->b()V

    return-void
.end method

.method public getAudioSessionId()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->a:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getAudioSessionId()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBufferPercentage()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->a:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->f:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentPosition()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->a:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDuration()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->a:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->a:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBufferingUpdate(Landroid/media/MediaPlayer;I)V
    .locals 0

    iput p2, p0, Lcom/twitter/android/widget/MediaPlayerView;->f:I

    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 3

    const/4 v2, 0x0

    iget v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->j:I

    iget v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->j:I

    iget-object v1, p0, Lcom/twitter/android/widget/MediaPlayerView;->i:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/widget/MediaPlayerView;->f()V

    iput v2, p0, Lcom/twitter/android/widget/MediaPlayerView;->g:I

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->n:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->n:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/widget/MediaPlayerView;->c()V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/widget/MediaPlayerView;->e()V

    invoke-direct {p0}, Lcom/twitter/android/widget/MediaPlayerView;->i()V

    goto :goto_0
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 1

    const/16 v0, 0x64

    if-ne p2, v0, :cond_0

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/MediaPlayerView;->a(I)V

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/widget/MediaPlayerView;->f()V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/MediaPlayerView;->a(I)V

    goto :goto_0
.end method

.method public onInfo(Landroid/media/MediaPlayer;II)Z
    .locals 3

    const/4 v2, 0x0

    const/16 v0, 0x321

    if-ne p2, v0, :cond_1

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/MediaPlayerView;->c(I)V

    :cond_0
    :goto_0
    return v2

    :cond_1
    const/16 v0, 0x2bd

    if-ne p2, v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->n:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->n:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :cond_2
    const/16 v0, 0x2be

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->n:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->n:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x4

    if-eq p1, v1, :cond_2

    const/16 v1, 0x18

    if-eq p1, v1, :cond_2

    const/16 v1, 0x19

    if-eq p1, v1, :cond_2

    const/16 v1, 0xa4

    if-eq p1, v1, :cond_2

    const/16 v1, 0x52

    if-eq p1, v1, :cond_2

    const/4 v1, 0x5

    if-eq p1, v1, :cond_2

    const/4 v1, 0x6

    if-eq p1, v1, :cond_2

    move v1, v0

    :goto_0
    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/twitter/android/widget/MediaPlayerView;->b:Lcom/twitter/android/widget/bb;

    if-eqz v1, :cond_8

    invoke-direct {p0}, Lcom/twitter/android/widget/MediaPlayerView;->g()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x4f

    if-eq p1, v1, :cond_0

    const/16 v1, 0x55

    if-ne p1, v1, :cond_4

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/widget/MediaPlayerView;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/widget/MediaPlayerView;->pause()V

    invoke-virtual {p0}, Lcom/twitter/android/widget/MediaPlayerView;->d()V

    :cond_1
    :goto_1
    return v0

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/widget/MediaPlayerView;->start()V

    invoke-virtual {p0}, Lcom/twitter/android/widget/MediaPlayerView;->e()V

    goto :goto_1

    :cond_4
    const/16 v1, 0x7e

    if-ne p1, v1, :cond_5

    iget-object v1, p0, Lcom/twitter/android/widget/MediaPlayerView;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/widget/MediaPlayerView;->start()V

    invoke-virtual {p0}, Lcom/twitter/android/widget/MediaPlayerView;->e()V

    goto :goto_1

    :cond_5
    const/16 v1, 0x56

    if-eq p1, v1, :cond_6

    const/16 v1, 0x7f

    if-ne p1, v1, :cond_8

    :cond_6
    iget-object v1, p0, Lcom/twitter/android/widget/MediaPlayerView;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/widget/MediaPlayerView;->pause()V

    invoke-virtual {p0}, Lcom/twitter/android/widget/MediaPlayerView;->d()V

    goto :goto_1

    :cond_7
    invoke-direct {p0}, Lcom/twitter/android/widget/MediaPlayerView;->j()V

    :cond_8
    invoke-super {p0, p1, p2}, Landroid/view/SurfaceView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 6

    iget v2, p0, Lcom/twitter/android/widget/MediaPlayerView;->d:I

    iget v3, p0, Lcom/twitter/android/widget/MediaPlayerView;->e:I

    invoke-static {v2, p1}, Lcom/twitter/android/widget/MediaPlayerView;->getDefaultSize(II)I

    move-result v1

    invoke-static {v3, p2}, Lcom/twitter/android/widget/MediaPlayerView;->getDefaultSize(II)I

    move-result v0

    if-lez v2, :cond_0

    if-lez v3, :cond_0

    mul-int v4, v2, v0

    mul-int v5, v3, v1

    if-le v4, v5, :cond_1

    mul-int v0, v1, v3

    div-int/2addr v0, v2

    :cond_0
    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/widget/MediaPlayerView;->setMeasuredDimension(II)V

    return-void

    :cond_1
    if-ge v4, v5, :cond_0

    div-int v1, v4, v3

    goto :goto_0
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 1

    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/MediaPlayerView;->setState(I)V

    invoke-virtual {p0}, Lcom/twitter/android/widget/MediaPlayerView;->start()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->b:Lcom/twitter/android/widget/bb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->b:Lcom/twitter/android/widget/bb;

    invoke-interface {v0}, Lcom/twitter/android/widget/bb;->a()V

    invoke-direct {p0}, Lcom/twitter/android/widget/MediaPlayerView;->j()V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->b:Lcom/twitter/android/widget/bb;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/widget/MediaPlayerView;->j()V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onVideoSizeChanged(Landroid/media/MediaPlayer;II)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/MediaPlayerView;->setState(I)V

    if-lez p2, :cond_0

    if-lez p3, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V

    :cond_0
    iput p2, p0, Lcom/twitter/android/widget/MediaPlayerView;->d:I

    iput p3, p0, Lcom/twitter/android/widget/MediaPlayerView;->e:I

    invoke-virtual {p0}, Lcom/twitter/android/widget/MediaPlayerView;->start()V

    return-void
.end method

.method public pause()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->a:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->q:Lkp;

    invoke-virtual {v0}, Lkp;->b()V

    :cond_0
    return-void
.end method

.method public seekTo(I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->a:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    :cond_0
    return-void
.end method

.method public setIsLooping(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/widget/MediaPlayerView;->k:Z

    return-void
.end method

.method public setMediaControllerListener(Lcom/twitter/android/widget/bd;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/MediaPlayerView;->m:Lcom/twitter/android/widget/bd;

    return-void
.end method

.method public setProgressBar(Landroid/widget/ProgressBar;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/MediaPlayerView;->n:Landroid/widget/ProgressBar;

    return-void
.end method

.method public setStartPosition(I)V
    .locals 0

    iput p1, p0, Lcom/twitter/android/widget/MediaPlayerView;->h:I

    return-void
.end method

.method public setUseSimplePlayPauseControls(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/widget/MediaPlayerView;->l:Z

    return-void
.end method

.method public start()V
    .locals 4

    const/16 v3, 0x8

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/MediaPlayerView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/MediaPlayerView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->q:Lkp;

    invoke-virtual {v0}, Lkp;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0, v3}, Lcom/twitter/android/widget/MediaPlayerView;->setState(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->m:Lcom/twitter/android/widget/bd;

    invoke-interface {v0}, Lcom/twitter/android/widget/bd;->f()V

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->c:Landroid/view/SurfaceHolder;

    iget v1, p0, Lcom/twitter/android/widget/MediaPlayerView;->d:I

    iget v2, p0, Lcom/twitter/android/widget/MediaPlayerView;->e:I

    invoke-interface {v0, v1, v2}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    iget v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->h:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->a:Landroid/media/MediaPlayer;

    iget v1, p0, Lcom/twitter/android/widget/MediaPlayerView;->h:I

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->h:I

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->n:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/MediaPlayerView;->n:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/widget/MediaPlayerView;->d()V

    :cond_2
    :goto_0
    return-void

    :cond_3
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/MediaPlayerView;->a(I)V

    goto :goto_0
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/widget/MediaPlayerView;->c()V

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/widget/MediaPlayerView;->f()V

    return-void
.end method
