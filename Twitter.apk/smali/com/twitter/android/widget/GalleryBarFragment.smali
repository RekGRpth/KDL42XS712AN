.class public Lcom/twitter/android/widget/GalleryBarFragment;
.super Landroid/support/v4/app/Fragment;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/twitter/internal/android/widget/v;


# instance fields
.field protected a:Lcom/twitter/android/widget/z;

.field private b:Lcom/twitter/internal/android/widget/HorizontalListView;

.field private c:Lcom/twitter/android/widget/t;

.field private d:Lcom/twitter/android/MemoryImageCache;

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryBarFragment;->c:Lcom/twitter/android/widget/t;

    invoke-virtual {v0, p2}, Lcom/twitter/android/widget/t;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    return-void
.end method

.method public a(Lcom/twitter/android/widget/z;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/GalleryBarFragment;->a:Lcom/twitter/android/widget/z;

    return-void
.end method

.method public a(Lcom/twitter/internal/android/widget/HorizontalListView;Z)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryBarFragment;->c:Lcom/twitter/android/widget/t;

    invoke-virtual {v0, p2}, Lcom/twitter/android/widget/t;->c(Z)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryBarFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/MemoryImageCache;->a(Landroid/support/v4/app/FragmentManager;)Lcom/twitter/android/MemoryImageCache;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryBarFragment;->d:Lcom/twitter/android/MemoryImageCache;

    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryBarFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0072    # com.twitter.android.R.dimen.gallery_bar_cell_size

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/widget/GalleryBarFragment;->e:I

    new-instance v0, Lcom/twitter/android/widget/t;

    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryBarFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/widget/GalleryBarFragment;->d:Lcom/twitter/android/MemoryImageCache;

    iget v3, p0, Lcom/twitter/android/widget/GalleryBarFragment;->e:I

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/android/widget/t;-><init>(Landroid/content/Context;Lcom/twitter/android/MemoryImageCache;IZ)V

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryBarFragment;->c:Lcom/twitter/android/widget/t;

    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 2

    const-string/jumbo v0, "_data NOT NULL AND _data != ? AND _size > ?"

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryBarFragment;->c:Lcom/twitter/android/widget/t;

    const-string/jumbo v1, "_data NOT NULL AND _data != ? AND _size > ?"

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/t;->a(Ljava/lang/String;)Landroid/support/v4/content/CursorLoader;

    move-result-object v0

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    const/4 v5, 0x0

    const v0, 0x7f03007b    # com.twitter.android.R.layout.gallery_bar

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/HorizontalListView;

    const v1, 0x7f03007c    # com.twitter.android.R.layout.gallery_bar_image_camera

    invoke-virtual {p1, v1, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/twitter/internal/android/widget/HorizontalListView$LayoutParams;

    iget v3, p0, Lcom/twitter/android/widget/GalleryBarFragment;->e:I

    iget v4, p0, Lcom/twitter/android/widget/GalleryBarFragment;->e:I

    invoke-direct {v2, v3, v4}, Lcom/twitter/internal/android/widget/HorizontalListView$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/twitter/android/widget/GalleryBarFragment;->c:Lcom/twitter/android/widget/t;

    invoke-virtual {v2, v1}, Lcom/twitter/android/widget/t;->a(Landroid/view/View;)V

    iget-object v1, p0, Lcom/twitter/android/widget/GalleryBarFragment;->c:Lcom/twitter/android/widget/t;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->setOnScrollListener(Lcom/twitter/internal/android/widget/v;)V

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/HorizontalListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {v0, v5}, Lcom/twitter/internal/android/widget/HorizontalListView;->setClipToPadding(Z)V

    iput-object v0, p0, Lcom/twitter/android/widget/GalleryBarFragment;->b:Lcom/twitter/internal/android/widget/HorizontalListView;

    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryBarFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v5, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryBarFragment;->c:Lcom/twitter/android/widget/t;

    invoke-virtual {v0}, Lcom/twitter/android/widget/t;->b()V

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryBarFragment;->a:Lcom/twitter/android/widget/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryBarFragment;->c:Lcom/twitter/android/widget/t;

    invoke-virtual {v0}, Lcom/twitter/android/widget/t;->c()I

    move-result v0

    if-ge p3, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryBarFragment;->a:Lcom/twitter/android/widget/z;

    invoke-interface {v0, p3}, Lcom/twitter/android/widget/z;->a(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/GalleryBarFragment;->a:Lcom/twitter/android/widget/z;

    invoke-static {p2}, Lcom/twitter/android/widget/t;->b(Landroid/view/View;)Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/widget/z;->a(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/widget/GalleryBarFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryBarFragment;->c:Lcom/twitter/android/widget/t;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/t;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    return-void
.end method

.method public onPause()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/GalleryBarFragment;->c:Lcom/twitter/android/widget/t;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/t;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    invoke-virtual {p0}, Lcom/twitter/android/widget/GalleryBarFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method
