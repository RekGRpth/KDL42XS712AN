.class public Lcom/twitter/android/widget/ScribeUrlPreference;
.super Lcom/twitter/android/widget/DebugUrlPreference;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8

    const-string/jumbo v3, "scribe_endpoint_enabled"

    const-string/jumbo v4, "scribe_endpoint_url"

    const-string/jumbo v5, "Enable Scribe Endpoint"

    const-string/jumbo v6, "Example: https://twitter.com/scribe"

    const-string/jumbo v7, "https://twitter.com/scribe"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/widget/DebugUrlPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method a()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "scribe"

    return-object v0
.end method
