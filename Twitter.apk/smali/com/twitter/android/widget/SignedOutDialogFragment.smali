.class public Lcom/twitter/android/widget/SignedOutDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "Twttr"


# static fields
.field private static a:Ljava/lang/String;

.field private static b:Lcom/twitter/android/SignedOutActivity;

.field private static c:Lcom/twitter/library/widget/TweetView;

.field private static d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic a()Lcom/twitter/android/SignedOutActivity;
    .locals 1

    sget-object v0, Lcom/twitter/android/widget/SignedOutDialogFragment;->b:Lcom/twitter/android/SignedOutActivity;

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lcom/twitter/android/SignedOutActivity;Lcom/twitter/library/widget/TweetView;Ljava/lang/String;)Lcom/twitter/android/widget/SignedOutDialogFragment;
    .locals 1

    new-instance v0, Lcom/twitter/android/widget/SignedOutDialogFragment;

    invoke-direct {v0}, Lcom/twitter/android/widget/SignedOutDialogFragment;-><init>()V

    sput-object p0, Lcom/twitter/android/widget/SignedOutDialogFragment;->a:Ljava/lang/String;

    sput-object p1, Lcom/twitter/android/widget/SignedOutDialogFragment;->b:Lcom/twitter/android/SignedOutActivity;

    sput-object p2, Lcom/twitter/android/widget/SignedOutDialogFragment;->c:Lcom/twitter/library/widget/TweetView;

    sput-object p3, Lcom/twitter/android/widget/SignedOutDialogFragment;->d:Ljava/lang/String;

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lcom/twitter/android/SignedOutActivity;Ljava/lang/String;)Lcom/twitter/android/widget/SignedOutDialogFragment;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, v0, p2}, Lcom/twitter/android/widget/SignedOutDialogFragment;->a(Ljava/lang/String;Lcom/twitter/android/SignedOutActivity;Lcom/twitter/library/widget/TweetView;Ljava/lang/String;)Lcom/twitter/android/widget/SignedOutDialogFragment;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/twitter/android/widget/SignedOutDialogFragment;->a:Ljava/lang/String;

    return-object v0
.end method

.method private c()Landroid/app/AlertDialog;
    .locals 7

    const/4 v5, 0x1

    new-instance v0, Landroid/app/AlertDialog$Builder;

    sget-object v1, Lcom/twitter/android/widget/SignedOutDialogFragment;->b:Lcom/twitter/android/SignedOutActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/twitter/android/widget/SignedOutDialogFragment;->b:Lcom/twitter/android/SignedOutActivity;

    invoke-virtual {v1}, Lcom/twitter/android/SignedOutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f022c    # com.twitter.android.R.string.logged_out_inline_dialog_signup

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/widget/cj;

    invoke-direct {v2, p0}, Lcom/twitter/android/widget/cj;-><init>(Lcom/twitter/android/widget/SignedOutDialogFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/widget/SignedOutDialogFragment;->b:Lcom/twitter/android/SignedOutActivity;

    invoke-virtual {v1}, Lcom/twitter/android/SignedOutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f022b    # com.twitter.android.R.string.logged_out_inline_dialog_signin

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/widget/ci;

    invoke-direct {v2, p0}, Lcom/twitter/android/widget/ci;-><init>(Lcom/twitter/android/widget/SignedOutDialogFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/widget/SignedOutDialogFragment;->b:Lcom/twitter/android/SignedOutActivity;

    invoke-static {v1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    const-wide/16 v3, 0x0

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v3, v5, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "loggedout_dialog:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/twitter/android/widget/SignedOutDialogFragment;->a:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ":::impression"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-object v0
.end method

.method private d()Landroid/app/AlertDialog;
    .locals 8

    invoke-direct {p0}, Lcom/twitter/android/widget/SignedOutDialogFragment;->c()Landroid/app/AlertDialog;

    move-result-object v6

    sget-object v0, Lcom/twitter/android/widget/SignedOutDialogFragment;->b:Lcom/twitter/android/SignedOutActivity;

    invoke-virtual {v0}, Lcom/twitter/android/SignedOutActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300ad    # com.twitter.android.R.layout.logged_out_profile_photo_dialog

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    const v0, 0x7f0901b9    # com.twitter.android.R.id.logged_out_profile_photo

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    sget-object v1, Lcom/twitter/android/widget/SignedOutDialogFragment;->c:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v1}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/library/provider/Tweet;

    move-result-object v3

    sget-object v1, Lcom/twitter/android/widget/SignedOutDialogFragment;->b:Lcom/twitter/android/SignedOutActivity;

    invoke-static {v1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    iget v2, v3, Lcom/twitter/library/provider/Tweet;->v:I

    iget-object v4, v3, Lcom/twitter/library/provider/Tweet;->k:Ljava/lang/String;

    invoke-interface {v1, v2, v4}, Lcom/twitter/library/widget/ap;->a(ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v7}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v2, v4, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v3}, Lcom/twitter/library/provider/Tweet;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    const v0, 0x7f0901ba    # com.twitter.android.R.id.logged_out_username

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v3}, Lcom/twitter/library/provider/Tweet;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0901bb    # com.twitter.android.R.id.logged_out_handle

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " @"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v3}, Lcom/twitter/library/provider/Tweet;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-object v0, Lcom/twitter/android/widget/SignedOutDialogFragment;->b:Lcom/twitter/android/SignedOutActivity;

    invoke-virtual {v3}, Lcom/twitter/library/provider/Tweet;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3}, Lcom/twitter/library/provider/Tweet;->g()J

    move-result-wide v4

    invoke-virtual {v0, v1, v4, v5}, Lcom/twitter/android/SignedOutActivity;->a(Ljava/lang/String;J)V

    sget-object v0, Lcom/twitter/android/widget/SignedOutDialogFragment;->b:Lcom/twitter/android/SignedOutActivity;

    invoke-virtual {v0}, Lcom/twitter/android/SignedOutActivity;->f()Lcom/twitter/android/ze;

    move-result-object v0

    invoke-virtual {v3}, Lcom/twitter/library/provider/Tweet;->g()J

    move-result-wide v1

    invoke-virtual {v3}, Lcom/twitter/library/provider/Tweet;->i()Ljava/lang/String;

    move-result-object v3

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/ze;->b(JLjava/lang/String;J)V

    const v0, 0x7f0901b8    # com.twitter.android.R.id.logged_out_message

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0f022d    # com.twitter.android.R.string.logged_out_profile_photo_click

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    return-object v6
.end method

.method private e()Landroid/app/AlertDialog;
    .locals 8

    const/4 v7, 0x0

    invoke-direct {p0}, Lcom/twitter/android/widget/SignedOutDialogFragment;->c()Landroid/app/AlertDialog;

    move-result-object v1

    sget-object v0, Lcom/twitter/android/widget/SignedOutDialogFragment;->b:Lcom/twitter/android/SignedOutActivity;

    invoke-virtual {v0}, Lcom/twitter/android/SignedOutActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0300ac    # com.twitter.android.R.layout.logged_out_inline_action_dialog

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f0901b8    # com.twitter.android.R.id.logged_out_message

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v3, Lcom/twitter/android/widget/SignedOutDialogFragment;->a:Ljava/lang/String;

    invoke-static {v3}, Lcom/twitter/library/view/TweetActionType;->valueOf(Ljava/lang/String;)Lcom/twitter/library/view/TweetActionType;

    move-result-object v3

    sget-object v4, Lcom/twitter/android/widget/ck;->a:[I

    invoke-virtual {v3}, Lcom/twitter/library/view/TweetActionType;->ordinal()I

    move-result v3

    aget v3, v4, v3

    packed-switch v3, :pswitch_data_0

    :goto_0
    sget-object v3, Lcom/twitter/android/widget/SignedOutDialogFragment;->b:Lcom/twitter/android/SignedOutActivity;

    invoke-virtual {v3}, Lcom/twitter/android/SignedOutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0081    # com.twitter.android.R.dimen.logged_out_dialog_padding

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    return-object v1

    :pswitch_0
    const v3, 0x7f0f022a    # com.twitter.android.R.string.logged_out_inline_dialog_retweet

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    const v3, 0x7f02027c    # com.twitter.android.R.drawable.ic_tweet_action_inline_retweet_on

    invoke-virtual {v0, v7, v3, v7, v7}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_0

    :pswitch_1
    const v3, 0x7f0f0227    # com.twitter.android.R.string.logged_out_inline_dialog_fav

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    const v3, 0x7f020276    # com.twitter.android.R.drawable.ic_tweet_action_inline_favorite_on

    invoke-virtual {v0, v7, v3, v7, v7}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_0

    :pswitch_2
    const v3, 0x7f0f0229    # com.twitter.android.R.string.logged_out_inline_dialog_reply

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    const v3, 0x7f02027d    # com.twitter.android.R.drawable.ic_tweet_action_inline_teach_reply

    invoke-virtual {v0, v7, v3, v7, v7}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_0

    :pswitch_3
    sget-object v3, Lcom/twitter/android/widget/SignedOutDialogFragment;->b:Lcom/twitter/android/SignedOutActivity;

    invoke-virtual {v3}, Lcom/twitter/android/SignedOutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0228    # com.twitter.android.R.string.logged_out_inline_dialog_follow

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    sget-object v6, Lcom/twitter/android/widget/SignedOutDialogFragment;->d:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v3, 0x7f0201c8    # com.twitter.android.R.drawable.ic_inline_action_follow_on

    invoke-virtual {v0, v7, v3, v7, v7}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public a(Landroid/support/v4/app/FragmentManager;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/widget/SignedOutDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/SignedOutDialogFragment;->setRetainInstance(Z)V

    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    sget-object v0, Lcom/twitter/android/widget/SignedOutDialogFragment;->a:Ljava/lang/String;

    sget-object v1, Lcom/twitter/android/SignedOutFragment;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/widget/SignedOutDialogFragment;->d()Landroid/app/AlertDialog;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/widget/SignedOutDialogFragment;->e()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/widget/SignedOutDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/widget/SignedOutDialogFragment;->getRetainInstance()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/widget/SignedOutDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setDismissMessage(Landroid/os/Message;)V

    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onDestroyView()V

    return-void
.end method
