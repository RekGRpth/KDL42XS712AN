.class public abstract Lcom/twitter/android/widget/TopicView;
.super Landroid/widget/RelativeLayout;
.source "Twttr"


# instance fields
.field private a:Lcom/twitter/android/widget/TopicView$TopicData;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/twitter/android/widget/TopicView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/widget/TopicView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/widget/TopicView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method protected static a(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 1

    if-eqz p0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method protected abstract a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end method

.method public a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLcom/twitter/library/widget/ap;Ljava/lang/String;Ljava/lang/String;[BZZLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12

    new-instance v0, Lcom/twitter/android/widget/TopicView$TopicData;

    move-object v1, p1

    move v2, p2

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object v5, p3

    move-object/from16 v6, p7

    move-object/from16 v7, p12

    move-object/from16 v8, p13

    move-object/from16 v9, p6

    move/from16 v10, p8

    move-object/from16 v11, p14

    invoke-direct/range {v0 .. v11}, Lcom/twitter/android/widget/TopicView$TopicData;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I[B)V

    iput-object v0, p0, Lcom/twitter/android/widget/TopicView;->a:Lcom/twitter/android/widget/TopicView$TopicData;

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I[B)V
    .locals 0

    return-void
.end method

.method public getImageKey()Lcom/twitter/library/util/m;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract getSeedHashtag()Ljava/lang/String;
.end method

.method public getTopicData()Lcom/twitter/android/widget/TopicView$TopicData;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/TopicView;->a:Lcom/twitter/android/widget/TopicView$TopicData;

    return-object v0
.end method

.method public abstract getTopicId()Ljava/lang/String;
.end method

.method public abstract getTopicType()I
.end method

.method public getUserImageKey()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public setCustomTopicDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    return-void
.end method

.method public setEventImages(Ljava/util/HashMap;)V
    .locals 0

    return-void
.end method

.method public setUserImage(Landroid/graphics/Bitmap;)V
    .locals 0

    return-void
.end method
