.class public Lcom/twitter/android/widget/UserCheckBoxPreference;
.super Landroid/preference/Preference;
.source "Twttr"


# instance fields
.field private a:Lcom/twitter/library/api/TwitterUser;

.field private b:Landroid/graphics/Bitmap;

.field private c:Z

.field private d:Z

.field private e:Landroid/widget/ImageView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/CheckBox;

.field private i:Landroid/widget/CheckBox;

.field private j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lgl;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->j:Z

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/widget/UserCheckBoxPreference;)Lcom/twitter/library/api/TwitterUser;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->a:Lcom/twitter/library/api/TwitterUser;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/widget/UserCheckBoxPreference;Ljava/lang/Object;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/widget/UserCheckBoxPreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/widget/UserCheckBoxPreference;)Landroid/widget/CheckBox;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->h:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/widget/UserCheckBoxPreference;Ljava/lang/Object;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/widget/UserCheckBoxPreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/twitter/android/widget/UserCheckBoxPreference;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->c:Z

    return v0
.end method

.method static synthetic d(Lcom/twitter/android/widget/UserCheckBoxPreference;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->d:Z

    return v0
.end method

.method static synthetic e(Lcom/twitter/android/widget/UserCheckBoxPreference;)Landroid/widget/CheckBox;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->i:Landroid/widget/CheckBox;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/library/api/TwitterUser;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->a:Lcom/twitter/library/api/TwitterUser;

    return-object v0
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->b:Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/twitter/android/widget/UserCheckBoxPreference;->notifyChanged()V

    return-void
.end method

.method public a(Lcom/twitter/library/api/TwitterUser;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->a:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {p0}, Lcom/twitter/android/widget/UserCheckBoxPreference;->notifyChanged()V

    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->c:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->c:Z

    invoke-virtual {p0}, Lcom/twitter/android/widget/UserCheckBoxPreference;->shouldDisableDependents()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/UserCheckBoxPreference;->notifyDependencyChange(Z)V

    invoke-virtual {p0}, Lcom/twitter/android/widget/UserCheckBoxPreference;->notifyChanged()V

    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->d:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->d:Z

    invoke-virtual {p0}, Lcom/twitter/android/widget/UserCheckBoxPreference;->shouldDisableDependents()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/UserCheckBoxPreference;->notifyDependencyChange(Z)V

    invoke-virtual {p0}, Lcom/twitter/android/widget/UserCheckBoxPreference;->notifyChanged()V

    :cond_0
    return-void
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->b:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->e:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->a:Lcom/twitter/library/api/TwitterUser;

    iget-object v1, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->f:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->g:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v3, 0x40

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, v0, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->h:Landroid/widget/CheckBox;

    iget-boolean v1, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->c:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v0, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->i:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->i:Landroid/widget/CheckBox;

    iget-boolean v1, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->d:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->e:Landroid/widget/ImageView;

    const v1, 0x7f02003a    # com.twitter.android.R.drawable.bg_no_profile_photo_md

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method protected onClick()V
    .locals 2

    const/4 v0, 0x0

    invoke-super {p0}, Landroid/preference/Preference;->onClick()V

    iget-boolean v1, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->j:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->c:Z

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_0

    const/16 v0, 0x10

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/UserCheckBoxPreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    :goto_1
    return-void

    :cond_2
    move v1, v0

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/UserCheckBoxPreference;->a(Z)V

    goto :goto_1
.end method

.method protected onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/preference/Preference;->onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03016c    # com.twitter.android.R.layout.user_checkbox_preference

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f09012d    # com.twitter.android.R.id.profile_image

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->e:Landroid/widget/ImageView;

    const v0, 0x7f090097    # com.twitter.android.R.id.name

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->f:Landroid/widget/TextView;

    const v0, 0x7f0900d2    # com.twitter.android.R.id.screen_name

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->g:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->j:Z

    if-eqz v0, :cond_0

    new-instance v2, Lcom/twitter/android/widget/ds;

    const/4 v0, 0x0

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/widget/ds;-><init>(Lcom/twitter/android/widget/UserCheckBoxPreference;Lcom/twitter/android/widget/dr;)V

    const v0, 0x7f0902b4    # com.twitter.android.R.id.checkbox_fav_people_device

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->h:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    const v0, 0x7f0902b5    # com.twitter.android.R.id.checkbox_fav_people_email

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->i:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->i:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->i:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    :goto_0
    return-object v1

    :cond_0
    const v0, 0x7f09021e    # com.twitter.android.R.id.checkbox

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->h:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onSetInitialValue(ZLjava/lang/Object;)V
    .locals 2

    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-boolean v1, p0, Lcom/twitter/android/widget/UserCheckBoxPreference;->j:Z

    if-eqz v1, :cond_1

    invoke-static {v0}, Lcom/twitter/library/provider/ay;->h(I)Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/UserCheckBoxPreference;->a(Z)V

    invoke-static {v0}, Lcom/twitter/library/provider/ay;->m(I)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/UserCheckBoxPreference;->b(Z)V

    :goto_1
    return-void

    :cond_0
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    :cond_1
    invoke-static {v0}, Lcom/twitter/library/provider/ay;->h(I)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/UserCheckBoxPreference;->a(Z)V

    goto :goto_1
.end method
