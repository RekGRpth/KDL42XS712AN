.class public Lcom/twitter/android/widget/g;
.super Lcom/twitter/android/aaa;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/content/Context;IILcom/twitter/library/widget/a;Lcom/twitter/library/util/FriendshipCache;I)V
    .locals 10

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move/from16 v7, p6

    invoke-direct/range {v0 .. v9}, Lcom/twitter/android/aaa;-><init>(Landroid/content/Context;IILcom/twitter/library/widget/a;Lcom/twitter/library/util/FriendshipCache;IIZZ)V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/widget/BaseUserView;Landroid/database/Cursor;J)V
    .locals 18

    const/4 v1, 0x6

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v2, 0x5

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v2, 0x4

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v2, 0x3

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, ""

    const/4 v9, 0x0

    const/16 v2, 0xa

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/twitter/library/api/PromotedContent;

    const/4 v2, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-long v11, v2

    const/4 v13, 0x0

    const/4 v2, 0x7

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    const/4 v15, 0x1

    :goto_0
    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    const/16 v16, 0x1

    :goto_1
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getPosition()I

    move-result v17

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-wide/from16 v3, p3

    invoke-virtual/range {v1 .. v17}, Lcom/twitter/android/widget/g;->a(Lcom/twitter/library/widget/BaseUserView;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/PromotedContent;JLjava/lang/String;IZZI)V

    return-void

    :cond_0
    const/4 v15, 0x0

    goto :goto_0

    :cond_1
    const/16 v16, 0x0

    goto :goto_1
.end method
