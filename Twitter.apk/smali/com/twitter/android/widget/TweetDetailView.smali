.class public Lcom/twitter/android/widget/TweetDetailView;
.super Landroid/widget/RelativeLayout;
.source "Twttr"


# static fields
.field private static final p:Ljava/text/SimpleDateFormat;


# instance fields
.field private A:Landroid/view/ViewGroup;

.field private B:Landroid/view/ViewGroup;

.field private C:Landroid/view/ViewGroup;

.field private D:Landroid/view/ViewGroup;

.field private E:Lcom/twitter/library/card/Card;

.field private F:Ljava/lang/CharSequence;

.field private G:Ljava/lang/CharSequence;

.field private H:Landroid/view/View;

.field private I:Lcom/twitter/internal/android/widget/TypefacesTextView;

.field private J:Lcom/twitter/internal/android/widget/TypefacesTextView;

.field private K:Lcom/twitter/library/api/TwitterStatus$Translation;

.field private L:Landroid/view/ViewGroup;

.field private M:Lcom/twitter/android/widget/TweetStatView;

.field private N:Lcom/twitter/android/widget/do;

.field private O:Z

.field private P:Lcom/twitter/library/util/m;

.field private Q:Lcom/twitter/library/api/TweetClassicCard;

.field private R:Lcom/twitter/internal/android/widget/ax;

.field private S:Lcom/twitter/library/widget/ActionButton;

.field private T:Z

.field private U:Lcom/twitter/library/api/TweetEntities;

.field private V:I

.field private W:Landroid/view/View;

.field private Z:Landroid/view/View;

.field public a:Landroid/widget/ImageView;

.field public b:Lcom/twitter/internal/android/widget/TypefacesTextView;

.field public c:Landroid/view/ViewGroup;

.field public d:Landroid/view/ViewGroup;

.field public e:Landroid/view/ViewGroup;

.field public f:Landroid/widget/TextView;

.field public g:Landroid/view/View;

.field public h:Landroid/view/View;

.field public i:Landroid/view/View;

.field public j:Landroid/view/View;

.field k:Lcom/twitter/library/api/ActivitySummary;

.field l:Lcom/twitter/android/widget/di;

.field m:Lcom/twitter/library/provider/Tweet;

.field n:Lcom/twitter/library/view/c;

.field o:Lcom/twitter/android/tq;

.field private q:Landroid/widget/TextView;

.field private r:Landroid/widget/TextView;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/widget/RelativeLayout;

.field private u:Lcom/twitter/library/widget/SocialBylineView;

.field private v:Landroid/widget/TextView;

.field private w:Landroid/widget/ImageView;

.field private x:Landroid/widget/TextView;

.field private y:Landroid/widget/TextView;

.field private z:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0}, Ljava/text/SimpleDateFormat;-><init>()V

    sput-object v0, Lcom/twitter/android/widget/TweetDetailView;->p:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/content/Context;)V

    return-void
.end method

.method private a(Lcom/twitter/library/api/TweetClassicCard;)Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/twitter/android/widget/df;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/widget/df;-><init>(Lcom/twitter/android/widget/TweetDetailView;Lcom/twitter/library/api/TweetClassicCard;)V

    invoke-static {v0}, Ljy;->a(Lcom/twitter/library/view/h;)Landroid/view/View$OnClickListener;

    move-result-object v0

    return-object v0
.end method

.method private a(ILcom/twitter/library/api/CardUser;Lcom/twitter/library/api/CardUser;Ljava/lang/String;Ljava/lang/String;)Landroid/view/ViewGroup;
    .locals 10

    const/4 v9, 0x1

    const/16 v8, 0x8

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v0, p1, p0, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v1, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/4 v3, 0x3

    const v4, 0x7f0902a0    # com.twitter.android.R.id.tweet_content

    invoke-virtual {v1, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v1, 0x7f09005b    # com.twitter.android.R.id.tweet_media_preview

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setId(I)V

    const v1, 0x7f0900b8    # com.twitter.android.R.id.author

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_0

    if-eqz p2, :cond_3

    new-array v3, v9, [Lcom/twitter/internal/android/widget/TypefacesSpan;

    new-instance v4, Lcom/twitter/internal/android/widget/TypefacesSpan;

    invoke-direct {v4, v2, v9}, Lcom/twitter/internal/android/widget/TypefacesSpan;-><init>(Landroid/content/Context;I)V

    aput-object v4, v3, v7

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0f04d3    # com.twitter.android.R.string.tweet_media_content_author

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p2, Lcom/twitter/library/api/CardUser;->fullName:Ljava/lang/String;

    aput-object v6, v5, v7

    iget-object v6, p2, Lcom/twitter/library/api/CardUser;->screenName:Ljava/lang/String;

    aput-object v6, v5, v9

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/16 v4, 0x22

    invoke-static {v3, v2, v4}, Lcom/twitter/library/util/Util;->a([Ljava/lang/Object;Ljava/lang/String;C)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    :goto_0
    const v1, 0x7f0900ba    # com.twitter.android.R.id.site_user

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_2

    if-eqz p3, :cond_4

    const v1, 0x7f090270    # com.twitter.android.R.id.site_user_name

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v3, p3, Lcom/twitter/library/api/CardUser;->fullName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p3, Lcom/twitter/library/api/CardUser;->screenName:Ljava/lang/String;

    if-eqz v1, :cond_1

    const v1, 0x7f090271    # com.twitter.android.R.id.site_user_screen_name

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v4, 0x40

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p3, Lcom/twitter/library/api/CardUser;->screenName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    iget-wide v3, p3, Lcom/twitter/library/api/CardUser;->userId:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_2
    :goto_1
    iput-object v2, p0, Lcom/twitter/android/widget/TweetDetailView;->g:Landroid/view/View;

    const v1, 0x7f090090    # com.twitter.android.R.id.title

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_2
    const v1, 0x7f0900b9    # com.twitter.android.R.id.desc

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_3
    return-object v0

    :cond_3
    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_4
    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_5
    invoke-virtual {v1, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    :cond_6
    invoke-virtual {v1, p5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3
.end method

.method private a(ILcom/twitter/library/api/TweetClassicCard;)Landroid/view/ViewGroup;
    .locals 6

    iget-object v2, p2, Lcom/twitter/library/api/TweetClassicCard;->siteUser:Lcom/twitter/library/api/CardUser;

    iget-object v3, p2, Lcom/twitter/library/api/TweetClassicCard;->authorUser:Lcom/twitter/library/api/CardUser;

    iget-object v4, p2, Lcom/twitter/library/api/TweetClassicCard;->title:Ljava/lang/String;

    iget-object v5, p2, Lcom/twitter/library/api/TweetClassicCard;->description:Ljava/lang/String;

    move-object v0, p0

    move v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/widget/TweetDetailView;->a(ILcom/twitter/library/api/CardUser;Lcom/twitter/library/api/CardUser;Ljava/lang/String;Ljava/lang/String;)Landroid/view/ViewGroup;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/widget/TweetDetailView;)Landroid/view/ViewGroup;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->D:Landroid/view/ViewGroup;

    return-object v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    sget-object v0, Lcom/twitter/android/widget/TweetDetailView;->p:Ljava/text/SimpleDateFormat;

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00eb    # com.twitter.android.R.string.datetime_format_long

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/widget/TweetDetailView;->O:Z

    invoke-static {p1}, Lcom/twitter/internal/android/widget/ax;->a(Landroid/content/Context;)Lcom/twitter/internal/android/widget/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->R:Lcom/twitter/internal/android/widget/ax;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->K:Lcom/twitter/library/api/TwitterStatus$Translation;

    return-void
.end method

.method private a(Landroid/content/Context;Landroid/view/View$OnClickListener;Landroid/view/ViewGroup;)V
    .locals 4

    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/4 v2, 0x3

    const v3, 0x7f0902a0    # com.twitter.android.R.id.tweet_content

    invoke-virtual {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v1, 0x7f09005b    # com.twitter.android.R.id.tweet_media_preview

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setId(I)V

    const v1, 0x7f0901cf    # com.twitter.android.R.id.media_display

    invoke-virtual {p3, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0901d1    # com.twitter.android.R.id.media_display_always

    invoke-virtual {p3, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/view/ViewGroup;)V

    return-void
.end method

.method private a(Landroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;IIIJZ)V
    .locals 3

    const v0, 0x7f020117    # com.twitter.android.R.drawable.ic_activity_follow_tweet_default

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->u:Lcom/twitter/library/widget/SocialBylineView;

    invoke-static/range {p1 .. p9}, Lcom/twitter/library/widget/TweetView;->a(Landroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;IIIJ)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/SocialBylineView;->setLabel(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->o:Lcom/twitter/android/tq;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->u:Lcom/twitter/library/widget/SocialBylineView;

    iget-object v2, p0, Lcom/twitter/android/widget/TweetDetailView;->o:Lcom/twitter/android/tq;

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/SocialBylineView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    packed-switch p2, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const v0, 0x7f02011d    # com.twitter.android.R.drawable.ic_activity_rt_tweet_default

    :goto_1
    :pswitch_2
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->u:Lcom/twitter/library/widget/SocialBylineView;

    invoke-virtual {v1, v0}, Lcom/twitter/library/widget/SocialBylineView;->setIcon(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->u:Lcom/twitter/library/widget/SocialBylineView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SocialBylineView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->u:Lcom/twitter/library/widget/SocialBylineView;

    invoke-virtual {v0, p10}, Lcom/twitter/library/widget/SocialBylineView;->setRenderRTL(Z)V

    goto :goto_0

    :pswitch_3
    const v0, 0x7f020115    # com.twitter.android.R.drawable.ic_activity_fave_tweet_default

    goto :goto_1

    :pswitch_4
    const v0, 0x7f02023e    # com.twitter.android.R.drawable.ic_social_proof_recommendation_default

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method private a(Landroid/content/res/Resources;ILjava/lang/String;Z)V
    .locals 11

    const/4 v5, 0x0

    const/4 v4, 0x0

    const-wide/16 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v6, v5

    move v7, v5

    move v10, p4

    invoke-direct/range {v0 .. v10}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;IIIJZ)V

    return-void
.end method

.method private a(Landroid/content/res/Resources;Lcom/twitter/library/provider/Tweet;IIZ)V
    .locals 6

    const/4 v5, 0x0

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->b:Lcom/twitter/internal/android/widget/TypefacesTextView;

    iget-object v2, p2, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v2, :cond_1

    iget-object v0, v2, Lcom/twitter/library/api/PromotedContent;->advertiserName:Ljava/lang/String;

    :goto_0
    if-eqz p5, :cond_2

    invoke-virtual {v1, v5, v5, p3, v5}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-virtual {v3, p4, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v5}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setVisibility(I)V

    if-eqz v2, :cond_0

    iget-object v0, v2, Lcom/twitter/library/api/PromotedContent;->socialContext:Ljava/lang/String;

    if-eqz v0, :cond_3

    const/4 v1, 0x3

    invoke-direct {p0, p1, v1, v0, p5}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/content/res/Resources;ILjava/lang/String;Z)V

    :cond_0
    :goto_2
    return-void

    :cond_1
    iget-object v0, p2, Lcom/twitter/library/provider/Tweet;->f:Ljava/lang/String;

    goto :goto_0

    :cond_2
    invoke-virtual {v1, p3, v5, v5, v5}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->u:Lcom/twitter/library/widget/SocialBylineView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SocialBylineView;->setVisibility(I)V

    goto :goto_2
.end method

.method private a(Landroid/content/res/Resources;Lcom/twitter/library/provider/Tweet;J)V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p2}, Lcom/twitter/library/provider/Tweet;->w()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;->a:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;

    iget-object v2, p0, Lcom/twitter/android/widget/TweetDetailView;->m:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v2}, Lcom/twitter/library/provider/Tweet;->w()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, p1, v2, p3, p4}, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;->a(Landroid/content/res/Resources;Ljava/util/ArrayList;J)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/widget/TweetDetailView;->s:Landroid/widget/TextView;

    new-instance v3, Lcom/twitter/android/widget/dd;

    invoke-direct {v3, p0, v0}, Lcom/twitter/android/widget/dd;-><init>(Lcom/twitter/android/widget/TweetDetailView;Ljava/util/ArrayList;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->t:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v1, 0xf

    invoke-virtual {v0, v1, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    invoke-direct {p0}, Lcom/twitter/android/widget/TweetDetailView;->i()V

    goto :goto_0
.end method

.method private a(Landroid/view/ViewGroup;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->B:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->B:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->B:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method private a(Landroid/view/ViewGroup;I)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/widget/TweetDetailView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :goto_0
    const/4 v1, 0x3

    invoke-virtual {v0, v1, p2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    goto :goto_0
.end method

.method private a(Lcom/twitter/android/client/c;Lcom/twitter/library/api/CardUser;)V
    .locals 3

    if-eqz p2, :cond_0

    iget-object v0, p1, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    const/4 v1, 0x2

    iget-object v2, p2, Lcom/twitter/library/api/CardUser;->profileImageUrl:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/twitter/library/widget/ap;->a(ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Landroid/view/ViewGroup;

    const v2, 0x7f09026f    # com.twitter.android.R.id.site_user_image

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v1, 0x7f0b0069    # com.twitter.android.R.color.placeholder_bg

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private a(Lcom/twitter/android/client/c;Ljava/util/List;)V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Landroid/view/ViewGroup;

    if-nez v1, :cond_0

    const v1, 0x7f0300e5    # com.twitter.android.R.layout.photo_preview

    move-object v0, p0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/widget/TweetDetailView;->a(ILcom/twitter/library/api/CardUser;Lcom/twitter/library/api/CardUser;Ljava/lang/String;Ljava/lang/String;)Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Landroid/view/ViewGroup;

    :cond_0
    move-object v3, v0

    const v0, 0x7f090210    # com.twitter.android.R.id.images

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/TweetMediaImagesView;

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/twitter/android/widget/TweetDetailView;->getMediaClickListener()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetMediaImagesView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, p2}, Lcom/twitter/library/widget/TweetMediaImagesView;->setMediaEntities(Ljava/util/List;)V

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetMediaImagesView;->setHeightWidthRatio(F)V

    :goto_0
    invoke-direct {p0, v3}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/view/ViewGroup;)V

    invoke-direct {p0, p1, v2}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/android/client/c;Lcom/twitter/library/api/CardUser;)V

    iput-object v2, p0, Lcom/twitter/android/widget/TweetDetailView;->Q:Lcom/twitter/library/api/TweetClassicCard;

    iput-object v2, p0, Lcom/twitter/android/widget/TweetDetailView;->P:Lcom/twitter/library/util/m;

    iput-boolean v6, p0, Lcom/twitter/android/widget/TweetDetailView;->T:Z

    return-void

    :pswitch_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetMediaImagesView;->setVisibility(I)V

    goto :goto_0

    :pswitch_1
    invoke-interface {p2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/MediaEntity;

    iget v4, v1, Lcom/twitter/library/api/MediaEntity;->width:I

    iget v1, v1, Lcom/twitter/library/api/MediaEntity;->height:I

    invoke-direct {p0, v4, v1}, Lcom/twitter/android/widget/TweetDetailView;->a(II)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-direct {p0, v4, v1}, Lcom/twitter/android/widget/TweetDetailView;->b(II)[I

    move-result-object v1

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    aget v5, v1, v7

    aget v1, v1, v6

    invoke-direct {v4, v5, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput v6, v4, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    invoke-virtual {v0, v4}, Lcom/twitter/library/widget/TweetMediaImagesView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_1
    int-to-float v1, v1

    int-to-float v4, v4

    div-float/2addr v1, v4

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetMediaImagesView;->setHeightWidthRatio(F)V

    goto :goto_0

    :pswitch_2
    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetMediaImagesView;->setHeightWidthRatio(F)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p1, p2}, Lcom/twitter/library/util/ad;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0f04b3    # com.twitter.android.R.string.translate_tweet_hide

    new-array v3, v8, [Ljava/lang/Object;

    aput-object v1, v3, v7

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "$b"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    new-instance v4, Landroid/text/style/ImageSpan;

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f02004b    # com.twitter.android.R.drawable.bing_logo

    invoke-direct {v4, v5, v6}, Landroid/text/style/ImageSpan;-><init>(Landroid/content/Context;I)V

    new-instance v5, Landroid/text/SpannableString;

    invoke-direct {v5, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    add-int/lit8 v2, v3, 0x2

    invoke-virtual {v5, v4, v3, v2, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    iput-object v5, p0, Lcom/twitter/android/widget/TweetDetailView;->G:Ljava/lang/CharSequence;

    const v2, 0x7f0f04b5    # com.twitter.android.R.string.translate_tweet_show

    new-array v3, v8, [Ljava/lang/Object;

    aput-object v1, v3, v7

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->F:Ljava/lang/CharSequence;

    return-void
.end method

.method private a(II)Z
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    if-ge p1, v0, :cond_0

    int-to-float v0, p2

    const/high16 v1, 0x3f800000    # 1.0f

    int-to-float v2, p1

    mul-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/twitter/library/provider/Tweet;)Z
    .locals 1

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->Y()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->X()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->W()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->Z()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/widget/TweetDetailView;)Lcom/twitter/library/api/TweetEntities;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->U:Lcom/twitter/library/api/TweetEntities;

    return-object v0
.end method

.method private b(Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030020    # com.twitter.android.R.layout.card_loading_view

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Landroid/view/ViewGroup;

    const v1, 0x7f09005b    # com.twitter.android.R.id.tweet_media_preview

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setId(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/view/ViewGroup;)V

    return-void
.end method

.method private b(Landroid/content/res/Resources;Lcom/twitter/library/provider/Tweet;IIZ)V
    .locals 5

    const/4 v2, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->b:Lcom/twitter/internal/android/widget/TypefacesTextView;

    if-eqz p5, :cond_0

    invoke-virtual {v0, v4, v4, p3, v4}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    :goto_0
    iget-boolean v1, p2, Lcom/twitter/library/provider/Tweet;->r:Z

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p2, Lcom/twitter/library/provider/Tweet;->g:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v1, p4, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setVisibility(I)V

    return-void

    :cond_0
    invoke-virtual {v0, p3, v4, v4, v4}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p2, Lcom/twitter/library/provider/Tweet;->f:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v1, p4, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private b(Lcom/twitter/library/provider/Tweet;)V
    .locals 10

    const/16 v9, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p1, Lcom/twitter/library/provider/Tweet;->E:Lcom/twitter/library/api/geo/TwitterPlace;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const v1, 0x7f0902a4    # com.twitter.android.R.id.geo_rich_view

    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iget v6, v0, Lcom/twitter/library/api/geo/TwitterPlace;->placeType:I

    iget-object v7, v0, Lcom/twitter/library/api/geo/TwitterPlace;->placeInfo:Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;

    if-ne v6, v4, :cond_2

    iget-object v1, v7, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->name:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v0, v7, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->name:Ljava/lang/String;

    move-object v2, v0

    :goto_1
    iget-boolean v0, p1, Lcom/twitter/library/provider/Tweet;->z:Z

    if-eqz v0, :cond_1

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->x:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->w:Landroid/widget/ImageView;

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->x:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget-object v0, v0, Lcom/twitter/library/api/geo/TwitterPlace;->fullName:Ljava/lang/String;

    move-object v2, v0

    goto :goto_1

    :cond_3
    const-string/jumbo v0, "android_poi_compose_1922"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v0, "android_poi_compose_1922"

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v8, "geotag"

    aput-object v8, v1, v3

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string/jumbo v0, "android_poi_compose_1922"

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v8, "geotag_no_timeline_text"

    aput-object v8, v1, v3

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_4
    move v0, v4

    :goto_2
    invoke-static {}, Lcom/twitter/library/featureswitch/a;->ae()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->ah()Z

    move-result v1

    if-nez v1, :cond_6

    :cond_5
    if-eqz v0, :cond_8

    :cond_6
    move v0, v4

    :goto_3
    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->x:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->w:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->x:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_7
    move v0, v3

    goto :goto_2

    :cond_8
    move v0, v3

    goto :goto_3

    :cond_9
    const v0, 0x7f0902a6    # com.twitter.android.R.id.geo_location_name

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0902a7    # com.twitter.android.R.id.geo_location_subtitle

    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eq v6, v4, :cond_c

    iget-object v4, v7, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->parent:Lcom/twitter/library/api/geo/TwitterPlace;

    if-eqz v4, :cond_b

    iget-object v4, v7, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->name:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_b

    iget-object v4, v7, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->parent:Lcom/twitter/library/api/geo/TwitterPlace;

    iget-object v4, v4, Lcom/twitter/library/api/geo/TwitterPlace;->placeInfo:Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;

    iget-object v4, v4, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->name:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_b

    iget-object v2, v7, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v7, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->parent:Lcom/twitter/library/api/geo/TwitterPlace;

    iget-object v0, v0, Lcom/twitter/library/api/geo/TwitterPlace;->placeInfo:Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;

    iget-object v0, v0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_a
    :goto_4
    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_b
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    :cond_c
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v7, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->attributes:Ljava/util/HashMap;

    if-eqz v0, :cond_a

    const-string/jumbo v2, "street_address"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4
.end method

.method private b(Lcom/twitter/library/provider/Tweet;Lcom/twitter/android/client/c;)V
    .locals 8

    const/16 v6, 0x8

    const v5, 0x7f090077    # com.twitter.android.R.id.image

    const/4 v7, 0x0

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->Q:Lcom/twitter/library/api/TweetClassicCard;

    if-nez v1, :cond_1

    invoke-static {p1, v7, v7}, Lcom/twitter/library/util/s;->a(Lcom/twitter/library/provider/Tweet;II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, p2, v0}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/android/client/c;Ljava/util/List;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, v1, Lcom/twitter/library/api/TweetClassicCard;->type:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget v0, p2, Lcom/twitter/android/client/c;->d:F

    invoke-virtual {v1, v0}, Lcom/twitter/library/api/TweetClassicCard;->a(F)Lcom/twitter/library/api/aa;

    move-result-object v2

    iget-object v3, v2, Lcom/twitter/library/api/aa;->a:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Landroid/view/ViewGroup;

    if-nez v0, :cond_2

    const v0, 0x7f030021    # com.twitter.android.R.layout.card_photo_preview

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/widget/TweetDetailView;->a(ILcom/twitter/library/api/TweetClassicCard;)Landroid/view/ViewGroup;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/OverlayImageView;

    iget v5, v2, Lcom/twitter/library/api/aa;->b:I

    iget v6, v2, Lcom/twitter/library/api/aa;->c:I

    invoke-direct {p0, v5, v6}, Lcom/twitter/android/widget/TweetDetailView;->a(II)Z

    move-result v5

    if-eqz v5, :cond_4

    iget v5, v2, Lcom/twitter/library/api/aa;->b:I

    iget v2, v2, Lcom/twitter/library/api/aa;->c:I

    invoke-direct {p0, v5, v2}, Lcom/twitter/android/widget/TweetDetailView;->b(II)[I

    move-result-object v2

    aget v5, v2, v7

    const/4 v6, 0x1

    aget v2, v2, v6

    invoke-virtual {v0, v5, v2}, Lcom/twitter/internal/android/widget/OverlayImageView;->a(II)V

    sget-object v2, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/OverlayImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    :goto_1
    invoke-direct {p0, v1}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/library/api/TweetClassicCard;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/OverlayImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    invoke-direct {p0, v4}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/view/ViewGroup;)V

    iput-object v4, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Landroid/view/ViewGroup;

    new-instance v0, Lcom/twitter/library/util/m;

    invoke-direct {v0, v3}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->P:Lcom/twitter/library/util/m;

    :cond_2
    new-instance v0, Lcom/twitter/library/util/m;

    invoke-direct {v0, v3}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/util/m;)Lcom/twitter/library/util/ae;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->setClassicCardPreviewImage(Lcom/twitter/library/util/ae;)V

    :cond_3
    iget-object v0, v1, Lcom/twitter/library/api/TweetClassicCard;->siteUser:Lcom/twitter/library/api/CardUser;

    invoke-direct {p0, p2, v0}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/android/client/c;Lcom/twitter/library/api/CardUser;)V

    goto :goto_0

    :cond_4
    iget v5, v2, Lcom/twitter/library/api/aa;->b:I

    iget v2, v2, Lcom/twitter/library/api/aa;->c:I

    invoke-virtual {v0, v5, v2}, Lcom/twitter/internal/android/widget/OverlayImageView;->a(II)V

    goto :goto_1

    :pswitch_1
    iget v0, p2, Lcom/twitter/android/client/c;->d:F

    invoke-virtual {v1, v0}, Lcom/twitter/library/api/TweetClassicCard;->a(F)Lcom/twitter/library/api/aa;

    move-result-object v2

    iget-object v3, v2, Lcom/twitter/library/api/aa;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Landroid/view/ViewGroup;

    if-nez v0, :cond_5

    const v0, 0x7f0300e9    # com.twitter.android.R.layout.player_preview

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/widget/TweetDetailView;->a(ILcom/twitter/library/api/TweetClassicCard;)Landroid/view/ViewGroup;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/OverlayImageView;

    iget v5, v2, Lcom/twitter/library/api/aa;->b:I

    iget v2, v2, Lcom/twitter/library/api/aa;->c:I

    invoke-virtual {v0, v5, v2}, Lcom/twitter/internal/android/widget/OverlayImageView;->a(II)V

    invoke-direct {p0, v1}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/library/api/TweetClassicCard;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/OverlayImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    invoke-direct {p0, v4}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/view/ViewGroup;)V

    iput-object v4, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Landroid/view/ViewGroup;

    new-instance v2, Lcom/twitter/library/util/m;

    invoke-direct {v2, v3}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/twitter/android/widget/TweetDetailView;->P:Lcom/twitter/library/util/m;

    :goto_2
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/OverlayImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/OverlayImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const v2, 0x7f0b0069    # com.twitter.android.R.color.placeholder_bg

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/OverlayImageView;->setBackgroundResource(I)V

    :goto_3
    iget-object v0, v1, Lcom/twitter/library/api/TweetClassicCard;->siteUser:Lcom/twitter/library/api/CardUser;

    invoke-direct {p0, p2, v0}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/android/client/c;Lcom/twitter/library/api/CardUser;)V

    goto/16 :goto_0

    :cond_5
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/OverlayImageView;

    goto :goto_2

    :cond_6
    new-instance v0, Lcom/twitter/library/util/m;

    invoke-direct {v0, v3}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/util/m;)Lcom/twitter/library/util/ae;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->setClassicCardPreviewImage(Lcom/twitter/library/util/ae;)V

    goto :goto_3

    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Landroid/view/ViewGroup;

    iget v2, p2, Lcom/twitter/android/client/c;->d:F

    invoke-virtual {v1, v2}, Lcom/twitter/library/api/TweetClassicCard;->a(F)Lcom/twitter/library/api/aa;

    move-result-object v2

    iget-object v2, v2, Lcom/twitter/library/api/aa;->a:Ljava/lang/String;

    if-nez v0, :cond_a

    const v0, 0x7f03014d    # com.twitter.android.R.layout.summary_preview

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/widget/TweetDetailView;->a(ILcom/twitter/library/api/TweetClassicCard;)Landroid/view/ViewGroup;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_4
    iget-object v4, v1, Lcom/twitter/library/api/TweetClassicCard;->url:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    invoke-direct {p0, v1}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/library/api/TweetClassicCard;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_7
    invoke-direct {p0, v3}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/view/ViewGroup;)V

    iput-object v3, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Landroid/view/ViewGroup;

    new-instance v3, Lcom/twitter/library/util/m;

    invoke-direct {v3, v2}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/twitter/android/widget/TweetDetailView;->P:Lcom/twitter/library/util/m;

    :goto_5
    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->P:Lcom/twitter/library/util/m;

    invoke-virtual {p2, v0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/util/m;)Lcom/twitter/library/util/ae;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->setClassicCardPreviewImage(Lcom/twitter/library/util/ae;)V

    :cond_8
    iget-object v0, v1, Lcom/twitter/library/api/TweetClassicCard;->siteUser:Lcom/twitter/library/api/CardUser;

    invoke-direct {p0, p2, v0}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/android/client/c;Lcom/twitter/library/api/CardUser;)V

    goto/16 :goto_0

    :cond_9
    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    goto :goto_4

    :cond_a
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    goto :goto_5

    :pswitch_3
    iget v0, p2, Lcom/twitter/android/client/c;->d:F

    invoke-virtual {v1, v0}, Lcom/twitter/library/api/TweetClassicCard;->a(F)Lcom/twitter/library/api/aa;

    move-result-object v0

    iget-object v2, v0, Lcom/twitter/library/api/aa;->a:Ljava/lang/String;

    iget-object v3, v1, Lcom/twitter/library/api/TweetClassicCard;->promotion:Lcom/twitter/library/api/Promotion;

    if-eqz v3, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Landroid/view/ViewGroup;

    if-nez v0, :cond_b

    const v0, 0x7f0300f2    # com.twitter.android.R.layout.promotion_preview

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/widget/TweetDetailView;->a(ILcom/twitter/library/api/TweetClassicCard;)Landroid/view/ViewGroup;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/OverlayImageView;

    invoke-direct {p0, v1}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/library/api/TweetClassicCard;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/OverlayImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f090223    # com.twitter.android.R.id.cta

    invoke-virtual {v4, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iget-object v5, v3, Lcom/twitter/library/api/Promotion;->targetCta:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_c

    iget-object v5, v3, Lcom/twitter/library/api/Promotion;->targetCta:Ljava/lang/String;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setVisibility(I)V

    :goto_6
    new-instance v5, Lcom/twitter/android/widget/de;

    invoke-direct {v5, p0, v3}, Lcom/twitter/android/widget/de;-><init>(Lcom/twitter/android/widget/TweetDetailView;Lcom/twitter/library/api/Promotion;)V

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    invoke-direct {p0, v4}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/view/ViewGroup;)V

    iput-object v4, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Landroid/view/ViewGroup;

    new-instance v0, Lcom/twitter/library/util/m;

    invoke-direct {v0, v2}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->P:Lcom/twitter/library/util/m;

    :cond_b
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->P:Lcom/twitter/library/util/m;

    invoke-virtual {p2, v0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/util/m;)Lcom/twitter/library/util/ae;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->setClassicCardPreviewImage(Lcom/twitter/library/util/ae;)V

    goto/16 :goto_0

    :cond_c
    invoke-virtual {v1, v6}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private b(II)[I
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v2, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    const v3, 0x7f0c0080    # com.twitter.android.R.dimen.list_row_padding

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, v1, Landroid/util/DisplayMetrics;->xdpi:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x43200000    # 160.0f

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, v2, v0

    mul-int v1, p2, v0

    div-int/2addr v1, p1

    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    aput v0, v2, v3

    const/4 v0, 0x1

    aput v1, v2, v0

    return-object v2
.end method

.method private getMediaClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/twitter/android/widget/dg;

    invoke-direct {v0, p0}, Lcom/twitter/android/widget/dg;-><init>(Lcom/twitter/android/widget/TweetDetailView;)V

    invoke-static {v0}, Ljy;->a(Lcom/twitter/library/view/h;)Landroid/view/View$OnClickListener;

    move-result-object v0

    return-object v0
.end method

.method private i()V
    .locals 9

    const/4 v8, 0x3

    const/16 v7, 0x8

    const/4 v6, 0x1

    const v5, 0x7f090097    # com.twitter.android.R.id.name

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->s:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->z:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->r:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/twitter/android/widget/TweetDetailView;->S:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v2}, Lcom/twitter/library/widget/ActionButton;->getVisibility()I

    move-result v2

    if-ne v2, v7, :cond_1

    const v2, 0x7f0900d2    # com.twitter.android.R.id.screen_name

    invoke-virtual {v0, v6, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0052    # com.twitter.android.R.dimen.convo_tweet_title_padding_left

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    invoke-virtual {v1, v8, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    invoke-virtual {v1, v6, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    const/4 v0, 0x6

    invoke-virtual {v1, v0, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    invoke-virtual {v1, v7, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0, v6, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0051    # com.twitter.android.R.dimen.convo_tweet_title_icon_padding_left

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    invoke-virtual {v1, v8, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    invoke-virtual {v1, v6, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    const/4 v0, 0x6

    invoke-virtual {v1, v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    invoke-virtual {v1, v7, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    iput v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    goto :goto_0
.end method

.method private setClassicCardPhotoPreviewImage(Lcom/twitter/library/util/ae;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/OverlayImageView;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/twitter/library/util/ae;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0202b7    # com.twitter.android.R.drawable.image_overlay

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/OverlayImageView;->setOverlayDrawable(I)V

    iget-object v1, p1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/OverlayImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/OverlayImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/OverlayImageView;->setOverlayDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/OverlayImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const v1, 0x7f020046    # com.twitter.android.R.drawable.bg_tweet_placeholder_photo_error

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/OverlayImageView;->setBackgroundResource(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/OverlayImageView;->setOverlayDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/OverlayImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const v1, 0x7f0b0069    # com.twitter.android.R.color.placeholder_bg

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/OverlayImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method private setClassicCardPlayerPreviewImage(Lcom/twitter/library/util/ae;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/OverlayImageView;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/twitter/library/util/ae;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0202ce    # com.twitter.android.R.drawable.player_overlay

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/OverlayImageView;->setOverlayDrawable(I)V

    iget-object v1, p1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/OverlayImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/OverlayImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/OverlayImageView;->setOverlayDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/OverlayImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const v1, 0x7f020048    # com.twitter.android.R.drawable.bg_tweet_placeholder_player_error

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/OverlayImageView;->setBackgroundResource(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/OverlayImageView;->setOverlayDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/OverlayImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private setClassicCardPreviewImage(Lcom/twitter/library/util/ae;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/widget/TweetDetailView;->O:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/widget/TweetDetailView;->O:Z

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->Q:Lcom/twitter/library/api/TweetClassicCard;

    iget v0, v0, Lcom/twitter/library/api/TweetClassicCard;->type:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-direct {p0, p1}, Lcom/twitter/android/widget/TweetDetailView;->setClassicCardPhotoPreviewImage(Lcom/twitter/library/util/ae;)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/twitter/android/widget/TweetDetailView;->setClassicCardPlayerPreviewImage(Lcom/twitter/library/util/ae;)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p1}, Lcom/twitter/android/widget/TweetDetailView;->setClassicCardSummaryPreviewImage(Lcom/twitter/library/util/ae;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private setClassicCardSummaryPreviewImage(Lcom/twitter/library/util/ae;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/util/ae;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    const/4 v5, -0x2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->m:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->S()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v0, v1, v1}, Lcom/twitter/library/util/s;->a(Lcom/twitter/library/provider/Tweet;II)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/u;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/widget/TweetDetailView;->y:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0201f5    # com.twitter.android.R.drawable.ic_photo_tag_tweet_detail

    invoke-static {v3, v0, v4}, Lcom/twitter/library/util/u;->a(Landroid/content/Context;Ljava/util/List;I)Ljava/lang/CharSequence;

    move-result-object v0

    sget-object v3, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v2, v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f0c00cb    # com.twitter.android.R.dimen.tweet_detail_media_tag_margin_top

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    :goto_0
    const v4, 0x7f0c00ca    # com.twitter.android.R.dimen.tweet_detail_media_tag_margin_bottom

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-virtual {v3, v1, v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->y:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->y:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->y:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->K:Lcom/twitter/library/api/TwitterStatus$Translation;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "translated_tweet"

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->K:Lcom/twitter/library/api/TwitterStatus$Translation;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v0, "show_translation"

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->J:Lcom/twitter/internal/android/widget/TypefacesTextView;

    invoke-virtual {v1}, Lcom/twitter/internal/android/widget/TypefacesTextView;->getVisibility()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 5

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300b8    # com.twitter.android.R.layout.media_data_charges_warning

    const/4 v3, 0x0

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {v2}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/client/c;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const v1, 0x7f0901ce    # com.twitter.android.R.id.warning_message

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-direct {p0, v2, p1, v0}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/content/Context;Landroid/view/View$OnClickListener;Landroid/view/ViewGroup;)V

    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;Z)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->S:Lcom/twitter/library/widget/ActionButton;

    const v1, 0x7f020086    # com.twitter.android.R.drawable.btn_follow_action_bg

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ActionButton;->setBackgroundResource(I)V

    const v1, 0x7f020084    # com.twitter.android.R.drawable.btn_follow

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ActionButton;->a(I)V

    invoke-virtual {v0, p2}, Lcom/twitter/library/widget/ActionButton;->setChecked(Z)V

    invoke-virtual {v0, p1}, Lcom/twitter/library/widget/ActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/twitter/android/widget/TweetDetailView;->i()V

    return-void
.end method

.method public a(Lcom/twitter/android/client/c;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->m:Lcom/twitter/library/provider/Tweet;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p1, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    iget v2, v0, Lcom/twitter/library/provider/Tweet;->v:I

    iget-object v0, v0, Lcom/twitter/library/provider/Tweet;->k:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/twitter/library/widget/ap;->a(ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->a:Landroid/widget/ImageView;

    const v1, 0x7f02003a    # com.twitter.android.R.drawable.bg_no_profile_photo_md

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/api/ActivitySummary;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, -0x1

    const/4 v3, -0x2

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->N:Lcom/twitter/android/widget/do;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->N:Lcom/twitter/android/widget/do;

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/twitter/android/widget/do;->a(Landroid/content/res/Resources;Lcom/twitter/library/api/ActivitySummary;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->N:Lcom/twitter/android/widget/do;

    iget-object v0, v0, Lcom/twitter/android/widget/do;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v4, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/twitter/android/widget/TweetDetailView;->C:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0, v5, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->C:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->c:Landroid/view/ViewGroup;

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/widget/TweetDetailView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :goto_0
    const/4 v1, 0x3

    const v2, 0x7f0902af    # com.twitter.android.R.id.action_bar_placeholder

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    return-void

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/api/ActivitySummary;Lcom/twitter/android/widget/di;)V
    .locals 4

    iget-object v0, p1, Lcom/twitter/library/api/ActivitySummary;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/library/api/ActivitySummary;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/twitter/library/api/ActivitySummary;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/library/api/ActivitySummary;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->N:Lcom/twitter/android/widget/do;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/twitter/android/widget/TweetDetailView;->k:Lcom/twitter/library/api/ActivitySummary;

    iput-object p2, p0, Lcom/twitter/android/widget/TweetDetailView;->l:Lcom/twitter/android/widget/di;

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->N:Lcom/twitter/android/widget/do;

    if-nez v0, :cond_1

    new-instance v0, Lcom/twitter/android/widget/do;

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030165    # com.twitter.android.R.layout.tweet_stats

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/widget/dh;

    invoke-direct {v2, p0}, Lcom/twitter/android/widget/dh;-><init>(Lcom/twitter/android/widget/TweetDetailView;)V

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/widget/do;-><init>(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->N:Lcom/twitter/android/widget/do;

    :cond_1
    invoke-virtual {p0, p1}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/library/api/ActivitySummary;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/android/client/c;)V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->b(Landroid/content/Context;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->l()Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->Q:Lcom/twitter/library/api/TweetClassicCard;

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/widget/TweetDetailView;->b(Lcom/twitter/library/provider/Tweet;Lcom/twitter/android/client/c;)V

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->e()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/android/client/c;Lcom/twitter/library/view/c;Lcom/twitter/library/api/TweetEntities;Ljava/lang/String;Lcom/twitter/library/client/aa;IILcom/twitter/android/tq;)V
    .locals 20

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/widget/TweetDetailView;->m:Lcom/twitter/library/provider/Tweet;

    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/widget/TweetDetailView;->U:Lcom/twitter/library/api/TweetEntities;

    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/widget/TweetDetailView;->n:Lcom/twitter/library/view/c;

    move-object/from16 v0, p9

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/widget/TweetDetailView;->o:Lcom/twitter/android/tq;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/widget/TweetDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    invoke-direct/range {p0 .. p1}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/library/provider/Tweet;)Z

    move-result v8

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->T()Lcom/twitter/library/api/MediaEntity;

    move-result-object v4

    if-nez v4, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->Q()Z

    move-result v4

    if-nez v4, :cond_0

    if-eqz v8, :cond_7

    :cond_0
    const/4 v5, 0x0

    invoke-static {}, Lgv;->a()Z

    move-result v6

    const/4 v7, 0x0

    const/4 v9, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Lcom/twitter/library/provider/Tweet;->a(ZZZZZ)Ljava/lang/String;

    move-result-object v4

    :goto_0
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_8

    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetDetailView;->f:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetDetailView;->r:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v6, 0x40

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/twitter/library/provider/Tweet;->p:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetDetailView;->S:Lcom/twitter/library/widget/ActionButton;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/twitter/library/provider/Tweet;->p:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/twitter/library/widget/ActionButton;->setUsername(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetDetailView;->S:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v4}, Lcom/twitter/library/widget/ActionButton;->a()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetDetailView;->q:Landroid/widget/TextView;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/twitter/library/provider/Tweet;->g:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-boolean v4, Lcom/twitter/library/util/Util;->c:Z

    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetDetailView;->f:Landroid/widget/TextView;

    if-eqz v4, :cond_2

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->C()Z

    move-result v4

    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetDetailView;->f:Landroid/widget/TextView;

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setGravity(I)V

    :cond_2
    invoke-virtual/range {p6 .. p6}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v17

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetDetailView;->b:Lcom/twitter/internal/android/widget/TypefacesTextView;

    move-object/from16 v0, p1

    move-wide/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/provider/Tweet;->a(J)Z

    move-result v19

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->D()Z

    move-result v5

    if-eqz v5, :cond_3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, v17

    invoke-direct {v0, v15, v1, v2, v3}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/content/res/Resources;Lcom/twitter/library/provider/Tweet;J)V

    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->F()Z

    move-result v5

    if-eqz v5, :cond_9

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setVisibility(I)V

    move-object/from16 v0, p1

    iget-boolean v4, v0, Lcom/twitter/library/provider/Tweet;->r:Z

    if-eqz v4, :cond_4

    if-nez v19, :cond_4

    const/16 v4, 0xd

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->k()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p2

    iget-boolean v6, v0, Lcom/twitter/android/client/c;->f:Z

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v4, v5, v6}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/content/res/Resources;ILjava/lang/String;Z)V

    :cond_4
    :goto_2
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/android/client/c;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetDetailView;->a:Landroid/widget/ImageView;

    move-object/from16 v0, p1

    iget-wide v5, v0, Lcom/twitter/library/provider/Tweet;->n:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    move-object/from16 v0, p1

    iget-boolean v4, v0, Lcom/twitter/library/provider/Tweet;->D:Z

    if-eqz v4, :cond_11

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetDetailView;->z:Landroid/widget/ImageView;

    const v5, 0x7f02028c    # com.twitter.android.R.drawable.ic_verified

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetDetailView;->z:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_3
    sget-object v4, Lcom/twitter/android/widget/TweetDetailView;->p:Ljava/text/SimpleDateFormat;

    move-object/from16 v0, p1

    iget-wide v5, v0, Lcom/twitter/library/provider/Tweet;->h:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {p0 .. p1}, Lcom/twitter/android/widget/TweetDetailView;->b(Lcom/twitter/library/provider/Tweet;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/widget/TweetDetailView;->v:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p1

    iget v4, v0, Lcom/twitter/library/provider/Tweet;->ah:I

    move-object/from16 v0, p1

    iget-wide v5, v0, Lcom/twitter/library/provider/Tweet;->q:J

    cmp-long v5, v5, v17

    if-nez v5, :cond_5

    if-nez v19, :cond_5

    if-lez v4, :cond_5

    move-object/from16 v0, p1

    iget v5, v0, Lcom/twitter/library/provider/Tweet;->ag:I

    if-lt v4, v5, :cond_5

    move-object/from16 v0, p1

    iget v5, v0, Lcom/twitter/library/provider/Tweet;->V:I

    if-lt v4, v5, :cond_5

    move-object/from16 v0, p1

    iget v5, v0, Lcom/twitter/library/provider/Tweet;->ae:I

    if-lt v4, v5, :cond_5

    invoke-static {}, Lgw;->b()I

    move-result v5

    if-lt v4, v5, :cond_5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/widget/TweetDetailView;->M:Lcom/twitter/android/widget/TweetStatView;

    const v6, 0x7f0e0021    # com.twitter.android.R.plurals.stat_label_views

    invoke-virtual {v15, v6, v4}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/twitter/android/widget/TweetStatView;->setName(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/widget/TweetDetailView;->M:Lcom/twitter/android/widget/TweetStatView;

    invoke-static {v15, v4}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/twitter/android/widget/TweetStatView;->setValue(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetDetailView;->L:Landroid/view/ViewGroup;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetDetailView;->C:Landroid/view/ViewGroup;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/widget/TweetDetailView;->e()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetDetailView;->K:Lcom/twitter/library/api/TwitterStatus$Translation;

    if-nez v4, :cond_14

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetDetailView;->m:Lcom/twitter/library/provider/Tweet;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/widget/TweetDetailView;->getContext()Landroid/content/Context;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v4, v5, v0}, Lcom/twitter/library/provider/Tweet;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/widget/TweetDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget-object v4, v4, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    if-eqz v4, :cond_13

    :goto_4
    invoke-static {v4}, Lcom/twitter/library/util/Util;->b(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/widget/TweetDetailView;->m:Lcom/twitter/library/provider/Tweet;

    iget-object v5, v5, Lcom/twitter/library/provider/Tweet;->af:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v4}, Lcom/twitter/android/widget/TweetDetailView;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetDetailView;->I:Lcom/twitter/internal/android/widget/TypefacesTextView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/widget/TweetDetailView;->F:Ljava/lang/CharSequence;

    invoke-virtual {v4, v5}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetDetailView;->H:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_6
    :goto_5
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/widget/TweetDetailView;->a()V

    return-void

    :cond_7
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/twitter/library/provider/Tweet;->d:Ljava/lang/String;

    goto/16 :goto_0

    :cond_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/widget/TweetDetailView;->f:Landroid/widget/TextView;

    const v6, 0x7f0b006b    # com.twitter.android.R.color.prefix

    invoke-virtual {v15, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    const v7, 0x7f0b0061    # com.twitter.android.R.color.link_selected

    invoke-virtual {v15, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    move-object/from16 v0, p4

    move-object/from16 v1, p3

    invoke-static {v4, v0, v1, v6, v7}, Lcom/twitter/library/view/d;->a(Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/view/c;II)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_9
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->E()Z

    move-result v5

    if-eqz v5, :cond_a

    if-nez v19, :cond_a

    const v7, 0x7f020124    # com.twitter.android.R.drawable.ic_badge_gov_default

    const v8, 0x7f0f0333    # com.twitter.android.R.string.promoted_by

    move-object/from16 v0, p2

    iget-boolean v9, v0, Lcom/twitter/android/client/c;->f:Z

    move-object/from16 v4, p0

    move-object v5, v15

    move-object/from16 v6, p1

    invoke-direct/range {v4 .. v9}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/content/res/Resources;Lcom/twitter/library/provider/Tweet;IIZ)V

    goto/16 :goto_2

    :cond_a
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->v()Z

    move-result v5

    if-eqz v5, :cond_b

    if-nez v19, :cond_b

    const v7, 0x7f020125    # com.twitter.android.R.drawable.ic_badge_promoted_default

    const v8, 0x7f0f0333    # com.twitter.android.R.string.promoted_by

    move-object/from16 v0, p2

    iget-boolean v9, v0, Lcom/twitter/android/client/c;->f:Z

    move-object/from16 v4, p0

    move-object v5, v15

    move-object/from16 v6, p1

    invoke-direct/range {v4 .. v9}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/content/res/Resources;Lcom/twitter/library/provider/Tweet;IIZ)V

    goto/16 :goto_2

    :cond_b
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->I()Z

    move-result v5

    if-eqz v5, :cond_c

    if-nez v19, :cond_c

    const v7, 0x7f020123    # com.twitter.android.R.drawable.ic_badge_alert_default

    const v8, 0x7f0f0205    # com.twitter.android.R.string.lifeline_alert

    move-object/from16 v0, p2

    iget-boolean v9, v0, Lcom/twitter/android/client/c;->f:Z

    move-object/from16 v4, p0

    move-object v5, v15

    move-object/from16 v6, p1

    invoke-direct/range {v4 .. v9}, Lcom/twitter/android/widget/TweetDetailView;->b(Landroid/content/res/Resources;Lcom/twitter/library/provider/Tweet;IIZ)V

    goto/16 :goto_2

    :cond_c
    move-object/from16 v0, p1

    iget-boolean v5, v0, Lcom/twitter/library/provider/Tweet;->r:Z

    if-eqz v5, :cond_d

    if-nez v19, :cond_d

    const/16 v4, 0xd

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->k()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p2

    iget-boolean v6, v0, Lcom/twitter/android/client/c;->f:Z

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v4, v5, v6}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/content/res/Resources;ILjava/lang/String;Z)V

    goto/16 :goto_2

    :cond_d
    move-object/from16 v0, p1

    iget v5, v0, Lcom/twitter/library/provider/Tweet;->P:I

    if-lez v5, :cond_e

    move-object/from16 v0, p1

    iget v6, v0, Lcom/twitter/library/provider/Tweet;->P:I

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/twitter/library/provider/Tweet;->Q:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/twitter/library/provider/Tweet;->aa:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v9, v0, Lcom/twitter/library/provider/Tweet;->R:I

    move-object/from16 v0, p1

    iget v10, v0, Lcom/twitter/library/provider/Tweet;->S:I

    move-object/from16 v0, p1

    iget v11, v0, Lcom/twitter/library/provider/Tweet;->ab:I

    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/twitter/library/provider/Tweet;->h:J

    move-object/from16 v0, p2

    iget-boolean v14, v0, Lcom/twitter/android/client/c;->f:Z

    move-object/from16 v4, p0

    move-object v5, v15

    invoke-direct/range {v4 .. v14}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;IIIJZ)V

    goto/16 :goto_2

    :cond_e
    if-lez p7, :cond_f

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const-wide/16 v12, 0x0

    move-object/from16 v0, p2

    iget-boolean v14, v0, Lcom/twitter/android/client/c;->f:Z

    move-object/from16 v4, p0

    move-object v5, v15

    move/from16 v6, p7

    move/from16 v11, p8

    invoke-direct/range {v4 .. v14}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;IIIJZ)V

    goto/16 :goto_2

    :cond_f
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_10

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetDetailView;->u:Lcom/twitter/library/widget/SocialBylineView;

    move-object/from16 v0, p5

    invoke-virtual {v4, v0}, Lcom/twitter/library/widget/SocialBylineView;->setLabel(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetDetailView;->u:Lcom/twitter/library/widget/SocialBylineView;

    const v5, 0x7f0201a9    # com.twitter.android.R.drawable.ic_follow_text

    invoke-virtual {v4, v5}, Lcom/twitter/library/widget/SocialBylineView;->setIcon(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetDetailView;->u:Lcom/twitter/library/widget/SocialBylineView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/twitter/library/widget/SocialBylineView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetDetailView;->u:Lcom/twitter/library/widget/SocialBylineView;

    move-object/from16 v0, p2

    iget-boolean v5, v0, Lcom/twitter/android/client/c;->f:Z

    invoke-virtual {v4, v5}, Lcom/twitter/library/widget/SocialBylineView;->setRenderRTL(Z)V

    goto/16 :goto_2

    :cond_10
    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetDetailView;->u:Lcom/twitter/library/widget/SocialBylineView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/twitter/library/widget/SocialBylineView;->setVisibility(I)V

    goto/16 :goto_2

    :cond_11
    move-object/from16 v0, p1

    iget-boolean v4, v0, Lcom/twitter/library/provider/Tweet;->m:Z

    if-eqz v4, :cond_12

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetDetailView;->z:Landroid/widget/ImageView;

    const v5, 0x7f0201d0    # com.twitter.android.R.drawable.ic_locked

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetDetailView;->z:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    :cond_12
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetDetailView;->z:Landroid/widget/ImageView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    :cond_13
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    goto/16 :goto_4

    :cond_14
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetDetailView;->K:Lcom/twitter/library/api/TwitterStatus$Translation;

    iget-object v4, v4, Lcom/twitter/library/api/TwitterStatus$Translation;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/widget/TweetDetailView;->K:Lcom/twitter/library/api/TwitterStatus$Translation;

    iget-object v5, v5, Lcom/twitter/library/api/TwitterStatus$Translation;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetDetailView;->K:Lcom/twitter/library/api/TwitterStatus$Translation;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/library/api/TwitterStatus$Translation;)Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetDetailView;->H:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_5
.end method

.method public a(Ljava/lang/Runnable;)V
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->B:Landroid/view/ViewGroup;

    const v2, 0x7f0201f2    # com.twitter.android.R.drawable.ic_photo_action_overlay_favorite_on

    const v3, 0x7f040006    # com.twitter.android.R.anim.double_tap_favorite

    invoke-static {v0, v1, v2, v3, p1}, Ljw;->a(Landroid/content/Context;Landroid/view/ViewGroup;IILjava/lang/Runnable;)V

    return-void
.end method

.method public a(Ljava/util/Map;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->Q:Lcom/twitter/library/api/TweetClassicCard;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->P:Lcom/twitter/library/util/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->P:Lcom/twitter/library/util/m;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ae;

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->setClassicCardPreviewImage(Lcom/twitter/library/util/ae;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/twitter/library/widget/TweetMediaImagesView;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/twitter/library/widget/TweetMediaImagesView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/widget/TweetMediaImagesView;->a(Ljava/util/Map;)Z

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/api/TwitterStatus$Translation;)Z
    .locals 7

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz p1, :cond_0

    iget-object v1, p1, Lcom/twitter/library/api/TwitterStatus$Translation;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/twitter/library/api/TwitterStatus$Translation;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->H:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v1, p1, Lcom/twitter/library/api/TwitterStatus$Translation;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/widget/TweetDetailView;->m:Lcom/twitter/library/provider/Tweet;

    iget-object v2, v2, Lcom/twitter/library/provider/Tweet;->af:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p1, Lcom/twitter/library/api/TwitterStatus$Translation;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/twitter/library/api/TwitterStatus$Translation;->c:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/twitter/android/widget/TweetDetailView;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->J:Lcom/twitter/internal/android/widget/TypefacesTextView;

    iget-object v2, p1, Lcom/twitter/library/api/TwitterStatus$Translation;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/api/TwitterStatus$Translation;->e:Lcom/twitter/library/api/TweetEntities;

    iget-object v4, p0, Lcom/twitter/android/widget/TweetDetailView;->n:Lcom/twitter/library/view/c;

    const v5, 0x7f0b006b    # com.twitter.android.R.color.prefix

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    const v6, 0x7f0b0061    # com.twitter.android.R.color.link_selected

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v2, v3, v4, v5, v0}, Lcom/twitter/library/view/d;->a(Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/view/c;II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    iput-object p1, p0, Lcom/twitter/android/widget/TweetDetailView;->K:Lcom/twitter/library/api/TwitterStatus$Translation;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->c:Landroid/view/ViewGroup;

    const v1, 0x7f09006c    # com.twitter.android.R.id.actionbar

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v1, 0x7f09006d    # com.twitter.android.R.id.reply

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    const v1, 0x7f09006e    # com.twitter.android.R.id.retweet

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    const v1, 0x7f09006f    # com.twitter.android.R.id.favorite

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    const v1, 0x7f090070    # com.twitter.android.R.id.share

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    const v1, 0x7f090071    # com.twitter.android.R.id.delete

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 4

    const-string/jumbo v0, "translated_tweet"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterStatus$Translation;

    const-string/jumbo v1, "show_translation"

    const/16 v2, 0x8

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/twitter/library/api/TwitterStatus$Translation;->b:Ljava/lang/String;

    iget-object v3, v0, Lcom/twitter/library/api/TwitterStatus$Translation;->c:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/twitter/android/widget/TweetDetailView;->a(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->K:Lcom/twitter/library/api/TwitterStatus$Translation;

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->J:Lcom/twitter/internal/android/widget/TypefacesTextView;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setVisibility(I)V

    iget-object v2, p0, Lcom/twitter/android/widget/TweetDetailView;->I:Lcom/twitter/internal/android/widget/TypefacesTextView;

    if-nez v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->G:Ljava/lang/CharSequence;

    :goto_0
    invoke-virtual {v2, v0}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->F:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public b(Landroid/view/View$OnClickListener;)V
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0300ec    # com.twitter.android.R.layout.possibly_sensitive_warning

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v1, p1, v0}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/content/Context;Landroid/view/View$OnClickListener;Landroid/view/ViewGroup;)V

    return-void
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->K:Lcom/twitter/library/api/TwitterStatus$Translation;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->J:Lcom/twitter/internal/android/widget/TypefacesTextView;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/TypefacesTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->I:Lcom/twitter/internal/android/widget/TypefacesTextView;

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->F:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->J:Lcom/twitter/internal/android/widget/TypefacesTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->I:Lcom/twitter/internal/android/widget/TypefacesTextView;

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->G:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->J:Lcom/twitter/internal/android/widget/TypefacesTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public e()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/library/api/ActivitySummary;)V

    return-void
.end method

.method public f()V
    .locals 2

    invoke-static {}, Lkl;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->d:Landroid/view/ViewGroup;

    const v1, 0x7f090053    # com.twitter.android.R.id.reserved_actionbar

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/view/ViewGroup;I)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->e:Landroid/view/ViewGroup;

    const v1, 0x7f09029c    # com.twitter.android.R.id.badge

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/view/ViewGroup;I)V

    :cond_0
    return-void
.end method

.method public g()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->E:Lcom/twitter/library/card/Card;

    invoke-virtual {v0}, Lcom/twitter/library/card/Card;->d()Lcom/twitter/library/card/CardView;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Landroid/view/ViewGroup;

    const v1, 0x7f09005b    # com.twitter.android.R.id.tweet_media_preview

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setId(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->invalidate()V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/view/ViewGroup;)V

    return-void
.end method

.method public getActionButton()Lcom/twitter/library/widget/ActionButton;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->S:Lcom/twitter/library/widget/ActionButton;

    return-object v0
.end method

.method public h()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->B:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->B:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->e()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 5

    const v4, 0x7f030044    # com.twitter.android.R.layout.convo_reply_box_placeholder

    const/4 v3, 0x4

    const/4 v2, 0x0

    const v0, 0x7f09029c    # com.twitter.android.R.id.badge

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    const v1, 0x7f09001f    # com.twitter.android.R.id.icon

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->z:Landroid/widget/ImageView;

    const v1, 0x7f0900d2    # com.twitter.android.R.id.screen_name

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->r:Landroid/widget/TextView;

    const v1, 0x7f090097    # com.twitter.android.R.id.name

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->q:Landroid/widget/TextView;

    const v1, 0x7f09012d    # com.twitter.android.R.id.profile_image

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->a:Landroid/widget/ImageView;

    const v1, 0x7f09029e    # com.twitter.android.R.id.replied_to

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->s:Landroid/widget/TextView;

    const v1, 0x7f09029d    # com.twitter.android.R.id.name_panel

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->t:Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->W:Landroid/view/View;

    const v0, 0x7f090036    # com.twitter.android.R.id.content

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->f:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->R:Lcom/twitter/internal/android/widget/ax;

    iget-object v1, v1, Lcom/twitter/internal/android/widget/ax;->e:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    const v0, 0x7f090059    # com.twitter.android.R.id.social_byline

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/SocialBylineView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->u:Lcom/twitter/library/widget/SocialBylineView;

    const v0, 0x7f0902a8    # com.twitter.android.R.id.byline_timestamp

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->v:Landroid/widget/TextView;

    const v0, 0x7f0902a9    # com.twitter.android.R.id.byline_location_icon

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->w:Landroid/widget/ImageView;

    const v0, 0x7f0902aa    # com.twitter.android.R.id.byline_location_name

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->x:Landroid/widget/TextView;

    const v0, 0x7f09020e    # com.twitter.android.R.id.media_tags

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->y:Landroid/widget/TextView;

    const v0, 0x7f09002c    # com.twitter.android.R.id.action_button

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/ActionButton;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->S:Lcom/twitter/library/widget/ActionButton;

    const v0, 0x7f0902a3    # com.twitter.android.R.id.preview_container

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->B:Landroid/view/ViewGroup;

    const v0, 0x7f0901c9    # com.twitter.android.R.id.stats_container

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->C:Landroid/view/ViewGroup;

    const v0, 0x7f0902ae    # com.twitter.android.R.id.tweet_view_stats

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->L:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->L:Landroid/view/ViewGroup;

    const v1, 0x7f09024f    # com.twitter.android.R.id.view_stat

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/TweetStatView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->M:Lcom/twitter/android/widget/TweetStatView;

    const v0, 0x7f0902a0    # com.twitter.android.R.id.tweet_content

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->D:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->y:Landroid/widget/TextView;

    new-instance v1, Lcom/twitter/android/widget/db;

    invoke-direct {v1, p0}, Lcom/twitter/android/widget/db;-><init>(Lcom/twitter/android/widget/TweetDetailView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09008a    # com.twitter.android.R.id.promoted_tweet

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/TypefacesTextView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->b:Lcom/twitter/internal/android/widget/TypefacesTextView;

    invoke-static {}, Lkl;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f030187    # com.twitter.android.R.layout.white_action_bar

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, v0, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->c:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->c:Landroid/view/ViewGroup;

    const v1, 0x7f090053    # com.twitter.android.R.id.reserved_actionbar

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setId(I)V

    const v0, 0x7f0902af    # com.twitter.android.R.id.action_bar_placeholder

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->Z:Landroid/view/View;

    invoke-static {}, Lkl;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v0, v4, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->d:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v0, v4, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->e:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_0
    const v0, 0x7f0902a2    # com.twitter.android.R.id.photo_layout

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->h:Landroid/view/View;

    const v0, 0x7f09029a    # com.twitter.android.R.id.onboarding_tweet_instructions

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->i:Landroid/view/View;

    const v0, 0x7f09029f    # com.twitter.android.R.id.preview_edit_button

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->j:Landroid/view/View;

    new-instance v0, Lcom/twitter/android/widget/dc;

    invoke-direct {v0, p0}, Lcom/twitter/android/widget/dc;-><init>(Lcom/twitter/android/widget/TweetDetailView;)V

    invoke-static {v0, v2}, Ljy;->a(Lcom/twitter/library/view/h;Z)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/TweetDetailView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->w:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->x:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->f:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/twitter/library/view/l;->a(Landroid/widget/TextView;)V

    const v0, 0x7f0902ac    # com.twitter.android.R.id.tweet_translation_link

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/TypefacesTextView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->I:Lcom/twitter/internal/android/widget/TypefacesTextView;

    const v0, 0x7f0902ad    # com.twitter.android.R.id.tweet_translation_text

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/TypefacesTextView;

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->J:Lcom/twitter/internal/android/widget/TypefacesTextView;

    const v0, 0x7f0902ab    # com.twitter.android.R.id.tweet_translation

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->H:Landroid/view/View;

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->J:Lcom/twitter/internal/android/widget/TypefacesTextView;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->J:Lcom/twitter/internal/android/widget/TypefacesTextView;

    invoke-static {v0}, Lcom/twitter/library/view/l;->a(Landroid/widget/TextView;)V

    return-void

    :cond_1
    const v0, 0x7f030006    # com.twitter.android.R.layout.action_bar

    goto/16 :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    iget-boolean v0, p0, Lcom/twitter/android/widget/TweetDetailView;->T:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/widget/TweetDetailView;->T:Z

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->A:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/twitter/library/widget/TweetMediaImagesView;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/twitter/library/widget/TweetMediaImagesView;

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetDetailView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetMediaImagesView;->a(Lcom/twitter/library/widget/ap;)Z

    :cond_0
    invoke-static {}, Lkl;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/android/widget/TweetDetailView;->V:I

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getBottom()I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/TweetDetailView;->V:I

    if-eqz v0, :cond_1

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetDetailView;->setActionBarBottom(I)V

    :cond_1
    return-void
.end method

.method public setActionBarBottom(I)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->W:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    add-int/2addr v1, v0

    iget-object v2, p0, Lcom/twitter/android/widget/TweetDetailView;->Z:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    add-int/2addr v0, v2

    invoke-static {p1, v1, v0}, Lcom/twitter/library/util/Util;->a(III)I

    move-result v0

    iget v1, p0, Lcom/twitter/android/widget/TweetDetailView;->V:I

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/widget/TweetDetailView;->c:Landroid/view/ViewGroup;

    iget v2, p0, Lcom/twitter/android/widget/TweetDetailView;->V:I

    sub-int v2, v0, v2

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->offsetTopAndBottom(I)V

    iput v0, p0, Lcom/twitter/android/widget/TweetDetailView;->V:I

    :cond_0
    return-void
.end method

.method public setCard(Lcom/twitter/library/card/Card;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/TweetDetailView;->E:Lcom/twitter/library/card/Card;

    return-void
.end method

.method public setTranslationButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/TweetDetailView;->I:Lcom/twitter/internal/android/widget/TypefacesTextView;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
