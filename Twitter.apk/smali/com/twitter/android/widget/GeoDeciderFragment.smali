.class public Lcom/twitter/android/widget/GeoDeciderFragment;
.super Landroid/support/v4/app/Fragment;
.source "Twttr"


# static fields
.field private static final d:[I

.field private static final e:[I


# instance fields
.field a:Landroid/widget/TextView;

.field b:Landroid/widget/TextView;

.field c:Lcom/twitter/library/platform/LocationProducer;

.field private f:Landroid/widget/CompoundButton;

.field private g:Landroid/widget/CompoundButton;

.field private h:Landroid/widget/SeekBar;

.field private i:Landroid/widget/SeekBar;

.field private j:Lcom/twitter/android/widget/ag;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/twitter/android/widget/GeoDeciderFragment;->d:[I

    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/twitter/android/widget/GeoDeciderFragment;->e:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x5
        0xa
        0xf
        0x1e
        0x2d
        0x3c
        0xb4
        0x12c
        0x258
        0x384
        0x708
    .end array-data

    :array_1
    .array-data 4
        0x1e
        0x3c
        0x78
        0xb4
        0x12c
        0x258
        0x384
        0x4b0
        0x708
        0xe10
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/widget/GeoDeciderFragment;)Landroid/widget/CompoundButton;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/GeoDeciderFragment;->f:Landroid/widget/CompoundButton;

    return-object v0
.end method

.method private a(I)Ljava/lang/String;
    .locals 2

    const/16 v1, 0x3c

    if-ge p1, v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "s"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    div-int/lit8 v0, p1, 0x3c

    if-ge v0, v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "m"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    div-int/lit8 v0, v0, 0x3c

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "h"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/widget/SeekBar;[II)V
    .locals 3

    if-eqz p1, :cond_0

    array-length v0, p2

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v1, 0x7fffffff

    const/4 v0, 0x0

    :goto_1
    array-length v2, p2

    if-ge v0, v2, :cond_3

    aget v2, p2, v0

    sub-int/2addr v2, p3

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    if-le v2, v1, :cond_2

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_1

    :cond_3
    array-length v0, p2

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0
.end method

.method private a(Landroid/widget/TextView;Ljava/lang/String;I)V
    .locals 2

    if-eqz p1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, p3}, Lcom/twitter/android/widget/GeoDeciderFragment;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/widget/GeoDeciderFragment;Landroid/widget/TextView;Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/widget/GeoDeciderFragment;->a(Landroid/widget/TextView;Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic a()[I
    .locals 1

    sget-object v0, Lcom/twitter/android/widget/GeoDeciderFragment;->d:[I

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/widget/GeoDeciderFragment;)Lcom/twitter/android/widget/ag;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/GeoDeciderFragment;->j:Lcom/twitter/android/widget/ag;

    return-object v0
.end method

.method static synthetic b()[I
    .locals 1

    sget-object v0, Lcom/twitter/android/widget/GeoDeciderFragment;->e:[I

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/android/widget/ag;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/GeoDeciderFragment;->j:Lcom/twitter/android/widget/ag;

    return-void
.end method

.method public a(Lcom/twitter/library/platform/LocationProducer;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/twitter/android/widget/GeoDeciderFragment;->c:Lcom/twitter/library/platform/LocationProducer;

    iget-object v0, p0, Lcom/twitter/android/widget/GeoDeciderFragment;->f:Landroid/widget/CompoundButton;

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/twitter/android/widget/GeoDeciderFragment;->f:Landroid/widget/CompoundButton;

    sget-object v0, Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;->a:Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;

    invoke-virtual {p1, v0}, Lcom/twitter/library/platform/LocationProducer;->a(Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;)I

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/GeoDeciderFragment;->g:Landroid/widget/CompoundButton;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/GeoDeciderFragment;->g:Landroid/widget/CompoundButton;

    sget-object v3, Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;->b:Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;

    invoke-virtual {p1, v3}, Lcom/twitter/library/platform/LocationProducer;->a(Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;)I

    move-result v3

    if-nez v3, :cond_5

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/GeoDeciderFragment;->h:Landroid/widget/SeekBar;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;->c:Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;

    invoke-virtual {p1, v0}, Lcom/twitter/library/platform/LocationProducer;->a(Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;)I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/widget/GeoDeciderFragment;->a:Landroid/widget/TextView;

    const-string/jumbo v2, "Update Duration"

    invoke-direct {p0, v1, v2, v0}, Lcom/twitter/android/widget/GeoDeciderFragment;->a(Landroid/widget/TextView;Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/twitter/android/widget/GeoDeciderFragment;->h:Landroid/widget/SeekBar;

    sget-object v2, Lcom/twitter/android/widget/GeoDeciderFragment;->d:[I

    invoke-direct {p0, v1, v2, v0}, Lcom/twitter/android/widget/GeoDeciderFragment;->a(Landroid/widget/SeekBar;[II)V

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/widget/GeoDeciderFragment;->i:Landroid/widget/SeekBar;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;->d:Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;

    invoke-virtual {p1, v0}, Lcom/twitter/library/platform/LocationProducer;->a(Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;)I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/widget/GeoDeciderFragment;->b:Landroid/widget/TextView;

    const-string/jumbo v2, "Update Interval"

    invoke-direct {p0, v1, v2, v0}, Lcom/twitter/android/widget/GeoDeciderFragment;->a(Landroid/widget/TextView;Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/twitter/android/widget/GeoDeciderFragment;->i:Landroid/widget/SeekBar;

    sget-object v2, Lcom/twitter/android/widget/GeoDeciderFragment;->e:[I

    invoke-direct {p0, v1, v2, v0}, Lcom/twitter/android/widget/GeoDeciderFragment;->a(Landroid/widget/SeekBar;[II)V

    :cond_3
    return-void

    :cond_4
    move v0, v2

    goto :goto_0

    :cond_5
    move v1, v2

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/GeoDeciderFragment;->setRetainInstance(Z)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    const v0, 0x7f030083    # com.twitter.android.R.layout.geo_decider

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f090197    # com.twitter.android.R.id.duration

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/GeoDeciderFragment;->a:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/widget/GeoDeciderFragment;->a:Landroid/widget/TextView;

    const-string/jumbo v2, "Update Duration"

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f090199    # com.twitter.android.R.id.interval

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/GeoDeciderFragment;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/widget/GeoDeciderFragment;->b:Landroid/widget/TextView;

    const-string/jumbo v2, "Update Interval"

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f090194    # com.twitter.android.R.id.geo_enable_switch

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lcom/twitter/android/widget/GeoDeciderFragment;->f:Landroid/widget/CompoundButton;

    iget-object v0, p0, Lcom/twitter/android/widget/GeoDeciderFragment;->f:Landroid/widget/CompoundButton;

    new-instance v2, Lcom/twitter/android/widget/ac;

    invoke-direct {v2, p0}, Lcom/twitter/android/widget/ac;-><init>(Lcom/twitter/android/widget/GeoDeciderFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const v0, 0x7f090198    # com.twitter.android.R.id.duration_seek

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/twitter/android/widget/GeoDeciderFragment;->h:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/twitter/android/widget/GeoDeciderFragment;->h:Landroid/widget/SeekBar;

    sget-object v2, Lcom/twitter/android/widget/GeoDeciderFragment;->d:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/GeoDeciderFragment;->h:Landroid/widget/SeekBar;

    new-instance v2, Lcom/twitter/android/widget/ad;

    invoke-direct {v2, p0}, Lcom/twitter/android/widget/ad;-><init>(Lcom/twitter/android/widget/GeoDeciderFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    const v0, 0x7f09019a    # com.twitter.android.R.id.interval_seek

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/twitter/android/widget/GeoDeciderFragment;->i:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/twitter/android/widget/GeoDeciderFragment;->i:Landroid/widget/SeekBar;

    sget-object v2, Lcom/twitter/android/widget/GeoDeciderFragment;->e:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/GeoDeciderFragment;->i:Landroid/widget/SeekBar;

    new-instance v2, Lcom/twitter/android/widget/ae;

    invoke-direct {v2, p0}, Lcom/twitter/android/widget/ae;-><init>(Lcom/twitter/android/widget/GeoDeciderFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    const v0, 0x7f090195    # com.twitter.android.R.id.geo_play_services

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/twitter/android/widget/GeoDeciderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/library/platform/p;->a(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    return-object v1

    :cond_0
    const v0, 0x7f090196    # com.twitter.android.R.id.play_services_switch

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lcom/twitter/android/widget/GeoDeciderFragment;->g:Landroid/widget/CompoundButton;

    iget-object v0, p0, Lcom/twitter/android/widget/GeoDeciderFragment;->g:Landroid/widget/CompoundButton;

    new-instance v2, Lcom/twitter/android/widget/af;

    invoke-direct {v2, p0}, Lcom/twitter/android/widget/af;-><init>(Lcom/twitter/android/widget/GeoDeciderFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method
