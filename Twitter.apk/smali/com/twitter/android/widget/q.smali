.class public Lcom/twitter/android/widget/q;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/p;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/twitter/android/client/c;Lcom/twitter/android/widget/TopicView$TopicData;)Lcom/twitter/android/widget/TopicView;
    .locals 22

    invoke-static/range {p1 .. p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03006a    # com.twitter.android.R.layout.event_sports_hdr_view

    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/widget/TopicView;

    move-object/from16 v0, p4

    iget-object v2, v0, Lcom/twitter/android/widget/TopicView$TopicData;->a:Ljava/lang/String;

    move-object/from16 v0, p4

    iget v3, v0, Lcom/twitter/android/widget/TopicView$TopicData;->b:I

    move-object/from16 v0, p4

    iget-object v4, v0, Lcom/twitter/android/widget/TopicView$TopicData;->e:Ljava/lang/String;

    move-object/from16 v0, p4

    iget-object v5, v0, Lcom/twitter/android/widget/TopicView$TopicData;->c:Ljava/lang/String;

    move-object/from16 v0, p4

    iget-object v6, v0, Lcom/twitter/android/widget/TopicView$TopicData;->d:Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v0, p4

    iget-object v8, v0, Lcom/twitter/android/widget/TopicView$TopicData;->f:Ljava/lang/String;

    move-object/from16 v0, p4

    iget v9, v0, Lcom/twitter/android/widget/TopicView$TopicData;->j:I

    const-wide/16 v10, 0x0

    move-object/from16 v0, p3

    iget-object v12, v0, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    move-object/from16 v0, p4

    iget-object v13, v0, Lcom/twitter/android/widget/TopicView$TopicData;->g:Ljava/lang/String;

    move-object/from16 v0, p4

    iget-object v14, v0, Lcom/twitter/android/widget/TopicView$TopicData;->h:Ljava/lang/String;

    move-object/from16 v0, p4

    iget-object v15, v0, Lcom/twitter/android/widget/TopicView$TopicData;->k:[B

    const/16 v16, 0x0

    const/16 v17, 0x1

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    invoke-virtual/range {v1 .. v21}, Lcom/twitter/android/widget/TopicView;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLcom/twitter/library/widget/ap;Ljava/lang/String;Ljava/lang/String;[BZZLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method public a(Lcom/twitter/android/widget/TopicView$TopicData;)Ljava/lang/String;
    .locals 1

    iget-object v0, p1, Lcom/twitter/android/widget/TopicView$TopicData;->k:[B

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterTopic$SportsEvent;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/twitter/library/api/TwitterTopic$SportsEvent;->summary:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/client/c;Landroid/view/View;Lcom/twitter/android/widget/TopicView$TopicData;)V
    .locals 6

    move-object v0, p2

    check-cast v0, Lcom/twitter/android/widget/TopicView;

    iget-object v1, p3, Lcom/twitter/android/widget/TopicView$TopicData;->c:Ljava/lang/String;

    iget-object v2, p3, Lcom/twitter/android/widget/TopicView$TopicData;->d:Ljava/lang/String;

    iget-object v3, p3, Lcom/twitter/android/widget/TopicView$TopicData;->f:Ljava/lang/String;

    iget v4, p3, Lcom/twitter/android/widget/TopicView$TopicData;->j:I

    iget-object v5, p3, Lcom/twitter/android/widget/TopicView$TopicData;->k:[B

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/widget/TopicView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I[B)V

    return-void
.end method
