.class Lcom/twitter/android/widget/cq;
.super Landroid/os/Handler;
.source "Twttr"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;

.field private b:Landroid/widget/Filter;

.field private c:Lcom/twitter/android/widget/cr;


# direct methods
.method public constructor <init>(Landroid/os/Looper;Landroid/widget/Filter$FilterListener;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/widget/cq;->a:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public a(Landroid/widget/Filter;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/cq;->b:Landroid/widget/Filter;

    return-void
.end method

.method public a(Lcom/twitter/android/widget/cr;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/cq;->c:Lcom/twitter/android/widget/cr;

    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/widget/cq;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Filter$FilterListener;

    iget-object v1, p0, Lcom/twitter/android/widget/cq;->b:Landroid/widget/Filter;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/twitter/android/widget/cq;->b:Landroid/widget/Filter;

    invoke-virtual {v2, v1, v0}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterListener;)V

    iget v0, p1, Landroid/os/Message;->arg1:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/cq;->c:Lcom/twitter/android/widget/cr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/cq;->c:Lcom/twitter/android/widget/cr;

    invoke-interface {v0, v1}, Lcom/twitter/android/widget/cr;->a(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
