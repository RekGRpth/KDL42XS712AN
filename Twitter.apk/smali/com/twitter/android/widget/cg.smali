.class public Lcom/twitter/android/widget/cg;
.super Landroid/widget/BaseAdapter;
.source "Twttr"


# instance fields
.field a:Z

.field private final b:[Landroid/widget/BaseAdapter;

.field private final c:[Z

.field private final d:I

.field private final e:[I

.field private final f:I


# direct methods
.method public constructor <init>([Landroid/widget/BaseAdapter;)V
    .locals 6

    const/4 v1, 0x0

    const v4, 0x7f030133    # com.twitter.android.R.layout.section_divider

    const/4 v5, 0x1

    move-object v0, p0

    move-object v2, v1

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/widget/cg;-><init>([Z[I[Landroid/widget/BaseAdapter;II)V

    return-void
.end method

.method public constructor <init>([Landroid/widget/BaseAdapter;I)V
    .locals 6

    const/4 v1, 0x0

    const v4, 0x7f030133    # com.twitter.android.R.layout.section_divider

    move-object v0, p0

    move-object v2, v1

    move-object v3, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/widget/cg;-><init>([Z[I[Landroid/widget/BaseAdapter;II)V

    return-void
.end method

.method public constructor <init>([Z[I[Landroid/widget/BaseAdapter;II)V
    .locals 4

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/widget/cg;->a:Z

    iput-object p1, p0, Lcom/twitter/android/widget/cg;->c:[Z

    iput-object p3, p0, Lcom/twitter/android/widget/cg;->b:[Landroid/widget/BaseAdapter;

    iput p4, p0, Lcom/twitter/android/widget/cg;->d:I

    iput-object p2, p0, Lcom/twitter/android/widget/cg;->e:[I

    iput p5, p0, Lcom/twitter/android/widget/cg;->f:I

    new-instance v1, Lcom/twitter/android/widget/ch;

    invoke-direct {v1, p0}, Lcom/twitter/android/widget/ch;-><init>(Lcom/twitter/android/widget/cg;)V

    array-length v2, p3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p3, v0

    invoke-virtual {v3, v1}, Landroid/widget/BaseAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private d(I)I
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/cg;->b:[Landroid/widget/BaseAdapter;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/twitter/android/widget/cg;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method public a(II)I
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/widget/cg;->c(I)I

    move-result v0

    sub-int v0, p2, v0

    return v0
.end method

.method protected a(Landroid/widget/BaseAdapter;IIILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/twitter/android/widget/cg;->e:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/cg;->e:[I

    aget v0, v0, p3

    :goto_0
    if-nez v0, :cond_3

    invoke-virtual {p1, p4, p5, p6}, Landroid/widget/BaseAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    iget-object v1, p0, Lcom/twitter/android/widget/cg;->c:[Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/widget/cg;->c:[Z

    aget-boolean v1, v1, p3

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    if-nez p2, :cond_2

    if-lez p3, :cond_2

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/GroupedRowView;->setGroupStyle(I)V

    :goto_2
    return-object v0

    :cond_0
    iget v0, p0, Lcom/twitter/android/widget/cg;->f:I

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/twitter/android/widget/cg;->b:[Landroid/widget/BaseAdapter;

    array-length v2, v2

    invoke-virtual {v0, p3, v2, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->a(IIZ)V

    goto :goto_2

    :cond_3
    invoke-virtual {p1, p4, p5, p6}, Landroid/widget/BaseAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_2
.end method

.method public a(I)Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    move v2, v1

    move v3, v1

    :goto_0
    if-ge v2, p1, :cond_0

    invoke-direct {p0, v2}, Lcom/twitter/android/widget/cg;->d(I)I

    move-result v4

    add-int/2addr v3, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    if-nez v3, :cond_1

    :goto_1
    return v1

    :cond_1
    iget-object v2, p0, Lcom/twitter/android/widget/cg;->e:[I

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/twitter/android/widget/cg;->e:[I

    aget v2, v2, p1

    :goto_2
    if-ne v2, v0, :cond_3

    :goto_3
    move v1, v0

    goto :goto_1

    :cond_2
    iget v2, p0, Lcom/twitter/android/widget/cg;->f:I

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_3
.end method

.method public areAllItemsEnabled()Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/widget/cg;->b:[Landroid/widget/BaseAdapter;

    array-length v3, v2

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_2

    invoke-virtual {p0, v2}, Lcom/twitter/android/widget/cg;->a(I)Z

    move-result v4

    if-eqz v4, :cond_0

    move v2, v0

    :goto_1
    if-nez v2, :cond_1

    invoke-super {p0}, Landroid/widget/BaseAdapter;->areAllItemsEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_2
    return v0

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_2

    :cond_2
    move v2, v1

    goto :goto_1
.end method

.method public b(I)I
    .locals 6

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/twitter/android/widget/cg;->b:[Landroid/widget/BaseAdapter;

    array-length v4, v3

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    invoke-virtual {v5}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v5

    if-lez v5, :cond_0

    add-int/2addr v0, v5

    invoke-virtual {p0, v2}, Lcom/twitter/android/widget/cg;->a(I)Z

    move-result v5

    if-eqz v5, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    if-ge p1, v0, :cond_2

    :cond_1
    return v2

    :cond_2
    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public c(I)I
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, p1, :cond_1

    iget-object v2, p0, Lcom/twitter/android/widget/cg;->b:[Landroid/widget/BaseAdapter;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    add-int/2addr v0, v2

    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/cg;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/twitter/android/widget/cg;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    add-int/lit8 v0, v0, 0x1

    :cond_2
    return v0
.end method

.method public getCount()I
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v2, p0, Lcom/twitter/android/widget/cg;->b:[Landroid/widget/BaseAdapter;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/cg;->d(I)I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 9

    const/4 v2, 0x0

    const/4 v0, 0x0

    iget-object v5, p0, Lcom/twitter/android/widget/cg;->b:[Landroid/widget/BaseAdapter;

    array-length v6, v5

    move v3, v0

    move v4, v0

    move v1, p1

    :goto_0
    if-ge v3, v6, :cond_3

    aget-object v7, v5, v3

    invoke-virtual {v7}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_4

    invoke-virtual {p0, v4}, Lcom/twitter/android/widget/cg;->a(I)Z

    move-result v8

    if-eqz v8, :cond_1

    add-int/lit8 v0, v0, 0x1

    if-nez v1, :cond_0

    move-object v0, v2

    :goto_1
    return-object v0

    :cond_0
    if-ge v1, v0, :cond_2

    add-int/lit8 v0, v1, -0x1

    invoke-virtual {v7, v0}, Landroid/widget/BaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_1

    :cond_1
    if-ge v1, v0, :cond_2

    invoke-virtual {v7, v1}, Landroid/widget/BaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_1

    :cond_2
    sub-int v0, v1, v0

    :goto_2
    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_0

    :cond_3
    move-object v0, v2

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public getItemId(I)J
    .locals 10

    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    iget-object v6, p0, Lcom/twitter/android/widget/cg;->b:[Landroid/widget/BaseAdapter;

    array-length v7, v6

    move v4, v0

    move v5, v0

    move v1, p1

    :goto_0
    if-ge v4, v7, :cond_3

    aget-object v8, v6, v4

    invoke-virtual {v8}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_4

    invoke-virtual {p0, v5}, Lcom/twitter/android/widget/cg;->a(I)Z

    move-result v9

    if-eqz v9, :cond_1

    add-int/lit8 v0, v0, 0x1

    if-nez v1, :cond_0

    move-wide v0, v2

    :goto_1
    return-wide v0

    :cond_0
    if-ge v1, v0, :cond_2

    add-int/lit8 v0, v1, -0x1

    invoke-virtual {v8, v0}, Landroid/widget/BaseAdapter;->getItemId(I)J

    move-result-wide v0

    goto :goto_1

    :cond_1
    if-ge v1, v0, :cond_2

    invoke-virtual {v8, v1}, Landroid/widget/BaseAdapter;->getItemId(I)J

    move-result-wide v0

    goto :goto_1

    :cond_2
    sub-int v0, v1, v0

    :goto_2
    add-int/lit8 v5, v5, 0x1

    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v1, v0

    goto :goto_0

    :cond_3
    move-wide v0, v2

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public getItemViewType(I)I
    .locals 10

    const/4 v2, 0x0

    const/4 v0, 0x1

    iget-object v6, p0, Lcom/twitter/android/widget/cg;->b:[Landroid/widget/BaseAdapter;

    array-length v7, v6

    move v3, v2

    move v4, v2

    move v5, v0

    move v1, p1

    :goto_0
    if-ge v3, v7, :cond_3

    aget-object v8, v6, v3

    invoke-virtual {v8}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_4

    invoke-virtual {p0, v4}, Lcom/twitter/android/widget/cg;->a(I)Z

    move-result v9

    if-eqz v9, :cond_1

    add-int/lit8 v0, v0, 0x1

    if-nez v1, :cond_0

    move v0, v2

    :goto_1
    return v0

    :cond_0
    if-ge v1, v0, :cond_2

    add-int/lit8 v0, v1, -0x1

    invoke-virtual {v8, v0}, Landroid/widget/BaseAdapter;->getItemViewType(I)I

    move-result v0

    add-int/2addr v0, v5

    goto :goto_1

    :cond_1
    if-ge v1, v0, :cond_2

    invoke-virtual {v8, v1}, Landroid/widget/BaseAdapter;->getItemViewType(I)I

    move-result v0

    add-int/2addr v0, v5

    goto :goto_1

    :cond_2
    sub-int v0, v1, v0

    :goto_2
    invoke-virtual {v8}, Landroid/widget/BaseAdapter;->getViewTypeCount()I

    move-result v1

    add-int/2addr v5, v1

    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_0

    :cond_3
    invoke-super {p0, v1}, Landroid/widget/BaseAdapter;->getItemViewType(I)I

    move-result v0

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    const/4 v6, 0x0

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    iget-object v8, p0, Lcom/twitter/android/widget/cg;->b:[Landroid/widget/BaseAdapter;

    array-length v9, v8

    move v5, v6

    move v2, v6

    move v3, v6

    move v4, p1

    :goto_0
    if-ge v5, v9, :cond_0

    aget-object v1, v8, v5

    invoke-virtual {v1}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_4

    invoke-virtual {p0, v3}, Lcom/twitter/android/widget/cg;->a(I)Z

    move-result v10

    if-eqz v10, :cond_2

    add-int/lit8 v0, v0, 0x1

    if-nez v4, :cond_1

    if-nez p2, :cond_0

    iget v0, p0, Lcom/twitter/android/widget/cg;->d:I

    invoke-virtual {v7, v0, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    :goto_1
    return-object p2

    :cond_1
    if-ge v4, v0, :cond_3

    add-int/lit8 v4, v4, -0x1

    move-object v0, p0

    move-object v5, p2

    move-object v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/widget/cg;->a(Landroid/widget/BaseAdapter;IIILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_1

    :cond_2
    if-ge v4, v0, :cond_3

    move-object v0, p0

    move-object v5, p2

    move-object v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/widget/cg;->a(Landroid/widget/BaseAdapter;IIILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_1

    :cond_3
    sub-int/2addr v4, v0

    add-int/lit8 v2, v2, 0x1

    :cond_4
    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 5

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/twitter/android/widget/cg;->b:[Landroid/widget/BaseAdapter;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    invoke-virtual {v4}, Landroid/widget/BaseAdapter;->getViewTypeCount()I

    move-result v4

    add-int/2addr v1, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    return v0
.end method

.method public isEnabled(I)Z
    .locals 5

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/twitter/android/widget/cg;->b:[Landroid/widget/BaseAdapter;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lcom/twitter/android/widget/cg;->b:[Landroid/widget/BaseAdapter;

    aget-object v3, v2, v0

    invoke-virtual {v3}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v2

    if-lez v2, :cond_3

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/cg;->a(I)Z

    move-result v4

    if-eqz v4, :cond_1

    add-int/lit8 v2, v2, 0x1

    if-nez p1, :cond_0

    :goto_1
    return v1

    :cond_0
    if-ge p1, v2, :cond_2

    add-int/lit8 v0, p1, -0x1

    invoke-virtual {v3, v0}, Landroid/widget/BaseAdapter;->isEnabled(I)Z

    move-result v1

    goto :goto_1

    :cond_1
    if-ge p1, v2, :cond_2

    invoke-virtual {v3, p1}, Landroid/widget/BaseAdapter;->isEnabled(I)Z

    move-result v1

    goto :goto_1

    :cond_2
    sub-int/2addr p1, v2

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    invoke-super {p0, p1}, Landroid/widget/BaseAdapter;->isEnabled(I)Z

    move-result v1

    goto :goto_1
.end method

.method public notifyDataSetChanged()V
    .locals 1

    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/widget/cg;->a:Z

    return-void
.end method
