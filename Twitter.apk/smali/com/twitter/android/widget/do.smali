.class public Lcom/twitter/android/widget/do;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Landroid/view/View;

.field public final b:Lcom/twitter/android/widget/TweetStatView;

.field public final c:Lcom/twitter/android/widget/TweetStatView;

.field public final d:Lcom/twitter/android/widget/TweetStatView;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/view/View$OnClickListener;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/widget/do;->a:Landroid/view/View;

    const v0, 0x7f09024e    # com.twitter.android.R.id.replies_stat

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/TweetStatView;

    invoke-virtual {v0, p2}, Lcom/twitter/android/widget/TweetStatView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/twitter/android/widget/do;->b:Lcom/twitter/android/widget/TweetStatView;

    const v0, 0x7f09024d    # com.twitter.android.R.id.retweets_stat

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/TweetStatView;

    invoke-virtual {v0, p2}, Lcom/twitter/android/widget/TweetStatView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/twitter/android/widget/do;->c:Lcom/twitter/android/widget/TweetStatView;

    const v0, 0x7f09024c    # com.twitter.android.R.id.favorites_stat

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/TweetStatView;

    invoke-virtual {v0, p2}, Lcom/twitter/android/widget/TweetStatView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/twitter/android/widget/do;->d:Lcom/twitter/android/widget/TweetStatView;

    return-void
.end method

.method private a(Lcom/twitter/android/widget/TweetStatView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-static {p2}, Lcom/twitter/library/api/ActivitySummary;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, p2}, Lcom/twitter/android/widget/TweetStatView;->setValue(Ljava/lang/CharSequence;)V

    invoke-virtual {p1, p3}, Lcom/twitter/android/widget/TweetStatView;->setName(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/twitter/android/widget/TweetStatView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/twitter/android/widget/TweetStatView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/res/Resources;Lcom/twitter/library/api/ActivitySummary;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/widget/do;->a(Landroid/content/res/Resources;Lcom/twitter/library/api/ActivitySummary;Ljava/lang/Boolean;)V

    return-void
.end method

.method public a(Landroid/content/res/Resources;Lcom/twitter/library/api/ActivitySummary;Ljava/lang/Boolean;)V
    .locals 7

    const/4 v6, 0x0

    const/16 v4, 0x8

    if-eqz p2, :cond_0

    iget-object v0, p2, Lcom/twitter/library/api/ActivitySummary;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/library/api/ActivitySummary;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p2, Lcom/twitter/library/api/ActivitySummary;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/library/api/ActivitySummary;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/do;->a:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/do;->a:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0e000d    # com.twitter.android.R.plurals.photo_stat_label_replies

    :goto_1
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    const v1, 0x7f0e000e    # com.twitter.android.R.plurals.photo_stat_label_retweets

    :goto_2
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_4

    const v2, 0x7f0e000c    # com.twitter.android.R.plurals.photo_stat_label_favorites

    :goto_3
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_6

    iget v3, p2, Lcom/twitter/library/api/ActivitySummary;->c:I

    if-lez v3, :cond_5

    iget-object v4, p0, Lcom/twitter/android/widget/do;->b:Lcom/twitter/android/widget/TweetStatView;

    invoke-static {p1, v3}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v4, v5, v0}, Lcom/twitter/android/widget/do;->a(Lcom/twitter/android/widget/TweetStatView;Ljava/lang/String;Ljava/lang/String;)V

    :goto_4
    :try_start_0
    iget-object v0, p2, Lcom/twitter/library/api/ActivitySummary;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_7

    iget-object v3, p0, Lcom/twitter/android/widget/do;->c:Lcom/twitter/android/widget/TweetStatView;

    invoke-static {p1, v0}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v1, v0}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v3, v4, v0}, Lcom/twitter/android/widget/do;->a(Lcom/twitter/android/widget/TweetStatView;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_5
    :try_start_1
    iget-object v0, p2, Lcom/twitter/library/api/ActivitySummary;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_8

    iget-object v1, p0, Lcom/twitter/android/widget/do;->d:Lcom/twitter/android/widget/TweetStatView;

    invoke-static {p1, v0}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v0}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v3, v0}, Lcom/twitter/android/widget/do;->a(Lcom/twitter/android/widget/TweetStatView;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/twitter/android/widget/do;->d:Lcom/twitter/android/widget/TweetStatView;

    iget-object v1, p2, Lcom/twitter/library/api/ActivitySummary;->a:Ljava/lang/String;

    invoke-virtual {p1, v2, v6}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/widget/do;->a(Lcom/twitter/android/widget/TweetStatView;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const v0, 0x7f0e001f    # com.twitter.android.R.plurals.stat_label_replies

    goto :goto_1

    :cond_3
    const v1, 0x7f0e0020    # com.twitter.android.R.plurals.stat_label_retweets

    goto :goto_2

    :cond_4
    const v2, 0x7f0e001e    # com.twitter.android.R.plurals.stat_label_favorites

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lcom/twitter/android/widget/do;->b:Lcom/twitter/android/widget/TweetStatView;

    invoke-virtual {v0, v4}, Lcom/twitter/android/widget/TweetStatView;->setVisibility(I)V

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lcom/twitter/android/widget/do;->b:Lcom/twitter/android/widget/TweetStatView;

    invoke-virtual {v0, v4}, Lcom/twitter/android/widget/TweetStatView;->setVisibility(I)V

    goto :goto_4

    :cond_7
    :try_start_2
    iget-object v0, p0, Lcom/twitter/android/widget/do;->c:Lcom/twitter/android/widget/TweetStatView;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/TweetStatView;->setVisibility(I)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_5

    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/twitter/android/widget/do;->c:Lcom/twitter/android/widget/TweetStatView;

    iget-object v3, p2, Lcom/twitter/library/api/ActivitySummary;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v6}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v3, v1}, Lcom/twitter/android/widget/do;->a(Lcom/twitter/android/widget/TweetStatView;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    :cond_8
    :try_start_3
    iget-object v0, p0, Lcom/twitter/android/widget/do;->d:Lcom/twitter/android/widget/TweetStatView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/TweetStatView;->setVisibility(I)V
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0
.end method
