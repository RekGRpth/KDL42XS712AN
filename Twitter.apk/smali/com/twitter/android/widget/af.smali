.class Lcom/twitter/android/widget/af;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/widget/GeoDeciderFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/widget/GeoDeciderFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/af;->a:Lcom/twitter/android/widget/GeoDeciderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4

    const/4 v3, -0x1

    iget-object v0, p0, Lcom/twitter/android/widget/af;->a:Lcom/twitter/android/widget/GeoDeciderFragment;

    iget-object v0, v0, Lcom/twitter/android/widget/GeoDeciderFragment;->c:Lcom/twitter/library/platform/LocationProducer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/af;->a:Lcom/twitter/android/widget/GeoDeciderFragment;

    iget-object v1, v0, Lcom/twitter/android/widget/GeoDeciderFragment;->c:Lcom/twitter/library/platform/LocationProducer;

    iget-object v0, p0, Lcom/twitter/android/widget/af;->a:Lcom/twitter/android/widget/GeoDeciderFragment;

    invoke-static {v0}, Lcom/twitter/android/widget/GeoDeciderFragment;->a(Lcom/twitter/android/widget/GeoDeciderFragment;)Landroid/widget/CompoundButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v2

    if-eqz p2, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v2, v0, v3, v3}, Lcom/twitter/library/platform/LocationProducer;->a(ZIII)V

    iget-object v0, p0, Lcom/twitter/android/widget/af;->a:Lcom/twitter/android/widget/GeoDeciderFragment;

    invoke-static {v0}, Lcom/twitter/android/widget/GeoDeciderFragment;->b(Lcom/twitter/android/widget/GeoDeciderFragment;)Lcom/twitter/android/widget/ag;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/af;->a:Lcom/twitter/android/widget/GeoDeciderFragment;

    invoke-static {v0}, Lcom/twitter/android/widget/GeoDeciderFragment;->b(Lcom/twitter/android/widget/GeoDeciderFragment;)Lcom/twitter/android/widget/ag;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/widget/ag;->b()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
