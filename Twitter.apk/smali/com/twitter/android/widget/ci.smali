.class Lcom/twitter/android/widget/ci;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/widget/SignedOutDialogFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/widget/SignedOutDialogFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/ci;->a:Lcom/twitter/android/widget/SignedOutDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/twitter/android/widget/SignedOutDialogFragment;->a()Lcom/twitter/android/SignedOutActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/LoginActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v1, Lcom/twitter/android/SignedOutFragment;->a:Ljava/lang/String;

    invoke-static {}, Lcom/twitter/android/widget/SignedOutDialogFragment;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {}, Lcom/twitter/android/widget/SignedOutDialogFragment;->a()Lcom/twitter/android/SignedOutActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/SignedOutActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
