.class public Lcom/twitter/android/widget/DraggableDrawerLayout;
.super Landroid/widget/RelativeLayout;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/a;


# instance fields
.field private A:Z

.field private B:I

.field private a:Landroid/content/Context;

.field private b:Lcom/twitter/android/widget/AnimatableRelativeLayout;

.field private c:I

.field private d:Landroid/view/View;

.field private e:F

.field private f:I

.field private g:Z

.field private h:Z

.field private i:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

.field private j:I

.field private k:Z

.field private l:I

.field private m:Landroid/widget/ImageView;

.field private n:Z

.field private o:I

.field private p:Lcom/twitter/android/widget/k;

.field private q:Landroid/view/VelocityTracker;

.field private r:I

.field private s:I

.field private t:F

.field private u:Landroid/widget/Scroller;

.field private v:Lcom/twitter/library/util/av;

.field private w:I

.field private x:I

.field private y:I

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, -0x1

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->f:I

    sget-object v0, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->b:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    iput-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->i:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    iput v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->o:I

    iput v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->x:I

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const/4 v1, -0x1

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->f:I

    sget-object v0, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->b:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    iput-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->i:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    iput v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->o:I

    iput v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->x:I

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    const/4 v1, -0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->f:I

    sget-object v0, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->b:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    iput-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->i:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    iput v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->o:I

    iput v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->x:I

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private a(ILandroid/view/View;Landroid/view/View;)I
    .locals 1

    :goto_0
    if-eq p2, p3, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v0

    add-int/2addr p1, v0

    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    move-object p2, v0

    goto :goto_0

    :cond_0
    return p1
.end method

.method private a(Landroid/view/MotionEvent;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->b:Lcom/twitter/android/widget/AnimatableRelativeLayout;

    invoke-virtual {v2}, Lcom/twitter/android/widget/AnimatableRelativeLayout;->getTop()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/view/MotionEvent;->setLocation(FF)V

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->b:Lcom/twitter/android/widget/AnimatableRelativeLayout;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/AnimatableRelativeLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    return-void
.end method

.method private a(Landroid/view/View;F)V
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    float-to-int v2, p2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v3

    float-to-int v4, p2

    sub-int/2addr v3, v4

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    return-void
.end method

.method private a(Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;ZIII)V
    .locals 6

    const/high16 v5, 0x40000000    # 2.0f

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->b:Lcom/twitter/android/widget/AnimatableRelativeLayout;

    invoke-virtual {v2}, Lcom/twitter/android/widget/AnimatableRelativeLayout;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->b:Lcom/twitter/android/widget/AnimatableRelativeLayout;

    sget-object v3, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->c:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    if-eq p1, v3, :cond_1

    if-eqz p2, :cond_3

    :cond_1
    :goto_1
    invoke-static {p5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    sub-int v4, p4, p3

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/twitter/android/widget/AnimatableRelativeLayout;->measure(II)V

    invoke-virtual {v2, v1, p3, p5, p4}, Lcom/twitter/android/widget/AnimatableRelativeLayout;->layout(IIII)V

    sget-object v2, Lcom/twitter/android/widget/j;->a:[I

    invoke-virtual {p1}, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->p:Lcom/twitter/android/widget/k;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->p:Lcom/twitter/android/widget/k;

    invoke-interface {v2, v1}, Lcom/twitter/android/widget/k;->f(Z)V

    :cond_2
    invoke-direct {p0, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setForegroundAlpha(Z)V

    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->w:I

    invoke-virtual {p0, v1, v0, p5, p3}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(IIII)V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getClientAreaHeight()I

    move-result v3

    add-int p4, p3, v3

    goto :goto_1

    :pswitch_1
    iget-object v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->p:Lcom/twitter/android/widget/k;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->p:Lcom/twitter/android/widget/k;

    invoke-interface {v2}, Lcom/twitter/android/widget/k;->t()V

    :cond_4
    invoke-direct {p0, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setForegroundAlpha(Z)V

    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->w:I

    invoke-virtual {p0, v1, v0, p5, p3}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(IIII)V

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->p:Lcom/twitter/android/widget/k;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->p:Lcom/twitter/android/widget/k;

    invoke-interface {v2, v0}, Lcom/twitter/android/widget/k;->f(Z)V

    :cond_5
    invoke-direct {p0, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setForegroundAlpha(Z)V

    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->w:I

    invoke-virtual {p0, v1, v0, p5, p3}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(IIII)V

    goto :goto_0

    :pswitch_3
    iget-object v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->p:Lcom/twitter/android/widget/k;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->p:Lcom/twitter/android/widget/k;

    sget-object v3, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->e:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    if-ne p1, v3, :cond_7

    :goto_2
    invoke-interface {v2, v0}, Lcom/twitter/android/widget/k;->g(Z)V

    :cond_6
    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->w:I

    iget v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->y:I

    invoke-virtual {p0, v1, v0, p5, v2}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(IIII)V

    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(I)Z
    .locals 2

    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->f:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->f:I

    sub-int/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->j:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(ILandroid/view/View;)Z
    .locals 1

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v0

    if-lt p1, v0, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v0

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(I)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    sget-object v2, Lcom/twitter/android/widget/j;->a:[I

    iget-object v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->i:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    invoke-virtual {v3}, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    if-lez p1, :cond_0

    iget-boolean v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->k:Z

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :pswitch_2
    if-gez p1, :cond_0

    iget-boolean v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->k:Z

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private c(Z)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    iput-boolean p1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->n:Z

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->v:Lcom/twitter/library/util/av;

    invoke-virtual {v0}, Lcom/twitter/library/util/av;->a()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->v:Lcom/twitter/library/util/av;

    invoke-virtual {v0}, Lcom/twitter/library/util/av;->b()V

    goto :goto_0
.end method

.method private d(Z)V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->p:Lcom/twitter/android/widget/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->p:Lcom/twitter/android/widget/k;

    invoke-interface {v0, p1}, Lcom/twitter/android/widget/k;->h(Z)V

    :cond_0
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->t:F

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->f()V

    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->n:Z

    if-eqz v0, :cond_2

    invoke-direct {p0, v5}, Lcom/twitter/android/widget/DraggableDrawerLayout;->c(Z)V

    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_3

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->m:Landroid/widget/ImageView;

    new-instance v1, Lcom/twitter/library/util/av;

    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getAlpha()F

    move-result v3

    const/16 v4, 0x96

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/twitter/library/util/av;-><init>(Landroid/widget/ImageView;FFI)V

    invoke-virtual {v1}, Lcom/twitter/library/util/av;->b()V

    :cond_3
    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->y:I

    invoke-direct {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getClientAreaHeight()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->b:Lcom/twitter/android/widget/AnimatableRelativeLayout;

    iget v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->y:I

    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getWidth()I

    move-result v3

    invoke-virtual {v1, v5, v2, v3, v0}, Lcom/twitter/android/widget/AnimatableRelativeLayout;->a(IIII)V

    goto :goto_0
.end method

.method private f()V
    .locals 10
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    const v6, 0x7fffffff

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->i:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    sget-object v3, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->a:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    if-eq v2, v3, :cond_4

    move v9, v0

    :goto_0
    if-nez v9, :cond_5

    :goto_1
    iget-object v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->q:Landroid/view/VelocityTracker;

    if-eqz v2, :cond_3

    const/16 v3, 0x3e8

    iget v4, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->s:I

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v4, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->r:I

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    neg-float v3, v2

    iput v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->t:F

    :cond_0
    iget-object v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->b:Lcom/twitter/android/widget/AnimatableRelativeLayout;

    invoke-virtual {v3}, Lcom/twitter/android/widget/AnimatableRelativeLayout;->getTop()I

    move-result v3

    if-eqz v9, :cond_6

    if-lez v3, :cond_6

    mul-int/lit16 v0, v3, 0x96

    mul-int/lit8 v0, v0, 0x2

    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getHeight()I

    move-result v3

    div-int/2addr v0, v3

    :goto_2
    iget-object v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->b:Lcom/twitter/android/widget/AnimatableRelativeLayout;

    invoke-virtual {v3, v0}, Lcom/twitter/android/widget/AnimatableRelativeLayout;->setAnimationDuration(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->u:Landroid/widget/Scroller;

    if-nez v0, :cond_1

    new-instance v0, Landroid/widget/Scroller;

    iget-object v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->a:Landroid/content/Context;

    invoke-direct {v0, v3}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->u:Landroid/widget/Scroller;

    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-le v0, v3, :cond_2

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->u:Landroid/widget/Scroller;

    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollFriction()F

    move-result v3

    const/high16 v4, 0x40400000    # 3.0f

    mul-float/2addr v3, v4

    invoke-virtual {v0, v3}, Landroid/widget/Scroller;->setFriction(F)V

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->u:Landroid/widget/Scroller;

    float-to-int v4, v2

    const/high16 v7, -0x80000000

    move v2, v1

    move v3, v1

    move v5, v1

    move v8, v6

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    :cond_3
    if-eqz v9, :cond_8

    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->B:I

    :goto_3
    iget-object v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->b:Lcom/twitter/android/widget/AnimatableRelativeLayout;

    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getWidth()I

    move-result v3

    invoke-direct {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getClientAreaHeight()I

    move-result v4

    invoke-virtual {v2, v1, v0, v3, v4}, Lcom/twitter/android/widget/AnimatableRelativeLayout;->a(IIII)V

    return-void

    :cond_4
    move v9, v1

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1

    :cond_6
    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getHeight()I

    move-result v0

    if-ge v3, v0, :cond_7

    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getHeight()I

    move-result v0

    sub-int/2addr v0, v3

    mul-int/lit16 v0, v0, 0x96

    mul-int/lit8 v0, v0, 0x2

    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getHeight()I

    move-result v3

    div-int/2addr v0, v3

    goto :goto_2

    :cond_7
    move v0, v1

    goto :goto_2

    :cond_8
    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getHeight()I

    move-result v0

    goto :goto_3
.end method

.method private getClientAreaHeight()I
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const v1, 0x1020002    # android.R.id.content

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v2, v1, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(ILandroid/view/View;Landroid/view/View;)I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    sub-int/2addr v0, v1

    return v0
.end method

.method private getDrawerUpTop()I
    .locals 2

    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->x:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->x:I

    iget v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->o:I

    sub-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getClientAreaHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    goto :goto_0
.end method

.method private setForegroundAlpha(Z)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    const/16 v1, 0xb

    if-eqz p1, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->m:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->m:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->m:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    goto :goto_0

    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v1, :cond_2

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->m:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->m:Landroid/widget/ImageView;

    const/high16 v1, 0x3f400000    # 0.75f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    goto :goto_0
.end method

.method private setView(Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->i:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;ZZII)I
    .locals 3

    if-eqz p2, :cond_0

    if-nez p3, :cond_0

    sub-int v0, p5, p4

    iget v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->o:I

    sub-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/twitter/android/widget/j;->a:[I

    invoke-virtual {p1}, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unknown view mode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    sub-int v0, p5, p4

    iget v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->o:I

    sub-int/2addr v0, v1

    goto :goto_0

    :pswitch_1
    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->y:I

    goto :goto_0

    :pswitch_2
    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->B:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a()V
    .locals 0

    return-void
.end method

.method a(IIII)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v1, p1

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v0, p3, v0

    iget-object v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->d:Landroid/view/View;

    invoke-virtual {v2, v1, p2, v0, p4}, Landroid/view/View;->layout(IIII)V

    return-void
.end method

.method a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->a:Landroid/content/Context;

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->j:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->r:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->s:I

    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->t:F

    if-eqz p2, :cond_0

    sget-object v0, Lcom/twitter/android/rg;->DraggableDrawerLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->c:I

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->l:I

    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->w:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 1

    sget-object v0, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->a:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setView(Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;)V

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->f()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->requestLayout()V

    goto :goto_0
.end method

.method public a(ZZZZZ)V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->i:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    sget-object v3, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->b:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    if-eq v0, v3, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->i:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    sget-object v3, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->c:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    if-eq v0, v3, :cond_0

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean p3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->z:Z

    iput-boolean p5, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->A:Z

    iget v3, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v3, v2, :cond_1

    sget-object v2, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->e:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    invoke-direct {p0, v2}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setView(Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;)V

    :goto_1
    iget-object v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->b:Lcom/twitter/android/widget/AnimatableRelativeLayout;

    invoke-virtual {v2, v1}, Lcom/twitter/android/widget/AnimatableRelativeLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->b:Lcom/twitter/android/widget/AnimatableRelativeLayout;

    invoke-virtual {v1}, Lcom/twitter/android/widget/AnimatableRelativeLayout;->requestFocus()Z

    if-eqz v0, :cond_5

    invoke-direct {p0, p4}, Lcom/twitter/android/widget/DraggableDrawerLayout;->d(Z)V

    :goto_2
    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    if-nez p1, :cond_3

    if-eqz p4, :cond_2

    sget-object v2, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->d:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    :goto_3
    invoke-direct {p0, v2}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setView(Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;)V

    goto :goto_1

    :cond_2
    sget-object v2, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->b:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    goto :goto_3

    :cond_3
    if-eqz p4, :cond_4

    sget-object v2, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->e:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    :goto_4
    invoke-direct {p0, v2}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setView(Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;)V

    goto :goto_1

    :cond_4
    sget-object v2, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->c:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    goto :goto_4

    :cond_5
    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->requestLayout()V

    goto :goto_2
.end method

.method public b()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->p:Lcom/twitter/android/widget/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->u:Landroid/widget/Scroller;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->u:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->i:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    sget-object v1, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->d:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    if-ne v0, v1, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->u:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrVelocity()F

    move-result v0

    neg-float v0, v0

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->p:Lcom/twitter/android/widget/k;

    invoke-interface {v1, v0}, Lcom/twitter/android/widget/k;->a(F)V

    :cond_0
    return-void

    :cond_1
    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->t:F

    goto :goto_0
.end method

.method public b(Z)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->i:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    sget-object v1, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->c:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->i:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    sget-object v1, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->e:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->i:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    sget-object v1, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->d:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->d:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setView(Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;)V

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->d(Z)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->requestLayout()V

    goto :goto_0
.end method

.method public c()V
    .locals 2

    sget-object v0, Lcom/twitter/android/widget/j;->a:[I

    iget-object v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->i:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    invoke-virtual {v1}, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    sget-object v0, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->c:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setView(Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;)V

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->e:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setView(Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public d()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(Z)V

    return-void
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->q:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->q:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->q:Landroid/view/VelocityTracker;

    :cond_0
    return-void
.end method

.method getAboveDrawerView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->d:Landroid/view/View;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 5

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->c:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->d:Landroid/view/View;

    const v0, 0x7f090102    # com.twitter.android.R.id.action_drawer

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/AnimatableRelativeLayout;

    iput-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->b:Lcom/twitter/android/widget/AnimatableRelativeLayout;

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->b:Lcom/twitter/android/widget/AnimatableRelativeLayout;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/AnimatableRelativeLayout;->setAnimationListener(Lcom/twitter/android/widget/a;)V

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->b:Lcom/twitter/android/widget/AnimatableRelativeLayout;

    const/16 v1, 0x96

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/AnimatableRelativeLayout;->setAnimationDuration(I)V

    iget v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->l:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->m:Landroid/widget/ImageView;

    new-instance v0, Lcom/twitter/library/util/av;

    iget-object v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->m:Landroid/widget/ImageView;

    const/4 v2, 0x0

    const/high16 v3, 0x3f400000    # 0.75f

    const/16 v4, 0x1f4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/library/util/av;-><init>(Landroid/widget/ImageView;FFI)V

    iput-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->v:Lcom/twitter/library/util/av;

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    iget-boolean v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->h:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->i:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    sget-object v4, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->c:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->i:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    sget-object v4, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->e:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    if-ne v3, v4, :cond_1

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_1
    const/4 v3, 0x3

    if-eq v2, v3, :cond_2

    if-ne v2, v0, :cond_3

    :cond_2
    iput-boolean v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->g:Z

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget-object v4, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->b:Lcom/twitter/android/widget/AnimatableRelativeLayout;

    invoke-virtual {v4}, Lcom/twitter/android/widget/AnimatableRelativeLayout;->getTop()I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    packed-switch v2, :pswitch_data_0

    :cond_4
    :pswitch_0
    move v0, v1

    goto :goto_0

    :pswitch_1
    iget-boolean v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->g:Z

    if-nez v2, :cond_5

    const/4 v2, 0x0

    cmpl-float v2, v3, v2

    if-ltz v2, :cond_4

    iget v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->f:I

    int-to-float v2, v2

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v4, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->j:I

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-lez v2, :cond_4

    :cond_5
    iput v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->e:F

    goto :goto_0

    :pswitch_2
    float-to-int v2, v3

    iput v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->f:I

    iget v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->o:I

    int-to-float v2, v2

    cmpl-float v2, v3, v2

    if-ltz v2, :cond_4

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->b:Lcom/twitter/android/widget/AnimatableRelativeLayout;

    invoke-virtual {v2}, Lcom/twitter/android/widget/AnimatableRelativeLayout;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    sub-int v4, p5, p3

    sub-int/2addr v3, v4

    const v4, 0x7f0c00c6    # com.twitter.android.R.dimen.threshold_keyboard_visible

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    if-le v3, v2, :cond_3

    iput-boolean v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->h:Z

    sub-int v2, p5, p3

    iput v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->x:I

    move v2, v0

    :goto_1
    iget v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->o:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    iget-object v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->b:Lcom/twitter/android/widget/AnimatableRelativeLayout;

    const-string/jumbo v4, "drawer_header"

    invoke-virtual {v3, v4}, Lcom/twitter/android/widget/AnimatableRelativeLayout;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v3

    if-nez v3, :cond_4

    move v3, v1

    :goto_2
    iput v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->o:I

    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getDrawerUpTop()I

    move-result v3

    iput v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->y:I

    iget-boolean v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->z:Z

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->i:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    sget-object v4, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->d:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    if-eq v3, v4, :cond_2

    iget-object v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->i:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    sget-object v4, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->e:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    if-ne v3, v4, :cond_5

    :cond_2
    move v3, v0

    :goto_3
    iget-object v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->i:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    move-object v0, p0

    move v4, p3

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;ZZII)I

    move-result v4

    iget-object v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->i:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    sub-int v6, p4, p2

    move-object v1, p0

    move v5, p5

    invoke-direct/range {v1 .. v6}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;ZIII)V

    goto :goto_0

    :cond_3
    iput-boolean v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->h:Z

    move v2, v1

    goto :goto_1

    :cond_4
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    goto :goto_2

    :cond_5
    move v3, v1

    goto :goto_3
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    const/4 v5, 0x3

    const/4 v6, 0x0

    const/4 v7, 0x1

    iget-object v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->b:Lcom/twitter/android/widget/AnimatableRelativeLayout;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    iget-boolean v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->h:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->i:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    sget-object v4, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->c:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->i:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    sget-object v4, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->e:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    if-ne v3, v4, :cond_1

    :cond_0
    :goto_0
    return v7

    :cond_1
    iget-object v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->q:Landroid/view/VelocityTracker;

    if-nez v3, :cond_2

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->q:Landroid/view/VelocityTracker;

    :cond_2
    iget-object v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->q:Landroid/view/VelocityTracker;

    invoke-virtual {v3, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-direct {p0, v1, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(ILandroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->g:Z

    if-nez v2, :cond_3

    invoke-direct {p0, p1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(Landroid/view/MotionEvent;)V

    :cond_3
    iget-boolean v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->k:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->i:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    sget-object v3, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->c:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    if-eq v2, v3, :cond_4

    invoke-direct {p0, v1, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(ILandroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_4

    int-to-float v0, v1

    iput v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->e:F

    iput v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->f:I

    goto :goto_0

    :cond_4
    iput-boolean v6, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->g:Z

    goto :goto_0

    :pswitch_1
    const/high16 v3, -0x80000000

    iput v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->f:I

    const/high16 v3, -0x31000000

    iput v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->e:F

    iget-boolean v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->g:Z

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->i:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    sget-object v4, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->d:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    if-ne v3, v4, :cond_9

    iget v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->B:I

    add-int/lit8 v3, v3, 0xa

    if-lt v2, v3, :cond_8

    sget-object v2, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->b:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    invoke-direct {p0, v2}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setView(Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;)V

    invoke-direct {p0, v6}, Lcom/twitter/android/widget/DraggableDrawerLayout;->d(Z)V

    :cond_5
    :goto_1
    invoke-direct {p0, v1, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(ILandroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->g:Z

    if-eqz v0, :cond_6

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->setAction(I)V

    :cond_6
    invoke-direct {p0, p1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(Landroid/view/MotionEvent;)V

    :cond_7
    iput-boolean v6, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->g:Z

    goto :goto_0

    :cond_8
    invoke-direct {p0, v7}, Lcom/twitter/android/widget/DraggableDrawerLayout;->d(Z)V

    goto :goto_1

    :cond_9
    iget v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->y:I

    add-int/lit8 v3, v3, -0x14

    if-gt v2, v3, :cond_a

    sget-object v2, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->d:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    invoke-direct {p0, v2}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setView(Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;)V

    invoke-direct {p0, v7}, Lcom/twitter/android/widget/DraggableDrawerLayout;->d(Z)V

    goto :goto_1

    :cond_a
    invoke-direct {p0, v6}, Lcom/twitter/android/widget/DraggableDrawerLayout;->d(Z)V

    goto :goto_1

    :pswitch_2
    iget v3, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->e:F

    int-to-float v4, v1

    sub-float/2addr v3, v4

    int-to-float v4, v1

    iput v4, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->e:F

    invoke-direct {p0, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(I)Z

    move-result v4

    if-eqz v4, :cond_d

    if-le v1, v2, :cond_d

    float-to-int v1, v3

    invoke-direct {p0, v1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->b(I)Z

    move-result v1

    if-eqz v1, :cond_d

    iput-boolean v7, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->g:Z

    iget-object v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->p:Lcom/twitter/android/widget/k;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->p:Lcom/twitter/android/widget/k;

    invoke-interface {v1}, Lcom/twitter/android/widget/k;->u()V

    :cond_b
    iget-boolean v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->n:Z

    if-nez v1, :cond_c

    invoke-direct {p0, v7}, Lcom/twitter/android/widget/DraggableDrawerLayout;->c(Z)V

    :cond_c
    :goto_2
    iget-boolean v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->g:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getTop()I

    move-result v1

    iget v4, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->B:I

    add-int/2addr v1, v4

    int-to-float v4, v2

    sub-float/2addr v4, v3

    int-to-float v5, v1

    cmpg-float v4, v4, v5

    if-gtz v4, :cond_f

    sget-object v0, Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;->d:Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->setView(Lcom/twitter/android/widget/DraggableDrawerLayout$DrawerState;)V

    iput-boolean v6, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->g:Z

    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->requestLayout()V

    invoke-direct {p0, p1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    :cond_d
    iget-boolean v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->g:Z

    if-eqz v1, :cond_e

    iget-boolean v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->A:Z

    if-eqz v1, :cond_e

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->setAction(I)V

    :cond_e
    invoke-direct {p0, p1}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(Landroid/view/MotionEvent;)V

    goto :goto_2

    :cond_f
    if-lt v2, v1, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v1, v4, :cond_10

    iget-object v1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->m:Landroid/widget/ImageView;

    const/high16 v4, 0x3f400000    # 0.75f

    const/high16 v5, 0x3f800000    # 1.0f

    int-to-float v2, v2

    sub-float/2addr v2, v3

    const/high16 v6, 0x40000000    # 2.0f

    mul-float/2addr v2, v6

    invoke-virtual {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->getHeight()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v2, v6

    sub-float v2, v5, v2

    invoke-static {v4, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    :cond_10
    invoke-direct {p0, v0, v3}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(Landroid/view/View;F)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setAllowGestures(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->h:Z

    return-void
.end method

.method public setDrawerDraggableState(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->k:Z

    return-void
.end method

.method public setDrawerLayoutAnimationListener(Lcom/twitter/android/widget/k;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->p:Lcom/twitter/android/widget/k;

    return-void
.end method

.method public setFullScreenTop(I)V
    .locals 0

    iput p1, p0, Lcom/twitter/android/widget/DraggableDrawerLayout;->B:I

    return-void
.end method
