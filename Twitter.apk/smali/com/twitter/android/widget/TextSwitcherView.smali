.class public Lcom/twitter/android/widget/TextSwitcherView;
.super Landroid/widget/TextSwitcher;
.source "Twttr"


# instance fields
.field private a:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/TextSwitcher;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/TextSwitcher;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/TextSwitcherView;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/TextSwitcherView;->a:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TextSwitcherView;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/widget/TextSwitcherView;->a:Ljava/lang/Runnable;

    return-void
.end method

.method public a(Ljava/lang/Runnable;J)V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/widget/TextSwitcherView;->a()V

    iput-object p1, p0, Lcom/twitter/android/widget/TextSwitcherView;->a:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/twitter/android/widget/TextSwitcherView;->a:Ljava/lang/Runnable;

    invoke-virtual {p0, v0, p2, p3}, Lcom/twitter/android/widget/TextSwitcherView;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Landroid/widget/TextSwitcher;->onDetachedFromWindow()V

    invoke-virtual {p0}, Lcom/twitter/android/widget/TextSwitcherView;->a()V

    return-void
.end method
