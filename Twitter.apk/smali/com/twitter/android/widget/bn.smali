.class Lcom/twitter/android/widget/bn;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/content/res/Resources;

.field final synthetic b:Lcom/twitter/android/widget/PoiItemView;

.field final synthetic c:Lcom/twitter/android/widget/bm;


# direct methods
.method constructor <init>(Lcom/twitter/android/widget/bm;Landroid/content/res/Resources;Lcom/twitter/android/widget/PoiItemView;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/bn;->c:Lcom/twitter/android/widget/bm;

    iput-object p2, p0, Lcom/twitter/android/widget/bn;->a:Landroid/content/res/Resources;

    iput-object p3, p0, Lcom/twitter/android/widget/bn;->b:Lcom/twitter/android/widget/PoiItemView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/widget/bn;->c:Lcom/twitter/android/widget/bm;

    iget-object v0, v0, Lcom/twitter/android/widget/bm;->c:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v0}, Lcom/twitter/android/widget/PoiFragment;->h(Lcom/twitter/android/widget/PoiFragment;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/widget/bn;->a:Landroid/content/res/Resources;

    const v2, 0x7f0b0022    # com.twitter.android.R.color.clear

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/bn;->b:Lcom/twitter/android/widget/PoiItemView;

    const v1, 0x7f0202bc    # com.twitter.android.R.drawable.list_row_background

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PoiItemView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/bn;->c:Lcom/twitter/android/widget/bm;

    iget-object v0, v0, Lcom/twitter/android/widget/bm;->c:Lcom/twitter/android/widget/PoiFragment;

    iget-object v1, p0, Lcom/twitter/android/widget/bn;->c:Lcom/twitter/android/widget/bm;

    iget-object v1, v1, Lcom/twitter/android/widget/bm;->c:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v1}, Lcom/twitter/android/widget/PoiFragment;->h(Lcom/twitter/android/widget/PoiFragment;)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/widget/bn;->c:Lcom/twitter/android/widget/bm;

    iget-object v2, v2, Lcom/twitter/android/widget/bm;->a:Lcom/twitter/library/api/geo/TwitterPlace;

    iget-object v3, p0, Lcom/twitter/android/widget/bn;->c:Lcom/twitter/android/widget/bm;

    iget v3, v3, Lcom/twitter/android/widget/bm;->b:I

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/widget/PoiFragment;->a(Lcom/twitter/android/widget/PoiFragment;Landroid/view/View;Lcom/twitter/library/api/geo/TwitterPlace;I)V

    return-void
.end method
