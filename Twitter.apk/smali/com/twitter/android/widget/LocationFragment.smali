.class public Lcom/twitter/android/widget/LocationFragment;
.super Landroid/support/v4/app/Fragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/platform/i;


# static fields
.field public static final a:Z


# instance fields
.field private b:Lcom/google/android/gms/maps/c;

.field private c:Z

.field private d:Landroid/location/Location;

.field private e:Lcom/google/android/gms/maps/SupportMapFragment;

.field private f:Lcom/twitter/android/widget/ap;

.field private g:Lcom/twitter/library/platform/LocationProducer;

.field private h:Landroid/widget/Button;

.field private i:Lcom/google/android/gms/maps/model/k;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/twitter/library/client/App;->n()Z

    move-result v0

    sput-boolean v0, Lcom/twitter/android/widget/LocationFragment;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method public static a(Z)Lcom/twitter/android/widget/LocationFragment;
    .locals 3

    new-instance v0, Lcom/twitter/android/widget/LocationFragment;

    invoke-direct {v0}, Lcom/twitter/android/widget/LocationFragment;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/os/Bundle;-><init>(I)V

    const-string/jumbo v2, "enabled"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/LocationFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/widget/LocationFragment;Landroid/location/Location;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/widget/LocationFragment;->b(Landroid/location/Location;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/widget/LocationFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/widget/LocationFragment;->c:Z

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/widget/LocationFragment;)Landroid/location/Location;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/LocationFragment;->d:Landroid/location/Location;

    return-object v0
.end method

.method private b()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/LocationFragment;->b:Lcom/google/android/gms/maps/c;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/LocationFragment;->e:Lcom/google/android/gms/maps/SupportMapFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/LocationFragment;->e:Lcom/google/android/gms/maps/SupportMapFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/SupportMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/LocationFragment;->b:Lcom/google/android/gms/maps/c;

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/LocationFragment;->b:Lcom/google/android/gms/maps/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/LocationFragment;->b:Lcom/google/android/gms/maps/c;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->d()Lcom/google/android/gms/maps/u;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/u;->g(Z)V

    iget-object v0, p0, Lcom/twitter/android/widget/LocationFragment;->b:Lcom/google/android/gms/maps/c;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->d()Lcom/google/android/gms/maps/u;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/u;->b(Z)V

    iget-object v0, p0, Lcom/twitter/android/widget/LocationFragment;->b:Lcom/google/android/gms/maps/c;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->d()Lcom/google/android/gms/maps/u;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/u;->c(Z)V

    iget-object v0, p0, Lcom/twitter/android/widget/LocationFragment;->b:Lcom/google/android/gms/maps/c;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->d()Lcom/google/android/gms/maps/u;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/u;->a(Z)V

    :cond_1
    return-void
.end method

.method private b(Landroid/location/Location;)V
    .locals 5

    iput-object p1, p0, Lcom/twitter/android/widget/LocationFragment;->d:Landroid/location/Location;

    invoke-direct {p0}, Lcom/twitter/android/widget/LocationFragment;->b()V

    iget-object v0, p0, Lcom/twitter/android/widget/LocationFragment;->b:Lcom/google/android/gms/maps/c;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    new-instance v1, Lcom/google/android/gms/maps/model/h;

    invoke-direct {v1}, Lcom/google/android/gms/maps/model/h;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/maps/model/h;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/h;

    iget-object v1, p0, Lcom/twitter/android/widget/LocationFragment;->b:Lcom/google/android/gms/maps/c;

    const/high16 v2, 0x41700000    # 15.0f

    invoke-static {v0, v2}, Lcom/google/android/gms/maps/b;->a(Lcom/google/android/gms/maps/model/LatLng;F)Lcom/google/android/gms/maps/a;

    move-result-object v2

    const/16 v3, 0x3e8

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/a;ILcom/google/android/gms/maps/h;)V

    iget-object v1, p0, Lcom/twitter/android/widget/LocationFragment;->i:Lcom/google/android/gms/maps/model/k;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/widget/LocationFragment;->i:Lcom/google/android/gms/maps/model/k;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/k;->a()V

    :cond_1
    new-instance v1, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Z)Lcom/google/android/gms/maps/model/MarkerOptions;

    iget-object v0, p0, Lcom/twitter/android/widget/LocationFragment;->b:Lcom/google/android/gms/maps/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/k;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/LocationFragment;->i:Lcom/google/android/gms/maps/model/k;

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3

    const v2, 0x7f09018e    # com.twitter.android.R.id.map_container

    sget-boolean v0, Lcom/twitter/android/widget/LocationFragment;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/LocationFragment;->e:Lcom/google/android/gms/maps/SupportMapFragment;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/widget/LocationFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/SupportMapFragment;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/maps/SupportMapFragment;

    invoke-direct {v0}, Lcom/google/android/gms/maps/SupportMapFragment;-><init>()V

    invoke-direct {p0}, Lcom/twitter/android/widget/LocationFragment;->b()V

    invoke-virtual {p0}, Lcom/twitter/android/widget/LocationFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_0
    iput-object v0, p0, Lcom/twitter/android/widget/LocationFragment;->e:Lcom/google/android/gms/maps/SupportMapFragment;

    iget-object v0, p0, Lcom/twitter/android/widget/LocationFragment;->d:Landroid/location/Location;

    if-eqz v0, :cond_1

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/twitter/android/widget/ao;

    invoke-direct {v1, p0}, Lcom/twitter/android/widget/ao;-><init>(Lcom/twitter/android/widget/LocationFragment;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    return-void
.end method

.method public a(Landroid/location/Location;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/twitter/android/widget/LocationFragment;->b(Landroid/location/Location;)V

    iget-object v0, p0, Lcom/twitter/android/widget/LocationFragment;->f:Lcom/twitter/android/widget/ap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/LocationFragment;->f:Lcom/twitter/android/widget/ap;

    invoke-interface {v0, p1}, Lcom/twitter/android/widget/ap;->a(Landroid/location/Location;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/android/widget/ap;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/LocationFragment;->f:Lcom/twitter/android/widget/ap;

    return-void
.end method

.method public b(Z)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/widget/LocationFragment;->g:Lcom/twitter/library/platform/LocationProducer;

    invoke-virtual {v0}, Lcom/twitter/library/platform/LocationProducer;->e()Z

    move-result v0

    if-eqz p1, :cond_1

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/twitter/android/widget/LocationFragment;->c:Z

    iget-object v1, p0, Lcom/twitter/android/widget/LocationFragment;->h:Landroid/widget/Button;

    const v2, 0x7f0f04ce    # com.twitter.android.R.string.turn_off_location

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/widget/LocationFragment;->f:Lcom/twitter/android/widget/ap;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/widget/LocationFragment;->f:Lcom/twitter/android/widget/ap;

    iget-boolean v2, p0, Lcom/twitter/android/widget/LocationFragment;->c:Z

    invoke-interface {v1, v2}, Lcom/twitter/android/widget/ap;->a(Z)V

    :cond_0
    if-eqz p1, :cond_2

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/widget/LocationFragment;->g:Lcom/twitter/library/platform/LocationProducer;

    invoke-virtual {v0, p0}, Lcom/twitter/library/platform/LocationProducer;->a(Lcom/twitter/library/platform/i;)V

    :goto_1
    return-void

    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/twitter/android/widget/LocationFragment;->c:Z

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/twitter/android/widget/LocationFragment;->d:Landroid/location/Location;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/widget/LocationFragment;->g:Lcom/twitter/library/platform/LocationProducer;

    invoke-virtual {v0, p0}, Lcom/twitter/library/platform/LocationProducer;->b(Lcom/twitter/library/platform/i;)V

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/LocationFragment;->setRetainInstance(Z)V

    if-eqz p1, :cond_0

    const-string/jumbo v0, "location"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    iput-object v0, p0, Lcom/twitter/android/widget/LocationFragment;->d:Landroid/location/Location;

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/widget/LocationFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "enabled"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/widget/LocationFragment;->c:Z

    invoke-virtual {p0}, Lcom/twitter/android/widget/LocationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/platform/LocationProducer;->a(Landroid/content/Context;)Lcom/twitter/library/platform/LocationProducer;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/LocationFragment;->g:Lcom/twitter/library/platform/LocationProducer;

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    const v0, 0x7f0300ab    # com.twitter.android.R.layout.location_fragment

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0901b7    # com.twitter.android.R.id.toggle_location

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/widget/LocationFragment;->h:Landroid/widget/Button;

    iget-object v0, p0, Lcom/twitter/android/widget/LocationFragment;->h:Landroid/widget/Button;

    new-instance v2, Lcom/twitter/android/widget/an;

    invoke-direct {v2, p0}, Lcom/twitter/android/widget/an;-><init>(Lcom/twitter/android/widget/LocationFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-boolean v0, p0, Lcom/twitter/android/widget/LocationFragment;->c:Z

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/LocationFragment;->b(Z)V

    return-object v1
.end method

.method public onDestroy()V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/widget/LocationFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f09018e    # com.twitter.android.R.id.map_container

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/SupportMapFragment;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/widget/LocationFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/LocationFragment;->g:Lcom/twitter/library/platform/LocationProducer;

    invoke-virtual {v0, p0}, Lcom/twitter/library/platform/LocationProducer;->b(Lcom/twitter/library/platform/i;)V

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    iget-boolean v0, p0, Lcom/twitter/android/widget/LocationFragment;->c:Z

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/LocationFragment;->b(Z)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string/jumbo v0, "location"

    iget-object v1, p0, Lcom/twitter/android/widget/LocationFragment;->d:Landroid/location/Location;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method
