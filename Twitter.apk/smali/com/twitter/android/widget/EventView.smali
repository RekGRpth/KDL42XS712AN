.class public Lcom/twitter/android/widget/EventView;
.super Lcom/twitter/android/widget/TopicView;
.source "Twttr"


# instance fields
.field a:Landroid/widget/ImageView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/LinearLayout;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:I

.field private j:I

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Lcom/twitter/library/util/m;

.field private n:Lcom/twitter/library/widget/ap;

.field private o:Landroid/graphics/drawable/Drawable;

.field private p:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/widget/TopicView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/widget/TopicView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/widget/TopicView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/widget/EventView;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/EventView;->p:Landroid/content/Context;

    return-object v0
.end method

.method protected static a(Ljava/lang/String;ILandroid/content/res/Resources;)Ljava/lang/String;
    .locals 4

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    if-lez p1, :cond_1

    const v0, 0x7f0f015e    # com.twitter.android.R.string.event_card_popularity

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2, p1}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    new-instance v0, Lcom/twitter/library/scribe/ScribeItem;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeItem;-><init>()V

    iput-object p1, v0, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    const/16 v1, 0x10

    iput v1, v0, Lcom/twitter/library/scribe/ScribeItem;->c:I

    iput p5, v0, Lcom/twitter/library/scribe/ScribeItem;->y:I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "search"

    aput-object v3, v2, v6

    invoke-static {p2}, Lcom/twitter/library/api/TwitterTopic;->c(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    const/4 v3, 0x2

    const-string/jumbo v4, "seeit_button"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const/4 v4, 0x0

    aput-object v4, v2, v3

    const/4 v3, 0x4

    aput-object p4, v2, v3

    invoke-static {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v4, v7, [Ljava/lang/String;

    aput-object v1, v4, v6

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/twitter/library/scribe/ScribeLog;->f(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method


# virtual methods
.method protected a(Landroid/widget/ImageView;Ljava/lang/String;Lcom/twitter/library/util/m;Lcom/twitter/library/widget/ap;)Lcom/twitter/library/util/m;
    .locals 3

    if-eqz p1, :cond_1

    new-instance v0, Lcom/twitter/library/util/m;

    invoke-virtual {p1}, Landroid/widget/ImageView;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/widget/ImageView;->getHeight()I

    move-result v2

    invoke-direct {v0, p2, v1, v2}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;II)V

    invoke-virtual {v0, p3}, Lcom/twitter/library/util/m;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    invoke-interface {p4, v0}, Lcom/twitter/library/widget/ap;->a(Lcom/twitter/library/util/m;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    move-object p3, v0

    :cond_1
    return-object p3
.end method

.method protected a()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/widget/EventView;->a:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/twitter/android/widget/EventView;->l:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/widget/EventView;->m:Lcom/twitter/library/util/m;

    iget-object v3, p0, Lcom/twitter/android/widget/EventView;->n:Lcom/twitter/library/widget/ap;

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/twitter/android/widget/EventView;->a(Landroid/widget/ImageView;Ljava/lang/String;Lcom/twitter/library/util/m;Lcom/twitter/library/widget/ap;)Lcom/twitter/library/util/m;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/EventView;->m:Lcom/twitter/library/util/m;

    return-void
.end method

.method protected a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    iput-object p1, p0, Lcom/twitter/android/widget/EventView;->p:Landroid/content/Context;

    if-eqz p2, :cond_0

    sget-object v0, Lcom/twitter/android/rg;->EventView:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/widget/EventView;->o:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    :cond_0
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;Landroid/widget/ImageView;Z)V
    .locals 3

    if-eqz p1, :cond_2

    if-eqz p3, :cond_1

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/twitter/android/widget/EventView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/twitter/android/widget/EventView;->o:Landroid/graphics/drawable/Drawable;

    aput-object v2, v1, v0

    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v0, v1}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p2}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    const v0, 0x7f0200d8    # com.twitter.android.R.drawable.event_placeholder

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLcom/twitter/library/widget/ap;Ljava/lang/String;Ljava/lang/String;[BZZLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    invoke-super/range {p0 .. p20}, Lcom/twitter/android/widget/TopicView;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLcom/twitter/library/widget/ap;Ljava/lang/String;Ljava/lang/String;[BZZLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/twitter/android/widget/EventView;->g:Ljava/lang/String;

    iput p2, p0, Lcom/twitter/android/widget/EventView;->i:I

    iput-object p3, p0, Lcom/twitter/android/widget/EventView;->l:Ljava/lang/String;

    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/twitter/android/widget/EventView;->n:Lcom/twitter/library/widget/ap;

    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/twitter/android/widget/EventView;->k:Ljava/lang/String;

    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/twitter/android/widget/EventView;->h:Ljava/lang/String;

    move/from16 v0, p8

    iput v0, p0, Lcom/twitter/android/widget/EventView;->j:I

    invoke-virtual {p0}, Lcom/twitter/android/widget/EventView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move-object/from16 v0, p7

    move/from16 v1, p8

    invoke-static {v0, v1, v2}, Lcom/twitter/android/widget/EventView;->a(Ljava/lang/String;ILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/widget/EventView;->b:Landroid/widget/TextView;

    invoke-static {v4, p4}, Lcom/twitter/android/widget/EventView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/twitter/android/widget/EventView;->c:Landroid/widget/TextView;

    invoke-static {v4, p5}, Lcom/twitter/android/widget/EventView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/twitter/android/widget/EventView;->d:Landroid/widget/TextView;

    invoke-static {v4, v3}, Lcom/twitter/android/widget/EventView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/twitter/android/widget/EventView;->a:Landroid/widget/ImageView;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/twitter/android/widget/EventView;->a:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getWidth()I

    move-result v3

    if-lez v3, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/widget/EventView;->a()V

    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/twitter/android/widget/EventView;->e:Landroid/widget/TextView;

    if-eqz v3, :cond_1

    if-eqz p15, :cond_5

    if-eqz p4, :cond_4

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x14

    if-gt v3, v4, :cond_4

    const v3, 0x7f0f0160    # com.twitter.android.R.string.event_footer_format

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p4, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    iget-object v3, p0, Lcom/twitter/android/widget/EventView;->e:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/twitter/android/widget/EventView;->e:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_1
    :goto_2
    invoke-static {}, Lcom/twitter/library/featureswitch/a;->p()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/twitter/android/widget/EventView;->f:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_2

    invoke-static/range {p13 .. p13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/twitter/android/widget/EventView;->f:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v9, p0, Lcom/twitter/android/widget/EventView;->f:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/twitter/android/widget/s;

    move-object v3, p0

    move-object v4, p1

    move v5, p2

    move-object/from16 v6, p12

    move/from16 v7, p8

    move-object/from16 v8, p13

    invoke-direct/range {v2 .. v8}, Lcom/twitter/android/widget/s;-><init>(Lcom/twitter/android/widget/EventView;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)V

    invoke-virtual {v9, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    return-void

    :cond_3
    iget-object v3, p0, Lcom/twitter/android/widget/EventView;->a:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_4
    const v3, 0x7f0f015f    # com.twitter.android.R.string.event_footer_default_text

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lcom/twitter/android/widget/EventView;->e:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I[B)V
    .locals 2

    invoke-super/range {p0 .. p5}, Lcom/twitter/android/widget/TopicView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I[B)V

    invoke-virtual {p0}, Lcom/twitter/android/widget/EventView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p3, p4, v0}, Lcom/twitter/android/widget/EventView;->a(Ljava/lang/String;ILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/widget/EventView;->b:Landroid/widget/TextView;

    invoke-static {v1, p1}, Lcom/twitter/android/widget/EventView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/widget/EventView;->c:Landroid/widget/TextView;

    invoke-static {v1, p2}, Lcom/twitter/android/widget/EventView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/widget/EventView;->d:Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/twitter/android/widget/EventView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    return-void
.end method

.method public getImageKey()Lcom/twitter/library/util/m;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/EventView;->m:Lcom/twitter/library/util/m;

    return-object v0
.end method

.method public getSeedHashtag()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/EventView;->k:Ljava/lang/String;

    return-object v0
.end method

.method public getTopicId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/EventView;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getTopicType()I
    .locals 1

    iget v0, p0, Lcom/twitter/android/widget/EventView;->i:I

    return v0
.end method

.method public getTweetCount()I
    .locals 1

    iget v0, p0, Lcom/twitter/android/widget/EventView;->j:I

    return v0
.end method

.method public getViewUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/EventView;->h:Ljava/lang/String;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/widget/TopicView;->onFinishInflate()V

    const v0, 0x7f09015c    # com.twitter.android.R.id.event_image

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/EventView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/widget/EventView;->a:Landroid/widget/ImageView;

    const v0, 0x7f090168    # com.twitter.android.R.id.event_title

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/EventView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/EventView;->b:Landroid/widget/TextView;

    const v0, 0x7f090169    # com.twitter.android.R.id.event_subtitle

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/EventView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/EventView;->c:Landroid/widget/TextView;

    const v0, 0x7f09016a    # com.twitter.android.R.id.event_popularity

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/EventView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/EventView;->d:Landroid/widget/TextView;

    const v0, 0x7f090163    # com.twitter.android.R.id.event_footer

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/EventView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/EventView;->e:Landroid/widget/TextView;

    const v0, 0x7f090167    # com.twitter.android.R.id.see_it_layout

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/EventView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/twitter/android/widget/EventView;->f:Landroid/widget/LinearLayout;

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    invoke-super/range {p0 .. p5}, Lcom/twitter/android/widget/TopicView;->onLayout(ZIIII)V

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/widget/EventView;->a()V

    :cond_0
    return-void
.end method

.method public setCustomTopicDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/EventView;->a:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/EventView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method protected setEventImage(Landroid/graphics/Bitmap;)V
    .locals 2

    iget-object v1, p0, Lcom/twitter/android/widget/EventView;->a:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/twitter/android/widget/EventView;->o:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, p1, v1, v0}, Lcom/twitter/android/widget/EventView;->a(Landroid/graphics/Bitmap;Landroid/widget/ImageView;Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setEventImages(Ljava/util/HashMap;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/EventView;->m:Lcom/twitter/library/util/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/EventView;->m:Lcom/twitter/library/util/m;

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/EventView;->m:Lcom/twitter/library/util/m;

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ae;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/twitter/library/util/ae;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, v0, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/EventView;->setEventImage(Landroid/graphics/Bitmap;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/EventView;->setEventImage(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method
