.class public Lcom/twitter/android/widget/t;
.super Landroid/support/v4/widget/CursorAdapter;
.source "Twttr"

# interfaces
.implements Landroid/widget/AbsListView$RecyclerListener;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/ArrayList;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/twitter/android/MemoryImageCache;

.field private final e:Ljava/util/Set;

.field private final f:Landroid/support/v4/util/SimpleArrayMap;

.field private final g:Z

.field private final h:Lcom/twitter/library/util/y;

.field private i:I

.field private j:Z

.field private k:Landroid/os/Handler;

.field private l:Z

.field private m:Lcom/twitter/android/widget/x;

.field private n:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "bucket_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "orientation"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/android/widget/t;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/android/MemoryImageCache;IZ)V
    .locals 7

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/t;->b:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/t;->e:Ljava/util/Set;

    new-instance v0, Landroid/support/v4/util/SimpleArrayMap;

    invoke-direct {v0}, Landroid/support/v4/util/SimpleArrayMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/t;->f:Landroid/support/v4/util/SimpleArrayMap;

    iput-boolean v1, p0, Lcom/twitter/android/widget/t;->l:Z

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/t;->c:Landroid/content/Context;

    iput p3, p0, Lcom/twitter/android/widget/t;->i:I

    iput-boolean p4, p0, Lcom/twitter/android/widget/t;->g:Z

    iput-object p2, p0, Lcom/twitter/android/widget/t;->d:Lcom/twitter/android/MemoryImageCache;

    new-instance v0, Lcom/twitter/library/util/y;

    const/4 v2, 0x2

    const-wide/16 v3, 0x8

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v6, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v6}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/util/y;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    iput-object v0, p0, Lcom/twitter/android/widget/t;->h:Lcom/twitter/library/util/y;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/android/widget/t;->k:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/widget/t;)Lcom/twitter/android/widget/x;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/t;->m:Lcom/twitter/android/widget/x;

    return-object v0
.end method

.method private a(Lcom/twitter/android/widget/w;)V
    .locals 1

    invoke-virtual {p1}, Lcom/twitter/android/widget/w;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/twitter/android/widget/w;->a(Z)V

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p1, Lcom/twitter/android/widget/w;->c:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/widget/t;->n:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/twitter/android/widget/w;->a(Z)V

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lcom/twitter/android/widget/t;->l:Z

    invoke-virtual {p1, v0}, Lcom/twitter/android/widget/w;->a(Z)V

    goto :goto_0
.end method

.method public static b(Landroid/view/View;)Landroid/net/Uri;
    .locals 2

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/twitter/android/widget/w;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/twitter/android/widget/w;

    iget-object v0, v0, Lcom/twitter/android/widget/w;->b:Landroid/net/Uri;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/twitter/android/widget/w;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/t;->e:Ljava/util/Set;

    iget-object v1, p1, Lcom/twitter/android/widget/w;->b:Landroid/net/Uri;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/android/widget/w;->c()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/twitter/android/widget/w;->d()V

    goto :goto_0
.end method

.method private c(Landroid/net/Uri;)Lcom/twitter/android/widget/w;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/t;->f:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v0, p1}, Landroid/support/v4/util/SimpleArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_1

    move-object v0, v1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/w;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/t;->f:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v0, p1}, Landroid/support/v4/util/SimpleArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    goto :goto_0
.end method

.method public static c(Landroid/view/View;)Z
    .locals 2

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/twitter/android/widget/w;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/twitter/android/widget/w;

    invoke-virtual {v0}, Lcom/twitter/android/widget/w;->b()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 3

    iget-object v2, p0, Lcom/twitter/android/widget/t;->f:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v2}, Landroid/support/v4/util/SimpleArrayMap;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    invoke-virtual {v2, v1}, Landroid/support/v4/util/SimpleArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/w;

    if-nez v0, :cond_0

    invoke-virtual {v2, v1}, Landroid/support/v4/util/SimpleArrayMap;->removeAt(I)Ljava/lang/Object;

    :goto_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-direct {p0, v0}, Lcom/twitter/android/widget/t;->b(Lcom/twitter/android/widget/w;)V

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/t;->a(Lcom/twitter/android/widget/w;)V

    goto :goto_1

    :cond_1
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Landroid/support/v4/content/CursorLoader;
    .locals 7

    new-instance v0, Landroid/support/v4/content/CursorLoader;

    iget-object v1, p0, Lcom/twitter/android/widget/t;->c:Landroid/content/Context;

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v3, Lcom/twitter/android/widget/t;->a:[Ljava/lang/String;

    const/4 v4, 0x2

    new-array v5, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v6, ""

    aput-object v6, v5, v4

    const/4 v4, 0x1

    const-string/jumbo v6, "0"

    aput-object v6, v5, v4

    const-string/jumbo v6, "datetaken DESC"

    move-object v4, p1

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/t;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    invoke-direct {p0}, Lcom/twitter/android/widget/t;->d()V

    return-void
.end method

.method public a(I)V
    .locals 1

    iget v0, p0, Lcom/twitter/android/widget/t;->i:I

    if-eq p1, v0, :cond_0

    iput p1, p0, Lcom/twitter/android/widget/t;->i:I

    invoke-virtual {p0}, Lcom/twitter/android/widget/t;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public a(Landroid/net/Uri;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/t;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, p1}, Lcom/twitter/android/widget/t;->c(Landroid/net/Uri;)Lcom/twitter/android/widget/w;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/t;->b(Lcom/twitter/android/widget/w;)V

    :cond_0
    return-void
.end method

.method a(Landroid/net/Uri;Landroid/graphics/Bitmap;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/twitter/android/widget/t;->c(Landroid/net/Uri;)Lcom/twitter/android/widget/w;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {v0, p2}, Lcom/twitter/android/widget/w;->a(Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_1
    const v1, 0x7f020285    # com.twitter.android.R.drawable.ic_tweet_placeholder_photo_dark_error

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/w;->a(I)V

    goto :goto_0
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/t;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Lcom/twitter/android/widget/x;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/t;->m:Lcom/twitter/android/widget/x;

    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/widget/t;->l:Z

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/twitter/android/widget/t;->l:Z

    invoke-direct {p0}, Lcom/twitter/android/widget/t;->d()V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/t;->h:Lcom/twitter/library/util/y;

    invoke-virtual {v0}, Lcom/twitter/library/util/y;->shutdownNow()Ljava/util/List;

    return-void
.end method

.method public b(Landroid/net/Uri;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/t;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    invoke-direct {p0, p1}, Lcom/twitter/android/widget/t;->c(Landroid/net/Uri;)Lcom/twitter/android/widget/w;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/t;->b(Lcom/twitter/android/widget/w;)V

    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/widget/t;->n:Z

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/twitter/android/widget/t;->n:Z

    invoke-direct {p0}, Lcom/twitter/android/widget/t;->d()V

    goto :goto_0
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 16

    move-object/from16 v0, p1

    instance-of v1, v0, Lcom/twitter/android/widget/BadgedImageView;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v1, p1

    check-cast v1, Lcom/twitter/android/widget/BadgedImageView;

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    move-object v15, v2

    check-cast v15, Lcom/twitter/android/widget/w;

    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const/4 v2, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v2, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    const/4 v2, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/t;->f:Landroid/support/v4/util/SimpleArrayMap;

    iget-object v10, v15, Lcom/twitter/android/widget/w;->b:Landroid/net/Uri;

    invoke-virtual {v2, v10}, Landroid/support/v4/util/SimpleArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v10

    if-eq v10, v15, :cond_8

    :cond_2
    new-instance v14, Ljava/lang/ref/WeakReference;

    invoke-direct {v14, v15}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    :goto_1
    iget-object v2, v15, Lcom/twitter/android/widget/w;->d:Ljava/util/concurrent/Future;

    if-eqz v2, :cond_3

    iget-object v2, v15, Lcom/twitter/android/widget/w;->d:Ljava/util/concurrent/Future;

    const/4 v10, 0x0

    invoke-interface {v2, v10}, Ljava/util/concurrent/Future;->cancel(Z)Z

    const/4 v2, 0x0

    iput-object v2, v15, Lcom/twitter/android/widget/w;->d:Ljava/util/concurrent/Future;

    :cond_3
    iput-wide v3, v15, Lcom/twitter/android/widget/w;->a:J

    iput-object v8, v15, Lcom/twitter/android/widget/w;->b:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/t;->f:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v2, v8, v14}, Landroid/support/v4/util/SimpleArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->as()Z

    move-result v2

    if-eqz v2, :cond_4

    const-string/jumbo v2, "image/gif"

    invoke-static {v9}, Lcom/twitter/library/util/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_2
    iput-boolean v2, v15, Lcom/twitter/android/widget/w;->c:Z

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/twitter/android/widget/t;->b(Lcom/twitter/android/widget/w;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/twitter/android/widget/t;->a(Lcom/twitter/android/widget/w;)V

    if-eqz v2, :cond_5

    const v2, 0x7f0201c0    # com.twitter.android.R.drawable.ic_gif_badge

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/BadgedImageView;->setBadge(I)V

    :goto_3
    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/widget/t;->d:Lcom/twitter/android/MemoryImageCache;

    if-eqz v9, :cond_6

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/widget/t;->d:Lcom/twitter/android/MemoryImageCache;

    invoke-virtual {v9, v2}, Lcom/twitter/android/MemoryImageCache;->a(Ljava/lang/String;)Lcom/twitter/android/lu;

    move-result-object v2

    :goto_4
    if-eqz v2, :cond_7

    iget-object v2, v2, Lcom/twitter/android/lu;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/BadgedImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    :cond_5
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/BadgedImageView;->setBadge(I)V

    goto :goto_3

    :cond_6
    const/4 v2, 0x0

    goto :goto_4

    :cond_7
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/BadgedImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    if-eqz v8, :cond_0

    new-instance v11, Lcom/twitter/android/widget/y;

    move-object/from16 v0, p0

    invoke-direct {v11, v0, v8}, Lcom/twitter/android/widget/y;-><init>(Lcom/twitter/android/widget/t;Landroid/net/Uri;)V

    new-instance v1, Lcom/twitter/android/widget/v;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/twitter/android/widget/t;->i:I

    move-object/from16 v0, p0

    iget v10, v0, Lcom/twitter/android/widget/t;->i:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/twitter/android/widget/t;->k:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/twitter/android/widget/t;->d:Lcom/twitter/android/MemoryImageCache;

    move-object/from16 v2, p2

    invoke-direct/range {v1 .. v14}, Lcom/twitter/android/widget/v;-><init>(Landroid/content/Context;JJILandroid/net/Uri;IILcom/twitter/android/widget/y;Landroid/os/Handler;Lcom/twitter/android/MemoryImageCache;Ljava/lang/ref/WeakReference;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/t;->h:Lcom/twitter/library/util/y;

    invoke-virtual {v2, v1}, Lcom/twitter/library/util/y;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v1

    iput-object v1, v15, Lcom/twitter/android/widget/w;->d:Ljava/util/concurrent/Future;

    goto/16 :goto_0

    :cond_8
    move-object v14, v2

    goto/16 :goto_1
.end method

.method public c()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/t;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method c(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/widget/t;->j:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/twitter/android/widget/t;->j:Z

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/t;->h:Lcom/twitter/library/util/y;

    invoke-virtual {v0}, Lcom/twitter/library/util/y;->a()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/t;->h:Lcom/twitter/library/util/y;

    invoke-virtual {v0}, Lcom/twitter/library/util/y;->b()V

    goto :goto_0
.end method

.method public getCount()I
    .locals 2

    iget v0, p0, Lcom/twitter/android/widget/t;->i:I

    if-eqz v0, :cond_0

    invoke-super {p0}, Landroid/support/v4/widget/CursorAdapter;->getCount()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/widget/t;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/t;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/t;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/t;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/t;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sub-int v0, p1, v0

    instance-of v1, p2, Lcom/twitter/android/widget/BadgedImageView;

    if-eqz v1, :cond_1

    :goto_1
    invoke-super {p0, v0, p2, p3}, Landroid/support/v4/widget/CursorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    goto :goto_1
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/t;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03007f    # com.twitter.android.R.layout.gallery_image

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/BadgedImageView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/BadgedImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v2, p0, Lcom/twitter/android/widget/t;->i:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v2, p0, Lcom/twitter/android/widget/t;->i:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    new-instance v1, Lcom/twitter/android/widget/w;

    invoke-direct {v1, v0}, Lcom/twitter/android/widget/w;-><init>(Lcom/twitter/android/widget/BadgedImageView;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/BadgedImageView;->setTag(Ljava/lang/Object;)V

    iget-boolean v2, p0, Lcom/twitter/android/widget/t;->g:Z

    if-eqz v2, :cond_0

    const v2, 0x7f09018c    # com.twitter.android.R.id.gallery_image_expand

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/BadgedImageView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    new-instance v3, Lcom/twitter/android/widget/u;

    invoke-direct {v3, p0, v1}, Lcom/twitter/android/widget/u;-><init>(Lcom/twitter/android/widget/t;Lcom/twitter/android/widget/w;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-object v0
.end method

.method public onMovedToScrapHeap(Landroid/view/View;)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/w;

    iget-object v1, p0, Lcom/twitter/android/widget/t;->f:Landroid/support/v4/util/SimpleArrayMap;

    iget-object v2, v0, Lcom/twitter/android/widget/w;->b:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/support/v4/util/SimpleArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v3, v0, Lcom/twitter/android/widget/w;->b:Landroid/net/Uri;

    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lcom/twitter/android/widget/w;->a:J

    iget-object v1, v0, Lcom/twitter/android/widget/w;->d:Ljava/util/concurrent/Future;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/twitter/android/widget/w;->d:Ljava/util/concurrent/Future;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    iput-object v3, v0, Lcom/twitter/android/widget/w;->d:Ljava/util/concurrent/Future;

    :cond_0
    check-cast p1, Lcom/twitter/android/widget/BadgedImageView;

    invoke-virtual {p1, v3}, Lcom/twitter/android/widget/BadgedImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method
