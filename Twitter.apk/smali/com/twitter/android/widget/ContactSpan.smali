.class public Lcom/twitter/android/widget/ContactSpan;
.super Landroid/text/style/StyleSpan;
.source "Twttr"


# instance fields
.field private a:Lcom/twitter/library/api/TwitterContact;


# direct methods
.method public constructor <init>(Lcom/twitter/library/api/TwitterContact;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Landroid/text/style/StyleSpan;-><init>(I)V

    iput-object p1, p0, Lcom/twitter/android/widget/ContactSpan;->a:Lcom/twitter/library/api/TwitterContact;

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/library/api/TwitterContact;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/ContactSpan;->a:Lcom/twitter/library/api/TwitterContact;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    if-eqz p1, :cond_1

    instance-of v0, p1, Lcom/twitter/android/widget/ContactSpan;

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/twitter/android/widget/ContactSpan;

    iget-object v0, p0, Lcom/twitter/android/widget/ContactSpan;->a:Lcom/twitter/library/api/TwitterContact;

    iget-object v1, p1, Lcom/twitter/android/widget/ContactSpan;->a:Lcom/twitter/library/api/TwitterContact;

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/TwitterContact;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/ContactSpan;->a:Lcom/twitter/library/api/TwitterContact;

    invoke-virtual {v0}, Lcom/twitter/library/api/TwitterContact;->hashCode()I

    move-result v0

    return v0
.end method
