.class Lcom/twitter/android/widget/dw;
.super Landroid/view/animation/ScaleAnimation;
.source "Twttr"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/widget/dt;


# direct methods
.method private constructor <init>(Lcom/twitter/android/widget/dt;I)V
    .locals 9

    const/4 v5, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/twitter/android/widget/dw;->a:Lcom/twitter/android/widget/dt;

    int-to-float v6, p2

    move-object v0, p0

    move v3, v1

    move v4, v2

    move v7, v5

    move v8, v1

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    const-wide/16 v0, 0x12c

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/widget/dw;->setDuration(J)V

    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/widget/dw;->setStartOffset(J)V

    new-instance v0, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {v0}, Landroid/view/animation/OvershootInterpolator;-><init>()V

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/dw;->setInterpolator(Landroid/view/animation/Interpolator;)V

    invoke-virtual {p0, p0}, Lcom/twitter/android/widget/dw;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    return-void
.end method

.method private constructor <init>(Lcom/twitter/android/widget/dt;Landroid/view/View;)V
    .locals 1

    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/widget/dw;-><init>(Lcom/twitter/android/widget/dt;Landroid/widget/RelativeLayout$LayoutParams;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/widget/dt;Landroid/view/View;Lcom/twitter/android/widget/du;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/widget/dw;-><init>(Lcom/twitter/android/widget/dt;Landroid/view/View;)V

    return-void
.end method

.method private constructor <init>(Lcom/twitter/android/widget/dt;Landroid/widget/RelativeLayout$LayoutParams;)V
    .locals 2

    iget v0, p2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v1, p2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/widget/dw;-><init>(Lcom/twitter/android/widget/dt;I)V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 4

    iget-object v1, p0, Lcom/twitter/android/widget/dw;->a:Lcom/twitter/android/widget/dt;

    const-string/jumbo v2, ""

    const-string/jumbo v3, "impression"

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->aw()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "default_on"

    :goto_0
    invoke-static {v1, v2, v3, v0}, Lcom/twitter/android/widget/dt;->a(Lcom/twitter/android/widget/dt;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const-string/jumbo v0, "default_off"

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/dw;->a:Lcom/twitter/android/widget/dt;

    invoke-static {v0}, Lcom/twitter/android/widget/dt;->c(Lcom/twitter/android/widget/dt;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
