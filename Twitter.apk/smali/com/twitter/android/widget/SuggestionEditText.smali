.class public abstract Lcom/twitter/android/widget/SuggestionEditText;
.super Landroid/widget/EditText;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/Filter$FilterListener;


# static fields
.field public static final a:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

.field public static final b:Landroid/widget/Filterable;

.field private static final e:Landroid/widget/Filter;


# instance fields
.field protected c:I

.field protected d:Landroid/widget/ListView;

.field private final f:Lcom/twitter/android/widget/cq;

.field private g:Lcom/twitter/android/widget/cr;

.field private h:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

.field private i:Landroid/widget/Filter;

.field private j:I

.field private k:J

.field private l:Lcom/twitter/internal/android/widget/e;

.field private m:Lcom/twitter/internal/android/widget/f;

.field private n:Landroid/view/View$OnClickListener;

.field private o:Z

.field private p:Landroid/database/DataSetObserver;

.field private q:Z

.field private r:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/android/widget/cm;

    invoke-direct {v0}, Lcom/twitter/android/widget/cm;-><init>()V

    sput-object v0, Lcom/twitter/android/widget/SuggestionEditText;->a:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    new-instance v0, Lcom/twitter/android/widget/cn;

    invoke-direct {v0}, Lcom/twitter/android/widget/cn;-><init>()V

    sput-object v0, Lcom/twitter/android/widget/SuggestionEditText;->b:Landroid/widget/Filterable;

    new-instance v0, Lcom/twitter/android/widget/co;

    invoke-direct {v0}, Lcom/twitter/android/widget/co;-><init>()V

    sput-object v0, Lcom/twitter/android/widget/SuggestionEditText;->e:Landroid/widget/Filter;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v1, p0, Lcom/twitter/android/widget/SuggestionEditText;->c:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->j:I

    iput-boolean v1, p0, Lcom/twitter/android/widget/SuggestionEditText;->r:Z

    new-instance v0, Lcom/twitter/android/widget/cq;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/twitter/android/widget/cq;-><init>(Landroid/os/Looper;Landroid/widget/Filter$FilterListener;)V

    iput-object v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->f:Lcom/twitter/android/widget/cq;

    return-void
.end method

.method static synthetic h()Landroid/widget/Filter;
    .locals 1

    sget-object v0, Lcom/twitter/android/widget/SuggestionEditText;->e:Landroid/widget/Filter;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/twitter/android/widget/SuggestionEditText;->o:Z

    iget-object v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->f:Lcom/twitter/android/widget/cq;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/cq;->removeMessages(I)V

    iput-boolean v1, p0, Lcom/twitter/android/widget/SuggestionEditText;->q:Z

    return-void
.end method

.method protected a(IILjava/lang/CharSequence;)V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    return-void
.end method

.method public a(Landroid/widget/MultiAutoCompleteTextView$Tokenizer;J)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/SuggestionEditText;->h:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    iput-wide p2, p0, Lcom/twitter/android/widget/SuggestionEditText;->k:J

    return-void
.end method

.method protected a(Ljava/lang/Object;I)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->i:Landroid/widget/Filter;

    if-nez v0, :cond_1

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/twitter/android/widget/SuggestionEditText;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->length()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/widget/SuggestionEditText;->setSelection(II)V

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->g:Lcom/twitter/android/widget/cr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->g:Lcom/twitter/android/widget/cr;

    invoke-interface {v0, p1, p2}, Lcom/twitter/android/widget/cr;->a(Ljava/lang/CharSequence;I)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->getSelectionEnd()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/widget/SuggestionEditText;->h:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v2, v0, v1}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v0

    iget-object v2, p0, Lcom/twitter/android/widget/SuggestionEditText;->h:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    iget-object v3, p0, Lcom/twitter/android/widget/SuggestionEditText;->i:Landroid/widget/Filter;

    invoke-virtual {v3, p1}, Landroid/widget/Filter;->convertResultToString(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->terminateToken(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p0, v0, v1, p1}, Lcom/twitter/android/widget/SuggestionEditText;->a(IILjava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->h:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->i:Landroid/widget/Filter;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->getSelectionEnd()I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/widget/SuggestionEditText;->h:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v3, v2, v0}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v3

    iget-object v4, p0, Lcom/twitter/android/widget/SuggestionEditText;->h:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v4, v2, v0}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenEnd(Ljava/lang/CharSequence;I)I

    move-result v4

    sub-int v0, v4, v3

    iget v5, p0, Lcom/twitter/android/widget/SuggestionEditText;->c:I

    if-ge v0, v5, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->a()V

    goto :goto_0

    :cond_2
    iget-object v5, p0, Lcom/twitter/android/widget/SuggestionEditText;->f:Lcom/twitter/android/widget/cq;

    invoke-virtual {v5, v1}, Landroid/os/Handler;->removeMessages(I)V

    if-eqz p1, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-interface {v2, v3, v4}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v5, v1, v0, v1, v2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/widget/SuggestionEditText;->k:J

    invoke-virtual {v5, v0, v1, v2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method protected a(I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected a(ZI)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->o:Z

    return v0
.end method

.method public c()Z
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->d()V

    iget-object v1, p0, Lcom/twitter/android/widget/SuggestionEditText;->g:Lcom/twitter/android/widget/cr;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/twitter/android/widget/SuggestionEditText;->o:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/widget/SuggestionEditText;->g:Lcom/twitter/android/widget/cr;

    invoke-interface {v1}, Lcom/twitter/android/widget/cr;->o_()V

    :cond_0
    iput-boolean v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->o:Z

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected d()V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->o:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->g:Lcom/twitter/android/widget/cr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->g:Lcom/twitter/android/widget/cr;

    invoke-interface {v0}, Lcom/twitter/android/widget/cr;->o_()V

    :cond_0
    return-void
.end method

.method protected e()V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->q:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->c()Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->q:Z

    :cond_1
    return-void
.end method

.method protected f()V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->a()V

    return-void
.end method

.method protected g()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->getWindowVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Lcom/twitter/android/widget/SuggestionEditText;->d:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getCount()I

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->a()V

    iput-boolean v1, p0, Lcom/twitter/android/widget/SuggestionEditText;->q:Z

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->n:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->n:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 2

    invoke-super {p0, p1}, Landroid/widget/EditText;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/twitter/internal/android/widget/e;

    invoke-direct {v1, v0}, Lcom/twitter/internal/android/widget/e;-><init>(Landroid/view/inputmethod/InputConnection;)V

    iput-object v1, p0, Lcom/twitter/android/widget/SuggestionEditText;->l:Lcom/twitter/internal/android/widget/e;

    iget-object v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->l:Lcom/twitter/internal/android/widget/e;

    iget-object v1, p0, Lcom/twitter/android/widget/SuggestionEditText;->m:Lcom/twitter/internal/android/widget/f;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/e;->a(Lcom/twitter/internal/android/widget/f;)V

    iget-object v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->l:Lcom/twitter/internal/android/widget/e;

    goto :goto_0
.end method

.method public onFilterComplete(I)V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->getSelectionEnd()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/widget/SuggestionEditText;->h:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/twitter/android/widget/SuggestionEditText;->c:I

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->c()Z

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->a()V

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->clearComposingText()V

    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0, p3}, Lcom/twitter/android/widget/SuggestionEditText;->a(Ljava/lang/Object;I)V

    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->a()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/widget/SuggestionEditText;->d:Landroid/widget/ListView;

    const/16 v2, 0x3e

    if-eq p1, v2, :cond_2

    invoke-virtual {v1}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v2

    if-gez v2, :cond_0

    const/16 v2, 0x42

    if-eq p1, v2, :cond_2

    const/16 v2, 0x17

    if-eq p1, v2, :cond_2

    :cond_0
    invoke-virtual {v1, p1, p2}, Landroid/widget/ListView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    invoke-virtual {p0, v2, p1}, Lcom/twitter/android/widget/SuggestionEditText;->a(ZI)Z

    move-result v3

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Landroid/widget/ListView;->requestFocusFromTouch()Z

    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->c()Z

    sparse-switch p1, :sswitch_data_0

    :cond_1
    if-eqz v3, :cond_2

    :goto_0
    :sswitch_0
    return v0

    :cond_2
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_0
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->d:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v1

    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    if-ltz v1, :cond_0

    invoke-virtual {v0, p1, p2}, Landroid/widget/ListView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onSelectionChanged(II)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->g:Lcom/twitter/android/widget/cr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->g:Lcom/twitter/android/widget/cr;

    invoke-interface {v0, p1, p2}, Lcom/twitter/android/widget/cr;->a(II)V

    :cond_0
    return-void
.end method

.method protected onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->r:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/SuggestionEditText;->a(Z)V

    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13

    const/4 v12, 0x2

    const/4 v7, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    :cond_0
    :pswitch_0
    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    invoke-super {p0, p1}, Landroid/widget/EditText;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    return v1

    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v4, v0

    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->getPaddingLeft()I

    move-result v5

    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->getPaddingRight()I

    move-result v6

    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->getCompoundPaddingLeft()I

    move-result v7

    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->getCompoundPaddingRight()I

    move-result v8

    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->getWidth()I

    move-result v9

    move v0, v1

    :goto_1
    array-length v10, v3

    if-ge v0, v10, :cond_3

    aget-object v10, v3, v0

    if-eqz v10, :cond_5

    if-nez v0, :cond_4

    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v10

    add-int/2addr v10, v5

    add-int/2addr v10, v7

    if-gt v4, v10, :cond_5

    iput v1, p0, Lcom/twitter/android/widget/SuggestionEditText;->j:I

    :cond_3
    :goto_2
    move v0, v1

    goto :goto_0

    :cond_4
    if-ne v0, v12, :cond_5

    sub-int v11, v9, v6

    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v10

    sub-int v10, v11, v10

    sub-int/2addr v10, v8

    if-lt v4, v10, :cond_5

    iput v12, p0, Lcom/twitter/android/widget/SuggestionEditText;->j:I

    goto :goto_2

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :pswitch_2
    iget v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->j:I

    if-eq v0, v7, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v3, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iget v5, p0, Lcom/twitter/android/widget/SuggestionEditText;->j:I

    aget-object v4, v4, v5

    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->getHeight()I

    move-result v5

    if-ge v0, v5, :cond_6

    if-lez v0, :cond_6

    move v0, v2

    :goto_3
    iget v5, p0, Lcom/twitter/android/widget/SuggestionEditText;->j:I

    packed-switch v5, :pswitch_data_1

    :pswitch_3
    move v3, v1

    :goto_4
    if-eqz v0, :cond_9

    if-eqz v3, :cond_9

    iget v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->j:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/SuggestionEditText;->a(I)Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v2

    :goto_5
    iput v7, p0, Lcom/twitter/android/widget/SuggestionEditText;->j:I

    goto/16 :goto_0

    :cond_6
    move v0, v1

    goto :goto_3

    :pswitch_4
    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->getPaddingLeft()I

    move-result v5

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    add-int/2addr v4, v5

    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->getCompoundPaddingLeft()I

    move-result v5

    add-int/2addr v4, v5

    if-gt v3, v4, :cond_7

    move v3, v2

    goto :goto_4

    :cond_7
    move v3, v1

    goto :goto_4

    :pswitch_5
    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->getWidth()I

    move-result v5

    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->getPaddingRight()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    sub-int v4, v5, v4

    invoke-virtual {p0}, Lcom/twitter/android/widget/SuggestionEditText;->getCompoundPaddingRight()I

    move-result v5

    sub-int/2addr v4, v5

    if-lt v3, v4, :cond_8

    move v3, v2

    goto :goto_4

    :cond_8
    move v3, v1

    goto :goto_4

    :cond_9
    move v0, v1

    goto :goto_5

    :pswitch_6
    iput v7, p0, Lcom/twitter/android/widget/SuggestionEditText;->j:I

    move v0, v1

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_6
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->d:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eq p1, v0, :cond_1

    iget-object v1, p0, Lcom/twitter/android/widget/SuggestionEditText;->p:Landroid/database/DataSetObserver;

    if-nez v1, :cond_2

    new-instance v0, Lcom/twitter/android/widget/cp;

    invoke-direct {v0, p0}, Lcom/twitter/android/widget/cp;-><init>(Lcom/twitter/android/widget/SuggestionEditText;)V

    iput-object v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->p:Landroid/database/DataSetObserver;

    :cond_0
    :goto_0
    if-eqz p1, :cond_3

    move-object v0, p1

    check-cast v0, Landroid/widget/Filterable;

    invoke-interface {v0}, Landroid/widget/Filterable;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->i:Landroid/widget/Filter;

    iget-object v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->p:Landroid/database/DataSetObserver;

    invoke-interface {p1, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->f:Lcom/twitter/android/widget/cq;

    iget-object v1, p0, Lcom/twitter/android/widget/SuggestionEditText;->i:Landroid/widget/Filter;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/cq;->a(Landroid/widget/Filter;)V

    iget-object v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->d:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    :cond_1
    return-void

    :cond_2
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/widget/SuggestionEditText;->p:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->i:Landroid/widget/Filter;

    goto :goto_1
.end method

.method public setBatchListener(Lcom/twitter/internal/android/widget/f;)V
    .locals 2

    iput-object p1, p0, Lcom/twitter/android/widget/SuggestionEditText;->m:Lcom/twitter/internal/android/widget/f;

    iget-object v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->l:Lcom/twitter/internal/android/widget/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->l:Lcom/twitter/internal/android/widget/e;

    iget-object v1, p0, Lcom/twitter/android/widget/SuggestionEditText;->m:Lcom/twitter/internal/android/widget/f;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/e;->a(Lcom/twitter/internal/android/widget/f;)V

    :cond_0
    return-void
.end method

.method public setFilteringEnabled(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/widget/SuggestionEditText;->r:Z

    return-void
.end method

.method protected setListView(Landroid/widget/ListView;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/SuggestionEditText;->d:Landroid/widget/ListView;

    invoke-virtual {p1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/SuggestionEditText;->n:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public setSuggestionListener(Lcom/twitter/android/widget/cr;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/widget/SuggestionEditText;->g:Lcom/twitter/android/widget/cr;

    iget-object v0, p0, Lcom/twitter/android/widget/SuggestionEditText;->f:Lcom/twitter/android/widget/cq;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/cq;->a(Lcom/twitter/android/widget/cr;)V

    return-void
.end method
