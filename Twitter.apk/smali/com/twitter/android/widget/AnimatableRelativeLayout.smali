.class public Lcom/twitter/android/widget/AnimatableRelativeLayout;
.super Landroid/widget/RelativeLayout;
.source "Twttr"


# instance fields
.field private a:I

.field private b:Z

.field private c:Lcom/twitter/android/widget/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public a(IIII)V
    .locals 9

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/widget/AnimatableRelativeLayout;->clearAnimation()V

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    invoke-virtual {p0}, Lcom/twitter/android/widget/AnimatableRelativeLayout;->getLeft()I

    move-result v3

    sub-int v3, p1, v3

    int-to-float v4, v3

    invoke-virtual {p0}, Lcom/twitter/android/widget/AnimatableRelativeLayout;->getTop()I

    move-result v3

    sub-int v3, p2, v3

    int-to-float v8, v3

    move v3, v1

    move v5, v1

    move v6, v2

    move v7, v1

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    iget v1, p0, Lcom/twitter/android/widget/AnimatableRelativeLayout;->a:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/twitter/android/widget/AnimatableRelativeLayout;->b:Z

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/AnimatableRelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/widget/AnimatableRelativeLayout;->b:Z

    return v0
.end method

.method protected onAnimationEnd()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/widget/AnimatableRelativeLayout;->clearAnimation()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/widget/AnimatableRelativeLayout;->b:Z

    invoke-super {p0}, Landroid/widget/RelativeLayout;->requestLayout()V

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAnimationEnd()V

    iget-object v0, p0, Lcom/twitter/android/widget/AnimatableRelativeLayout;->c:Lcom/twitter/android/widget/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/AnimatableRelativeLayout;->c:Lcom/twitter/android/widget/a;

    invoke-interface {v0}, Lcom/twitter/android/widget/a;->b()V

    :cond_0
    return-void
.end method

.method public setAnimationDuration(I)V
    .locals 0

    iput p1, p0, Lcom/twitter/android/widget/AnimatableRelativeLayout;->a:I

    return-void
.end method

.method public setAnimationListener(Lcom/twitter/android/widget/a;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/AnimatableRelativeLayout;->c:Lcom/twitter/android/widget/a;

    return-void
.end method

.method public startAnimation(Landroid/view/animation/Animation;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/AnimatableRelativeLayout;->c:Lcom/twitter/android/widget/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/AnimatableRelativeLayout;->c:Lcom/twitter/android/widget/a;

    invoke-interface {v0}, Lcom/twitter/android/widget/a;->a()V

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method
