.class Lcom/twitter/android/widget/c;
.super Lcom/twitter/android/util/ad;
.source "Twttr"


# instance fields
.field private a:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Lcom/twitter/android/widget/AutomatedEllipsisTextView;J)V
    .locals 1

    invoke-direct {p0, p2, p3}, Lcom/twitter/android/util/ad;-><init>(J)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/widget/c;->a:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/AutomatedEllipsisTextView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lcom/twitter/android/widget/AutomatedEllipsisTextView;->c()V

    const/4 v0, 0x1

    goto :goto_0
.end method
