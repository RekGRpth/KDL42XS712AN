.class public Lcom/twitter/android/widget/RetweetDialogFragment;
.super Lcom/twitter/android/widget/PromptDialogFragment;
.source "Twttr"


# instance fields
.field private a:Lcom/twitter/android/ru;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/widget/PromptDialogFragment;-><init>()V

    return-void
.end method

.method private static a()I
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string/jumbo v0, "android_rt_action_1837"

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "copy_share"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "android_rt_action_1837"

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "copy_share_timelines"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const v0, 0x7f0f04ef    # com.twitter.android.R.string.tweets_retweet_exp_share

    :goto_0
    return v0

    :cond_1
    const v0, 0x7f0f04ed    # com.twitter.android.R.string.tweets_retweet

    goto :goto_0
.end method

.method private static a(Z)I
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string/jumbo v0, "android_rt_action_1837"

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "copy_share"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-array v0, v5, [I

    fill-array-data v0, :array_0

    :goto_0
    if-eqz p0, :cond_2

    aget v0, v0, v4

    :goto_1
    return v0

    :cond_0
    const-string/jumbo v0, "android_rt_action_1837"

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "copy_share_timelines"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-array v0, v5, [I

    fill-array-data v0, :array_1

    goto :goto_0

    :cond_1
    new-array v0, v5, [I

    fill-array-data v0, :array_2

    goto :goto_0

    :cond_2
    aget v0, v0, v3

    goto :goto_1

    :array_0
    .array-data 4
        0x7f0f0365    # com.twitter.android.R.string.retweet_confirm_message_exp_share
        0x7f0f050a    # com.twitter.android.R.string.undo_retweet_confirm_message_exp_share
    .end array-data

    :array_1
    .array-data 4
        0x7f0f0366    # com.twitter.android.R.string.retweet_confirm_message_exp_share_timeline
        0x7f0f050a    # com.twitter.android.R.string.undo_retweet_confirm_message_exp_share
    .end array-data

    :array_2
    .array-data 4
        0x7f0f0364    # com.twitter.android.R.string.retweet_confirm_message
        0x7f0f0509    # com.twitter.android.R.string.undo_retweet_confirm_message
    .end array-data
.end method

.method public static a(IJLcom/twitter/library/provider/ParcelableTweet;ZZ)Lcom/twitter/android/widget/RetweetDialogFragment;
    .locals 7

    new-instance v6, Lcom/twitter/android/widget/RetweetDialogFragment;

    invoke-direct {v6}, Lcom/twitter/android/widget/RetweetDialogFragment;-><init>()V

    move v0, p0

    move-wide v1, p1

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-static/range {v0 .. v6}, Lcom/twitter/android/widget/RetweetDialogFragment;->a(IJLcom/twitter/library/provider/ParcelableTweet;ZZLcom/twitter/android/widget/RetweetDialogFragment;)V

    return-object v6
.end method

.method private a(IIJLcom/twitter/library/provider/ParcelableTweet;ZLjava/lang/String;)V
    .locals 7

    iget-object v0, p0, Lcom/twitter/android/widget/RetweetDialogFragment;->a:Lcom/twitter/android/ru;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/widget/RetweetDialogFragment;->a:Lcom/twitter/android/ru;

    move v1, p2

    move-wide v2, p3

    move-object v4, p5

    move v5, p6

    move-object v6, p7

    invoke-interface/range {v0 .. v6}, Lcom/twitter/android/ru;->a(IJLcom/twitter/library/provider/ParcelableTweet;ZLjava/lang/String;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/widget/RetweetDialogFragment;->a:Lcom/twitter/android/ru;

    move v1, p2

    move-wide v2, p3

    move-object v4, p5

    move v5, p6

    invoke-interface/range {v0 .. v5}, Lcom/twitter/android/ru;->a(IJLcom/twitter/library/provider/ParcelableTweet;Z)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/widget/RetweetDialogFragment;->a:Lcom/twitter/android/ru;

    move v1, p2

    move-wide v2, p3

    move-object v4, p5

    move v5, p6

    invoke-interface/range {v0 .. v5}, Lcom/twitter/android/ru;->b(IJLcom/twitter/library/provider/ParcelableTweet;Z)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/twitter/android/widget/RetweetDialogFragment;->a:Lcom/twitter/android/ru;

    move v1, p2

    move-wide v2, p3

    move-object v4, p5

    move v5, p6

    invoke-interface/range {v0 .. v5}, Lcom/twitter/android/ru;->c(IJLcom/twitter/library/provider/ParcelableTweet;Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected static a(IJLcom/twitter/library/provider/ParcelableTweet;ZZLcom/twitter/android/widget/RetweetDialogFragment;)V
    .locals 2

    invoke-virtual {p6, p0}, Lcom/twitter/android/widget/RetweetDialogFragment;->c(I)V

    invoke-static {}, Lcom/twitter/android/widget/RetweetDialogFragment;->a()I

    move-result v0

    invoke-virtual {p6, v0}, Lcom/twitter/android/widget/RetweetDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-static {}, Lcom/twitter/android/widget/RetweetDialogFragment;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->h(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0089    # com.twitter.android.R.string.cancel

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-static {p4}, Lcom/twitter/android/widget/RetweetDialogFragment;->b(Z)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-static {p4}, Lcom/twitter/android/widget/RetweetDialogFragment;->a(Z)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    invoke-virtual {p6}, Lcom/twitter/android/widget/RetweetDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "undo"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v1, "tweet"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v1, "add_main"

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method private static b()I
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string/jumbo v0, "android_rt_action_1837"

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "copy_share"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "android_rt_action_1837"

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "copy_share_timelines"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "android_rt_action_1837"

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "copy_comment"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const v0, 0x7f0f033e    # com.twitter.android.R.string.quote_exp_comment

    :goto_0
    return v0

    :cond_1
    const v0, 0x7f0f033d    # com.twitter.android.R.string.quote

    goto :goto_0
.end method

.method private static b(Z)I
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    if-eqz p0, :cond_0

    const v0, 0x7f0f04fa    # com.twitter.android.R.string.tweets_undo_retweet

    :goto_0
    return v0

    :cond_0
    const-string/jumbo v0, "android_rt_action_1837"

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "copy_share"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "android_rt_action_1837"

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "copy_share_timelines"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const v0, 0x7f0f04ef    # com.twitter.android.R.string.tweets_retweet_exp_share

    goto :goto_0

    :cond_2
    const v0, 0x7f0f04ed    # com.twitter.android.R.string.tweets_retweet

    goto :goto_0
.end method


# virtual methods
.method protected a(I)V
    .locals 8

    iget-object v0, p0, Lcom/twitter/android/widget/RetweetDialogFragment;->a:Lcom/twitter/android/ru;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/widget/RetweetDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    const-string/jumbo v1, "tweet"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Lcom/twitter/library/provider/ParcelableTweet;

    const-string/jumbo v1, "undo"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    const/4 v7, 0x0

    move-object v0, p0

    move v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/widget/RetweetDialogFragment;->a(IIJLcom/twitter/library/provider/ParcelableTweet;ZLjava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/ru;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/RetweetDialogFragment;->a:Lcom/twitter/android/ru;

    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/widget/PromptDialogFragment;->onAttach(Landroid/app/Activity;)V

    iget-object v0, p0, Lcom/twitter/android/widget/RetweetDialogFragment;->a:Lcom/twitter/android/ru;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/widget/RetweetDialogFragment;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/twitter/android/ru;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/twitter/android/ru;

    iput-object v0, p0, Lcom/twitter/android/widget/RetweetDialogFragment;->a:Lcom/twitter/android/ru;

    :cond_0
    :goto_0
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/RetweetDialogFragment;->a(I)V

    return-void

    :cond_1
    instance-of v0, p1, Lcom/twitter/android/ru;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/twitter/android/ru;

    iput-object p1, p0, Lcom/twitter/android/widget/RetweetDialogFragment;->a:Lcom/twitter/android/ru;

    goto :goto_0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/android/widget/PromptDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/RetweetDialogFragment;->a(I)V

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 13

    invoke-virtual {p0}, Lcom/twitter/android/widget/RetweetDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/widget/RetweetDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v7

    const-string/jumbo v2, "id"

    invoke-virtual {v7, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string/jumbo v3, "user_id"

    invoke-virtual {v7, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    const-string/jumbo v5, "tweet"

    invoke-virtual {v7, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Lcom/twitter/library/provider/ParcelableTweet;

    const-string/jumbo v6, "undo"

    invoke-virtual {v7, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v8

    invoke-virtual {v8, v3, v4}, Lcom/twitter/library/client/aa;->a(J)Lcom/twitter/library/client/Session;

    move-result-object v8

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    if-eqz v6, :cond_0

    invoke-interface {v5}, Lcom/twitter/library/provider/ParcelableTweet;->b()J

    move-result-wide v9

    invoke-virtual {v1, v8, v9, v10}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;J)Ljava/lang/String;

    move-result-object v7

    :goto_1
    const/4 v1, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/widget/RetweetDialogFragment;->a(IIJLcom/twitter/library/provider/ParcelableTweet;ZLjava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-interface {v5}, Lcom/twitter/library/provider/ParcelableTweet;->b()J

    move-result-wide v9

    invoke-interface {v5}, Lcom/twitter/library/provider/ParcelableTweet;->m()Lcom/twitter/library/api/PromotedContent;

    move-result-object v0

    invoke-virtual {v1, v8, v9, v10, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    :pswitch_1
    invoke-virtual {v8}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    sget-object v8, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->d:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    invoke-static {v0, v8}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Landroid/content/Context;Lcom/twitter/android/composer/ComposerIntentWrapper$Action;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v8

    const v9, 0x7f0f033f    # com.twitter.android.R.string.quote_format

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-interface {v5}, Lcom/twitter/library/provider/ParcelableTweet;->i()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-interface {v5}, Lcom/twitter/library/provider/ParcelableTweet;->a()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {p0, v9, v10}, Lcom/twitter/android/widget/RetweetDialogFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Ljava/lang/String;[I)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v8

    invoke-virtual {v8, v1}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Ljava/lang/String;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v8

    invoke-virtual {v8}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a()Landroid/content/Intent;

    move-result-object v8

    const-string/jumbo v9, "add_main"

    const/4 v10, 0x0

    invoke-virtual {v7, v9, v10}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-static {v8, v0, v1}, Lcom/twitter/android/MainActivity;->a(Landroid/content/Intent;Landroid/content/Context;Ljava/lang/String;)V

    :goto_2
    const/4 v1, 0x1

    const/4 v7, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/widget/RetweetDialogFragment;->a(IIJLcom/twitter/library/provider/ParcelableTweet;ZLjava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v8}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_2

    :pswitch_2
    const/4 v1, 0x2

    const/4 v7, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/widget/RetweetDialogFragment;->a(IIJLcom/twitter/library/provider/ParcelableTweet;ZLjava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
