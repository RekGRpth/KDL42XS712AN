.class Lcom/twitter/android/widget/ch;
.super Landroid/database/DataSetObserver;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/widget/cg;


# direct methods
.method constructor <init>(Lcom/twitter/android/widget/cg;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/ch;->a:Lcom/twitter/android/widget/cg;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/ch;->a:Lcom/twitter/android/widget/cg;

    iget-boolean v0, v0, Lcom/twitter/android/widget/cg;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/ch;->a:Lcom/twitter/android/widget/cg;

    invoke-virtual {v0}, Lcom/twitter/android/widget/cg;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public onInvalidated()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/ch;->a:Lcom/twitter/android/widget/cg;

    invoke-virtual {v0}, Lcom/twitter/android/widget/cg;->notifyDataSetInvalidated()V

    return-void
.end method
