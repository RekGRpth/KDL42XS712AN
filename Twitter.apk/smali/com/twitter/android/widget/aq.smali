.class public Lcom/twitter/android/widget/aq;
.super Landroid/text/SpannableStringBuilder;
.source "Twttr"


# static fields
.field static final a:Landroid/text/Editable$Factory;


# instance fields
.field private b:Lcom/twitter/android/widget/as;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/android/widget/ar;

    invoke-direct {v0}, Lcom/twitter/android/widget/ar;-><init>()V

    sput-object v0, Lcom/twitter/android/widget/aq;->a:Landroid/text/Editable$Factory;

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public a(ILjava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/widget/aq;->a(ILjava/lang/String;Z)V

    return-void
.end method

.method public a(ILjava/lang/String;Z)V
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/widget/aq;->length()I

    move-result v0

    const-class v2, Lcom/twitter/android/widget/au;

    invoke-virtual {p0, v1, v0, v2}, Lcom/twitter/android/widget/aq;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/widget/au;

    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    invoke-virtual {v3}, Lcom/twitter/android/widget/au;->a()I

    move-result v4

    if-ne v4, p1, :cond_1

    invoke-virtual {p0, v3}, Lcom/twitter/android/widget/aq;->getSpanStart(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {p0, v3}, Lcom/twitter/android/widget/aq;->getSpanEnd(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {p0, v3}, Lcom/twitter/android/widget/aq;->removeSpan(Ljava/lang/Object;)V

    invoke-virtual {p0, v0, v1, p2}, Lcom/twitter/android/widget/aq;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    if-eqz p3, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v0

    const/16 v2, 0x21

    invoke-virtual {p0, v3, v0, v1, v2}, Lcom/twitter/android/widget/aq;->setSpan(Ljava/lang/Object;III)V

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/widget/as;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/aq;->b:Lcom/twitter/android/widget/as;

    return-void
.end method

.method public bridge synthetic replace(IILjava/lang/CharSequence;II)Landroid/text/Editable;
    .locals 1

    invoke-virtual/range {p0 .. p5}, Lcom/twitter/android/widget/aq;->replace(IILjava/lang/CharSequence;II)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public replace(IILjava/lang/CharSequence;II)Landroid/text/SpannableStringBuilder;
    .locals 8

    const-class v0, Lcom/twitter/android/widget/au;

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/widget/aq;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/widget/au;

    array-length v1, v0

    if-nez v1, :cond_1

    invoke-super/range {p0 .. p5}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;II)Landroid/text/SpannableStringBuilder;

    move-result-object p0

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    const/4 v2, 0x0

    array-length v3, v0

    const/4 v1, 0x0

    move v7, v1

    move-object v1, v2

    move v2, v7

    :goto_1
    if-ge v2, v3, :cond_4

    aget-object v4, v0, v2

    invoke-virtual {p0, v4}, Lcom/twitter/android/widget/aq;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    invoke-virtual {p0, v4}, Lcom/twitter/android/widget/aq;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    if-le p2, v5, :cond_3

    if-ge p1, v6, :cond_3

    if-nez v1, :cond_2

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :cond_2
    invoke-virtual {v4}, Lcom/twitter/android/widget/au;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    if-eqz v1, :cond_5

    if-ne p4, p5, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/aq;->b:Lcom/twitter/android/widget/as;

    invoke-interface {v0, v1}, Lcom/twitter/android/widget/as;->a(Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_5
    invoke-super/range {p0 .. p5}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;II)Landroid/text/SpannableStringBuilder;

    move-result-object p0

    goto :goto_0
.end method
