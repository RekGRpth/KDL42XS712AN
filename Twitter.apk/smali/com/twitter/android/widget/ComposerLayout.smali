.class public Lcom/twitter/android/widget/ComposerLayout;
.super Lcom/twitter/android/widget/DraggableDrawerLayout;
.source "Twttr"


# instance fields
.field private a:I

.field private b:Landroid/widget/ScrollView;

.field private c:I

.field private d:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/widget/DraggableDrawerLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/widget/DraggableDrawerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/widget/DraggableDrawerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method a(IIII)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/ComposerLayout;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    sub-int v0, p4, v0

    iget-object v1, p0, Lcom/twitter/android/widget/ComposerLayout;->d:Landroid/view/View;

    invoke-virtual {v1, p1, v0, p3, p4}, Landroid/view/View;->layout(IIII)V

    invoke-super {p0, p1, p2, p3, v0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(IIII)V

    invoke-virtual {p0}, Lcom/twitter/android/widget/ComposerLayout;->getAboveDrawerView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/widget/ComposerLayout;->b:Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v3

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int v1, v3, v1

    sub-int/2addr v0, p2

    invoke-virtual {v2, v4, v4, v1, v0}, Landroid/widget/ScrollView;->layout(IIII)V

    return-void
.end method

.method a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/widget/DraggableDrawerLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    if-eqz p2, :cond_0

    sget-object v0, Lcom/twitter/android/rg;->ComposerLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/ComposerLayout;->a:I

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/ComposerLayout;->c:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/widget/DraggableDrawerLayout;->onFinishInflate()V

    iget v0, p0, Lcom/twitter/android/widget/ComposerLayout;->a:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/ComposerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/twitter/android/widget/ComposerLayout;->b:Landroid/widget/ScrollView;

    iget v0, p0, Lcom/twitter/android/widget/ComposerLayout;->c:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/ComposerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/ComposerLayout;->d:Landroid/view/View;

    return-void
.end method
