.class Lcom/twitter/android/widget/dm;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public a:I

.field public b:Landroid/graphics/Rect;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>(IIIIILjava/lang/String;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Rect;

    add-int/lit8 v1, p3, 0x1

    add-int/lit8 v2, p4, 0x1

    invoke-direct {v0, p1, p2, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/twitter/android/widget/dm;->b:Landroid/graphics/Rect;

    iget-object v0, p0, Lcom/twitter/android/widget/dm;->b:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v3, v3, v4, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/twitter/android/widget/dm;->b:Landroid/graphics/Rect;

    :cond_0
    iput p5, p0, Lcom/twitter/android/widget/dm;->a:I

    iput-object p6, p0, Lcom/twitter/android/widget/dm;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a(II)Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/dm;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    return v0
.end method
