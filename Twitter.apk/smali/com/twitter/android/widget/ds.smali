.class Lcom/twitter/android/widget/ds;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/widget/UserCheckBoxPreference;


# direct methods
.method private constructor <init>(Lcom/twitter/android/widget/UserCheckBoxPreference;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/ds;->a:Lcom/twitter/android/widget/UserCheckBoxPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/widget/UserCheckBoxPreference;Lcom/twitter/android/widget/dr;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/widget/ds;-><init>(Lcom/twitter/android/widget/UserCheckBoxPreference;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    iget-object v0, p0, Lcom/twitter/android/widget/ds;->a:Lcom/twitter/android/widget/UserCheckBoxPreference;

    invoke-virtual {v0}, Lcom/twitter/android/widget/UserCheckBoxPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/widget/ds;->a:Lcom/twitter/android/widget/UserCheckBoxPreference;

    invoke-static {v1}, Lcom/twitter/android/widget/UserCheckBoxPreference;->a(Lcom/twitter/android/widget/UserCheckBoxPreference;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/api/TwitterUser;->a()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "settings:notifications:favorite_people:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/twitter/android/widget/ds;->a:Lcom/twitter/android/widget/UserCheckBoxPreference;

    invoke-static {v2}, Lcom/twitter/android/widget/UserCheckBoxPreference;->b(Lcom/twitter/android/widget/UserCheckBoxPreference;)Landroid/widget/CheckBox;

    move-result-object v2

    if-ne p1, v2, :cond_5

    iget-object v2, p0, Lcom/twitter/android/widget/ds;->a:Lcom/twitter/android/widget/UserCheckBoxPreference;

    invoke-static {v2}, Lcom/twitter/android/widget/UserCheckBoxPreference;->c(Lcom/twitter/android/widget/UserCheckBoxPreference;)Z

    move-result v2

    if-nez v2, :cond_3

    :goto_0
    if-eqz v1, :cond_0

    const/16 v0, 0x10

    :cond_0
    iget-object v2, p0, Lcom/twitter/android/widget/ds;->a:Lcom/twitter/android/widget/UserCheckBoxPreference;

    invoke-static {v2}, Lcom/twitter/android/widget/UserCheckBoxPreference;->d(Lcom/twitter/android/widget/UserCheckBoxPreference;)Z

    move-result v2

    if-eqz v2, :cond_1

    or-int/lit16 v0, v0, 0x1000

    :cond_1
    iget-object v2, p0, Lcom/twitter/android/widget/ds;->a:Lcom/twitter/android/widget/UserCheckBoxPreference;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/twitter/android/widget/UserCheckBoxPreference;->a(Lcom/twitter/android/widget/UserCheckBoxPreference;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/widget/ds;->a:Lcom/twitter/android/widget/UserCheckBoxPreference;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/UserCheckBoxPreference;->a(Z)V

    const-string/jumbo v2, "device"

    if-eqz v1, :cond_4

    const-string/jumbo v0, "follow"

    :goto_1
    invoke-direct {p0, v2, v0}, Lcom/twitter/android/widget/ds;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    :goto_2
    return-void

    :cond_3
    move v1, v0

    goto :goto_0

    :cond_4
    const-string/jumbo v0, "unfollow"

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lcom/twitter/android/widget/ds;->a:Lcom/twitter/android/widget/UserCheckBoxPreference;

    invoke-static {v2}, Lcom/twitter/android/widget/UserCheckBoxPreference;->e(Lcom/twitter/android/widget/UserCheckBoxPreference;)Landroid/widget/CheckBox;

    move-result-object v2

    if-ne p1, v2, :cond_2

    iget-object v2, p0, Lcom/twitter/android/widget/ds;->a:Lcom/twitter/android/widget/UserCheckBoxPreference;

    invoke-static {v2}, Lcom/twitter/android/widget/UserCheckBoxPreference;->d(Lcom/twitter/android/widget/UserCheckBoxPreference;)Z

    move-result v2

    if-nez v2, :cond_8

    :goto_3
    if-eqz v1, :cond_6

    const/16 v0, 0x1000

    :cond_6
    iget-object v2, p0, Lcom/twitter/android/widget/ds;->a:Lcom/twitter/android/widget/UserCheckBoxPreference;

    invoke-static {v2}, Lcom/twitter/android/widget/UserCheckBoxPreference;->c(Lcom/twitter/android/widget/UserCheckBoxPreference;)Z

    move-result v2

    if-eqz v2, :cond_7

    or-int/lit8 v0, v0, 0x10

    :cond_7
    iget-object v2, p0, Lcom/twitter/android/widget/ds;->a:Lcom/twitter/android/widget/UserCheckBoxPreference;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/twitter/android/widget/UserCheckBoxPreference;->b(Lcom/twitter/android/widget/UserCheckBoxPreference;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/widget/ds;->a:Lcom/twitter/android/widget/UserCheckBoxPreference;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/UserCheckBoxPreference;->b(Z)V

    const-string/jumbo v2, "email"

    if-eqz v1, :cond_9

    const-string/jumbo v0, "follow"

    :goto_4
    invoke-direct {p0, v2, v0}, Lcom/twitter/android/widget/ds;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_8
    move v1, v0

    goto :goto_3

    :cond_9
    const-string/jumbo v0, "unfollow"

    goto :goto_4
.end method
