.class public Lcom/twitter/android/widget/PhoneVerificationDialog;
.super Lcom/twitter/android/widget/PromptDialogFragment;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/widget/PromptDialogFragment;-><init>()V

    return-void
.end method

.method public static a(ILandroid/content/Context;)Lcom/twitter/android/widget/PhoneVerificationDialog;
    .locals 6

    const v5, 0x7f0f0551    # com.twitter.android.R.string.verify_phone

    new-instance v0, Lcom/twitter/android/widget/PhoneVerificationDialog;

    invoke-direct {v0}, Lcom/twitter/android/widget/PhoneVerificationDialog;-><init>()V

    invoke-static {p1}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;)Lcom/twitter/android/util/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/twitter/android/util/d;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/PhoneVerificationDialog;->c(I)V

    invoke-virtual {v0, v5}, Lcom/twitter/android/widget/PhoneVerificationDialog;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    const v2, 0x7f0f0302    # com.twitter.android.R.string.phone_verification_dialog_message

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PhoneVerificationDialog;->b(Ljava/lang/String;)Lcom/twitter/android/widget/PromptDialogFragment;

    invoke-virtual {v0, v5}, Lcom/twitter/android/widget/PhoneVerificationDialog;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    const v1, 0x7f0f0057    # com.twitter.android.R.string.button_action_dismiss

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PhoneVerificationDialog;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    const/4 v0, -0x1

    if-ne v0, p2, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/widget/PhoneVerificationDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-static {v0}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;)Lcom/twitter/android/util/d;

    move-result-object v2

    invoke-interface {v2}, Lcom/twitter/android/util/d;->d()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/twitter/android/widget/bg;

    invoke-direct {v4, p0, v0, v1}, Lcom/twitter/android/widget/bg;-><init>(Lcom/twitter/android/widget/PhoneVerificationDialog;Landroid/content/Context;Lcom/twitter/android/client/c;)V

    invoke-virtual {v1, v4}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/j;)V

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v4, "device_registration_normalized_phone_number"

    const-string/jumbo v5, ""

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    invoke-interface {v2}, Lcom/twitter/android/util/d;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/twitter/android/client/c;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/twitter/android/widget/PromptDialogFragment;->onClick(Landroid/content/DialogInterface;I)V

    return-void
.end method
