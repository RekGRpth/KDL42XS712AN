.class Lcom/twitter/android/widget/aa;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lcom/twitter/android/widget/GalleryGridFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/widget/GalleryGridFragment;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/aa;->b:Lcom/twitter/android/widget/GalleryGridFragment;

    iput-object p2, p0, Lcom/twitter/android/widget/aa;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/widget/aa;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/widget/aa;->b:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v1}, Lcom/twitter/android/widget/GalleryGridFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c004e    # com.twitter.android.R.dimen.composer_grid_view_divider

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    const v3, 0x7f0d0007    # com.twitter.android.R.integer.num_gallery_grid_columns

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    add-int/lit8 v3, v1, 0x1

    mul-int/2addr v2, v3

    sub-int/2addr v0, v2

    div-int/2addr v0, v1

    iget-object v2, p0, Lcom/twitter/android/widget/aa;->b:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-static {v2}, Lcom/twitter/android/widget/GalleryGridFragment;->a(Lcom/twitter/android/widget/GalleryGridFragment;)Lcom/twitter/android/widget/t;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/twitter/android/widget/t;->a(I)V

    iget-object v2, p0, Lcom/twitter/android/widget/aa;->b:Lcom/twitter/android/widget/GalleryGridFragment;

    iget-object v3, p0, Lcom/twitter/android/widget/aa;->b:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-static {v3}, Lcom/twitter/android/widget/GalleryGridFragment;->b(Lcom/twitter/android/widget/GalleryGridFragment;)I

    move-result v3

    iget-object v4, p0, Lcom/twitter/android/widget/aa;->b:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-static {v4}, Lcom/twitter/android/widget/GalleryGridFragment;->c(Lcom/twitter/android/widget/GalleryGridFragment;)I

    move-result v4

    invoke-static {v2, v0, v3, v1, v4}, Lcom/twitter/android/widget/GalleryGridFragment;->a(Lcom/twitter/android/widget/GalleryGridFragment;IIII)V

    iget-object v1, p0, Lcom/twitter/android/widget/aa;->b:Lcom/twitter/android/widget/GalleryGridFragment;

    iget-object v2, p0, Lcom/twitter/android/widget/aa;->b:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-static {v2}, Lcom/twitter/android/widget/GalleryGridFragment;->d(Lcom/twitter/android/widget/GalleryGridFragment;)[Landroid/view/View;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/twitter/android/widget/GalleryGridFragment;->a(Lcom/twitter/android/widget/GalleryGridFragment;[Landroid/view/View;I)V

    iget-object v0, p0, Lcom/twitter/android/widget/aa;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method
