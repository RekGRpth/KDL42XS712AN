.class public Lcom/twitter/android/widget/SportsTeamRowView;
.super Landroid/widget/RelativeLayout;
.source "Twttr"


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/ImageView;

.field private e:Lcom/twitter/library/util/m;

.field private f:I

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    const v1, 0x7f01003b    # com.twitter.android.R.attr.sportsTeamRowViewStyle

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/widget/SportsTeamRowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const v0, 0x7f01003b    # com.twitter.android.R.attr.sportsTeamRowViewStyle

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/widget/SportsTeamRowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-object v0, Lcom/twitter/android/rg;->SportsTeamRowView:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const v1, 0x7f030141    # com.twitter.android.R.layout.sports_team_row

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/SportsTeamRowView;->f:I

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/widget/SportsTeamRowView;->g:Z

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0}, Lcom/twitter/android/widget/SportsTeamRowView;->a()V

    return-void
.end method

.method protected static a(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 1

    if-eqz p0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method protected a(Landroid/widget/ImageView;Lcom/twitter/library/util/m;Ljava/lang/String;Lcom/twitter/library/widget/ap;)Lcom/twitter/library/util/m;
    .locals 0

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1, p3, p2, p4}, Lcom/twitter/android/widget/SportsTeamRowView;->a(Landroid/widget/ImageView;Ljava/lang/String;Lcom/twitter/library/util/m;Lcom/twitter/library/widget/ap;)Lcom/twitter/library/util/m;

    move-result-object p2

    :cond_0
    return-object p2
.end method

.method protected a(Landroid/widget/ImageView;Ljava/lang/String;Lcom/twitter/library/util/m;Lcom/twitter/library/widget/ap;)Lcom/twitter/library/util/m;
    .locals 2

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    new-instance v0, Lcom/twitter/library/util/m;

    invoke-direct {v0, p2, v1, v1}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;II)V

    invoke-virtual {v0, p3}, Lcom/twitter/library/util/m;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    invoke-interface {p4, v0}, Lcom/twitter/library/widget/ap;->a(Lcom/twitter/library/util/m;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    move-object p3, v0

    :cond_1
    return-object p3
.end method

.method public a()V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/widget/SportsTeamRowView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/twitter/android/widget/SportsTeamRowView;->f:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    return-void
.end method

.method public a(Lcom/twitter/library/api/TwitterTopic$SportsEvent$Player;Lcom/twitter/library/widget/ap;)V
    .locals 3

    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/widget/SportsTeamRowView;->g:Z

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/twitter/library/api/TwitterTopic$SportsEvent$Player;->name:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/widget/SportsTeamRowView;->a:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/twitter/library/api/TwitterTopic$SportsEvent$Player;->location:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/twitter/android/widget/SportsTeamRowView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/widget/SportsTeamRowView;->b:Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/twitter/android/widget/SportsTeamRowView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/widget/SportsTeamRowView;->c:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/twitter/library/api/TwitterTopic$SportsEvent$Player;->score:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/android/widget/SportsTeamRowView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/widget/SportsTeamRowView;->d:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/twitter/android/widget/SportsTeamRowView;->e:Lcom/twitter/library/util/m;

    iget-object v2, p1, Lcom/twitter/library/api/TwitterTopic$SportsEvent$Player;->logoUrl:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v2, p2}, Lcom/twitter/android/widget/SportsTeamRowView;->a(Landroid/widget/ImageView;Lcom/twitter/library/util/m;Ljava/lang/String;Lcom/twitter/library/widget/ap;)Lcom/twitter/library/util/m;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/SportsTeamRowView;->e:Lcom/twitter/library/util/m;

    :cond_0
    return-void

    :cond_1
    iget-object v0, p1, Lcom/twitter/library/api/TwitterTopic$SportsEvent$Player;->name:Ljava/lang/String;

    goto :goto_0
.end method

.method public getLogoKey()Lcom/twitter/library/util/m;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/SportsTeamRowView;->e:Lcom/twitter/library/util/m;

    return-object v0
.end method

.method public getLogoView()Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/SportsTeamRowView;->d:Landroid/widget/ImageView;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    const v0, 0x7f090277    # com.twitter.android.R.id.team_location

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/SportsTeamRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/SportsTeamRowView;->a:Landroid/widget/TextView;

    const v0, 0x7f090275    # com.twitter.android.R.id.team_name

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/SportsTeamRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/SportsTeamRowView;->b:Landroid/widget/TextView;

    const v0, 0x7f090276    # com.twitter.android.R.id.score

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/SportsTeamRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/SportsTeamRowView;->c:Landroid/widget/TextView;

    const v0, 0x7f090274    # com.twitter.android.R.id.team_logo

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/SportsTeamRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/widget/SportsTeamRowView;->d:Landroid/widget/ImageView;

    return-void
.end method
