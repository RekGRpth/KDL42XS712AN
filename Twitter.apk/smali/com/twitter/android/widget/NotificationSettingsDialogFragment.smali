.class public final Lcom/twitter/android/widget/NotificationSettingsDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "Twttr"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Landroid/accounts/Account;

.field private c:I

.field private d:Ljava/lang/String;

.field private e:Landroid/content/Context;

.field private f:Lcom/twitter/library/scribe/ScribeItem;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Lcom/twitter/android/client/c;

.field private j:Lcom/twitter/android/GCMChangeReceiver;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/widget/NotificationSettingsDialogFragment;I)I
    .locals 1

    iget v0, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->c:I

    xor-int/2addr v0, p1

    iput v0, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->c:I

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/widget/NotificationSettingsDialogFragment;)Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->b:Landroid/accounts/Account;

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/android/widget/NotificationSettingsDialogFragment;
    .locals 3

    new-instance v0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;

    invoke-direct {v0}, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    const/4 v2, 0x4

    invoke-direct {v1, v2}, Landroid/os/Bundle;-><init>(I)V

    const-string/jumbo v2, "account_name"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "collapse_key"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "event_id"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "query"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/widget/NotificationSettingsDialogFragment;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iget-object v2, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->i:Lcom/twitter/android/client/c;

    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v3, v0, v1}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v4, "search"

    aput-object v4, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v4, "universal_top"

    aput-object v4, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v4, ""

    aput-object v4, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v4, "recommendation"

    aput-object v4, v0, v1

    const/4 v1, 0x4

    aput-object p1, v0, v1

    invoke-virtual {v3, v0}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->f:Lcom/twitter/library/scribe/ScribeItem;

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->f(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/widget/NotificationSettingsDialogFragment;)I
    .locals 1

    iget v0, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->c:I

    return v0
.end method

.method static synthetic c(Lcom/twitter/android/widget/NotificationSettingsDialogFragment;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->i:Lcom/twitter/android/client/c;

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    iput-object p1, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->e:Landroid/content/Context;

    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    const-string/jumbo v0, "cancel"

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->a(Ljava/lang/String;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    sget-object v1, Lcom/twitter/library/platform/PushService;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v1, Lcom/twitter/android/GCMChangeReceiver;

    invoke-direct {v1}, Lcom/twitter/android/GCMChangeReceiver;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->j:Lcom/twitter/android/GCMChangeReceiver;

    iget-object v1, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->e:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->j:Lcom/twitter/android/GCMChangeReceiver;

    sget-object v3, Lcom/twitter/library/provider/w;->a:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 11

    const/4 v10, 0x2

    const/4 v0, -0x1

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-nez p1, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "account_name"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->a:Ljava/lang/String;

    const-string/jumbo v2, "collapse_key"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->d:Ljava/lang/String;

    const-string/jumbo v2, "event_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->g:Ljava/lang/String;

    const-string/jumbo v2, "query"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->h:Ljava/lang/String;

    :goto_0
    new-instance v1, Lcom/twitter/library/scribe/ScribeItem;

    invoke-direct {v1}, Lcom/twitter/library/scribe/ScribeItem;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->f:Lcom/twitter/library/scribe/ScribeItem;

    iget-object v1, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->e:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->i:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    iget-object v1, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->e:Landroid/content/Context;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/samsung/model/g;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    array-length v3, v2

    move v1, v6

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    iget-object v8, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v9, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->a:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    iput-object v4, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->b:Landroid/accounts/Account;

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_1
    move v1, v0

    :goto_2
    packed-switch v1, :pswitch_data_1

    const-string/jumbo v2, ""

    const-string/jumbo v1, ""

    :goto_3
    iget-object v3, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->e:Landroid/content/Context;

    iget-object v4, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->b:Landroid/accounts/Account;

    invoke-static {v3, v4}, Lcom/twitter/library/platform/PushService;->b(Landroid/content/Context;Landroid/accounts/Account;)I

    move-result v3

    iput v3, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->c:I

    iget v3, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->c:I

    and-int/2addr v3, v0

    if-ne v0, v3, :cond_4

    move v4, v5

    :goto_4
    if-eqz v4, :cond_5

    const v3, 0x7f0f02d4    # com.twitter.android.R.string.off

    :goto_5
    invoke-virtual {v7, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v8, v10, [Ljava/lang/String;

    const v9, 0x7f0f033c    # com.twitter.android.R.string.push_opt_in_out_string

    new-array v10, v10, [Ljava/lang/Object;

    aput-object v3, v10, v6

    aput-object v2, v10, v5

    invoke-virtual {v7, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v8, v6

    const v2, 0x7f0f0089    # com.twitter.android.R.string.cancel

    invoke-virtual {v7, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v8, v5

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v3, Lcom/twitter/android/widget/bf;

    invoke-direct {v3, p0, v0, v4}, Lcom/twitter/android/widget/bf;-><init>(Lcom/twitter/android/widget/NotificationSettingsDialogFragment;IZ)V

    invoke-virtual {v1, v8, v3}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    :cond_2
    const-string/jumbo v1, "account_name"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->a:Ljava/lang/String;

    const-string/jumbo v1, "collapse_key"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->d:Ljava/lang/String;

    const-string/jumbo v1, "event_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->g:Ljava/lang/String;

    const-string/jumbo v1, "query"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->h:Ljava/lang/String;

    goto/16 :goto_0

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    :pswitch_0
    const-string/jumbo v2, "event_parrot"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v6

    goto/16 :goto_2

    :pswitch_1
    const v0, 0x7f0f029b    # com.twitter.android.R.string.news

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v0, 0x7f0f0347    # com.twitter.android.R.string.recommended_news

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v0, Lcom/twitter/library/provider/NotificationSetting;->k:Lcom/twitter/library/provider/NotificationSetting;

    iget v0, v0, Lcom/twitter/library/provider/NotificationSetting;->enabledFor:I

    iget-object v3, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->f:Lcom/twitter/library/scribe/ScribeItem;

    iget-object v4, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->g:Ljava/lang/String;

    iput-object v4, v3, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->f:Lcom/twitter/library/scribe/ScribeItem;

    iget-object v4, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->h:Ljava/lang/String;

    iput-object v4, v3, Lcom/twitter/library/scribe/ScribeItem;->w:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->f:Lcom/twitter/library/scribe/ScribeItem;

    const/16 v4, 0xc

    iput v4, v3, Lcom/twitter/library/scribe/ScribeItem;->c:I

    iget-object v3, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->f:Lcom/twitter/library/scribe/ScribeItem;

    const-string/jumbo v4, "event_parrot"

    iput-object v4, v3, Lcom/twitter/library/scribe/ScribeItem;->x:Ljava/lang/String;

    goto/16 :goto_3

    :cond_4
    move v4, v6

    goto/16 :goto_4

    :cond_5
    const v3, 0x7f0f02d6    # com.twitter.android.R.string.on

    goto/16 :goto_5

    :pswitch_data_0
    .packed-switch 0x390582db
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onDestroy()V

    iget-object v0, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->e:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->j:Lcom/twitter/android/GCMChangeReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string/jumbo v0, "account_name"

    iget-object v1, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "collapse_key"

    iget-object v1, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "event_id"

    iget-object v1, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "query"

    iget-object v1, p0, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
