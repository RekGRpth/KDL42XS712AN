.class Lcom/twitter/android/widget/s;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:I

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:I

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Lcom/twitter/android/widget/EventView;


# direct methods
.method constructor <init>(Lcom/twitter/android/widget/EventView;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/s;->f:Lcom/twitter/android/widget/EventView;

    iput-object p2, p0, Lcom/twitter/android/widget/s;->a:Ljava/lang/String;

    iput p3, p0, Lcom/twitter/android/widget/s;->b:I

    iput-object p4, p0, Lcom/twitter/android/widget/s;->c:Ljava/lang/String;

    iput p5, p0, Lcom/twitter/android/widget/s;->d:I

    iput-object p6, p0, Lcom/twitter/android/widget/s;->e:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/widget/s;->f:Lcom/twitter/android/widget/EventView;

    invoke-static {v0}, Lcom/twitter/android/widget/EventView;->a(Lcom/twitter/android/widget/EventView;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/widget/s;->a:Ljava/lang/String;

    iget v2, p0, Lcom/twitter/android/widget/s;->b:I

    iget-object v3, p0, Lcom/twitter/android/widget/s;->c:Ljava/lang/String;

    const-string/jumbo v4, "click"

    iget v5, p0, Lcom/twitter/android/widget/s;->d:I

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/widget/EventView;->a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lcom/twitter/android/widget/s;->e:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v1, p0, Lcom/twitter/android/widget/s;->f:Lcom/twitter/android/widget/EventView;

    invoke-static {v1}, Lcom/twitter/android/widget/EventView;->a(Lcom/twitter/android/widget/EventView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
