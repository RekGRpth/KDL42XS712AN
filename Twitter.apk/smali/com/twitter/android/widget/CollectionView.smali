.class public Lcom/twitter/android/widget/CollectionView;
.super Lcom/twitter/android/widget/TopicView;
.source "Twttr"


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:Ljava/lang/String;

.field private h:Landroid/content/Intent;

.field private i:J

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Landroid/content/Context;

.field private m:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/twitter/android/widget/TopicView;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/twitter/android/widget/f;

    invoke-direct {v0, p0}, Lcom/twitter/android/widget/f;-><init>(Lcom/twitter/android/widget/CollectionView;)V

    iput-object v0, p0, Lcom/twitter/android/widget/CollectionView;->m:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/widget/TopicView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/twitter/android/widget/f;

    invoke-direct {v0, p0}, Lcom/twitter/android/widget/f;-><init>(Lcom/twitter/android/widget/CollectionView;)V

    iput-object v0, p0, Lcom/twitter/android/widget/CollectionView;->m:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/widget/TopicView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Lcom/twitter/android/widget/f;

    invoke-direct {v0, p0}, Lcom/twitter/android/widget/f;-><init>(Lcom/twitter/android/widget/CollectionView;)V

    iput-object v0, p0, Lcom/twitter/android/widget/CollectionView;->m:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private a()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/widget/CollectionView;->l:Landroid/content/Context;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/widget/CollectionView;->l:Landroid/content/Context;

    const-class v2, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "user_id"

    iget-wide v2, p0, Lcom/twitter/android/widget/CollectionView;->i:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "screen_name"

    iget-object v2, p0, Lcom/twitter/android/widget/CollectionView;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/widget/CollectionView;->l:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/widget/CollectionView;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/widget/CollectionView;->a()V

    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/CollectionView;->l:Landroid/content/Context;

    return-void
.end method

.method public a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLcom/twitter/library/widget/ap;Ljava/lang/String;Ljava/lang/String;[BZZLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    iput-object p1, p0, Lcom/twitter/android/widget/CollectionView;->e:Ljava/lang/String;

    iput p2, p0, Lcom/twitter/android/widget/CollectionView;->f:I

    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/twitter/android/widget/CollectionView;->g:Ljava/lang/String;

    invoke-virtual/range {p17 .. p17}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/twitter/android/widget/CollectionView;->i:J

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/twitter/android/widget/CollectionView;->j:Ljava/lang/String;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/twitter/android/widget/CollectionView;->k:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/widget/CollectionView;->a:Landroid/widget/TextView;

    invoke-static {v1, p4}, Lcom/twitter/android/widget/CollectionView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/widget/CollectionView;->d:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/twitter/android/widget/CollectionView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static/range {p14 .. p14}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/TwitterTopic$TwitterList;

    if-eqz v3, :cond_6

    if-eqz v1, :cond_6

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, v1, Lcom/twitter/library/api/TwitterTopic$TwitterList;->members:I

    if-lez v2, :cond_0

    const v2, 0x7f0e0003    # com.twitter.android.R.plurals.list_members_count

    iget v5, v1, Lcom/twitter/library/api/TwitterTopic$TwitterList;->members:I

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, v1, Lcom/twitter/library/api/TwitterTopic$TwitterList;->members:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v3, v2, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-wide v5, v1, Lcom/twitter/library/api/TwitterTopic$TwitterList;->mostRecentTweetTimestampMillis:J

    const-wide/16 v7, 0x0

    cmp-long v2, v5, v7

    if-lez v2, :cond_2

    iget-wide v5, v1, Lcom/twitter/library/api/TwitterTopic$TwitterList;->mostRecentTweetTimestampMillis:J

    invoke-static {v3, v5, v6}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-wide v8, v1, Lcom/twitter/library/api/TwitterTopic$TwitterList;->mostRecentTweetTimestampMillis:J

    sub-long/2addr v6, v8

    const-wide/32 v8, 0x240c8400

    cmp-long v2, v6, v8

    if-ltz v2, :cond_4

    const v2, 0x7f0f0201    # com.twitter.android.R.string.last_tweet_time

    :goto_0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_1

    const/16 v6, 0x20

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0xb7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0x20

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v5, v6, v7

    invoke-virtual {v3, v2, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_5

    iget-object v2, p0, Lcom/twitter/android/widget/CollectionView;->b:Landroid/widget/TextView;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/twitter/android/widget/CollectionView;->b:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    iget-object v2, p0, Lcom/twitter/android/widget/CollectionView;->c:Landroid/widget/TextView;

    const v4, 0x7f0f0213    # com.twitter.android.R.string.lists_by_username

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/twitter/android/widget/CollectionView;->j:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/twitter/android/widget/CollectionView;->k:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v2, 0x7

    if-ne p2, v2, :cond_7

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/twitter/android/widget/CollectionView;->l:Landroid/content/Context;

    const-class v4, Lcom/twitter/android/ListTabActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "owner_id"

    move-object/from16 v0, p17

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string/jumbo v3, "creator_id"

    move-object/from16 v0, p17

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string/jumbo v3, "list_id"

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string/jumbo v3, "list_name"

    invoke-virtual {v2, v3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v3, "list_description"

    invoke-virtual {v2, v3, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v3, "list_fullname"

    invoke-virtual {v2, v3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v3, "list_mode"

    iget v1, v1, Lcom/twitter/library/api/TwitterTopic$TwitterList;->mode:I

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iput-object v2, p0, Lcom/twitter/android/widget/CollectionView;->h:Landroid/content/Intent;

    :cond_3
    :goto_2
    return-void

    :cond_4
    const v2, 0x7f0f0202    # com.twitter.android.R.string.last_tweet_time_ago

    goto/16 :goto_0

    :cond_5
    iget-object v2, p0, Lcom/twitter/android/widget/CollectionView;->b:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_6
    iget-object v2, p0, Lcom/twitter/android/widget/CollectionView;->b:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_7
    const/4 v1, 0x6

    if-ne p2, v1, :cond_3

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/widget/CollectionView;->l:Landroid/content/Context;

    const-class v3, Lcom/twitter/android/CollectionPermalinkActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "type"

    const/16 v3, 0x1b

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "timeline_tag"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/widget/CollectionView;->h:Landroid/content/Intent;

    goto :goto_2
.end method

.method public getLaunchIntent()Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/CollectionView;->h:Landroid/content/Intent;

    return-object v0
.end method

.method public getSeedHashtag()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/CollectionView;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getTopicId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/CollectionView;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getTopicType()I
    .locals 1

    iget v0, p0, Lcom/twitter/android/widget/CollectionView;->f:I

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/widget/TopicView;->onFinishInflate()V

    const v0, 0x7f090097    # com.twitter.android.R.id.name

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/CollectionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/CollectionView;->a:Landroid/widget/TextView;

    const v0, 0x7f0900de    # com.twitter.android.R.id.metadata

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/CollectionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/CollectionView;->b:Landroid/widget/TextView;

    const v0, 0x7f0900dc    # com.twitter.android.R.id.owner_byline

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/CollectionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/CollectionView;->c:Landroid/widget/TextView;

    const v0, 0x7f0900dd    # com.twitter.android.R.id.description

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/CollectionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/CollectionView;->d:Landroid/widget/TextView;

    return-void
.end method
