.class public Lcom/twitter/android/widget/bu;
.super Landroid/widget/BaseAdapter;
.source "Twttr"

# interfaces
.implements Landroid/widget/Filterable;


# instance fields
.field final synthetic a:Lcom/twitter/android/widget/PoiFragment;

.field private b:Ljava/util/ArrayList;

.field private c:Landroid/view/LayoutInflater;

.field private d:I

.field private e:Lcom/twitter/android/widget/bw;

.field private f:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Lcom/twitter/android/widget/PoiFragment;Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/widget/bu;->a:Lcom/twitter/android/widget/PoiFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    invoke-virtual {p1}, Lcom/twitter/android/widget/PoiFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/bu;->f:Landroid/content/res/Resources;

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/bu;->c:Landroid/view/LayoutInflater;

    iput p3, p0, Lcom/twitter/android/widget/bu;->d:I

    if-nez p4, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/bu;->b:Ljava/util/ArrayList;

    :goto_0
    return-void

    :cond_0
    iput-object p4, p0, Lcom/twitter/android/widget/bu;->b:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method private a(Lcom/twitter/library/api/geo/TwitterPlace;)Ljava/lang/String;
    .locals 3

    iget v0, p1, Lcom/twitter/library/api/geo/TwitterPlace;->placeType:I

    const-string/jumbo v1, "poi"

    invoke-static {v1}, Lcom/twitter/library/api/geo/TwitterPlace;->a(Ljava/lang/String;)I

    move-result v1

    if-ne v0, v1, :cond_1

    iget-object v0, p1, Lcom/twitter/library/api/geo/TwitterPlace;->placeInfo:Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;

    iget-object v0, v0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->name:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p1, Lcom/twitter/library/api/geo/TwitterPlace;->fullName:Ljava/lang/String;

    const-string/jumbo v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p1, Lcom/twitter/library/api/geo/TwitterPlace;->fullName:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public a(I)Lcom/twitter/library/api/geo/TwitterPlace;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/bu;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/geo/TwitterPlace;

    return-object v0
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 2

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/bu;->a:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v0}, Lcom/twitter/android/widget/PoiFragment;->l(Lcom/twitter/android/widget/PoiFragment;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iput-object p1, p0, Lcom/twitter/android/widget/bu;->b:Ljava/util/ArrayList;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lcom/twitter/android/widget/bu;->notifyDataSetChanged()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/bu;->b:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/twitter/android/widget/bu;->a:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v1}, Lcom/twitter/android/widget/PoiFragment;->m(Lcom/twitter/android/widget/PoiFragment;)Ljava/util/ArrayList;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/bu;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/widget/bu;->e:Lcom/twitter/android/widget/bw;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/widget/bw;

    iget-object v1, p0, Lcom/twitter/android/widget/bu;->a:Lcom/twitter/android/widget/PoiFragment;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/widget/bw;-><init>(Lcom/twitter/android/widget/PoiFragment;Lcom/twitter/android/widget/bh;)V

    iput-object v0, p0, Lcom/twitter/android/widget/bu;->e:Lcom/twitter/android/widget/bw;

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/bu;->e:Lcom/twitter/android/widget/bw;

    return-object v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/widget/bu;->a(I)Lcom/twitter/library/api/geo/TwitterPlace;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 16

    if-nez p2, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/widget/bu;->c:Landroid/view/LayoutInflater;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/android/widget/bu;->d:I

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance v1, Lcom/twitter/android/widget/bv;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Lcom/twitter/android/widget/bv;-><init>(Lcom/twitter/android/widget/bu;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    new-instance v1, Lcom/twitter/android/widget/by;

    move-object/from16 v0, p2

    invoke-direct {v1, v0}, Lcom/twitter/android/widget/by;-><init>(Landroid/view/View;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v14, v1, Lcom/twitter/android/widget/by;->a:Lcom/twitter/android/widget/PoiItemView;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/widget/bu;->a:Lcom/twitter/android/widget/PoiFragment;

    move/from16 v0, p1

    invoke-static {v1, v0}, Lcom/twitter/android/widget/PoiFragment;->a(Lcom/twitter/android/widget/PoiFragment;I)I

    move-result v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/bu;->a(I)Lcom/twitter/library/api/geo/TwitterPlace;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/twitter/android/widget/bu;->a(Lcom/twitter/library/api/geo/TwitterPlace;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/bu;->a:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v2}, Lcom/twitter/android/widget/PoiFragment;->f(Lcom/twitter/android/widget/PoiFragment;)Lcom/twitter/library/api/geo/TwitterPlace;

    move-result-object v2

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/bu;->a:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v2}, Lcom/twitter/android/widget/PoiFragment;->f(Lcom/twitter/android/widget/PoiFragment;)Lcom/twitter/library/api/geo/TwitterPlace;

    move-result-object v2

    invoke-virtual {v2, v15}, Lcom/twitter/library/api/geo/TwitterPlace;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    move v13, v2

    :goto_1
    if-eqz v13, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/bu;->a:Lcom/twitter/android/widget/PoiFragment;

    invoke-virtual {v2}, Lcom/twitter/android/widget/PoiFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f030c    # com.twitter.android.R.string.poi_remove_place

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v10, v1

    :goto_2
    iget-object v3, v15, Lcom/twitter/library/api/geo/TwitterPlace;->placeInfo:Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;

    iget-object v4, v3, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->attributes:Ljava/util/HashMap;

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz v4, :cond_3

    const-string/jumbo v1, "twitter"

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string/jumbo v2, "street_address"

    invoke-virtual {v4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object v11, v2

    move-object v12, v1

    :goto_3
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/widget/bu;->a:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v1}, Lcom/twitter/android/widget/PoiFragment;->k(Lcom/twitter/android/widget/PoiFragment;)Lcom/twitter/library/platform/LocationProducer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/platform/LocationProducer;->a()Landroid/location/Location;

    move-result-object v4

    iget-object v7, v3, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->boundingCoordinates:Ljava/util/ArrayList;

    iget v1, v15, Lcom/twitter/library/api/geo/TwitterPlace;->placeType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    if-eqz v7, :cond_0

    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v4, :cond_0

    const/4 v1, 0x1

    new-array v9, v1, [F

    invoke-virtual {v4}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    invoke-virtual {v4}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    const/4 v5, 0x0

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v7

    invoke-static/range {v1 .. v9}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    const/4 v1, 0x0

    aget v1, v9, v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/bu;->f:Landroid/content/res/Resources;

    invoke-static {v1, v2}, Lcom/twitter/android/widget/PoiFragment;->a(FLandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v5

    :cond_0
    move-object v1, v14

    move-object v2, v10

    move-object v3, v12

    move-object v4, v11

    move v6, v13

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/android/widget/PoiItemView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/widget/bu;->a:Lcom/twitter/android/widget/PoiFragment;

    move/from16 v0, p1

    invoke-static {v1, v15, v0}, Lcom/twitter/android/widget/PoiFragment;->a(Lcom/twitter/android/widget/PoiFragment;Lcom/twitter/library/api/geo/TwitterPlace;I)V

    return-object p2

    :cond_1
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/widget/by;

    goto/16 :goto_0

    :cond_2
    const/4 v2, 0x0

    move v13, v2

    goto/16 :goto_1

    :cond_3
    move-object v11, v2

    move-object v12, v1

    goto :goto_3

    :cond_4
    move-object v10, v1

    goto/16 :goto_2
.end method
