.class public Lcom/twitter/android/widget/aj;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Ljava/util/ArrayList;

.field private final b:Ljava/util/HashMap;

.field private c:Landroid/widget/ListView;

.field private d:Lcom/twitter/library/scribe/ScribeAssociation;

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/aj;->a:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/aj;->b:Ljava/util/HashMap;

    const/16 v0, 0xc8

    iput v0, p0, Lcom/twitter/android/widget/aj;->e:I

    const/16 v0, 0x7530

    iput v0, p0, Lcom/twitter/android/widget/aj;->f:I

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/widget/aj;)Lcom/twitter/library/scribe/ScribeAssociation;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/aj;->d:Lcom/twitter/library/scribe/ScribeAssociation;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/widget/aj;)Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/aj;->c:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/widget/aj;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/aj;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/widget/aj;)I
    .locals 1

    iget v0, p0, Lcom/twitter/android/widget/aj;->e:I

    return v0
.end method

.method static synthetic e(Lcom/twitter/android/widget/aj;)I
    .locals 1

    iget v0, p0, Lcom/twitter/android/widget/aj;->f:I

    return v0
.end method


# virtual methods
.method public a(J)Ljava/util/ArrayList;
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/widget/aj;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iget-object v2, p0, Lcom/twitter/android/widget/aj;->b:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/ak;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/widget/ak;->a(J)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/aj;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method public a(FF)V
    .locals 2

    const/high16 v1, 0x447a0000    # 1000.0f

    mul-float v0, p1, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/twitter/android/widget/aj;->e:I

    mul-float v0, p2, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/twitter/android/widget/aj;->f:I

    return-void
.end method

.method public a(Landroid/view/View;Lcom/twitter/library/provider/Tweet;)V
    .locals 4

    iget-wide v1, p2, Lcom/twitter/library/provider/Tweet;->L:J

    iget-object v0, p0, Lcom/twitter/android/widget/aj;->b:Ljava/util/HashMap;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/ak;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/widget/ak;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/android/widget/ak;-><init>(Lcom/twitter/android/widget/aj;Landroid/view/View;Lcom/twitter/library/provider/Tweet;)V

    iget-object v3, p0, Lcom/twitter/android/widget/aj;->b:Ljava/util/HashMap;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v3, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/widget/ak;->a(J)V

    return-void

    :cond_0
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, Lcom/twitter/android/widget/ak;->d:Ljava/lang/ref/WeakReference;

    goto :goto_0
.end method

.method public a(Landroid/widget/ListView;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/aj;->c:Landroid/widget/ListView;

    return-void
.end method

.method public a(Lcom/twitter/library/scribe/ScribeAssociation;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/aj;->d:Lcom/twitter/library/scribe/ScribeAssociation;

    return-void
.end method
