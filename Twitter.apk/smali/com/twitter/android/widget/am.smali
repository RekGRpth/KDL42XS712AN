.class Lcom/twitter/android/widget/am;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/widget/LocalePreference;


# direct methods
.method constructor <init>(Lcom/twitter/android/widget/LocalePreference;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/am;->a:Lcom/twitter/android/widget/LocalePreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/am;->a:Lcom/twitter/android/widget/LocalePreference;

    invoke-static {v0}, Lcom/twitter/android/widget/LocalePreference;->a(Lcom/twitter/android/widget/LocalePreference;)Landroid/widget/Spinner;

    move-result-object v1

    const v0, 0x7f0901b5    # com.twitter.android.R.id.custom_locale

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setEnabled(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
