.class Lcom/twitter/android/widget/h;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/widget/DebugUrlPreference;


# direct methods
.method constructor <init>(Lcom/twitter/android/widget/DebugUrlPreference;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/h;->a:Lcom/twitter/android/widget/DebugUrlPreference;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private a(Ljava/net/URL;)Z
    .locals 4

    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v2, 0x2710

    :try_start_1
    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    const/16 v2, 0x3a98

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    const-string/jumbo v2, "GET"

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->connect()V

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_1
    :goto_0
    return v1

    :catch_0
    move-exception v0

    move-object v0, v2

    :goto_1
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_2
    throw v0

    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_2

    :catch_1
    move-exception v2

    goto :goto_1
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 9

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/h;->a:Lcom/twitter/android/widget/DebugUrlPreference;

    invoke-static {v0}, Lcom/twitter/android/widget/DebugUrlPreference;->a(Lcom/twitter/android/widget/DebugUrlPreference;)Z

    move-result v3

    aget-object v2, p1, v1

    if-eqz v3, :cond_5

    const-string/jumbo v0, "https"

    iget-object v4, p0, Lcom/twitter/android/widget/h;->a:Lcom/twitter/android/widget/DebugUrlPreference;

    invoke-virtual {v4}, Lcom/twitter/android/widget/DebugUrlPreference;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v0, v4}, Lcom/twitter/android/widget/DebugUrlPreference;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/net/URL;

    move-result-object v4

    if-eqz v4, :cond_4

    :try_start_0
    invoke-direct {p0, v4}, Lcom/twitter/android/widget/h;->a(Ljava/net/URL;)Z
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-nez v0, :cond_0

    :try_start_1
    new-instance v5, Ljava/net/URL;

    invoke-virtual {v4}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, ""

    invoke-direct {v5, v6, v7, v8}, Ljava/net/URL;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v5}, Lcom/twitter/android/widget/h;->a(Ljava/net/URL;)Z
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v4}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/twitter/android/widget/h;->a:Lcom/twitter/android/widget/DebugUrlPreference;

    invoke-static {v4}, Lcom/twitter/android/widget/DebugUrlPreference;->d(Lcom/twitter/android/widget/DebugUrlPreference;)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/widget/h;->a:Lcom/twitter/android/widget/DebugUrlPreference;

    invoke-static {v5}, Lcom/twitter/android/widget/DebugUrlPreference;->c(Lcom/twitter/android/widget/DebugUrlPreference;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/widget/h;->a:Lcom/twitter/android/widget/DebugUrlPreference;

    invoke-static {v5}, Lcom/twitter/android/widget/DebugUrlPreference;->b(Lcom/twitter/android/widget/DebugUrlPreference;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    if-eqz v3, :cond_2

    if-eqz v0, :cond_3

    :cond_2
    const/4 v1, 0x1

    :cond_3
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    :catch_1
    move-exception v5

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/h;->a:Lcom/twitter/android/widget/DebugUrlPreference;

    invoke-virtual {v0}, Lcom/twitter/android/widget/DebugUrlPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->h(Landroid/content/Context;)V

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v1, "This is not a valid url."

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    const/16 v1, 0x30

    invoke-virtual {v0, v1, v3, v3}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/twitter/android/widget/h;->a:Lcom/twitter/android/widget/DebugUrlPreference;

    iget-object v0, v0, Lcom/twitter/android/widget/DebugUrlPreference;->a:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/h;->a:Lcom/twitter/android/widget/DebugUrlPreference;

    iget-object v0, v0, Lcom/twitter/android/widget/DebugUrlPreference;->b:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v3}, Landroid/widget/RadioGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/h;->a:Lcom/twitter/android/widget/DebugUrlPreference;

    iget-object v0, v0, Lcom/twitter/android/widget/DebugUrlPreference;->c:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/twitter/android/widget/h;->a([Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/twitter/android/widget/h;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/twitter/android/widget/h;->a:Lcom/twitter/android/widget/DebugUrlPreference;

    iget-object v0, v0, Lcom/twitter/android/widget/DebugUrlPreference;->b:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/h;->a:Lcom/twitter/android/widget/DebugUrlPreference;

    iget-object v0, v0, Lcom/twitter/android/widget/DebugUrlPreference;->c:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/h;->a:Lcom/twitter/android/widget/DebugUrlPreference;

    iget-object v0, v0, Lcom/twitter/android/widget/DebugUrlPreference;->a:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    return-void
.end method
