.class public Lcom/twitter/android/widget/ct;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Lcom/twitter/android/widget/cu;

.field private c:I


# direct methods
.method public constructor <init>(Landroid/widget/TextView;Lcom/twitter/android/widget/cu;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/widget/ct;->c:I

    iput-object p1, p0, Lcom/twitter/android/widget/ct;->a:Landroid/widget/TextView;

    iput-object p2, p0, Lcom/twitter/android/widget/ct;->b:Lcom/twitter/android/widget/cu;

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 13

    const/4 v11, 0x2

    const/4 v2, 0x1

    const/4 v12, -0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/ct;->a:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/ct;->b:Lcom/twitter/android/widget/cu;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    iget-object v3, p0, Lcom/twitter/android/widget/ct;->a:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/widget/ct;->a:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v4

    iget-object v5, p0, Lcom/twitter/android/widget/ct;->a:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v5

    iget-object v6, p0, Lcom/twitter/android/widget/ct;->a:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    move-result v6

    iget-object v7, p0, Lcom/twitter/android/widget/ct;->a:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    move-result v7

    iget-object v8, p0, Lcom/twitter/android/widget/ct;->a:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getWidth()I

    move-result v8

    iget-object v9, p0, Lcom/twitter/android/widget/ct;->a:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getHeight()I

    move-result v9

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v2, v0

    move v0, v1

    :goto_1
    array-length v9, v3

    if-ge v0, v9, :cond_0

    aget-object v9, v3, v0

    if-eqz v9, :cond_3

    if-nez v0, :cond_2

    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    add-int/2addr v9, v4

    add-int/2addr v9, v6

    if-gt v2, v9, :cond_3

    iput v1, p0, Lcom/twitter/android/widget/ct;->c:I

    goto :goto_0

    :cond_2
    if-ne v0, v11, :cond_3

    sub-int v10, v8, v5

    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    sub-int v9, v10, v9

    sub-int/2addr v9, v7

    if-lt v2, v9, :cond_3

    iput v11, p0, Lcom/twitter/android/widget/ct;->c:I

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :pswitch_2
    iget v0, p0, Lcom/twitter/android/widget/ct;->c:I

    if-eq v0, v12, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v10, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iget v11, p0, Lcom/twitter/android/widget/ct;->c:I

    aget-object v3, v3, v11

    if-ge v0, v9, :cond_6

    if-lez v0, :cond_6

    move v0, v2

    :goto_2
    iget v9, p0, Lcom/twitter/android/widget/ct;->c:I

    packed-switch v9, :pswitch_data_1

    :pswitch_3
    move v2, v1

    :cond_4
    :goto_3
    if-eqz v0, :cond_5

    if-eqz v2, :cond_5

    iget-object v0, p0, Lcom/twitter/android/widget/ct;->b:Lcom/twitter/android/widget/cu;

    iget v1, p0, Lcom/twitter/android/widget/ct;->c:I

    invoke-interface {v0, v1}, Lcom/twitter/android/widget/cu;->a(I)Z

    move-result v1

    :cond_5
    iput v12, p0, Lcom/twitter/android/widget/ct;->c:I

    goto/16 :goto_0

    :cond_6
    move v0, v1

    goto :goto_2

    :pswitch_4
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    add-int/2addr v3, v4

    add-int/2addr v3, v6

    if-le v10, v3, :cond_4

    move v2, v1

    goto :goto_3

    :pswitch_5
    sub-int v4, v8, v5

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    sub-int v3, v4, v3

    sub-int/2addr v3, v7

    if-ge v10, v3, :cond_4

    move v2, v1

    goto :goto_3

    :pswitch_6
    iput v12, p0, Lcom/twitter/android/widget/ct;->c:I

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_6
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method
