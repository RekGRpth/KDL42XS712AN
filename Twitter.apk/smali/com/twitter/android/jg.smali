.class Lcom/twitter/android/jg;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/client/ac;
.implements Lcom/twitter/library/client/ae;


# instance fields
.field final synthetic a:Lcom/twitter/android/LoginActivity;


# direct methods
.method public constructor <init>(Lcom/twitter/android/LoginActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/twitter/library/client/Session;I[IZ)V
    .locals 10

    const v9, 0x7f0f0232    # com.twitter.android.R.string.login_forgot_password

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    invoke-virtual {v0}, Lcom/twitter/android/LoginActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    invoke-virtual {v0, v7}, Lcom/twitter/android/LoginActivity;->removeDialog(I)V

    iget-object v0, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    invoke-static {v0}, Lcom/twitter/android/LoginActivity;->e(Lcom/twitter/android/LoginActivity;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    if-ne p2, v8, :cond_4

    const v1, 0x7f0f0495    # com.twitter.android.R.string.sync_contacts_account_create_error

    iget-object v0, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    iget-boolean v0, v0, Lcom/twitter/android/LoginActivity;->a:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    invoke-virtual {v0}, Lcom/twitter/android/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v5, "accountAuthenticatorResponse"

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountAuthenticatorResponse;

    if-eqz v0, :cond_2

    const/16 v5, 0x190

    iget-object v6, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    invoke-virtual {v6, v1}, Lcom/twitter/android/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/accounts/AccountAuthenticatorResponse;->onError(ILjava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    invoke-static {v0}, Lcom/twitter/android/LoginActivity;->f(Lcom/twitter/android/LoginActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    new-array v5, v7, [Ljava/lang/String;

    const-string/jumbo v6, "login::::failure"

    aput-object v6, v5, v2

    invoke-virtual {v0, v3, v4, v5}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    invoke-static {v1, v0, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    invoke-static {v0}, Lcom/twitter/android/LoginActivity;->j(Lcom/twitter/android/LoginActivity;)I

    move-result v0

    const/4 v1, 0x4

    if-lt v0, v1, :cond_8

    iget-object v0, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    invoke-static {v0, v2}, Lcom/twitter/android/LoginActivity;->a(Lcom/twitter/android/LoginActivity;I)I

    invoke-static {v8}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0571    # com.twitter.android.R.string.yes

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f029d    # com.twitter.android.R.string.no

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    invoke-virtual {v1}, Lcom/twitter/android/LoginActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    iget-object v0, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    invoke-static {v0}, Lcom/twitter/android/LoginActivity;->k(Lcom/twitter/android/LoginActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v5, "login::forgot_password_prompt::impression"

    aput-object v5, v1, v2

    invoke-virtual {v0, v3, v4, v1}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    if-eqz p3, :cond_5

    array-length v0, p3

    if-nez v0, :cond_6

    :cond_5
    move v0, v2

    :goto_2
    sparse-switch v0, :sswitch_data_0

    const v0, 0x7f0f0231    # com.twitter.android.R.string.login_error_generic

    goto :goto_1

    :cond_6
    aget v0, p3, v2

    goto :goto_2

    :sswitch_0
    iget-object v0, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    invoke-static {v0}, Lcom/twitter/android/LoginActivity;->g(Lcom/twitter/android/LoginActivity;)I

    if-nez p4, :cond_7

    const v0, 0x7f0f0230    # com.twitter.android.R.string.login_error_authentication

    goto :goto_1

    :cond_7
    move v0, v2

    goto :goto_1

    :sswitch_1
    iget-object v0, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    invoke-static {v0}, Lcom/twitter/android/LoginActivity;->h(Lcom/twitter/android/LoginActivity;)V

    goto/16 :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    invoke-static {v0}, Lcom/twitter/android/LoginActivity;->i(Lcom/twitter/android/LoginActivity;)V

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    invoke-static {v0}, Lcom/twitter/android/LoginActivity;->l(Lcom/twitter/android/LoginActivity;)I

    move-result v0

    if-ne v0, v7, :cond_0

    iget-object v0, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    invoke-static {v0}, Lcom/twitter/android/LoginActivity;->j(Lcom/twitter/android/LoginActivity;)I

    move-result v0

    if-lez v0, :cond_0

    if-eqz p4, :cond_0

    const/4 v0, 0x5

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f02de    # com.twitter.android.R.string.one_factor_dialog

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f04a7    # com.twitter.android.R.string.text_message

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f02d5    # com.twitter.android.R.string.ok

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    invoke-virtual {v1}, Lcom/twitter/android/LoginActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_0
        0xe7 -> :sswitch_1
        0xf4 -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;II[IZ)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p4, v1}, Lcom/twitter/android/jg;->a(Lcom/twitter/library/client/Session;I[IZ)V

    iget-object v0, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    invoke-static {v0, v1}, Lcom/twitter/android/LoginActivity;->a(Lcom/twitter/android/LoginActivity;Z)Z

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;I[I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/android/jg;->a(Lcom/twitter/library/client/Session;I[IZ)V

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Lcom/twitter/library/network/LoginVerificationRequiredResponse;)V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    invoke-virtual {v0}, Lcom/twitter/android/LoginActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    invoke-virtual {v0, v3}, Lcom/twitter/android/LoginActivity;->removeDialog(I)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    const-class v2, Lcom/twitter/android/VerifyLoginActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "login_verification_required_response"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "username"

    iget-object v2, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    invoke-static {v2}, Lcom/twitter/android/LoginActivity;->c(Lcom/twitter/android/LoginActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "session_id"

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    invoke-static {v1}, Lcom/twitter/android/LoginActivity;->d(Lcom/twitter/android/LoginActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    invoke-virtual {v1}, Lcom/twitter/android/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "android.intent.extra.INTENT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "start_main"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/twitter/android/LoginActivity;->a(Lcom/twitter/android/LoginActivity;Z)Z

    iget-object v1, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    invoke-virtual {v0}, Lcom/twitter/android/LoginActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/LoginActivity;->removeDialog(I)V

    iget-object v0, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/LoginActivity;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;Z)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    invoke-virtual {v0}, Lcom/twitter/android/LoginActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/LoginActivity;->removeDialog(I)V

    iget-object v0, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/LoginActivity;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/LoginActivity;->a(Lcom/twitter/android/LoginActivity;Z)Z

    goto :goto_0
.end method

.method public b(Lcom/twitter/library/client/Session;I[I)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/android/jg;->a(Lcom/twitter/library/client/Session;I[IZ)V

    return-void
.end method

.method public b(Lcom/twitter/library/client/Session;Lcom/twitter/library/network/LoginVerificationRequiredResponse;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    invoke-virtual {v0}, Lcom/twitter/android/LoginActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/LoginActivity;->removeDialog(I)V

    iget-object v0, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    const-class v3, Lcom/twitter/android/LoginChallengeActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "login_challenge_required_response"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "username"

    iget-object v3, p0, Lcom/twitter/android/jg;->a:Lcom/twitter/android/LoginActivity;

    invoke-static {v3}, Lcom/twitter/android/LoginActivity;->c(Lcom/twitter/android/LoginActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "session_id"

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method
