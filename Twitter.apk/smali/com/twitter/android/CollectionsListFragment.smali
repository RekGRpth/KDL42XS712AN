.class public Lcom/twitter/android/CollectionsListFragment;
.super Lcom/twitter/android/client/BaseListFragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/ty;


# instance fields
.field private a:I

.field private b:J

.field private c:Z

.field private d:Z

.field private e:Lcom/twitter/android/cw;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseListFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;IILcom/twitter/library/service/b;)V
    .locals 2

    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/android/client/BaseListFragment;->a(Landroid/content/Context;IILcom/twitter/library/service/b;)V

    invoke-virtual {p4}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f0f00a3    # com.twitter.android.R.string.collections_fetch_error

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    check-cast p4, Ljf;

    invoke-virtual {p4}, Ljf;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/CollectionsListFragment;->c:Z

    goto :goto_0
.end method

.method protected a(Landroid/database/Cursor;)V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/CollectionsListFragment;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/CollectionsListFragment;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/CollectionsListFragment;->a(I)Z

    :cond_0
    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2

    const/4 v1, 0x3

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseListFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    iget-boolean v0, p0, Lcom/twitter/android/CollectionsListFragment;->d:Z

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p0, v1}, Lcom/twitter/android/CollectionsListFragment;->a(I)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/CollectionsListFragment;->d:Z

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, v1}, Lcom/twitter/android/CollectionsListFragment;->b(I)V

    goto :goto_0
.end method

.method protected a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 4

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f090118    # com.twitter.android.R.id.create_new_item

    if-ne v0, v1, :cond_1

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/CollectionsListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/CollectionCreateEditActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/CollectionsListFragment;->startActivity(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/e;

    invoke-virtual {p1}, Landroid/widget/ListView;->getChoiceMode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/CollectionsListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/twitter/android/CollectionPermalinkActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "type"

    const/16 v3, 0x1b

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "timeline_tag"

    iget-object v0, v0, Lcom/twitter/android/widget/e;->c:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/twitter/android/CollectionsListFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/twitter/android/CollectionsListFragment;->e:Lcom/twitter/android/cw;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/CollectionsListFragment;->e:Lcom/twitter/android/cw;

    iget-object v0, v0, Lcom/twitter/android/widget/e;->c:Ljava/lang/String;

    invoke-interface {v1, v0}, Lcom/twitter/android/cw;->a(Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcom/twitter/android/cw;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/CollectionsListFragment;->e:Lcom/twitter/android/cw;

    return-void
.end method

.method protected a(Z)V
    .locals 2

    const/4 v1, 0x3

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->a(Z)V

    if-eqz p1, :cond_1

    invoke-virtual {p0, v1}, Lcom/twitter/android/CollectionsListFragment;->a_(I)V

    invoke-virtual {p0}, Lcom/twitter/android/CollectionsListFragment;->P()Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/CollectionsListFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {p0, v1}, Lcom/twitter/android/CollectionsListFragment;->a_(I)V

    invoke-virtual {p0}, Lcom/twitter/android/CollectionsListFragment;->p()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/CollectionsListFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/twitter/android/CollectionsListFragment;->a(I)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/CollectionsListFragment;->d:Z

    goto :goto_0
.end method

.method protected a(I)Z
    .locals 8

    const/4 v1, 0x0

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    move v5, v0

    :goto_0
    iget v2, p0, Lcom/twitter/android/CollectionsListFragment;->a:I

    if-nez v2, :cond_1

    const/16 v2, 0xc8

    move v6, v2

    :goto_1
    iget v2, p0, Lcom/twitter/android/CollectionsListFragment;->a:I

    if-nez v2, :cond_2

    move v7, v1

    :goto_2
    new-instance v0, Ljf;

    invoke-virtual {p0}, Lcom/twitter/android/CollectionsListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/CollectionsListFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    iget-wide v3, p0, Lcom/twitter/android/CollectionsListFragment;->b:J

    invoke-direct/range {v0 .. v5}, Ljf;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JI)V

    invoke-virtual {v0, v7}, Ljf;->c(I)Lcom/twitter/library/service/b;

    invoke-virtual {p0, v0, v6, p1}, Lcom/twitter/android/CollectionsListFragment;->a(Lcom/twitter/library/service/b;II)Z

    move-result v0

    return v0

    :cond_0
    move v5, v1

    goto :goto_0

    :cond_1
    const/16 v2, 0xc9

    move v6, v2

    goto :goto_1

    :cond_2
    move v7, v0

    goto :goto_2
.end method

.method public i()V
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/android/CollectionsListFragment;->a(I)Z

    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    if-eqz p1, :cond_1

    const-string/jumbo v0, "user_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/CollectionsListFragment;->b:J

    const-string/jumbo v0, "is_last"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/CollectionsListFragment;->c:Z

    const-string/jumbo v0, "list_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/CollectionsListFragment;->a:I

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/CollectionsListFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/CollectionsListFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    new-instance v2, Lcom/twitter/android/cv;

    invoke-virtual {p0}, Lcom/twitter/android/CollectionsListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3, v0, v1}, Lcom/twitter/android/cv;-><init>(Landroid/content/Context;J)V

    iput-object v2, p0, Lcom/twitter/android/CollectionsListFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/CollectionsListFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/CollectionsListFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/CollectionsListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/CollectionsListFragment;->b:J

    invoke-virtual {p0}, Lcom/twitter/android/CollectionsListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "list_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/CollectionsListFragment;->a:I

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/CollectionsListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "empty_title"

    const v2, 0x7f0f00a7    # com.twitter.android.R.string.collections_no_content

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v1, "empty_desc"

    const v2, 0x7f0f00a6    # com.twitter.android.R.string.collections_no_collection_created

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 8

    iget v0, p0, Lcom/twitter/android/CollectionsListFragment;->a:I

    if-nez v0, :cond_0

    const-string/jumbo v4, "ev_type=6 AND list_mapping_user_id=? AND list_mapping_type=0"

    :goto_0
    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-wide v1, p0, Lcom/twitter/android/CollectionsListFragment;->b:J

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v0

    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/twitter/android/CollectionsListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/provider/ai;->a:Landroid/net/Uri;

    iget-wide v6, p0, Lcom/twitter/android/CollectionsListFragment;->Q:J

    invoke-static {v2, v6, v7}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/library/provider/cf;->a:[Ljava/lang/String;

    const-string/jumbo v6, "ev_title ASC "

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_0
    const-string/jumbo v4, "ev_type=6 AND list_mapping_user_id=? AND list_mapping_type=2"

    goto :goto_0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/CollectionsListFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onResume()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/CollectionsListFragment;->a(Z)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "user_id"

    iget-wide v1, p0, Lcom/twitter/android/CollectionsListFragment;->b:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v0, "is_last"

    iget-boolean v1, p0, Lcom/twitter/android/CollectionsListFragment;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "list_type"

    iget v1, p0, Lcom/twitter/android/CollectionsListFragment;->a:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public u_()V
    .locals 0

    return-void
.end method
