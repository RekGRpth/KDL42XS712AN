.class public abstract Lcom/twitter/android/SearchFragment;
.super Lcom/twitter/android/TweetListFragment;
.source "Twttr"


# static fields
.field protected static final a:Landroid/util/SparseArray;


# instance fields
.field private final J:Ljava/lang/Runnable;

.field private K:I

.field private L:Z

.field private U:Z

.field private V:Z

.field private W:Lcom/twitter/android/th;

.field private X:Landroid/content/SharedPreferences;

.field protected b:Ljava/lang/String;

.field protected c:Ljava/lang/String;

.field protected d:Ljava/lang/String;

.field protected e:Ljava/lang/String;

.field protected f:Ljava/lang/String;

.field protected g:Ljava/lang/String;

.field protected h:Ljava/lang/String;

.field protected i:Z

.field protected j:Z

.field protected k:Z

.field protected l:Z

.field protected m:Z

.field protected n:I

.field protected o:Ljava/lang/String;

.field protected p:Lcom/twitter/android/se;

.field protected q:J

.field protected r:Z

.field protected s:I

.field protected t:I

.field protected u:I

.field protected v:Z

.field protected w:Z

.field protected x:Z

.field protected y:J

.field private final z:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Landroid/util/SparseArray;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    sput-object v0, Lcom/twitter/android/SearchFragment;->a:Landroid/util/SparseArray;

    sget-object v0, Lcom/twitter/android/SearchFragment;->a:Landroid/util/SparseArray;

    const/4 v1, 0x0

    const-wide/16 v2, 0x2710

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/android/SearchFragment;->a:Landroid/util/SparseArray;

    const/4 v1, 0x1

    const-wide/16 v2, 0x7530

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/android/SearchFragment;->a:Landroid/util/SparseArray;

    const/4 v1, 0x2

    const-wide/32 v2, 0xea60

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/android/SearchFragment;->a:Landroid/util/SparseArray;

    const/4 v1, 0x3

    const-wide/32 v2, 0x1d4c0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/TweetListFragment;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/SearchFragment;->z:Landroid/os/Handler;

    new-instance v0, Lcom/twitter/android/sd;

    invoke-direct {v0, p0}, Lcom/twitter/android/sd;-><init>(Lcom/twitter/android/SearchFragment;)V

    iput-object v0, p0, Lcom/twitter/android/SearchFragment;->J:Ljava/lang/Runnable;

    return-void
.end method

.method public static d_(I)Ljava/lang/String;
    .locals 1

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    const-string/jumbo v0, "everything"

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "people"

    goto :goto_0

    :pswitch_2
    const-string/jumbo v0, "photos"

    goto :goto_0

    :pswitch_3
    const-string/jumbo v0, "video"

    goto :goto_0

    :pswitch_4
    const-string/jumbo v0, "news"

    goto :goto_0

    :pswitch_5
    const-string/jumbo v0, "timeline"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public A()Lcom/twitter/refresh/widget/a;
    .locals 6

    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->A()Lcom/twitter/refresh/widget/a;

    move-result-object v1

    iget-wide v2, v1, Lcom/twitter/refresh/widget/a;->b:J

    const-wide/32 v4, -0x80000000

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->z()I

    move-result v0

    const/4 v2, 0x1

    if-le v0, v2, :cond_0

    iget v0, v1, Lcom/twitter/refresh/widget/a;->a:I

    add-int/lit8 v2, v0, 0x1

    new-instance v0, Lcom/twitter/refresh/widget/a;

    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->V()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/ListView;->getItemIdAtPosition(I)J

    move-result-wide v3

    iget v1, v1, Lcom/twitter/refresh/widget/a;->c:I

    invoke-direct {v0, v2, v3, v4, v1}, Lcom/twitter/refresh/widget/a;-><init>(IJI)V

    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public C()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->c:Ljava/lang/String;

    return-object v0
.end method

.method public D()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->e:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public E()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/SearchFragment;->i:Z

    return v0
.end method

.method public F()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/SearchFragment;->j:Z

    return v0
.end method

.method protected G()I
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/SearchFragment;->L:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/twitter/android/SearchFragment;->K:I

    goto :goto_0
.end method

.method public H()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/SearchFragment;->U:Z

    return v0
.end method

.method public I()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/SearchFragment;->V:Z

    return v0
.end method

.method public J()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->W:Lcom/twitter/android/th;

    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->V()Landroid/widget/ListView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/th;->a(Landroid/widget/ListView;I)V

    return-void
.end method

.method public K()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->W:Lcom/twitter/android/th;

    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->V()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/th;->b(Landroid/widget/ListView;)V

    return-void
.end method

.method public abstract L()I
.end method

.method protected abstract M()Ljava/lang/String;
.end method

.method protected abstract N()V
.end method

.method protected abstract O()V
.end method

.method public a(J)I
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/SearchFragment;->b(J)I

    move-result v1

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    add-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    const v0, 0x7f03008c    # com.twitter.android.R.layout.grouped_list_header_fragment

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/android/SearchFragment;->a(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected abstract a(Landroid/content/Context;)V
.end method

.method protected a(Landroid/content/Context;IILcom/twitter/library/service/b;)V
    .locals 2

    const/4 v0, 0x1

    if-ne p2, v0, :cond_2

    iget-object v0, p4, Lcom/twitter/library/service/b;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    invoke-virtual {p0, p3}, Lcom/twitter/android/SearchFragment;->b(I)V

    check-cast p4, Lcom/twitter/library/api/search/c;

    invoke-virtual {p4}, Lcom/twitter/library/api/search/c;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/android/SearchFragment;->t:I

    invoke-virtual {p4}, Lcom/twitter/library/api/search/c;->e()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/twitter/android/SearchFragment;->t:I

    invoke-virtual {p4}, Lcom/twitter/library/api/search/c;->f()I

    move-result v0

    if-lez v0, :cond_0

    iget v1, p0, Lcom/twitter/android/SearchFragment;->s:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/twitter/android/SearchFragment;->s:I

    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->p:Lcom/twitter/android/se;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->p:Lcom/twitter/android/se;

    iget v1, p0, Lcom/twitter/android/SearchFragment;->s:I

    invoke-interface {v0, v1}, Lcom/twitter/android/se;->a(I)V

    :cond_0
    iget v0, p0, Lcom/twitter/android/SearchFragment;->u:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/android/SearchFragment;->u:I

    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->q()V

    invoke-virtual {p4}, Lcom/twitter/library/api/search/c;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchFragment;->a(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p4}, Lcom/twitter/library/api/search/c;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/SearchFragment;->y:J

    :cond_2
    return-void
.end method

.method protected a(Landroid/database/Cursor;)V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/SearchFragment;->r:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->z()I

    move-result v0

    const/16 v1, 0x190

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchFragment;->d(I)Z

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/android/se;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/SearchFragment;->p:Lcom/twitter/android/se;

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/twitter/android/SearchFragment;->s:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/twitter/android/SearchFragment;->s:I

    invoke-interface {p1, v0}, Lcom/twitter/android/se;->a(I)V

    :cond_0
    return-void
.end method

.method protected a(Lcom/twitter/refresh/widget/a;Z)V
    .locals 3

    iget-wide v0, p1, Lcom/twitter/refresh/widget/a;->b:J

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/SearchFragment;->b(J)I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->V()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    if-nez p2, :cond_1

    :cond_0
    iget v2, p1, Lcom/twitter/refresh/widget/a;->c:I

    invoke-virtual {v1, v0, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    :cond_1
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->ao()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/ToolBar;->setSubtitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public a(Landroid/widget/AbsListView;III)Z
    .locals 2

    const/4 v1, 0x1

    if-nez p3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    if-nez p2, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->al()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->z()I

    move-result v0

    if-lez v0, :cond_0

    if-lez p2, :cond_0

    add-int v0, p2, p3

    if-lt v0, p4, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchFragment;->a(Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method protected abstract b(J)I
.end method

.method protected b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    const v0, 0x7f030090    # com.twitter.android.R.layout.grouped_msg_list_fragment

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/android/SearchFragment;->a(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected abstract b(Ljava/lang/String;)V
.end method

.method protected c()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->c()V

    iget-boolean v0, p0, Lcom/twitter/android/SearchFragment;->w:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchFragment;->a(Z)V

    :cond_0
    return-void
.end method

.method public d(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/SearchFragment;->L:Z

    return-void
.end method

.method protected abstract d(I)Z
.end method

.method public e()V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->smoothScrollToPosition(I)V

    iget v0, p0, Lcom/twitter/android/SearchFragment;->t:I

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->v()V

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/SearchFragment;->h:Ljava/lang/String;

    aput-object v1, v0, v2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->M()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "new_tweet_prompt"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "click"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchFragment;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected i()V
    .locals 1

    iget v0, p0, Lcom/twitter/android/SearchFragment;->s:I

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->v()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchFragment;->d(I)Z

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v0, "query"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    :cond_0
    iput-object v0, p0, Lcom/twitter/android/SearchFragment;->c:Ljava/lang/String;

    const-string/jumbo v2, "query_name"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    iput-object v2, p0, Lcom/twitter/android/SearchFragment;->b:Ljava/lang/String;

    :goto_0
    const-string/jumbo v0, "q_type"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/SearchFragment;->K:I

    const-string/jumbo v0, "recent"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchFragment;->k:Z

    const-string/jumbo v0, "realtime"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchFragment;->l:Z

    const-string/jumbo v0, "follows"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchFragment;->j:Z

    const-string/jumbo v0, "near"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchFragment;->i:Z

    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->E:Lcom/twitter/android/xr;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->E:Lcom/twitter/android/xr;

    iget-object v1, p0, Lcom/twitter/android/SearchFragment;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/xr;->a(Ljava/lang/String;)V

    :cond_1
    iget-boolean v0, p0, Lcom/twitter/android/SearchFragment;->x:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->W:Lcom/twitter/android/th;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c009d    # com.twitter.android.R.dimen.nav_bar_height

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    new-instance v1, Lcom/twitter/android/th;

    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/twitter/android/th;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/twitter/android/SearchFragment;->W:Lcom/twitter/android/th;

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->W:Lcom/twitter/android/th;

    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->V()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/th;->a(Landroid/widget/ListView;)V

    :cond_3
    return-void

    :cond_4
    iput-object v0, p0, Lcom/twitter/android/SearchFragment;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_1

    const-string/jumbo v2, "search_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/android/SearchFragment;->q:J

    const-string/jumbo v2, "is_last"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/android/SearchFragment;->r:Z

    const-string/jumbo v2, "q_source"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/SearchFragment;->d:Ljava/lang/String;

    const-string/jumbo v2, "polled_organic_count"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/twitter/android/SearchFragment;->s:I

    const-string/jumbo v2, "polled_total_count"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/twitter/android/SearchFragment;->t:I

    const-string/jumbo v2, "poll_count"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/twitter/android/SearchFragment;->u:I

    const-string/jumbo v2, "should_poll"

    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/android/SearchFragment;->v:Z

    const-string/jumbo v2, "should_refresh"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/android/SearchFragment;->w:Z

    const-string/jumbo v2, "follows"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/android/SearchFragment;->j:Z

    const-string/jumbo v2, "near"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/android/SearchFragment;->i:Z

    const-string/jumbo v2, "terminal"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/android/SearchFragment;->U:Z

    const-string/jumbo v2, "search_button"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/android/SearchFragment;->V:Z

    const-string/jumbo v2, "seed_hashtag"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/SearchFragment;->e:Ljava/lang/String;

    const-string/jumbo v2, "query_name"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/SearchFragment;->b:Ljava/lang/String;

    const-string/jumbo v2, "timeline_type"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/SearchFragment;->f:Ljava/lang/String;

    const-string/jumbo v2, "experiments"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/SearchFragment;->g:Ljava/lang/String;

    const-string/jumbo v2, "should_shim"

    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/android/SearchFragment;->x:Z

    const-string/jumbo v2, "scribe_page"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/SearchFragment;->h:Ljava/lang/String;

    const-string/jumbo v2, "notification_setting_key"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/SearchFragment;->o:Ljava/lang/String;

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/twitter/android/SearchFragment;->o:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/android/SearchFragment;->m:Z

    const/4 v0, 0x3

    iput v0, p0, Lcom/twitter/android/SearchFragment;->n:I

    invoke-virtual {p0, p0}, Lcom/twitter/android/SearchFragment;->a(Lcom/twitter/android/client/ah;)V

    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string/jumbo v2, "search"

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchFragment;->X:Landroid/content/SharedPreferences;

    return-void

    :cond_1
    iput-boolean v1, p0, Lcom/twitter/android/SearchFragment;->r:Z

    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "search_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/twitter/android/SearchFragment;->q:J

    const-string/jumbo v3, "q_source"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/android/SearchFragment;->d:Ljava/lang/String;

    const-string/jumbo v3, "terminal"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/twitter/android/SearchFragment;->U:Z

    const-string/jumbo v3, "search_button"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/twitter/android/SearchFragment;->V:Z

    const-string/jumbo v3, "should_poll"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/twitter/android/SearchFragment;->v:Z

    const-string/jumbo v3, "should_refresh"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/twitter/android/SearchFragment;->w:Z

    const-string/jumbo v3, "seed_hashtag"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/android/SearchFragment;->e:Ljava/lang/String;

    const-string/jumbo v3, "query_name"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/android/SearchFragment;->b:Ljava/lang/String;

    const-string/jumbo v3, "timeline_type"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/android/SearchFragment;->f:Ljava/lang/String;

    const-string/jumbo v3, "experiments"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/android/SearchFragment;->g:Ljava/lang/String;

    const-string/jumbo v3, "should_shim"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/twitter/android/SearchFragment;->x:Z

    const-string/jumbo v3, "scribe_page"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/android/SearchFragment;->h:Ljava/lang/String;

    const-string/jumbo v3, "follows"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/twitter/android/SearchFragment;->j:Z

    const-string/jumbo v3, "near"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/twitter/android/SearchFragment;->i:Z

    const-string/jumbo v3, "notification_setting_key"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/SearchFragment;->o:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/SearchFragment;->h:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string/jumbo v2, "search"

    iput-object v2, p0, Lcom/twitter/android/SearchFragment;->h:Ljava/lang/String;

    :cond_2
    iget-object v2, p0, Lcom/twitter/android/SearchFragment;->d:Ljava/lang/String;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/SearchFragment;->o:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/twitter/android/SearchFragment;->o:Ljava/lang/String;

    const/4 v2, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :cond_3
    :goto_2
    packed-switch v2, :pswitch_data_1

    goto/16 :goto_0

    :pswitch_0
    const-string/jumbo v2, "evpa"

    iput-object v2, p0, Lcom/twitter/android/SearchFragment;->d:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_1
    const-string/jumbo v4, "event_parrot"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    move v2, v1

    goto :goto_2

    :cond_4
    move v0, v1

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x390582db
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->onResume()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchFragment;->a(Z)V

    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->N()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "is_last"

    iget-boolean v1, p0, Lcom/twitter/android/SearchFragment;->r:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "q_source"

    iget-object v1, p0, Lcom/twitter/android/SearchFragment;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "polled_organic_count"

    iget v1, p0, Lcom/twitter/android/SearchFragment;->s:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "polled_total_count"

    iget v1, p0, Lcom/twitter/android/SearchFragment;->t:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "poll_count"

    iget v1, p0, Lcom/twitter/android/SearchFragment;->u:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "should_poll"

    iget-boolean v1, p0, Lcom/twitter/android/SearchFragment;->v:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "should_refresh"

    iget-boolean v1, p0, Lcom/twitter/android/SearchFragment;->w:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "follows"

    iget-boolean v1, p0, Lcom/twitter/android/SearchFragment;->j:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "near"

    iget-boolean v1, p0, Lcom/twitter/android/SearchFragment;->i:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "terminal"

    iget-boolean v1, p0, Lcom/twitter/android/SearchFragment;->U:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "search_button"

    iget-boolean v1, p0, Lcom/twitter/android/SearchFragment;->V:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "should_shim"

    iget-boolean v1, p0, Lcom/twitter/android/SearchFragment;->x:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "query_name"

    iget-object v1, p0, Lcom/twitter/android/SearchFragment;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "seed_hashtag"

    iget-object v1, p0, Lcom/twitter/android/SearchFragment;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "timeline_type"

    iget-object v1, p0, Lcom/twitter/android/SearchFragment;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "experiments"

    iget-object v1, p0, Lcom/twitter/android/SearchFragment;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "scribe_page"

    iget-object v1, p0, Lcom/twitter/android/SearchFragment;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "notification_setting_key"

    iget-object v1, p0, Lcom/twitter/android/SearchFragment;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onStop()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->z:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/android/SearchFragment;->J:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->O()V

    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->onStop()V

    return-void
.end method

.method protected q()V
    .locals 4

    iget-boolean v0, p0, Lcom/twitter/android/SearchFragment;->v:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->u()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-wide v0, p0, Lcom/twitter/android/SearchFragment;->y:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    iget-wide v0, p0, Lcom/twitter/android/SearchFragment;->y:J

    :goto_1
    iget-object v2, p0, Lcom/twitter/android/SearchFragment;->z:Landroid/os/Handler;

    iget-object v3, p0, Lcom/twitter/android/SearchFragment;->J:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v2, p0, Lcom/twitter/android/SearchFragment;->z:Landroid/os/Handler;

    iget-object v3, p0, Lcom/twitter/android/SearchFragment;->J:Ljava/lang/Runnable;

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/twitter/android/SearchFragment;->a:Landroid/util/SparseArray;

    iget v1, p0, Lcom/twitter/android/SearchFragment;->u:I

    const-wide/32 v2, 0x493e0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_1
.end method

.method public q_()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->b:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method protected r()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->X:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "refresh_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method protected s()Z
    .locals 6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/twitter/android/SearchFragment;->X:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "refresh_time"

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iget-boolean v4, p0, Lcom/twitter/android/SearchFragment;->w:Z

    if-eqz v4, :cond_0

    const-wide/32 v4, 0xdbba0

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract u()Z
.end method

.method protected v()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->p:Lcom/twitter/android/se;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/SearchFragment;->p:Lcom/twitter/android/se;

    invoke-interface {v0}, Lcom/twitter/android/se;->a()V

    :cond_0
    const/4 v0, 0x4

    iput v0, p0, Lcom/twitter/android/SearchFragment;->n:I

    iget v0, p0, Lcom/twitter/android/SearchFragment;->n:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchFragment;->a_(I)V

    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->ak()V

    invoke-virtual {p0}, Lcom/twitter/android/SearchFragment;->y()V

    iput v1, p0, Lcom/twitter/android/SearchFragment;->u:I

    iput v1, p0, Lcom/twitter/android/SearchFragment;->s:I

    iput v1, p0, Lcom/twitter/android/SearchFragment;->t:I

    return-void
.end method

.method protected abstract y()V
.end method

.method protected abstract z()I
.end method
