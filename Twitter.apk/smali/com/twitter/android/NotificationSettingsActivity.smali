.class public Lcom/twitter/android/NotificationSettingsActivity;
.super Lcom/twitter/android/client/BasePreferenceActivity;
.source "Twttr"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;
.implements Lcom/twitter/library/featureswitch/g;


# instance fields
.field A:Ljava/lang/String;

.field B:Z

.field C:Landroid/preference/PreferenceCategory;

.field D:Landroid/preference/Preference;

.field E:Landroid/preference/Preference;

.field F:Landroid/preference/Preference;

.field G:Landroid/preference/Preference;

.field H:Landroid/preference/Preference;

.field I:Landroid/preference/Preference;

.field J:Landroid/preference/Preference;

.field K:Landroid/preference/Preference;

.field L:Landroid/preference/Preference;

.field M:Landroid/preference/Preference;

.field N:Landroid/preference/Preference;

.field O:Landroid/preference/Preference;

.field private R:Z

.field private S:Landroid/preference/CheckBoxPreference;

.field private T:Lcom/twitter/android/nw;

.field private U:Z

.field a:Ljava/lang/String;

.field b:Lcom/twitter/library/client/Session;

.field c:Z

.field d:Ljava/lang/String;

.field e:Z

.field f:I

.field g:Z

.field h:I

.field i:Z

.field j:Z

.field k:I

.field l:I

.field m:I

.field n:I

.field o:I

.field p:I

.field q:I

.field r:I

.field s:I

.field t:I

.field u:I

.field v:I

.field w:I

.field x:I

.field y:Z

.field z:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BasePreferenceActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/NotificationSettingsActivity;)Landroid/preference/CheckBoxPreference;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->S:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method private a(Landroid/preference/Preference;I)V
    .locals 1

    if-nez p2, :cond_1

    const v0, 0x7f0f040e    # com.twitter.android.R.string.settings_notif_tweets_summary_off

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    if-ne p2, v0, :cond_2

    const v0, 0x7f0f0400    # com.twitter.android.R.string.settings_notif_rtfav_good_summary

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(I)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    const v0, 0x7f0f03ff    # com.twitter.android.R.string.settings_notif_rtfav_all_summary

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(I)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->Q:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->Q:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->Q:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/NotificationSettingsActivity;->d()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private e()V
    .locals 4

    iget-boolean v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->U:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/NotificationSettingsActivity;->c()Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/NotificationSettingsActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/library/api/UserSettings;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-boolean v2, p0, Lcom/twitter/android/NotificationSettingsActivity;->e:Z

    iput-boolean v2, v1, Lcom/twitter/library/api/UserSettings;->o:Z

    iget-object v2, p0, Lcom/twitter/android/NotificationSettingsActivity;->Q:Lcom/twitter/android/client/c;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v1, v3}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/UserSettings;Z)Ljava/lang/String;

    :cond_0
    return-void
.end method

.method static synthetic f(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->Q:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/NotificationSettingsActivity;->d()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->Q:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/NotificationSettingsActivity;->d()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->Q:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/NotificationSettingsActivity;->d()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic l(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->Q:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method static synthetic m(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/NotificationSettingsActivity;->d()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic n(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->Q:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method static synthetic o(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/NotificationSettingsActivity;->d()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic p(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->Q:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method static synthetic q(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/NotificationSettingsActivity;->d()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic r(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->Q:Lcom/twitter/android/client/c;

    return-object v0
.end method


# virtual methods
.method protected a()V
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/android/NotificationSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iget-object v2, p0, Lcom/twitter/android/NotificationSettingsActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->b:Lcom/twitter/library/client/Session;

    invoke-static {v0}, Lcom/twitter/library/featureswitch/a;->a(Lcom/twitter/library/client/Session;)V

    :cond_0
    return-void
.end method

.method a(IZ)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->F:Landroid/preference/Preference;

    invoke-direct {p0, v0, p1}, Lcom/twitter/android/NotificationSettingsActivity;->a(Landroid/preference/Preference;I)V

    iput p1, p0, Lcom/twitter/android/NotificationSettingsActivity;->f:I

    iput-boolean p2, p0, Lcom/twitter/android/NotificationSettingsActivity;->g:Z

    return-void
.end method

.method public a(J)V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/NotificationSettingsActivity;->b()V

    return-void
.end method

.method a(ZIZ)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p3, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->e:Z

    if-eq v0, p1, :cond_1

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/twitter/android/NotificationSettingsActivity;->D:Landroid/preference/Preference;

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/NotificationSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e0014    # com.twitter.android.R.plurals.settings_notif_tweets_count

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v1, v2

    invoke-virtual {v4, v5, p2, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_1
    iput-boolean p1, p0, Lcom/twitter/android/NotificationSettingsActivity;->e:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/NotificationSettingsActivity;->e()V

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    const v1, 0x7f0f040e    # com.twitter.android.R.string.settings_notif_tweets_summary_off

    invoke-virtual {v3, v1}, Landroid/preference/Preference;->setSummary(I)V

    goto :goto_1
.end method

.method protected b()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->C:Landroid/preference/PreferenceCategory;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/util/ag;->a(Lcom/twitter/library/api/TwitterUser;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->C:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/twitter/android/NotificationSettingsActivity;->O:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->O:Landroid/preference/Preference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->O:Landroid/preference/Preference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->C:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/twitter/android/NotificationSettingsActivity;->O:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method b(IZ)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->G:Landroid/preference/Preference;

    invoke-direct {p0, v0, p1}, Lcom/twitter/android/NotificationSettingsActivity;->a(Landroid/preference/Preference;I)V

    iput p1, p0, Lcom/twitter/android/NotificationSettingsActivity;->h:I

    iput-boolean p2, p0, Lcom/twitter/android/NotificationSettingsActivity;->i:Z

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v1, -0x1

    const/4 v2, 0x0

    if-ne p1, v3, :cond_1

    if-ne p2, v1, :cond_0

    const-string/jumbo v0, "enabled"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "enabled"

    invoke-virtual {p3, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    const-string/jumbo v1, "count"

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {p0, v0, v1, v3}, Lcom/twitter/android/NotificationSettingsActivity;->a(ZIZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    if-ne p2, v1, :cond_0

    const-string/jumbo v0, "pref_mention"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "pref_choice"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_2
    const-string/jumbo v0, "pref_choice"

    invoke-virtual {p3, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const-string/jumbo v1, "pref_mention"

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/NotificationSettingsActivity;->a(IZ)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x3

    if-ne p1, v0, :cond_5

    if-ne p2, v1, :cond_0

    const-string/jumbo v0, "pref_mention"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string/jumbo v0, "pref_choice"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_4
    const-string/jumbo v0, "pref_choice"

    invoke-virtual {p3, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const-string/jumbo v1, "pref_mention"

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/NotificationSettingsActivity;->b(IZ)V

    goto :goto_0

    :cond_5
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/client/BasePreferenceActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    const v4, 0x7f0f03f8    # com.twitter.android.R.string.settings_notif_mentions_and_photo_tags_title

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BasePreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f0f041b    # com.twitter.android.R.string.settings_notifications_title

    invoke-virtual {p0, v2}, Lcom/twitter/android/NotificationSettingsActivity;->setTitle(I)V

    invoke-virtual {p0}, Lcom/twitter/android/NotificationSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "account_name"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/NotificationSettingsActivity;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/NotificationSettingsActivity;->c()Lcom/twitter/library/client/aa;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/NotificationSettingsActivity;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/NotificationSettingsActivity;->b:Lcom/twitter/library/client/Session;

    iput-boolean v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->R:Z

    invoke-static {p0}, Lcom/twitter/library/platform/PushService;->c(Landroid/content/Context;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/android/NotificationSettingsActivity;->c:Z

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v3, "rt_fav_settings_enabled"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/android/NotificationSettingsActivity;->j:Z

    invoke-static {}, Lgl;->a()Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/android/NotificationSettingsActivity;->U:Z

    iget-boolean v2, p0, Lcom/twitter/android/NotificationSettingsActivity;->c:Z

    if-eqz v2, :cond_4

    const v2, 0x7f06000f    # com.twitter.android.R.xml.notification_prefs_gcm

    invoke-virtual {p0, v2}, Lcom/twitter/android/NotificationSettingsActivity;->addPreferencesFromResource(I)V

    :goto_0
    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/client/c;->f()Z

    move-result v2

    if-nez v2, :cond_0

    move v1, v0

    :cond_0
    const-string/jumbo v0, "notif_lifeline_alerts"

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->L:Landroid/preference/Preference;

    iget-boolean v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->c:Z

    if-eqz v0, :cond_6

    const-string/jumbo v0, "notif_tweets"

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->D:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->D:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-boolean v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->U:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->D:Landroid/preference/Preference;

    const v2, 0x7f0f0403    # com.twitter.android.R.string.settings_notif_timeline_title_favorite_people

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setTitle(I)V

    :cond_1
    const-string/jumbo v0, "notif_mentions_choice"

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->E:Landroid/preference/Preference;

    const-string/jumbo v0, "notif_enabled"

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->S:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->S:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    const-string/jumbo v0, "notif_address_book"

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->I:Landroid/preference/Preference;

    const-string/jumbo v0, "notif_experimental"

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->J:Landroid/preference/Preference;

    const-string/jumbo v0, "notif_recommendations"

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->M:Landroid/preference/Preference;

    const-string/jumbo v0, "notif_news"

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->N:Landroid/preference/Preference;

    const-string/jumbo v0, "notif_vit_notable_event"

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->O:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/twitter/android/NotificationSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v2, "notif_pref_category"

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->C:Landroid/preference/PreferenceCategory;

    iget-boolean v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->j:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->C:Landroid/preference/PreferenceCategory;

    const-string/jumbo v2, "notif_retweets_choice"

    invoke-virtual {p0, v2}, Lcom/twitter/android/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->C:Landroid/preference/PreferenceCategory;

    const-string/jumbo v2, "notif_favorites_choice"

    invoke-virtual {p0, v2}, Lcom/twitter/android/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    const-string/jumbo v0, "notif_retweets"

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->F:Landroid/preference/Preference;

    const-string/jumbo v0, "notif_favorites"

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->G:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->F:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->G:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    :goto_1
    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->C:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/twitter/android/NotificationSettingsActivity;->L:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    :cond_2
    :goto_2
    const-string/jumbo v0, "notif_follows"

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->H:Landroid/preference/Preference;

    const-string/jumbo v0, "notif_direct_messages"

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->K:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->E:Landroid/preference/Preference;

    invoke-virtual {v0, v4}, Landroid/preference/Preference;->setTitle(I)V

    iget-boolean v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->c:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->E:Landroid/preference/Preference;

    check-cast v0, Lcom/twitter/android/widget/CheckBoxListPreference;

    invoke-virtual {v0, v4}, Lcom/twitter/android/widget/CheckBoxListPreference;->setDialogTitle(I)V

    :cond_3
    const-string/jumbo v0, "ringtone"

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    return-void

    :cond_4
    const v2, 0x7f06000e    # com.twitter.android.R.xml.notification_prefs

    invoke-virtual {p0, v2}, Lcom/twitter/android/NotificationSettingsActivity;->addPreferencesFromResource(I)V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->C:Landroid/preference/PreferenceCategory;

    const-string/jumbo v2, "notif_retweets"

    invoke-virtual {p0, v2}, Lcom/twitter/android/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->C:Landroid/preference/PreferenceCategory;

    const-string/jumbo v2, "notif_favorites"

    invoke-virtual {p0, v2}, Lcom/twitter/android/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    const-string/jumbo v0, "notif_retweets_choice"

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->F:Landroid/preference/Preference;

    const-string/jumbo v0, "notif_favorites_choice"

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->G:Landroid/preference/Preference;

    goto :goto_1

    :cond_6
    const-string/jumbo v0, "notif_timeline"

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->D:Landroid/preference/Preference;

    const-string/jumbo v0, "notif_mentions"

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->E:Landroid/preference/Preference;

    const-string/jumbo v0, "notif_retweets"

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->F:Landroid/preference/Preference;

    const-string/jumbo v0, "notif_favorites"

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->G:Landroid/preference/Preference;

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/NotificationSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/NotificationSettingsActivity;->L:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_2
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BasePreferenceActivity;->onDestroy()V

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->T:Lcom/twitter/android/nw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->T:Lcom/twitter/android/nw;

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationSettingsActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Lcom/twitter/android/client/BasePreferenceActivity;->onPause()V

    invoke-static {p0}, Lcom/twitter/library/featureswitch/a;->b(Lcom/twitter/library/featureswitch/g;)V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "ringtone"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p2, Ljava/lang/String;

    iput-object p2, p0, Lcom/twitter/android/NotificationSettingsActivity;->d:Ljava/lang/String;

    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_0
    check-cast p1, Landroid/preference/ListPreference;

    check-cast p2, Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/twitter/library/util/Util;->a(Landroid/preference/ListPreference;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 5

    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    const-string/jumbo v2, "notif_tweets"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iput-boolean v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->R:Z

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/TweetSettingsActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "account_name"

    iget-object v3, p0, Lcom/twitter/android/NotificationSettingsActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "enabled"

    iget-boolean v3, p0, Lcom/twitter/android/NotificationSettingsActivity;->e:Z

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/NotificationSettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return v1

    :cond_0
    const-string/jumbo v2, "notif_retweets"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iput-boolean v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->R:Z

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/RtFavSettingsActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "pref_choice"

    iget v3, p0, Lcom/twitter/android/NotificationSettingsActivity;->f:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "pref_choice_key"

    const-string/jumbo v3, "notif_retweets_choice"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "pref_mention"

    iget-boolean v3, p0, Lcom/twitter/android/NotificationSettingsActivity;->g:Z

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "pref_mention_key"

    const-string/jumbo v3, "notif_retweeted_mention"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "pref_title"

    const v3, 0x7f0f03fe    # com.twitter.android.R.string.settings_notif_retweets_title

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "pref_xml"

    const v3, 0x7f060014    # com.twitter.android.R.xml.retweet_notification_prefs

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const/4 v2, 0x2

    invoke-virtual {p0, v0, v2}, Lcom/twitter/android/NotificationSettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_1
    const-string/jumbo v2, "notif_favorites"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iput-boolean v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->R:Z

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/RtFavSettingsActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "pref_choice"

    iget v3, p0, Lcom/twitter/android/NotificationSettingsActivity;->h:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "pref_choice_key"

    const-string/jumbo v3, "notif_favorites_choice"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "pref_mention"

    iget-boolean v3, p0, Lcom/twitter/android/NotificationSettingsActivity;->i:Z

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "pref_mention_key"

    const-string/jumbo v3, "notif_favorited_mention"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "pref_title"

    const v3, 0x7f0f03f4    # com.twitter.android.R.string.settings_notif_favorites_title

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "pref_xml"

    const v3, 0x7f06000b    # com.twitter.android.R.xml.favorite_notification_prefs

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const/4 v2, 0x3

    invoke-virtual {p0, v0, v2}, Lcom/twitter/android/NotificationSettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_2
    invoke-static {p0}, Lcom/google/android/gcm/a;->h(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/twitter/android/NotificationSettingsActivity;->a:Ljava/lang/String;

    invoke-static {p0, v2}, Lcom/twitter/library/api/account/k;->f(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const v0, 0x7f0f008b    # com.twitter.android.R.string.cannot_turn_off_notification

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_3
    iget-object v2, p0, Lcom/twitter/android/NotificationSettingsActivity;->S:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    :cond_4
    iget-object v2, p0, Lcom/twitter/android/NotificationSettingsActivity;->S:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_0

    :cond_5
    iget-object v2, p0, Lcom/twitter/android/NotificationSettingsActivity;->S:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v0}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    sget-object v2, Lcom/twitter/library/platform/PushService;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v2, Lcom/twitter/library/platform/PushService;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v2, Lcom/twitter/android/nw;

    invoke-direct {v2, p0, v4}, Lcom/twitter/android/nw;-><init>(Lcom/twitter/android/NotificationSettingsActivity;Lcom/twitter/android/nu;)V

    iput-object v2, p0, Lcom/twitter/android/NotificationSettingsActivity;->T:Lcom/twitter/android/nw;

    iget-object v2, p0, Lcom/twitter/android/NotificationSettingsActivity;->T:Lcom/twitter/android/nw;

    sget-object v3, Lcom/twitter/library/provider/w;->a:Ljava/lang/String;

    invoke-virtual {p0, v2, v0, v3, v4}, Lcom/twitter/android/NotificationSettingsActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    invoke-static {p0}, Lcom/twitter/library/platform/PushService;->e(Landroid/content/Context;)V

    goto/16 :goto_0
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BasePreferenceActivity;->onResume()V

    invoke-static {}, Lgl;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->U:Z

    invoke-static {p0}, Lcom/twitter/library/featureswitch/a;->a(Lcom/twitter/library/featureswitch/g;)V

    invoke-virtual {p0}, Lcom/twitter/android/NotificationSettingsActivity;->a()V

    invoke-virtual {p0}, Lcom/twitter/android/NotificationSettingsActivity;->b()V

    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/client/BasePreferenceActivity;->onStart()V

    iget-boolean v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->R:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/twitter/android/nv;

    iget-object v1, p0, Lcom/twitter/android/NotificationSettingsActivity;->a:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/nv;-><init>(Lcom/twitter/android/NotificationSettingsActivity;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/nv;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->R:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->R:Z

    goto :goto_0
.end method

.method public onStop()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/client/BasePreferenceActivity;->onStop()V

    iget-boolean v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->R:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/NotificationSettingsActivity;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/twitter/android/nx;

    iget-object v1, p0, Lcom/twitter/android/NotificationSettingsActivity;->a:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/nx;-><init>(Lcom/twitter/android/NotificationSettingsActivity;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/nx;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    return-void
.end method
