.class public final enum Lcom/twitter/android/FollowFlowController$Initiator;
.super Ljava/lang/Enum;
.source "Twttr"


# static fields
.field public static final enum a:Lcom/twitter/android/FollowFlowController$Initiator;

.field public static final enum b:Lcom/twitter/android/FollowFlowController$Initiator;

.field public static final enum c:Lcom/twitter/android/FollowFlowController$Initiator;

.field public static final enum d:Lcom/twitter/android/FollowFlowController$Initiator;

.field private static final synthetic e:[Lcom/twitter/android/FollowFlowController$Initiator;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/android/FollowFlowController$Initiator;

    const-string/jumbo v1, "SIGNUP"

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/FollowFlowController$Initiator;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/FollowFlowController$Initiator;->a:Lcom/twitter/android/FollowFlowController$Initiator;

    new-instance v0, Lcom/twitter/android/FollowFlowController$Initiator;

    const-string/jumbo v1, "DISCO"

    invoke-direct {v0, v1, v3}, Lcom/twitter/android/FollowFlowController$Initiator;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/FollowFlowController$Initiator;->b:Lcom/twitter/android/FollowFlowController$Initiator;

    new-instance v0, Lcom/twitter/android/FollowFlowController$Initiator;

    const-string/jumbo v1, "PROMPT"

    invoke-direct {v0, v1, v4}, Lcom/twitter/android/FollowFlowController$Initiator;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/FollowFlowController$Initiator;->c:Lcom/twitter/android/FollowFlowController$Initiator;

    new-instance v0, Lcom/twitter/android/FollowFlowController$Initiator;

    const-string/jumbo v1, "OPT_IN"

    invoke-direct {v0, v1, v5}, Lcom/twitter/android/FollowFlowController$Initiator;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/FollowFlowController$Initiator;->d:Lcom/twitter/android/FollowFlowController$Initiator;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/twitter/android/FollowFlowController$Initiator;

    sget-object v1, Lcom/twitter/android/FollowFlowController$Initiator;->a:Lcom/twitter/android/FollowFlowController$Initiator;

    aput-object v1, v0, v2

    sget-object v1, Lcom/twitter/android/FollowFlowController$Initiator;->b:Lcom/twitter/android/FollowFlowController$Initiator;

    aput-object v1, v0, v3

    sget-object v1, Lcom/twitter/android/FollowFlowController$Initiator;->c:Lcom/twitter/android/FollowFlowController$Initiator;

    aput-object v1, v0, v4

    sget-object v1, Lcom/twitter/android/FollowFlowController$Initiator;->d:Lcom/twitter/android/FollowFlowController$Initiator;

    aput-object v1, v0, v5

    sput-object v0, Lcom/twitter/android/FollowFlowController$Initiator;->e:[Lcom/twitter/android/FollowFlowController$Initiator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/android/FollowFlowController$Initiator;
    .locals 1

    const-class v0, Lcom/twitter/android/FollowFlowController$Initiator;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/FollowFlowController$Initiator;

    return-object v0
.end method

.method public static values()[Lcom/twitter/android/FollowFlowController$Initiator;
    .locals 1

    sget-object v0, Lcom/twitter/android/FollowFlowController$Initiator;->e:[Lcom/twitter/android/FollowFlowController$Initiator;

    invoke-virtual {v0}, [Lcom/twitter/android/FollowFlowController$Initiator;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/FollowFlowController$Initiator;

    return-object v0
.end method
