.class Lcom/twitter/android/lf;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/aq;


# instance fields
.field final synthetic a:Landroid/widget/TextView;

.field final synthetic b:Landroid/view/View;

.field final synthetic c:Landroid/widget/ListView;

.field final synthetic d:Lcom/twitter/android/MediaTagFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/MediaTagFragment;Landroid/widget/TextView;Landroid/view/View;Landroid/widget/ListView;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/lf;->d:Lcom/twitter/android/MediaTagFragment;

    iput-object p2, p0, Lcom/twitter/android/lf;->a:Landroid/widget/TextView;

    iput-object p3, p0, Lcom/twitter/android/lf;->b:Landroid/view/View;

    iput-object p4, p0, Lcom/twitter/android/lf;->c:Landroid/widget/ListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/CharSequence;Landroid/database/Cursor;)V
    .locals 3

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/lf;->d:Lcom/twitter/android/MediaTagFragment;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    invoke-static {v1, v2}, Lcom/twitter/android/MediaTagFragment;->a(Lcom/twitter/android/MediaTagFragment;Z)Z

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/lf;->d:Lcom/twitter/android/MediaTagFragment;

    invoke-static {v2}, Lcom/twitter/android/MediaTagFragment;->b(Lcom/twitter/android/MediaTagFragment;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-lez v1, :cond_2

    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/twitter/android/lf;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/twitter/android/lf;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/lf;->c:Landroid/widget/ListView;

    new-instance v1, Lcom/twitter/android/lg;

    invoke-direct {v1, p0}, Lcom/twitter/android/lg;-><init>(Lcom/twitter/android/lf;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/16 v0, 0x8

    goto :goto_1
.end method
