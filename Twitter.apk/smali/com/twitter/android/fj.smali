.class Lcom/twitter/android/fj;
.super Landroid/os/AsyncTask;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/android/EditProfileOnboardingActivity;


# direct methods
.method private constructor <init>(Lcom/twitter/android/EditProfileOnboardingActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/fj;->a:Lcom/twitter/android/EditProfileOnboardingActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/EditProfileOnboardingActivity;Lcom/twitter/android/fi;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/fj;-><init>(Lcom/twitter/android/EditProfileOnboardingActivity;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 7

    const/4 v0, 0x0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lcom/twitter/android/fj;->a:Lcom/twitter/android/EditProfileOnboardingActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/ContactsContract$Profile;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/provider/ContactsContract$Contacts;->openContactPhotoInputStream(Landroid/content/ContentResolver;Landroid/net/Uri;Z)Ljava/io/InputStream;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/fj;->a:Lcom/twitter/android/EditProfileOnboardingActivity;

    iget-wide v4, v4, Lcom/twitter/android/EditProfileOnboardingActivity;->j:J

    invoke-static {v1, v3, v4, v5}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;ZJ)Ljava/io/File;

    move-result-object v3

    if-eqz v3, :cond_0

    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v4, 0x1000

    :try_start_1
    invoke-static {v2, v1, v4}, Lcom/twitter/internal/util/h;->a(Ljava/io/InputStream;Ljava/io/OutputStream;I)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-static {v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    iget-object v0, p0, Lcom/twitter/android/fj;->a:Lcom/twitter/android/EditProfileOnboardingActivity;

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/android/EditProfileOnboardingActivity;->c:Landroid/net/Uri;

    invoke-static {v3}, Lkw;->a(Ljava/io/File;)Lkw;

    move-result-object v0

    invoke-virtual {v0}, Lkw;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    move-object v1, v0

    :goto_1
    invoke-static {v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_2
    invoke-static {v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v3

    goto :goto_1
.end method

.method protected a(Landroid/graphics/Bitmap;)V
    .locals 7

    const/4 v6, 0x1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/fj;->a:Lcom/twitter/android/EditProfileOnboardingActivity;

    invoke-static {v0}, Lcom/twitter/android/EditProfileOnboardingActivity;->d(Lcom/twitter/android/EditProfileOnboardingActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/fj;->a:Lcom/twitter/android/EditProfileOnboardingActivity;

    invoke-static {v1}, Lcom/twitter/android/EditProfileOnboardingActivity;->c(Lcom/twitter/android/EditProfileOnboardingActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v6, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "welcome:edit_profile::avatar:prefill"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/fj;->a:Lcom/twitter/android/EditProfileOnboardingActivity;

    invoke-static {v0, v6}, Lcom/twitter/android/EditProfileOnboardingActivity;->a(Lcom/twitter/android/EditProfileOnboardingActivity;Z)Z

    iget-object v0, p0, Lcom/twitter/android/fj;->a:Lcom/twitter/android/EditProfileOnboardingActivity;

    iget-object v1, p0, Lcom/twitter/android/fj;->a:Lcom/twitter/android/EditProfileOnboardingActivity;

    iget-object v1, v1, Lcom/twitter/android/EditProfileOnboardingActivity;->o:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getId()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/twitter/android/EditProfileOnboardingActivity;->a(Landroid/graphics/Bitmap;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/fj;->a:Lcom/twitter/android/EditProfileOnboardingActivity;

    invoke-static {v0}, Lcom/twitter/android/EditProfileOnboardingActivity;->e(Lcom/twitter/android/EditProfileOnboardingActivity;)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lcom/twitter/android/fj;->a([Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/twitter/android/fj;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method
