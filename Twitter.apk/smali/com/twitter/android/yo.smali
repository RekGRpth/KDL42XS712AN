.class public Lcom/twitter/android/yo;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/android/yq;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/android/yr;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/twitter/android/yq;

    invoke-direct {v0, p1, p2}, Lcom/twitter/android/yq;-><init>(Landroid/app/Activity;Lcom/twitter/android/yr;)V

    iput-object v0, p0, Lcom/twitter/android/yo;->a:Lcom/twitter/android/yq;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 4

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/twitter/android/yo;->a:Lcom/twitter/android/yq;

    invoke-virtual {v0, v2}, Lcom/twitter/android/yq;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v2}, Lcom/twitter/android/yq;->removeMessages(I)V

    :cond_0
    invoke-virtual {v0, v2, p1}, Lcom/twitter/android/yq;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/yq;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method
