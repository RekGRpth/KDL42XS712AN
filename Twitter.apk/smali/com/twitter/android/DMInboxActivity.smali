.class public Lcom/twitter/android/DMInboxActivity;
.super Lcom/twitter/android/ListFragmentActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/ce;


# instance fields
.field private a:Lcom/twitter/android/dw;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/ListFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a(Z)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a(I)V

    return-object v0
.end method

.method protected a(Landroid/content/Intent;Lcom/twitter/library/client/e;)Lcom/twitter/android/iu;
    .locals 2

    new-instance v0, Lcom/twitter/android/iu;

    new-instance v1, Lcom/twitter/android/DMInboxFragment;

    invoke-direct {v1}, Lcom/twitter/android/DMInboxFragment;-><init>()V

    invoke-direct {v0, v1}, Lcom/twitter/android/iu;-><init>(Lcom/twitter/android/client/BaseListFragment;)V

    return-object v0
.end method

.method protected a(Landroid/content/Intent;)Ljava/lang/CharSequence;
    .locals 1

    const v0, 0x7f0f01d0    # com.twitter.android.R.string.home_direct_messages

    invoke-virtual {p0, v0}, Lcom/twitter/android/DMInboxActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "messages:inbox::mark_all_as_read:click"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    new-instance v0, Lcom/twitter/library/api/conversations/aj;

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/api/conversations/aj;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {p0, v0, v6, v5}, Lcom/twitter/android/DMInboxActivity;->a(Lcom/twitter/library/service/b;II)Z

    :cond_0
    return-void
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/twitter/android/ListFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V

    new-instance v0, Lcom/twitter/android/dw;

    invoke-direct {v0, p0}, Lcom/twitter/android/dw;-><init>(Lcom/twitter/android/DMInboxActivity;)V

    iput-object v0, p0, Lcom/twitter/android/DMInboxActivity;->a:Lcom/twitter/android/dw;

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/DMInboxActivity;->a:Lcom/twitter/android/dw;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/z;)V

    return-void
.end method

.method public a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 1

    const v0, 0x7f110017    # com.twitter.android.R.menu.message_inbox_toolbar

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    const/4 v0, 0x1

    return v0
.end method

.method public a(Lhn;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p1}, Lhn;->a()I

    move-result v1

    const v2, 0x7f090322    # com.twitter.android.R.id.menu_compose_dm

    if-ne v1, v2, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/DMConversationActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/twitter/android/DMInboxActivity;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return v0

    :cond_0
    const v2, 0x7f090323    # com.twitter.android.R.id.dm_mark_as_read

    if-ne v1, v2, :cond_1

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f0289    # com.twitter.android.R.string.messages_mark_all_read_confirmation

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f0571    # com.twitter.android.R.string.yes

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f029d    # com.twitter.android.R.string.no

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/android/widget/ce;)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Lcom/twitter/android/ListFragmentActivity;->a(Lhn;)Z

    move-result v0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/ListFragmentActivity;->onDestroy()V

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/DMInboxActivity;->a:Lcom/twitter/android/dw;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->b(Lcom/twitter/library/client/z;)V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/ListFragmentActivity;->onResume()V

    invoke-static {p0}, Lcom/twitter/android/client/aw;->a(Landroid/content/Context;)Lcom/twitter/android/client/aw;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/aw;->d(Ljava/lang/String;)V

    return-void
.end method
