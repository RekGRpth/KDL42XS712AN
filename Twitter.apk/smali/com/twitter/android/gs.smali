.class public Lcom/twitter/android/gs;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/support/v4/app/FragmentActivity;

.field private final b:Landroid/support/v4/app/FragmentManager;

.field private final c:[Lcom/twitter/android/gr;

.field private final d:I

.field private e:Lcom/twitter/android/gr;

.field private f:I


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;I[Lcom/twitter/android/gr;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/gs;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/gs;->b:Landroid/support/v4/app/FragmentManager;

    iput p2, p0, Lcom/twitter/android/gs;->d:I

    iput-object p3, p0, Lcom/twitter/android/gs;->c:[Lcom/twitter/android/gr;

    return-void
.end method

.method private a(Lcom/twitter/android/gr;)V
    .locals 5

    iget-object v2, p0, Lcom/twitter/android/gs;->b:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    iget-object v0, p0, Lcom/twitter/android/gs;->e:Lcom/twitter/android/gr;

    if-nez v0, :cond_2

    iget v0, p0, Lcom/twitter/android/gs;->d:I

    invoke-virtual {v2, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/gs;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {p1, v0}, Lcom/twitter/android/gr;->a(Landroid/support/v4/app/FragmentActivity;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eq v1, v0, :cond_1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->isDetached()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v3, v1}, Landroid/support/v4/app/FragmentTransaction;->detach(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    :cond_0
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isDetached()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v3, v0}, Landroid/support/v4/app/FragmentTransaction;->attach(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    :goto_1
    const/16 v1, 0x1003

    invoke-virtual {v3, v1}, Landroid/support/v4/app/FragmentTransaction;->setTransition(I)Landroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p1, Lcom/twitter/android/gr;->d:Z

    iput-object p1, p0, Lcom/twitter/android/gs;->e:Lcom/twitter/android/gr;

    check-cast v0, Lcom/twitter/android/ty;

    invoke-interface {v0}, Lcom/twitter/android/ty;->u_()V

    return-void

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/gs;->e:Lcom/twitter/android/gr;

    iget-object v1, p0, Lcom/twitter/android/gs;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0, v1}, Lcom/twitter/android/gr;->a(Landroid/support/v4/app/FragmentActivity;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_3
    iget v1, p0, Lcom/twitter/android/gs;->d:I

    iget-object v4, p1, Lcom/twitter/android/gr;->c:Ljava/lang/String;

    invoke-virtual {v3, v1, v0, v4}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    goto :goto_1
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/gs;->e:Lcom/twitter/android/gr;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/gs;->e:Lcom/twitter/android/gr;

    iget-object v0, v0, Lcom/twitter/android/gr;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(I)Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/gs;->c:[Lcom/twitter/android/gr;

    aget-object v0, v0, p1

    iget-object v1, p0, Lcom/twitter/android/gs;->e:Lcom/twitter/android/gr;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, v0}, Lcom/twitter/android/gs;->a(Lcom/twitter/android/gr;)V

    iput p1, p0, Lcom/twitter/android/gs;->f:I

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 7

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/android/gs;->e:Lcom/twitter/android/gr;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/gs;->e:Lcom/twitter/android/gr;

    iget-object v1, v1, Lcom/twitter/android/gr;->c:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v3, p0, Lcom/twitter/android/gs;->c:[Lcom/twitter/android/gr;

    array-length v4, v3

    move v1, v0

    move v2, v0

    :goto_1
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    iget-object v6, v5, Lcom/twitter/android/gr;->c:Ljava/lang/String;

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-direct {p0, v5}, Lcom/twitter/android/gs;->a(Lcom/twitter/android/gr;)V

    iput v2, p0, Lcom/twitter/android/gs;->f:I

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method
