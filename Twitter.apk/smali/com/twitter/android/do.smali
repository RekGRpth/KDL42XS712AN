.class Lcom/twitter/android/do;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/ce;


# instance fields
.field final synthetic a:Landroid/database/Cursor;

.field final synthetic b:Lcom/twitter/android/DMConversationFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/DMConversationFragment;Landroid/database/Cursor;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/do;->b:Lcom/twitter/android/DMConversationFragment;

    iput-object p2, p0, Lcom/twitter/android/do;->a:Landroid/database/Cursor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/DialogInterface;II)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    packed-switch p3, :pswitch_data_0

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/do;->b:Lcom/twitter/android/DMConversationFragment;

    invoke-static {v0, v6}, Lcom/twitter/android/DMConversationFragment;->b(Lcom/twitter/android/DMConversationFragment;Z)Z

    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/do;->b:Lcom/twitter/android/DMConversationFragment;

    iget-object v1, p0, Lcom/twitter/android/do;->a:Landroid/database/Cursor;

    invoke-static {v0, v1}, Lcom/twitter/android/DMConversationFragment;->a(Lcom/twitter/android/DMConversationFragment;Landroid/database/Cursor;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/do;->b:Lcom/twitter/android/DMConversationFragment;

    invoke-static {v0}, Lcom/twitter/android/DMConversationFragment;->g(Lcom/twitter/android/DMConversationFragment;)Lcom/twitter/library/client/Session;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/do;->b:Lcom/twitter/android/DMConversationFragment;

    invoke-static {v1}, Lcom/twitter/android/DMConversationFragment;->h(Lcom/twitter/android/DMConversationFragment;)Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v7, [Ljava/lang/String;

    const-string/jumbo v5, "messages:thread:message::delete"

    aput-object v5, v4, v6

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/do;->b:Lcom/twitter/android/DMConversationFragment;

    new-instance v2, Lcom/twitter/library/api/conversations/ag;

    iget-object v3, p0, Lcom/twitter/android/do;->b:Lcom/twitter/android/DMConversationFragment;

    invoke-virtual {v3}, Lcom/twitter/android/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/do;->a:Landroid/database/Cursor;

    invoke-interface {v4, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v0, v4}, Lcom/twitter/library/api/conversations/ag;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    invoke-static {v1, v2, v7, v6}, Lcom/twitter/android/DMConversationFragment;->c(Lcom/twitter/android/DMConversationFragment;Lcom/twitter/library/service/b;II)Z

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/do;->b:Lcom/twitter/android/DMConversationFragment;

    iget-object v1, p0, Lcom/twitter/android/do;->a:Landroid/database/Cursor;

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v6}, Lcom/twitter/android/DMConversationFragment;->a(Lcom/twitter/android/DMConversationFragment;Ljava/lang/String;Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
