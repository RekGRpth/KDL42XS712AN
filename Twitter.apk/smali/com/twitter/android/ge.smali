.class Lcom/twitter/android/ge;
.super Landroid/support/v4/view/PagerAdapter;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/FilterActivity;

.field private final b:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Lcom/twitter/android/FilterActivity;)V
    .locals 2

    iput-object p1, p0, Lcom/twitter/android/ge;->a:Lcom/twitter/android/FilterActivity;

    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    sget v1, Lcom/twitter/android/FilterActivity;->a:I

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/ge;->b:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;I)V
    .locals 9

    const/4 v2, 0x0

    sget-object v0, Lcom/twitter/library/util/n;->a:[I

    aget v3, v0, p2

    iget-object v0, p0, Lcom/twitter/android/ge;->a:Lcom/twitter/android/FilterActivity;

    iget-object v0, v0, Lcom/twitter/android/FilterActivity;->b:Lcom/twitter/android/FilterManager;

    invoke-virtual {v0}, Lcom/twitter/android/FilterManager;->d()Lcom/twitter/android/gk;

    move-result-object v4

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/gf;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/gf;

    iget-object v5, v1, Lcom/twitter/android/gf;->a:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/twitter/android/ge;->a:Lcom/twitter/android/FilterActivity;

    iget-object v1, v1, Lcom/twitter/android/FilterActivity;->b:Lcom/twitter/android/FilterManager;

    invoke-virtual {v1, v3}, Lcom/twitter/android/FilterManager;->c(I)Lcom/twitter/android/gh;

    move-result-object v6

    if-eqz v6, :cond_3

    iget-object v1, v6, Lcom/twitter/android/gh;->c:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_0
    sget-object v7, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v5, v7}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    if-eqz v1, :cond_0

    iget-object v7, v6, Lcom/twitter/android/gh;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v5, v7}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    iget-object v7, p0, Lcom/twitter/android/ge;->a:Lcom/twitter/android/FilterActivity;

    invoke-static {}, Lcom/twitter/android/FilterActivity;->b()[I

    move-result-object v8

    aget v8, v8, p2

    invoke-virtual {v7, v8}, Lcom/twitter/android/FilterActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    if-eqz v4, :cond_4

    if-eqz v1, :cond_1

    iget-boolean v1, v6, Lcom/twitter/android/gh;->a:Z

    iget-object v4, p0, Lcom/twitter/android/ge;->a:Lcom/twitter/android/FilterActivity;

    invoke-static {v4}, Lcom/twitter/android/FilterActivity;->j(Lcom/twitter/android/FilterActivity;)Z

    move-result v4

    if-eq v1, v4, :cond_2

    :cond_1
    iget-object v0, v0, Lcom/twitter/android/gf;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/ge;->a:Lcom/twitter/android/FilterActivity;

    iget-object v0, v0, Lcom/twitter/android/FilterActivity;->b:Lcom/twitter/android/FilterManager;

    invoke-virtual {v0, p1, v3}, Lcom/twitter/android/FilterManager;->a(Landroid/view/View;I)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1
.end method

.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/ge;->a:Lcom/twitter/android/FilterActivity;

    iget-object v0, v0, Lcom/twitter/android/FilterActivity;->b:Lcom/twitter/android/FilterManager;

    sget-object v1, Lcom/twitter/library/util/n;->a:[I

    aget v1, v1, p2

    invoke-virtual {v0, v1}, Lcom/twitter/android/FilterManager;->d(I)V

    check-cast p3, Landroid/view/View;

    const v0, 0x7f090077    # com.twitter.android.R.id.image

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/twitter/android/ge;->b:Ljava/util/ArrayList;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public getCount()I
    .locals 1

    sget v0, Lcom/twitter/android/FilterActivity;->a:I

    return v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 1

    const/4 v0, -0x2

    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 3

    const/4 v2, 0x0

    if-ltz p2, :cond_0

    sget v0, Lcom/twitter/android/FilterActivity;->a:I

    if-lt p2, v0, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ge;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/ge;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_3

    :cond_2
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030074    # com.twitter.android.R.layout.filter_pager_image

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    :goto_1
    new-instance v2, Lcom/twitter/android/gf;

    invoke-direct {v2}, Lcom/twitter/android/gf;-><init>()V

    const v0, 0x7f090077    # com.twitter.android.R.id.image

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/twitter/android/gf;->a:Landroid/widget/ImageView;

    const v0, 0x7f090180    # com.twitter.android.R.id.filter_progress

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v2, Lcom/twitter/android/gf;->b:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-virtual {p0, v1, p2}, Lcom/twitter/android/ge;->a(Landroid/view/View;I)V

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_3
    move-object v1, v0

    goto :goto_1
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
