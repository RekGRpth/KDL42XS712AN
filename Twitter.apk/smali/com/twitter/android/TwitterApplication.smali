.class public Lcom/twitter/android/TwitterApplication;
.super Lcom/twitter/library/client/App;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 16

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-wide v7, 0x145ce1d6c09L

    const-string/jumbo v9, "com.twitter.android"

    const-string/jumbo v10, "r"

    const/16 v11, 0x198

    const/4 v12, 0x0

    const/4 v13, 0x1

    const/4 v14, 0x0

    const-string/jumbo v15, "default"

    move-object/from16 v0, p0

    invoke-direct/range {v0 .. v15}, Lcom/twitter/library/client/App;-><init>(ZZZZZZJLjava/lang/String;Ljava/lang/String;IZZZLjava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/TwitterApplication;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/TwitterApplication;->s()V

    return-void
.end method

.method private s()V
    .locals 6

    const/4 v5, 0x0

    invoke-static {p0}, Lcom/twitter/library/api/b;->a(Landroid/content/Context;)Lcom/twitter/library/api/b;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "adid_no_tracking_enabled"

    invoke-interface {v1, v2, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    const-string/jumbo v3, "adid_identifier"

    const-string/jumbo v4, ""

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/twitter/library/api/b;

    invoke-direct {v4, v3, v2}, Lcom/twitter/library/api/b;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0, v4}, Lcom/twitter/library/api/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "adid_no_tracking_enabled"

    invoke-virtual {v0}, Lcom/twitter/library/api/b;->b()Z

    move-result v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "adid_identifier"

    invoke-virtual {v0}, Lcom/twitter/library/api/b;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/b;)V

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "app::::become_active"

    aput-object v4, v3, v5

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    const-string/jumbo v3, "app_download_client_event"

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->g(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    const-string/jumbo v3, "6"

    invoke-virtual {v0}, Lcom/twitter/library/api/b;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {v0}, Lcom/twitter/library/api/b;->b()Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Z)Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {v1, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto :goto_0
.end method

.method private t()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    const-string/jumbo v0, "debug_prefs"

    invoke-virtual {p0, v0, v2}, Lcom/twitter/android/TwitterApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "custom_locale_enabled"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "custom_language"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "custom_country"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/util/Locale;

    invoke-direct {v2, v1, v0}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    invoke-virtual {p0}, Lcom/twitter/android/TwitterApplication;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iput-object v2, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->d()V

    :cond_0
    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/library/client/App;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-static {}, Lcom/twitter/android/TwitterApplication;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/TwitterApplication;->t()V

    :cond_0
    return-void
.end method

.method public onCreate()V
    .locals 3

    invoke-super {p0}, Lcom/twitter/library/client/App;->onCreate()V

    new-instance v0, Lcom/twitter/android/yj;

    invoke-direct {v0, p0}, Lcom/twitter/android/yj;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    new-instance v0, Lcom/twitter/android/yk;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/yk;-><init>(Lcom/twitter/android/TwitterApplication;Lcom/twitter/android/yg;)V

    invoke-static {v0}, Lcom/crashlytics/android/d;->a(Lcom/crashlytics/android/t;)V

    invoke-static {p0}, Lcom/crashlytics/android/d;->a(Landroid/content/Context;)V

    invoke-static {}, Lcom/twitter/android/TwitterApplication;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/TwitterApplication;->t()V

    :cond_0
    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Landroid/content/ComponentName;)V

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/yg;

    invoke-direct {v2, p0, v1, v0}, Lcom/twitter/android/yg;-><init>(Lcom/twitter/android/TwitterApplication;Lcom/twitter/library/client/aa;Lcom/twitter/android/client/c;)V

    invoke-static {v2}, Lcom/twitter/library/client/l;->a(Lcom/twitter/library/client/k;)V

    return-void
.end method

.method public onLowMemory()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/library/client/App;->onLowMemory()V

    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->ak()V

    return-void
.end method
