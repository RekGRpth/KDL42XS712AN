.class public abstract Lcom/twitter/android/h;
.super Landroid/support/v4/widget/CursorAdapter;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/client/av;
.implements Lcom/twitter/library/view/h;


# instance fields
.field private A:Z

.field private B:Lcom/twitter/android/p;

.field protected final a:Lcom/twitter/android/client/c;

.field protected final b:Lcom/twitter/library/client/aa;

.field protected final c:Ljava/util/ArrayList;

.field protected final d:Lcom/twitter/library/widget/ap;

.field protected final e:Ljava/util/ArrayList;

.field protected final f:Lcom/twitter/library/widget/aa;

.field protected final g:Ljava/util/ArrayList;

.field protected final h:Lcom/twitter/library/view/c;

.field protected final i:Lcom/twitter/library/util/FriendshipCache;

.field protected final j:Landroid/view/View$OnClickListener;

.field protected final k:Landroid/util/SparseArray;

.field protected final l:Landroid/util/SparseArray;

.field protected final m:Landroid/util/SparseArray;

.field protected final n:Landroid/util/SparseArray;

.field protected final o:Ljava/util/ArrayList;

.field protected final p:Lcom/twitter/android/o;

.field protected q:J

.field protected r:[Lcom/twitter/internal/android/widget/TypefacesSpan;

.field protected s:[Lcom/twitter/internal/android/widget/TypefacesSpan;

.field protected t:I

.field protected u:[I

.field protected v:Z

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;IZLcom/twitter/android/client/c;Lcom/twitter/library/widget/ap;Lcom/twitter/library/widget/aa;Lcom/twitter/library/view/c;Lcom/twitter/android/o;Lcom/twitter/library/util/FriendshipCache;)V
    .locals 6

    const/4 v5, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/h;->c:Ljava/util/ArrayList;

    const/4 v0, 0x6

    iput v0, p0, Lcom/twitter/android/h;->t:I

    invoke-static {p1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/h;->b:Lcom/twitter/library/client/aa;

    iput-object p5, p0, Lcom/twitter/android/h;->d:Lcom/twitter/library/widget/ap;

    iput-object p6, p0, Lcom/twitter/android/h;->f:Lcom/twitter/library/widget/aa;

    iput-object p7, p0, Lcom/twitter/android/h;->h:Lcom/twitter/library/view/c;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/h;->e:Ljava/util/ArrayList;

    iput-object p4, p0, Lcom/twitter/android/h;->a:Lcom/twitter/android/client/c;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/h;->g:Ljava/util/ArrayList;

    new-array v0, v1, [Lcom/twitter/internal/android/widget/TypefacesSpan;

    new-instance v3, Lcom/twitter/internal/android/widget/TypefacesSpan;

    invoke-direct {v3, p1, v2}, Lcom/twitter/internal/android/widget/TypefacesSpan;-><init>(Landroid/content/Context;I)V

    aput-object v3, v0, v2

    iput-object v0, p0, Lcom/twitter/android/h;->r:[Lcom/twitter/internal/android/widget/TypefacesSpan;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/twitter/internal/android/widget/TypefacesSpan;

    iget-object v3, p0, Lcom/twitter/android/h;->r:[Lcom/twitter/internal/android/widget/TypefacesSpan;

    aget-object v3, v3, v2

    aput-object v3, v0, v2

    new-instance v3, Lcom/twitter/internal/android/widget/TypefacesSpan;

    invoke-direct {v3, p1, v2}, Lcom/twitter/internal/android/widget/TypefacesSpan;-><init>(Landroid/content/Context;I)V

    aput-object v3, v0, v1

    const/4 v3, 0x2

    new-instance v4, Lcom/twitter/internal/android/widget/TypefacesSpan;

    invoke-direct {v4, p1, v2}, Lcom/twitter/internal/android/widget/TypefacesSpan;-><init>(Landroid/content/Context;I)V

    aput-object v4, v0, v3

    new-instance v3, Lcom/twitter/internal/android/widget/TypefacesSpan;

    invoke-direct {v3, p1, v2}, Lcom/twitter/internal/android/widget/TypefacesSpan;-><init>(Landroid/content/Context;I)V

    aput-object v3, v0, v5

    iput-object v0, p0, Lcom/twitter/android/h;->s:[Lcom/twitter/internal/android/widget/TypefacesSpan;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/h;->k:Landroid/util/SparseArray;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/h;->l:Landroid/util/SparseArray;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/h;->m:Landroid/util/SparseArray;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/h;->n:Landroid/util/SparseArray;

    invoke-virtual {p4}, Lcom/twitter/android/client/c;->af()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/h;->y:Z

    iput-boolean p3, p0, Lcom/twitter/android/h;->w:Z

    iget-object v0, p0, Lcom/twitter/android/h;->b:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/library/api/UserSettings;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/twitter/library/api/UserSettings;->k:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/h;->z:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/h;->o:Ljava/util/ArrayList;

    new-array v0, v5, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/twitter/android/h;->u:[I

    iput-object p8, p0, Lcom/twitter/android/h;->p:Lcom/twitter/android/o;

    iput-object p9, p0, Lcom/twitter/android/h;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-static {}, Lgv;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/h;->v:Z

    invoke-static {p0}, Ljy;->a(Lcom/twitter/library/view/h;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/h;->j:Landroid/view/View$OnClickListener;

    const-string/jumbo v0, "android_wtf_show_bio_1605"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v0, "android_wtf_show_bio_1605"

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v3, "show_bio"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/h;->A:Z

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x7f030009    # com.twitter.android.R.layout.activity_social_header
        0x7f03000d    # com.twitter.android.R.layout.activity_user_with_bio_view
        0x7f03000e    # com.twitter.android.R.layout.activity_view_all
    .end array-data
.end method

.method protected static a(I)I
    .locals 1

    const/4 v0, 0x4

    if-le p0, v0, :cond_0

    const/4 v0, 0x3

    :goto_0
    return v0

    :cond_0
    invoke-static {v0, p0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method private a(Landroid/database/Cursor;ILandroid/util/SparseArray;)Ljava/util/ArrayList;
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p3, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/SoftReference;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    :goto_0
    if-nez v0, :cond_0

    invoke-interface {p1, p2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    new-instance v2, Ljava/lang/ref/SoftReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p3, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method private a(Lcom/twitter/android/ActivityCursor;ILandroid/util/SparseArray;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/twitter/android/ActivityCursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p3, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/SoftReference;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    :goto_0
    if-nez v0, :cond_0

    invoke-virtual {p1, p2}, Lcom/twitter/android/ActivityCursor;->getBlob(I)[B

    move-result-object v0

    invoke-virtual {p1, p4, v0}, Lcom/twitter/android/ActivityCursor;->a(Lcom/twitter/android/ActivityCursor$ObjectField;[B)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    new-instance v2, Ljava/lang/ref/SoftReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p3, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method private a(Lcom/twitter/android/ActivityCursor;ILandroid/util/SparseArray;Lcom/twitter/android/ActivityCursor$ObjectField;Lcom/twitter/android/ActivityCursor$IdType;)Ljava/util/ArrayList;
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/twitter/android/ActivityCursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p3, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/SoftReference;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    :goto_0
    if-nez v0, :cond_0

    invoke-virtual {p1, p2}, Lcom/twitter/android/ActivityCursor;->getBlob(I)[B

    move-result-object v0

    invoke-virtual {p1, p4, v0, p5}, Lcom/twitter/android/ActivityCursor;->a(Lcom/twitter/android/ActivityCursor$ObjectField;[BLcom/twitter/android/ActivityCursor$IdType;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    new-instance v2, Ljava/lang/ref/SoftReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p3, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method protected static b()Landroid/widget/AbsListView$LayoutParams;
    .locals 3

    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method protected static c()Landroid/widget/LinearLayout$LayoutParams;
    .locals 3

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    return-object v0
.end method

.method private d(Landroid/view/View;)Lcom/twitter/library/widget/TweetView;
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/twitter/android/j;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/twitter/android/j;

    iget-object v0, v0, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/h;->k:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    iget-object v0, p0, Lcom/twitter/android/h;->l:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    iget-object v0, p0, Lcom/twitter/android/h;->m:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    iget-object v0, p0, Lcom/twitter/android/h;->n:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    return-void
.end method


# virtual methods
.method protected a(Ljava/util/ArrayList;)I
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public abstract a(Landroid/content/Context;Landroid/view/View;Landroid/database/Cursor;)Landroid/content/Intent;
.end method

.method protected a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 2

    const v0, 0x7f030008    # com.twitter.android.R.layout.activity_gap

    const/4 v1, 0x0

    invoke-virtual {p4, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/m;

    invoke-direct {v1, v0}, Lcom/twitter/android/m;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-object v0
.end method

.method protected a(Landroid/view/LayoutInflater;Landroid/content/Context;IZLandroid/view/View$OnClickListener;)Landroid/view/View;
    .locals 7

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_0

    iget-object v1, p0, Lcom/twitter/android/h;->o:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/twitter/android/h;->u:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    iget-boolean v4, p0, Lcom/twitter/android/h;->A:Z

    invoke-static {p1, v1, v3, p0, v4}, Lcom/twitter/android/r;->a(Landroid/view/LayoutInflater;Ljava/util/ArrayList;ILandroid/view/View$OnClickListener;Z)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v5, p0, Lcom/twitter/android/h;->u:[I

    iget-object v6, p0, Lcom/twitter/android/h;->g:Ljava/util/ArrayList;

    move-object v0, p1

    move-object v1, p2

    move v3, p4

    move-object v4, p5

    invoke-static/range {v0 .. v6}, Lcom/twitter/android/n;->a(Landroid/view/LayoutInflater;Landroid/content/Context;Ljava/util/ArrayList;ZLandroid/view/View$OnClickListener;[ILjava/util/ArrayList;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    const v0, 0x7f030161    # com.twitter.android.R.layout.tweet_row_view

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/j;

    invoke-direct {v1, v0}, Lcom/twitter/android/j;-><init>(Landroid/view/View;)V

    iget-object v2, v1, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v3, p0, Lcom/twitter/android/h;->d:Lcom/twitter/library/widget/ap;

    invoke-virtual {v2, v3}, Lcom/twitter/library/widget/TweetView;->setProvider(Lcom/twitter/library/widget/ap;)V

    iget-object v2, v1, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v3, p0, Lcom/twitter/android/h;->f:Lcom/twitter/library/widget/aa;

    invoke-virtual {v2, v3}, Lcom/twitter/library/widget/TweetView;->setOnProfileImageClickListener(Lcom/twitter/library/widget/aa;)V

    iget-object v2, p0, Lcom/twitter/android/h;->a:Lcom/twitter/android/client/c;

    invoke-virtual {v2}, Lcom/twitter/android/client/c;->ab()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v3, p0, Lcom/twitter/android/h;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v2, v3}, Lcom/twitter/library/widget/TweetView;->setFriendshipCache(Lcom/twitter/library/util/FriendshipCache;)V

    :cond_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/twitter/android/h;->e:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method protected a(Landroid/database/Cursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;
    .locals 3

    const/4 v2, 0x3

    sget-object v0, Lcom/twitter/android/i;->a:[I

    invoke-virtual {p2}, Lcom/twitter/android/ActivityCursor$ObjectField;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v2, v0, :cond_0

    const/16 v0, 0x8

    iget-object v1, p0, Lcom/twitter/android/h;->n:Landroid/util/SparseArray;

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/h;->a(Landroid/database/Cursor;ILandroid/util/SparseArray;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const/16 v0, 0xa

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v2, v0, :cond_0

    const/16 v0, 0xb

    iget-object v1, p0, Lcom/twitter/android/h;->n:Landroid/util/SparseArray;

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/h;->a(Landroid/database/Cursor;ILandroid/util/SparseArray;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected a(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;
    .locals 3

    const/4 v2, 0x1

    sget-object v0, Lcom/twitter/android/i;->a:[I

    invoke-virtual {p2}, Lcom/twitter/android/ActivityCursor$ObjectField;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/twitter/android/ActivityCursor;->getInt(I)I

    move-result v0

    if-ne v2, v0, :cond_0

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/twitter/android/h;->k:Landroid/util/SparseArray;

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/twitter/android/h;->a(Lcom/twitter/android/ActivityCursor;ILandroid/util/SparseArray;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x7

    invoke-virtual {p1, v0}, Lcom/twitter/android/ActivityCursor;->getInt(I)I

    move-result v0

    if-ne v2, v0, :cond_0

    const/16 v0, 0x8

    iget-object v1, p0, Lcom/twitter/android/h;->l:Landroid/util/SparseArray;

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/twitter/android/h;->a(Lcom/twitter/android/ActivityCursor;ILandroid/util/SparseArray;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected a(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;Lcom/twitter/android/ActivityCursor$IdType;)Ljava/util/ArrayList;
    .locals 6

    const/4 v2, 0x2

    sget-object v0, Lcom/twitter/android/i;->a:[I

    invoke-virtual {p2}, Lcom/twitter/android/ActivityCursor$ObjectField;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    const/4 v0, 0x7

    invoke-virtual {p1, v0}, Lcom/twitter/android/ActivityCursor;->getInt(I)I

    move-result v0

    if-ne v2, v0, :cond_0

    const/16 v2, 0x8

    iget-object v3, p0, Lcom/twitter/android/h;->m:Landroid/util/SparseArray;

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/h;->a(Lcom/twitter/android/ActivityCursor;ILandroid/util/SparseArray;Lcom/twitter/android/ActivityCursor$ObjectField;Lcom/twitter/android/ActivityCursor$IdType;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lcom/twitter/android/ActivityCursor;->getInt(I)I

    move-result v0

    if-ne v2, v0, :cond_0

    const/16 v2, 0xb

    iget-object v3, p0, Lcom/twitter/android/h;->m:Landroid/util/SparseArray;

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/h;->a(Lcom/twitter/android/ActivityCursor;ILandroid/util/SparseArray;Lcom/twitter/android/ActivityCursor$ObjectField;Lcom/twitter/android/ActivityCursor$IdType;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/h;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/h;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/j;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/h;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :goto_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v2, v0, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v2}, Lcom/twitter/library/widget/TweetView;->b()V

    iget-object v0, v0, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->a()V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public a(J)V
    .locals 0

    iput-wide p1, p0, Lcom/twitter/android/h;->q:J

    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/twitter/android/h;->d(Landroid/view/View;)Lcom/twitter/library/widget/TweetView;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetView;->setDt2fPressed(Z)V

    :cond_0
    return-void
.end method

.method protected a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 10

    const-wide/16 v4, -0x1

    const/16 v9, 0x8

    const/4 v1, 0x0

    const/4 v6, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/m;

    iget-object v2, p0, Lcom/twitter/android/h;->c:Ljava/util/ArrayList;

    const/16 v3, 0xc

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/twitter/android/m;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, v0, Lcom/twitter/android/m;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v7

    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-wide v8, v4

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/android/h;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/provider/ActivityDataList;JIIJ)V

    return-void

    :cond_0
    iget-object v2, v0, Lcom/twitter/android/m;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v9}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, v0, Lcom/twitter/android/m;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected a(Landroid/view/View;Landroid/content/res/Resources;Landroid/database/Cursor;JI[IZJ)V
    .locals 15

    move-object/from16 v1, p3

    check-cast v1, Lcom/twitter/android/ActivityCursor;

    sget-object v2, Lcom/twitter/android/ActivityCursor$ObjectField;->a:Lcom/twitter/android/ActivityCursor$ObjectField;

    invoke-virtual {p0, v1, v2}, Lcom/twitter/android/h;->a(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v13

    const/4 v2, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    if-eqz p8, :cond_0

    sget-object v2, Lcom/twitter/android/ActivityCursor$ObjectField;->a:Lcom/twitter/android/ActivityCursor$ObjectField;

    :goto_0
    invoke-virtual {p0, v1, v2}, Lcom/twitter/android/h;->a(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz p8, :cond_1

    const/4 v1, 0x3

    :goto_1
    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const v5, 0x7f020116    # com.twitter.android.R.drawable.ic_activity_follow_default

    move-object v1, p0

    move-object/from16 v2, p1

    move-wide/from16 v6, p4

    move/from16 v8, p6

    move-object/from16 v9, p2

    move-wide/from16 v10, p9

    invoke-virtual/range {v1 .. v11}, Lcom/twitter/android/h;->a(Landroid/view/View;Ljava/util/ArrayList;IIJILandroid/content/res/Resources;J)V

    const/4 v12, 0x4

    move-object v5, p0

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p7

    move-object/from16 v9, p7

    move-object v10, v13

    move v11, v14

    move v13, v4

    invoke-virtual/range {v5 .. v13}, Lcom/twitter/android/h;->a(Landroid/view/View;Landroid/content/res/Resources;[I[ILjava/util/ArrayList;III)V

    move-object/from16 v0, p1

    invoke-virtual {p0, v0, v3}, Lcom/twitter/android/h;->a(Landroid/view/View;Ljava/util/ArrayList;)V

    return-void

    :cond_0
    sget-object v2, Lcom/twitter/android/ActivityCursor$ObjectField;->b:Lcom/twitter/android/ActivityCursor$ObjectField;

    goto :goto_0

    :cond_1
    const/4 v1, 0x6

    goto :goto_1
.end method

.method protected a(Landroid/view/View;Landroid/content/res/Resources;[I[ILjava/util/ArrayList;III)V
    .locals 9

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/n;

    iget-object v1, v0, Lcom/twitter/android/n;->a:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/twitter/android/n;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/q;

    iget-object v1, v0, Lcom/twitter/android/q;->e:Landroid/widget/TextView;

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, Lcom/twitter/android/h;->a(Landroid/widget/TextView;Landroid/content/res/Resources;[I[ILjava/util/ArrayList;III)V

    :cond_0
    return-void
.end method

.method protected a(Landroid/view/View;Lcom/twitter/library/provider/Tweet;JIII)V
    .locals 4

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/j;

    iget-object v1, p0, Lcom/twitter/android/h;->i:Lcom/twitter/library/util/FriendshipCache;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/h;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v1, p2}, Lcom/twitter/library/util/FriendshipCache;->a(Lcom/twitter/library/provider/Tweet;)V

    :cond_0
    iput-wide p3, v0, Lcom/twitter/android/j;->a:J

    iget-object v1, v0, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v2, p0, Lcom/twitter/android/h;->a:Lcom/twitter/android/client/c;

    iget-boolean v2, v2, Lcom/twitter/android/client/c;->f:Z

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/TweetView;->setRenderRTL(Z)V

    iget-object v1, v0, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    iget-boolean v2, p0, Lcom/twitter/android/h;->z:Z

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/TweetView;->setDisplaySensitiveMedia(Z)V

    iget-object v2, v0, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    iget-boolean v1, p0, Lcom/twitter/android/h;->w:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/twitter/android/h;->y:Z

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/twitter/library/widget/TweetView;->setAlwaysExpandMedia(Z)V

    iget-object v1, v0, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v2, p0, Lcom/twitter/android/h;->a:Lcom/twitter/android/client/c;

    invoke-virtual {v2}, Lcom/twitter/android/client/c;->ad()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/TweetView;->setSmartCrop(Z)V

    iget-object v1, v0, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    iget-boolean v2, p0, Lcom/twitter/android/h;->v:Z

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/TweetView;->setSimplifyUrls(Z)V

    iget-object v1, v0, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v2, p0, Lcom/twitter/android/h;->a:Lcom/twitter/android/client/c;

    invoke-virtual {v2}, Lcom/twitter/android/client/c;->al()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/TweetView;->setAmplifyEnabled(Z)V

    iget-object v1, v0, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v2, p0, Lcom/twitter/android/h;->a:Lcom/twitter/android/client/c;

    invoke-virtual {v2}, Lcom/twitter/android/client/c;->V()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/TweetView;->setContentSize(F)V

    iget-object v1, v0, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v2, p0, Lcom/twitter/android/h;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/android/h;->b:Lcom/twitter/library/client/aa;

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, Lcom/twitter/library/provider/Tweet;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/TweetView;->setWillTranslate(Z)V

    iget-object v1, v0, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v1, p7}, Lcom/twitter/library/widget/TweetView;->setSocialContextCount(I)V

    iget-object v1, v0, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v1, p5}, Lcom/twitter/library/widget/TweetView;->setSocialContextType(I)V

    iget-object v1, v0, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    iget-boolean v2, p0, Lcom/twitter/android/h;->x:Z

    const/4 v3, 0x0

    invoke-virtual {v1, p2, v2, v3}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/provider/Tweet;ZLcom/twitter/library/card/k;)V

    iget-object v0, v0, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    sget-object v1, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;->c:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetView;->setRepliedUserSummaryMode(Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;)V

    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected a(Landroid/view/View;Ljava/util/ArrayList;)V
    .locals 1

    const/4 v0, 0x3

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/h;->a(Landroid/view/View;Ljava/util/ArrayList;I)V

    return-void
.end method

.method protected a(Landroid/view/View;Ljava/util/ArrayList;I)V
    .locals 4

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/n;

    iget-object v1, v0, Lcom/twitter/android/n;->c:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/twitter/android/n;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/k;

    iget-object v1, v0, Lcom/twitter/android/k;->b:Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    invoke-virtual {p0, p2}, Lcom/twitter/android/h;->a(Ljava/util/ArrayList;)I

    move-result v1

    if-le v1, p3, :cond_1

    iget-object v2, v0, Lcom/twitter/android/k;->b:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, v0, Lcom/twitter/android/k;->b:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0, p2, p3, v1}, Lcom/twitter/android/h;->a(Landroid/view/ViewGroup;Ljava/util/ArrayList;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, v0, Lcom/twitter/android/k;->b:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method protected a(Landroid/view/View;Ljava/util/ArrayList;IIJILandroid/content/res/Resources;J)V
    .locals 17

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    move-object v13, v4

    check-cast v13, Lcom/twitter/android/n;

    iget-object v4, v13, Lcom/twitter/android/n;->b:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v15

    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v4, v15}, Ljava/lang/Math;->min(II)I

    move-result v16

    const/4 v4, 0x0

    move v14, v4

    :goto_0
    if-ge v14, v15, :cond_1

    move/from16 v0, v16

    if-ge v14, v0, :cond_0

    iget-object v4, v13, Lcom/twitter/android/n;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, v13, Lcom/twitter/android/n;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/twitter/library/api/TwitterUser;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/h;->a:Lcom/twitter/android/client/c;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/h;->i:Lcom/twitter/library/util/FriendshipCache;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/h;->h:Lcom/twitter/library/view/c;

    const/4 v10, 0x1

    move-object/from16 v8, p8

    move-wide/from16 v11, p9

    invoke-static/range {v4 .. v12}, Lcom/twitter/android/r;->a(Landroid/view/View;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/android/client/c;Lcom/twitter/library/util/FriendshipCache;Landroid/content/res/Resources;Lcom/twitter/library/view/c;ZJ)V

    :goto_1
    add-int/lit8 v4, v14, 0x1

    move v14, v4

    goto :goto_0

    :cond_0
    iget-object v4, v13, Lcom/twitter/android/n;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_1
    iget-object v4, v13, Lcom/twitter/android/n;->a:Landroid/view/View;

    if-eqz v4, :cond_2

    iget-object v4, v13, Lcom/twitter/android/n;->a:Landroid/view/View;

    move/from16 v0, p4

    move-wide/from16 v1, p5

    move/from16 v3, p7

    invoke-static {v4, v0, v1, v2, v3}, Lcom/twitter/android/q;->a(Landroid/view/View;IJI)V

    :cond_2
    iget-object v4, v13, Lcom/twitter/android/n;->c:Landroid/view/View;

    if-eqz v4, :cond_3

    iget-object v4, v13, Lcom/twitter/android/n;->c:Landroid/view/View;

    move/from16 v5, p3

    move-wide/from16 v6, p5

    move/from16 v8, p7

    move-object/from16 v9, p8

    invoke-static/range {v4 .. v9}, Lcom/twitter/android/k;->a(Landroid/view/View;IJILandroid/content/res/Resources;)V

    :cond_3
    return-void
.end method

.method protected a(Landroid/view/ViewGroup;Ljava/util/ArrayList;)V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/twitter/android/h;->a(Landroid/view/ViewGroup;Ljava/util/ArrayList;II)V

    return-void
.end method

.method protected a(Landroid/view/ViewGroup;Ljava/util/ArrayList;II)V
    .locals 7

    const/16 v6, 0x8

    const/4 v4, 0x0

    invoke-virtual {p0, p2}, Lcom/twitter/android/h;->a(Ljava/util/ArrayList;)I

    move-result v0

    sub-int/2addr v0, p3

    if-lez v0, :cond_1

    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    move v3, v4

    :goto_0
    if-ge v3, v2, :cond_0

    if-ge p3, p4, :cond_0

    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/h;->a(Landroid/widget/ImageView;Lcom/twitter/library/api/TwitterUser;)V

    add-int/lit8 p3, p3, 0x1

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_1
    if-ge v1, v5, :cond_2

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    invoke-virtual {p1, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_2
    return-void
.end method

.method protected a(Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V
    .locals 1

    if-eqz p2, :cond_0

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_0
    return-void

    :cond_0
    const v0, 0x7f02003b    # com.twitter.android.R.drawable.bg_no_profile_photo_sm

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method protected a(Landroid/widget/ImageView;Lcom/twitter/library/api/TwitterUser;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/h;->a:Lcom/twitter/android/client/c;

    iget-object v0, v0, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    const/4 v1, 0x2

    iget-object v2, p2, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/twitter/library/widget/ap;->a(ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/h;->a(Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V

    invoke-virtual {p2}, Lcom/twitter/library/api/TwitterUser;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    return-void
.end method

.method protected a(Landroid/widget/ImageView;Ljava/util/HashMap;)V
    .locals 2

    invoke-virtual {p1}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ae;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/util/ae;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/h;->a(Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method protected a(Landroid/widget/TextView;Landroid/content/res/Resources;[I[ILjava/util/ArrayList;III)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/h;->a:Lcom/twitter/android/client/c;

    iget-boolean v1, v0, Lcom/twitter/android/client/c;->f:Z

    invoke-virtual {p0, p5}, Lcom/twitter/android/h;->a(Ljava/util/ArrayList;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x3

    aget v2, p3, v0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    aput-object v0, v3, v4

    add-int/lit8 v0, p6, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-virtual {p2, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/twitter/library/util/Util;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/h;->r:[Lcom/twitter/internal/android/widget/TypefacesSpan;

    invoke-virtual {p0, p1, v0, v1}, Lcom/twitter/android/h;->a(Landroid/widget/TextView;Ljava/lang/String;[Lcom/twitter/internal/android/widget/TypefacesSpan;)V

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, p1, v2, v2}, Lcom/twitter/android/h;->a(Landroid/widget/TextView;Ljava/lang/String;[Lcom/twitter/internal/android/widget/TypefacesSpan;)V

    goto :goto_0

    :pswitch_1
    if-ne p8, v3, :cond_0

    const/4 v0, 0x4

    aget v2, p3, v0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    aput-object v0, v3, v4

    invoke-static {p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-virtual {p2, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/twitter/library/util/Util;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/h;->r:[Lcom/twitter/internal/android/widget/TypefacesSpan;

    invoke-virtual {p0, p1, v0, v1}, Lcom/twitter/android/h;->a(Landroid/widget/TextView;Ljava/lang/String;[Lcom/twitter/internal/android/widget/TypefacesSpan;)V

    goto :goto_0

    :cond_0
    if-le p8, v5, :cond_1

    const/4 v0, 0x5

    aget v2, p3, v0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    aput-object v0, v3, v4

    invoke-static {p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-virtual {p2, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/twitter/library/util/Util;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/h;->r:[Lcom/twitter/internal/android/widget/TypefacesSpan;

    invoke-virtual {p0, p1, v0, v1}, Lcom/twitter/android/h;->a(Landroid/widget/TextView;Ljava/lang/String;[Lcom/twitter/internal/android/widget/TypefacesSpan;)V

    goto :goto_0

    :cond_1
    aget v2, p3, v5

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {p5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    aput-object v0, v3, v4

    invoke-virtual {p2, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/twitter/library/util/Util;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/h;->r:[Lcom/twitter/internal/android/widget/TypefacesSpan;

    invoke-virtual {p0, p1, v0, v1}, Lcom/twitter/android/h;->a(Landroid/widget/TextView;Ljava/lang/String;[Lcom/twitter/internal/android/widget/TypefacesSpan;)V

    goto :goto_0

    :pswitch_2
    aget v2, p3, v3

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    aput-object v0, v3, v4

    invoke-virtual {p5, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    aput-object v0, v3, v5

    invoke-virtual {p2, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/twitter/library/util/Util;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/h;->s:[Lcom/twitter/internal/android/widget/TypefacesSpan;

    invoke-virtual {p0, p1, v0, v1}, Lcom/twitter/android/h;->a(Landroid/widget/TextView;Ljava/lang/String;[Lcom/twitter/internal/android/widget/TypefacesSpan;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected a(Landroid/widget/TextView;Ljava/lang/String;[Lcom/twitter/internal/android/widget/TypefacesSpan;)V
    .locals 1

    if-nez p2, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0x22

    invoke-static {p3, p2, v0}, Lcom/twitter/library/util/Util;->a([Ljava/lang/Object;Ljava/lang/String;C)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/p;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/h;->B:Lcom/twitter/android/p;

    return-void
.end method

.method protected a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/provider/ActivityDataList;JIIJ)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/h;->B:Lcom/twitter/android/p;

    if-eqz v0, :cond_1

    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x7

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v1, "tweet"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v1, "list"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string/jumbo v1, "position"

    invoke-virtual {v0, v1, p7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v1, "event_type"

    invoke-virtual {v0, v1, p6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v1, "activity_row_id"

    invoke-virtual {v0, v1, p4, p5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-wide/16 v1, 0x0

    cmp-long v1, p8, v1

    if-lez v1, :cond_0

    const-string/jumbo v1, "magic_rec_id"

    invoke-virtual {v0, v1, p8, p9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/h;->B:Lcom/twitter/android/p;

    invoke-interface {v1, v0}, Lcom/twitter/android/p;->a(Landroid/os/Bundle;)V

    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;Z)V
    .locals 5

    const/4 v0, 0x1

    iget v1, p1, Lcom/twitter/library/util/ao;->g:I

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/twitter/android/h;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_3

    iget-object v0, p0, Lcom/twitter/android/h;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/j;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/h;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    :goto_1
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v1}, Lcom/twitter/library/widget/TweetView;->getUserImageKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/twitter/library/util/ae;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v0, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v1, v1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v3, v1, p3}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Bitmap;Z)V

    :cond_2
    iget-object v1, v0, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v1}, Lcom/twitter/library/widget/TweetView;->getSummaryImageKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/util/ae;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v0, v1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v3, v0, p3}, Lcom/twitter/library/widget/TweetView;->b(Landroid/graphics/Bitmap;Z)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/h;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_2
    if-ltz v2, :cond_9

    iget-object v0, p0, Lcom/twitter/android/h;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/r;

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/h;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_4
    :goto_3
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_2

    :cond_5
    iget-object v1, v0, Lcom/twitter/android/r;->a:Lcom/twitter/android/widget/ActivityUserView;

    invoke-virtual {v1}, Lcom/twitter/android/widget/ActivityUserView;->getImageTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/twitter/library/util/ae;->b()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, v0, Lcom/twitter/android/r;->a:Lcom/twitter/android/widget/ActivityUserView;

    iget-object v0, v1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v3, v0}, Lcom/twitter/android/widget/ActivityUserView;->setUserImage(Landroid/graphics/Bitmap;)V

    goto :goto_3

    :cond_6
    const/4 v0, 0x2

    iget v1, p1, Lcom/twitter/library/util/ao;->g:I

    if-ne v0, v1, :cond_9

    iget-object v0, p0, Lcom/twitter/android/h;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_4
    if-ltz v3, :cond_9

    iget-object v0, p0, Lcom/twitter/android/h;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/twitter/android/h;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_7
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_4

    :cond_8
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v1

    if-nez v1, :cond_7

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_5
    if-ge v2, v4, :cond_7

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {p0, v1, p2}, Lcom/twitter/android/h;->a(Landroid/widget/ImageView;Ljava/util/HashMap;)V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_5

    :cond_9
    return-void
.end method

.method public a(Ljava/util/HashMap;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/h;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/h;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/j;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/h;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :goto_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, v0, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2}, Lcom/twitter/library/widget/TweetView;->a(Ljava/util/HashMap;Z)Z

    goto :goto_1

    :cond_1
    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/h;->y:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/twitter/android/h;->y:Z

    invoke-virtual {p0}, Lcom/twitter/android/h;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method protected b(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;
    .locals 1

    sget-object v0, Lcom/twitter/android/ActivityCursor$IdType;->a:Lcom/twitter/android/ActivityCursor$IdType;

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/h;->a(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;Lcom/twitter/android/ActivityCursor$IdType;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public b(J)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/h;->c:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public b(Landroid/view/View;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/twitter/android/h;->d(Landroid/view/View;)Lcom/twitter/library/widget/TweetView;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetView;->setDt2fPressed(Z)V

    iget-object v1, p0, Lcom/twitter/android/h;->p:Lcom/twitter/android/o;

    invoke-interface {v1, v0}, Lcom/twitter/android/o;->a(Lcom/twitter/library/widget/TweetView;)V

    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/h;->w:Z

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/twitter/android/h;->w:Z

    invoke-virtual {p0}, Lcom/twitter/android/h;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public c(Landroid/view/View;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/twitter/android/h;->d(Landroid/view/View;)Lcom/twitter/library/widget/TweetView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Ljy;->a(Lcom/twitter/library/widget/TweetView;)V

    :cond_0
    return-void
.end method

.method public c(Z)V
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/android/h;->x:Z

    if-eq v0, p1, :cond_1

    iput-boolean p1, p0, Lcom/twitter/android/h;->x:Z

    iget-boolean v0, p0, Lcom/twitter/android/h;->x:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/h;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/h;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/yd;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/h;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :goto_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->r()V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public d()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public d(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/h;->v:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/twitter/android/h;->v:Z

    invoke-virtual {p0}, Lcom/twitter/android/h;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public e()I
    .locals 1

    const/16 v0, 0x8

    return v0
.end method

.method public f()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/h;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method public g()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/h;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public getItemViewType(I)I
    .locals 4

    const/4 v3, 0x3

    const/4 v2, 0x1

    invoke-virtual {p0, p1}, Lcom/twitter/android/h;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ActivityCursor;

    invoke-virtual {v0, v2}, Lcom/twitter/android/ActivityCursor;->getInt(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    const/4 v0, -0x1

    :goto_0
    return v0

    :pswitch_1
    const/16 v0, 0xc

    goto :goto_0

    :pswitch_2
    iget v1, p0, Lcom/twitter/android/h;->t:I

    invoke-virtual {v0, v1}, Lcom/twitter/android/ActivityCursor;->getInt(I)I

    move-result v0

    if-lez v0, :cond_4

    invoke-static {v0}, Lcom/twitter/android/h;->a(I)I

    move-result v1

    if-ne v1, v2, :cond_0

    const/4 v0, 0x5

    goto :goto_0

    :cond_0
    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    const/4 v0, 0x6

    goto :goto_0

    :cond_1
    if-ne v1, v3, :cond_2

    if-ne v0, v3, :cond_2

    const/4 v0, 0x7

    goto :goto_0

    :cond_2
    const/4 v0, 0x4

    if-ne v1, v0, :cond_3

    const/16 v0, 0x8

    goto :goto_0

    :cond_3
    const/16 v0, 0x9

    goto :goto_0

    :cond_4
    :pswitch_3
    const/16 v0, 0xa

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    const/16 v0, 0x13

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    const/4 v7, 0x1

    const/4 v2, 0x0

    const-wide/16 v4, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/twitter/android/j;

    if-eqz v1, :cond_1

    invoke-direct {p0, p1}, Lcom/twitter/android/h;->d(Landroid/view/View;)Lcom/twitter/library/widget/TweetView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/library/provider/Tweet;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/h;->p:Lcom/twitter/android/o;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/twitter/android/o;->a(Lcom/twitter/library/widget/TweetView;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v1, v0, Lcom/twitter/android/r;

    if-eqz v1, :cond_2

    check-cast v0, Lcom/twitter/android/r;

    iget-object v1, p0, Lcom/twitter/android/h;->p:Lcom/twitter/android/o;

    iget-object v2, v0, Lcom/twitter/android/r;->b:Lcom/twitter/library/api/TwitterUser;

    iget-wide v2, v2, Lcom/twitter/library/api/TwitterUser;->userId:J

    iget-object v0, v0, Lcom/twitter/android/r;->b:Lcom/twitter/library/api/TwitterUser;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v0}, Lcom/twitter/android/o;->a(JLjava/lang/String;)V

    goto :goto_0

    :cond_2
    instance-of v1, v0, Lcom/twitter/android/k;

    if-eqz v1, :cond_3

    move-object v3, v0

    check-cast v3, Lcom/twitter/android/k;

    iget-object v0, p0, Lcom/twitter/android/h;->p:Lcom/twitter/android/o;

    iget-wide v1, v3, Lcom/twitter/android/k;->c:J

    iget v3, v3, Lcom/twitter/android/k;->d:I

    invoke-interface/range {v0 .. v5}, Lcom/twitter/android/o;->a(JIJ)V

    goto :goto_0

    :cond_3
    instance-of v1, v0, Lcom/twitter/android/q;

    if-eqz v1, :cond_4

    move-object v3, v0

    check-cast v3, Lcom/twitter/android/q;

    iget-object v0, p0, Lcom/twitter/android/h;->p:Lcom/twitter/android/o;

    iget-wide v1, v3, Lcom/twitter/android/q;->b:J

    iget v3, v3, Lcom/twitter/android/q;->c:I

    invoke-interface/range {v0 .. v5}, Lcom/twitter/android/o;->a(JIJ)V

    goto :goto_0

    :cond_4
    instance-of v1, v0, Lcom/twitter/android/ad;

    if-eqz v1, :cond_7

    move-object v3, v0

    check-cast v3, Lcom/twitter/android/ad;

    iget-object v6, v3, Lcom/twitter/android/ad;->i:Lcom/twitter/library/provider/ActivityDataList;

    if-eqz v6, :cond_5

    iget-object v0, p0, Lcom/twitter/android/h;->p:Lcom/twitter/android/o;

    iget-wide v1, v6, Lcom/twitter/library/provider/ActivityDataList;->id:J

    iget-object v3, v6, Lcom/twitter/library/provider/ActivityDataList;->name:Ljava/lang/String;

    iget-object v4, v6, Lcom/twitter/library/provider/ActivityDataList;->fullName:Ljava/lang/String;

    iget-wide v5, v6, Lcom/twitter/library/provider/ActivityDataList;->creatorUserId:J

    invoke-interface/range {v0 .. v6}, Lcom/twitter/android/o;->a(JLjava/lang/String;Ljava/lang/String;J)V

    goto :goto_0

    :cond_5
    iget v0, v3, Lcom/twitter/android/ad;->g:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_6

    iget-object v0, v3, Lcom/twitter/android/ad;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_6

    iget-object v0, v3, Lcom/twitter/android/ad;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v7, :cond_6

    iget-object v0, v3, Lcom/twitter/android/ad;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    iget-object v1, p0, Lcom/twitter/android/h;->p:Lcom/twitter/android/o;

    invoke-virtual {v0}, Lcom/twitter/library/api/TwitterUser;->a()J

    move-result-wide v2

    iget-object v0, v0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v0}, Lcom/twitter/android/o;->a(JLjava/lang/String;)V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/twitter/android/h;->p:Lcom/twitter/android/o;

    iget-wide v1, v3, Lcom/twitter/android/ad;->f:J

    iget v3, v3, Lcom/twitter/android/ad;->g:I

    invoke-interface/range {v0 .. v5}, Lcom/twitter/android/o;->a(JIJ)V

    goto :goto_0

    :cond_7
    instance-of v1, v0, Lcom/twitter/android/l;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/twitter/android/l;

    check-cast p1, Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {p1}, Lcom/twitter/library/widget/ActionButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {p1, v2}, Lcom/twitter/library/widget/ActionButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/twitter/android/h;->p:Lcom/twitter/android/o;

    iget-object v0, v0, Lcom/twitter/android/l;->a:Lcom/twitter/library/api/TwitterUser;

    invoke-interface {v1, v0}, Lcom/twitter/android/o;->b(Lcom/twitter/library/api/TwitterUser;)V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p1, v7}, Lcom/twitter/library/widget/ActionButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/twitter/android/h;->p:Lcom/twitter/android/o;

    iget-object v0, v0, Lcom/twitter/android/l;->a:Lcom/twitter/library/api/TwitterUser;

    invoke-interface {v1, v0}, Lcom/twitter/android/o;->a(Lcom/twitter/library/api/TwitterUser;)V

    goto/16 :goto_0
.end method

.method public onContentChanged()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/h;->h()V

    invoke-super {p0}, Landroid/support/v4/widget/CursorAdapter;->onContentChanged()V

    return-void
.end method

.method public swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/h;->h()V

    invoke-super {p0, p1}, Landroid/support/v4/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
