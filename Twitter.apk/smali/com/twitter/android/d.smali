.class Lcom/twitter/android/d;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/AccountSettingsActivity;

.field private final b:Ljava/lang/String;

.field private c:Z

.field private d:Z


# direct methods
.method public constructor <init>(Lcom/twitter/android/AccountSettingsActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/d;->a:Lcom/twitter/android/AccountSettingsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/twitter/android/d;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 11

    const/4 v10, 0x0

    iget-object v0, p0, Lcom/twitter/android/d;->a:Lcom/twitter/android/AccountSettingsActivity;

    invoke-virtual {v0}, Lcom/twitter/android/AccountSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/d;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/twitter/library/util/a;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v3

    if-nez v3, :cond_0

    :goto_0
    return-object v10

    :cond_0
    iget-boolean v4, p0, Lcom/twitter/android/d;->c:Z

    iget-boolean v5, p0, Lcom/twitter/android/d;->d:Z

    iget-object v0, p0, Lcom/twitter/android/d;->a:Lcom/twitter/android/AccountSettingsActivity;

    const-string/jumbo v6, "polling_interval"

    invoke-virtual {v0, v6}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iget-object v0, p0, Lcom/twitter/android/d;->a:Lcom/twitter/android/AccountSettingsActivity;

    iget v0, v0, Lcom/twitter/android/AccountSettingsActivity;->c:I

    if-eq v0, v6, :cond_4

    const/4 v0, 0x1

    :goto_1
    if-eqz v5, :cond_1

    sget-object v7, Lcom/twitter/library/provider/w;->c:Ljava/lang/String;

    invoke-static {v3, v7, v4}, Lcom/twitter/library/util/a;->a(Landroid/accounts/Account;Ljava/lang/String;Z)V

    :cond_1
    iget-object v3, p0, Lcom/twitter/android/d;->a:Lcom/twitter/android/AccountSettingsActivity;

    iget-boolean v3, v3, Lcom/twitter/android/AccountSettingsActivity;->d:Z

    new-instance v7, Landroid/content/ContentValues;

    const/4 v8, 0x4

    invoke-direct {v7, v8}, Landroid/content/ContentValues;-><init>(I)V

    const-string/jumbo v8, "interval"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {v1}, Lcom/twitter/library/provider/f;->a(Landroid/content/Context;)Lcom/twitter/library/provider/f;

    move-result-object v8

    invoke-virtual {v8, v2, v7, v3}, Lcom/twitter/library/provider/f;->a(Ljava/lang/String;Landroid/content/ContentValues;Z)I

    if-nez v0, :cond_2

    if-eqz v5, :cond_3

    :cond_2
    if-eqz v4, :cond_3

    invoke-static {v1}, Lcom/twitter/library/platform/TwitterDataSyncService;->a(Landroid/content/Context;)V

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/d;->a:Lcom/twitter/android/AccountSettingsActivity;

    iput-boolean v4, v0, Lcom/twitter/android/AccountSettingsActivity;->a:Z

    iget-object v0, p0, Lcom/twitter/android/d;->a:Lcom/twitter/android/AccountSettingsActivity;

    iput v6, v0, Lcom/twitter/android/AccountSettingsActivity;->c:I

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/d;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected onPreExecute()V
    .locals 2

    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    iget-object v0, p0, Lcom/twitter/android/d;->a:Lcom/twitter/android/AccountSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/AccountSettingsActivity;->e:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/d;->c:Z

    iget-object v0, p0, Lcom/twitter/android/d;->a:Lcom/twitter/android/AccountSettingsActivity;

    iget-boolean v0, v0, Lcom/twitter/android/AccountSettingsActivity;->a:Z

    iget-boolean v1, p0, Lcom/twitter/android/d;->c:Z

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/d;->d:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
