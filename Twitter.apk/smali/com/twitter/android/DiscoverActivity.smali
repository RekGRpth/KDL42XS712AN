.class public Lcom/twitter/android/DiscoverActivity;
.super Lcom/twitter/android/ListFragmentActivity;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/ListFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Intent;Lcom/twitter/library/client/e;)Lcom/twitter/android/iu;
    .locals 2

    new-instance v0, Lcom/twitter/android/iu;

    new-instance v1, Lcom/twitter/android/DiscoverFragment;

    invoke-direct {v1}, Lcom/twitter/android/DiscoverFragment;-><init>()V

    invoke-direct {v0, v1}, Lcom/twitter/android/iu;-><init>(Lcom/twitter/android/client/BaseListFragment;)V

    return-object v0
.end method

.method protected a(Landroid/content/Intent;)Ljava/lang/CharSequence;
    .locals 1

    const v0, 0x7f0f0499    # com.twitter.android.R.string.tab_title_discover

    invoke-virtual {p0, v0}, Lcom/twitter/android/DiscoverActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/twitter/android/ListFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverActivity;->L()Lcom/twitter/android/client/bn;

    move-result-object v0

    const-string/jumbo v1, "discover"

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/bn;->a(Ljava/lang/String;)Lcom/twitter/android/client/bn;

    return-void
.end method
