.class Lcom/twitter/android/ln;
.super Lcom/twitter/android/widget/cf;
.source "Twttr"


# instance fields
.field private a:Lcom/twitter/library/api/MediaTag;


# direct methods
.method public constructor <init>(Lcom/twitter/library/api/MediaTag;Landroid/content/res/Resources;)V
    .locals 9

    const v0, 0x7f0b007e    # com.twitter.android.R.color.white

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    const v0, 0x7f0b007b    # com.twitter.android.R.color.twitter_blue

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    const v0, 0x7f0c0088    # com.twitter.android.R.dimen.media_tag_span_border_radius

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v3, v0

    const v0, 0x7f0c008a    # com.twitter.android.R.dimen.media_tag_span_left_right_padding

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v4, v0

    const v0, 0x7f0c008c    # com.twitter.android.R.dimen.media_tag_span_top_padding

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v5, v0

    const v0, 0x7f0c0089    # com.twitter.android.R.dimen.media_tag_span_bottom_padding

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v6, v0

    const/4 v7, 0x0

    const v0, 0x7f0c008b    # com.twitter.android.R.dimen.media_tag_span_right_margin

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v8, v0

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/widget/cf;-><init>(IIFFFFFF)V

    iput-object p1, p0, Lcom/twitter/android/ln;->a:Lcom/twitter/library/api/MediaTag;

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/library/api/MediaTag;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ln;->a:Lcom/twitter/library/api/MediaTag;

    return-object v0
.end method
