.class public abstract Lcom/twitter/android/PostActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnLongClickListener;
.implements Lcom/twitter/android/AttachMediaListener;
.implements Lcom/twitter/android/f;
.implements Lcom/twitter/android/widget/ab;
.implements Lcom/twitter/android/widget/as;
.implements Lcom/twitter/android/widget/k;
.implements Lcom/twitter/android/wz;
.implements Lcom/twitter/library/platform/i;
.implements Lcom/twitter/library/util/ar;


# instance fields
.field protected A:Z

.field protected B:Lcom/twitter/android/util/z;

.field C:J

.field private E:Lcom/twitter/android/PostActivity$PhotoAction;

.field private F:Z

.field private G:Z

.field private H:J

.field private I:Z

.field private J:I

.field private K:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

.field private L:Landroid/content/SharedPreferences;

.field private M:Lcom/twitter/library/api/PromotedContent;

.field private N:Z

.field private O:Z

.field private P:Z

.field private Q:Z

.field private R:Z

.field private S:Z

.field private T:Lcom/twitter/library/telephony/a;

.field private U:I

.field private V:Z

.field private W:Landroid/widget/Button;

.field private X:Z

.field private Y:Z

.field private Z:Lcom/twitter/android/e;

.field protected final a:Landroid/support/v4/util/SimpleArrayMap;

.field protected b:Lcom/twitter/android/widget/MediaAttachmentsView;

.field protected c:Landroid/widget/ImageView;

.field protected d:Landroid/widget/TextView;

.field protected e:Landroid/widget/TextView;

.field protected f:Landroid/widget/ImageButton;

.field protected g:Landroid/widget/ImageButton;

.field protected h:Landroid/widget/TextView;

.field protected i:Lcom/twitter/android/widget/ComposerLayout;

.field protected j:Landroid/widget/TextView;

.field protected k:Landroid/view/View;

.field protected l:Landroid/view/View;

.field protected m:Lcom/twitter/library/widget/ObservableScrollView;

.field protected n:Lcom/twitter/android/widget/LocationFragment;

.field protected o:Lcom/twitter/android/widget/GalleryGridFragment;

.field protected p:Lcom/twitter/android/TweetBoxFragment;

.field protected q:Lcom/twitter/android/widget/PoiFragment;

.field protected r:Lcom/twitter/android/PostStorage;

.field protected s:Lcom/twitter/library/client/Session;

.field protected t:Z

.field protected u:Z

.field protected v:I

.field protected w:Z

.field protected x:J

.field protected y:Lcom/twitter/android/PhotoSelectHelper;

.field protected z:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, -0x1

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    new-instance v0, Landroid/support/v4/util/SimpleArrayMap;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/support/v4/util/SimpleArrayMap;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/PostActivity;->a:Landroid/support/v4/util/SimpleArrayMap;

    iput v2, p0, Lcom/twitter/android/PostActivity;->v:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/android/PostActivity;->x:J

    sget-object v0, Lcom/twitter/android/PostActivity$PhotoAction;->a:Lcom/twitter/android/PostActivity$PhotoAction;

    iput-object v0, p0, Lcom/twitter/android/PostActivity;->E:Lcom/twitter/android/PostActivity$PhotoAction;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/PostActivity;->R:Z

    iput v2, p0, Lcom/twitter/android/PostActivity;->U:I

    return-void
.end method

.method static synthetic A(Lcom/twitter/android/PostActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/PostActivity;->ah()V

    return-void
.end method

.method static synthetic B(Lcom/twitter/android/PostActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic C(Lcom/twitter/android/PostActivity;)Lcom/twitter/library/platform/LocationProducer;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->N()Lcom/twitter/library/platform/LocationProducer;

    move-result-object v0

    return-object v0
.end method

.method static synthetic D(Lcom/twitter/android/PostActivity;)Lcom/twitter/library/platform/LocationProducer;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->N()Lcom/twitter/library/platform/LocationProducer;

    move-result-object v0

    return-object v0
.end method

.method static synthetic E(Lcom/twitter/android/PostActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic F(Lcom/twitter/android/PostActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic G(Lcom/twitter/android/PostActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic H(Lcom/twitter/android/PostActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->W:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic I(Lcom/twitter/android/PostActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/PostActivity;->al()V

    return-void
.end method

.method private P()V
    .locals 3

    const v0, 0x7f0900df    # com.twitter.android.R.id.root_layout

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/pr;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/pr;-><init>(Lcom/twitter/android/PostActivity;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-void
.end method

.method private Q()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/twitter/android/util/z;->a()Lcom/twitter/android/util/z;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/PostActivity;->B:Lcom/twitter/android/util/z;

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    new-instance v1, Lcom/twitter/android/ps;

    invoke-direct {v1, p0}, Lcom/twitter/android/ps;-><init>(Lcom/twitter/android/PostActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PoiFragment;->a(Lcom/twitter/android/widget/bx;)V

    goto :goto_0
.end method

.method private a(Lcom/twitter/android/PostStorage;)Ljava/util/List;
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/twitter/android/PostStorage;->f()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/twitter/android/PostStorage;->d()Lcom/twitter/android/PostStorage$MediaItem;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/view/View;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00c6    # com.twitter.android.R.dimen.threshold_keyboard_visible

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    if-le v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->h()V

    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->R:Z

    invoke-direct {p0, v0}, Lcom/twitter/android/PostActivity;->k(Z)V

    return-void

    :cond_1
    iget v0, p0, Lcom/twitter/android/PostActivity;->v:I

    if-nez v0, :cond_0

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/PostActivity;->v:I

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/PostActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/PostActivity;->aq()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/PostActivity;Landroid/location/Location;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/PostActivity;->b(Landroid/location/Location;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/PostActivity;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/PostActivity;->a(Landroid/view/View;)V

    return-void
.end method

.method private a(Lcom/twitter/android/PostStorage;Lcom/twitter/android/PostActivity$PhotoAction;)V
    .locals 4

    invoke-direct {p0, p1}, Lcom/twitter/android/PostActivity;->a(Lcom/twitter/android/PostStorage;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/PostStorage$MediaItem;

    iget-object v0, v0, Lcom/twitter/android/PostStorage$MediaItem;->a:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->e(Landroid/net/Uri;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->y:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0, v1}, Lcom/twitter/android/PhotoSelectHelper;->a(Ljava/util/List;)V

    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2}, Ljava/util/LinkedHashSet;-><init>()V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/PostStorage$MediaItem;

    iget-object v3, v0, Lcom/twitter/android/PostStorage$MediaItem;->d:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    iget-object v0, v0, Lcom/twitter/android/PostStorage$MediaItem;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {p0, v0}, Lcom/twitter/android/PostActivity;->b(Ljava/util/List;)V

    invoke-virtual {p0, p2}, Lcom/twitter/android/PostActivity;->a(Lcom/twitter/android/PostActivity$PhotoAction;)V

    :cond_3
    return-void
.end method

.method private a(Lcom/twitter/library/api/TweetEntities;)V
    .locals 9

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    if-eqz p1, :cond_0

    invoke-virtual {p1, v6}, Lcom/twitter/library/api/TweetEntities;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/PostActivity;->H:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_1

    const-string/jumbo v0, ":drafts:composition:"

    :goto_0
    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v3, p0, Lcom/twitter/android/PostActivity;->s:Lcom/twitter/library/client/Session;

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v3, v8, [Ljava/lang/String;

    aput-object v0, v3, v7

    const-string/jumbo v4, "send_photo_tweet"

    aput-object v4, v3, v6

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    iget-object v2, p1, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/twitter/library/util/u;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v4, p0, Lcom/twitter/android/PostActivity;->s:Lcom/twitter/library/client/Session;

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v4, v8, [Ljava/lang/String;

    aput-object v0, v4, v7

    iget v0, p0, Lcom/twitter/android/PostActivity;->J:I

    if-ne v0, v6, :cond_2

    const-string/jumbo v0, "reply_with_tags"

    :goto_1
    aput-object v0, v4, v6

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->e(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_0
    return-void

    :cond_1
    const-string/jumbo v0, ":composition::"

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "tweet_with_tags"

    goto :goto_1
.end method

.method private static a(Ljava/lang/StringBuilder;Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/16 v0, 0x20

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 4

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    :cond_2
    new-instance v1, Lcom/twitter/android/PostStorage$MediaItem;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    invoke-direct {v1, v0}, Lcom/twitter/android/PostStorage$MediaItem;-><init>(Lcom/twitter/library/api/MediaEntity;)V

    iget-object v0, v1, Lcom/twitter/android/PostStorage$MediaItem;->a:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->e(Landroid/net/Uri;)V

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    :goto_1
    iget-object v1, p0, Lcom/twitter/android/PostActivity;->y:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v1, v0}, Lcom/twitter/android/PhotoSelectHelper;->a(Ljava/util/List;)V

    invoke-static {p1}, Lcom/twitter/library/util/u;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/PostActivity;->b(Ljava/util/List;)V

    sget-object v0, Lcom/twitter/android/PostActivity$PhotoAction;->b:Lcom/twitter/android/PostActivity$PhotoAction;

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->a(Lcom/twitter/android/PostActivity$PhotoAction;)V

    goto :goto_0

    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    new-instance v3, Lcom/twitter/android/PostStorage$MediaItem;

    invoke-direct {v3, v0}, Lcom/twitter/android/PostStorage$MediaItem;-><init>(Lcom/twitter/library/api/MediaEntity;)V

    iget-object v0, v3, Lcom/twitter/android/PostStorage$MediaItem;->a:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->e(Landroid/net/Uri;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method private a(Landroid/net/Uri;Z)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/PostActivity;->a:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v1, p1}, Landroid/support/v4/util/SimpleArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz p2, :cond_0

    const v1, 0x7f0f0317    # com.twitter.android.R.string.post_photo_already_attached

    invoke-virtual {p0, v1}, Lcom/twitter/android/PostActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/twitter/android/PostActivity;->a:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v1}, Landroid/support/v4/util/SimpleArrayMap;->size()I

    move-result v1

    const/4 v2, 0x4

    if-ge v1, v2, :cond_0

    invoke-virtual {p0, p1}, Lcom/twitter/android/PostActivity;->e(Landroid/net/Uri;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/PostActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/PostActivity;->Q:Z

    return p1
.end method

.method private ac()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->n:Lcom/twitter/android/widget/LocationFragment;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->n:Lcom/twitter/android/widget/LocationFragment;

    new-instance v1, Lcom/twitter/android/pt;

    invoke-direct {v1, p0}, Lcom/twitter/android/pt;-><init>(Lcom/twitter/android/PostActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/LocationFragment;->a(Lcom/twitter/android/widget/ap;)V

    goto :goto_0
.end method

.method private ad()Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/twitter/android/PostActivity;->am()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/twitter/android/PostActivity;->F:Z

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/client/c;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/twitter/android/PostActivity;->G:Z

    if-nez v2, :cond_1

    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/twitter/android/PostActivity;->z:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/twitter/android/PostActivity;->A:Z

    if-eqz v2, :cond_2

    :cond_0
    :goto_1
    return v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private ae()V
    .locals 6

    const v5, 0x7f090104    # com.twitter.android.R.id.action_drawer_container

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-direct {p0}, Lcom/twitter/android/PostActivity;->af()V

    invoke-direct {p0}, Lcom/twitter/android/PostActivity;->ad()Z

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    iget-boolean v2, p0, Lcom/twitter/android/PostActivity;->z:Z

    if-eqz v2, :cond_3

    if-eqz v0, :cond_0

    iput-boolean v3, p0, Lcom/twitter/android/PostActivity;->Y:Z

    :cond_0
    iget-object v2, p0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    if-nez v2, :cond_2

    const/4 v2, 0x0

    invoke-static {v3, v0, v2}, Lcom/twitter/android/widget/PoiFragment;->a(IZLjava/lang/String;)Lcom/twitter/android/widget/PoiFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    const-string/jumbo v2, "poi"

    invoke-virtual {v1, v5, v0, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    invoke-direct {p0}, Lcom/twitter/android/PostActivity;->Q()V

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->n:Lcom/twitter/android/widget/LocationFragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->n:Lcom/twitter/android/widget/LocationFragment;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    :cond_1
    :goto_1
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    invoke-virtual {p0, v4, v3}, Lcom/twitter/android/PostActivity;->a(IZ)V

    return-void

    :cond_2
    iget-object v2, p0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    invoke-virtual {v2, v0}, Lcom/twitter/android/widget/PoiFragment;->a(Z)V

    goto :goto_0

    :cond_3
    iput-boolean v4, p0, Lcom/twitter/android/PostActivity;->Y:Z

    iget-object v2, p0, Lcom/twitter/android/PostActivity;->n:Lcom/twitter/android/widget/LocationFragment;

    if-nez v2, :cond_4

    invoke-static {v0}, Lcom/twitter/android/widget/LocationFragment;->a(Z)Lcom/twitter/android/widget/LocationFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/PostActivity;->n:Lcom/twitter/android/widget/LocationFragment;

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->n:Lcom/twitter/android/widget/LocationFragment;

    const-string/jumbo v2, "location"

    invoke-virtual {v1, v5, v0, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    invoke-direct {p0}, Lcom/twitter/android/PostActivity;->ac()V

    :goto_2
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/twitter/android/PostActivity;->n:Lcom/twitter/android/widget/LocationFragment;

    invoke-virtual {v2, v0}, Lcom/twitter/android/widget/LocationFragment;->b(Z)V

    goto :goto_2
.end method

.method private af()V
    .locals 1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->ae()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/twitter/android/client/ao;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/PostActivity;->z:Z

    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->z:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->ag()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/PostActivity;->A:Z

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ag()V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/twitter/android/PostActivity;->c(Z)V

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->N()Lcom/twitter/library/platform/LocationProducer;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/library/platform/LocationProducer;->b(Lcom/twitter/library/platform/i;)V

    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->z:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/PostActivity;->v:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->z:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/twitter/android/PostActivity;->v:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v2, v0}, Lcom/twitter/android/PostActivity;->a(IZ)V

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/PostStorage;->a(Lcom/twitter/android/PostStorage$LocationItem;)V

    return-void
.end method

.method private ah()V
    .locals 5

    const/4 v4, 0x1

    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->F:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->s:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/library/api/UserSettings;

    move-result-object v0

    if-eqz v0, :cond_0

    iput-boolean v4, v0, Lcom/twitter/library/api/UserSettings;->c:Z

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/PostActivity;->s:Lcom/twitter/library/client/Session;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/UserSettings;Z)Ljava/lang/String;

    iput-boolean v4, p0, Lcom/twitter/android/PostActivity;->F:Z

    :cond_0
    return-void
.end method

.method private ai()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->s:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/twitter/android/PostActivity;->x:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/PostActivity;->x:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/client/aa;->a(J)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->d(Lcom/twitter/library/client/Session;)V

    :cond_0
    return-void
.end method

.method private aj()V
    .locals 5

    invoke-static {p0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Lcom/twitter/android/PostActivity;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->n()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/twitter/android/PostActivity;->H:J

    invoke-virtual {v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->h()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/twitter/android/PostActivity;->C:J

    invoke-virtual {v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->i()Lcom/twitter/library/api/PromotedContent;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/PostActivity;->M:Lcom/twitter/library/api/PromotedContent;

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    iget v2, p0, Lcom/twitter/android/PostActivity;->J:I

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const v2, 0x7f0f031c    # com.twitter.android.R.string.post_tweet

    invoke-virtual {p0, v2}, Lcom/twitter/android/PostActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/TweetBoxFragment;->b(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/twitter/android/PostActivity;->ak()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->g()[I

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v3, v1, v2}, Lcom/twitter/android/TweetBoxFragment;->a(Ljava/lang/String;[I)V

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->K:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    sget-object v2, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->f:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->e()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->y:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    sget-object v4, Lcom/twitter/android/PhotoSelectHelper$AddMediaMode;->c:Lcom/twitter/android/PhotoSelectHelper$AddMediaMode;

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/twitter/android/PhotoSelectHelper;->a(Landroid/net/Uri;JLcom/twitter/android/PhotoSelectHelper$AddMediaMode;)V

    goto :goto_0

    :pswitch_1
    const v2, 0x7f0f0313    # com.twitter.android.R.string.post_button_reply

    invoke-virtual {p0, v2}, Lcom/twitter/android/PostActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/TweetBoxFragment;->b(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->l()[Lcom/twitter/library/provider/ParcelableTweet;

    move-result-object v1

    if-eqz v1, :cond_3

    array-length v2, v1

    if-lez v2, :cond_3

    invoke-static {}, Lkn;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    const v2, 0x7f0900f6    # com.twitter.android.R.id.in_reply_to_box

    invoke-virtual {p0, v2}, Lcom/twitter/android/PostActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    new-instance v3, Lcom/twitter/android/pd;

    invoke-direct {v3, p0}, Lcom/twitter/android/pd;-><init>(Lcom/twitter/android/PostActivity;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    const v3, 0x7f0f056f    # com.twitter.android.R.string.write_reply

    invoke-virtual {p0, v3}, Lcom/twitter/android/PostActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/TweetBoxFragment;->a(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->f()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v3, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->g()[I

    move-result-object v0

    invoke-virtual {v3, v2, v0}, Lcom/twitter/android/TweetBoxFragment;->a(Ljava/lang/String;[I)V

    :goto_1
    array-length v0, v1

    add-int/lit8 v0, v0, -0x1

    aget-object v0, v1, v0

    invoke-interface {v0}, Lcom/twitter/library/provider/ParcelableTweet;->d()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/twitter/android/PostActivity;->C:J

    invoke-interface {v0}, Lcom/twitter/library/provider/ParcelableTweet;->m()Lcom/twitter/library/api/PromotedContent;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/PostActivity;->M:Lcom/twitter/library/api/PromotedContent;

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetBoxFragment;->a([Landroid/os/Parcelable;)V

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->d()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v2, "uri"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private ak()Ljava/lang/String;
    .locals 10

    const/4 v9, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "twitter"

    invoke-virtual {v0}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v0, "text"

    invoke-virtual {v3, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "message"

    invoke-virtual {v3, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    const-string/jumbo v4, "post"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    :try_start_0
    const-string/jumbo v1, "in_reply_to_status_id"

    invoke-virtual {v3, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/android/PostActivity;->C:J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    if-eqz v0, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    :goto_1
    const-string/jumbo v1, "url"

    invoke-virtual {v3, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/PostActivity;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    const-string/jumbo v1, "hashtags"

    invoke-virtual {v3, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string/jumbo v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    move v1, v2

    :goto_2
    if-ge v1, v5, :cond_2

    aget-object v6, v4, v1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "#"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/twitter/android/PostActivity;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :catch_0
    move-exception v1

    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/twitter/android/PostActivity;->C:J

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_1

    :cond_2
    const-string/jumbo v1, "via"

    invoke-virtual {v3, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    const v3, 0x7f0f04db    # com.twitter.android.R.string.tweet_via

    new-array v4, v9, [Ljava/lang/Object;

    aput-object v1, v4, v2

    invoke-virtual {p0, v3, v4}, Lcom/twitter/android/PostActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/PostActivity;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_3
    return-object v0

    :cond_4
    const-string/jumbo v4, "quote"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const v1, 0x7f0f033f    # com.twitter.android.R.string.quote_format

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const-string/jumbo v5, "screen_name"

    invoke-virtual {v3, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v2

    aput-object v0, v4, v9

    invoke-virtual {p0, v1, v4}, Lcom/twitter/android/PostActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_5
    const/4 v0, 0x0

    goto :goto_3

    :cond_6
    const-string/jumbo v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method private al()V
    .locals 6

    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->t:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "composition:lifeline_alerts:::impression"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    const/16 v0, 0x206

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->showDialog(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->p()V

    goto :goto_0
.end method

.method private am()Z
    .locals 4

    const/4 v0, 0x0

    new-instance v1, Lcom/twitter/library/client/f;

    iget-object v2, p0, Lcom/twitter/android/PostActivity;->s:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/twitter/android/PostActivity;->L:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "location_enabled"

    invoke-virtual {v1, v3}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string/jumbo v2, "location_enabled"

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;Z)Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz v2, :cond_0

    const-string/jumbo v1, "location_enabled"

    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method private an()V
    .locals 3

    new-instance v0, Lcom/twitter/library/client/f;

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->s:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v1, "location_enabled"

    iget-boolean v2, p0, Lcom/twitter/android/PostActivity;->u:Z

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;Z)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V

    return-void
.end method

.method private ao()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->K:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    sget-object v1, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->e:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ap()V
    .locals 6

    const/4 v4, 0x4

    const/4 v2, 0x2

    const/4 v5, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/PostActivity;->F:Z

    if-nez v1, :cond_0

    const/16 v0, 0x207

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->showDialog(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/twitter/android/client/c;->e()Z

    move-result v1

    if-nez v1, :cond_1

    const/16 v1, 0x204

    invoke-virtual {p0, v1}, Lcom/twitter/android/PostActivity;->showDialog(I)V

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "location_prompt::::impression"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->N()Lcom/twitter/library/platform/LocationProducer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/platform/LocationProducer;->e()Z

    move-result v1

    if-nez v1, :cond_2

    const/16 v0, 0x205

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->showDialog(I)V

    goto :goto_0

    :cond_2
    iget-boolean v1, p0, Lcom/twitter/android/PostActivity;->z:Z

    if-eqz v1, :cond_4

    iget v1, p0, Lcom/twitter/android/PostActivity;->v:I

    if-eq v1, v4, :cond_3

    invoke-virtual {p0, v4, v3}, Lcom/twitter/android/PostActivity;->a(IZ)V

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "compose:poi::map_pin:open"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v5, v3}, Lcom/twitter/android/PostActivity;->a(IZ)V

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "compose:poi::map_pin:close"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->N:Z

    if-eqz v0, :cond_6

    iget v0, p0, Lcom/twitter/android/PostActivity;->v:I

    if-eq v0, v2, :cond_5

    invoke-virtual {p0, v2, v3}, Lcom/twitter/android/PostActivity;->a(IZ)V

    goto :goto_0

    :cond_5
    invoke-virtual {p0, v5, v3}, Lcom/twitter/android/PostActivity;->a(IZ)V

    goto :goto_0

    :cond_6
    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->u:Z

    if-eqz v0, :cond_7

    invoke-direct {p0}, Lcom/twitter/android/PostActivity;->ag()V

    goto :goto_0

    :cond_7
    invoke-virtual {p0, v3}, Lcom/twitter/android/PostActivity;->c(Z)V

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->N()Lcom/twitter/library/platform/LocationProducer;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/library/platform/LocationProducer;->a(Lcom/twitter/library/platform/i;)V

    goto/16 :goto_0
.end method

.method private aq()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->l()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Lcom/twitter/android/TweetBoxFragment;->a(I)V

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/PostActivity;->a(IZ)V

    return-void
.end method

.method private b(Landroid/location/Location;)V
    .locals 12

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage;->c()Lcom/twitter/android/PostStorage$LocationItem;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v1, Lcom/twitter/android/PostStorage$LocationItem;

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v0}, Lcom/twitter/android/PostStorage$LocationItem;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0, v1}, Lcom/twitter/android/PostStorage;->a(Lcom/twitter/android/PostStorage$LocationItem;)V

    move-object v11, v1

    :goto_0
    if-eqz p1, :cond_0

    invoke-virtual {v11, p1}, Lcom/twitter/android/PostStorage$LocationItem;->a(Landroid/location/Location;)V

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->s:Lcom/twitter/library/client/Session;

    new-instance v2, Lcom/twitter/android/pu;

    invoke-direct {v2, p0}, Lcom/twitter/android/pu;-><init>(Lcom/twitter/android/PostActivity;)V

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v3

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v5

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v10}, Lcom/twitter/android/client/ao;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/android/client/ap;DDLjava/lang/String;ILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Lcom/twitter/android/PostStorage$LocationItem;->b(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->z:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->X:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->B:Lcom/twitter/android/util/z;

    invoke-virtual {v0, p1}, Lcom/twitter/android/util/z;->a(Landroid/location/Location;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->s:Lcom/twitter/library/client/Session;

    new-instance v2, Lcom/twitter/android/pu;

    invoke-direct {v2, p0}, Lcom/twitter/android/pu;-><init>(Lcom/twitter/android/PostActivity;)V

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v3

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v5

    const/4 v7, 0x0

    const/16 v8, 0xc8

    const-string/jumbo v9, "poi"

    const/4 v10, 0x1

    const/4 v11, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v11}, Lcom/twitter/android/client/ao;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/android/client/ap;DDLjava/lang/String;ILjava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/PostActivity;->X:Z

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->B:Lcom/twitter/android/util/z;

    invoke-virtual {v1}, Lcom/twitter/android/util/z;->e()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PoiFragment;->a(Ljava/util/List;)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PoiFragment;->b(Z)V

    goto :goto_1

    :cond_2
    move-object v11, v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/PostActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/PostActivity;->ap()V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/PostActivity;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/PostActivity;->j(Z)V

    return-void
.end method

.method private b(Lcom/twitter/android/PostStorage$MediaItem;)V
    .locals 6

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->n()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0, p1}, Lcom/twitter/android/PostStorage;->b(Lcom/twitter/android/PostStorage$MediaItem;)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage;->f()Ljava/util/Collection;

    move-result-object v2

    iget-boolean v3, p1, Lcom/twitter/android/PostStorage$MediaItem;->k:Z

    const/4 v1, 0x0

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/PostStorage$MediaItem;

    if-ne v0, p1, :cond_2

    :cond_1
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/PostStorage$MediaItem;

    iget-object v0, v0, Lcom/twitter/android/PostStorage$MediaItem;->a:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->d(Landroid/net/Uri;)V

    goto :goto_1

    :cond_2
    iget-object v5, v0, Lcom/twitter/android/PostStorage$MediaItem;->d:Ljava/util/ArrayList;

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/twitter/android/PostStorage$MediaItem;->d:Ljava/util/ArrayList;

    iput-object v5, p1, Lcom/twitter/android/PostStorage$MediaItem;->d:Ljava/util/ArrayList;

    :cond_3
    if-nez v3, :cond_4

    iget-boolean v5, v0, Lcom/twitter/android/PostStorage$MediaItem;->k:Z

    if-eqz v5, :cond_0

    :cond_4
    if-nez v1, :cond_5

    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    :cond_5
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->B()Z

    :goto_2
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->a:Landroid/support/v4/util/SimpleArrayMap;

    iget-object v1, p1, Lcom/twitter/android/PostStorage$MediaItem;->a:Landroid/net/Uri;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/util/SimpleArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_7
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0, p1}, Lcom/twitter/android/PostStorage;->a(Lcom/twitter/android/PostStorage$MediaItem;)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->a:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v0}, Landroid/support/v4/util/SimpleArrayMap;->clear()V

    goto :goto_2
.end method

.method private b(Ljava/util/List;)V
    .locals 4

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->W:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Lcom/twitter/library/util/u;->a(Landroid/content/Context;Ljava/util/List;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->W:Landroid/widget/Button;

    const v1, 0x7f020091    # com.twitter.android.R.drawable.btn_media_tag_prompt_with_tags

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->W:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b008c    # com.twitter.android.R.color.btn_media_tag_prompt_with_tags

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->W:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0004    # com.twitter.android.R.plurals.media_tag_prompt

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getQuantityText(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->W:Landroid/widget/Button;

    const v1, 0x7f020090    # com.twitter.android.R.drawable.btn_media_tag_prompt

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->W:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b008b    # com.twitter.android.R.color.btn_media_tag_prompt

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/android/PostActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->Q:Z

    return v0
.end method

.method static synthetic c(Lcom/twitter/android/PostActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/PostActivity;->Y:Z

    return p1
.end method

.method static synthetic d(Lcom/twitter/android/PostActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/PostActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->Y:Z

    return v0
.end method

.method static synthetic f(Lcom/twitter/android/PostActivity;)Lcom/twitter/library/platform/LocationProducer;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->N()Lcom/twitter/library/platform/LocationProducer;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/PostActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/PostActivity;)I
    .locals 1

    iget v0, p0, Lcom/twitter/android/PostActivity;->J:I

    return v0
.end method

.method static synthetic i(Lcom/twitter/android/PostActivity;)J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/android/PostActivity;->H:J

    return-wide v0
.end method

.method private i(Z)V
    .locals 4

    const v3, 0x7f090104    # com.twitter.android.R.id.action_drawer_container

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->k()Lcom/twitter/android/widget/GalleryGridFragment;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/PostActivity;->o:Lcom/twitter/android/widget/GalleryGridFragment;

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->o:Lcom/twitter/android/widget/GalleryGridFragment;

    const-string/jumbo v2, "gallery"

    invoke-virtual {v0, v3, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    iget-boolean v1, p0, Lcom/twitter/android/PostActivity;->z:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v1, p1, v2}, Lcom/twitter/android/widget/PoiFragment;->a(IZLjava/lang/String;)Lcom/twitter/android/widget/PoiFragment;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    const-string/jumbo v2, "poi"

    invoke-virtual {v0, v3, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const-string/jumbo v3, "android_at_mention_contact_1081"

    invoke-static {p0, v1, v2, v3}, Lju;->a(Landroid/content/Context;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->m()I

    move-result v2

    const-string/jumbo v3, "mention_visible"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v2, v1}, Lcom/twitter/android/TweetBoxFragment;->a(IZ)Lcom/twitter/android/TweetBoxFragment;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v1, p0}, Lcom/twitter/android/TweetBoxFragment;->a(Lcom/twitter/android/wz;)V

    const v1, 0x7f0900f7    # com.twitter.android.R.id.tweet_box

    iget-object v2, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    const-string/jumbo v3, "tweet_box"

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    return-void

    :cond_0
    invoke-static {p1}, Lcom/twitter/android/widget/LocationFragment;->a(Z)Lcom/twitter/android/widget/LocationFragment;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/PostActivity;->n:Lcom/twitter/android/widget/LocationFragment;

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->n:Lcom/twitter/android/widget/LocationFragment;

    const-string/jumbo v2, "location"

    invoke-virtual {v0, v3, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    goto :goto_0
.end method

.method static synthetic j(Lcom/twitter/android/PostActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private j(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->z:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->N:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->u:Z

    if-ne p1, v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->N()Lcom/twitter/library/platform/LocationProducer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/platform/LocationProducer;->e()Z

    move-result v0

    if-nez v0, :cond_3

    const/16 v0, 0x205

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->showDialog(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->c(Z)V

    goto :goto_0

    :cond_3
    if-eqz p1, :cond_4

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->c(Z)V

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/twitter/android/PostActivity;->ag()V

    goto :goto_0
.end method

.method static synthetic k(Lcom/twitter/android/PostActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private k(Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/twitter/android/PostActivity;->R:Z

    if-eqz p1, :cond_1

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->b()V

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->c()V

    goto :goto_0
.end method

.method static synthetic l(Lcom/twitter/android/PostActivity;)Lcom/twitter/library/api/PromotedContent;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->M:Lcom/twitter/library/api/PromotedContent;

    return-object v0
.end method

.method static synthetic m(Lcom/twitter/android/PostActivity;)Z
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/PostActivity;->ao()Z

    move-result v0

    return v0
.end method

.method static synthetic n(Lcom/twitter/android/PostActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic o(Lcom/twitter/android/PostActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic p(Lcom/twitter/android/PostActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/PostActivity;->ai()V

    return-void
.end method

.method static synthetic q(Lcom/twitter/android/PostActivity;)V
    .locals 0

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->j()V

    return-void
.end method

.method static synthetic r(Lcom/twitter/android/PostActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic s(Lcom/twitter/android/PostActivity;)Lcom/twitter/library/platform/LocationProducer;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->N()Lcom/twitter/library/platform/LocationProducer;

    move-result-object v0

    return-object v0
.end method

.method static synthetic t(Lcom/twitter/android/PostActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->N:Z

    return v0
.end method

.method static synthetic u(Lcom/twitter/android/PostActivity;)Lcom/twitter/library/platform/LocationProducer;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->N()Lcom/twitter/library/platform/LocationProducer;

    move-result-object v0

    return-object v0
.end method

.method static synthetic v(Lcom/twitter/android/PostActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic w(Lcom/twitter/android/PostActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic x(Lcom/twitter/android/PostActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic y(Lcom/twitter/android/PostActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic z(Lcom/twitter/android/PostActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected A()Z
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage;->f()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/PostStorage$MediaItem;

    iget-boolean v0, v0, Lcom/twitter/android/PostStorage$MediaItem;->k:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->a:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v0}, Landroid/support/v4/util/SimpleArrayMap;->size()I

    move-result v0

    const/4 v2, 0x4

    if-lt v0, v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected B()Z
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->A()Z

    move-result v4

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->a:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v0}, Landroid/support/v4/util/SimpleArrayMap;->size()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/twitter/android/PostActivity;->o:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v3, v4}, Lcom/twitter/android/widget/GalleryGridFragment;->a(Z)V

    iget-object v5, p0, Lcom/twitter/android/PostActivity;->o:Lcom/twitter/android/widget/GalleryGridFragment;

    if-nez v0, :cond_1

    move v3, v1

    :goto_1
    invoke-virtual {v5, v3}, Lcom/twitter/android/widget/GalleryGridFragment;->b(Z)V

    if-eqz v4, :cond_2

    if-eqz v0, :cond_2

    :goto_2
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v3, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->f()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a(Z)V

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->c(I)V

    return-object v0
.end method

.method public a()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/PostActivity;->a(IZ)V

    return-void
.end method

.method public a(F)V
    .locals 2

    iget v0, p0, Lcom/twitter/android/PostActivity;->v:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->o:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/GalleryGridFragment;->a(F)V

    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 4

    rsub-int v1, p1, 0x8c

    iget-object v2, p0, Lcom/twitter/android/PostActivity;->h:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    if-ltz v1, :cond_1

    const v0, 0x7f0b0057    # com.twitter.android.R.color.gray

    :goto_0
    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->h:Landroid/widget/TextView;

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    if-ltz v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/android/PostActivity;->w:Z

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->V()V

    return-void

    :cond_1
    const v0, 0x7f0b0072    # com.twitter.android.R.color.red

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected a(IZ)V
    .locals 1

    iget v0, p0, Lcom/twitter/android/PostActivity;->v:I

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->S:Z

    if-nez v0, :cond_1

    iput p1, p0, Lcom/twitter/android/PostActivity;->U:I

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/twitter/android/PostActivity;->c(I)V

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/PostActivity;->b(IZ)V

    goto :goto_0
.end method

.method protected a(Landroid/graphics/Bitmap;)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->c:Landroid/widget/ImageView;

    const v1, 0x7f02003a    # com.twitter.android.R.drawable.bg_no_profile_photo_md

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public a(Landroid/location/Location;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/PostActivity;->b(Landroid/location/Location;)V

    return-void
.end method

.method public a(Landroid/net/Uri;Lcom/twitter/android/AttachMediaListener$MediaAttachProgress;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->b:Lcom/twitter/android/widget/MediaAttachmentsView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/MediaAttachmentsView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->b:Lcom/twitter/android/widget/MediaAttachmentsView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/twitter/android/widget/MediaAttachmentsView;->a(Landroid/net/Uri;Lcom/twitter/android/widget/ax;)V

    return-void
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 9

    const v8, 0x7f0900eb    # com.twitter.android.R.id.composer

    const v4, 0x7f0900df    # com.twitter.android.R.id.root_layout

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/twitter/android/e;

    invoke-virtual {p0, v4}, Lcom/twitter/android/PostActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1, p0}, Lcom/twitter/android/e;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/twitter/android/f;)V

    iput-object v0, p0, Lcom/twitter/android/PostActivity;->Z:Lcom/twitter/android/e;

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v5

    invoke-static {p0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Lcom/twitter/android/PostActivity;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/android/composer/ComposerIntentWrapper;->b()Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/PostActivity;->K:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    invoke-direct {p0}, Lcom/twitter/android/PostActivity;->P()V

    invoke-virtual {p0, v4}, Lcom/twitter/android/PostActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/ComposerLayout;

    iput-object v0, p0, Lcom/twitter/android/PostActivity;->i:Lcom/twitter/android/widget/ComposerLayout;

    invoke-virtual {v0}, Lcom/twitter/android/widget/ComposerLayout;->d()V

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/ComposerLayout;->setDrawerLayoutAnimationListener(Lcom/twitter/android/widget/k;)V

    new-instance v0, Lcom/twitter/android/pv;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/pv;-><init>(Lcom/twitter/android/PostActivity;Lcom/twitter/android/ow;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->a(Lcom/twitter/library/client/j;)V

    invoke-virtual {v6}, Lcom/twitter/android/composer/ComposerIntentWrapper;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    move-object v1, v0

    :goto_0
    iput-object v1, p0, Lcom/twitter/android/PostActivity;->s:Lcom/twitter/library/client/Session;

    new-instance v0, Lcom/twitter/android/PhotoSelectHelper;

    const-string/jumbo v4, "composition"

    sget-object v7, Lcom/twitter/android/PhotoSelectHelper$MediaType;->d:Ljava/util/EnumSet;

    invoke-direct {v0, p0, p0, v4, v7}, Lcom/twitter/android/PhotoSelectHelper;-><init>(Landroid/app/Activity;Lcom/twitter/android/AttachMediaListener;Ljava/lang/String;Ljava/util/EnumSet;)V

    iput-object v0, p0, Lcom/twitter/android/PostActivity;->y:Lcom/twitter/android/PhotoSelectHelper;

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->y:Lcom/twitter/android/PhotoSelectHelper;

    iget-object v4, p0, Lcom/twitter/android/PostActivity;->s:Lcom/twitter/library/client/Session;

    invoke-virtual {v0, v4}, Lcom/twitter/android/PhotoSelectHelper;->a(Lcom/twitter/library/client/Session;)V

    invoke-virtual {p0, v3}, Lcom/twitter/android/PostActivity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/PostActivity;->L:Landroid/content/SharedPreferences;

    invoke-virtual {p0, v8}, Lcom/twitter/android/PostActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v4, Lcom/twitter/android/ow;

    invoke-direct {v4, p0}, Lcom/twitter/android/ow;-><init>(Lcom/twitter/android/PostActivity;)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09008d    # com.twitter.android.R.id.account_row

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    new-instance v0, Lcom/twitter/android/ph;

    invoke-direct {v0, p0}, Lcom/twitter/android/ph;-><init>(Lcom/twitter/android/PostActivity;)V

    invoke-virtual {v4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0900f0    # com.twitter.android.R.id.account_image

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/PostActivity;->c:Landroid/widget/ImageView;

    const v0, 0x7f0900f2    # com.twitter.android.R.id.account_name

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/PostActivity;->d:Landroid/widget/TextView;

    const v0, 0x7f0900f1    # com.twitter.android.R.id.user_name

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/PostActivity;->e:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->a(Lcom/twitter/library/api/TwitterUser;)V

    const v0, 0x7f0900ff    # com.twitter.android.R.id.location

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/twitter/android/PostActivity;->g:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->g:Landroid/widget/ImageButton;

    new-instance v4, Lcom/twitter/android/pm;

    invoke-direct {v4, p0}, Lcom/twitter/android/pm;-><init>(Lcom/twitter/android/PostActivity;)V

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0900fc    # com.twitter.android.R.id.scroll_padding

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/PostActivity;->k:Landroid/view/View;

    const v0, 0x7f0900fd    # com.twitter.android.R.id.mention_contact_tip

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/PostActivity;->l:Landroid/view/View;

    const v0, 0x7f0900f9    # com.twitter.android.R.id.media_tag_prompt

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/PostActivity;->W:Landroid/widget/Button;

    const v0, 0x7f0900ed    # com.twitter.android.R.id.scroll

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/ObservableScrollView;

    new-instance v4, Lcom/twitter/android/pn;

    invoke-direct {v4, p0, v0}, Lcom/twitter/android/pn;-><init>(Lcom/twitter/android/PostActivity;Lcom/twitter/library/widget/ObservableScrollView;)V

    invoke-virtual {v0, v4}, Lcom/twitter/library/widget/ObservableScrollView;->setObservableScrollViewListener(Lcom/twitter/library/widget/j;)V

    iput-object v0, p0, Lcom/twitter/android/PostActivity;->m:Lcom/twitter/library/widget/ObservableScrollView;

    const v0, 0x7f0900fb    # com.twitter.android.R.id.lifeline_and_location_name

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/PostActivity;->j:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->j:Landroid/widget/TextView;

    new-instance v4, Lcom/twitter/android/pp;

    invoke-direct {v4, p0, v5}, Lcom/twitter/android/pp;-><init>(Lcom/twitter/android/PostActivity;Lcom/twitter/android/client/c;)V

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->g()V

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->N()Lcom/twitter/library/platform/LocationProducer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/platform/LocationProducer;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/library/api/UserSettings;

    move-result-object v0

    if-nez v0, :cond_3

    iput-boolean v3, p0, Lcom/twitter/android/PostActivity;->F:Z

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->g:Landroid/widget/ImageButton;

    const/4 v4, 0x4

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    invoke-virtual {v5, v1}, Lcom/twitter/android/client/c;->d(Lcom/twitter/library/client/Session;)Ljava/lang/String;

    :goto_1
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/PostActivity;->x:J

    const v0, 0x7f0900f8    # com.twitter.android.R.id.media_attachments

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/MediaAttachmentsView;

    iput-object v0, p0, Lcom/twitter/android/PostActivity;->b:Lcom/twitter/android/widget/MediaAttachmentsView;

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->b:Lcom/twitter/android/widget/MediaAttachmentsView;

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->n()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/MediaAttachmentsView;->a(Z)V

    invoke-virtual {p0, v8}, Lcom/twitter/android/PostActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    invoke-static {p0}, Lcom/twitter/library/platform/p;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-boolean v0, Lcom/twitter/android/widget/LocationFragment;->a:Z

    if-eqz v0, :cond_5

    move v0, v2

    :goto_2
    iput-boolean v0, p0, Lcom/twitter/android/PostActivity;->N:Z

    iput-boolean v3, p0, Lcom/twitter/android/PostActivity;->S:Z

    invoke-direct {p0}, Lcom/twitter/android/PostActivity;->af()V

    if-eqz p1, :cond_8

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->y:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0, p1}, Lcom/twitter/android/PhotoSelectHelper;->a(Landroid/os/Bundle;)V

    const-string/jumbo v0, "data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/PostStorage;

    iput-object v0, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    const-string/jumbo v0, "loc"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    const-string/jumbo v0, "mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/PostActivity;->J:I

    const-string/jumbo v0, "do_post"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/PostActivity;->G:Z

    const-string/jumbo v0, "location_hint_count"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/PostActivity;->I:Z

    const-string/jumbo v0, "lifeline_alert"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    const-string/jumbo v0, "drawer"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/PostActivity;->U:I

    const-string/jumbo v0, "text"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v7, "selection"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v7

    invoke-virtual {v6, v0, v7}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Ljava/lang/String;[I)Lcom/twitter/android/composer/ComposerIntentWrapper;

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v7

    const-string/jumbo v0, "gallery"

    invoke-virtual {v7, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/GalleryGridFragment;

    iput-object v0, p0, Lcom/twitter/android/PostActivity;->o:Lcom/twitter/android/widget/GalleryGridFragment;

    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->z:Z

    if-eqz v0, :cond_6

    const-string/jumbo v0, "poi"

    invoke-virtual {v7, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/PoiFragment;

    iput-object v0, p0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    :goto_3
    const-string/jumbo v0, "tweet_box"

    invoke-virtual {v7, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/TweetBoxFragment;

    iput-object v0, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0, p0}, Lcom/twitter/android/TweetBoxFragment;->a(Lcom/twitter/android/wz;)V

    const-string/jumbo v0, "a"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    sget-object v7, Lcom/twitter/android/PostActivity$PhotoAction;->c:Lcom/twitter/android/PostActivity$PhotoAction;

    invoke-virtual {v7}, Lcom/twitter/android/PostActivity$PhotoAction;->ordinal()I

    move-result v7

    if-ne v0, v7, :cond_7

    sget-object v0, Lcom/twitter/android/PostActivity$PhotoAction;->c:Lcom/twitter/android/PostActivity$PhotoAction;

    :goto_4
    iget-object v7, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-direct {p0, v7, v0}, Lcom/twitter/android/PostActivity;->a(Lcom/twitter/android/PostStorage;Lcom/twitter/android/PostActivity$PhotoAction;)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage;->c()Lcom/twitter/android/PostStorage$LocationItem;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage$LocationItem;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->b(Ljava/lang/String;)V

    :cond_0
    const-string/jumbo v0, "show_link_hint"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/PostActivity;->P:Z

    move v0, v4

    :goto_5
    iget-object v4, p0, Lcom/twitter/android/PostActivity;->o:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->l()[Landroid/view/View;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/twitter/android/widget/GalleryGridFragment;->a([Landroid/view/View;)V

    iget-boolean v4, p0, Lcom/twitter/android/PostActivity;->z:Z

    if-eqz v4, :cond_e

    invoke-direct {p0}, Lcom/twitter/android/PostActivity;->Q()V

    :goto_6
    iget-object v4, p0, Lcom/twitter/android/PostActivity;->o:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v4, p0}, Lcom/twitter/android/widget/GalleryGridFragment;->a(Lcom/twitter/android/widget/ab;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->c(Z)V

    const v0, 0x7f090101    # com.twitter.android.R.id.lifeline_alert

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/twitter/android/PostActivity;->f:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->f:Landroid/widget/ImageButton;

    new-instance v4, Lcom/twitter/android/pq;

    invoke-direct {v4, p0}, Lcom/twitter/android/pq;-><init>(Lcom/twitter/android/PostActivity;)V

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->o()V

    invoke-virtual {p0, v1}, Lcom/twitter/android/PostActivity;->b(Z)V

    invoke-direct {p0}, Lcom/twitter/android/PostActivity;->aj()V

    invoke-virtual {v5, v2, p0}, Lcom/twitter/android/client/c;->a(ILcom/twitter/library/util/ar;)V

    const-string/jumbo v0, ":composition:::impression"

    invoke-virtual {v6}, Lcom/twitter/android/composer/ComposerIntentWrapper;->o()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_f

    iget-wide v6, p0, Lcom/twitter/android/PostActivity;->x:J

    const-string/jumbo v1, ":composition:::impression"

    invoke-virtual {v5, v6, v7, v1, v0}, Lcom/twitter/android/client/c;->a(JLjava/lang/String;Ljava/lang/String;)V

    :goto_7
    invoke-static {}, Lcom/twitter/library/telephony/a;->a()Lcom/twitter/library/telephony/a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/PostActivity;->T:Lcom/twitter/library/telephony/a;

    invoke-direct {p0}, Lcom/twitter/android/PostActivity;->ao()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    iget-wide v6, p0, Lcom/twitter/android/PostActivity;->x:J

    invoke-direct {v0, v6, v7}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v1, v2, [Ljava/lang/String;

    const-string/jumbo v2, "welcome:composition:::impression"

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_1
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_0

    :cond_3
    iget-boolean v0, v0, Lcom/twitter/library/api/UserSettings;->c:Z

    iput-boolean v0, p0, Lcom/twitter/android/PostActivity;->F:Z

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_1

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->g:Landroid/widget/ImageButton;

    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    iput-boolean v3, p0, Lcom/twitter/android/PostActivity;->F:Z

    goto/16 :goto_1

    :cond_5
    move v0, v3

    goto/16 :goto_2

    :cond_6
    const-string/jumbo v0, "location"

    invoke-virtual {v7, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/LocationFragment;

    iput-object v0, p0, Lcom/twitter/android/PostActivity;->n:Lcom/twitter/android/widget/LocationFragment;

    goto/16 :goto_3

    :cond_7
    sget-object v0, Lcom/twitter/android/PostActivity$PhotoAction;->b:Lcom/twitter/android/PostActivity$PhotoAction;

    goto/16 :goto_4

    :cond_8
    new-instance v0, Lcom/twitter/android/PostStorage;

    invoke-direct {v0}, Lcom/twitter/android/PostStorage;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    iput-boolean v3, p0, Lcom/twitter/android/PostActivity;->I:Z

    invoke-direct {p0}, Lcom/twitter/android/PostActivity;->ad()Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/twitter/android/PostActivity;->i(Z)V

    iput-boolean v3, p0, Lcom/twitter/android/PostActivity;->G:Z

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->K:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    sget-object v4, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->c:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    if-ne v0, v4, :cond_a

    iput v2, p0, Lcom/twitter/android/PostActivity;->J:I

    :goto_8
    invoke-virtual {v6}, Lcom/twitter/android/composer/ComposerIntentWrapper;->k()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v6}, Lcom/twitter/android/composer/ComposerIntentWrapper;->m()Lcom/twitter/library/api/TweetEntities;

    move-result-object v4

    if-eqz v4, :cond_b

    iget-object v0, v4, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/twitter/android/PostActivity;->a(Ljava/util/List;)V

    iget-object v0, v4, Lcom/twitter/library/api/TweetEntities;->contacts:Ljava/util/ArrayList;

    if-eqz v0, :cond_9

    iget-object v0, v4, Lcom/twitter/library/api/TweetEntities;->contacts:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    iget-object v4, v4, Lcom/twitter/library/api/TweetEntities;->contacts:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Lcom/twitter/android/TweetBoxFragment;->a(Ljava/util/ArrayList;)V

    :cond_9
    :goto_9
    invoke-virtual {v6}, Lcom/twitter/android/composer/ComposerIntentWrapper;->c()Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;

    move-result-object v0

    sget-object v4, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;->b:Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;

    if-ne v0, v4, :cond_c

    iput v2, p0, Lcom/twitter/android/PostActivity;->U:I

    :goto_a
    invoke-virtual {v6}, Lcom/twitter/android/composer/ComposerIntentWrapper;->c()Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;

    move-result-object v0

    sget-object v4, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;->c:Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;

    if-ne v0, v4, :cond_d

    move v0, v2

    :goto_b
    iput-boolean v0, p0, Lcom/twitter/android/PostActivity;->V:Z

    iput-boolean v2, p0, Lcom/twitter/android/PostActivity;->P:Z

    move v0, v1

    move v1, v3

    goto/16 :goto_5

    :cond_a
    iput v3, p0, Lcom/twitter/android/PostActivity;->J:I

    goto :goto_8

    :cond_b
    if-eqz v0, :cond_9

    sget-object v4, Lcom/twitter/android/PostActivity$PhotoAction;->c:Lcom/twitter/android/PostActivity$PhotoAction;

    invoke-virtual {p0, v4}, Lcom/twitter/android/PostActivity;->a(Lcom/twitter/android/PostActivity$PhotoAction;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->e(Landroid/net/Uri;)V

    iget-object v4, p0, Lcom/twitter/android/PostActivity;->y:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v4, v0, v3}, Lcom/twitter/android/PhotoSelectHelper;->a(Landroid/net/Uri;Z)V

    goto :goto_9

    :cond_c
    iput v3, p0, Lcom/twitter/android/PostActivity;->U:I

    goto :goto_a

    :cond_d
    move v0, v3

    goto :goto_b

    :cond_e
    invoke-direct {p0}, Lcom/twitter/android/PostActivity;->ac()V

    goto/16 :goto_6

    :cond_f
    iget-wide v0, p0, Lcom/twitter/android/PostActivity;->x:J

    new-array v4, v2, [Ljava/lang/String;

    const-string/jumbo v6, ":composition:::impression"

    aput-object v6, v4, v3

    invoke-virtual {v5, v0, v1, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto/16 :goto_7
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 3

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/twitter/library/provider/Tweet;

    invoke-direct {v0, p2}, Lcom/twitter/library/provider/Tweet;-><init>(Landroid/database/Cursor;)V

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v1, v0}, Lcom/twitter/android/TweetBoxFragment;->a(Landroid/os/Parcelable;)V

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->d()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/twitter/android/PostActivity;->C:J

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->m()Lcom/twitter/library/api/PromotedContent;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/PostActivity;->M:Lcom/twitter/library/api/PromotedContent;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;Lcom/twitter/android/PostStorage$MediaItem;)V
    .locals 3

    if-eqz p2, :cond_0

    iget-object v0, p2, Lcom/twitter/android/PostStorage$MediaItem;->a:Landroid/net/Uri;

    :goto_0
    sget-object v1, Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;->b:Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;

    if-ne p1, v1, :cond_4

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->n()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v1, v0}, Lcom/twitter/android/PostStorage;->a(Landroid/net/Uri;)Lcom/twitter/android/PostStorage$MediaItem;

    move-result-object v1

    :goto_1
    if-nez v1, :cond_2

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->d(Landroid/net/Uri;)V

    :goto_2
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v1}, Lcom/twitter/android/PostStorage;->d()Lcom/twitter/android/PostStorage$MediaItem;

    move-result-object v1

    goto :goto_1

    :cond_2
    iget-object v0, v1, Lcom/twitter/android/PostStorage$MediaItem;->e:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    invoke-virtual {p0, v1}, Lcom/twitter/android/PostActivity;->a(Lcom/twitter/android/PostStorage$MediaItem;)V

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->y:Lcom/twitter/android/PhotoSelectHelper;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/PhotoSelectHelper;->a(Lcom/twitter/android/PostStorage$MediaItem;Z)V

    goto :goto_2

    :cond_4
    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->n()Z

    move-result v1

    if-eqz v1, :cond_6

    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->a:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v1, v0}, Landroid/support/v4/util/SimpleArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v1, v0}, Lcom/twitter/android/PostStorage;->b(Landroid/net/Uri;)V

    :cond_5
    :goto_3
    iget-object v1, p0, Lcom/twitter/android/PostActivity;->b:Lcom/twitter/android/widget/MediaAttachmentsView;

    new-instance v2, Lcom/twitter/android/pk;

    invoke-direct {v2, p0}, Lcom/twitter/android/pk;-><init>(Lcom/twitter/android/PostActivity;)V

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/widget/MediaAttachmentsView;->b(Landroid/net/Uri;Lcom/twitter/android/widget/ax;)V

    const v0, 0x7f0f0222    # com.twitter.android.R.string.load_image_failure

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_2

    :cond_6
    iget-object v1, p0, Lcom/twitter/android/PostActivity;->a:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v1}, Landroid/support/v4/util/SimpleArrayMap;->clear()V

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v1}, Lcom/twitter/android/PostStorage;->e()V

    goto :goto_3
.end method

.method protected a(Lcom/twitter/android/PostActivity$PhotoAction;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->E:Lcom/twitter/android/PostActivity$PhotoAction;

    if-eq p1, v0, :cond_0

    iput-object p1, p0, Lcom/twitter/android/PostActivity;->E:Lcom/twitter/android/PostActivity$PhotoAction;

    invoke-virtual {p0, p1}, Lcom/twitter/android/PostActivity;->b(Lcom/twitter/android/PostActivity$PhotoAction;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/android/PostStorage$MediaItem;)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->b:Lcom/twitter/android/widget/MediaAttachmentsView;

    iget-object v1, p1, Lcom/twitter/android/PostStorage$MediaItem;->a:Landroid/net/Uri;

    iget-object v2, p1, Lcom/twitter/android/PostStorage$MediaItem;->e:Landroid/graphics/Bitmap;

    iget-boolean v3, p1, Lcom/twitter/android/PostStorage$MediaItem;->k:Z

    new-instance v4, Lcom/twitter/android/pf;

    invoke-direct {v4, p0}, Lcom/twitter/android/pf;-><init>(Lcom/twitter/android/PostActivity;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/widget/MediaAttachmentsView;->a(Landroid/net/Uri;Landroid/graphics/Bitmap;ZLcom/twitter/android/widget/ax;)V

    invoke-direct {p0, p1}, Lcom/twitter/android/PostActivity;->b(Lcom/twitter/android/PostStorage$MediaItem;)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->g()V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->m:Lcom/twitter/library/widget/ObservableScrollView;

    new-instance v1, Lcom/twitter/android/pg;

    invoke-direct {v1, p0}, Lcom/twitter/android/pg;-><init>(Lcom/twitter/android/PostActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ObservableScrollView;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->s:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    iget-boolean v1, p1, Lcom/twitter/android/PostStorage$MediaItem;->k:Z

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->W:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/PostActivity;->b(Ljava/util/List;)V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->s()V

    return-void

    :cond_1
    iget-boolean v1, v0, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    if-eqz v1, :cond_2

    iget v0, v0, Lcom/twitter/library/api/TwitterUser;->followersCount:I

    if-lez v0, :cond_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->W:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p1, Lcom/twitter/android/PostStorage$MediaItem;->d:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/twitter/android/PostActivity;->b(Ljava/util/List;)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->W:Landroid/widget/Button;

    new-instance v1, Lcom/twitter/android/pi;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/pi;-><init>(Lcom/twitter/android/PostActivity;Lcom/twitter/android/PostStorage$MediaItem;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0086    # com.twitter.android.R.dimen.media_tag_compose_prompt_hit_area_padding

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->W:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iget-object v3, p0, Lcom/twitter/android/PostActivity;->W:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/Button;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v3

    new-instance v4, Lcom/twitter/android/pj;

    invoke-direct {v4, p0, v2, v1, v0}, Lcom/twitter/android/pj;-><init>(Lcom/twitter/android/PostActivity;Landroid/graphics/Rect;ILandroid/view/View;)V

    invoke-virtual {v3, v4}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/api/TwitterUser;)V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->g(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->a(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->e:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->d:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x40

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected a(Lcom/twitter/library/api/UserSettings;)V
    .locals 2

    iget-boolean v0, p1, Lcom/twitter/library/api/UserSettings;->c:Z

    iput-boolean v0, p0, Lcom/twitter/android/PostActivity;->F:Z

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->N()Lcom/twitter/library/platform/LocationProducer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/platform/LocationProducer;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->g:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->g:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    invoke-direct {p0}, Lcom/twitter/android/PostActivity;->ae()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->s:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/api/TwitterUser;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ae;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/util/ae;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->a(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/aa;->d(Lcom/twitter/library/client/Session;)V

    iput-object v2, p0, Lcom/twitter/android/PostActivity;->s:Lcom/twitter/library/client/Session;

    invoke-virtual {p0, v3}, Lcom/twitter/android/PostActivity;->a(Lcom/twitter/library/api/TwitterUser;)V

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/library/api/UserSettings;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, v1}, Lcom/twitter/android/PostActivity;->a(Lcom/twitter/library/api/UserSettings;)V

    :goto_0
    iget-boolean v0, v3, Lcom/twitter/library/api/TwitterUser;->isLifelineInstitution:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->t:Z

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->b(Z)V

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->o()V

    return-void

    :cond_1
    invoke-virtual {v0, v2}, Lcom/twitter/android/client/c;->d(Lcom/twitter/library/client/Session;)Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->t:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->b(Z)V

    goto :goto_1
.end method

.method public a(Ljava/lang/String;I)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    const v0, 0x7f0901a5    # com.twitter.android.R.id.in_reply_to_text

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-lez p2, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f01e5    # com.twitter.android.R.string.in_reply_to_and_more

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f01e4    # com.twitter.android.R.string.in_reply_to

    new-array v3, v5, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    iput-object p1, v0, Lcom/twitter/android/PostStorage;->a:Ljava/util/ArrayList;

    const/16 v0, 0x202

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->showDialog(I)V

    return-void
.end method

.method public a(Z)V
    .locals 0

    return-void
.end method

.method public a([Lcom/twitter/library/api/TwitterContact;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->l:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public a(Landroid/net/Uri;)Z
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/PostActivity;->a(Landroid/net/Uri;Z)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/PostActivity;->al()V

    return-void
.end method

.method protected b(I)V
    .locals 0

    return-void
.end method

.method protected b(IZ)V
    .locals 12

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    iput p1, p0, Lcom/twitter/android/PostActivity;->v:I

    return-void

    :pswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/PostActivity;->O:Z

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->i:Lcom/twitter/android/widget/ComposerLayout;

    invoke-virtual {v0}, Lcom/twitter/android/widget/ComposerLayout;->d()V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetBoxFragment;->c(Z)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->k:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetBoxFragment;->c(Z)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->a:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v0}, Landroid/support/v4/util/SimpleArrayMap;->size()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v1, 0x1

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->i:Lcom/twitter/android/widget/ComposerLayout;

    const/4 v3, 0x0

    const/4 v5, 0x0

    move v2, p2

    move v4, v1

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/widget/ComposerLayout;->a(ZZZZZ)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetBoxFragment;->c(Z)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->i:Lcom/twitter/android/widget/ComposerLayout;

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/widget/ComposerLayout;->a(ZZZZZ)V

    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->u:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->n:Lcom/twitter/android/widget/LocationFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/LocationFragment;->b(Z)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetBoxFragment;->c(Z)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->i:Lcom/twitter/android/widget/ComposerLayout;

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    move v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/widget/ComposerLayout;->a(ZZZZZ)V

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v1}, Lcom/twitter/android/PostStorage;->c()Lcom/twitter/android/PostStorage$LocationItem;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage;->c()Lcom/twitter/android/PostStorage$LocationItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage$LocationItem;->a()Landroid/location/Location;

    move-result-object v0

    :cond_2
    if-eqz v0, :cond_4

    iget-boolean v1, p0, Lcom/twitter/android/PostActivity;->u:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->B:Lcom/twitter/android/util/z;

    invoke-virtual {v1, v0}, Lcom/twitter/android/util/z;->a(Landroid/location/Location;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PoiFragment;->b(Z)V

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->s:Lcom/twitter/library/client/Session;

    new-instance v2, Lcom/twitter/android/pu;

    invoke-direct {v2, p0}, Lcom/twitter/android/pu;-><init>(Lcom/twitter/android/PostActivity;)V

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v3

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v5

    const/4 v7, 0x0

    const/16 v8, 0xc8

    const-string/jumbo v9, "poi"

    const/4 v10, 0x1

    const/4 v11, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v11}, Lcom/twitter/android/client/ao;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/android/client/ap;DDLjava/lang/String;ILjava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    invoke-virtual {v0}, Lcom/twitter/android/widget/PoiFragment;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->B:Lcom/twitter/android/util/z;

    invoke-virtual {v1}, Lcom/twitter/android/util/z;->e()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PoiFragment;->a(Ljava/util/List;)V

    goto/16 :goto_0

    :cond_4
    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->u:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PoiFragment;->a(Z)V

    :cond_5
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PoiFragment;->b(Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/PostActivity;->X:Z

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->N()Lcom/twitter/library/platform/LocationProducer;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/library/platform/LocationProducer;->a(Lcom/twitter/library/platform/i;)V

    goto/16 :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->i:Lcom/twitter/android/widget/ComposerLayout;

    invoke-virtual {v0, p2}, Lcom/twitter/android/widget/ComposerLayout;->a(Z)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetBoxFragment;->c(Z)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/PostActivity;->Q:Z

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public b(Landroid/net/Uri;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->n()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->a:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v0, p1}, Landroid/support/v4/util/SimpleArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->y:Lcom/twitter/android/PhotoSelectHelper;

    new-instance v1, Lcom/twitter/android/pe;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/pe;-><init>(Lcom/twitter/android/PostActivity;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/PhotoSelectHelper;->a(Lcom/twitter/android/ou;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1, v4}, Lcom/twitter/android/PostActivity;->a(Landroid/net/Uri;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_2
    sget-object v0, Lcom/twitter/android/PostActivity$PhotoAction;->b:Lcom/twitter/android/PostActivity$PhotoAction;

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->a(Lcom/twitter/android/PostActivity$PhotoAction;)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->y:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/PhotoSelectHelper$AddMediaMode;->c:Lcom/twitter/android/PhotoSelectHelper$AddMediaMode;

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/twitter/android/PhotoSelectHelper;->a(Landroid/net/Uri;JLcom/twitter/android/PhotoSelectHelper$AddMediaMode;)V

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->n()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, v4, v5}, Lcom/twitter/android/PostActivity;->a(IZ)V

    goto :goto_0

    :cond_3
    invoke-direct {p0, p1, v5}, Lcom/twitter/android/PostActivity;->a(Landroid/net/Uri;Z)Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_0
.end method

.method protected abstract b(Lcom/twitter/android/PostActivity$PhotoAction;)V
.end method

.method protected b(Ljava/lang/String;)V
    .locals 7

    const/16 v4, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->s:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/PostActivity;->j:Landroid/widget/TextView;

    const v0, 0x7f0900fa    # com.twitter.android.R.id.badge_icon

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-boolean v3, p0, Lcom/twitter/android/PostActivity;->t:Z

    if-eqz v3, :cond_1

    if-eqz v1, :cond_1

    iget-object v1, v1, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    if-eqz p1, :cond_0

    iget-boolean v3, p0, Lcom/twitter/android/PostActivity;->u:Z

    if-eqz v3, :cond_0

    const v3, 0x7f0f020a    # com.twitter.android.R.string.lifeline_alert_title_with_loc

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v5

    aput-object p1, v4, v6

    invoke-virtual {p0, v3, v4}, Lcom/twitter/android/PostActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    const v1, 0x7f020123    # com.twitter.android.R.drawable.ic_badge_alert_default

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->q()V

    return-void

    :cond_0
    const v3, 0x7f0f0209    # com.twitter.android.R.string.lifeline_alert_title

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-virtual {p0, v3, v4}, Lcom/twitter/android/PostActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    if-eqz p1, :cond_2

    iget-boolean v1, p0, Lcom/twitter/android/PostActivity;->u:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/twitter/android/PostActivity;->z:Z

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setClickable(Z)V

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    const v1, 0x7f020280    # com.twitter.android.R.drawable.ic_tweet_attr_geo_poi_default

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    :cond_2
    const-string/jumbo v1, ""

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method protected b(Z)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    iput-boolean p1, p0, Lcom/twitter/android/PostActivity;->t:Z

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "composition:lifeline_alerts:::select"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->f:Landroid/widget/ImageButton;

    const v1, 0x7f020170    # com.twitter.android.R.drawable.ic_dialog_lifeline_alert_active

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0, v6}, Lcom/twitter/android/TweetBoxFragment;->b(Z)V

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage;->c()Lcom/twitter/android/PostStorage$LocationItem;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage$LocationItem;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->b(Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->f:Landroid/widget/ImageButton;

    const v1, 0x7f02016f    # com.twitter.android.R.drawable.ic_dialog_lifeline_alert

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0, v5}, Lcom/twitter/android/TweetBoxFragment;->b(Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->b(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected c(I)V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const/4 v1, 0x1

    if-ne p1, v1, :cond_2

    iget-boolean v1, p0, Lcom/twitter/android/PostActivity;->z:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/PostActivity;->o:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    :cond_0
    :goto_1
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    return-void

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/PostActivity;->n:Lcom/twitter/android/widget/LocationFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    goto :goto_0

    :cond_2
    const/4 v1, 0x2

    if-ne p1, v1, :cond_3

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->o:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->n:Lcom/twitter/android/widget/LocationFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->n:Lcom/twitter/android/widget/LocationFragment;

    invoke-virtual {v1}, Lcom/twitter/android/widget/LocationFragment;->a()V

    goto :goto_1

    :cond_3
    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->o:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    goto :goto_1
.end method

.method public c(Landroid/net/Uri;)V
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->a:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v0}, Landroid/support/v4/util/SimpleArrayMap;->size()I

    move-result v0

    const/4 v1, 0x4

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->a:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v0, p1}, Landroid/support/v4/util/SimpleArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/twitter/android/PostActivity$PhotoAction;->b:Lcom/twitter/android/PostActivity$PhotoAction;

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->a(Lcom/twitter/android/PostActivity$PhotoAction;)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->y:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/PhotoSelectHelper$AddMediaMode;->a:Lcom/twitter/android/PhotoSelectHelper$AddMediaMode;

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/twitter/android/PhotoSelectHelper;->a(Landroid/net/Uri;JLcom/twitter/android/PhotoSelectHelper$AddMediaMode;)V

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->n()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/PostActivity;->a(IZ)V

    goto :goto_0
.end method

.method c(Z)V
    .locals 4

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->g:Landroid/widget/ImageButton;

    const v0, 0x7f0f0059    # com.twitter.android.R.string.button_action_geo

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz p1, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0f02d7    # com.twitter.android.R.string.on_status

    invoke-virtual {p0, v3}, Lcom/twitter/android/PostActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f02016e    # com.twitter.android.R.drawable.ic_dialog_geo_active

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iput-boolean p1, p0, Lcom/twitter/android/PostActivity;->u:Z

    return-void

    :cond_0
    const v2, 0x7f02016d    # com.twitter.android.R.drawable.ic_dialog_geo

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/twitter/android/PostActivity;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->s()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->l:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->i:Lcom/twitter/android/widget/ComposerLayout;

    invoke-virtual {v0}, Lcom/twitter/android/widget/ComposerLayout;->requestLayout()V

    :cond_0
    return-void
.end method

.method public d(I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0, p1}, Lcom/twitter/android/PostStorage;->a(I)V

    return-void
.end method

.method protected d(Landroid/net/Uri;)V
    .locals 2

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0, p1}, Lcom/twitter/android/PostStorage;->b(Landroid/net/Uri;)V

    invoke-virtual {p0, p1}, Lcom/twitter/android/PostActivity;->f(Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->b:Lcom/twitter/android/widget/MediaAttachmentsView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/MediaAttachmentsView;->a(Landroid/net/Uri;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->b:Lcom/twitter/android/widget/MediaAttachmentsView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/MediaAttachmentsView;->getMediaCount()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->b:Lcom/twitter/android/widget/MediaAttachmentsView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/MediaAttachmentsView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->W:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->h()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/PostActivity;->b(Ljava/util/List;)V

    :cond_1
    sget-object v0, Lcom/twitter/android/PostActivity$PhotoAction;->a:Lcom/twitter/android/PostActivity$PhotoAction;

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->a(Lcom/twitter/android/PostActivity$PhotoAction;)V

    return-void

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage;->e()V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->a:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v0}, Landroid/support/v4/util/SimpleArrayMap;->clear()V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->o:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v0}, Lcom/twitter/android/widget/GalleryGridFragment;->a()V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->b:Lcom/twitter/android/widget/MediaAttachmentsView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/MediaAttachmentsView;->a()V

    goto :goto_0
.end method

.method protected d(Z)V
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v1, p1}, Lcom/twitter/android/TweetBoxFragment;->c(Z)V

    if-eqz p1, :cond_0

    iput v0, p0, Lcom/twitter/android/PostActivity;->v:I

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/PostActivity;->i:Lcom/twitter/android/widget/ComposerLayout;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/ComposerLayout;->setAllowGestures(Z)V

    return-void
.end method

.method protected e(Landroid/net/Uri;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->a:Landroid/support/v4/util/SimpleArrayMap;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, p1, v1}, Landroid/support/v4/util/SimpleArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->o:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/GalleryGridFragment;->b(Landroid/net/Uri;)V

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->B()Z

    :cond_0
    return-void
.end method

.method public e(Z)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->i:Lcom/twitter/android/widget/ComposerLayout;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/ComposerLayout;->setDrawerDraggableState(Z)V

    return-void
.end method

.method protected abstract f()I
.end method

.method protected f(Landroid/net/Uri;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->a:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v0, p1}, Landroid/support/v4/util/SimpleArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->o:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/GalleryGridFragment;->c(Landroid/net/Uri;)V

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->B()Z

    :cond_0
    return-void
.end method

.method public f(Z)V
    .locals 2

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/android/PostActivity;->k(Z)V

    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->z:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/PostActivity;->v:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/PoiFragment;->f(Z)V

    :cond_0
    return-void
.end method

.method public finish()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->finish()V

    const v0, 0x7f04000b    # com.twitter.android.R.anim.modal_activity_close_enter

    const v1, 0x7f04000c    # com.twitter.android.R.anim.modal_activity_close_exit

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/PostActivity;->overridePendingTransition(II)V

    return-void
.end method

.method protected abstract g()V
.end method

.method public g(Z)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/PostActivity;->k(Z)V

    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->z:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/PostActivity;->v:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/PoiFragment;->g(Z)V

    :cond_0
    return-void
.end method

.method protected h()V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->O:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->i:Lcom/twitter/android/widget/ComposerLayout;

    invoke-virtual {v0}, Lcom/twitter/android/widget/ComposerLayout;->d()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/PostActivity;->O:Z

    :cond_0
    return-void
.end method

.method public h(Z)V
    .locals 2

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/twitter/android/PostActivity;->k(Z)V

    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->z:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/PostActivity;->v:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/PoiFragment;->h(Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected j()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x203

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->showDialog(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->setResult(I)V

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->j()V

    goto :goto_0
.end method

.method protected abstract k()Lcom/twitter/android/widget/GalleryGridFragment;
.end method

.method protected abstract l()[Landroid/view/View;
.end method

.method protected abstract m()I
.end method

.method protected n()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected o()V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->s:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    iget-boolean v0, v0, Lcom/twitter/library/api/TwitterUser;->isLifelineInstitution:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->f:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->f:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    const/4 v3, 0x3

    const/4 v2, 0x0

    const/4 v1, -0x1

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/client/BaseFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    sparse-switch p1, :sswitch_data_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->y:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    move v1, p1

    move v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/PhotoSelectHelper;->a(IILandroid/content/Intent;J)V

    return-void

    :sswitch_0
    if-eqz p3, :cond_0

    invoke-virtual {p0, v3, v2}, Lcom/twitter/android/PostActivity;->a(IZ)V

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/android/PostActivity$PhotoAction;->b:Lcom/twitter/android/PostActivity$PhotoAction;

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->a(Lcom/twitter/android/PostActivity$PhotoAction;)V

    invoke-virtual {p0, v3, v2}, Lcom/twitter/android/PostActivity;->a(IZ)V

    goto :goto_0

    :sswitch_1
    if-eq p2, v1, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->V:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->finish()V

    goto :goto_0

    :cond_1
    iput-boolean v2, p0, Lcom/twitter/android/PostActivity;->V:Z

    sget-object v0, Lcom/twitter/android/PostActivity$PhotoAction;->c:Lcom/twitter/android/PostActivity$PhotoAction;

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->a(Lcom/twitter/android/PostActivity$PhotoAction;)V

    invoke-virtual {p0, v3, v2}, Lcom/twitter/android/PostActivity;->a(IZ)V

    goto :goto_0

    :sswitch_2
    invoke-static {p3}, Lcom/twitter/android/PhotoSelectHelper;->a(Landroid/content/Intent;)Landroid/net/Uri;

    move-result-object v0

    if-ne p2, v1, :cond_2

    if-nez v0, :cond_0

    :cond_2
    sget-object v0, Lcom/twitter/android/PostActivity$PhotoAction;->a:Lcom/twitter/android/PostActivity$PhotoAction;

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->a(Lcom/twitter/android/PostActivity$PhotoAction;)V

    goto :goto_0

    :sswitch_3
    if-eq p2, v1, :cond_3

    sget-object v0, Lcom/twitter/android/PostActivity$PhotoAction;->a:Lcom/twitter/android/PostActivity$PhotoAction;

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->a(Lcom/twitter/android/PostActivity$PhotoAction;)V

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/twitter/android/PostActivity$PhotoAction;->d:Lcom/twitter/android/PostActivity$PhotoAction;

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->a(Lcom/twitter/android/PostActivity$PhotoAction;)V

    goto :goto_0

    :sswitch_4
    if-ne p2, v1, :cond_0

    const-string/jumbo v0, "photo_tags"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v1}, Lcom/twitter/android/PostStorage;->f()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/PostStorage$MediaItem;

    iput-object v0, v1, Lcom/twitter/android/PostStorage$MediaItem;->d:Ljava/util/ArrayList;

    goto :goto_1

    :cond_4
    invoke-direct {p0, v0}, Lcom/twitter/android/PostActivity;->b(Ljava/util/List;)V

    goto :goto_0

    :sswitch_5
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    const-string/jumbo v0, "account"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/UserAccount;

    iget-object v0, v0, Lcom/twitter/android/UserAccount;->a:Landroid/accounts/Account;

    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/PostActivity;->s:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->Z:Lcom/twitter/android/e;

    invoke-virtual {v1, v0}, Lcom/twitter/android/e;->a(Landroid/accounts/Account;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x101 -> :sswitch_0
        0x102 -> :sswitch_1
        0x103 -> :sswitch_2
        0x104 -> :sswitch_3
        0x201 -> :sswitch_4
        0x202 -> :sswitch_5
    .end sparse-switch
.end method

.method public onBackPressed()V
    .locals 3

    const/4 v2, 0x3

    iget v0, p0, Lcom/twitter/android/PostActivity;->v:I

    if-eq v0, v2, :cond_0

    iget v0, p0, Lcom/twitter/android/PostActivity;->v:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v2, v0}, Lcom/twitter/android/PostActivity;->a(IZ)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x201

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->showDialog(I)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/PostActivity;->ai()V

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 6

    const/4 v5, 0x0

    const v2, 0x7f0f031b    # com.twitter.android.R.string.post_title_tweet

    const/4 v4, 0x0

    const v3, 0x7f0f0089    # com.twitter.android.R.string.cancel

    packed-switch p1, :pswitch_data_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->y:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0, p1}, Lcom/twitter/android/PhotoSelectHelper;->a(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Lcom/twitter/android/ox;

    invoke-direct {v0, p0}, Lcom/twitter/android/ox;-><init>(Lcom/twitter/android/PostActivity;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f0315    # com.twitter.android.R.string.post_delete_question

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f0571    # com.twitter.android.R.string.yes

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f029d    # com.twitter.android.R.string.no

    invoke-virtual {v0, v1, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/twitter/android/oy;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/oy;-><init>(Lcom/twitter/android/PostActivity;I)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f0318    # com.twitter.android.R.string.post_quit_question

    invoke-virtual {p0, v2}, Lcom/twitter/android/PostActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f0385    # com.twitter.android.R.string.save

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f0111    # com.twitter.android.R.string.discard

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/oz;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/oz;-><init>(Lcom/twitter/android/PostActivity;Lcom/twitter/android/client/c;)V

    invoke-virtual {v0, p0, v1}, Lcom/twitter/android/client/c;->a(Landroid/app/Activity;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    new-instance v0, Lcom/twitter/android/pa;

    invoke-direct {v0, p0}, Lcom/twitter/android/pa;-><init>(Lcom/twitter/android/PostActivity;)V

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Lcom/twitter/android/client/c;->b(Landroid/app/Activity;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    new-instance v0, Lcom/twitter/android/pb;

    invoke-direct {v0, p0}, Lcom/twitter/android/pb;-><init>(Lcom/twitter/android/PostActivity;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f0208    # com.twitter.android.R.string.lifeline_alert_confirm_title

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f0207    # com.twitter.android.R.string.lifeline_alert_confirm_message

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f0206    # com.twitter.android.R.string.lifeline_alert_confirm_continue

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v3, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_5
    new-instance v0, Lcom/twitter/android/pc;

    invoke-direct {v0, p0}, Lcom/twitter/android/pc;-><init>(Lcom/twitter/android/PostActivity;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0f04d2    # com.twitter.android.R.string.tweet_location_title

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f04d1    # com.twitter.android.R.string.tweet_location_message

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f015a    # com.twitter.android.R.string.enable

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v3, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x201
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7

    const/4 v4, 0x0

    packed-switch p1, :pswitch_data_0

    move-object v0, v4

    :goto_0
    return-object v0

    :pswitch_0
    const-string/jumbo v0, "uri"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->s:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget-object v3, Lcom/twitter/library/provider/Tweet;->a:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onDestroy()V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->y:Lcom/twitter/android/PhotoSelectHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->y:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0}, Lcom/twitter/android/PhotoSelectHelper;->a()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage;->h()V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage;->h()V

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Lcom/twitter/android/client/c;->b(ILcom/twitter/library/util/ar;)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/PoiFragment;->a(Lcom/twitter/android/widget/bx;)V

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->n:Lcom/twitter/android/widget/LocationFragment;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->n:Lcom/twitter/android/widget/LocationFragment;

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/LocationFragment;->a(Lcom/twitter/android/widget/ap;)V

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0, v2}, Lcom/twitter/android/TweetBoxFragment;->a(Lcom/twitter/internal/android/widget/af;)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0, v2}, Lcom/twitter/android/TweetBoxFragment;->a(Lcom/twitter/android/wz;)V

    :cond_5
    invoke-static {}, Lcom/twitter/android/provider/SuggestionsProvider;->b()V

    invoke-static {}, Lcom/twitter/android/provider/SuggestionsProvider;->c()V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->i:Lcom/twitter/android/widget/ComposerLayout;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->i:Lcom/twitter/android/widget/ComposerLayout;

    invoke-virtual {v0}, Lcom/twitter/android/widget/ComposerLayout;->e()V

    iput-object v2, p0, Lcom/twitter/android/PostActivity;->i:Lcom/twitter/android/widget/ComposerLayout;

    :cond_6
    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/PostActivity;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0

    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0902db    # com.twitter.android.R.id.text_content

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/PostActivity;->S:Z

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onPause()V

    invoke-direct {p0}, Lcom/twitter/android/PostActivity;->an()V

    const/16 v0, 0x202

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->removeDialog(I)V

    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 1

    const v0, 0x7f0f031b    # com.twitter.android.R.string.post_title_tweet

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p2, v0}, Landroid/app/Dialog;->setTitle(I)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p2, v0}, Landroid/app/Dialog;->setTitle(I)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p2, v0}, Landroid/app/Dialog;->setTitle(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x201
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onResume()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onResume()V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->c()Landroid/text/Editable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/aq;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/aq;->a(Lcom/twitter/android/widget/as;)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->s()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->l:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->N()Lcom/twitter/library/platform/LocationProducer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/platform/LocationProducer;->e()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    invoke-virtual {p0, v1}, Lcom/twitter/android/PostActivity;->c(Z)V

    :cond_2
    :goto_0
    const-string/jumbo v0, "android_at_mention_contact_1081"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    return-void

    :cond_3
    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->u:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->G:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->z:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/PoiFragment;->a(Z)V

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->N()Lcom/twitter/library/platform/LocationProducer;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/library/platform/LocationProducer;->a(Lcom/twitter/library/platform/i;)V

    goto :goto_0

    :cond_4
    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->N:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->n:Lcom/twitter/android/widget/LocationFragment;

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/LocationFragment;->b(Z)V

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->N()Lcom/twitter/library/platform/LocationProducer;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/library/platform/LocationProducer;->a(Lcom/twitter/library/platform/i;)V

    goto :goto_0
.end method

.method protected onResumeFragments()V
    .locals 3

    const/4 v2, -0x1

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onResumeFragments()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/PostActivity;->S:Z

    iget v0, p0, Lcom/twitter/android/PostActivity;->U:I

    if-eq v0, v2, :cond_0

    iget v0, p0, Lcom/twitter/android/PostActivity;->U:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/PostActivity;->a(IZ)V

    :cond_0
    iput v2, p0, Lcom/twitter/android/PostActivity;->U:I

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "data"

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v0, "mode"

    iget v1, p0, Lcom/twitter/android/PostActivity;->J:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "loc"

    iget-boolean v1, p0, Lcom/twitter/android/PostActivity;->u:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "location_hint_count"

    iget-boolean v1, p0, Lcom/twitter/android/PostActivity;->I:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "lifeline_alert"

    iget-boolean v1, p0, Lcom/twitter/android/PostActivity;->t:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "do_post"

    iget-boolean v1, p0, Lcom/twitter/android/PostActivity;->G:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "text"

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v1}, Lcom/twitter/android/TweetBoxFragment;->j()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "selection"

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v1}, Lcom/twitter/android/TweetBoxFragment;->k()[I

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    const-string/jumbo v0, "a"

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->E:Lcom/twitter/android/PostActivity$PhotoAction;

    invoke-virtual {v1}, Lcom/twitter/android/PostActivity$PhotoAction;->ordinal()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "show_link_hint"

    iget-boolean v1, p0, Lcom/twitter/android/PostActivity;->P:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "drawer"

    iget v1, p0, Lcom/twitter/android/PostActivity;->v:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->y:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0, p1}, Lcom/twitter/android/PhotoSelectHelper;->b(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onStart()V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->V:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->y:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0}, Lcom/twitter/android/PhotoSelectHelper;->c()V

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onStart()V

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->N()Lcom/twitter/library/platform/LocationProducer;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/library/platform/LocationProducer;->b(Lcom/twitter/library/platform/i;)V

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onStop()V

    return-void
.end method

.method p()V
    .locals 12

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/PostActivity;->G:Z

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v11

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->s:Lcom/twitter/library/client/Session;

    iget-boolean v2, p0, Lcom/twitter/android/PostActivity;->u:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v2}, Lcom/twitter/android/PostStorage;->a()Landroid/location/Location;

    move-result-object v5

    :goto_0
    iget-object v2, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v2}, Lcom/twitter/android/TweetBoxFragment;->o()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v3}, Lcom/twitter/android/TweetBoxFragment;->p()Ljava/util/ArrayList;

    move-result-object v10

    iget-wide v3, p0, Lcom/twitter/android/PostActivity;->H:J

    const-wide/16 v6, 0x0

    cmp-long v3, v3, v6

    if-eqz v3, :cond_0

    iget-wide v3, p0, Lcom/twitter/android/PostActivity;->H:J

    invoke-virtual {v11, v1, v3, v4}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/Session;J)V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->r()Lcom/twitter/library/api/TweetEntities;

    move-result-object v8

    if-eqz v8, :cond_1

    const/4 v3, 0x1

    invoke-virtual {v8, v3}, Lcom/twitter/library/api/TweetEntities;->a(I)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    invoke-virtual {v8, v3}, Lcom/twitter/library/api/TweetEntities;->b(I)Lcom/twitter/library/api/MediaEntity;

    move-result-object v3

    iget-object v3, v3, Lcom/twitter/library/api/MediaEntity;->url:Ljava/lang/String;

    const-string/jumbo v4, "com.twitter"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {v0, v3}, Lgp;->a(Landroid/content/Context;Ljava/lang/String;)V

    :cond_1
    iget-boolean v3, p0, Lcom/twitter/android/PostActivity;->z:Z

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    invoke-virtual {v3}, Lcom/twitter/android/widget/PoiFragment;->a()Lcom/twitter/library/api/geo/TwitterPlace;

    move-result-object v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    invoke-virtual {v3}, Lcom/twitter/android/widget/PoiFragment;->a()Lcom/twitter/library/api/geo/TwitterPlace;

    move-result-object v3

    iget-object v6, v3, Lcom/twitter/library/api/geo/TwitterPlace;->placeId:Ljava/lang/String;

    :goto_1
    iget-wide v3, p0, Lcom/twitter/android/PostActivity;->C:J

    iget-object v7, p0, Lcom/twitter/android/PostActivity;->M:Lcom/twitter/library/api/PromotedContent;

    invoke-direct {p0}, Lcom/twitter/android/PostActivity;->ao()Z

    move-result v9

    invoke-static/range {v0 .. v10}, Lcom/twitter/android/client/cd;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;JLandroid/location/Location;Ljava/lang/String;Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/api/TweetEntities;ZLjava/util/ArrayList;)Ljava/lang/String;

    iget v0, p0, Lcom/twitter/android/PostActivity;->J:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_7

    iget-wide v2, p0, Lcom/twitter/android/PostActivity;->H:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_6

    const-string/jumbo v0, ":drafts:composition::send_reply"

    :goto_2
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v3, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v11, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    invoke-direct {p0, v8}, Lcom/twitter/android/PostActivity;->a(Lcom/twitter/library/api/TweetEntities;)V

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-wide v3, p0, Lcom/twitter/android/PostActivity;->H:J

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-eqz v0, :cond_9

    const-string/jumbo v0, ":drafts:composition::geotag"

    :goto_3
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v11, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_2
    invoke-direct {p0}, Lcom/twitter/android/PostActivity;->ao()Z

    move-result v0

    if-eqz v0, :cond_a

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->d(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->finish()V

    :goto_4
    return-void

    :cond_3
    const/4 v5, 0x0

    goto/16 :goto_0

    :cond_4
    iget-boolean v3, p0, Lcom/twitter/android/PostActivity;->u:Z

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v3}, Lcom/twitter/android/PostStorage;->b()Ljava/lang/String;

    move-result-object v3

    :goto_5
    move-object v6, v3

    goto :goto_1

    :cond_5
    const/4 v3, 0x0

    goto :goto_5

    :cond_6
    const-string/jumbo v0, ":composition:::send_reply"

    goto :goto_2

    :cond_7
    iget-wide v2, p0, Lcom/twitter/android/PostActivity;->H:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_8

    const-string/jumbo v0, ":drafts:composition::send_tweet"

    goto :goto_2

    :cond_8
    const-string/jumbo v0, ":composition:::send_tweet"

    goto :goto_2

    :cond_9
    const-string/jumbo v0, ":composition:::geotag"

    goto :goto_3

    :cond_a
    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "android.intent.extra.RETURN_RESULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v3, "android.intent.extra.RETURN_RESULT"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {p0, v2, v1}, Lcom/twitter/android/PostActivity;->setResult(ILandroid/content/Intent;)V

    const-string/jumbo v1, "android.intent.extra.INTENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    if-eqz v0, :cond_b

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->d(Landroid/content/Intent;)V

    :cond_b
    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->finish()V

    goto :goto_4
.end method

.method public p_()V
    .locals 0

    return-void
.end method

.method protected q()Z
    .locals 4

    iget-wide v0, p0, Lcom/twitter/android/PostActivity;->H:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method r()Lcom/twitter/library/api/TweetEntities;
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-direct {p0, v0}, Lcom/twitter/android/PostActivity;->a(Lcom/twitter/android/PostStorage;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/PostStorage$MediaItem;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage$MediaItem;->b()Lcom/twitter/library/api/MediaEntity;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->s()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :cond_1
    :goto_1
    return-object v0

    :cond_2
    new-instance v0, Lcom/twitter/library/api/TweetEntities;

    invoke-direct {v0}, Lcom/twitter/library/api/TweetEntities;-><init>()V

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    iput-object v1, v0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    :cond_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iput-object v2, v0, Lcom/twitter/library/api/TweetEntities;->contacts:Ljava/util/ArrayList;

    goto :goto_1
.end method

.method protected s()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->a:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v0}, Landroid/support/v4/util/SimpleArrayMap;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/PostActivity;->a(IZ)V

    :cond_0
    return-void
.end method

.method public t()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/android/PostActivity;->k(Z)V

    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->z:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    invoke-virtual {v0}, Lcom/twitter/android/widget/PoiFragment;->t()V

    :cond_0
    return-void
.end method

.method public u()V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/PostActivity;->k(Z)V

    iget-boolean v0, p0, Lcom/twitter/android/PostActivity;->z:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/PostActivity;->v:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    invoke-virtual {v0}, Lcom/twitter/android/widget/PoiFragment;->u()V

    :cond_0
    return-void
.end method

.method protected v()V
    .locals 3

    invoke-static {p0}, Lcom/twitter/library/util/a;->b(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/AccountsDialogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "account_name"

    iget-object v2, p0, Lcom/twitter/android/PostActivity;->s:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x202

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/PostActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    return-void
.end method

.method protected w()V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v1}, Lcom/twitter/android/PostStorage;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->ai()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/twitter/android/pl;

    invoke-direct {v0, p0}, Lcom/twitter/android/pl;-><init>(Lcom/twitter/android/PostActivity;)V

    invoke-static {p0, v0}, Lcom/twitter/android/client/be;->a(Landroid/content/Context;Lcom/twitter/android/client/ak;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/PostActivity;->d(Z)V

    invoke-direct {p0}, Lcom/twitter/android/PostActivity;->al()V

    goto :goto_0
.end method

.method protected x()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->n()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->y:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0}, Lcom/twitter/android/PhotoSelectHelper;->b()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->y:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0}, Lcom/twitter/android/PhotoSelectHelper;->c()V

    return-void
.end method

.method protected y()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->n()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->y:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0}, Lcom/twitter/android/PhotoSelectHelper;->b()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->y:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0}, Lcom/twitter/android/PhotoSelectHelper;->d()V

    return-void
.end method

.method protected z()V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->n()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostActivity;->y:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0}, Lcom/twitter/android/PhotoSelectHelper;->b()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage;->d()Lcom/twitter/android/PostStorage$MediaItem;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/android/spen/d;->a(Landroid/content/Context;Lcom/twitter/android/PostStorage$MediaItem;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/PostActivity;->y:Lcom/twitter/android/PhotoSelectHelper;

    const/16 v1, 0x104

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/PostActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method
