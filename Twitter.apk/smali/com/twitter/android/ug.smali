.class public Lcom/twitter/android/ug;
.super Lcom/twitter/android/client/z;
.source "Twttr"


# instance fields
.field public final a:Z

.field public final b:Z

.field public final c:I


# direct methods
.method public constructor <init>(Lcom/twitter/android/client/BaseFragmentActivity;)V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    invoke-virtual {p1}, Lcom/twitter/android/client/BaseFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    if-eqz v4, :cond_0

    const-string/jumbo v0, "twitter"

    invoke-virtual {v4}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/ug;->b:Z

    iget-boolean v0, p0, Lcom/twitter/android/ug;->b:Z

    if-eqz v0, :cond_3

    invoke-virtual {v4}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v4, "user_timeline"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iput v1, p0, Lcom/twitter/android/ug;->c:I

    :goto_1
    iget v0, p0, Lcom/twitter/android/ug;->c:I

    sparse-switch v0, :sswitch_data_0

    iput-boolean v1, p0, Lcom/twitter/android/ug;->a:Z

    :goto_2
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    const-string/jumbo v4, "favorites"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/android/ug;->c:I

    goto :goto_1

    :cond_2
    const-string/jumbo v0, "type"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ug;->c:I

    goto :goto_1

    :cond_3
    const-string/jumbo v0, "type"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ug;->c:I

    goto :goto_1

    :sswitch_0
    invoke-virtual {p0, v2}, Lcom/twitter/android/ug;->c(Z)V

    iput-boolean v1, p0, Lcom/twitter/android/ug;->a:Z

    goto :goto_2

    :sswitch_1
    iput-boolean v2, p0, Lcom/twitter/android/ug;->a:Z

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x9 -> :sswitch_0
        0xc -> :sswitch_1
        0xe -> :sswitch_1
        0x16 -> :sswitch_0
        0x1a -> :sswitch_0
    .end sparse-switch
.end method
