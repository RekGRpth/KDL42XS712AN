.class Lcom/twitter/android/fk;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/EditProfileOnboardingActivity;


# direct methods
.method private constructor <init>(Lcom/twitter/android/EditProfileOnboardingActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/fk;->a:Lcom/twitter/android/EditProfileOnboardingActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/EditProfileOnboardingActivity;Lcom/twitter/android/fi;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/fk;-><init>(Lcom/twitter/android/EditProfileOnboardingActivity;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 2

    const/4 v0, 0x0

    aget-object v0, p1, v0

    iget-object v1, p0, Lcom/twitter/android/fk;->a:Lcom/twitter/android/EditProfileOnboardingActivity;

    invoke-static {v1, v0}, Lkw;->a(Landroid/content/Context;Landroid/net/Uri;)Lkw;

    move-result-object v0

    const/16 v1, 0x640

    invoke-virtual {v0, v1}, Lkw;->a(I)Lkw;

    move-result-object v0

    invoke-virtual {v0}, Lkw;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/graphics/Bitmap;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/fk;->a:Lcom/twitter/android/EditProfileOnboardingActivity;

    invoke-virtual {v0, p1}, Lcom/twitter/android/EditProfileOnboardingActivity;->a(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lcom/twitter/android/fk;->a([Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/twitter/android/fk;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method
