.class public final Lcom/twitter/android/AmplifyPlayerFragment;
.super Landroid/support/v4/app/Fragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/amplify/i;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/view/View$OnClickListener;

.field private c:Lcom/twitter/library/card/element/Player;

.field private d:Lcom/twitter/library/amplify/AmplifyPlayer;

.field private e:Lcom/twitter/library/amplify/n;

.field private f:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

.field private g:Lcom/twitter/android/AmplifyPlayerFragment$VideoLayout;

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/twitter/android/AmplifyPlayerFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/AmplifyPlayerFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    new-instance v0, Lcom/twitter/android/aj;

    invoke-direct {v0, p0}, Lcom/twitter/android/aj;-><init>(Lcom/twitter/android/AmplifyPlayerFragment;)V

    iput-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->b:Landroid/view/View$OnClickListener;

    iput-boolean v1, p0, Lcom/twitter/android/AmplifyPlayerFragment;->i:Z

    invoke-virtual {p0, v1}, Lcom/twitter/android/AmplifyPlayerFragment;->setRetainInstance(Z)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/AmplifyPlayerFragment;)Lcom/twitter/library/amplify/control/AmplifyVideoControlView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->f:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    new-instance v0, Lcom/twitter/library/amplify/n;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/AmplifyPlayerFragment;->d:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/amplify/n;-><init>(Landroid/content/Context;Lcom/twitter/library/amplify/AmplifyPlayer;)V

    iput-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->e:Lcom/twitter/library/amplify/n;

    new-instance v0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {p0}, Lcom/twitter/android/AmplifyPlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/AmplifyPlayerFragment;->d:Lcom/twitter/library/amplify/AmplifyPlayer;

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;-><init>(Landroid/content/Context;Lcom/twitter/library/amplify/AmplifyPlayer;Z)V

    iput-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->f:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/twitter/android/AmplifyPlayerFragment$VideoLayout;

    invoke-virtual {p0}, Lcom/twitter/android/AmplifyPlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/AmplifyPlayerFragment;->e:Lcom/twitter/library/amplify/n;

    iget-object v3, p0, Lcom/twitter/android/AmplifyPlayerFragment;->f:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/android/AmplifyPlayerFragment$VideoLayout;-><init>(Landroid/content/Context;Lcom/twitter/library/amplify/n;Lcom/twitter/library/amplify/control/AmplifyVideoControlView;)V

    iput-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->g:Lcom/twitter/android/AmplifyPlayerFragment$VideoLayout;

    check-cast p1, Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->g:Lcom/twitter/android/AmplifyPlayerFragment$VideoLayout;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->d:Lcom/twitter/library/amplify/AmplifyPlayer;

    const-string/jumbo v1, "fullscreen"

    invoke-virtual {v0, p0, v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Lcom/twitter/library/amplify/i;Ljava/lang/String;)V

    return-void
.end method

.method private i()Z
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->d:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->s()Lcom/twitter/library/amplify/r;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/AmplifyPlayerFragment;->d:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->u()Lcom/twitter/library/amplify/model/AmplifyVideo;

    move-result-object v1

    iget v0, v0, Lcom/twitter/library/amplify/r;->a:I

    int-to-double v2, v0

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/amplify/model/AmplifyVideo;->e(D)Z

    move-result v0

    return v0
.end method

.method private j()V
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/AmplifyPlayerFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/AmplifyPlayerFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/AmplifyPlayerFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    :goto_0
    return-void

    :cond_0
    iput-boolean v2, p0, Lcom/twitter/android/AmplifyPlayerFragment;->h:Z

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->e:Lcom/twitter/library/amplify/n;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/library/amplify/n;->a(Lcom/twitter/library/amplify/n;Z)V

    return-void
.end method

.method public a(II)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->e:Lcom/twitter/library/amplify/n;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/amplify/n;->a(II)V

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->f:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->a(II)V

    return-void
.end method

.method public a(IIZZ)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->f:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->b()V

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->e:Lcom/twitter/library/amplify/n;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/amplify/n;->a(II)V

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->f:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-direct {p0}, Lcom/twitter/android/AmplifyPlayerFragment;->i()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->b(Z)V

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->f:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->a(II)V

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->d:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->d:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0, v2}, Lcom/twitter/library/amplify/AmplifyPlayer;->g(Z)V

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->d:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0, v2}, Lcom/twitter/library/amplify/AmplifyPlayer;->e(Z)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->f:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->i()V

    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->f:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {p0}, Lcom/twitter/android/AmplifyPlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->a(Landroid/content/Context;I)V

    return-void
.end method

.method public a(Lcom/twitter/library/amplify/AmplifyPlayer$PlayerStartType;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->f:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->e()V

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->e:Lcom/twitter/library/amplify/n;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/library/amplify/n;->a(Lcom/twitter/library/amplify/n;Z)V

    return-void
.end method

.method public a(Lcom/twitter/library/card/element/Player;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/AmplifyPlayerFragment;->c:Lcom/twitter/library/card/element/Player;

    invoke-virtual {p1}, Lcom/twitter/library/card/element/Player;->b()Lcom/twitter/library/card/element/h;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/amplify/o;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/o;->i()Lcom/twitter/library/amplify/AmplifyPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->d:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {p0}, Lcom/twitter/android/AmplifyPlayerFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/AmplifyPlayerFragment;->a(Landroid/view/View;)V

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/AmplifyPlayerFragment;->i:Z

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->f:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->i()V

    return-void
.end method

.method public b(II)V
    .locals 1

    const/16 v0, 0x2bd

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->f:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->a()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x2be

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->f:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->b()V

    goto :goto_0
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->f:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->a()V

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->f:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->d()V

    return-void
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->f:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-direct {p0}, Lcom/twitter/android/AmplifyPlayerFragment;->i()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->a(Z)V

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->e:Lcom/twitter/library/amplify/n;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/library/amplify/n;->a(Lcom/twitter/library/amplify/n;Z)V

    return-void
.end method

.method public e()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->e:Lcom/twitter/library/amplify/n;

    iget-object v1, p0, Lcom/twitter/android/AmplifyPlayerFragment;->d:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->r()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/twitter/library/amplify/n;->a(Lcom/twitter/library/amplify/n;Z)V

    return-void
.end method

.method public f()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->e:Lcom/twitter/library/amplify/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->e:Lcom/twitter/library/amplify/n;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/n;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->f:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->c()V

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->g:Lcom/twitter/android/AmplifyPlayerFragment$VideoLayout;

    iget-object v1, p0, Lcom/twitter/android/AmplifyPlayerFragment;->e:Lcom/twitter/library/amplify/n;

    invoke-virtual {v1}, Lcom/twitter/library/amplify/n;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/AmplifyPlayerFragment$VideoLayout;->removeView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->e:Lcom/twitter/library/amplify/n;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/library/amplify/n;->a(Lcom/twitter/library/amplify/n;Z)V

    return-void
.end method

.method public g()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->f:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->n()V

    return-void
.end method

.method public h()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/AmplifyPlayerFragment;->j()V

    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    invoke-virtual {p0}, Lcom/twitter/android/AmplifyPlayerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iget-boolean v1, p0, Lcom/twitter/android/AmplifyPlayerFragment;->i:Z

    if-nez v1, :cond_0

    if-ne v0, v2, :cond_0

    iput-boolean v2, p0, Lcom/twitter/android/AmplifyPlayerFragment;->j:Z

    invoke-direct {p0}, Lcom/twitter/android/AmplifyPlayerFragment;->j()V

    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f030014    # com.twitter.android.R.layout.amplify_player_fragment

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 3

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    invoke-virtual {p0}, Lcom/twitter/android/AmplifyPlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->c:Lcom/twitter/library/card/element/Player;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->d:Lcom/twitter/library/amplify/AmplifyPlayer;

    iget-object v1, p0, Lcom/twitter/android/AmplifyPlayerFragment;->d:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->r()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->g(Z)V

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->d:Lcom/twitter/library/amplify/AmplifyPlayer;

    const/4 v1, 0x0

    const-string/jumbo v2, "fullscreen"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Lcom/twitter/library/amplify/i;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->d:Lcom/twitter/library/amplify/AmplifyPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->d(Z)V

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->d:Lcom/twitter/library/amplify/AmplifyPlayer;

    iget-boolean v1, p0, Lcom/twitter/android/AmplifyPlayerFragment;->j:Z

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->b(Z)V

    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->e:Lcom/twitter/library/amplify/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->e:Lcom/twitter/library/amplify/n;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/n;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->g:Lcom/twitter/android/AmplifyPlayerFragment$VideoLayout;

    iget-object v1, p0, Lcom/twitter/android/AmplifyPlayerFragment;->e:Lcom/twitter/library/amplify/n;

    invoke-virtual {v1}, Lcom/twitter/library/amplify/n;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/AmplifyPlayerFragment$VideoLayout;->removeView(Landroid/view/View;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->k:Z

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    iget-boolean v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->h:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/AmplifyPlayerFragment;->j()V

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->k:Z

    if-eqz v0, :cond_3

    iput-boolean v1, p0, Lcom/twitter/android/AmplifyPlayerFragment;->k:Z

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->d:Lcom/twitter/library/amplify/AmplifyPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->d:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->g(Z)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->g:Lcom/twitter/android/AmplifyPlayerFragment$VideoLayout;

    invoke-virtual {v0}, Lcom/twitter/android/AmplifyPlayerFragment$VideoLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/AmplifyPlayerFragment;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/AmplifyPlayerFragment;->g:Lcom/twitter/android/AmplifyPlayerFragment$VideoLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/AmplifyPlayerFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/AmplifyPlayerFragment;->a(Landroid/view/View;)V

    :cond_3
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    iget-boolean v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->j:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/AmplifyPlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment;->c:Lcom/twitter/library/card/element/Player;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/twitter/android/AmplifyPlayerFragment;->a(Landroid/view/View;)V

    :cond_0
    return-void
.end method
