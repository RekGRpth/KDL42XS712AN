.class final Lcom/twitter/android/rt;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/twitter/android/client/c;

.field final synthetic c:Lcom/twitter/library/client/Session;

.field final synthetic d:Lcom/twitter/library/provider/Tweet;

.field final synthetic e:Lcom/twitter/android/ru;

.field final synthetic f:I

.field final synthetic g:J


# direct methods
.method constructor <init>(ZLcom/twitter/android/client/c;Lcom/twitter/library/client/Session;Lcom/twitter/library/provider/Tweet;Lcom/twitter/android/ru;IJ)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/rt;->a:Z

    iput-object p2, p0, Lcom/twitter/android/rt;->b:Lcom/twitter/android/client/c;

    iput-object p3, p0, Lcom/twitter/android/rt;->c:Lcom/twitter/library/client/Session;

    iput-object p4, p0, Lcom/twitter/android/rt;->d:Lcom/twitter/library/provider/Tweet;

    iput-object p5, p0, Lcom/twitter/android/rt;->e:Lcom/twitter/android/ru;

    iput p6, p0, Lcom/twitter/android/rt;->f:I

    iput-wide p7, p0, Lcom/twitter/android/rt;->g:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/rt;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/rt;->b:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/rt;->c:Lcom/twitter/library/client/Session;

    iget-object v2, p0, Lcom/twitter/android/rt;->d:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v2}, Lcom/twitter/library/provider/Tweet;->b()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;J)Ljava/lang/String;

    move-result-object v6

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/rt;->e:Lcom/twitter/android/ru;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/rt;->e:Lcom/twitter/android/ru;

    iget v1, p0, Lcom/twitter/android/rt;->f:I

    iget-wide v2, p0, Lcom/twitter/android/rt;->g:J

    iget-object v4, p0, Lcom/twitter/android/rt;->d:Lcom/twitter/library/provider/Tweet;

    iget-boolean v5, p0, Lcom/twitter/android/rt;->a:Z

    invoke-interface/range {v0 .. v6}, Lcom/twitter/android/ru;->a(IJLcom/twitter/library/provider/ParcelableTweet;ZLjava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/rt;->b:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/rt;->c:Lcom/twitter/library/client/Session;

    iget-object v2, p0, Lcom/twitter/android/rt;->d:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v2}, Lcom/twitter/library/provider/Tweet;->b()J

    move-result-wide v2

    iget-object v4, p0, Lcom/twitter/android/rt;->d:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v4}, Lcom/twitter/library/provider/Tweet;->m()Lcom/twitter/library/api/PromotedContent;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    move-result-object v6

    goto :goto_0
.end method
