.class Lcom/twitter/android/ql;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/TextView;

.field final synthetic b:Lcom/twitter/android/ProfileFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/ProfileFragment;Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/ql;->b:Lcom/twitter/android/ProfileFragment;

    iput-object p2, p0, Lcom/twitter/android/ql;->a:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/ql;->b:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->A:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v2, Lcom/twitter/library/api/UrlEntity;

    invoke-direct {v2}, Lcom/twitter/library/api/UrlEntity;-><init>()V

    iput-object v0, v2, Lcom/twitter/library/api/UrlEntity;->url:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/ql;->b:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/ql;->b:Lcom/twitter/android/ProfileFragment;

    invoke-static {v3}, Lcom/twitter/android/ProfileFragment;->w(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string/jumbo v7, "profile::header:bio_link:click"

    aput-object v7, v5, v6

    invoke-virtual {v0, v3, v4, v5}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/ql;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-wide/16 v3, 0x0

    iget-object v5, p0, Lcom/twitter/android/ql;->b:Lcom/twitter/android/ProfileFragment;

    invoke-static {v5}, Lcom/twitter/android/ProfileFragment;->x(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v7

    move-object v5, v1

    move-object v6, v1

    move-object v8, v1

    invoke-static/range {v0 .. v8}, Lcom/twitter/android/client/be;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/UrlEntity;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
