.class Lcom/twitter/android/hg;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/GalleryActivity;

.field final synthetic b:Lcom/twitter/android/hd;


# direct methods
.method constructor <init>(Lcom/twitter/android/hd;Lcom/twitter/android/GalleryActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/hg;->b:Lcom/twitter/android/hd;

    iput-object p2, p0, Lcom/twitter/android/hg;->a:Lcom/twitter/android/GalleryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    instance-of v0, p2, Lcom/twitter/library/widget/UserView;

    if-eqz v0, :cond_1

    check-cast p2, Lcom/twitter/library/widget/UserView;

    iget-object v0, p0, Lcom/twitter/android/hg;->b:Lcom/twitter/android/hd;

    iget-object v0, v0, Lcom/twitter/android/hd;->b:Lcom/twitter/android/GalleryActivity;

    iget-object v0, v0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/hg;->b:Lcom/twitter/android/hd;

    iget-object v0, v0, Lcom/twitter/android/hd;->b:Lcom/twitter/android/GalleryActivity;

    iget-object v0, v0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    iget-object v0, v0, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/hg;->b:Lcom/twitter/android/hd;

    iget-object v0, v0, Lcom/twitter/android/hd;->b:Lcom/twitter/android/GalleryActivity;

    invoke-static {v0}, Lcom/twitter/android/GalleryActivity;->k(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->f:Lcom/twitter/library/api/PromotedEvent;

    iget-object v2, p0, Lcom/twitter/android/hg;->b:Lcom/twitter/android/hd;

    iget-object v2, v2, Lcom/twitter/android/hd;->b:Lcom/twitter/android/GalleryActivity;

    iget-object v2, v2, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    iget-object v2, v2, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/hg;->b:Lcom/twitter/android/hd;

    invoke-virtual {v0, p2}, Lcom/twitter/android/hd;->a(Lcom/twitter/library/widget/UserView;)V

    :cond_1
    return-void
.end method
