.class Lcom/twitter/android/jq;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/library/api/account/LoginVerificationRequest;

.field final synthetic b:Lcom/twitter/android/jp;


# direct methods
.method constructor <init>(Lcom/twitter/android/jp;Lcom/twitter/library/api/account/LoginVerificationRequest;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/jq;->b:Lcom/twitter/android/jp;

    iput-object p2, p0, Lcom/twitter/android/jq;->a:Lcom/twitter/library/api/account/LoginVerificationRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    const/4 v6, 0x0

    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v1, p0, Lcom/twitter/android/jq;->b:Lcom/twitter/android/jp;

    iget-object v1, v1, Lcom/twitter/android/jp;->a:Lcom/twitter/android/LoginVerificationFragment;

    invoke-static {v1}, Lcom/twitter/android/LoginVerificationFragment;->b(Lcom/twitter/android/LoginVerificationFragment;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "login_verification::request:accept:click"

    aput-object v2, v1, v6

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/jq;->a:Lcom/twitter/library/api/account/LoginVerificationRequest;

    iget-object v1, v1, Lcom/twitter/library/api/account/LoginVerificationRequest;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/library/scribe/ScribeItem;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    iget-object v1, p0, Lcom/twitter/android/jq;->b:Lcom/twitter/android/jp;

    iget-object v1, v1, Lcom/twitter/android/jp;->a:Lcom/twitter/android/LoginVerificationFragment;

    invoke-static {v1}, Lcom/twitter/android/LoginVerificationFragment;->f(Lcom/twitter/android/LoginVerificationFragment;)Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    new-instance v0, Lcom/twitter/android/jl;

    iget-object v1, p0, Lcom/twitter/android/jq;->b:Lcom/twitter/android/jp;

    iget-object v1, v1, Lcom/twitter/android/jp;->a:Lcom/twitter/android/LoginVerificationFragment;

    iget-object v2, p0, Lcom/twitter/android/jq;->b:Lcom/twitter/android/jp;

    iget-object v2, v2, Lcom/twitter/android/jp;->a:Lcom/twitter/android/LoginVerificationFragment;

    invoke-virtual {v2}, Lcom/twitter/android/LoginVerificationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/jq;->a:Lcom/twitter/library/api/account/LoginVerificationRequest;

    iget-object v3, v3, Lcom/twitter/library/api/account/LoginVerificationRequest;->g:Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/jq;->a:Lcom/twitter/library/api/account/LoginVerificationRequest;

    iget-object v4, v4, Lcom/twitter/library/api/account/LoginVerificationRequest;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/android/jq;->a:Lcom/twitter/library/api/account/LoginVerificationRequest;

    iget-object v5, v5, Lcom/twitter/library/api/account/LoginVerificationRequest;->c:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/jl;-><init>(Lcom/twitter/android/LoginVerificationFragment;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-array v1, v6, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/jl;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
