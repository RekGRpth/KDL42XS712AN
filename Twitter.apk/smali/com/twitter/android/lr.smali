.class public Lcom/twitter/android/lr;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/android/MediaTweetActivity;


# direct methods
.method public constructor <init>(Lcom/twitter/android/MediaTweetActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/lr;->a:Lcom/twitter/android/MediaTweetActivity;

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/lr;->a:Lcom/twitter/android/MediaTweetActivity;

    invoke-static {v0, p2}, Lcom/twitter/android/MediaTweetActivity;->c(Lcom/twitter/android/MediaTweetActivity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/lr;->a:Lcom/twitter/android/MediaTweetActivity;

    const v1, 0x7f0f04ee    # com.twitter.android.R.string.tweets_retweet_error

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JI)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/lr;->a:Lcom/twitter/android/MediaTweetActivity;

    invoke-static {v0, p2}, Lcom/twitter/android/MediaTweetActivity;->b(Lcom/twitter/android/MediaTweetActivity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_0

    const/16 v0, 0x194

    if-eq p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/lr;->a:Lcom/twitter/android/MediaTweetActivity;

    const v1, 0x7f0f04ec    # com.twitter.android.R.string.tweets_remove_favorite_error

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;I[ILjava/lang/String;JI)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/lr;->a:Lcom/twitter/android/MediaTweetActivity;

    invoke-static {v0, p2}, Lcom/twitter/android/MediaTweetActivity;->a(Lcom/twitter/android/MediaTweetActivity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_0

    const/16 v0, 0x8b

    invoke-static {p4, v0}, Lcom/twitter/library/util/Util;->a([II)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/lr;->a:Lcom/twitter/android/MediaTweetActivity;

    const v1, 0x7f0f04dd    # com.twitter.android.R.string.tweets_add_favorite_error

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/lr;->a:Lcom/twitter/android/MediaTweetActivity;

    invoke-static {v0, p2}, Lcom/twitter/android/MediaTweetActivity;->d(Lcom/twitter/android/MediaTweetActivity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/lr;->a:Lcom/twitter/android/MediaTweetActivity;

    const v1, 0x7f0f04e0    # com.twitter.android.R.string.tweets_delete_status_error

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method
