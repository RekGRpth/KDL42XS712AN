.class Lcom/twitter/android/hq;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/google/android/gms/maps/j;


# instance fields
.field final synthetic a:Lcom/twitter/android/GeoDebugActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/GeoDebugActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/hq;->a:Lcom/twitter/android/GeoDebugActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/maps/model/k;)V
    .locals 3

    new-instance v1, Lcom/twitter/android/hu;

    iget-object v2, p0, Lcom/twitter/android/hq;->a:Lcom/twitter/android/GeoDebugActivity;

    iget-object v0, p0, Lcom/twitter/android/hq;->a:Lcom/twitter/android/GeoDebugActivity;

    iget-object v0, v0, Lcom/twitter/android/GeoDebugActivity;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ib;

    iget-object v0, v0, Lcom/twitter/android/ib;->a:Landroid/location/Location;

    invoke-direct {v1, v2, p1, v0}, Lcom/twitter/android/hu;-><init>(Lcom/twitter/android/GeoDebugActivity;Lcom/google/android/gms/maps/model/k;Landroid/location/Location;)V

    new-instance v0, Lcom/twitter/android/hr;

    invoke-direct {v0, p0}, Lcom/twitter/android/hr;-><init>(Lcom/twitter/android/hq;)V

    invoke-virtual {v1, v0}, Lcom/twitter/android/hu;->a(Lcom/twitter/android/ht;)V

    iget-object v0, p0, Lcom/twitter/android/hq;->a:Lcom/twitter/android/GeoDebugActivity;

    invoke-virtual {v0}, Lcom/twitter/android/GeoDebugActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v2, "EditLocation"

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/hu;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method
