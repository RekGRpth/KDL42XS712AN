.class public Lcom/twitter/android/AuthorizeAppFragment;
.super Landroid/support/v4/app/Fragment;
.source "Twttr"

# interfaces
.implements Landroid/accounts/OnAccountsUpdateListener;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/library/util/ar;


# instance fields
.field private a:Lcom/twitter/android/client/c;

.field private b:Lcom/twitter/android/an;

.field private c:Lcom/twitter/android/UserAccount;

.field private d:Lcom/twitter/library/widget/UserView;

.field private e:Ljava/lang/CharSequence;

.field private f:Ljava/lang/CharSequence;

.field private g:[Landroid/accounts/Account;

.field private h:Lcom/twitter/android/util/a;

.field private i:Lcom/twitter/library/client/aa;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->a:Lcom/twitter/android/client/c;

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v2, p0, Lcom/twitter/android/AuthorizeAppFragment;->i:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "sso_sdk:::"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/AuthorizeAppFragment;->f:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/UserAccount;)V
    .locals 4

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->d:Lcom/twitter/library/widget/UserView;

    iget-object v1, p1, Lcom/twitter/android/UserAccount;->b:Lcom/twitter/library/api/TwitterUser;

    iget-object v2, v1, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v3, p0, Lcom/twitter/android/AuthorizeAppFragment;->a:Lcom/twitter/android/client/c;

    invoke-virtual {v3, v2}, Lcom/twitter/android/client/c;->g(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/UserView;->setUserImage(Landroid/graphics/Bitmap;)V

    :goto_0
    iget-object v2, v1, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    iget-object v3, v1, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/widget/UserView;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v2, v1, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/UserView;->setProtected(Z)V

    iget-boolean v1, v1, Lcom/twitter/library/api/TwitterUser;->verified:Z

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/UserView;->setVerified(Z)V

    :cond_0
    iput-object p1, p0, Lcom/twitter/android/AuthorizeAppFragment;->c:Lcom/twitter/android/UserAccount;

    return-void

    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/UserView;->setUserImage(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/an;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/AuthorizeAppFragment;->b:Lcom/twitter/android/an;

    return-void
.end method

.method public a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->d:Lcom/twitter/library/widget/UserView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->c:Lcom/twitter/android/UserAccount;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->c:Lcom/twitter/android/UserAccount;

    iget-object v0, v0, Lcom/twitter/android/UserAccount;->b:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v0}, Lcom/twitter/library/api/TwitterUser;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ae;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/AuthorizeAppFragment;->d:Lcom/twitter/library/widget/UserView;

    iget-object v0, v0, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0}, Lcom/twitter/library/widget/UserView;->setUserImage(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public onAccountsUpdated([Landroid/accounts/Account;)V
    .locals 8

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/AuthorizeAppFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/library/util/a;->a(Landroid/accounts/AccountManager;)[Landroid/accounts/Account;

    move-result-object v4

    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->h:Lcom/twitter/android/util/a;

    invoke-static {v4, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->g:[Landroid/accounts/Account;

    invoke-static {v4, v0}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    array-length v0, v4

    if-nez v0, :cond_1

    invoke-virtual {p0, v1}, Lcom/twitter/android/AuthorizeAppFragment;->a(Lcom/twitter/android/UserAccount;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v5, p0, Lcom/twitter/android/AuthorizeAppFragment;->c:Lcom/twitter/android/UserAccount;

    array-length v6, v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v6, :cond_5

    aget-object v0, v4, v2

    if-eqz v5, :cond_2

    iget-object v7, v5, Lcom/twitter/android/UserAccount;->a:Landroid/accounts/Account;

    invoke-virtual {v0, v7}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    :cond_2
    :goto_2
    if-eqz v0, :cond_3

    new-instance v1, Lcom/twitter/android/UserAccount;

    invoke-static {v3, v0}, Lcom/twitter/library/util/a;->a(Landroid/accounts/AccountManager;Landroid/accounts/Account;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/twitter/android/UserAccount;-><init>(Landroid/accounts/Account;Lcom/twitter/library/api/TwitterUser;)V

    invoke-virtual {p0, v1}, Lcom/twitter/android/AuthorizeAppFragment;->a(Lcom/twitter/android/UserAccount;)V

    :cond_3
    iput-object v4, p0, Lcom/twitter/android/AuthorizeAppFragment;->g:[Landroid/accounts/Account;

    goto :goto_0

    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_2
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->c:Lcom/twitter/android/UserAccount;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->c:Lcom/twitter/android/UserAccount;

    invoke-virtual {p0, v0}, Lcom/twitter/android/AuthorizeAppFragment;->a(Lcom/twitter/android/UserAccount;)V

    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    const-string/jumbo v0, "account"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/UserAccount;

    invoke-virtual {p0, v0}, Lcom/twitter/android/AuthorizeAppFragment;->a(Lcom/twitter/android/UserAccount;)V

    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f09008e    # com.twitter.android.R.id.account

    if-ne v0, v1, :cond_1

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/AuthorizeAppFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/AccountsDialogActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/AuthorizeAppFragment;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v1, 0x7f090092    # com.twitter.android.R.id.ok_button

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->b:Lcom/twitter/android/an;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->c:Lcom/twitter/android/UserAccount;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->b:Lcom/twitter/android/an;

    iget-object v1, p0, Lcom/twitter/android/AuthorizeAppFragment;->c:Lcom/twitter/android/UserAccount;

    iget-object v1, v1, Lcom/twitter/android/UserAccount;->a:Landroid/accounts/Account;

    invoke-interface {v0, v1}, Lcom/twitter/android/an;->a(Landroid/accounts/Account;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    check-cast p1, Landroid/widget/Button;

    const v0, 0x7f0f003b    # com.twitter.android.R.string.authenticator_activity_authenticating

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(I)V

    const-string/jumbo v0, "success"

    invoke-direct {p0, v0}, Lcom/twitter/android/AuthorizeAppFragment;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const v1, 0x7f090093    # com.twitter.android.R.id.cancel_button

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->b:Lcom/twitter/android/an;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->b:Lcom/twitter/android/an;

    invoke-interface {v0}, Lcom/twitter/android/an;->a()V

    const-string/jumbo v0, "cancel"

    invoke-direct {p0, v0}, Lcom/twitter/android/AuthorizeAppFragment;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/AuthorizeAppFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->d()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v4

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/twitter/library/util/a;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v3

    new-instance v5, Lcom/twitter/android/UserAccount;

    invoke-direct {v5, v3, v4}, Lcom/twitter/android/UserAccount;-><init>(Landroid/accounts/Account;Lcom/twitter/library/api/TwitterUser;)V

    iput-object v5, p0, Lcom/twitter/android/AuthorizeAppFragment;->c:Lcom/twitter/android/UserAccount;

    :cond_0
    iput-object v1, p0, Lcom/twitter/android/AuthorizeAppFragment;->a:Lcom/twitter/android/client/c;

    iput-object v2, p0, Lcom/twitter/android/AuthorizeAppFragment;->i:Lcom/twitter/library/client/aa;

    invoke-virtual {p0}, Lcom/twitter/android/AuthorizeAppFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string/jumbo v3, "app_name"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/android/AuthorizeAppFragment;->e:Ljava/lang/CharSequence;

    const-string/jumbo v3, "app_consumer_key"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/AuthorizeAppFragment;->f:Ljava/lang/CharSequence;

    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v1, v2, p0}, Lcom/twitter/android/client/c;->a(ILcom/twitter/library/util/ar;)V

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/a;->a(Landroid/accounts/AccountManager;)[Landroid/accounts/Account;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/AuthorizeAppFragment;->g:[Landroid/accounts/Account;

    new-instance v1, Lcom/twitter/android/util/a;

    invoke-direct {v1}, Lcom/twitter/android/util/a;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/AuthorizeAppFragment;->h:Lcom/twitter/android/util/a;

    iget-object v1, p0, Lcom/twitter/android/AuthorizeAppFragment;->g:[Landroid/accounts/Account;

    iget-object v2, p0, Lcom/twitter/android/AuthorizeAppFragment;->h:Lcom/twitter/android/util/a;

    invoke-static {v1, v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v1, v2}, Landroid/accounts/AccountManager;->addOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;Landroid/os/Handler;Z)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f030016    # com.twitter.android.R.layout.authorize

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->a:Lcom/twitter/android/client/c;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Lcom/twitter/android/client/c;->b(ILcom/twitter/library/util/ar;)V

    invoke-virtual {p0}, Lcom/twitter/android/AuthorizeAppFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/accounts/AccountManager;->removeOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;)V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    const-string/jumbo v0, "impression"

    invoke-direct {p0, v0}, Lcom/twitter/android/AuthorizeAppFragment;->a(Ljava/lang/String;)V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    const v0, 0x7f090092    # com.twitter.android.R.id.ok_button

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090093    # com.twitter.android.R.id.cancel_button

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->e:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    const v0, 0x7f090090    # com.twitter.android.R.id.title

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0f0044    # com.twitter.android.R.string.authorize_app_title

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/android/AuthorizeAppFragment;->e:Ljava/lang/CharSequence;

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/twitter/android/AuthorizeAppFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f090091    # com.twitter.android.R.id.message

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0f0043    # com.twitter.android.R.string.authorize_app_message

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/android/AuthorizeAppFragment;->e:Ljava/lang/CharSequence;

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/twitter/android/AuthorizeAppFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    const v0, 0x7f09008d    # com.twitter.android.R.id.account_row

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/UserView;

    iput-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->d:Lcom/twitter/library/widget/UserView;

    iget-object v0, p0, Lcom/twitter/android/AuthorizeAppFragment;->d:Lcom/twitter/library/widget/UserView;

    invoke-virtual {v0, v4}, Lcom/twitter/library/widget/UserView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
