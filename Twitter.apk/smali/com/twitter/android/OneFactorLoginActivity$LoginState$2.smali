.class final enum Lcom/twitter/android/OneFactorLoginActivity$LoginState$2;
.super Lcom/twitter/android/OneFactorLoginActivity$LoginState;
.source "Twttr"


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/OneFactorLoginActivity$LoginState;-><init>(Ljava/lang/String;ILcom/twitter/android/od;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/OneFactorLoginActivity;)V
    .locals 6

    const/4 v3, 0x1

    invoke-static {p1}, Lcom/twitter/android/OneFactorLoginActivity;->f(Lcom/twitter/android/OneFactorLoginActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {p1}, Lcom/twitter/android/OneFactorLoginActivity;->e(Lcom/twitter/android/OneFactorLoginActivity;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v4, 0x7530

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const v1, 0x7f0f02e8    # com.twitter.android.R.string.one_factor_waiting_for_sms

    const v2, 0x7f0f02e5    # com.twitter.android.R.string.one_factor_manually_enter

    const/4 v5, 0x0

    move-object v0, p1

    move v4, v3

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/OneFactorLoginActivity;->a(Lcom/twitter/android/OneFactorLoginActivity;IIZZZ)V

    return-void
.end method

.method public b(Lcom/twitter/android/OneFactorLoginActivity;)V
    .locals 2

    sget-object v0, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->c:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    invoke-static {p1, v0}, Lcom/twitter/android/OneFactorLoginActivity;->a(Lcom/twitter/android/OneFactorLoginActivity;Lcom/twitter/android/OneFactorLoginActivity$LoginState;)V

    invoke-static {p1}, Lcom/twitter/android/OneFactorLoginActivity;->f(Lcom/twitter/android/OneFactorLoginActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {p1}, Lcom/twitter/android/OneFactorLoginActivity;->e(Lcom/twitter/android/OneFactorLoginActivity;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method public c(Lcom/twitter/android/OneFactorLoginActivity;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
