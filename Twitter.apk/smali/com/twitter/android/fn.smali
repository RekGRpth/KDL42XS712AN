.class Lcom/twitter/android/fn;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/EventSearchActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/EventSearchActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/fn;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/fn;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v0}, Lcom/twitter/android/EventSearchActivity;->b(Lcom/twitter/android/EventSearchActivity;)Lcom/twitter/android/fr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/fr;->a()I

    move-result v0

    if-ne p3, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/fn;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v0}, Lcom/twitter/android/EventSearchActivity;->c(Lcom/twitter/android/EventSearchActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iget-object v1, p0, Lcom/twitter/android/fn;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-virtual {v1}, Lcom/twitter/android/EventSearchActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhb;->a(Landroid/support/v4/app/FragmentManager;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/SearchFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/SearchFragment;->e()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/fn;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v0}, Lcom/twitter/android/EventSearchActivity;->d(Lcom/twitter/android/EventSearchActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    iget-object v0, p0, Lcom/twitter/android/fn;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v0}, Lcom/twitter/android/EventSearchActivity;->b(Lcom/twitter/android/EventSearchActivity;)Lcom/twitter/android/fr;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/twitter/android/fr;->a(I)V

    goto :goto_0
.end method
