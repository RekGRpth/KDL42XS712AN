.class Lcom/twitter/android/aa;
.super Lcom/twitter/library/client/z;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/ActivityFragment;


# direct methods
.method private constructor <init>(Lcom/twitter/android/ActivityFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/aa;->a:Lcom/twitter/android/ActivityFragment;

    invoke-direct {p0}, Lcom/twitter/library/client/z;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/ActivityFragment;Lcom/twitter/android/x;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/aa;-><init>(Lcom/twitter/android/ActivityFragment;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/aa;->a:Lcom/twitter/android/ActivityFragment;

    invoke-static {v0}, Lcom/twitter/android/ActivityFragment;->c(Lcom/twitter/android/ActivityFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/aa;->a:Lcom/twitter/android/ActivityFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/ActivityFragment;->a(Lcom/twitter/android/ActivityFragment;Z)Z

    iget-object v0, p0, Lcom/twitter/android/aa;->a:Lcom/twitter/android/ActivityFragment;

    invoke-static {v0}, Lcom/twitter/android/ActivityFragment;->e(Lcom/twitter/android/ActivityFragment;)Lcom/twitter/android/util/w;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/aa;->a:Lcom/twitter/android/ActivityFragment;

    invoke-static {v1}, Lcom/twitter/android/ActivityFragment;->d(Lcom/twitter/android/ActivityFragment;)I

    move-result v1

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/util/w;->a(IJLjava/lang/String;)V

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/library/client/z;->a(Lcom/twitter/library/client/Session;)V

    iget-object v0, p0, Lcom/twitter/android/aa;->a:Lcom/twitter/android/ActivityFragment;

    invoke-static {v0}, Lcom/twitter/android/ActivityFragment;->f(Lcom/twitter/android/ActivityFragment;)V

    return-void
.end method
