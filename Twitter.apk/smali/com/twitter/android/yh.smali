.class Lcom/twitter/android/yh;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/twitter/android/yg;


# direct methods
.method constructor <init>(Lcom/twitter/android/yg;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/yh;->a:Lcom/twitter/android/yg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/yh;->a:Lcom/twitter/android/yg;

    iput-boolean v5, v0, Lcom/twitter/android/yg;->c:Z

    iget-object v0, p0, Lcom/twitter/android/yh;->a:Lcom/twitter/android/yg;

    iget-wide v0, v0, Lcom/twitter/android/yg;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/twitter/android/yh;->a:Lcom/twitter/android/yg;

    iget-wide v2, v2, Lcom/twitter/android/yg;->a:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x1f4

    sub-long/2addr v0, v2

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v3, p0, Lcom/twitter/android/yh;->a:Lcom/twitter/android/yg;

    iget-object v3, v3, Lcom/twitter/android/yg;->f:Lcom/twitter/library/client/aa;

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "app::::become_inactive"

    aput-object v4, v3, v5

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    long-to-int v0, v0

    invoke-virtual {v2, v0}, Lcom/twitter/library/scribe/ScribeLog;->g(I)Lcom/twitter/library/scribe/ScribeLog;

    iget-object v0, p0, Lcom/twitter/android/yh;->a:Lcom/twitter/android/yg;

    iget-object v0, v0, Lcom/twitter/android/yg;->g:Lcom/twitter/android/client/c;

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_0
    return-void
.end method
