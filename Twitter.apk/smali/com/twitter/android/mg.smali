.class Lcom/twitter/android/mg;
.super Lcom/twitter/library/client/z;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/MessagesActivity;


# direct methods
.method public constructor <init>(Lcom/twitter/android/MessagesActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/mg;->a:Lcom/twitter/android/MessagesActivity;

    invoke-direct {p0}, Lcom/twitter/library/client/z;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/mg;->a:Lcom/twitter/android/MessagesActivity;

    invoke-virtual {v0}, Lcom/twitter/android/MessagesActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/mg;->a:Lcom/twitter/android/MessagesActivity;

    invoke-static {v1}, Lcom/twitter/android/MessagesActivity;->c(Lcom/twitter/android/MessagesActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/MessagesFragment;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/MessagesFragment;->f(J)V

    :cond_0
    return-void
.end method
