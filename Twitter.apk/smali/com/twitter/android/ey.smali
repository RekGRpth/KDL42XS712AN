.class public Lcom/twitter/android/ey;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lhe;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/net/Uri;)Lhb;
    .locals 2

    new-instance v0, Lhd;

    const-class v1, Lcom/twitter/android/DiscoverFragment;

    invoke-direct {v0, p2, v1}, Lhd;-><init>(Landroid/net/Uri;Ljava/lang/Class;)V

    invoke-static {p1}, Lcom/twitter/android/MainActivity;->a(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhd;->a(Landroid/os/Bundle;)Lhd;

    move-result-object v0

    const v1, 0x7f0f0112    # com.twitter.android.R.string.discover

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhd;->a(Ljava/lang/CharSequence;)Lhd;

    move-result-object v0

    const v1, 0x7f0201ed    # com.twitter.android.R.drawable.ic_perch_toptweets_default

    invoke-virtual {v0, v1}, Lhd;->a(I)Lhd;

    move-result-object v0

    const v1, 0x7f020252    # com.twitter.android.R.drawable.ic_tab_discover

    invoke-virtual {v0, v1}, Lhd;->b(I)Lhd;

    move-result-object v0

    const-string/jumbo v1, "discover"

    invoke-virtual {v0, v1}, Lhd;->a(Ljava/lang/String;)Lhd;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhd;->b(Z)Lhd;

    move-result-object v0

    invoke-virtual {v0}, Lhd;->a()Lhb;

    move-result-object v0

    return-object v0
.end method
