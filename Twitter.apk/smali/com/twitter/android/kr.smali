.class Lcom/twitter/android/kr;
.super Landroid/support/v4/app/FragmentPagerAdapter;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/MainActivity;

.field private final b:Landroid/support/v4/view/ViewPager;

.field private final c:Ljava/util/ArrayList;

.field private d:I


# direct methods
.method public constructor <init>(Lcom/twitter/android/MainActivity;Lcom/twitter/android/MainActivity;Ljava/util/ArrayList;Landroid/support/v4/view/ViewPager;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/kr;->a:Lcom/twitter/android/MainActivity;

    invoke-virtual {p2}, Lcom/twitter/android/MainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v4/app/FragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/kr;->d:I

    iput-object p4, p0, Lcom/twitter/android/kr;->b:Landroid/support/v4/view/ViewPager;

    iget-object v0, p0, Lcom/twitter/android/kr;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    iput-object p3, p0, Lcom/twitter/android/kr;->c:Ljava/util/ArrayList;

    new-instance v0, Lcom/twitter/android/ks;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/ks;-><init>(Lcom/twitter/android/kr;Lcom/twitter/android/MainActivity;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/kr;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/kr;I)I
    .locals 0

    iput p1, p0, Lcom/twitter/android/kr;->d:I

    return p1
.end method


# virtual methods
.method public a(Landroid/net/Uri;)I
    .locals 2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/kr;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/kr;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iget-object v0, v0, Lhb;->c:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public a(I)Lhb;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/kr;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    return-object v0
.end method

.method public a()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/kr;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method public a(ILjava/lang/String;Lhb;Lhb;)V
    .locals 9

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide/16 v1, -0x1

    const-string/jumbo v0, ""

    iget v5, p0, Lcom/twitter/android/kr;->d:I

    const/4 v6, -0x1

    if-eq v5, v6, :cond_2

    if-nez p3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/kr;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v0}, Lcom/twitter/android/MainActivity;->j(Lcom/twitter/android/MainActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/kr;->d:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    :goto_0
    iget-object v0, v0, Lhb;->h:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/kr;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v1}, Lcom/twitter/android/MainActivity;->D(Lcom/twitter/android/MainActivity;)J

    move-result-wide v1

    sub-long v1, v3, v1

    move-wide v7, v1

    move-wide v2, v7

    move-object v1, v0

    :goto_1
    if-nez p4, :cond_0

    iget-object v0, p0, Lcom/twitter/android/kr;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    :goto_2
    new-instance v4, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v5, p0, Lcom/twitter/android/kr;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v5}, Lcom/twitter/android/MainActivity;->E(Lcom/twitter/android/MainActivity;)Lcom/twitter/library/client/Session;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    const/4 v1, 0x1

    const-string/jumbo v6, ""

    aput-object v6, v5, v1

    const/4 v1, 0x2

    aput-object p2, v5, v1

    const/4 v1, 0x3

    iget-object v0, v0, Lhb;->h:Ljava/lang/String;

    aput-object v0, v5, v1

    const/4 v0, 0x4

    const-string/jumbo v1, "navigate"

    aput-object v1, v5, v0

    invoke-virtual {v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->g(I)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/kr;->d:I

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->e(I)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/library/scribe/ScribeLog;->d(I)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/kr;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v1}, Lcom/twitter/android/MainActivity;->F(Lcom/twitter/android/MainActivity;)Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void

    :cond_0
    move-object v0, p4

    goto :goto_2

    :cond_1
    move-object v0, p3

    goto :goto_0

    :cond_2
    move-wide v7, v1

    move-wide v2, v7

    move-object v1, v0

    goto :goto_1
.end method

.method public b(I)Landroid/net/Uri;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/kr;->a(I)Lhb;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lhb;->c:Landroid/net/Uri;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Lhb;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/kr;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/kr;->a(I)Lhb;

    move-result-object v0

    return-object v0
.end method

.method public c()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/kr;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/kr;->b(I)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/kr;->c:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/twitter/android/kr;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iget-object v0, v0, Lhb;->h:Ljava/lang/String;

    return-object v0
.end method

.method public e()V
    .locals 1

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/kr;->d:I

    invoke-virtual {p0}, Lcom/twitter/android/kr;->notifyDataSetChanged()V

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/kr;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/kr;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iget-object v1, p0, Lcom/twitter/android/kr;->a:Lcom/twitter/android/MainActivity;

    iget-object v2, v0, Lhb;->a:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Lhb;->b:Landroid/os/Bundle;

    invoke-static {v1, v2, v0}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/kr;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iget v0, v0, Lhb;->f:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/kr;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iget-object v2, p0, Lcom/twitter/android/kr;->a:Lcom/twitter/android/MainActivity;

    invoke-virtual {v2}, Lcom/twitter/android/MainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v0, v2}, Lhb;->a(Landroid/support/v4/app/FragmentManager;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-ne v2, p1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/kr;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    :goto_0
    return v0

    :cond_1
    const/4 v0, -0x2

    goto :goto_0
.end method

.method public getPageTitle(I)Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/kr;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iget-object v0, v0, Lhb;->d:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 4

    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentPagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/BaseListFragment;

    iget-object v1, p0, Lcom/twitter/android/kr;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhb;

    invoke-virtual {v1, v0}, Lhb;->a(Landroid/support/v4/app/Fragment;)V

    new-instance v2, Lcom/twitter/android/kq;

    iget-object v3, p0, Lcom/twitter/android/kr;->a:Lcom/twitter/android/MainActivity;

    iget-object v1, v1, Lhb;->c:Landroid/net/Uri;

    invoke-direct {v2, v3, v1}, Lcom/twitter/android/kq;-><init>(Lcom/twitter/android/MainActivity;Landroid/net/Uri;)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/BaseListFragment;->a(Lcom/twitter/android/client/ai;)Lcom/twitter/android/client/BaseListFragment;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/kr;->a:Lcom/twitter/android/MainActivity;

    iget-object v2, v2, Lcom/twitter/android/MainActivity;->h:Lcom/twitter/android/fe;

    invoke-virtual {v1, v2}, Lcom/twitter/android/client/BaseListFragment;->a(Lcom/twitter/refresh/widget/e;)Lcom/twitter/android/client/BaseListFragment;

    iget-object v1, p0, Lcom/twitter/android/kr;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    if-ne p2, v1, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/client/BaseListFragment;->Z()V

    :cond_0
    return-object v0
.end method

.method public onPageScrollStateChanged(I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/kr;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v0}, Lcom/twitter/android/MainActivity;->A(Lcom/twitter/android/MainActivity;)Lcom/twitter/internal/android/widget/HorizontalListView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(I)V

    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/kr;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v0}, Lcom/twitter/android/MainActivity;->A(Lcom/twitter/android/MainActivity;)Lcom/twitter/internal/android/widget/HorizontalListView;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(IF)V

    iget-object v0, p0, Lcom/twitter/android/kr;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v0}, Lcom/twitter/android/MainActivity;->z(Lcom/twitter/android/MainActivity;)Lcom/twitter/internal/android/widget/DockLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/kr;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v0}, Lcom/twitter/android/MainActivity;->z(Lcom/twitter/android/MainActivity;)Lcom/twitter/internal/android/widget/DockLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/DockLayout;->a()V

    :cond_0
    return-void
.end method

.method public onPageSelected(I)V
    .locals 7

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/twitter/android/kr;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v0}, Lcom/twitter/android/MainActivity;->e(Lcom/twitter/android/MainActivity;)Lcom/twitter/android/km;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/km;->a(I)V

    iget-object v0, p0, Lcom/twitter/android/kr;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v0}, Lcom/twitter/android/MainActivity;->A(Lcom/twitter/android/MainActivity;)Lcom/twitter/internal/android/widget/HorizontalListView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/HorizontalListView;->b(I)V

    iget-object v0, p0, Lcom/twitter/android/kr;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iget-object v1, p0, Lcom/twitter/android/kr;->a:Lcom/twitter/android/MainActivity;

    iget-object v2, v0, Lhb;->c:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/twitter/android/MainActivity;->b(Landroid/net/Uri;)V

    iget-object v1, p0, Lcom/twitter/android/kr;->a:Lcom/twitter/android/MainActivity;

    invoke-virtual {v1}, Lcom/twitter/android/MainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    const/4 v1, 0x0

    iget v2, p0, Lcom/twitter/android/kr;->d:I

    const/4 v6, -0x1

    if-eq v2, v6, :cond_5

    iget-object v1, p0, Lcom/twitter/android/kr;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v1}, Lcom/twitter/android/MainActivity;->j(Lcom/twitter/android/MainActivity;)Ljava/util/ArrayList;

    move-result-object v1

    iget v2, p0, Lcom/twitter/android/kr;->d:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhb;

    invoke-virtual {v1, v5}, Lhb;->a(Landroid/support/v4/app/FragmentManager;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/client/BaseListFragment;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/twitter/android/client/BaseListFragment;->aa()V

    :cond_0
    move-object v2, v1

    :goto_0
    invoke-virtual {v0, v5}, Lhb;->a(Landroid/support/v4/app/FragmentManager;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/client/BaseListFragment;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/twitter/android/client/BaseListFragment;->Z()V

    :cond_1
    iget-object v5, p0, Lcom/twitter/android/kr;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v5}, Lcom/twitter/android/MainActivity;->z(Lcom/twitter/android/MainActivity;)Lcom/twitter/internal/android/widget/DockLayout;

    move-result-object v5

    if-eqz v5, :cond_2

    iget v5, p0, Lcom/twitter/android/kr;->d:I

    if-eq v5, p1, :cond_2

    iget-object v5, p0, Lcom/twitter/android/kr;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v5}, Lcom/twitter/android/MainActivity;->z(Lcom/twitter/android/MainActivity;)Lcom/twitter/internal/android/widget/DockLayout;

    move-result-object v5

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/twitter/android/client/BaseListFragment;->V()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    if-nez v1, :cond_3

    move v1, v3

    :goto_1
    invoke-virtual {v5, v1}, Lcom/twitter/internal/android/widget/DockLayout;->setTopLocked(Z)V

    :cond_2
    const-string/jumbo v1, ""

    invoke-virtual {p0, p1, v1, v2, v0}, Lcom/twitter/android/kr;->a(ILjava/lang/String;Lhb;Lhb;)V

    iput p1, p0, Lcom/twitter/android/kr;->d:I

    iget-object v1, p0, Lcom/twitter/android/kr;->a:Lcom/twitter/android/MainActivity;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-static {v1, v5, v6}, Lcom/twitter/android/MainActivity;->a(Lcom/twitter/android/MainActivity;J)J

    iget-object v1, p0, Lcom/twitter/android/kr;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v1}, Lcom/twitter/android/MainActivity;->d(Lcom/twitter/android/MainActivity;)Lcom/twitter/android/kf;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/kf;->d(Lhb;)I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/kr;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v1}, Lcom/twitter/android/MainActivity;->B(Lcom/twitter/android/MainActivity;)Lcom/mobeta/android/dslv/DragSortListView;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Lcom/mobeta/android/dslv/DragSortListView;->setItemChecked(IZ)V

    iget-object v1, p0, Lcom/twitter/android/kr;->a:Lcom/twitter/android/MainActivity;

    invoke-virtual {v1}, Lcom/twitter/android/MainActivity;->V()V

    iget-object v1, p0, Lcom/twitter/android/kr;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v1}, Lcom/twitter/android/MainActivity;->C(Lcom/twitter/android/MainActivity;)Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    move-result-object v1

    if-nez v0, :cond_4

    :goto_2
    invoke-virtual {v1, v3}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->setDrawerOpenable(Z)V

    return-void

    :cond_3
    move v1, v4

    goto :goto_1

    :cond_4
    move v3, v4

    goto :goto_2

    :cond_5
    move-object v2, v1

    goto :goto_0
.end method

.method public restoreState(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 6

    check-cast p1, Lcom/twitter/android/MainActivity$PageSavedState;

    iget-object v3, p1, Lcom/twitter/android/MainActivity$PageSavedState;->a:[Ljava/lang/String;

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    array-length v0, v3

    if-ge v2, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/kr;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    aget-object v0, v3, v2

    iget-object v1, p0, Lcom/twitter/android/kr;->a:Lcom/twitter/android/MainActivity;

    invoke-virtual {v1}, Lcom/twitter/android/MainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/BaseListFragment;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/kr;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhb;

    invoke-virtual {v1, v0}, Lhb;->a(Landroid/support/v4/app/Fragment;)V

    new-instance v4, Lcom/twitter/android/kq;

    iget-object v5, p0, Lcom/twitter/android/kr;->a:Lcom/twitter/android/MainActivity;

    iget-object v1, v1, Lhb;->c:Landroid/net/Uri;

    invoke-direct {v4, v5, v1}, Lcom/twitter/android/kq;-><init>(Lcom/twitter/android/MainActivity;Landroid/net/Uri;)V

    invoke-virtual {v0, v4}, Lcom/twitter/android/client/BaseListFragment;->a(Lcom/twitter/android/client/ai;)Lcom/twitter/android/client/BaseListFragment;

    iget-object v1, p0, Lcom/twitter/android/kr;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v1}, Lcom/twitter/android/MainActivity;->z(Lcom/twitter/android/MainActivity;)Lcom/twitter/internal/android/widget/DockLayout;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/kr;->a:Lcom/twitter/android/MainActivity;

    iget-object v1, v1, Lcom/twitter/android/MainActivity;->h:Lcom/twitter/android/fe;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/BaseListFragment;->a(Lcom/twitter/refresh/widget/e;)Lcom/twitter/android/client/BaseListFragment;

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public saveState()Landroid/os/Parcelable;
    .locals 2

    new-instance v0, Lcom/twitter/android/MainActivity$PageSavedState;

    iget-object v1, p0, Lcom/twitter/android/kr;->c:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Lcom/twitter/android/MainActivity$PageSavedState;-><init>(Ljava/util/ArrayList;)V

    return-object v0
.end method
