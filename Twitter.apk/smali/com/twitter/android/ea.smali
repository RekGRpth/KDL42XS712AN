.class Lcom/twitter/android/ea;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Z

.field public final g:J

.field public final h:Z

.field public i:Lcom/twitter/library/api/conversations/DMPhoto;

.field public j:Ljava/lang/StringBuilder;

.field public k:Z

.field final synthetic l:Lcom/twitter/android/dx;


# direct methods
.method public constructor <init>(Lcom/twitter/android/dx;Landroid/database/Cursor;)V
    .locals 8

    const/4 v7, 0x5

    const/4 v2, 0x1

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/twitter/android/ea;->l:Lcom/twitter/android/dx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/twitter/android/ea;->f:Z

    iput-boolean v0, p0, Lcom/twitter/android/ea;->k:Z

    const/4 v1, 0x2

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/ea;->a:Ljava/lang/String;

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {p1}, Lcom/twitter/android/dx;->a(Lcom/twitter/android/dx;)Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    cmp-long v1, v3, v5

    if-nez v1, :cond_0

    move v0, v2

    :cond_0
    iput-boolean v0, p0, Lcom/twitter/android/ea;->h:Z

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/ea;->b:J

    const/16 v0, 0xb

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ea;->d:Ljava/lang/String;

    const/16 v0, 0xc

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ea;->e:Ljava/lang/String;

    const/16 v0, 0xd

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ea;->c:Ljava/lang/String;

    const/16 v0, 0x9

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/ea;->g:J

    const/16 v0, 0xa

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/conversations/DMMessage;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, ""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/ea;->j:Ljava/lang/StringBuilder;

    :goto_0
    return-void

    :cond_1
    iget-object v3, v0, Lcom/twitter/library/api/conversations/DMMessage;->entities:Lcom/twitter/library/api/TweetEntities;

    iget-object v1, v0, Lcom/twitter/library/api/conversations/DMMessage;->text:Ljava/lang/String;

    if-nez v1, :cond_3

    const-string/jumbo v1, ""

    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v4, p0, Lcom/twitter/android/ea;->j:Ljava/lang/StringBuilder;

    const-string/jumbo v1, "photo"

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/conversations/DMMessage;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/twitter/library/api/conversations/DMMessage;->attachment:Lcom/twitter/library/api/conversations/k;

    check-cast v1, Lcom/twitter/library/api/conversations/DMPhoto;

    iput-object v1, p0, Lcom/twitter/android/ea;->i:Lcom/twitter/library/api/conversations/DMPhoto;

    iget-object v1, p0, Lcom/twitter/android/ea;->j:Ljava/lang/StringBuilder;

    invoke-static {v1, v0}, Lcom/twitter/library/api/conversations/ae;->a(Ljava/lang/StringBuilder;Lcom/twitter/library/api/conversations/DMMessage;)V

    iget-object v0, p0, Lcom/twitter/android/ea;->j:Ljava/lang/StringBuilder;

    invoke-static {v0}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/twitter/android/dx;->b(Lcom/twitter/android/dx;)Landroid/content/Context;

    move-result-object v1

    const v4, 0x7f0f0114    # com.twitter.android.R.string.dm_attachment_image

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/ea;->j:Ljava/lang/StringBuilder;

    iput-boolean v2, p0, Lcom/twitter/android/ea;->k:Z

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/ea;->j:Ljava/lang/StringBuilder;

    invoke-static {v0, v3}, Lcom/twitter/library/api/TweetEntities;->a(Ljava/lang/StringBuilder;Lcom/twitter/library/api/TweetEntities;)Ljava/lang/StringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ea;->j:Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_3
    iget-object v1, v0, Lcom/twitter/library/api/conversations/DMMessage;->text:Ljava/lang/String;

    goto :goto_1
.end method
