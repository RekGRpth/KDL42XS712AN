.class Lcom/twitter/android/tn;
.super Landroid/widget/BaseAdapter;
.source "Twttr"


# instance fields
.field protected final a:Lcom/twitter/android/client/c;

.field protected b:[Lcom/twitter/android/to;

.field private final c:I


# direct methods
.method constructor <init>(Lcom/twitter/android/client/c;Lcom/twitter/android/to;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/twitter/android/to;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/tn;-><init>(Lcom/twitter/android/client/c;[Lcom/twitter/android/to;)V

    return-void
.end method

.method constructor <init>(Lcom/twitter/android/client/c;[Lcom/twitter/android/to;)V
    .locals 1

    const v0, 0x7f03013c    # com.twitter.android.R.layout.simple_row_view

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/tn;-><init>(Lcom/twitter/android/client/c;[Lcom/twitter/android/to;I)V

    return-void
.end method

.method constructor <init>(Lcom/twitter/android/client/c;[Lcom/twitter/android/to;I)V
    .locals 0

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/tn;->a:Lcom/twitter/android/client/c;

    iput-object p2, p0, Lcom/twitter/android/tn;->b:[Lcom/twitter/android/to;

    iput p3, p0, Lcom/twitter/android/tn;->c:I

    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/tn;->b:[Lcom/twitter/android/to;

    aget-object v0, v0, p1

    iput p2, v0, Lcom/twitter/android/to;->a:I

    invoke-virtual {p0}, Lcom/twitter/android/tn;->notifyDataSetChanged()V

    return-void
.end method

.method public a([Lcom/twitter/android/to;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/tn;->b:[Lcom/twitter/android/to;

    invoke-virtual {p0}, Lcom/twitter/android/tn;->notifyDataSetChanged()V

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/tn;->b:[Lcom/twitter/android/to;

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/tn;->b:[Lcom/twitter/android/to;

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/twitter/android/to;->c:Landroid/content/Intent;

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    iget v0, p0, Lcom/twitter/android/tn;->c:I

    iget-object v1, p0, Lcom/twitter/android/tn;->b:[Lcom/twitter/android/to;

    aget-object v1, v1, p1

    iget-object v2, p0, Lcom/twitter/android/tn;->a:Lcom/twitter/android/client/c;

    invoke-virtual {v2}, Lcom/twitter/android/client/c;->V()F

    move-result v2

    invoke-static {v0, p2, p3, v1, v2}, Lcom/twitter/android/tp;->a(ILandroid/view/View;Landroid/view/ViewGroup;Lcom/twitter/android/to;F)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
