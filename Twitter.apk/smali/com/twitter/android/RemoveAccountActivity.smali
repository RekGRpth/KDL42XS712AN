.class public Lcom/twitter/android/RemoveAccountActivity;
.super Lcom/twitter/android/client/BasePreferenceActivity;
.source "Twttr"


# instance fields
.field a:Landroid/accounts/AccountAuthenticatorResponse;

.field b:Ljava/lang/String;

.field c:Z

.field private d:Lcom/twitter/android/rk;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BasePreferenceActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/RemoveAccountActivity;->finish()V

    iget-boolean v0, p0, Lcom/twitter/android/RemoveAccountActivity;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/RemoveAccountActivity;->moveTaskToBack(Z)Z

    :cond_0
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/RemoveAccountActivity;->a()V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BasePreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/RemoveAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/RemoveAccountActivity;->a:Landroid/accounts/AccountAuthenticatorResponse;

    iput-boolean v2, p0, Lcom/twitter/android/RemoveAccountActivity;->c:Z

    const-string/jumbo v0, "account_name"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/RemoveAccountActivity;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/RemoveAccountActivity;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/android/RemoveAccountActivity;->setTitle(Ljava/lang/CharSequence;)V

    const-string/jumbo v0, "authenticator_response"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "authenticator_response"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountAuthenticatorResponse;

    iput-object v0, p0, Lcom/twitter/android/RemoveAccountActivity;->a:Landroid/accounts/AccountAuthenticatorResponse;

    :cond_0
    const-string/jumbo v0, "from_system_settings"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "from_system_settings"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/RemoveAccountActivity;->c:Z

    :cond_1
    new-instance v0, Lcom/twitter/android/rk;

    invoke-direct {v0, p0}, Lcom/twitter/android/rk;-><init>(Lcom/twitter/android/RemoveAccountActivity;)V

    iput-object v0, p0, Lcom/twitter/android/RemoveAccountActivity;->d:Lcom/twitter/android/rk;

    invoke-virtual {p0}, Lcom/twitter/android/RemoveAccountActivity;->c()Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/RemoveAccountActivity;->d:Lcom/twitter/android/rk;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/z;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/RemoveAccountDialogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "account_name"

    iget-object v2, p0, Lcom/twitter/android/RemoveAccountActivity;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/RemoveAccountActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/client/BasePreferenceActivity;->onDestroy()V

    invoke-virtual {p0}, Lcom/twitter/android/RemoveAccountActivity;->c()Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/RemoveAccountActivity;->d:Lcom/twitter/android/rk;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->b(Lcom/twitter/library/client/z;)V

    return-void
.end method
