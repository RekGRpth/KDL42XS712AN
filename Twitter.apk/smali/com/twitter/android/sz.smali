.class public Lcom/twitter/android/sz;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/SecuritySettingsActivity;

.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/twitter/android/SecuritySettingsActivity;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/sz;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/twitter/android/sz;->b:Landroid/content/Context;

    iput-object p3, p0, Lcom/twitter/android/sz;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public varargs a([Ljava/lang/Void;)Lcom/twitter/library/api/account/m;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/sz;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/sz;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/library/api/account/k;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/twitter/library/api/account/m;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/api/account/m;)V
    .locals 5

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/sz;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-virtual {v0}, Lcom/twitter/android/SecuritySettingsActivity;->a()V

    iget-object v0, p0, Lcom/twitter/android/sz;->a:Lcom/twitter/android/SecuritySettingsActivity;

    const-string/jumbo v1, "login_verification"

    invoke-virtual {v0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/twitter/android/sz;->a:Lcom/twitter/android/SecuritySettingsActivity;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->showDialog(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/sz;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/SecuritySettingsActivity;->q(Lcom/twitter/android/SecuritySettingsActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/sz;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/SecuritySettingsActivity;->A(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/sz;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-virtual {v1}, Lcom/twitter/android/SecuritySettingsActivity;->c()Lcom/twitter/library/client/aa;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/sz;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v1

    iget-object v2, p1, Lcom/twitter/library/api/account/m;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/api/account/m;->c:Ljava/lang/String;

    iget v4, p1, Lcom/twitter/library/api/account/m;->a:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/sz;->a([Ljava/lang/Void;)Lcom/twitter/library/api/account/m;

    move-result-object v0

    return-object v0
.end method

.method public synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/twitter/library/api/account/m;

    invoke-virtual {p0, p1}, Lcom/twitter/android/sz;->a(Lcom/twitter/library/api/account/m;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/sz;->a:Lcom/twitter/android/SecuritySettingsActivity;

    iget-object v1, p0, Lcom/twitter/android/sz;->a:Lcom/twitter/android/SecuritySettingsActivity;

    const v2, 0x7f0f0248    # com.twitter.android.R.string.login_verification_initializing

    invoke-virtual {v1, v2}, Lcom/twitter/android/SecuritySettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->a(Ljava/lang/String;)V

    return-void
.end method
