.class public Lcom/twitter/android/DMConversationFragment;
.super Lcom/twitter/android/client/BaseListFragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/ce;
.implements Lcom/twitter/library/view/c;


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:Ljava/text/SimpleDateFormat;

.field private static final c:Ljava/text/SimpleDateFormat;

.field private static final d:Ljava/text/SimpleDateFormat;


# instance fields
.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Z

.field private i:Z

.field private j:Lcom/twitter/android/el;

.field private k:Ljava/util/Map;

.field private l:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "entry_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "conversation_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "user_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "entry_type"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "created"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "s_profile_image_url"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "data"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/android/DMConversationFragment;->a:[Ljava/lang/String;

    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0}, Ljava/text/SimpleDateFormat;-><init>()V

    sput-object v0, Lcom/twitter/android/DMConversationFragment;->b:Ljava/text/SimpleDateFormat;

    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0}, Ljava/text/SimpleDateFormat;-><init>()V

    sput-object v0, Lcom/twitter/android/DMConversationFragment;->c:Ljava/text/SimpleDateFormat;

    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0}, Ljava/text/SimpleDateFormat;-><init>()V

    sput-object v0, Lcom/twitter/android/DMConversationFragment;->d:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/client/BaseListFragment;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/DMConversationFragment;->k:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/DMConversationFragment;->l:Ljava/util/Map;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/DMConversationFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/DMConversationFragment;->e:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/DMConversationFragment;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/DMConversationFragment;->d(I)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/DMConversationFragment;Landroid/database/Cursor;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/DMConversationFragment;->f(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/DMConversationFragment;Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/DMConversationFragment;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 3

    new-instance v0, Lcom/twitter/library/api/conversations/ar;

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2, p1, p2}, Lcom/twitter/library/api/conversations/ar;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Z)V

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/DMConversationFragment;->a(Lcom/twitter/library/service/b;II)Z

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/DMConversationFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/DMConversationFragment;->i:Z

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/DMConversationFragment;Lcom/twitter/library/service/b;II)Z
    .locals 1

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/DMConversationFragment;->a(Lcom/twitter/library/service/b;II)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/DMConversationFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/DMConversationFragment;->i:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/android/DMConversationFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/DMConversationFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/DMConversationFragment;->f:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/twitter/android/DMConversationFragment;Lcom/twitter/library/service/b;II)Z
    .locals 1

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/DMConversationFragment;->a(Lcom/twitter/library/service/b;II)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/DMConversationFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/DMConversationFragment;->h:Z

    return p1
.end method

.method static synthetic c(Lcom/twitter/android/DMConversationFragment;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/DMConversationFragment;->g:Ljava/lang/String;

    return-object v0
.end method

.method private c(Landroid/database/Cursor;)V
    .locals 2

    const/4 v0, 0x5

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f080006    # com.twitter.android.R.array.dm_longpress

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->f(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    new-instance v1, Lcom/twitter/android/dn;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/dn;-><init>(Lcom/twitter/android/DMConversationFragment;Landroid/database/Cursor;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/android/widget/ce;)Lcom/twitter/android/widget/PromptDialogFragment;

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    return-void
.end method

.method static synthetic c(Lcom/twitter/android/DMConversationFragment;Lcom/twitter/library/service/b;II)Z
    .locals 1

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/DMConversationFragment;->a(Lcom/twitter/library/service/b;II)Z

    move-result v0

    return v0
.end method

.method static synthetic d(Lcom/twitter/android/DMConversationFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private d(I)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/DMConversationFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-virtual {p0, v0}, Lcom/twitter/android/DMConversationFragment;->b(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/twitter/android/DMConversationFragment;->e(Landroid/database/Cursor;)V

    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/DMConversationFragment;->h:Z

    return-void

    :cond_0
    invoke-direct {p0, v0}, Lcom/twitter/android/DMConversationFragment;->c(Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method static synthetic e(Lcom/twitter/android/DMConversationFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method private e(Landroid/database/Cursor;)V
    .locals 2

    const/4 v0, 0x6

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f080005    # com.twitter.android.R.array.dm_draft_longpress

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->f(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    new-instance v1, Lcom/twitter/android/do;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/do;-><init>(Lcom/twitter/android/DMConversationFragment;Landroid/database/Cursor;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/android/widget/ce;)Lcom/twitter/android/widget/PromptDialogFragment;

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    return-void
.end method

.method static synthetic f(Lcom/twitter/android/DMConversationFragment;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/DMConversationFragment;->e:Ljava/lang/String;

    return-object v0
.end method

.method private f(Landroid/database/Cursor;)V
    .locals 6

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "messages:thread:message::copy"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/conversations/DMMessage;

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v0, v0, Lcom/twitter/library/api/conversations/DMMessage;->text:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/twitter/library/util/Util;->b(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic g(Lcom/twitter/android/DMConversationFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/DMConversationFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/DMConversationFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/DMConversationFragment;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/DMConversationFragment;->k:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/android/DMConversationFragment;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/DMConversationFragment;->l:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic l(Lcom/twitter/android/DMConversationFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic m(Lcom/twitter/android/DMConversationFragment;)Lcom/twitter/library/scribe/ScribeAssociation;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/DMConversationFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    return-object v0
.end method

.method static synthetic n(Lcom/twitter/android/DMConversationFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic q()Ljava/text/SimpleDateFormat;
    .locals 1

    sget-object v0, Lcom/twitter/android/DMConversationFragment;->b:Ljava/text/SimpleDateFormat;

    return-object v0
.end method

.method static synthetic r()Ljava/text/SimpleDateFormat;
    .locals 1

    sget-object v0, Lcom/twitter/android/DMConversationFragment;->c:Ljava/text/SimpleDateFormat;

    return-object v0
.end method

.method static synthetic s()Ljava/text/SimpleDateFormat;
    .locals 1

    sget-object v0, Lcom/twitter/android/DMConversationFragment;->d:Ljava/text/SimpleDateFormat;

    return-object v0
.end method

.method private u()V
    .locals 3

    invoke-direct {p0}, Lcom/twitter/android/DMConversationFragment;->v()V

    new-instance v0, Lcom/twitter/android/el;

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/el;-><init>(Landroid/app/Activity;Lcom/twitter/library/client/Session;)V

    iput-object v0, p0, Lcom/twitter/android/DMConversationFragment;->j:Lcom/twitter/android/el;

    iget-object v0, p0, Lcom/twitter/android/DMConversationFragment;->j:Lcom/twitter/android/el;

    invoke-virtual {v0}, Lcom/twitter/android/el;->start()V

    return-void
.end method

.method private v()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/DMConversationFragment;->j:Lcom/twitter/android/el;

    invoke-static {v0}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/DMConversationFragment;->j:Lcom/twitter/android/el;

    return-void
.end method


# virtual methods
.method protected a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    const v0, 0x7f03004e    # com.twitter.android.R.layout.dm_conversation_fragment

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/android/DMConversationFragment;->a(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/content/Context;IILcom/twitter/library/service/b;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/android/client/BaseListFragment;->a(Landroid/content/Context;IILcom/twitter/library/service/b;)V

    invoke-virtual {p4}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-nez v0, :cond_0

    check-cast p4, Lcom/twitter/library/api/conversations/ar;

    invoke-virtual {p4}, Lcom/twitter/library/api/conversations/ar;->h()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x7

    invoke-static {v1}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f010c    # com.twitter.android.R.string.direct_message_error_title

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f0319    # com.twitter.android.R.string.post_retry_direct_messsage_question

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f0362    # com.twitter.android.R.string.retry

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f0089    # com.twitter.android.R.string.cancel

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/dp;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/dp;-><init>(Lcom/twitter/android/DMConversationFragment;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/android/widget/ce;)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    :pswitch_3
    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0f0283    # com.twitter.android.R.string.message_delete_failed

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :pswitch_4
    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0f035a    # com.twitter.android.R.string.report_success

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :pswitch_5
    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->V()Landroid/widget/ListView;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/widget/PageableListView;

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/PageableListView;->a(Z)V

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    check-cast p4, Lcom/twitter/library/api/conversations/s;

    invoke-interface {p4}, Lcom/twitter/library/api/conversations/s;->f()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/twitter/library/widget/PageableListView;->setOnPageScrollListener(Lcom/twitter/library/widget/k;)V

    :cond_1
    iput-boolean v2, p0, Lcom/twitter/android/DMConversationFragment;->i:Z

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_4
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 7

    const/4 v3, -0x1

    const/4 v4, 0x1

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v2, p0, Lcom/twitter/android/DMConversationFragment;->g:Ljava/lang/String;

    if-eqz v2, :cond_0

    if-ne p3, v3, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v4, [Ljava/lang/String;

    const-string/jumbo v5, "messages:thread:thread::delete"

    aput-object v5, v4, v6

    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/DMConversationFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/twitter/library/api/conversations/af;

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/DMConversationFragment;->g:Ljava/lang/String;

    invoke-direct {v0, v2, v1, v3}, Lcom/twitter/library/api/conversations/af;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1, v6}, Lcom/twitter/android/DMConversationFragment;->a(Lcom/twitter/library/service/b;II)Z

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/twitter/android/DMConversationFragment;->f:Ljava/lang/String;

    if-eqz v2, :cond_1

    packed-switch p3, :pswitch_data_1

    :cond_1
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/DMConversationFragment;->f:Ljava/lang/String;

    goto :goto_0

    :pswitch_2
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v4, [Ljava/lang/String;

    const-string/jumbo v5, "messages:thread:message:spam:report_as_spam"

    aput-object v5, v4, v6

    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    new-instance v0, Lcom/twitter/library/api/conversations/aq;

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/DMConversationFragment;->f:Ljava/lang/String;

    const-string/jumbo v4, "spam"

    invoke-direct {v0, v2, v1, v3, v4}, Lcom/twitter/library/api/conversations/aq;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1, v6}, Lcom/twitter/android/DMConversationFragment;->a(Lcom/twitter/library/service/b;II)Z

    goto :goto_1

    :pswitch_3
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v4, [Ljava/lang/String;

    const-string/jumbo v5, "messages:thread:message:abusive:report_as_spam"

    aput-object v5, v4, v6

    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    new-instance v0, Lcom/twitter/library/api/conversations/aq;

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/DMConversationFragment;->f:Ljava/lang/String;

    const-string/jumbo v4, "abuse"

    invoke-direct {v0, v2, v1, v3, v4}, Lcom/twitter/library/api/conversations/aq;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1, v6}, Lcom/twitter/android/DMConversationFragment;->a(Lcom/twitter/library/service/b;II)Z

    const/4 v0, 0x4

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0261    # com.twitter.android.R.string.mark_as_abusive_follow_up

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0571    # com.twitter.android.R.string.yes

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f029d    # com.twitter.android.R.string.no

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {v0, p0, v6}, Lcom/twitter/android/widget/PromptDialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_1

    :pswitch_4
    if-ne p3, v3, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/WebViewActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const v1, 0x7f0f04fc    # com.twitter.android.R.string.twitter_abuse_help

    invoke-virtual {p0, v1}, Lcom/twitter/android/DMConversationFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/DMConversationFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseListFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/twitter/android/DMConversationFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v4}, Lcom/twitter/android/DMConversationFragment;->a_(Z)V

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->ap()V

    return-void

    :cond_0
    new-instance v0, Lcom/twitter/library/api/conversations/aj;

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/api/conversations/aj;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1, v4}, Lcom/twitter/android/DMConversationFragment;->a(Lcom/twitter/library/service/b;II)Z

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/DMConversationFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v1}, Landroid/support/v4/widget/CursorAdapter;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->smoothScrollToPosition(I)V

    goto :goto_0
.end method

.method protected a(Lcom/twitter/internal/android/widget/ToolBar;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->a(Lcom/twitter/internal/android/widget/ToolBar;)V

    iget-object v1, p0, Lcom/twitter/android/DMConversationFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    const/4 v0, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    const v1, 0x7f090324    # com.twitter.android.R.id.menu_messages_delete

    invoke-virtual {p1, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v1

    invoke-virtual {v1, v0}, Lhn;->b(Z)Lhn;

    return-void
.end method

.method public a(Lcom/twitter/library/api/MediaEntity;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/api/Promotion;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/api/TweetClassicCard;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/api/UrlEntity;)V
    .locals 9

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/twitter/android/DMConversationFragment;->h:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    const-string/jumbo v5, "messages:thread:::open_link"

    iget-object v7, p0, Lcom/twitter/android/DMConversationFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v2, p1

    move-object v6, v1

    move-object v8, v1

    invoke-static/range {v0 .. v8}, Lcom/twitter/android/client/be;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/UrlEntity;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseListFragment;->a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)V

    const v0, 0x7f110018    # com.twitter.android.R.menu.messages_thread

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/DMConversationFragment;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/twitter/android/DMConversationFragment;->g:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->P()Z

    :cond_0
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 0

    return-void
.end method

.method public a(Lhn;)Z
    .locals 2

    invoke-virtual {p1}, Lhn;->a()I

    move-result v0

    const v1, 0x7f090324    # com.twitter.android.R.id.menu_messages_delete

    if-ne v0, v1, :cond_0

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0285    # com.twitter.android.R.string.messages_delete_conversation

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0286    # com.twitter.android.R.string.messages_delete_conversation_question

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0571    # com.twitter.android.R.string.yes

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f029d    # com.twitter.android.R.string.no

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->a(Lhn;)Z

    move-result v0

    goto :goto_0
.end method

.method public a_(J)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/DMConversationFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public b(J)V
    .locals 0

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/twitter/android/DMConversationFragment;->h:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v3, :cond_0

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x40

    if-ne v0, v1, :cond_2

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "screen_name"

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/DMConversationFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x23

    if-ne v0, v1, :cond_3

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/SearchActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "query"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "q_source"

    const-string/jumbo v2, "hashtag_click"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "scribe_context"

    const-string/jumbo v2, "hashtag"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/DMConversationFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x24

    if-ne v0, v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/SearchActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "query"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "q_source"

    const-string/jumbo v2, "cashtag_click"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "scribe_context"

    const-string/jumbo v2, "cashtag"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/DMConversationFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method protected b(Landroid/database/Cursor;)Z
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()V
    .locals 0

    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->p()V

    new-instance v1, Lcom/twitter/android/dr;

    invoke-direct {v1, p0}, Lcom/twitter/android/dr;-><init>(Lcom/twitter/android/DMConversationFragment;)V

    iput-object v1, p0, Lcom/twitter/android/DMConversationFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/PageableListView;

    new-instance v2, Lcom/twitter/android/dl;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/dl;-><init>(Lcom/twitter/android/DMConversationFragment;Lcom/twitter/library/widget/PageableListView;)V

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/PageableListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    invoke-virtual {v0}, Lcom/twitter/library/widget/PageableListView;->a()V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/PageableListView;->a(Z)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/PageableListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v1, Lcom/twitter/android/dm;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/dm;-><init>(Lcom/twitter/android/DMConversationFragment;Lcom/twitter/library/widget/PageableListView;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/PageableListView;->setOnPageScrollListener(Lcom/twitter/library/widget/k;)V

    new-instance v0, Lcom/twitter/android/dq;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/dq;-><init>(Lcom/twitter/android/DMConversationFragment;Lcom/twitter/android/dl;)V

    iput-object v0, p0, Lcom/twitter/android/DMConversationFragment;->O:Lcom/twitter/library/client/j;

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v3, 0x1

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    sget-object v0, Lcom/twitter/android/DMConversationFragment;->b:Ljava/text/SimpleDateFormat;

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00ed    # com.twitter.android.R.string.datetime_format_time_only

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    sget-object v0, Lcom/twitter/android/DMConversationFragment;->c:Ljava/text/SimpleDateFormat;

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00ea    # com.twitter.android.R.string.datetime_format_day_time_only

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    sget-object v0, Lcom/twitter/android/DMConversationFragment;->d:Ljava/text/SimpleDateFormat;

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00ec    # com.twitter.android.R.string.datetime_format_long_friendly

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "conversation_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/DMConversationFragment;->g:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/twitter/android/DMConversationFragment;->l(Z)V

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "messages:thread:::impression"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 8

    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/provider/z;->a:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/android/DMConversationFragment;->a:[Ljava/lang/String;

    const-string/jumbo v4, "conversation_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/twitter/android/DMConversationFragment;->g:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/DMConversationFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onStart()V
    .locals 0

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onStart()V

    invoke-direct {p0}, Lcom/twitter/android/DMConversationFragment;->u()V

    return-void
.end method

.method public onStop()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/DMConversationFragment;->v()V

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onStop()V

    return-void
.end method
