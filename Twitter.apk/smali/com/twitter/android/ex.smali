.class Lcom/twitter/android/ex;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/ob;


# instance fields
.field final synthetic a:Lcom/twitter/android/DiscoverFragment;


# direct methods
.method public constructor <init>(Lcom/twitter/android/DiscoverFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/ex;->a:Lcom/twitter/android/DiscoverFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;Lcom/twitter/android/ew;Landroid/os/Bundle;)V
    .locals 10

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/twitter/android/ex;->a:Lcom/twitter/android/DiscoverFragment;

    invoke-static {v0}, Lcom/twitter/android/DiscoverFragment;->m(Lcom/twitter/android/DiscoverFragment;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    iget v0, p2, Lcom/twitter/android/ew;->d:I

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/ex;->a:Lcom/twitter/android/DiscoverFragment;

    iget-wide v3, p2, Lcom/twitter/android/ew;->a:J

    iget-wide v5, p2, Lcom/twitter/android/ew;->b:J

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/DiscoverFragment;->a(JJJ)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/ex;->a:Lcom/twitter/android/DiscoverFragment;

    iget-object v0, v0, Lcom/twitter/android/DiscoverFragment;->d:Ljava/util/ArrayList;

    iget-wide v1, p2, Lcom/twitter/android/ew;->a:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v0, p2, Lcom/twitter/android/ew;->c:I

    if-ne v0, v8, :cond_3

    iget-object v0, p2, Lcom/twitter/android/ew;->e:Lcom/twitter/library/provider/Tweet;

    iget-object v1, p0, Lcom/twitter/android/ex;->a:Lcom/twitter/android/DiscoverFragment;

    invoke-static {v1}, Lcom/twitter/android/DiscoverFragment;->n(Lcom/twitter/android/DiscoverFragment;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v1

    invoke-static {v7, v0, v1, v7}, Lcom/twitter/library/scribe/ScribeItem;->a(Landroid/content/Context;Lcom/twitter/library/provider/ParcelableTweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    :goto_0
    iget-object v1, p2, Lcom/twitter/android/ew;->e:Lcom/twitter/library/provider/Tweet;

    if-eqz v1, :cond_4

    iget-object v1, p2, Lcom/twitter/android/ew;->e:Lcom/twitter/library/provider/Tweet;

    iget-object v1, v1, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    :goto_1
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/twitter/android/ex;->a:Lcom/twitter/android/DiscoverFragment;

    invoke-static {v2}, Lcom/twitter/android/DiscoverFragment;->o(Lcom/twitter/android/DiscoverFragment;)Lcom/twitter/android/client/c;

    move-result-object v2

    sget-object v3, Lcom/twitter/library/api/PromotedEvent;->a:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {v2, v3, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V

    :cond_0
    move-object v7, v0

    :cond_1
    :goto_2
    if-eqz v7, :cond_2

    const-string/jumbo v0, "position"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, v7, Lcom/twitter/library/scribe/ScribeItem;->g:I

    iget-object v0, p0, Lcom/twitter/android/ex;->a:Lcom/twitter/android/DiscoverFragment;

    iget-object v0, v0, Lcom/twitter/android/DiscoverFragment;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    return-void

    :cond_3
    iget v0, p2, Lcom/twitter/android/ew;->c:I

    if-ne v0, v9, :cond_6

    iget-wide v0, p2, Lcom/twitter/android/ew;->f:J

    iget-object v2, p2, Lcom/twitter/android/ew;->g:Lcom/twitter/library/api/PromotedContent;

    iget-object v3, p2, Lcom/twitter/android/ew;->h:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v7}, Lcom/twitter/library/scribe/ScribeItem;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    goto :goto_0

    :cond_4
    iget-object v1, p2, Lcom/twitter/android/ew;->g:Lcom/twitter/library/api/PromotedContent;

    goto :goto_1

    :cond_5
    iget v0, p2, Lcom/twitter/android/ew;->c:I

    if-ne v0, v9, :cond_1

    iget v0, p2, Lcom/twitter/android/ew;->d:I

    if-ne v0, v8, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ex;->a:Lcom/twitter/android/DiscoverFragment;

    iget-wide v3, p2, Lcom/twitter/android/ew;->a:J

    iget-wide v5, p2, Lcom/twitter/android/ew;->b:J

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/DiscoverFragment;->a(JJJ)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-wide v0, p2, Lcom/twitter/android/ew;->f:J

    iget-object v2, p2, Lcom/twitter/android/ew;->g:Lcom/twitter/library/api/PromotedContent;

    iget-object v3, p2, Lcom/twitter/android/ew;->h:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v7}, Lcom/twitter/library/scribe/ScribeItem;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v7

    goto :goto_2

    :cond_6
    move-object v0, v7

    goto :goto_0
.end method

.method public bridge synthetic a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V
    .locals 0

    check-cast p2, Lcom/twitter/android/ew;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/ex;->a(Landroid/view/View;Lcom/twitter/android/ew;Landroid/os/Bundle;)V

    return-void
.end method
