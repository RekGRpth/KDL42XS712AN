.class public Lcom/twitter/android/MessagesEmptyFragment;
.super Landroid/support/v4/app/Fragment;
.source "Twttr"


# instance fields
.field a:Lcom/twitter/android/mm;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    instance-of v0, p1, Lcom/twitter/android/mm;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/twitter/android/mm;

    iput-object p1, p0, Lcom/twitter/android/MessagesEmptyFragment;->a:Lcom/twitter/android/mm;

    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    const v0, 0x7f03007a    # com.twitter.android.R.layout.fragment_messages_empty

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f090186    # com.twitter.android.R.id.new_message_btn

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/ml;

    invoke-direct {v2, p0}, Lcom/twitter/android/ml;-><init>(Lcom/twitter/android/MessagesEmptyFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method
