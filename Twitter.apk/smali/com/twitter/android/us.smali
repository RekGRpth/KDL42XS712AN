.class Lcom/twitter/android/us;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Lcom/twitter/internal/android/widget/GroupedRowView;

.field public final b:Landroid/widget/ImageView;

.field public final c:Landroid/widget/TextView;

.field public final d:Landroid/widget/TextView;

.field public final e:Landroid/widget/TextView;

.field public f:Lcom/twitter/android/widget/TopicView$TopicData;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v0, p1

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    iput-object v0, p0, Lcom/twitter/android/us;->a:Lcom/twitter/internal/android/widget/GroupedRowView;

    const v0, 0x7f09001f    # com.twitter.android.R.id.icon

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/us;->b:Landroid/widget/ImageView;

    const v0, 0x7f09009d    # com.twitter.android.R.id.text_item

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/us;->c:Landroid/widget/TextView;

    const v0, 0x7f09004d    # com.twitter.android.R.id.promoted

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/us;->d:Landroid/widget/TextView;

    const v0, 0x7f090291    # com.twitter.android.R.id.sub_title

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/us;->e:Landroid/widget/TextView;

    return-void
.end method
