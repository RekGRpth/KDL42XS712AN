.class Lcom/twitter/android/mv;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/ce;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/twitter/library/api/TweetEntities;

.field final synthetic c:J

.field final synthetic d:Lcom/twitter/android/MessagesThreadFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/MessagesThreadFragment;Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;J)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/mv;->d:Lcom/twitter/android/MessagesThreadFragment;

    iput-object p2, p0, Lcom/twitter/android/mv;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/twitter/android/mv;->b:Lcom/twitter/library/api/TweetEntities;

    iput-wide p4, p0, Lcom/twitter/android/mv;->c:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/DialogInterface;II)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    packed-switch p3, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/mv;->d:Lcom/twitter/android/MessagesThreadFragment;

    iget-object v1, p0, Lcom/twitter/android/mv;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/mv;->b:Lcom/twitter/library/api/TweetEntities;

    invoke-static {v0, v1, v2}, Lcom/twitter/android/MessagesThreadFragment;->a(Lcom/twitter/android/MessagesThreadFragment;Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/mv;->d:Lcom/twitter/android/MessagesThreadFragment;

    invoke-static {v0}, Lcom/twitter/android/MessagesThreadFragment;->d(Lcom/twitter/android/MessagesThreadFragment;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/mv;->d:Lcom/twitter/android/MessagesThreadFragment;

    invoke-static {v1}, Lcom/twitter/android/MessagesThreadFragment;->e(Lcom/twitter/android/MessagesThreadFragment;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v7, [Ljava/lang/String;

    const-string/jumbo v5, "messages:thread:message::delete"

    aput-object v5, v4, v6

    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    new-instance v2, Liv;

    iget-object v3, p0, Lcom/twitter/android/mv;->d:Lcom/twitter/android/MessagesThreadFragment;

    invoke-virtual {v3}, Lcom/twitter/android/MessagesThreadFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Liv;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    iget-wide v3, p0, Lcom/twitter/android/mv;->c:J

    invoke-virtual {v2, v3, v4, v6}, Liv;->a(JZ)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/b;->c(I)Lcom/twitter/library/service/b;

    iget-object v0, p0, Lcom/twitter/android/mv;->d:Lcom/twitter/android/MessagesThreadFragment;

    invoke-static {v0, v2, v7, v6}, Lcom/twitter/android/MessagesThreadFragment;->a(Lcom/twitter/android/MessagesThreadFragment;Lcom/twitter/library/service/b;II)Z

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/mv;->d:Lcom/twitter/android/MessagesThreadFragment;

    iget-wide v1, p0, Lcom/twitter/android/mv;->c:J

    invoke-static {v0, v1, v2, v6}, Lcom/twitter/android/MessagesThreadFragment;->a(Lcom/twitter/android/MessagesThreadFragment;JZ)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
