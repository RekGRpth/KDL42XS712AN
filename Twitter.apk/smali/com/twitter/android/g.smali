.class Lcom/twitter/android/g;
.super Landroid/widget/ArrayAdapter;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/AccountsDialogActivity;


# direct methods
.method public constructor <init>(Lcom/twitter/android/AccountsDialogActivity;Landroid/content/Context;[Lcom/twitter/android/UserAccount;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/g;->a:Lcom/twitter/android/AccountsDialogActivity;

    const/4 v0, 0x0

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    const/4 v7, 0x0

    if-nez p2, :cond_1

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030003    # com.twitter.android.R.layout.account_row_view

    invoke-virtual {v0, v1, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f090068    # com.twitter.android.R.id.checkmark

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v3, v0

    :goto_0
    move-object v0, v1

    check-cast v0, Lcom/twitter/library/widget/UserView;

    invoke-virtual {p0, p1}, Lcom/twitter/android/g;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/UserAccount;

    iget-object v5, v2, Lcom/twitter/android/UserAccount;->b:Lcom/twitter/library/api/TwitterUser;

    iget-object v4, v5, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    if-eqz v4, :cond_2

    iget-object v6, p0, Lcom/twitter/android/g;->a:Lcom/twitter/android/AccountsDialogActivity;

    iget-object v6, v6, Lcom/twitter/android/AccountsDialogActivity;->a:Lcom/twitter/android/client/c;

    invoke-virtual {v6, v4}, Lcom/twitter/android/client/c;->g(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/twitter/library/widget/UserView;->setUserImage(Landroid/graphics/Bitmap;)V

    :goto_1
    iget-object v4, v5, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    iget-object v6, v5, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    invoke-virtual {v0, v4, v6}, Lcom/twitter/library/widget/UserView;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v4, v5, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    invoke-virtual {v0, v4}, Lcom/twitter/library/widget/UserView;->setProtected(Z)V

    iget-boolean v4, v5, Lcom/twitter/library/api/TwitterUser;->verified:Z

    invoke-virtual {v0, v4}, Lcom/twitter/library/widget/UserView;->setVerified(Z)V

    const-string/jumbo v4, ""

    iget-object v6, v5, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, v5, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, ". "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, "@"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v5, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v2, v2, Lcom/twitter/android/UserAccount;->a:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/android/g;->a:Lcom/twitter/android/AccountsDialogActivity;

    iget-object v5, v5, Lcom/twitter/android/AccountsDialogActivity;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/g;->getContext()Landroid/content/Context;

    move-result-object v3

    const v5, 0x7f0f03c4    # com.twitter.android.R.string.selected_status

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ". "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_2
    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/UserView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-object v1

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    move-object v3, v0

    move-object v1, p2

    goto/16 :goto_0

    :cond_2
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/twitter/library/widget/UserView;->setUserImage(Landroid/graphics/Bitmap;)V

    goto/16 :goto_1

    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    move-object v2, v4

    goto :goto_2
.end method
