.class public Lcom/twitter/android/vc;
.super Lcom/twitter/android/yb;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/android/vn;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Lcom/twitter/android/vn;)V
    .locals 9

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    move-object v5, p3

    move-object v6, v3

    move-object v7, v3

    move-object v8, v3

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/yb;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object p4, p0, Lcom/twitter/android/vc;->a:Lcom/twitter/android/vn;

    return-void
.end method

.method private a(Lcom/twitter/library/provider/Tweet;)Z
    .locals 1

    if-eqz p1, :cond_0

    iget v0, p1, Lcom/twitter/library/provider/Tweet;->Z:I

    invoke-static {v0}, Lcom/twitter/library/provider/au;->g(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/android/uk;
    .locals 2

    invoke-virtual {p1}, Lcom/twitter/library/widget/TweetView;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/twitter/android/uk;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/twitter/android/uk;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/widget/TweetView;)V
    .locals 7

    const/4 v4, 0x0

    invoke-direct {p0, p1}, Lcom/twitter/android/vc;->a(Lcom/twitter/library/provider/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p2}, Lcom/twitter/android/vc;->c(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/android/uk;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/android/uk;->a:Lcom/twitter/library/api/TimelineScribeContent;

    const-string/jumbo v1, "discover_module"

    iget-wide v2, p1, Lcom/twitter/library/provider/Tweet;->u:J

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/scribe/ScribeItem;->a(Lcom/twitter/library/api/TimelineScribeContent;Ljava/lang/String;JILjava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/vc;->c:Lcom/twitter/android/client/c;

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v3, p0, Lcom/twitter/android/vc;->d:Lcom/twitter/library/client/aa;

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    invoke-direct {v2, v5, v6}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/twitter/android/vc;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v6}, Lcom/twitter/library/scribe/ScribeAssociation;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "::discover_module:platform_photo_card:click"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(JJLjava/lang/String;Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/widget/TweetView;)V
    .locals 7

    const/4 v4, 0x0

    invoke-super/range {p0 .. p7}, Lcom/twitter/android/yb;->a(JJLjava/lang/String;Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/widget/TweetView;)V

    if-nez p7, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p7}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/library/provider/Tweet;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/twitter/android/vc;->a(Lcom/twitter/library/provider/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p7}, Lcom/twitter/android/vc;->c(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/android/uk;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/android/uk;->a:Lcom/twitter/library/api/TimelineScribeContent;

    const-string/jumbo v1, "discover_module"

    iget-wide v2, v2, Lcom/twitter/library/provider/Tweet;->u:J

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/scribe/ScribeItem;->a(Lcom/twitter/library/api/TimelineScribeContent;Ljava/lang/String;JILjava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/vc;->c:Lcom/twitter/android/client/c;

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v3, p0, Lcom/twitter/android/vc;->d:Lcom/twitter/library/client/aa;

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    invoke-direct {v2, v5, v6}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/twitter/android/vc;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v6}, Lcom/twitter/library/scribe/ScribeAssociation;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "::discover_module:avatar:click"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/MediaEntity;Lcom/twitter/library/widget/TweetView;)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/yb;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/MediaEntity;Lcom/twitter/library/widget/TweetView;)V

    if-eqz p3, :cond_0

    invoke-direct {p0, p1, p3}, Lcom/twitter/android/vc;->c(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/widget/TweetView;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/TweetClassicCard;Lcom/twitter/library/widget/TweetView;)V
    .locals 2

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/yb;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/TweetClassicCard;Lcom/twitter/library/widget/TweetView;)V

    if-eqz p3, :cond_0

    const/4 v0, 0x1

    iget v1, p2, Lcom/twitter/library/api/TweetClassicCard;->type:I

    if-ne v0, v1, :cond_0

    invoke-direct {p0, p1, p3}, Lcom/twitter/android/vc;->c(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/widget/TweetView;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/widget/TweetView;)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/vc;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/support/v4/app/Fragment;

    if-eqz v2, :cond_0

    invoke-direct {p0, p1}, Lcom/twitter/android/vc;->a(Lcom/twitter/library/provider/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/twitter/library/widget/TweetView;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/android/ul;

    iget-object v0, p0, Lcom/twitter/android/vc;->c:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/vc;->d:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/vc;->a:Lcom/twitter/android/vn;

    iget-object v5, p0, Lcom/twitter/android/vc;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/TimelineFragment;->a(Lcom/twitter/android/client/c;Lcom/twitter/library/client/aa;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/android/vn;Lcom/twitter/android/ul;Lcom/twitter/library/scribe/ScribeAssociation;)V

    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/widget/TweetView;)V
    .locals 8

    const/4 v4, 0x0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/yb;->b(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/widget/TweetView;)V

    iget-object v0, p0, Lcom/twitter/android/vc;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/twitter/android/vc;->a(Lcom/twitter/library/provider/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p2}, Lcom/twitter/android/vc;->c(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/android/uk;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/android/uk;->a:Lcom/twitter/library/api/TimelineScribeContent;

    iget-object v6, p0, Lcom/twitter/android/vc;->c:Lcom/twitter/android/client/c;

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v2, p0, Lcom/twitter/android/vc;->p:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/twitter/android/vc;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v5}, Lcom/twitter/library/scribe/ScribeAssociation;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, "::discover_module:more:click"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v7

    const-string/jumbo v1, "discover_module"

    iget-object v2, v0, Lcom/twitter/library/api/TimelineScribeContent;->id:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/scribe/ScribeItem;->a(Lcom/twitter/library/api/TimelineScribeContent;Ljava/lang/String;JILjava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/widget/TweetView;)V
    .locals 9

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/yb;->b(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/widget/TweetView;)V

    invoke-virtual {p2}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/library/provider/Tweet;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/twitter/android/vc;->a(Lcom/twitter/library/provider/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/library/view/TweetActionType;->b:Lcom/twitter/library/view/TweetActionType;

    if-ne v0, p1, :cond_2

    iget-boolean v0, v2, Lcom/twitter/library/provider/Tweet;->l:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "unfavorite"

    :goto_0
    move-object v6, v0

    :goto_1
    invoke-direct {p0, p2}, Lcom/twitter/android/vc;->c(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/android/uk;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/android/uk;->a:Lcom/twitter/library/api/TimelineScribeContent;

    const-string/jumbo v1, "discover_module"

    iget-wide v2, v2, Lcom/twitter/library/provider/Tweet;->u:J

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/scribe/ScribeItem;->a(Lcom/twitter/library/api/TimelineScribeContent;Ljava/lang/String;JILjava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/vc;->c:Lcom/twitter/android/client/c;

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v3, p0, Lcom/twitter/android/vc;->d:Lcom/twitter/library/client/aa;

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v7

    invoke-direct {v2, v7, v8}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    iget-object v7, p0, Lcom/twitter/android/vc;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v7}, Lcom/twitter/library/scribe/ScribeAssociation;->b()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v4

    const/4 v4, 0x1

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string/jumbo v5, "discover_module"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string/jumbo v5, "tweet"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    aput-object v6, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_0
    return-void

    :cond_1
    const-string/jumbo v0, "favorite"

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/twitter/library/view/TweetActionType;->d:Lcom/twitter/library/view/TweetActionType;

    if-ne v0, p1, :cond_3

    const-string/jumbo v0, "reply"

    move-object v6, v0

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/twitter/library/view/TweetActionType;->c:Lcom/twitter/library/view/TweetActionType;

    if-ne v0, p1, :cond_5

    iget-boolean v0, v2, Lcom/twitter/library/provider/Tweet;->r:Z

    if-eqz v0, :cond_4

    const-string/jumbo v0, "unretweet"

    :goto_2
    move-object v6, v0

    goto :goto_1

    :cond_4
    const-string/jumbo v0, "retweet"

    goto :goto_2

    :cond_5
    const-string/jumbo v0, "unknown"

    move-object v6, v0

    goto :goto_1
.end method
