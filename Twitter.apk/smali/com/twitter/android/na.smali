.class Lcom/twitter/android/na;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/library/api/MediaEntity;

.field final synthetic b:Lcom/twitter/android/my;


# direct methods
.method constructor <init>(Lcom/twitter/android/my;Lcom/twitter/library/api/MediaEntity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/na;->b:Lcom/twitter/android/my;

    iput-object p2, p0, Lcom/twitter/android/na;->a:Lcom/twitter/library/api/MediaEntity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/na;->b:Lcom/twitter/android/my;

    iget-object v1, v1, Lcom/twitter/android/my;->a:Lcom/twitter/android/MessagesThreadFragment;

    invoke-virtual {v1}, Lcom/twitter/android/MessagesThreadFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/GalleryActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "dm"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "media"

    iget-object v2, p0, Lcom/twitter/android/na;->a:Lcom/twitter/library/api/MediaEntity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    iget-object v2, p0, Lcom/twitter/android/na;->b:Lcom/twitter/android/my;

    iget-object v2, v2, Lcom/twitter/android/my;->a:Lcom/twitter/android/MessagesThreadFragment;

    invoke-static {v2}, Lcom/twitter/android/MessagesThreadFragment;->l(Lcom/twitter/android/MessagesThreadFragment;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/na;->b:Lcom/twitter/android/my;

    iget-object v1, v1, Lcom/twitter/android/my;->a:Lcom/twitter/android/MessagesThreadFragment;

    invoke-virtual {v1, v0}, Lcom/twitter/android/MessagesThreadFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
