.class public Lcom/twitter/android/CollectionPermalinkFragment;
.super Lcom/twitter/android/TimelineFragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/ce;


# instance fields
.field private a:Lcom/twitter/android/widget/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/TimelineFragment;-><init>()V

    return-void
.end method

.method private a(Lcom/twitter/android/widget/e;)V
    .locals 7

    const/4 v2, 0x0

    iget-boolean v0, p1, Lcom/twitter/android/widget/e;->j:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x66

    :goto_0
    iget-boolean v1, p1, Lcom/twitter/android/widget/e;->j:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    new-instance v3, Ljk;

    invoke-virtual {p0}, Lcom/twitter/android/CollectionPermalinkFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {p0}, Lcom/twitter/android/CollectionPermalinkFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/CollectionPermalinkFragment;->a:Lcom/twitter/android/widget/e;

    iget-object v6, v6, Lcom/twitter/android/widget/e;->c:Ljava/lang/String;

    invoke-direct {v3, v4, v5, v6}, Ljk;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljk;->c(I)Lcom/twitter/library/service/b;

    invoke-virtual {p0, v3, v0, v2}, Lcom/twitter/android/CollectionPermalinkFragment;->a(Lcom/twitter/library/service/b;II)Z

    return-void

    :cond_0
    const/16 v0, 0x65

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method


# virtual methods
.method public a(Landroid/content/Context;IILcom/twitter/library/service/b;)V
    .locals 4

    const/4 v2, 0x1

    invoke-virtual {p4}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    packed-switch p2, :pswitch_data_0

    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/android/TimelineFragment;->a(Landroid/content/Context;IILcom/twitter/library/service/b;)V

    const/4 v1, 0x3

    if-ne p3, v1, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/CollectionPermalinkFragment;->U()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/CollectionPermalinkFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p4, Lcom/twitter/library/service/b;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/twitter/android/CollectionPermalinkFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-nez v0, :cond_1

    const v0, 0x7f0f009d    # com.twitter.android.R.string.collections_delete_fail

    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/CollectionPermalinkFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    :pswitch_1
    iget-object v1, p4, Lcom/twitter/library/service/b;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/twitter/android/CollectionPermalinkFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-nez v0, :cond_3

    const/16 v0, 0x65

    if-ne p2, v0, :cond_2

    const v0, 0x7f0f00a4    # com.twitter.android.R.string.collections_follow_fail

    :goto_1
    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    const v0, 0x7f0f00a9    # com.twitter.android.R.string.collections_unfollow_fail

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/CollectionPermalinkFragment;->a:Lcom/twitter/android/widget/e;

    invoke-virtual {p0}, Lcom/twitter/android/CollectionPermalinkFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/CollectionPermalinkFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/widget/e;->a(Landroid/content/Context;J)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    if-ne p2, v6, :cond_0

    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/CollectionPermalinkFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    new-instance v1, Ljh;

    invoke-virtual {p0}, Lcom/twitter/android/CollectionPermalinkFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/CollectionPermalinkFragment;->a:Lcom/twitter/android/widget/e;

    iget-object v3, v3, Lcom/twitter/android/widget/e;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v0, v3}, Ljh;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    const/16 v2, 0x64

    invoke-virtual {p0, v1, v2, v5}, Lcom/twitter/android/CollectionPermalinkFragment;->a(Lcom/twitter/library/service/b;II)Z

    invoke-virtual {p0}, Lcom/twitter/android/CollectionPermalinkFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/CollectionPermalinkFragment;->l()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v5

    aput-object v7, v0, v6

    const/4 v4, 0x2

    aput-object v7, v0, v4

    const/4 v4, 0x3

    const-string/jumbo v5, "custom_timeline"

    aput-object v5, v0, v4

    const/4 v4, 0x4

    const-string/jumbo v5, "delete"

    aput-object v5, v0, v4

    invoke-virtual {v1, v2, v3, v0}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 4

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/TimelineFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/CollectionPermalinkFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/CollectionPermalinkFragment;->a:Lcom/twitter/android/widget/e;

    invoke-virtual {v1, v0, p2}, Lcom/twitter/android/widget/e;->a(Landroid/content/Context;Landroid/database/Cursor;)V

    invoke-virtual {p0}, Lcom/twitter/android/CollectionPermalinkFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/twitter/android/widget/e;->a(Landroid/content/Context;J)V

    invoke-virtual {p0}, Lcom/twitter/android/CollectionPermalinkFragment;->ao()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    iget-object v1, v1, Lcom/twitter/android/widget/e;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/twitter/android/CollectionPermalinkFragment;->ap()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Lcom/twitter/internal/android/widget/ToolBar;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/TimelineFragment;->a(Lcom/twitter/internal/android/widget/ToolBar;)V

    const v0, 0x7f09032f    # com.twitter.android.R.id.menu_share

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v3

    iget-object v0, p0, Lcom/twitter/android/CollectionPermalinkFragment;->a:Lcom/twitter/android/widget/e;

    iget-object v0, v0, Lcom/twitter/android/widget/e;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lhn;->b(Z)Lhn;

    iget-wide v3, p0, Lcom/twitter/android/CollectionPermalinkFragment;->Q:J

    iget-object v0, p0, Lcom/twitter/android/CollectionPermalinkFragment;->a:Lcom/twitter/android/widget/e;

    iget-wide v5, v0, Lcom/twitter/android/widget/e;->f:J

    cmp-long v0, v3, v5

    if-nez v0, :cond_3

    :goto_1
    invoke-static {}, Lgj;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f09032e    # com.twitter.android.R.id.menu_edit

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    invoke-virtual {v0, v1}, Lhn;->b(Z)Lhn;

    :cond_0
    invoke-static {}, Lgj;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f09032d    # com.twitter.android.R.id.menu_delete

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    invoke-virtual {v0, v1}, Lhn;->b(Z)Lhn;

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method protected a(Lcom/twitter/refresh/widget/a;Z)V
    .locals 0

    return-void
.end method

.method protected a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/twitter/android/TimelineFragment;->a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)V

    const v0, 0x7f110023    # com.twitter.android.R.menu.toolbar_share

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    invoke-static {}, Lgj;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f110021    # com.twitter.android.R.menu.toolbar_edit

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    :cond_0
    invoke-static {}, Lgj;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f110020    # com.twitter.android.R.menu.toolbar_delete

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    :cond_1
    return-void
.end method

.method public a(Lhn;)Z
    .locals 7

    const/4 v6, 0x1

    iget-object v5, p0, Lcom/twitter/android/CollectionPermalinkFragment;->a:Lcom/twitter/android/widget/e;

    invoke-virtual {p1}, Lhn;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/twitter/android/TimelineFragment;->a(Lhn;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/android/CollectionPermalinkFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, v5, Lcom/twitter/android/widget/e;->g:Ljava/lang/String;

    iget-object v2, v5, Lcom/twitter/android/widget/e;->h:Ljava/lang/String;

    iget-object v3, v5, Lcom/twitter/android/widget/e;->d:Ljava/lang/String;

    iget-object v4, v5, Lcom/twitter/android/widget/e;->e:Ljava/lang/String;

    iget-object v5, v5, Lcom/twitter/android/widget/e;->i:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v6

    goto :goto_0

    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/CollectionPermalinkFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/CollectionCreateEditActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "timeline_id"

    iget-object v2, v5, Lcom/twitter/android/widget/e;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "timeline_name"

    iget-object v2, v5, Lcom/twitter/android/widget/e;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "timeline_description"

    iget-object v2, v5, Lcom/twitter/android/widget/e;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/CollectionPermalinkFragment;->startActivity(Landroid/content/Intent;)V

    move v0, v6

    goto :goto_0

    :pswitch_2
    invoke-static {v6}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f009e    # com.twitter.android.R.string.collections_delete_question

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0571    # com.twitter.android.R.string.yes

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0089    # com.twitter.android.R.string.cancel

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0328    # com.twitter.android.R.string.profile_tab_title_collections_owned_by

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/android/widget/ce;)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/CollectionPermalinkFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    move v0, v6

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f09032d
        :pswitch_2    # com.twitter.android.R.id.menu_delete
        :pswitch_1    # com.twitter.android.R.id.menu_edit
        :pswitch_0    # com.twitter.android.R.id.menu_share
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-super {p0, p1}, Lcom/twitter/android/TimelineFragment;->onClick(Landroid/view/View;)V

    :goto_0
    return-void

    :sswitch_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/CollectionPermalinkFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "user_id"

    iget-object v2, p0, Lcom/twitter/android/CollectionPermalinkFragment;->a:Lcom/twitter/android/widget/e;

    iget-wide v2, v2, Lcom/twitter/android/widget/e;->f:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/CollectionPermalinkFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/twitter/android/CollectionPermalinkFragment;->a:Lcom/twitter/android/widget/e;

    invoke-direct {p0, v0}, Lcom/twitter/android/CollectionPermalinkFragment;->a(Lcom/twitter/android/widget/e;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f09002c -> :sswitch_1    # com.twitter.android.R.id.action_button
        0x7f0900dc -> :sswitch_0    # com.twitter.android.R.id.owner_byline
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/CollectionPermalinkFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "empty_title"

    const v2, 0x7f0f00a2    # com.twitter.android.R.string.collections_empty_collection

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v1, "empty_desc"

    const v2, 0x7f0f00a8    # com.twitter.android.R.string.collections_no_tweets

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/TimelineFragment;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/CollectionPermalinkFragment;->l(Z)V

    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 8

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/TimelineFragment;->onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/twitter/android/CollectionPermalinkFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/provider/ai;->a:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/twitter/android/CollectionPermalinkFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/library/provider/cf;->a:[Ljava/lang/String;

    const-string/jumbo v4, "ev_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/twitter/android/CollectionPermalinkFragment;->e:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/CollectionPermalinkFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 1

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/twitter/android/TimelineFragment;->onLoaderReset(Landroid/support/v4/content/Loader;)V

    :pswitch_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/CollectionPermalinkFragment;->b(ILcom/twitter/library/util/ar;)V

    invoke-super {p0}, Lcom/twitter/android/TimelineFragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/TimelineFragment;->onResume()V

    const/4 v0, 0x2

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/CollectionPermalinkFragment;->a(ILcom/twitter/library/util/ar;)V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1, p2}, Lcom/twitter/android/TimelineFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/CollectionPermalinkFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030030    # com.twitter.android.R.layout.collection_header

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/CollectionPermalinkFragment;->V()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    new-instance v1, Lcom/twitter/android/widget/e;

    invoke-direct {v1, v0}, Lcom/twitter/android/widget/e;-><init>(Landroid/view/View;)V

    iget-object v0, v1, Lcom/twitter/android/widget/e;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, v1, Lcom/twitter/android/widget/e;->b:Lcom/twitter/internal/android/widget/ShadowTextView;

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/ShadowTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object v1, p0, Lcom/twitter/android/CollectionPermalinkFragment;->a:Lcom/twitter/android/widget/e;

    return-void
.end method

.method protected p()V
    .locals 3

    invoke-super {p0}, Lcom/twitter/android/TimelineFragment;->p()V

    invoke-virtual {p0}, Lcom/twitter/android/CollectionPermalinkFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method

.method protected q()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/CollectionPermalinkFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/CollectionPermalinkFragment;->f:Z

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected r()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/CollectionPermalinkFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method
