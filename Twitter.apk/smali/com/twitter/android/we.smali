.class Lcom/twitter/android/we;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:J

.field final synthetic b:Landroid/content/Intent;

.field final synthetic c:Lcom/twitter/android/TweetActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/TweetActivity;JLandroid/content/Intent;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/we;->c:Lcom/twitter/android/TweetActivity;

    iput-wide p2, p0, Lcom/twitter/android/we;->a:J

    iput-object p4, p0, Lcom/twitter/android/we;->b:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    const/4 v0, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/twitter/android/we;->c:Lcom/twitter/android/TweetActivity;

    iget v1, v1, Lcom/twitter/android/TweetActivity;->g:I

    packed-switch v1, :pswitch_data_1

    :goto_1
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/we;->c:Lcom/twitter/android/TweetActivity;

    invoke-static {v1}, Lcom/twitter/android/TweetActivity;->h(Lcom/twitter/android/TweetActivity;)Lcom/twitter/android/client/c;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/we;->c:Lcom/twitter/android/TweetActivity;

    invoke-static {v2}, Lcom/twitter/android/TweetActivity;->g(Lcom/twitter/android/TweetActivity;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v5, [Ljava/lang/String;

    aput-object v0, v4, v6

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/we;->c:Lcom/twitter/android/TweetActivity;

    invoke-virtual {v0}, Lcom/twitter/android/TweetActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/we;->c:Lcom/twitter/android/TweetActivity;

    invoke-static {v1}, Lcom/twitter/android/TweetActivity;->f(Lcom/twitter/android/TweetActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lgr;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/we;->c:Lcom/twitter/android/TweetActivity;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/twitter/android/we;->c:Lcom/twitter/android/TweetActivity;

    const-class v4, Lcom/twitter/android/TweetSettingsActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "account_name"

    iget-object v4, p0, Lcom/twitter/android/we;->c:Lcom/twitter/android/TweetActivity;

    invoke-static {v4}, Lcom/twitter/android/TweetActivity;->f(Lcom/twitter/android/TweetActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "enabled"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "from_notification_landing"

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/TweetActivity;->startActivity(Landroid/content/Intent;)V

    const-string/jumbo v0, "tweet:notification_landing:tweet:settings:click"

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/we;->c:Lcom/twitter/android/TweetActivity;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetActivity;->showDialog(I)V

    const-string/jumbo v0, "tweet:notification_landing:favorite:settings:click"

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/twitter/android/we;->c:Lcom/twitter/android/TweetActivity;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetActivity;->showDialog(I)V

    const-string/jumbo v0, "tweet:notification_landing:mention:settings:click"

    goto :goto_1

    :pswitch_4
    iget-object v0, p0, Lcom/twitter/android/we;->c:Lcom/twitter/android/TweetActivity;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetActivity;->showDialog(I)V

    const-string/jumbo v0, "tweet:notification_landing:retweet:settings:click"

    goto :goto_1

    :pswitch_5
    iget-wide v1, p0, Lcom/twitter/android/we;->a:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/we;->c:Lcom/twitter/android/TweetActivity;

    iget v1, v1, Lcom/twitter/android/TweetActivity;->g:I

    packed-switch v1, :pswitch_data_2

    :goto_2
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/twitter/android/we;->c:Lcom/twitter/android/TweetActivity;

    invoke-static {v1}, Lcom/twitter/android/TweetActivity;->j(Lcom/twitter/android/TweetActivity;)Lcom/twitter/android/client/c;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/we;->c:Lcom/twitter/android/TweetActivity;

    invoke-static {v2}, Lcom/twitter/android/TweetActivity;->i(Lcom/twitter/android/TweetActivity;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v5, [Ljava/lang/String;

    aput-object v0, v4, v6

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/we;->c:Lcom/twitter/android/TweetActivity;

    iget-object v1, p0, Lcom/twitter/android/we;->b:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetActivity;->c(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/twitter/android/we;->c:Lcom/twitter/android/TweetActivity;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/we;->c:Lcom/twitter/android/TweetActivity;

    const-class v3, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "user_id"

    iget-wide v3, p0, Lcom/twitter/android/we;->a:J

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_6
    const-string/jumbo v0, "tweet:notification_landing:favorite:header:click"

    goto :goto_2

    :pswitch_7
    const-string/jumbo v0, "tweet:notification_landing:mention:header:click"

    goto :goto_2

    :pswitch_8
    const-string/jumbo v0, "tweet:notification_landing:retweet:header:click"

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x7f090293
        :pswitch_0    # com.twitter.android.R.id.notification_setting_button
        :pswitch_5    # com.twitter.android.R.id.notification_reason_icon
        :pswitch_5    # com.twitter.android.R.id.notification_reason_text
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_8
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
