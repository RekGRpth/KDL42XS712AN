.class Lcom/twitter/android/fs;
.super Landroid/support/v4/app/FragmentPagerAdapter;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/EventSearchActivity;

.field private final b:Landroid/support/v4/view/ViewPager;

.field private final c:Ljava/util/ArrayList;

.field private d:I


# direct methods
.method public constructor <init>(Lcom/twitter/android/EventSearchActivity;Landroid/support/v4/app/FragmentActivity;Ljava/util/ArrayList;Landroid/support/v4/view/ViewPager;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/fs;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-virtual {p2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v4/app/FragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/fs;->d:I

    iput-object p4, p0, Lcom/twitter/android/fs;->b:Landroid/support/v4/view/ViewPager;

    iget-object v0, p0, Lcom/twitter/android/fs;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    iput-object p3, p0, Lcom/twitter/android/fs;->c:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/fs;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/fs;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iget-object v1, p0, Lcom/twitter/android/fs;->a:Lcom/twitter/android/EventSearchActivity;

    iget-object v2, v0, Lhb;->a:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Lhb;->b:Landroid/os/Bundle;

    invoke-static {v1, v2, v0}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/fs;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iget v0, v0, Lhb;->f:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/fs;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iget-object v2, p0, Lcom/twitter/android/fs;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-virtual {v2}, Lcom/twitter/android/EventSearchActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v0, v2}, Lhb;->a(Landroid/support/v4/app/FragmentManager;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-ne v2, p1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/fs;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    :goto_0
    return v0

    :cond_1
    const/4 v0, -0x2

    goto :goto_0
.end method

.method public getPageTitle(I)Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/fs;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iget-object v0, v0, Lhb;->d:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 5

    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentPagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/BaseListFragment;

    iget-object v1, p0, Lcom/twitter/android/fs;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v1, v0}, Lcom/twitter/android/EventSearchActivity;->a(Lcom/twitter/android/EventSearchActivity;Landroid/support/v4/app/Fragment;)V

    move-object v1, v0

    check-cast v1, Lcom/twitter/android/SearchFragment;

    new-instance v3, Lcom/twitter/android/ft;

    iget-object v4, p0, Lcom/twitter/android/fs;->a:Lcom/twitter/android/EventSearchActivity;

    iget-object v2, p0, Lcom/twitter/android/fs;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhb;

    iget-object v2, v2, Lhb;->c:Landroid/net/Uri;

    invoke-direct {v3, v4, v2}, Lcom/twitter/android/ft;-><init>(Lcom/twitter/android/EventSearchActivity;Landroid/net/Uri;)V

    invoke-virtual {v1, v3}, Lcom/twitter/android/SearchFragment;->a(Lcom/twitter/android/se;)V

    iget-object v1, p0, Lcom/twitter/android/fs;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhb;

    invoke-virtual {v1, v0}, Lhb;->a(Landroid/support/v4/app/Fragment;)V

    iget-object v1, p0, Lcom/twitter/android/fs;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    if-ne p2, v1, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/client/BaseListFragment;->Z()V

    :cond_0
    return-object v0
.end method

.method public onPageScrollStateChanged(I)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/fs;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v0}, Lcom/twitter/android/EventSearchActivity;->j(Lcom/twitter/android/EventSearchActivity;)Lcom/twitter/internal/android/widget/HorizontalListView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(I)V

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/fs;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v0}, Lcom/twitter/android/EventSearchActivity;->b(Lcom/twitter/android/EventSearchActivity;)Lcom/twitter/android/fr;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/fr;->a(Lcom/twitter/android/fr;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iget-object v2, p0, Lcom/twitter/android/fs;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-virtual {v2}, Lcom/twitter/android/EventSearchActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v0, v2}, Lhb;->a(Landroid/support/v4/app/FragmentManager;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/fs;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v2, v0}, Lcom/twitter/android/EventSearchActivity;->a(Lcom/twitter/android/EventSearchActivity;Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/fs;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v0}, Lcom/twitter/android/EventSearchActivity;->j(Lcom/twitter/android/EventSearchActivity;)Lcom/twitter/internal/android/widget/HorizontalListView;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(IF)V

    return-void
.end method

.method public onPageSelected(I)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/fs;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v0}, Lcom/twitter/android/EventSearchActivity;->b(Lcom/twitter/android/EventSearchActivity;)Lcom/twitter/android/fr;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/fr;->a(I)V

    iget-object v0, p0, Lcom/twitter/android/fs;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v0}, Lcom/twitter/android/EventSearchActivity;->j(Lcom/twitter/android/EventSearchActivity;)Lcom/twitter/internal/android/widget/HorizontalListView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/HorizontalListView;->b(I)V

    iget-object v0, p0, Lcom/twitter/android/fs;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iget-object v1, p0, Lcom/twitter/android/fs;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-virtual {v1}, Lcom/twitter/android/EventSearchActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    iget v1, p0, Lcom/twitter/android/fs;->d:I

    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    iget-object v1, p0, Lcom/twitter/android/fs;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v1}, Lcom/twitter/android/EventSearchActivity;->c(Lcom/twitter/android/EventSearchActivity;)Ljava/util/ArrayList;

    move-result-object v1

    iget v3, p0, Lcom/twitter/android/fs;->d:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhb;

    invoke-virtual {v1, v2}, Lhb;->a(Landroid/support/v4/app/FragmentManager;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/client/BaseListFragment;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/twitter/android/client/BaseListFragment;->aa()V

    :cond_0
    invoke-virtual {v0, v2}, Lhb;->a(Landroid/support/v4/app/FragmentManager;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/BaseListFragment;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/twitter/android/fs;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v1, v0}, Lcom/twitter/android/EventSearchActivity;->a(Lcom/twitter/android/EventSearchActivity;Landroid/support/v4/app/Fragment;)V

    invoke-virtual {v0}, Lcom/twitter/android/client/BaseListFragment;->Z()V

    :cond_1
    iput p1, p0, Lcom/twitter/android/fs;->d:I

    iget-object v0, p0, Lcom/twitter/android/fs;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-virtual {v0}, Lcom/twitter/android/EventSearchActivity;->V()V

    return-void
.end method
