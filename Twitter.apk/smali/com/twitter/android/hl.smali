.class Lcom/twitter/android/hl;
.super Landroid/support/v4/content/CursorLoader;
.source "Twttr"


# instance fields
.field private a:Ljava/util/List;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v6, "type DESC"

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    move-object v6, p6

    goto :goto_0
.end method

.method private static a(Ljava/util/List;Lcom/twitter/library/provider/Tweet;)V
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p1, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v0, v0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-static {p1, v1, v1}, Lcom/twitter/library/util/s;->a(Lcom/twitter/library/provider/Tweet;II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    invoke-static {v0}, Lcom/twitter/library/util/s;->a(Lcom/twitter/library/api/MediaEntity;)Lcom/twitter/library/util/m;

    move-result-object v0

    new-instance v2, Lcom/twitter/android/gz;

    invoke-direct {v2, p1, v0}, Lcom/twitter/android/gz;-><init>(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/util/m;)V

    invoke-interface {p0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/hl;->a:Ljava/util/List;

    return-object v0
.end method

.method public loadInBackground()Landroid/database/Cursor;
    .locals 7

    invoke-super {p0}, Landroid/support/v4/content/CursorLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/hl;->a:Ljava/util/List;

    :goto_0
    return-object v0

    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :cond_2
    new-instance v2, Lcom/twitter/library/provider/Tweet;

    invoke-direct {v2, v0}, Lcom/twitter/library/provider/Tweet;-><init>(Landroid/database/Cursor;)V

    iget-object v3, v2, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    if-eqz v3, :cond_3

    iget-object v3, v2, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    iget-object v3, v3, Lcom/twitter/library/api/TwitterStatusCard;->classicCard:Lcom/twitter/library/api/TweetClassicCard;

    new-instance v4, Lcom/twitter/library/util/m;

    iget-object v5, v3, Lcom/twitter/library/api/TweetClassicCard;->imageUrl:Ljava/lang/String;

    iget v6, v3, Lcom/twitter/library/api/TweetClassicCard;->imageWidth:I

    iget v3, v3, Lcom/twitter/library/api/TweetClassicCard;->imageHeight:I

    invoke-direct {v4, v5, v6, v3}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;II)V

    new-instance v3, Lcom/twitter/android/gz;

    invoke-direct {v3, v2, v4}, Lcom/twitter/android/gz;-><init>(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/util/m;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_2

    iput-object v1, p0, Lcom/twitter/android/hl;->a:Ljava/util/List;

    goto :goto_0

    :cond_3
    invoke-static {v1, v2}, Lcom/twitter/android/hl;->a(Ljava/util/List;Lcom/twitter/library/provider/Tweet;)V

    goto :goto_1
.end method

.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/hl;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
