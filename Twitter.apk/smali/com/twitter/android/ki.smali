.class public Lcom/twitter/android/ki;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/internal/android/widget/n;


# instance fields
.field private a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;


# direct methods
.method public constructor <init>(Lcom/twitter/internal/android/widget/HiddenDrawerLayout;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/ki;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    return-void
.end method

.method private f()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ki;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ki;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->f()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ki;->a:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->e()V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/ki;->e()V

    return-void
.end method

.method public a(Lhn;)Z
    .locals 2

    invoke-virtual {p1}, Lhn;->a()I

    move-result v0

    const v1, 0x7f090045    # com.twitter.android.R.id.home

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/ki;->f()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 0

    return-void
.end method

.method public c()V
    .locals 0

    return-void
.end method

.method public d()V
    .locals 0

    return-void
.end method

.method public e()V
    .locals 0

    return-void
.end method
