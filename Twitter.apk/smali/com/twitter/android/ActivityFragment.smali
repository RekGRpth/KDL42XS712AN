.class public Lcom/twitter/android/ActivityFragment;
.super Lcom/twitter/android/client/BaseListFragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/client/ah;
.implements Lcom/twitter/android/o;
.implements Lcom/twitter/android/p;
.implements Lcom/twitter/android/ty;
.implements Lcom/twitter/library/widget/aa;


# instance fields
.field protected a:Lcom/twitter/android/vs;

.field private final b:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private c:I

.field private d:Z

.field private e:I

.field private f:J

.field private g:J

.field private h:J

.field private i:Landroid/view/View;

.field private j:Lcom/twitter/android/yb;

.field private k:Lcom/twitter/android/vv;

.field private l:Landroid/support/v4/content/Loader;

.field private m:Landroid/database/Cursor;

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:J

.field private r:Lcom/twitter/library/util/FriendshipCache;

.field private s:Landroid/content/SharedPreferences;

.field private t:Lcom/twitter/android/ab;

.field private u:Z

.field private v:Z

.field private w:Lcom/twitter/android/util/w;

.field private x:Lcom/twitter/library/widget/TweetView;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/twitter/android/client/BaseListFragment;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/ActivityFragment;->a:Lcom/twitter/android/vs;

    new-instance v0, Lcom/twitter/android/x;

    invoke-direct {v0, p0}, Lcom/twitter/android/x;-><init>(Lcom/twitter/android/ActivityFragment;)V

    iput-object v0, p0, Lcom/twitter/android/ActivityFragment;->b:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/twitter/android/ActivityFragment;->q:J

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/ActivityFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/ActivityFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/ActivityFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/ActivityFragment;Ljava/util/HashMap;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/twitter/android/ActivityFragment;->b(Ljava/util/HashMap;)V

    return-void
.end method

.method private a(IJ)Z
    .locals 12

    const/16 v6, 0x14

    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-virtual {p0, p1}, Lcom/twitter/android/ActivityFragment;->c_(I)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return v10

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    packed-switch p1, :pswitch_data_0

    move v8, v10

    move v11, v10

    :goto_1
    iget-object v1, p0, Lcom/twitter/android/ActivityFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1}, Lcom/twitter/library/scribe/ScribeAssociation;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ActivityFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v2}, Lcom/twitter/library/scribe/ScribeAssociation;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, p1}, Lcom/twitter/android/ActivityFragment;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v7

    iget v1, p0, Lcom/twitter/android/ActivityFragment;->c:I

    invoke-direct {p0, p1}, Lcom/twitter/android/ActivityFragment;->d(I)J

    move-result-wide v2

    invoke-direct {p0, p1}, Lcom/twitter/android/ActivityFragment;->g(I)J

    move-result-wide v4

    invoke-virtual/range {v0 .. v7}, Lcom/twitter/android/client/c;->a(IJJILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/twitter/android/ActivityFragment;->a(Ljava/lang/String;I)V

    if-eqz v11, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    const-string/jumbo v2, "connect_timeline"

    invoke-static {v0}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;)Lcom/twitter/android/util/d;

    move-result-object v3

    invoke-interface {v3}, Lcom/twitter/android/util/d;->g()Z

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/api/q;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v10, v1}, Lcom/twitter/android/ActivityFragment;->a(Lcom/twitter/library/service/b;II)Z

    :cond_1
    if-eqz v8, :cond_2

    invoke-direct {p0}, Lcom/twitter/android/ActivityFragment;->z()V

    :cond_2
    const/4 v0, 0x7

    if-ne p1, v0, :cond_3

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/h;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/android/h;->b(J)V

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetChanged()V

    :goto_2
    move v10, v9

    goto :goto_0

    :pswitch_0
    invoke-virtual {v0}, Lcom/twitter/android/client/c;->s()Z

    move-result v1

    move v8, v9

    move v11, v1

    goto :goto_1

    :pswitch_1
    move v8, v9

    move v11, v10

    move v6, v10

    goto :goto_1

    :cond_3
    invoke-virtual {p0, p1}, Lcom/twitter/android/ActivityFragment;->a_(I)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic a(Lcom/twitter/android/ActivityFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/ActivityFragment;->v:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/android/ActivityFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/ActivityFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/ActivityFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/ActivityFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/ActivityFragment;->u:Z

    return v0
.end method

.method static synthetic d(Lcom/twitter/android/ActivityFragment;)I
    .locals 1

    iget v0, p0, Lcom/twitter/android/ActivityFragment;->c:I

    return v0
.end method

.method private d(I)J
    .locals 4

    const-wide/16 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid fetch type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    iget-object v2, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v2}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0xc

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    :cond_0
    :goto_0
    :pswitch_2
    return-wide v0

    :pswitch_3
    iget-wide v0, p0, Lcom/twitter/android/ActivityFragment;->h:J

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic e(Lcom/twitter/android/ActivityFragment;)Lcom/twitter/android/util/w;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->w:Lcom/twitter/android/util/w;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/ActivityFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/ActivityFragment;->v()V

    return-void
.end method

.method private g(I)J
    .locals 4

    const-wide/16 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid fetch type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    iget-wide v0, p0, Lcom/twitter/android/ActivityFragment;->g:J

    :cond_0
    :goto_0
    :pswitch_2
    return-wide v0

    :pswitch_3
    iget-object v2, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v2}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->moveToLast()Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0xd

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic g(Lcom/twitter/android/ActivityFragment;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->af()Z

    move-result v0

    return v0
.end method

.method static synthetic h(Lcom/twitter/android/ActivityFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/ActivityFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/ActivityFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/android/ActivityFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method private q()Lcom/twitter/android/kt;
    .locals 10

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v3

    new-instance v0, Lcom/twitter/android/kt;

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x2

    iget-object v4, v3, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    iget-object v5, p0, Lcom/twitter/android/ActivityFragment;->j:Lcom/twitter/android/yb;

    new-instance v6, Lcom/twitter/android/widget/cd;

    iget-object v7, p0, Lcom/twitter/android/ActivityFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v6, p0, v7}, Lcom/twitter/android/widget/cd;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;)V

    iget-object v8, p0, Lcom/twitter/android/ActivityFragment;->r:Lcom/twitter/library/util/FriendshipCache;

    iget-object v9, p0, Lcom/twitter/android/ActivityFragment;->w:Lcom/twitter/android/util/w;

    move-object v7, p0

    invoke-direct/range {v0 .. v9}, Lcom/twitter/android/kt;-><init>(Landroid/content/Context;ILcom/twitter/android/client/c;Lcom/twitter/library/widget/ap;Lcom/twitter/library/widget/aa;Lcom/twitter/library/view/c;Lcom/twitter/android/o;Lcom/twitter/library/util/FriendshipCache;Lcom/twitter/android/util/v;)V

    invoke-virtual {v0, p0}, Lcom/twitter/android/kt;->a(Lcom/twitter/android/p;)V

    return-object v0
.end method

.method private r()V
    .locals 4

    iget v0, p0, Lcom/twitter/android/ActivityFragment;->c:I

    invoke-static {v0}, Lcom/twitter/library/provider/x;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/ActivityFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1}, Lcom/twitter/library/scribe/ScribeAssociation;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ActivityFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1}, Lcom/twitter/library/scribe/ScribeAssociation;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":stream::results"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/ActivityFragment;->t:Lcom/twitter/android/ab;

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3, v0}, Lcom/twitter/android/ab;->a(JLjava/lang/String;)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/ActivityFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1}, Lcom/twitter/library/scribe/ScribeAssociation;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "::stream::results"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private s()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/twitter/android/ActivityFragment;->c:I

    packed-switch v0, :pswitch_data_0

    const-string/jumbo v0, "activity"

    :goto_0
    return-object v0

    :pswitch_0
    const-string/jumbo v0, "activity_filtered"

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "activity_following"

    goto :goto_0

    :pswitch_2
    const-string/jumbo v0, "activity_verified"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private u()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->i:Landroid/view/View;

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03000a    # com.twitter.android.R.layout.activity_stork_text

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/ActivityFragment;->i:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/ActivityFragment;->i:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/ActivityFragment;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->V()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    goto :goto_0
.end method

.method private v()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/h;

    invoke-static {}, Lgv;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/h;->d(Z)V

    return-void
.end method

.method private y()V
    .locals 1

    invoke-static {}, Lgv;->b()V

    const-string/jumbo v0, "android_rt_action_1837"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v0, "android_bioforward_joined_twitter_1455"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    instance-of v0, v0, Lcom/twitter/android/kt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/kt;

    iget-boolean v0, v0, Lcom/twitter/android/kt;->w:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "android_teachable_moments_1489"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private z()V
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/android/ActivityFragment;->u:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/ActivityFragment;->v:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/ActivityFragment;->v:Z

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/client/Session;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ActivityFragment;->d(Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(JIJ)V
    .locals 8

    const/4 v2, -0x1

    const v6, 0x7f0f0014    # com.twitter.android.R.string.activity_retweeted

    const v5, 0x7f0f0011    # com.twitter.android.R.string.activity_favorited

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/ActivityDetailActivity;

    invoke-direct {v0, v3, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "type"

    iget v4, p0, Lcom/twitter/android/ActivityFragment;->c:I

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "event_type"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "user_tag"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v4

    iget v0, p0, Lcom/twitter/android/ActivityFragment;->c:I

    invoke-static {v0}, Lcom/twitter/library/provider/x;->a(I)Z

    move-result v1

    packed-switch p3, :pswitch_data_0

    :pswitch_0
    move v0, v2

    move v1, v2

    :goto_0
    if-eq v1, v2, :cond_0

    const-string/jumbo v2, "cluster_follow"

    invoke-static {v3, v1}, Lcom/twitter/android/util/c;->a(Landroid/content/Context;I)Z

    move-result v3

    invoke-virtual {v4, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string/jumbo v2, "cluster_follow_experiment"

    invoke-virtual {v4, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v1, "cluster_follow_type"

    invoke-virtual {v4, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, v4}, Lcom/twitter/android/ActivityFragment;->startActivity(Landroid/content/Intent;)V

    return-void

    :pswitch_1
    if-eqz v1, :cond_1

    const v0, 0x7f0f01ad    # com.twitter.android.R.string.followed_you_title

    :goto_1
    const-string/jumbo v5, "title_res_id"

    invoke-virtual {v4, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    if-eqz v1, :cond_2

    const/4 v0, 0x5

    :goto_2
    const/4 v1, 0x2

    move v7, v1

    move v1, v0

    move v0, v7

    goto :goto_0

    :cond_1
    const v0, 0x7f0f0013    # com.twitter.android.R.string.activity_followed

    goto :goto_1

    :cond_2
    const/16 v0, 0x8

    goto :goto_2

    :pswitch_2
    const-string/jumbo v0, "title_res_id"

    const v1, 0x7f0f01fa    # com.twitter.android.R.string.joined_twitter_title

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/16 v0, 0x14

    move v1, v0

    move v0, v2

    goto :goto_0

    :pswitch_3
    const-string/jumbo v0, "title_res_id"

    invoke-virtual {v4, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v5, "status_tag"

    invoke-virtual {v0, v5, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    if-eqz v1, :cond_3

    const/4 v0, 0x7

    move v1, v0

    move v0, v2

    goto :goto_0

    :cond_3
    const/16 v0, 0xa

    move v1, v0

    move v0, v2

    goto :goto_0

    :pswitch_4
    const-string/jumbo v0, "title_res_id"

    invoke-virtual {v4, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "status_tag"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const/16 v0, 0x12

    move v1, v0

    move v0, v2

    goto :goto_0

    :pswitch_5
    const-string/jumbo v0, "title_res_id"

    invoke-virtual {v4, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "status_tag"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const/16 v0, 0x13

    move v1, v0

    move v0, v2

    goto :goto_0

    :pswitch_6
    const-string/jumbo v0, "title_res_id"

    invoke-virtual {v4, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "status_tag"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move v0, v2

    move v1, v2

    goto/16 :goto_0

    :pswitch_7
    const-string/jumbo v0, "title_res_id"

    invoke-virtual {v4, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v5, "status_tag"

    invoke-virtual {v0, v5, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    if-eqz v1, :cond_4

    const/4 v0, 0x6

    move v1, v0

    move v0, v2

    goto/16 :goto_0

    :cond_4
    const/16 v0, 0x9

    move v1, v0

    move v0, v2

    goto/16 :goto_0

    :pswitch_8
    const-string/jumbo v0, "title_res_id"

    invoke-virtual {v4, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "status_tag"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const/16 v0, 0x10

    move v1, v0

    move v0, v2

    goto/16 :goto_0

    :pswitch_9
    const-string/jumbo v0, "title_res_id"

    invoke-virtual {v4, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "status_tag"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const/16 v0, 0x11

    move v1, v0

    move v0, v2

    goto/16 :goto_0

    :pswitch_a
    const-string/jumbo v0, "title_res_id"

    invoke-virtual {v4, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "status_tag"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move v0, v2

    move v1, v2

    goto/16 :goto_0

    :pswitch_b
    const-string/jumbo v0, "title_res_id"

    const v1, 0x7f0f032e    # com.twitter.android.R.string.profile_tab_title_lists_owned_by

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "list_id"

    invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move v0, v2

    move v1, v2

    goto/16 :goto_0

    :pswitch_c
    const-string/jumbo v0, "title_res_id"

    const v1, 0x7f0f032d    # com.twitter.android.R.string.profile_tab_title_lists_member_of

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "list_id"

    invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move v0, v2

    move v1, v2

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_1
        :pswitch_c
        :pswitch_b
        :pswitch_0
        :pswitch_8
        :pswitch_4
        :pswitch_9
        :pswitch_5
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_a
    .end packed-switch
.end method

.method protected a(JJ)V
    .locals 5

    iget-boolean v0, p0, Lcom/twitter/android/ActivityFragment;->u:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/ActivityFragment;->v:Z

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->au()Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/twitter/library/client/aa;->a(J)Lcom/twitter/library/client/Session;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ActivityFragment;->w:Lcom/twitter/android/util/w;

    iget v2, p0, Lcom/twitter/android/ActivityFragment;->c:I

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/twitter/android/util/w;->a(IJLjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a(JJLjava/lang/String;Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/widget/TweetView;)V
    .locals 7

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/twitter/android/ActivityFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-static {v1, p1, p2, v2}, Lcom/twitter/library/scribe/ScribeAssociation;->a(IJLcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v5

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->ab()Z

    move-result v6

    move-wide v1, p3

    move-object v3, p5

    move-object v4, p6

    invoke-static/range {v0 .. v6}, Lcom/twitter/android/ProfileActivity;->a(Landroid/content/Context;JLjava/lang/String;Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/scribe/ScribeAssociation;Z)V

    return-void
.end method

.method public a(JLjava/lang/String;)V
    .locals 6

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->ab()Z

    move-result v0

    if-eqz v0, :cond_0

    const-class v0, Lcom/twitter/android/RootProfileActivity;

    :goto_0
    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v0, "user_id"

    invoke-virtual {v1, v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "screen_name"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    const/4 v2, 0x5

    iget-wide v3, p0, Lcom/twitter/android/ActivityFragment;->Q:J

    iget-object v5, p0, Lcom/twitter/android/ActivityFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-static {v2, v3, v4, v5}, Lcom/twitter/library/scribe/ScribeAssociation;->a(IJLcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/ActivityFragment;->startActivity(Landroid/content/Intent;)V

    return-void

    :cond_0
    const-class v0, Lcom/twitter/android/ProfileActivity;

    goto :goto_0
.end method

.method public a(JLjava/lang/String;Ljava/lang/String;J)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/ListTabActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "list_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "list_name"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "list_fullname"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "creator_id"

    invoke-virtual {v0, v1, p5, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ActivityFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method protected a(Landroid/content/Context;IILcom/twitter/library/service/b;)V
    .locals 2

    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/android/client/BaseListFragment;->a(Landroid/content/Context;IILcom/twitter/library/service/b;)V

    const/4 v0, 0x6

    if-ne p3, v0, :cond_0

    invoke-virtual {p4}, Lcom/twitter/library/service/b;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/p;->a(Lcom/twitter/library/client/Session;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p4}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    iget-object v0, v0, Lcom/twitter/library/service/e;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "prompt"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/Prompt;

    invoke-virtual {p0, v0}, Lcom/twitter/android/ActivityFragment;->a(Lcom/twitter/library/api/Prompt;)V

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->an()V

    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/card/Card;I)V
    .locals 0

    return-void
.end method

.method protected a(Landroid/database/Cursor;)V
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xe

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/16 v1, 0x190

    if-ge v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/ActivityFragment;->n:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, v2}, Lcom/twitter/android/ActivityFragment;->a(I)Z

    iput-boolean v2, p0, Lcom/twitter/android/ActivityFragment;->n:Z

    :cond_0
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->t:Lcom/twitter/android/ab;

    invoke-virtual {v0, p1}, Lcom/twitter/android/ab;->a(Landroid/os/Bundle;)V

    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    check-cast v0, Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->ar()Z

    move-result v1

    if-eqz v1, :cond_2

    iput-object p2, p0, Lcom/twitter/android/ActivityFragment;->m:Landroid/database/Cursor;

    iput-object p1, p0, Lcom/twitter/android/ActivityFragment;->l:Landroid/support/v4/content/Loader;

    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/twitter/android/ActivityFragment;->j(I)I

    invoke-virtual {v0}, Lcom/twitter/refresh/widget/RefreshableListView;->c()V

    iget-object v1, p0, Lcom/twitter/android/ActivityFragment;->l:Landroid/support/v4/content/Loader;

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/twitter/library/client/App;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "ANDROID-6081"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/crashlytics/android/d;->a(Ljava/lang/Throwable;)V

    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/twitter/android/ActivityFragment;->n:Z

    if-eqz v1, :cond_1

    invoke-virtual {v0, v5}, Lcom/twitter/refresh/widget/RefreshableListView;->a(Z)V

    iput-boolean v5, p0, Lcom/twitter/android/ActivityFragment;->n:Z

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/h;

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/h;->a(J)V

    return-void

    :cond_2
    iget-boolean v1, p0, Lcom/twitter/android/ActivityFragment;->p:Z

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->A()Lcom/twitter/refresh/widget/a;

    move-result-object v1

    move-object v2, v1

    :goto_1
    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseListFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    iget-boolean v1, p0, Lcom/twitter/android/ActivityFragment;->p:Z

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v1}, Landroid/support/v4/widget/CursorAdapter;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    iput-boolean v6, p0, Lcom/twitter/android/ActivityFragment;->o:Z

    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/twitter/android/ActivityFragment;->a(I)Z

    :cond_3
    :goto_2
    iput-boolean v6, p0, Lcom/twitter/android/ActivityFragment;->p:Z

    :goto_3
    invoke-virtual {p0, v2, v6}, Lcom/twitter/android/ActivityFragment;->a(Lcom/twitter/refresh/widget/a;Z)V

    goto :goto_0

    :cond_4
    new-instance v1, Lcom/twitter/refresh/widget/a;

    iget-wide v2, p0, Lcom/twitter/android/ActivityFragment;->f:J

    iget v4, p0, Lcom/twitter/android/ActivityFragment;->e:I

    invoke-direct {v1, v5, v2, v3, v4}, Lcom/twitter/refresh/widget/a;-><init>(IJI)V

    move-object v2, v1

    goto :goto_1

    :cond_5
    invoke-virtual {p0, v5}, Lcom/twitter/android/ActivityFragment;->a_(Z)V

    iget-object v1, p0, Lcom/twitter/android/ActivityFragment;->T:Lcom/twitter/android/client/ag;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/twitter/android/ActivityFragment;->T:Lcom/twitter/android/client/ag;

    invoke-interface {v1}, Lcom/twitter/android/client/ag;->d()V

    :cond_6
    invoke-virtual {p0, v5}, Lcom/twitter/android/ActivityFragment;->i(Z)V

    iget v1, p0, Lcom/twitter/android/ActivityFragment;->c:I

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v1, Lcom/twitter/android/kt;

    invoke-virtual {v1}, Lcom/twitter/android/kt;->h()V

    goto :goto_2

    :cond_7
    iget v1, p0, Lcom/twitter/android/ActivityFragment;->c:I

    if-nez v1, :cond_8

    iget-boolean v1, p0, Lcom/twitter/android/ActivityFragment;->o:Z

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v1, Lcom/twitter/android/kt;

    invoke-virtual {v1}, Lcom/twitter/android/kt;->h()V

    :cond_8
    iput-boolean v5, p0, Lcom/twitter/android/ActivityFragment;->o:Z

    goto :goto_3
.end method

.method protected a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 4

    const/16 v3, 0xc

    invoke-virtual {p1, p2}, Landroid/widget/ListView;->getPositionForView(Landroid/view/View;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/twitter/android/ActivityFragment;->g:J

    const/16 v1, 0xd

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/twitter/android/ActivityFragment;->h:J

    const/4 v1, 0x7

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-direct {p0, v1, v2, v3}, Lcom/twitter/android/ActivityFragment;->a(IJ)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v1, Lcom/twitter/android/h;

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v1, v2, p2, v0}, Lcom/twitter/android/h;->a(Landroid/content/Context;Landroid/view/View;Landroid/database/Cursor;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ActivityFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/api/TwitterUser;)V
    .locals 8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->r:Lcom/twitter/library/util/FriendshipCache;

    iget-wide v2, p1, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/util/FriendshipCache;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->r:Lcom/twitter/library/util/FriendshipCache;

    iget-wide v2, p1, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/util/FriendshipCache;->h(J)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    or-int/lit8 v0, v0, 0x1

    or-int/lit8 v0, v0, 0x40

    iget-object v2, p0, Lcom/twitter/android/ActivityFragment;->r:Lcom/twitter/library/util/FriendshipCache;

    iget-wide v3, p1, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v2, v3, v4, v0}, Lcom/twitter/library/util/FriendshipCache;->b(JI)V

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-wide v2, p1, Lcom/twitter/library/api/TwitterUser;->userId:J

    iget-object v4, p1, Lcom/twitter/library/api/TwitterUser;->promotedContent:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/twitter/android/client/c;->a(JZLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/twitter/android/ActivityFragment;->d(Ljava/lang/String;)V

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/ActivityFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v5, ""

    const-string/jumbo v6, "user"

    const-string/jumbo v7, "follow"

    invoke-static {v4, v5, v6, v7}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-wide v2, p1, Lcom/twitter/library/api/TwitterUser;->userId:J

    iget-object v4, p1, Lcom/twitter/library/api/TwitterUser;->promotedContent:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {p1}, Lcom/twitter/library/api/TwitterUser;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ActivityFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/provider/Tweet;JLcom/twitter/library/widget/TweetView;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/MediaEntity;Lcom/twitter/library/widget/TweetView;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/TweetClassicCard;Lcom/twitter/library/widget/TweetView;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/UrlEntity;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/widget/TweetView;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/widget/TweetView;I)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/provider/Tweet;Ljava/lang/String;)V
    .locals 10

    const-wide/16 v3, -0x1

    const/4 v5, 0x0

    const/4 v6, -0x1

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v8, v3

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/android/ActivityFragment;->a(Lcom/twitter/library/provider/Tweet;Ljava/lang/String;JIILcom/twitter/library/scribe/ScribeItem;J)V

    return-void
.end method

.method public a(Lcom/twitter/library/provider/Tweet;Ljava/lang/String;JIILcom/twitter/library/scribe/ScribeItem;J)V
    .locals 4

    new-instance v0, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->a(I)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    iget-wide v1, p1, Lcom/twitter/library/provider/Tweet;->o:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ActivityFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1}, Lcom/twitter/library/scribe/ScribeAssociation;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ActivityFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1}, Lcom/twitter/library/scribe/ScribeAssociation;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v1

    if-eqz p2, :cond_0

    invoke-virtual {v1, p2}, Lcom/twitter/library/scribe/ScribeAssociation;->d(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    :cond_0
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->ab()Z

    move-result v0

    if-eqz v0, :cond_1

    const-class v0, Lcom/twitter/android/RootTweetActivity;

    :goto_0
    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v0, "tw"

    invoke-virtual {v2, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "association"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "social_context_type"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "social_context_user_count"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "activity_row_id"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "scribe_item"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "magic_rec_id"

    invoke-virtual {v0, v1, p8, p9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ActivityFragment;->startActivity(Landroid/content/Intent;)V

    return-void

    :cond_1
    const-class v0, Lcom/twitter/android/TweetActivity;

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;Z)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/h;

    if-nez p3, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, p1, p2, v1}, Lcom/twitter/android/h;->a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/widget/TweetView;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->j:Lcom/twitter/android/yb;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/yb;->a(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/widget/TweetView;)V

    return-void
.end method

.method public a(Lcom/twitter/library/widget/TweetView;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->j:Lcom/twitter/android/yb;

    invoke-virtual {v0, p1}, Lcom/twitter/android/yb;->a(Lcom/twitter/library/widget/TweetView;)V

    return-void
.end method

.method public a(Lcom/twitter/library/widget/TweetView;Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p1}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/library/provider/Tweet;

    move-result-object v0

    iput-object p1, p0, Lcom/twitter/android/ActivityFragment;->x:Lcom/twitter/library/widget/TweetView;

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Lcom/twitter/library/widget/TweetView;->setHighlighted(Z)V

    invoke-virtual {p0, v0, p2}, Lcom/twitter/android/ActivityFragment;->a(Lcom/twitter/library/provider/Tweet;Ljava/lang/String;)V

    return-void
.end method

.method protected a(Lcom/twitter/refresh/widget/a;)V
    .locals 4

    new-instance v0, Lcom/twitter/library/client/f;

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "activity"

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "activity_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/twitter/android/ActivityFragment;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-wide v2, p1, Lcom/twitter/refresh/widget/a;->b:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;J)Lcom/twitter/library/client/f;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "off_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/twitter/android/ActivityFragment;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget v2, p1, Lcom/twitter/refresh/widget/a;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;I)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V

    iget-wide v0, p1, Lcom/twitter/refresh/widget/a;->b:J

    iput-wide v0, p0, Lcom/twitter/android/ActivityFragment;->f:J

    iget v0, p1, Lcom/twitter/refresh/widget/a;->c:I

    iput v0, p0, Lcom/twitter/android/ActivityFragment;->e:I

    return-void
.end method

.method protected a(Lcom/twitter/refresh/widget/a;Z)V
    .locals 4

    iget-wide v0, p1, Lcom/twitter/refresh/widget/a;->b:J

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ActivityFragment;->a(J)I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->V()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    if-nez p2, :cond_1

    :cond_0
    iget v2, p1, Lcom/twitter/refresh/widget/a;->c:I

    invoke-virtual {v1, v0, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    :cond_1
    return-void
.end method

.method protected a(Z)V
    .locals 3

    const/4 v2, 0x3

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->a(Z)V

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/h;

    if-eqz p1, :cond_2

    invoke-virtual {p0, v2}, Lcom/twitter/android/ActivityFragment;->a_(I)V

    iget-boolean v0, p0, Lcom/twitter/android/ActivityFragment;->u:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->w:Lcom/twitter/android/util/w;

    invoke-virtual {v0}, Lcom/twitter/android/util/w;->c()V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->P()Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {v0}, Lcom/twitter/android/h;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    if-nez v1, :cond_3

    invoke-virtual {p0, v2}, Lcom/twitter/android/ActivityFragment;->a_(I)V

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->p()V

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lcom/twitter/android/h;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v2}, Lcom/twitter/android/ActivityFragment;->a(I)Z

    goto :goto_0
.end method

.method protected a(I)Z
    .locals 2

    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/ActivityFragment;->a(IJ)Z

    move-result v0

    return v0
.end method

.method public a(Landroid/widget/AbsListView;I)Z
    .locals 2

    const/4 v0, 0x2

    const/4 v1, 0x0

    if-eq p2, v0, :cond_0

    if-nez p2, :cond_1

    :cond_0
    if-ne p2, v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/twitter/android/ActivityFragment;->n(Z)V

    :cond_1
    return v1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Landroid/widget/AbsListView;III)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/ActivityFragment;->u:Z

    if-eqz v0, :cond_0

    if-lez p3, :cond_0

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->w:Lcom/twitter/android/util/w;

    invoke-virtual {v0}, Lcom/twitter/android/util/w;->b()V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected a_()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->a_()V

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/h;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/h;->a()V

    :cond_0
    return-void
.end method

.method protected b()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->b()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ActivityFragment;->a(Z)V

    return-void
.end method

.method protected b(I)V
    .locals 2

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-virtual {p0, v1}, Lcom/twitter/android/ActivityFragment;->a_(Z)V

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->T:Lcom/twitter/android/client/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->T:Lcom/twitter/android/client/ag;

    invoke-interface {v0}, Lcom/twitter/android/client/ag;->d()V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->ar()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->h()V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->T:Lcom/twitter/android/client/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->T:Lcom/twitter/android/client/ag;

    invoke-interface {v0}, Lcom/twitter/android/client/ag;->d()V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    check-cast v0, Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-virtual {v0, v1}, Lcom/twitter/refresh/widget/RefreshableListView;->a(Z)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/h;

    invoke-virtual {v0}, Lcom/twitter/android/h;->g()V

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetChanged()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public b(Lcom/twitter/library/api/TwitterUser;)V
    .locals 8

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->r:Lcom/twitter/library/util/FriendshipCache;

    iget-wide v1, p1, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/util/FriendshipCache;->e(J)V

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-wide v1, p1, Lcom/twitter/library/api/TwitterUser;->userId:J

    iget-object v3, p1, Lcom/twitter/library/api/TwitterUser;->promotedContent:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/ActivityFragment;->d(Ljava/lang/String;)V

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/ActivityFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v5, ""

    const-string/jumbo v6, "user"

    const-string/jumbo v7, "unfollow"

    invoke-static {v4, v5, v6, v7}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-wide v2, p1, Lcom/twitter/library/api/TwitterUser;->userId:J

    iget-object v4, p1, Lcom/twitter/library/api/TwitterUser;->promotedContent:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {p1}, Lcom/twitter/library/api/TwitterUser;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ActivityFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method public b(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/UrlEntity;)V
    .locals 9

    const/4 v6, 0x0

    iget v0, p0, Lcom/twitter/android/ActivityFragment;->c:I

    invoke-static {v0}, Lcom/twitter/library/provider/x;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "connect:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/twitter/android/ActivityFragment;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ":tweet:link:open_link"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v7, p0, Lcom/twitter/android/ActivityFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v1, p1

    move-object v2, p2

    move-object v8, v6

    invoke-static/range {v0 .. v8}, Lcom/twitter/android/client/be;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/UrlEntity;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/widget/TweetView;)V
    .locals 0

    return-void
.end method

.method public b(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/widget/TweetView;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->j:Lcom/twitter/android/yb;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/yb;->b(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/widget/TweetView;)V

    return-void
.end method

.method public b(Z)V
    .locals 0

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->an()V

    :cond_0
    return-void
.end method

.method protected c()V
    .locals 5

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->c()V

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v0, "ref_event"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "ref_event"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "ref_event"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    :goto_0
    iget v1, p0, Lcom/twitter/android/ActivityFragment;->c:I

    invoke-static {v1}, Lcom/twitter/library/provider/x;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "connect:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/twitter/android/ActivityFragment;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ":::impression"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4, v1, v0}, Lcom/twitter/android/client/c;->a(JLjava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const-string/jumbo v1, "network_activity::::impression"

    goto :goto_1
.end method

.method protected d()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->d()V

    invoke-direct {p0}, Lcom/twitter/android/ActivityFragment;->r()V

    iget-boolean v0, p0, Lcom/twitter/android/ActivityFragment;->u:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->w:Lcom/twitter/android/util/w;

    invoke-virtual {v0}, Lcom/twitter/android/util/w;->c()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->i:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->i:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method public f()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->l:Landroid/support/v4/content/Loader;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->l:Landroid/support/v4/content/Loader;

    iget-object v1, p0, Lcom/twitter/android/ActivityFragment;->m:Landroid/database/Cursor;

    invoke-super {p0, v0, v1}, Lcom/twitter/android/client/BaseListFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    iput-object v2, p0, Lcom/twitter/android/ActivityFragment;->l:Landroid/support/v4/content/Loader;

    iput-object v2, p0, Lcom/twitter/android/ActivityFragment;->m:Landroid/database/Cursor;

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ActivityFragment;->m(Z)V

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->i:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->i:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iput-object v2, p0, Lcom/twitter/android/ActivityFragment;->i:Landroid/view/View;

    :cond_1
    return-void
.end method

.method public g()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->P()Z

    :cond_0
    return-void
.end method

.method protected h()V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->g()V

    return-void
.end method

.method protected i()V
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/android/ActivityFragment;->a(I)Z

    return-void
.end method

.method protected j()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected k()V
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Lcom/twitter/library/client/f;

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "activity"

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "activity_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/twitter/android/ActivityFragment;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/twitter/android/ActivityFragment;->f:J

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "off_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/twitter/android/ActivityFragment;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ActivityFragment;->e:I

    new-instance v0, Lcom/twitter/refresh/widget/a;

    iget-wide v1, p0, Lcom/twitter/android/ActivityFragment;->f:J

    iget v3, p0, Lcom/twitter/android/ActivityFragment;->e:I

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/twitter/refresh/widget/a;-><init>(IJI)V

    invoke-virtual {p0, v0, v4}, Lcom/twitter/android/ActivityFragment;->a(Lcom/twitter/refresh/widget/a;Z)V

    return-void
.end method

.method protected l()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0}, Lcom/twitter/library/scribe/ScribeAssociation;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected m()Z
    .locals 1

    iget v0, p0, Lcom/twitter/android/ActivityFragment;->c:I

    invoke-static {v0}, Lcom/twitter/library/provider/x;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/platform/TwitterDataSyncService;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected n()V
    .locals 1

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/twitter/android/ActivityFragment;->a(I)Z

    return-void
.end method

.method protected o()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 12

    const/4 v10, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v3

    new-instance v0, Lcom/twitter/android/vn;

    iget-object v2, p0, Lcom/twitter/android/ActivityFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v0, p0, v2}, Lcom/twitter/android/vn;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;)V

    new-instance v2, Lcom/twitter/android/yb;

    iget-object v4, p0, Lcom/twitter/android/ActivityFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    iget-object v5, p0, Lcom/twitter/android/ActivityFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v6, "tweet"

    const-string/jumbo v7, "avatar"

    const-string/jumbo v8, "profile_click"

    invoke-static {v5, v6, v7, v8}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/twitter/android/ActivityFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v7}, Lcom/twitter/library/scribe/ScribeAssociation;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "::tweet:link:open_link"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, p0, v4, v5, v6}, Lcom/twitter/android/yb;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/twitter/android/ActivityFragment;->j:Lcom/twitter/android/yb;

    new-instance v2, Lcom/twitter/android/vs;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4, v0}, Lcom/twitter/android/vs;-><init>(Landroid/content/Context;Lcom/twitter/android/vu;)V

    iput-object v2, p0, Lcom/twitter/android/ActivityFragment;->a:Lcom/twitter/android/vs;

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->a:Lcom/twitter/android/vs;

    iget-wide v4, p0, Lcom/twitter/android/ActivityFragment;->q:J

    invoke-virtual {v0, v4, v5}, Lcom/twitter/android/vs;->a(J)V

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->V()Landroid/widget/ListView;

    move-result-object v11

    new-instance v0, Lcom/twitter/android/vv;

    iget-object v2, p0, Lcom/twitter/android/ActivityFragment;->a:Lcom/twitter/android/vs;

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v4

    invoke-direct {v0, p0, v2, v11, v4}, Lcom/twitter/android/vv;-><init>(Lcom/twitter/android/client/BaseListFragment;Lcom/twitter/android/vs;Landroid/widget/ListView;I)V

    iput-object v0, p0, Lcom/twitter/android/ActivityFragment;->k:Lcom/twitter/android/vv;

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->k:Lcom/twitter/android/vv;

    invoke-virtual {p0, v0}, Lcom/twitter/android/ActivityFragment;->a(Landroid/view/View$OnTouchListener;)Lcom/twitter/android/client/BaseListFragment;

    new-instance v0, Lcom/twitter/library/util/FriendshipCache;

    invoke-direct {v0}, Lcom/twitter/library/util/FriendshipCache;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ActivityFragment;->r:Lcom/twitter/library/util/FriendshipCache;

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/android/ActivityFragment;->c:I

    invoke-static {v0}, Lcom/twitter/library/provider/x;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/twitter/android/ActivityFragment;->q()Lcom/twitter/android/kt;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/h;

    invoke-virtual {p0, v0}, Lcom/twitter/android/ActivityFragment;->a(Lcom/twitter/android/client/av;)Lcom/twitter/android/client/BaseListFragment;

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/h;

    invoke-virtual {v0, p0}, Lcom/twitter/android/h;->a(Lcom/twitter/android/p;)V

    :cond_0
    if-eqz p1, :cond_4

    const-string/jumbo v0, "spinning_gap_ids"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/h;

    array-length v4, v3

    move v2, v10

    :goto_1
    if-ge v2, v4, :cond_2

    aget-wide v5, v3, v2

    invoke-virtual {v0, v5, v6}, Lcom/twitter/android/h;->b(J)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    new-instance v0, Lcom/twitter/android/nt;

    const/4 v2, 0x2

    iget-object v4, v3, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    iget-object v5, p0, Lcom/twitter/android/ActivityFragment;->a:Lcom/twitter/android/vs;

    iget-object v6, p0, Lcom/twitter/android/ActivityFragment;->j:Lcom/twitter/android/yb;

    new-instance v7, Lcom/twitter/android/widget/cd;

    iget-object v8, p0, Lcom/twitter/android/ActivityFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v7, p0, v8}, Lcom/twitter/android/widget/cd;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;)V

    iget-object v8, p0, Lcom/twitter/android/ActivityFragment;->r:Lcom/twitter/library/util/FriendshipCache;

    move-object v9, p0

    invoke-direct/range {v0 .. v9}, Lcom/twitter/android/nt;-><init>(Landroid/content/Context;ILcom/twitter/android/client/c;Lcom/twitter/library/widget/ap;Lcom/twitter/android/vs;Lcom/twitter/library/widget/aa;Lcom/twitter/library/view/c;Lcom/twitter/library/util/FriendshipCache;Lcom/twitter/android/o;)V

    iput-object v0, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetChanged()V

    :cond_3
    const-string/jumbo v0, "state_show_stork"

    invoke-virtual {p1, v0, v10}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/twitter/android/ActivityFragment;->u()V

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->s:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "show_stork_text"

    invoke-interface {v0, v2, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-direct {p0}, Lcom/twitter/android/ActivityFragment;->u()V

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->s:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "show_stork_text"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_5
    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v11, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v0, Lcom/twitter/android/aa;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/twitter/android/aa;-><init>(Lcom/twitter/android/ActivityFragment;Lcom/twitter/android/x;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/ActivityFragment;->a(Lcom/twitter/library/client/z;)V

    new-instance v0, Lcom/twitter/android/y;

    invoke-direct {v0, p0}, Lcom/twitter/android/y;-><init>(Lcom/twitter/android/ActivityFragment;)V

    iput-object v0, p0, Lcom/twitter/android/ActivityFragment;->O:Lcom/twitter/library/client/j;

    new-instance v0, Lcom/twitter/android/ab;

    iget-object v2, p0, Lcom/twitter/android/ActivityFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/ab;-><init>(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeAssociation;)V

    iput-object v0, p0, Lcom/twitter/android/ActivityFragment;->t:Lcom/twitter/android/ab;

    invoke-direct {p0}, Lcom/twitter/android/ActivityFragment;->z()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v3, 0x6

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "activity_type"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ActivityFragment;->c:I

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "activity_mention_only"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/ActivityFragment;->d:Z

    iget v0, p0, Lcom/twitter/android/ActivityFragment;->c:I

    invoke-static {v0}, Lcom/twitter/library/provider/x;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    invoke-virtual {v0, v3}, Lcom/twitter/library/scribe/ScribeAssociation;->a(I)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    const-string/jumbo v1, "connect"

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    invoke-direct {p0}, Lcom/twitter/android/ActivityFragment;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ActivityFragment;->b(Lcom/twitter/library/scribe/ScribeAssociation;)V

    :goto_0
    if-eqz p1, :cond_0

    const-string/jumbo v0, "state_revealer_id"

    const-wide/high16 v1, -0x8000000000000000L

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/ActivityFragment;->q:J

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ActivityFragment;->s:Landroid/content/SharedPreferences;

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->s:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/twitter/android/ActivityFragment;->b:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    invoke-virtual {p0, p0}, Lcom/twitter/android/ActivityFragment;->a(Lcom/twitter/android/client/ah;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/ActivityFragment;->a(ILcom/twitter/library/util/ar;)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/ActivityFragment;->a(ILcom/twitter/library/util/ar;)V

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v5

    iget v0, p0, Lcom/twitter/android/ActivityFragment;->c:I

    invoke-static {v0}, Lcom/twitter/library/provider/x;->a(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/ActivityFragment;->u:Z

    iget-boolean v0, p0, Lcom/twitter/android/ActivityFragment;->u:Z

    if-eqz v0, :cond_1

    new-instance v0, Lcom/twitter/android/util/w;

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget v2, p0, Lcom/twitter/android/ActivityFragment;->c:I

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/util/w;-><init>(Landroid/content/Context;IJLjava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/ActivityFragment;->w:Lcom/twitter/android/util/w;

    :cond_1
    return-void

    :cond_2
    new-instance v0, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    invoke-virtual {v0, v3}, Lcom/twitter/library/scribe/ScribeAssociation;->a(I)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    const-string/jumbo v1, "network_activity"

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    const-string/jumbo v1, "activity"

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ActivityFragment;->b(Lcom/twitter/library/scribe/ScribeAssociation;)V

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 11

    iget-boolean v0, p0, Lcom/twitter/android/ActivityFragment;->d:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "type=? AND event IN (2, 3)"

    :goto_0
    invoke-static {}, Lcom/twitter/library/featureswitch/a;->aE()Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " AND event NOT IN (18, 19, 20)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v7

    new-instance v0, Lcom/twitter/android/ac;

    invoke-virtual {p0}, Lcom/twitter/android/ActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/provider/x;->a:Landroid/net/Uri;

    invoke-static {v2, v7, v8}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/library/provider/bb;->a:[Ljava/lang/String;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    iget v9, p0, Lcom/twitter/android/ActivityFragment;->c:I

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v5, v6

    const/4 v6, 0x0

    iget-object v9, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v9, Lcom/twitter/android/h;

    invoke-virtual {v9}, Lcom/twitter/android/h;->d()I

    move-result v9

    iget-object v10, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v10, Lcom/twitter/android/h;

    invoke-virtual {v10}, Lcom/twitter/android/h;->e()I

    move-result v10

    invoke-direct/range {v0 .. v10}, Lcom/twitter/android/ac;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;JII)V

    return-object v0

    :cond_0
    const-string/jumbo v0, "type=?"

    goto :goto_0

    :cond_1
    move-object v4, v0

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onDestroy()V

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->s:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/twitter/android/ActivityFragment;->b:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/ActivityFragment;->b(ILcom/twitter/library/util/ar;)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/ActivityFragment;->b(ILcom/twitter/library/util/ar;)V

    return-void
.end method

.method public onDestroyView()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->k:Lcom/twitter/android/vv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->k:Lcom/twitter/android/vv;

    invoke-virtual {p0, v0}, Lcom/twitter/android/ActivityFragment;->b(Landroid/view/View$OnTouchListener;)Lcom/twitter/android/client/BaseListFragment;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/ActivityFragment;->k:Lcom/twitter/android/vv;

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onDestroyView()V

    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/ActivityFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onResume()V

    invoke-direct {p0}, Lcom/twitter/android/ActivityFragment;->y()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/h;

    invoke-virtual {v0}, Lcom/twitter/android/h;->f()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "spinning_gap_ids"

    invoke-static {v0}, Lcom/twitter/library/util/Util;->b(Ljava/util/Collection;)[J

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->a:Lcom/twitter/android/vs;

    if-eqz v0, :cond_1

    const-string/jumbo v0, "state_revealer_id"

    iget-object v1, p0, Lcom/twitter/android/ActivityFragment;->a:Lcom/twitter/android/vs;

    invoke-virtual {v1}, Lcom/twitter/android/vs;->a()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->i:Landroid/view/View;

    if-eqz v0, :cond_2

    const-string/jumbo v0, "state_show_stork"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_2
    return-void
.end method

.method public onStop()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onStop()V

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->x:Lcom/twitter/library/widget/TweetView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ActivityFragment;->x:Lcom/twitter/library/widget/TweetView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetView;->setHighlighted(Z)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/ActivityFragment;->x:Lcom/twitter/library/widget/TweetView;

    :cond_0
    return-void
.end method

.method public u_()V
    .locals 0

    return-void
.end method
