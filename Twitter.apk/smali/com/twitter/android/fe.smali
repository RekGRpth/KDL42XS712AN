.class public Lcom/twitter/android/fe;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/refresh/widget/e;


# instance fields
.field private final a:Lcom/twitter/internal/android/widget/DockLayout;


# direct methods
.method public constructor <init>(Lcom/twitter/internal/android/widget/DockLayout;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/fe;->a:Lcom/twitter/internal/android/widget/DockLayout;

    return-void
.end method


# virtual methods
.method public b(Z)V
    .locals 2

    iget-object v1, p0, Lcom/twitter/android/fe;->a:Lcom/twitter/internal/android/widget/DockLayout;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/internal/android/widget/DockLayout;->setTopLocked(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b_()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/fe;->a:Lcom/twitter/internal/android/widget/DockLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/DockLayout;->setTopLocked(Z)V

    return-void
.end method

.method public x_()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/fe;->a:Lcom/twitter/internal/android/widget/DockLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/DockLayout;->setTopLocked(Z)V

    return-void
.end method
