.class Lcom/twitter/android/rm;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/RemoveAccountDialogActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/RemoveAccountDialogActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/rm;->a:Lcom/twitter/android/RemoveAccountDialogActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/twitter/android/rm;->a:Lcom/twitter/android/RemoveAccountDialogActivity;

    iget-object v1, p0, Lcom/twitter/android/rm;->a:Lcom/twitter/android/RemoveAccountDialogActivity;

    invoke-static {v1}, Lcom/twitter/android/RemoveAccountDialogActivity;->a(Lcom/twitter/android/RemoveAccountDialogActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/library/api/account/k;->f(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/rm;->a:Lcom/twitter/android/RemoveAccountDialogActivity;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/twitter/android/RemoveAccountDialogActivity;->showDialog(I)V

    iget-object v0, p0, Lcom/twitter/android/rm;->a:Lcom/twitter/android/RemoveAccountDialogActivity;

    iput-boolean v2, v0, Lcom/twitter/android/RemoveAccountDialogActivity;->b:Z

    iget-object v0, p0, Lcom/twitter/android/rm;->a:Lcom/twitter/android/RemoveAccountDialogActivity;

    iput-boolean v2, v0, Lcom/twitter/android/RemoveAccountDialogActivity;->a:Z

    iget-object v0, p0, Lcom/twitter/android/rm;->a:Lcom/twitter/android/RemoveAccountDialogActivity;

    iget-object v0, v0, Lcom/twitter/android/RemoveAccountDialogActivity;->c:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/rm;->a:Lcom/twitter/android/RemoveAccountDialogActivity;

    iget-object v1, v1, Lcom/twitter/android/RemoveAccountDialogActivity;->d:Lcom/twitter/library/client/aa;

    iget-object v2, p0, Lcom/twitter/android/rm;->a:Lcom/twitter/android/RemoveAccountDialogActivity;

    invoke-static {v2}, Lcom/twitter/android/RemoveAccountDialogActivity;->a(Lcom/twitter/android/RemoveAccountDialogActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/rm;->a:Lcom/twitter/android/RemoveAccountDialogActivity;

    invoke-static {v2}, Lcom/twitter/android/RemoveAccountDialogActivity;->a(Lcom/twitter/android/RemoveAccountDialogActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/rm;->a:Lcom/twitter/android/RemoveAccountDialogActivity;

    invoke-virtual {v0}, Lcom/twitter/android/RemoveAccountDialogActivity;->a()V

    goto :goto_0
.end method
