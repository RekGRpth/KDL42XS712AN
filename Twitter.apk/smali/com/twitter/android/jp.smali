.class Lcom/twitter/android/jp;
.super Landroid/widget/ArrayAdapter;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/LoginVerificationFragment;


# direct methods
.method public constructor <init>(Lcom/twitter/android/LoginVerificationFragment;Landroid/content/Context;IILjava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/jp;->a:Lcom/twitter/android/LoginVerificationFragment;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 19

    if-nez p1, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/jp;->a:Lcom/twitter/android/LoginVerificationFragment;

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Lcom/twitter/android/LoginVerificationFragment;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    :goto_0
    return-object v3

    :cond_0
    const/4 v3, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p3

    invoke-super {v0, v1, v3, v2}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v13

    invoke-virtual/range {p0 .. p1}, Lcom/twitter/android/jp;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    move-object v9, v3

    check-cast v9, Lcom/twitter/library/api/account/LoginVerificationRequest;

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    const v3, 0x7f09002d    # com.twitter.android.R.id.action_button_accept

    invoke-virtual {v13, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object v10, v3

    check-cast v10, Landroid/widget/ImageButton;

    const v3, 0x7f09002f    # com.twitter.android.R.id.action_button_deny

    invoke-virtual {v13, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object v11, v3

    check-cast v11, Landroid/widget/ImageButton;

    const v3, 0x7f0901c8    # com.twitter.android.R.id.login_verification_request_content

    invoke-virtual {v13, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object v12, v3

    check-cast v12, Landroid/widget/TextView;

    iget-object v3, v9, Lcom/twitter/library/api/account/LoginVerificationRequest;->d:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/jp;->a:Lcom/twitter/android/LoginVerificationFragment;

    const v4, 0x7f0f0258    # com.twitter.android.R.string.login_verification_unknown_geo

    invoke-virtual {v3, v4}, Lcom/twitter/android/LoginVerificationFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_1
    iget-object v4, v9, Lcom/twitter/library/api/account/LoginVerificationRequest;->e:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/jp;->a:Lcom/twitter/android/LoginVerificationFragment;

    const v7, 0x7f0f0257    # com.twitter.android.R.string.login_verification_unknown_browser

    invoke-virtual {v4, v7}, Lcom/twitter/android/LoginVerificationFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    :goto_2
    iget-wide v7, v9, Lcom/twitter/library/api/account/LoginVerificationRequest;->f:J

    sub-long/2addr v7, v5

    invoke-static {v7, v8}, Ljava/lang/Math;->abs(J)J

    move-result-wide v7

    const-wide/16 v14, 0x4e20

    cmp-long v7, v7, v14

    if-ltz v7, :cond_1

    iget-wide v7, v9, Lcom/twitter/library/api/account/LoginVerificationRequest;->f:J

    cmp-long v5, v7, v5

    if-lez v5, :cond_4

    :cond_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/jp;->a:Lcom/twitter/android/LoginVerificationFragment;

    invoke-static {v5}, Lcom/twitter/android/LoginVerificationFragment;->e(Lcom/twitter/android/LoginVerificationFragment;)[Landroid/text/style/StyleSpan;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/jp;->a:Lcom/twitter/android/LoginVerificationFragment;

    const v7, 0x7f0f023b    # com.twitter.android.R.string.login_verification_accept_request_just_now

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v3, v8, v14

    const/4 v3, 0x1

    aput-object v4, v8, v3

    invoke-virtual {v6, v7, v8}, Lcom/twitter/android/LoginVerificationFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x22

    invoke-static {v5, v3, v4}, Lcom/twitter/library/util/Util;->a([Ljava/lang/Object;Ljava/lang/String;C)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v12, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    new-instance v3, Lcom/twitter/android/jq;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v9}, Lcom/twitter/android/jq;-><init>(Lcom/twitter/android/jp;Lcom/twitter/library/api/account/LoginVerificationRequest;)V

    new-instance v4, Lcom/twitter/android/jr;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v9}, Lcom/twitter/android/jr;-><init>(Lcom/twitter/android/jp;Lcom/twitter/library/api/account/LoginVerificationRequest;)V

    invoke-virtual {v10, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v11, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v3, v13

    goto/16 :goto_0

    :cond_2
    iget-object v3, v9, Lcom/twitter/library/api/account/LoginVerificationRequest;->d:Ljava/lang/String;

    goto :goto_1

    :cond_3
    iget-object v4, v9, Lcom/twitter/library/api/account/LoginVerificationRequest;->e:Ljava/lang/String;

    goto :goto_2

    :cond_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/jp;->a:Lcom/twitter/android/LoginVerificationFragment;

    invoke-static {v5}, Lcom/twitter/android/LoginVerificationFragment;->e(Lcom/twitter/android/LoginVerificationFragment;)[Landroid/text/style/StyleSpan;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/twitter/android/jp;->a:Lcom/twitter/android/LoginVerificationFragment;

    const v16, 0x7f0f023a    # com.twitter.android.R.string.login_verification_accept_request

    const/4 v5, 0x3

    new-array v0, v5, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/4 v5, 0x0

    aput-object v3, v17, v5

    const/4 v3, 0x1

    aput-object v4, v17, v3

    const/16 v18, 0x2

    iget-wide v3, v9, Lcom/twitter/library/api/account/LoginVerificationRequest;->f:J

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    invoke-static/range {v3 .. v8}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(JJJ)Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v17, v18

    invoke-virtual/range {v15 .. v17}, Lcom/twitter/android/LoginVerificationFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x22

    invoke-static {v14, v3, v4}, Lcom/twitter/library/util/Util;->a([Ljava/lang/Object;Ljava/lang/String;C)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v12, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method
