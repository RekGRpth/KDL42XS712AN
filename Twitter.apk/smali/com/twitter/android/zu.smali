.class Lcom/twitter/android/zu;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/widget/MultiAutoCompleteTextView$Tokenizer;


# instance fields
.field final synthetic a:Lcom/twitter/android/UserSelectFragment;


# direct methods
.method private constructor <init>(Lcom/twitter/android/UserSelectFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/zu;->a:Lcom/twitter/android/UserSelectFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/UserSelectFragment;Lcom/twitter/android/zn;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/zu;-><init>(Lcom/twitter/android/UserSelectFragment;)V

    return-void
.end method


# virtual methods
.method public findTokenEnd(Ljava/lang/CharSequence;I)I
    .locals 9

    const/4 v1, 0x0

    if-gtz p2, :cond_0

    :goto_0
    return p2

    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    iget-object v0, p0, Lcom/twitter/android/zu;->a:Lcom/twitter/android/UserSelectFragment;

    iget-object v0, v0, Lcom/twitter/android/UserSelectFragment;->a:Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

    invoke-virtual {v0}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->getText()Landroid/text/Editable;

    move-result-object v5

    iget-object v0, p0, Lcom/twitter/android/zu;->a:Lcom/twitter/android/UserSelectFragment;

    iget-object v0, v0, Lcom/twitter/android/UserSelectFragment;->a:Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

    invoke-virtual {v0}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->length()I

    move-result v0

    const-class v2, Lcom/twitter/android/zt;

    invoke-interface {v5, v1, v0, v2}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/zt;

    array-length v6, v0

    move v4, v1

    :goto_1
    if-ge v4, v6, :cond_4

    aget-object v2, v0, v4

    invoke-interface {v5, v2}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v1

    invoke-interface {v5, v2}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v2

    if-ne p2, v1, :cond_1

    move p2, v1

    goto :goto_0

    :cond_1
    sub-int v7, v1, p2

    sub-int v8, v3, p2

    if-ge v7, v8, :cond_2

    sub-int v7, v1, p2

    if-lez v7, :cond_2

    move v3, v1

    :cond_2
    if-ge v1, p2, :cond_3

    if-le v2, p2, :cond_3

    move p2, v2

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    :cond_4
    move p2, v3

    goto :goto_0
.end method

.method public findTokenStart(Ljava/lang/CharSequence;I)I
    .locals 9

    const/4 v1, 0x0

    if-gtz p2, :cond_0

    :goto_0
    return p2

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/zu;->a:Lcom/twitter/android/UserSelectFragment;

    iget-object v0, v0, Lcom/twitter/android/UserSelectFragment;->a:Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

    invoke-virtual {v0}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->getText()Landroid/text/Editable;

    move-result-object v5

    iget-object v0, p0, Lcom/twitter/android/zu;->a:Lcom/twitter/android/UserSelectFragment;

    iget-object v0, v0, Lcom/twitter/android/UserSelectFragment;->a:Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

    invoke-virtual {v0}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->length()I

    move-result v0

    const-class v2, Lcom/twitter/android/zt;

    invoke-interface {v5, v1, v0, v2}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/zt;

    array-length v6, v0

    move v4, v1

    :goto_1
    if-ge v4, v6, :cond_4

    aget-object v2, v0, v4

    invoke-interface {v5, v2}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v3

    invoke-interface {v5, v2}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v2

    if-ne p2, v2, :cond_1

    move p2, v2

    goto :goto_0

    :cond_1
    sub-int v7, p2, v2

    sub-int v8, p2, v1

    if-ge v7, v8, :cond_2

    sub-int v7, p2, v2

    if-lez v7, :cond_2

    move v1, v2

    :cond_2
    if-ge v3, p2, :cond_3

    if-le v2, p2, :cond_3

    move p2, v3

    goto :goto_0

    :cond_3
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    :cond_4
    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_5

    invoke-interface {v5, v1}, Landroid/text/Editable;->charAt(I)C

    move-result v0

    const/16 v2, 0x20

    if-ne v0, v2, :cond_5

    add-int/lit8 v0, v1, 0x1

    :goto_2
    move p2, v0

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method public terminateToken(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0

    return-object p1
.end method
