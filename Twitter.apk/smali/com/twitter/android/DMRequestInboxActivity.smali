.class public Lcom/twitter/android/DMRequestInboxActivity;
.super Lcom/twitter/android/ListFragmentActivity;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/ListFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a(Z)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a(I)V

    return-object v0
.end method

.method protected a(Landroid/content/Intent;Lcom/twitter/library/client/e;)Lcom/twitter/android/iu;
    .locals 2

    new-instance v0, Lcom/twitter/android/iu;

    new-instance v1, Lcom/twitter/android/DMRequestInboxFragment;

    invoke-direct {v1}, Lcom/twitter/android/DMRequestInboxFragment;-><init>()V

    invoke-direct {v0, v1}, Lcom/twitter/android/iu;-><init>(Lcom/twitter/android/client/BaseListFragment;)V

    return-object v0
.end method

.method protected a(Landroid/content/Intent;)Ljava/lang/CharSequence;
    .locals 1

    const v0, 0x7f0f01d0    # com.twitter.android.R.string.home_direct_messages

    invoke-virtual {p0, v0}, Lcom/twitter/android/DMRequestInboxActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected j()V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/DMRequestInboxActivity;->W()V

    return-void
.end method
