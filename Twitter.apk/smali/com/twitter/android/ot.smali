.class final Lcom/twitter/android/ot;
.super Landroid/os/AsyncTask;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/ou;


# instance fields
.field final synthetic a:Lcom/twitter/android/PhotoSelectHelper;

.field private final b:Ljava/lang/ref/WeakReference;

.field private final c:Landroid/content/Context;

.field private final d:Ljava/util/Collection;

.field private final e:J

.field private final f:Z


# direct methods
.method constructor <init>(Lcom/twitter/android/PhotoSelectHelper;Lcom/twitter/android/AttachMediaListener;Landroid/content/Context;Ljava/util/Collection;ZJ)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/ot;->a:Lcom/twitter/android/PhotoSelectHelper;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/ot;->b:Ljava/lang/ref/WeakReference;

    iput-object p3, p0, Lcom/twitter/android/ot;->c:Landroid/content/Context;

    iput-object p4, p0, Lcom/twitter/android/ot;->d:Ljava/util/Collection;

    iput-boolean p5, p0, Lcom/twitter/android/ot;->f:Z

    iput-wide p6, p0, Lcom/twitter/android/ot;->e:J

    return-void
.end method

.method private a(Lcom/twitter/android/PostStorage$MediaItem;)V
    .locals 7

    const/4 v6, 0x1

    :try_start_0
    invoke-direct {p0, p1}, Lcom/twitter/android/ot;->b(Lcom/twitter/android/PostStorage$MediaItem;)F

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/ot;->c:Landroid/content/Context;

    const v2, 0x7f0f018c    # com.twitter.android.R.string.file_size_format

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/twitter/android/PostStorage$MediaItem;->g:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/ot;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/android/PostStorage$MediaItem;->b:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "title"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, Lcom/twitter/android/PostStorage$MediaItem;->f:Ljava/lang/String;

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    iget-object v0, p1, Lcom/twitter/android/PostStorage$MediaItem;->b:Landroid/net/Uri;

    iget-boolean v1, p0, Lcom/twitter/android/ot;->f:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/twitter/android/ot;->c:Landroid/content/Context;

    iget-wide v2, p0, Lcom/twitter/android/ot;->e:J

    invoke-static {v1, v0, v2, v3}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;Landroid/net/Uri;J)Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    iput v0, p1, Lcom/twitter/android/PostStorage$MediaItem;->c:I

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v1, p1, Lcom/twitter/android/PostStorage$MediaItem;->b:Landroid/net/Uri;

    invoke-static {v1}, Lcom/twitter/library/util/n;->b(Landroid/net/Uri;)Z

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p1, Lcom/twitter/android/PostStorage$MediaItem;->b:Landroid/net/Uri;

    :cond_4
    iget-object v0, p1, Lcom/twitter/android/PostStorage$MediaItem;->e:Landroid/graphics/Bitmap;

    if-nez v0, :cond_2

    iget-object v0, p1, Lcom/twitter/android/PostStorage$MediaItem;->b:Landroid/net/Uri;

    iget-object v1, p0, Lcom/twitter/android/ot;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ot;->c:Landroid/content/Context;

    invoke-static {v2, v0}, Lkw;->a(Landroid/content/Context;Landroid/net/Uri;)Lkw;

    move-result-object v2

    iget v3, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v2, v3, v1}, Lkw;->a(II)Lkw;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v2, v1}, Lkw;->a(Landroid/graphics/Bitmap$Config;)Lkw;

    invoke-virtual {v2}, Lkw;->b()Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_5

    const/4 v0, 0x1

    iput v0, p1, Lcom/twitter/android/PostStorage$MediaItem;->c:I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iput v6, p1, Lcom/twitter/android/PostStorage$MediaItem;->c:I

    invoke-static {v0}, Lcom/crashlytics/android/d;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_5
    :try_start_1
    iget-object v2, p0, Lcom/twitter/android/ot;->c:Landroid/content/Context;

    invoke-static {v2}, Lcom/twitter/media/filters/c;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_7

    iget v2, p1, Lcom/twitter/android/PostStorage$MediaItem;->h:I

    invoke-static {v2}, Lcom/twitter/media/filters/c;->a(I)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-boolean v2, p1, Lcom/twitter/android/PostStorage$MediaItem;->i:Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v2, :cond_7

    :cond_6
    :try_start_2
    iget-object v2, p0, Lcom/twitter/android/ot;->c:Landroid/content/Context;

    iget v3, p1, Lcom/twitter/android/PostStorage$MediaItem;->h:I

    iget-boolean v4, p1, Lcom/twitter/android/PostStorage$MediaItem;->i:Z

    invoke-static {v2, v0, v1, v3, v4}, Lcom/twitter/media/filters/c;->a(Landroid/content/Context;Landroid/net/Uri;Landroid/graphics/Bitmap;IZ)Z
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_7
    :goto_1
    :try_start_3
    iput-object v1, p1, Lcom/twitter/android/PostStorage$MediaItem;->e:Landroid/graphics/Bitmap;

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/crashlytics/android/d;->a(Ljava/lang/Throwable;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1
.end method

.method private b(Lcom/twitter/android/PostStorage$MediaItem;)F
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/twitter/android/ot;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p1, Lcom/twitter/android/PostStorage$MediaItem;->b:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->available()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    int-to-float v0, v0

    const/high16 v2, 0x44800000    # 1024.0f

    div-float/2addr v0, v2

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    :goto_0
    return v0

    :catch_0
    move-exception v1

    move-object v1, v0

    :goto_1
    const/4 v0, 0x0

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_2
    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/util/Collection;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/ot;->d:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/PostStorage$MediaItem;

    invoke-direct {p0, v0}, Lcom/twitter/android/ot;->a(Lcom/twitter/android/PostStorage$MediaItem;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ot;->d:Ljava/util/Collection;

    return-object v0
.end method

.method protected a(Ljava/util/Collection;)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/ot;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/AttachMediaListener;

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/PostStorage$MediaItem;

    if-nez v0, :cond_0

    invoke-virtual {v1}, Lcom/twitter/android/PostStorage$MediaItem;->c()V

    goto :goto_0

    :cond_0
    iget v3, v1, Lcom/twitter/android/PostStorage$MediaItem;->c:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    sget-object v3, Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;->a:Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;

    invoke-interface {v0, v3, v1}, Lcom/twitter/android/AttachMediaListener;->a(Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;Lcom/twitter/android/PostStorage$MediaItem;)V

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    iput v3, v1, Lcom/twitter/android/PostStorage$MediaItem;->c:I

    invoke-interface {v0, v1}, Lcom/twitter/android/AttachMediaListener;->a(Lcom/twitter/android/PostStorage$MediaItem;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/ot;->a:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0}, Lcom/twitter/android/PhotoSelectHelper;->e()V

    return-void
.end method

.method public a()Z
    .locals 2

    const/4 v1, 0x0

    new-array v0, v1, [Ljava/lang/Void;

    invoke-virtual {p0, v0}, Lcom/twitter/android/ot;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return v1
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ot;->cancel(Z)Z

    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/ot;->a([Ljava/lang/Void;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/twitter/android/ot;->a(Ljava/util/Collection;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/ot;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/AttachMediaListener;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/ot;->d:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/PostStorage$MediaItem;

    iget-object v1, v1, Lcom/twitter/android/PostStorage$MediaItem;->a:Landroid/net/Uri;

    sget-object v3, Lcom/twitter/android/AttachMediaListener$MediaAttachProgress;->c:Lcom/twitter/android/AttachMediaListener$MediaAttachProgress;

    invoke-interface {v0, v1, v3}, Lcom/twitter/android/AttachMediaListener;->a(Landroid/net/Uri;Lcom/twitter/android/AttachMediaListener$MediaAttachProgress;)V

    goto :goto_0

    :cond_0
    return-void
.end method
