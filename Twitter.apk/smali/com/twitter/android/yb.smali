.class public Lcom/twitter/android/yb;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/widget/aa;


# instance fields
.field private a:Z

.field protected final b:Ljava/lang/ref/WeakReference;

.field protected final c:Lcom/twitter/android/client/c;

.field protected final d:Lcom/twitter/library/client/aa;

.field protected final e:Lcom/twitter/library/scribe/ScribeAssociation;

.field protected final f:Ljava/lang/String;

.field protected final g:Ljava/lang/String;

.field protected final h:Ljava/lang/String;

.field protected final i:Ljava/lang/String;

.field protected final j:Ljava/lang/String;

.field protected final k:Landroid/net/Uri;

.field protected final l:[Ljava/lang/String;

.field protected final m:Ljava/lang/String;

.field protected final n:[Ljava/lang/String;

.field protected final o:Ljava/lang/String;

.field protected p:Lcom/twitter/library/client/Session;

.field protected q:Lcom/twitter/library/scribe/ScribeAssociation;

.field private r:Ljava/lang/String;

.field private final s:Lcom/twitter/android/vn;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    const/4 v3, 0x0

    const-string/jumbo v6, "tweet:::platform_photo_card:click"

    const-string/jumbo v7, "tweet:::platform_player_card:click"

    const-string/jumbo v8, "tweet:::platform_summary_card:open_link"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/yb;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 14

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v13}, Lcom/twitter/android/yb;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/yb;->r:Ljava/lang/String;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/yb;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/yb;->c:Lcom/twitter/android/client/c;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/yb;->d:Lcom/twitter/library/client/aa;

    iput-object p2, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/yb;->a:Z

    iput-object p3, p0, Lcom/twitter/android/yb;->r:Ljava/lang/String;

    iput-object p4, p0, Lcom/twitter/android/yb;->f:Ljava/lang/String;

    iput-object p5, p0, Lcom/twitter/android/yb;->g:Ljava/lang/String;

    iput-object p6, p0, Lcom/twitter/android/yb;->h:Ljava/lang/String;

    iput-object p7, p0, Lcom/twitter/android/yb;->i:Ljava/lang/String;

    iput-object p8, p0, Lcom/twitter/android/yb;->j:Ljava/lang/String;

    iput-object p9, p0, Lcom/twitter/android/yb;->k:Landroid/net/Uri;

    iput-object p10, p0, Lcom/twitter/android/yb;->l:[Ljava/lang/String;

    iput-object p11, p0, Lcom/twitter/android/yb;->m:Ljava/lang/String;

    iput-object p12, p0, Lcom/twitter/android/yb;->n:[Ljava/lang/String;

    iput-object p13, p0, Lcom/twitter/android/yb;->o:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/yb;->d:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/yb;->p:Lcom/twitter/library/client/Session;

    new-instance v0, Lcom/twitter/android/vn;

    invoke-direct {v0, p1, p2}, Lcom/twitter/android/vn;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;)V

    iput-object v0, p0, Lcom/twitter/android/yb;->s:Lcom/twitter/android/vn;

    return-void
.end method

.method private static a(Lcom/twitter/library/provider/Tweet;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->n()Lcom/twitter/library/api/TwitterStatusCard;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->n()Lcom/twitter/library/api/TwitterStatusCard;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    invoke-virtual {v0}, Lcom/twitter/library/card/instance/CardInstanceData;->c()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private a(Lcom/twitter/library/provider/Tweet;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-static {p1}, Lcom/twitter/library/provider/Tweet;->b(Lcom/twitter/library/provider/Tweet;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-static {v1, v0, p2, p3}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/card/Card;)V
    .locals 2

    const-string/jumbo v0, "dial_phone"

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/android/yb;->c(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/card/Card;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/yb;->c:Lcom/twitter/android/client/c;

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method private a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/card/Card;Ljava/lang/String;)V
    .locals 6

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/yb;->c(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/card/Card;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/scribe/ScribeLog;->c()Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v0, Lcom/twitter/library/scribe/ScribeItem;->n:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/yb;->c:Lcom/twitter/android/client/c;

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->h()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/twitter/android/yb;->c:Lcom/twitter/android/client/c;

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->g()Lcom/twitter/library/api/b;

    move-result-object v4

    const/4 v0, 0x0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-static {v2, v3}, Lcom/twitter/library/util/Util;->d(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "3"

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_1
    if-eqz v4, :cond_2

    const-string/jumbo v2, "6"

    invoke-virtual {v4}, Lcom/twitter/library/api/b;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {v4}, Lcom/twitter/library/api/b;->b()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Z)Lcom/twitter/library/scribe/ScribeLog;

    :cond_2
    iget-object v2, p0, Lcom/twitter/android/yb;->c:Lcom/twitter/android/client/c;

    invoke-virtual {v2, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/yb;->c(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/card/Card;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    const-string/jumbo v2, "app_download_client_event"

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->g(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    if-eqz v3, :cond_3

    const-string/jumbo v2, "4"

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_3
    if-eqz v0, :cond_4

    const-string/jumbo v2, "3"

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/yb;->c:Lcom/twitter/android/client/c;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/scribe/ScribeLog;)V

    goto :goto_0
.end method

.method private a(Landroid/support/v4/app/Fragment;Lcom/twitter/library/provider/Tweet;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-static {}, Lcom/twitter/android/card/n;->e()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-eqz p5, :cond_0

    new-instance v2, Lcom/twitter/library/util/MediaDescriptor;

    invoke-direct {v2, p5, v6}, Lcom/twitter/library/util/MediaDescriptor;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/twitter/android/MediaPlayerActivity;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "image_url"

    invoke-virtual {v2, v3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "aud"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "is_looping"

    invoke-virtual {v3, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "simple_controls"

    invoke-virtual {v3, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "player_url"

    invoke-virtual {v3, v4, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "player_stream_urls"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v3, "tweet"

    invoke-virtual {v1, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v3, "video_position"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v3, "video_index"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {v0, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/TweetActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-wide v1, p2, Lcom/twitter/library/provider/Tweet;->u:J

    iget-object v3, p0, Lcom/twitter/android/yb;->p:Lcom/twitter/library/client/Session;

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-static {v1, v2, v3, v4}, Lcom/twitter/library/provider/w;->b(JJ)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    iget-object v2, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private a(Lcom/twitter/library/scribe/ScribeLog;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/yb;->c:Lcom/twitter/android/client/c;

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/library/scribe/ScribeLog;->c()Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v1

    const-string/jumbo v2, "app_download_client_event"

    invoke-virtual {p1, v2}, Lcom/twitter/library/scribe/ScribeLog;->g(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v1, v1, Lcom/twitter/library/scribe/ScribeItem;->n:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/twitter/library/util/Util;->d(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "3"

    invoke-virtual {p1, v2, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    const-string/jumbo v1, "4"

    invoke-virtual {p1, v1, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/yb;->c:Lcom/twitter/android/client/c;

    invoke-virtual {v0, p1}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method private b(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/card/Card;Ljava/lang/String;)V
    .locals 8

    const/4 v5, 0x0

    iget-object v0, p2, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "open_link"

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/android/yb;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/card/Card;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/yb;->d:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    iget-object v7, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p1

    move-object v1, p2

    move-object v2, p4

    move-object v6, v5

    invoke-static/range {v0 .. v7}, Lcom/twitter/android/client/be;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    return-void
.end method

.method private c(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/card/Card;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 8

    const/4 v7, 0x0

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lcom/twitter/library/card/Card;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "platform_forward_card"

    :goto_0
    iget-object v2, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/twitter/library/scribe/ScribeAssociation;->b()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-static {p2}, Lcom/twitter/library/provider/Tweet;->b(Lcom/twitter/library/provider/Tweet;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v5, p0, Lcom/twitter/android/yb;->d:Lcom/twitter/library/client/aa;

    invoke-virtual {v5}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    const/4 v1, 0x1

    aput-object v7, v5, v1

    const/4 v1, 0x2

    aput-object v3, v5, v1

    const/4 v1, 0x3

    aput-object v0, v5, v1

    const/4 v0, 0x4

    aput-object p4, v5, v0

    invoke-virtual {v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, p1, p2, v2, v7}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v0, "platform_card"

    goto :goto_0

    :cond_1
    const-string/jumbo v1, "home"

    goto :goto_1
.end method


# virtual methods
.method protected a(I)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/yb;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/yb;->c:Lcom/twitter/android/client/c;

    invoke-virtual {v1}, Lcom/twitter/android/client/c;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/twitter/android/util/u;->a(Landroid/content/Context;)Lcom/twitter/android/util/u;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/util/u;->a(I)I

    :cond_0
    return-void
.end method

.method public a(JJLjava/lang/String;Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/widget/TweetView;)V
    .locals 7

    iget-object v0, p0, Lcom/twitter/android/yb;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/support/v4/app/Fragment;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/twitter/android/yb;->c:Lcom/twitter/android/client/c;

    invoke-virtual {p7}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/library/provider/Tweet;

    move-result-object v2

    iget-object v4, p0, Lcom/twitter/android/yb;->f:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v2, p0, Lcom/twitter/android/yb;->f:Ljava/lang/String;

    :goto_0
    new-instance v4, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v5, p0, Lcom/twitter/android/yb;->p:Lcom/twitter/library/client/Session;

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-virtual {v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, p3, p4, p6, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    iget-object v4, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v2, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {p0, p7}, Lcom/twitter/android/yb;->b(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    iget-object v4, p0, Lcom/twitter/android/yb;->r:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/twitter/library/scribe/ScribeLog;->f(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    iget-object v2, p0, Lcom/twitter/android/yb;->q:Lcom/twitter/library/scribe/ScribeAssociation;

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/twitter/android/yb;->q:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-static {v2, p1, p2, v3}, Lcom/twitter/library/scribe/ScribeAssociation;->a(IJLcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v5

    :goto_1
    check-cast v1, Lcom/twitter/android/client/BaseListFragment;

    invoke-virtual {v1}, Lcom/twitter/android/client/BaseListFragment;->ab()Z

    move-result v6

    move-wide v1, p3

    move-object v3, p5

    move-object v4, p6

    invoke-static/range {v0 .. v6}, Lcom/twitter/android/ProfileActivity;->a(Landroid/content/Context;JLjava/lang/String;Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/scribe/ScribeAssociation;Z)V

    :cond_0
    return-void

    :cond_1
    const-string/jumbo v4, "avatar"

    const-string/jumbo v5, "profile_click"

    invoke-direct {p0, v2, v4, v5}, Lcom/twitter/android/yb;->a(Lcom/twitter/library/provider/Tweet;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-static {v2, p1, p2, v3}, Lcom/twitter/library/scribe/ScribeAssociation;->a(IJLcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v5

    goto :goto_1

    :cond_3
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/card/Card;I)V
    .locals 8

    const/4 v5, 0x0

    if-nez p4, :cond_4

    invoke-virtual {p2}, Lcom/twitter/library/provider/Tweet;->V()Lcom/twitter/library/card/instance/CardInstanceData;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, v1, Lcom/twitter/library/card/instance/CardInstanceData;->bindingValues:Ljava/util/HashMap;

    const-string/jumbo v2, "phone_number_dial"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/instance/BindingValue;

    if-nez v0, :cond_2

    iget-object v0, v1, Lcom/twitter/library/card/instance/CardInstanceData;->bindingValues:Ljava/util/HashMap;

    const-string/jumbo v1, "phone_number_value"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/instance/BindingValue;

    :cond_2
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/twitter/library/card/instance/BindingValue;->value:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/yb;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/card/Card;)V

    iget-object v1, p2, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v1, :cond_3

    iget-object v2, p0, Lcom/twitter/android/yb;->c:Lcom/twitter/android/client/c;

    sget-object v3, Lcom/twitter/library/api/PromotedEvent;->o:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {v2, v3, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V

    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "tel:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/android/yb;->d:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    iget-object v7, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p1

    move-object v1, p2

    move-object v6, v5

    invoke-static/range {v0 .. v7}, Lcom/twitter/android/client/be;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x1

    if-ne p4, v0, :cond_0

    invoke-virtual {p2}, Lcom/twitter/library/provider/Tweet;->aa()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1, v0}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "open_app"

    invoke-direct {p0, p1, p2, p3, v1}, Lcom/twitter/android/yb;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/card/Card;Ljava/lang/String;)V

    iget-object v1, p2, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/twitter/android/yb;->c:Lcom/twitter/android/client/c;

    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->m:Lcom/twitter/library/api/PromotedEvent;

    iget-object v3, p2, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V

    :cond_5
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_6
    invoke-static {p1, v0}, Lcom/twitter/library/util/v;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "install_app"

    invoke-direct {p0, p1, p2, p3, v1}, Lcom/twitter/android/yb;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/card/Card;Ljava/lang/String;)V

    iget-object v1, p2, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/twitter/android/yb;->c:Lcom/twitter/android/client/c;

    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->n:Lcom/twitter/library/api/PromotedEvent;

    iget-object v3, p2, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V

    :cond_7
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/android/yb;->b(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/card/Card;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public a(Landroid/support/v4/app/Fragment;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/MediaEntity;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/twitter/android/yb;->a(Landroid/support/v4/app/Fragment;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/MediaEntity;Z)V

    return-void
.end method

.method public a(Landroid/support/v4/app/Fragment;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/MediaEntity;Lcom/twitter/library/scribe/ScribeItem;)V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/yb;->p:Lcom/twitter/library/client/Session;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/yb;->h:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/yb;->h:Ljava/lang/String;

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/yb;->c:Lcom/twitter/android/client/c;

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v3, p0, Lcom/twitter/android/yb;->p:Lcom/twitter/library/client/Session;

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v5, p2, v2, v5}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/yb;->r:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->f(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p2, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/yb;->c:Lcom/twitter/android/client/c;

    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->l:Lcom/twitter/library/api/PromotedEvent;

    iget-object v2, p2, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V

    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/yb;->a(Landroid/support/v4/app/Fragment;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/MediaEntity;)V

    return-void

    :cond_2
    const-string/jumbo v0, "platform_photo_card"

    const-string/jumbo v1, "click"

    invoke-direct {p0, p2, v0, v1}, Lcom/twitter/android/yb;->a(Lcom/twitter/library/provider/Tweet;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/support/v4/app/Fragment;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/MediaEntity;Z)V
    .locals 6

    const/4 v5, 0x1

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/GalleryActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "etc"

    iget-boolean v2, p0, Lcom/twitter/android/yb;->a:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    iget-object v2, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1}, Lcom/twitter/library/scribe/ScribeAssociation;->b()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "home"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string/jumbo v1, "context"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/yb;->l:[Ljava/lang/String;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/twitter/android/yb;->k:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "prj"

    iget-object v3, p0, Lcom/twitter/android/yb;->l:[Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "sel"

    iget-object v3, p0, Lcom/twitter/android/yb;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "selArgs"

    iget-object v3, p0, Lcom/twitter/android/yb;->n:[Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "orderBy"

    iget-object v3, p0, Lcom/twitter/android/yb;->o:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "id"

    iget-wide v3, p2, Lcom/twitter/library/provider/Tweet;->u:J

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "controls"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_1
    :goto_1
    invoke-virtual {p1, v0, v5}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void

    :cond_2
    const-string/jumbo v2, "tweet"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string/jumbo v1, "context"

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    :cond_3
    const-string/jumbo v2, "profile_tweets"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string/jumbo v1, "context"

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    :cond_4
    const-string/jumbo v2, "list"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string/jumbo v1, "context"

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    :cond_5
    const-string/jumbo v2, "favorites"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string/jumbo v1, "context"

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_0

    :cond_6
    const-string/jumbo v2, "network_activity"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "context"

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_0

    :cond_7
    const-string/jumbo v1, "tw"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    if-eqz p3, :cond_8

    const-string/jumbo v1, "media"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_8
    if-eqz p4, :cond_1

    const-string/jumbo v1, "tagged_user_list"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_1
.end method

.method public a(Landroid/support/v4/app/Fragment;Lcom/twitter/library/provider/Tweet;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZ)V
    .locals 13

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    if-eqz p4, :cond_0

    new-instance v1, Lcom/twitter/library/util/MediaDescriptor;

    const/4 v2, 0x1

    move-object/from16 v0, p4

    invoke-direct {v1, v0, v2}, Lcom/twitter/library/util/MediaDescriptor;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v12, p9

    invoke-virtual/range {v1 .. v12}, Lcom/twitter/android/yb;->a(Landroid/support/v4/app/Fragment;Lcom/twitter/library/provider/Tweet;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;ZZZIIZ)V

    return-void
.end method

.method public a(Landroid/support/v4/app/Fragment;Lcom/twitter/library/provider/Tweet;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;ZZZIIZ)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/yb;->p:Lcom/twitter/library/client/Session;

    if-eqz v0, :cond_0

    if-eqz p11, :cond_2

    const-string/jumbo v0, "platform_forward_player_card"

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/yb;->i:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/twitter/android/yb;->i:Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Lcom/twitter/android/yb;->c:Lcom/twitter/android/client/c;

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v3, p0, Lcom/twitter/android/yb;->p:Lcom/twitter/library/client/Session;

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    const/4 v4, 0x0

    invoke-virtual {v0, v2, p2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/yb;->r:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->f(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p2, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/yb;->c:Lcom/twitter/android/client/c;

    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->l:Lcom/twitter/library/api/PromotedEvent;

    iget-object v2, p2, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V

    :cond_1
    if-eqz p4, :cond_5

    invoke-virtual {p4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_5

    invoke-virtual {p2}, Lcom/twitter/library/provider/Tweet;->Q()Z

    move-result v0

    if-eqz v0, :cond_4

    const-class v0, Lcom/twitter/android/AmplifyMediaPlayerActivity;

    :goto_2
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v0, "image_url"

    invoke-virtual {v1, v0, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "aud"

    invoke-virtual {v0, v2, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "is_looping"

    invoke-virtual {v0, v2, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "simple_controls"

    invoke-virtual {v0, v2, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "player_url"

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "player_stream_urls"

    invoke-virtual {v0, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "tweet"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "video_position"

    invoke-virtual {v0, v2, p10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "video_index"

    invoke-virtual {v0, v2, p9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "amplify"

    invoke-virtual {p2}, Lcom/twitter/library/provider/Tweet;->Q()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v0, 0x5

    invoke-virtual {p1, v1, v0}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_3
    return-void

    :cond_2
    const-string/jumbo v0, "platform_player_card"

    goto/16 :goto_0

    :cond_3
    const-string/jumbo v1, "click"

    invoke-direct {p0, p2, v0, v1}, Lcom/twitter/android/yb;->a(Lcom/twitter/library/provider/Tweet;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_4
    const-class v0, Lcom/twitter/android/MediaPlayerActivity;

    goto :goto_2

    :cond_5
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v4/app/Fragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v0

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0f0514    # com.twitter.android.R.string.unsupported_feature

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_3
.end method

.method public a(Landroid/support/v4/app/Fragment;Lcom/twitter/library/provider/Tweet;ZZ)V
    .locals 6

    const/4 v5, 0x0

    if-eqz p4, :cond_0

    const-string/jumbo v0, "tweet:::platform_forward_amplify_card:click"

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/yb;->c:Lcom/twitter/android/client/c;

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v3, p0, Lcom/twitter/android/yb;->p:Lcom/twitter/library/client/Session;

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v5, p2, v2, v5}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/yb;->r:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->f(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    if-nez p3, :cond_1

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/TweetActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-wide v1, p2, Lcom/twitter/library/provider/Tweet;->u:J

    iget-object v3, p0, Lcom/twitter/android/yb;->p:Lcom/twitter/library/client/Session;

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-static {v1, v2, v3, v4}, Lcom/twitter/library/provider/w;->b(JJ)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    iget-object v2, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_1
    return-void

    :cond_0
    const-string/jumbo v0, "tweet:::platform_amplify_card:click"

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/AmplifyMediaPlayerActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p2}, Lcom/twitter/android/yb;->a(Lcom/twitter/library/provider/Tweet;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "stream_url"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "tw"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/4 v1, 0x5

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1
.end method

.method public a(Lcom/twitter/library/client/Session;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/yb;->p:Lcom/twitter/library/client/Session;

    return-void
.end method

.method public a(Lcom/twitter/library/provider/Tweet;JLcom/twitter/library/widget/TweetView;)V
    .locals 11

    const/4 v10, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/yb;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0, p4}, Lcom/twitter/android/yb;->b(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/yb;->p:Lcom/twitter/library/client/Session;

    if-eqz v5, :cond_2

    const-string/jumbo v5, "media_tag_summary"

    const-string/jumbo v6, "click"

    invoke-direct {p0, p1, v5, v6}, Lcom/twitter/android/yb;->a(Lcom/twitter/library/provider/Tweet;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/yb;->c:Lcom/twitter/android/client/c;

    new-instance v7, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v8, p0, Lcom/twitter/android/yb;->p:Lcom/twitter/library/client/Session;

    invoke-virtual {v8}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    invoke-direct {v7, v8, v9}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v8, v1, [Ljava/lang/String;

    aput-object v5, v8, v2

    invoke-virtual {v7, v8}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v5

    iget-object v7, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v5, v10, p1, v7, v10}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v5

    iget-object v7, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v5, v7}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/yb;->r:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->f(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v4

    invoke-virtual {v6, v4}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_2
    iget-object v4, p1, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    invoke-virtual {v4, p2, p3}, Lcom/twitter/library/api/TweetEntities;->b(J)Lcom/twitter/library/api/MediaEntity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/api/MediaEntity;->b()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    if-ne v6, v1, :cond_4

    if-nez p4, :cond_4

    iget-object v1, p1, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/twitter/android/yb;->c:Lcom/twitter/android/client/c;

    sget-object v4, Lcom/twitter/library/api/PromotedEvent;->f:Lcom/twitter/library/api/PromotedEvent;

    iget-object v6, p1, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v1, v4, v6}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V

    :cond_3
    new-instance v4, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v4, v3, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "screen_name"

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/MediaTag;

    iget-object v1, v1, Lcom/twitter/library/api/MediaTag;->screenName:Ljava/lang/String;

    invoke-virtual {v4, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_4
    if-nez p4, :cond_5

    :goto_1
    invoke-virtual {p0, v0, p1, v4, v1}, Lcom/twitter/android/yb;->a(Landroid/support/v4/app/Fragment;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/MediaEntity;Z)V

    goto/16 :goto_0

    :cond_5
    move v1, v2

    goto :goto_1
.end method

.method public a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/MediaEntity;Lcom/twitter/library/widget/TweetView;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/yb;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p3}, Lcom/twitter/android/yb;->b(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v1

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/twitter/android/yb;->a(Landroid/support/v4/app/Fragment;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/MediaEntity;Lcom/twitter/library/scribe/ScribeItem;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/TweetClassicCard;Lcom/twitter/library/widget/TweetView;)V
    .locals 12

    const/4 v8, 0x2

    const/4 v6, 0x1

    const/4 v2, 0x0

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/twitter/android/yb;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/Fragment;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v5, p0, Lcom/twitter/android/yb;->p:Lcom/twitter/library/client/Session;

    iget v0, p2, Lcom/twitter/library/api/TweetClassicCard;->type:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0, p3}, Lcom/twitter/android/yb;->b(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    invoke-virtual {p0, v1, p1, v2, v0}, Lcom/twitter/android/yb;->a(Landroid/support/v4/app/Fragment;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/MediaEntity;Lcom/twitter/library/scribe/ScribeItem;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->Q()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, v1, p1, v6, v7}, Lcom/twitter/android/yb;->a(Landroid/support/v4/app/Fragment;Lcom/twitter/library/provider/Tweet;ZZ)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->R()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v3, p2, Lcom/twitter/library/api/TweetClassicCard;->playerUrl:Ljava/lang/String;

    iget-object v4, p2, Lcom/twitter/library/api/TweetClassicCard;->imageUrl:Ljava/lang/String;

    iget-object v5, p2, Lcom/twitter/library/api/TweetClassicCard;->playerStreamUrl:Ljava/lang/String;

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/yb;->a(Landroid/support/v4/app/Fragment;Lcom/twitter/library/provider/Tweet;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/yb;->c:Lcom/twitter/android/client/c;

    iget v0, v0, Lcom/twitter/android/client/c;->d:F

    invoke-virtual {p2, v0}, Lcom/twitter/library/api/TweetClassicCard;->a(F)Lcom/twitter/library/api/aa;

    move-result-object v0

    iget-object v3, p2, Lcom/twitter/library/api/TweetClassicCard;->playerUrl:Ljava/lang/String;

    iget-object v4, p2, Lcom/twitter/library/api/TweetClassicCard;->playerStreamUrl:Ljava/lang/String;

    iget-object v5, v0, Lcom/twitter/library/api/aa;->a:Ljava/lang/String;

    iget v0, p2, Lcom/twitter/library/api/TweetClassicCard;->playerType:I

    if-ne v0, v8, :cond_4

    :goto_1
    move-object v0, p0

    move-object v2, p1

    move v8, v7

    move v9, v7

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/android/yb;->a(Landroid/support/v4/app/Fragment;Lcom/twitter/library/provider/Tweet;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZ)V

    goto :goto_0

    :cond_4
    move v6, v7

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/yb;->p:Lcom/twitter/library/client/Session;

    if-eqz v0, :cond_6

    iget-object v0, p2, Lcom/twitter/library/api/TweetClassicCard;->url:Ljava/lang/String;

    if-eqz v0, :cond_e

    iget-object v0, p1, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    if-eqz v0, :cond_e

    iget-object v0, p1, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v0, v0, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    if-eqz v0, :cond_e

    iget-object v0, p1, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v0, v0, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/UrlEntity;

    iget-object v8, p2, Lcom/twitter/library/api/TweetClassicCard;->url:Ljava/lang/String;

    iget-object v9, v0, Lcom/twitter/library/api/UrlEntity;->url:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    iget-object v0, v0, Lcom/twitter/library/api/UrlEntity;->expandedUrl:Ljava/lang/String;

    :goto_2
    iget-object v3, p0, Lcom/twitter/android/yb;->j:Ljava/lang/String;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/twitter/android/yb;->j:Ljava/lang/String;

    :goto_3
    iget-object v8, p0, Lcom/twitter/android/yb;->c:Lcom/twitter/android/client/c;

    new-instance v9, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v5, v6, [Ljava/lang/String;

    aput-object v3, v5, v7

    invoke-virtual {v9, v5}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    iget-object v5, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v3, v2, p1, v5, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    iget-object v3, p2, Lcom/twitter/library/api/TweetClassicCard;->url:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lcom/twitter/library/scribe/ScribeLog;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/yb;->r:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->f(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_6
    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->V()Lcom/twitter/library/card/instance/CardInstanceData;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, v0, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/library/api/TwitterStatusCard;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/twitter/android/TweetActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-wide v2, p1, Lcom/twitter/library/provider/Tweet;->u:J

    iget-object v4, p0, Lcom/twitter/android/yb;->p:Lcom/twitter/library/client/Session;

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Lcom/twitter/library/provider/w;->b(JJ)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "association"

    iget-object v3, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_7
    const-string/jumbo v3, "platform_summary_card"

    const-string/jumbo v8, "open_link"

    invoke-direct {p0, p1, v3, v8}, Lcom/twitter/android/yb;->a(Lcom/twitter/library/provider/Tweet;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    :cond_8
    :try_start_0
    new-instance v0, Lcom/twitter/android/yc;

    invoke-direct {v0, p0, v1, p2}, Lcom/twitter/android/yc;-><init>(Lcom/twitter/android/yb;Landroid/support/v4/app/Fragment;Lcom/twitter/library/api/TweetClassicCard;)V

    iget-object v2, p0, Lcom/twitter/android/yb;->c:Lcom/twitter/android/client/c;

    invoke-virtual {v2}, Lcom/twitter/android/client/c;->ah()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/twitter/android/client/be;->a(Landroid/content/Context;Lcom/twitter/android/client/ak;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const v0, 0x7f0f0514    # com.twitter.android.R.string.unsupported_feature

    invoke-static {v4, v0, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_9
    :try_start_1
    invoke-interface {v0}, Lcom/twitter/android/client/ak;->a()V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :pswitch_3
    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->Y()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->V()Lcom/twitter/library/card/instance/CardInstanceData;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/twitter/library/card/instance/CardInstanceData;->bindingValues:Ljava/util/HashMap;

    const-string/jumbo v1, "website_url"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/instance/BindingValue;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/twitter/library/card/instance/BindingValue;->value:Ljava/lang/Object;

    if-eqz v0, :cond_0

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    new-instance v1, Lcom/twitter/library/api/UrlEntity;

    invoke-direct {v1}, Lcom/twitter/library/api/UrlEntity;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/twitter/library/api/UrlEntity;->url:Ljava/lang/String;

    invoke-virtual {p0, p1, v1}, Lcom/twitter/android/yb;->b(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/UrlEntity;)V

    goto/16 :goto_0

    :cond_a
    if-eqz v5, :cond_b

    iget-object v0, p0, Lcom/twitter/android/yb;->c:Lcom/twitter/android/client/c;

    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v4, v6, [Ljava/lang/String;

    const-string/jumbo v5, "tweet:::platform_promotion_card:click"

    aput-object v5, v4, v7

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v3, v2, p1, v4, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/yb;->r:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->f(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_b
    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->N()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/twitter/android/yb;->c:Lcom/twitter/android/client/c;

    iget v0, v0, Lcom/twitter/android/client/c;->d:F

    invoke-virtual {p2, v0}, Lcom/twitter/library/api/TweetClassicCard;->a(F)Lcom/twitter/library/api/aa;

    move-result-object v0

    iget-object v3, p2, Lcom/twitter/library/api/TweetClassicCard;->playerUrl:Ljava/lang/String;

    iget-object v4, p2, Lcom/twitter/library/api/TweetClassicCard;->playerStreamUrl:Ljava/lang/String;

    iget-object v5, v0, Lcom/twitter/library/api/aa;->a:Ljava/lang/String;

    iget v0, p2, Lcom/twitter/library/api/TweetClassicCard;->playerType:I

    if-ne v0, v8, :cond_c

    :goto_4
    move-object v0, p0

    move-object v2, p1

    move v8, v7

    move v9, v7

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/android/yb;->a(Landroid/support/v4/app/Fragment;Lcom/twitter/library/provider/Tweet;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZ)V

    goto/16 :goto_0

    :cond_c
    move v6, v7

    goto :goto_4

    :cond_d
    invoke-virtual {p0, v1, p1, v2, v2}, Lcom/twitter/android/yb;->a(Landroid/support/v4/app/Fragment;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/MediaEntity;Lcom/twitter/library/scribe/ScribeItem;)V

    goto/16 :goto_0

    :cond_e
    move-object v0, v2

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/UrlEntity;)V
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0}, Lcom/twitter/library/scribe/ScribeAssociation;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {v0}, Lcom/twitter/library/scribe/ScribeAssociation;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    const/4 v2, 0x2

    const-string/jumbo v3, ":attribution:open_link"

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/yb;->c:Lcom/twitter/android/client/c;

    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v4, p0, Lcom/twitter/android/yb;->p:Lcom/twitter/library/client/Session;

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v4, v7, [Ljava/lang/String;

    aput-object v1, v4, v6

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v1, v8, p1, v0, v8}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/yb;->b(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/UrlEntity;)V

    return-void
.end method

.method public a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/widget/TweetView;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/widget/TweetView;I)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/scribe/ScribeAssociation;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/yb;->q:Lcom/twitter/library/scribe/ScribeAssociation;

    return-void
.end method

.method public a(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/widget/TweetView;)V
    .locals 2

    const/4 v1, 0x2

    iget-object v0, p0, Lcom/twitter/android/yb;->s:Lcom/twitter/android/vn;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/library/view/TweetActionType;->c:Lcom/twitter/library/view/TweetActionType;

    if-ne p1, v0, :cond_1

    invoke-virtual {p0, v1}, Lcom/twitter/android/yb;->a(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/twitter/library/view/TweetActionType;->d:Lcom/twitter/library/view/TweetActionType;

    if-ne p1, v0, :cond_2

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/yb;->a(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v1}, Lcom/twitter/android/yb;->a(I)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/widget/TweetView;)V
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-virtual {p1}, Lcom/twitter/library/widget/TweetView;->n()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/yb;->p:Lcom/twitter/library/client/Session;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0}, Lcom/twitter/library/scribe/ScribeAssociation;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {v0}, Lcom/twitter/library/scribe/ScribeAssociation;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    const/4 v2, 0x2

    const-string/jumbo v3, ":tweet:double_click"

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/yb;->c:Lcom/twitter/android/client/c;

    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v4, p0, Lcom/twitter/android/yb;->p:Lcom/twitter/library/client/Session;

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v4, v7, [Ljava/lang/String;

    aput-object v1, v4, v6

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {p1}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/library/provider/Tweet;

    move-result-object v3

    invoke-virtual {v1, v8, v3, v0, v8}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_0
    invoke-virtual {p1}, Lcom/twitter/library/widget/TweetView;->m()V

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/yb;->a:Z

    return-void
.end method

.method public a()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/yb;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/network/OAuthToken;Lcom/twitter/library/provider/Tweet;Ljava/util/HashMap;Lcom/twitter/library/scribe/ScribeAssociation;)Z
    .locals 8

    iget-object v0, p0, Lcom/twitter/android/yb;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz p2, :cond_2

    iget-object v3, p2, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    if-eqz v3, :cond_2

    const-string/jumbo v3, "impression_id"

    iget-object v4, p2, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v3, p0, Lcom/twitter/android/yb;->c:Lcom/twitter/android/client/c;

    new-instance v4, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v5, p0, Lcom/twitter/android/yb;->p:Lcom/twitter/library/client/Session;

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string/jumbo v7, "tweet:::platform_promotion_card:open_link"

    aput-object v7, v5, v6

    invoke-virtual {v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4, v5, p4, p6, v6}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v4

    invoke-virtual {v4, p6}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, p1, v5}, Lcom/twitter/library/scribe/ScribeLog;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/yb;->r:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->f(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/twitter/android/WebViewActivity;

    invoke-direct {v3, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "token"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "headers"

    invoke-virtual {v1, v2, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->startActivity(Landroid/content/Intent;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected b(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/library/scribe/ScribeItem;
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/widget/TweetView;->getScribeItem()Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/UrlEntity;)V
    .locals 9

    iget-object v0, p0, Lcom/twitter/android/yb;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/support/v4/app/Fragment;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->t()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v1, p2, Lcom/twitter/library/api/UrlEntity;->expandedUrl:Ljava/lang/String;

    iget-object v2, p1, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    iget-object v0, p0, Lcom/twitter/android/yb;->d:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->h()Lcom/twitter/library/network/OAuthToken;

    move-result-object v3

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p0

    move-object v4, p1

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/yb;->a(Ljava/lang/String;Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/network/OAuthToken;Lcom/twitter/library/provider/Tweet;Ljava/util/HashMap;Lcom/twitter/library/scribe/ScribeAssociation;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p2, Lcom/twitter/library/api/UrlEntity;->url:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/twitter/library/provider/Tweet;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, v1, p1, v0, v2}, Lcom/twitter/android/yb;->a(Landroid/support/v4/app/Fragment;Lcom/twitter/library/provider/Tweet;ZZ)V

    goto :goto_0

    :cond_2
    iget-object v1, p1, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    if-eqz v1, :cond_3

    iget-object v1, p1, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    iget-object v1, v1, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    const/4 v3, 0x0

    invoke-static {v1, p1, v2, v3}, Lcom/twitter/library/scribe/ScribeItem;->a(Landroid/content/Context;Lcom/twitter/library/provider/ParcelableTweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/library/scribe/ScribeItem;->n:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v2, p0, Lcom/twitter/android/yb;->p:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/yb;->g:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, p1, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    iget-object v2, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    iget-object v2, p0, Lcom/twitter/android/yb;->r:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->f(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {p0, v1}, Lcom/twitter/android/yb;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_3
    iget-object v1, p0, Lcom/twitter/android/yb;->p:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    iget-object v5, p0, Lcom/twitter/android/yb;->g:Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    iget-object v8, p0, Lcom/twitter/android/yb;->r:Ljava/lang/String;

    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v8}, Lcom/twitter/android/client/be;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/UrlEntity;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/yb;->p:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    iget-object v5, p0, Lcom/twitter/android/yb;->g:Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/twitter/android/yb;->e:Lcom/twitter/library/scribe/ScribeAssociation;

    iget-object v8, p0, Lcom/twitter/android/yb;->r:Ljava/lang/String;

    move-object v2, p2

    invoke-static/range {v0 .. v8}, Lcom/twitter/android/client/be;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/UrlEntity;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/widget/TweetView;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/yb;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {p2}, Lcom/twitter/library/widget/TweetView;->getPivotIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/widget/TweetView;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/yb;->s:Lcom/twitter/android/vn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/yb;->s:Lcom/twitter/android/vn;

    invoke-virtual {p2}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/library/provider/Tweet;

    move-result-object v1

    invoke-virtual {p2}, Lcom/twitter/library/widget/TweetView;->getFriendshipCache()Lcom/twitter/library/util/FriendshipCache;

    move-result-object v2

    invoke-virtual {p0, p2}, Lcom/twitter/android/yb;->b(Lcom/twitter/library/widget/TweetView;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v3

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/twitter/android/vn;->a(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/util/FriendshipCache;Lcom/twitter/library/scribe/ScribeItem;)V

    :cond_0
    return-void
.end method
