.class Lcom/twitter/android/fw;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/ExperimentSettingsActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/ExperimentSettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/fw;->a:Lcom/twitter/android/ExperimentSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/fw;->a:Lcom/twitter/android/ExperimentSettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/ExperimentSettingsActivity;->e(Lcom/twitter/android/ExperimentSettingsActivity;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v0, p0, Lcom/twitter/android/fw;->a:Lcom/twitter/android/ExperimentSettingsActivity;

    iget-object v1, p0, Lcom/twitter/android/fw;->a:Lcom/twitter/android/ExperimentSettingsActivity;

    invoke-static {v1}, Lcom/twitter/android/ExperimentSettingsActivity;->f(Lcom/twitter/android/ExperimentSettingsActivity;)Lcom/twitter/android/client/c;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/client/c;->l(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/ExperimentSettingsActivity;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/fw;->a:Lcom/twitter/android/ExperimentSettingsActivity;

    iget-object v1, p0, Lcom/twitter/android/fw;->a:Lcom/twitter/android/ExperimentSettingsActivity;

    invoke-static {v1}, Lcom/twitter/android/ExperimentSettingsActivity;->h(Lcom/twitter/android/ExperimentSettingsActivity;)Lcom/twitter/android/client/c;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/fw;->a:Lcom/twitter/android/ExperimentSettingsActivity;

    invoke-static {v2}, Lcom/twitter/android/ExperimentSettingsActivity;->g(Lcom/twitter/android/ExperimentSettingsActivity;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/client/c;->l(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/ExperimentSettingsActivity;->a(Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method
