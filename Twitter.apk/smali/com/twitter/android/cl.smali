.class Lcom/twitter/android/cl;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/ChangePasswordActivity;


# direct methods
.method private constructor <init>(Lcom/twitter/android/ChangePasswordActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/cl;->a:Lcom/twitter/android/ChangePasswordActivity;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/ChangePasswordActivity;Lcom/twitter/android/ck;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/cl;-><init>(Lcom/twitter/android/ChangePasswordActivity;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;I[ILjava/lang/String;)V
    .locals 6

    const/4 v5, 0x0

    const-string/jumbo v0, "OK"

    invoke-virtual {p5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/cl;->a:Lcom/twitter/android/ChangePasswordActivity;

    invoke-static {v0}, Lcom/twitter/android/ChangePasswordActivity;->a(Lcom/twitter/android/ChangePasswordActivity;)Landroid/widget/EditText;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/cl;->a:Lcom/twitter/android/ChangePasswordActivity;

    invoke-static {v0}, Lcom/twitter/android/ChangePasswordActivity;->b(Lcom/twitter/android/ChangePasswordActivity;)Landroid/widget/EditText;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/cl;->a:Lcom/twitter/android/ChangePasswordActivity;

    invoke-static {v0}, Lcom/twitter/android/ChangePasswordActivity;->c(Lcom/twitter/android/ChangePasswordActivity;)Landroid/widget/EditText;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const-string/jumbo v0, "settings:change_password::change_password:success"

    const v1, 0x7f0f02ef    # com.twitter.android.R.string.password_change_success

    :goto_0
    iget-object v2, p0, Lcom/twitter/android/cl;->a:Lcom/twitter/android/ChangePasswordActivity;

    invoke-virtual {v2}, Lcom/twitter/android/ChangePasswordActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    iget-object v1, p0, Lcom/twitter/android/cl;->a:Lcom/twitter/android/ChangePasswordActivity;

    invoke-static {v1}, Lcom/twitter/android/ChangePasswordActivity;->d(Lcom/twitter/android/ChangePasswordActivity;)Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    aput-object v0, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {p4}, Lcom/twitter/android/ChangePasswordActivity;->a([I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    const-string/jumbo v0, "settings:change_password::change_password:failure"

    const v1, 0x7f0f02ec    # com.twitter.android.R.string.password_change_failure

    goto :goto_0

    :sswitch_0
    const-string/jumbo v0, "settings:change_password::change_password:wrong_old"

    const v1, 0x7f0f02ee    # com.twitter.android.R.string.password_change_failure_wrong_old

    goto :goto_0

    :sswitch_1
    const-string/jumbo v0, "settings:change_password::change_password:minimum_length"

    const v1, 0x7f0f0443    # com.twitter.android.R.string.signup_error_password

    goto :goto_0

    :sswitch_2
    const-string/jumbo v0, "settings:change_password::change_password:weak"

    const v1, 0x7f0f02ed    # com.twitter.android.R.string.password_change_failure_too_week

    goto :goto_0

    :sswitch_3
    const-string/jumbo v0, "settings:change_password::change_password:mismatch"

    const v1, 0x7f0f02f0    # com.twitter.android.R.string.password_mismatch

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x3c -> :sswitch_3
        0x3e -> :sswitch_1
        0x72 -> :sswitch_0
        0xee -> :sswitch_2
    .end sparse-switch
.end method
