.class Lcom/twitter/android/mb;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Z

.field public final g:J

.field public final h:Z

.field public i:Lcom/twitter/library/api/MediaEntity;

.field public j:Ljava/lang/StringBuilder;

.field public k:Z

.field final synthetic l:Lcom/twitter/android/lx;


# direct methods
.method public constructor <init>(Lcom/twitter/android/lx;Landroid/database/Cursor;)V
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x1

    iput-object p1, p0, Lcom/twitter/android/mb;->l:Lcom/twitter/android/lx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/twitter/android/mb;->k:Z

    const/16 v2, 0xd

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/mb;->a:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {p1}, Lcom/twitter/android/lx;->a(Lcom/twitter/android/lx;)Lcom/twitter/library/client/aa;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    cmp-long v4, v2, v4

    if-nez v4, :cond_2

    const/4 v2, 0x3

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/android/mb;->b:J

    const/4 v2, 0x4

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/mb;->e:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/mb;->d:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/mb;->c:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/twitter/android/mb;->h:Z

    :goto_0
    const/16 v2, 0xe

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-lez v2, :cond_0

    move v0, v1

    :cond_0
    iput-boolean v0, p0, Lcom/twitter/android/mb;->f:Z

    const/16 v0, 0xb

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/android/mb;->g:J

    new-instance v0, Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/mb;->j:Ljava/lang/StringBuilder;

    const/16 v0, 0xc

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TweetEntities;

    invoke-static {p1}, Lcom/twitter/android/lx;->b(Lcom/twitter/android/lx;)Lcom/twitter/android/client/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/client/c;->L()Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/TweetEntities;->a(I)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/TweetEntities;->b(I)Lcom/twitter/library/api/MediaEntity;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/mb;->i:Lcom/twitter/library/api/MediaEntity;

    iget-object v2, p0, Lcom/twitter/android/mb;->j:Ljava/lang/StringBuilder;

    invoke-static {v2, v0}, Lcom/twitter/android/lw;->a(Ljava/lang/StringBuilder;Lcom/twitter/library/api/TweetEntities;)V

    iget-object v2, p0, Lcom/twitter/android/mb;->j:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/twitter/android/lx;->c(Lcom/twitter/android/lx;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0f0114    # com.twitter.android.R.string.dm_attachment_image

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/twitter/android/mb;->j:Ljava/lang/StringBuilder;

    iput-boolean v1, p0, Lcom/twitter/android/mb;->k:Z

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/mb;->j:Ljava/lang/StringBuilder;

    invoke-static {v1, v0}, Lcom/twitter/library/api/TweetEntities;->a(Ljava/lang/StringBuilder;Lcom/twitter/library/api/TweetEntities;)Ljava/lang/StringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/mb;->j:Ljava/lang/StringBuilder;

    return-void

    :cond_2
    iput-wide v2, p0, Lcom/twitter/android/mb;->b:J

    const/16 v2, 0x8

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/mb;->e:Ljava/lang/String;

    const/16 v2, 0x9

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/mb;->d:Ljava/lang/String;

    const/16 v2, 0xa

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/mb;->c:Ljava/lang/String;

    iput-boolean v0, p0, Lcom/twitter/android/mb;->h:Z

    goto/16 :goto_0
.end method
