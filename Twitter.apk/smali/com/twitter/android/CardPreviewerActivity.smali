.class public Lcom/twitter/android/CardPreviewerActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/twitter/android/bu;

.field private c:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/CardPreviewerActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/CardPreviewerActivity;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const v1, 0x7f030022    # com.twitter.android.R.layout.card_previewer_activity

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/library/client/e;)V
    .locals 2

    const v0, 0x7f0f00fd    # com.twitter.android.R.string.developer_card_previewer_title

    invoke-virtual {p0, v0}, Lcom/twitter/android/CardPreviewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/CardPreviewerActivity;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/twitter/android/CardPreviewerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "host"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/CardPreviewerActivity;->a:Ljava/lang/String;

    new-instance v0, Lcom/twitter/android/bu;

    invoke-virtual {p0}, Lcom/twitter/android/CardPreviewerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/bu;-><init>(Lcom/twitter/android/CardPreviewerActivity;Landroid/support/v4/app/FragmentManager;)V

    iput-object v0, p0, Lcom/twitter/android/CardPreviewerActivity;->b:Lcom/twitter/android/bu;

    const v0, 0x7f0900bb    # com.twitter.android.R.id.pager

    invoke-virtual {p0, v0}, Lcom/twitter/android/CardPreviewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/twitter/android/CardPreviewerActivity;->c:Landroid/support/v4/view/ViewPager;

    iget-object v0, p0, Lcom/twitter/android/CardPreviewerActivity;->c:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/twitter/android/CardPreviewerActivity;->b:Lcom/twitter/android/bu;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    return-void
.end method

.method public onStart()V
    .locals 0

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onStart()V

    invoke-static {}, Lcom/twitter/library/card/CardDebugLog;->a()V

    invoke-static {}, Lcom/twitter/library/card/CardDebugLog;->c()V

    return-void
.end method

.method public onStop()V
    .locals 0

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onStop()V

    invoke-static {}, Lcom/twitter/library/card/CardDebugLog;->d()V

    invoke-static {}, Lcom/twitter/library/card/CardDebugLog;->b()V

    return-void
.end method
