.class public Lcom/twitter/android/ok;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:J

.field public final b:Lcom/twitter/library/provider/Tweet;

.field public final c:Lcom/twitter/library/api/MediaEntity;

.field public final d:Lcom/twitter/library/util/m;

.field public final e:F


# direct methods
.method constructor <init>(JLcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/MediaEntity;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/twitter/android/ok;->a:J

    iput-object p3, p0, Lcom/twitter/android/ok;->b:Lcom/twitter/library/provider/Tweet;

    iput-object p4, p0, Lcom/twitter/android/ok;->c:Lcom/twitter/library/api/MediaEntity;

    new-instance v0, Lcom/twitter/library/util/m;

    iget-object v1, p4, Lcom/twitter/library/api/MediaEntity;->mediaUrl:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/ok;->d:Lcom/twitter/library/util/m;

    iget v0, p4, Lcom/twitter/library/api/MediaEntity;->width:I

    int-to-float v0, v0

    iget v1, p4, Lcom/twitter/library/api/MediaEntity;->height:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/twitter/android/ok;->e:F

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/android/ok;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/twitter/android/ok;

    iget-wide v2, p0, Lcom/twitter/android/ok;->a:J

    iget-wide v4, p1, Lcom/twitter/android/ok;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget v2, p1, Lcom/twitter/android/ok;->e:F

    iget v3, p0, Lcom/twitter/android/ok;->e:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/twitter/android/ok;->c:Lcom/twitter/library/api/MediaEntity;

    iget-object v3, p1, Lcom/twitter/android/ok;->c:Lcom/twitter/library/api/MediaEntity;

    invoke-virtual {v2, v3}, Lcom/twitter/library/api/MediaEntity;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/twitter/android/ok;->d:Lcom/twitter/library/util/m;

    iget-object v3, p1, Lcom/twitter/android/ok;->d:Lcom/twitter/library/util/m;

    invoke-virtual {v2, v3}, Lcom/twitter/library/util/m;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lcom/twitter/android/ok;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v3, p1, Lcom/twitter/android/ok;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v2, v3}, Lcom/twitter/library/provider/Tweet;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    iget-wide v0, p0, Lcom/twitter/android/ok;->a:J

    iget-wide v2, p0, Lcom/twitter/android/ok;->a:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/android/ok;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v1}, Lcom/twitter/library/provider/Tweet;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/android/ok;->c:Lcom/twitter/library/api/MediaEntity;

    invoke-virtual {v1}, Lcom/twitter/library/api/MediaEntity;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/twitter/android/ok;->d:Lcom/twitter/library/util/m;

    invoke-virtual {v1}, Lcom/twitter/library/util/m;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget v0, p0, Lcom/twitter/android/ok;->e:F

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/ok;->e:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
