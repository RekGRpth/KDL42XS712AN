.class final enum Lcom/twitter/android/OneFactorLoginActivity$LoginState$3;
.super Lcom/twitter/android/OneFactorLoginActivity$LoginState;
.source "Twttr"


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/OneFactorLoginActivity$LoginState;-><init>(Ljava/lang/String;ILcom/twitter/android/od;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/OneFactorLoginActivity;)V
    .locals 6

    const v1, 0x7f0f02e0    # com.twitter.android.R.string.one_factor_enter_sms_code

    const v2, 0x7f0f0236    # com.twitter.android.R.string.login_signin

    invoke-virtual {p0, p1}, Lcom/twitter/android/OneFactorLoginActivity$LoginState$3;->c(Lcom/twitter/android/OneFactorLoginActivity;)Z

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/OneFactorLoginActivity;->a(Lcom/twitter/android/OneFactorLoginActivity;IIZZZ)V

    return-void
.end method

.method public b(Lcom/twitter/android/OneFactorLoginActivity;)V
    .locals 6

    invoke-static {p1}, Lcom/twitter/android/OneFactorLoginActivity;->p(Lcom/twitter/android/OneFactorLoginActivity;)V

    invoke-static {p1}, Lcom/twitter/android/OneFactorLoginActivity;->q(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-static {p1}, Lcom/twitter/android/OneFactorLoginActivity;->a(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "login::1fa:manual:submit"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    return-void
.end method

.method public c(Lcom/twitter/android/OneFactorLoginActivity;)Z
    .locals 2

    invoke-static {p1}, Lcom/twitter/android/OneFactorLoginActivity;->r(Lcom/twitter/android/OneFactorLoginActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->length()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
