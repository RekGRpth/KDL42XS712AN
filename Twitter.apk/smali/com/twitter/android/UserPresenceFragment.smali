.class public Lcom/twitter/android/UserPresenceFragment;
.super Lcom/twitter/android/client/BaseListFragment;
.source "Twttr"


# instance fields
.field private a:J

.field private b:Lcom/twitter/android/zj;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseListFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/UserPresenceFragment;)Z
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/UserPresenceFragment;->r()Z

    move-result v0

    return v0
.end method

.method private d(I)Z
    .locals 6

    const/4 v0, 0x0

    invoke-virtual {p0, p1}, Lcom/twitter/android/UserPresenceFragment;->c_(I)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    new-instance v1, Lcom/twitter/library/api/conversations/ao;

    invoke-virtual {p0}, Lcom/twitter/android/UserPresenceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/UserPresenceFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-wide v4, p0, Lcom/twitter/android/UserPresenceFragment;->a:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/twitter/library/api/conversations/ao;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    const/16 v2, 0xb

    invoke-virtual {p0, v1, v2, v0}, Lcom/twitter/android/UserPresenceFragment;->a(Lcom/twitter/library/service/b;II)Z

    move-result v0

    goto :goto_0
.end method

.method private r()Z
    .locals 6

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/UserPresenceFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v2, p0, Lcom/twitter/android/UserPresenceFragment;->a:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    const/4 v2, 0x4

    :try_start_0
    invoke-direct {p0, v2}, Lcom/twitter/android/UserPresenceFragment;->d(I)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method protected a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    const v0, 0x7f030175    # com.twitter.android.R.layout.user_presence_list

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/android/UserPresenceFragment;->a(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseListFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/twitter/android/UserPresenceFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/UserPresenceFragment;->a_(Z)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/UserPresenceFragment;->b(I)V

    invoke-virtual {p0}, Lcom/twitter/android/UserPresenceFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/UserPresenceFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v1}, Landroid/support/v4/widget/CursorAdapter;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->smoothScrollToPosition(I)V

    goto :goto_0
.end method

.method protected a(Z)V
    .locals 2

    const/4 v1, 0x3

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->a(Z)V

    invoke-virtual {p0}, Lcom/twitter/android/UserPresenceFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    if-eqz p1, :cond_0

    invoke-virtual {p0, v1}, Lcom/twitter/android/UserPresenceFragment;->a_(I)V

    invoke-virtual {p0}, Lcom/twitter/android/UserPresenceFragment;->P()Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/UserPresenceFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p0, v1}, Lcom/twitter/android/UserPresenceFragment;->a_(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/UserPresenceFragment;->h(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/UserPresenceFragment;->i(Z)V

    goto :goto_0
.end method

.method protected b()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->b()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/UserPresenceFragment;->a(Z)V

    return-void
.end method

.method public b(J)V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-wide p1, p0, Lcom/twitter/android/UserPresenceFragment;->a:J

    iget-object v0, p0, Lcom/twitter/android/UserPresenceFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p0, v5}, Lcom/twitter/android/UserPresenceFragment;->h(I)V

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/UserPresenceFragment;->b:Lcom/twitter/android/zj;

    invoke-virtual {v0}, Lcom/twitter/android/zj;->b()V

    invoke-virtual {p0}, Lcom/twitter/android/UserPresenceFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/UserPresenceFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "tweet"

    aput-object v4, v2, v3

    const-string/jumbo v3, "typing_indicator"

    aput-object v3, v2, v5

    const/4 v3, 0x2

    aput-object v6, v2, v3

    const/4 v3, 0x3

    aput-object v6, v2, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "impression"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/UserPresenceFragment;->P()Z

    goto :goto_1
.end method

.method public e()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/UserPresenceFragment;->b:Lcom/twitter/android/zj;

    invoke-virtual {v0}, Lcom/twitter/android/zj;->c()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/twitter/android/UserPresenceFragment;->a:J

    return-void
.end method

.method public i()V
    .locals 1

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/twitter/android/UserPresenceFragment;->d(I)Z

    return-void
.end method

.method protected n()V
    .locals 1

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/twitter/android/UserPresenceFragment;->d(I)Z

    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    new-instance v0, Lcom/twitter/android/zg;

    invoke-virtual {p0}, Lcom/twitter/android/UserPresenceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/twitter/android/zg;-><init>(Lcom/twitter/android/UserPresenceFragment;Landroid/content/Context;Landroid/database/Cursor;I)V

    iput-object v0, p0, Lcom/twitter/android/UserPresenceFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {p0}, Lcom/twitter/android/UserPresenceFragment;->V()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/UserPresenceFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "convo_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/UserPresenceFragment;->a:J

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/UserPresenceFragment;->l(Z)V

    new-instance v0, Lcom/twitter/android/zj;

    const-wide/16 v1, 0x1388

    invoke-direct {v0, p0, v1, v2}, Lcom/twitter/android/zj;-><init>(Lcom/twitter/android/UserPresenceFragment;J)V

    iput-object v0, p0, Lcom/twitter/android/UserPresenceFragment;->b:Lcom/twitter/android/zj;

    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 12

    const/4 v6, 0x0

    packed-switch p1, :pswitch_data_0

    move-object v0, v6

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/twitter/android/UserPresenceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/provider/aa;->c:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    iget-wide v3, p0, Lcom/twitter/android/UserPresenceFragment;->a:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string/jumbo v3, "ownerId"

    iget-wide v4, p0, Lcom/twitter/android/UserPresenceFragment;->Q:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/library/provider/cj;->b:[Ljava/lang/String;

    const-string/jumbo v4, "conversation_id=? AND entry_type=? AND created>?"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-wide v8, p0, Lcom/twitter/android/UserPresenceFragment;->a:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v7

    const/4 v7, 0x1

    const/16 v8, 0x9

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v7

    const/4 v7, 0x2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const-wide/16 v10, 0x7530

    sub-long/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v7

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/UserPresenceFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onPause()V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/UserPresenceFragment;->e()V

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onPause()V

    return-void
.end method

.method public q()Z
    .locals 4

    iget-wide v0, p0, Lcom/twitter/android/UserPresenceFragment;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
