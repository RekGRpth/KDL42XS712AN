.class Lcom/twitter/android/hv;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/twitter/android/hu;


# direct methods
.method constructor <init>(Lcom/twitter/android/hu;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/hv;->a:Lcom/twitter/android/hu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/hv;->a:Lcom/twitter/android/hu;

    invoke-static {v0}, Lcom/twitter/android/hu;->a(Lcom/twitter/android/hu;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "H-Accuracy"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/hv;->a:Lcom/twitter/android/hu;

    invoke-static {v0}, Lcom/twitter/android/hu;->b(Lcom/twitter/android/hu;)Landroid/location/Location;

    move-result-object v0

    invoke-virtual {v0}, Landroid/location/Location;->removeAccuracy()V

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/hv;->a:Lcom/twitter/android/hu;

    invoke-static {v0}, Lcom/twitter/android/hu;->c(Lcom/twitter/android/hu;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/hv;->a:Lcom/twitter/android/hu;

    invoke-static {v1}, Lcom/twitter/android/hu;->b(Lcom/twitter/android/hu;)Landroid/location/Location;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/platform/h;->b(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/hv;->a:Lcom/twitter/android/hu;

    invoke-static {v0}, Lcom/twitter/android/hu;->b(Lcom/twitter/android/hu;)Landroid/location/Location;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/location/Location;->setAccuracy(F)V

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_0

    :cond_3
    const-string/jumbo v2, "Altitude"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/hv;->a:Lcom/twitter/android/hu;

    invoke-static {v0}, Lcom/twitter/android/hu;->b(Lcom/twitter/android/hu;)Landroid/location/Location;

    move-result-object v0

    invoke-virtual {v0}, Landroid/location/Location;->removeAltitude()V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/hv;->a:Lcom/twitter/android/hu;

    invoke-static {v0}, Lcom/twitter/android/hu;->b(Lcom/twitter/android/hu;)Landroid/location/Location;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    float-to-double v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setAltitude(D)V

    goto :goto_1

    :cond_5
    const-string/jumbo v2, "Speed"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/twitter/android/hv;->a:Lcom/twitter/android/hu;

    invoke-static {v0}, Lcom/twitter/android/hu;->b(Lcom/twitter/android/hu;)Landroid/location/Location;

    move-result-object v0

    invoke-virtual {v0}, Landroid/location/Location;->removeSpeed()V

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/twitter/android/hv;->a:Lcom/twitter/android/hu;

    invoke-static {v0}, Lcom/twitter/android/hu;->b(Lcom/twitter/android/hu;)Landroid/location/Location;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/location/Location;->setSpeed(F)V

    goto :goto_1

    :cond_7
    const-string/jumbo v2, "Bearing"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/twitter/android/hv;->a:Lcom/twitter/android/hu;

    invoke-static {v0}, Lcom/twitter/android/hu;->b(Lcom/twitter/android/hu;)Landroid/location/Location;

    move-result-object v0

    invoke-virtual {v0}, Landroid/location/Location;->removeBearing()V

    goto/16 :goto_1

    :cond_8
    iget-object v0, p0, Lcom/twitter/android/hv;->a:Lcom/twitter/android/hu;

    invoke-static {v0}, Lcom/twitter/android/hu;->b(Lcom/twitter/android/hu;)Landroid/location/Location;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/location/Location;->setBearing(F)V

    goto/16 :goto_1

    :cond_9
    const-string/jumbo v2, "Timestamp"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/hv;->a:Lcom/twitter/android/hu;

    invoke-static {v0}, Lcom/twitter/android/hu;->b(Lcom/twitter/android/hu;)Landroid/location/Location;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setTime(J)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_1
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
