.class public Lcom/twitter/android/fy;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lhe;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/net/Uri;)Lhb;
    .locals 3

    invoke-static {p1}, Lcom/twitter/android/MainActivity;->a(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    invoke-static {}, Lgl;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "type"

    const/16 v2, 0x16

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v1, Lhd;

    const-class v2, Lcom/twitter/android/FavoritePeopleTimelineFragment;

    invoke-direct {v1, p2, v2}, Lhd;-><init>(Landroid/net/Uri;Ljava/lang/Class;)V

    invoke-virtual {v1, v0}, Lhd;->a(Landroid/os/Bundle;)Lhd;

    move-result-object v0

    const v1, 0x7f0f016c    # com.twitter.android.R.string.favorite_people

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhd;->a(Ljava/lang/CharSequence;)Lhd;

    move-result-object v0

    const-string/jumbo v1, "favorite_people"

    invoke-virtual {v0, v1}, Lhd;->a(Ljava/lang/String;)Lhd;

    move-result-object v0

    invoke-virtual {v0}, Lhd;->a()Lhb;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
