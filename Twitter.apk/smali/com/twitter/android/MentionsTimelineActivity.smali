.class public Lcom/twitter/android/MentionsTimelineActivity;
.super Lcom/twitter/android/NotificationsBaseTimelineActivity;
.source "Twttr"


# static fields
.field private static final d:Landroid/util/SparseIntArray;

.field private static final e:Landroid/util/SparseIntArray;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0, v6}, Landroid/util/SparseIntArray;-><init>(I)V

    sput-object v0, Lcom/twitter/android/MentionsTimelineActivity;->d:Landroid/util/SparseIntArray;

    sget-object v0, Lcom/twitter/android/MentionsTimelineActivity;->d:Landroid/util/SparseIntArray;

    const/4 v1, 0x5

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/twitter/android/MentionsTimelineActivity;->d:Landroid/util/SparseIntArray;

    const/16 v1, 0x17

    invoke-virtual {v0, v3, v1}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/twitter/android/MentionsTimelineActivity;->d:Landroid/util/SparseIntArray;

    const/16 v1, 0x18

    invoke-virtual {v0, v4, v1}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/twitter/android/MentionsTimelineActivity;->d:Landroid/util/SparseIntArray;

    const/16 v1, 0x19

    invoke-virtual {v0, v5, v1}, Landroid/util/SparseIntArray;->put(II)V

    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0, v6}, Landroid/util/SparseIntArray;-><init>(I)V

    sput-object v0, Lcom/twitter/android/MentionsTimelineActivity;->e:Landroid/util/SparseIntArray;

    sget-object v0, Lcom/twitter/android/MentionsTimelineActivity;->e:Landroid/util/SparseIntArray;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/twitter/android/MentionsTimelineActivity;->e:Landroid/util/SparseIntArray;

    const/16 v1, 0x17

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/twitter/android/MentionsTimelineActivity;->e:Landroid/util/SparseIntArray;

    const/16 v1, 0x18

    invoke-virtual {v0, v1, v4}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/twitter/android/MentionsTimelineActivity;->e:Landroid/util/SparseIntArray;

    const/16 v1, 0x19

    invoke-virtual {v0, v1, v5}, Landroid/util/SparseIntArray;->put(II)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/NotificationsBaseTimelineActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/os/Bundle;Landroid/content/Context;Lcom/twitter/library/client/aa;)Ljava/lang/Class;
    .locals 3

    const-string/jumbo v0, "type"

    sget-object v1, Lcom/twitter/android/MentionsTimelineActivity;->d:Landroid/util/SparseIntArray;

    invoke-static {p1, p2}, Lcom/twitter/android/MentionsTimelineActivity;->a(Landroid/content/Context;Lcom/twitter/library/client/aa;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "empty_title"

    const v1, 0x7f0f0151    # com.twitter.android.R.string.empty_mentions

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "empty_desc"

    const v1, 0x7f0f0152    # com.twitter.android.R.string.empty_mentions_desc

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-class v0, Lcom/twitter/android/MentionTimelineFragment;

    return-object v0
.end method


# virtual methods
.method protected a(Landroid/content/Intent;Lcom/twitter/library/client/e;)Lcom/twitter/android/iu;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/MentionsTimelineActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/MentionsTimelineActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/twitter/android/MentionsTimelineActivity;->a(Landroid/os/Bundle;Landroid/content/Context;Lcom/twitter/library/client/aa;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v0}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/BaseListFragment;

    invoke-virtual {v0, p0}, Lcom/twitter/android/client/BaseListFragment;->a(Lcom/twitter/android/client/ag;)V

    new-instance v1, Lcom/twitter/android/iu;

    const-string/jumbo v2, "connect"

    invoke-direct {v1, v0, v2}, Lcom/twitter/android/iu;-><init>(Lcom/twitter/android/client/BaseListFragment;Ljava/lang/String;)V

    return-object v1
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/library/client/e;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/twitter/android/NotificationsBaseTimelineActivity;->a(Landroid/os/Bundle;Lcom/twitter/library/client/e;)V

    invoke-virtual {p0}, Lcom/twitter/android/MentionsTimelineActivity;->L()Lcom/twitter/android/client/bn;

    move-result-object v0

    const-string/jumbo v1, "connect"

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/bn;->a(Ljava/lang/String;)Lcom/twitter/android/client/bn;

    return-void
.end method
