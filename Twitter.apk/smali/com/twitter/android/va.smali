.class public Lcom/twitter/android/va;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic b:Lcom/twitter/android/TimelineFragment;


# direct methods
.method public constructor <init>(Lcom/twitter/android/TimelineFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/va;->b:Lcom/twitter/android/TimelineFragment;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;JLjava/lang/String;ILjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/va;->b:Lcom/twitter/android/TimelineFragment;

    invoke-static {v0}, Lcom/twitter/android/TimelineFragment;->c(Lcom/twitter/android/TimelineFragment;)V

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/util/HashMap;)V
    .locals 6

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/va;->b:Lcom/twitter/android/TimelineFragment;

    invoke-static {v0}, Lcom/twitter/android/TimelineFragment;->f(Lcom/twitter/android/TimelineFragment;)Lcom/twitter/library/util/FriendshipCache;

    move-result-object v0

    if-eqz v0, :cond_1

    if-eqz p4, :cond_1

    invoke-virtual {p4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iget-object v1, p0, Lcom/twitter/android/va;->b:Lcom/twitter/android/TimelineFragment;

    invoke-static {v1}, Lcom/twitter/android/TimelineFragment;->f(Lcom/twitter/android/TimelineFragment;)Lcom/twitter/library/util/FriendshipCache;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v4, v5, v0}, Lcom/twitter/library/util/FriendshipCache;->c(JI)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/va;->b:Lcom/twitter/android/TimelineFragment;

    invoke-static {v0}, Lcom/twitter/android/TimelineFragment;->g(Lcom/twitter/android/TimelineFragment;)Landroid/support/v4/widget/CursorAdapter;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/va;->b:Lcom/twitter/android/TimelineFragment;

    invoke-static {v0}, Lcom/twitter/android/TimelineFragment;->h(Lcom/twitter/android/TimelineFragment;)Landroid/support/v4/widget/CursorAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ye;

    invoke-virtual {v0}, Lcom/twitter/android/ye;->e()V

    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;I[ILjava/lang/String;J)V
    .locals 1

    invoke-super/range {p0 .. p7}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;I[ILjava/lang/String;J)V

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/va;->b:Lcom/twitter/android/TimelineFragment;

    invoke-static {v0}, Lcom/twitter/android/TimelineFragment;->d(Lcom/twitter/android/TimelineFragment;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/va;->b:Lcom/twitter/android/TimelineFragment;

    invoke-static {v0}, Lcom/twitter/android/TimelineFragment;->e(Lcom/twitter/android/TimelineFragment;)Landroid/support/v4/widget/CursorAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;I[ILjava/lang/String;Lcom/twitter/library/api/TwitterUser;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/va;->b:Lcom/twitter/android/TimelineFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/TimelineFragment;->a(Lcom/twitter/android/TimelineFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p6, :cond_0

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/va;->b:Lcom/twitter/android/TimelineFragment;

    iget v1, p6, Lcom/twitter/library/api/TwitterUser;->friendsCount:I

    iput v1, v0, Lcom/twitter/android/TimelineFragment;->d:I

    :cond_0
    return-void
.end method

.method public a(Ljava/util/HashMap;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/va;->b:Lcom/twitter/android/TimelineFragment;

    invoke-virtual {v0, p1}, Lcom/twitter/android/TimelineFragment;->a(Ljava/util/HashMap;)V

    return-void
.end method
