.class Lcom/twitter/android/fm;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:I

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Lcom/twitter/android/widget/EventView;

.field final synthetic f:Lcom/twitter/library/api/TimelineScribeContent;

.field final synthetic g:Ljava/lang/String;

.field final synthetic h:Lcom/twitter/android/fl;


# direct methods
.method constructor <init>(Lcom/twitter/android/fl;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/twitter/android/widget/EventView;Lcom/twitter/library/api/TimelineScribeContent;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/fm;->h:Lcom/twitter/android/fl;

    iput-object p2, p0, Lcom/twitter/android/fm;->a:Ljava/lang/String;

    iput p3, p0, Lcom/twitter/android/fm;->b:I

    iput-object p4, p0, Lcom/twitter/android/fm;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/twitter/android/fm;->d:Ljava/lang/String;

    iput-object p6, p0, Lcom/twitter/android/fm;->e:Lcom/twitter/android/widget/EventView;

    iput-object p7, p0, Lcom/twitter/android/fm;->f:Lcom/twitter/library/api/TimelineScribeContent;

    iput-object p8, p0, Lcom/twitter/android/fm;->g:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/fm;->h:Lcom/twitter/android/fl;

    invoke-static {v0}, Lcom/twitter/android/fl;->a(Lcom/twitter/android/fl;)Lcom/twitter/android/vf;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/fm;->a:Ljava/lang/String;

    iget v2, p0, Lcom/twitter/android/fm;->b:I

    iget-object v3, p0, Lcom/twitter/android/fm;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/fm;->d:Ljava/lang/String;

    iget-object v6, p0, Lcom/twitter/android/fm;->e:Lcom/twitter/android/widget/EventView;

    invoke-virtual {v6}, Lcom/twitter/android/widget/EventView;->getTopicData()Lcom/twitter/android/widget/TopicView$TopicData;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/vf;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/android/widget/TopicView$TopicData;)V

    iget-object v6, p0, Lcom/twitter/android/fm;->f:Lcom/twitter/library/api/TimelineScribeContent;

    iget-object v7, p0, Lcom/twitter/android/fm;->g:Ljava/lang/String;

    const-wide/16 v8, -0x1

    const/16 v10, 0x10

    iget-object v11, p0, Lcom/twitter/android/fm;->a:Ljava/lang/String;

    invoke-static/range {v6 .. v11}, Lcom/twitter/library/scribe/ScribeItem;->a(Lcom/twitter/library/api/TimelineScribeContent;Ljava/lang/String;JILjava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/fm;->h:Lcom/twitter/android/fl;

    invoke-static {v1}, Lcom/twitter/android/fl;->d(Lcom/twitter/android/fl;)Lcom/twitter/android/client/c;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v3, p0, Lcom/twitter/android/fm;->h:Lcom/twitter/android/fl;

    invoke-static {v3}, Lcom/twitter/android/fl;->c(Lcom/twitter/android/fl;)Lcom/twitter/library/client/aa;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v6, p0, Lcom/twitter/android/fm;->h:Lcom/twitter/android/fl;

    invoke-static {v6}, Lcom/twitter/android/fl;->b(Lcom/twitter/android/fl;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v4

    const/4 v4, 0x1

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/twitter/android/fm;->g:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string/jumbo v5, "event"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string/jumbo v5, "click"

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method
