.class public final enum Lcom/twitter/android/PostActivity$PhotoAction;
.super Ljava/lang/Enum;
.source "Twttr"


# static fields
.field public static final enum a:Lcom/twitter/android/PostActivity$PhotoAction;

.field public static final enum b:Lcom/twitter/android/PostActivity$PhotoAction;

.field public static final enum c:Lcom/twitter/android/PostActivity$PhotoAction;

.field public static final enum d:Lcom/twitter/android/PostActivity$PhotoAction;

.field private static final synthetic e:[Lcom/twitter/android/PostActivity$PhotoAction;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/android/PostActivity$PhotoAction;

    const-string/jumbo v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/PostActivity$PhotoAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/PostActivity$PhotoAction;->a:Lcom/twitter/android/PostActivity$PhotoAction;

    new-instance v0, Lcom/twitter/android/PostActivity$PhotoAction;

    const-string/jumbo v1, "GALLERY"

    invoke-direct {v0, v1, v3}, Lcom/twitter/android/PostActivity$PhotoAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/PostActivity$PhotoAction;->b:Lcom/twitter/android/PostActivity$PhotoAction;

    new-instance v0, Lcom/twitter/android/PostActivity$PhotoAction;

    const-string/jumbo v1, "CAMERA"

    invoke-direct {v0, v1, v4}, Lcom/twitter/android/PostActivity$PhotoAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/PostActivity$PhotoAction;->c:Lcom/twitter/android/PostActivity$PhotoAction;

    new-instance v0, Lcom/twitter/android/PostActivity$PhotoAction;

    const-string/jumbo v1, "CANVAS"

    invoke-direct {v0, v1, v5}, Lcom/twitter/android/PostActivity$PhotoAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/PostActivity$PhotoAction;->d:Lcom/twitter/android/PostActivity$PhotoAction;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/twitter/android/PostActivity$PhotoAction;

    sget-object v1, Lcom/twitter/android/PostActivity$PhotoAction;->a:Lcom/twitter/android/PostActivity$PhotoAction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/twitter/android/PostActivity$PhotoAction;->b:Lcom/twitter/android/PostActivity$PhotoAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/twitter/android/PostActivity$PhotoAction;->c:Lcom/twitter/android/PostActivity$PhotoAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/twitter/android/PostActivity$PhotoAction;->d:Lcom/twitter/android/PostActivity$PhotoAction;

    aput-object v1, v0, v5

    sput-object v0, Lcom/twitter/android/PostActivity$PhotoAction;->e:[Lcom/twitter/android/PostActivity$PhotoAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/android/PostActivity$PhotoAction;
    .locals 1

    const-class v0, Lcom/twitter/android/PostActivity$PhotoAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/PostActivity$PhotoAction;

    return-object v0
.end method

.method public static values()[Lcom/twitter/android/PostActivity$PhotoAction;
    .locals 1

    sget-object v0, Lcom/twitter/android/PostActivity$PhotoAction;->e:[Lcom/twitter/android/PostActivity$PhotoAction;

    invoke-virtual {v0}, [Lcom/twitter/android/PostActivity$PhotoAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/PostActivity$PhotoAction;

    return-object v0
.end method
