.class Lcom/twitter/android/lt;
.super Landroid/support/v4/util/LruCache;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/MemoryImageCache;


# direct methods
.method constructor <init>(Lcom/twitter/android/MemoryImageCache;I)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/lt;->a:Lcom/twitter/android/MemoryImageCache;

    invoke-direct {p0, p2}, Landroid/support/v4/util/LruCache;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/String;Lcom/twitter/android/lu;)I
    .locals 2

    iget-object v0, p2, Lcom/twitter/android/lu;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v0

    iget-object v1, p2, Lcom/twitter/android/lu;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    mul-int/2addr v0, v1

    return v0
.end method

.method protected a(ZLjava/lang/String;Lcom/twitter/android/lu;Lcom/twitter/android/lu;)V
    .locals 4

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/lt;->a:Lcom/twitter/android/MemoryImageCache;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/lt;->a:Lcom/twitter/android/MemoryImageCache;

    invoke-static {v0}, Lcom/twitter/android/MemoryImageCache;->a(Lcom/twitter/android/MemoryImageCache;)Landroid/support/v4/util/SimpleArrayMap;

    move-result-object v0

    new-instance v2, Ljava/lang/ref/SoftReference;

    iget-object v3, p3, Lcom/twitter/android/lu;->a:Landroid/graphics/Bitmap;

    invoke-direct {v2, v3}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, p2, v2}, Landroid/support/v4/util/SimpleArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1

    :goto_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/lt;->a:Lcom/twitter/android/MemoryImageCache;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lcom/twitter/android/lt;->a:Lcom/twitter/android/MemoryImageCache;

    invoke-static {v0}, Lcom/twitter/android/MemoryImageCache;->a(Lcom/twitter/android/MemoryImageCache;)Landroid/support/v4/util/SimpleArrayMap;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/support/v4/util/SimpleArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0
.end method

.method protected synthetic entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Ljava/lang/String;

    check-cast p3, Lcom/twitter/android/lu;

    check-cast p4, Lcom/twitter/android/lu;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/twitter/android/lt;->a(ZLjava/lang/String;Lcom/twitter/android/lu;Lcom/twitter/android/lu;)V

    return-void
.end method

.method protected synthetic sizeOf(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Ljava/lang/String;

    check-cast p2, Lcom/twitter/android/lu;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/lt;->a(Ljava/lang/String;Lcom/twitter/android/lu;)I

    move-result v0

    return v0
.end method

.method public trimToSize(I)V
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v4/util/LruCache;->trimToSize(I)V

    iget-object v0, p0, Lcom/twitter/android/lt;->a:Lcom/twitter/android/MemoryImageCache;

    invoke-virtual {v0}, Lcom/twitter/android/MemoryImageCache;->a()V

    return-void
.end method
