.class final Lcom/twitter/android/qv;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/ProfileFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/ProfileFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;IJJ)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/ProfileFragment;->l(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0f021b    # com.twitter.android.R.string.lists_member_added_success

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0f021a    # com.twitter.android.R.string.lists_member_added_error

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JIJJLcom/twitter/library/api/TwitterUser;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/ProfileFragment;->g(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iget-object v2, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v2}, Lcom/twitter/android/ProfileFragment;->J(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    sparse-switch p7, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Unsupported type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->l:Lcom/twitter/android/qz;

    invoke-virtual {v0}, Lcom/twitter/android/qz;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/rh;

    move-object v1, v0

    :goto_0
    if-eqz p12, :cond_3

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/twitter/android/rh;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->requery()Z

    invoke-virtual {v1}, Lcom/twitter/android/rh;->notifyDataSetChanged()V

    :cond_0
    invoke-virtual {v1, p10, p11}, Lcom/twitter/android/rh;->b(J)Ljava/lang/Long;

    move-result-object v2

    if-eqz v2, :cond_2

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/twitter/android/zw;->a(Landroid/widget/ListView;J)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    invoke-virtual {v1, p10, p11}, Lcom/twitter/android/rh;->c(J)Ljava/lang/Long;

    :cond_2
    return-void

    :sswitch_1
    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->n:Lcom/twitter/android/qz;

    invoke-virtual {v0}, Lcom/twitter/android/qz;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/rh;

    move-object v1, v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_1
        0x14 -> :sswitch_0
    .end sparse-switch
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JJII[Lcom/twitter/library/api/TwitterUser;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x4

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/util/AppMetrics;->a(Landroid/content/Context;JI)V

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/ProfileFragment;->k(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_0

    if-lez p10, :cond_0

    const/16 v0, 0xa

    if-ne p9, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x3

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x14

    if-ne p9, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x7

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;Lcom/twitter/library/api/TwitterUser;)V
    .locals 10

    const v9, 0x7f0f00ee    # com.twitter.android.R.string.default_error_message

    const/16 v8, 0xc8

    const/16 v7, 0x10

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v2, p2}, Lcom/twitter/android/ProfileFragment;->h(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v2}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/twitter/library/platform/PushService;->f(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    iget-object v5, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget v5, v5, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v5}, Lcom/twitter/library/provider/ay;->h(I)Z

    move-result v5

    iget-object v6, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v6}, Lcom/twitter/android/ProfileFragment;->E(Lcom/twitter/android/ProfileFragment;)Ljava/util/HashMap;

    move-result-object v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v6}, Lcom/twitter/android/ProfileFragment;->E(Lcom/twitter/android/ProfileFragment;)Ljava/util/HashMap;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_2

    if-eq p3, v8, :cond_0

    iget-object v1, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v2, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget v2, v2, Lcom/twitter/android/ProfileFragment;->H:I

    const/16 v3, 0x100

    invoke-static {v2, v3}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v2

    iput v2, v1, Lcom/twitter/android/ProfileFragment;->H:I

    iget-object v1, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    const v2, 0x7f0f050b    # com.twitter.android.R.string.unfollow

    const/4 v3, 0x2

    invoke-virtual {v1, v0, v2, v3}, Lcom/twitter/android/ProfileFragment;->a(ZII)V

    iget-object v1, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v1}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v9, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    if-ne p3, v8, :cond_3

    if-nez v4, :cond_0

    :cond_3
    const/16 v6, 0x3e9

    if-eq p3, v6, :cond_4

    if-nez v4, :cond_6

    :cond_4
    invoke-static {v2}, Lcom/twitter/android/client/aw;->a(Landroid/content/Context;)Lcom/twitter/android/client/aw;

    move-result-object v2

    if-nez v4, :cond_5

    :goto_1
    invoke-virtual {v2, v3, v0}, Lcom/twitter/android/client/aw;->a(Ljava/lang/String;Z)V

    if-eqz v5, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0, v1}, Lcom/twitter/android/ProfileFragment;->d(Z)V

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1

    :cond_6
    iget-object v2, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v2}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2, v9, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    if-eqz v5, :cond_7

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v2, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget v2, v2, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v2, v7}, Lcom/twitter/library/provider/ay;->b(II)I

    move-result v2

    iput v2, v0, Lcom/twitter/android/ProfileFragment;->H:I

    :goto_2
    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0, v1}, Lcom/twitter/android/ProfileFragment;->d(Z)V

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v2, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget v2, v2, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v2, v7}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v2

    iput v2, v0, Lcom/twitter/android/ProfileFragment;->H:I

    goto :goto_2
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;[J)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/ProfileFragment;->n(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->r()V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;I[ILjava/lang/String;J)V
    .locals 13

    iget-object v2, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v2, p2}, Lcom/twitter/android/ProfileFragment;->b(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-object v3, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v3}, Lcom/twitter/android/ProfileFragment;->z(Lcom/twitter/android/ProfileFragment;)Ljava/util/HashMap;

    move-result-object v3

    if-eqz v3, :cond_4

    iget-object v2, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v2}, Lcom/twitter/android/ProfileFragment;->z(Lcom/twitter/android/ProfileFragment;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Ljava/lang/Integer;

    if-eqz v8, :cond_0

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    const/16 v2, 0xc8

    move/from16 v0, p3

    if-eq v0, v2, :cond_3

    iget-object v2, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v3, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget v3, v3, Lcom/twitter/android/ProfileFragment;->H:I

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/twitter/library/provider/ay;->b(II)I

    move-result v3

    iput v3, v2, Lcom/twitter/android/ProfileFragment;->H:I

    iget-object v2, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    const/4 v3, 0x1

    const v4, 0x7f0f019f    # com.twitter.android.R.string.follow

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lcom/twitter/android/ProfileFragment;->a(ZII)V

    :goto_0
    iget-object v2, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/twitter/android/ProfileFragment;->d(Z)V

    :cond_0
    :goto_1
    return-void

    :sswitch_0
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/16 v3, 0xa

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v2, v2, Lcom/twitter/android/ProfileFragment;->n:Lcom/twitter/android/qz;

    move-object v9, v2

    :goto_2
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    iget-object v2, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v7

    move-wide/from16 v5, p6

    invoke-virtual/range {v2 .. v7}, Lcom/twitter/android/ProfileFragment;->a(JJI)Ljava/lang/Long;

    move-result-object v5

    const/16 v2, 0xc8

    move/from16 v0, p3

    if-eq v0, v2, :cond_2

    iget-object v2, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v2}, Lcom/twitter/android/ProfileFragment;->A(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    cmp-long v2, v3, v5

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v2}, Lcom/twitter/android/ProfileFragment;->B(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/library/util/FriendshipCache;

    move-result-object v2

    move-wide/from16 v0, p6

    invoke-virtual {v2, v0, v1}, Lcom/twitter/library/util/FriendshipCache;->e(J)V

    invoke-virtual {v9}, Lcom/twitter/android/qz;->notifyDataSetChanged()V

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v2, v2, Lcom/twitter/android/ProfileFragment;->l:Lcom/twitter/android/qz;

    move-object v9, v2

    goto :goto_2

    :cond_2
    if-eqz v5, :cond_0

    invoke-virtual {v9}, Lcom/twitter/android/qz;->d()Landroid/widget/ListAdapter;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/rh;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    move-wide/from16 v0, p6

    invoke-virtual {v2, v0, v1, v3, v4}, Lcom/twitter/android/rh;->a(JJ)V

    iget-object v12, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v2, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v2}, Lcom/twitter/android/ProfileFragment;->C(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/client/c;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-wide v4, v3, Lcom/twitter/android/ProfileFragment;->I:J

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const-wide/16 v7, -0x1

    const/4 v11, 0x0

    move-object v3, p1

    move-wide/from16 v9, p6

    invoke-virtual/range {v2 .. v11}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;JIJJLjava/lang/Integer;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v12, v2}, Lcom/twitter/android/ProfileFragment;->c(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    iget-object v9, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v2, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v2}, Lcom/twitter/android/ProfileFragment;->D(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/client/c;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/16 v5, 0x14

    const/4 v6, 0x3

    move-wide/from16 v7, p6

    invoke-virtual/range {v2 .. v8}, Lcom/twitter/android/client/c;->a(ZIIIJ)Ljava/lang/String;

    move-result-object v2

    invoke-static {v9, v2}, Lcom/twitter/android/ProfileFragment;->d(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v2}, Lcom/twitter/android/ProfileFragment;->E(Lcom/twitter/android/ProfileFragment;)Ljava/util/HashMap;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v2}, Lcom/twitter/android/ProfileFragment;->E(Lcom/twitter/android/ProfileFragment;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    if-eqz v2, :cond_0

    const/16 v3, 0xc8

    move/from16 v0, p3

    if-eq v0, v3, :cond_6

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/twitter/library/provider/ay;->b(I)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v3, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget v3, v3, Lcom/twitter/android/ProfileFragment;->H:I

    const/16 v4, 0x100

    invoke-static {v3, v4}, Lcom/twitter/library/provider/ay;->b(II)I

    move-result v3

    iput v3, v2, Lcom/twitter/android/ProfileFragment;->H:I

    iget-object v2, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    const/4 v3, 0x1

    const v4, 0x7f0f050b    # com.twitter.android.R.string.unfollow

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5}, Lcom/twitter/android/ProfileFragment;->a(ZII)V

    goto/16 :goto_1

    :cond_5
    iget-object v2, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v3, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget v3, v3, Lcom/twitter/android/ProfileFragment;->H:I

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/twitter/library/provider/ay;->b(II)I

    move-result v3

    iput v3, v2, Lcom/twitter/android/ProfileFragment;->H:I

    iget-object v2, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    const/4 v3, 0x1

    const v4, 0x7f0f019f    # com.twitter.android.R.string.follow

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5}, Lcom/twitter/android/ProfileFragment;->a(ZII)V

    goto/16 :goto_1

    :cond_6
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/twitter/library/provider/ay;->b(I)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v9, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v2, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v2}, Lcom/twitter/android/ProfileFragment;->F(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/client/c;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/16 v5, 0x14

    const/4 v6, 0x3

    move-wide/from16 v7, p6

    invoke-virtual/range {v2 .. v8}, Lcom/twitter/android/client/c;->a(ZIIIJ)Ljava/lang/String;

    move-result-object v2

    invoke-static {v9, v2}, Lcom/twitter/android/ProfileFragment;->e(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;)V

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_0
    .end sparse-switch
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;I[ILjava/lang/String;Lcom/twitter/library/api/TwitterUser;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/ProfileFragment;->m(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_0

    if-eqz p6, :cond_0

    iget-wide v0, p6, Lcom/twitter/library/api/TwitterUser;->userId:J

    iget-object v2, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-wide v2, v2, Lcom/twitter/android/ProfileFragment;->I:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0, p6}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/library/api/TwitterUser;)V

    :cond_0
    return-void
.end method

.method public a(Ljava/util/HashMap;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->h:Lcom/twitter/library/util/m;

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ae;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v1, v1, Lcom/twitter/android/ProfileFragment;->j:Lcom/twitter/android/widget/ProfileHeader;

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v3, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v3}, Lcom/twitter/android/ProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v0, v0, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-direct {v2, v3, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/ProfileHeader;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->i:Lcom/twitter/library/util/m;

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ae;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0}, Lcom/twitter/android/ProfileFragment;->a(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->j:Lcom/twitter/android/widget/ProfileHeader;

    iget-object v1, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v1}, Lcom/twitter/android/ProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0031    # com.twitter.android.R.color.dark_gray

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ProfileHeader;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method public b(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;Lcom/twitter/library/api/TwitterUser;)V
    .locals 6

    const/16 v4, 0x200

    const/4 v3, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/ProfileFragment;->i(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget v0, v0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v0}, Lcom/twitter/library/provider/ay;->f(I)Z

    move-result v0

    const/16 v2, 0xc8

    if-ne p3, v2, :cond_2

    if-eqz v0, :cond_1

    const v0, 0x7f0f0542    # com.twitter.android.R.string.users_turn_on_retweets_success

    :goto_1
    iget-object v2, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v4, v4, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v4}, Lcom/twitter/library/api/TwitterUser;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v2, v0, v3}, Lcom/twitter/android/ProfileFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_1
    const v0, 0x7f0f0540    # com.twitter.android.R.string.users_turn_off_retweets_success

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v1}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0f00ee    # com.twitter.android.R.string.default_error_message

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v1, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget v1, v1, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v1, v4}, Lcom/twitter/library/provider/ay;->b(II)I

    move-result v1

    iput v1, v0, Lcom/twitter/android/ProfileFragment;->H:I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v1, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget v1, v1, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v1, v4}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v1

    iput v1, v0, Lcom/twitter/android/ProfileFragment;->H:I

    goto :goto_0
.end method

.method public c(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 7

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/ProfileFragment;->f(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->G(Lcom/twitter/android/ProfileFragment;)Ljava/util/HashMap;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->G(Lcom/twitter/android/ProfileFragment;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/lang/Integer;

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->I(Lcom/twitter/android/ProfileFragment;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v1, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v1, v1, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    iget v1, v1, Lcom/twitter/library/api/TwitterUser;->friendship:I

    iput v1, v0, Lcom/twitter/android/ProfileFragment;->H:I

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    const/4 v1, 0x1

    const v2, 0x7f0f050b    # com.twitter.android.R.string.unfollow

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/ProfileFragment;->a(ZII)V

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/ProfileFragment;->d(Z)V

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0f052b    # com.twitter.android.R.string.users_destroy_friendship_error

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/rb;

    invoke-virtual {v0}, Lcom/twitter/android/rb;->notifyDataSetChanged()V

    :cond_1
    :goto_1
    return-void

    :sswitch_0
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->n:Lcom/twitter/android/qz;

    move-object v6, v0

    :goto_2
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v5

    move-wide v3, p5

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/ProfileFragment;->a(JJI)Ljava/lang/Long;

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->H(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    cmp-long v0, v1, v3

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->B(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/library/util/FriendshipCache;

    move-result-object v0

    invoke-virtual {v0, p5, p6}, Lcom/twitter/library/util/FriendshipCache;->b(J)V

    invoke-virtual {v6}, Lcom/twitter/android/qz;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0f052b    # com.twitter.android.R.string.users_destroy_friendship_error

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->l:Lcom/twitter/android/qz;

    move-object v6, v0

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v1, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget v1, v1, Lcom/twitter/android/ProfileFragment;->H:I

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v1

    iput v1, v0, Lcom/twitter/android/ProfileFragment;->H:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_0
    .end sparse-switch
.end method

.method public d(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-wide v0, v0, Lcom/twitter/android/ProfileFragment;->I:J

    cmp-long v0, p5, v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->q()V

    :cond_0
    return-void
.end method

.method public e(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 2

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-wide v0, v0, Lcom/twitter/android/ProfileFragment;->I:J

    cmp-long v0, p5, v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget v0, v0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v0}, Lcom/twitter/library/provider/ay;->e(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->q()V

    :cond_0
    return-void
.end method

.method public f(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 2

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-wide v0, v0, Lcom/twitter/android/ProfileFragment;->I:J

    cmp-long v0, p5, v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->q()V

    :cond_0
    return-void
.end method

.method public g(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 4

    const/4 v3, 0x0

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v1, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    iget v1, v1, Lcom/twitter/android/ProfileFragment;->H:I

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v1

    iput v1, v0, Lcom/twitter/android/ProfileFragment;->H:I

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    const/4 v1, 0x1

    const v2, 0x7f0f0508    # com.twitter.android.R.string.unblock

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/ProfileFragment;->a(ZII)V

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0, v3}, Lcom/twitter/android/ProfileFragment;->d(Z)V

    :cond_0
    return-void
.end method

.method public h(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/ProfileFragment;->j(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v1}, Lcom/twitter/android/ProfileFragment;->K(Lcom/twitter/android/ProfileFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x5

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/qv;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_0
    return-void
.end method
