.class Lcom/twitter/android/rp;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/RemoveAccountDialogActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/RemoveAccountDialogActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/rp;->a:Lcom/twitter/android/RemoveAccountDialogActivity;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;ILjava/lang/String;[I)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/rp;->a:Lcom/twitter/android/RemoveAccountDialogActivity;

    iget-boolean v0, v0, Lcom/twitter/android/RemoveAccountDialogActivity;->a:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/rp;->a:Lcom/twitter/android/RemoveAccountDialogActivity;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/twitter/android/RemoveAccountDialogActivity;->removeDialog(I)V

    iget-object v0, p0, Lcom/twitter/android/rp;->a:Lcom/twitter/android/RemoveAccountDialogActivity;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/twitter/android/RemoveAccountDialogActivity;->a:Z

    const/16 v0, 0xc8

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/rp;->a:Lcom/twitter/android/RemoveAccountDialogActivity;

    invoke-virtual {v0}, Lcom/twitter/android/RemoveAccountDialogActivity;->a()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/rp;->a:Lcom/twitter/android/RemoveAccountDialogActivity;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/android/RemoveAccountDialogActivity;->showDialog(I)V

    goto :goto_0
.end method
