.class Lcom/twitter/android/ev;
.super Lcom/twitter/android/ic;
.source "Twttr"


# instance fields
.field private final h:Ljava/util/ArrayList;

.field private final i:Lcom/twitter/library/widget/a;

.field private final j:Lcom/twitter/library/util/FriendshipCache;

.field private final k:Lcom/twitter/android/ex;

.field private final l:Ljava/util/HashMap;

.field private final m:Landroid/view/animation/Animation;


# direct methods
.method public constructor <init>(Landroid/app/Activity;ILcom/twitter/library/widget/ap;Lcom/twitter/android/client/c;Lcom/twitter/library/widget/aa;Lcom/twitter/android/vs;Lcom/twitter/library/widget/a;Lcom/twitter/library/util/FriendshipCache;Lcom/twitter/android/ex;)V
    .locals 12

    const/4 v4, 0x1

    invoke-virtual/range {p4 .. p4}, Lcom/twitter/android/client/c;->aa()Z

    move-result v5

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v7, p3

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    move-object/from16 v11, p8

    invoke-direct/range {v1 .. v11}, Lcom/twitter/android/ic;-><init>(Landroid/app/Activity;IZZZLcom/twitter/library/widget/ap;Lcom/twitter/android/client/c;Lcom/twitter/library/widget/aa;Lcom/twitter/android/vs;Lcom/twitter/library/util/FriendshipCache;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/ev;->h:Ljava/util/ArrayList;

    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/twitter/android/ev;->i:Lcom/twitter/library/widget/a;

    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/twitter/android/ev;->j:Lcom/twitter/library/util/FriendshipCache;

    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/twitter/android/ev;->k:Lcom/twitter/android/ex;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/ev;->l:Ljava/util/HashMap;

    const v1, 0x7f040007    # com.twitter.android.R.anim.fade_in

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/ev;->m:Landroid/view/animation/Animation;

    return-void
.end method

.method private a(Lcom/twitter/internal/android/widget/GroupedRowView;Landroid/database/Cursor;)Lcom/twitter/android/ew;
    .locals 19

    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/twitter/library/widget/UserSocialView;

    invoke-virtual {v7}, Lcom/twitter/library/widget/UserSocialView;->getTag()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/twitter/android/fb;

    sget v1, Lcom/twitter/library/provider/bi;->i:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    sget v1, Lcom/twitter/library/provider/bi;->t:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/ev;->b:Lcom/twitter/android/client/c;

    invoke-virtual {v1}, Lcom/twitter/android/client/c;->V()F

    move-result v1

    invoke-virtual {v7, v1}, Lcom/twitter/library/widget/UserSocialView;->setContentSize(F)V

    invoke-virtual {v7, v3, v4}, Lcom/twitter/library/widget/UserSocialView;->setUserId(J)V

    sget v1, Lcom/twitter/library/provider/bi;->l:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/ev;->b:Lcom/twitter/android/client/c;

    iget-object v1, v1, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    const/4 v2, 0x1

    invoke-interface {v1, v2, v9}, Lcom/twitter/library/widget/ap;->a(ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/twitter/library/widget/UserSocialView;->setUserImage(Landroid/graphics/Bitmap;)V

    :goto_0
    const/4 v1, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/twitter/android/ev;->a(Landroid/view/View;JJ)V

    sget v1, Lcom/twitter/library/provider/bi;->p:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput-wide v3, v8, Lcom/twitter/android/fb;->d:J

    iput v1, v8, Lcom/twitter/android/fb;->e:I

    move-object/from16 v0, v18

    iput-object v0, v8, Lcom/twitter/android/fb;->f:Ljava/lang/String;

    iput-object v9, v8, Lcom/twitter/android/fb;->g:Ljava/lang/String;

    iput-wide v5, v8, Lcom/twitter/android/fb;->c:J

    sget v1, Lcom/twitter/library/provider/bi;->h:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v8, Lcom/twitter/android/fb;->a:I

    const-string/jumbo v1, "android_wtf_show_bio_1605"

    invoke-static {v1}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v1, "android_wtf_show_bio_1605"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string/jumbo v9, "show_bio"

    aput-object v9, v2, v8

    invoke-static {v1, v2}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget v1, Lcom/twitter/library/provider/bi;->u:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v7, v1, v2}, Lcom/twitter/library/widget/UserSocialView;->a(Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;)V

    :goto_1
    sget v1, Lcom/twitter/library/provider/bi;->j:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/twitter/library/provider/bi;->k:I

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v1, v2}, Lcom/twitter/library/widget/UserSocialView;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget v1, Lcom/twitter/library/provider/bi;->m:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    and-int/lit8 v1, v2, 0x1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_2
    invoke-virtual {v7, v1}, Lcom/twitter/library/widget/UserSocialView;->setProtected(Z)V

    and-int/lit8 v1, v2, 0x2

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_3
    invoke-virtual {v7, v1}, Lcom/twitter/library/widget/UserSocialView;->setVerified(Z)V

    sget v1, Lcom/twitter/library/provider/bi;->n:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/twitter/library/api/PromotedContent;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/ev;->b:Lcom/twitter/android/client/c;

    iget-boolean v1, v1, Lcom/twitter/android/client/c;->f:Z

    move-object/from16 v0, v17

    invoke-virtual {v7, v0, v1}, Lcom/twitter/library/widget/UserSocialView;->a(Lcom/twitter/library/api/PromotedContent;Z)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/ev;->j:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v1, v3, v4}, Lcom/twitter/library/util/FriendshipCache;->a(J)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, v7, Lcom/twitter/library/widget/UserSocialView;->m:Lcom/twitter/library/widget/ActionButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/ev;->j:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/util/FriendshipCache;->i(J)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/ActionButton;->setChecked(Z)V

    :goto_4
    sget v1, Lcom/twitter/library/provider/bi;->q:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    const/4 v1, 0x1

    if-ne v8, v1, :cond_5

    const v9, 0x7f020117    # com.twitter.android.R.drawable.ic_activity_follow_tweet_default

    sget v1, Lcom/twitter/library/provider/bi;->r:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    sget v1, Lcom/twitter/library/provider/bi;->s:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/ev;->b:Lcom/twitter/android/client/c;

    iget-boolean v12, v1, Lcom/twitter/android/client/c;->f:Z

    invoke-virtual/range {v7 .. v12}, Lcom/twitter/library/widget/UserSocialView;->a(IILjava/lang/String;IZ)V

    :goto_5
    new-instance v7, Lcom/twitter/android/ew;

    sget v1, Lcom/twitter/library/provider/bi;->f:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    sget v1, Lcom/twitter/library/provider/bi;->g:I

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const/4 v14, 0x0

    move-wide v8, v5

    move-wide v10, v3

    move-wide v15, v3

    invoke-direct/range {v7 .. v18}, Lcom/twitter/android/ew;-><init>(JJIILcom/twitter/library/provider/Tweet;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;)V

    return-object v7

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v7, v1}, Lcom/twitter/library/widget/UserSocialView;->setUserImage(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    :cond_1
    invoke-virtual {v7}, Lcom/twitter/library/widget/UserSocialView;->a()V

    goto/16 :goto_1

    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_3

    :cond_4
    iget-object v1, v7, Lcom/twitter/library/widget/UserSocialView;->m:Lcom/twitter/library/widget/ActionButton;

    sget v2, Lcom/twitter/library/provider/bi;->p:I

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Lcom/twitter/library/provider/ay;->b(I)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/ActionButton;->setChecked(Z)V

    goto :goto_4

    :cond_5
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/ev;->b:Lcom/twitter/android/client/c;

    iget-boolean v12, v1, Lcom/twitter/android/client/c;->f:Z

    invoke-virtual/range {v7 .. v12}, Lcom/twitter/library/widget/UserSocialView;->a(IILjava/lang/String;IZ)V

    goto :goto_5
.end method

.method private a(Landroid/view/View;JJ)V
    .locals 5

    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    iget-object v0, p0, Lcom/twitter/android/ev;->l:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v1, p4, v3

    if-nez v1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v1, p2, v3

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/ev;->l:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/twitter/android/ev;->m:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public a(J)Ljava/lang/Long;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/ev;->l:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method

.method public a()V
    .locals 3

    invoke-super {p0}, Lcom/twitter/android/ic;->a()V

    iget-object v0, p0, Lcom/twitter/android/ev;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    iget-object v0, p0, Lcom/twitter/android/ev;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/zx;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ev;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    :goto_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v2, v0, Lcom/twitter/android/zx;->g:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lcom/twitter/android/zx;->b:Lcom/twitter/library/widget/BaseUserView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/BaseUserView;->invalidate()V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public a(JJ)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/ev;->l:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;Z)V
    .locals 4

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/ic;->a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;Z)V

    iget-object v0, p0, Lcom/twitter/android/ev;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_2

    iget-object v0, p0, Lcom/twitter/android/ev;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/zx;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ev;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    :goto_1
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lcom/twitter/android/zx;->g:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/zx;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/util/ae;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/twitter/android/zx;->b:Lcom/twitter/library/widget/BaseUserView;

    iget-object v0, v1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v3, v0}, Lcom/twitter/library/widget/BaseUserView;->setUserImage(Landroid/graphics/Bitmap;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public b(J)Ljava/lang/Long;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/ev;->l:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ev;->l:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 20

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v16

    const-string/jumbo v3, "start"

    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v17

    const-string/jumbo v3, "end"

    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v18

    move-object/from16 v15, p1

    check-cast v15, Lcom/twitter/internal/android/widget/GroupedRowView;

    const-string/jumbo v3, "position"

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-static/range {p3 .. p3}, Lcom/twitter/android/DiscoverFragment;->b(Landroid/database/Cursor;)Z

    move-result v3

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/twitter/android/ev;->a(Landroid/database/Cursor;)Z

    move-result v3

    if-eqz v3, :cond_3

    if-eqz v17, :cond_1

    const v3, 0x7f0202ef    # com.twitter.android.R.drawable.timeline_gap_top_bg

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/ev;->a(Landroid/view/View;Landroid/database/Cursor;I)V

    :cond_0
    :goto_0
    if-eqz v17, :cond_7

    if-eqz v18, :cond_7

    const/4 v3, 0x1

    invoke-virtual {v15, v3}, Lcom/twitter/internal/android/widget/GroupedRowView;->setSingle(Z)V

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    if-nez v3, :cond_5

    const/4 v3, 0x1

    invoke-virtual {v15, v3}, Lcom/twitter/internal/android/widget/GroupedRowView;->setGroupStyle(I)V

    :goto_1
    return-void

    :cond_1
    if-eqz v18, :cond_2

    const v3, 0x7f0202ee    # com.twitter.android.R.drawable.timeline_gap_bottom_bg

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/ev;->a(Landroid/view/View;Landroid/database/Cursor;I)V

    goto :goto_0

    :cond_2
    const v3, 0x7f0202ed    # com.twitter.android.R.drawable.timeline_gap_bg

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/ev;->a(Landroid/view/View;Landroid/database/Cursor;I)V

    goto :goto_0

    :cond_3
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/ev;->a(Landroid/view/View;Landroid/database/Cursor;)Lcom/twitter/library/provider/Tweet;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/ev;->k:Lcom/twitter/android/ex;

    move-object/from16 v19, v0

    new-instance v3, Lcom/twitter/android/ew;

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iget-wide v6, v10, Lcom/twitter/library/provider/Tweet;->o:J

    sget v8, Lcom/twitter/library/provider/bi;->f:I

    move-object/from16 v0, p3

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    sget v9, Lcom/twitter/library/provider/bi;->g:I

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    const-wide/16 v11, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-direct/range {v3 .. v14}, Lcom/twitter/android/ew;-><init>(JJIILcom/twitter/library/provider/Tweet;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v3, v2}, Lcom/twitter/android/ex;->a(Landroid/view/View;Lcom/twitter/android/ew;Landroid/os/Bundle;)V

    sget v3, Lcom/twitter/library/provider/bi;->v:I

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/DiscoverStoryMetadata;

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/android/yd;

    iget-object v4, v4, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v4, v3}, Lcom/twitter/library/widget/TweetView;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_4
    invoke-static/range {p3 .. p3}, Lcom/twitter/android/DiscoverFragment;->c(Landroid/database/Cursor;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object/from16 v3, p1

    check-cast v3, Lcom/twitter/internal/android/widget/GroupedRowView;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v3, v1}, Lcom/twitter/android/ev;->a(Lcom/twitter/internal/android/widget/GroupedRowView;Landroid/database/Cursor;)Lcom/twitter/android/ew;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/ev;->k:Lcom/twitter/android/ex;

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v4, v0, v3, v1}, Lcom/twitter/android/ex;->a(Landroid/view/View;Lcom/twitter/android/ew;Landroid/os/Bundle;)V

    goto/16 :goto_0

    :cond_5
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v3, v4, :cond_6

    const/4 v3, 0x3

    invoke-virtual {v15, v3}, Lcom/twitter/internal/android/widget/GroupedRowView;->setGroupStyle(I)V

    goto/16 :goto_1

    :cond_6
    const/4 v3, 0x2

    invoke-virtual {v15, v3}, Lcom/twitter/internal/android/widget/GroupedRowView;->setGroupStyle(I)V

    goto/16 :goto_1

    :cond_7
    const/4 v3, 0x0

    invoke-virtual {v15, v3}, Lcom/twitter/internal/android/widget/GroupedRowView;->setSingle(Z)V

    if-eqz v17, :cond_9

    const/4 v3, 0x1

    invoke-virtual {v15, v3}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    if-nez v3, :cond_8

    const/4 v3, 0x1

    invoke-virtual {v15, v3}, Lcom/twitter/internal/android/widget/GroupedRowView;->setGroupStyle(I)V

    goto/16 :goto_1

    :cond_8
    const/4 v3, 0x2

    invoke-virtual {v15, v3}, Lcom/twitter/internal/android/widget/GroupedRowView;->setGroupStyle(I)V

    goto/16 :goto_1

    :cond_9
    if-eqz v18, :cond_b

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v3, v4, :cond_a

    const/4 v3, 0x3

    invoke-virtual {v15, v3}, Lcom/twitter/internal/android/widget/GroupedRowView;->setGroupStyle(I)V

    :cond_a
    const/4 v3, 0x3

    invoke-virtual {v15, v3}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    goto/16 :goto_1

    :cond_b
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/twitter/android/ev;->getItemViewType(I)I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_c

    const/4 v3, 0x0

    invoke-virtual {v15, v3}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    goto/16 :goto_1

    :cond_c
    const/4 v3, 0x2

    invoke-virtual {v15, v3}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    goto/16 :goto_1
.end method

.method public getItemId(I)J
    .locals 2

    invoke-virtual {p0, p1}, Lcom/twitter/android/ev;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 2

    invoke-virtual {p0, p1}, Lcom/twitter/android/ev;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-static {v0}, Lcom/twitter/android/DiscoverFragment;->b(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, v0}, Lcom/twitter/android/ev;->a(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-static {v0}, Lcom/twitter/android/DiscoverFragment;->c(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-static {p2}, Lcom/twitter/android/DiscoverFragment;->b(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/ic;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p2}, Lcom/twitter/android/DiscoverFragment;->c(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030098    # com.twitter.android.R.layout.grouped_user_social_row_view

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/GroupedRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/widget/UserView;

    iget-object v2, v1, Lcom/twitter/library/widget/UserView;->m:Lcom/twitter/library/widget/ActionButton;

    const v3, 0x7f020086    # com.twitter.android.R.drawable.btn_follow_action_bg

    invoke-virtual {v2, v3}, Lcom/twitter/library/widget/ActionButton;->setBackgroundResource(I)V

    const v2, 0x7f020085    # com.twitter.android.R.drawable.btn_follow_action

    iget-object v3, p0, Lcom/twitter/android/ev;->i:Lcom/twitter/library/widget/a;

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/widget/UserView;->a(ILcom/twitter/library/widget/a;)V

    new-instance v2, Lcom/twitter/android/fb;

    invoke-direct {v2, v1}, Lcom/twitter/android/fb;-><init>(Lcom/twitter/library/widget/UserView;)V

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/UserView;->setTag(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/twitter/android/ev;->h:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const v0, 0x7f03008f    # com.twitter.android.R.layout.grouped_more_row_view

    new-instance v1, Lcom/twitter/android/to;

    const v2, 0x7f0f0561    # com.twitter.android.R.string.who_to_follow_view_more

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/android/to;-><init>(Ljava/lang/String;Landroid/content/Intent;)V

    iget-object v2, p0, Lcom/twitter/android/ev;->b:Lcom/twitter/android/client/c;

    invoke-virtual {v2}, Lcom/twitter/android/client/c;->V()F

    move-result v2

    invoke-static {v0, v3, p3, v1, v2}, Lcom/twitter/android/tp;->a(ILandroid/view/View;Landroid/view/ViewGroup;Lcom/twitter/android/to;F)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/GroupedRowView;->setSingle(Z)V

    goto :goto_0
.end method
