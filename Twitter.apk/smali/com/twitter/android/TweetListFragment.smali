.class public abstract Lcom/twitter/android/TweetListFragment;
.super Lcom/twitter/android/client/BaseListFragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/client/ah;
.implements Lcom/twitter/android/ob;
.implements Lcom/twitter/library/util/ac;


# instance fields
.field protected A:Lcom/twitter/library/provider/Tweet;

.field protected B:Lcom/twitter/android/vn;

.field protected C:Lcom/twitter/android/vs;

.field protected D:Lcom/twitter/android/vv;

.field protected E:Lcom/twitter/android/xr;

.field protected F:I

.field protected G:Lcom/twitter/library/scribe/ScribeItem;

.field protected H:J

.field protected I:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseListFragment;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/TweetListFragment;->B:Lcom/twitter/android/vn;

    iput-object v1, p0, Lcom/twitter/android/TweetListFragment;->C:Lcom/twitter/android/vs;

    iput-object v1, p0, Lcom/twitter/android/TweetListFragment;->D:Lcom/twitter/android/vv;

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/TweetListFragment;->F:I

    iput-object v1, p0, Lcom/twitter/android/TweetListFragment;->G:Lcom/twitter/library/scribe/ScribeItem;

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/twitter/android/TweetListFragment;->H:J

    return-void
.end method


# virtual methods
.method protected Q()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetListFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/util/ac;)V

    return-void
.end method

.method protected R()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetListFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/util/ac;)V

    return-void
.end method

.method protected a(Lcom/twitter/library/provider/Tweet;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->E:Lcom/twitter/android/xr;

    invoke-virtual {v0, p1}, Lcom/twitter/android/xr;->a(Lcom/twitter/library/provider/Tweet;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a(JJ)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->E:Lcom/twitter/android/xr;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/xr;->a(J)V

    return-void
.end method

.method protected a(JLjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->E:Lcom/twitter/android/xr;

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/android/xr;->a(JLjava/lang/String;)V

    return-void
.end method

.method protected a(JLjava/lang/String;J)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->E:Lcom/twitter/android/xr;

    move-wide v1, p1

    move-object v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/xr;->a(JLjava/lang/String;J)V

    return-void
.end method

.method public a(Landroid/view/View;Lcom/twitter/library/provider/Tweet;Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->E:Lcom/twitter/android/xr;

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/android/xr;->a(Landroid/view/View;Lcom/twitter/library/provider/Tweet;Landroid/os/Bundle;)V

    return-void
.end method

.method public bridge synthetic a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V
    .locals 0

    check-cast p2, Lcom/twitter/library/provider/Tweet;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/TweetListFragment;->a(Landroid/view/View;Lcom/twitter/library/provider/Tweet;Landroid/os/Bundle;)V

    return-void
.end method

.method public a(Lcom/twitter/library/util/aa;Ljava/util/HashMap;)V
    .locals 0

    invoke-virtual {p0, p2}, Lcom/twitter/android/TweetListFragment;->a(Ljava/util/HashMap;)V

    return-void
.end method

.method protected a(Ljava/util/HashMap;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->M:Lcom/twitter/android/client/au;

    invoke-virtual {v0}, Lcom/twitter/android/client/au;->a()Z

    move-result v1

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->M:Lcom/twitter/android/client/au;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/au;->a(Z)V

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/ye;

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, p1, v1}, Lcom/twitter/android/ye;->a(Ljava/util/HashMap;Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected a(Z)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->a(Z)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->E:Lcom/twitter/android/xr;

    invoke-virtual {v0}, Lcom/twitter/android/xr;->b()V

    :cond_0
    return-void
.end method

.method public a(Landroid/widget/AbsListView;I)Z
    .locals 4

    const/4 v3, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/TweetListFragment;->E:Lcom/twitter/android/xr;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/TweetListFragment;->E:Lcom/twitter/android/xr;

    invoke-virtual {v2, p1, p2}, Lcom/twitter/android/xr;->a(Landroid/widget/AbsListView;I)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    if-eq p2, v3, :cond_1

    if-nez p2, :cond_2

    :cond_1
    if-ne p2, v3, :cond_3

    :goto_1
    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetListFragment;->n(Z)V

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public a(Landroid/widget/AbsListView;III)Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->E:Lcom/twitter/android/xr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->E:Lcom/twitter/android/xr;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/twitter/android/xr;->a(Landroid/widget/AbsListView;III)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected d()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->d()V

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->E:Lcom/twitter/android/xr;

    invoke-virtual {v0}, Lcom/twitter/android/xr;->a()V

    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetListFragment;->Q()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v1, 0x1

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string/jumbo v0, "state_delete_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/Tweet;

    iput-object v0, p0, Lcom/twitter/android/TweetListFragment;->A:Lcom/twitter/library/provider/Tweet;

    const-string/jumbo v0, "state_revealer_id"

    const-wide/high16 v2, -0x8000000000000000L

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/android/TweetListFragment;->H:J

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/TweetListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string/jumbo v2, "en_act"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/TweetListFragment;->I:Z

    invoke-virtual {p0, p0}, Lcom/twitter/android/TweetListFragment;->a(Lcom/twitter/android/client/ah;)V

    iput-boolean v1, p0, Lcom/twitter/android/TweetListFragment;->N:Z

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetListFragment;->R()V

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->D:Lcom/twitter/android/vv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->D:Lcom/twitter/android/vv;

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetListFragment;->b(Landroid/view/View$OnTouchListener;)Lcom/twitter/android/client/BaseListFragment;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/TweetListFragment;->D:Lcom/twitter/android/vv;

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onDestroyView()V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onResume()V

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->E:Lcom/twitter/android/xr;

    invoke-virtual {p0}, Lcom/twitter/android/TweetListFragment;->V()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/xr;->a(Landroid/widget/ListView;)V

    invoke-static {}, Lkn;->a()V

    invoke-static {}, Lko;->a()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->A:Lcom/twitter/library/provider/Tweet;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "state_delete_key"

    iget-object v1, p0, Lcom/twitter/android/TweetListFragment;->A:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->C:Lcom/twitter/android/vs;

    if-eqz v0, :cond_1

    const-string/jumbo v0, "state_revealer_id"

    iget-object v1, p0, Lcom/twitter/android/TweetListFragment;->C:Lcom/twitter/android/vs;

    invoke-virtual {v1}, Lcom/twitter/android/vs;->a()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :cond_1
    return-void
.end method

.method public onStop()V
    .locals 3

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onStop()V

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->E:Lcom/twitter/android/xr;

    invoke-virtual {p0}, Lcom/twitter/android/TweetListFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/xr;->a(J)V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/TweetListFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/TweetListFragment;->I:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->B:Lcom/twitter/android/vn;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/vn;

    iget-object v2, p0, Lcom/twitter/android/TweetListFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v0, p0, v2}, Lcom/twitter/android/vn;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;)V

    iput-object v0, p0, Lcom/twitter/android/TweetListFragment;->B:Lcom/twitter/android/vn;

    :cond_0
    new-instance v0, Lcom/twitter/android/vs;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/TweetListFragment;->B:Lcom/twitter/android/vn;

    invoke-direct {v0, v2, v3}, Lcom/twitter/android/vs;-><init>(Landroid/content/Context;Lcom/twitter/android/vu;)V

    iput-object v0, p0, Lcom/twitter/android/TweetListFragment;->C:Lcom/twitter/android/vs;

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->C:Lcom/twitter/android/vs;

    iget-wide v2, p0, Lcom/twitter/android/TweetListFragment;->H:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/vs;->a(J)V

    new-instance v0, Lcom/twitter/android/vv;

    iget-object v2, p0, Lcom/twitter/android/TweetListFragment;->C:Lcom/twitter/android/vs;

    invoke-virtual {p0}, Lcom/twitter/android/TweetListFragment;->V()Landroid/widget/ListView;

    move-result-object v3

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v4

    invoke-direct {v0, p0, v2, v3, v4}, Lcom/twitter/android/vv;-><init>(Lcom/twitter/android/client/BaseListFragment;Lcom/twitter/android/vs;Landroid/widget/ListView;I)V

    iput-object v0, p0, Lcom/twitter/android/TweetListFragment;->D:Lcom/twitter/android/vv;

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->D:Lcom/twitter/android/vv;

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetListFragment;->a(Landroid/view/View$OnTouchListener;)Lcom/twitter/android/client/BaseListFragment;

    :cond_1
    new-instance v0, Lcom/twitter/android/xr;

    iget-object v2, p0, Lcom/twitter/android/TweetListFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    iget v3, p0, Lcom/twitter/android/TweetListFragment;->F:I

    iget-object v4, p0, Lcom/twitter/android/TweetListFragment;->G:Lcom/twitter/library/scribe/ScribeItem;

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/xr;-><init>(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeAssociation;ILcom/twitter/library/scribe/ScribeItem;Z)V

    iput-object v0, p0, Lcom/twitter/android/TweetListFragment;->E:Lcom/twitter/android/xr;

    return-void
.end method
