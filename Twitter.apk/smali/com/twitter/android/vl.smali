.class Lcom/twitter/android/vl;
.super Landroid/support/v4/app/FragmentPagerAdapter;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/TrendsActivity;

.field private final b:Landroid/support/v4/view/ViewPager;

.field private final c:Ljava/util/ArrayList;

.field private d:I


# direct methods
.method public constructor <init>(Lcom/twitter/android/TrendsActivity;Landroid/support/v4/app/FragmentActivity;Ljava/util/ArrayList;Landroid/support/v4/view/ViewPager;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/vl;->a:Lcom/twitter/android/TrendsActivity;

    invoke-virtual {p2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v4/app/FragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/vl;->d:I

    iput-object p4, p0, Lcom/twitter/android/vl;->b:Landroid/support/v4/view/ViewPager;

    iget-object v0, p0, Lcom/twitter/android/vl;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    iput-object p3, p0, Lcom/twitter/android/vl;->c:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public a(I)Lhb;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/vl;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/vl;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/vl;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iget-object v1, p0, Lcom/twitter/android/vl;->a:Lcom/twitter/android/TrendsActivity;

    iget-object v2, v0, Lhb;->a:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Lhb;->b:Landroid/os/Bundle;

    invoke-static {v1, v2, v0}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/vl;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iget v0, v0, Lhb;->f:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/vl;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iget-object v2, p0, Lcom/twitter/android/vl;->a:Lcom/twitter/android/TrendsActivity;

    invoke-virtual {v2}, Lcom/twitter/android/TrendsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v0, v2}, Lhb;->a(Landroid/support/v4/app/FragmentManager;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-ne v2, p1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/vl;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    :goto_0
    return v0

    :cond_1
    const/4 v0, -0x2

    goto :goto_0
.end method

.method public getPageTitle(I)Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/vl;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iget-object v0, v0, Lhb;->d:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentPagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/BaseListFragment;

    iget-object v1, p0, Lcom/twitter/android/vl;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhb;

    invoke-virtual {v1, v0}, Lhb;->a(Landroid/support/v4/app/Fragment;)V

    iget-object v1, p0, Lcom/twitter/android/vl;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    if-ne p2, v1, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/client/BaseListFragment;->Z()V

    :cond_0
    return-object v0
.end method

.method public onPageScrollStateChanged(I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/vl;->a:Lcom/twitter/android/TrendsActivity;

    invoke-static {v0}, Lcom/twitter/android/TrendsActivity;->i(Lcom/twitter/android/TrendsActivity;)Lcom/twitter/internal/android/widget/HorizontalListView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(I)V

    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/vl;->a:Lcom/twitter/android/TrendsActivity;

    invoke-static {v0}, Lcom/twitter/android/TrendsActivity;->i(Lcom/twitter/android/TrendsActivity;)Lcom/twitter/internal/android/widget/HorizontalListView;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(IF)V

    return-void
.end method

.method public onPageSelected(I)V
    .locals 8

    const/4 v5, 0x1

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/twitter/android/vl;->a:Lcom/twitter/android/TrendsActivity;

    invoke-static {v0}, Lcom/twitter/android/TrendsActivity;->j(Lcom/twitter/android/TrendsActivity;)Lcom/twitter/android/vk;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/vk;->a(I)V

    iget-object v0, p0, Lcom/twitter/android/vl;->a:Lcom/twitter/android/TrendsActivity;

    invoke-static {v0}, Lcom/twitter/android/TrendsActivity;->i(Lcom/twitter/android/TrendsActivity;)Lcom/twitter/internal/android/widget/HorizontalListView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/HorizontalListView;->b(I)V

    iget-object v0, p0, Lcom/twitter/android/vl;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iget-object v1, p0, Lcom/twitter/android/vl;->a:Lcom/twitter/android/TrendsActivity;

    invoke-virtual {v1}, Lcom/twitter/android/TrendsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    iget v1, p0, Lcom/twitter/android/vl;->d:I

    const/4 v3, -0x1

    if-eq v1, v3, :cond_1

    iget-object v1, p0, Lcom/twitter/android/vl;->a:Lcom/twitter/android/TrendsActivity;

    invoke-static {v1}, Lcom/twitter/android/TrendsActivity;->b(Lcom/twitter/android/TrendsActivity;)Ljava/util/ArrayList;

    move-result-object v1

    iget v3, p0, Lcom/twitter/android/vl;->d:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhb;

    invoke-virtual {v1, v2}, Lhb;->a(Landroid/support/v4/app/FragmentManager;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/client/BaseListFragment;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/twitter/android/client/BaseListFragment;->aa()V

    :cond_0
    if-nez p1, :cond_4

    iget-object v1, p0, Lcom/twitter/android/vl;->a:Lcom/twitter/android/TrendsActivity;

    invoke-static {v1}, Lcom/twitter/android/TrendsActivity;->b(Lcom/twitter/android/TrendsActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v3, 0x2

    if-ne v1, v3, :cond_4

    iget-object v1, p0, Lcom/twitter/android/vl;->a:Lcom/twitter/android/TrendsActivity;

    invoke-static {v1}, Lcom/twitter/android/TrendsActivity;->l(Lcom/twitter/android/TrendsActivity;)Lcom/twitter/android/client/c;

    move-result-object v1

    iget-object v3, p0, Lcom/twitter/android/vl;->a:Lcom/twitter/android/TrendsActivity;

    invoke-static {v3}, Lcom/twitter/android/TrendsActivity;->k(Lcom/twitter/android/TrendsActivity;)Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    new-array v5, v5, [Ljava/lang/String;

    const-string/jumbo v6, "trends:detail_view::detail_view:navigate"

    aput-object v6, v5, v7

    invoke-virtual {v1, v3, v4, v5}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_1
    :goto_0
    invoke-virtual {v0, v2}, Lhb;->a(Landroid/support/v4/app/FragmentManager;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/BaseListFragment;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/twitter/android/client/BaseListFragment;->Z()V

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/vl;->a:Lcom/twitter/android/TrendsActivity;

    invoke-static {v0}, Lcom/twitter/android/TrendsActivity;->o(Lcom/twitter/android/TrendsActivity;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/vl;->a:Lcom/twitter/android/TrendsActivity;

    invoke-static {v0}, Lcom/twitter/android/TrendsActivity;->p(Lcom/twitter/android/TrendsActivity;)Lcom/twitter/library/widget/ComposerBarLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/widget/ComposerBarLayout;->a()V

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/vl;->a:Lcom/twitter/android/TrendsActivity;

    invoke-static {v0}, Lcom/twitter/android/TrendsActivity;->q(Lcom/twitter/android/TrendsActivity;)V

    iput p1, p0, Lcom/twitter/android/vl;->d:I

    iget-object v0, p0, Lcom/twitter/android/vl;->a:Lcom/twitter/android/TrendsActivity;

    invoke-virtual {v0}, Lcom/twitter/android/TrendsActivity;->V()V

    return-void

    :cond_4
    iget-object v1, p0, Lcom/twitter/android/vl;->a:Lcom/twitter/android/TrendsActivity;

    invoke-static {v1}, Lcom/twitter/android/TrendsActivity;->n(Lcom/twitter/android/TrendsActivity;)Lcom/twitter/android/client/c;

    move-result-object v1

    iget-object v3, p0, Lcom/twitter/android/vl;->a:Lcom/twitter/android/TrendsActivity;

    invoke-static {v3}, Lcom/twitter/android/TrendsActivity;->m(Lcom/twitter/android/TrendsActivity;)Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    new-array v5, v5, [Ljava/lang/String;

    const-string/jumbo v6, "trends:list_view::list_view:navigate"

    aput-object v6, v5, v7

    invoke-virtual {v1, v3, v4, v5}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0
.end method
