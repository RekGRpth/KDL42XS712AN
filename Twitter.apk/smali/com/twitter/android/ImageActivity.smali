.class public Lcom/twitter/android/ImageActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"


# instance fields
.field a:Landroid/widget/ImageView;

.field b:Landroid/widget/ProgressBar;

.field c:Landroid/graphics/Bitmap;

.field d:Landroid/net/Uri;

.field e:Landroid/net/Uri;

.field f:Landroid/net/Uri;

.field g:Ljava/lang/String;

.field h:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const v1, 0x7f030099    # com.twitter.android.R.layout.image_activity

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->c(Z)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->a(Z)V

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->c(I)V

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 6

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/ImageActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/ImageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Must be started with a valid Uri"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const v0, 0x7f090077    # com.twitter.android.R.id.image

    invoke-virtual {p0, v0}, Lcom/twitter/android/ImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/ImageActivity;->a:Landroid/widget/ImageView;

    const v0, 0x7f0900e0    # com.twitter.android.R.id.loading

    invoke-virtual {p0, v0}, Lcom/twitter/android/ImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/twitter/android/ImageActivity;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ImageActivity;->e:Landroid/net/Uri;

    const-string/jumbo v0, "android.intent.extra.STREAM"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "android.intent.extra.STREAM"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/twitter/android/ImageActivity;->f:Landroid/net/Uri;

    :goto_0
    const-string/jumbo v0, "android.intent.extra.TEXT"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ImageActivity;->g:Ljava/lang/String;

    const-string/jumbo v0, "image_url"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ImageActivity;->h:Ljava/lang/String;

    if-eqz p1, :cond_2

    const-string/jumbo v0, "image_bitmap"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "image_bitmap"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    :goto_1
    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/twitter/android/ImageActivity;->a:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v2, p0, Lcom/twitter/android/ImageActivity;->a:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iput-object v0, p0, Lcom/twitter/android/ImageActivity;->c:Landroid/graphics/Bitmap;

    :goto_2
    invoke-virtual {p0}, Lcom/twitter/android/ImageActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v4, "tweet::photo::impression"

    aput-object v4, v0, v5

    invoke-virtual {v1, v2, v3, v0}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ImageActivity;->e:Landroid/net/Uri;

    iput-object v0, p0, Lcom/twitter/android/ImageActivity;->f:Landroid/net/Uri;

    goto :goto_0

    :cond_2
    iget-object v0, v1, Lcom/twitter/android/client/c;->b:Lcom/twitter/library/util/aa;

    new-instance v2, Lcom/twitter/library/util/m;

    iget-object v3, p0, Lcom/twitter/android/ImageActivity;->e:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/twitter/library/util/aa;->b(Lcom/twitter/library/util/m;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    :cond_3
    new-instance v0, Lcom/twitter/android/ik;

    invoke-direct {v0, p0}, Lcom/twitter/android/ik;-><init>(Lcom/twitter/android/ImageActivity;)V

    new-array v2, v4, [Landroid/net/Uri;

    iget-object v3, p0, Lcom/twitter/android/ImageActivity;->e:Landroid/net/Uri;

    aput-object v3, v2, v5

    invoke-virtual {v0, v2}, Lcom/twitter/android/ik;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_2
.end method

.method protected a(Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lcom/twitter/internal/android/widget/ToolBar;)Z

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.SEND"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v3, 0x10000000

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "text/plain"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "android.intent.extra.TEXT"

    iget-object v4, p0, Lcom/twitter/android/ImageActivity;->h:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0}, Lcom/twitter/android/ImageActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/high16 v4, 0x10000

    invoke-virtual {v0, v3, v4}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v4

    iget-object v0, p0, Lcom/twitter/android/ImageActivity;->c:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    const v5, 0x7f090070    # com.twitter.android.R.id.share

    invoke-virtual {p1, v5}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v5

    invoke-virtual {v5, v3}, Lhn;->a(Landroid/content/Intent;)Lhn;

    move-result-object v5

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_2

    move v3, v1

    :goto_1
    invoke-virtual {v5, v3}, Lhn;->c(Z)Lhn;

    const v3, 0x7f090141    # com.twitter.android.R.id.save

    invoke-virtual {p1, v3}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v3

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ImageActivity;->d:Landroid/net/Uri;

    if-nez v0, :cond_0

    move v2, v1

    :cond_0
    invoke-virtual {v3, v2}, Lhn;->c(Z)Lhn;

    return v1

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v3, v2

    goto :goto_1
.end method

.method protected a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z

    const v0, 0x7f110011    # com.twitter.android.R.menu.image_viewer

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    const/4 v0, 0x1

    return v0
.end method

.method public a(Lhn;)Z
    .locals 5

    const/4 v0, 0x1

    invoke-virtual {p1}, Lhn;->a()I

    move-result v1

    const v2, 0x7f090141    # com.twitter.android.R.id.save

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/twitter/android/ImageActivity;->c:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    new-instance v1, Lcom/twitter/android/il;

    invoke-direct {v1, p0}, Lcom/twitter/android/il;-><init>(Lcom/twitter/android/ImageActivity;)V

    new-array v2, v0, [Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/ImageActivity;->c:Landroid/graphics/Bitmap;

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/android/il;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    :goto_0
    return v0

    :cond_1
    const v2, 0x7f090318    # com.twitter.android.R.id.open

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/twitter/android/ImageActivity;->f:Landroid/net/Uri;

    invoke-static {p0, v1}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhn;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 2

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0f038a    # com.twitter.android.R.string.saving

    invoke-virtual {p0, v1}, Lcom/twitter/android/ImageActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/ImageActivity;->c:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "image_bitmap"

    iget-object v1, p0, Lcom/twitter/android/ImageActivity;->c:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
