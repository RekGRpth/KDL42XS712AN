.class Lcom/twitter/android/hb;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field private a:Ljava/lang/ref/WeakReference;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/twitter/android/client/BaseFragmentActivity;)V
    .locals 1

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/hb;->a:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method private c()Lcom/twitter/android/client/BaseFragmentActivity;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/hb;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/hb;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/BaseFragmentActivity;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected varargs a([Lcom/twitter/android/hi;)Landroid/net/Uri;
    .locals 4

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/twitter/android/hb;->c()Lcom/twitter/android/client/BaseFragmentActivity;

    move-result-object v2

    if-eqz v2, :cond_2

    const/4 v0, 0x0

    aget-object v3, p1, v0

    if-eqz v3, :cond_0

    iget-object v0, v3, Lcom/twitter/android/hi;->f:Ljava/lang/ref/WeakReference;

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {v2}, Lcom/twitter/android/client/BaseFragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, v3, Lcom/twitter/android/hi;->d:Lcom/twitter/library/util/m;

    iget-object v3, v3, Lcom/twitter/library/util/m;->a:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v0, v3, v1}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;Lcom/twitter/library/util/l;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_2
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_2
.end method

.method protected a(Landroid/net/Uri;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/twitter/android/hb;->c()Lcom/twitter/android/client/BaseFragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/BaseFragmentActivity;->removeDialog(I)V

    if-eqz p1, :cond_1

    const v1, 0x7f0f0387    # com.twitter.android.R.string.save_image_success

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/hb;->b:Z

    return-void

    :cond_1
    const v1, 0x7f0f0386    # com.twitter.android.R.string.save_image_failure

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/client/BaseFragmentActivity;)V
    .locals 1

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/hb;->a:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/hb;->b:Z

    return v0
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/hb;->a:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Lcom/twitter/android/hi;

    invoke-virtual {p0, p1}, Lcom/twitter/android/hb;->a([Lcom/twitter/android/hi;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lcom/twitter/android/hb;->a(Landroid/net/Uri;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    invoke-direct {p0}, Lcom/twitter/android/hb;->c()Lcom/twitter/android/client/BaseFragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/BaseFragmentActivity;->showDialog(I)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/hb;->b:Z

    return-void
.end method
