.class Lcom/twitter/android/bk;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/BaseSignUpActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/BaseSignUpActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;Ljava/util/ArrayList;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    invoke-static {v0, p2}, Lcom/twitter/android/BaseSignUpActivity;->c(Lcom/twitter/android/BaseSignUpActivity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_0

    if-eqz p5, :cond_0

    invoke-virtual {p5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    iget-object v0, v0, Lcom/twitter/android/BaseSignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    iget-object v1, v0, Lcom/twitter/android/BaseSignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    const/4 v0, 0x0

    invoke-virtual {p5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Lcom/twitter/internal/android/widget/PopupEditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    const/4 v1, 0x1

    iput v1, v0, Lcom/twitter/android/BaseSignUpActivity;->m:I

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    iget-object v0, v0, Lcom/twitter/android/BaseSignUpActivity;->o:Lcom/twitter/android/zy;

    invoke-virtual {v0, p5}, Lcom/twitter/android/zy;->a(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    invoke-virtual {v0}, Lcom/twitter/android/BaseSignUpActivity;->l_()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    iget-object v0, v0, Lcom/twitter/android/BaseSignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->a()V

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    invoke-virtual {v0}, Lcom/twitter/android/BaseSignUpActivity;->a()V

    goto :goto_0
.end method

.method public b(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    invoke-static {v0, p2}, Lcom/twitter/android/BaseSignUpActivity;->a(Lcom/twitter/android/BaseSignUpActivity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sparse-switch p3, :sswitch_data_0

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    invoke-virtual {v0}, Lcom/twitter/android/BaseSignUpActivity;->a()V

    :cond_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    iget-object v1, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    iget-object v1, v1, Lcom/twitter/android/BaseSignUpActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v0, v1, v3}, Lcom/twitter/android/BaseSignUpActivity;->a(Landroid/widget/EditText;Z)V

    iget-object v0, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    iput v3, v0, Lcom/twitter/android/BaseSignUpActivity;->l:I

    iget-object v0, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    iget-object v1, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    iget-object v1, v1, Lcom/twitter/android/BaseSignUpActivity;->b:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    iget-object v2, v2, Lcom/twitter/android/BaseSignUpActivity;->h:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/BaseSignUpActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    iget-object v1, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    iget-object v1, v1, Lcom/twitter/android/BaseSignUpActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/BaseSignUpActivity;->a(Landroid/widget/EditText;Z)V

    iget-object v0, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    iput v2, v0, Lcom/twitter/android/BaseSignUpActivity;->l:I

    iget-object v0, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    iget-object v1, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    iget-object v1, v1, Lcom/twitter/android/BaseSignUpActivity;->b:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    iget-object v2, v2, Lcom/twitter/android/BaseSignUpActivity;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v2, p4}, Lcom/twitter/android/BaseSignUpActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0x190 -> :sswitch_1
    .end sparse-switch
.end method

.method public c(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    invoke-static {v0, p2}, Lcom/twitter/android/BaseSignUpActivity;->b(Lcom/twitter/android/BaseSignUpActivity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sparse-switch p3, :sswitch_data_0

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    invoke-virtual {v0}, Lcom/twitter/android/BaseSignUpActivity;->a()V

    :cond_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    iget-object v1, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    iget-object v1, v1, Lcom/twitter/android/BaseSignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0, v1, v3}, Lcom/twitter/android/BaseSignUpActivity;->a(Landroid/widget/EditText;Z)V

    iget-object v0, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    iput v3, v0, Lcom/twitter/android/BaseSignUpActivity;->m:I

    iget-object v0, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    iget-object v1, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    iget-object v1, v1, Lcom/twitter/android/BaseSignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    iget-object v2, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    iget-object v2, v2, Lcom/twitter/android/BaseSignUpActivity;->i:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/BaseSignUpActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    iget-object v1, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    iget-object v1, v1, Lcom/twitter/android/BaseSignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/BaseSignUpActivity;->a(Landroid/widget/EditText;Z)V

    iget-object v0, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    iput v2, v0, Lcom/twitter/android/BaseSignUpActivity;->m:I

    iget-object v0, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    iget-object v1, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    iget-object v1, v1, Lcom/twitter/android/BaseSignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    iget-object v2, p0, Lcom/twitter/android/bk;->a:Lcom/twitter/android/BaseSignUpActivity;

    iget-object v2, v2, Lcom/twitter/android/BaseSignUpActivity;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v2, p4}, Lcom/twitter/android/BaseSignUpActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0x190 -> :sswitch_1
    .end sparse-switch
.end method
