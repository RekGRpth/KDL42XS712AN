.class Lcom/twitter/android/ee;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# instance fields
.field final synthetic a:Lcom/twitter/android/DMInboxFragment;


# direct methods
.method private constructor <init>(Lcom/twitter/android/DMInboxFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/ee;->a:Lcom/twitter/android/DMInboxFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/DMInboxFragment;Lcom/twitter/android/ec;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/ee;-><init>(Lcom/twitter/android/DMInboxFragment;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ee;->a:Lcom/twitter/android/DMInboxFragment;

    invoke-static {v0}, Lcom/twitter/android/DMInboxFragment;->b(Lcom/twitter/android/DMInboxFragment;)V

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/ee;->a:Lcom/twitter/android/DMInboxFragment;

    invoke-static {v0}, Lcom/twitter/android/DMInboxFragment;->e(Lcom/twitter/android/DMInboxFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/ef;

    invoke-direct {v1, p0, p2}, Lcom/twitter/android/ef;-><init>(Lcom/twitter/android/ee;Landroid/database/Cursor;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ee;->a:Lcom/twitter/android/DMInboxFragment;

    invoke-static {v0}, Lcom/twitter/android/DMInboxFragment;->c(Lcom/twitter/android/DMInboxFragment;)V

    iget-object v0, p0, Lcom/twitter/android/ee;->a:Lcom/twitter/android/DMInboxFragment;

    invoke-static {v0}, Lcom/twitter/android/DMInboxFragment;->d(Lcom/twitter/android/DMInboxFragment;)Landroid/view/View;

    move-result-object v0

    const v2, 0x7f090128    # com.twitter.android.R.id.request_count

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7

    const/4 v4, 0x0

    new-instance v0, Lcom/twitter/android/bl;

    iget-object v1, p0, Lcom/twitter/android/ee;->a:Lcom/twitter/android/DMInboxFragment;

    invoke-virtual {v1}, Lcom/twitter/android/DMInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/provider/ad;->a:Landroid/net/Uri;

    iget-object v3, p0, Lcom/twitter/android/ee;->a:Lcom/twitter/android/DMInboxFragment;

    invoke-static {v3}, Lcom/twitter/android/DMInboxFragment;->a(Lcom/twitter/android/DMInboxFragment;)Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    invoke-static {v2, v5, v6}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/library/provider/ad;->b:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/bl;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/ee;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ee;->a:Lcom/twitter/android/DMInboxFragment;

    invoke-static {v0}, Lcom/twitter/android/DMInboxFragment;->e(Lcom/twitter/android/DMInboxFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    return-void
.end method
