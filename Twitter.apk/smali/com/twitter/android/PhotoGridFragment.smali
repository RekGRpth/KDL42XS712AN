.class public Lcom/twitter/android/PhotoGridFragment;
.super Lcom/twitter/android/client/BaseListFragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/client/ah;
.implements Lcom/twitter/library/util/ac;


# instance fields
.field a:Z

.field private final b:Ljava/util/ArrayList;

.field private final c:Ljava/util/HashSet;

.field private d:J

.field private e:Landroid/net/Uri;

.field private f:Lcom/twitter/android/oi;

.field private g:Lcom/twitter/library/util/aa;

.field private h:Z

.field private i:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/client/BaseListFragment;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/PhotoGridFragment;->b:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/PhotoGridFragment;->c:Ljava/util/HashSet;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/PhotoGridFragment;)Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PhotoGridFragment;->e:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/PhotoGridFragment;)Ljava/util/HashSet;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PhotoGridFragment;->c:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/PhotoGridFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/PhotoGridFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method private d(I)J
    .locals 4

    const-wide/16 v0, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid fetch type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v2, p0, Lcom/twitter/android/PhotoGridFragment;->f:Lcom/twitter/android/oi;

    invoke-virtual {v2}, Lcom/twitter/android/oi;->a()Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0x15

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    :cond_0
    :pswitch_1
    return-wide v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic d(Lcom/twitter/android/PhotoGridFragment;)Lcom/twitter/library/scribe/ScribeAssociation;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PhotoGridFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/PhotoGridFragment;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PhotoGridFragment;->b:Ljava/util/ArrayList;

    return-object v0
.end method

.method private e()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PhotoGridFragment;->f:Lcom/twitter/android/oi;

    invoke-virtual {v0}, Lcom/twitter/android/oi;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/PhotoGridFragment;->h:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g(I)J
    .locals 4

    const-wide/16 v0, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid fetch type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v2, p0, Lcom/twitter/android/PhotoGridFragment;->f:Lcom/twitter/android/oi;

    invoke-virtual {v2}, Lcom/twitter/android/oi;->a()Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->moveToLast()Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0x15

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    :cond_0
    :pswitch_1
    return-wide v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    const v0, 0x7f0300e1    # com.twitter.android.R.layout.photo_grid_layout

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/android/PhotoGridFragment;->a(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;IILcom/twitter/library/service/b;)V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p4, Lcom/twitter/library/service/b;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/android/PhotoGridFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    invoke-virtual {p4}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-nez v0, :cond_1

    const v0, 0x7f0f04e3    # com.twitter.android.R.string.tweets_fetch_error

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p4}, Lcom/twitter/library/service/b;->u()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/PhotoGridFragment;->b(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    check-cast p4, Ljs;

    invoke-virtual {p4}, Ljs;->e()I

    move-result v0

    if-nez v0, :cond_0

    iput-boolean v1, p0, Lcom/twitter/android/PhotoGridFragment;->a:Z

    goto :goto_0
.end method

.method protected a(Landroid/database/Cursor;)V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/PhotoGridFragment;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/PhotoGridFragment;->i:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/PhotoGridFragment;->a:Z

    if-nez v0, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/16 v1, 0x190

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/PhotoGridFragment;->c(I)Z

    :cond_0
    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PhotoGridFragment;->f:Lcom/twitter/android/oi;

    invoke-virtual {v0, p2}, Lcom/twitter/android/oi;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    iget v0, p0, Lcom/twitter/android/PhotoGridFragment;->i:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/PhotoGridFragment;->b(I)V

    invoke-direct {p0}, Lcom/twitter/android/PhotoGridFragment;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/PhotoGridFragment;->c(I)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/PhotoGridFragment;->h:Z

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/util/aa;Ljava/util/HashMap;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PhotoGridFragment;->f:Lcom/twitter/android/oi;

    invoke-virtual {v0, p2}, Lcom/twitter/android/oi;->a(Ljava/util/HashMap;)V

    return-void
.end method

.method protected a(Z)V
    .locals 2

    const/4 v1, 0x5

    if-eqz p1, :cond_1

    invoke-virtual {p0, v1}, Lcom/twitter/android/PhotoGridFragment;->a_(I)V

    invoke-virtual {p0}, Lcom/twitter/android/PhotoGridFragment;->P()Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/PhotoGridFragment;->f:Lcom/twitter/android/oi;

    invoke-virtual {v0}, Lcom/twitter/android/oi;->a()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {p0, v1}, Lcom/twitter/android/PhotoGridFragment;->a_(I)V

    invoke-virtual {p0}, Lcom/twitter/android/PhotoGridFragment;->p()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/twitter/android/PhotoGridFragment;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/PhotoGridFragment;->c(I)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/PhotoGridFragment;->h:Z

    goto :goto_0
.end method

.method public a(Landroid/widget/AbsListView;I)Z
    .locals 3

    const/4 v0, 0x2

    const/4 v1, 0x0

    if-eq p2, v0, :cond_0

    if-nez p2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/twitter/android/PhotoGridFragment;->f:Lcom/twitter/android/oi;

    if-ne p2, v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/twitter/android/oi;->c(Z)V

    :cond_1
    return v1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Landroid/widget/AbsListView;III)Z
    .locals 2

    const/4 v1, 0x1

    if-nez p3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    if-nez p2, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/PhotoGridFragment;->al()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/PhotoGridFragment;->f:Lcom/twitter/android/oi;

    invoke-virtual {v0}, Lcom/twitter/android/oi;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    if-lez p2, :cond_0

    add-int v0, p2, p3

    if-lt v0, p4, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PhotoGridFragment;->f:Lcom/twitter/android/oi;

    invoke-virtual {v0}, Lcom/twitter/android/oi;->a()Landroid/database/Cursor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/PhotoGridFragment;->a(Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method protected a_(I)V
    .locals 0

    iput p1, p0, Lcom/twitter/android/PhotoGridFragment;->i:I

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->a_(I)V

    return-void
.end method

.method protected b(I)V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/PhotoGridFragment;->i:I

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->b(I)V

    return-void
.end method

.method protected c(I)Z
    .locals 6

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lcom/twitter/android/PhotoGridFragment;->c_(I)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return v1

    :cond_0
    iput p1, p0, Lcom/twitter/android/PhotoGridFragment;->i:I

    new-instance v0, Ljs;

    invoke-virtual {p0}, Lcom/twitter/android/PhotoGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/PhotoGridFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-wide v4, p0, Lcom/twitter/android/PhotoGridFragment;->d:J

    invoke-direct {v0, v2, v3, v4, v5}, Ljs;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V

    invoke-direct {p0, p1}, Lcom/twitter/android/PhotoGridFragment;->d(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljs;->b(J)Ljs;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/twitter/android/PhotoGridFragment;->g(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljs;->c(J)Ljs;

    move-result-object v2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    :cond_1
    const/16 v0, 0x14

    :goto_1
    invoke-virtual {v2, v0}, Ljs;->a(I)Ljs;

    move-result-object v0

    const/16 v2, 0x11

    invoke-virtual {v0, v2}, Ljs;->c(I)Lcom/twitter/library/service/b;

    invoke-virtual {p0}, Lcom/twitter/android/PhotoGridFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/twitter/android/client/c;->c(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    invoke-virtual {p0, v0, v1, p1}, Lcom/twitter/android/PhotoGridFragment;->a(Lcom/twitter/library/service/b;II)Z

    move-result v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method protected i()V
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/android/PhotoGridFragment;->c(I)Z

    return-void
.end method

.method protected o()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PhotoGridFragment;->f:Lcom/twitter/android/oi;

    invoke-virtual {v0}, Lcom/twitter/android/oi;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 9

    const-wide/16 v2, -0x1

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/PhotoGridFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz p1, :cond_1

    const-string/jumbo v0, "user_id"

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/PhotoGridFragment;->d:J

    const-string/jumbo v0, "is_last"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/PhotoGridFragment;->a:Z

    :goto_0
    new-instance v0, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->a(I)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/PhotoGridFragment;->d:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    const-string/jumbo v1, "photo_grid"

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/PhotoGridFragment;->b(Lcom/twitter/library/scribe/ScribeAssociation;)V

    invoke-virtual {p0}, Lcom/twitter/android/PhotoGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/PhotoGridFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/PhotoGridFragment;->g:Lcom/twitter/library/util/aa;

    if-nez v2, :cond_0

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->f(Landroid/content/Context;)Lcom/twitter/library/util/aa;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/twitter/library/util/aa;->a(Lcom/twitter/library/util/ac;)V

    iput-object v2, p0, Lcom/twitter/android/PhotoGridFragment;->g:Lcom/twitter/library/util/aa;

    :cond_0
    sget-object v0, Lcom/twitter/library/provider/at;->r:Landroid/net/Uri;

    iget-wide v3, p0, Lcom/twitter/android/PhotoGridFragment;->d:J

    invoke-static {v0, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v3, "ownerId"

    invoke-virtual {p0}, Lcom/twitter/android/PhotoGridFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/PhotoGridFragment;->e:Landroid/net/Uri;

    new-instance v0, Lcom/twitter/android/oi;

    invoke-virtual {p0}, Lcom/twitter/android/PhotoGridFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0009    # com.twitter.android.R.integer.photo_target_ratio

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    int-to-float v3, v3

    const/4 v4, 0x1

    const/4 v5, -0x1

    new-instance v6, Lcom/twitter/android/om;

    invoke-direct {v6, p0, v1}, Lcom/twitter/android/om;-><init>(Lcom/twitter/android/PhotoGridFragment;Landroid/content/Context;)V

    new-instance v7, Lcom/twitter/android/on;

    const/4 v8, 0x0

    invoke-direct {v7, p0, v8}, Lcom/twitter/android/on;-><init>(Lcom/twitter/android/PhotoGridFragment;Lcom/twitter/android/om;)V

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/oi;-><init>(Landroid/content/Context;Lcom/twitter/library/util/aa;FIILandroid/view/View$OnClickListener;Lcom/twitter/android/ob;)V

    iput-object v0, p0, Lcom/twitter/android/PhotoGridFragment;->f:Lcom/twitter/android/oi;

    iget-object v0, p0, Lcom/twitter/android/PhotoGridFragment;->f:Lcom/twitter/android/oi;

    invoke-virtual {p0, v0}, Lcom/twitter/android/PhotoGridFragment;->a(Lcom/twitter/android/client/av;)Lcom/twitter/android/client/BaseListFragment;

    invoke-virtual {p0}, Lcom/twitter/android/PhotoGridFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/PhotoGridFragment;->f:Lcom/twitter/android/oi;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void

    :cond_1
    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/PhotoGridFragment;->d:J

    goto/16 :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, p0}, Lcom/twitter/android/PhotoGridFragment;->a(Lcom/twitter/android/client/ah;)V

    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7

    invoke-virtual {p0}, Lcom/twitter/android/PhotoGridFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/library/api/UserSettings;

    move-result-object v0

    iget-boolean v0, v0, Lcom/twitter/library/api/UserSettings;->k:Z

    if-eqz v0, :cond_0

    const-string/jumbo v4, "flags&1 != 0"

    :goto_0
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/twitter/android/PhotoGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/PhotoGridFragment;->e:Landroid/net/Uri;

    sget-object v3, Lcom/twitter/library/provider/Tweet;->a:[Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v6, "updated_at DESC, _id ASC"

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_0
    const-string/jumbo v4, "flags&1 != 0 AND flags&64 = 0"

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/PhotoGridFragment;->f:Lcom/twitter/android/oi;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/oi;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onDestroy()V

    return-void
.end method

.method public onDestroyView()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PhotoGridFragment;->g:Lcom/twitter/library/util/aa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PhotoGridFragment;->g:Lcom/twitter/library/util/aa;

    invoke-virtual {v0, p0}, Lcom/twitter/library/util/aa;->b(Lcom/twitter/library/util/ac;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/PhotoGridFragment;->g:Lcom/twitter/library/util/aa;

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onDestroyView()V

    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/PhotoGridFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/PhotoGridFragment;->f:Lcom/twitter/android/oi;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/oi;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    return-void
.end method

.method public onPause()V
    .locals 5

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onPause()V

    iget-object v0, p0, Lcom/twitter/android/PhotoGridFragment;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "profile:photo_grid:stream::tweets"

    invoke-virtual {p0}, Lcom/twitter/android/PhotoGridFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/PhotoGridFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/PhotoGridFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/PhotoGridFragment;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/util/ArrayList;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    iget-object v0, p0, Lcom/twitter/android/PhotoGridFragment;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onResume()V

    iget-wide v0, p0, Lcom/twitter/android/PhotoGridFragment;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-virtual {p0, v4}, Lcom/twitter/android/PhotoGridFragment;->a(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/PhotoGridFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/PhotoGridFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "profile:photo_grid:::impression"

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/PhotoGridFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "user_id"

    iget-wide v1, p0, Lcom/twitter/android/PhotoGridFragment;->d:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v0, "is_last"

    iget-boolean v1, p0, Lcom/twitter/android/PhotoGridFragment;->a:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
