.class public abstract Lcom/twitter/android/BaseEditProfileActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Lcom/twitter/android/widget/ce;
.implements Lcom/twitter/library/util/ar;


# instance fields
.field protected a:Lcom/twitter/library/scribe/ScribeAssociation;

.field b:Landroid/net/Uri;

.field c:Landroid/net/Uri;

.field d:Landroid/net/Uri;

.field e:Landroid/net/Uri;

.field f:Landroid/net/Uri;

.field g:Z

.field h:Z

.field i:Z

.field j:J

.field k:Landroid/graphics/Rect;

.field l:Lcom/twitter/library/util/m;

.field m:Lcom/twitter/library/util/m;

.field n:Landroid/widget/ImageView;

.field o:Landroid/widget/ImageView;

.field p:Landroid/widget/EditText;

.field q:Ljava/lang/String;

.field r:Ljava/lang/String;

.field s:Ljava/lang/String;

.field t:Ljava/lang/String;

.field private u:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/BaseEditProfileActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/io/File;)Z
    .locals 1

    if-eqz p1, :cond_0

    invoke-static {p0}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/BaseEditProfileActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/BaseEditProfileActivity;)Lcom/twitter/library/client/aa;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected abstract a()Lcom/twitter/library/scribe/ScribeAssociation;
.end method

.method protected a(I)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/twitter/android/BaseEditProfileActivity;->setResult(I)V

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->j()V

    return-void
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 11

    const/4 v0, 0x2

    const v10, 0x7f0f0514    # com.twitter.android.R.string.unsupported_feature

    const/4 v9, 0x0

    const/4 v3, 0x1

    const/4 v8, 0x0

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->e:Landroid/net/Uri;

    invoke-static {v0}, Lcom/twitter/library/util/n;->b(Landroid/net/Uri;)Z

    invoke-virtual {p0, v8}, Lcom/twitter/android/BaseEditProfileActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->finish()V

    goto :goto_0

    :pswitch_1
    if-nez p3, :cond_2

    iput-boolean v8, p0, Lcom/twitter/android/BaseEditProfileActivity;->g:Z

    iget-wide v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->j:J

    invoke-static {p0, v8, v0, v1}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;ZJ)Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/BaseEditProfileActivity;->a(Ljava/io/File;)Z

    move-result v1

    if-nez v1, :cond_1

    const v0, 0x7f0f0088    # com.twitter.android.R.string.camera_photo_error

    invoke-static {p0, v0, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_1
    const v1, 0x7f0f013c    # com.twitter.android.R.string.edit_profile_header

    invoke-virtual {p0, v1}, Lcom/twitter/android/BaseEditProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v3, "title"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v3, "description"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->f:Landroid/net/Uri;

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "output"

    iget-object v2, p0, Lcom/twitter/android/BaseEditProfileActivity;->f:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/BaseEditProfileActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {p0, v10}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;I)V

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->f:Landroid/net/Uri;

    invoke-static {v0}, Lcom/twitter/library/util/n;->b(Landroid/net/Uri;)Z

    iput-object v9, p0, Lcom/twitter/android/BaseEditProfileActivity;->f:Landroid/net/Uri;

    goto :goto_0

    :cond_2
    if-ne p3, v3, :cond_3

    iput-boolean v8, p0, Lcom/twitter/android/BaseEditProfileActivity;->g:Z

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.PICK"

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/4 v1, 0x2

    :try_start_1
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/BaseEditProfileActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {p0, v10}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;I)V

    goto/16 :goto_0

    :cond_3
    if-ne p3, v0, :cond_0

    iput-object v9, p0, Lcom/twitter/android/BaseEditProfileActivity;->b:Landroid/net/Uri;

    iput-object v9, p0, Lcom/twitter/android/BaseEditProfileActivity;->e:Landroid/net/Uri;

    iput-boolean v3, p0, Lcom/twitter/android/BaseEditProfileActivity;->g:Z

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->n:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getId()I

    move-result v0

    invoke-virtual {p0, v9, v0}, Lcom/twitter/android/BaseEditProfileActivity;->a(Landroid/graphics/Bitmap;I)V

    goto/16 :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    if-nez p3, :cond_5

    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/BaseEditProfileActivity;->a:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v5, "change_avatar_dialog"

    const-string/jumbo v6, "take_photo"

    const-string/jumbo v7, "click"

    invoke-static {v4, v5, v6, v7}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->j:J

    invoke-static {p0, v8, v0, v1}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;ZJ)Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/BaseEditProfileActivity;->a(Ljava/io/File;)Z

    move-result v1

    if-nez v1, :cond_4

    const v0, 0x7f0f0088    # com.twitter.android.R.string.camera_photo_error

    invoke-static {p0, v0, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_4
    const v1, 0x7f0f0138    # com.twitter.android.R.string.edit_profile_avatar

    invoke-virtual {p0, v1}, Lcom/twitter/android/BaseEditProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v3, "title"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v3, "description"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Landroid/net/Uri;

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "output"

    iget-object v2, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x4

    :try_start_2
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/BaseEditProfileActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v0

    invoke-static {p0, v10}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;I)V

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Landroid/net/Uri;

    invoke-static {v0}, Lcom/twitter/library/util/n;->b(Landroid/net/Uri;)Z

    iput-object v9, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Landroid/net/Uri;

    goto/16 :goto_0

    :cond_5
    if-ne p3, v3, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/BaseEditProfileActivity;->a:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v5, "change_avatar_dialog"

    const-string/jumbo v6, "choose_photo"

    const-string/jumbo v7, "click"

    invoke-static {v4, v5, v6, v7}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.PICK"

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/4 v1, 0x5

    :try_start_3
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/BaseEditProfileActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_3
    .catch Landroid/content/ActivityNotFoundException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    :catch_3
    move-exception v0

    invoke-static {p0, v10}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method protected abstract a(Landroid/content/Intent;)V
.end method

.method protected abstract a(Landroid/graphics/Bitmap;)V
.end method

.method a(Landroid/graphics/Bitmap;I)V
    .locals 3

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->n:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getId()I

    move-result v1

    if-ne p2, v1, :cond_3

    iput-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->u:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->n:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0031    # com.twitter.android.R.color.dark_gray

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->o:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getId()I

    move-result v1

    if-ne p2, v1, :cond_0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->o:Landroid/widget/ImageView;

    const v1, 0x7f02003a    # com.twitter.android.R.drawable.bg_no_profile_photo_md

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method

.method protected abstract a(Landroid/net/Uri;)V
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 10

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v3

    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->a()Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->a:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    new-array v0, v1, [Ljava/lang/String;

    iget-object v6, p0, Lcom/twitter/android/BaseEditProfileActivity;->a:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v7, ""

    const-string/jumbo v8, ""

    const-string/jumbo v9, "impression"

    invoke-static {v6, v7, v8, v9}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v2

    invoke-virtual {v3, v4, v5, v0}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    const v0, 0x7f09007a    # com.twitter.android.R.id.avatar_image

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseEditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->o:Landroid/widget/ImageView;

    const v0, 0x7f090148    # com.twitter.android.R.id.header_image

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseEditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->n:Landroid/widget/ImageView;

    const v0, 0x7f090147    # com.twitter.android.R.id.edit_bio

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseEditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->p:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v4

    iget-object v0, v4, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->q:Ljava/lang/String;

    iget-wide v5, v4, Lcom/twitter/library/api/TwitterUser;->userId:J

    iput-wide v5, p0, Lcom/twitter/android/BaseEditProfileActivity;->j:J

    if-eqz p1, :cond_4

    const-string/jumbo v0, "header_uri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->f:Landroid/net/Uri;

    const-string/jumbo v0, "cached_header_uri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->e:Landroid/net/Uri;

    const-string/jumbo v0, "pending_avatar_uri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Landroid/net/Uri;

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Landroid/net/Uri;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/twitter/android/ax;

    iget-object v5, p0, Lcom/twitter/android/BaseEditProfileActivity;->o:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getId()I

    move-result v5

    invoke-direct {v0, p0, v5}, Lcom/twitter/android/ax;-><init>(Lcom/twitter/android/BaseEditProfileActivity;I)V

    new-array v5, v1, [Landroid/net/Uri;

    iget-object v6, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Landroid/net/Uri;

    aput-object v6, v5, v2

    invoke-virtual {v0, v5}, Lcom/twitter/android/ax;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    const-string/jumbo v0, "pending_header_uri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->b:Landroid/net/Uri;

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->e:Landroid/net/Uri;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->e:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseEditProfileActivity;->a(Landroid/net/Uri;)V

    :cond_1
    :goto_0
    new-instance v0, Lcom/twitter/library/util/m;

    iget-object v5, v4, Lcom/twitter/library/api/TwitterUser;->profileHeaderImageUrl:Ljava/lang/String;

    iget v6, v3, Lcom/twitter/android/client/c;->d:F

    invoke-static {v5, v6}, Lcom/twitter/library/util/Util;->a(Ljava/lang/String;F)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->l:Lcom/twitter/library/util/m;

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->e:Landroid/net/Uri;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->l:Lcom/twitter/library/util/m;

    invoke-virtual {v3, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/util/m;)Landroid/graphics/Bitmap;

    move-result-object v5

    if-eqz v5, :cond_5

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->h:Z

    invoke-virtual {p0, v5}, Lcom/twitter/android/BaseEditProfileActivity;->a(Landroid/graphics/Bitmap;)V

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0c00d4    # com.twitter.android.R.dimen.user_image_size

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget-object v2, v4, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/twitter/library/util/Util;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    new-instance v4, Lcom/twitter/library/util/m;

    invoke-direct {v4, v2, v0, v0}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;II)V

    iput-object v4, p0, Lcom/twitter/android/BaseEditProfileActivity;->m:Lcom/twitter/library/util/m;

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Landroid/net/Uri;

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->i:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->m:Lcom/twitter/library/util/m;

    invoke-virtual {v3, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/util/m;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/BaseEditProfileActivity;->o:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getId()I

    move-result v2

    invoke-virtual {p0, v0, v2}, Lcom/twitter/android/BaseEditProfileActivity;->a(Landroid/graphics/Bitmap;I)V

    :cond_3
    new-instance v0, Lcom/twitter/android/aw;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/twitter/android/aw;-><init>(Lcom/twitter/android/BaseEditProfileActivity;Lcom/twitter/android/av;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseEditProfileActivity;->a(Lcom/twitter/library/client/j;)V

    invoke-virtual {v3, v1, p0}, Lcom/twitter/android/client/c;->a(ILcom/twitter/library/util/ar;)V

    return-void

    :cond_4
    iget-wide v5, p0, Lcom/twitter/android/BaseEditProfileActivity;->j:J

    invoke-static {p0, v5, v6}, Lcom/twitter/library/util/Util;->b(Landroid/content/Context;J)Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->e:Landroid/net/Uri;

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->e:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseEditProfileActivity;->a(Landroid/net/Uri;)V

    iput-boolean v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->h:Z

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_1
.end method

.method public a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;)V
    .locals 3

    const/4 v0, 0x1

    iget v1, p1, Lcom/twitter/library/util/ao;->g:I

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->j:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/aa;->a(J)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/api/TwitterUser;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ae;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->o:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getId()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/BaseEditProfileActivity;->a(Landroid/graphics/Bitmap;I)V

    :cond_0
    return-void
.end method

.method protected d_()V
    .locals 2

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f080003    # com.twitter.android.R.array.change_photo_options

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->f(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/content/DialogInterface$OnCancelListener;)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    return-void
.end method

.method protected e()Z
    .locals 4

    const-string/jumbo v0, "android_edit_profile_nux_1267"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "edit_profile_composer"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected e_()V
    .locals 3

    const v2, 0x7f080002    # com.twitter.android.R.array.change_or_remove_photo_options

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->f(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->u:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->f(I)Lcom/twitter/android/widget/PromptDialogFragment;

    :goto_0
    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/content/DialogInterface$OnCancelListener;)Lcom/twitter/android/widget/PromptDialogFragment;

    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    return-void

    :cond_0
    const v1, 0x7f080003    # com.twitter.android.R.array.change_photo_options

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->f(I)Lcom/twitter/android/widget/PromptDialogFragment;

    goto :goto_0
.end method

.method protected abstract f()Ljava/lang/String;
.end method

.method protected f_()V
    .locals 2

    const/4 v0, 0x3

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0135    # com.twitter.android.R.string.edit_profile

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const/high16 v1, 0x7f0f0000    # com.twitter.android.R.string.abandon_changes_question

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0111    # com.twitter.android.R.string.discard

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0089    # com.twitter.android.R.string.cancel

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    return-void
.end method

.method protected abstract g()Ljava/lang/String;
.end method

.method g_()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->g:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->h:Z

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Landroid/net/Uri;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->e:Landroid/net/Uri;

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract h()Ljava/lang/String;
.end method

.method protected j()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->f_()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseEditProfileActivity;->setResult(I)V

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->j()V

    goto :goto_0
.end method

.method protected k()Z
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->g_()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected l()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->p:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->r:Ljava/lang/String;

    if-nez v1, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->r:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->r:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract m()Z
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, -0x1

    const/4 v2, 0x0

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/client/BaseFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-ne p2, v0, :cond_3

    if-ne p1, v4, :cond_1

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->f:Landroid/net/Uri;

    :goto_1
    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->b:Landroid/net/Uri;

    if-eqz v0, :cond_2

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/EditProfileCropActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "uri"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/BaseEditProfileActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_1
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    goto :goto_1

    :cond_2
    const v0, 0x7f0f0327    # com.twitter.android.R.string.profile_header_update_error

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    if-ne p1, v4, :cond_0

    iput-object v2, p0, Lcom/twitter/android/BaseEditProfileActivity;->b:Landroid/net/Uri;

    goto :goto_0

    :pswitch_1
    if-ne p2, v0, :cond_4

    if-eqz p3, :cond_4

    const-string/jumbo v0, "uri"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->e:Landroid/net/Uri;

    invoke-static {v1}, Lcom/twitter/library/util/n;->b(Landroid/net/Uri;)Z

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseEditProfileActivity;->a(Landroid/net/Uri;)V

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->e:Landroid/net/Uri;

    const-string/jumbo v0, "cropped_rect"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->k:Landroid/graphics/Rect;

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->b:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->b:Landroid/net/Uri;

    invoke-static {v0}, Lcom/twitter/library/util/n;->b(Landroid/net/Uri;)Z

    iput-object v2, p0, Lcom/twitter/android/BaseEditProfileActivity;->b:Landroid/net/Uri;

    goto :goto_0

    :cond_4
    iput-object v2, p0, Lcom/twitter/android/BaseEditProfileActivity;->b:Landroid/net/Uri;

    iput-object v2, p0, Lcom/twitter/android/BaseEditProfileActivity;->e:Landroid/net/Uri;

    goto :goto_0

    :pswitch_2
    if-ne p2, v0, :cond_9

    iput-object v2, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Landroid/net/Uri;

    const/4 v0, 0x4

    if-ne p1, v0, :cond_7

    iget-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->e()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-wide v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->j:J

    invoke-static {p0, v0, v1, v2}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;Landroid/net/Uri;J)Ljava/io/File;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/twitter/android/BaseEditProfileActivity;->a(Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Landroid/net/Uri;

    :cond_5
    :goto_2
    if-eqz v0, :cond_8

    new-instance v1, Lcom/twitter/android/ax;

    iget-object v2, p0, Lcom/twitter/android/BaseEditProfileActivity;->o:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getId()I

    move-result v2

    invoke-direct {v1, p0, v2}, Lcom/twitter/android/ax;-><init>(Lcom/twitter/android/BaseEditProfileActivity;I)V

    new-array v2, v4, [Landroid/net/Uri;

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/android/ax;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    :cond_6
    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Landroid/net/Uri;

    goto :goto_2

    :cond_7
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Landroid/net/Uri;

    iput-object v0, p0, Lcom/twitter/android/BaseEditProfileActivity;->d:Landroid/net/Uri;

    goto :goto_2

    :cond_8
    const v0, 0x7f0f0323    # com.twitter.android.R.string.profile_avatar_update_error

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_9
    iput-object v2, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Landroid/net/Uri;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->f_()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0

    return-void
.end method

.method public onClickHandler(Landroid/view/View;)V
    .locals 9

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f09007c    # com.twitter.android.R.id.header_container

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/android/BaseEditProfileActivity;->a:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v6, ""

    const-string/jumbo v7, "header_image"

    const-string/jumbo v8, "click"

    invoke-static {v5, v6, v7, v8}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->e_()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v1, 0x7f090142    # com.twitter.android.R.id.avatar_container

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->d_()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/BaseEditProfileActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Lcom/twitter/android/client/c;->b(ILcom/twitter/library/util/ar;)V

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onDestroy()V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "header_uri"

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->f:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v0, "pending_avatar_uri"

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->c:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v0, "cached_header_uri"

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->e:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v0, "pending_header_uri"

    iget-object v1, p0, Lcom/twitter/android/BaseEditProfileActivity;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method
