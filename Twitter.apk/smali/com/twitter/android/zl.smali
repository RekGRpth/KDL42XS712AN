.class final Lcom/twitter/android/zl;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/UserQueryActivity;


# direct methods
.method private constructor <init>(Lcom/twitter/android/UserQueryActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/zl;->a:Lcom/twitter/android/UserQueryActivity;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/UserQueryActivity;Lcom/twitter/android/zk;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/zl;-><init>(Lcom/twitter/android/UserQueryActivity;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;I[ILjava/lang/String;Lcom/twitter/library/api/TwitterUser;)V
    .locals 3

    const/4 v0, 0x0

    const v1, 0x7f0f0518    # com.twitter.android.R.string.user_info_fetch_error

    iget-object v2, p0, Lcom/twitter/android/zl;->a:Lcom/twitter/android/UserQueryActivity;

    invoke-static {v2, p2}, Lcom/twitter/android/UserQueryActivity;->a(Lcom/twitter/android/UserQueryActivity;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/zl;->a:Lcom/twitter/android/UserQueryActivity;

    iput-boolean v0, v2, Lcom/twitter/android/UserQueryActivity;->d:Z

    sparse-switch p3, :sswitch_data_0

    move v0, v1

    :goto_0
    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/zl;->a:Lcom/twitter/android/UserQueryActivity;

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/twitter/android/zl;->a:Lcom/twitter/android/UserQueryActivity;

    invoke-virtual {v0}, Lcom/twitter/android/UserQueryActivity;->finish()V

    :cond_0
    return-void

    :sswitch_0
    if-eqz p6, :cond_1

    iget-object v1, p0, Lcom/twitter/android/zl;->a:Lcom/twitter/android/UserQueryActivity;

    invoke-virtual {v1, p6}, Lcom/twitter/android/UserQueryActivity;->a(Lcom/twitter/library/api/TwitterUser;)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    :sswitch_1
    const v0, 0x7f0f0519    # com.twitter.android.R.string.user_not_found

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x3f

    invoke-static {p4, v0}, Lcom/twitter/library/util/Util;->a([II)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0f0493    # com.twitter.android.R.string.suspended_user

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0x193 -> :sswitch_2
        0x194 -> :sswitch_1
    .end sparse-switch
.end method
