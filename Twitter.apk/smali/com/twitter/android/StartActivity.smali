.class public Lcom/twitter/android/StartActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Lcom/twitter/android/client/c;

.field private b:Lcom/twitter/library/client/aa;

.field private c:Lcom/twitter/android/util/r;

.field private d:I

.field private e:Landroid/support/v4/view/ViewPager;

.field private f:Lcom/twitter/android/ts;

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Lcom/twitter/android/tv;

.field private k:Lcom/twitter/android/tu;

.field private l:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    new-instance v0, Lcom/twitter/android/tv;

    invoke-direct {v0, p0}, Lcom/twitter/android/tv;-><init>(Lcom/twitter/android/StartActivity;)V

    iput-object v0, p0, Lcom/twitter/android/StartActivity;->j:Lcom/twitter/android/tv;

    new-instance v0, Lcom/twitter/android/tu;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/tu;-><init>(Lcom/twitter/android/StartActivity;Lcom/twitter/android/tr;)V

    iput-object v0, p0, Lcom/twitter/android/StartActivity;->k:Lcom/twitter/android/tu;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/StartActivity;)Lcom/twitter/library/client/aa;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->b:Lcom/twitter/library/client/aa;

    return-object v0
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/StartActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public static a(Landroid/app/Activity;Landroid/content/Intent;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/StartActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "android.intent.extra.INTENT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/StartActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/twitter/android/StartActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    const/4 v5, 0x1

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/EditAccountActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "fullname"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "avatar_uri"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "username"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "email"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "password"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "default_password"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "follow flow"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "bogus entry so progress indicator shows four total steps"

    aput-object v4, v2, v3

    const-string/jumbo v3, "nux type edit account"

    aput-object v3, v2, v5

    const/4 v3, 0x2

    const-string/jumbo v4, "follow_friends"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "nux_tag_invite"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "follow_recommendations"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "follow flow progress"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "android.intent.extra.INTENT"

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/twitter/android/WelcomeActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/StartActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/StartActivity;->finish()V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/StartActivity;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->a:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method private c()V
    .locals 5

    const/4 v3, 0x0

    const v0, 0x7f09027d    # com.twitter.android.R.id.fullname

    invoke-virtual {p0, v0}, Lcom/twitter/android/StartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0900c7    # com.twitter.android.R.id.email

    invoke-virtual {p0, v1}, Lcom/twitter/android/StartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f09007a    # com.twitter.android.R.id.avatar_image

    invoke-virtual {p0, v2}, Lcom/twitter/android/StartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v4}, Lcom/twitter/android/util/r;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v0}, Lcom/twitter/android/util/r;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v0}, Lcom/twitter/android/util/r;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/android/StartActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    const/4 v1, 0x0

    :try_start_1
    invoke-static {v0, v1}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :goto_0
    invoke-static {v0}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    move-object v0, v3

    :goto_2
    invoke-static {v0}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    :goto_3
    invoke-static {v3}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :cond_0
    move-object v0, v3

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/android/StartActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/StartActivity;->i:Z

    return v0
.end method

.method static synthetic d(Lcom/twitter/android/StartActivity;)Lcom/twitter/android/util/r;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    return-object v0
.end method

.method private d()V
    .locals 10

    const/4 v0, 0x1

    const/4 v7, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/StartActivity;->showDialog(I)V

    iput-boolean v0, p0, Lcom/twitter/android/StartActivity;->i:Z

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v0}, Lcom/twitter/android/util/r;->q()Lcom/twitter/android/util/r;

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->b:Lcom/twitter/library/client/aa;

    iget-object v1, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v1}, Lcom/twitter/android/util/r;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v2}, Lcom/twitter/android/util/r;->f()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v3}, Lcom/twitter/android/util/r;->e()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v4}, Lcom/twitter/android/util/r;->h()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, ""

    const-string/jumbo v6, ""

    iget-object v9, p0, Lcom/twitter/android/StartActivity;->k:Lcom/twitter/android/tu;

    move v8, v7

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/library/client/aa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/twitter/library/client/ak;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/StartActivity;->l:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 3

    if-eqz p2, :cond_1

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "display_name"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v1, v0}, Lcom/twitter/android/util/r;->c(Ljava/lang/String;)Lcom/twitter/android/util/r;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    const-string/jumbo v1, "name"

    const-string/jumbo v2, "success"

    invoke-interface {v0, v1, v2}, Lcom/twitter/android/util/r;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-direct {p0}, Lcom/twitter/android/StartActivity;->c()V

    invoke-virtual {p0}, Lcom/twitter/android/StartActivity;->b()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    const-string/jumbo v1, "name"

    const-string/jumbo v2, "failure_empty"

    invoke-interface {v0, v1, v2}, Lcom/twitter/android/util/r;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v0}, Lcom/twitter/android/util/r;->n()Lcom/twitter/android/util/r;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    const-string/jumbo v1, "name"

    const-string/jumbo v2, "failure"

    invoke-interface {v0, v1, v2}, Lcom/twitter/android/util/r;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v0}, Lcom/twitter/android/util/r;->n()Lcom/twitter/android/util/r;

    goto :goto_0
.end method

.method public a()Z
    .locals 4

    const-string/jumbo v0, "android_logged_out_carousel_1720"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "enabled"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method b()V
    .locals 3

    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v1}, Lcom/twitter/android/util/r;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v2}, Lcom/twitter/android/util/r;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->d(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    return-void
.end method

.method public onAttachedToWindow()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onAttachedToWindow()V

    invoke-virtual {p0}, Lcom/twitter/android/StartActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setFormat(I)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    const/4 v3, 0x1

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f09027a    # com.twitter.android.R.id.sign_in

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->a:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/StartActivity;->b:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "signup:form:sign_in:button:click"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/LoginActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "android.intent.extra.INTENT"

    invoke-virtual {p0}, Lcom/twitter/android/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "android.intent.extra.INTENT"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/StartActivity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v1, 0x7f09027e    # com.twitter.android.R.id.signup_default

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->a:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/StartActivity;->b:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "signup:form:instant_card:button:click"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/twitter/android/StartActivity;->d()V

    goto :goto_0

    :cond_2
    const v1, 0x7f09027b    # com.twitter.android.R.id.sign_up

    if-eq v0, v1, :cond_3

    const v1, 0x7f090280    # com.twitter.android.R.id.signup_custom

    if-ne v0, v1, :cond_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/StartActivity;->a:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/StartActivity;->b:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "signup:form:sign_up:button:click"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/SignUpActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "android.intent.extra.INTENT"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string/jumbo v2, "android.intent.extra.INTENT"

    const-string/jumbo v3, "android.intent.extra.INTENT"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :goto_1
    invoke-virtual {p0, v1}, Lcom/twitter/android/StartActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_4
    const-string/jumbo v0, "android.intent.extra.INTENT"

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/twitter/android/WelcomeActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_1
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iget v1, p0, Lcom/twitter/android/StartActivity;->d:I

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/StartActivity;->h:Z

    if-eqz v0, :cond_0

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/twitter/android/StartActivity;->d:I

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->e:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/StartActivity;->e:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/twitter/android/StartActivity;->e:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    iget-object v1, p0, Lcom/twitter/android/StartActivity;->e:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 12

    const v11, 0x7f090278    # com.twitter.android.R.id.signed_out_second_line

    const v10, 0x7f09013a    # com.twitter.android.R.id.signed_out_first_line

    const v9, 0x7f030142    # com.twitter.android.R.layout.start

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Landroid/os/StatFs;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v0

    int-to-long v5, v0

    mul-long/2addr v3, v5

    const-wide/32 v5, 0x19000

    cmp-long v0, v3, v5

    if-lez v0, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v2, p0, Lcom/twitter/android/StartActivity;->i:Z

    iput-boolean v0, p0, Lcom/twitter/android/StartActivity;->g:Z

    if-eqz v0, :cond_a

    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/StartActivity;->a:Lcom/twitter/android/client/c;

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/android/StartActivity;->b:Lcom/twitter/library/client/aa;

    invoke-static {p0}, Lcom/twitter/android/util/q;->a(Landroid/content/Context;)Lcom/twitter/android/util/r;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-virtual {p0}, Lcom/twitter/android/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "scribe_event"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string/jumbo v4, "scribe_context"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string/jumbo v4, "scribe_event"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "scribe_context"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/StartActivity;->b:Lcom/twitter/library/client/aa;

    invoke-virtual {v6}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    aput-object v4, v8, v2

    aput-object v5, v8, v1

    invoke-virtual {v0, v6, v7, v8}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    const-string/jumbo v4, "scribe_event"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    const-string/jumbo v4, "scribe_context"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    :cond_0
    invoke-static {p0}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;)Lcom/twitter/android/util/d;

    move-result-object v3

    invoke-interface {v3}, Lcom/twitter/android/util/d;->e()V

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->d()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p0}, Lcom/twitter/android/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/android/util/AppMetrics;->a(Landroid/content/Context;Landroid/content/Intent;)V

    const-string/jumbo v0, "app:ready"

    invoke-static {p0, v0}, Lcom/twitter/android/util/AppMetrics;->a(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v0}, Lcom/twitter/android/util/r;->k()Lcom/twitter/android/util/r;

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v0}, Lcom/twitter/android/util/r;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v0}, Lcom/twitter/android/util/r;->c()V

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v0}, Lcom/twitter/android/util/r;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->a:Lcom/twitter/android/client/c;

    iget-object v3, p0, Lcom/twitter/android/StartActivity;->b:Lcom/twitter/library/client/aa;

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v5, "edit_account:form:::return_impression"

    aput-object v5, v1, v2

    invoke-virtual {v0, v3, v4, v1}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v0}, Lcom/twitter/android/util/r;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v0}, Lcom/twitter/android/util/r;->f()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v0}, Lcom/twitter/android/util/r;->e()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v0}, Lcom/twitter/android/util/r;->h()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v0}, Lcom/twitter/android/util/r;->g()Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v0}, Lcom/twitter/android/util/r;->i()Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/StartActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v2

    goto/16 :goto_0

    :cond_3
    invoke-static {p0}, Lcom/twitter/android/FollowFlowController;->f(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/twitter/android/MainActivity;->a(Landroid/app/Activity;Landroid/net/Uri;)V

    goto :goto_1

    :cond_4
    invoke-static {p0}, Lcom/twitter/library/util/Util;->a(Landroid/app/Activity;)V

    new-instance v3, Lgg;

    invoke-direct {v3, p0}, Lgg;-><init>(Landroid/content/Context;)V

    if-nez p1, :cond_6

    iget-object v4, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v4}, Lcom/twitter/android/util/r;->j()Lcom/twitter/android/util/r;

    move-result-object v4

    invoke-interface {v4}, Lcom/twitter/android/util/r;->l()Lcom/twitter/android/util/r;

    move-result-object v4

    invoke-interface {v4}, Lcom/twitter/android/util/r;->m()Lcom/twitter/android/util/r;

    move-result-object v4

    invoke-interface {v4}, Lcom/twitter/android/util/r;->o()Lcom/twitter/android/util/r;

    move-result-object v4

    invoke-interface {v4}, Lcom/twitter/android/util/r;->p()Lcom/twitter/android/util/r;

    :goto_2
    iget-object v4, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v4}, Lcom/twitter/android/util/r;->a()Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v4}, Lcom/twitter/android/util/r;->c()V

    :cond_5
    iget-object v4, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v4}, Lcom/twitter/android/util/r;->a()Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v4}, Lcom/twitter/android/util/r;->b()Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v3, p0, Lcom/twitter/android/StartActivity;->b:Lcom/twitter/library/client/aa;

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v5, "signup:form:instant_card::impression"

    aput-object v5, v1, v2

    invoke-virtual {v0, v3, v4, v1}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    const v0, 0x7f030144    # com.twitter.android.R.layout.start_instant_signup

    invoke-virtual {p0, v0}, Lcom/twitter/android/StartActivity;->setContentView(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_7

    invoke-virtual {p0}, Lcom/twitter/android/StartActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :goto_3
    const v0, 0x7f090080    # com.twitter.android.R.id.tos

    invoke-virtual {p0, v0}, Lcom/twitter/android/StartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    const v0, 0x7f09027e    # com.twitter.android.R.id.signup_default

    invoke-virtual {p0, v0}, Lcom/twitter/android/StartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090280    # com.twitter.android.R.id.signup_custom

    invoke-virtual {p0, v0}, Lcom/twitter/android/StartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_4
    const v0, 0x7f09027a    # com.twitter.android.R.id.sign_in

    invoke-virtual {p0, v0}, Lcom/twitter/android/StartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    :cond_6
    iget-object v4, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v4, p1}, Lcom/twitter/android/util/r;->a(Landroid/os/Bundle;)Lcom/twitter/android/util/r;

    const-string/jumbo v4, "reqId"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/StartActivity;->l:Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/StartActivity;->b:Lcom/twitter/library/client/aa;

    iget-object v5, p0, Lcom/twitter/android/StartActivity;->l:Ljava/lang/String;

    iget-object v6, p0, Lcom/twitter/android/StartActivity;->k:Lcom/twitter/android/tu;

    invoke-virtual {v4, v5, v6}, Lcom/twitter/library/client/aa;->a(Ljava/lang/String;Lcom/twitter/library/client/ah;)V

    goto/16 :goto_2

    :cond_7
    iget-object v0, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v0}, Lcom/twitter/android/util/r;->n()Lcom/twitter/android/util/r;

    invoke-direct {p0}, Lcom/twitter/android/StartActivity;->c()V

    invoke-virtual {p0}, Lcom/twitter/android/StartActivity;->b()V

    goto :goto_3

    :cond_8
    invoke-virtual {p0}, Lcom/twitter/android/StartActivity;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    const v0, 0x7f030143    # com.twitter.android.R.layout.start_carousel

    invoke-virtual {p0, v0}, Lcom/twitter/android/StartActivity;->setContentView(I)V

    const v0, 0x7f0900bb    # com.twitter.android.R.id.pager

    invoke-virtual {p0, v0}, Lcom/twitter/android/StartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/twitter/android/StartActivity;->e:Landroid/support/v4/view/ViewPager;

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->e:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v3}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    const v0, 0x7f09027c    # com.twitter.android.R.id.pager_pip

    invoke-virtual {p0, v0}, Lcom/twitter/android/StartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/PipView;

    iget-object v3, p0, Lcom/twitter/android/StartActivity;->e:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    new-instance v3, Lcom/twitter/android/ts;

    iget-object v4, p0, Lcom/twitter/android/StartActivity;->e:Landroid/support/v4/view/ViewPager;

    invoke-direct {v3, p0, v4, v0}, Lcom/twitter/android/ts;-><init>(Lcom/twitter/android/StartActivity;Landroid/support/v4/view/ViewPager;Lcom/twitter/android/widget/PipView;)V

    iput-object v3, p0, Lcom/twitter/android/StartActivity;->f:Lcom/twitter/android/ts;

    iget-object v3, p0, Lcom/twitter/android/StartActivity;->e:Landroid/support/v4/view/ViewPager;

    iget-object v4, p0, Lcom/twitter/android/StartActivity;->f:Lcom/twitter/android/ts;

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/PipView;->setPipOnPosition(I)V

    iput-boolean v1, p0, Lcom/twitter/android/StartActivity;->h:Z

    const v0, 0x7f09027b    # com.twitter.android.R.id.sign_up

    invoke-virtual {p0, v0}, Lcom/twitter/android/StartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_4

    :cond_9
    invoke-virtual {p0, v9}, Lcom/twitter/android/StartActivity;->setContentView(I)V

    invoke-virtual {p0, v10}, Lcom/twitter/android/StartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0f0435    # com.twitter.android.R.string.signed_out_first_line

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {p0, v11}, Lcom/twitter/android/StartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0f0436    # com.twitter.android.R.string.signed_out_second_line

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f09027b    # com.twitter.android.R.id.sign_up

    invoke-virtual {p0, v0}, Lcom/twitter/android/StartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_4

    :cond_a
    invoke-virtual {p0, v9}, Lcom/twitter/android/StartActivity;->setContentView(I)V

    const v0, 0x7f090279    # com.twitter.android.R.id.sign_in_bar

    invoke-virtual {p0, v0}, Lcom/twitter/android/StartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v10}, Lcom/twitter/android/StartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0f0435    # com.twitter.android.R.string.signed_out_first_line

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {p0, v11}, Lcom/twitter/android/StartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0f0436    # com.twitter.android.R.string.signed_out_second_line

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    const v1, 0x7f0f043c    # com.twitter.android.R.string.signup_creating

    invoke-virtual {p0, v1}, Lcom/twitter/android/StartActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    const/4 v4, 0x0

    new-instance v0, Landroid/support/v4/content/CursorLoader;

    sget-object v2, Landroid/provider/ContactsContract$Profile;->CONTENT_URI:Landroid/net/Uri;

    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v5, "display_name"

    aput-object v5, v3, v1

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onDestroy()V

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->b:Lcom/twitter/library/client/aa;

    iget-object v1, p0, Lcom/twitter/android/StartActivity;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->e(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/StartActivity;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0

    return-void
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onPause()V

    iget-boolean v0, p0, Lcom/twitter/android/StartActivity;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->f:Lcom/twitter/android/ts;

    invoke-static {v0}, Lcom/twitter/android/ts;->a(Lcom/twitter/android/ts;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/StartActivity;->a:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/StartActivity;->j:Lcom/twitter/android/tv;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/j;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/StartActivity;->removeDialog(I)V

    return-void
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v0}, Lcom/twitter/android/util/r;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v0}, Lcom/twitter/android/util/r;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iget-boolean v1, p0, Lcom/twitter/android/StartActivity;->g:Z

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    const-string/jumbo v0, "android_logged_out_carousel_1720"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/StartActivity;->h:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->f:Lcom/twitter/android/ts;

    const/16 v1, 0x1388

    invoke-static {v0, v1}, Lcom/twitter/android/ts;->a(Lcom/twitter/android/ts;I)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/StartActivity;->a:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/StartActivity;->j:Lcom/twitter/android/tv;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/j;)V

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/StartActivity;->c:Lcom/twitter/android/util/r;

    invoke-interface {v0, p1}, Lcom/twitter/android/util/r;->b(Landroid/os/Bundle;)Lcom/twitter/android/util/r;

    const-string/jumbo v0, "reqId"

    iget-object v1, p0, Lcom/twitter/android/StartActivity;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected onStart()V
    .locals 6

    const/4 v5, 0x0

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStart()V

    iget-boolean v0, p0, Lcom/twitter/android/StartActivity;->g:Z

    if-eqz v0, :cond_4

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/twitter/android/FollowFlowController;->f(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/twitter/android/FollowFlowController;->g(Landroid/app/Activity;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/twitter/android/MainActivity;->a(Landroid/app/Activity;Landroid/net/Uri;)V

    goto :goto_0

    :cond_1
    invoke-static {p0}, Lcom/twitter/android/SignedOutActivity;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "android_signed_out_1960"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    :cond_2
    invoke-static {p0}, Lcom/twitter/android/SignedOutActivity;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/SignedOutActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "type"

    const/16 v2, 0x1b

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "timeline_tag"

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->Z()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/StartActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/StartActivity;->finish()V

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/StartActivity;->a:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/StartActivity;->b:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "front::::impression"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    new-instance v0, Lcom/twitter/android/tr;

    invoke-direct {v0, p0}, Lcom/twitter/android/tr;-><init>(Lcom/twitter/android/StartActivity;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0f015c    # com.twitter.android.R.string.error_low_internal_storage

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f006d    # com.twitter.android.R.string.button_exit

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f03c6    # com.twitter.android.R.string.settings

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method
