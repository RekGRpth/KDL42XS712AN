.class public Lcom/twitter/android/DiscoverFragment;
.super Lcom/twitter/android/TweetListFragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/ty;
.implements Lcom/twitter/library/widget/a;


# instance fields
.field a:Lcom/twitter/library/util/FriendshipCache;

.field b:I

.field c:I

.field final d:Ljava/util/ArrayList;

.field final e:Ljava/util/ArrayList;

.field final f:Ljava/util/HashMap;

.field private final g:Ljava/util/HashMap;

.field private final h:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private i:Lcom/twitter/android/ex;

.field private j:J

.field private k:I

.field private l:Z

.field private m:Lcom/twitter/android/fd;

.field private n:I

.field private o:I

.field private p:Z

.field private q:Z

.field private r:Lcom/twitter/library/scribe/ScribeLog;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/TweetListFragment;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/DiscoverFragment;->d:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/DiscoverFragment;->e:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/DiscoverFragment;->f:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/DiscoverFragment;->g:Ljava/util/HashMap;

    new-instance v0, Lcom/twitter/android/eu;

    invoke-direct {v0, p0}, Lcom/twitter/android/eu;-><init>(Lcom/twitter/android/DiscoverFragment;)V

    iput-object v0, p0, Lcom/twitter/android/DiscoverFragment;->h:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/DiscoverFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/DiscoverFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/DiscoverFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method private a(IJ)V
    .locals 2

    const/4 v0, 0x7

    if-ne p1, v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/ev;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/android/ev;->c(J)V

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetChanged()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcom/twitter/android/DiscoverFragment;->a_(I)V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Ljt;

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    const-string/jumbo v3, "TREND"

    invoke-direct {v1, p1, v2, v3}, Ljt;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->c(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    const/4 v0, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, Lcom/twitter/android/DiscoverFragment;->a(Lcom/twitter/library/service/b;II)Z

    return-void
.end method

.method private a(Lcom/twitter/internal/android/widget/GroupedRowView;Landroid/database/Cursor;I)V
    .locals 12

    invoke-virtual {p1}, Lcom/twitter/internal/android/widget/GroupedRowView;->getTag()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/twitter/android/fc;

    sget v0, Lcom/twitter/library/provider/cc;->F:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v0, Lcom/twitter/library/provider/cc;->E:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v0, Lcom/twitter/library/provider/cc;->C:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v0, Lcom/twitter/library/provider/cc;->D:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    sget v0, Lcom/twitter/library/provider/cc;->N:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget v0, Lcom/twitter/library/provider/cc;->K:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    sget v0, Lcom/twitter/library/provider/cc;->L:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v9, v3}, Lcom/twitter/android/fc;->a(Ljava/lang/String;)V

    invoke-virtual {v9, v10, v0}, Lcom/twitter/android/fc;->a(Lcom/twitter/library/api/PromotedContent;Landroid/content/Context;)V

    invoke-virtual {v9, v11, v2, v0}, Lcom/twitter/android/fc;->a(IILandroid/content/Context;)V

    invoke-virtual {v9, v2}, Lcom/twitter/android/fc;->a(I)V

    iput-object v4, v9, Lcom/twitter/android/fc;->h:Ljava/lang/String;

    if-eqz v10, :cond_1

    const/4 v7, 0x1

    :goto_0
    const-string/jumbo v5, "trend"

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/twitter/android/vf;->a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/twitter/android/widget/TopicView$TopicData;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, v9, Lcom/twitter/android/fc;->g:Landroid/content/Intent;

    new-instance v0, Lcom/twitter/library/scribe/ScribeItem;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeItem;-><init>()V

    iput p3, v0, Lcom/twitter/library/scribe/ScribeItem;->h:I

    invoke-static {v2}, Lcom/twitter/library/api/TwitterTopic;->d(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/scribe/ScribeItem;->x:Ljava/lang/String;

    iput-object v3, v0, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    iput v11, v0, Lcom/twitter/library/scribe/ScribeItem;->y:I

    if-eqz v7, :cond_0

    iget-wide v1, v10, Lcom/twitter/library/api/PromotedContent;->promotedTrendId:J

    iput-wide v1, v9, Lcom/twitter/android/fc;->f:J

    iget-wide v1, v10, Lcom/twitter/library/api/PromotedContent;->promotedTrendId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/scribe/ScribeItem;->e:Ljava/lang/String;

    :cond_0
    iput-object v0, v9, Lcom/twitter/android/fc;->i:Lcom/twitter/library/scribe/ScribeItem;

    iget-object v1, p0, Lcom/twitter/android/DiscoverFragment;->r:Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {v1, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    return-void

    :cond_1
    const/4 v7, 0x0

    goto :goto_0
.end method

.method private a(IJJ)Z
    .locals 9

    const/4 v2, 0x2

    const/4 v1, 0x0

    const/4 v8, 0x1

    invoke-virtual {p0, p1}, Lcom/twitter/android/DiscoverFragment;->c_(I)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->l()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "discover"

    const/4 v5, 0x7

    invoke-static {v3, v4, p1, v5}, Lcom/twitter/android/DiscoverFragment;->a(Ljava/lang/String;Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v7

    sparse-switch p1, :sswitch_data_0

    invoke-virtual {p0, v8}, Lcom/twitter/android/DiscoverFragment;->c(I)V

    move v2, v1

    :goto_1
    :sswitch_0
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/DiscoverFragment;->a(IJ)V

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    move-wide v3, p2

    move-wide v5, p4

    invoke-virtual/range {v0 .. v7}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;IJJLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/twitter/android/DiscoverFragment;->a(Ljava/lang/String;I)V

    move v1, v8

    goto :goto_0

    :sswitch_1
    move v2, v8

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x7 -> :sswitch_0
    .end sparse-switch
.end method

.method private a(Landroid/content/Context;I)Z
    .locals 10

    const-wide/32 v8, 0x493e0

    const-wide/16 v6, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Lcom/twitter/library/client/f;

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "discover_prefs"

    invoke-direct {v2, p1, v3, v4}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    packed-switch p2, :pswitch_data_0

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :pswitch_0
    const-string/jumbo v5, "last_refresh"

    invoke-virtual {v2, v5, v6, v7}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;J)J

    move-result-wide v5

    add-long/2addr v5, v8

    cmp-long v2, v3, v5

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0

    :pswitch_1
    const-string/jumbo v5, "last_refresh_trends"

    invoke-virtual {v2, v5, v6, v7}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;J)J

    move-result-wide v5

    add-long/2addr v5, v8

    cmp-long v2, v3, v5

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic b(Lcom/twitter/android/DiscoverFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/DiscoverFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/DiscoverFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method private b(J)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v1, p1, p2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->l()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "discover:stream::results"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/DiscoverFragment;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/util/ArrayList;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method static b(Landroid/database/Cursor;)Z
    .locals 2

    const/4 v0, 0x1

    sget v1, Lcom/twitter/library/provider/bi;->f:I

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/android/DiscoverFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method private c(J)V
    .locals 4

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->au()Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/twitter/library/client/aa;->a(J)Lcom/twitter/library/client/Session;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/DiscoverFragment;->d:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v2}, Lcom/twitter/library/util/Util;->b(Ljava/util/Collection;)[J

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;[J)Ljava/lang/String;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/android/DiscoverFragment;Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/twitter/android/DiscoverFragment;->d(Ljava/lang/String;)V

    return-void
.end method

.method static c(Landroid/database/Cursor;)Z
    .locals 2

    sget v0, Lcom/twitter/library/provider/bi;->f:I

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/twitter/android/DiscoverFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/DiscoverFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/DiscoverFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/DiscoverFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/DiscoverFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/DiscoverFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private e(Landroid/database/Cursor;)V
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->m:Lcom/twitter/android/fd;

    iget-object v0, v0, Lcom/twitter/android/fd;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v1, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    iput-object v1, p0, Lcom/twitter/android/DiscoverFragment;->r:Lcom/twitter/library/scribe/ScribeLog;

    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->size()I

    move-result v0

    if-lt v1, v0, :cond_1

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    invoke-direct {p0, v0, p1, v1}, Lcom/twitter/android/DiscoverFragment;->a(Lcom/twitter/internal/android/widget/GroupedRowView;Landroid/database/Cursor;I)V

    add-int/lit8 v0, v1, 0x1

    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/DiscoverFragment;->m:Lcom/twitter/android/fd;

    invoke-virtual {v1, v2}, Lcom/twitter/android/fd;->a(I)V

    :goto_1
    iput v0, p0, Lcom/twitter/android/DiscoverFragment;->o:I

    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->m:Lcom/twitter/android/fd;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/fd;->a(I)V

    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method static synthetic f(Lcom/twitter/android/DiscoverFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/DiscoverFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/DiscoverFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/DiscoverFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/DiscoverFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/android/DiscoverFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method static synthetic l(Lcom/twitter/android/DiscoverFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method static synthetic m(Lcom/twitter/android/DiscoverFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic n(Lcom/twitter/android/DiscoverFragment;)Lcom/twitter/library/scribe/ScribeAssociation;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    return-object v0
.end method

.method static synthetic o(Lcom/twitter/android/DiscoverFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method private r()V
    .locals 3

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0, v2}, Lcom/twitter/android/DiscoverFragment;->e(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0, v2}, Lcom/twitter/android/DiscoverFragment;->a(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/twitter/android/DiscoverFragment;->a(Landroid/content/Context;)V

    invoke-virtual {p0, v2}, Lcom/twitter/android/DiscoverFragment;->c(I)V

    :cond_0
    return-void
.end method

.method private s()Z
    .locals 6

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->V()Landroid/widget/ListView;

    move-result-object v2

    iget v1, p0, Lcom/twitter/android/DiscoverFragment;->o:I

    if-lez v1, :cond_0

    invoke-virtual {v2}, Landroid/widget/ListView;->getChildCount()I

    move-result v1

    add-int/lit8 v3, v1, -0x1

    move v1, v0

    :goto_0
    if-gt v1, v3, :cond_0

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v5, p0, Lcom/twitter/android/DiscoverFragment;->m:Lcom/twitter/android/fd;

    invoke-virtual {v5, v4}, Lcom/twitter/android/fd;->a(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private u()V
    .locals 7

    const/4 v6, 0x0

    iget-boolean v0, p0, Lcom/twitter/android/DiscoverFragment;->p:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/DiscoverFragment;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->l()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x1

    const-string/jumbo v5, "discover:trend_module::impression"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iput-boolean v6, p0, Lcom/twitter/android/DiscoverFragment;->p:Z

    :cond_0
    return-void
.end method

.method private v()V
    .locals 5

    const/4 v4, 0x0

    iget-boolean v0, p0, Lcom/twitter/android/DiscoverFragment;->q:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->r:Lcom/twitter/library/scribe/ScribeLog;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/DiscoverFragment;->r:Lcom/twitter/library/scribe/ScribeLog;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "discover:discover:trend_module:trend:results"

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    iput-boolean v4, p0, Lcom/twitter/android/DiscoverFragment;->q:Z

    :cond_0
    return-void
.end method


# virtual methods
.method protected a(JJ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/DiscoverFragment;->b(J)V

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/DiscoverFragment;->c(J)V

    return-void
.end method

.method a(JJJI)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->g:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/DiscoverFragment;->g:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v2, Landroid/util/Pair;

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public a(Landroid/content/Context;IILcom/twitter/library/service/b;)V
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/android/TweetListFragment;->a(Landroid/content/Context;IILcom/twitter/library/service/b;)V

    invoke-virtual {p4}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    if-ne p2, v2, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p4}, Lcom/twitter/library/service/b;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/service/p;->e:Ljava/lang/String;

    const-string/jumbo v1, "last_refresh_trends"

    invoke-virtual {p0, p1, v0, v1}, Lcom/twitter/android/DiscoverFragment;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_0
    return-void
.end method

.method a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    new-instance v0, Lcom/twitter/library/client/f;

    const-string/jumbo v1, "discover_prefs"

    invoke-direct {v0, p1, p2, v1}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, p3, v1, v2}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;J)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V

    return-void
.end method

.method protected a(Landroid/database/Cursor;)V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xe

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/16 v1, 0x190

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/DiscoverFragment;->a(I)Z

    :cond_0
    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v0, 0x0

    const/4 v4, 0x2

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-super {p0, p1, p2}, Lcom/twitter/android/TweetListFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    const/4 v2, 0x5

    invoke-virtual {p0, v2}, Lcom/twitter/android/DiscoverFragment;->b(I)V

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    invoke-virtual {p0, v4}, Lcom/twitter/android/DiscoverFragment;->e(I)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v2, v1, v5, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    iget v2, p0, Lcom/twitter/android/DiscoverFragment;->c:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/twitter/android/DiscoverFragment;->c:I

    :cond_1
    iget-object v2, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v2}, Landroid/support/v4/widget/CursorAdapter;->isEmpty()Z

    move-result v2

    invoke-virtual {p0, v1}, Lcom/twitter/android/DiscoverFragment;->d(I)Z

    move-result v3

    if-nez v3, :cond_2

    if-eqz v2, :cond_3

    const/4 v3, 0x3

    invoke-virtual {p0, v3}, Lcom/twitter/android/DiscoverFragment;->a(I)Z

    :cond_2
    :goto_1
    iget-boolean v3, p0, Lcom/twitter/android/DiscoverFragment;->l:Z

    if-eqz v3, :cond_0

    if-nez v2, :cond_0

    new-instance v2, Lcom/twitter/refresh/widget/a;

    iget-wide v3, p0, Lcom/twitter/android/DiscoverFragment;->j:J

    iget v5, p0, Lcom/twitter/android/DiscoverFragment;->k:I

    invoke-direct {v2, v0, v3, v4, v5}, Lcom/twitter/refresh/widget/a;-><init>(IJI)V

    invoke-virtual {p0, v2, v1}, Lcom/twitter/android/DiscoverFragment;->a(Lcom/twitter/refresh/widget/a;Z)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v0}, Lcom/twitter/android/DiscoverFragment;->i(Z)V

    goto :goto_1

    :pswitch_1
    if-eqz p2, :cond_4

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-nez v3, :cond_5

    :cond_4
    move v0, v1

    :cond_5
    if-nez v0, :cond_7

    invoke-direct {p0, p2}, Lcom/twitter/android/DiscoverFragment;->e(Landroid/database/Cursor;)V

    :goto_2
    invoke-virtual {p0, v4}, Lcom/twitter/android/DiscoverFragment;->d(I)Z

    move-result v1

    if-nez v1, :cond_0

    if-nez v0, :cond_6

    invoke-direct {p0, v2, v4}, Lcom/twitter/android/DiscoverFragment;->a(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_6
    invoke-direct {p0, v2}, Lcom/twitter/android/DiscoverFragment;->a(Landroid/content/Context;)V

    invoke-virtual {p0, v4}, Lcom/twitter/android/DiscoverFragment;->c(I)V

    goto :goto_0

    :cond_7
    invoke-direct {p0, v5}, Lcom/twitter/android/DiscoverFragment;->e(Landroid/database/Cursor;)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 11

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->m:Lcom/twitter/android/fd;

    iget-object v0, v0, Lcom/twitter/android/fd;->a:Lcom/twitter/internal/android/widget/GroupedRowView;

    if-ne p2, v0, :cond_1

    const/4 v0, 0x1

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/twitter/android/TrendsActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "type"

    const/16 v3, 0x10

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "timeline_tag"

    const-string/jumbo v3, "TREND"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "empty_title"

    const v3, 0x7f0f0153    # com.twitter.android.R.string.empty_timeline

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "empty_desc"

    const v3, 0x7f0f0154    # com.twitter.android.R.string.empty_timeline_desc

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/DiscoverFragment;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v2, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string/jumbo v7, "discover:discover:trend_module:header:click"

    aput-object v7, v3, v6

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :goto_0
    if-nez v0, :cond_0

    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/twitter/android/DiscoverFragment;->b(Landroid/database/Cursor;)Z

    move-result v1

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v2

    if-eqz v1, :cond_7

    const/16 v3, 0x12

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/4 v6, 0x2

    if-ne v3, v6, :cond_7

    const/4 v1, 0x7

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const/16 v4, 0x15

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/DiscoverFragment;->a(IJJ)Z

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->m:Lcom/twitter/android/fd;

    iget-object v0, v0, Lcom/twitter/android/fd;->c:Lcom/twitter/internal/android/widget/GroupedRowView;

    if-ne p2, v0, :cond_2

    const/4 v0, 0x1

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/twitter/android/ActivitiesActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "activity_type"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/DiscoverFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->m:Lcom/twitter/android/fd;

    iget-object v0, v0, Lcom/twitter/android/fd;->d:Lcom/twitter/internal/android/widget/GroupedRowView;

    if-ne p2, v0, :cond_4

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->q()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->r()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->t()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/featureswitch/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v6

    new-instance v7, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v7, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string/jumbo v10, "discover:discover:trend_module:olympics:click"

    aput-object v10, v8, v9

    invoke-virtual {v7, v8}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :goto_2
    new-instance v6, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    const-class v8, Lcom/twitter/android/SearchTerminalActivity;

    invoke-direct {v6, v7, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v7, "query"

    invoke-virtual {v6, v7, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v6, "terminal"

    const/4 v7, 0x1

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v6, "search_button"

    const/4 v7, 0x1

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v6, "event_id"

    invoke-virtual {v1, v6, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "query_name"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/DiscoverFragment;->startActivity(Landroid/content/Intent;)V

    move v0, v3

    goto/16 :goto_0

    :cond_3
    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->m:Lcom/twitter/android/fd;

    iget-object v0, v0, Lcom/twitter/android/fd;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v0, p2}, Ljava/util/LinkedHashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/fc;

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    iget-boolean v2, v0, Lcom/twitter/android/fc;->e:Z

    if-eqz v2, :cond_5

    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->h:Lcom/twitter/library/api/PromotedEvent;

    iget-wide v6, v0, Lcom/twitter/android/fc;->f:J

    invoke-virtual {v1, v2, v6, v7}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;J)V

    :cond_5
    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->l()Ljava/lang/String;

    move-result-object v2

    new-instance v6, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v6, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v2, v7, v8

    const/4 v8, 0x1

    const-string/jumbo v9, "discover:trend_module:trend:click"

    aput-object v9, v7, v8

    invoke-virtual {v6, v7}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v6

    iget-object v7, v0, Lcom/twitter/android/fc;->i:Lcom/twitter/library/scribe/ScribeItem;

    invoke-virtual {v6, v7}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    new-instance v6, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v6, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v2, v7, v8

    const/4 v2, 0x1

    const-string/jumbo v8, "discover:trend_module:trend:search"

    aput-object v8, v7, v2

    invoke-virtual {v6, v7}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    iget-object v6, v0, Lcom/twitter/android/fc;->h:Ljava/lang/String;

    invoke-virtual {v2, v6}, Lcom/twitter/library/scribe/ScribeLog;->f(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    iget-object v0, v0, Lcom/twitter/android/fc;->g:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/DiscoverFragment;->startActivity(Landroid/content/Intent;)V

    :cond_6
    move v0, v3

    goto/16 :goto_0

    :cond_7
    if-eqz v1, :cond_9

    new-instance v3, Lcom/twitter/library/provider/Tweet;

    invoke-direct {v3, v0}, Lcom/twitter/library/provider/Tweet;-><init>(Landroid/database/Cursor;)V

    sget v1, Lcom/twitter/library/provider/bi;->v:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/DiscoverStoryMetadata;

    new-instance v6, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->ab()Z

    move-result v1

    if-eqz v1, :cond_8

    const-class v1, Lcom/twitter/android/RootTweetActivity;

    :goto_3
    invoke-direct {v6, v7, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "tw"

    invoke-virtual {v6, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v6, "association"

    iget-object v7, p0, Lcom/twitter/android/DiscoverFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/DiscoverFragment;->startActivity(Landroid/content/Intent;)V

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v1, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->l()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string/jumbo v6, "discover:tweet::click"

    aput-object v6, v4, v5

    invoke-virtual {v1, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/android/DiscoverFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    const/4 v6, 0x0

    invoke-virtual {v1, v4, v3, v5, v6}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v3, p0, Lcom/twitter/android/DiscoverFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v3}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-static {v0}, Lcom/twitter/library/scribe/ScribeItem;->a(Lcom/twitter/library/api/DiscoverStoryMetadata;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto/16 :goto_1

    :cond_8
    const-class v1, Lcom/twitter/android/TweetActivity;

    goto :goto_3

    :cond_9
    invoke-static {v0}, Lcom/twitter/android/DiscoverFragment;->c(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_d

    sget v1, Lcom/twitter/library/provider/bi;->i:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->ab()Z

    move-result v0

    if-eqz v0, :cond_c

    const-class v0, Lcom/twitter/android/RootProfileActivity;

    :goto_4
    invoke-direct {v1, v3, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v0, "user_id"

    invoke-virtual {v1, v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "type"

    const/16 v3, 0x9

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    new-instance v3, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v3}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    const/4 v8, 0x5

    invoke-virtual {v3, v8}, Lcom/twitter/library/scribe/ScribeAssociation;->a(I)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v3

    iget-wide v8, p0, Lcom/twitter/android/DiscoverFragment;->Q:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Lcom/twitter/library/scribe/ScribeAssociation;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v3

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->l()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v3

    const-string/jumbo v8, "who_to_follow"

    invoke-virtual {v3, v8}, Lcom/twitter/library/scribe/ScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->a:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0, v6, v7}, Lcom/twitter/library/util/FriendshipCache;->h(J)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_a

    const-string/jumbo v3, "friendship"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_a
    check-cast p2, Lcom/twitter/internal/android/widget/GroupedRowView;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/UserView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/UserView;->getPromotedContent()Lcom/twitter/library/api/PromotedContent;

    move-result-object v3

    if-eqz v3, :cond_b

    sget-object v8, Lcom/twitter/library/api/PromotedEvent;->d:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {v2, v8, v3}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V

    const-string/jumbo v8, "pc"

    invoke-virtual {v1, v8, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_b
    const/4 v8, 0x1

    invoke-virtual {p0, v1, v8}, Lcom/twitter/android/DiscoverFragment;->startActivityForResult(Landroid/content/Intent;I)V

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v1, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->l()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x1

    const-string/jumbo v8, "discover:user_module:user:profile_click"

    aput-object v8, v4, v5

    invoke-virtual {v1, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0}, Lcom/twitter/library/widget/UserView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/zx;

    iget-object v0, v0, Lcom/twitter/android/zx;->f:Ljava/lang/String;

    invoke-virtual {v1, v6, v7, v3, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/DiscoverFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto/16 :goto_1

    :cond_c
    const-class v0, Lcom/twitter/android/ProfileActivity;

    goto/16 :goto_4

    :cond_d
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->ab()Z

    move-result v0

    if-eqz v0, :cond_e

    const-class v0, Lcom/twitter/android/RootTabbedFindPeopleActivity;

    :goto_5
    invoke-direct {v1, v3, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v0, 0x2

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/DiscoverFragment;->startActivityForResult(Landroid/content/Intent;I)V

    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v0, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->l()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "discover:user_module:more:click"

    aput-object v4, v1, v3

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/DiscoverFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto/16 :goto_1

    :cond_e
    const-class v0, Lcom/twitter/android/TabbedFindPeopleActivity;

    goto :goto_5
.end method

.method protected a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;Z)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/ev;

    if-eqz v0, :cond_0

    if-nez p3, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, p1, p2, v1}, Lcom/twitter/android/ev;->a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/refresh/widget/a;)V
    .locals 4

    iget-boolean v0, p0, Lcom/twitter/android/DiscoverFragment;->l:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/twitter/library/client/f;

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "discover_prefs"

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v1, "item"

    iget-wide v2, p1, Lcom/twitter/refresh/widget/a;->b:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;J)Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v1, "off"

    iget v2, p1, Lcom/twitter/refresh/widget/a;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;I)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V

    :cond_0
    return-void
.end method

.method protected a(Lcom/twitter/refresh/widget/a;Z)V
    .locals 4

    iget-boolean v0, p0, Lcom/twitter/android/DiscoverFragment;->l:Z

    if-eqz v0, :cond_1

    iget-wide v0, p1, Lcom/twitter/refresh/widget/a;->b:J

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/DiscoverFragment;->a(J)I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->V()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    if-nez p2, :cond_1

    :cond_0
    iget v2, p1, Lcom/twitter/refresh/widget/a;->c:I

    invoke-virtual {v1, v0, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    :cond_1
    return-void
.end method

.method protected a(Z)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x5

    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->a(Z)V

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->q()V

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    if-eqz p1, :cond_0

    invoke-virtual {p0, v1}, Lcom/twitter/android/DiscoverFragment;->a_(I)V

    iget-object v1, p0, Lcom/twitter/android/DiscoverFragment;->a:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v1}, Lcom/twitter/library/util/FriendshipCache;->b()V

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->P()Z

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    invoke-direct {p0, v2}, Lcom/twitter/android/DiscoverFragment;->e(Landroid/database/Cursor;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p0, v1}, Lcom/twitter/android/DiscoverFragment;->a_(I)V

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->p()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/DiscoverFragment;->a(I)Z

    invoke-direct {p0}, Lcom/twitter/android/DiscoverFragment;->r()V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/DiscoverFragment;->i(Z)V

    goto :goto_0
.end method

.method protected a(I)Z
    .locals 6

    const-wide/16 v2, 0x0

    move-object v0, p0

    move v1, p1

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/DiscoverFragment;->a(IJJ)Z

    move-result v0

    return v0
.end method

.method a(JJJ)Z
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->f:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/DiscoverFragment;->f:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    :goto_0
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v0, v2, p5

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_1
    return v0

    :cond_0
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method public a(Landroid/widget/AbsListView;III)Z
    .locals 1

    if-nez p2, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/DiscoverFragment;->u()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/DiscoverFragment;->q:Z

    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/android/TweetListFragment;->a(Landroid/widget/AbsListView;III)Z

    move-result v0

    return v0
.end method

.method protected a_()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->a_()V

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/ev;

    invoke-virtual {v0}, Lcom/twitter/android/ev;->a()V

    invoke-virtual {v0}, Lcom/twitter/android/ev;->d()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->m:Lcom/twitter/android/fd;

    invoke-virtual {v0}, Lcom/twitter/android/fd;->a()V

    return-void
.end method

.method b(JJ)Landroid/util/Pair;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/ev;

    invoke-virtual {v0, p3, p4}, Lcom/twitter/android/ev;->b(J)Ljava/lang/Long;

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->g:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    if-eqz v0, :cond_0

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->b()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/DiscoverFragment;->a(Z)V

    return-void
.end method

.method protected b(I)V
    .locals 1

    const/4 v0, 0x7

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/ev;

    invoke-virtual {v0}, Lcom/twitter/android/ev;->g()V

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetChanged()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->b(I)V

    goto :goto_0
.end method

.method protected c()V
    .locals 8

    const/4 v7, 0x1

    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->c()V

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/ev;

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/twitter/android/client/c;->ai()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/twitter/android/ev;->b(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v0, v7, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->l()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "::::impression"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v4

    invoke-virtual {v1, v2, v3, v0}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iput-boolean v7, p0, Lcom/twitter/android/DiscoverFragment;->p:Z

    invoke-direct {p0}, Lcom/twitter/android/DiscoverFragment;->u()V

    return-void
.end method

.method c(I)V
    .locals 1

    iget v0, p0, Lcom/twitter/android/DiscoverFragment;->b:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/twitter/android/DiscoverFragment;->b:I

    return-void
.end method

.method protected d()V
    .locals 12

    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->d()V

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    iget-object v4, p0, Lcom/twitter/android/DiscoverFragment;->g:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v10, v4

    check-cast v10, Ljava/util/HashMap;

    if-eqz v10, :cond_1

    invoke-virtual {v10}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v9, v4

    check-cast v9, Ljava/util/Map$Entry;

    const/16 v4, 0x9

    const-wide/16 v5, -0x1

    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/util/Pair;

    iget-object v9, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;JIJJLjava/lang/Integer;)Ljava/lang/String;

    goto :goto_0

    :cond_0
    invoke-virtual {v10}, Ljava/util/HashMap;->clear()V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/ev;

    invoke-virtual {v0}, Lcom/twitter/android/ev;->b()V

    invoke-direct {p0, v2, v3}, Lcom/twitter/android/DiscoverFragment;->b(J)V

    invoke-direct {p0}, Lcom/twitter/android/DiscoverFragment;->v()V

    invoke-direct {p0, v2, v3}, Lcom/twitter/android/DiscoverFragment;->c(J)V

    return-void
.end method

.method d(I)Z
    .locals 1

    iget v0, p0, Lcom/twitter/android/DiscoverFragment;->b:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method e(I)Z
    .locals 1

    iget v0, p0, Lcom/twitter/android/DiscoverFragment;->c:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected i()V
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/android/DiscoverFragment;->a(I)Z

    return-void
.end method

.method protected j()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected k()V
    .locals 5

    const/4 v4, 0x0

    iget-boolean v0, p0, Lcom/twitter/android/DiscoverFragment;->l:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/twitter/library/client/f;

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "discover_prefs"

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "item"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/twitter/android/DiscoverFragment;->j:J

    const-string/jumbo v1, "off"

    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/DiscoverFragment;->k:I

    new-instance v0, Lcom/twitter/refresh/widget/a;

    iget-wide v1, p0, Lcom/twitter/android/DiscoverFragment;->j:J

    iget v3, p0, Lcom/twitter/android/DiscoverFragment;->k:I

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/twitter/refresh/widget/a;-><init>(IJI)V

    invoke-virtual {p0, v0, v4}, Lcom/twitter/android/DiscoverFragment;->a(Lcom/twitter/refresh/widget/a;Z)V

    :cond_0
    return-void
.end method

.method protected l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "discover"

    return-object v0
.end method

.method protected n()V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/DiscoverFragment;->a(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/twitter/android/DiscoverFragment;->a(I)Z

    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/DiscoverFragment;->r()V

    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 14

    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v11

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v6

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->l()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeAssociation;->a(I)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    const-string/jumbo v2, "discover"

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    const-string/jumbo v2, "tweet"

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeAssociation;->d(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v2

    new-instance v12, Lcom/twitter/android/ev;

    const/4 v8, 0x2

    iget-object v13, v6, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    new-instance v0, Lcom/twitter/android/fa;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ":discover:tweet:avatar:profile_click"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ":discover:tweet::open_link"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v5, ":discover:tweet:photo:click"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/fa;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v7, p0, Lcom/twitter/android/DiscoverFragment;->C:Lcom/twitter/android/vs;

    iget-object v9, p0, Lcom/twitter/android/DiscoverFragment;->a:Lcom/twitter/library/util/FriendshipCache;

    iget-object v10, p0, Lcom/twitter/android/DiscoverFragment;->i:Lcom/twitter/android/ex;

    move-object v1, v12

    move-object v2, v11

    move v3, v8

    move-object v4, v13

    move-object v5, v6

    move-object v6, v0

    move-object v8, p0

    invoke-direct/range {v1 .. v10}, Lcom/twitter/android/ev;-><init>(Landroid/app/Activity;ILcom/twitter/library/widget/ap;Lcom/twitter/android/client/c;Lcom/twitter/library/widget/aa;Lcom/twitter/android/vs;Lcom/twitter/library/widget/a;Lcom/twitter/library/util/FriendshipCache;Lcom/twitter/android/ex;)V

    if-eqz p1, :cond_1

    const-string/jumbo v0, "spinning_gap_ids"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v1

    if-eqz v1, :cond_1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-wide v3, v1, v0

    invoke-virtual {v12, v3, v4}, Lcom/twitter/android/ev;->c(J)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v12}, Lcom/twitter/android/ev;->notifyDataSetChanged()V

    :cond_1
    invoke-virtual {p0, v12}, Lcom/twitter/android/DiscoverFragment;->a(Lcom/twitter/android/client/av;)Lcom/twitter/android/client/BaseListFragment;

    iput-object v12, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    instance-of v0, v0, Lcom/twitter/android/ev;

    if-eqz v0, :cond_3

    const-string/jumbo v0, "android_discover_inline_counts_1876"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "inline_counts"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/ev;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/ev;->i(Z)V

    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->V()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "legacy"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0068    # com.twitter.android.R.dimen.font_size_large

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/DiscoverFragment;->n:I

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->q()Z

    move-result v0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->s()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    if-nez v4, :cond_4

    if-eqz v0, :cond_4

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v5, 0x1

    :goto_1
    new-instance v0, Lcom/twitter/android/fd;

    iget v3, p0, Lcom/twitter/android/DiscoverFragment;->n:I

    move-object v1, v11

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/fd;-><init>(Landroid/content/Context;Landroid/widget/ListView;IZZ)V

    iput-object v0, p0, Lcom/twitter/android/DiscoverFragment;->m:Lcom/twitter/android/fd;

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v0, Lcom/twitter/android/ez;

    invoke-direct {v0, p0}, Lcom/twitter/android/ez;-><init>(Lcom/twitter/android/DiscoverFragment;)V

    iput-object v0, p0, Lcom/twitter/android/DiscoverFragment;->O:Lcom/twitter/library/client/j;

    return-void

    :cond_4
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    const-wide/16 v2, 0x0

    const/4 v0, -0x1

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-ne v0, p2, :cond_0

    if-eqz p3, :cond_0

    const-string/jumbo v0, "user_id"

    invoke-virtual {p3, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    const-string/jumbo v2, "friendship"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "friendship"

    const/4 v3, 0x0

    invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iget-object v3, p0, Lcom/twitter/android/DiscoverFragment;->a:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v3, v0, v1, v2}, Lcom/twitter/library/util/FriendshipCache;->a(JI)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v3, v0, v1, v2}, Lcom/twitter/library/util/FriendshipCache;->b(JI)V

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetChanged()V

    goto :goto_0

    :pswitch_1
    if-ne v0, p2, :cond_0

    if-eqz p3, :cond_0

    const-string/jumbo v0, "friendship_cache"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/DiscoverFragment;->a:Lcom/twitter/library/util/FriendshipCache;

    const-string/jumbo v0, "friendship_cache"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v1, v0}, Lcom/twitter/library/util/FriendshipCache;->a(Lcom/twitter/library/util/FriendshipCache;)V

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetChanged()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic onClick(Lcom/twitter/library/widget/BaseUserView;JI)V
    .locals 0

    check-cast p1, Lcom/twitter/library/widget/UserView;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/twitter/android/DiscoverFragment;->onClick(Lcom/twitter/library/widget/UserView;JI)V

    return-void
.end method

.method public onClick(Lcom/twitter/library/widget/UserView;JI)V
    .locals 11

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getPromotedContent()Lcom/twitter/library/api/PromotedContent;

    move-result-object v9

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v10

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getTag()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/twitter/android/fb;

    iget-object v0, p1, Lcom/twitter/library/widget/UserView;->m:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0}, Lcom/twitter/library/widget/ActionButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->a:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/library/util/FriendshipCache;->e(J)V

    invoke-virtual {p0, v1, v2, p2, p3}, Lcom/twitter/android/DiscoverFragment;->b(JJ)Landroid/util/Pair;

    invoke-virtual {v10, p2, p3, v9}, Lcom/twitter/android/client/c;->a(JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/DiscoverFragment;->d(Ljava/lang/String;)V

    const-string/jumbo v0, "unfollow"

    :goto_0
    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->l()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "discover:user_module:user"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object v0, v3, v4

    invoke-static {v3}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v3, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-virtual {v3, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, v8, Lcom/twitter/android/fb;->f:Ljava/lang/String;

    invoke-virtual {v0, p2, p3, v9, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/DiscoverFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v10, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->a:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/library/util/FriendshipCache;->b(J)V

    iget-wide v5, v8, Lcom/twitter/android/fb;->c:J

    iget v7, v8, Lcom/twitter/android/fb;->a:I

    move-object v0, p0

    move-wide v3, p2

    invoke-virtual/range {v0 .. v7}, Lcom/twitter/android/DiscoverFragment;->a(JJJI)V

    const/4 v0, 0x0

    invoke-virtual {v10, p2, p3, v0, v9}, Lcom/twitter/android/client/c;->a(JZLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/DiscoverFragment;->d(Ljava/lang/String;)V

    const-string/jumbo v0, "follow"

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "no_tweet_marker"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/DiscoverFragment;->l:Z

    new-instance v0, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->a(I)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/DiscoverFragment;->b(Lcom/twitter/library/scribe/ScribeAssociation;)V

    if-eqz p1, :cond_0

    const-string/jumbo v0, "friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/FriendshipCache;

    iput-object v0, p0, Lcom/twitter/android/DiscoverFragment;->a:Lcom/twitter/library/util/FriendshipCache;

    :goto_0
    new-instance v0, Lcom/twitter/android/ex;

    invoke-direct {v0, p0}, Lcom/twitter/android/ex;-><init>(Lcom/twitter/android/DiscoverFragment;)V

    iput-object v0, p0, Lcom/twitter/android/DiscoverFragment;->i:Lcom/twitter/android/ex;

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/DiscoverFragment;->h:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/DiscoverFragment;->a(ILcom/twitter/library/util/ar;)V

    return-void

    :cond_0
    new-instance v0, Lcom/twitter/library/util/FriendshipCache;

    invoke-direct {v0}, Lcom/twitter/library/util/FriendshipCache;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/DiscoverFragment;->a:Lcom/twitter/library/util/FriendshipCache;

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 11

    const/4 v10, 0x3

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    packed-switch p1, :pswitch_data_0

    move-object v0, v3

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/twitter/library/provider/ag;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "ownerId"

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    new-instance v0, Lcom/twitter/android/bl;

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/bl;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x2

    new-array v5, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v1, "TREND"

    aput-object v1, v5, v0

    const/4 v0, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v0

    const-string/jumbo v0, "t_tag=? AND t_data_type=?"

    sget-object v3, Lcom/twitter/library/provider/cc;->a:[Ljava/lang/String;

    const-string/jumbo v0, "t_updated_at DESC, _id ASC"

    new-instance v0, Lcom/twitter/android/bl;

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/provider/av;->d:Landroid/net/Uri;

    iget-wide v8, p0, Lcom/twitter/android/DiscoverFragment;->Q:J

    invoke-static {v2, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2, v6, v7}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string/jumbo v4, "limit"

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v4, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    const-string/jumbo v4, "t_tag=? AND t_data_type=?"

    const-string/jumbo v6, "t_updated_at DESC, _id ASC"

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/bl;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const v0, 0x7f03008c    # com.twitter.android.R.layout.grouped_list_header_fragment

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/android/DiscoverFragment;->a(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->onDestroy()V

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/DiscoverFragment;->h:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/DiscoverFragment;->b(ILcom/twitter/library/util/ar;)V

    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/DiscoverFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 1

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onLoaderReset(Landroid/support/v4/content/Loader;)V

    :goto_0
    return-void

    :pswitch_0
    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onLoaderReset(Landroid/support/v4/content/Loader;)V

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/DiscoverFragment;->e(Landroid/database/Cursor;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->onResume()V

    invoke-virtual {p0}, Lcom/twitter/android/DiscoverFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    const-string/jumbo v0, "android_discover_inline_counts_1876"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->a:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0}, Lcom/twitter/library/util/FriendshipCache;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "friendship_cache"

    iget-object v1, p0, Lcom/twitter/android/DiscoverFragment;->a:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/DiscoverFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/ev;

    invoke-virtual {v0}, Lcom/twitter/android/ev;->f()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "spinning_gap_ids"

    invoke-static {v0}, Lcom/twitter/library/util/Util;->b(Ljava/util/Collection;)[J

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    :cond_1
    return-void
.end method

.method q()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/DiscoverFragment;->b:I

    return-void
.end method

.method public u_()V
    .locals 0

    return-void
.end method
