.class public Lcom/twitter/android/TrendLocationsFragment;
.super Lcom/twitter/android/client/BaseListFragment;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/Filterable;


# instance fields
.field a:Landroid/widget/Filter;

.field b:Z

.field private c:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseListFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/TrendLocationsFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TrendLocationsFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/TrendLocationsFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/TrendLocationsFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/TrendLocationsFragment;I)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/twitter/android/TrendLocationsFragment;->b(I)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/TrendLocationsFragment;I)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/twitter/android/TrendLocationsFragment;->a_(I)V

    return-void
.end method

.method private e()V
    .locals 3

    const/4 v2, 0x3

    invoke-virtual {p0, v2}, Lcom/twitter/android/TrendLocationsFragment;->a_(I)V

    invoke-virtual {p0}, Lcom/twitter/android/TrendLocationsFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/TrendLocationsFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/Session;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v2}, Lcom/twitter/android/TrendLocationsFragment;->a(Ljava/lang/String;I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/TrendLocationsFragment;->b:Z

    return-void
.end method


# virtual methods
.method protected P()Z
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/TrendLocationsFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    const/4 v0, 0x1

    return v0
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 1

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/twitter/android/TrendLocationsFragment;->b(I)V

    iget-object v0, p0, Lcom/twitter/android/TrendLocationsFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0, p2}, Landroid/support/v4/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    iget-object v0, p0, Lcom/twitter/android/TrendLocationsFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/TrendLocationsFragment;->b:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/TrendLocationsFragment;->e()V

    :cond_0
    return-void
.end method

.method protected a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 7

    invoke-virtual {p0}, Lcom/twitter/android/TrendLocationsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    if-eqz v0, :cond_0

    const/4 v2, -0x1

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v4, "woeid"

    const/4 v5, 0x1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "loc_name"

    const/4 v5, 0x2

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    :cond_0
    return-void
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/TrendLocationsFragment;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TrendLocationsFragment;->a:Landroid/widget/Filter;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/vh;

    invoke-direct {v0, p0}, Lcom/twitter/android/vh;-><init>(Lcom/twitter/android/TrendLocationsFragment;)V

    iput-object v0, p0, Lcom/twitter/android/TrendLocationsFragment;->a:Landroid/widget/Filter;

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TrendLocationsFragment;->a:Landroid/widget/Filter;

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 9

    const/4 v7, 0x1

    const/4 v3, 0x0

    const/4 v6, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/TrendLocationsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f030132    # com.twitter.android.R.layout.search_view

    invoke-virtual {v0, v2, v3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/TrendLocationsFragment;->ao()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setCustomView(Landroid/view/View;)V

    const v2, 0x7f090212    # com.twitter.android.R.id.query

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const v2, 0x7f0f04c4    # com.twitter.android.R.string.trends_search_loc

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setHint(I)V

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iput-object v0, p0, Lcom/twitter/android/TrendLocationsFragment;->c:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/twitter/android/TrendLocationsFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/vg;

    const v2, 0x7f03013b    # com.twitter.android.R.layout.simple_row_text_view

    new-array v4, v7, [Ljava/lang/String;

    const-string/jumbo v5, "display_name"

    aput-object v5, v4, v6

    new-array v5, v7, [I

    const v8, 0x7f090090    # com.twitter.android.R.id.title

    aput v8, v5, v6

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/vg;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    iput-object v0, p0, Lcom/twitter/android/TrendLocationsFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/TrendLocationsFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TrendLocationsFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lcom/twitter/android/TrendLocationsFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/f;

    invoke-virtual {p0}, Lcom/twitter/android/TrendLocationsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v4, "trend_loc_prefs"

    invoke-direct {v1, v2, v0, v4}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/TrendLocationsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    if-eqz v0, :cond_2

    const-string/jumbo v2, "lang"

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v4, "country"

    invoke-virtual {v1, v4, v3}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-string/jumbo v0, "last_refresh"

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v0, v4, v5}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;J)J

    move-result-wide v0

    const-wide/32 v4, 0x493e0

    add-long/2addr v0, v4

    cmp-long v0, v2, v0

    if-gez v0, :cond_2

    :goto_0
    new-instance v0, Lcom/twitter/android/vi;

    invoke-direct {v0, p0}, Lcom/twitter/android/vi;-><init>(Lcom/twitter/android/TrendLocationsFragment;)V

    iput-object v0, p0, Lcom/twitter/android/TrendLocationsFragment;->O:Lcom/twitter/library/client/j;

    if-eqz v6, :cond_1

    invoke-direct {p0}, Lcom/twitter/android/TrendLocationsFragment;->e()V

    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/twitter/android/TrendLocationsFragment;->a_(I)V

    invoke-virtual {p0}, Lcom/twitter/android/TrendLocationsFragment;->p()V

    goto :goto_1

    :cond_2
    move v6, v7

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7

    const/4 v4, 0x0

    new-instance v0, Lcom/twitter/android/bl;

    invoke-virtual {p0}, Lcom/twitter/android/TrendLocationsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/provider/SuggestionsProvider;->f:Landroid/net/Uri;

    sget-object v3, Lcom/twitter/android/provider/f;->a:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/bl;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onDestroy()V

    iget-object v0, p0, Lcom/twitter/android/TrendLocationsFragment;->c:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/TrendLocationsFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/TrendLocationsFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method protected p()V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/TrendLocationsFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method
