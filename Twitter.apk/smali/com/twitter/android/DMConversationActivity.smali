.class public Lcom/twitter/android/DMConversationActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/android/AttachMediaListener;
.implements Lcom/twitter/android/widget/ce;
.implements Lcom/twitter/android/wz;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/twitter/android/PhotoSelectHelper;

.field private c:Lcom/twitter/android/PostStorage$MediaItem;

.field private d:Z

.field private e:Z

.field private f:Lcom/twitter/android/DMConversationFragment;

.field private g:Lcom/twitter/android/DMComposeFragment;

.field private h:Ljava/lang/Runnable;

.field private i:Landroid/os/Handler;

.field private j:I

.field private k:Landroid/widget/ImageView;

.field private l:Landroid/widget/RelativeLayout;

.field private m:Landroid/widget/ImageView;

.field private n:Landroid/widget/ProgressBar;

.field private o:Landroid/widget/ImageView;

.field private p:Landroid/widget/Button;

.field private q:Landroid/widget/TextView;

.field private r:Lcom/twitter/android/TweetBoxFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    return-void
.end method

.method private a(Landroid/content/Intent;)Landroid/os/Bundle;
    .locals 1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    :cond_0
    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/DMConversationActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/graphics/Bitmap;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->n:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->l:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->o:Landroid/widget/ImageView;

    const v1, 0x7f020169    # com.twitter.android.R.drawable.ic_dialog_camera_active

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->r:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->q()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/DMConversationActivity;Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/DMConversationActivity;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    new-instance v0, Lcom/twitter/android/dj;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/dj;-><init>(Lcom/twitter/android/DMConversationActivity;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/DMConversationActivity;->h:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/android/DMConversationActivity;->h:Ljava/lang/Runnable;

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 3

    new-instance v0, Lcom/twitter/library/api/conversations/ar;

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/twitter/library/api/conversations/ar;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Z)V

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/DMConversationActivity;->a(Lcom/twitter/library/service/b;II)Z

    return-void
.end method

.method private a(Ljava/lang/String;[J)V
    .locals 3

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/twitter/android/DMConversationActivity;->a:Ljava/lang/String;

    invoke-direct {p0}, Lcom/twitter/android/DMConversationActivity;->l()V

    new-instance v0, Lcom/twitter/library/api/conversations/ai;

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/twitter/library/api/conversations/ai;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;[J)V

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/DMConversationActivity;->a(Lcom/twitter/library/service/b;II)Z

    goto :goto_0
.end method

.method private a([J)V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/twitter/library/api/conversations/f;->a(J[J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/twitter/android/DMConversationActivity;->a(Ljava/lang/String;[J)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/DMConversationActivity;)Landroid/widget/RelativeLayout;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->l:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/DMConversationActivity;->a(Ljava/lang/String;[J)V

    return-void
.end method

.method private b(Ljava/lang/String;I)V
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/android/DMConversationActivity;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x4

    if-ne p2, v0, :cond_1

    :cond_0
    const/16 v0, 0x301

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f010c    # com.twitter.android.R.string.direct_message_error_title

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0319    # com.twitter.android.R.string.post_retry_direct_messsage_question

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0362    # com.twitter.android.R.string.retry

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0089    # com.twitter.android.R.string.cancel

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/dk;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/dk;-><init>(Lcom/twitter/android/DMConversationActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/android/widget/ce;)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/DMConversationActivity;->d:Z

    :cond_1
    return-void
.end method

.method private c(Landroid/os/Bundle;)V
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v1, "tweet_box"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/TweetBoxFragment;

    iput-object v0, p0, Lcom/twitter/android/DMConversationActivity;->r:Lcom/twitter/android/TweetBoxFragment;

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->r:Lcom/twitter/android/TweetBoxFragment;

    if-nez v0, :cond_0

    const v0, 0x7f03015c    # com.twitter.android.R.layout.tweet_box

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/TweetBoxFragment;->a(IZ)Lcom/twitter/android/TweetBoxFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/DMConversationActivity;->r:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0900f7    # com.twitter.android.R.id.tweet_box

    iget-object v2, p0, Lcom/twitter/android/DMConversationActivity;->r:Lcom/twitter/android/TweetBoxFragment;

    const-string/jumbo v3, "tweet_box"

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->r:Lcom/twitter/android/TweetBoxFragment;

    const v1, 0x7f0f0314    # com.twitter.android.R.string.post_button_send

    invoke-virtual {p0, v1}, Lcom/twitter/android/DMConversationActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetBoxFragment;->b(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->r:Lcom/twitter/android/TweetBoxFragment;

    const v1, 0x7f0f031a    # com.twitter.android.R.string.post_title_direct_message

    invoke-virtual {p0, v1}, Lcom/twitter/android/DMConversationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetBoxFragment;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->r:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0, p0}, Lcom/twitter/android/TweetBoxFragment;->a(Lcom/twitter/android/wz;)V

    if-eqz p1, :cond_1

    const-string/jumbo v0, "android.intent.extra.TEXT"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->r:Lcom/twitter/android/TweetBoxFragment;

    const-string/jumbo v1, "android.intent.extra.TEXT"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/TweetBoxFragment;->a(Ljava/lang/String;[I)V

    :cond_1
    return-void
.end method

.method static synthetic c(Lcom/twitter/android/DMConversationActivity;)V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->W()V

    return-void
.end method

.method private g()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->r:Lcom/twitter/android/TweetBoxFragment;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetBoxFragment;->a(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/twitter/android/DMConversationActivity;->h()V

    return-void
.end method

.method private h()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->k:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->o:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->o:Landroid/widget/ImageView;

    const v1, 0x7f020168    # com.twitter.android.R.drawable.ic_dialog_camera

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->l:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->r:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->q()V

    return-void
.end method

.method private k()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private l()V
    .locals 5

    const v4, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    const/4 v3, 0x2

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->f:Lcom/twitter/android/DMConversationFragment;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/DMConversationFragment;

    invoke-direct {v1}, Lcom/twitter/android/DMConversationFragment;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/DMConversationActivity;->f:Lcom/twitter/android/DMConversationFragment;

    const-string/jumbo v1, "conversation_id"

    iget-object v2, p0, Lcom/twitter/android/DMConversationActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/DMConversationActivity;->f:Lcom/twitter/android/DMConversationFragment;

    invoke-virtual {v1, v0}, Lcom/twitter/android/DMConversationFragment;->a(Landroid/content/Intent;)Lcom/twitter/android/client/BaseListFragment;

    :goto_0
    iget v0, p0, Lcom/twitter/android/DMConversationActivity;->j:I

    if-eq v0, v3, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/DMConversationActivity;->j:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/twitter/android/DMConversationActivity;->f:Lcom/twitter/android/DMConversationFragment;

    const-string/jumbo v2, "mthread"

    invoke-virtual {v0, v4, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    :goto_1
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    iput v3, p0, Lcom/twitter/android/DMConversationActivity;->j:I

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->f:Lcom/twitter/android/DMConversationFragment;

    iget-object v1, p0, Lcom/twitter/android/DMConversationActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/DMConversationFragment;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/twitter/android/DMConversationActivity;->f:Lcom/twitter/android/DMConversationFragment;

    const-string/jumbo v2, "mthread"

    invoke-virtual {v0, v4, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    goto :goto_1
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const v1, 0x7f0300c9    # com.twitter.android.R.layout.messages_thread

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a(Z)V

    return-object v0
.end method

.method public a()V
    .locals 6

    iget v0, p0, Lcom/twitter/android/DMConversationActivity;->j:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->g:Lcom/twitter/android/DMComposeFragment;

    invoke-virtual {v0}, Lcom/twitter/android/DMComposeFragment;->f()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v0

    new-array v2, v0, [J

    const/4 v0, 0x0

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    add-int/lit8 v0, v1, 0x1

    aput-wide v4, v2, v1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-direct {p0, v2}, Lcom/twitter/android/DMConversationActivity;->a([J)V

    :cond_1
    return-void
.end method

.method public a(I)V
    .locals 3

    rsub-int v0, p1, 0x8c

    iget-object v1, p0, Lcom/twitter/android/DMConversationActivity;->c:Lcom/twitter/android/PostStorage$MediaItem;

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, -0x18

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/DMConversationActivity;->q:Landroid/widget/TextView;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-ltz v0, :cond_1

    const/16 v1, 0x8c

    if-ge v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/DMConversationActivity;->e:Z

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->p:Landroid/widget/Button;

    iget-boolean v1, p0, Lcom/twitter/android/DMConversationActivity;->e:Z

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(IILcom/twitter/library/service/b;)V
    .locals 5

    const v4, 0x7f0f0115    # com.twitter.android.R.string.dm_blocked_generic

    const/4 v3, 0x0

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/client/BaseFragmentActivity;->a(IILcom/twitter/library/service/b;)V

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    :cond_0
    move-object v0, p3

    check-cast v0, Lcom/twitter/library/api/conversations/as;

    invoke-virtual {p3}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/service/e;

    invoke-virtual {v1}, Lcom/twitter/library/service/e;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/twitter/library/api/conversations/as;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/DMConversationActivity;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->f:Lcom/twitter/android/DMConversationFragment;

    iget-object v1, p0, Lcom/twitter/android/DMConversationActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/DMConversationFragment;->a(Ljava/lang/String;)V

    :cond_1
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/DMConversationActivity;->h:Ljava/lang/Runnable;

    :cond_2
    :goto_1
    return-void

    :cond_3
    invoke-virtual {v0}, Lcom/twitter/library/api/conversations/as;->g()[I

    move-result-object v2

    invoke-virtual {v1}, Lcom/twitter/library/service/e;->c()I

    move-result v1

    invoke-virtual {v0}, Lcom/twitter/library/api/conversations/as;->h()Ljava/lang/String;

    move-result-object v0

    packed-switch v1, :pswitch_data_0

    invoke-direct {p0, v0, p1}, Lcom/twitter/android/DMConversationActivity;->b(Ljava/lang/String;I)V

    goto :goto_0

    :pswitch_0
    const/16 v1, 0x96

    invoke-static {v2, v1}, Lcom/twitter/library/util/Util;->a([II)Z

    move-result v1

    if-eqz v1, :cond_4

    const v0, 0x7f0f011b    # com.twitter.android.R.string.dm_error_not_follower

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_4
    const/16 v1, 0xfc

    invoke-static {v2, v1}, Lcom/twitter/library/util/Util;->a([II)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/twitter/android/DMConversationActivity;->h:Ljava/lang/Runnable;

    if-nez v1, :cond_5

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/client/c;->M()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-direct {p0, v0}, Lcom/twitter/android/DMConversationActivity;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    invoke-static {p0, v4, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_6
    const/16 v0, 0x97

    invoke-static {v2, v0}, Lcom/twitter/library/util/Util;->a([II)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-static {p0, v4, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_7
    const v0, 0x7f0f0119    # com.twitter.android.R.string.dm_error_generic

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0f011a    # com.twitter.android.R.string.dm_error_non_existing

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x193
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(J)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [J

    const/4 v1, 0x0

    aput-wide p1, v0, v1

    invoke-direct {p0, v0}, Lcom/twitter/android/DMConversationActivity;->a([J)V

    return-void
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 1

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-nez p3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->b:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0}, Lcom/twitter/android/PhotoSelectHelper;->c()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    if-ne v0, p3, :cond_0

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->b:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0}, Lcom/twitter/android/PhotoSelectHelper;->d()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x302
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/net/Uri;Lcom/twitter/android/AttachMediaListener$MediaAttachProgress;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->l:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->n:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->k:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->k:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    if-nez p1, :cond_3

    invoke-direct {p0, v2}, Lcom/twitter/android/DMConversationActivity;->a(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v0

    move-object v1, v0

    :goto_0
    iput v5, p0, Lcom/twitter/android/DMConversationActivity;->j:I

    const-string/jumbo v0, "error_dialog"

    invoke-virtual {v1, v0, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/DMConversationActivity;->d:Z

    new-instance v0, Lcom/twitter/android/PhotoSelectHelper;

    const-string/jumbo v3, "dm_composition"

    sget-object v4, Lcom/twitter/android/PhotoSelectHelper$MediaType;->c:Ljava/util/EnumSet;

    invoke-direct {v0, p0, p0, v3, v4}, Lcom/twitter/android/PhotoSelectHelper;-><init>(Landroid/app/Activity;Lcom/twitter/android/AttachMediaListener;Ljava/lang/String;Ljava/util/EnumSet;)V

    iput-object v0, p0, Lcom/twitter/android/DMConversationActivity;->b:Lcom/twitter/android/PhotoSelectHelper;

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->b:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/android/PhotoSelectHelper;->a(Lcom/twitter/library/client/Session;)V

    const v0, 0x7f090111    # com.twitter.android.R.id.photo_preview

    invoke-virtual {p0, v0}, Lcom/twitter/android/DMConversationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/DMConversationActivity;->k:Landroid/widget/ImageView;

    const v0, 0x7f090110    # com.twitter.android.R.id.photo_container

    invoke-virtual {p0, v0}, Lcom/twitter/android/DMConversationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/twitter/android/DMConversationActivity;->l:Landroid/widget/RelativeLayout;

    const v0, 0x7f0901e9    # com.twitter.android.R.id.photo_progress

    invoke-virtual {p0, v0}, Lcom/twitter/android/DMConversationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/twitter/android/DMConversationActivity;->n:Landroid/widget/ProgressBar;

    const v0, 0x7f09010f    # com.twitter.android.R.id.photo_compose

    invoke-virtual {p0, v0}, Lcom/twitter/android/DMConversationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/DMConversationActivity;->o:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090112    # com.twitter.android.R.id.photo_dismiss

    invoke-virtual {p0, v0}, Lcom/twitter/android/DMConversationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/DMConversationActivity;->m:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090113    # com.twitter.android.R.id.tweet_button

    invoke-virtual {p0, v0}, Lcom/twitter/android/DMConversationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/DMConversationActivity;->p:Landroid/widget/Button;

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->p:Landroid/widget/Button;

    const v3, 0x7f0f0314    # com.twitter.android.R.string.post_button_send

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->p:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->p:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0900f4    # com.twitter.android.R.id.count

    invoke-virtual {p0, v0}, Lcom/twitter/android/DMConversationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/DMConversationActivity;->q:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->q:Landroid/widget/TextView;

    const/16 v3, 0x8c

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v1}, Lcom/twitter/android/DMConversationActivity;->c(Landroid/os/Bundle;)V

    const-string/jumbo v0, "conversation_id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "user_ids"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v1

    if-eqz v0, :cond_4

    invoke-direct {p0, v0}, Lcom/twitter/android/DMConversationActivity;->b(Ljava/lang/String;)V

    :cond_0
    :goto_1
    if-nez p1, :cond_5

    invoke-direct {p0}, Lcom/twitter/android/DMConversationActivity;->k()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/twitter/android/DMComposeFragment;

    invoke-direct {v0}, Lcom/twitter/android/DMComposeFragment;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/DMConversationActivity;->g:Lcom/twitter/android/DMComposeFragment;

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->g:Lcom/twitter/android/DMComposeFragment;

    invoke-virtual {v0, v2}, Lcom/twitter/android/DMComposeFragment;->a(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    iget-object v3, p0, Lcom/twitter/android/DMConversationActivity;->g:Lcom/twitter/android/DMComposeFragment;

    const-string/jumbo v4, "mthread"

    invoke-virtual {v0, v1, v3, v4}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    iput v6, p0, Lcom/twitter/android/DMConversationActivity;->j:I

    :cond_1
    :goto_2
    const-string/jumbo v0, "notification::::open"

    const-string/jumbo v1, "ref_event"

    invoke-virtual {v2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/twitter/library/api/conversations/ac;

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/api/conversations/ac;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1, v5}, Lcom/twitter/android/DMConversationActivity;->a(Lcom/twitter/library/service/b;II)Z

    :cond_2
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/DMConversationActivity;->i:Landroid/os/Handler;

    return-void

    :cond_3
    move-object v1, p1

    goto/16 :goto_0

    :cond_4
    if-eqz v1, :cond_0

    invoke-direct {p0, v1}, Lcom/twitter/android/DMConversationActivity;->a([J)V

    goto :goto_1

    :cond_5
    invoke-direct {p0}, Lcom/twitter/android/DMConversationActivity;->k()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v1, "mthread"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/DMConversationFragment;

    iput-object v0, p0, Lcom/twitter/android/DMConversationActivity;->f:Lcom/twitter/android/DMConversationFragment;

    :goto_3
    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->b:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0, p1}, Lcom/twitter/android/PhotoSelectHelper;->a(Landroid/os/Bundle;)V

    const-string/jumbo v0, "media_item"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/PostStorage$MediaItem;

    iput-object v0, p0, Lcom/twitter/android/DMConversationActivity;->c:Lcom/twitter/android/PostStorage$MediaItem;

    goto :goto_2

    :cond_6
    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v1, "mthread"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/DMComposeFragment;

    iput-object v0, p0, Lcom/twitter/android/DMConversationActivity;->g:Lcom/twitter/android/DMComposeFragment;

    goto :goto_3
.end method

.method public a(Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;Lcom/twitter/android/PostStorage$MediaItem;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->c:Lcom/twitter/android/PostStorage$MediaItem;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->c:Lcom/twitter/android/PostStorage$MediaItem;

    iget-object v0, v0, Lcom/twitter/android/PostStorage$MediaItem;->e:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/twitter/android/DMConversationActivity;->a(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/PostStorage$MediaItem;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->c:Lcom/twitter/android/PostStorage$MediaItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->c:Lcom/twitter/android/PostStorage$MediaItem;

    invoke-virtual {v0, p1}, Lcom/twitter/android/PostStorage$MediaItem;->a(Lcom/twitter/android/PostStorage$MediaItem;)V

    :cond_0
    iput-object p1, p0, Lcom/twitter/android/DMConversationActivity;->c:Lcom/twitter/android/PostStorage$MediaItem;

    iget-object v0, p1, Lcom/twitter/android/PostStorage$MediaItem;->e:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/twitter/android/DMConversationActivity;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 0

    return-void
.end method

.method public a(Z)V
    .locals 0

    return-void
.end method

.method public a([Lcom/twitter/library/api/TwitterContact;)V
    .locals 0

    return-void
.end method

.method public a(Landroid/net/Uri;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public b()V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->e()V

    return-void
.end method

.method public d()V
    .locals 0

    return-void
.end method

.method e()V
    .locals 10

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "messages:thread:::send_dm"

    aput-object v4, v3, v9

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->r:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->j()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->c:Lcom/twitter/android/PostStorage$MediaItem;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->c:Lcom/twitter/android/PostStorage$MediaItem;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage$MediaItem;->b()Lcom/twitter/library/api/MediaEntity;

    move-result-object v7

    :goto_0
    new-instance v0, Lcom/twitter/library/api/conversations/ak;

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    iget-object v5, p0, Lcom/twitter/android/DMConversationActivity;->a:Ljava/lang/String;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/api/conversations/ak;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/api/MediaEntity;)V

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1, v9}, Lcom/twitter/android/DMConversationActivity;->a(Lcom/twitter/library/service/b;II)Z

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->c:Lcom/twitter/android/PostStorage$MediaItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->c:Lcom/twitter/android/PostStorage$MediaItem;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage$MediaItem;->c()V

    iput-object v8, p0, Lcom/twitter/android/DMConversationActivity;->c:Lcom/twitter/android/PostStorage$MediaItem;

    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/DMConversationActivity;->g()V

    return-void

    :cond_1
    move-object v7, v8

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/twitter/android/DMConversationActivity;->j:I

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/DMConversationActivity;->g:Lcom/twitter/android/DMComposeFragment;

    invoke-virtual {v1}, Lcom/twitter/android/DMComposeFragment;->a()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/DMConversationActivity;->r:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v1}, Lcom/twitter/android/TweetBoxFragment;->e()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/DMConversationActivity;->c:Lcom/twitter/android/PostStorage$MediaItem;

    if-eqz v1, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected j()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x304

    invoke-virtual {p0, v0}, Lcom/twitter/android/DMConversationActivity;->showDialog(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->W()V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->b:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    move v1, p1

    move v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/PhotoSelectHelper;->a(IILandroid/content/Intent;J)V

    return-void
.end method

.method public onBackPressed()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x303

    invoke-virtual {p0, v0}, Lcom/twitter/android/DMConversationActivity;->showDialog(I)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f09010f    # com.twitter.android.R.id.photo_compose

    if-ne v1, v0, :cond_1

    const/16 v0, 0x302

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f080004    # com.twitter.android.R.array.choose_photo_options

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->f(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/android/widget/ce;)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v1, 0x7f090112    # com.twitter.android.R.id.photo_dismiss

    if-ne v1, v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->c:Lcom/twitter/android/PostStorage$MediaItem;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->c:Lcom/twitter/android/PostStorage$MediaItem;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage$MediaItem;->d()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/DMConversationActivity;->c:Lcom/twitter/android/PostStorage$MediaItem;

    :cond_2
    invoke-direct {p0}, Lcom/twitter/android/DMConversationActivity;->h()V

    goto :goto_0

    :cond_3
    const v1, 0x7f090113    # com.twitter.android.R.id.tweet_button

    if-ne v1, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->e()V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3

    packed-switch p1, :pswitch_data_0

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->b:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0, p1}, Lcom/twitter/android/PhotoSelectHelper;->a(I)Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Lcom/twitter/android/di;

    invoke-direct {v0, p0}, Lcom/twitter/android/di;-><init>(Lcom/twitter/android/DMConversationActivity;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0f0297    # com.twitter.android.R.string.new_message

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x7f0f0000    # com.twitter.android.R.string.abandon_changes_question

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f0111    # com.twitter.android.R.string.discard

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f0089    # com.twitter.android.R.string.cancel

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x303
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onDestroy()V

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->b:Lcom/twitter/android/PhotoSelectHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->b:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0}, Lcom/twitter/android/PhotoSelectHelper;->a()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->c:Lcom/twitter/android/PostStorage$MediaItem;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->c:Lcom/twitter/android/PostStorage$MediaItem;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage$MediaItem;->c()V

    :cond_1
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onPostCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/twitter/android/DMConversationActivity;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->r:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->b()V

    :cond_0
    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/DMConversationActivity;->a(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object p1

    :cond_1
    const-string/jumbo v0, "keyboard_open"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->r:Lcom/twitter/android/TweetBoxFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetBoxFragment;->c(Z)V

    :cond_2
    return-void
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onResume()V

    invoke-static {p0}, Lcom/twitter/android/client/aw;->a(Landroid/content/Context;)Lcom/twitter/android/client/aw;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/DMConversationActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/aw;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->c:Lcom/twitter/android/PostStorage$MediaItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->b:Lcom/twitter/android/PhotoSelectHelper;

    iget-object v1, p0, Lcom/twitter/android/DMConversationActivity;->c:Lcom/twitter/android/PostStorage$MediaItem;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/PhotoSelectHelper;->a(Lcom/twitter/android/PostStorage$MediaItem;Z)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/DMConversationActivity;->c:Lcom/twitter/android/PostStorage$MediaItem;

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "conversation_id"

    iget-object v1, p0, Lcom/twitter/android/DMConversationActivity;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "android.intent.extra.TEXT"

    iget-object v1, p0, Lcom/twitter/android/DMConversationActivity;->r:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v1}, Lcom/twitter/android/TweetBoxFragment;->j()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "error_dialog"

    iget-boolean v1, p0, Lcom/twitter/android/DMConversationActivity;->d:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "media_item"

    iget-object v1, p0, Lcom/twitter/android/DMConversationActivity;->c:Lcom/twitter/android/PostStorage$MediaItem;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/twitter/android/DMConversationActivity;->b:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0, p1}, Lcom/twitter/android/PhotoSelectHelper;->b(Landroid/os/Bundle;)V

    return-void
.end method

.method public p_()V
    .locals 0

    return-void
.end method
