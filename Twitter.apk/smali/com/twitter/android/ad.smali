.class public Lcom/twitter/android/ad;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Landroid/widget/ImageView;

.field public final b:Landroid/widget/TextView;

.field public final c:Landroid/widget/TextView;

.field public final d:Landroid/view/ViewGroup;

.field public final e:Landroid/view/ViewGroup;

.field public f:J

.field public g:I

.field public h:Ljava/util/ArrayList;

.field public i:Lcom/twitter/library/provider/ActivityDataList;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7f090077    # com.twitter.android.R.id.image

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/ad;->a:Landroid/widget/ImageView;

    const v0, 0x7f090076    # com.twitter.android.R.id.title_view

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/ad;->b:Landroid/widget/TextView;

    const v0, 0x7f090033    # com.twitter.android.R.id.body

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/ad;->d:Landroid/view/ViewGroup;

    const v0, 0x7f090078    # com.twitter.android.R.id.user_images_container

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/ad;->e:Landroid/view/ViewGroup;

    const v0, 0x7f090079    # com.twitter.android.R.id.view_all_text

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/ad;->c:Landroid/widget/TextView;

    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;II)Landroid/view/View;
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/ad;

    invoke-direct {v1, v0}, Lcom/twitter/android/ad;-><init>(Landroid/view/View;)V

    iput p4, v1, Lcom/twitter/android/ad;->g:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v0, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, v1, Lcom/twitter/android/ad;->e:Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/ref/WeakReference;

    iget-object v1, v1, Lcom/twitter/android/ad;->e:Landroid/view/ViewGroup;

    invoke-direct {v2, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/view/View;ILjava/util/ArrayList;Ljava/util/ArrayList;ZJIZ)V
    .locals 6

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ad;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-wide p6, v0, Lcom/twitter/android/ad;->f:J

    iput p8, v0, Lcom/twitter/android/ad;->g:I

    iput-object p3, v0, Lcom/twitter/android/ad;->h:Ljava/util/ArrayList;

    iget-object v2, v0, Lcom/twitter/android/ad;->a:Landroid/widget/ImageView;

    invoke-virtual {v2, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    if-eqz p5, :cond_1

    iget-object v2, v0, Lcom/twitter/android/ad;->c:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    const v3, 0x7f0c000a    # com.twitter.android.R.dimen.activity_header_margin_top

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v4

    const v5, 0x7f0c000b    # com.twitter.android.R.dimen.activity_multi_padding_bottom_with_view_all

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p1, v2, v3, v4, v1}, Landroid/view/View;->setPadding(IIII)V

    :goto_0
    if-nez p4, :cond_2

    const/4 v1, 0x0

    :goto_1
    if-lez v1, :cond_4

    iget-object v2, v0, Lcom/twitter/android/ad;->d:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v2, v0, Lcom/twitter/android/ad;->d:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    invoke-static {v1, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    const/4 v1, 0x0

    move v4, v1

    :goto_2
    if-ge v4, v3, :cond_3

    iget-object v1, v0, Lcom/twitter/android/ad;->d:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/internal/android/widget/TypefacesTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setVisibility(I)V

    if-eqz p9, :cond_0

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setGravity(I)V

    :cond_0
    invoke-virtual {p4, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v2}, Lcom/twitter/library/provider/Tweet;->A()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    :cond_1
    iget-object v1, v0, Lcom/twitter/android/ad;->c:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p4}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto :goto_1

    :cond_3
    move v2, v3

    :goto_3
    if-ge v2, v5, :cond_5

    iget-object v1, v0, Lcom/twitter/android/ad;->d:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/internal/android/widget/TypefacesTextView;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setVisibility(I)V

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setTag(Ljava/lang/Object;)V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    :cond_4
    iget-object v0, v0, Lcom/twitter/android/ad;->d:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_5
    return-void
.end method
