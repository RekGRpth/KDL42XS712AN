.class Lcom/twitter/android/w;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/cu;


# instance fields
.field final synthetic a:Lcom/twitter/android/ActivityDetailFragment;

.field private b:I


# direct methods
.method constructor <init>(Lcom/twitter/android/ActivityDetailFragment;I)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/w;->a:Lcom/twitter/android/ActivityDetailFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/twitter/android/w;->b:I

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/widget/UserView;J)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/w;->a:Lcom/twitter/android/ActivityDetailFragment;

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getUserId()J

    move-result-wide v1

    const-string/jumbo v3, "cluster_follow"

    invoke-static {v0, p1, v1, v2, v3}, Lcom/twitter/android/ActivityDetailFragment;->a(Lcom/twitter/android/ActivityDetailFragment;Landroid/view/View;JLjava/lang/String;)V

    return-void
.end method

.method public a(Lcom/twitter/library/widget/UserView;Lcom/twitter/library/api/PromotedContent;I)V
    .locals 2

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/w;->a:Lcom/twitter/android/ActivityDetailFragment;

    invoke-static {v0}, Lcom/twitter/android/ActivityDetailFragment;->h(Lcom/twitter/android/ActivityDetailFragment;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/w;->a:Lcom/twitter/android/ActivityDetailFragment;

    invoke-static {v0}, Lcom/twitter/android/ActivityDetailFragment;->i(Lcom/twitter/android/ActivityDetailFragment;)Lcom/twitter/android/client/c;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->a:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {v0, v1, p2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V

    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/library/widget/UserView;J)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/w;->a:Lcom/twitter/android/ActivityDetailFragment;

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getUserId()J

    move-result-wide v1

    iget v3, p0, Lcom/twitter/android/w;->b:I

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/twitter/android/ActivityDetailFragment;->onClick(Lcom/twitter/library/widget/UserView;JI)V

    return-void
.end method
