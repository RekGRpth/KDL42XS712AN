.class Lcom/twitter/android/jl;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/LoginVerificationFragment;

.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/twitter/android/LoginVerificationFragment;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/jl;->a:Lcom/twitter/android/LoginVerificationFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/jl;->b:Landroid/content/Context;

    iput-object p3, p0, Lcom/twitter/android/jl;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/twitter/android/jl;->d:Ljava/lang/String;

    iput-object p5, p0, Lcom/twitter/android/jl;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public varargs a([Ljava/lang/Void;)Lcom/twitter/library/api/account/l;
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/jl;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/jl;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/jl;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/jl;->e:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/api/account/k;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/api/account/l;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/api/account/l;)V
    .locals 6

    const/4 v5, 0x0

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/jl;->a:Lcom/twitter/android/LoginVerificationFragment;

    invoke-virtual {v0}, Lcom/twitter/android/LoginVerificationFragment;->e()V

    iget-object v0, p0, Lcom/twitter/android/jl;->a:Lcom/twitter/android/LoginVerificationFragment;

    invoke-static {v0}, Lcom/twitter/android/LoginVerificationFragment;->c(Lcom/twitter/android/LoginVerificationFragment;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/jl;->a:Lcom/twitter/android/LoginVerificationFragment;

    invoke-static {v1}, Lcom/twitter/android/LoginVerificationFragment;->b(Lcom/twitter/android/LoginVerificationFragment;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "login_verification::request:accept:error"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/jl;->a:Lcom/twitter/android/LoginVerificationFragment;

    const v1, 0x7f0f0242    # com.twitter.android.R.string.login_verification_currently_unavailable

    invoke-static {v0, v1}, Lcom/twitter/android/LoginVerificationFragment;->a(Lcom/twitter/android/LoginVerificationFragment;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/jl;->a:Lcom/twitter/android/LoginVerificationFragment;

    new-instance v1, Lcom/twitter/library/api/account/a;

    iget-object v2, p0, Lcom/twitter/android/jl;->a:Lcom/twitter/android/LoginVerificationFragment;

    invoke-virtual {v2}, Lcom/twitter/android/LoginVerificationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/jl;->a:Lcom/twitter/android/LoginVerificationFragment;

    invoke-static {v3}, Lcom/twitter/android/LoginVerificationFragment;->b(Lcom/twitter/android/LoginVerificationFragment;)Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-direct {v1, v2, v3, p1}, Lcom/twitter/library/api/account/a;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/api/account/l;)V

    const/4 v2, 0x3

    invoke-static {v0, v1, v2, v5}, Lcom/twitter/android/LoginVerificationFragment;->a(Lcom/twitter/android/LoginVerificationFragment;Lcom/twitter/library/service/b;II)Z

    goto :goto_0
.end method

.method public synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/jl;->a([Ljava/lang/Void;)Lcom/twitter/library/api/account/l;

    move-result-object v0

    return-object v0
.end method

.method public synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/twitter/library/api/account/l;

    invoke-virtual {p0, p1}, Lcom/twitter/android/jl;->a(Lcom/twitter/library/api/account/l;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/jl;->a:Lcom/twitter/android/LoginVerificationFragment;

    iget-object v1, p0, Lcom/twitter/android/jl;->a:Lcom/twitter/android/LoginVerificationFragment;

    const v2, 0x7f0f023d    # com.twitter.android.R.string.login_verification_approving_request

    invoke-virtual {v1, v2}, Lcom/twitter/android/LoginVerificationFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/LoginVerificationFragment;->a(Ljava/lang/String;)V

    return-void
.end method
