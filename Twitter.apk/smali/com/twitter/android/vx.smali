.class Lcom/twitter/android/vx;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/wz;


# instance fields
.field final synthetic a:Lcom/twitter/android/TweetActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/TweetActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/vx;->a:Lcom/twitter/android/TweetActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/vx;->a:Lcom/twitter/android/TweetActivity;

    iget-object v0, v0, Lcom/twitter/android/TweetActivity;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/vx;->a:Lcom/twitter/android/TweetActivity;

    invoke-static {v0}, Lcom/twitter/android/TweetActivity;->a(Lcom/twitter/android/TweetActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Lkl;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/vx;->a:Lcom/twitter/android/TweetActivity;

    invoke-static {v0}, Lcom/twitter/android/TweetActivity;->b(Lcom/twitter/android/TweetActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/vx;->a:Lcom/twitter/android/TweetActivity;

    invoke-static {v0}, Lcom/twitter/android/TweetActivity;->c(Lcom/twitter/android/TweetActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/vx;->a:Lcom/twitter/android/TweetActivity;

    iget-object v0, v0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/android/TweetFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetFragment;->e(Z)V

    return-void
.end method

.method public a(I)V
    .locals 4

    rsub-int v1, p1, 0x8c

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/vx;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/vx;->a:Lcom/twitter/android/TweetActivity;

    iget-object v2, v0, Lcom/twitter/android/TweetActivity;->e:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/vx;->a:Lcom/twitter/android/TweetActivity;

    invoke-virtual {v0}, Lcom/twitter/android/TweetActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    if-ltz v1, :cond_0

    const v0, 0x7f0b0057    # com.twitter.android.R.color.gray

    :goto_0
    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/twitter/android/vx;->a:Lcom/twitter/android/TweetActivity;

    iget-object v2, v0, Lcom/twitter/android/TweetActivity;->f:Landroid/widget/Button;

    if-ltz v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/vx;->a:Lcom/twitter/android/TweetActivity;

    iget-object v0, v0, Lcom/twitter/android/TweetActivity;->d:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->v()I

    move-result v0

    if-ge v1, v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setEnabled(Z)V

    return-void

    :cond_0
    const v0, 0x7f0b0072    # com.twitter.android.R.color.red

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Ljava/lang/String;I)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/vx;->a:Lcom/twitter/android/TweetActivity;

    const v1, 0x7f0901a5    # com.twitter.android.R.id.in_reply_to_text

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-lez p2, :cond_0

    iget-object v1, p0, Lcom/twitter/android/vx;->a:Lcom/twitter/android/TweetActivity;

    invoke-virtual {v1}, Lcom/twitter/android/TweetActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f01e5    # com.twitter.android.R.string.in_reply_to_and_more

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/vx;->a:Lcom/twitter/android/TweetActivity;

    invoke-virtual {v1}, Lcom/twitter/android/TweetActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f01e4    # com.twitter.android.R.string.in_reply_to

    new-array v3, v5, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/vx;->a:Lcom/twitter/android/TweetActivity;

    invoke-static {v0, p1}, Lcom/twitter/android/TweetActivity;->a(Lcom/twitter/android/TweetActivity;I)Z

    iget-object v0, p0, Lcom/twitter/android/vx;->a:Lcom/twitter/android/TweetActivity;

    iget-object v0, v0, Lcom/twitter/android/TweetActivity;->e:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/vx;->a:Lcom/twitter/android/TweetActivity;

    iget-object v1, v1, Lcom/twitter/android/TweetActivity;->f:Landroid/widget/Button;

    iget-object v2, p0, Lcom/twitter/android/vx;->a:Lcom/twitter/android/TweetActivity;

    invoke-static {v2}, Lcom/twitter/android/TweetActivity;->e(Lcom/twitter/android/TweetActivity;)Z

    move-result v2

    if-nez v2, :cond_0

    if-ltz v0, :cond_0

    iget-object v2, p0, Lcom/twitter/android/vx;->a:Lcom/twitter/android/TweetActivity;

    iget-object v2, v2, Lcom/twitter/android/TweetActivity;->d:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v2}, Lcom/twitter/android/TweetBoxFragment;->v()I

    move-result v2

    if-ge v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a([Lcom/twitter/library/api/TwitterContact;)V
    .locals 0

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/vx;->a:Lcom/twitter/android/TweetActivity;

    invoke-static {v0}, Lcom/twitter/android/TweetActivity;->d(Lcom/twitter/android/TweetActivity;)V

    return-void
.end method

.method public d()V
    .locals 0

    return-void
.end method

.method public p_()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/vx;->a:Lcom/twitter/android/TweetActivity;

    iget-object v0, v0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/android/TweetFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetFragment;->e(Z)V

    return-void
.end method
