.class Lcom/twitter/android/kc;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# instance fields
.field final synthetic a:Lcom/twitter/android/MainActivity;


# direct methods
.method public constructor <init>(Lcom/twitter/android/MainActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/kc;->a:Lcom/twitter/android/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 3

    const/4 v2, 0x1

    if-eqz p2, :cond_1

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/kc;->a:Lcom/twitter/android/MainActivity;

    sget-object v1, Lcom/twitter/android/MainActivity;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/MainActivity;->a(Landroid/net/Uri;I)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/kc;->a:Lcom/twitter/android/MainActivity;

    const/4 v1, 0x3

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/twitter/android/MainActivity;->a(Lcom/twitter/android/MainActivity;I)V

    iget-object v0, p0, Lcom/twitter/android/kc;->a:Lcom/twitter/android/MainActivity;

    const/4 v1, 0x4

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/twitter/android/MainActivity;->b(Lcom/twitter/android/MainActivity;I)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/kc;->a:Lcom/twitter/android/MainActivity;

    iget-object v1, p0, Lcom/twitter/android/kc;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v1}, Lcom/twitter/android/MainActivity;->a(Lcom/twitter/android/MainActivity;)Lcom/twitter/android/kr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/kr;->b()Lhb;

    move-result-object v1

    iget-object v1, v1, Lhb;->c:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/twitter/android/MainActivity;->a(Lcom/twitter/android/MainActivity;Landroid/net/Uri;)V

    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7

    const/4 v4, 0x0

    new-instance v0, Landroid/support/v4/content/CursorLoader;

    iget-object v1, p0, Lcom/twitter/android/kc;->a:Lcom/twitter/android/MainActivity;

    sget-object v2, Lcom/twitter/library/provider/k;->a:Landroid/net/Uri;

    iget-object v3, p0, Lcom/twitter/android/kc;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v3}, Lcom/twitter/android/MainActivity;->y(Lcom/twitter/android/MainActivity;)Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/library/provider/g;->a:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/kc;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0

    return-void
.end method
