.class public Lcom/twitter/android/at;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/BackupCodeFragment;

.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/String;

.field private final d:Z


# direct methods
.method constructor <init>(Lcom/twitter/android/BackupCodeFragment;Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/at;->a:Lcom/twitter/android/BackupCodeFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/at;->b:Landroid/content/Context;

    iput-object p3, p0, Lcom/twitter/android/at;->c:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/twitter/android/at;->d:Z

    return-void
.end method


# virtual methods
.method public varargs a([Ljava/lang/Void;)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/at;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/at;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/library/api/account/k;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/at;->a:Lcom/twitter/android/BackupCodeFragment;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/twitter/android/BackupCodeFragment;->b(Lcom/twitter/android/BackupCodeFragment;I)V

    iget-object v0, p0, Lcom/twitter/android/at;->a:Lcom/twitter/android/BackupCodeFragment;

    iget-boolean v1, p0, Lcom/twitter/android/at;->d:Z

    invoke-virtual {v0, p1, v1}, Lcom/twitter/android/BackupCodeFragment;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method public synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/at;->a([Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/twitter/android/at;->a(Ljava/lang/String;)V

    return-void
.end method

.method public onPreExecute()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/at;->a:Lcom/twitter/android/BackupCodeFragment;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/twitter/android/BackupCodeFragment;->a(Lcom/twitter/android/BackupCodeFragment;I)V

    return-void
.end method
