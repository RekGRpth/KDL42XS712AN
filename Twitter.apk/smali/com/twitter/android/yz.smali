.class public Lcom/twitter/android/yz;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:I

.field public final b:I

.field public final c:Ljava/util/ArrayList;

.field public final d:J

.field public final e:Lcom/twitter/library/api/PromotedContent;


# direct methods
.method constructor <init>(IJLcom/twitter/library/api/PromotedContent;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/android/yz;->a:I

    iput p1, p0, Lcom/twitter/android/yz;->b:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/yz;->c:Ljava/util/ArrayList;

    iput-wide p2, p0, Lcom/twitter/android/yz;->d:J

    iput-object p4, p0, Lcom/twitter/android/yz;->e:Lcom/twitter/library/api/PromotedContent;

    return-void
.end method

.method constructor <init>(Ljava/util/ArrayList;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/android/yz;->a:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/yz;->b:I

    iput-object p1, p0, Lcom/twitter/android/yz;->c:Ljava/util/ArrayList;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/android/yz;->d:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/yz;->e:Lcom/twitter/library/api/PromotedContent;

    return-void
.end method
