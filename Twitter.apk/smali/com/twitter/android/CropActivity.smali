.class public Lcom/twitter/android/CropActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Lcom/twitter/android/de;


# instance fields
.field protected a:Landroid/net/Uri;

.field protected b:Lcom/twitter/android/CropManager;

.field protected c:Lcom/twitter/library/widget/CroppableImageView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    return-void
.end method

.method private a(ZZ)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/CropActivity;->c:Lcom/twitter/library/widget/CroppableImageView;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/CroppableImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/CropActivity;->c:Lcom/twitter/library/widget/CroppableImageView;

    iget-object v1, p0, Lcom/twitter/android/CropActivity;->b:Lcom/twitter/android/CropManager;

    iget-object v1, v1, Lcom/twitter/android/CropManager;->a:Lcom/twitter/android/dc;

    iget-object v1, v1, Lcom/twitter/android/dc;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/CroppableImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_0
    if-eqz p2, :cond_0

    invoke-virtual {p0, v2}, Lcom/twitter/android/CropActivity;->removeDialog(I)V

    :cond_0
    return-void

    :cond_1
    const v0, 0x7f0f0222    # com.twitter.android.R.string.load_image_failure

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0, v1}, Lcom/twitter/android/CropActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/twitter/android/CropActivity;->finish()V

    goto :goto_0
.end method

.method private f()V
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/CropActivity;->showDialog(I)V

    invoke-virtual {p0}, Lcom/twitter/android/CropActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "enhance"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/android/CropActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "filter_id"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/CropActivity;->b:Lcom/twitter/android/CropManager;

    iget-object v3, p0, Lcom/twitter/android/CropActivity;->a:Landroid/net/Uri;

    invoke-virtual {v2, p0, v3, v1, v0}, Lcom/twitter/android/CropManager;->a(Landroid/content/Context;Landroid/net/Uri;IZ)Z

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a(Z)V

    return-object v0
.end method

.method protected a()V
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/CropActivity;->showDialog(I)V

    iget-object v0, p0, Lcom/twitter/android/CropActivity;->c:Lcom/twitter/library/widget/CroppableImageView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/CroppableImageView;->getImageSelection()Landroid/graphics/RectF;

    move-result-object v0

    new-instance v1, Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    float-to-int v2, v2

    iget v3, v0, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    iget v4, v0, Landroid/graphics/RectF;->right:F

    float-to-int v4, v4

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    invoke-direct {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v0, p0, Lcom/twitter/android/CropActivity;->b:Lcom/twitter/android/CropManager;

    iget-object v2, p0, Lcom/twitter/android/CropActivity;->a:Landroid/net/Uri;

    iget-object v3, p0, Lcom/twitter/android/CropActivity;->c:Lcom/twitter/library/widget/CroppableImageView;

    invoke-virtual {v3}, Lcom/twitter/library/widget/CroppableImageView;->getImageRotation()I

    move-result v3

    invoke-virtual {v0, p0, v2, v1, v3}, Lcom/twitter/android/CropManager;->a(Landroid/content/Context;Landroid/net/Uri;Landroid/graphics/Rect;I)Z

    return-void
.end method

.method public a(Landroid/net/Uri;Landroid/graphics/Rect;I)V
    .locals 4

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    const-string/jumbo v2, "uri"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "cropped_rect"

    invoke-virtual {v2, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, v0, v1, p3}, Lcom/twitter/android/CropActivity;->a(ZLandroid/content/Intent;I)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/CropActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "uri"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/twitter/android/CropActivity;->a:Landroid/net/Uri;

    const v0, 0x7f090077    # com.twitter.android.R.id.image

    invoke-virtual {p0, v0}, Lcom/twitter/android/CropActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/CroppableImageView;

    iput-object v0, p0, Lcom/twitter/android/CropActivity;->c:Lcom/twitter/library/widget/CroppableImageView;

    iget-object v0, p0, Lcom/twitter/android/CropActivity;->c:Lcom/twitter/library/widget/CroppableImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/CroppableImageView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/twitter/android/CropActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/CropManager;->a(Landroid/support/v4/app/FragmentManager;)Lcom/twitter/android/CropManager;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/CropActivity;->b:Lcom/twitter/android/CropManager;

    iget-object v0, p0, Lcom/twitter/android/CropActivity;->b:Lcom/twitter/android/CropManager;

    invoke-virtual {v0, p0}, Lcom/twitter/android/CropManager;->a(Lcom/twitter/android/de;)V

    iget-object v0, p0, Lcom/twitter/android/CropActivity;->b:Lcom/twitter/android/CropManager;

    invoke-virtual {v0}, Lcom/twitter/android/CropManager;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/CropActivity;->b:Lcom/twitter/android/CropManager;

    invoke-virtual {v0}, Lcom/twitter/android/CropManager;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/CropActivity;->b:Lcom/twitter/android/CropManager;

    invoke-virtual {v0}, Lcom/twitter/android/CropManager;->a()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/twitter/android/CropActivity;->f()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/CropActivity;->a(ZZ)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/CropActivity;->a(ZZ)V

    return-void
.end method

.method protected a(ZLandroid/content/Intent;I)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/twitter/android/CropActivity;->removeDialog(I)V

    if-eqz p1, :cond_0

    const/4 v0, -0x1

    invoke-virtual {p0, v0, p2}, Lcom/twitter/android/CropActivity;->setResult(ILandroid/content/Intent;)V

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/CropActivity;->finish()V

    return-void

    :cond_0
    if-ne p3, v2, :cond_1

    const v0, 0x7f0f01ef    # com.twitter.android.R.string.insufficient_disk_space

    :goto_1
    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0, v1}, Lcom/twitter/android/CropActivity;->setResult(I)V

    goto :goto_0

    :cond_1
    const v0, 0x7f0f00e7    # com.twitter.android.R.string.cropping_image_failure

    goto :goto_1
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/CropActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/twitter/android/CropActivity;->finish()V

    return-void
.end method

.method public onClickHandler(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f090119    # com.twitter.android.R.id.save_button

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/CropActivity;->a()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v1, 0x7f090093    # com.twitter.android.R.id.cancel_button

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/CropActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/twitter/android/CropActivity;->finish()V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    invoke-virtual {v0, p0}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    const v1, 0x7f0f00e6    # com.twitter.android.R.string.cropping_image

    invoke-virtual {p0, v1}, Lcom/twitter/android/CropActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    const v1, 0x7f0f0225    # com.twitter.android.R.string.loading_image

    invoke-virtual {p0, v1}, Lcom/twitter/android/CropActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/CropActivity;->b:Lcom/twitter/android/CropManager;

    invoke-virtual {v0, p0}, Lcom/twitter/android/CropManager;->b(Lcom/twitter/android/de;)V

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onDestroy()V

    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
