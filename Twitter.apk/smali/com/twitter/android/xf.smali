.class Lcom/twitter/android/xf;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/TweetFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/TweetFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/xf;->a:Lcom/twitter/android/TweetFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0901d1    # com.twitter.android.R.id.media_display_always

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xf;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->g(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/client/c;->b(ZZ)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/xf;->a:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetDetailView;->h()V

    iget-object v0, p0, Lcom/twitter/android/xf;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0, v3}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/android/TweetFragment;Z)Z

    invoke-virtual {p1, v2}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/twitter/android/xf;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->h(Lcom/twitter/android/TweetFragment;)V

    return-void
.end method
