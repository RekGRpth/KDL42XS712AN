.class public Lcom/twitter/android/InReplyToDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# instance fields
.field private a:Lcom/twitter/android/client/c;

.field private b:Lcom/twitter/android/io;

.field private c:Lcom/twitter/android/in;

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/ArrayList;)Lcom/twitter/android/InReplyToDialogFragment;
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/twitter/android/InReplyToDialogFragment;->a(Ljava/util/ArrayList;Ljava/util/HashSet;Z)Lcom/twitter/android/InReplyToDialogFragment;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/ArrayList;Ljava/util/HashSet;)Lcom/twitter/android/InReplyToDialogFragment;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/twitter/android/InReplyToDialogFragment;->a(Ljava/util/ArrayList;Ljava/util/HashSet;Z)Lcom/twitter/android/InReplyToDialogFragment;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/ArrayList;Ljava/util/HashSet;Z)Lcom/twitter/android/InReplyToDialogFragment;
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "all_users"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string/jumbo v1, "selected_users"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string/jumbo v1, "is_read_only"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    new-instance v1, Lcom/twitter/android/InReplyToDialogFragment;

    invoke-direct {v1}, Lcom/twitter/android/InReplyToDialogFragment;-><init>()V

    invoke-virtual {v1, v0}, Lcom/twitter/android/InReplyToDialogFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method

.method static synthetic a(Lcom/twitter/android/InReplyToDialogFragment;)Lcom/twitter/android/in;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/InReplyToDialogFragment;->c:Lcom/twitter/android/in;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/InReplyToDialogFragment;)Lcom/twitter/android/io;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/InReplyToDialogFragment;->b:Lcom/twitter/android/io;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/support/v4/content/Loader;Ljava/util/List;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/InReplyToDialogFragment;->b:Lcom/twitter/android/io;

    invoke-virtual {v0, p2}, Lcom/twitter/android/io;->a(Ljava/util/List;)V

    return-void
.end method

.method public a(Lcom/twitter/android/in;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/InReplyToDialogFragment;->c:Lcom/twitter/android/in;

    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/InReplyToDialogFragment;->c:Lcom/twitter/android/in;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/InReplyToDialogFragment;->c:Lcom/twitter/android/in;

    invoke-interface {v0}, Lcom/twitter/android/in;->a()V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/InReplyToDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "No arguments provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/InReplyToDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/InReplyToDialogFragment;->a:Lcom/twitter/android/client/c;

    const-string/jumbo v0, "is_read_only"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/InReplyToDialogFragment;->d:Z

    if-eqz p1, :cond_1

    const-string/jumbo v0, "selected_users"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    move-object v3, v0

    :goto_0
    const-string/jumbo v0, "all_users"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    new-instance v0, Lcom/twitter/android/io;

    invoke-virtual {p0}, Lcom/twitter/android/InReplyToDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-boolean v4, p0, Lcom/twitter/android/InReplyToDialogFragment;->d:Z

    iget-object v5, p0, Lcom/twitter/android/InReplyToDialogFragment;->c:Lcom/twitter/android/in;

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/io;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/HashSet;ZLcom/twitter/android/in;)V

    iput-object v0, p0, Lcom/twitter/android/InReplyToDialogFragment;->b:Lcom/twitter/android/io;

    iget-object v0, p0, Lcom/twitter/android/InReplyToDialogFragment;->a:Lcom/twitter/android/client/c;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/twitter/android/InReplyToDialogFragment;->b:Lcom/twitter/android/io;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->a(ILcom/twitter/library/util/ar;)V

    invoke-virtual {p0}, Lcom/twitter/android/InReplyToDialogFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void

    :cond_1
    const-string/jumbo v0, "selected_users"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    move-object v3, v0

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/android/InReplyToDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03009c    # com.twitter.android.R.layout.in_reply_to_list_view

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0901ab    # com.twitter.android.R.id.in_reply_to_list_view

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iget-object v2, p0, Lcom/twitter/android/InReplyToDialogFragment;->b:Lcom/twitter/android/io;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/twitter/android/InReplyToDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/twitter/android/InReplyToDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f01e7    # com.twitter.android.R.string.in_reply_to_text

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f01e6    # com.twitter.android.R.string.in_reply_to_done

    new-instance v2, Lcom/twitter/android/im;

    invoke-direct {v2, p0}, Lcom/twitter/android/im;-><init>(Lcom/twitter/android/InReplyToDialogFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/InReplyToDialogFragment;->b:Lcom/twitter/android/io;

    invoke-virtual {v0}, Lcom/twitter/android/io;->getCount()I

    move-result v2

    new-array v3, v2, [J

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/InReplyToDialogFragment;->b:Lcom/twitter/android/io;

    invoke-virtual {v0, v1}, Lcom/twitter/android/io;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/RepliedUser;

    iget-wide v4, v0, Lcom/twitter/library/api/RepliedUser;->userId:J

    aput-wide v4, v3, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/twitter/android/yl;

    invoke-virtual {p0}, Lcom/twitter/android/InReplyToDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lcom/twitter/android/yl;-><init>(Landroid/content/Context;[J)V

    return-object v0
.end method

.method public onDestroy()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/InReplyToDialogFragment;->a:Lcom/twitter/android/client/c;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/twitter/android/InReplyToDialogFragment;->b:Lcom/twitter/android/io;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->b(ILcom/twitter/library/util/ar;)V

    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onDestroy()V

    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Ljava/util/List;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/InReplyToDialogFragment;->a(Landroid/support/v4/content/Loader;Ljava/util/List;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "selected_users"

    iget-object v1, p0, Lcom/twitter/android/InReplyToDialogFragment;->b:Lcom/twitter/android/io;

    invoke-virtual {v1}, Lcom/twitter/android/io;->a()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    return-void
.end method
