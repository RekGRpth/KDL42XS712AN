.class public Lcom/twitter/android/BaseMessagesActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/AttachMediaListener;
.implements Lcom/twitter/android/mk;
.implements Lcom/twitter/android/widget/ce;


# instance fields
.field public a:Ljava/lang/String;

.field protected b:Ljava/lang/String;

.field protected c:Ljava/lang/String;

.field protected d:J

.field protected e:Ljava/lang/String;

.field private f:Lcom/twitter/android/MessagesThreadFragment;

.field private g:Lcom/twitter/android/MessagesComposeFragment;

.field private h:Lcom/twitter/android/TweetBoxFragment;

.field private i:Landroid/widget/Button;

.field private j:Landroid/widget/TextView;

.field private k:Z

.field private l:Landroid/os/Handler;

.field private m:Ljava/lang/Runnable;

.field private n:Landroid/widget/ImageView;

.field private o:Lcom/twitter/android/PhotoSelectHelper;

.field private p:Lcom/twitter/android/PostStorage;

.field private q:Lcom/twitter/android/PostStorage$MediaItem;

.field private r:Landroid/widget/RelativeLayout;

.field private s:Landroid/widget/ImageView;

.field private t:Landroid/widget/ImageView;

.field private u:Landroid/widget/ProgressBar;

.field private v:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/BaseMessagesActivity;->v:Z

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/BaseMessagesActivity;)Lcom/twitter/android/PostStorage;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->p:Lcom/twitter/android/PostStorage;

    return-object v0
.end method

.method private a(J)V
    .locals 4

    new-instance v0, Lcom/twitter/android/bf;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/android/bf;-><init>(Lcom/twitter/android/BaseMessagesActivity;J)V

    iput-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->m:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->l:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/android/BaseMessagesActivity;->m:Ljava/lang/Runnable;

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private a(JI)V
    .locals 4

    iget-boolean v0, p0, Lcom/twitter/android/BaseMessagesActivity;->v:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/android/BaseMessagesActivity;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    if-ne p3, v0, :cond_1

    :cond_0
    const/16 v0, 0x401

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f010c    # com.twitter.android.R.string.direct_message_error_title

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0319    # com.twitter.android.R.string.post_retry_direct_messsage_question

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0362    # com.twitter.android.R.string.retry

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0089    # com.twitter.android.R.string.cancel

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/bg;

    invoke-direct {v1, p0, p1, p2}, Lcom/twitter/android/bg;-><init>(Lcom/twitter/android/BaseMessagesActivity;J)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/android/widget/ce;)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/BaseMessagesActivity;->v:Z

    :cond_1
    return-void
.end method

.method private a(JZ)V
    .locals 3

    new-instance v0, Liv;

    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Liv;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {v0, p1, p2, p3}, Liv;->a(JZ)V

    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    move-result-object v1

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Lcom/twitter/library/service/b;->c(I)Lcom/twitter/library/service/b;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/BaseMessagesActivity;->a(Lcom/twitter/library/service/b;II)Z

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/BaseMessagesActivity;JZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/BaseMessagesActivity;->a(JZ)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/BaseMessagesActivity;Ljava/lang/String;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/BaseMessagesActivity;->e(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/BaseMessagesActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/BaseMessagesActivity;->k:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/android/BaseMessagesActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/BaseMessagesActivity;->h()V

    return-void
.end method

.method static synthetic c(Lcom/twitter/android/BaseMessagesActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->j:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/BaseMessagesActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/BaseMessagesActivity;->k:Z

    return v0
.end method

.method static synthetic e(Lcom/twitter/android/BaseMessagesActivity;)Z
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/BaseMessagesActivity;->k()Z

    move-result v0

    return v0
.end method

.method static synthetic f(Lcom/twitter/android/BaseMessagesActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->i:Landroid/widget/Button;

    return-object v0
.end method

.method private f()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->h:Lcom/twitter/android/TweetBoxFragment;

    const v1, 0x7f0f0314    # com.twitter.android.R.string.post_button_send

    invoke-virtual {p0, v1}, Lcom/twitter/android/BaseMessagesActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetBoxFragment;->b(Ljava/lang/CharSequence;)V

    const v1, 0x7f0f031a    # com.twitter.android.R.string.post_title_direct_message

    invoke-virtual {p0, v1}, Lcom/twitter/android/BaseMessagesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetBoxFragment;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic g(Lcom/twitter/android/BaseMessagesActivity;)Lcom/twitter/android/MessagesComposeFragment;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->g:Lcom/twitter/android/MessagesComposeFragment;

    return-object v0
.end method

.method private g()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->h:Lcom/twitter/android/TweetBoxFragment;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetBoxFragment;->a(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/twitter/android/BaseMessagesActivity;->h()V

    return-void
.end method

.method static synthetic h(Lcom/twitter/android/BaseMessagesActivity;)Lcom/twitter/android/MessagesThreadFragment;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->f:Lcom/twitter/android/MessagesThreadFragment;

    return-object v0
.end method

.method private h()V
    .locals 3

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->K()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->s:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->n:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->n:Landroid/widget/ImageView;

    const v1, 0x7f020168    # com.twitter.android.R.drawable.ic_dialog_camera

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->r:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->h:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->q()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->r:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic i(Lcom/twitter/android/BaseMessagesActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/BaseMessagesActivity;)Landroid/widget/RelativeLayout;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->r:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/android/BaseMessagesActivity;)V
    .locals 0

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->j()V

    return-void
.end method

.method private k()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private l()Z
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->f:Lcom/twitter/android/MessagesThreadFragment;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/android/BaseMessagesActivity;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;
    .locals 7

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/BaseMessagesActivity;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    iput-object v3, p0, Lcom/twitter/android/BaseMessagesActivity;->e:Ljava/lang/String;

    :cond_0
    iget-object v4, p0, Lcom/twitter/android/BaseMessagesActivity;->e:Ljava/lang/String;

    const v1, 0x7f0901ed    # com.twitter.android.R.id.messages_composer_container

    invoke-virtual {p0, v1}, Lcom/twitter/android/BaseMessagesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    if-eqz v5, :cond_1

    const-string/jumbo v1, "mempty"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v5, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/BaseMessagesActivity;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/BaseMessagesActivity;->e:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v5

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v5, v0}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_5

    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_4
    move v2, v1

    :goto_1
    packed-switch v2, :pswitch_data_0

    move-object v1, v3

    :goto_2
    if-eqz v1, :cond_5

    if-eqz p2, :cond_5

    invoke-virtual {v1, p2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    :cond_5
    if-eqz v1, :cond_7

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v2

    if-nez v2, :cond_6

    const v2, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    invoke-virtual {v5, v2, v1, p1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    :cond_6
    iput-object p1, p0, Lcom/twitter/android/BaseMessagesActivity;->e:Ljava/lang/String;

    :cond_7
    invoke-virtual {v5}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :goto_3
    const-string/jumbo v2, "mselect"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    move-object v0, v1

    check-cast v0, Lcom/twitter/android/MessagesComposeFragment;

    invoke-virtual {v0, p0}, Lcom/twitter/android/MessagesComposeFragment;->a(Lcom/twitter/android/mk;)V

    :cond_8
    :goto_4
    return-object v1

    :cond_9
    move v1, v2

    goto :goto_0

    :sswitch_0
    const-string/jumbo v6, "mempty"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    goto :goto_1

    :sswitch_1
    const-string/jumbo v2, "mthread"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    goto :goto_1

    :sswitch_2
    const-string/jumbo v2, "mselect"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x2

    goto :goto_1

    :pswitch_0
    new-instance v1, Lcom/twitter/android/MessagesEmptyFragment;

    invoke-direct {v1}, Lcom/twitter/android/MessagesEmptyFragment;-><init>()V

    goto :goto_2

    :pswitch_1
    new-instance v1, Lcom/twitter/android/MessagesThreadFragment;

    invoke-direct {v1}, Lcom/twitter/android/MessagesThreadFragment;-><init>()V

    goto :goto_2

    :pswitch_2
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/twitter/android/BaseMessagesActivity;->d:J

    new-instance v1, Lcom/twitter/android/MessagesComposeFragment;

    invoke-direct {v1}, Lcom/twitter/android/MessagesComposeFragment;-><init>()V

    goto :goto_2

    :cond_a
    move-object v1, v0

    goto :goto_3

    :cond_b
    const-string/jumbo v2, "mselect"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    check-cast v0, Lcom/twitter/android/MessagesComposeFragment;

    invoke-virtual {v0, v3}, Lcom/twitter/android/MessagesComposeFragment;->a(Lcom/twitter/android/mk;)V

    goto :goto_4

    :sswitch_data_0
    .sparse-switch
        -0x403d3f00 -> :sswitch_0
        0x50066fa9 -> :sswitch_2
        0x51e848b7 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const v1, 0x7f0300c9    # com.twitter.android.R.layout.messages_thread

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a(Z)V

    return-object v0
.end method

.method a()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->f:Lcom/twitter/android/MessagesThreadFragment;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v7, [Ljava/lang/String;

    const-string/jumbo v5, "messages:thread:::send_dm"

    aput-object v5, v4, v6

    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->h:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->j()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->p:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage;->d()Lcom/twitter/android/PostStorage$MediaItem;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage$MediaItem;->b()Lcom/twitter/library/api/MediaEntity;

    move-result-object v0

    :goto_1
    new-instance v3, Liv;

    invoke-direct {v3, p0, v1}, Liv;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    move-result-object v4

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Lcom/twitter/library/service/b;->c(I)Lcom/twitter/library/service/b;

    move-result-object v4

    const-string/jumbo v5, "content"

    invoke-virtual {v4, v5, v2}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v2

    const-string/jumbo v4, "media"

    invoke-virtual {v2, v4, v0}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Ljava/io/Serializable;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v2, "recipient_id"

    iget-wide v4, p0, Lcom/twitter/android/BaseMessagesActivity;->d:J

    invoke-virtual {v0, v2, v4, v5}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v2, "recipient_username"

    iget-object v4, p0, Lcom/twitter/android/BaseMessagesActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v4}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v2, "sender_id"

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-virtual {v0, v2, v4, v5}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->p:Lcom/twitter/android/PostStorage;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->p:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage;->h()V

    new-instance v0, Lcom/twitter/android/PostStorage;

    invoke-direct {v0}, Lcom/twitter/android/PostStorage;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->p:Lcom/twitter/android/PostStorage;

    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/BaseMessagesActivity;->g()V

    invoke-virtual {p0, v3, v7, v6}, Lcom/twitter/android/BaseMessagesActivity;->a(Lcom/twitter/library/service/b;II)Z

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v7, [Ljava/lang/String;

    const-string/jumbo v5, "messages:compose:::send_dm"

    aput-object v5, v4, v6

    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(IILcom/twitter/library/service/b;)V
    .locals 8

    const v7, 0x7f0f0115    # com.twitter.android.R.string.dm_blocked_generic

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/client/BaseFragmentActivity;->a(IILcom/twitter/library/service/b;)V

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    if-ne p1, v5, :cond_4

    :cond_0
    iget-object v1, p3, Lcom/twitter/library/service/b;->k:Landroid/os/Bundle;

    invoke-virtual {p3}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v2

    if-eqz v2, :cond_5

    const-string/jumbo v0, "user"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    if-eqz v0, :cond_1

    iget-wide v1, v0, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-eqz v1, :cond_1

    iget v1, v0, Lcom/twitter/library/api/TwitterUser;->friendship:I

    invoke-static {v1}, Lcom/twitter/library/provider/ay;->b(I)Z

    move-result v1

    if-nez v1, :cond_1

    const/16 v1, 0x402

    invoke-static {v1}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f019f    # com.twitter.android.R.string.follow

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f0104    # com.twitter.android.R.string.dialog_not_following_message

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, v0, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {p0, v2, v3}, Lcom/twitter/android/BaseMessagesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->b(Ljava/lang/String;)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f0105    # com.twitter.android.R.string.dialog_not_following_positive

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f0057    # com.twitter.android.R.string.button_action_dismiss

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/bh;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/bh;-><init>(Lcom/twitter/android/BaseMessagesActivity;Lcom/twitter/library/api/TwitterUser;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/android/widget/ce;)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    :cond_1
    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/twitter/android/BaseMessagesActivity;->l()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-wide v1, v0, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, v0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    iget-object v3, v0, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    iget-object v4, v0, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/BaseMessagesActivity;->a(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->f:Lcom/twitter/android/MessagesThreadFragment;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->f:Lcom/twitter/android/MessagesThreadFragment;

    invoke-virtual {v0}, Lcom/twitter/android/MessagesThreadFragment;->V()Landroid/widget/ListView;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/CursorAdapter;

    if-ne p1, v5, :cond_3

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->requery()Z

    invoke-virtual {v1}, Landroid/widget/ListView;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setSelection(I)V

    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->m:Ljava/lang/Runnable;

    :cond_4
    :goto_1
    return-void

    :cond_5
    const-string/jumbo v2, "custom_errors"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v2

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->c()I

    move-result v0

    const-string/jumbo v3, "draft_id"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    packed-switch v0, :pswitch_data_0

    invoke-direct {p0, v3, v4, p1}, Lcom/twitter/android/BaseMessagesActivity;->a(JI)V

    goto :goto_0

    :pswitch_0
    const/16 v0, 0x96

    invoke-static {v2, v0}, Lcom/twitter/library/util/Util;->a([II)Z

    move-result v0

    if-eqz v0, :cond_6

    const v0, 0x7f0f011b    # com.twitter.android.R.string.dm_error_not_follower

    invoke-static {p0, v0, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_6
    const/16 v0, 0xfc

    invoke-static {v2, v0}, Lcom/twitter/library/util/Util;->a([II)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->m:Ljava/lang/Runnable;

    if-nez v0, :cond_7

    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->M()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-direct {p0, v3, v4}, Lcom/twitter/android/BaseMessagesActivity;->a(J)V

    goto :goto_1

    :cond_7
    invoke-static {p0, v7, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_8
    const/16 v0, 0x97

    invoke-static {v2, v0}, Lcom/twitter/library/util/Util;->a([II)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-static {p0, v7, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_9
    const v0, 0x7f0f0119    # com.twitter.android.R.string.dm_error_generic

    invoke-static {p0, v0, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :pswitch_1
    const v0, 0x7f0f011a    # com.twitter.android.R.string.dm_error_non_existing

    invoke-static {p0, v0, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x193
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 1

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-nez p3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->o:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0}, Lcom/twitter/android/PhotoSelectHelper;->c()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->o:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0}, Lcom/twitter/android/PhotoSelectHelper;->d()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x403
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/net/Uri;Lcom/twitter/android/AttachMediaListener$MediaAttachProgress;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->r:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->u:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->s:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method public a(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/BaseMessagesActivity;->v:Z

    const v0, 0x7f090111    # com.twitter.android.R.id.photo_preview

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseMessagesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->s:Landroid/widget/ImageView;

    const v0, 0x7f090110    # com.twitter.android.R.id.photo_container

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseMessagesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->r:Landroid/widget/RelativeLayout;

    const v0, 0x7f090112    # com.twitter.android.R.id.photo_dismiss

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseMessagesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->t:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->t:Landroid/widget/ImageView;

    new-instance v2, Lcom/twitter/android/ba;

    invoke-direct {v2, p0}, Lcom/twitter/android/ba;-><init>(Lcom/twitter/android/BaseMessagesActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0901e9    # com.twitter.android.R.id.photo_progress

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseMessagesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->u:Landroid/widget/ProgressBar;

    const v0, 0x7f09010f    # com.twitter.android.R.id.photo_compose

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseMessagesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->n:Landroid/widget/ImageView;

    invoke-virtual {v1}, Lcom/twitter/android/client/c;->K()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->n:Landroid/widget/ImageView;

    new-instance v1, Lcom/twitter/android/bb;

    invoke-direct {v1, p0}, Lcom/twitter/android/bb;-><init>(Lcom/twitter/android/BaseMessagesActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    const v0, 0x7f090113    # com.twitter.android.R.id.tweet_button

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseMessagesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->i:Landroid/widget/Button;

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->i:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->i:Landroid/widget/Button;

    const v1, 0x7f0f0314    # com.twitter.android.R.string.post_button_send

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->i:Landroid/widget/Button;

    new-instance v1, Lcom/twitter/android/bc;

    invoke-direct {v1, p0}, Lcom/twitter/android/bc;-><init>(Lcom/twitter/android/BaseMessagesActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/twitter/android/PhotoSelectHelper;

    const-string/jumbo v1, "dm_composition"

    sget-object v2, Lcom/twitter/android/PhotoSelectHelper$MediaType;->c:Ljava/util/EnumSet;

    invoke-direct {v0, p0, p0, v1, v2}, Lcom/twitter/android/PhotoSelectHelper;-><init>(Landroid/app/Activity;Lcom/twitter/android/AttachMediaListener;Ljava/lang/String;Ljava/util/EnumSet;)V

    iput-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->o:Lcom/twitter/android/PhotoSelectHelper;

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->o:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/PhotoSelectHelper;->a(Lcom/twitter/library/client/Session;)V

    const v0, 0x7f0900f4    # com.twitter.android.R.id.count

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseMessagesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->j:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->j:Landroid/widget/TextView;

    const/16 v1, 0x8c

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v1, "tweet_box"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/TweetBoxFragment;

    if-nez v0, :cond_0

    const v0, 0x7f03015c    # com.twitter.android.R.layout.tweet_box

    invoke-static {v0, v3}, Lcom/twitter/android/TweetBoxFragment;->a(IZ)Lcom/twitter/android/TweetBoxFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0900f7    # com.twitter.android.R.id.tweet_box

    const-string/jumbo v3, "tweet_box"

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_0
    new-instance v1, Lcom/twitter/android/bd;

    invoke-direct {v1, p0}, Lcom/twitter/android/bd;-><init>(Lcom/twitter/android/BaseMessagesActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetBoxFragment;->a(Lcom/twitter/android/wz;)V

    iput-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->h:Lcom/twitter/android/TweetBoxFragment;

    invoke-direct {p0}, Lcom/twitter/android/BaseMessagesActivity;->f()V

    const-string/jumbo v0, "android.intent.extra.TEXT"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->h:Lcom/twitter/android/TweetBoxFragment;

    const-string/jumbo v1, "android.intent.extra.TEXT"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/TweetBoxFragment;->a(Ljava/lang/String;[I)V

    :cond_1
    if-nez p2, :cond_3

    new-instance v0, Lcom/twitter/android/PostStorage;

    invoke-direct {v0}, Lcom/twitter/android/PostStorage;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->p:Lcom/twitter/android/PostStorage;

    :goto_1
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->l:Landroid/os/Handler;

    new-instance v0, Lcom/twitter/android/bi;

    invoke-direct {v0, p0}, Lcom/twitter/android/bi;-><init>(Lcom/twitter/android/BaseMessagesActivity;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseMessagesActivity;->a(Lcom/twitter/library/client/j;)V

    return-void

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->n:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->o:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0, p2}, Lcom/twitter/android/PhotoSelectHelper;->a(Landroid/os/Bundle;)V

    const-string/jumbo v0, "data"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/PostStorage;

    iput-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->p:Lcom/twitter/android/PostStorage;

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->p:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage;->d()Lcom/twitter/android/PostStorage$MediaItem;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->q:Lcom/twitter/android/PostStorage$MediaItem;

    const-string/jumbo v0, "error_dialog"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/BaseMessagesActivity;->v:Z

    goto :goto_1
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 5

    const-wide/16 v3, 0x0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V

    if-eqz p1, :cond_0

    const-string/jumbo v0, "detail_pane_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->e:Ljava/lang/String;

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_2

    const-string/jumbo v1, "via_notification"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "notification::::open"

    const-string/jumbo v2, "ref_event"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->b(Z)Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->k()Ljava/lang/String;

    :cond_2
    return-void
.end method

.method public a(Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;Lcom/twitter/android/PostStorage$MediaItem;)V
    .locals 2

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->p:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage;->e()V

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->r:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->u:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method

.method public a(Lcom/twitter/android/PostStorage$MediaItem;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->u:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->s:Landroid/widget/ImageView;

    iget-object v1, p1, Lcom/twitter/android/PostStorage$MediaItem;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->s:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->r:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->n:Landroid/widget/ImageView;

    const v1, 0x7f020169    # com.twitter.android.R.drawable.ic_dialog_camera_active

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->p:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0, p1}, Lcom/twitter/android/PostStorage;->a(Lcom/twitter/android/PostStorage$MediaItem;)V

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->h:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->q()V

    return-void
.end method

.method public a(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/BaseMessagesActivity;->a(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->h:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->b()V

    goto :goto_0
.end method

.method public a(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 9

    const-wide/16 v7, 0x0

    const/4 v6, 0x0

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/twitter/android/BaseMessagesActivity;->d:J

    cmp-long v0, v2, v7

    if-eqz v0, :cond_0

    iget-wide v2, p0, Lcom/twitter/android/BaseMessagesActivity;->d:J

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "user_id"

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v1, "user_name"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "user_fullname"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "user_profile_image"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x3

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/BaseMessagesActivity;->showDialog(ILandroid/os/Bundle;)Z

    :goto_0
    return-void

    :cond_0
    iput-object p3, p0, Lcom/twitter/android/BaseMessagesActivity;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/twitter/android/BaseMessagesActivity;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/twitter/android/BaseMessagesActivity;->c:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/android/BaseMessagesActivity;->d:J

    invoke-direct {p0}, Lcom/twitter/android/BaseMessagesActivity;->g()V

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->i:Landroid/widget/Button;

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/twitter/android/BaseMessagesActivity;->i:Landroid/widget/Button;

    iget-boolean v0, p0, Lcom/twitter/android/BaseMessagesActivity;->k:Z

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/twitter/android/BaseMessagesActivity;->k()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_1
    iget-wide v2, p0, Lcom/twitter/android/BaseMessagesActivity;->d:J

    cmp-long v0, v2, v7

    if-lez v0, :cond_4

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v2, "user_id"

    iget-wide v3, p0, Lcom/twitter/android/BaseMessagesActivity;->d:J

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v2, "user_fullname"

    iget-object v3, p0, Lcom/twitter/android/BaseMessagesActivity;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "mthread"

    invoke-virtual {p0, v2, v0}, Lcom/twitter/android/BaseMessagesActivity;->a(Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/MessagesThreadFragment;

    iput-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->f:Lcom/twitter/android/MessagesThreadFragment;

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->f:Lcom/twitter/android/MessagesThreadFragment;

    iget-wide v2, p0, Lcom/twitter/android/BaseMessagesActivity;->d:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/MessagesThreadFragment;->b_(J)V

    if-eqz p5, :cond_2

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->h:Lcom/twitter/android/TweetBoxFragment;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->h:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetBoxFragment;->c(Z)V

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->h:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "android.intent.extra.TEXT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Lcom/twitter/android/TweetBoxFragment;->a(Ljava/lang/String;[I)V

    :cond_2
    :goto_2
    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->e()V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    const-string/jumbo v0, "mselect"

    invoke-virtual {p0, v0, v6}, Lcom/twitter/android/BaseMessagesActivity;->a(Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/MessagesComposeFragment;

    iput-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->g:Lcom/twitter/android/MessagesComposeFragment;

    goto :goto_2
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/BaseMessagesActivity;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->i_()V

    return-void
.end method

.method public a(Landroid/net/Uri;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->g:Lcom/twitter/android/MessagesComposeFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->g:Lcom/twitter/android/MessagesComposeFragment;

    iget-object v0, v0, Lcom/twitter/android/MessagesComposeFragment;->a:Landroid/widget/AutoCompleteTextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->g:Lcom/twitter/android/MessagesComposeFragment;

    iget-object v0, v0, Lcom/twitter/android/MessagesComposeFragment;->a:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->h:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->e()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->p:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected e()V
    .locals 2

    const-string/jumbo v0, "mselect"

    iget-object v1, p0, Lcom/twitter/android/BaseMessagesActivity;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const v0, 0x7f0f0297    # com.twitter.android.R.string.new_message

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseMessagesActivity;->setTitle(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseMessagesActivity;->a(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/BaseMessagesActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/BaseMessagesActivity;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/BaseMessagesActivity;->b:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/twitter/android/BaseMessagesActivity;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseMessagesActivity;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseMessagesActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public i_()V
    .locals 2

    iget-object v1, p0, Lcom/twitter/android/BaseMessagesActivity;->i:Landroid/widget/Button;

    iget-boolean v0, p0, Lcom/twitter/android/BaseMessagesActivity;->k:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/BaseMessagesActivity;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected j()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseMessagesActivity;->showDialog(I)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->j()V

    goto :goto_0
.end method

.method public j_()V
    .locals 6

    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/BaseMessagesActivity;->a(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->o:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    move v1, p1

    move v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/PhotoSelectHelper;->a(IILandroid/content/Intent;J)V

    return-void
.end method

.method public onBackPressed()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseMessagesActivity;->showDialog(I)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/BaseMessagesActivity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    packed-switch p1, :pswitch_data_0

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->o:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0, p1}, Lcom/twitter/android/PhotoSelectHelper;->a(I)Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Lcom/twitter/android/be;

    invoke-direct {v0, p0, p2, p1}, Lcom/twitter/android/be;-><init>(Lcom/twitter/android/BaseMessagesActivity;Landroid/os/Bundle;I)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0f0297    # com.twitter.android.R.string.new_message

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x7f0f0000    # com.twitter.android.R.string.abandon_changes_question

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f0111    # com.twitter.android.R.string.discard

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f0089    # com.twitter.android.R.string.cancel

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onDestroy()V

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->p:Lcom/twitter/android/PostStorage;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->p:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage;->h()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->o:Lcom/twitter/android/PhotoSelectHelper;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->o:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0}, Lcom/twitter/android/PhotoSelectHelper;->a()V

    :cond_1
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onPostCreate(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->h:Lcom/twitter/android/TweetBoxFragment;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->h:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->b()V

    :cond_2
    if-nez p1, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    :cond_3
    if-eqz p1, :cond_0

    const-string/jumbo v0, "keyboard_open"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->h:Lcom/twitter/android/TweetBoxFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetBoxFragment;->c(Z)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onResume()V

    invoke-static {p0}, Lcom/twitter/android/client/aw;->a(Landroid/content/Context;)Lcom/twitter/android/client/aw;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/BaseMessagesActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/aw;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->q:Lcom/twitter/android/PostStorage$MediaItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->o:Lcom/twitter/android/PhotoSelectHelper;

    iget-object v1, p0, Lcom/twitter/android/BaseMessagesActivity;->q:Lcom/twitter/android/PostStorage$MediaItem;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/PhotoSelectHelper;->a(Lcom/twitter/android/PostStorage$MediaItem;Z)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->q:Lcom/twitter/android/PostStorage$MediaItem;

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->h:Lcom/twitter/android/TweetBoxFragment;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v0, "user_id"

    iget-wide v1, p0, Lcom/twitter/android/BaseMessagesActivity;->d:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v0, "user_fullname"

    iget-object v1, p0, Lcom/twitter/android/BaseMessagesActivity;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "user_profile_image"

    iget-object v1, p0, Lcom/twitter/android/BaseMessagesActivity;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "user_name"

    iget-object v1, p0, Lcom/twitter/android/BaseMessagesActivity;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "android.intent.extra.TEXT"

    iget-object v1, p0, Lcom/twitter/android/BaseMessagesActivity;->h:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v1}, Lcom/twitter/android/TweetBoxFragment;->j()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "data"

    iget-object v1, p0, Lcom/twitter/android/BaseMessagesActivity;->p:Lcom/twitter/android/PostStorage;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v0, "error_dialog"

    iget-boolean v1, p0, Lcom/twitter/android/BaseMessagesActivity;->v:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "detail_pane_state"

    iget-object v1, p0, Lcom/twitter/android/BaseMessagesActivity;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/BaseMessagesActivity;->o:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0, p1}, Lcom/twitter/android/PhotoSelectHelper;->b(Landroid/os/Bundle;)V

    goto :goto_0
.end method
