.class Lcom/twitter/android/yg;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/client/k;


# instance fields
.field a:J

.field final b:Landroid/os/Handler;

.field c:Z

.field d:J

.field e:Ljava/lang/Runnable;

.field final synthetic f:Lcom/twitter/library/client/aa;

.field final synthetic g:Lcom/twitter/android/client/c;

.field final synthetic h:Lcom/twitter/android/TwitterApplication;


# direct methods
.method constructor <init>(Lcom/twitter/android/TwitterApplication;Lcom/twitter/library/client/aa;Lcom/twitter/android/client/c;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/yg;->h:Lcom/twitter/android/TwitterApplication;

    iput-object p2, p0, Lcom/twitter/android/yg;->f:Lcom/twitter/library/client/aa;

    iput-object p3, p0, Lcom/twitter/android/yg;->g:Lcom/twitter/android/client/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/yg;->b:Landroid/os/Handler;

    new-instance v0, Lcom/twitter/android/yh;

    invoke-direct {v0, p0}, Lcom/twitter/android/yh;-><init>(Lcom/twitter/android/yg;)V

    iput-object v0, p0, Lcom/twitter/android/yg;->e:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/yg;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/android/yg;->e:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/yg;->c:Z

    iget-object v0, p0, Lcom/twitter/android/yg;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/android/yg;->e:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public b()V
    .locals 7

    const/4 v6, 0x0

    iget-boolean v0, p0, Lcom/twitter/android/yg;->c:Z

    if-eqz v0, :cond_1

    iput-boolean v6, p0, Lcom/twitter/android/yg;->c:Z

    iget-object v0, p0, Lcom/twitter/android/yg;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/android/yg;->e:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/twitter/android/yg;->d:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x7d0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    iput-wide v0, p0, Lcom/twitter/android/yg;->d:J

    new-instance v0, Lcom/twitter/android/yi;

    invoke-direct {v0, p0}, Lcom/twitter/android/yi;-><init>(Lcom/twitter/android/yg;)V

    new-array v1, v6, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/yi;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    return-void

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/yg;->a:J

    goto :goto_0
.end method
