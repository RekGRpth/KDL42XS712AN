.class public Lcom/twitter/android/CardDebugDetailActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const v1, 0x7f03001d    # com.twitter.android.R.layout.card_debug_detail_activity

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/library/client/e;)V
    .locals 3

    const v0, 0x7f0f00fd    # com.twitter.android.R.string.developer_card_previewer_title

    invoke-virtual {p0, v0}, Lcom/twitter/android/CardDebugDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/CardDebugDetailActivity;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/twitter/android/CardDebugDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const v0, 0x7f0900b3    # com.twitter.android.R.id.entry_detail

    invoke-virtual {p0, v0}, Lcom/twitter/android/CardDebugDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "detail_text"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
