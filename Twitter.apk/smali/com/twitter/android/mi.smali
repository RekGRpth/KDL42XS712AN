.class Lcom/twitter/android/mi;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/client/c;

.field final synthetic b:Landroid/widget/ListView;

.field final synthetic c:Lcom/twitter/android/MessagesComposeFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/MessagesComposeFragment;Lcom/twitter/android/client/c;Landroid/widget/ListView;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/mi;->c:Lcom/twitter/android/MessagesComposeFragment;

    iput-object p2, p0, Lcom/twitter/android/mi;->a:Lcom/twitter/android/client/c;

    iput-object p3, p0, Lcom/twitter/android/mi;->b:Landroid/widget/ListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/mi;->a:Lcom/twitter/android/client/c;

    iget-object v2, p0, Lcom/twitter/android/mi;->c:Lcom/twitter/android/MessagesComposeFragment;

    invoke-static {v2}, Lcom/twitter/android/MessagesComposeFragment;->b(Lcom/twitter/android/MessagesComposeFragment;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v6, [Ljava/lang/String;

    const-string/jumbo v5, "messages:compose:typeahead:query:select"

    aput-object v5, v4, v7

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/mi;->b:Landroid/widget/ListView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setVisibility(I)V

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/mi;->c:Lcom/twitter/android/MessagesComposeFragment;

    invoke-static {v1}, Lcom/twitter/android/MessagesComposeFragment;->a(Lcom/twitter/android/MessagesComposeFragment;)Lcom/twitter/android/mk;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/mi;->c:Lcom/twitter/android/MessagesComposeFragment;

    invoke-static {v1}, Lcom/twitter/android/MessagesComposeFragment;->a(Lcom/twitter/android/MessagesComposeFragment;)Lcom/twitter/android/mk;

    move-result-object v1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "@"

    const-string/jumbo v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x4

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/twitter/android/mk;->a(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/mi;->a:Lcom/twitter/android/client/c;

    iget-object v2, p0, Lcom/twitter/android/mi;->c:Lcom/twitter/android/MessagesComposeFragment;

    invoke-static {v2}, Lcom/twitter/android/MessagesComposeFragment;->c(Lcom/twitter/android/MessagesComposeFragment;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v6, [Ljava/lang/String;

    const-string/jumbo v5, "messages:compose:typeahead:user:select"

    aput-object v5, v4, v7

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0
.end method
