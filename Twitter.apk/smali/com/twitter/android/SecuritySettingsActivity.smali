.class public Lcom/twitter/android/SecuritySettingsActivity;
.super Lcom/twitter/android/client/BasePreferenceActivity;
.source "Twttr"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private a:Lcom/twitter/android/tb;

.field private b:Ljava/lang/String;

.field private c:Lcom/twitter/android/ta;

.field private d:Z

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BasePreferenceActivity;-><init>()V

    return-void
.end method

.method static synthetic A(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->Q:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method static synthetic B(Lcom/twitter/android/SecuritySettingsActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->d:Z

    return v0
.end method

.method static synthetic a([I)I
    .locals 1

    invoke-static {p0}, Lcom/twitter/android/SecuritySettingsActivity;->b([I)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->d()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private a(I)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/twitter/android/SecuritySettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/SecuritySettingsActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/SecuritySettingsActivity;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/SecuritySettingsActivity;Ljava/lang/String;I[I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/SecuritySettingsActivity;->a(Ljava/lang/String;I[I)V

    return-void
.end method

.method private a(Ljava/lang/String;I[I)V
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    const-string/jumbo v0, "settings:login_verification:"

    invoke-static {p3}, Lcom/twitter/android/SecuritySettingsActivity;->b([I)I

    move-result v0

    const/16 v1, 0x58

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/SecuritySettingsActivity;->Q:Lcom/twitter/android/client/c;

    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->d()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v8, [Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "settings:login_verification:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "::rate_limit"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->d()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v2, v8, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "settings:login_verification:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "::failure"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->d(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/scribe/ScribeLog;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->Q:Lcom/twitter/android/client/c;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method private a(Z)V
    .locals 3

    if-eqz p1, :cond_0

    const v0, 0x7f0f023f    # com.twitter.android.R.string.login_verification_checking_eligibility

    invoke-virtual {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->a(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->Q:Lcom/twitter/android/client/c;

    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->c()Lcom/twitter/library/client/aa;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/SecuritySettingsActivity;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/SecuritySettingsActivity;->b:Ljava/lang/String;

    invoke-static {p0, v2}, Lcom/twitter/library/api/account/k;->e(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/SecuritySettingsActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/SecuritySettingsActivity;->d:Z

    return p1
.end method

.method private static b([I)I
    .locals 2

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    array-length v1, p0

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    aget v0, p0, v0

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    new-instance v0, Lcom/twitter/android/sq;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/sq;-><init>(Lcom/twitter/android/SecuritySettingsActivity;Ljava/lang/String;)V

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->Q:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method private b()V
    .locals 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->e:Z

    new-instance v0, Lcom/twitter/android/sz;

    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/SecuritySettingsActivity;->b:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2}, Lcom/twitter/android/sz;-><init>(Lcom/twitter/android/SecuritySettingsActivity;Landroid/content/Context;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/sz;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/SecuritySettingsActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/SecuritySettingsActivity;->e:Z

    return p1
.end method

.method static synthetic c(Lcom/twitter/android/SecuritySettingsActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/SecuritySettingsActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/SecuritySettingsActivity;->f:Z

    return p1
.end method

.method static synthetic d(Lcom/twitter/android/SecuritySettingsActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/SecuritySettingsActivity;->b()V

    return-void
.end method

.method static synthetic e(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->d()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private e()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/SecuritySettingsActivity;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/library/api/account/k;->f(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/SecuritySettingsActivity;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/library/api/account/k;->e(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic f(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->Q:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->d()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->Q:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->d()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->Q:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->d()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic l(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->Q:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method static synthetic m(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->d()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic n(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->Q:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method static synthetic o(Lcom/twitter/android/SecuritySettingsActivity;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/SecuritySettingsActivity;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic p(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->Q:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method static synthetic q(Lcom/twitter/android/SecuritySettingsActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->e:Z

    return v0
.end method

.method static synthetic r(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->d()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic s(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->Q:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method static synthetic t(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->d()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic u(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->Q:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method static synthetic v(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->Q:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method static synthetic w(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->Q:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method static synthetic x(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->Q:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method static synthetic y(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->d()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic z(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->Q:Lcom/twitter/android/client/c;

    return-object v0
.end method


# virtual methods
.method a()V
    .locals 1

    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->removeDialog(I)V

    return-void
.end method

.method a(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "msg"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x7

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/SecuritySettingsActivity;->showDialog(ILandroid/os/Bundle;)Z

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BasePreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->Q:Lcom/twitter/android/client/c;

    const v0, 0x7f0f0425    # com.twitter.android.R.string.settings_security_title

    invoke-virtual {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->setTitle(I)V

    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "account_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->b:Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string/jumbo v0, "enrolling"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->e:Z

    :goto_0
    const v0, 0x7f060016    # com.twitter.android.R.xml.security_prefs

    invoke-virtual {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/SecuritySettingsActivity;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/library/api/account/k;->f(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    const-string/jumbo v0, "login_verification"

    invoke-virtual {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    const-string/jumbo v0, "login_verification_generate_code"

    invoke-virtual {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    const-string/jumbo v0, "login_verification_check_requests"

    invoke-virtual {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    new-instance v0, Lcom/twitter/android/tb;

    invoke-direct {v0, p0, v4}, Lcom/twitter/android/tb;-><init>(Lcom/twitter/android/SecuritySettingsActivity;Lcom/twitter/android/sq;)V

    iput-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->a:Lcom/twitter/android/tb;

    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->Q:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/SecuritySettingsActivity;->a:Lcom/twitter/android/tb;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/j;)V

    new-instance v0, Lcom/twitter/android/ta;

    invoke-direct {v0, p0}, Lcom/twitter/android/ta;-><init>(Lcom/twitter/android/SecuritySettingsActivity;)V

    iput-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->c:Lcom/twitter/android/ta;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    sget-object v1, Lcom/twitter/library/platform/PushService;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v1, Lcom/twitter/library/platform/PushService;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/SecuritySettingsActivity;->c:Lcom/twitter/android/ta;

    sget-object v2, Lcom/twitter/library/provider/w;->a:Ljava/lang/String;

    invoke-virtual {p0, v1, v0, v2, v4}, Lcom/twitter/android/SecuritySettingsActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    iput-boolean v3, p0, Lcom/twitter/android/SecuritySettingsActivity;->d:Z

    iput-boolean v3, p0, Lcom/twitter/android/SecuritySettingsActivity;->f:Z

    return-void

    :cond_0
    iput-boolean v3, p0, Lcom/twitter/android/SecuritySettingsActivity;->e:Z

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 10

    const v9, 0x104000a    # android.R.string.ok

    const v8, 0x1040009    # android.R.string.no

    const/4 v7, 0x0

    const v5, 0x7f0f0249    # com.twitter.android.R.string.login_verification_more_stuff_required_title

    const v6, 0x7f0f0089    # com.twitter.android.R.string.cancel

    new-instance v0, Lcom/twitter/android/sr;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/sr;-><init>(Lcom/twitter/android/SecuritySettingsActivity;I)V

    new-instance v1, Lcom/twitter/android/ss;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/ss;-><init>(Lcom/twitter/android/SecuritySettingsActivity;I)V

    new-instance v2, Lcom/twitter/android/st;

    invoke-direct {v2, p0}, Lcom/twitter/android/st;-><init>(Lcom/twitter/android/SecuritySettingsActivity;)V

    new-instance v3, Lcom/twitter/android/su;

    invoke-direct {v3, p0}, Lcom/twitter/android/su;-><init>(Lcom/twitter/android/SecuritySettingsActivity;)V

    new-instance v4, Lcom/twitter/android/sv;

    invoke-direct {v4, p0}, Lcom/twitter/android/sv;-><init>(Lcom/twitter/android/SecuritySettingsActivity;)V

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    invoke-super {p0, p1}, Lcom/twitter/android/client/BasePreferenceActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0f0243    # com.twitter.android.R.string.login_verification_currently_unavailable_title

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f0242    # com.twitter.android.R.string.login_verification_currently_unavailable

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v9, v4}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    new-instance v0, Lcom/twitter/android/sw;

    invoke-direct {v0, p0}, Lcom/twitter/android/sw;-><init>(Lcom/twitter/android/SecuritySettingsActivity;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v5, 0x7f0f023c    # com.twitter.android.R.string.login_verification_add_a_phone_message

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v5, 0x7f0f0018    # com.twitter.android.R.string.add_phone

    invoke-virtual {v1, v5, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v6, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_0

    :pswitch_3
    new-instance v0, Lcom/twitter/android/sx;

    invoke-direct {v0, p0}, Lcom/twitter/android/sx;-><init>(Lcom/twitter/android/SecuritySettingsActivity;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f024c    # com.twitter.android.R.string.login_verification_no_verified_email_message

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f054e    # com.twitter.android.R.string.verify_email

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v6, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    const-string/jumbo v0, "settings:login_verification:switch:ok:click"

    invoke-direct {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->b(Ljava/lang/String;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f0246    # com.twitter.android.R.string.login_verification_enrolled_elsewhere_message

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f0494    # com.twitter.android.R.string.switch_device

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v6, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_5
    const-string/jumbo v2, "settings:login_verification:enroll:ok:click"

    invoke-direct {p0, v2}, Lcom/twitter/android/SecuritySettingsActivity;->b(Ljava/lang/String;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v2

    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0f0241    # com.twitter.android.R.string.login_verification_confirmation_title

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0f0240    # com.twitter.android.R.string.login_verification_confirmation_message

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x1040013    # android.R.string.yes

    invoke-virtual {v3, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v8, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_6
    new-instance v2, Lcom/twitter/android/sy;

    invoke-direct {v2, p0}, Lcom/twitter/android/sy;-><init>(Lcom/twitter/android/SecuritySettingsActivity;)V

    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0f0110    # com.twitter.android.R.string.disable_login_verification_confirmation_title

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0f010f    # com.twitter.android.R.string.disable_login_verification_confirmation_message

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x1040013    # android.R.string.yes

    invoke-virtual {v3, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v8, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_7
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0f0244    # com.twitter.android.R.string.login_verification_enabled_failure

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x1080027    # android.R.drawable.ic_dialog_alert

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v9, v0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_8
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0f024b    # com.twitter.android.R.string.login_verification_no_push_sorry_title

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0f024a    # com.twitter.android.R.string.login_verification_no_push_sorry

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0f02d5    # com.twitter.android.R.string.ok

    invoke-virtual {v2, v3, v0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_9
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v7}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    invoke-virtual {v0, v7}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_8
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/client/BasePreferenceActivity;->onDestroy()V

    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->Q:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/SecuritySettingsActivity;->a:Lcom/twitter/android/tb;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/j;)V

    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->c:Lcom/twitter/android/ta;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->c:Lcom/twitter/android/ta;

    invoke-virtual {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "login_verification"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/twitter/android/SecuritySettingsActivity;->Q:Lcom/twitter/android/client/c;

    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->d()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    new-array v5, v1, [Ljava/lang/String;

    const-string/jumbo v6, "settings:login_verification:::select"

    aput-object v6, v5, v0

    invoke-virtual {v2, v3, v4, v5}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/twitter/android/SecuritySettingsActivity;->f:Z

    if-nez v2, :cond_0

    invoke-virtual {p0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->showDialog(I)V

    :goto_0
    return v0

    :cond_0
    const/16 v1, 0xb

    invoke-virtual {p0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->showDialog(I)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/twitter/android/SecuritySettingsActivity;->Q:Lcom/twitter/android/client/c;

    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->d()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v5, "settings:login_verification:::deselect"

    aput-object v5, v1, v0

    invoke-virtual {v2, v3, v4, v1}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->showDialog(I)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "login_verification_generate_code"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/BackupCodeActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "bc_account_name"

    iget-object v3, p0, Lcom/twitter/android/SecuritySettingsActivity;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return v0

    :cond_0
    const-string/jumbo v2, "login_verification_check_requests"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/LoginVerificationActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "lv_account_name"

    iget-object v3, p0, Lcom/twitter/android/SecuritySettingsActivity;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V
    .locals 6

    const/4 v3, 0x1

    const/4 v5, 0x0

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BasePreferenceActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    :goto_0
    return-void

    :sswitch_0
    check-cast p2, Landroid/app/ProgressDialog;

    const-string/jumbo v0, "msg"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    :cond_0
    invoke-virtual {p2, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->Q:Lcom/twitter/android/client/c;

    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->d()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "settings:login_verification:enroll::impression"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BasePreferenceActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/twitter/android/SecuritySettingsActivity;->Q:Lcom/twitter/android/client/c;

    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->d()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "settings:login_verification:unenroll::impression"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BasePreferenceActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x7 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onResume()V
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/twitter/android/client/BasePreferenceActivity;->onResume()V

    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->d()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->d()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/SecuritySettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/android/StartActivity;->a(Landroid/app/Activity;Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    iget-object v3, p0, Lcom/twitter/android/SecuritySettingsActivity;->Q:Lcom/twitter/android/client/c;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    new-array v2, v0, [Ljava/lang/String;

    const-string/jumbo v6, "settings:login_verification:::impression"

    aput-object v6, v2, v1

    invoke-virtual {v3, v4, v5, v2}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/twitter/android/SecuritySettingsActivity;->e:Z

    if-nez v2, :cond_1

    :goto_1
    invoke-direct {p0, v0}, Lcom/twitter/android/SecuritySettingsActivity;->a(Z)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BasePreferenceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "enrolling"

    iget-boolean v1, p0, Lcom/twitter/android/SecuritySettingsActivity;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
