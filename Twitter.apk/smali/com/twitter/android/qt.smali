.class Lcom/twitter/android/qt;
.super Landroid/support/v4/view/PagerAdapter;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/ProfileFragment;

.field private final b:Landroid/view/LayoutInflater;

.field private c:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/twitter/android/ProfileFragment;Landroid/view/LayoutInflater;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/qt;->a:Lcom/twitter/android/ProfileFragment;

    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    iput-object p2, p0, Lcom/twitter/android/qt;->b:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/qt;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->z:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qt;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->C:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qt;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->A:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/qt;->c:Landroid/view/View;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qt;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->z:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qt;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->C:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qt;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->A:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 3

    const/4 v2, 0x0

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qt;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f030179    # com.twitter.android.R.layout.user_profile_header_main

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/qt;->a:Lcom/twitter/android/ProfileFragment;

    const v0, 0x7f09012d    # com.twitter.android.R.id.profile_image

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/twitter/android/ProfileFragment;->d:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/twitter/android/qt;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->v_()V

    iget-object v2, p0, Lcom/twitter/android/qt;->a:Lcom/twitter/android/ProfileFragment;

    const v0, 0x7f090097    # com.twitter.android.R.id.name

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/twitter/android/ProfileFragment;->p:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/qt;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->z()V

    iget-object v2, p0, Lcom/twitter/android/qt;->a:Lcom/twitter/android/ProfileFragment;

    const v0, 0x7f0900f1    # com.twitter.android.R.id.user_name

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/twitter/android/ProfileFragment;->q:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/twitter/android/qt;->a:Lcom/twitter/android/ProfileFragment;

    const v0, 0x7f09003e    # com.twitter.android.R.id.follows_you

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/twitter/android/ProfileFragment;->r:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/qt;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->y()V

    iget-object v0, p0, Lcom/twitter/android/qt;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->D()V

    iget-object v2, p0, Lcom/twitter/android/qt;->a:Lcom/twitter/android/ProfileFragment;

    const v0, 0x7f09001f    # com.twitter.android.R.id.icon

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/twitter/android/ProfileFragment;->e:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/twitter/android/qt;->a:Lcom/twitter/android/ProfileFragment;

    const v0, 0x7f0902c6    # com.twitter.android.R.id.translator_icon

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/twitter/android/ProfileFragment;->f:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/twitter/android/qt;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->w_()V

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/qt;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f03017a    # com.twitter.android.R.layout.user_profile_header_more

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/qt;->a:Lcom/twitter/android/ProfileFragment;

    const v0, 0x7f09005c    # com.twitter.android.R.id.user_bio

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/twitter/android/ProfileFragment;->s:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/qt;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->C()V

    iget-object v2, p0, Lcom/twitter/android/qt;->a:Lcom/twitter/android/ProfileFragment;

    const v0, 0x7f0902c7    # com.twitter.android.R.id.user_location

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/twitter/android/ProfileFragment;->t:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/qt;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->u()V

    iget-object v2, p0, Lcom/twitter/android/qt;->a:Lcom/twitter/android/ProfileFragment;

    const v0, 0x7f0902c8    # com.twitter.android.R.id.user_url

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/twitter/android/ProfileFragment;->u:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/qt;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->v()V

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iput-object v1, p0, Lcom/twitter/android/qt;->c:Landroid/view/View;

    move-object v0, v1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Number of items is not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
