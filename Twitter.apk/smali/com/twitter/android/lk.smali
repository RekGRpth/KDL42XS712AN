.class Lcom/twitter/android/lk;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/AttachMediaListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/MediaTagFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/MediaTagFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/lk;->a:Lcom/twitter/android/MediaTagFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;Lcom/twitter/android/AttachMediaListener$MediaAttachProgress;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;Lcom/twitter/android/PostStorage$MediaItem;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/android/PostStorage$MediaItem;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/lk;->a:Lcom/twitter/android/MediaTagFragment;

    invoke-virtual {v0}, Lcom/twitter/android/MediaTagFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput v0, p1, Lcom/twitter/android/PostStorage$MediaItem;->c:I

    iget-object v0, p0, Lcom/twitter/android/lk;->a:Lcom/twitter/android/MediaTagFragment;

    iget-object v1, p1, Lcom/twitter/android/PostStorage$MediaItem;->e:Landroid/graphics/Bitmap;

    invoke-static {v0, v1}, Lcom/twitter/android/MediaTagFragment;->a(Lcom/twitter/android/MediaTagFragment;Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method public a(Landroid/net/Uri;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
