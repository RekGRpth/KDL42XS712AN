.class Lcom/twitter/android/xq;
.super Landroid/widget/BaseAdapter;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/TweetFragment;

.field private final b:Lcom/twitter/library/widget/ap;

.field private final c:Lcom/twitter/library/provider/Tweet;

.field private final d:Ljava/util/ArrayList;

.field private e:Lcom/twitter/android/ob;

.field private f:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Lcom/twitter/android/TweetFragment;Lcom/twitter/library/widget/ap;Lcom/twitter/library/provider/Tweet;)V
    .locals 2

    iput-object p1, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/xq;->f:Ljava/util/ArrayList;

    iput-object p2, p0, Lcom/twitter/android/xq;->b:Lcom/twitter/library/widget/ap;

    iput-object p3, p0, Lcom/twitter/android/xq;->c:Lcom/twitter/library/provider/Tweet;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/xq;->d:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/twitter/android/xq;->d:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/twitter/android/xq;->c:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/provider/Tweet;)I
    .locals 2

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/xq;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-virtual {p0, v0}, Lcom/twitter/android/xq;->a(I)Lcom/twitter/library/provider/Tweet;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/twitter/library/provider/Tweet;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public a(I)Lcom/twitter/library/provider/Tweet;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/xq;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/Tweet;

    return-object v0
.end method

.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/xq;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/xq;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/TweetView;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xq;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :goto_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->a()V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public a(ILcom/twitter/library/provider/Tweet;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/xq;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    return-void
.end method

.method public a(J)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/xq;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->b()J

    move-result-wide v2

    cmp-long v2, v2, p1

    if-nez v2, :cond_0

    iget-object v1, p0, Lcom/twitter/android/xq;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/twitter/android/xq;->notifyDataSetChanged()V

    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/android/ob;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/xq;->e:Lcom/twitter/android/ob;

    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/xq;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/twitter/android/xq;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method public a(Ljava/util/HashMap;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/xq;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_2

    iget-object v0, p0, Lcom/twitter/android/xq;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/TweetView;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/xq;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    :goto_1
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->getUserImageKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/util/ae;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v1, v1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Bitmap;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Bitmap;Z)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public a(Ljava/util/HashMap;Z)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/xq;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    iget-object v0, p0, Lcom/twitter/android/xq;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/TweetView;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/xq;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    :goto_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/widget/TweetView;->a(Ljava/util/HashMap;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    const-string/jumbo v3, "show"

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/library/provider/Tweet;

    move-result-object v0

    invoke-static {v2, v3, v0}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/android/TweetFragment;Ljava/lang/String;Lcom/twitter/library/provider/Tweet;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/xq;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/xq;->a(I)Lcom/twitter/library/provider/Tweet;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    invoke-virtual {p0, p1}, Lcom/twitter/android/xq;->a(I)Lcom/twitter/library/provider/Tweet;

    move-result-object v0

    iget-wide v0, v0, Lcom/twitter/library/provider/Tweet;->o:J

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/xq;->c:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {p0, p1}, Lcom/twitter/android/xq;->a(I)Lcom/twitter/library/provider/Tweet;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/Tweet;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, p1}, Lcom/twitter/android/xq;->a(I)Lcom/twitter/library/provider/Tweet;

    move-result-object v3

    iget-object v0, p0, Lcom/twitter/android/xq;->c:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0, v3}, Lcom/twitter/library/provider/Tweet;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v1, p0, Lcom/twitter/android/xq;->c:Lcom/twitter/library/provider/Tweet;

    iget-object v0, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->B(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/client/c;

    move-result-object v2

    if-nez p2, :cond_6

    iget-object v0, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    iget-object v3, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v3}, Lcom/twitter/android/TweetFragment;->C(Lcom/twitter/android/TweetFragment;)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    iget-object v3, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v3}, Lcom/twitter/android/TweetFragment;->C(Lcom/twitter/android/TweetFragment;)I

    move-result v7

    :goto_0
    iget-object v3, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    iget-object v4, v1, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v5, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    iget-object v5, v5, Lcom/twitter/android/TweetFragment;->n:Ljava/lang/String;

    iget-object v6, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v6}, Lcom/twitter/android/TweetFragment;->D(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/client/aa;

    move-result-object v6

    iget-object v8, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v8}, Lcom/twitter/android/TweetFragment;->E(Lcom/twitter/android/TweetFragment;)I

    move-result v8

    iget-object v9, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v9}, Lcom/twitter/android/TweetFragment;->F(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/tq;

    move-result-object v9

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/android/client/c;Lcom/twitter/library/view/c;Lcom/twitter/library/api/TweetEntities;Ljava/lang/String;Lcom/twitter/library/client/aa;IILcom/twitter/android/tq;)V

    iget-wide v3, v1, Lcom/twitter/library/provider/Tweet;->ad:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-eqz v3, :cond_3

    iget-object v2, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v2}, Lcom/twitter/android/TweetFragment;->z(Lcom/twitter/android/TweetFragment;)V

    :goto_1
    iget-object v2, v1, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    if-eqz v2, :cond_4

    iget-object v3, v2, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v3, v2}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/android/TweetFragment;Lcom/twitter/library/api/TwitterStatusCard;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    iget-object v2, v2, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/android/xn;

    const-string/jumbo v3, "show"

    invoke-virtual {v2, v3}, Lcom/twitter/android/xn;->g(Ljava/lang/String;)V

    :cond_0
    :goto_2
    iget-object v2, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v2}, Lcom/twitter/android/TweetFragment;->G(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v2}, Lcom/twitter/android/TweetFragment;->H(Lcom/twitter/android/TweetFragment;)Landroid/view/ViewGroup;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v2}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    iget-object v3, v3, Lcom/twitter/android/TweetFragment;->a:Lcom/twitter/library/client/Session;

    iget-object v4, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v4}, Lcom/twitter/android/TweetFragment;->H(Lcom/twitter/android/TweetFragment;)Landroid/view/ViewGroup;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v2, v3, v1, v4, v5}, Lcom/twitter/android/widget/dn;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/provider/Tweet;Landroid/view/ViewGroup;Landroid/view/View$OnClickListener;)V

    :cond_1
    :goto_3
    iget-object v2, p0, Lcom/twitter/android/xq;->e:Lcom/twitter/android/ob;

    if-eqz v2, :cond_2

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2, v10}, Landroid/os/Bundle;-><init>(I)V

    const-string/jumbo v3, "position"

    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v3, p0, Lcom/twitter/android/xq;->e:Lcom/twitter/android/ob;

    invoke-interface {v3, v0, v1, v2}, Lcom/twitter/android/ob;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    :cond_2
    return-object v0

    :pswitch_0
    const/16 v7, 0x1e

    goto/16 :goto_0

    :pswitch_1
    const/16 v7, 0x1f

    goto/16 :goto_0

    :pswitch_2
    const/16 v7, 0x20

    goto/16 :goto_0

    :cond_3
    iget-object v3, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    iget-object v4, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    iget-object v4, v4, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v4, v4, Lcom/twitter/library/provider/Tweet;->p:Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    iget-object v5, v5, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v5, v5, Lcom/twitter/library/provider/Tweet;->n:J

    invoke-virtual {v2, v4, v5, v6}, Lcom/twitter/android/client/c;->a(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/twitter/android/TweetFragment;->l(Lcom/twitter/android/TweetFragment;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_4
    iget-object v2, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    iget-object v2, v2, Lcom/twitter/android/TweetFragment;->d:Lcom/twitter/library/api/TweetClassicCard;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/twitter/android/widget/TweetDetailView;->g:Landroid/view/View;

    if-eqz v2, :cond_5

    iget-object v2, v0, Lcom/twitter/android/widget/TweetDetailView;->g:Landroid/view/View;

    iget-object v3, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_5
    iget-object v2, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    iget-object v3, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    iget-object v3, v3, Lcom/twitter/android/TweetFragment;->d:Lcom/twitter/library/api/TweetClassicCard;

    invoke-virtual {v2, v3, v1}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/api/TweetClassicCard;Lcom/twitter/library/provider/Tweet;)V

    goto :goto_2

    :cond_6
    move-object v0, p2

    check-cast v0, Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/android/client/c;)V

    move-object v0, p2

    goto/16 :goto_2

    :cond_7
    iget-object v0, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->I(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/client/c;

    move-result-object v4

    if-nez p2, :cond_e

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030041    # com.twitter.android.R.layout.conversation_row

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    const v0, 0x7f090095    # com.twitter.android.R.id.row

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/TweetView;

    iget-object v1, p0, Lcom/twitter/android/xq;->b:Lcom/twitter/library/widget/ap;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetView;->setProvider(Lcom/twitter/library/widget/ap;)V

    iget-boolean v1, v4, Lcom/twitter/android/client/c;->f:Z

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetView;->setRenderRTL(Z)V

    iget-object v1, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v1}, Lcom/twitter/android/TweetFragment;->J(Lcom/twitter/android/TweetFragment;)Z

    move-result v1

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    iget-object v1, v1, Lcom/twitter/android/TweetFragment;->j:Lcom/twitter/android/yb;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetView;->setOnProfileImageClickListener(Lcom/twitter/library/widget/aa;)V

    :cond_8
    invoke-virtual {v4}, Lcom/twitter/android/client/c;->V()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetView;->setContentSize(F)V

    invoke-static {}, Lkl;->f()Z

    move-result v1

    if-nez v1, :cond_d

    move v1, v10

    :goto_4
    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetView;->setHideInlineActions(Z)V

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v5, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v5}, Lcom/twitter/android/TweetFragment;->K(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/client/Session;

    move-result-object v5

    invoke-virtual {v3, v1, v5}, Lcom/twitter/library/provider/Tweet;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetView;->setWillTranslate(Z)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/twitter/android/xq;->f:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/ref/WeakReference;

    invoke-direct {v5, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_5
    invoke-virtual {v4}, Lcom/twitter/android/client/c;->S()Z

    move-result v1

    if-eqz v1, :cond_a

    iget-wide v4, v3, Lcom/twitter/library/provider/Tweet;->h:J

    iget-object v1, p0, Lcom/twitter/android/xq;->c:Lcom/twitter/library/provider/Tweet;

    iget-wide v6, v1, Lcom/twitter/library/provider/Tweet;->h:J

    cmp-long v1, v4, v6

    if-gez v1, :cond_f

    move v1, v10

    :goto_6
    if-nez v1, :cond_9

    iget-wide v4, v3, Lcom/twitter/library/provider/Tweet;->q:J

    iget-object v1, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v1}, Lcom/twitter/android/TweetFragment;->L(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-eqz v1, :cond_9

    iget v1, v3, Lcom/twitter/library/provider/Tweet;->ac:I

    invoke-static {v1}, Lcom/twitter/library/provider/ay;->b(I)Z

    move-result v1

    if-eqz v1, :cond_10

    :cond_9
    invoke-virtual {v0, v10}, Lcom/twitter/library/widget/TweetView;->setAlwaysExpandMedia(Z)V

    invoke-virtual {v0, v10}, Lcom/twitter/library/widget/TweetView;->setSimplifyUrls(Z)V

    :cond_a
    :goto_7
    invoke-virtual {v0, v10}, Lcom/twitter/library/widget/TweetView;->setHideGeoInfo(Z)V

    invoke-static {}, Lkn;->b()Z

    move-result v1

    if-eqz v1, :cond_c

    const v1, 0x7f02002e    # com.twitter.android.R.drawable.bg_list_convo_row

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetView;->setBackgroundResource(I)V

    invoke-virtual {v3}, Lcom/twitter/library/provider/Tweet;->D()Z

    move-result v1

    if-eqz v1, :cond_c

    iget-wide v4, v3, Lcom/twitter/library/provider/Tweet;->j:J

    iget-object v1, p0, Lcom/twitter/android/xq;->c:Lcom/twitter/library/provider/Tweet;

    iget-wide v6, v1, Lcom/twitter/library/provider/Tweet;->u:J

    cmp-long v1, v4, v6

    if-eqz v1, :cond_b

    invoke-virtual {p0, v3}, Lcom/twitter/android/xq;->a(Lcom/twitter/library/provider/Tweet;)I

    move-result v1

    iget-object v4, p0, Lcom/twitter/android/xq;->c:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {p0, v4}, Lcom/twitter/android/xq;->a(Lcom/twitter/library/provider/Tweet;)I

    move-result v4

    if-ge v1, v4, :cond_12

    :cond_b
    iput v2, v3, Lcom/twitter/library/provider/Tweet;->P:I

    iput-object v9, v3, Lcom/twitter/library/provider/Tweet;->Q:Ljava/lang/String;

    :cond_c
    :goto_8
    invoke-virtual {v0, v3}, Lcom/twitter/library/widget/TweetView;->setTweet(Lcom/twitter/library/provider/Tweet;)V

    move-object v1, v3

    move-object v0, p2

    goto/16 :goto_3

    :cond_d
    move v1, v2

    goto/16 :goto_4

    :cond_e
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/TweetView;

    goto :goto_5

    :cond_f
    move v1, v2

    goto :goto_6

    :cond_10
    invoke-virtual {v3}, Lcom/twitter/library/provider/Tweet;->q()Z

    move-result v1

    if-eqz v1, :cond_11

    iget-object v1, p0, Lcom/twitter/android/xq;->a:Lcom/twitter/android/TweetFragment;

    const-string/jumbo v4, "skip"

    invoke-static {v1, v4, v3}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/android/TweetFragment;Ljava/lang/String;Lcom/twitter/library/provider/Tweet;)V

    :cond_11
    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/TweetView;->setAlwaysExpandMedia(Z)V

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/TweetView;->setSimplifyUrls(Z)V

    goto :goto_7

    :cond_12
    move v1, v2

    :goto_9
    if-ge v1, p1, :cond_c

    invoke-virtual {p0, v1}, Lcom/twitter/android/xq;->a(I)Lcom/twitter/library/provider/Tweet;

    move-result-object v4

    iget-wide v5, v3, Lcom/twitter/library/provider/Tweet;->j:J

    iget-wide v7, v4, Lcom/twitter/library/provider/Tweet;->u:J

    cmp-long v5, v5, v7

    if-nez v5, :cond_13

    const/16 v1, 0x18

    iput v1, v3, Lcom/twitter/library/provider/Tweet;->P:I

    iget-object v1, v4, Lcom/twitter/library/provider/Tweet;->f:Ljava/lang/String;

    iput-object v1, v3, Lcom/twitter/library/provider/Tweet;->Q:Ljava/lang/String;

    iput-object v9, v3, Lcom/twitter/library/provider/Tweet;->aa:Ljava/lang/String;

    iput v2, v3, Lcom/twitter/library/provider/Tweet;->ab:I

    goto :goto_8

    :cond_13
    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    :pswitch_data_0
    .packed-switch 0x12
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method
