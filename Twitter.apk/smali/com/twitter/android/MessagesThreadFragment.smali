.class public Lcom/twitter/android/MessagesThreadFragment;
.super Lcom/twitter/android/client/BaseListFragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/ce;
.implements Lcom/twitter/library/view/c;


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:Ljava/text/SimpleDateFormat;

.field private static final c:Ljava/text/SimpleDateFormat;

.field private static final d:Ljava/text/SimpleDateFormat;


# instance fields
.field private e:J

.field private f:Ljava/lang/String;

.field private g:J

.field private h:Z

.field private i:Lcom/twitter/android/md;

.field private j:Ljava/util/HashMap;

.field private k:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "msg_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "sender_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "content"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "created"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "s_profile_image_url"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "entities"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "status"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "draft"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/android/MessagesThreadFragment;->a:[Ljava/lang/String;

    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0}, Ljava/text/SimpleDateFormat;-><init>()V

    sput-object v0, Lcom/twitter/android/MessagesThreadFragment;->b:Ljava/text/SimpleDateFormat;

    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0}, Ljava/text/SimpleDateFormat;-><init>()V

    sput-object v0, Lcom/twitter/android/MessagesThreadFragment;->c:Ljava/text/SimpleDateFormat;

    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0}, Ljava/text/SimpleDateFormat;-><init>()V

    sput-object v0, Lcom/twitter/android/MessagesThreadFragment;->d:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/twitter/android/client/BaseListFragment;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/android/MessagesThreadFragment;->g:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/MessagesThreadFragment;->h:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/MessagesThreadFragment;->j:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/MessagesThreadFragment;->k:Ljava/util/HashMap;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/MessagesThreadFragment;J)J
    .locals 0

    iput-wide p1, p0, Lcom/twitter/android/MessagesThreadFragment;->g:J

    return-wide p1
.end method

.method static synthetic a(Lcom/twitter/android/MessagesThreadFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MessagesThreadFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method private a(JZ)V
    .locals 3

    new-instance v0, Liv;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Liv;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {v0, p1, p2, p3}, Liv;->a(JZ)V

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    move-result-object v1

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Lcom/twitter/library/service/b;->c(I)Lcom/twitter/library/service/b;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/MessagesThreadFragment;->a(Lcom/twitter/library/service/b;II)Z

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/MessagesThreadFragment;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/MessagesThreadFragment;->d(I)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/MessagesThreadFragment;JZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/MessagesThreadFragment;->a(JZ)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/MessagesThreadFragment;Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/twitter/android/MessagesThreadFragment;->d(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/MessagesThreadFragment;Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/MessagesThreadFragment;->a(Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;)V
    .locals 7

    const/4 v6, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v6, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "messages:thread:message::copy"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-virtual {p2, v6}, Lcom/twitter/library/api/TweetEntities;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0, p2}, Lcom/twitter/android/lw;->a(Ljava/lang/StringBuilder;Lcom/twitter/library/api/TweetEntities;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/twitter/library/util/Util;->b(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/MessagesThreadFragment;Lcom/twitter/library/service/b;II)Z
    .locals 1

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/MessagesThreadFragment;->a(Lcom/twitter/library/service/b;II)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/MessagesThreadFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/MessagesThreadFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/MessagesThreadFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/MessagesThreadFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/MessagesThreadFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method private d(I)V
    .locals 3

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/twitter/android/MessagesThreadFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    const/16 v2, 0x8

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-ne v2, v1, :cond_0

    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {p0, v0}, Lcom/twitter/android/MessagesThreadFragment;->c(Landroid/database/Cursor;)V

    :goto_1
    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v0}, Lcom/twitter/android/MessagesThreadFragment;->b(Landroid/database/Cursor;)V

    goto :goto_1
.end method

.method static synthetic e(Lcom/twitter/android/MessagesThreadFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/MessagesThreadFragment;)J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/android/MessagesThreadFragment;->e:J

    return-wide v0
.end method

.method static synthetic g(Lcom/twitter/android/MessagesThreadFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/MessagesThreadFragment;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MessagesThreadFragment;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/MessagesThreadFragment;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MessagesThreadFragment;->j:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/MessagesThreadFragment;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MessagesThreadFragment;->k:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/android/MessagesThreadFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic l(Lcom/twitter/android/MessagesThreadFragment;)Lcom/twitter/library/scribe/ScribeAssociation;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MessagesThreadFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    return-object v0
.end method

.method static synthetic q()Ljava/text/SimpleDateFormat;
    .locals 1

    sget-object v0, Lcom/twitter/android/MessagesThreadFragment;->b:Ljava/text/SimpleDateFormat;

    return-object v0
.end method

.method static synthetic r()Ljava/text/SimpleDateFormat;
    .locals 1

    sget-object v0, Lcom/twitter/android/MessagesThreadFragment;->c:Ljava/text/SimpleDateFormat;

    return-object v0
.end method

.method static synthetic s()Ljava/text/SimpleDateFormat;
    .locals 1

    sget-object v0, Lcom/twitter/android/MessagesThreadFragment;->d:Ljava/text/SimpleDateFormat;

    return-object v0
.end method


# virtual methods
.method protected a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    const v0, 0x7f0300ca    # com.twitter.android.R.layout.messages_thread_fragment

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/android/MessagesThreadFragment;->a(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/content/Context;IILcom/twitter/library/service/b;)V
    .locals 4

    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/android/client/BaseListFragment;->a(Landroid/content/Context;IILcom/twitter/library/service/b;)V

    invoke-virtual {p4}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    const/4 v1, 0x2

    if-ne p2, v1, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p4, Lcom/twitter/library/service/b;->k:Landroid/os/Bundle;

    const-string/jumbo v1, "draft_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    const/4 v2, 0x7

    invoke-static {v2}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v2

    const v3, 0x7f0f010c    # com.twitter.android.R.string.direct_message_error_title

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v2

    const v3, 0x7f0f0319    # com.twitter.android.R.string.post_retry_direct_messsage_question

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v2

    const v3, 0x7f0f0362    # com.twitter.android.R.string.retry

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v2

    const v3, 0x7f0f0089    # com.twitter.android.R.string.cancel

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v2

    new-instance v3, Lcom/twitter/android/mw;

    invoke-direct {v3, p0, v0, v1}, Lcom/twitter/android/mw;-><init>(Lcom/twitter/android/MessagesThreadFragment;J)V

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/android/widget/ce;)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    :cond_0
    return-void
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 11

    const-wide/16 v9, 0x0

    const-wide/16 v7, -0x1

    const/4 v1, -0x1

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-ne p3, v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "messages:thread:thread::delete"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/MessagesThreadFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v1}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v1, p0, Lcom/twitter/android/MessagesThreadFragment;->e:J

    cmp-long v1, v1, v9

    if-eqz v1, :cond_0

    iget-wide v1, p0, Lcom/twitter/android/MessagesThreadFragment;->Q:J

    cmp-long v1, v1, v9

    if-eqz v1, :cond_0

    iget-wide v1, p0, Lcom/twitter/android/MessagesThreadFragment;->e:J

    iget-wide v3, p0, Lcom/twitter/android/MessagesThreadFragment;->Q:J

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/twitter/android/MessagesThreadFragment;->b(JJ)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/String;

    aput-object v1, v2, v5

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/c;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/MessagesThreadFragment;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/MessagesThreadFragment;->i:Lcom/twitter/android/md;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MessagesThreadFragment;->i:Lcom/twitter/android/md;

    invoke-interface {v0, v1}, Lcom/twitter/android/md;->a_(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    iget-wide v1, p0, Lcom/twitter/android/MessagesThreadFragment;->g:J

    cmp-long v1, v1, v7

    if-eqz v1, :cond_1

    packed-switch p3, :pswitch_data_1

    :cond_1
    :goto_1
    iput-wide v7, p0, Lcom/twitter/android/MessagesThreadFragment;->g:J

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "messages:thread:message:spam:report_as_spam"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/twitter/android/MessagesThreadFragment;->g:J

    const-string/jumbo v3, "spam"

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/MessagesThreadFragment;->d(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "messages:thread:message:abusive:report_as_spam"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/twitter/android/MessagesThreadFragment;->g:J

    const-string/jumbo v3, "abuse"

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/MessagesThreadFragment;->d(Ljava/lang/String;)V

    const/4 v0, 0x4

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0261    # com.twitter.android.R.string.mark_as_abusive_follow_up

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0571    # com.twitter.android.R.string.yes

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f029d    # com.twitter.android.R.string.no

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {v0, p0, v5}, Lcom/twitter/android/widget/PromptDialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_1

    :pswitch_4
    if-ne p3, v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/WebViewActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const v1, 0x7f0f04fc    # com.twitter.android.R.string.twitter_abuse_help

    invoke-virtual {p0, v1}, Lcom/twitter/android/MessagesThreadFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/MessagesThreadFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 3

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseListFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/twitter/android/MessagesThreadFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/MessagesThreadFragment;->a_(Z)V

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->ap()V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/MessagesThreadFragment;->e:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->d(J)Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/MessagesThreadFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v1}, Landroid/support/v4/widget/CursorAdapter;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0
.end method

.method protected a(Lcom/twitter/internal/android/widget/ToolBar;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->a(Lcom/twitter/internal/android/widget/ToolBar;)V

    iget-object v1, p0, Lcom/twitter/android/MessagesThreadFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    const/4 v0, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    const v1, 0x7f090324    # com.twitter.android.R.id.menu_messages_delete

    invoke-virtual {p1, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v1

    invoke-virtual {v1, v0}, Lhn;->b(Z)Lhn;

    return-void
.end method

.method public a(Lcom/twitter/library/api/MediaEntity;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/api/Promotion;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/api/TweetClassicCard;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/api/UrlEntity;)V
    .locals 9

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/twitter/android/MessagesThreadFragment;->h:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    const-string/jumbo v5, "messages:thread:::open_link"

    iget-object v7, p0, Lcom/twitter/android/MessagesThreadFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v2, p1

    move-object v6, v1

    move-object v8, v1

    invoke-static/range {v0 .. v8}, Lcom/twitter/android/client/be;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/UrlEntity;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseListFragment;->a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)V

    const v0, 0x7f110018    # com.twitter.android.R.menu.messages_thread

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 0

    return-void
.end method

.method public a(Lhn;)Z
    .locals 2

    invoke-virtual {p1}, Lhn;->a()I

    move-result v0

    const v1, 0x7f090324    # com.twitter.android.R.id.menu_messages_delete

    if-ne v0, v1, :cond_0

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/twitter/android/MessagesDestroyThreadDialogFragment;->a(I)Lcom/twitter/android/MessagesDestroyThreadDialogFragment;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->a(Lhn;)Z

    move-result v0

    goto :goto_0
.end method

.method protected b(JJ)Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1, p2, p3, p4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1, p2, p3, p4}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(J)V
    .locals 0

    return-void
.end method

.method protected b(Landroid/database/Cursor;)V
    .locals 7

    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/TweetEntities;

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const/4 v0, 0x5

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f080006    # com.twitter.android.R.array.dm_longpress

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->f(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v6

    const/4 v0, 0x0

    invoke-virtual {v6, p0, v0}, Lcom/twitter/android/widget/PromptDialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    new-instance v0, Lcom/twitter/android/mu;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/mu;-><init>(Lcom/twitter/android/MessagesThreadFragment;Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;J)V

    invoke-virtual {v6, v0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/android/widget/ce;)Lcom/twitter/android/widget/PromptDialogFragment;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/twitter/android/MessagesThreadFragment;->h:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v3, :cond_0

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x40

    if-ne v0, v1, :cond_2

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "screen_name"

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/MessagesThreadFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x23

    if-ne v0, v1, :cond_3

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/SearchActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "query"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "q_source"

    const-string/jumbo v2, "hashtag_click"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "scribe_context"

    const-string/jumbo v2, "hashtag"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/MessagesThreadFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x24

    if-ne v0, v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/SearchActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "query"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "q_source"

    const-string/jumbo v2, "cashtag_click"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "scribe_context"

    const-string/jumbo v2, "cashtag"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/MessagesThreadFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public b_(J)V
    .locals 2

    iget-wide v0, p0, Lcom/twitter/android/MessagesThreadFragment;->e:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_0

    iput-wide p1, p0, Lcom/twitter/android/MessagesThreadFragment;->e:J

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/MessagesThreadFragment;->a_(Z)V

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->P()Z

    :cond_0
    return-void
.end method

.method public c(J)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/MessagesThreadFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method protected c(Landroid/database/Cursor;)V
    .locals 8

    const/4 v1, 0x6

    const/4 v7, 0x0

    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/TweetEntities;

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v1}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f080005    # com.twitter.android.R.array.dm_draft_longpress

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->f(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v6

    invoke-virtual {v6, p0, v7}, Lcom/twitter/android/widget/PromptDialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    new-instance v0, Lcom/twitter/android/mv;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/mv;-><init>(Lcom/twitter/android/MessagesThreadFragment;Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;J)V

    invoke-virtual {v6, v0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/android/widget/ce;)Lcom/twitter/android/widget/PromptDialogFragment;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    return-void
.end method

.method public e()V
    .locals 0

    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->p()V

    new-instance v0, Lcom/twitter/android/my;

    invoke-direct {v0, p0}, Lcom/twitter/android/my;-><init>(Lcom/twitter/android/MessagesThreadFragment;)V

    iput-object v0, p0, Lcom/twitter/android/MessagesThreadFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->V()Landroid/widget/ListView;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/ms;

    invoke-direct {v2, p0}, Lcom/twitter/android/ms;-><init>(Lcom/twitter/android/MessagesThreadFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    new-instance v2, Lcom/twitter/android/mt;

    invoke-direct {v2, p0}, Lcom/twitter/android/mt;-><init>(Lcom/twitter/android/MessagesThreadFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v0, Lcom/twitter/android/mx;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/mx;-><init>(Lcom/twitter/android/MessagesThreadFragment;Lcom/twitter/android/ms;)V

    iput-object v0, p0, Lcom/twitter/android/MessagesThreadFragment;->O:Lcom/twitter/library/client/j;

    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onAttach(Landroid/app/Activity;)V

    instance-of v0, p1, Lcom/twitter/android/md;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/twitter/android/md;

    iput-object p1, p0, Lcom/twitter/android/MessagesThreadFragment;->i:Lcom/twitter/android/md;

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v4, 0x1

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    sget-object v0, Lcom/twitter/android/MessagesThreadFragment;->b:Ljava/text/SimpleDateFormat;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00ed    # com.twitter.android.R.string.datetime_format_time_only

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    sget-object v0, Lcom/twitter/android/MessagesThreadFragment;->c:Ljava/text/SimpleDateFormat;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00ea    # com.twitter.android.R.string.datetime_format_day_time_only

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    sget-object v0, Lcom/twitter/android/MessagesThreadFragment;->d:Ljava/text/SimpleDateFormat;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00ec    # com.twitter.android.R.string.datetime_format_long_friendly

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/MessagesThreadFragment;->e:J

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "user_fullname"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MessagesThreadFragment;->f:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lcom/twitter/android/MessagesThreadFragment;->l(Z)V

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "messages:thread:::impression"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 9

    const/4 v6, 0x0

    const/4 v8, 0x0

    iget-wide v0, p0, Lcom/twitter/android/MessagesThreadFragment;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-virtual {p0, v8}, Lcom/twitter/android/MessagesThreadFragment;->a_(Z)V

    :goto_0
    return-object v6

    :cond_0
    iget-wide v0, p0, Lcom/twitter/android/MessagesThreadFragment;->e:J

    iget-wide v2, p0, Lcom/twitter/android/MessagesThreadFragment;->Q:J

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/twitter/android/MessagesThreadFragment;->b(JJ)Ljava/lang/String;

    move-result-object v7

    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/provider/ak;->a:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesThreadFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/android/MessagesThreadFragment;->a:[Ljava/lang/String;

    const-string/jumbo v4, "thread=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    aput-object v7, v5, v8

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v6, v0

    goto :goto_0
.end method

.method public onDetach()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onDetach()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/MessagesThreadFragment;->i:Lcom/twitter/android/md;

    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/MessagesThreadFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method
