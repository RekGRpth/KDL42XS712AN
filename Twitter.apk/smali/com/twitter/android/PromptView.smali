.class public Lcom/twitter/android/PromptView;
.super Landroid/widget/LinearLayout;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Lcom/twitter/library/view/c;


# instance fields
.field private a:Z

.field private b:Lcom/twitter/library/api/Prompt;

.field private c:Landroid/graphics/Bitmap;

.field private d:Landroid/content/Context;

.field private e:Landroid/widget/RelativeLayout;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/Button;

.field private i:Landroid/widget/ImageView;

.field private j:Landroid/widget/LinearLayout;

.field private k:Landroid/text/format/Time;

.field private final l:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/PromptView;->k:Landroid/text/format/Time;

    iput-object p1, p0, Lcom/twitter/android/PromptView;->d:Landroid/content/Context;

    iput-object p2, p0, Lcom/twitter/android/PromptView;->l:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/PromptView;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300f3    # com.twitter.android.R.layout.prompt_view

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/twitter/android/PromptView;->e:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/twitter/android/PromptView;->e:Landroid/widget/RelativeLayout;

    const v1, 0x7f090043    # com.twitter.android.R.id.header

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/PromptView;->f:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/PromptView;->e:Landroid/widget/RelativeLayout;

    const v1, 0x7f090226    # com.twitter.android.R.id.prompt_text

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/PromptView;->g:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/PromptView;->e:Landroid/widget/RelativeLayout;

    const v1, 0x7f090229    # com.twitter.android.R.id.click

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/PromptView;->h:Landroid/widget/Button;

    iget-object v0, p0, Lcom/twitter/android/PromptView;->e:Landroid/widget/RelativeLayout;

    const v1, 0x7f090228    # com.twitter.android.R.id.prompt_image

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/PromptView;->i:Landroid/widget/ImageView;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/PromptView;->j:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/twitter/android/PromptView;->h:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PromptView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/PromptView;->e:Landroid/widget/RelativeLayout;

    const v1, 0x7f0901a3    # com.twitter.android.R.id.dismiss

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/PromptView;->g:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/twitter/library/view/l;->a(Landroid/widget/TextView;)V

    iget-object v0, p0, Lcom/twitter/android/PromptView;->f:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/twitter/library/view/l;->a(Landroid/widget/TextView;)V

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/twitter/android/PromptView;->setVisibility(I)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/PromptView;)Lcom/twitter/library/api/Prompt;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PromptView;->b:Lcom/twitter/library/api/Prompt;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/PromptView;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PromptView;->l:Ljava/lang/String;

    return-object v0
.end method

.method private c()V
    .locals 8

    const/4 v7, 0x3

    const/4 v2, 0x0

    new-instance v3, Lcom/twitter/android/rc;

    invoke-direct {v3, p0}, Lcom/twitter/android/rc;-><init>(Lcom/twitter/android/PromptView;)V

    iget-object v0, p0, Lcom/twitter/android/PromptView;->j:Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    const v0, 0x7f090227    # com.twitter.android.R.id.photo_thumbnails_stub

    invoke-virtual {p0, v0}, Lcom/twitter/android/PromptView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/twitter/android/PromptView;->j:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/twitter/android/PromptView;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    move v1, v2

    :goto_0
    if-ge v1, v7, :cond_0

    const v0, 0x7f0300e6    # com.twitter.android.R.layout.photo_prompt_thumbnail

    iget-object v5, p0, Lcom/twitter/android/PromptView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v0, v5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setId(I)V

    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/twitter/android/PromptView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_1
    if-ge v1, v7, :cond_1

    iget-object v0, p0, Lcom/twitter/android/PromptView;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/PromptView;->b:Lcom/twitter/library/api/Prompt;

    iget-object v3, v3, Lcom/twitter/library/api/Prompt;->j:[I

    aget v3, v3, v1

    int-to-long v3, v3

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-static {v0, v3, v4, v5, v6}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v3

    iget-object v0, p0, Lcom/twitter/android/PromptView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/PromptView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/PromptView;->h:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/api/MediaEntity;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/api/Promotion;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/api/Prompt;Landroid/graphics/Bitmap;)V
    .locals 7

    const/16 v6, 0x8

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/PromptView;->a:Z

    iget-object v0, p0, Lcom/twitter/android/PromptView;->b:Lcom/twitter/library/api/Prompt;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PromptView;->b:Lcom/twitter/library/api/Prompt;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/PromptView;->b:Lcom/twitter/library/api/Prompt;

    invoke-virtual {v0, p1}, Lcom/twitter/library/api/Prompt;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iput-object p1, p0, Lcom/twitter/android/PromptView;->b:Lcom/twitter/library/api/Prompt;

    iput-object p2, p0, Lcom/twitter/android/PromptView;->c:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/twitter/android/PromptView;->b:Lcom/twitter/library/api/Prompt;

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/PromptView;->removeAllViews()V

    invoke-virtual {p0, v6}, Lcom/twitter/android/PromptView;->setVisibility(I)V

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/PromptView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/PromptView;->e:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v0}, Lcom/twitter/android/PromptView;->addView(Landroid/view/View;)V

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/PromptView;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b006b    # com.twitter.android.R.color.prefix

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    const v3, 0x7f0b0061    # com.twitter.android.R.color.link_selected

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    iget-object v0, p0, Lcom/twitter/android/PromptView;->b:Lcom/twitter/library/api/Prompt;

    iget-object v0, v0, Lcom/twitter/library/api/Prompt;->l:Lcom/twitter/library/api/TweetEntities;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/twitter/android/PromptView;->b:Lcom/twitter/library/api/Prompt;

    iget-object v0, v0, Lcom/twitter/library/api/Prompt;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/PromptView;->b:Lcom/twitter/library/api/Prompt;

    iget-object v4, v4, Lcom/twitter/library/api/Prompt;->l:Lcom/twitter/library/api/TweetEntities;

    invoke-static {v0, v4, p0, v2, v3}, Lcom/twitter/library/view/d;->a(Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/view/c;II)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_2
    iget-object v4, p0, Lcom/twitter/android/PromptView;->b:Lcom/twitter/library/api/Prompt;

    iget-object v4, v4, Lcom/twitter/library/api/Prompt;->k:Lcom/twitter/library/api/TweetEntities;

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/twitter/android/PromptView;->b:Lcom/twitter/library/api/Prompt;

    iget-object v4, v4, Lcom/twitter/library/api/Prompt;->a:Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/android/PromptView;->b:Lcom/twitter/library/api/Prompt;

    iget-object v5, v5, Lcom/twitter/library/api/Prompt;->k:Lcom/twitter/library/api/TweetEntities;

    invoke-static {v4, v5, p0, v2, v3}, Lcom/twitter/library/view/d;->a(Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/view/c;II)Ljava/lang/CharSequence;

    move-result-object v2

    :goto_3
    iget-object v3, p0, Lcom/twitter/android/PromptView;->f:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/PromptView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/PromptView;->h:Landroid/widget/Button;

    iget-object v2, p0, Lcom/twitter/android/PromptView;->b:Lcom/twitter/library/api/Prompt;

    iget-object v2, v2, Lcom/twitter/library/api/Prompt;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p1, Lcom/twitter/library/api/Prompt;->j:[I

    if-eqz v0, :cond_8

    invoke-direct {p0}, Lcom/twitter/android/PromptView;->c()V

    :cond_5
    :goto_4
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/twitter/android/PromptView;->c:Landroid/graphics/Bitmap;

    invoke-direct {v0, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    sget-object v2, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeX(Landroid/graphics/Shader$TileMode;)V

    iget-object v2, p0, Lcom/twitter/android/PromptView;->i:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, v1}, Lcom/twitter/android/PromptView;->setVisibility(I)V

    goto/16 :goto_1

    :cond_6
    iget-object v0, p0, Lcom/twitter/android/PromptView;->b:Lcom/twitter/library/api/Prompt;

    iget-object v0, v0, Lcom/twitter/library/api/Prompt;->d:Ljava/lang/String;

    goto :goto_2

    :cond_7
    iget-object v2, p0, Lcom/twitter/android/PromptView;->b:Lcom/twitter/library/api/Prompt;

    iget-object v2, v2, Lcom/twitter/library/api/Prompt;->a:Ljava/lang/String;

    goto :goto_3

    :cond_8
    iget-object v0, p0, Lcom/twitter/android/PromptView;->j:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/PromptView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_4
.end method

.method public a(Lcom/twitter/library/api/TweetClassicCard;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/api/UrlEntity;)V
    .locals 9

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/PromptView;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/PromptView;->d:Landroid/content/Context;

    invoke-static {v2}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    move-object v2, p1

    move-object v5, v1

    move-object v6, v1

    move-object v7, v1

    move-object v8, v1

    invoke-static/range {v0 .. v8}, Lcom/twitter/android/client/be;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/UrlEntity;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 0

    return-void
.end method

.method public a()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PromptView;->b:Lcom/twitter/library/api/Prompt;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(J)V
    .locals 0

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v4, :cond_0

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x40

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/PromptView;->d:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/PromptView;->d:Landroid/content/Context;

    const-class v3, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "screen_name"

    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x23

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/twitter/android/PromptView;->d:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/PromptView;->d:Landroid/content/Context;

    const-class v3, Lcom/twitter/android/SearchActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "query"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "q_source"

    const-string/jumbo v3, "hashtag_click"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "scribe_context"

    const-string/jumbo v3, "hashtag"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x24

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PromptView;->d:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/PromptView;->d:Landroid/content/Context;

    const-class v3, Lcom/twitter/android/SearchActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "query"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "q_source"

    const-string/jumbo v3, "cashtag_click"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "scribe_context"

    const-string/jumbo v3, "cashtag"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public b()Z
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/PromptView;->b:Lcom/twitter/library/api/Prompt;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    new-instance v1, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/twitter/android/PromptView;->k:Landroid/text/format/Time;

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    iget v2, v1, Landroid/text/format/Time;->second:I

    iget-object v3, p0, Lcom/twitter/android/PromptView;->b:Lcom/twitter/library/api/Prompt;

    iget v3, v3, Lcom/twitter/library/api/Prompt;->h:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/text/format/Time;->second:I

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->normalize(Z)J

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->after(Landroid/text/format/Time;)Z

    move-result v0

    goto :goto_0
.end method

.method public e()V
    .locals 0

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 14

    const/4 v13, 0x1

    const/4 v12, 0x0

    const/4 v1, 0x0

    iget-object v8, p0, Lcom/twitter/android/PromptView;->b:Lcom/twitter/library/api/Prompt;

    if-nez v8, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v9

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v10

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f0901a3    # com.twitter.android.R.id.dismiss

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/twitter/android/PromptView;->b:Lcom/twitter/library/api/Prompt;

    iget-object v2, v2, Lcom/twitter/library/api/Prompt;->j:[I

    if-eqz v2, :cond_2

    invoke-static {v0}, Lgp;->c(Landroid/content/Context;)V

    :cond_1
    :goto_1
    invoke-virtual {p0, v1, v1}, Lcom/twitter/android/PromptView;->a(Lcom/twitter/library/api/Prompt;Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_2
    new-array v0, v13, [Ljava/lang/String;

    const-string/jumbo v2, "ddg:android_prompt_on_empty_ptr_1554::dismiss:track"

    aput-object v2, v0, v12

    invoke-virtual {v9, v10, v11, v0}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget v0, v8, Lcom/twitter/library/api/Prompt;->b:I

    invoke-virtual {v9, v0}, Lcom/twitter/android/client/c;->e(I)V

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lcom/twitter/android/PromptView;->b:Lcom/twitter/library/api/Prompt;

    iget-object v2, v2, Lcom/twitter/library/api/Prompt;->j:[I

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/twitter/android/PromptView;->b:Lcom/twitter/library/api/Prompt;

    iget-object v2, v2, Lcom/twitter/library/api/Prompt;->f:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/twitter/android/PromptView;->b:Lcom/twitter/library/api/Prompt;

    iget-object v2, v2, Lcom/twitter/library/api/Prompt;->f:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    move-object v5, v1

    move-object v6, v1

    move-object v7, v1

    invoke-static/range {v0 .. v7}, Lcom/twitter/android/client/be;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    :cond_4
    new-array v0, v13, [Ljava/lang/String;

    const-string/jumbo v2, "ddg:android_prompt_on_empty_ptr_1554::click:track"

    aput-object v2, v0, v12

    invoke-virtual {v9, v10, v11, v0}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget v0, v8, Lcom/twitter/library/api/Prompt;->b:I

    invoke-virtual {v9, v0}, Lcom/twitter/android/client/c;->d(I)V

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    const/4 v5, 0x0

    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    iget-boolean v0, p0, Lcom/twitter/android/PromptView;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PromptView;->b:Lcom/twitter/library/api/Prompt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PromptView;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/PromptView;->b:Lcom/twitter/library/api/Prompt;

    iget-object v1, v1, Lcom/twitter/library/api/Prompt;->j:[I

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/PromptView;->d:Landroid/content/Context;

    invoke-static {v0}, Lgp;->b(Landroid/content/Context;)V

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/PromptView;->k:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    iput-boolean v5, p0, Lcom/twitter/android/PromptView;->a:Z

    :cond_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/PromptView;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "ddg:android_prompt_on_empty_ptr_1554::impression:track"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/PromptView;->b:Lcom/twitter/library/api/Prompt;

    iget v1, v1, Lcom/twitter/library/api/Prompt;->b:I

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->c(I)V

    goto :goto_0
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 10

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/twitter/android/PromptView;->b:Lcom/twitter/library/api/Prompt;

    if-nez v3, :cond_0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    :try_start_0
    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const/4 v6, 0x0

    invoke-virtual {v5, v0, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "\n\n--- User agent ---\n\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v4}, Lcom/twitter/library/network/aa;->a(Landroid/content/Context;)Lcom/twitter/library/network/aa;

    move-result-object v7

    iget-object v7, v7, Lcom/twitter/library/network/aa;->e:Lcom/twitter/library/network/ac;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "\n\n--- Prompt ---\n\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Landroid/content/Intent;

    const-string/jumbo v8, "android.intent.action.SEND"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v8, "text/plain"

    invoke-virtual {v7, v8}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    const-string/jumbo v8, "android.intent.extra.TEXT"

    invoke-virtual {v7, v8, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    const-string/jumbo v7, "android.intent.extra.SUBJECT"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Debug: Android v"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v8, ", "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v8, "prompt id "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, v3, Lcom/twitter/library/api/Prompt;->b:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "android.intent.extra.EMAIL"

    new-array v6, v2, [Ljava/lang/String;

    const-string/jumbo v7, "promptbird@twitter.com"

    aput-object v7, v6, v1

    invoke-virtual {v0, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x10000

    invoke-virtual {v5, v0, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v4, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_1
    move v1, v2

    goto/16 :goto_0

    :catch_0
    move-exception v0

    move v0, v1

    goto/16 :goto_1
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    check-cast p1, Lcom/twitter/android/PromptView$SavedState;

    invoke-virtual {p1}, Lcom/twitter/android/PromptView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-object v0, p1, Lcom/twitter/android/PromptView$SavedState;->b:Lcom/twitter/library/api/Prompt;

    iget-object v1, p1, Lcom/twitter/android/PromptView$SavedState;->c:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/PromptView;->a(Lcom/twitter/library/api/Prompt;Landroid/graphics/Bitmap;)V

    iget-boolean v0, p1, Lcom/twitter/android/PromptView$SavedState;->a:Z

    iput-boolean v0, p0, Lcom/twitter/android/PromptView;->a:Z

    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    new-instance v0, Lcom/twitter/android/PromptView$SavedState;

    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/PromptView$SavedState;-><init>(Landroid/os/Parcelable;)V

    iget-object v1, p0, Lcom/twitter/android/PromptView;->b:Lcom/twitter/library/api/Prompt;

    iput-object v1, v0, Lcom/twitter/android/PromptView$SavedState;->b:Lcom/twitter/library/api/Prompt;

    iget-boolean v1, p0, Lcom/twitter/android/PromptView;->a:Z

    iput-boolean v1, v0, Lcom/twitter/android/PromptView$SavedState;->a:Z

    iget-object v1, p0, Lcom/twitter/android/PromptView;->c:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/twitter/android/PromptView$SavedState;->c:Landroid/graphics/Bitmap;

    return-object v0
.end method
