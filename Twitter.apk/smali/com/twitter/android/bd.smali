.class Lcom/twitter/android/bd;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/wz;


# instance fields
.field final synthetic a:Lcom/twitter/android/BaseMessagesActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/BaseMessagesActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/bd;->a:Lcom/twitter/android/BaseMessagesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public a(I)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    rsub-int v0, p1, 0x8c

    iget-object v3, p0, Lcom/twitter/android/bd;->a:Lcom/twitter/android/BaseMessagesActivity;

    invoke-static {v3}, Lcom/twitter/android/BaseMessagesActivity;->a(Lcom/twitter/android/BaseMessagesActivity;)Lcom/twitter/android/PostStorage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/android/PostStorage;->g()Z

    move-result v3

    if-eqz v3, :cond_0

    add-int/lit8 v0, v0, -0x17

    :cond_0
    iget-object v3, p0, Lcom/twitter/android/bd;->a:Lcom/twitter/android/BaseMessagesActivity;

    invoke-static {v3}, Lcom/twitter/android/BaseMessagesActivity;->c(Lcom/twitter/android/BaseMessagesActivity;)Landroid/widget/TextView;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/twitter/android/bd;->a:Lcom/twitter/android/BaseMessagesActivity;

    if-ltz v0, :cond_1

    const/16 v4, 0x8c

    if-ge v0, v4, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v3, v0}, Lcom/twitter/android/BaseMessagesActivity;->a(Lcom/twitter/android/BaseMessagesActivity;Z)Z

    iget-object v0, p0, Lcom/twitter/android/bd;->a:Lcom/twitter/android/BaseMessagesActivity;

    invoke-static {v0}, Lcom/twitter/android/BaseMessagesActivity;->f(Lcom/twitter/android/BaseMessagesActivity;)Landroid/widget/Button;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/bd;->a:Lcom/twitter/android/BaseMessagesActivity;

    invoke-static {v3}, Lcom/twitter/android/BaseMessagesActivity;->d(Lcom/twitter/android/BaseMessagesActivity;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/twitter/android/bd;->a:Lcom/twitter/android/BaseMessagesActivity;

    invoke-static {v3}, Lcom/twitter/android/BaseMessagesActivity;->e(Lcom/twitter/android/BaseMessagesActivity;)Z

    move-result v3

    if-eqz v3, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public a(Ljava/lang/String;I)V
    .locals 0

    return-void
.end method

.method public a(Z)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/bd;->a:Lcom/twitter/android/BaseMessagesActivity;

    invoke-static {v0}, Lcom/twitter/android/BaseMessagesActivity;->g(Lcom/twitter/android/BaseMessagesActivity;)Lcom/twitter/android/MessagesComposeFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/bd;->a:Lcom/twitter/android/BaseMessagesActivity;

    invoke-static {v0}, Lcom/twitter/android/BaseMessagesActivity;->h(Lcom/twitter/android/BaseMessagesActivity;)Lcom/twitter/android/MessagesThreadFragment;

    move-result-object v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/bd;->a:Lcom/twitter/android/BaseMessagesActivity;

    invoke-static {v0}, Lcom/twitter/android/BaseMessagesActivity;->g(Lcom/twitter/android/BaseMessagesActivity;)Lcom/twitter/android/MessagesComposeFragment;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/MessagesComposeFragment;->onFocusChange(Landroid/view/View;Z)V

    :cond_0
    return-void
.end method

.method public a([Lcom/twitter/library/api/TwitterContact;)V
    .locals 0

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/bd;->a:Lcom/twitter/android/BaseMessagesActivity;

    invoke-static {v0}, Lcom/twitter/android/BaseMessagesActivity;->e(Lcom/twitter/android/BaseMessagesActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/bd;->a:Lcom/twitter/android/BaseMessagesActivity;

    invoke-virtual {v0}, Lcom/twitter/android/BaseMessagesActivity;->a()V

    :cond_0
    return-void
.end method

.method public d()V
    .locals 0

    return-void
.end method

.method public p_()V
    .locals 0

    return-void
.end method
