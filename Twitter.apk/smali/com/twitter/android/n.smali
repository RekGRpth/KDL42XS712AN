.class Lcom/twitter/android/n;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public a:Landroid/view/View;

.field public b:Ljava/util/ArrayList;

.field public c:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Ljava/util/ArrayList;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/n;->a:Landroid/view/View;

    iput-object p2, p0, Lcom/twitter/android/n;->b:Ljava/util/ArrayList;

    iput-object p3, p0, Lcom/twitter/android/n;->c:Landroid/view/View;

    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;Landroid/content/Context;Ljava/util/ArrayList;ZLandroid/view/View$OnClickListener;[ILjava/util/ArrayList;)Landroid/view/View;
    .locals 8

    const/4 v7, 0x0

    invoke-static {}, Lcom/twitter/android/h;->b()Landroid/widget/AbsListView$LayoutParams;

    move-result-object v0

    new-instance v1, Lcom/twitter/internal/android/widget/HighlightedLinearLayout;

    invoke-direct {v1, p1}, Lcom/twitter/internal/android/widget/HighlightedLinearLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    const/16 v0, 0x30

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setGravity(I)V

    aget v0, p5, v7

    invoke-static {p0, p4, v0}, Lcom/twitter/android/q;->a(Landroid/view/LayoutInflater;Landroid/view/View$OnClickListener;I)Landroid/view/View;

    move-result-object v3

    invoke-static {}, Lcom/twitter/android/h;->c()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v0

    const v4, 0x7f0c000a    # com.twitter.android.R.dimen.activity_header_margin_top

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    invoke-virtual {v0, v7, v4, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {v1, v3, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const v0, 0x7f0c000c    # com.twitter.android.R.dimen.activity_multi_row_margin_bottom

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {}, Lcom/twitter/android/h;->c()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v6

    invoke-virtual {v6, v7, v7, v7, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {v1, v0, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    if-eqz p3, :cond_1

    const/4 v0, 0x2

    aget v0, p5, v0

    invoke-static {p0, p4, v0, p6}, Lcom/twitter/android/k;->a(Landroid/view/LayoutInflater;Landroid/view/View$OnClickListener;ILjava/util/ArrayList;)Landroid/view/View;

    move-result-object v0

    invoke-static {}, Lcom/twitter/android/h;->c()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v4

    invoke-virtual {v1, v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const v4, 0x7f0c000b    # com.twitter.android.R.dimen.activity_multi_padding_bottom_with_view_all

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-virtual {v1, v7, v7, v7, v2}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    :cond_1
    new-instance v2, Lcom/twitter/android/n;

    invoke-direct {v2, v3, p2, v0}, Lcom/twitter/android/n;-><init>(Landroid/view/View;Ljava/util/ArrayList;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    return-object v1
.end method
