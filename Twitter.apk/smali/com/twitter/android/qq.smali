.class public Lcom/twitter/android/qq;
.super Landroid/widget/BaseAdapter;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/android/ProfileFragment;

.field private c:Lcom/twitter/library/api/TwitterUser;

.field private d:I

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/android/ProfileFragment;)V
    .locals 1

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/qq;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/twitter/android/qq;->b:Lcom/twitter/android/ProfileFragment;

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/qq;->d:I

    return-void
.end method

.method private b()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/qq;->b:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->L(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/qq;->d:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/twitter/android/qq;->c:Lcom/twitter/library/api/TwitterUser;

    iget-wide v1, v1, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->j(J)Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/qq;->b:Lcom/twitter/android/ProfileFragment;

    iget-object v1, p0, Lcom/twitter/android/qq;->b:Lcom/twitter/android/ProfileFragment;

    iget v1, v1, Lcom/twitter/android/ProfileFragment;->H:I

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v1

    iput v1, v0, Lcom/twitter/android/ProfileFragment;->H:I

    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/qq;->a(Z)V

    iget-object v0, p0, Lcom/twitter/android/qq;->b:Lcom/twitter/android/ProfileFragment;

    iget-object v1, p0, Lcom/twitter/android/qq;->b:Lcom/twitter/android/ProfileFragment;

    iget v1, v1, Lcom/twitter/android/ProfileFragment;->H:I

    const/16 v2, 0x20

    invoke-static {v1, v2}, Lcom/twitter/library/provider/ay;->b(II)I

    move-result v1

    iput v1, v0, Lcom/twitter/android/ProfileFragment;->H:I

    return-void

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/qq;->c:Lcom/twitter/library/api/TwitterUser;

    iget-wide v1, v1, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->k(J)Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/twitter/android/qq;->d:I

    return v0
.end method

.method public a(I)V
    .locals 1

    iget v0, p0, Lcom/twitter/android/qq;->d:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/twitter/android/qq;->d:I

    invoke-virtual {p0}, Lcom/twitter/android/qq;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/api/TwitterUser;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/qq;->c:Lcom/twitter/library/api/TwitterUser;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qq;->c:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v0, p1}, Lcom/twitter/library/api/TwitterUser;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/twitter/android/qq;->c:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {p0}, Lcom/twitter/android/qq;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/qq;->e:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/twitter/android/qq;->e:Z

    invoke-virtual {p0}, Lcom/twitter/android/qq;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/qq;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    const/4 v5, 0x0

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qq;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300f5    # com.twitter.android.R.layout.protected_follow

    invoke-virtual {v0, v1, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance v0, Lcom/twitter/android/qr;

    invoke-direct {v0, p2}, Lcom/twitter/android/qr;-><init>(Landroid/view/View;)V

    iget-object v1, v0, Lcom/twitter/android/qr;->b:Lcom/twitter/internal/android/widget/ShadowTextView;

    invoke-virtual {v1, p0}, Lcom/twitter/internal/android/widget/ShadowTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Lcom/twitter/android/qr;->c:Lcom/twitter/internal/android/widget/ShadowTextView;

    invoke-virtual {v1, p0}, Lcom/twitter/internal/android/widget/ShadowTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v1, v0, Lcom/twitter/android/qr;->c:Lcom/twitter/internal/android/widget/ShadowTextView;

    invoke-virtual {v1, v0}, Lcom/twitter/internal/android/widget/ShadowTextView;->setTag(Ljava/lang/Object;)V

    iget-object v1, v0, Lcom/twitter/android/qr;->b:Lcom/twitter/internal/android/widget/ShadowTextView;

    invoke-virtual {v1, v0}, Lcom/twitter/internal/android/widget/ShadowTextView;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/qq;->c:Lcom/twitter/library/api/TwitterUser;

    if-eqz v1, :cond_1

    iget-object v0, v0, Lcom/twitter/android/qr;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/qq;->a:Landroid/content/Context;

    const v2, 0x7f0f0336    # com.twitter.android.R.string.protected_follower_request

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/twitter/android/qq;->c:Lcom/twitter/library/api/TwitterUser;

    iget-object v4, v4, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-object p2

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/qr;

    goto :goto_0

    :cond_1
    iget-object v0, v0, Lcom/twitter/android/qr;->a:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f09002c    # com.twitter.android.R.id.action_button

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Lcom/twitter/android/qq;->d:I

    invoke-direct {p0}, Lcom/twitter/android/qq;->b()V

    return-void

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method
