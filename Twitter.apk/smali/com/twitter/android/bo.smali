.class Lcom/twitter/android/bo;
.super Landroid/widget/ArrayAdapter;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/card/h;


# instance fields
.field final a:Ljava/text/DateFormat;

.field final b:Ljava/util/HashMap;

.field final c:Landroid/graphics/drawable/Drawable;

.field final d:Landroid/os/Handler;

.field final e:Ljava/lang/Runnable;

.field final synthetic f:Lcom/twitter/android/CardDebugFragment;


# direct methods
.method public constructor <init>(Lcom/twitter/android/CardDebugFragment;Landroid/content/Context;)V
    .locals 7

    iput-object p1, p0, Lcom/twitter/android/bo;->f:Lcom/twitter/android/CardDebugFragment;

    const v0, 0x7f03001f    # com.twitter.android.R.layout.card_debug_log_item

    invoke-direct {p0, p2, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    new-instance v0, Lcom/twitter/android/bp;

    invoke-direct {v0, p0}, Lcom/twitter/android/bp;-><init>(Lcom/twitter/android/bo;)V

    iput-object v0, p0, Lcom/twitter/android/bo;->e:Ljava/lang/Runnable;

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "HH:mm:ss.SSS"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/bo;->a:Ljava/text/DateFormat;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020139    # com.twitter.android.R.drawable.ic_card_details

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/bo;->c:Landroid/graphics/drawable/Drawable;

    new-instance v1, Lcom/twitter/android/bs;

    const v2, 0x7f02013a    # com.twitter.android.R.drawable.ic_card_error

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const v3, 0x7f0b003c    # com.twitter.android.R.color.debug_card_previewer_error_icon

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    const v4, 0x7f100029    # com.twitter.android.R.style.CardDebugLogStyle_Error

    invoke-direct {v1, v2, v3, v4}, Lcom/twitter/android/bs;-><init>(Landroid/graphics/drawable/Drawable;II)V

    new-instance v2, Lcom/twitter/android/bs;

    const v3, 0x7f02013f    # com.twitter.android.R.drawable.ic_card_warning

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const v4, 0x7f0b0040    # com.twitter.android.R.color.debug_card_previewer_warning_icon

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    const v5, 0x7f10002b    # com.twitter.android.R.style.CardDebugLogStyle_Warning

    invoke-direct {v2, v3, v4, v5}, Lcom/twitter/android/bs;-><init>(Landroid/graphics/drawable/Drawable;II)V

    new-instance v3, Lcom/twitter/android/bs;

    const v4, 0x7f02013b    # com.twitter.android.R.drawable.ic_card_info

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    const v5, 0x7f0b003e    # com.twitter.android.R.color.debug_card_previewer_info_icon

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    const v6, 0x7f10002a    # com.twitter.android.R.style.CardDebugLogStyle_Info

    invoke-direct {v3, v4, v5, v6}, Lcom/twitter/android/bs;-><init>(Landroid/graphics/drawable/Drawable;II)V

    new-instance v4, Lcom/twitter/android/bs;

    const v5, 0x7f020138    # com.twitter.android.R.drawable.ic_card_debug

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    const v6, 0x7f0b0038    # com.twitter.android.R.color.debug_card_previewer_debug_icon

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    const v6, 0x7f100028    # com.twitter.android.R.style.CardDebugLogStyle_Debug

    invoke-direct {v4, v5, v0, v6}, Lcom/twitter/android/bs;-><init>(Landroid/graphics/drawable/Drawable;II)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/bo;->b:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/twitter/android/bo;->b:Ljava/util/HashMap;

    sget-object v5, Lcom/twitter/library/card/CardDebugLog$Severity;->d:Lcom/twitter/library/card/CardDebugLog$Severity;

    invoke-virtual {v0, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/twitter/android/bo;->b:Ljava/util/HashMap;

    sget-object v1, Lcom/twitter/library/card/CardDebugLog$Severity;->c:Lcom/twitter/library/card/CardDebugLog$Severity;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/twitter/android/bo;->b:Ljava/util/HashMap;

    sget-object v1, Lcom/twitter/library/card/CardDebugLog$Severity;->b:Lcom/twitter/library/card/CardDebugLog$Severity;

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/twitter/android/bo;->b:Ljava/util/HashMap;

    sget-object v1, Lcom/twitter/library/card/CardDebugLog$Severity;->a:Lcom/twitter/library/card/CardDebugLog$Severity;

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/bo;->d:Landroid/os/Handler;

    invoke-static {}, Lcom/twitter/library/card/CardDebugLog;->h()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/i;

    invoke-virtual {p0, v0}, Lcom/twitter/android/bo;->add(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/bo;->d:Landroid/os/Handler;

    new-instance v1, Lcom/twitter/android/br;

    invoke-direct {v1, p0}, Lcom/twitter/android/br;-><init>(Lcom/twitter/android/bo;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public a(Lcom/twitter/library/card/i;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/bo;->d:Landroid/os/Handler;

    new-instance v1, Lcom/twitter/android/bq;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/bq;-><init>(Lcom/twitter/android/bo;Lcom/twitter/library/card/i;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method b()V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/bo;->notifyDataSetChanged()V

    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    const/4 v3, 0x0

    if-nez p2, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/bo;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f03001f    # com.twitter.android.R.layout.card_debug_log_item

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    new-instance v2, Lcom/twitter/android/bt;

    const v0, 0x7f0900b6    # com.twitter.android.R.id.timestamp

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0900b7    # com.twitter.android.R.id.entry

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-direct {v2, v0, v1}, Lcom/twitter/android/bt;-><init>(Landroid/widget/TextView;Landroid/widget/TextView;)V

    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bt;

    invoke-virtual {p0, p1}, Lcom/twitter/android/bo;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/card/i;

    const-wide/16 v4, 0x65

    if-lez p1, :cond_1

    add-int/lit8 v2, p1, -0x1

    invoke-virtual {p0, v2}, Lcom/twitter/android/bo;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/card/i;

    iget-object v4, v1, Lcom/twitter/library/card/i;->d:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    iget-object v2, v2, Lcom/twitter/library/card/i;->d:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    :cond_1
    if-eqz p1, :cond_2

    const-wide/16 v6, 0x64

    cmp-long v2, v4, v6

    if-lez v2, :cond_3

    :cond_2
    iget-object v2, v0, Lcom/twitter/android/bt;->a:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/twitter/android/bo;->a:Ljava/text/DateFormat;

    iget-object v5, v1, Lcom/twitter/library/card/i;->d:Ljava/util/Date;

    invoke-virtual {v4, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v0, Lcom/twitter/android/bt;->a:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    iget-object v2, p0, Lcom/twitter/android/bo;->b:Ljava/util/HashMap;

    iget-object v4, v1, Lcom/twitter/library/card/i;->a:Lcom/twitter/library/card/CardDebugLog$Severity;

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/bs;

    iget-object v4, v0, Lcom/twitter/android/bt;->b:Landroid/widget/TextView;

    iget-object v5, v1, Lcom/twitter/library/card/i;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, v0, Lcom/twitter/android/bt;->b:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/twitter/android/bo;->f:Lcom/twitter/android/CardDebugFragment;

    invoke-virtual {v5}, Lcom/twitter/android/CardDebugFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    iget v6, v2, Lcom/twitter/android/bs;->b:I

    invoke-virtual {v4, v5, v6}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    iget-object v4, v0, Lcom/twitter/android/bt;->b:Landroid/widget/TextView;

    iget-object v2, v2, Lcom/twitter/android/bs;->a:Landroid/graphics/drawable/Drawable;

    iget-object v0, v1, Lcom/twitter/library/card/i;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v0, v3

    :goto_1
    invoke-virtual {v4, v2, v3, v0, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    return-object p2

    :cond_3
    iget-object v2, v0, Lcom/twitter/android/bt;->a:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/bo;->c:Landroid/graphics/drawable/Drawable;

    goto :goto_1
.end method
