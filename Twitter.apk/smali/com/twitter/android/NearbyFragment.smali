.class public Lcom/twitter/android/NearbyFragment;
.super Lcom/twitter/android/TimelineFragment;
.source "Twttr"

# interfaces
.implements Lcom/google/android/gms/maps/i;
.implements Lcom/google/android/gms/maps/l;


# instance fields
.field private J:Landroid/widget/ImageView;

.field private K:Landroid/widget/RelativeLayout;

.field private L:Landroid/os/Handler;

.field private U:Ljava/lang/Runnable;

.field private V:Lcom/twitter/android/nd;

.field private W:Ljava/util/HashMap;

.field private X:Ljava/util/HashMap;

.field private Y:Landroid/content/SharedPreferences;

.field private final Z:Lcom/google/android/gms/maps/h;

.field private a:Lcom/twitter/library/platform/LocationProducer;

.field private final aa:Lcom/google/android/gms/maps/h;

.field private final ab:Lcom/twitter/library/platform/i;

.field private final ac:Lcom/twitter/library/platform/i;

.field private final ad:Landroid/view/animation/TranslateAnimation;

.field private final ae:Landroid/view/animation/TranslateAnimation;

.field private b:Lcom/twitter/android/ns;

.field private c:Landroid/widget/RelativeLayout;

.field private g:Landroid/widget/RelativeLayout;

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:[D

.field private q:F

.field private r:I

.field private s:I

.field private t:Lcom/google/android/gms/maps/c;

.field private u:Lcom/google/android/gms/maps/SupportMapFragment;

.field private v:Lcom/google/android/gms/maps/model/CameraPosition;

.field private w:Lcom/google/android/gms/maps/model/k;

.field private x:Landroid/graphics/Bitmap;

.field private y:Landroid/widget/Button;

.field private z:Landroid/widget/ImageButton;


# direct methods
.method public constructor <init>()V
    .locals 9

    invoke-direct {p0}, Lcom/twitter/android/TimelineFragment;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/NearbyFragment;->W:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/NearbyFragment;->X:Ljava/util/HashMap;

    new-instance v0, Lcom/twitter/android/nh;

    invoke-direct {v0, p0}, Lcom/twitter/android/nh;-><init>(Lcom/twitter/android/NearbyFragment;)V

    iput-object v0, p0, Lcom/twitter/android/NearbyFragment;->Z:Lcom/google/android/gms/maps/h;

    new-instance v0, Lcom/twitter/android/ni;

    invoke-direct {v0, p0}, Lcom/twitter/android/ni;-><init>(Lcom/twitter/android/NearbyFragment;)V

    iput-object v0, p0, Lcom/twitter/android/NearbyFragment;->aa:Lcom/google/android/gms/maps/h;

    new-instance v0, Lcom/twitter/android/nj;

    invoke-direct {v0, p0}, Lcom/twitter/android/nj;-><init>(Lcom/twitter/android/NearbyFragment;)V

    iput-object v0, p0, Lcom/twitter/android/NearbyFragment;->ab:Lcom/twitter/library/platform/i;

    new-instance v0, Lcom/twitter/android/nk;

    invoke-direct {v0, p0}, Lcom/twitter/android/nk;-><init>(Lcom/twitter/android/NearbyFragment;)V

    iput-object v0, p0, Lcom/twitter/android/NearbyFragment;->ac:Lcom/twitter/library/platform/i;

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    iput-object v0, p0, Lcom/twitter/android/NearbyFragment;->ad:Landroid/view/animation/TranslateAnimation;

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    iput-object v0, p0, Lcom/twitter/android/NearbyFragment;->ae:Landroid/view/animation/TranslateAnimation;

    return-void
.end method

.method private G()V
    .locals 3

    const v2, 0x7f0901ee    # com.twitter.android.R.id.map

    iget-boolean v0, p0, Lcom/twitter/android/NearbyFragment;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->u:Lcom/google/android/gms/maps/SupportMapFragment;

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/NearbyFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/SupportMapFragment;

    iput-object v0, p0, Lcom/twitter/android/NearbyFragment;->u:Lcom/google/android/gms/maps/SupportMapFragment;

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->u:Lcom/google/android/gms/maps/SupportMapFragment;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/maps/SupportMapFragment;

    invoke-direct {v0}, Lcom/google/android/gms/maps/SupportMapFragment;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/NearbyFragment;->u:Lcom/google/android/gms/maps/SupportMapFragment;

    invoke-virtual {p0}, Lcom/twitter/android/NearbyFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/NearbyFragment;->u:Lcom/google/android/gms/maps/SupportMapFragment;

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    invoke-direct {p0}, Lcom/twitter/android/NearbyFragment;->H()V

    goto :goto_0
.end method

.method private H()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->u:Lcom/google/android/gms/maps/SupportMapFragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->u:Lcom/google/android/gms/maps/SupportMapFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/SupportMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->d()Lcom/google/android/gms/maps/u;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/u;->b(Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/u;->f(Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/u;->d(Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/u;->e(Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/u;->a(Z)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/i;)V

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/l;)V

    :cond_1
    return-void
.end method

.method private I()V
    .locals 13

    const/4 v1, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->V:Lcom/twitter/android/nd;

    invoke-virtual {v0}, Lcom/twitter/android/nd;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/NearbyFragment;->i:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/NearbyFragment;->H()V

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->g:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_4

    const/4 v2, 0x0

    iget v0, p0, Lcom/twitter/android/NearbyFragment;->s:I

    int-to-float v4, v0

    const-wide/16 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/NearbyFragment;->a(IFIFJ)V

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->g:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->a:Lcom/twitter/library/platform/LocationProducer;

    invoke-virtual {v0}, Lcom/twitter/library/platform/LocationProducer;->a()Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/i;)V

    iget-object v2, p0, Lcom/twitter/android/NearbyFragment;->v:Lcom/google/android/gms/maps/model/CameraPosition;

    if-eqz v2, :cond_3

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->v:Lcom/google/android/gms/maps/model/CameraPosition;

    iget-object v0, v0, Lcom/google/android/gms/maps/model/CameraPosition;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v5, v0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->v:Lcom/google/android/gms/maps/model/CameraPosition;

    iget-object v0, v0, Lcom/google/android/gms/maps/model/CameraPosition;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v7, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->v:Lcom/google/android/gms/maps/model/CameraPosition;

    iget v9, v0, Lcom/google/android/gms/maps/model/CameraPosition;->b:F

    iget-object v10, p0, Lcom/twitter/android/NearbyFragment;->aa:Lcom/google/android/gms/maps/h;

    iget-boolean v0, p0, Lcom/twitter/android/NearbyFragment;->l:Z

    if-nez v0, :cond_2

    move v11, v1

    :goto_1
    move-object v4, p0

    move v12, v3

    invoke-direct/range {v4 .. v12}, Lcom/twitter/android/NearbyFragment;->a(DDFLcom/google/android/gms/maps/h;ZI)V

    goto :goto_0

    :cond_2
    move v11, v3

    goto :goto_1

    :cond_3
    const/high16 v2, 0x41400000    # 12.0f

    iput v2, p0, Lcom/twitter/android/NearbyFragment;->q:F

    new-instance v2, Lcom/google/android/gms/maps/model/c;

    invoke-direct {v2}, Lcom/google/android/gms/maps/model/c;-><init>()V

    iget v4, p0, Lcom/twitter/android/NearbyFragment;->q:F

    invoke-virtual {v2, v4}, Lcom/google/android/gms/maps/model/c;->a(F)Lcom/google/android/gms/maps/model/c;

    move-result-object v2

    new-instance v4, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v5

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v7

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v2, v4}, Lcom/google/android/gms/maps/model/c;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/maps/model/c;->a()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/NearbyFragment;->v:Lcom/google/android/gms/maps/model/CameraPosition;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v5

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v7

    iget v9, p0, Lcom/twitter/android/NearbyFragment;->q:F

    iget-object v10, p0, Lcom/twitter/android/NearbyFragment;->aa:Lcom/google/android/gms/maps/h;

    move-object v4, p0

    move v11, v1

    move v12, v3

    invoke-direct/range {v4 .. v12}, Lcom/twitter/android/NearbyFragment;->a(DDFLcom/google/android/gms/maps/h;ZI)V

    goto/16 :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/twitter/android/NearbyFragment;->J()V

    goto/16 :goto_0
.end method

.method private J()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/NearbyFragment;->j:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/NearbyFragment;->ag()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/NearbyFragment;->j:Z

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->W:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/k;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/k;->a()V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->W:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->w:Lcom/google/android/gms/maps/model/k;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->a:Lcom/twitter/library/platform/LocationProducer;

    iget-object v1, p0, Lcom/twitter/android/NearbyFragment;->ab:Lcom/twitter/library/platform/i;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/platform/LocationProducer;->a(Lcom/twitter/library/platform/i;J)V

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->e()Lcom/google/android/gms/maps/r;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/nr;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/r;->a()Lcom/google/android/gms/maps/model/VisibleRegion;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/maps/model/VisibleRegion;->e:Lcom/google/android/gms/maps/model/LatLngBounds;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/nr;-><init>(Lcom/twitter/android/NearbyFragment;Lcom/google/android/gms/maps/model/LatLngBounds;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lcom/twitter/android/nr;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private K()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->a:Lcom/twitter/library/platform/LocationProducer;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->a:Lcom/twitter/library/platform/LocationProducer;

    iget-object v1, p0, Lcom/twitter/android/NearbyFragment;->ac:Lcom/twitter/library/platform/i;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/platform/LocationProducer;->a(Lcom/twitter/library/platform/i;J)V

    goto :goto_0
.end method

.method private L()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->b()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    iget v0, v0, Lcom/google/android/gms/maps/model/CameraPosition;->b:F

    invoke-direct {p0, v0}, Lcom/twitter/android/NearbyFragment;->a(F)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/twitter/android/NearbyFragment;->j:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/NearbyFragment;->i()V

    iput v0, p0, Lcom/twitter/android/NearbyFragment;->q:F

    goto :goto_0
.end method

.method private M()[D
    .locals 12

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->a:Lcom/twitter/library/platform/LocationProducer;

    invoke-virtual {v0}, Lcom/twitter/library/platform/LocationProducer;->a()Landroid/location/Location;

    move-result-object v3

    if-nez v3, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x4

    new-array v2, v0, [D

    invoke-virtual {v3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    const-wide v4, 0x3f9eb851eb851eb8L    # 0.03

    sub-double/2addr v0, v4

    const/4 v4, 0x0

    const-wide v5, -0x3fa9800000000000L    # -90.0

    cmpg-double v5, v0, v5

    if-gez v5, :cond_1

    const-wide v5, 0x4056800000000000L    # 90.0

    add-double/2addr v0, v5

    :cond_1
    aput-wide v0, v2, v4

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    const-wide v4, 0x3f9eb851eb851eb8L    # 0.03

    sub-double/2addr v0, v4

    const/4 v4, 0x1

    const-wide v5, -0x3f99800000000000L    # -180.0

    cmpg-double v5, v0, v5

    if-gez v5, :cond_2

    const-wide v5, 0x4066800000000000L    # 180.0

    add-double/2addr v0, v5

    :cond_2
    aput-wide v0, v2, v4

    invoke-virtual {v3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    const-wide v4, 0x3f9eb851eb851eb8L    # 0.03

    add-double/2addr v0, v4

    const/4 v4, 0x2

    const-wide v5, 0x4056800000000000L    # 90.0

    cmpl-double v5, v0, v5

    if-lez v5, :cond_3

    const-wide v5, 0x4056800000000000L    # 90.0

    sub-double/2addr v0, v5

    :cond_3
    aput-wide v0, v2, v4

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    const-wide v3, 0x3f9eb851eb851eb8L    # 0.03

    add-double/2addr v0, v3

    const/4 v3, 0x3

    const-wide v4, 0x4066800000000000L    # 180.0

    cmpl-double v4, v0, v4

    if-lez v4, :cond_4

    const-wide v4, 0x4066800000000000L    # 180.0

    sub-double/2addr v0, v4

    :cond_4
    aput-wide v0, v2, v3

    move-object v0, v2

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->e()Lcom/google/android/gms/maps/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/r;->a()Lcom/google/android/gms/maps/model/VisibleRegion;

    move-result-object v0

    if-nez v0, :cond_6

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_6
    const/4 v1, 0x4

    new-array v2, v1, [D

    iget-object v3, v0, Lcom/google/android/gms/maps/model/VisibleRegion;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-object v4, v0, Lcom/google/android/gms/maps/model/VisibleRegion;->d:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v0, v4, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-wide v5, v3, Lcom/google/android/gms/maps/model/LatLng;->a:D

    sub-double/2addr v0, v5

    const-wide/high16 v5, 0x4010000000000000L    # 4.0

    div-double v5, v0, v5

    iget-wide v0, v4, Lcom/google/android/gms/maps/model/LatLng;->b:D

    iget-wide v7, v3, Lcom/google/android/gms/maps/model/LatLng;->b:D

    sub-double/2addr v0, v7

    const-wide/16 v7, 0x0

    cmpg-double v7, v0, v7

    if-gez v7, :cond_7

    const-wide v7, 0x4076800000000000L    # 360.0

    add-double/2addr v0, v7

    :cond_7
    const-wide/high16 v7, 0x4010000000000000L    # 4.0

    div-double/2addr v0, v7

    new-instance v7, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v8, v3, Lcom/google/android/gms/maps/model/LatLng;->a:D

    sub-double/2addr v8, v5

    iget-wide v10, v3, Lcom/google/android/gms/maps/model/LatLng;->b:D

    sub-double/2addr v10, v0

    invoke-direct {v7, v8, v9, v10, v11}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    new-instance v3, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v8, v4, Lcom/google/android/gms/maps/model/LatLng;->a:D

    add-double/2addr v5, v8

    iget-wide v8, v4, Lcom/google/android/gms/maps/model/LatLng;->b:D

    add-double/2addr v0, v8

    invoke-direct {v3, v5, v6, v0, v1}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    const/4 v0, 0x0

    iget-wide v4, v7, Lcom/google/android/gms/maps/model/LatLng;->a:D

    aput-wide v4, v2, v0

    const/4 v0, 0x1

    iget-wide v4, v7, Lcom/google/android/gms/maps/model/LatLng;->b:D

    aput-wide v4, v2, v0

    const/4 v0, 0x2

    iget-wide v4, v3, Lcom/google/android/gms/maps/model/LatLng;->a:D

    aput-wide v4, v2, v0

    const/4 v0, 0x3

    iget-wide v3, v3, Lcom/google/android/gms/maps/model/LatLng;->b:D

    aput-wide v3, v2, v0

    move-object v0, v2

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/NearbyFragment;DD)D
    .locals 2

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/NearbyFragment;->b(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic a(Lcom/twitter/android/NearbyFragment;)Lcom/google/android/gms/maps/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    return-object v0
.end method

.method private a(Landroid/location/Location;Lcom/google/android/gms/maps/model/a;Z)Lcom/google/android/gms/maps/model/k;
    .locals 5

    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-direct {p0, v0, p2, p3}, Lcom/twitter/android/NearbyFragment;->a(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/a;Z)Lcom/google/android/gms/maps/model/k;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/a;Z)Lcom/google/android/gms/maps/model/k;
    .locals 4

    const/high16 v3, 0x3f000000    # 0.5f

    if-nez p2, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/gms/maps/model/b;->a(F)Lcom/google/android/gms/maps/model/a;

    move-result-object p2

    :cond_0
    new-instance v0, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    invoke-virtual {v0, p2}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/a;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Z)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v1

    invoke-virtual {v1, v3, v3}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(FF)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/google/android/gms/maps/model/MarkerOptions;->b(Z)Lcom/google/android/gms/maps/model/MarkerOptions;

    iget-object v1, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/k;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/NearbyFragment;Landroid/location/Location;Lcom/google/android/gms/maps/model/a;Z)Lcom/google/android/gms/maps/model/k;
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/NearbyFragment;->a(Landroid/location/Location;Lcom/google/android/gms/maps/model/a;Z)Lcom/google/android/gms/maps/model/k;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/NearbyFragment;Lcom/google/android/gms/maps/model/k;)Lcom/google/android/gms/maps/model/k;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/NearbyFragment;->w:Lcom/google/android/gms/maps/model/k;

    return-object p1
.end method

.method private a(DDFLcom/google/android/gms/maps/h;ZI)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    const/4 v1, 0x0

    cmpg-float v1, p5, v1

    if-gtz v1, :cond_2

    invoke-static {v0}, Lcom/google/android/gms/maps/b;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/a;

    move-result-object v0

    :goto_1
    if-eqz p7, :cond_4

    if-gtz p8, :cond_3

    iget-object v1, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    invoke-virtual {v1, v0, p6}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/a;Lcom/google/android/gms/maps/h;)V

    goto :goto_0

    :cond_2
    invoke-static {v0, p5}, Lcom/google/android/gms/maps/b;->a(Lcom/google/android/gms/maps/model/LatLng;F)Lcom/google/android/gms/maps/a;

    move-result-object v0

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    invoke-virtual {v1, v0, p8, p6}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/a;ILcom/google/android/gms/maps/h;)V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/a;)V

    if-eqz p6, :cond_0

    invoke-interface {p6}, Lcom/google/android/gms/maps/h;->a()V

    goto :goto_0
.end method

.method private a(FFLcom/google/android/gms/maps/h;I)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    if-eqz v0, :cond_0

    cmpl-float v0, p1, v1

    if-nez v0, :cond_1

    cmpl-float v0, p2, v1

    if-nez v0, :cond_1

    if-eqz p3, :cond_1

    invoke-interface {p3}, Lcom/google/android/gms/maps/h;->a()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-lez p4, :cond_2

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    invoke-static {p1, p2}, Lcom/google/android/gms/maps/b;->a(FF)Lcom/google/android/gms/maps/a;

    move-result-object v1

    invoke-virtual {v0, v1, p4, p3}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/a;ILcom/google/android/gms/maps/h;)V

    goto :goto_0

    :cond_2
    if-nez p4, :cond_3

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    invoke-static {p1, p2}, Lcom/google/android/gms/maps/b;->a(FF)Lcom/google/android/gms/maps/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/a;)V

    if-eqz p3, :cond_0

    invoke-interface {p3}, Lcom/google/android/gms/maps/h;->a()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    invoke-static {p1, p2}, Lcom/google/android/gms/maps/b;->a(FF)Lcom/google/android/gms/maps/a;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/a;Lcom/google/android/gms/maps/h;)V

    goto :goto_0
.end method

.method private a(IFIFJ)V
    .locals 9

    const/4 v2, 0x0

    const/4 v1, 0x1

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    move v3, v1

    move v4, v2

    move v5, p1

    move v6, p2

    move v7, p3

    move v8, p4

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    const-wide/16 v1, 0x0

    cmp-long v1, p5, v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, p5, p6}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/NearbyFragment;->g:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    if-nez p3, :cond_1

    float-to-int v0, p4

    iput v0, p0, Lcom/twitter/android/NearbyFragment;->r:I

    :cond_1
    return-void
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 9

    const/4 v1, 0x1

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/twitter/android/NearbyFragment;->i:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/NearbyFragment;->h:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/NearbyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0c009d    # com.twitter.android.R.dimen.nav_bar_height

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p0}, Lcom/twitter/android/NearbyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v4, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    sub-int/2addr v2, v0

    invoke-virtual {p0}, Lcom/twitter/android/NearbyFragment;->ao()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/internal/android/widget/ToolBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    sub-int/2addr v2, v5

    div-int/lit8 v4, v4, 0x2

    invoke-virtual {p0}, Lcom/twitter/android/NearbyFragment;->V()Landroid/widget/ListView;

    move-result-object v5

    new-instance v6, Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/twitter/android/NearbyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v7, Landroid/widget/AbsListView$LayoutParams;

    const/4 v8, -0x1

    invoke-direct {v7, v8, v4}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v7, Lcom/twitter/android/np;

    invoke-direct {v7, p0}, Lcom/twitter/android/np;-><init>(Lcom/twitter/android/NearbyFragment;)V

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v0, v4

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/twitter/android/NearbyFragment;->s:I

    iput-boolean v1, p0, Lcom/twitter/android/NearbyFragment;->i:Z

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/NearbyFragment;->H()V

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    if-eqz v0, :cond_0

    const/4 v2, 0x0

    iget v0, p0, Lcom/twitter/android/NearbyFragment;->s:I

    int-to-float v4, v0

    const-wide/16 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/NearbyFragment;->a(IFIFJ)V

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->g:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/NearbyFragment;DDFLcom/google/android/gms/maps/h;ZI)V
    .locals 0

    invoke-direct/range {p0 .. p8}, Lcom/twitter/android/NearbyFragment;->a(DDFLcom/google/android/gms/maps/h;ZI)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/NearbyFragment;IFIFJ)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/twitter/android/NearbyFragment;->a(IFIFJ)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/NearbyFragment;ZZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/NearbyFragment;->a(ZZ)V

    return-void
.end method

.method private a(ZZ)V
    .locals 7

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->ad:Landroid/view/animation/TranslateAnimation;

    const-wide/16 v1, 0x12c

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->ad:Landroid/view/animation/TranslateAnimation;

    new-instance v1, Lcom/twitter/android/nq;

    invoke-direct {v1, p0}, Lcom/twitter/android/nq;-><init>(Lcom/twitter/android/NearbyFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/android/NearbyFragment;->e(Z)V

    if-eqz p2, :cond_0

    const/4 v1, 0x0

    iget v0, p0, Lcom/twitter/android/NearbyFragment;->r:I

    int-to-float v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-wide/16 v5, 0x12c

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/NearbyFragment;->a(IFIFJ)V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/NearbyFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/NearbyFragment;->ad:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->startAnimation(Landroid/view/animation/Animation;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->ae:Landroid/view/animation/TranslateAnimation;

    const-wide/16 v1, 0x12c

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/NearbyFragment;->e(Z)V

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->c:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/twitter/android/NearbyFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    if-eqz p2, :cond_2

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget v0, p0, Lcom/twitter/android/NearbyFragment;->s:I

    int-to-float v4, v0

    const-wide/16 v5, 0x12c

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/NearbyFragment;->a(IFIFJ)V

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/NearbyFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/NearbyFragment;->ae:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private a(D)Z
    .locals 2

    const-wide/high16 v0, 0x4028000000000000L    # 12.0

    cmpl-double v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(DD)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    cmpg-double v2, p1, v4

    if-gez v2, :cond_1

    cmpl-double v2, p3, v4

    if-lez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    cmpl-double v2, p1, v4

    if-lez v2, :cond_2

    cmpg-double v2, p3, v4

    if-gez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    sub-double v2, p1, p3

    cmpl-double v2, v2, v4

    if-gez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private a(F)Z
    .locals 14

    const/4 v13, 0x3

    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    const/4 v8, 0x1

    const/high16 v1, 0x41000000    # 8.0f

    cmpg-float v1, p1, v1

    if-gez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/NearbyFragment;->p:[D

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/c;->e()Lcom/google/android/gms/maps/r;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/maps/r;->a()Lcom/google/android/gms/maps/model/VisibleRegion;

    move-result-object v1

    iget-object v9, v1, Lcom/google/android/gms/maps/model/VisibleRegion;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-object v10, v1, Lcom/google/android/gms/maps/model/VisibleRegion;->d:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v4, v9, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-object v1, p0, Lcom/twitter/android/NearbyFragment;->p:[D

    aget-wide v0, v1, v0

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    iget-wide v4, v10, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-object v6, p0, Lcom/twitter/android/NearbyFragment;->p:[D

    const/4 v7, 0x2

    aget-wide v6, v6, v7

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    cmpg-double v6, v4, v0

    if-gtz v6, :cond_2

    move v0, v8

    goto :goto_0

    :cond_2
    sub-double v0, v4, v0

    iget-wide v4, v10, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-wide v6, v9, Lcom/google/android/gms/maps/model/LatLng;->a:D

    sub-double/2addr v4, v6

    cmpl-double v6, v0, v2

    if-eqz v6, :cond_9

    div-double v0, v4, v0

    :goto_1
    invoke-direct {p0, v0, v1}, Lcom/twitter/android/NearbyFragment;->a(D)Z

    move-result v4

    if-eqz v4, :cond_3

    move v0, v8

    goto :goto_0

    :cond_3
    iget-wide v4, v9, Lcom/google/android/gms/maps/model/LatLng;->b:D

    iget-object v6, p0, Lcom/twitter/android/NearbyFragment;->p:[D

    aget-wide v6, v6, v8

    invoke-direct {p0, v4, v5, v6, v7}, Lcom/twitter/android/NearbyFragment;->a(DD)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-wide v4, v9, Lcom/google/android/gms/maps/model/LatLng;->b:D

    :goto_2
    iget-wide v6, v10, Lcom/google/android/gms/maps/model/LatLng;->b:D

    iget-object v11, p0, Lcom/twitter/android/NearbyFragment;->p:[D

    aget-wide v11, v11, v13

    invoke-direct {p0, v6, v7, v11, v12}, Lcom/twitter/android/NearbyFragment;->a(DD)Z

    move-result v6

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/twitter/android/NearbyFragment;->p:[D

    aget-wide v6, v6, v13

    :goto_3
    invoke-direct {p0, v4, v5, v6, v7}, Lcom/twitter/android/NearbyFragment;->a(DD)Z

    move-result v11

    if-eqz v11, :cond_6

    move v0, v8

    goto :goto_0

    :cond_4
    iget-object v4, p0, Lcom/twitter/android/NearbyFragment;->p:[D

    aget-wide v4, v4, v8

    goto :goto_2

    :cond_5
    iget-wide v6, v10, Lcom/google/android/gms/maps/model/LatLng;->b:D

    goto :goto_3

    :cond_6
    invoke-direct {p0, v4, v5, v6, v7}, Lcom/twitter/android/NearbyFragment;->b(DD)D

    move-result-wide v4

    iget-wide v6, v9, Lcom/google/android/gms/maps/model/LatLng;->b:D

    iget-wide v9, v10, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-direct {p0, v6, v7, v9, v10}, Lcom/twitter/android/NearbyFragment;->b(DD)D

    move-result-wide v6

    cmpl-double v9, v4, v2

    if-eqz v9, :cond_7

    div-double v2, v6, v4

    :cond_7
    div-double v4, v6, v4

    invoke-direct {p0, v4, v5}, Lcom/twitter/android/NearbyFragment;->a(D)Z

    move-result v4

    if-eqz v4, :cond_8

    move v0, v8

    goto/16 :goto_0

    :cond_8
    mul-double/2addr v0, v2

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/NearbyFragment;->a(D)Z

    move-result v0

    goto/16 :goto_0

    :cond_9
    move-wide v0, v2

    goto :goto_1
.end method

.method static synthetic a(Lcom/twitter/android/NearbyFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/NearbyFragment;->n:Z

    return p1
.end method

.method private b(DD)D
    .locals 3

    const-wide/16 v1, 0x0

    cmpl-double v0, p1, v1

    if-lez v0, :cond_0

    cmpg-double v0, p3, v1

    if-gez v0, :cond_0

    const-wide v0, 0x4076800000000000L    # 360.0

    add-double/2addr v0, p3

    sub-double/2addr v0, p1

    :goto_0
    return-wide v0

    :cond_0
    sub-double v0, p3, p1

    goto :goto_0
.end method

.method private b(F)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->x:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/NearbyFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/android/NearbyFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v4, Landroid/graphics/Path;

    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    div-int/lit8 v1, v1, 0x2

    iget v5, p0, Lcom/twitter/android/NearbyFragment;->s:I

    add-int/2addr v1, v5

    int-to-float v1, v1

    sget-object v5, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v4, v0, v1, p1, v5}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    sget-object v0, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    invoke-virtual {v3, v4, v0}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    invoke-virtual {p0}, Lcom/twitter/android/NearbyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0003    # com.twitter.android.R.color.black

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->J:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iput-object v2, p0, Lcom/twitter/android/NearbyFragment;->x:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/NearbyFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/NearbyFragment;->J()V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/NearbyFragment;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/NearbyFragment;->f(Z)V

    return-void
.end method

.method private b(Lcom/google/android/gms/maps/model/k;)Z
    .locals 3

    const/4 v1, 0x1

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->V:Lcom/twitter/android/nd;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/twitter/android/nd;->a(Ljava/util/ArrayList;)V

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->W:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/twitter/android/NearbyFragment;->V:Lcom/twitter/android/nd;

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->W:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Lcom/twitter/android/nd;->a(Ljava/util/ArrayList;)V

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/NearbyFragment;Lcom/google/android/gms/maps/model/k;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/twitter/android/NearbyFragment;->b(Lcom/google/android/gms/maps/model/k;)Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/twitter/android/NearbyFragment;)Lcom/google/android/gms/maps/model/k;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->w:Lcom/google/android/gms/maps/model/k;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/NearbyFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/NearbyFragment;->j:Z

    return p1
.end method

.method static synthetic d(Lcom/twitter/android/NearbyFragment;)Lcom/twitter/library/platform/LocationProducer;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->a:Lcom/twitter/library/platform/LocationProducer;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/NearbyFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/NearbyFragment;->L()V

    return-void
.end method

.method private e(Z)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->d()Lcom/google/android/gms/maps/u;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/u;->d(Z)V

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->K:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->d()Lcom/google/android/gms/maps/u;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/maps/u;->d(Z)V

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->K:Landroid/widget/RelativeLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic f(Lcom/twitter/android/NearbyFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/NearbyFragment;->K()V

    return-void
.end method

.method private f(Z)V
    .locals 3

    iget-boolean v0, p0, Lcom/twitter/android/NearbyFragment;->m:Z

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/NearbyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0201de    # com.twitter.android.R.drawable.ic_nearby_pin_stacked

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lcom/twitter/android/NearbyFragment;->b(F)V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->J:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/twitter/android/NearbyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f040007    # com.twitter.android.R.anim.fade_in

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v1, p0, Lcom/twitter/android/NearbyFragment;->J:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    :goto_1
    iput-boolean p1, p0, Lcom/twitter/android/NearbyFragment;->m:Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->J:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->J:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method static synthetic g(Lcom/twitter/android/NearbyFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/NearbyFragment;->n:Z

    return v0
.end method

.method static synthetic h(Lcom/twitter/android/NearbyFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/NearbyFragment;->m:Z

    return v0
.end method

.method static synthetic i(Lcom/twitter/android/NearbyFragment;)Landroid/widget/RelativeLayout;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->c:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/NearbyFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/NearbyFragment;->i:Z

    return v0
.end method

.method static synthetic k(Lcom/twitter/android/NearbyFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/NearbyFragment;->o:Z

    return v0
.end method

.method static synthetic l(Lcom/twitter/android/NearbyFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/NearbyFragment;->k:Z

    return v0
.end method

.method static synthetic m(Lcom/twitter/android/NearbyFragment;)I
    .locals 1

    iget v0, p0, Lcom/twitter/android/NearbyFragment;->r:I

    return v0
.end method

.method static synthetic n(Lcom/twitter/android/NearbyFragment;)I
    .locals 1

    iget v0, p0, Lcom/twitter/android/NearbyFragment;->s:I

    return v0
.end method

.method static synthetic o(Lcom/twitter/android/NearbyFragment;)Lcom/twitter/android/nd;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->V:Lcom/twitter/android/nd;

    return-object v0
.end method

.method static synthetic p(Lcom/twitter/android/NearbyFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/NearbyFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic q(Lcom/twitter/android/NearbyFragment;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->X:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic r(Lcom/twitter/android/NearbyFragment;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->W:Ljava/util/HashMap;

    return-object v0
.end method


# virtual methods
.method protected a(Landroid/database/Cursor;)V
    .locals 0

    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/TimelineFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    invoke-direct {p0}, Lcom/twitter/android/NearbyFragment;->I()V

    return-void
.end method

.method public a(Lcom/google/android/gms/maps/model/CameraPosition;)V
    .locals 4

    iput-object p1, p0, Lcom/twitter/android/NearbyFragment;->v:Lcom/google/android/gms/maps/model/CameraPosition;

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->L:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/android/NearbyFragment;->U:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget v1, p1, Lcom/google/android/gms/maps/model/CameraPosition;->b:F

    iget v0, p0, Lcom/twitter/android/NearbyFragment;->q:F

    sub-float v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v2, 0x3f333333    # 0.7f

    cmpl-float v0, v0, v2

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/NearbyFragment;->J()V

    iput v1, p0, Lcom/twitter/android/NearbyFragment;->q:F

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->L:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/android/NearbyFragment;->U:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;Z)V
    .locals 5

    const/4 v4, 0x1

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/TimelineFragment;->a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;Z)V

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->X:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/NearbyFragment;->X:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/k;

    iget-object v2, p0, Lcom/twitter/android/NearbyFragment;->W:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/NearbyFragment;->W:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v2, v4, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/NearbyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v1, v1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-static {v2, v1, v4}, Lcom/twitter/android/nf;->a(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/maps/model/b;->a(Landroid/graphics/Bitmap;)Lcom/google/android/gms/maps/model/a;

    move-result-object v1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/k;->a(Lcom/google/android/gms/maps/model/a;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/NearbyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v1, v1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-static {v2, v1}, Lcom/twitter/android/nf;->a(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/maps/model/b;->a(Landroid/graphics/Bitmap;)Lcom/google/android/gms/maps/model/a;

    move-result-object v1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->X:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    return-void
.end method

.method public a(Lcom/google/android/gms/maps/model/k;)Z
    .locals 5

    const/4 v4, 0x1

    invoke-direct {p0, p1}, Lcom/twitter/android/NearbyFragment;->b(Lcom/google/android/gms/maps/model/k;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v4}, Lcom/twitter/android/NearbyFragment;->f(Z)V

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->e()Lcom/google/android/gms/maps/r;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/k;->b()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/r;->a(Lcom/google/android/gms/maps/model/LatLng;)Landroid/graphics/Point;

    move-result-object v0

    iget v1, v0, Landroid/graphics/Point;->x:I

    invoke-virtual {p0}, Lcom/twitter/android/NearbyFragment;->getView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {p0}, Lcom/twitter/android/NearbyFragment;->getView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v0, v2

    iget v2, p0, Lcom/twitter/android/NearbyFragment;->s:I

    sub-int/2addr v0, v2

    int-to-float v0, v0

    iget-object v2, p0, Lcom/twitter/android/NearbyFragment;->t:Lcom/google/android/gms/maps/c;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/i;)V

    iget-object v2, p0, Lcom/twitter/android/NearbyFragment;->Z:Lcom/google/android/gms/maps/h;

    const/16 v3, 0x12c

    invoke-direct {p0, v1, v0, v2, v3}, Lcom/twitter/android/NearbyFragment;->a(FFLcom/google/android/gms/maps/h;I)V

    const/4 v0, 0x0

    invoke-direct {p0, v4, v0}, Lcom/twitter/android/NearbyFragment;->a(ZZ)V

    :cond_0
    return v4
.end method

.method public b_()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/TimelineFragment;->b_()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/NearbyFragment;->o:Z

    return-void
.end method

.method public c(Z)V
    .locals 7

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/twitter/android/NearbyFragment;->m:Z

    if-eqz v0, :cond_0

    move p1, v1

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/TimelineFragment;->c(Z)V

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->u:Lcom/google/android/gms/maps/SupportMapFragment;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/NearbyFragment;->m:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/NearbyFragment;->n:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/twitter/android/NearbyFragment;->r:I

    int-to-float v2, v0

    iget v0, p0, Lcom/twitter/android/NearbyFragment;->s:I

    int-to-float v4, v0

    const-wide/16 v5, 0x64

    move-object v0, p0

    move v3, v1

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/NearbyFragment;->a(IFIFJ)V

    iput-boolean p1, p0, Lcom/twitter/android/NearbyFragment;->k:Z

    :cond_1
    return-void
.end method

.method protected d(I)Lcom/twitter/library/service/b;
    .locals 5

    invoke-direct {p0}, Lcom/twitter/android/NearbyFragment;->M()[D

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iput-object v1, p0, Lcom/twitter/android/NearbyFragment;->p:[D

    new-instance v0, Ljq;

    invoke-virtual {p0}, Lcom/twitter/android/NearbyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/NearbyFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    const/16 v4, 0x64

    invoke-direct {v0, v2, v3, v4, v1}, Ljq;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;I[D)V

    goto :goto_0
.end method

.method public f()V
    .locals 7

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/twitter/android/TimelineFragment;->f()V

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->u:Lcom/google/android/gms/maps/SupportMapFragment;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/NearbyFragment;->m:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/NearbyFragment;->n:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/android/NearbyFragment;->r:I

    int-to-float v2, v0

    iget v0, p0, Lcom/twitter/android/NearbyFragment;->s:I

    int-to-float v4, v0

    const-wide/16 v5, 0x64

    move-object v0, p0

    move v3, v1

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/NearbyFragment;->a(IFIFJ)V

    iput-boolean v1, p0, Lcom/twitter/android/NearbyFragment;->k:Z

    :cond_0
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/twitter/android/TimelineFragment;->onActivityCreated(Landroid/os/Bundle;)V

    new-instance v0, Lcom/twitter/android/nd;

    iget-object v1, p0, Lcom/twitter/android/NearbyFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {p0}, Lcom/twitter/android/NearbyFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/nd;-><init>(Landroid/support/v4/widget/CursorAdapter;Lcom/twitter/android/client/c;)V

    iput-object v0, p0, Lcom/twitter/android/NearbyFragment;->V:Lcom/twitter/android/nd;

    invoke-virtual {p0}, Lcom/twitter/android/NearbyFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/NearbyFragment;->V:Lcom/twitter/android/nd;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lcom/twitter/android/NearbyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    invoke-direct {p0, p1}, Lcom/twitter/android/NearbyFragment;->a(Landroid/os/Bundle;)V

    :cond_0
    new-instance v1, Lcom/twitter/android/ns;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/ns;-><init>(Lcom/twitter/android/NearbyFragment;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/twitter/android/NearbyFragment;->b:Lcom/twitter/android/ns;

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->b:Lcom/twitter/android/ns;

    invoke-virtual {p0, v0}, Lcom/twitter/android/NearbyFragment;->a(Landroid/view/View$OnTouchListener;)Lcom/twitter/android/client/BaseListFragment;

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11

    const/4 v10, 0x4

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/high16 v9, 0x7fc00000    # NaNf

    invoke-virtual {p0}, Lcom/twitter/android/NearbyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/platform/p;->a(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/NearbyFragment;->h:Z

    invoke-virtual {p0}, Lcom/twitter/android/NearbyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/platform/LocationProducer;->a(Landroid/content/Context;)Lcom/twitter/library/platform/LocationProducer;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/NearbyFragment;->a:Lcom/twitter/library/platform/LocationProducer;

    invoke-virtual {p0}, Lcom/twitter/android/NearbyFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/NearbyFragment;->Y:Landroid/content/SharedPreferences;

    invoke-super {p0, p1}, Lcom/twitter/android/TimelineFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/NearbyFragment;->L:Landroid/os/Handler;

    new-instance v0, Lcom/twitter/android/nm;

    invoke-direct {v0, p0}, Lcom/twitter/android/nm;-><init>(Lcom/twitter/android/NearbyFragment;)V

    iput-object v0, p0, Lcom/twitter/android/NearbyFragment;->U:Ljava/lang/Runnable;

    invoke-direct {p0}, Lcom/twitter/android/NearbyFragment;->G()V

    if-eqz p1, :cond_1

    iput-boolean v2, p0, Lcom/twitter/android/NearbyFragment;->l:Z

    const-string/jumbo v0, "camera_position"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/CameraPosition;

    iput-object v0, p0, Lcom/twitter/android/NearbyFragment;->v:Lcom/google/android/gms/maps/model/CameraPosition;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->ad:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v2}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->ae:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v2}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    new-array v3, v10, [F

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->Y:Landroid/content/SharedPreferences;

    const-string/jumbo v4, "sw_lat"

    invoke-interface {v0, v4, v9}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    aput v0, v3, v1

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->Y:Landroid/content/SharedPreferences;

    const-string/jumbo v4, "sw_long"

    invoke-interface {v0, v4, v9}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    aput v0, v3, v2

    const/4 v0, 0x2

    iget-object v4, p0, Lcom/twitter/android/NearbyFragment;->Y:Landroid/content/SharedPreferences;

    const-string/jumbo v5, "ne_lat"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v4

    aput v4, v3, v0

    const/4 v0, 0x3

    iget-object v4, p0, Lcom/twitter/android/NearbyFragment;->Y:Landroid/content/SharedPreferences;

    const-string/jumbo v5, "ne_long"

    invoke-interface {v4, v5, v9}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v4

    aput v4, v3, v0

    array-length v4, v3

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_4

    aget v5, v3, v0

    invoke-static {v5}, Ljava/lang/Float;->isNaN(F)Z

    move-result v5

    if-eqz v5, :cond_2

    move v0, v1

    :goto_2
    if-eqz v0, :cond_3

    new-array v0, v10, [D

    iput-object v0, p0, Lcom/twitter/android/NearbyFragment;->p:[D

    move v0, v1

    :goto_3
    array-length v1, v3

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Lcom/twitter/android/NearbyFragment;->p:[D

    aget v2, v3, v0

    float-to-double v4, v2

    aput-wide v4, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->Y:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "camera_lat"

    invoke-interface {v0, v3, v9}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    float-to-double v3, v0

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->Y:Landroid/content/SharedPreferences;

    const-string/jumbo v5, "camera_long"

    invoke-interface {v0, v5, v9}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    float-to-double v5, v0

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->Y:Landroid/content/SharedPreferences;

    const-string/jumbo v7, "camera_zoom"

    invoke-interface {v0, v7, v9}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    invoke-static {v3, v4}, Ljava/lang/Double;->isNaN(D)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-static {v5, v6}, Ljava/lang/Double;->isNaN(D)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v7

    if-nez v7, :cond_0

    new-instance v7, Lcom/google/android/gms/maps/model/c;

    invoke-direct {v7}, Lcom/google/android/gms/maps/model/c;-><init>()V

    new-instance v8, Lcom/google/android/gms/maps/model/LatLng;

    invoke-direct {v8, v3, v4, v5, v6}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v7, v8}, Lcom/google/android/gms/maps/model/c;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/c;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/gms/maps/model/c;->a(F)Lcom/google/android/gms/maps/model/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/c;->a()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/NearbyFragment;->v:Lcom/google/android/gms/maps/model/CameraPosition;

    goto/16 :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    return-void

    :cond_4
    move v0, v2

    goto :goto_2
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    const v0, 0x7f0300d0    # com.twitter.android.R.layout.nearby_fragment

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/android/NearbyFragment;->a(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0901f3    # com.twitter.android.R.id.list_container

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/twitter/android/NearbyFragment;->c:Landroid/widget/RelativeLayout;

    const v0, 0x7f09018e    # com.twitter.android.R.id.map_container

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/twitter/android/NearbyFragment;->g:Landroid/widget/RelativeLayout;

    const v0, 0x7f0901ef    # com.twitter.android.R.id.fader

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/NearbyFragment;->J:Landroid/widget/ImageView;

    const v0, 0x7f0901f0    # com.twitter.android.R.id.btn_bar

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/twitter/android/NearbyFragment;->K:Landroid/widget/RelativeLayout;

    const v0, 0x7f0901f1    # com.twitter.android.R.id.my_location

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/twitter/android/NearbyFragment;->z:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->z:Landroid/widget/ImageButton;

    new-instance v2, Lcom/twitter/android/nn;

    invoke-direct {v2, p0}, Lcom/twitter/android/nn;-><init>(Lcom/twitter/android/NearbyFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0901f2    # com.twitter.android.R.id.view_timeline

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/NearbyFragment;->y:Landroid/widget/Button;

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->y:Landroid/widget/Button;

    new-instance v2, Lcom/twitter/android/no;

    invoke-direct {v2, p0}, Lcom/twitter/android/no;-><init>(Lcom/twitter/android/NearbyFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method public onDestroyView()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->b:Lcom/twitter/android/ns;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/NearbyFragment;->b:Lcom/twitter/android/ns;

    invoke-virtual {p0, v0}, Lcom/twitter/android/NearbyFragment;->b(Landroid/view/View$OnTouchListener;)Lcom/twitter/android/client/BaseListFragment;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/NearbyFragment;->b:Lcom/twitter/android/ns;

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/TimelineFragment;->onDestroyView()V

    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/NearbyFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-super {p0, p1}, Lcom/twitter/android/TimelineFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/twitter/android/NearbyFragment;->Y:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/NearbyFragment;->p:[D

    if-eqz v3, :cond_0

    const-string/jumbo v3, "sw_lat"

    iget-object v4, p0, Lcom/twitter/android/NearbyFragment;->p:[D

    aget-wide v4, v4, v0

    double-to-float v0, v4

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v0, "sw_long"

    iget-object v3, p0, Lcom/twitter/android/NearbyFragment;->p:[D

    aget-wide v3, v3, v1

    double-to-float v3, v3

    invoke-interface {v2, v0, v3}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v0, "ne_lat"

    iget-object v3, p0, Lcom/twitter/android/NearbyFragment;->p:[D

    const/4 v4, 0x2

    aget-wide v3, v3, v4

    double-to-float v3, v3

    invoke-interface {v2, v0, v3}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v0, "ne_long"

    iget-object v3, p0, Lcom/twitter/android/NearbyFragment;->p:[D

    const/4 v4, 0x3

    aget-wide v3, v3, v4

    double-to-float v3, v3

    invoke-interface {v2, v0, v3}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    move v0, v1

    :cond_0
    iget-object v3, p0, Lcom/twitter/android/NearbyFragment;->v:Lcom/google/android/gms/maps/model/CameraPosition;

    if-eqz v3, :cond_2

    const-string/jumbo v0, "camera_position"

    iget-object v3, p0, Lcom/twitter/android/NearbyFragment;->v:Lcom/google/android/gms/maps/model/CameraPosition;

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v0, "camera_lat"

    iget-object v3, p0, Lcom/twitter/android/NearbyFragment;->v:Lcom/google/android/gms/maps/model/CameraPosition;

    iget-object v3, v3, Lcom/google/android/gms/maps/model/CameraPosition;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v3, v3, Lcom/google/android/gms/maps/model/LatLng;->a:D

    double-to-float v3, v3

    invoke-interface {v2, v0, v3}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v0, "camera_long"

    iget-object v3, p0, Lcom/twitter/android/NearbyFragment;->v:Lcom/google/android/gms/maps/model/CameraPosition;

    iget-object v3, v3, Lcom/google/android/gms/maps/model/CameraPosition;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v3, v3, Lcom/google/android/gms/maps/model/LatLng;->b:D

    double-to-float v3, v3

    invoke-interface {v2, v0, v3}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v0, "camera_zoom"

    iget-object v3, p0, Lcom/twitter/android/NearbyFragment;->v:Lcom/google/android/gms/maps/model/CameraPosition;

    iget v3, v3, Lcom/google/android/gms/maps/model/CameraPosition;->b:F

    invoke-interface {v2, v0, v3}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    :goto_0
    if-eqz v1, :cond_1

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_1
    return-void

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method protected r()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public t_()V
    .locals 0

    invoke-super {p0}, Lcom/twitter/android/TimelineFragment;->t_()V

    invoke-direct {p0}, Lcom/twitter/android/NearbyFragment;->I()V

    return-void
.end method

.method public x_()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/TimelineFragment;->x_()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/NearbyFragment;->o:Z

    return-void
.end method
