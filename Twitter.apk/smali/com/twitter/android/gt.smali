.class Lcom/twitter/android/gt;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic a:Landroid/support/v4/view/GestureDetectorCompat;

.field final synthetic b:Landroid/view/ScaleGestureDetector;

.field final synthetic c:Lcom/twitter/android/GalleryActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/GalleryActivity;Landroid/support/v4/view/GestureDetectorCompat;Landroid/view/ScaleGestureDetector;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/gt;->c:Lcom/twitter/android/GalleryActivity;

    iput-object p2, p0, Lcom/twitter/android/gt;->a:Landroid/support/v4/view/GestureDetectorCompat;

    iput-object p3, p0, Lcom/twitter/android/gt;->b:Landroid/view/ScaleGestureDetector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/gt;->a:Landroid/support/v4/view/GestureDetectorCompat;

    invoke-virtual {v0, p2}, Landroid/support/v4/view/GestureDetectorCompat;->onTouchEvent(Landroid/view/MotionEvent;)Z

    iget-object v0, p0, Lcom/twitter/android/gt;->b:Landroid/view/ScaleGestureDetector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/gt;->b:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, p2}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/gt;->c:Lcom/twitter/android/GalleryActivity;

    invoke-static {v0}, Lcom/twitter/android/GalleryActivity;->a(Lcom/twitter/android/GalleryActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/support/v4/view/ViewPager;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
