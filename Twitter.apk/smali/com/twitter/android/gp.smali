.class Lcom/twitter/android/gp;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/FollowActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/FollowActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/gp;->a:Lcom/twitter/android/FollowActivity;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method


# virtual methods
.method public e(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .locals 3

    if-nez p4, :cond_0

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/gp;->a:Lcom/twitter/android/FollowActivity;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "device_registration_normalized_phone_number"

    invoke-interface {v1, v2, p4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    const-string/jumbo v1, "device_registration_sms_text"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/gp;->a:Lcom/twitter/android/FollowActivity;

    invoke-static {v1}, Lcom/twitter/android/FollowActivity;->a(Lcom/twitter/android/FollowActivity;)Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, p4, v0}, Lcom/twitter/android/client/c;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/gp;->a:Lcom/twitter/android/FollowActivity;

    invoke-static {v0}, Lcom/twitter/android/FollowActivity;->b(Lcom/twitter/android/FollowActivity;)Lcom/twitter/android/UsersFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/UsersFragment;->u()V

    return-void
.end method

.method public f(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .locals 3

    sparse-switch p3, :sswitch_data_0

    invoke-static {}, Lcom/twitter/android/FollowActivity;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "digits"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Device registration failed with error code "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/gp;->a:Lcom/twitter/android/FollowActivity;

    invoke-static {v0}, Lcom/twitter/android/FollowActivity;->c(Lcom/twitter/android/FollowActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/j;)V

    return-void

    :sswitch_0
    invoke-static {}, Lcom/twitter/android/FollowActivity;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "digits"

    const-string/jumbo v1, "Device registration successful."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/gp;->a:Lcom/twitter/android/FollowActivity;

    invoke-static {v0}, Lcom/twitter/android/FollowActivity;->b(Lcom/twitter/android/FollowActivity;)Lcom/twitter/android/UsersFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/UsersFragment;->u()V

    goto :goto_0

    :sswitch_1
    invoke-static {}, Lcom/twitter/android/FollowActivity;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "digits"

    const-string/jumbo v1, "Device registration endpoint not found."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/twitter/android/FollowActivity;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "digits"

    const-string/jumbo v1, "Device registration failed: bad request."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0x190 -> :sswitch_2
        0x194 -> :sswitch_1
    .end sparse-switch
.end method
