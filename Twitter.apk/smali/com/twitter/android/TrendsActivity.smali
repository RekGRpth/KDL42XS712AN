.class public Lcom/twitter/android/TrendsActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/ce;


# static fields
.field private static final a:Landroid/net/Uri;

.field private static final b:Landroid/net/Uri;


# instance fields
.field private c:Landroid/content/SharedPreferences;

.field private d:Ljava/util/ArrayList;

.field private e:Landroid/support/v4/view/ViewPager;

.field private f:Lcom/twitter/android/vl;

.field private g:Lcom/twitter/android/vk;

.field private h:Lcom/twitter/internal/android/widget/HorizontalListView;

.field private i:Lcom/twitter/library/widget/ComposerBarLayout;

.field private j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string/jumbo v0, "twitter://trends/list"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/TrendsActivity;->a:Landroid/net/Uri;

    const-string/jumbo v0, "twitter://trends/details"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/TrendsActivity;->b:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/TrendsActivity;)Landroid/support/v4/view/ViewPager;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TrendsActivity;->e:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method private a(Landroid/net/Uri;Z)Lhb;
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    new-instance v1, Lhd;

    const-class v0, Lcom/twitter/android/TrendsFragment;

    invoke-direct {v1, p1, v0}, Lhd;-><init>(Landroid/net/Uri;Ljava/lang/Class;)V

    invoke-virtual {p0}, Lcom/twitter/android/TrendsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0, v5}, Lcom/twitter/android/TimelineFragment;->a(Landroid/content/Intent;Z)Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v0, "is_collapsed"

    invoke-virtual {v2, v0, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    if-eqz p2, :cond_1

    const-string/jumbo v0, "list_view"

    :goto_0
    const-string/jumbo v3, "scribe_section"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v3, "orientation_shim"

    invoke-virtual {v2, v3, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {p0}, Lcom/twitter/android/TrendsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "needs_refresh"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string/jumbo v3, "needs_refresh"

    invoke-virtual {v2, v3, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    invoke-virtual {v1, v2}, Lhd;->a(Landroid/os/Bundle;)Lhd;

    invoke-virtual {v1, v0}, Lhd;->a(Ljava/lang/String;)Lhd;

    if-eqz p2, :cond_2

    const v0, 0x7f0f04c3    # com.twitter.android.R.string.trends_scope_list

    :goto_1
    invoke-virtual {p0, v0}, Lcom/twitter/android/TrendsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lhd;->a(Ljava/lang/CharSequence;)Lhd;

    invoke-virtual {v1}, Lhd;->a()Lhb;

    move-result-object v0

    return-object v0

    :cond_1
    const-string/jumbo v0, "detail_view"

    goto :goto_0

    :cond_2
    const v0, 0x7f0f04c2    # com.twitter.android.R.string.trends_scope_detail

    goto :goto_1
.end method

.method static synthetic a(Lcom/twitter/android/TrendsActivity;Lcom/twitter/library/api/UserSettings;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/TrendsActivity;->a(Lcom/twitter/library/api/UserSettings;)V

    return-void
.end method

.method private a(Lcom/twitter/library/api/UserSettings;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-boolean v0, p1, Lcom/twitter/library/api/UserSettings;->p:Z

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/twitter/library/api/UserSettings;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/twitter/library/api/UserSettings;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/android/TrendsActivity;->a(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/TrendsActivity;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/TrendsActivity;Ljava/lang/String;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/TrendsActivity;->e(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/TrendsActivity;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TrendsActivity;->d:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/TrendsActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TrendsActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/TrendsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TrendsActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/TrendsActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TrendsActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/TrendsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TrendsActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/TrendsActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TrendsActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/TrendsActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TrendsActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private h()Ljava/util/ArrayList;
    .locals 3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-boolean v1, p0, Lcom/twitter/android/TrendsActivity;->j:Z

    if-nez v1, :cond_0

    sget-object v1, Lcom/twitter/android/TrendsActivity;->b:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/twitter/android/TrendsActivity;->a(Landroid/net/Uri;Z)Lhb;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    sget-object v1, Lcom/twitter/android/TrendsActivity;->a:Landroid/net/Uri;

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/twitter/android/TrendsActivity;->a(Landroid/net/Uri;Z)Lhb;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/TrendsActivity;)Lcom/twitter/internal/android/widget/HorizontalListView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TrendsActivity;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/TrendsActivity;)Lcom/twitter/android/vk;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TrendsActivity;->g:Lcom/twitter/android/vk;

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/android/TrendsActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TrendsActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private k()V
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/twitter/android/TrendsActivity;->c:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "scope_option"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/TrendsActivity;->e:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    return-void

    :pswitch_0
    iget-object v2, p0, Lcom/twitter/android/TrendsActivity;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eq v2, v1, :cond_0

    move v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic l(Lcom/twitter/android/TrendsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TrendsActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method private l()V
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/twitter/android/TrendsActivity;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/TrendsActivity;->e:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/TrendsActivity;->c:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "scope_option"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method static synthetic m(Lcom/twitter/android/TrendsActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TrendsActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic n(Lcom/twitter/android/TrendsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TrendsActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic o(Lcom/twitter/android/TrendsActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/TrendsActivity;->j:Z

    return v0
.end method

.method static synthetic p(Lcom/twitter/android/TrendsActivity;)Lcom/twitter/library/widget/ComposerBarLayout;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TrendsActivity;->i:Lcom/twitter/library/widget/ComposerBarLayout;

    return-object v0
.end method

.method static synthetic q(Lcom/twitter/android/TrendsActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/TrendsActivity;->l()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const v1, 0x7f03015a    # com.twitter.android.R.layout.trends_activity

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(Z)V

    return-object v0
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 3

    const/4 v2, 0x1

    if-ne p2, v2, :cond_0

    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/TrendLocationsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0, v2}, Lcom/twitter/android/TrendsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    return-void
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 4

    const/4 v1, 0x0

    const v0, 0x7f0f04c5    # com.twitter.android.R.string.trends_title_worldwide

    invoke-virtual {p0, v0}, Lcom/twitter/android/TrendsActivity;->setTitle(I)V

    invoke-virtual {p0, v1}, Lcom/twitter/android/TrendsActivity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TrendsActivity;->c:Landroid/content/SharedPreferences;

    invoke-virtual {p0}, Lcom/twitter/android/TrendsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/TrendsActivity;->j:Z

    invoke-direct {p0}, Lcom/twitter/android/TrendsActivity;->h()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TrendsActivity;->d:Ljava/util/ArrayList;

    const v0, 0x7f0900bb    # com.twitter.android.R.id.pager

    invoke-virtual {p0, v0}, Lcom/twitter/android/TrendsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/twitter/android/TrendsActivity;->e:Landroid/support/v4/view/ViewPager;

    iget-object v0, p0, Lcom/twitter/android/TrendsActivity;->e:Landroid/support/v4/view/ViewPager;

    const v2, 0x7f0b0063    # com.twitter.android.R.color.list_margin_bg

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setPageMarginDrawable(I)V

    new-instance v0, Lcom/twitter/android/vl;

    iget-object v2, p0, Lcom/twitter/android/TrendsActivity;->d:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/twitter/android/TrendsActivity;->e:Landroid/support/v4/view/ViewPager;

    invoke-direct {v0, p0, p0, v2, v3}, Lcom/twitter/android/vl;-><init>(Lcom/twitter/android/TrendsActivity;Landroid/support/v4/app/FragmentActivity;Ljava/util/ArrayList;Landroid/support/v4/view/ViewPager;)V

    iput-object v0, p0, Lcom/twitter/android/TrendsActivity;->f:Lcom/twitter/android/vl;

    iget-object v0, p0, Lcom/twitter/android/TrendsActivity;->e:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/twitter/android/TrendsActivity;->f:Lcom/twitter/android/vl;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    const v0, 0x7f0900e1    # com.twitter.android.R.id.dock

    invoke-virtual {p0, v0}, Lcom/twitter/android/TrendsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/ComposerBarLayout;

    iput-object v0, p0, Lcom/twitter/android/TrendsActivity;->i:Lcom/twitter/library/widget/ComposerBarLayout;

    const v0, 0x7f090156    # com.twitter.android.R.id.tabs

    invoke-virtual {p0, v0}, Lcom/twitter/android/TrendsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/HorizontalListView;

    iget-boolean v2, p0, Lcom/twitter/android/TrendsActivity;->j:Z

    if-nez v2, :cond_1

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->setVisibility(I)V

    iget-object v1, p0, Lcom/twitter/android/TrendsActivity;->i:Lcom/twitter/library/widget/ComposerBarLayout;

    invoke-virtual {v1}, Lcom/twitter/library/widget/ComposerBarLayout;->a()V

    :goto_1
    new-instance v1, Lcom/twitter/android/vk;

    iget-object v2, p0, Lcom/twitter/android/TrendsActivity;->d:Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Lcom/twitter/android/vk;-><init>(Ljava/util/ArrayList;)V

    iput-object v1, p0, Lcom/twitter/android/TrendsActivity;->g:Lcom/twitter/android/vk;

    iget-object v1, p0, Lcom/twitter/android/TrendsActivity;->g:Lcom/twitter/android/vk;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v1, Lcom/twitter/android/vj;

    invoke-direct {v1, p0}, Lcom/twitter/android/vj;-><init>(Lcom/twitter/android/TrendsActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iput-object v0, p0, Lcom/twitter/android/TrendsActivity;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    new-instance v0, Lcom/twitter/android/vm;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p0, v1}, Lcom/twitter/android/vm;-><init>(Lcom/twitter/android/TrendsActivity;Landroid/content/Context;Lcom/twitter/android/vj;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/TrendsActivity;->a(Lcom/twitter/library/client/j;)V

    invoke-virtual {p0}, Lcom/twitter/android/TrendsActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/library/api/UserSettings;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/TrendsActivity;->a(Lcom/twitter/library/api/UserSettings;)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->setVisibility(I)V

    iget-object v1, p0, Lcom/twitter/android/TrendsActivity;->i:Lcom/twitter/library/widget/ComposerBarLayout;

    invoke-virtual {v1}, Lcom/twitter/library/widget/ComposerBarLayout;->b()V

    goto :goto_1
.end method

.method protected a(Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lcom/twitter/internal/android/widget/ToolBar;)Z

    invoke-virtual {p0}, Lcom/twitter/android/TrendsActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/library/api/UserSettings;

    move-result-object v0

    const v3, 0x7f090331    # com.twitter.android.R.id.trends_menu_get_personalized

    invoke-virtual {p1, v3}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v3

    if-eqz v0, :cond_2

    iget-boolean v0, v0, Lcom/twitter/library/api/UserSettings;->p:Z

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lhn;->b(Z)Lhn;

    :cond_0
    const v0, 0x7f090309    # com.twitter.android.R.id.toolbar_search

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, v2}, Lhn;->b(Z)Lhn;

    :cond_1
    return v1

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method protected a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z

    invoke-virtual {p0}, Lcom/twitter/android/TrendsActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f110024    # com.twitter.android.R.menu.trends

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public a(Lhn;)Z
    .locals 4

    const/4 v0, 0x1

    invoke-virtual {p1}, Lhn;->a()I

    move-result v1

    const v2, 0x7f090331    # com.twitter.android.R.id.trends_menu_get_personalized

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/TrendsActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/library/api/UserSettings;

    move-result-object v2

    if-eqz v2, :cond_0

    iput-boolean v0, v2, Lcom/twitter/library/api/UserSettings;->p:Z

    invoke-virtual {p0}, Lcom/twitter/android/TrendsActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v3

    invoke-virtual {v3, v1, v2, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/UserSettings;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/TrendsActivity;->d(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lhn;->b(Z)Lhn;

    :goto_0
    return v0

    :cond_1
    const v2, 0x7f090330    # com.twitter.android.R.id.trends_menu_change_loc

    if-ne v1, v2, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/TrendsActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/library/api/UserSettings;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-boolean v1, v1, Lcom/twitter/library/api/UserSettings;->p:Z

    if-eqz v1, :cond_2

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f04b9    # com.twitter.android.R.string.trends_change_loc_title

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f04b8    # com.twitter.android.R.string.trends_change_loc_msg

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f04b6    # com.twitter.android.R.string.trends_change_loc_continue

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f04b7    # com.twitter.android.R.string.trends_change_loc_keep

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/android/widget/ce;)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/TrendsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_0

    :cond_2
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/TrendLocationsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/TrendsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_3
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhn;)Z

    move-result v0

    goto :goto_0
.end method

.method public f()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TrendsActivity;->g()Lcom/twitter/android/client/BaseListFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/client/BaseListFragment;->aB()V

    :cond_0
    return-void
.end method

.method public g()Lcom/twitter/android/client/BaseListFragment;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/TrendsActivity;->f:Lcom/twitter/android/vl;

    iget-object v1, p0, Lcom/twitter/android/TrendsActivity;->e:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/vl;->a(I)Lhb;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/TrendsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhb;->a(Landroid/support/v4/app/FragmentManager;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/BaseListFragment;

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 8

    const/4 v7, 0x1

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/client/BaseFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    if-ne p1, v7, :cond_1

    if-eqz p3, :cond_1

    const-string/jumbo v0, "woeid"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/TrendsActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    const-string/jumbo v1, "woeid"

    const-wide/16 v2, 0x1

    invoke-virtual {p3, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    invoke-virtual {p0}, Lcom/twitter/android/TrendsActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/library/api/UserSettings;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-boolean v5, v4, Lcom/twitter/library/api/UserSettings;->p:Z

    if-nez v5, :cond_0

    iget-wide v5, v4, Lcom/twitter/library/api/UserSettings;->a:J

    cmp-long v5, v5, v1

    if-eqz v5, :cond_1

    :cond_0
    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/twitter/library/api/UserSettings;->p:Z

    iput-wide v1, v4, Lcom/twitter/library/api/UserSettings;->a:J

    const-string/jumbo v1, "loc_name"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v4, Lcom/twitter/library/api/UserSettings;->b:Ljava/lang/String;

    invoke-virtual {v0, v3, v4, v7}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/UserSettings;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/TrendsActivity;->d(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onResume()V

    invoke-direct {p0}, Lcom/twitter/android/TrendsActivity;->k()V

    return-void
.end method

.method protected s_()Landroid/content/Intent;
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/TrendsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "from_up_navigation"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object v0
.end method
