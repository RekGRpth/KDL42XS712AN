.class public Lcom/twitter/android/zj;
.super Lcom/twitter/android/util/ad;
.source "Twttr"


# instance fields
.field private a:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Lcom/twitter/android/UserPresenceFragment;J)V
    .locals 1

    invoke-direct {p0, p2, p3}, Lcom/twitter/android/util/ad;-><init>(J)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/zj;->a:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/zj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/UserPresenceFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/UserPresenceFragment;->isAdded()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-static {v0}, Lcom/twitter/android/UserPresenceFragment;->a(Lcom/twitter/android/UserPresenceFragment;)Z

    const/4 v0, 0x1

    goto :goto_0
.end method
