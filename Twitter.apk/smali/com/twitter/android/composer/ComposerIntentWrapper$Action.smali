.class public final enum Lcom/twitter/android/composer/ComposerIntentWrapper$Action;
.super Ljava/lang/Enum;
.source "Twttr"


# static fields
.field public static final enum a:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

.field public static final enum b:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

.field public static final enum c:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

.field public static final enum d:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

.field public static final enum e:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

.field public static final enum f:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

.field private static final synthetic g:[Lcom/twitter/android/composer/ComposerIntentWrapper$Action;


# instance fields
.field final mUri:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    const-string/jumbo v1, "NONE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v4, v2}, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->a:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    new-instance v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    const-string/jumbo v1, "TWEET"

    const-string/jumbo v2, "com.twitter.android.post.status"

    invoke-direct {v0, v1, v5, v2}, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->b:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    new-instance v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    const-string/jumbo v1, "REPLY"

    const-string/jumbo v2, "com.twitter.android.post.reply"

    invoke-direct {v0, v1, v6, v2}, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->c:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    new-instance v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    const-string/jumbo v1, "QUOTE"

    const-string/jumbo v2, "com.twitter.android.post.quote"

    invoke-direct {v0, v1, v7, v2}, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->d:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    new-instance v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    const-string/jumbo v1, "ONBOARDING"

    const-string/jumbo v2, "com.twitter.android.post.onboarding"

    invoke-direct {v0, v1, v8, v2}, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->e:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    new-instance v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    const-string/jumbo v1, "SEND"

    const/4 v2, 0x5

    const-string/jumbo v3, "android.intent.action.SEND"

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->f:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    sget-object v1, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->a:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->b:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->c:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    aput-object v1, v0, v6

    sget-object v1, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->d:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    aput-object v1, v0, v7

    sget-object v1, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->e:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->f:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->g:[Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->mUri:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/android/composer/ComposerIntentWrapper$Action;
    .locals 1

    const-class v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    return-object v0
.end method

.method public static values()[Lcom/twitter/android/composer/ComposerIntentWrapper$Action;
    .locals 1

    sget-object v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->g:[Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    invoke-virtual {v0}, [Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    return-object v0
.end method
