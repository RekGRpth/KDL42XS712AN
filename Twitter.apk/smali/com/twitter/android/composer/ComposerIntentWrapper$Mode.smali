.class public final enum Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;
.super Ljava/lang/Enum;
.source "Twttr"


# static fields
.field public static final enum a:Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;

.field public static final enum b:Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;

.field public static final enum c:Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;

.field private static final synthetic d:[Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;


# instance fields
.field final mIntentExtraName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;

    const-string/jumbo v1, "TEXT"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;->a:Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;

    new-instance v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;

    const-string/jumbo v1, "PHOTO_PICKER"

    const-string/jumbo v2, "open_gallery"

    invoke-direct {v0, v1, v4, v2}, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;->b:Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;

    new-instance v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;

    const-string/jumbo v1, "CAMERA"

    const-string/jumbo v2, "launch_camera"

    invoke-direct {v0, v1, v5, v2}, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;->c:Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;

    sget-object v1, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;->a:Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;->b:Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;->c:Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;

    aput-object v1, v0, v5

    sput-object v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;->d:[Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;->mIntentExtraName:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;
    .locals 1

    const-class v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;

    return-object v0
.end method

.method public static values()[Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;
    .locals 1

    sget-object v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;->d:[Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;

    invoke-virtual {v0}, [Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;

    return-object v0
.end method
