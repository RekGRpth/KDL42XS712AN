.class public Lcom/twitter/android/composer/ComposerIntentWrapper;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private a:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/twitter/android/composer/ComposerIntentWrapper;
    .locals 1

    sget-object v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->b:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    invoke-static {p0, v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Landroid/content/Context;Lcom/twitter/android/composer/ComposerIntentWrapper$Action;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/android/composer/ComposerIntentWrapper$Action;)Lcom/twitter/android/composer/ComposerIntentWrapper;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/twitter/android/composer/ComposerIntentWrapper;->p()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p1, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->mUri:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v1, Lcom/twitter/android/composer/ComposerIntentWrapper;

    invoke-direct {v1, v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;-><init>(Landroid/content/Intent;)V

    return-object v1
.end method

.method public static a(Lcom/twitter/android/PostActivity;)Lcom/twitter/android/composer/ComposerIntentWrapper;
    .locals 2

    new-instance v0, Lcom/twitter/android/composer/ComposerIntentWrapper;

    invoke-virtual {p0}, Lcom/twitter/android/PostActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/composer/ComposerIntentWrapper;-><init>(Landroid/content/Intent;)V

    return-object v0
.end method

.method public static a(Lcom/twitter/android/composer/ComposerIntentWrapper$Action;)Lcom/twitter/android/composer/ComposerIntentWrapper;
    .locals 3

    new-instance v0, Lcom/twitter/android/composer/ComposerIntentWrapper;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->mUri:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/twitter/android/composer/ComposerIntentWrapper;-><init>(Landroid/content/Intent;)V

    return-object v0
.end method

.method public static p()Ljava/lang/Class;
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->aC()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "android_compose_people_button_1951"

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "unassigned"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-class v0, Lcom/twitter/android/composer/TextFirstComposerActivity;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/twitter/library/featureswitch/a;->y()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->aH()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const-class v0, Lcom/twitter/android/composer/TextFirstComposerActivity;

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "android_multi_photo_composer_2024"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v0, "android_multi_photo_composer_2024"

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "multi_photo_composer"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-class v0, Lcom/twitter/android/composer/ComposerActivity;

    goto :goto_0

    :cond_3
    const-string/jumbo v0, "android_multi_photo_composer_2024"

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "control"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-class v0, Lcom/twitter/android/composer/TextFirstComposerActivity;

    goto :goto_0

    :cond_4
    invoke-static {}, Lgp;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    const-class v0, Lcom/twitter/android/composer/ComposerActivity;

    goto :goto_0

    :cond_5
    const-class v0, Lcom/twitter/android/composer/TextFirstComposerActivity;

    goto :goto_0
.end method


# virtual methods
.method public a()Landroid/content/Intent;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    return-object v0
.end method

.method public a(I)Lcom/twitter/android/composer/ComposerIntentWrapper;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    return-object p0
.end method

.method public a(J)Lcom/twitter/android/composer/ComposerIntentWrapper;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.extra.UID"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    return-object p0
.end method

.method public a(JLcom/twitter/library/api/TweetEntities;)Lcom/twitter/android/composer/ComposerIntentWrapper;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    const-string/jumbo v1, "draft_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    const-string/jumbo v1, "entities"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    return-object p0
.end method

.method public a(Landroid/content/Intent;)Lcom/twitter/android/composer/ComposerIntentWrapper;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.extra.INTENT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    return-object p0
.end method

.method public a(Landroid/net/Uri;)Lcom/twitter/android/composer/ComposerIntentWrapper;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    return-object p0
.end method

.method public a(Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;)Lcom/twitter/android/composer/ComposerIntentWrapper;
    .locals 3

    iget-object v0, p1, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;->mIntentExtraName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    iget-object v1, p1, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;->mIntentExtraName:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_0
    return-object p0
.end method

.method public a(Lcom/twitter/library/api/PromotedContent;)Lcom/twitter/android/composer/ComposerIntentWrapper;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    const-string/jumbo v1, "pc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    return-object p0
.end method

.method public a(Lcom/twitter/library/provider/ParcelableTweet;)Lcom/twitter/android/composer/ComposerIntentWrapper;
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/twitter/library/provider/ParcelableTweet;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a([Lcom/twitter/library/provider/ParcelableTweet;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/android/composer/ComposerIntentWrapper;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    const-string/jumbo v1, "account_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public a(Ljava/lang/String;[I)Lcom/twitter/android/composer/ComposerIntentWrapper;
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, p2}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a([I)Lcom/twitter/android/composer/ComposerIntentWrapper;

    :cond_0
    return-object p0
.end method

.method public a([I)Lcom/twitter/android/composer/ComposerIntentWrapper;
    .locals 2

    if-eqz p1, :cond_0

    array-length v0, p1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    const-string/jumbo v1, "selection"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    :cond_0
    return-object p0
.end method

.method public a([Lcom/twitter/library/provider/ParcelableTweet;)Lcom/twitter/android/composer/ComposerIntentWrapper;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    const-string/jumbo v1, "reply_to_tweet"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    return-object p0
.end method

.method public a(Landroid/app/Activity;I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    invoke-virtual {p1, v0, p2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public a(Landroid/support/v4/app/Fragment;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/support/v4/app/Fragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public b()Lcom/twitter/android/composer/ComposerIntentWrapper$Action;
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->a:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->values()[Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_2

    aget-object v0, v3, v1

    iget-object v5, v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->mUri:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/net/Uri;)Lcom/twitter/android/composer/ComposerIntentWrapper;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    const-string/jumbo v1, "media_uri"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/android/composer/ComposerIntentWrapper;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    const-string/jumbo v1, "ref_event"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public b(Ljava/lang/String;[I)Lcom/twitter/android/composer/ComposerIntentWrapper;
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    const-string/jumbo v1, "prefilled_text"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, p2}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a([I)Lcom/twitter/android/composer/ComposerIntentWrapper;

    :cond_0
    return-object p0
.end method

.method public b(Landroid/content/Context;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public c()Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    sget-object v1, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;->b:Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;

    iget-object v1, v1, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;->mIntentExtraName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;->b:Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    sget-object v1, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;->c:Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;

    iget-object v1, v1, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;->mIntentExtraName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;->c:Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;->a:Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;

    goto :goto_0
.end method

.method public d()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public e()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    const-string/jumbo v1, "prefilled_text"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public g()[I
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    const-string/jumbo v1, "selection"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v0

    return-object v0
.end method

.method public h()J
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.extra.UID"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public i()Lcom/twitter/library/api/PromotedContent;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    const-string/jumbo v1, "pc"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lcom/twitter/library/api/PromotedContent;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    const-string/jumbo v1, "account_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public k()Landroid/net/Uri;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    const-string/jumbo v1, "media_uri"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    return-object v0
.end method

.method public l()[Lcom/twitter/library/provider/ParcelableTweet;
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    const-string/jumbo v1, "reply_to_tweet"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    array-length v0, v1

    new-array v0, v0, [Lcom/twitter/library/provider/ParcelableTweet;

    array-length v2, v1

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public m()Lcom/twitter/library/api/TweetEntities;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    const-string/jumbo v1, "entities"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TweetEntities;

    return-object v0
.end method

.method public n()J
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    const-string/jumbo v1, "draft_id"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public o()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerIntentWrapper;->a:Landroid/content/Intent;

    const-string/jumbo v1, "ref_event"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
