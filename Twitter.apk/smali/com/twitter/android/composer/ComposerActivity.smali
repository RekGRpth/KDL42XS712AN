.class public Lcom/twitter/android/composer/ComposerActivity;
.super Lcom/twitter/android/PostActivity;
.source "Twttr"


# static fields
.field private static final E:Ljava/lang/String;


# instance fields
.field private F:I

.field private G:Landroid/widget/Button;

.field private H:Landroid/view/View;

.field private I:Landroid/view/View;

.field private J:Landroid/view/View;

.field private K:Landroid/widget/TextView;

.field private L:[Landroid/widget/FrameLayout;

.field private M:Z

.field private N:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x8c

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/composer/ComposerActivity;->E:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/PostActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/composer/ComposerActivity;)V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->w()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/composer/ComposerActivity;IZ)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/composer/ComposerActivity;->a(IZ)V

    return-void
.end method

.method private ac()V
    .locals 2

    const v0, 0x7f0900fe    # com.twitter.android.R.id.tweet_footer_bar

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->I:Landroid/view/View;

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->I:Landroid/view/View;

    const v1, 0x7f090100    # com.twitter.android.R.id.photo_picker_button

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/composer/c;

    invoke-direct {v1, p0}, Lcom/twitter/android/composer/c;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private ad()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->K:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v1}, Landroid/support/v4/util/SimpleArrayMap;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private ae()V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v1}, Lcom/twitter/android/PostStorage;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->I:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_1
    iget v1, p0, Lcom/twitter/android/composer/ComposerActivity;->v:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/twitter/android/composer/ComposerActivity;->F:I

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->f:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/composer/ComposerActivity;)V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->x()V

    return-void
.end method

.method static synthetic c(Lcom/twitter/android/composer/ComposerActivity;)V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->y()V

    return-void
.end method

.method private i(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/twitter/android/composer/ComposerActivity;->M:Z

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->G:Landroid/widget/Button;

    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j(Z)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->h:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    iget v0, p0, Lcom/twitter/android/composer/ComposerActivity;->F:I

    const/16 v1, 0x8c

    if-gt v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->h:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->h:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method protected B()Z
    .locals 7

    invoke-super {p0}, Lcom/twitter/android/PostActivity;->B()Z

    move-result v1

    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->P()[Landroid/widget/FrameLayout;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    if-eqz v1, :cond_1

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getForeground()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    if-nez v5, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0075    # com.twitter.android.R.color.soft_white

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :cond_2
    return v1
.end method

.method protected P()[Landroid/widget/FrameLayout;
    .locals 6

    const/4 v5, 0x0

    const v4, 0x7f090077    # com.twitter.android.R.id.image

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->L:[Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->L:[Landroid/widget/FrameLayout;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v0, 0x7f030081    # com.twitter.android.R.layout.gallery_image_camera

    invoke-virtual {v1, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/twitter/android/composer/d;

    invoke-direct {v3, p0, v0}, Lcom/twitter/android/composer/d;-><init>(Lcom/twitter/android/composer/ComposerActivity;Landroid/widget/FrameLayout;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f030080    # com.twitter.android.R.layout.gallery_image_album

    invoke-virtual {v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/twitter/android/composer/e;

    invoke-direct {v3, p0, v1}, Lcom/twitter/android/composer/e;-><init>(Lcom/twitter/android/composer/ComposerActivity;Landroid/widget/FrameLayout;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v2, 0x2

    new-array v2, v2, [Landroid/widget/FrameLayout;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    aput-object v1, v2, v0

    iput-object v2, p0, Lcom/twitter/android/composer/ComposerActivity;->L:[Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->L:[Landroid/widget/FrameLayout;

    goto :goto_0
.end method

.method Q()V
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/twitter/android/composer/ComposerActivity;->v:I

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->i:Lcom/twitter/android/widget/ComposerLayout;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ComposerLayout;->setAllowGestures(Z)V

    invoke-virtual {p0, v1, v1}, Lcom/twitter/android/composer/ComposerActivity;->a(IZ)V

    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/PostActivity;->a(I)V

    iput p1, p0, Lcom/twitter/android/composer/ComposerActivity;->F:I

    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->ae()V

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->G:Landroid/widget/Button;

    iget-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->w:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/composer/ComposerActivity;->M:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/net/Uri;Lcom/twitter/android/AttachMediaListener$MediaAttachProgress;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/twitter/android/PostActivity;->a(Landroid/net/Uri;Lcom/twitter/android/AttachMediaListener$MediaAttachProgress;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->i(Z)V

    return-void
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 3

    if-eqz p1, :cond_0

    const-string/jumbo v0, "char_count"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/composer/ComposerActivity;->F:I

    :cond_0
    const v0, 0x7f0900ef    # com.twitter.android.R.id.composer_toolbar

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0900f4    # com.twitter.android.R.id.count

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->h:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->h:Landroid/widget/TextView;

    sget-object v2, Lcom/twitter/android/composer/ComposerActivity;->E:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0900f3    # com.twitter.android.R.id.composer_post

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->G:Landroid/widget/Button;

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->G:Landroid/widget/Button;

    new-instance v2, Lcom/twitter/android/composer/a;

    invoke-direct {v2, p0}, Lcom/twitter/android/composer/a;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0900f5    # com.twitter.android.R.id.toolbar_border

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->H:Landroid/view/View;

    const v0, 0x7f090105    # com.twitter.android.R.id.full_screen_gallery_header

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->J:Landroid/view/View;

    const v0, 0x7f090108    # com.twitter.android.R.id.composer_header_image_count

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->K:Landroid/widget/TextView;

    new-instance v0, Lcom/twitter/android/composer/b;

    invoke-direct {v0, p0}, Lcom/twitter/android/composer/b;-><init>(Lcom/twitter/android/composer/ComposerActivity;)V

    const v2, 0x7f090106    # com.twitter.android.R.id.go_back

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f090109    # com.twitter.android.R.id.select_button

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    const v1, 0x7f09010a    # com.twitter.android.R.id.full_screen_gallery_header_border

    invoke-virtual {p0, v1}, Lcom/twitter/android/composer/ComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/twitter/android/composer/ComposerActivity;->N:I

    invoke-super {p0, p1, p2}, Lcom/twitter/android/PostActivity;->a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V

    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->ac()V

    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->ae()V

    return-void
.end method

.method public a(Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;Lcom/twitter/android/PostStorage$MediaItem;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/twitter/android/PostActivity;->a(Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;Lcom/twitter/android/PostStorage$MediaItem;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->i(Z)V

    return-void
.end method

.method public a(Lcom/twitter/android/PostStorage$MediaItem;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/android/PostActivity;->a(Lcom/twitter/android/PostStorage$MediaItem;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->i(Z)V

    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->ae()V

    return-void
.end method

.method public a(Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/composer/ComposerActivity;->j(Z)V

    return-void
.end method

.method protected b(I)V
    .locals 2

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->H:Landroid/view/View;

    if-nez p1, :cond_0

    const/4 v0, 0x4

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(IZ)V
    .locals 2

    iget v0, p0, Lcom/twitter/android/composer/ComposerActivity;->v:I

    if-ne v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->i:Lcom/twitter/android/widget/ComposerLayout;

    iget v1, p0, Lcom/twitter/android/composer/ComposerActivity;->N:I

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ComposerLayout;->setFullScreenTop(I)V

    :goto_1
    invoke-super {p0, p1, p2}, Lcom/twitter/android/PostActivity;->b(IZ)V

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->ae()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->i:Lcom/twitter/android/widget/ComposerLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ComposerLayout;->setFullScreenTop(I)V

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->J:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method protected b(Lcom/twitter/android/PostActivity$PhotoAction;)V
    .locals 0

    return-void
.end method

.method protected e(Landroid/net/Uri;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/twitter/android/PostActivity;->e(Landroid/net/Uri;)V

    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->ad()V

    return-void
.end method

.method protected f()I
    .locals 1

    const v0, 0x7f030038    # com.twitter.android.R.layout.composer_layout

    return v0
.end method

.method protected f(Landroid/net/Uri;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/twitter/android/PostActivity;->f(Landroid/net/Uri;)V

    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->ad()V

    return-void
.end method

.method public f(Z)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/PostActivity;->f(Z)V

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->J:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method protected g()V
    .locals 0

    return-void
.end method

.method public g(Z)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/PostActivity;->g(Z)V

    iget v0, p0, Lcom/twitter/android/composer/ComposerActivity;->v:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->J:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->G:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setClickable(Z)V

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->i:Lcom/twitter/android/widget/ComposerLayout;

    invoke-virtual {v0}, Lcom/twitter/android/widget/ComposerLayout;->c()V

    :cond_0
    return-void
.end method

.method protected h()V
    .locals 0

    invoke-super {p0}, Lcom/twitter/android/PostActivity;->h()V

    invoke-direct {p0}, Lcom/twitter/android/composer/ComposerActivity;->ae()V

    return-void
.end method

.method public h(Z)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/PostActivity;->h(Z)V

    iget v0, p0, Lcom/twitter/android/composer/ComposerActivity;->v:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/composer/ComposerActivity;->J:Landroid/view/View;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected k()Lcom/twitter/android/widget/GalleryGridFragment;
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b007e    # com.twitter.android.R.color.white

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    const/4 v1, 0x1

    invoke-static {v0, v2, v2, v1}, Lcom/twitter/android/widget/GalleryGridFragment;->a(IIIZ)Lcom/twitter/android/widget/GalleryGridFragment;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic l()[Landroid/view/View;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->P()[Landroid/widget/FrameLayout;

    move-result-object v0

    return-object v0
.end method

.method protected m()I
    .locals 1

    const v0, 0x7f030039    # com.twitter.android.R.layout.composer_media_first_tweet_box

    return v0
.end method

.method protected n()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final onClickHandler(Landroid/view/View;)V
    .locals 6

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f090216    # com.twitter.android.R.id.mention_person

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/composer/ComposerActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, ":composition::people_icon:click"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->m()V

    :cond_0
    return-void
.end method

.method protected onResumeFragments()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/PostActivity;->onResumeFragments()V

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->n()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/twitter/android/composer/ComposerActivity;->j(Z)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/PostActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "char_count"

    iget v1, p0, Lcom/twitter/android/composer/ComposerActivity;->F:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method protected s()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->a:Landroid/support/v4/util/SimpleArrayMap;

    invoke-virtual {v0}, Landroid/support/v4/util/SimpleArrayMap;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->J:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/composer/ComposerActivity;->a(IZ)V

    :cond_0
    return-void
.end method

.method public t()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/PostActivity;->t()V

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->J:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/composer/ComposerActivity;->G:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setClickable(Z)V

    return-void
.end method
