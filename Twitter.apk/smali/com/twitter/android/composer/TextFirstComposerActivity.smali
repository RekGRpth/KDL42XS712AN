.class public Lcom/twitter/android/composer/TextFirstComposerActivity;
.super Lcom/twitter/android/PostActivity;
.source "Twttr"


# instance fields
.field private E:Landroid/widget/ImageButton;

.field private F:Landroid/widget/ImageButton;

.field private G:Landroid/widget/ImageButton;

.field private H:Landroid/widget/ImageButton;

.field private I:Landroid/view/View;

.field private J:Landroid/view/View;

.field private K:Landroid/view/View;

.field private L:Landroid/view/View;

.field private M:Landroid/view/View;

.field private N:Landroid/view/View;

.field private O:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/PostActivity;-><init>()V

    return-void
.end method

.method private P()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->H:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/twitter/android/spen/d;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "android_spen_button_display_1813"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v0, "android_spen_button_display_1813"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "hide_button"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->H:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->K:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private Q()V
    .locals 5

    const/16 v4, 0x8

    const/4 v3, 0x0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->aC()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "android_compose_people_button_1951"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v0, "android_compose_people_button_1951"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "enabled"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->G:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->L:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->G:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->L:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/composer/TextFirstComposerActivity;)V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->y()V

    return-void
.end method

.method private i(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->O:Z

    invoke-virtual {p0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    const v1, 0x7f0900f3    # com.twitter.android.R.id.composer_post

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->w:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lhn;->c(Z)Lhn;

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/net/Uri;Lcom/twitter/android/AttachMediaListener$MediaAttachProgress;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/twitter/android/PostActivity;->a(Landroid/net/Uri;Lcom/twitter/android/AttachMediaListener$MediaAttachProgress;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->i(Z)V

    return-void
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 2

    const v0, 0x7f0900e7    # com.twitter.android.R.id.camera

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->E:Landroid/widget/ImageButton;

    const v0, 0x7f0900e6    # com.twitter.android.R.id.gallery

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->F:Landroid/widget/ImageButton;

    const v0, 0x7f090216    # com.twitter.android.R.id.mention_person

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->G:Landroid/widget/ImageButton;

    const v0, 0x7f090217    # com.twitter.android.R.id.spen

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->H:Landroid/widget/ImageButton;

    const v0, 0x7f09021a    # com.twitter.android.R.id.divot_location

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->I:Landroid/view/View;

    const v0, 0x7f090219    # com.twitter.android.R.id.divot_gallery

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->J:Landroid/view/View;

    const v0, 0x7f090218    # com.twitter.android.R.id.divot_mention_person

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->L:Landroid/view/View;

    const v0, 0x7f09021b    # com.twitter.android.R.id.divot_spen

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->K:Landroid/view/View;

    const v0, 0x7f09021c    # com.twitter.android.R.id.divot_lifeline_alert

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->M:Landroid/view/View;

    const v0, 0x7f09021d    # com.twitter.android.R.id.blank_divot

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->N:Landroid/view/View;

    iget-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->N:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-super {p0, p1, p2}, Lcom/twitter/android/PostActivity;->a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V

    invoke-direct {p0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->P()V

    invoke-direct {p0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->Q()V

    invoke-virtual {p0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const v0, 0x7f09008d    # com.twitter.android.R.id.account_row

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;Lcom/twitter/android/PostStorage$MediaItem;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/twitter/android/PostActivity;->a(Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;Lcom/twitter/android/PostStorage$MediaItem;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->i(Z)V

    return-void
.end method

.method public a(Lcom/twitter/android/PostStorage$MediaItem;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/android/PostActivity;->a(Lcom/twitter/android/PostStorage$MediaItem;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->i(Z)V

    return-void
.end method

.method protected a(Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 3

    const/4 v1, 0x1

    invoke-super {p0, p1}, Lcom/twitter/android/PostActivity;->a(Lcom/twitter/internal/android/widget/ToolBar;)Z

    const v0, 0x7f0900f3    # com.twitter.android.R.id.composer_post

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v2

    iget-boolean v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->w:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->O:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Lhn;->c(Z)Lhn;

    return v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 3

    const/4 v1, 0x1

    invoke-super {p0, p1, p2}, Lcom/twitter/android/PostActivity;->a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z

    const v0, 0x7f11001c    # com.twitter.android.R.menu.post_menu

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    const v0, 0x7f0900f3    # com.twitter.android.R.id.composer_post

    invoke-virtual {p2, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v2

    iget-boolean v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->w:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->O:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Lhn;->c(Z)Lhn;

    return v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lhn;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p1}, Lhn;->a()I

    move-result v1

    const v2, 0x7f090317    # com.twitter.android.R.id.accounts

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->v()V

    :goto_0
    return v0

    :cond_0
    const v2, 0x7f0900f3    # com.twitter.android.R.id.composer_post

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->w()V

    goto :goto_0

    :cond_1
    const v2, 0x7f090045    # com.twitter.android.R.id.home

    if-ne v1, v2, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->q()Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v1, 0x201

    invoke-virtual {p0, v1}, Lcom/twitter/android/composer/TextFirstComposerActivity;->showDialog(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->finish()V

    goto :goto_0

    :cond_3
    invoke-super {p0, p1}, Lcom/twitter/android/PostActivity;->a(Lhn;)Z

    move-result v0

    goto :goto_0
.end method

.method protected b(IZ)V
    .locals 6

    const v5, 0x7f0200cd    # com.twitter.android.R.drawable.divot_tile

    const v4, 0x7f0200cc    # com.twitter.android.R.drawable.divot

    const/16 v3, 0x8

    const/4 v2, 0x0

    iget v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->v:I

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->v:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->v:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->I:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_2
    :goto_1
    packed-switch p1, :pswitch_data_0

    :goto_2
    invoke-super {p0, p1, p2}, Lcom/twitter/android/PostActivity;->b(IZ)V

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->v:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->J:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->N:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->J:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->N:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->I:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->N:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :pswitch_3
    iget-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->N:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method protected b(Lcom/twitter/android/PostActivity$PhotoAction;)V
    .locals 7

    const v6, 0x7f0f02d7    # com.twitter.android.R.string.on_status

    const v5, 0x7f020172    # com.twitter.android.R.drawable.ic_dialog_photo

    const v4, 0x7f020168    # com.twitter.android.R.drawable.ic_dialog_camera

    const v0, 0x7f0f005c    # com.twitter.android.R.string.button_action_photo

    invoke-virtual {p0, v0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0f0054    # com.twitter.android.R.string.button_action_camera

    invoke-virtual {p0, v1}, Lcom/twitter/android/composer/TextFirstComposerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/composer/g;->a:[I

    invoke-virtual {p1}, Lcom/twitter/android/PostActivity$PhotoAction;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    iget-object v2, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->E:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v2, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->E:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->F:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v1, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->F:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v2, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->E:Landroid/widget/ImageButton;

    const v3, 0x7f020169    # com.twitter.android.R.drawable.ic_dialog_camera_active

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v2, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->E:Landroid/widget/ImageButton;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v6}, Lcom/twitter/android/composer/TextFirstComposerActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->F:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v1, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->F:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->E:Landroid/widget/ImageButton;

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v2, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->E:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->F:Landroid/widget/ImageButton;

    const v2, 0x7f020173    # com.twitter.android.R.drawable.ic_dialog_photo_active

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v1, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->F:Landroid/widget/ImageButton;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v6}, Lcom/twitter/android/composer/TextFirstComposerActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected f()I
    .locals 1

    const v0, 0x7f0300ed    # com.twitter.android.R.layout.post_layout

    return v0
.end method

.method protected g()V
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03003a    # com.twitter.android.R.layout.composer_title

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0900f4    # com.twitter.android.R.id.count

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->h:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->h:Landroid/widget/TextView;

    const/16 v2, 0x8c

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    new-instance v2, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;

    const/4 v3, 0x5

    invoke-direct {v2, v3}, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/internal/android/widget/ToolBar;->a(Landroid/view/View;Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;)V

    return-void
.end method

.method protected k()Lcom/twitter/android/widget/GalleryGridFragment;
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b002d    # com.twitter.android.R.color.composer_drawer_bg

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    const v2, 0x7f0c004d    # com.twitter.android.R.dimen.composer_grid_header_height

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    const v3, 0x7f0c004b    # com.twitter.android.R.dimen.composer_divot_height

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v1, v2, v0}, Lcom/twitter/android/widget/GalleryGridFragment;->a(III)Lcom/twitter/android/widget/GalleryGridFragment;

    move-result-object v0

    return-object v0
.end method

.method protected l()[Landroid/view/View;
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030036    # com.twitter.android.R.layout.composer_gallery_album_btn

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/composer/f;

    invoke-direct {v1, p0}, Lcom/twitter/android/composer/f;-><init>(Lcom/twitter/android/composer/TextFirstComposerActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/view/View;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    return-object v1
.end method

.method protected m()I
    .locals 1

    const v0, 0x7f03003b    # com.twitter.android.R.layout.composer_tweet_box

    return v0
.end method

.method protected o()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/PostActivity;->o()V

    iget-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->M:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->f:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final onClickHandler(Landroid/view/View;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v3, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0900e6    # com.twitter.android.R.id.gallery

    if-ne v0, v1, :cond_2

    iget v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->v:I

    if-eq v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->i:Lcom/twitter/android/widget/ComposerLayout;

    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/ComposerLayout;->setAllowGestures(Z)V

    invoke-virtual {p0, v3, v3}, Lcom/twitter/android/composer/TextFirstComposerActivity;->a(IZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->i:Lcom/twitter/android/widget/ComposerLayout;

    invoke-virtual {v0, v5}, Lcom/twitter/android/widget/ComposerLayout;->setAllowGestures(Z)V

    invoke-virtual {p0, v5, v3}, Lcom/twitter/android/composer/TextFirstComposerActivity;->a(IZ)V

    goto :goto_0

    :cond_2
    const v1, 0x7f0900e7    # com.twitter.android.R.id.camera

    if-ne v0, v1, :cond_3

    invoke-virtual {p0, v5}, Lcom/twitter/android/composer/TextFirstComposerActivity;->d(Z)V

    invoke-virtual {p0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->x()V

    goto :goto_0

    :cond_3
    const v1, 0x7f090029    # com.twitter.android.R.id.photo

    if-ne v0, v1, :cond_4

    const/4 v0, 0x3

    invoke-virtual {p0, v0, v3}, Lcom/twitter/android/composer/TextFirstComposerActivity;->a(IZ)V

    goto :goto_0

    :cond_4
    const v1, 0x7f090217    # com.twitter.android.R.id.spen

    if-ne v0, v1, :cond_5

    invoke-virtual {p0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, ":composition::spen_icon:click"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-virtual {p0, v5}, Lcom/twitter/android/composer/TextFirstComposerActivity;->d(Z)V

    invoke-virtual {p0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->z()V

    goto :goto_0

    :cond_5
    const v1, 0x7f090216    # com.twitter.android.R.id.mention_person

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/composer/TextFirstComposerActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, ":composition::people_icon:click"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/composer/TextFirstComposerActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->m()V

    goto :goto_0
.end method
