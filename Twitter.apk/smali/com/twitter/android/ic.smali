.class public Lcom/twitter/android/ic;
.super Lcom/twitter/android/ye;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/app/Activity;IZZZLcom/twitter/library/widget/ap;Lcom/twitter/android/client/c;Lcom/twitter/library/widget/aa;Lcom/twitter/android/vs;Lcom/twitter/library/util/FriendshipCache;)V
    .locals 14

    const/4 v6, 0x0

    const v12, 0x7f030095    # com.twitter.android.R.layout.grouped_timeline_gap

    const v13, 0x7f030096    # com.twitter.android.R.layout.grouped_tweet_row_view

    move-object v0, p0

    move-object v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v13}, Lcom/twitter/android/ye;-><init>(Landroid/app/Activity;IZZZZLcom/twitter/library/widget/ap;Lcom/twitter/android/client/c;Lcom/twitter/library/widget/aa;Lcom/twitter/android/vs;Lcom/twitter/library/util/FriendshipCache;II)V

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;IZZZLcom/twitter/library/widget/ap;Lcom/twitter/android/client/c;Lcom/twitter/library/widget/aa;Lcom/twitter/android/vs;Lcom/twitter/library/util/FriendshipCache;Z)V
    .locals 15

    const/4 v6, 0x0

    const v12, 0x7f030095    # com.twitter.android.R.layout.grouped_timeline_gap

    const v13, 0x7f030096    # com.twitter.android.R.layout.grouped_tweet_row_view

    move-object v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move/from16 v14, p11

    invoke-direct/range {v0 .. v14}, Lcom/twitter/android/ye;-><init>(Landroid/app/Activity;IZZZZLcom/twitter/library/widget/ap;Lcom/twitter/android/client/c;Lcom/twitter/library/widget/aa;Lcom/twitter/android/vs;Lcom/twitter/library/util/FriendshipCache;IIZ)V

    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 2

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/ye;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    check-cast p1, Lcom/twitter/internal/android/widget/GroupedRowView;

    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->a(II)V

    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/ye;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/ic;->h()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ic;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Lcom/twitter/internal/android/widget/GroupedRowView;->a(II)V

    :cond_1
    return-object v1
.end method
