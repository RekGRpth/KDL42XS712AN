.class Lcom/twitter/android/yc;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/client/ak;


# instance fields
.field final synthetic a:Landroid/support/v4/app/Fragment;

.field final synthetic b:Lcom/twitter/library/api/TweetClassicCard;

.field final synthetic c:Lcom/twitter/android/yb;


# direct methods
.method constructor <init>(Lcom/twitter/android/yb;Landroid/support/v4/app/Fragment;Lcom/twitter/library/api/TweetClassicCard;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/yc;->c:Lcom/twitter/android/yb;

    iput-object p2, p0, Lcom/twitter/android/yc;->a:Landroid/support/v4/app/Fragment;

    iput-object p3, p0, Lcom/twitter/android/yc;->b:Lcom/twitter/library/api/TweetClassicCard;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/yc;->a:Landroid/support/v4/app/Fragment;

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/twitter/android/yc;->b:Lcom/twitter/library/api/TweetClassicCard;

    iget-object v2, v2, Lcom/twitter/library/api/TweetClassicCard;->url:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public b()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
