.class public Lcom/twitter/android/ve;
.super Lcom/twitter/android/ao;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    const v0, 0x7f030174    # com.twitter.android.R.layout.user_list_row_view

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/ao;-><init>(Landroid/app/Activity;I)V

    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/CharSequence;)Landroid/database/Cursor;
    .locals 6

    const/4 v4, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ve;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->e:Landroid/net/Uri;

    sget-object v2, Lcom/twitter/android/provider/m;->a:[Ljava/lang/String;

    const-string/jumbo v3, "friendship&(1|2)=1|2"

    const-string/jumbo v5, "graph_weight DESC, LOWER(name) ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    :cond_0
    return-object v4
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/ve;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
