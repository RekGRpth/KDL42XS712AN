.class Lcom/twitter/android/ik;
.super Landroid/os/AsyncTask;
.source "Twttr"

# interfaces
.implements Lcom/twitter/internal/network/p;


# instance fields
.field final synthetic a:Lcom/twitter/android/ImageActivity;


# direct methods
.method public constructor <init>(Lcom/twitter/android/ImageActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/ik;->a:Lcom/twitter/android/ImageActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 6

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/twitter/android/ik;->a:Lcom/twitter/android/ImageActivity;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v3}, Lcom/twitter/library/util/Util;->j(Landroid/content/Context;)Ljava/io/File;

    move-result-object v4

    new-instance v2, Lcom/twitter/library/service/m;

    invoke-direct {v2, v4}, Lcom/twitter/library/service/m;-><init>(Ljava/io/File;)V

    new-instance v5, Lcom/twitter/library/network/d;

    invoke-direct {v5, v3, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {v5, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    :try_start_0
    const-string/jumbo v0, "window"

    invoke-virtual {v3, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-static {v3, v2}, Lkw;->a(Landroid/content/Context;Ljava/io/InputStream;)Lkw;

    move-result-object v3

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v5

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    invoke-virtual {v3, v5, v0}, Lkw;->b(II)Lkw;

    move-result-object v0

    invoke-virtual {v0}, Lkw;->b()Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    invoke-static {v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_1
    invoke-static {v0}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    :cond_1
    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_2
    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_2

    :catch_1
    move-exception v0

    move-object v0, v2

    goto :goto_1
.end method

.method public a(JJ)V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    long-to-int v2, p1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    long-to-int v2, p3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/twitter/android/ik;->publishProgress([Ljava/lang/Object;)V

    return-void
.end method

.method protected a(Landroid/graphics/Bitmap;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/ik;->a:Lcom/twitter/android/ImageActivity;

    iget-object v0, v0, Lcom/twitter/android/ImageActivity;->b:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/ik;->a:Lcom/twitter/android/ImageActivity;

    iput-object p1, v0, Lcom/twitter/android/ImageActivity;->c:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/twitter/android/ik;->a:Lcom/twitter/android/ImageActivity;

    invoke-virtual {v0}, Lcom/twitter/android/ImageActivity;->V()V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ik;->a:Lcom/twitter/android/ImageActivity;

    iget-object v0, v0, Lcom/twitter/android/ImageActivity;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/twitter/android/ik;->a:Lcom/twitter/android/ImageActivity;

    iget-object v0, v0, Lcom/twitter/android/ImageActivity;->a:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ik;->a:Lcom/twitter/android/ImageActivity;

    const v1, 0x7f0f0222    # com.twitter.android.R.string.load_image_failure

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected varargs a([Ljava/lang/Integer;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    aget-object v0, p1, v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ik;->a:Lcom/twitter/android/ImageActivity;

    iget-object v0, v0, Lcom/twitter/android/ImageActivity;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ik;->a:Lcom/twitter/android/ImageActivity;

    iget-object v0, v0, Lcom/twitter/android/ImageActivity;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    iget-object v0, p0, Lcom/twitter/android/ik;->a:Lcom/twitter/android/ImageActivity;

    iget-object v0, v0, Lcom/twitter/android/ImageActivity;->b:Landroid/widget/ProgressBar;

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    iget-object v0, p0, Lcom/twitter/android/ik;->a:Lcom/twitter/android/ImageActivity;

    iget-object v0, v0, Lcom/twitter/android/ImageActivity;->b:Landroid/widget/ProgressBar;

    aget-object v1, p1, v2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lcom/twitter/android/ik;->a([Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/twitter/android/ik;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/ik;->a:Lcom/twitter/android/ImageActivity;

    iget-object v0, v0, Lcom/twitter/android/ImageActivity;->b:Landroid/widget/ProgressBar;

    const v1, 0x7fffffff

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    iget-object v0, p0, Lcom/twitter/android/ik;->a:Lcom/twitter/android/ImageActivity;

    iget-object v0, v0, Lcom/twitter/android/ImageActivity;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/twitter/android/ik;->a:Lcom/twitter/android/ImageActivity;

    iget-object v0, v0, Lcom/twitter/android/ImageActivity;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/ik;->a:Lcom/twitter/android/ImageActivity;

    iget-object v0, v0, Lcom/twitter/android/ImageActivity;->a:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method protected synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/twitter/android/ik;->a([Ljava/lang/Integer;)V

    return-void
.end method
