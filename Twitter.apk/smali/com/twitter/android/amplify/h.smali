.class final Lcom/twitter/android/amplify/h;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/service/c;


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Lcom/twitter/android/amplify/VideoPlayerChromeView;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/amplify/h;->a:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public a(IILcom/twitter/library/service/b;)V
    .locals 0

    return-void
.end method

.method public b(IILcom/twitter/library/service/b;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/amplify/h;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/amplify/VideoPlayerChromeView;

    instance-of v1, p3, Lir;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    check-cast p3, Lir;

    invoke-virtual {p3}, Lir;->e()I

    move-result v1

    invoke-static {v0, v1}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->a(Lcom/twitter/android/amplify/VideoPlayerChromeView;I)V

    :cond_0
    return-void
.end method
