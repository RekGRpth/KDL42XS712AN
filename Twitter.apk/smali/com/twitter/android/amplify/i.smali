.class final Lcom/twitter/android/amplify/i;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/ru;


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;


# direct methods
.method constructor <init>(Lcom/twitter/android/amplify/VideoPlayerChromeView;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/amplify/i;->a:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public a(IJLcom/twitter/library/provider/ParcelableTweet;Z)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/amplify/i;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/amplify/VideoPlayerChromeView;

    if-eqz v0, :cond_0

    if-nez p5, :cond_0

    const-string/jumbo v1, "quote"

    invoke-virtual {v0, v1}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a(IJLcom/twitter/library/provider/ParcelableTweet;ZLjava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/amplify/i;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/amplify/VideoPlayerChromeView;

    if-eqz v0, :cond_0

    if-eqz p5, :cond_1

    const v1, 0x7f0201b7    # com.twitter.android.R.drawable.ic_gallery_action_rt_off

    invoke-virtual {v0, v1}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->setRTButtonResource(I)V

    const-string/jumbo v1, "unretweet"

    invoke-virtual {v0, v1}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->a(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v1, 0x7f0201ba    # com.twitter.android.R.drawable.ic_gallery_action_rt_on

    invoke-virtual {v0, v1}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->setRTButtonResource(I)V

    const-string/jumbo v1, "retweet"

    invoke-virtual {v0, v1}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b(IJLcom/twitter/library/provider/ParcelableTweet;Z)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/amplify/i;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/amplify/VideoPlayerChromeView;

    if-eqz v0, :cond_0

    const-string/jumbo v1, "retweet_dialog::dismiss"

    invoke-virtual {v0, v1}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public c(IJLcom/twitter/library/provider/ParcelableTweet;Z)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/amplify/i;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/amplify/VideoPlayerChromeView;

    if-eqz v0, :cond_0

    const-string/jumbo v1, "retweet_dialog::impression"

    invoke-virtual {v0, v1}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
