.class Lcom/twitter/android/amplify/g;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field final synthetic a:Lcom/twitter/internal/android/widget/TypefacesTextView;

.field final synthetic b:Lcom/twitter/internal/android/widget/TypefacesTextView;

.field final synthetic c:Lcom/twitter/android/amplify/VideoPlayerChromeView;


# direct methods
.method constructor <init>(Lcom/twitter/android/amplify/VideoPlayerChromeView;Lcom/twitter/internal/android/widget/TypefacesTextView;Lcom/twitter/internal/android/widget/TypefacesTextView;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/amplify/g;->c:Lcom/twitter/android/amplify/VideoPlayerChromeView;

    iput-object p2, p0, Lcom/twitter/android/amplify/g;->a:Lcom/twitter/internal/android/widget/TypefacesTextView;

    iput-object p3, p0, Lcom/twitter/android/amplify/g;->b:Lcom/twitter/internal/android/widget/TypefacesTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/amplify/g;->a:Lcom/twitter/internal/android/widget/TypefacesTextView;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/TypefacesTextView;->getRight()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/amplify/g;->b:Lcom/twitter/internal/android/widget/TypefacesTextView;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/TypefacesTextView;->getRight()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/amplify/g;->c:Lcom/twitter/android/amplify/VideoPlayerChromeView;

    iget-object v0, v0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->b:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0}, Lcom/twitter/library/widget/ActionButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/twitter/android/amplify/g;->a:Lcom/twitter/internal/android/widget/TypefacesTextView;

    invoke-virtual {v1}, Lcom/twitter/internal/android/widget/TypefacesTextView;->getRight()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/amplify/g;->b:Lcom/twitter/internal/android/widget/TypefacesTextView;

    invoke-virtual {v2}, Lcom/twitter/internal/android/widget/TypefacesTextView;->getRight()I

    move-result v2

    if-le v1, v2, :cond_1

    const v1, 0x7f09008a    # com.twitter.android.R.id.promoted_tweet

    :goto_0
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    iget-object v1, p0, Lcom/twitter/android/amplify/g;->c:Lcom/twitter/android/amplify/VideoPlayerChromeView;

    iget-object v1, v1, Lcom/twitter/android/amplify/VideoPlayerChromeView;->b:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v1, v0}, Lcom/twitter/library/widget/ActionButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/twitter/android/amplify/g;->c:Lcom/twitter/android/amplify/VideoPlayerChromeView;

    iget-object v0, v0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->b:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0}, Lcom/twitter/library/widget/ActionButton;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_2

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const v1, 0x7f090089    # com.twitter.android.R.id.promoted_advertiser_name

    goto :goto_0

    :cond_2
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_1
.end method
