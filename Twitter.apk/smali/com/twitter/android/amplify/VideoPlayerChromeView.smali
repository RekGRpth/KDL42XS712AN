.class public Lcom/twitter/android/amplify/VideoPlayerChromeView;
.super Landroid/widget/RelativeLayout;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/library/amplify/control/h;


# instance fields
.field a:Z

.field b:Lcom/twitter/library/widget/ActionButton;

.field private c:Landroid/view/ViewGroup;

.field private d:Lcom/twitter/internal/android/widget/TypefacesTextView;

.field private e:Lcom/twitter/internal/android/widget/TypefacesTextView;

.field private f:Z

.field private g:Landroid/view/ViewGroup;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/ImageButton;

.field private j:Landroid/widget/ImageButton;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/TextView;

.field private final m:Lcom/twitter/android/amplify/i;

.field private n:Lcom/twitter/library/amplify/AmplifyPlayer;

.field private o:Z

.field private p:Z

.field private q:Lcom/twitter/library/amplify/control/f;

.field private r:Landroid/content/res/Resources;

.field private s:I

.field private t:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/twitter/android/amplify/i;

    invoke-direct {v0, p0}, Lcom/twitter/android/amplify/i;-><init>(Lcom/twitter/android/amplify/VideoPlayerChromeView;)V

    iput-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->m:Lcom/twitter/android/amplify/i;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/amplify/VideoPlayerChromeView;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->setPromoterFriendship(I)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/amplify/VideoPlayerChromeView;Landroid/content/Context;Lcom/twitter/library/amplify/AmplifyPlayer;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->b(Landroid/content/Context;Lcom/twitter/library/amplify/AmplifyPlayer;Z)V

    return-void
.end method

.method private a(Lcom/twitter/internal/android/widget/TypefacesTextView;Lcom/twitter/internal/android/widget/TypefacesTextView;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->b:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0}, Lcom/twitter/library/widget/ActionButton;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/twitter/android/amplify/g;

    invoke-direct {v1, p0, p1, p2}, Lcom/twitter/android/amplify/g;-><init>(Lcom/twitter/android/amplify/VideoPlayerChromeView;Lcom/twitter/internal/android/widget/TypefacesTextView;Lcom/twitter/internal/android/widget/TypefacesTextView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :cond_0
    return-void
.end method

.method private b(Landroid/content/Context;Lcom/twitter/library/amplify/AmplifyPlayer;Z)V
    .locals 6

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->setWillNotDraw(Z)V

    const-string/jumbo v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f030013    # com.twitter.android.R.layout.amplify_player_chrome

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->c:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->c:Landroid/view/ViewGroup;

    invoke-virtual {p0, v1}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->addView(Landroid/view/View;)V

    iput-object p2, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->n:Lcom/twitter/library/amplify/AmplifyPlayer;

    iput-boolean p3, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->o:Z

    iget-object v1, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->n:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->h()Lcom/twitter/library/provider/Tweet;

    move-result-object v4

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->r:Landroid/content/res/Resources;

    invoke-static {p1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    if-eqz v4, :cond_0

    invoke-static {v1, v4}, Lcom/twitter/android/amplify/d;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/provider/Tweet;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->f:Z

    iget-object v1, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->c:Landroid/view/ViewGroup;

    const v5, 0x7f09008b    # com.twitter.android.R.id.amplify_action_button

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/widget/ActionButton;

    iput-object v1, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->b:Lcom/twitter/library/widget/ActionButton;

    iget-object v1, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->c:Landroid/view/ViewGroup;

    const v5, 0x7f09008a    # com.twitter.android.R.id.promoted_tweet

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/internal/android/widget/TypefacesTextView;

    iput-object v1, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->d:Lcom/twitter/internal/android/widget/TypefacesTextView;

    iget-object v1, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->c:Landroid/view/ViewGroup;

    const v5, 0x7f090089    # com.twitter.android.R.id.promoted_advertiser_name

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/internal/android/widget/TypefacesTextView;

    iput-object v1, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->e:Lcom/twitter/internal/android/widget/TypefacesTextView;

    invoke-direct {p0, v4}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->c(Lcom/twitter/library/provider/Tweet;)V

    const v1, 0x7f0300b5    # com.twitter.android.R.layout.media_action_bar

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->g:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->g:Landroid/view/ViewGroup;

    const v1, 0x7f0901ca    # com.twitter.android.R.id.reply_label

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->h:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->g:Landroid/view/ViewGroup;

    const v1, 0x7f09006f    # com.twitter.android.R.id.favorite

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->j:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->g:Landroid/view/ViewGroup;

    const v1, 0x7f0901cc    # com.twitter.android.R.id.favorite_label

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->k:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->g:Landroid/view/ViewGroup;

    const v1, 0x7f09006e    # com.twitter.android.R.id.retweet

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->i:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->g:Landroid/view/ViewGroup;

    const v1, 0x7f0901cb    # com.twitter.android.R.id.retweet_label

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->a()V

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->g:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iput-boolean v2, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->a:Z

    return-void

    :cond_0
    move v1, v3

    goto/16 :goto_0
.end method

.method private c(Lcom/twitter/library/provider/Tweet;)V
    .locals 8

    const/16 v3, 0x8

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/twitter/android/amplify/d;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/provider/Tweet;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->e:Lcom/twitter/internal/android/widget/TypefacesTextView;

    invoke-virtual {v0, v3}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->d:Lcom/twitter/internal/android/widget/TypefacesTextView;

    invoke-virtual {v0, v3}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->b:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0, v3}, Lcom/twitter/library/widget/ActionButton;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->m()Lcom/twitter/library/api/PromotedContent;

    move-result-object v2

    iget-wide v3, v2, Lcom/twitter/library/api/PromotedContent;->advertiserId:J

    invoke-static {v0}, Lcom/twitter/library/client/w;->a(Landroid/content/Context;)Lcom/twitter/library/client/w;

    move-result-object v5

    new-instance v6, Lir;

    invoke-direct {v6, v0, v1}, Lir;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {v6, v3, v4}, Lir;->a(J)Lir;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/amplify/h;

    invoke-direct {v1, p0}, Lcom/twitter/android/amplify/h;-><init>(Lcom/twitter/android/amplify/VideoPlayerChromeView;)V

    invoke-virtual {v5, v0, v7, v7, v1}, Lcom/twitter/library/client/w;->a(Lcom/twitter/library/service/b;IILcom/twitter/library/service/c;)Z

    invoke-virtual {v2}, Lcom/twitter/library/api/PromotedContent;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f020124    # com.twitter.android.R.drawable.ic_badge_gov_default

    :goto_1
    iget-boolean v1, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->p:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->d:Lcom/twitter/internal/android/widget/TypefacesTextView;

    invoke-virtual {v1, v7, v7, v0, v7}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    :goto_2
    iget-object v0, v2, Lcom/twitter/library/api/PromotedContent;->advertiserName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, v2, Lcom/twitter/library/api/PromotedContent;->advertiserName:Ljava/lang/String;

    :goto_3
    iget-object v1, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->e:Lcom/twitter/internal/android/widget/TypefacesTextView;

    invoke-virtual {v1, v0}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->b:Lcom/twitter/library/widget/ActionButton;

    const v1, 0x7f020085    # com.twitter.android.R.drawable.btn_follow_action

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ActionButton;->a(I)V

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->b:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0, p0}, Lcom/twitter/library/widget/ActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->b:Lcom/twitter/library/widget/ActionButton;

    const v1, 0x7f020086    # com.twitter.android.R.drawable.btn_follow_action_bg

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ActionButton;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->d:Lcom/twitter/internal/android/widget/TypefacesTextView;

    iget-object v1, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->e:Lcom/twitter/internal/android/widget/TypefacesTextView;

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->a(Lcom/twitter/internal/android/widget/TypefacesTextView;Lcom/twitter/internal/android/widget/TypefacesTextView;)V

    goto :goto_0

    :cond_1
    const v0, 0x7f020125    # com.twitter.android.R.drawable.ic_badge_promoted_default

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->d:Lcom/twitter/internal/android/widget/TypefacesTextView;

    invoke-virtual {v1, v0, v7, v7, v7}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_2

    :cond_3
    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->h()Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method private d(Lcom/twitter/library/provider/Tweet;)V
    .locals 9

    const v1, 0x7f0b007e    # com.twitter.android.R.color.white

    invoke-virtual {p0}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v0, p1, Lcom/twitter/library/provider/Tweet;->ag:I

    iget v3, p1, Lcom/twitter/library/provider/Tweet;->ae:I

    iget v4, p1, Lcom/twitter/library/provider/Tweet;->V:I

    invoke-virtual {p0}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    invoke-static {v5}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/android/client/c;->T()Z

    move-result v5

    iget-object v8, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->h:Landroid/widget/TextView;

    if-eqz v5, :cond_1

    if-lez v0, :cond_1

    invoke-static {v2, v0}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->k:Landroid/widget/TextView;

    if-lez v3, :cond_2

    invoke-static {v2, v3}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->k:Landroid/widget/TextView;

    iget-boolean v0, p1, Lcom/twitter/library/provider/Tweet;->l:Z

    if-eqz v0, :cond_3

    const v0, 0x7f0b0066    # com.twitter.android.R.color.orange

    :goto_2
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v3, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->l:Landroid/widget/TextView;

    if-lez v4, :cond_4

    invoke-static {v2, v4}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->l:Landroid/widget/TextView;

    iget-boolean v3, p1, Lcom/twitter/library/provider/Tweet;->r:Z

    if-eqz v3, :cond_0

    invoke-virtual {p1, v6, v7}, Lcom/twitter/library/provider/Tweet;->a(J)Z

    move-result v3

    if-eqz v3, :cond_0

    const v1, 0x7f0b0058    # com.twitter.android.R.color.green

    :cond_0
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void

    :cond_1
    const-string/jumbo v0, ""

    goto :goto_0

    :cond_2
    const-string/jumbo v0, ""

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    const-string/jumbo v0, ""

    goto :goto_3
.end method

.method private g()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->f:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()V
    .locals 3

    const/4 v2, -0x1

    invoke-direct {p0}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->j()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->g:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->r:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v1, v0, :cond_3

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-boolean v1, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->f:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    const v2, 0x7f09008b    # com.twitter.android.R.id.amplify_action_button

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    :cond_2
    iget-object v1, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->c:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->g:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->g:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->g:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->addView(Landroid/view/View;)V

    goto :goto_1
.end method

.method private j()Z
    .locals 1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->o:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setPromoterFriendship(I)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->b:Lcom/twitter/library/widget/ActionButton;

    invoke-static {p1}, Lcom/twitter/library/provider/ay;->b(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ActionButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->b:Lcom/twitter/library/widget/ActionButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ActionButton;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method a()V
    .locals 5

    invoke-direct {p0}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->i()V

    invoke-virtual {p0}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->d()V

    new-instance v0, Lcom/twitter/library/amplify/control/f;

    iget-object v1, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->c:Landroid/view/ViewGroup;

    invoke-direct {p0}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->h()Z

    move-result v2

    const v3, 0x7f040018    # com.twitter.android.R.anim.slide_down_from_top

    const v4, 0x7f04001b    # com.twitter.android.R.anim.slide_up_to_top

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/library/amplify/control/f;-><init>(Landroid/view/View;ZII)V

    iput-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->q:Lcom/twitter/library/amplify/control/f;

    invoke-direct {p0}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->h()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->c:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->c:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(II)V
    .locals 0

    iput p1, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->s:I

    iput p2, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->t:I

    return-void
.end method

.method public a(Landroid/content/Context;Lcom/twitter/library/amplify/AmplifyPlayer;Z)V
    .locals 3

    new-instance v0, Lcom/twitter/android/amplify/f;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/twitter/android/amplify/f;-><init>(Lcom/twitter/android/amplify/VideoPlayerChromeView;Landroid/content/Context;Lcom/twitter/library/amplify/AmplifyPlayer;Z)V

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-ne v1, v2, :cond_0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :goto_0
    return-void

    :cond_0
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/provider/Tweet;)V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    iget-boolean v2, p1, Lcom/twitter/library/provider/Tweet;->l:Z

    if-eqz v2, :cond_0

    invoke-virtual {p0, p1, v1, v0}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->b(Lcom/twitter/library/provider/Tweet;Lcom/twitter/android/client/c;Lcom/twitter/library/client/Session;)V

    const/4 v0, 0x0

    iput-boolean v0, p1, Lcom/twitter/library/provider/Tweet;->l:Z

    const-string/jumbo v0, "unfavorite"

    invoke-virtual {p0, v0}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->a(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1, v1, v0}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/android/client/c;Lcom/twitter/library/client/Session;)V

    const/4 v0, 0x1

    iput-boolean v0, p1, Lcom/twitter/library/provider/Tweet;->l:Z

    const-string/jumbo v0, "favorite"

    invoke-virtual {p0, v0}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method a(Lcom/twitter/library/provider/Tweet;ILandroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 6

    const/4 v0, 0x0

    const v1, 0x7f09006d    # com.twitter.android.R.id.reply

    if-ne p2, v1, :cond_1

    invoke-virtual {p0, p1, p3, p4}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->a(Lcom/twitter/library/provider/Tweet;Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const-string/jumbo v0, "reply"

    invoke-virtual {p0, v0}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->a(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v1, 0x7f09006f    # com.twitter.android.R.id.favorite

    if-ne p2, v1, :cond_2

    invoke-virtual {p0, p1}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->a(Lcom/twitter/library/provider/Tweet;)V

    goto :goto_0

    :cond_2
    const v1, 0x7f09006e    # com.twitter.android.R.id.retweet

    if-ne p2, v1, :cond_3

    instance-of v1, p3, Landroid/support/v4/app/FragmentActivity;

    if-eqz v1, :cond_0

    move-object v5, p3

    check-cast v5, Landroid/support/v4/app/FragmentActivity;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->m:Lcom/twitter/android/amplify/i;

    move-object v1, p1

    move v2, v0

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/rs;->a(ILcom/twitter/library/provider/Tweet;ZLandroid/support/v4/app/Fragment;Lcom/twitter/android/ru;Landroid/support/v4/app/FragmentActivity;)V

    goto :goto_0

    :cond_3
    const v0, 0x7f090070    # com.twitter.android.R.id.share

    if-ne p2, v0, :cond_4

    invoke-virtual {p0, p1, p3}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->a(Lcom/twitter/library/provider/Tweet;Landroid/content/Context;)V

    const-string/jumbo v0, "share"

    invoke-virtual {p0, v0}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const v0, 0x7f09008b    # com.twitter.android.R.id.amplify_action_button

    if-ne p2, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->b()V

    goto :goto_0
.end method

.method a(Lcom/twitter/library/provider/Tweet;Landroid/content/Context;)V
    .locals 8

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/twitter/library/provider/Tweet;->p:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/provider/Tweet;->d:Ljava/lang/String;

    iget-wide v4, p1, Lcom/twitter/library/provider/Tweet;->h:J

    iget-wide v6, p1, Lcom/twitter/library/provider/Tweet;->o:J

    move-object v0, p2

    invoke-static/range {v0 .. v7}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    return-void
.end method

.method a(Lcom/twitter/library/provider/Tweet;Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 2

    sget-object v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->c:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    invoke-static {p2, v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Landroid/content/Context;Lcom/twitter/android/composer/ComposerIntentWrapper$Action;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Lcom/twitter/library/provider/ParcelableTweet;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    invoke-virtual {p3}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Ljava/lang/String;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/android/composer/ComposerIntentWrapper;->b(Landroid/content/Context;)V

    return-void
.end method

.method a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/android/client/c;Lcom/twitter/library/client/Session;)V
    .locals 7

    iget-wide v2, p1, Lcom/twitter/library/provider/Tweet;->o:J

    iget-wide v4, p1, Lcom/twitter/library/provider/Tweet;->C:J

    iget-object v6, p1, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    move-object v0, p2

    move-object v1, p3

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;JJLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    return-void
.end method

.method a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->n:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0, p1}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Ljava/lang/String;)V

    return-void
.end method

.method b()V
    .locals 2

    iget-object v1, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->n:Lcom/twitter/library/amplify/AmplifyPlayer;

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->b:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0}, Lcom/twitter/library/widget/ActionButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->f(Z)V

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->b:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0}, Lcom/twitter/library/widget/ActionButton;->toggle()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b(Lcom/twitter/library/provider/Tweet;)V
    .locals 13

    invoke-virtual {p0}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x5

    new-array v12, v2, [I

    fill-array-data v12, :array_0

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->g:Landroid/view/ViewGroup;

    const v4, 0x7f0201ba    # com.twitter.android.R.drawable.ic_gallery_action_rt_on

    const v5, 0x7f0201b7    # com.twitter.android.R.drawable.ic_gallery_action_rt_off

    const v3, 0x7f0f007d    # com.twitter.android.R.string.button_status_retweeted

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v3, 0x7f0f005e    # com.twitter.android.R.string.button_action_retweet

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const v8, 0x7f0201b1    # com.twitter.android.R.drawable.ic_gallery_action_fave_on

    const v9, 0x7f0201ae    # com.twitter.android.R.drawable.ic_gallery_action_fave_off

    const v3, 0x7f0f007c    # com.twitter.android.R.string.button_status_favorited

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    const v3, 0x7f0f0058    # com.twitter.android.R.string.button_action_fave

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    move-object v1, p1

    move-object v3, p0

    invoke-static/range {v0 .. v12}, Lcom/twitter/android/widget/dn;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/provider/Tweet;Landroid/view/ViewGroup;Landroid/view/View$OnClickListener;IILjava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;[I)V

    invoke-direct {p0, p1}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->d(Lcom/twitter/library/provider/Tweet;)V

    :cond_0
    return-void

    nop

    :array_0
    .array-data 4
        0x7f09006d    # com.twitter.android.R.id.reply
        0x7f09006e    # com.twitter.android.R.id.retweet
        0x7f09006f    # com.twitter.android.R.id.favorite
        0x7f090070    # com.twitter.android.R.id.share
        0x7f0901a3    # com.twitter.android.R.id.dismiss
    .end array-data
.end method

.method b(Lcom/twitter/library/provider/Tweet;Lcom/twitter/android/client/c;Lcom/twitter/library/client/Session;)V
    .locals 3

    iget-wide v0, p1, Lcom/twitter/library/provider/Tweet;->o:J

    iget-object v2, p1, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {p2, p3, v0, v1, v2}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/Session;JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    return-void
.end method

.method public c()V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->d()V

    :cond_0
    return-void
.end method

.method d()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->n:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->h()Lcom/twitter/library/provider/Tweet;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->b(Lcom/twitter/library/provider/Tweet;)V

    :cond_0
    return-void
.end method

.method public e()V
    .locals 2

    invoke-direct {p0}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->q:Lcom/twitter/library/amplify/control/f;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/f;->b()V

    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->g:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method public f()V
    .locals 2

    invoke-direct {p0}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->g()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->q:Lcom/twitter/library/amplify/control/f;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/f;->a()V

    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->g:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->n:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->h()Lcom/twitter/library/provider/Tweet;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {p0}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->a(Lcom/twitter/library/provider/Tweet;ILandroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->b(Lcom/twitter/library/provider/Tweet;)V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-boolean v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->a()V

    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    iget-boolean v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->a:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->c:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v1

    add-int/2addr v1, p3

    invoke-virtual {v0, p2, p3, p4, v1}, Landroid/view/ViewGroup;->layout(IIII)V

    invoke-direct {p0}, Lcom/twitter/android/amplify/VideoPlayerChromeView;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->r:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v1, v0, :cond_0

    iget v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->s:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->t:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->s:I

    iget v1, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->t:I

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/amplify/n;->a(IIIIII)Landroid/graphics/Rect;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->g:Landroid/view/ViewGroup;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, p2, v0, p4, p5}, Landroid/view/ViewGroup;->layout(IIII)V

    goto :goto_0
.end method

.method setRTButtonResource(I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/amplify/VideoPlayerChromeView;->i:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setImageResource(I)V

    return-void
.end method
