.class public Lcom/twitter/android/amplify/a;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string/jumbo v1, "playback_0"

    const-string/jumbo v2, "start"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "playback_25"

    const-string/jumbo v2, "firstQuartile"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "playback_50"

    const-string/jumbo v2, "midpoint"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "playback_75"

    const-string/jumbo v2, "thirdQuartile"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "playback_100"

    const-string/jumbo v2, "complete"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "pause"

    const-string/jumbo v2, "pause"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "resume"

    const-string/jumbo v2, "resume"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "rewind"

    const-string/jumbo v2, "rewind"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/amplify/a;->a:Ljava/util/Map;

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;ILcom/twitter/library/amplify/model/AmplifyVideo;DIZLjava/lang/String;Z)V
    .locals 18

    const/4 v5, 0x2

    move/from16 v0, p8

    if-ne v0, v5, :cond_0

    const-string/jumbo v5, "2"

    move-object/from16 v17, v5

    :goto_0
    if-eqz p2, :cond_1

    invoke-virtual/range {p2 .. p2}, Lcom/twitter/library/scribe/ScribeAssociation;->b()Ljava/lang/String;

    move-result-object v5

    move-object v6, v5

    :goto_1
    if-eqz p11, :cond_2

    const-string/jumbo v5, "tweet:platform_forward_amplify_card"

    :goto_2
    new-instance v7, Lcom/twitter/library/scribe/ScribeLog;

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v8

    invoke-virtual {v8}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v8

    invoke-virtual {v8}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    invoke-direct {v7, v8, v9}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object v6, v8, v9

    const/4 v6, 0x1

    const-string/jumbo v9, ""

    aput-object v9, v8, v6

    const/4 v6, 0x2

    aput-object v5, v8, v6

    const/4 v5, 0x3

    aput-object p3, v8, v5

    invoke-virtual {v7, v8}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v5

    if-eqz p5, :cond_3

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {p5 .. p7}, Lcom/twitter/library/amplify/model/AmplifyVideo;->b(D)Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {p5 .. p7}, Lcom/twitter/library/amplify/model/AmplifyVideo;->c(D)Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p5 .. p7}, Lcom/twitter/library/amplify/model/AmplifyVideo;->d(D)J

    move-result-wide v13

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move/from16 v10, p4

    move/from16 v15, p9

    move-object/from16 v16, p10

    invoke-virtual/range {v5 .. v16}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;JZLjava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    :goto_3
    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Lcom/twitter/library/scribe/ScribeLog;->j(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p5

    move-wide/from16 v3, p6

    invoke-static {v0, v1, v2, v3, v4}, Lcom/twitter/android/amplify/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/amplify/model/AmplifyVideo;D)V

    return-void

    :cond_0
    const-string/jumbo v5, "1"

    move-object/from16 v17, v5

    goto :goto_0

    :cond_1
    const-string/jumbo v5, "tweet"

    move-object v6, v5

    goto :goto_1

    :cond_2
    const-string/jumbo v5, "tweet:platform_amplify_card"

    goto :goto_2

    :cond_3
    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2, v6}, Lcom/twitter/library/scribe/ScribeItem;->a(Landroid/content/Context;Lcom/twitter/library/provider/ParcelableTweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeItem;)V

    invoke-virtual {v5, v6}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    goto :goto_3
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeAssociation;Lcom/twitter/library/provider/Tweet;ZZ)V
    .locals 11

    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v4

    if-eqz p3, :cond_0

    const-string/jumbo v0, "follow"

    :goto_0
    invoke-virtual {p2}, Lcom/twitter/library/provider/Tweet;->m()Lcom/twitter/library/api/PromotedContent;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-wide v1, v1, Lcom/twitter/library/api/PromotedContent;->advertiserId:J

    move-wide v2, v1

    :goto_1
    if-eqz p3, :cond_2

    iput v9, p2, Lcom/twitter/library/provider/Tweet;->ac:I

    iget-object v1, p2, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v4, v2, v3, v8, v1}, Lcom/twitter/android/client/c;->a(JZLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    :goto_2
    if-eqz p4, :cond_3

    const-string/jumbo v1, "platform_forward_amplify_card"

    :goto_3
    new-instance v4, Lcom/twitter/library/scribe/ScribeLog;

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v5, v9, [Ljava/lang/String;

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/String;

    const-string/jumbo v7, "tweet::tweet:"

    aput-object v7, v6, v8

    aput-object v1, v6, v9

    const/4 v1, 0x2

    aput-object v0, v6, v1

    invoke-static {v6}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v8

    invoke-virtual {v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v0, v2, v3, v1, v10}, Lcom/twitter/library/scribe/ScribeLog;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, p0, p2, p1, v10}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    return-void

    :cond_0
    const-string/jumbo v0, "unfollow"

    goto :goto_0

    :cond_1
    invoke-virtual {p2}, Lcom/twitter/library/provider/Tweet;->f()J

    move-result-wide v1

    move-wide v2, v1

    goto :goto_1

    :cond_2
    iput v8, p2, Lcom/twitter/library/provider/Tweet;->ac:I

    iget-object v1, p2, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v4, v2, v3, v1}, Lcom/twitter/android/client/c;->a(JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    goto :goto_2

    :cond_3
    const-string/jumbo v1, "platform_amplify_card"

    goto :goto_3
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/amplify/model/AmplifyVideo;D)V
    .locals 4

    sget-object v0, Lcom/twitter/android/amplify/a;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2, v0, p3, p4}, Lcom/twitter/library/amplify/model/AmplifyVideo;->a(Ljava/lang/String;D)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    new-instance v1, Lcom/twitter/android/amplify/c;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/twitter/android/amplify/c;-><init>(Ljava/util/List;Lcom/twitter/android/amplify/b;)V

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-virtual {v1, v0}, Lcom/twitter/android/amplify/c;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    return-void
.end method
