.class public Lcom/twitter/android/sp;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final m:Landroid/util/SparseIntArray;


# instance fields
.field public final a:J

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:Ljava/lang/String;

.field public g:I

.field public h:Lcom/twitter/library/api/TwitterSearchSuggestion;

.field public i:Ljava/lang/String;

.field public j:Lcom/twitter/library/api/TwitterSearchHighlight;

.field public k:Lcom/twitter/library/api/af;

.field public l:Lcom/twitter/library/api/TwitterSearchFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v3, 0x1f

    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    sput-object v0, Lcom/twitter/android/sp;->m:Landroid/util/SparseIntArray;

    sget-object v0, Lcom/twitter/android/sp;->m:Landroid/util/SparseIntArray;

    const/4 v1, 0x2

    const/16 v2, 0x1e

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/twitter/android/sp;->m:Landroid/util/SparseIntArray;

    const/4 v1, 0x4

    const/16 v2, 0x1d

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/twitter/android/sp;->m:Landroid/util/SparseIntArray;

    const/4 v1, 0x7

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/twitter/android/sp;->m:Landroid/util/SparseIntArray;

    const/4 v1, 0x6

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->put(II)V

    return-void
.end method

.method public constructor <init>(JIIIILjava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/twitter/android/sp;->a:J

    iput p3, p0, Lcom/twitter/android/sp;->b:I

    iput p4, p0, Lcom/twitter/android/sp;->c:I

    iput p5, p0, Lcom/twitter/android/sp;->d:I

    iput p6, p0, Lcom/twitter/android/sp;->e:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/twitter/android/sp;->g:I

    iput-object p7, p0, Lcom/twitter/android/sp;->f:Ljava/lang/String;

    return-void
.end method

.method public static a(I)I
    .locals 2

    sget-object v0, Lcom/twitter/android/sp;->m:Landroid/util/SparseIntArray;

    const/16 v1, 0xb

    invoke-virtual {v0, p0, v1}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    return v0
.end method
