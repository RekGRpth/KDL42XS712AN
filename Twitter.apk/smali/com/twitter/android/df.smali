.class Lcom/twitter/android/df;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;

.field private final b:Landroid/content/Context;

.field private final c:I

.field private final d:Z


# direct methods
.method constructor <init>(Lcom/twitter/android/CropManager;Landroid/content/Context;IZ)V
    .locals 1

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/df;->a:Ljava/lang/ref/WeakReference;

    iput-object p2, p0, Lcom/twitter/android/df;->b:Landroid/content/Context;

    iput p3, p0, Lcom/twitter/android/df;->c:I

    iput-boolean p4, p0, Lcom/twitter/android/df;->d:Z

    return-void
.end method


# virtual methods
.method protected varargs a([Landroid/net/Uri;)Lcom/twitter/android/dc;
    .locals 8

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/twitter/android/df;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/df;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move-object v0, v1

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/4 v2, 0x0

    :try_start_0
    aget-object v5, p1, v2

    invoke-static {v3, v5}, Lkw;->a(Landroid/content/Context;Landroid/net/Uri;)Lkw;

    move-result-object v2

    invoke-virtual {v2}, Lkw;->c()Landroid/graphics/Rect;

    move-result-object v6

    iget v7, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v2, v7, v0}, Lkw;->a(II)Lkw;

    move-result-object v0

    invoke-virtual {v0}, Lkw;->b()Landroid/graphics/Bitmap;

    move-result-object v2

    new-instance v0, Lcom/twitter/android/dc;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v7

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    invoke-direct {v0, v2, v7, v6}, Lcom/twitter/android/dc;-><init>(Landroid/graphics/Bitmap;II)V

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/df;->isCancelled()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_5

    :cond_3
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/twitter/media/filters/Filters;->a()V

    :cond_4
    move-object v0, v1

    goto :goto_0

    :cond_5
    :try_start_1
    iget v2, p0, Lcom/twitter/android/df;->c:I

    if-nez v2, :cond_6

    iget-boolean v2, p0, Lcom/twitter/android/df;->d:Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v2, :cond_6

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/twitter/media/filters/Filters;->a()V

    goto :goto_0

    :cond_6
    :try_start_2
    iget-object v6, v0, Lcom/twitter/android/dc;->a:Landroid/graphics/Bitmap;

    new-instance v2, Lcom/twitter/media/filters/Filters;

    invoke-direct {v2}, Lcom/twitter/media/filters/Filters;-><init>()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const v7, 0x7f070004    # com.twitter.android.R.raw.filter_resources

    :try_start_3
    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->openRawResourceFd(I)Landroid/content/res/AssetFileDescriptor;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/twitter/media/filters/Filters;->a(Landroid/content/Context;Landroid/content/res/AssetFileDescriptor;)Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {p0}, Lcom/twitter/android/df;->isCancelled()Z

    move-result v3

    if-nez v3, :cond_7

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    iget-boolean v7, p0, Lcom/twitter/android/df;->d:Z

    invoke-virtual {v2, v5, v3, v4, v7}, Lcom/twitter/media/filters/Filters;->a(Landroid/net/Uri;IIZ)I

    move-result v3

    if-lez v3, :cond_7

    invoke-virtual {p0}, Lcom/twitter/android/df;->isCancelled()Z

    move-result v4

    if-nez v4, :cond_7

    iget v4, p0, Lcom/twitter/android/df;->c:I

    invoke-virtual {v2, v4, v3, v6}, Lcom/twitter/media/filters/Filters;->a(IILandroid/graphics/Bitmap;)Z
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v3

    if-eqz v3, :cond_7

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/twitter/media/filters/Filters;->a()V

    goto/16 :goto_0

    :cond_7
    if-eqz v2, :cond_8

    invoke-virtual {v2}, Lcom/twitter/media/filters/Filters;->a()V

    :cond_8
    move-object v0, v1

    goto/16 :goto_0

    :catch_0
    move-exception v0

    move-object v2, v1

    :goto_1
    :try_start_4
    invoke-static {v0}, Lcom/crashlytics/android/d;->a(Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Lcom/twitter/media/filters/Filters;->a()V

    :cond_9
    move-object v0, v1

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/twitter/media/filters/Filters;->a()V

    :cond_a
    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method protected a(Lcom/twitter/android/dc;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/df;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/CropManager;

    if-eqz v0, :cond_0

    invoke-static {v0, p1}, Lcom/twitter/android/CropManager;->a(Lcom/twitter/android/CropManager;Lcom/twitter/android/dc;)V

    :cond_0
    return-void
.end method

.method protected b(Lcom/twitter/android/dc;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/df;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/CropManager;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/CropManager;->a(Lcom/twitter/android/CropManager;Lcom/twitter/android/dc;)V

    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lcom/twitter/android/df;->a([Landroid/net/Uri;)Lcom/twitter/android/dc;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/twitter/android/dc;

    invoke-virtual {p0, p1}, Lcom/twitter/android/df;->b(Lcom/twitter/android/dc;)V

    return-void
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/twitter/android/dc;

    invoke-virtual {p0, p1}, Lcom/twitter/android/df;->a(Lcom/twitter/android/dc;)V

    return-void
.end method
