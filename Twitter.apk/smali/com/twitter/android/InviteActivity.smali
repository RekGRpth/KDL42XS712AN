.class public Lcom/twitter/android/InviteActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/android/aag;
.implements Lcom/twitter/android/ir;
.implements Lcom/twitter/android/widget/ce;


# instance fields
.field private a:Lcom/twitter/android/FollowFlowController;

.field private b:Z

.field private c:Lcom/twitter/android/InviteFragment;

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    iput-boolean v0, p0, Lcom/twitter/android/InviteActivity;->b:Z

    iput v0, p0, Lcom/twitter/android/InviteActivity;->d:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/InviteActivity;->e:I

    return-void
.end method

.method private f()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/InviteActivity;->c:Lcom/twitter/android/InviteFragment;

    invoke-virtual {v0}, Lcom/twitter/android/InviteFragment;->q()[Lcom/twitter/library/api/TwitterContact;

    move-result-object v0

    array-length v1, v0

    if-lez v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/InviteActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a([Lcom/twitter/library/api/TwitterContact;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/InviteActivity;->d(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/InviteActivity;->g()V

    return-void
.end method

.method private g()V
    .locals 3

    const-class v0, Lcom/twitter/android/TabbedFindPeopleActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/InviteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "originating_activity"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/TabbedFindPeopleActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "hide_contacts_import_cta"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/InviteActivity;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/InviteActivity;->a:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v0, p0}, Lcom/twitter/android/FollowFlowController;->a(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method private h()V
    .locals 7

    iget-boolean v0, p0, Lcom/twitter/android/InviteActivity;->b:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/twitter/android/InviteActivity;->e:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const v0, 0x7f0f0224    # com.twitter.android.R.string.loading

    invoke-virtual {p0, v0}, Lcom/twitter/android/InviteActivity;->setTitle(I)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/twitter/android/InviteActivity;->e:I

    if-lez v0, :cond_1

    iget v0, p0, Lcom/twitter/android/InviteActivity;->d:I

    int-to-float v0, v0

    iget v1, p0, Lcom/twitter/android/InviteActivity;->e:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    :goto_1
    const v1, 0x7f0f0226    # com.twitter.android.R.string.loading_progress

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Ljava/text/NumberFormat;->getPercentInstance()Ljava/text/NumberFormat;

    move-result-object v4

    float-to-double v5, v0

    invoke-virtual {v4, v5, v6}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/twitter/android/InviteActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/InviteActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const v0, 0x7f0f01f8    # com.twitter.android.R.string.invites_title

    invoke-virtual {p0, v0}, Lcom/twitter/android/InviteActivity;->setTitle(I)V

    goto :goto_0
.end method

.method private k()V
    .locals 5

    const/4 v4, -0x2

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300d8    # com.twitter.android.R.layout.nux_progress_indicator

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f09000b    # com.twitter.android.R.id.indicator

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/twitter/android/InviteActivity;->a:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v2, p0}, Lcom/twitter/android/FollowFlowController;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/twitter/android/InviteActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    new-instance v2, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;

    const/4 v3, 0x5

    invoke-direct {v2, v4, v4, v3}, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/internal/android/widget/ToolBar;->a(Landroid/view/View;Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/InviteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-nez p1, :cond_1

    const-string/jumbo v1, "flow_controller"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    :goto_0
    check-cast v0, Lcom/twitter/android/FollowFlowController;

    check-cast v0, Lcom/twitter/android/FollowFlowController;

    iput-object v0, p0, Lcom/twitter/android/InviteActivity;->a:Lcom/twitter/android/FollowFlowController;

    iget-object v0, p0, Lcom/twitter/android/InviteActivity;->a:Lcom/twitter/android/FollowFlowController;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/FollowFlowController;

    invoke-direct {v0}, Lcom/twitter/android/FollowFlowController;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/InviteActivity;->a:Lcom/twitter/android/FollowFlowController;

    :cond_0
    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    iget-object v1, p0, Lcom/twitter/android/InviteActivity;->a:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v1}, Lcom/twitter/android/FollowFlowController;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x7f03004a    # com.twitter.android.R.layout.digits_nux_activity

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->c(I)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->a(Z)V

    :goto_1
    return-object v0

    :cond_1
    const-string/jumbo v0, "flow_controller"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->a(Z)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->a(I)V

    goto :goto_1
.end method

.method public a(IIILjava/util/ArrayList;)V
    .locals 2

    const/4 v1, -0x1

    if-eq p3, v1, :cond_0

    iput p3, p0, Lcom/twitter/android/InviteActivity;->e:I

    if-eq p2, v1, :cond_0

    iget v0, p0, Lcom/twitter/android/InviteActivity;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/android/InviteActivity;->d:I

    :cond_0
    iget v0, p0, Lcom/twitter/android/InviteActivity;->e:I

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/twitter/android/InviteActivity;->d:I

    iget v1, p0, Lcom/twitter/android/InviteActivity;->e:I

    if-ne v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/InviteActivity;->b:Z

    :cond_2
    invoke-direct {p0}, Lcom/twitter/android/InviteActivity;->h()V

    invoke-virtual {p0}, Lcom/twitter/android/InviteActivity;->V()V

    return-void
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 7

    const/4 v6, 0x0

    const/4 v3, 0x1

    if-ne p2, v3, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/InviteActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    const/4 v2, -0x1

    if-ne p3, v2, :cond_1

    invoke-direct {p0}, Lcom/twitter/android/InviteActivity;->f()V

    invoke-virtual {p0}, Lcom/twitter/android/InviteActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/twitter/android/InviteActivity;->a:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v5}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ":invite_contacts:pre_select_confirm_dialog:continue:click"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v2, v0, v1, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/InviteActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/twitter/android/InviteActivity;->a:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v5}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ":invite_contacts:pre_select_confirm_dialog:cancel:click"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v2, v0, v1, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 6

    const v5, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/InviteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-nez p1, :cond_1

    invoke-static {v1, v4}, Lcom/twitter/android/InviteFragment;->a(Landroid/content/Intent;Z)Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v0, "empty_desc"

    const v3, 0x7f0f0150    # com.twitter.android.R.string.empty_invites

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "flow_controller"

    iget-object v3, p0, Lcom/twitter/android/InviteActivity;->a:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v0, "select_all_title"

    const v3, 0x7f0f050c    # com.twitter.android.R.string.unmatched_contacts_format

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v3, "select_all_subtitle"

    iget-object v0, p0, Lcom/twitter/android/InviteActivity;->a:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v0}, Lcom/twitter/android/FollowFlowController;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0f010a    # com.twitter.android.R.string.digits_invite_friends_header

    :goto_0
    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v0, Lcom/twitter/android/InviteFragment;

    invoke-direct {v0}, Lcom/twitter/android/InviteFragment;-><init>()V

    invoke-virtual {v0, v2}, Lcom/twitter/android/InviteFragment;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {v0, p0}, Lcom/twitter/android/InviteFragment;->a(Lcom/twitter/android/aag;)V

    const-string/jumbo v2, "load_contacts"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/InviteActivity;->b:Z

    invoke-virtual {p0}, Lcom/twitter/android/InviteActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1, v5, v0}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    move-object v1, v0

    :goto_1
    iput-object v1, p0, Lcom/twitter/android/InviteActivity;->c:Lcom/twitter/android/InviteFragment;

    iget-object v0, p0, Lcom/twitter/android/InviteActivity;->a:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v0}, Lcom/twitter/android/FollowFlowController;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/twitter/android/InviteActivity;->k()V

    invoke-virtual {v1, p0}, Lcom/twitter/android/InviteFragment;->a(Lcom/twitter/android/ir;)V

    const v0, 0x7f09011b    # com.twitter.android.R.id.skip

    invoke-virtual {p0, v0}, Lcom/twitter/android/InviteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09011d    # com.twitter.android.R.id.next

    invoke-virtual {p0, v0}, Lcom/twitter/android/InviteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09011c    # com.twitter.android.R.id.digits_cta

    invoke-virtual {p0, v0}, Lcom/twitter/android/InviteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/twitter/android/InviteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f01f6    # com.twitter.android.R.string.invite_n

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/twitter/android/InviteFragment;->e()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/twitter/android/InviteActivity;->b()V

    :goto_2
    invoke-direct {p0}, Lcom/twitter/android/InviteActivity;->h()V

    return-void

    :cond_0
    const v0, 0x7f0f0392    # com.twitter.android.R.string.scanned_contacts_subtitle

    goto/16 :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/InviteActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/InviteFragment;

    invoke-virtual {v0, p0}, Lcom/twitter/android/InviteFragment;->a(Lcom/twitter/android/aag;)V

    const-string/jumbo v1, "loading"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/InviteActivity;->b:Z

    const-string/jumbo v1, "page_count"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/InviteActivity;->d:I

    const-string/jumbo v1, "page_total"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/InviteActivity;->e:I

    move-object v1, v0

    goto/16 :goto_1

    :cond_2
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/twitter/android/InviteFragment;->a(Lcom/twitter/android/ir;)V

    goto :goto_2
.end method

.method protected a(Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 5

    const v4, 0x7f090328    # com.twitter.android.R.id.menu_finish

    const v3, 0x7f090327    # com.twitter.android.R.id.menu_next

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lcom/twitter/internal/android/widget/ToolBar;)Z

    iget-object v0, p0, Lcom/twitter/android/InviteActivity;->a:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v0}, Lcom/twitter/android/FollowFlowController;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/InviteActivity;->b:Z

    if-eqz v0, :cond_1

    invoke-virtual {p1, v3}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    invoke-virtual {v0, v2}, Lhn;->b(Z)Lhn;

    invoke-virtual {p1, v4}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    invoke-virtual {v0, v2}, Lhn;->b(Z)Lhn;

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/InviteActivity;->a:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v0}, Lcom/twitter/android/FollowFlowController;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1, v4}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    invoke-virtual {v0, v1}, Lhn;->b(Z)Lhn;

    goto :goto_0

    :cond_2
    invoke-virtual {p1, v3}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    invoke-virtual {v0, v1}, Lhn;->b(Z)Lhn;

    goto :goto_0
.end method

.method protected a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z

    iget-object v0, p0, Lcom/twitter/android/InviteActivity;->a:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v0}, Lcom/twitter/android/FollowFlowController;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f11001b    # com.twitter.android.R.menu.onboarding

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public a(Lhn;)Z
    .locals 9

    const/4 v8, 0x0

    const/4 v0, 0x1

    invoke-virtual {p1}, Lhn;->a()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/InviteActivity;->a:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v2}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f090327    # com.twitter.android.R.id.menu_next

    if-eq v1, v3, :cond_0

    const v3, 0x7f090328    # com.twitter.android.R.id.menu_finish

    if-ne v1, v3, :cond_3

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/InviteActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/InviteActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    iget-object v5, p0, Lcom/twitter/android/InviteActivity;->c:Lcom/twitter/android/InviteFragment;

    invoke-virtual {v5}, Lcom/twitter/android/InviteFragment;->r()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v5

    const v6, 0x7f0f01f8    # com.twitter.android.R.string.invites_title

    invoke-virtual {v5, v6}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v5

    const v6, 0x7f0f01f5    # com.twitter.android.R.string.invite_all_confirm_message

    invoke-virtual {v5, v6}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v5

    const v6, 0x7f0f00cf    # com.twitter.android.R.string.cont

    invoke-virtual {v5, v6}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v5

    const v6, 0x7f0f0089    # com.twitter.android.R.string.cancel

    invoke-virtual {v5, v6}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v5

    invoke-virtual {p0}, Lcom/twitter/android/InviteActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    new-array v5, v0, [Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ":invite_contacts:pre_select_confirm_dialog::impression"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v1, v3, v4, v5}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :goto_0
    iget-object v5, p0, Lcom/twitter/android/InviteActivity;->a:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v5}, Lcom/twitter/android/FollowFlowController;->e()Z

    move-result v5

    if-eqz v5, :cond_2

    new-array v5, v0, [Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, ":invite_contacts::done:click"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v8

    invoke-virtual {v1, v3, v4, v5}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :goto_1
    return v0

    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/InviteActivity;->f()V

    goto :goto_0

    :cond_2
    new-array v5, v0, [Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, ":invite_contacts::next:click"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v8

    invoke-virtual {v1, v3, v4, v5}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhn;)Z

    move-result v0

    goto :goto_1
.end method

.method public b()V
    .locals 8

    const/16 v7, 0x8

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/twitter/android/InviteActivity;->c:Lcom/twitter/android/InviteFragment;

    invoke-virtual {v0}, Lcom/twitter/android/InviteFragment;->e()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const v0, 0x7f09011c    # com.twitter.android.R.id.digits_cta

    invoke-virtual {p0, v0}, Lcom/twitter/android/InviteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f09011d    # com.twitter.android.R.id.next

    invoke-virtual {p0, v1}, Lcom/twitter/android/InviteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    const v2, 0x7f09011b    # com.twitter.android.R.id.skip

    invoke-virtual {p0, v2}, Lcom/twitter/android/InviteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    if-lez v3, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/InviteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f01f6    # com.twitter.android.R.string.invite_n

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {v2, v6}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {v2, v7}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 6

    invoke-virtual {p0}, Lcom/twitter/android/InviteActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/InviteActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/android/InviteActivity;->a:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v5}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "invite_contacts::back_button:click"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onBackPressed()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    const/4 v3, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f09011c    # com.twitter.android.R.id.digits_cta

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/InviteActivity;->c:Lcom/twitter/android/InviteFragment;

    invoke-virtual {v0}, Lcom/twitter/android/InviteFragment;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v5}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f01f8    # com.twitter.android.R.string.invites_title

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f01f5    # com.twitter.android.R.string.invite_all_confirm_message

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f00cf    # com.twitter.android.R.string.cont

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0089    # com.twitter.android.R.string.cancel

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/InviteActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    invoke-virtual {p0}, Lcom/twitter/android/InviteActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/InviteActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v5, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/twitter/android/InviteActivity;->a:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v5}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ":invite_contacts:pre_select_confirm_dialog::impression"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/InviteActivity;->f()V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f09011b    # com.twitter.android.R.id.skip

    if-ne v0, v1, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/InviteActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/InviteActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/InviteActivity;->a:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v4}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    const-string/jumbo v4, "invite_contacts:::skip"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/twitter/android/InviteActivity;->g()V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f09011d    # com.twitter.android.R.id.next

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/InviteActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/InviteActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/InviteActivity;->a:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v4}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    const-string/jumbo v4, "invite_contacts:::next"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected onPause()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/InviteActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/twitter/android/FollowFlowController;->e(Landroid/app/Activity;)V

    :goto_0
    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onPause()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/InviteActivity;->a:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v0, p0}, Lcom/twitter/android/FollowFlowController;->d(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 0

    invoke-static {p0}, Lcom/twitter/android/FollowFlowController;->e(Landroid/app/Activity;)V

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onResume()V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "flow_controller"

    iget-object v1, p0, Lcom/twitter/android/InviteActivity;->a:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v0, "loading"

    iget-boolean v1, p0, Lcom/twitter/android/InviteActivity;->b:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "page_count"

    iget v1, p0, Lcom/twitter/android/InviteActivity;->d:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "page_total"

    iget v1, p0, Lcom/twitter/android/InviteActivity;->e:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method
