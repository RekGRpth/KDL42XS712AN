.class public Lcom/twitter/android/TweetActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/android/AttachMediaListener;
.implements Lcom/twitter/android/widget/ab;
.implements Lcom/twitter/android/widget/ce;
.implements Lcom/twitter/android/xl;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Lcom/twitter/library/service/a;

.field private C:Lcom/twitter/library/telephony/a;

.field private E:Lcom/twitter/android/PhotoSelectHelper;

.field private F:Landroid/view/View;

.field private G:Lcom/twitter/android/widget/GalleryGridFragment;

.field private H:Landroid/widget/RelativeLayout;

.field private I:Landroid/widget/ImageView;

.field private J:Landroid/widget/ImageView;

.field private K:Lcom/twitter/android/PostStorage;

.field private L:Z

.field private M:Z

.field private N:Z

.field private O:J

.field private P:I

.field private Q:I

.field private R:Lcom/twitter/library/scribe/ScribeItem;

.field private S:J

.field private T:Z

.field private final U:Z

.field private V:Ljava/lang/String;

.field private W:I

.field private X:Lcom/twitter/android/wo;

.field a:Lcom/twitter/library/client/Session;

.field b:Lcom/twitter/library/provider/Tweet;

.field c:Lcom/twitter/android/TweetFragment;

.field d:Lcom/twitter/android/TweetBoxFragment;

.field e:Landroid/widget/TextView;

.field f:Landroid/widget/Button;

.field g:I

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/TextView;

.field private n:Z

.field private o:Z

.field private p:Landroid/view/View;

.field private q:I

.field private r:Z

.field private s:Landroid/net/Uri;

.field private t:Z

.field private u:Z

.field private v:Lcom/twitter/library/scribe/ScribeAssociation;

.field private w:Landroid/net/Uri;

.field private x:Z

.field private y:Ljava/lang/String;

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    invoke-static {}, Lcom/twitter/android/util/af;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/TweetActivity;->U:Z

    return-void
.end method

.method private A()Lcom/twitter/library/api/TweetEntities;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/PostStorage$MediaItem;

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetActivity;->b(Lcom/twitter/android/PostStorage$MediaItem;)Lcom/twitter/library/api/TweetEntities;

    move-result-object v0

    return-object v0
.end method

.method private B()Lcom/twitter/android/widget/GalleryGridFragment;
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b002d    # com.twitter.android.R.color.composer_drawer_bg

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    const/4 v2, 0x0

    const v3, 0x7f0c004b    # com.twitter.android.R.dimen.composer_divot_height

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v1, v2, v0}, Lcom/twitter/android/widget/GalleryGridFragment;->a(III)Lcom/twitter/android/widget/GalleryGridFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f090288    # com.twitter.android.R.id.gallery_grid_fragment

    const-string/jumbo v3, "gallery"

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    return-object v0
.end method

.method private P()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->J:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->J:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->H:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->H:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->I:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->I:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_2
    return-void
.end method

.method private Q()V
    .locals 3

    const v0, 0x7f0900df    # com.twitter.android.R.id.root_layout

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/wd;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/wd;-><init>(Lcom/twitter/android/TweetActivity;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-void
.end method

.method private a(IILcom/twitter/library/provider/NotificationSetting;)Landroid/app/Dialog;
    .locals 8

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->V:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    iget-object v6, p0, Lcom/twitter/android/TweetActivity;->V:Ljava/lang/String;

    iget v7, p0, Lcom/twitter/android/TweetActivity;->W:I

    move v0, p1

    move v1, p2

    move-object v2, p3

    move-object v3, p0

    invoke-static/range {v0 .. v7}, Lgr;->a(IILcom/twitter/library/provider/NotificationSetting;Landroid/content/Context;JLjava/lang/String;I)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/TweetActivity;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/TweetActivity;->s:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/TweetActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->i:Landroid/widget/TextView;

    return-object v0
.end method

.method private a(JLjava/util/List;Ljava/util/Set;)V
    .locals 9

    new-instance v0, Ljc;

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    move-object v1, p0

    move-wide v3, p1

    move-object v7, p3

    move-object v8, p4

    invoke-direct/range {v0 .. v8}, Ljc;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJLjava/util/List;Ljava/util/Set;)V

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/TweetActivity;->a(Lcom/twitter/library/service/b;II)Z

    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    sub-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c00c6    # com.twitter.android.R.dimen.threshold_keyboard_visible

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    if-le v0, v3, :cond_4

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/TweetActivity;->r:Z

    iget-boolean v0, p0, Lcom/twitter/android/TweetActivity;->r:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->F:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->F:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v2}, Lcom/twitter/android/TweetActivity;->d(Z)V

    :cond_0
    :goto_1
    iput-boolean v1, p0, Lcom/twitter/android/TweetActivity;->z:Z

    iget-boolean v0, p0, Lcom/twitter/android/TweetActivity;->L:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->K:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage;->d()Lcom/twitter/android/PostStorage$MediaItem;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->E:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/PhotoSelectHelper;->a(Lcom/twitter/android/PostStorage$MediaItem;Z)V

    :cond_1
    iput-boolean v2, p0, Lcom/twitter/android/TweetActivity;->L:Z

    :cond_2
    const v0, 0x7f0901ea    # com.twitter.android.R.id.persistent_reply

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_6

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    :goto_2
    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/android/TweetFragment;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v1}, Lcom/twitter/android/TweetFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2, v2, v2, v0}, Landroid/view/View;->setPadding(IIII)V

    :cond_3
    return-void

    :cond_4
    move v0, v2

    goto :goto_0

    :cond_5
    iget-boolean v0, p0, Lcom/twitter/android/TweetActivity;->M:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/twitter/android/TweetActivity;->d(Z)V

    iput-boolean v2, p0, Lcom/twitter/android/TweetActivity;->M:Z

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_2
.end method

.method private a(Landroid/widget/PopupWindow;)V
    .locals 6

    const/4 v5, 0x1

    invoke-virtual {p1}, Landroid/widget/PopupWindow;->dismiss()V

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0, v5}, Lcom/twitter/android/TweetBoxFragment;->a(Z)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "tweet"

    aput-object v4, v2, v3

    const-string/jumbo v3, "composition"

    aput-object v3, v2, v5

    const/4 v3, 0x2

    const-string/jumbo v4, "hint"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const/4 v4, 0x0

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "dismiss"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/TweetActivity;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/TweetActivity;->a(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/TweetActivity;Landroid/widget/PopupWindow;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/TweetActivity;->a(Landroid/widget/PopupWindow;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/TweetActivity;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/TweetActivity;->a(Z)V

    return-void
.end method

.method private a(Lcom/twitter/android/TweetFragment;)V
    .locals 6

    const v5, 0x7f090116    # com.twitter.android.R.id.convo_placeholder_count

    const v4, 0x7f090115    # com.twitter.android.R.id.convo_placeholder_tweet_text

    const v0, 0x7f090297    # com.twitter.android.R.id.convo_reply_placeholder

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/wf;

    invoke-direct {v2, p0}, Lcom/twitter/android/wf;-><init>(Lcom/twitter/android/TweetActivity;)V

    invoke-virtual {p0, v5}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->i:Landroid/widget/TextView;

    invoke-virtual {p0, v4}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->h:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {}, Lkl;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    iget-object v1, v0, Lcom/twitter/android/widget/TweetDetailView;->e:Landroid/view/ViewGroup;

    iget-object v0, p1, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    iget-object v3, v0, Lcom/twitter/android/widget/TweetDetailView;->d:Landroid/view/ViewGroup;

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->k:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->m:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->j:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->l:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v3, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 5

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "tweet:composition:cancel_reply_sheet"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "click"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method private a(Z)V
    .locals 4

    const/16 v3, 0x8

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lcom/twitter/android/TweetActivity;->c(Z)V

    const v1, 0x7f0900f6    # com.twitter.android.R.id.in_reply_to_box

    invoke-virtual {p0, v1}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f09010e    # com.twitter.android.R.id.photo_row

    invoke-virtual {p0, v2}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz p1, :cond_1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-direct {p0, v0}, Lcom/twitter/android/TweetActivity;->b(Z)V

    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->p()V

    return-void

    :cond_1
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/TweetActivity;I)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/TweetActivity;->o:Z

    or-int/2addr v0, p1

    int-to-byte v0, v0

    iput-boolean v0, p0, Lcom/twitter/android/TweetActivity;->o:Z

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/TweetActivity;Lcom/twitter/library/service/b;II)Z
    .locals 1

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/TweetActivity;->a(Lcom/twitter/library/service/b;II)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/TweetActivity;Ljava/lang/String;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/TweetActivity;->e(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private ac()V
    .locals 3

    new-instance v0, Liz;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    invoke-direct {v0, p0, v1}, Liz;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v1, v1, Lcom/twitter/library/provider/Tweet;->n:J

    invoke-virtual {v0, v1, v2}, Liz;->a(J)Ljb;

    move-result-object v0

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/TweetActivity;->a(Lcom/twitter/library/service/b;II)Z

    return-void
.end method

.method private ad()V
    .locals 3

    new-instance v0, Lja;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    invoke-direct {v0, p0, v1}, Lja;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v1, v1, Lcom/twitter/library/provider/Tweet;->n:J

    invoke-virtual {v0, v1, v2}, Lja;->a(J)Ljb;

    move-result-object v0

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/TweetActivity;->a(Lcom/twitter/library/service/b;II)Z

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/TweetActivity;I)I
    .locals 0

    iput p1, p0, Lcom/twitter/android/TweetActivity;->W:I

    return p1
.end method

.method static synthetic b(Lcom/twitter/android/TweetActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->k:Landroid/widget/TextView;

    return-object v0
.end method

.method private b(Lcom/twitter/android/PostStorage$MediaItem;)Lcom/twitter/library/api/TweetEntities;
    .locals 3

    if-eqz p1, :cond_1

    new-instance v0, Lcom/twitter/library/api/TweetEntities;

    invoke-direct {v0}, Lcom/twitter/library/api/TweetEntities;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, v0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/twitter/android/PostStorage$MediaItem;->b:Landroid/net/Uri;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/twitter/android/PostStorage$MediaItem;->b()Lcom/twitter/library/api/MediaEntity;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Z)V
    .locals 2

    const v0, 0x7f090297    # com.twitter.android.R.id.convo_reply_placeholder

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/TweetActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/TweetActivity;->M:Z

    return p1
.end method

.method static synthetic c(Lcom/twitter/android/TweetActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->m:Landroid/widget/TextView;

    return-object v0
.end method

.method private c(Landroid/os/Bundle;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->E:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0, p1}, Lcom/twitter/android/PhotoSelectHelper;->a(Landroid/os/Bundle;)V

    const-string/jumbo v0, "data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/PostStorage;

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->K:Lcom/twitter/android/PostStorage;

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->K:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage;->d()Lcom/twitter/android/PostStorage$MediaItem;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/twitter/android/PostStorage$MediaItem;->b:Landroid/net/Uri;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/twitter/android/TweetActivity;->z:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->E:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v1, v0, v4}, Lcom/twitter/android/PhotoSelectHelper;->a(Lcom/twitter/android/PostStorage$MediaItem;Z)V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v1, "gallery"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/GalleryGridFragment;

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->G:Lcom/twitter/android/widget/GalleryGridFragment;

    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030036    # com.twitter.android.R.layout.composer_gallery_album_btn

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/wk;

    invoke-direct {v2, p0}, Lcom/twitter/android/wk;-><init>(Lcom/twitter/android/TweetActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f030037    # com.twitter.android.R.layout.composer_gallery_camera_btn

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v2, Lcom/twitter/android/wl;

    invoke-direct {v2, p0}, Lcom/twitter/android/wl;-><init>(Lcom/twitter/android/TweetActivity;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->G:Lcom/twitter/android/widget/GalleryGridFragment;

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/view/View;

    aput-object v1, v3, v4

    aput-object v0, v3, v5

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/GalleryGridFragment;->a([Landroid/view/View;)V

    return-void

    :cond_1
    iput-boolean v5, p0, Lcom/twitter/android/TweetActivity;->L:Z

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->B()Lcom/twitter/android/widget/GalleryGridFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->G:Lcom/twitter/android/widget/GalleryGridFragment;

    new-instance v0, Lcom/twitter/android/PostStorage;

    invoke-direct {v0}, Lcom/twitter/android/PostStorage;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->K:Lcom/twitter/android/PostStorage;

    goto :goto_1
.end method

.method static synthetic c(Lcom/twitter/android/TweetActivity;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/TweetActivity;->d(Z)V

    return-void
.end method

.method private c(Lcom/twitter/internal/android/widget/ToolBar;)V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    invoke-static {v1, v0, v0}, Lcom/twitter/library/util/s;->a(Lcom/twitter/library/provider/Tweet;II)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/twitter/library/util/u;->a(Ljava/util/List;J)Z

    move-result v0

    :cond_0
    const v1, 0x7f090329    # com.twitter.android.R.id.remove_tag

    invoke-virtual {p1, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v1

    invoke-virtual {v1, v0}, Lhn;->b(Z)Lhn;

    return-void
.end method

.method private c(Z)V
    .locals 2

    const v0, 0x7f0901ea    # com.twitter.android.R.id.persistent_reply

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz p1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lcom/twitter/android/TweetBoxFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetBoxFragment;->c(Z)V

    :goto_0
    iput-boolean p1, p0, Lcom/twitter/android/TweetActivity;->N:Z

    return-void

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/twitter/android/TweetActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->w()V

    return-void
.end method

.method private d(Z)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->F:Landroid/view/View;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->F:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method static synthetic d(Lcom/twitter/android/TweetActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/TweetActivity;->u:Z

    return p1
.end method

.method static synthetic e(Lcom/twitter/android/TweetActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/TweetActivity;->u:Z

    return v0
.end method

.method static synthetic f(Lcom/twitter/android/TweetActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->V:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/TweetActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/TweetActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/TweetActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/TweetActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/android/TweetActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic l(Lcom/twitter/android/TweetActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->p()V

    return-void
.end method

.method static synthetic m(Lcom/twitter/android/TweetActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic n(Lcom/twitter/android/TweetActivity;)Lcom/twitter/android/PostStorage;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->K:Lcom/twitter/android/PostStorage;

    return-object v0
.end method

.method private n()V
    .locals 9

    const v8, 0x7f090294    # com.twitter.android.R.id.notification_reason_icon

    const v7, 0x7f090295    # com.twitter.android.R.id.notification_reason_text

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "notification_reason"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {}, Lgr;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lgr;->a(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/twitter/android/TweetActivity;->g:I

    const-string/jumbo v2, "notification_sender_id"

    const-wide/16 v3, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    const-string/jumbo v4, "notification_recipient"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/TweetActivity;->V:Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/TweetActivity;->V:Ljava/lang/String;

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/TweetActivity;->V:Ljava/lang/String;

    :cond_0
    new-instance v4, Lcom/twitter/android/wn;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/twitter/android/wn;-><init>(Lcom/twitter/android/TweetActivity;Lcom/twitter/android/vx;)V

    new-array v5, v6, [Ljava/lang/Void;

    invoke-virtual {v4, v5}, Lcom/twitter/android/wn;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    new-instance v4, Lcom/twitter/android/we;

    invoke-direct {v4, p0, v2, v3, v0}, Lcom/twitter/android/we;-><init>(Lcom/twitter/android/TweetActivity;JLandroid/content/Intent;)V

    const v0, 0x7f090292    # com.twitter.android.R.id.notification_landing_bar

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f090293    # com.twitter.android.R.id.notification_setting_button

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v7}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v0, p0, Lcom/twitter/android/TweetActivity;->g:I

    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, v7}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, v7}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v8}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f02011c    # com.twitter.android.R.drawable.ic_activity_rt_default

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, v7}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v8}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f020114    # com.twitter.android.R.drawable.ic_activity_fave_default

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic o(Lcom/twitter/android/TweetActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method private o()V
    .locals 2

    invoke-static {}, Lkn;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f030042    # com.twitter.android.R.layout.convo_persistent_reply_box

    move v1, v0

    :goto_0
    const v0, 0x7f090298    # com.twitter.android.R.id.persistent_reply_stub

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    invoke-static {}, Lkn;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->q()V

    const v0, 0x7f0900f6    # com.twitter.android.R.id.in_reply_to_box

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/wg;

    invoke-direct {v1, p0}, Lcom/twitter/android/wg;-><init>(Lcom/twitter/android/TweetActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    invoke-static {}, Lkn;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f090296    # com.twitter.android.R.id.new_replies_banner

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->p:Landroid/view/View;

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->p:Landroid/view/View;

    new-instance v1, Lcom/twitter/android/wh;

    invoke-direct {v1, p0}, Lcom/twitter/android/wh;-><init>(Lcom/twitter/android/TweetActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    return-void

    :cond_2
    const v0, 0x7f0300df    # com.twitter.android.R.layout.persistent_reply_box

    move v1, v0

    goto :goto_0
.end method

.method static synthetic p(Lcom/twitter/android/TweetActivity;)Lcom/twitter/android/PhotoSelectHelper;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->E:Lcom/twitter/android/PhotoSelectHelper;

    return-object v0
.end method

.method private p()V
    .locals 2

    iget v0, p0, Lcom/twitter/android/TweetActivity;->q:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->p:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/TweetActivity;->q:I

    :cond_0
    return-void
.end method

.method private q()V
    .locals 3

    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->Q()V

    const v0, 0x7f0901ea    # com.twitter.android.R.id.persistent_reply

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    const v0, 0x7f090219    # com.twitter.android.R.id.divot_gallery

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0200d0    # com.twitter.android.R.drawable.divot_white

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const v0, 0x7f090111    # com.twitter.android.R.id.photo_preview

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->J:Landroid/widget/ImageView;

    const v0, 0x7f090110    # com.twitter.android.R.id.photo_container

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->H:Landroid/widget/RelativeLayout;

    const v0, 0x7f090112    # com.twitter.android.R.id.photo_dismiss

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    new-instance v1, Lcom/twitter/android/wi;

    invoke-direct {v1, p0}, Lcom/twitter/android/wi;-><init>(Lcom/twitter/android/TweetActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09010f    # com.twitter.android.R.id.photo_compose

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->I:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->I:Landroid/widget/ImageView;

    new-instance v1, Lcom/twitter/android/wj;

    invoke-direct {v1, p0}, Lcom/twitter/android/wj;-><init>(Lcom/twitter/android/TweetActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/twitter/android/PhotoSelectHelper;

    const-string/jumbo v1, "reply_composition"

    sget-object v2, Lcom/twitter/android/PhotoSelectHelper$MediaType;->d:Ljava/util/EnumSet;

    invoke-direct {v0, p0, p0, v1, v2}, Lcom/twitter/android/PhotoSelectHelper;-><init>(Landroid/app/Activity;Lcom/twitter/android/AttachMediaListener;Ljava/lang/String;Ljava/util/EnumSet;)V

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->E:Lcom/twitter/android/PhotoSelectHelper;

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->E:Lcom/twitter/android/PhotoSelectHelper;

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/PhotoSelectHelper;->a(Lcom/twitter/library/client/Session;)V

    return-void
.end method

.method static synthetic q(Lcom/twitter/android/TweetActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/TweetActivity;->n:Z

    return v0
.end method

.method private r()V
    .locals 7

    const v6, 0x7f0f02fc    # com.twitter.android.R.string.persistent_reply_hint

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lcom/twitter/android/TweetBoxFragment;

    const v1, 0x7f0f0313    # com.twitter.android.R.string.post_button_reply

    invoke-virtual {p0, v1}, Lcom/twitter/android/TweetActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetBoxFragment;->b(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    if-eqz v0, :cond_0

    invoke-static {}, Lkn;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lcom/twitter/android/TweetBoxFragment;

    const v1, 0x7f0f056f    # com.twitter.android.R.string.write_reply

    invoke-virtual {p0, v1}, Lcom/twitter/android/TweetActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetBoxFragment;->a(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lcom/twitter/android/TweetBoxFragment;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetBoxFragment;->a(Landroid/os/Parcelable;)V

    invoke-static {}, Lkn;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->h:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v3}, Lcom/twitter/library/provider/Tweet;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v1, v6, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v3}, Lcom/twitter/library/provider/Tweet;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v1, v6, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetBoxFragment;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-static {}, Lkl;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->h:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v3}, Lcom/twitter/library/provider/Tweet;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v1, v6, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->j:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v3}, Lcom/twitter/library/provider/Tweet;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v1, v6, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v3}, Lcom/twitter/library/provider/Tweet;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v1, v6, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method static synthetic r(Lcom/twitter/android/TweetActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->t()V

    return-void
.end method

.method static synthetic s(Lcom/twitter/android/TweetActivity;)Lcom/twitter/library/api/TweetEntities;
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->A()Lcom/twitter/library/api/TweetEntities;

    move-result-object v0

    return-object v0
.end method

.method private s()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0, v2}, Lcom/twitter/android/TweetBoxFragment;->c(Z)V

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lcom/twitter/android/TweetBoxFragment;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetBoxFragment;->a(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->d()V

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->a()V

    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->r()V

    invoke-static {}, Lkn;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v2}, Lcom/twitter/android/TweetActivity;->a(Z)V

    invoke-direct {p0, v2}, Lcom/twitter/android/TweetActivity;->d(Z)V

    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->P()V

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->h()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lkl;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, v2}, Lcom/twitter/android/TweetActivity;->c(Z)V

    goto :goto_0

    :cond_2
    invoke-static {}, Lkl;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v2}, Lcom/twitter/android/TweetActivity;->c(Z)V

    invoke-direct {p0, v2}, Lcom/twitter/android/TweetActivity;->b(Z)V

    iput-boolean v2, p0, Lcom/twitter/android/TweetActivity;->o:Z

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->v()V

    goto :goto_0
.end method

.method private t()V
    .locals 10

    const v5, 0x7f0900f6    # com.twitter.android.R.id.in_reply_to_box

    const/4 v9, 0x2

    const/4 v6, -0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    new-instance v1, Lcom/twitter/library/client/f;

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "conversation"

    invoke-direct {v1, p0, v0, v2}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "has_shown_hint"

    invoke-virtual {v1, v0, v7}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f030043    # com.twitter.android.R.layout.convo_popup_layout

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f090114    # com.twitter.android.R.id.convo_composer_hint

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f00d3    # com.twitter.android.R.string.convo_reply_hint

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v7, v7}, Landroid/view/View;->measure(II)V

    new-instance v0, Landroid/widget/PopupWindow;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    invoke-direct {v0, v2, v3, v4}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    new-array v3, v9, [I

    invoke-virtual {p0, v5}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v7, v7}, Landroid/view/View;->measure(II)V

    invoke-virtual {v4, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    invoke-virtual {p0, v5}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    rsub-int/lit8 v5, v5, 0x0

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    sub-int v4, v5, v4

    invoke-virtual {v0, v3, v7, v4}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    invoke-virtual {v0, v6, v6}, Landroid/widget/PopupWindow;->update(II)V

    iput-boolean v8, p0, Lcom/twitter/android/TweetActivity;->n:Z

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->d:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v3, v7}, Lcom/twitter/android/TweetBoxFragment;->a(Z)V

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lcom/twitter/android/wm;

    invoke-direct {v4, p0, v0}, Lcom/twitter/android/wm;-><init>(Lcom/twitter/android/TweetActivity;Landroid/widget/PopupWindow;)V

    const-wide/16 v5, 0x2328

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    new-instance v3, Lcom/twitter/android/wa;

    invoke-direct {v3, p0, v0}, Lcom/twitter/android/wa;-><init>(Lcom/twitter/android/TweetActivity;Landroid/widget/PopupWindow;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "tweet"

    aput-object v4, v3, v7

    const-string/jumbo v4, "composition"

    aput-object v4, v3, v8

    const-string/jumbo v4, "hint"

    aput-object v4, v3, v9

    const/4 v4, 0x3

    const/4 v5, 0x0

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string/jumbo v5, "impression"

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    invoke-virtual {v1}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v1, "has_shown_hint"

    invoke-virtual {v0, v1, v8}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;Z)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V

    goto/16 :goto_0
.end method

.method private u()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    iget-boolean v0, p0, Lcom/twitter/android/TweetActivity;->x:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->y()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v0, v0, Lcom/twitter/library/provider/Tweet;->C:J

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/provider/w;->a(JJ)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->s:Landroid/net/Uri;

    iput-object v4, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v5, v4, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "twitter"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :try_start_0
    const-string/jumbo v1, "status_id"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/provider/w;->a(JJ)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->s:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->v()V

    goto :goto_0

    :cond_2
    const-string/jumbo v2, "vnd.android.cursor.item/vnd.twitter.android.statuses"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->s:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v5, v4, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->v()V

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->v()V

    goto :goto_0
.end method

.method private v()V
    .locals 2

    const v0, 0x7f0f04e4    # com.twitter.android.R.string.tweets_get_status_error

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->finish()V

    return-void
.end method

.method private w()V
    .locals 13

    const/4 v5, 0x0

    const/4 v12, 0x1

    const/4 v9, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v11

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->o()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->p()Ljava/util/ArrayList;

    move-result-object v10

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    if-nez v0, :cond_0

    const v0, 0x7f0f04e4    # com.twitter.android.R.string.tweets_get_status_error

    invoke-static {p0, v0, v12}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/android/TweetFragment;

    iget-object v3, v3, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/library/provider/Tweet;

    iget-wide v3, v3, Lcom/twitter/library/provider/Tweet;->u:J

    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->z()Lcom/twitter/library/api/TweetEntities;

    move-result-object v8

    move-object v6, v5

    move-object v7, v5

    invoke-static/range {v0 .. v10}, Lcom/twitter/android/client/cd;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;JLandroid/location/Location;Ljava/lang/String;Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/api/TweetEntities;ZLjava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetActivity;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->u()V

    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v1, v12, [Ljava/lang/String;

    const-string/jumbo v2, "tweet:composition:::send_reply"

    aput-object v2, v1, v9

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v11, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0, v9}, Lcom/twitter/android/TweetBoxFragment;->c(Z)V

    iput-boolean v12, p0, Lcom/twitter/android/TweetActivity;->u:Z

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->K:Lcom/twitter/android/PostStorage;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->K:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage;->h()V

    new-instance v0, Lcom/twitter/android/PostStorage;

    invoke-direct {v0}, Lcom/twitter/android/PostStorage;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->K:Lcom/twitter/android/PostStorage;

    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->s()V

    goto :goto_0
.end method

.method private x()V
    .locals 7

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v3, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const-string/jumbo v5, "welcome:compose:::impression"

    aput-object v5, v4, v6

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    const v3, 0x7f090299    # com.twitter.android.R.id.preview_tweet_actions_layout

    invoke-virtual {p0, v3}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    const v4, 0x7f090222    # com.twitter.android.R.id.preview_tweet_button

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    new-instance v5, Lcom/twitter/android/wb;

    invoke-direct {v5, p0, v0, v1, v2}, Lcom/twitter/android/wb;-><init>(Lcom/twitter/android/TweetActivity;Lcom/twitter/android/client/c;J)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v4, 0x7f090221    # com.twitter.android.R.id.preview_cancel_button

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    new-instance v4, Lcom/twitter/android/wc;

    invoke-direct {v4, p0, v0, v1, v2}, Lcom/twitter/android/wc;-><init>(Lcom/twitter/android/TweetActivity;Lcom/twitter/android/client/c;J)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    invoke-direct {p0, v6}, Lcom/twitter/android/TweetActivity;->b(Z)V

    const v0, 0x7f0901ea    # com.twitter.android.R.id.persistent_reply

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private y()V
    .locals 11

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/android/TweetFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetFragment;->h(Z)V

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->w:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/android/TweetFragment;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->w:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetFragment;->b(Landroid/net/Uri;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/android/TweetFragment;

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "holy_tweet_instructions"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetFragment;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/android/TweetFragment;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->y:Ljava/lang/String;

    iget v4, p0, Lcom/twitter/android/TweetActivity;->P:I

    iget v5, p0, Lcom/twitter/android/TweetActivity;->Q:I

    iget-wide v6, p0, Lcom/twitter/android/TweetActivity;->O:J

    iget-object v8, p0, Lcom/twitter/android/TweetActivity;->R:Lcom/twitter/library/scribe/ScribeItem;

    iget-wide v9, p0, Lcom/twitter/android/TweetActivity;->S:J

    invoke-virtual/range {v0 .. v10}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/client/Session;Ljava/lang/String;IIJLcom/twitter/library/scribe/ScribeItem;J)V

    return-void
.end method

.method private z()Lcom/twitter/library/api/TweetEntities;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->K:Lcom/twitter/android/PostStorage;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->K:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage;->d()Lcom/twitter/android/PostStorage$MediaItem;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetActivity;->b(Lcom/twitter/android/PostStorage$MediaItem;)Lcom/twitter/library/api/TweetEntities;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const v1, 0x7f03015b    # com.twitter.android.R.layout.tweet

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->c(Z)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->a(I)V

    return-object v0
.end method

.method public a(IILcom/twitter/library/service/b;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v3, 0x1

    iget-boolean v0, p0, Lcom/twitter/android/TweetActivity;->T:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p3}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v5, v1, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_0

    :cond_1
    const v0, 0x7f0f04e4    # com.twitter.android.R.string.tweets_get_status_error

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->finish()V

    goto :goto_0

    :pswitch_1
    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0f001c    # com.twitter.android.R.string.add_tweet_to_collection_success

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    const v0, 0x7f0f001b    # com.twitter.android.R.string.add_tweet_to_collection_error

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :pswitch_2
    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f0f034f    # com.twitter.android.R.string.remove_tweet_from_collection_success

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->finish()V

    goto :goto_0

    :cond_3
    const v0, 0x7f0f034d    # com.twitter.android.R.string.remove_tweet_from_collection_error

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :pswitch_3
    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    const v0, 0x7f0f026e    # com.twitter.android.R.string.media_tag_delete_success

    check-cast p3, Ljc;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v1, v1, Lcom/twitter/library/provider/Tweet;->u:J

    invoke-virtual {p3}, Ljc;->e()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v1}, Lcom/twitter/library/provider/Tweet;->o()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/util/s;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p3}, Ljc;->f()Ljava/util/Set;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/twitter/library/util/u;->a(Ljava/util/List;Ljava/util/Set;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v2, v1}, Lcom/twitter/android/TweetFragment;->a(Ljava/util/List;)V

    :cond_4
    :goto_1
    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_5
    const v0, 0x7f0f026d    # com.twitter.android.R.string.media_tag_delete_error

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v1

    const v2, 0x7f090329    # com.twitter.android.R.id.remove_tag

    invoke-virtual {v1, v2}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v1

    invoke-virtual {v1, v3}, Lhn;->b(Z)Lhn;

    goto :goto_1

    :pswitch_4
    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v0, v0, Lcom/twitter/library/service/e;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "muted_username"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/android/util/af;->b(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->finish()V

    goto/16 :goto_0

    :cond_6
    invoke-static {p0}, Lcom/twitter/android/util/af;->a(Landroid/content/Context;)V

    goto/16 :goto_0

    :pswitch_5
    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v0, v0, Lcom/twitter/library/service/e;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "muted_username"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/android/util/af;->c(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget v1, v1, Lcom/twitter/library/provider/Tweet;->ac:I

    const/16 v2, 0x2000

    invoke-static {v1, v2}, Lcom/twitter/library/provider/ay;->b(II)I

    move-result v1

    iput v1, v0, Lcom/twitter/library/provider/Tweet;->ac:I

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->V()V

    goto/16 :goto_0

    :cond_7
    invoke-static {p0}, Lcom/twitter/android/util/af;->b(Landroid/content/Context;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public a(IZ)V
    .locals 9

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-lez p1, :cond_0

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0, v5, v6}, Lcom/twitter/android/TweetFragment;->a(ZZ)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v2, v5, [Ljava/lang/String;

    const-string/jumbo v3, "tweet::::show_new_tweets"

    aput-object v3, v2, v6

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->v:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v7, v2, v3, v7}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/twitter/library/scribe/ScribeLog;->c(I)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->e(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/twitter/android/TweetActivity;->r:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/android/TweetActivity;->q:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/twitter/android/TweetActivity;->q:I

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0008    # com.twitter.android.R.plurals.new_replies

    iget v2, p0, Lcom/twitter/android/TweetActivity;->q:I

    new-array v3, v5, [Ljava/lang/Object;

    iget v4, p0, Lcom/twitter/android/TweetActivity;->q:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const v0, 0x7f0901f4    # com.twitter.android.R.id.new_replies_text

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->p:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->p:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->p:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->p:Landroid/view/View;

    const v1, 0x7f04001a    # com.twitter.android.R.anim.slide_up

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    const-string/jumbo v0, "impression"

    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v3, v5, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "tweet:::new_replies:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->v:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v7, v2, v3, v7}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/twitter/library/scribe/ScribeLog;->c(I)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget v2, p0, Lcom/twitter/android/TweetActivity;->q:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->e(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto/16 :goto_0

    :cond_2
    const-string/jumbo v0, "update"

    goto :goto_1
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 10

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v0, -0x1

    const/4 v9, 0x0

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-ne p3, v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->d:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v2}, Lcom/twitter/android/TweetBoxFragment;->i()Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, 0x0

    iget-object v5, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v5, v5, Lcom/twitter/library/provider/Tweet;->o:J

    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->z()Lcom/twitter/library/api/TweetEntities;

    move-result-object v7

    iget-object v8, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v8, v8, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual/range {v0 .. v8}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;JJLcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/PromotedContent;)V

    invoke-virtual {p0, v9}, Lcom/twitter/android/TweetActivity;->setResult(I)V

    const-string/jumbo v0, "save_draft"

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetActivity;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->finish()V

    goto :goto_0

    :cond_1
    const/4 v0, -0x3

    if-ne p3, v0, :cond_0

    invoke-virtual {p0, v9}, Lcom/twitter/android/TweetActivity;->setResult(I)V

    const-string/jumbo v0, "dont_save"

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetActivity;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->finish()V

    goto :goto_0

    :pswitch_1
    if-ne p3, v0, :cond_0

    new-instance v0, Ljr;

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->A:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v1}, Lcom/twitter/library/provider/Tweet;->b()J

    move-result-wide v4

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ljr;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;J)V

    invoke-virtual {p0, v0, v7, v9}, Lcom/twitter/android/TweetActivity;->a(Lcom/twitter/library/service/b;II)Z

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/TweetActivity;->v:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v4}, Lcom/twitter/library/scribe/ScribeAssociation;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v9

    iget-object v4, p0, Lcom/twitter/android/TweetActivity;->v:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v4}, Lcom/twitter/library/scribe/ScribeAssociation;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    const-string/jumbo v4, "tweet"

    aput-object v4, v3, v7

    const/4 v4, 0x3

    const/4 v5, 0x0

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string/jumbo v5, "remove_from_timeline"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_2
    if-ne p3, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    const v1, 0x7f090329    # com.twitter.android.R.id.remove_tag

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    invoke-virtual {v0, v9}, Lhn;->b(Z)Lhn;

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v0, v0, Lcom/twitter/library/provider/Tweet;->u:J

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v2}, Lcom/twitter/library/provider/Tweet;->o()Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/library/util/s;->b(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/twitter/android/TweetActivity;->a(JLjava/util/List;Ljava/util/Set;)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "tweet::tweet:remove_my_media_tag:click"

    aput-object v3, v2, v9

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto/16 :goto_0

    :pswitch_3
    if-ne p3, v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "tweet::tweet::mute_user"

    aput-object v4, v3, v9

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->ac()V

    goto/16 :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "tweet::tweet:mute_dialog:cancel"

    aput-object v4, v3, v9

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(Landroid/net/Uri;Lcom/twitter/android/AttachMediaListener$MediaAttachProgress;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetActivity;->d(Z)V

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->g()V

    return-void
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 11

    const-wide/16 v6, -0x1

    const/4 v5, -0x1

    const/4 v10, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    iput-boolean v2, p0, Lcom/twitter/android/TweetActivity;->T:Z

    const-string/jumbo v0, "tweet:complete"

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/client/c;->a()Lcom/twitter/library/metrics/h;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/library/metrics/o;->a(Ljava/lang/String;Lcom/twitter/library/metrics/h;)Lcom/twitter/library/metrics/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/metrics/o;->i()V

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->L()Lcom/twitter/android/client/bn;

    move-result-object v0

    const-string/jumbo v1, "tweet"

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/bn;->a(Ljava/lang/String;)Lcom/twitter/android/client/bn;

    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->o()V

    const-string/jumbo v0, "mode"

    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v2, :cond_7

    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/TweetActivity;->x:Z

    const-string/jumbo v0, "reason"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->y:Ljava/lang/String;

    const v0, 0x7f0900f4    # com.twitter.android.R.id.count

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->e:Landroid/widget/TextView;

    const v0, 0x7f090113    # com.twitter.android.R.id.tweet_button

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->f:Landroid/widget/Button;

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->f:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string/jumbo v0, "activity_row_id"

    invoke-virtual {v4, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/TweetActivity;->O:J

    const-string/jumbo v0, "scribe_item"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/scribe/ScribeItem;

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->R:Lcom/twitter/library/scribe/ScribeItem;

    const-string/jumbo v0, "magic_rec_id"

    invoke-virtual {v4, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/TweetActivity;->S:J

    const-string/jumbo v0, "social_context_type"

    invoke-virtual {v4, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/TweetActivity;->P:I

    const-string/jumbo v0, "social_context_user_count"

    invoke-virtual {v4, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/TweetActivity;->Q:I

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    const v0, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    invoke-virtual {v5, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/TweetFragment;

    invoke-static {}, Lkn;->b()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lkn;->f()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lkl;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {v0, p0}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/android/xl;)V

    :cond_1
    invoke-direct {p0, v0}, Lcom/twitter/android/TweetActivity;->a(Lcom/twitter/android/TweetFragment;)V

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/android/TweetFragment;

    const-string/jumbo v1, "media_uri"

    invoke-virtual {v4, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v1, "media_uri"

    invoke-virtual {v4, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    iput-object v1, p0, Lcom/twitter/android/TweetActivity;->w:Landroid/net/Uri;

    :cond_2
    const-string/jumbo v1, "timeline_id"

    invoke-virtual {v4, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/TweetActivity;->A:Ljava/lang/String;

    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->n()V

    const-string/jumbo v1, "tweet_box"

    invoke-virtual {v5, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/TweetBoxFragment;

    if-nez v1, :cond_3

    const v1, 0x7f03015c    # com.twitter.android.R.layout.tweet_box

    invoke-static {v1, v3}, Lcom/twitter/android/TweetBoxFragment;->a(IZ)Lcom/twitter/android/TweetBoxFragment;

    move-result-object v1

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v5

    const v6, 0x7f0900f7    # com.twitter.android.R.id.tweet_box

    const-string/jumbo v7, "tweet_box"

    invoke-virtual {v5, v6, v1, v7}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_3
    new-instance v5, Lcom/twitter/android/vx;

    invoke-direct {v5, p0}, Lcom/twitter/android/vx;-><init>(Lcom/twitter/android/TweetActivity;)V

    invoke-virtual {v1, v5}, Lcom/twitter/android/TweetBoxFragment;->a(Lcom/twitter/android/wz;)V

    iput-object v1, p0, Lcom/twitter/android/TweetActivity;->d:Lcom/twitter/android/TweetBoxFragment;

    iget-boolean v1, p0, Lcom/twitter/android/TweetActivity;->x:Z

    if-eqz v1, :cond_8

    const v1, 0x7f0f02db    # com.twitter.android.R.string.onboarding_tweet_title

    invoke-virtual {p0, v1}, Lcom/twitter/android/TweetActivity;->setTitle(I)V

    :goto_1
    const-string/jumbo v1, "association"

    invoke-virtual {v4, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/scribe/ScribeAssociation;

    iput-object v1, p0, Lcom/twitter/android/TweetActivity;->v:Lcom/twitter/library/scribe/ScribeAssociation;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->v:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/scribe/ScribeAssociation;)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v5

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v6

    new-instance v1, Lcom/twitter/android/wo;

    invoke-direct {v1, p0, v10}, Lcom/twitter/android/wo;-><init>(Lcom/twitter/android/TweetActivity;Lcom/twitter/android/vx;)V

    iput-object v1, p0, Lcom/twitter/android/TweetActivity;->X:Lcom/twitter/android/wo;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->X:Lcom/twitter/android/wo;

    invoke-virtual {v5, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/j;)V

    new-instance v1, Lcom/twitter/android/wp;

    invoke-direct {v1, p0, v10}, Lcom/twitter/android/wp;-><init>(Lcom/twitter/android/TweetActivity;Lcom/twitter/android/vx;)V

    iput-object v1, p0, Lcom/twitter/android/TweetActivity;->B:Lcom/twitter/library/service/a;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->B:Lcom/twitter/library/service/a;

    invoke-virtual {p0, v1}, Lcom/twitter/android/TweetActivity;->a(Lcom/twitter/library/service/a;)V

    if-eqz p1, :cond_9

    const-string/jumbo v1, "t"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/provider/Tweet;

    iput-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    const-string/jumbo v1, "reply_box_visible"

    invoke-virtual {p1, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/TweetActivity;->N:Z

    const-string/jumbo v1, "sticky"

    invoke-virtual {p1, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/TweetActivity;->o:Z

    :goto_2
    invoke-static {}, Lkl;->c()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-boolean v1, p0, Lcom/twitter/android/TweetActivity;->o:Z

    if-nez v1, :cond_a

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->v()V

    :cond_4
    :goto_3
    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->r()V

    invoke-static {}, Lkn;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0, p1}, Lcom/twitter/android/TweetActivity;->c(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->G:Lcom/twitter/android/widget/GalleryGridFragment;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/GalleryGridFragment;->a(Lcom/twitter/android/widget/ab;)V

    const v0, 0x7f090287    # com.twitter.android.R.id.gallery_grid_drawer

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->F:Landroid/view/View;

    :cond_5
    invoke-static {}, Lcom/twitter/library/telephony/a;->a()Lcom/twitter/library/telephony/a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->C:Lcom/twitter/library/telephony/a;

    iget-boolean v0, p0, Lcom/twitter/android/TweetActivity;->x:Z

    if-eqz v0, :cond_6

    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->x()V

    :cond_6
    return-void

    :cond_7
    move v0, v3

    goto/16 :goto_0

    :cond_8
    const v1, 0x7f0f04d7    # com.twitter.android.R.string.tweet_title

    invoke-virtual {p0, v1}, Lcom/twitter/android/TweetActivity;->setTitle(I)V

    goto/16 :goto_1

    :cond_9
    const-string/jumbo v1, "tw"

    invoke-virtual {v4, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/provider/Tweet;

    iput-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    const-string/jumbo v1, "reply"

    invoke-virtual {v4, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/TweetActivity;->N:Z

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v7, "tweet:"

    aput-object v7, v1, v3

    iget-object v7, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    invoke-static {v7}, Lcom/twitter/library/provider/Tweet;->b(Lcom/twitter/library/provider/Tweet;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v1, v2

    const/4 v7, 0x2

    const-string/jumbo v8, ":impression"

    aput-object v8, v1, v7

    invoke-static {v1}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v7, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {v6}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    invoke-direct {v7, v8, v9}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v6, v2, [Ljava/lang/String;

    aput-object v1, v6, v3

    invoke-virtual {v7, v6}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v6, "ref_event"

    invoke-virtual {v4, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->c([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->v:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v10, v2, v3, v10}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->v:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->R:Lcom/twitter/library/scribe/ScribeItem;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto/16 :goto_2

    :cond_a
    invoke-static {}, Lkl;->e()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->y()V

    goto/16 :goto_3
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 12

    const/4 v1, 0x1

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v11

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    if-nez v0, :cond_4

    if-eqz p2, :cond_2

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/twitter/library/provider/Tweet;

    invoke-direct {v0, p2}, Lcom/twitter/library/provider/Tweet;-><init>(Landroid/database/Cursor;)V

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/android/TweetFragment;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->y:Ljava/lang/String;

    iget v4, p0, Lcom/twitter/android/TweetActivity;->P:I

    iget v5, p0, Lcom/twitter/android/TweetActivity;->Q:I

    iget-wide v6, p0, Lcom/twitter/android/TweetActivity;->O:J

    iget-object v8, p0, Lcom/twitter/android/TweetActivity;->R:Lcom/twitter/library/scribe/ScribeItem;

    iget-wide v9, p0, Lcom/twitter/android/TweetActivity;->S:J

    invoke-virtual/range {v0 .. v10}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/client/Session;Ljava/lang/String;IIJLcom/twitter/library/scribe/ScribeItem;J)V

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v0, v0, Lcom/twitter/library/provider/Tweet;->n:J

    invoke-virtual {v11, v0, v1}, Lcom/twitter/android/client/c;->a(J)Ljava/lang/String;

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->V()V

    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->r()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-boolean v0, p0, Lcom/twitter/android/TweetActivity;->t:Z

    if-nez v0, :cond_3

    iput-boolean v1, p0, Lcom/twitter/android/TweetActivity;->t:Z

    new-instance v0, Ljs;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, p0, v1, v2, v3}, Ljs;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->s:Landroid/net/Uri;

    invoke-static {v1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljs;->a(J)Ljs;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljs;->c(I)Lcom/twitter/library/service/b;

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->c(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    invoke-virtual {p0, v0, v4, v4}, Lcom/twitter/android/TweetActivity;->a(Lcom/twitter/library/service/b;II)Z

    goto :goto_0

    :cond_3
    const v0, 0x7f0f04e4    # com.twitter.android.R.string.tweets_get_status_error

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->finish()V

    goto :goto_0

    :cond_4
    if-eqz p2, :cond_1

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0, p2}, Lcom/twitter/library/provider/Tweet;->a(Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/android/TweetFragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->z()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;Lcom/twitter/android/PostStorage$MediaItem;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/android/PostStorage$MediaItem;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->K:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0, p1}, Lcom/twitter/android/PostStorage;->a(Lcom/twitter/android/PostStorage$MediaItem;)V

    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->P()V

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->h()V

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->H:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->H:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->J:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->J:Landroid/widget/ImageView;

    iget-object v1, p1, Lcom/twitter/android/PostStorage$MediaItem;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->I:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->I:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_2
    return-void
.end method

.method public a(Lcom/twitter/library/provider/Tweet;)V
    .locals 2

    const/4 v1, 0x1

    invoke-static {}, Lkn;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/twitter/android/TweetActivity;->a(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lkl;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0, p1}, Lcom/twitter/android/TweetBoxFragment;->a(Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->y()V

    invoke-direct {p0, v1}, Lcom/twitter/android/TweetActivity;->c(Z)V

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetFragment;->e(Z)V

    goto :goto_0
.end method

.method public a(Landroid/net/Uri;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public a(Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    const v0, 0x7f090301    # com.twitter.android.R.id.menu_report_tweet

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v3, v3, Lcom/twitter/library/provider/Tweet;->n:J

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-eqz v3, :cond_3

    move v4, v1

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0, v4}, Lhn;->b(Z)Lhn;

    :cond_0
    const v0, 0x7f090325    # com.twitter.android.R.id.menu_mute

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v5

    if-eqz v5, :cond_1

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->v()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->F()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Lcom/twitter/library/provider/Tweet;->a(J)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget v3, v3, Lcom/twitter/library/provider/Tweet;->ac:I

    invoke-static {v3}, Lcom/twitter/library/provider/ay;->e(I)Z

    move-result v3

    if-eqz v3, :cond_5

    move v3, v1

    :goto_2
    if-eqz v4, :cond_8

    if-nez v0, :cond_8

    if-nez v3, :cond_8

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget v0, v0, Lcom/twitter/library/provider/Tweet;->ac:I

    invoke-static {v0}, Lcom/twitter/library/provider/ay;->d(I)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    invoke-static {p0, v0}, Lcom/twitter/android/util/af;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lhn;->a(Ljava/lang/CharSequence;)Lhn;

    :goto_4
    invoke-virtual {v5, v1}, Lhn;->b(Z)Lhn;

    :cond_1
    :goto_5
    invoke-direct {p0, p1}, Lcom/twitter/android/TweetActivity;->c(Lcom/twitter/internal/android/widget/ToolBar;)V

    iget-boolean v0, p0, Lcom/twitter/android/TweetActivity;->x:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-boolean v0, v0, Lcom/twitter/library/provider/Tweet;->m:Z

    if-nez v0, :cond_9

    invoke-static {}, Lgj;->e()Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v1

    :goto_6
    const v3, 0x7f090306    # com.twitter.android.R.id.menu_add_to_timeline

    invoke-virtual {p1, v3}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v3

    invoke-virtual {v3, v0}, Lhn;->b(Z)Lhn;

    const v3, 0x7f090307    # com.twitter.android.R.id.menu_remove_from_timeline

    invoke-virtual {p1, v3}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v3

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->A:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    :goto_7
    invoke-virtual {v3, v1}, Lhn;->b(Z)Lhn;

    :cond_2
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lcom/twitter/internal/android/widget/ToolBar;)Z

    move-result v0

    return v0

    :cond_3
    move v4, v2

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    move v3, v2

    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_3

    :cond_7
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    invoke-static {p0, v0}, Lcom/twitter/android/util/af;->b(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lhn;->a(Ljava/lang/CharSequence;)Lhn;

    goto :goto_4

    :cond_8
    invoke-virtual {v5, v2}, Lhn;->b(Z)Lhn;

    goto :goto_5

    :cond_9
    move v0, v2

    goto :goto_6

    :cond_a
    move v1, v2

    goto :goto_7
.end method

.method protected a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z

    iget-boolean v0, p0, Lcom/twitter/android/TweetActivity;->x:Z

    if-nez v0, :cond_0

    invoke-static {}, Lkn;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const v0, 0x7f090309    # com.twitter.android.R.id.toolbar_search

    invoke-virtual {p2, v0}, Lcom/twitter/internal/android/widget/ToolBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    iget-boolean v0, p0, Lcom/twitter/android/TweetActivity;->x:Z

    if-nez v0, :cond_2

    const v0, 0x7f110006    # com.twitter.android.R.menu.curate_timeline

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    :cond_2
    const v0, 0x7f11001d    # com.twitter.android.R.menu.remove_tag

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    iget-boolean v0, p0, Lcom/twitter/android/TweetActivity;->U:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/twitter/android/TweetActivity;->x:Z

    if-nez v0, :cond_3

    const v0, 0x7f110019    # com.twitter.android.R.menu.mute

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->N()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/twitter/android/TweetActivity;->x:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/twitter/android/TweetActivity;->U:Z

    if-eqz v0, :cond_5

    const v0, 0x7f110001    # com.twitter.android.R.menu.block_or_report

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    :cond_4
    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_5
    const v0, 0x7f110025    # com.twitter.android.R.menu.tweet

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    goto :goto_0
.end method

.method public a(Lhn;)Z
    .locals 13

    const/4 v5, 0x0

    const v12, 0x7f0f04da    # com.twitter.android.R.string.tweet_url

    const/4 v10, 0x2

    const/4 v6, 0x0

    const/4 v9, 0x1

    invoke-virtual {p1}, Lhn;->a()I

    move-result v0

    const v1, 0x7f090301    # com.twitter.android.R.id.menu_report_tweet

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v0, v0, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-nez v0, :cond_1

    move v0, v6

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-boolean v1, v1, Lcom/twitter/library/provider/Tweet;->r:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v7, v1, Lcom/twitter/library/provider/Tweet;->n:J

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v2, v1, Lcom/twitter/library/provider/Tweet;->p:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v3, v1, Lcom/twitter/library/provider/Tweet;->u:J

    new-array v1, v10, [Ljava/lang/Object;

    iget-object v10, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v10, v10, Lcom/twitter/library/provider/Tweet;->p:Ljava/lang/String;

    aput-object v10, v1, v6

    iget-object v6, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v10, v6, Lcom/twitter/library/provider/Tweet;->o:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v1, v9

    invoke-virtual {p0, v12, v1}, Lcom/twitter/android/TweetActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-wide v6, v7

    :goto_1
    new-instance v8, Landroid/content/Intent;

    const-class v10, Lcom/twitter/android/ReportTweetActivity;

    invoke-direct {v8, p0, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v10, "spammer_id"

    invoke-virtual {v8, v10, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v6

    const-string/jumbo v7, "spammer_username"

    invoke-virtual {v6, v7, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v6, "status_id"

    invoke-virtual {v2, v6, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "status_url"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "association"

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->v:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "pc_impression_id"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "pc_earned"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "friendship"

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget v2, v2, Lcom/twitter/library/provider/Tweet;->ac:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x65

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/TweetActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    :goto_2
    return v9

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v0, v0, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v0}, Lcom/twitter/library/api/PromotedContent;->b()Z

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v1, v1, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    iget-object v5, v1, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    goto/16 :goto_0

    :cond_2
    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v7, v1, Lcom/twitter/library/provider/Tweet;->q:J

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v2, v1, Lcom/twitter/library/provider/Tweet;->e:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v3, v1, Lcom/twitter/library/provider/Tweet;->u:J

    new-array v1, v10, [Ljava/lang/Object;

    iget-object v10, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v10, v10, Lcom/twitter/library/provider/Tweet;->e:Ljava/lang/String;

    aput-object v10, v1, v6

    iget-object v6, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v10, v6, Lcom/twitter/library/provider/Tweet;->u:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v1, v9

    invoke-virtual {p0, v12, v1}, Lcom/twitter/android/TweetActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-wide v6, v7

    goto :goto_1

    :cond_3
    const v1, 0x7f090045    # com.twitter.android.R.id.home

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->e()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {}, Lkn;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->K:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage;->g()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_4
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->h()V

    goto :goto_2

    :cond_5
    const v1, 0x7f090329    # com.twitter.android.R.id.remove_tag

    if-ne v0, v1, :cond_7

    const/4 v0, 0x3

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f034c    # com.twitter.android.R.string.remove_self_media_tag_question

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f02d5    # com.twitter.android.R.string.ok

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0089    # com.twitter.android.R.string.cancel

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/android/widget/ce;)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    :cond_6
    :goto_3
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhn;)Z

    move-result v9

    goto/16 :goto_2

    :cond_7
    const v1, 0x7f090306    # com.twitter.android.R.id.menu_add_to_timeline

    if-ne v0, v1, :cond_8

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/AddToCollectionActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "android.intent.action.PICK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x67

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/TweetActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_2

    :cond_8
    const v1, 0x7f090307    # com.twitter.android.R.id.menu_remove_from_timeline

    if-ne v0, v1, :cond_9

    invoke-static {v10}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f034b    # com.twitter.android.R.string.remove_from_collection

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f034e    # com.twitter.android.R.string.remove_tweet_from_collection_question

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0571    # com.twitter.android.R.string.yes

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f029d    # com.twitter.android.R.string.no

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/android/widget/ce;)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_3

    :cond_9
    const v1, 0x7f090325    # com.twitter.android.R.id.menu_mute

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget v0, v0, Lcom/twitter/library/provider/Tweet;->ac:I

    invoke-static {v0}, Lcom/twitter/library/provider/ay;->d(I)Z

    move-result v0

    if-eqz v0, :cond_a

    move v0, v9

    :goto_4
    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v1, v1, Lcom/twitter/library/provider/Tweet;->p:Ljava/lang/String;

    if-eqz v0, :cond_b

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v9, [Ljava/lang/String;

    const-string/jumbo v4, "tweet::tweet:unmute_dialog:open"

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v9, [Ljava/lang/String;

    const-string/jumbo v4, "tweet::tweet:unmute_dialog:unmute_user"

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->ad()V

    goto/16 :goto_2

    :cond_a
    move v0, v6

    goto :goto_4

    :cond_b
    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v3, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v3, v9, [Ljava/lang/String;

    const-string/jumbo v4, "tweet::tweet:mute_dialog:open"

    aput-object v4, v3, v6

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget v2, v0, Lcom/twitter/library/provider/Tweet;->ac:I

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/util/af;->a(Landroid/content/Context;Ljava/lang/String;IILandroid/support/v4/app/FragmentManager;Landroid/support/v4/app/Fragment;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v9, [Ljava/lang/String;

    const-string/jumbo v4, "tweet::tweet:mute_dialog:mute_user"

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->ac()V

    goto/16 :goto_2
.end method

.method protected a_(Landroid/net/Uri;)V
    .locals 6

    iget v0, p0, Lcom/twitter/android/TweetActivity;->g:I

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a_(Landroid/net/Uri;)V

    return-void

    :pswitch_0
    const-string/jumbo v0, "tweet:notification_landing:tweet:home:click"

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "tweet:notification_landing:favorite:home:click"

    goto :goto_0

    :pswitch_2
    const-string/jumbo v0, "tweet:notification_landing:mention:home:click"

    goto :goto_0

    :pswitch_3
    const-string/jumbo v0, "tweet:notification_landing:retweet:home:click"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public b(Landroid/net/Uri;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->E:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/PhotoSelectHelper$AddMediaMode;->c:Lcom/twitter/android/PhotoSelectHelper$AddMediaMode;

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/twitter/android/PhotoSelectHelper;->a(Landroid/net/Uri;JLcom/twitter/android/PhotoSelectHelper$AddMediaMode;)V

    return-void
.end method

.method public c(Landroid/net/Uri;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->E:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/PhotoSelectHelper$AddMediaMode;->a:Lcom/twitter/android/PhotoSelectHelper$AddMediaMode;

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/twitter/android/PhotoSelectHelper;->a(Landroid/net/Uri;JLcom/twitter/android/PhotoSelectHelper$AddMediaMode;)V

    return-void
.end method

.method public e(Z)V
    .locals 0

    return-void
.end method

.method public f()V
    .locals 0

    return-void
.end method

.method public g()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->p()V

    return-void
.end method

.method public h()V
    .locals 2

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f031b    # com.twitter.android.R.string.post_title_tweet

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0318    # com.twitter.android.R.string.post_quit_question

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0385    # com.twitter.android.R.string.save

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0111    # com.twitter.android.R.string.discard

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->h(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0089    # com.twitter.android.R.string.cancel

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/android/widget/ce;)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    return-void
.end method

.method public k()Landroid/text/Editable;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->j()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public l()[I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->k()[I

    move-result-object v0

    return-object v0
.end method

.method public m()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->K:Lcom/twitter/android/PostStorage;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->K:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage;->e()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->E:Lcom/twitter/android/PhotoSelectHelper;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->E:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0}, Lcom/twitter/android/PhotoSelectHelper;->a()V

    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->s()V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 9

    const-wide/16 v3, 0x0

    const/4 v8, 0x0

    const/4 v2, -0x1

    const/16 v1, 0xa

    const/4 v7, 0x1

    const v0, 0xffff

    and-int/2addr v0, p1

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/client/BaseFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    return-void

    :sswitch_0
    if-ne v2, p2, :cond_0

    invoke-virtual {p0, v1}, Lcom/twitter/android/TweetActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->finish()V

    goto :goto_0

    :sswitch_1
    if-ne v2, p2, :cond_0

    const-string/jumbo v0, "result_timeline_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v0, Ljd;

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v1}, Lcom/twitter/library/provider/Tweet;->b()J

    move-result-wide v4

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v1}, Lcom/twitter/library/provider/Tweet;->m()Lcom/twitter/library/api/PromotedContent;

    move-result-object v6

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Ljd;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;)V

    invoke-virtual {p0, v0, v7, v8}, Lcom/twitter/android/TweetActivity;->a(Lcom/twitter/library/service/b;II)Z

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/TweetActivity;->v:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v4}, Lcom/twitter/library/scribe/ScribeAssociation;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    iget-object v4, p0, Lcom/twitter/android/TweetActivity;->v:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v4}, Lcom/twitter/library/scribe/ScribeAssociation;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    const/4 v4, 0x2

    const-string/jumbo v5, "tweet"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const/4 v5, 0x0

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string/jumbo v5, "add_to_timeline"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0

    :sswitch_2
    if-ne v1, p2, :cond_1

    invoke-virtual {p0, v1}, Lcom/twitter/android/TweetActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->finish()V

    goto :goto_0

    :cond_1
    if-ne v7, p2, :cond_0

    const-string/jumbo v0, "status_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "status_id"

    invoke-virtual {p3, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->c:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/android/TweetFragment;->d(J)V

    goto :goto_0

    :sswitch_3
    if-eqz p3, :cond_0

    const-string/jumbo v0, "deleted"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v0, v0, Lcom/twitter/library/provider/Tweet;->u:J

    const-string/jumbo v2, "deleted"

    invoke-virtual {p3, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->finish()V

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->E:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    move v1, p1

    move v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/PhotoSelectHelper;->a(IILandroid/content/Intent;J)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_3
        0x3 -> :sswitch_2
        0x65 -> :sswitch_0
        0x67 -> :sswitch_1
        0x101 -> :sswitch_4
        0x102 -> :sswitch_4
        0x103 -> :sswitch_4
    .end sparse-switch
.end method

.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->F:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->F:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetActivity;->d(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->h()V

    goto :goto_0

    :cond_1
    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->w()V

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    const/4 v0, 0x3

    const v1, 0x7f0f03f4    # com.twitter.android.R.string.settings_notif_favorites_title

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->c:Lcom/twitter/library/provider/NotificationSetting;

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/TweetActivity;->a(IILcom/twitter/library/provider/NotificationSetting;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x4

    const v1, 0x7f0f03f9    # com.twitter.android.R.string.settings_notif_mentions_title

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->a:Lcom/twitter/library/provider/NotificationSetting;

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/TweetActivity;->a(IILcom/twitter/library/provider/NotificationSetting;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    const v1, 0x7f0f03fe    # com.twitter.android.R.string.settings_notif_retweets_title

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->b:Lcom/twitter/library/provider/NotificationSetting;

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/TweetActivity;->a(IILcom/twitter/library/provider/NotificationSetting;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->s:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    const-string/jumbo v1, "status_groups_retweets_view"

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v3, Lcom/twitter/library/provider/Tweet;->b:[Ljava/lang/String;

    :goto_0
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    iget-object v2, p0, Lcom/twitter/android/TweetActivity;->s:Landroid/net/Uri;

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_0
    sget-object v3, Lcom/twitter/library/provider/Tweet;->a:[Ljava/lang/String;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/TweetActivity;->T:Z

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onDestroy()V

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->K:Lcom/twitter/android/PostStorage;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->K:Lcom/twitter/android/PostStorage;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage;->h()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->E:Lcom/twitter/android/PhotoSelectHelper;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->E:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0}, Lcom/twitter/android/PhotoSelectHelper;->a()V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->B:Lcom/twitter/library/service/a;

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetActivity;->b(Lcom/twitter/library/service/a;)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->X:Lcom/twitter/android/wo;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/j;)V

    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/TweetActivity;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onResume()V

    const-string/jumbo v0, "android_rt_action_1837"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    invoke-static {}, Lkl;->a()V

    iget-boolean v0, p0, Lcom/twitter/android/TweetActivity;->x:Z

    if-nez v0, :cond_0

    invoke-static {}, Lkn;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/TweetActivity;->N:Z

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetActivity;->a(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lkl;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/android/TweetActivity;->N:Z

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetActivity;->c(Z)V

    goto :goto_0

    :cond_2
    invoke-static {}, Lkl;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/TweetActivity;->N:Z

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetActivity;->c(Z)V

    iget-boolean v0, p0, Lcom/twitter/android/TweetActivity;->N:Z

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-direct {p0, v0}, Lcom/twitter/android/TweetActivity;->b(Z)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "t"

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-static {}, Lkn;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "data"

    iget-object v1, p0, Lcom/twitter/android/TweetActivity;->K:Lcom/twitter/android/PostStorage;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v0, "reply_box_visible"

    iget-boolean v1, p0, Lcom/twitter/android/TweetActivity;->N:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->E:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0, p1}, Lcom/twitter/android/PhotoSelectHelper;->b(Landroid/os/Bundle;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lkl;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "reply_box_visible"

    iget-boolean v1, p0, Lcom/twitter/android/TweetActivity;->N:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "sticky"

    iget-boolean v1, p0, Lcom/twitter/android/TweetActivity;->o:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public onStart()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onStart()V

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/TweetActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    invoke-direct {p0}, Lcom/twitter/android/TweetActivity;->u()V

    :cond_0
    invoke-static {}, Lkn;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lcom/twitter/android/TweetBoxFragment;

    new-instance v1, Lcom/twitter/android/vy;

    invoke-direct {v1, p0}, Lcom/twitter/android/vy;-><init>(Lcom/twitter/android/TweetActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetBoxFragment;->a(Landroid/text/TextWatcher;)V

    :cond_1
    invoke-static {}, Lkn;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/TweetActivity;->d:Lcom/twitter/android/TweetBoxFragment;

    new-instance v1, Lcom/twitter/android/vz;

    invoke-direct {v1, p0}, Lcom/twitter/android/vz;-><init>(Lcom/twitter/android/TweetActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetBoxFragment;->a(Landroid/text/TextWatcher;)V

    :cond_2
    return-void
.end method
