.class public Lcom/twitter/android/samsung/model/b;
.super Lcom/twitter/android/samsung/model/WidgetViewModel;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:J

.field private g:J

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:J


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/samsung/model/WidgetViewModel;-><init>()V

    return-void
.end method

.method private a(I)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "clickType"

    sget-object v2, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->i:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    invoke-virtual {v2}, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v1, "appWidgetId"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    return-object v1
.end method

.method public static a(Lorg/json/JSONObject;)Lcom/twitter/android/samsung/model/b;
    .locals 3

    new-instance v0, Lcom/twitter/android/samsung/model/b;

    invoke-direct {v0}, Lcom/twitter/android/samsung/model/b;-><init>()V

    :try_start_0
    const-string/jumbo v1, "name"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/android/samsung/model/b;->a:Ljava/lang/String;

    const-string/jumbo v1, "username"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/android/samsung/model/b;->b:Ljava/lang/String;

    const-string/jumbo v1, "originalStatusName"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/android/samsung/model/b;->c:Ljava/lang/String;

    const-string/jumbo v1, "originalStatusUsername"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/android/samsung/model/b;->d:Ljava/lang/String;

    const-string/jumbo v1, "content"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/android/samsung/model/b;->e:Ljava/lang/String;

    const-string/jumbo v1, "createdAt"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/twitter/android/samsung/model/b;->f:J

    const-string/jumbo v1, "originalStatusId"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/twitter/android/samsung/model/b;->g:J

    const-string/jumbo v1, "imageUrl"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/android/samsung/model/b;->h:Ljava/lang/String;

    const-string/jumbo v1, "profileImageUrl"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/android/samsung/model/b;->i:Ljava/lang/String;

    const-string/jumbo v1, "id"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/twitter/android/samsung/model/b;->j:J
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private b(I)Landroid/content/Intent;
    .locals 4

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    new-instance v1, Lcom/twitter/library/provider/Tweet;

    invoke-direct {v1}, Lcom/twitter/library/provider/Tweet;-><init>()V

    iget-object v2, p0, Lcom/twitter/android/samsung/model/b;->c:Ljava/lang/String;

    iput-object v2, v1, Lcom/twitter/library/provider/Tweet;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/samsung/model/b;->d:Ljava/lang/String;

    iput-object v2, v1, Lcom/twitter/library/provider/Tweet;->p:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/samsung/model/b;->e:Ljava/lang/String;

    iput-object v2, v1, Lcom/twitter/library/provider/Tweet;->d:Ljava/lang/String;

    iget-wide v2, p0, Lcom/twitter/android/samsung/model/b;->f:J

    iput-wide v2, v1, Lcom/twitter/library/provider/Tweet;->h:J

    iget-wide v2, p0, Lcom/twitter/android/samsung/model/b;->g:J

    iput-wide v2, v1, Lcom/twitter/library/provider/Tweet;->o:J

    const-string/jumbo v2, "tweet"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v1, "appWidgetId"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v1, "clickType"

    sget-object v2, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->e:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    invoke-virtual {v2}, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    return-object v1
.end method


# virtual methods
.method public a()I
    .locals 1

    const v0, 0x7f030194    # com.twitter.android.R.layout.widget_single_item

    return v0
.end method

.method public a(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 4

    const/16 v3, 0x8

    const v0, 0x7f0902e8    # com.twitter.android.R.id.twitter_name

    iget-object v1, p0, Lcom/twitter/android/samsung/model/b;->a:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v0, 0x7f0902e9    # com.twitter.android.R.id.twitter_handle

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "@"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/samsung/model/b;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v0, 0x7f09001e    # com.twitter.android.R.id.text

    iget-object v1, p0, Lcom/twitter/android/samsung/model/b;->e:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v0, 0x7f0902f3    # com.twitter.android.R.id.logged_out_share

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v0, 0x7f0902f4    # com.twitter.android.R.id.button_frame_divider

    invoke-virtual {p2, v0, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v0, 0x7f0902f5    # com.twitter.android.R.id.logged_in_button_frame

    invoke-virtual {p2, v0, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    return-void
.end method

.method public a(Landroid/content/Context;Landroid/widget/RemoteViews;I)V
    .locals 2

    const v0, 0x7f0902f3    # com.twitter.android.R.id.logged_out_share

    invoke-direct {p0, p3}, Lcom/twitter/android/samsung/model/b;->b(I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    const v0, 0x7f0902e6    # com.twitter.android.R.id.flipper_item_image

    invoke-direct {p0, p3}, Lcom/twitter/android/samsung/model/b;->a(I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    return-void
.end method

.method public a(Lcom/twitter/library/api/TwitterStatus;)V
    .locals 2

    iget-object v0, p1, Lcom/twitter/library/api/TwitterStatus;->t:Lcom/twitter/library/api/TwitterUser;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/samsung/model/b;->a:Ljava/lang/String;

    iget-object v0, p1, Lcom/twitter/library/api/TwitterStatus;->t:Lcom/twitter/library/api/TwitterUser;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/samsung/model/b;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/twitter/library/api/TwitterStatus;->c()Lcom/twitter/library/api/TwitterStatus;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatus;->n:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/samsung/model/b;->e:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/twitter/library/api/TwitterStatus;->c()Lcom/twitter/library/api/TwitterStatus;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatus;->t:Lcom/twitter/library/api/TwitterUser;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/samsung/model/b;->c:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/twitter/library/api/TwitterStatus;->c()Lcom/twitter/library/api/TwitterStatus;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatus;->t:Lcom/twitter/library/api/TwitterUser;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/samsung/model/b;->d:Ljava/lang/String;

    iget-wide v0, p1, Lcom/twitter/library/api/TwitterStatus;->o:J

    iput-wide v0, p0, Lcom/twitter/android/samsung/model/b;->f:J

    invoke-virtual {p1}, Lcom/twitter/library/api/TwitterStatus;->c()Lcom/twitter/library/api/TwitterStatus;

    move-result-object v0

    iget-wide v0, v0, Lcom/twitter/library/api/TwitterStatus;->a:J

    iput-wide v0, p0, Lcom/twitter/android/samsung/model/b;->g:J

    invoke-virtual {p1}, Lcom/twitter/library/api/TwitterStatus;->c()Lcom/twitter/library/api/TwitterStatus;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatus;->j:Lcom/twitter/library/api/TweetEntities;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/twitter/android/samsung/model/b;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/TweetEntities;->b(I)Lcom/twitter/library/api/MediaEntity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/twitter/library/api/MediaEntity;->mediaUrl:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/samsung/model/b;->h:Ljava/lang/String;

    :cond_0
    invoke-virtual {p1}, Lcom/twitter/library/api/TwitterStatus;->c()Lcom/twitter/library/api/TwitterStatus;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatus;->t:Lcom/twitter/library/api/TwitterUser;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/samsung/model/b;->i:Ljava/lang/String;

    iget-wide v0, p1, Lcom/twitter/library/api/TwitterStatus;->a:J

    iput-wide v0, p0, Lcom/twitter/android/samsung/model/b;->j:J

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/samsung/model/b;->h:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/samsung/model/b;->i:Ljava/lang/String;

    return-object v0
.end method

.method public d()Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;
    .locals 1

    sget-object v0, Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;->e:Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;

    return-object v0
.end method

.method public e()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/android/samsung/model/b;->j:J

    return-wide v0
.end method

.method public f()Lorg/json/JSONObject;
    .locals 4

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string/jumbo v1, "name"

    iget-object v2, p0, Lcom/twitter/android/samsung/model/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "username"

    iget-object v2, p0, Lcom/twitter/android/samsung/model/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "originalStatusName"

    iget-object v2, p0, Lcom/twitter/android/samsung/model/b;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "originalStatusUsername"

    iget-object v2, p0, Lcom/twitter/android/samsung/model/b;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "content"

    iget-object v2, p0, Lcom/twitter/android/samsung/model/b;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "createdAt"

    iget-wide v2, p0, Lcom/twitter/android/samsung/model/b;->f:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string/jumbo v1, "originalStatusId"

    iget-wide v2, p0, Lcom/twitter/android/samsung/model/b;->g:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string/jumbo v1, "imageUrl"

    iget-object v2, p0, Lcom/twitter/android/samsung/model/b;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "profileImageUrl"

    iget-object v2, p0, Lcom/twitter/android/samsung/model/b;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "id"

    iget-wide v2, p0, Lcom/twitter/android/samsung/model/b;->j:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method
