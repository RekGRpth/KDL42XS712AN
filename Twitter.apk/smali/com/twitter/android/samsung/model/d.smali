.class public Lcom/twitter/android/samsung/model/d;
.super Lcom/twitter/android/samsung/model/WidgetViewModel;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static a:J


# instance fields
.field private b:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/twitter/android/samsung/model/d;->a:J

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    invoke-direct {p0}, Lcom/twitter/android/samsung/model/WidgetViewModel;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/android/samsung/model/d;->b:J

    sget-wide v0, Lcom/twitter/android/samsung/model/d;->a:J

    const-wide/16 v2, 0x1

    sub-long v2, v0, v2

    sput-wide v2, Lcom/twitter/android/samsung/model/d;->a:J

    iput-wide v0, p0, Lcom/twitter/android/samsung/model/d;->b:J

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    const v0, 0x7f030198    # com.twitter.android.R.layout.widget_single_item_welcome

    return v0
.end method

.method public a(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 0

    return-void
.end method

.method public a(Landroid/content/Context;Landroid/widget/RemoteViews;I)V
    .locals 2

    const v0, 0x7f0902fe    # com.twitter.android.R.id.widget_sign_in

    invoke-static {p3}, Lcom/twitter/android/samsung/single/p;->a(I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    const v0, 0x7f0902fd    # com.twitter.android.R.id.widget_sign_up

    invoke-static {p3}, Lcom/twitter/android/samsung/single/p;->b(I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    return-void
.end method

.method public d()Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;
    .locals 1

    sget-object v0, Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;->a:Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;

    return-object v0
.end method

.method public e()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/android/samsung/model/d;->b:J

    return-wide v0
.end method
