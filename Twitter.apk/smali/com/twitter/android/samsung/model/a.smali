.class public Lcom/twitter/android/samsung/model/a;
.super Lcom/twitter/android/samsung/model/WidgetViewModel;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private a:Lcom/twitter/library/provider/Tweet;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/samsung/model/WidgetViewModel;-><init>()V

    return-void
.end method

.method private a(Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;I)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "tweet"

    iget-object v2, p0, Lcom/twitter/android/samsung/model/a;->a:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v1, "appWidgetId"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v1, "clickType"

    invoke-virtual {p1}, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    return-object v1
.end method


# virtual methods
.method public a()I
    .locals 1

    const v0, 0x7f030194    # com.twitter.android.R.layout.widget_single_item

    return v0
.end method

.method public a(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 4

    const/4 v3, 0x0

    const v0, 0x7f0902e8    # com.twitter.android.R.id.twitter_name

    iget-object v1, p0, Lcom/twitter/android/samsung/model/a;->a:Lcom/twitter/library/provider/Tweet;

    iget-object v1, v1, Lcom/twitter/library/provider/Tweet;->g:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v0, 0x7f0902e9    # com.twitter.android.R.id.twitter_handle

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "@"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/samsung/model/a;->a:Lcom/twitter/library/provider/Tweet;

    iget-object v2, v2, Lcom/twitter/library/provider/Tweet;->p:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v0, 0x7f09001e    # com.twitter.android.R.id.text

    iget-object v1, p0, Lcom/twitter/android/samsung/model/a;->a:Lcom/twitter/library/provider/Tweet;

    iget-object v1, v1, Lcom/twitter/library/provider/Tweet;->G:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v0, 0x7f0902f3    # com.twitter.android.R.id.logged_out_share

    const/16 v1, 0x8

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v0, 0x7f0902f4    # com.twitter.android.R.id.button_frame_divider

    invoke-virtual {p2, v0, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v0, 0x7f0902f5    # com.twitter.android.R.id.logged_in_button_frame

    invoke-virtual {p2, v0, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    return-void
.end method

.method public a(Landroid/content/Context;Landroid/widget/RemoteViews;I)V
    .locals 6

    const v5, 0x7f0902f8    # com.twitter.android.R.id.favourite_button

    const v4, 0x7f0902f7    # com.twitter.android.R.id.retweet_button

    invoke-static {p1, p3}, Lcom/twitter/android/samsung/single/k;->b(Landroid/content/Context;I)Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const v2, 0x7f0902e6    # com.twitter.android.R.id.flipper_item_image

    sget-object v3, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->a:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    invoke-direct {p0, v3, p3}, Lcom/twitter/android/samsung/model/a;->a(Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;I)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    const v2, 0x7f0902f6    # com.twitter.android.R.id.reply_button

    sget-object v3, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->b:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    invoke-direct {p0, v3, p3}, Lcom/twitter/android/samsung/model/a;->a(Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;I)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    sget-object v2, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->c:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    invoke-direct {p0, v2, p3}, Lcom/twitter/android/samsung/model/a;->a(Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;I)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p2, v4, v2}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    sget-object v2, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->d:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    invoke-direct {p0, v2, p3}, Lcom/twitter/android/samsung/model/a;->a(Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;I)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p2, v5, v2}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    const v2, 0x7f0902f9    # com.twitter.android.R.id.share_button

    sget-object v3, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->e:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    invoke-direct {p0, v3, p3}, Lcom/twitter/android/samsung/model/a;->a(Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;I)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    const v2, 0x7f0902e7    # com.twitter.android.R.id.thumbnail

    sget-object v3, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->f:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    invoke-direct {p0, v3, p3}, Lcom/twitter/android/samsung/model/a;->a(Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;I)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    const v2, 0x7f0902e8    # com.twitter.android.R.id.twitter_name

    sget-object v3, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->f:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    invoke-direct {p0, v3, p3}, Lcom/twitter/android/samsung/model/a;->a(Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;I)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    const v2, 0x7f0902e9    # com.twitter.android.R.id.twitter_handle

    sget-object v3, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->f:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    invoke-direct {p0, v3, p3}, Lcom/twitter/android/samsung/model/a;->a(Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;I)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    const v2, 0x7f09001e    # com.twitter.android.R.id.text

    sget-object v3, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->a:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    invoke-direct {p0, v3, p3}, Lcom/twitter/android/samsung/model/a;->a(Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;I)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    iget-object v2, p0, Lcom/twitter/android/samsung/model/a;->a:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/library/provider/Tweet;->a(J)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "setImageResource"

    const v1, 0x7f0202fa    # com.twitter.android.R.drawable.widget_action_retweet_on

    invoke-virtual {p2, v4, v0, v1}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/samsung/model/a;->a:Lcom/twitter/library/provider/Tweet;

    iget-boolean v0, v0, Lcom/twitter/library/provider/Tweet;->l:Z

    if-eqz v0, :cond_2

    const-string/jumbo v0, "setImageResource"

    const v1, 0x7f0202f6    # com.twitter.android.R.drawable.widget_action_fave_on

    invoke-virtual {p2, v5, v0, v1}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const-string/jumbo v0, "setImageResource"

    const v1, 0x7f0202f9    # com.twitter.android.R.drawable.widget_action_retweet_off

    invoke-virtual {p2, v4, v0, v1}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "setImageResource"

    const v1, 0x7f0202f5    # com.twitter.android.R.drawable.widget_action_fave_off

    invoke-virtual {p2, v5, v0, v1}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    goto :goto_1
.end method

.method public a(Lcom/twitter/library/provider/Tweet;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/samsung/model/a;->a:Lcom/twitter/library/provider/Tweet;

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/samsung/model/a;->a:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->T()Lcom/twitter/library/api/MediaEntity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/twitter/library/api/MediaEntity;->mediaUrl:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/samsung/model/a;->a:Lcom/twitter/library/provider/Tweet;

    iget-object v0, v0, Lcom/twitter/library/provider/Tweet;->k:Ljava/lang/String;

    return-object v0
.end method

.method public d()Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;
    .locals 1

    sget-object v0, Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;->d:Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;

    return-object v0
.end method

.method public e()J
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/samsung/model/a;->a:Lcom/twitter/library/provider/Tweet;

    iget-wide v0, v0, Lcom/twitter/library/provider/Tweet;->u:J

    return-wide v0
.end method
