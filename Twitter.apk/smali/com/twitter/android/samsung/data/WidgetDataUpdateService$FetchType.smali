.class public final enum Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;
.super Ljava/lang/Enum;
.source "Twttr"


# static fields
.field public static final enum a:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;

.field public static final enum b:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;

.field public static final enum c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;

.field private static final synthetic d:[Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;

    const-string/jumbo v1, "PAGE_OLDER"

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;->a:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;

    new-instance v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;

    const-string/jumbo v1, "PAGE_CURRENT"

    invoke-direct {v0, v1, v3}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;->b:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;

    new-instance v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;

    const-string/jumbo v1, "PAGE_NEWER"

    invoke-direct {v0, v1, v4}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;->c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;

    sget-object v1, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;->a:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;->b:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;->c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;->d:[Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;
    .locals 1

    const-class v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;

    return-object v0
.end method

.method public static values()[Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;
    .locals 1

    sget-object v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;->d:[Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;

    invoke-virtual {v0}, [Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;

    return-object v0
.end method
