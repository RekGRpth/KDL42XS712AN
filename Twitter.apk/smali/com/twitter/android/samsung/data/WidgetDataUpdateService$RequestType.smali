.class public final enum Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;
.super Ljava/lang/Enum;
.source "Twttr"


# static fields
.field public static final enum a:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

.field public static final enum b:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

.field public static final enum c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

.field public static final enum d:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

.field private static final synthetic e:[Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    const-string/jumbo v1, "LOGGED_OUT"

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;->a:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    new-instance v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    const-string/jumbo v1, "DISCOVER"

    invoke-direct {v0, v1, v3}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;->b:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    new-instance v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    const-string/jumbo v1, "APPONLYAUTH"

    invoke-direct {v0, v1, v4}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;->c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    new-instance v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    const-string/jumbo v1, "GUESTTOKEN"

    invoke-direct {v0, v1, v5}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;->d:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    sget-object v1, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;->a:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;->b:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;->c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;->d:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;->e:[Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;
    .locals 1

    const-class v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    return-object v0
.end method

.method public static values()[Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;
    .locals 1

    sget-object v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;->e:[Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    invoke-virtual {v0}, [Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    return-object v0
.end method
