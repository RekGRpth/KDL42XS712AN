.class Lcom/twitter/android/samsung/data/f;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field a:Ljava/lang/String;

.field b:I

.field c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/samsung/data/f;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/twitter/android/samsung/data/f;->c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    iput p3, p0, Lcom/twitter/android/samsung/data/f;->b:I

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    instance-of v2, p1, Lcom/twitter/android/samsung/data/f;

    if-eqz v2, :cond_3

    check-cast p1, Lcom/twitter/android/samsung/data/f;

    iget-object v2, p0, Lcom/twitter/android/samsung/data/f;->a:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p1, Lcom/twitter/android/samsung/data/f;->a:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p1, Lcom/twitter/android/samsung/data/f;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/samsung/data/f;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p1, Lcom/twitter/android/samsung/data/f;->c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    iget-object v3, p0, Lcom/twitter/android/samsung/data/f;->c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    if-ne v2, v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p1, Lcom/twitter/android/samsung/data/f;->c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    iget-object v3, p0, Lcom/twitter/android/samsung/data/f;->c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method
