.class public Lcom/twitter/android/samsung/data/j;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static a:Landroid/util/SparseArray;

.field private static b:Landroid/util/SparseArray;

.field private static c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/twitter/android/samsung/data/j;->a:Landroid/util/SparseArray;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/twitter/android/samsung/data/j;->b:Landroid/util/SparseArray;

    const/4 v0, -0x1

    sput v0, Lcom/twitter/android/samsung/data/j;->c:I

    return-void
.end method

.method public static a()Ljava/util/List;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;->c:Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;

    invoke-static {v1}, Lcom/twitter/android/samsung/model/h;->a(Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;)Lcom/twitter/android/samsung/model/WidgetViewModel;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method private static declared-synchronized a(Landroid/content/Context;)Ljava/util/List;
    .locals 6

    const/4 v0, 0x0

    const-class v3, Lcom/twitter/android/samsung/data/j;

    monitor-enter v3

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p0}, Lcom/twitter/android/samsung/data/j;->b(Landroid/content/Context;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    if-nez v4, :cond_0

    :goto_0
    monitor-exit v3

    return-object v0

    :cond_0
    const/4 v2, 0x0

    :goto_1
    :try_start_1
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v2, v5, :cond_1

    invoke-virtual {v4, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    invoke-static {v5}, Lcom/twitter/android/samsung/model/b;->a(Lorg/json/JSONObject;)Lcom/twitter/android/samsung/model/b;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static declared-synchronized a(Landroid/content/Context;I)Ljava/util/List;
    .locals 2

    const-class v1, Lcom/twitter/android/samsung/data/j;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/android/samsung/data/j;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(Landroid/database/Cursor;)Ljava/util/List;
    .locals 5

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x14

    if-ge v1, v0, :cond_0

    sget-object v0, Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;->d:Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;

    invoke-static {v0}, Lcom/twitter/android/samsung/model/h;->a(Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;)Lcom/twitter/android/samsung/model/WidgetViewModel;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/samsung/model/a;

    new-instance v3, Lcom/twitter/library/provider/Tweet;

    invoke-direct {v3, p0}, Lcom/twitter/library/provider/Tweet;-><init>(Landroid/database/Cursor;)V

    invoke-virtual {v3}, Lcom/twitter/library/provider/Tweet;->T()Lcom/twitter/library/api/MediaEntity;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v4, v4, Lcom/twitter/library/api/MediaEntity;->mediaUrl:Ljava/lang/String;

    if-eqz v4, :cond_1

    invoke-virtual {v0, v3}, Lcom/twitter/android/samsung/model/a;->a(Lcom/twitter/library/provider/Tweet;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    move v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private static a(Ljava/util/List;)Ljava/util/List;
    .locals 4

    const/4 v3, 0x2

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    if-ne v0, v3, :cond_0

    sget-object v2, Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;->a:Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;

    invoke-static {v2}, Lcom/twitter/android/samsung/model/h;->a(Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;)Lcom/twitter/android/samsung/model/WidgetViewModel;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    if-le v0, v3, :cond_1

    add-int/lit8 v2, v0, -0x2

    rem-int/lit8 v2, v2, 0x7

    if-nez v2, :cond_1

    sget-object v2, Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;->a:Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;

    invoke-static {v2}, Lcom/twitter/android/samsung/model/h;->a(Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;)Lcom/twitter/android/samsung/model/WidgetViewModel;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-object v1
.end method

.method public static declared-synchronized a(I)V
    .locals 4

    const-class v1, Lcom/twitter/android/samsung/data/j;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/android/samsung/data/j;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->remove(I)V

    sget-object v0, Lcom/twitter/android/samsung/data/j;->a:Landroid/util/SparseArray;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, p0, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;Ljava/util/List;)V
    .locals 6

    const-class v2, Lcom/twitter/android/samsung/data/j;

    monitor-enter v2

    :try_start_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4}, Lorg/json/JSONArray;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterStatus;

    invoke-static {v0}, Lcom/twitter/android/samsung/data/j;->a(Lcom/twitter/library/api/TwitterStatus;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;->e:Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;

    invoke-static {v1}, Lcom/twitter/android/samsung/model/h;->a(Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;)Lcom/twitter/android/samsung/model/WidgetViewModel;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/samsung/model/b;

    invoke-virtual {v1, v0}, Lcom/twitter/android/samsung/model/b;->a(Lcom/twitter/library/api/TwitterStatus;)V

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Lcom/twitter/android/samsung/model/b;->f()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v4, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    :try_start_1
    invoke-static {p0, v4}, Lcom/twitter/android/samsung/data/j;->a(Landroid/content/Context;Lorg/json/JSONArray;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v2

    return-void
.end method

.method private static declared-synchronized a(Landroid/content/Context;Lorg/json/JSONArray;)V
    .locals 5

    const-class v1, Lcom/twitter/android/samsung/data/j;

    monitor-enter v1

    :try_start_0
    const-string/jumbo v0, "loggedOutExperienceStore.txt"

    invoke-virtual {p1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1

    invoke-static {p0, p1}, Lcom/twitter/android/samsung/model/g;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/twitter/library/api/TwitterStatus;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/twitter/library/api/TwitterStatus;->c()Lcom/twitter/library/api/TwitterStatus;

    move-result-object v1

    iget-object v2, v1, Lcom/twitter/library/api/TwitterStatus;->j:Lcom/twitter/library/api/TweetEntities;

    if-eqz v2, :cond_0

    iget-object v1, v1, Lcom/twitter/library/api/TwitterStatus;->j:Lcom/twitter/library/api/TweetEntities;

    invoke-virtual {v1, v0}, Lcom/twitter/library/api/TweetEntities;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static declared-synchronized b(Landroid/content/Context;)Lorg/json/JSONArray;
    .locals 6

    const-class v1, Lcom/twitter/android/samsung/data/j;

    monitor-enter v1

    :try_start_0
    const-string/jumbo v0, "loggedOutExperienceStore.txt"

    new-instance v2, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v3, 0x400

    new-array v3, v3, [B

    :goto_0
    invoke-virtual {v0, v3}, Ljava/io/FileInputStream;->read([B)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_0

    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v3}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    :goto_1
    monitor-exit v1

    return-object v0

    :cond_0
    :try_start_2
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    new-instance v0, Lorg/json/JSONArray;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized b(Landroid/content/Context;I)Z
    .locals 7

    const/4 v0, 0x0

    const/4 v1, 0x1

    const-class v2, Lcom/twitter/android/samsung/data/j;

    monitor-enter v2

    :try_start_0
    sget-object v3, Lcom/twitter/android/samsung/data/j;->b:Landroid/util/SparseArray;

    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    if-eqz v3, :cond_0

    :goto_0
    monitor-exit v2

    return v0

    :cond_0
    :try_start_1
    invoke-static {p0, p1}, Lcom/twitter/android/samsung/single/k;->g(Landroid/content/Context;I)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {p0, p1}, Lcom/twitter/android/samsung/data/j;->f(Landroid/content/Context;I)Landroid/database/Cursor;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_2

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-static {v3}, Lcom/twitter/android/samsung/data/j;->a(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x5

    if-ge v5, v6, :cond_1

    sget v5, Lcom/twitter/android/samsung/data/j;->c:I

    if-eq v5, v4, :cond_1

    invoke-static {p0}, Lcom/twitter/android/samsung/single/p;->c(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1

    sput v4, Lcom/twitter/android/samsung/data/j;->c:I

    sget-object v1, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;->a:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;

    invoke-static {p0, p1, v1}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a(Landroid/content/Context;ILcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    const/4 v0, -0x1

    :try_start_2
    sput v0, Lcom/twitter/android/samsung/data/j;->c:I

    sget-object v0, Lcom/twitter/android/samsung/data/j;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/android/samsung/data/j;->a:Landroid/util/SparseArray;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, p1, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-static {p0}, Lcom/twitter/android/samsung/single/p;->c(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3

    sget-object v1, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;->b:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;

    invoke-static {p0, p1, v1}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a(Landroid/content/Context;ILcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;)V

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/twitter/android/samsung/data/j;->b:Landroid/util/SparseArray;

    invoke-static {p0, p1}, Lcom/twitter/android/samsung/data/j;->c(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, p1, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/android/samsung/data/j;->a:Landroid/util/SparseArray;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, p1, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move v0, v1

    goto :goto_0

    :cond_4
    invoke-static {p0, p1}, Lcom/twitter/android/samsung/data/j;->e(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v3

    sget-object v4, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;->a:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    invoke-static {p0, v4}, Lcom/twitter/android/samsung/single/k;->b(Landroid/content/Context;Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;)Z

    move-result v4

    if-eqz v3, :cond_5

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_5

    sget-object v0, Lcom/twitter/android/samsung/data/j;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/android/samsung/data/j;->a:Landroid/util/SparseArray;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, p1, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move v0, v1

    goto/16 :goto_0

    :cond_5
    if-nez v4, :cond_6

    invoke-static {p0}, Lcom/twitter/android/samsung/single/p;->c(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-static {p0}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a(Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_6
    sget-object v3, Lcom/twitter/android/samsung/data/j;->b:Landroid/util/SparseArray;

    if-nez v4, :cond_7

    invoke-static {p0, p1}, Lcom/twitter/android/samsung/data/j;->c(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v0

    :goto_1
    invoke-virtual {v3, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/android/samsung/data/j;->a:Landroid/util/SparseArray;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, p1, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move v0, v1

    goto/16 :goto_0

    :cond_7
    invoke-static {p0, p1}, Lcom/twitter/android/samsung/data/j;->d(Landroid/content/Context;I)Ljava/util/List;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    goto :goto_1
.end method

.method public static c(Landroid/content/Context;I)Ljava/util/List;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;->c:Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;

    invoke-static {v1}, Lcom/twitter/android/samsung/model/h;->a(Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;)Lcom/twitter/android/samsung/model/WidgetViewModel;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;->b:Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;

    invoke-static {v1}, Lcom/twitter/android/samsung/model/h;->a(Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;)Lcom/twitter/android/samsung/model/WidgetViewModel;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public static d(Landroid/content/Context;I)Ljava/util/List;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;->f:Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;

    invoke-static {v1}, Lcom/twitter/android/samsung/model/h;->a(Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;)Lcom/twitter/android/samsung/model/WidgetViewModel;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method private static declared-synchronized e(Landroid/content/Context;I)Ljava/util/List;
    .locals 3

    const-class v1, Lcom/twitter/android/samsung/data/j;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lcom/twitter/android/samsung/data/j;->a(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Lcom/twitter/android/samsung/single/p;->d(Landroid/content/Context;I)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v0}, Lcom/twitter/android/samsung/data/j;->a(Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :cond_0
    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static f(Landroid/content/Context;I)Landroid/database/Cursor;
    .locals 7

    const/4 v2, 0x0

    invoke-static {p0, p1}, Lcom/twitter/android/samsung/single/k;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/android/samsung/data/j;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Lcom/twitter/android/samsung/single/k;->b(Landroid/content/Context;I)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/provider/ag;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string/jumbo v5, "ownerId"

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v5, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide/32 v5, 0x5265c00

    sub-long/2addr v3, v5

    const-string/jumbo v5, "newer"

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v5, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    :cond_0
    return-object v2
.end method
