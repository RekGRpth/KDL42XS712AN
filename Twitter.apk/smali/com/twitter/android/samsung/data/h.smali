.class public Lcom/twitter/android/samsung/data/h;
.super Landroid/os/AsyncTask;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/lang/String;

.field private c:Lcom/twitter/android/samsung/data/g;

.field private d:J


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/twitter/android/samsung/data/f;JLjava/lang/String;Lcom/twitter/android/samsung/data/g;)V
    .locals 0

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/samsung/data/h;->a:Landroid/content/Context;

    iput-wide p3, p0, Lcom/twitter/android/samsung/data/h;->d:J

    iput-object p5, p0, Lcom/twitter/android/samsung/data/h;->b:Ljava/lang/String;

    iput-object p6, p0, Lcom/twitter/android/samsung/data/h;->c:Lcom/twitter/android/samsung/data/g;

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Long;
    .locals 7

    const/4 v6, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/samsung/data/h;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/samsung/data/h;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/twitter/library/network/aa;->a(Landroid/content/Context;)Lcom/twitter/library/network/aa;

    move-result-object v2

    iget-object v2, v2, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string/jumbo v5, "1.1"

    aput-object v5, v3, v4

    const-string/jumbo v4, "search"

    aput-object v4, v3, v6

    const/4 v4, 0x2

    const-string/jumbo v5, "tweets"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "q"

    invoke-static {v2, v3, v0}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "count"

    const/16 v3, 0x14

    invoke-static {v2, v0, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    const-string/jumbo v0, "include_entities"

    invoke-static {v2, v0, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const/16 v0, 0x3b

    invoke-static {v0, v1}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v0

    new-instance v3, Lcom/twitter/library/network/d;

    iget-object v4, p0, Lcom/twitter/android/samsung/data/h;->a:Landroid/content/Context;

    invoke-direct {v3, v4, v2}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/samsung/data/h;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/twitter/android/samsung/single/k;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Bearer "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "Authorization"

    invoke-virtual {v2, v4, v3}, Lcom/twitter/internal/network/HttpOperation;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/internal/network/HttpOperation;

    iget-object v3, p0, Lcom/twitter/android/samsung/data/h;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/twitter/android/samsung/single/k;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "X-Guest-Token"

    invoke-virtual {v2, v4, v3}, Lcom/twitter/internal/network/HttpOperation;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/internal/network/HttpOperation;

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iget-object v1, p0, Lcom/twitter/android/samsung/data/h;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/twitter/android/samsung/data/j;->a(Landroid/content/Context;Ljava/util/List;)V

    iget-wide v0, p0, Lcom/twitter/android/samsung/data/h;->d:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/al;

    iget v4, v0, Lcom/twitter/library/api/al;->a:I

    const/16 v5, 0x58

    if-ne v4, v5, :cond_2

    invoke-static {v2}, Lcom/twitter/library/network/aa;->a(Lcom/twitter/internal/network/HttpOperation;)Lcom/twitter/library/api/RateLimit;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-wide v4, v0, Lcom/twitter/library/api/RateLimit;->b:J

    iget-object v0, p0, Lcom/twitter/android/samsung/data/h;->c:Lcom/twitter/android/samsung/data/g;

    invoke-interface {v0, v4, v5}, Lcom/twitter/android/samsung/data/g;->a(J)V

    goto :goto_1

    :cond_2
    iget-object v4, p0, Lcom/twitter/android/samsung/data/h;->c:Lcom/twitter/android/samsung/data/g;

    iget v5, v0, Lcom/twitter/library/api/al;->a:I

    iget-object v0, v0, Lcom/twitter/library/api/al;->b:Ljava/lang/String;

    invoke-interface {v4, v5, v0, v1}, Lcom/twitter/android/samsung/data/g;->a(ILjava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method protected a(Ljava/lang/Long;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/samsung/data/h;->c:Lcom/twitter/android/samsung/data/g;

    invoke-interface {v0, p1}, Lcom/twitter/android/samsung/data/g;->a(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/samsung/data/h;->a([Ljava/lang/Void;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/twitter/android/samsung/data/h;->a(Ljava/lang/Long;)V

    return-void
.end method
