.class Lcom/twitter/android/samsung/data/q;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/samsung/data/g;


# instance fields
.field final synthetic a:Lcom/twitter/android/samsung/data/f;

.field final synthetic b:I

.field final synthetic c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService;


# direct methods
.method constructor <init>(Lcom/twitter/android/samsung/data/WidgetDataUpdateService;Lcom/twitter/android/samsung/data/f;I)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/samsung/data/q;->c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService;

    iput-object p2, p0, Lcom/twitter/android/samsung/data/q;->a:Lcom/twitter/android/samsung/data/f;

    iput p3, p0, Lcom/twitter/android/samsung/data/q;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILjava/lang/String;Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/samsung/data/q;->c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService;

    invoke-static {v0}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a(Lcom/twitter/android/samsung/data/WidgetDataUpdateService;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/samsung/data/q;->a:Lcom/twitter/android/samsung/data/f;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/samsung/data/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/android/samsung/data/e;->a(ILjava/lang/String;Ljava/lang/Exception;)V

    iget-object v0, p0, Lcom/twitter/android/samsung/data/q;->c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService;

    invoke-static {v0}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a(Lcom/twitter/android/samsung/data/WidgetDataUpdateService;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/samsung/data/q;->a:Lcom/twitter/android/samsung/data/f;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public a(J)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/samsung/data/q;->c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService;

    sget-object v1, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;->b:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    invoke-static {v0, v1, p1, p2}, Lcom/twitter/android/samsung/single/k;->a(Landroid/content/Context;Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;J)V

    iget-object v0, p0, Lcom/twitter/android/samsung/data/q;->c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService;

    invoke-static {v0}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a(Lcom/twitter/android/samsung/data/WidgetDataUpdateService;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/samsung/data/q;->a:Lcom/twitter/android/samsung/data/f;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/twitter/android/samsung/data/q;->c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService;

    iget v1, p0, Lcom/twitter/android/samsung/data/q;->b:I

    invoke-static {v0, v1}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->b(Landroid/content/Context;I)V

    return-void
.end method

.method public a(Lcom/twitter/library/api/search/d;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/samsung/data/q;->c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService;

    invoke-static {v0}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a(Lcom/twitter/android/samsung/data/WidgetDataUpdateService;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/samsung/data/q;->a:Lcom/twitter/android/samsung/data/f;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/samsung/data/e;

    invoke-virtual {v0, p1}, Lcom/twitter/android/samsung/data/e;->a(Lcom/twitter/library/api/search/d;)V

    iget-object v0, p0, Lcom/twitter/android/samsung/data/q;->c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService;

    invoke-static {v0}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a(Lcom/twitter/android/samsung/data/WidgetDataUpdateService;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/samsung/data/q;->a:Lcom/twitter/android/samsung/data/f;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/twitter/library/api/search/d;

    invoke-virtual {p0, p1}, Lcom/twitter/android/samsung/data/q;->a(Lcom/twitter/library/api/search/d;)V

    return-void
.end method
