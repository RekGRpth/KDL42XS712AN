.class Lcom/twitter/android/samsung/data/m;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/samsung/data/g;


# instance fields
.field final synthetic a:Lcom/twitter/android/samsung/data/f;

.field final synthetic b:I

.field final synthetic c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService;


# direct methods
.method constructor <init>(Lcom/twitter/android/samsung/data/WidgetDataUpdateService;Lcom/twitter/android/samsung/data/f;I)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/samsung/data/m;->c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService;

    iput-object p2, p0, Lcom/twitter/android/samsung/data/m;->a:Lcom/twitter/android/samsung/data/f;

    iput p3, p0, Lcom/twitter/android/samsung/data/m;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILjava/lang/String;Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/samsung/data/m;->c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService;

    invoke-static {v0}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a(Lcom/twitter/android/samsung/data/WidgetDataUpdateService;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/samsung/data/m;->a:Lcom/twitter/android/samsung/data/f;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/samsung/data/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/android/samsung/data/e;->a(ILjava/lang/String;Ljava/lang/Exception;)V

    iget-object v0, p0, Lcom/twitter/android/samsung/data/m;->c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService;

    invoke-static {v0}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a(Lcom/twitter/android/samsung/data/WidgetDataUpdateService;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/samsung/data/m;->a:Lcom/twitter/android/samsung/data/f;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public a(J)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/samsung/data/m;->c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService;

    sget-object v1, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;->d:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    invoke-static {v0, v1, p1, p2}, Lcom/twitter/android/samsung/single/k;->a(Landroid/content/Context;Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;J)V

    iget-object v0, p0, Lcom/twitter/android/samsung/data/m;->c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService;

    invoke-static {v0}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a(Lcom/twitter/android/samsung/data/WidgetDataUpdateService;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/samsung/data/m;->a:Lcom/twitter/android/samsung/data/f;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/twitter/android/samsung/data/m;->c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService;

    iget v1, p0, Lcom/twitter/android/samsung/data/m;->b:I

    invoke-static {v0, v1}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->b(Landroid/content/Context;I)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/twitter/android/samsung/data/m;->a(Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "guest_token"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/samsung/data/m;->c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService;

    invoke-static {v0}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a(Lcom/twitter/android/samsung/data/WidgetDataUpdateService;)Ljava/util/Map;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/samsung/data/m;->a:Lcom/twitter/android/samsung/data/f;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/samsung/data/e;

    invoke-virtual {v0, v1}, Lcom/twitter/android/samsung/data/e;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/samsung/data/m;->c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService;

    invoke-static {v0}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a(Lcom/twitter/android/samsung/data/WidgetDataUpdateService;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/samsung/data/m;->a:Lcom/twitter/android/samsung/data/f;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
