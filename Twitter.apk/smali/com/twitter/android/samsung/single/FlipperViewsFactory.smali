.class public Lcom/twitter/android/samsung/single/FlipperViewsFactory;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/widget/RemoteViewsService$RemoteViewsFactory;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final a:Landroid/util/SparseBooleanArray;


# instance fields
.field private b:Ljava/util/List;

.field private final c:Landroid/content/Context;

.field private d:I

.field private e:Lcom/twitter/library/util/aa;

.field private f:Lcom/twitter/library/util/aa;

.field private g:Ljava/util/List;

.field private h:Ljava/util/List;

.field private i:Lcom/twitter/library/util/ac;

.field private j:Lcom/twitter/library/util/ac;

.field private k:Ljava/lang/Object;

.field private l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    sput-object v0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->a:Landroid/util/SparseBooleanArray;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/twitter/android/samsung/data/j;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->b:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->g:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->h:Ljava/util/List;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->k:Ljava/lang/Object;

    iput-object p1, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->c:Landroid/content/Context;

    invoke-direct {p0, p2}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->a(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    const-string/jumbo v1, "ss_widget_photo_cache"

    new-instance v2, Lcom/twitter/android/samsung/single/f;

    invoke-direct {v2, p0, p1}, Lcom/twitter/android/samsung/single/f;-><init>(Lcom/twitter/android/samsung/single/FlipperViewsFactory;Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->a(Ljava/lang/String;Lcom/twitter/library/util/ab;)Lcom/twitter/library/util/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->e:Lcom/twitter/library/util/aa;

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->c:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->f(Landroid/content/Context;)Lcom/twitter/library/util/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->f:Lcom/twitter/library/util/aa;

    return-void
.end method

.method private a()Lcom/twitter/library/util/ac;
    .locals 1

    new-instance v0, Lcom/twitter/android/samsung/single/d;

    invoke-direct {v0, p0}, Lcom/twitter/android/samsung/single/d;-><init>(Lcom/twitter/android/samsung/single/FlipperViewsFactory;)V

    iput-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->i:Lcom/twitter/library/util/ac;

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->i:Lcom/twitter/library/util/ac;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/samsung/single/FlipperViewsFactory;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->k:Ljava/lang/Object;

    return-object v0
.end method

.method private a(Ljava/util/List;)Ljava/util/List;
    .locals 2

    const/4 v0, 0x5

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v1, v0, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    :cond_0
    if-eqz v0, :cond_1

    const/4 v1, 0x0

    invoke-interface {p1, v1, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    :cond_1
    return-object p1
.end method

.method private a(Landroid/content/Intent;)V
    .locals 2

    const-string/jumbo v0, "appWidgetId"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->d:I

    return-void
.end method

.method private a(Lcom/twitter/android/samsung/single/FlipperViewsFactory$ImageType;Z)V
    .locals 3

    iget-object v1, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->k:Ljava/lang/Object;

    monitor-enter v1

    if-nez p2, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/samsung/single/p;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/twitter/android/samsung/single/g;->a:[I

    invoke-virtual {p1}, Lcom/twitter/android/samsung/single/FlipperViewsFactory$ImageType;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->i:Lcom/twitter/library/util/ac;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->j:Lcom/twitter/library/util/ac;

    if-nez v0, :cond_1

    sget-object v0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->a:Landroid/util/SparseBooleanArray;

    iget v2, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->d:I

    invoke-virtual {v0, v2}, Landroid/util/SparseBooleanArray;->delete(I)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->c:Landroid/content/Context;

    invoke-direct {p0}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->c()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    monitor-exit v1

    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->h()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :pswitch_1
    :try_start_1
    invoke-direct {p0}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->g()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lcom/twitter/android/samsung/single/FlipperViewsFactory;Lcom/twitter/android/samsung/single/FlipperViewsFactory$ImageType;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->a(Lcom/twitter/android/samsung/single/FlipperViewsFactory$ImageType;Z)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/samsung/single/FlipperViewsFactory;Lcom/twitter/library/util/ae;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->a(Lcom/twitter/library/util/ae;)Z

    move-result v0

    return v0
.end method

.method private a(Lcom/twitter/library/util/ae;)Z
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/util/ae;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()Lcom/twitter/library/util/ac;
    .locals 1

    new-instance v0, Lcom/twitter/android/samsung/single/e;

    invoke-direct {v0, p0}, Lcom/twitter/android/samsung/single/e;-><init>(Lcom/twitter/android/samsung/single/FlipperViewsFactory;)V

    iput-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->j:Lcom/twitter/library/util/ac;

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->j:Lcom/twitter/library/util/ac;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/samsung/single/FlipperViewsFactory;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->g:Ljava/util/List;

    return-object v0
.end method

.method private b(Ljava/util/List;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->d(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->g:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->c(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->h:Ljava/util/List;

    return-void
.end method

.method private c()Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->c:Landroid/content/Context;

    const-class v2, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v1, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "appWidgetId"

    iget v2, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->d:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/samsung/single/FlipperViewsFactory;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->h:Ljava/util/List;

    return-object v0
.end method

.method private c(Ljava/util/List;)Ljava/util/List;
    .locals 4

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/samsung/model/WidgetViewModel;

    invoke-virtual {v0}, Lcom/twitter/android/samsung/model/WidgetViewModel;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v3, Lcom/twitter/library/util/m;

    invoke-direct {v3, v0}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->f:Lcom/twitter/library/util/aa;

    invoke-virtual {v0, v3}, Lcom/twitter/library/util/aa;->b(Ljava/lang/Object;)Lcom/twitter/library/util/af;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ae;

    invoke-direct {p0, v0}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->a(Lcom/twitter/library/util/ae;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method private d(Ljava/util/List;)Ljava/util/List;
    .locals 6

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/samsung/model/WidgetViewModel;

    invoke-virtual {v0}, Lcom/twitter/android/samsung/model/WidgetViewModel;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v3, Lcom/twitter/library/util/m;

    const-string/jumbo v4, "small"

    const-string/jumbo v5, "large"

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->e:Lcom/twitter/library/util/aa;

    invoke-virtual {v0, v3}, Lcom/twitter/library/util/aa;->b(Ljava/lang/Object;)Lcom/twitter/library/util/af;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ae;

    invoke-direct {p0, v0}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->a(Lcom/twitter/library/util/ae;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method private d()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/samsung/single/p;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->f()V

    :cond_0
    return-void
.end method

.method private e(Ljava/util/List;)V
    .locals 6

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/m;

    iget v1, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->d:I

    int-to-long v1, v1

    iget-object v4, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->c:Landroid/content/Context;

    iget v5, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->d:I

    invoke-static {v4, v5}, Lcom/twitter/android/samsung/single/k;->f(Landroid/content/Context;I)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->c:Landroid/content/Context;

    iget v5, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->d:I

    invoke-static {v4, v5}, Lcom/twitter/android/samsung/single/k;->b(Landroid/content/Context;I)Ljava/lang/Long;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    :cond_0
    iget-object v4, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->e:Lcom/twitter/library/util/aa;

    invoke-virtual {v4, v1, v2, v0}, Lcom/twitter/library/util/aa;->a(JLcom/twitter/library/util/m;)Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_1
    return-void
.end method

.method private f()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/samsung/single/p;->c(Landroid/content/Context;)Z

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->k:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v2, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->a:Landroid/util/SparseBooleanArray;

    iget v3, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->d:I

    invoke-virtual {v2, v3}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v2

    if-nez v2, :cond_1

    if-eqz v0, :cond_2

    sget-object v0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->a:Landroid/util/SparseBooleanArray;

    iget v2, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->d:I

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->e:Lcom/twitter/library/util/aa;

    invoke-direct {p0}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->a()Lcom/twitter/library/util/ac;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/util/aa;->a(Lcom/twitter/library/util/ac;)V

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->g:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->e(Ljava/util/List;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->f:Lcom/twitter/library/util/aa;

    invoke-direct {p0}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->b()Lcom/twitter/library/util/ac;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/util/aa;->a(Lcom/twitter/library/util/ac;)V

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->h:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->f(Ljava/util/List;)V

    :cond_1
    :goto_0
    monitor-exit v1

    return-void

    :cond_2
    invoke-direct {p0}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->h()V

    invoke-direct {p0}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->g()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private f(Ljava/util/List;)V
    .locals 6

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/m;

    iget v1, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->d:I

    int-to-long v1, v1

    iget-object v4, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->c:Landroid/content/Context;

    iget v5, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->d:I

    invoke-static {v4, v5}, Lcom/twitter/android/samsung/single/k;->f(Landroid/content/Context;I)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v1, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->c:Landroid/content/Context;

    iget v2, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->d:I

    invoke-static {v1, v2}, Lcom/twitter/android/samsung/single/k;->b(Landroid/content/Context;I)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    :cond_0
    iget-object v4, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->f:Lcom/twitter/library/util/aa;

    invoke-virtual {v4, v1, v2, v0}, Lcom/twitter/library/util/aa;->a(JLcom/twitter/library/util/m;)Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_1
    return-void
.end method

.method private g()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->j:Lcom/twitter/library/util/ac;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->f:Lcom/twitter/library/util/aa;

    iget-object v1, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->j:Lcom/twitter/library/util/ac;

    invoke-virtual {v0, v1}, Lcom/twitter/library/util/aa;->b(Lcom/twitter/library/util/ac;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->j:Lcom/twitter/library/util/ac;

    :cond_0
    return-void
.end method

.method private h()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->i:Lcom/twitter/library/util/ac;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->e:Lcom/twitter/library/util/aa;

    iget-object v1, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->i:Lcom/twitter/library/util/ac;

    invoke-virtual {v0, v1}, Lcom/twitter/library/util/aa;->b(Lcom/twitter/library/util/ac;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->i:Lcom/twitter/library/util/ac;

    :cond_0
    return-void
.end method

.method private i()V
    .locals 1

    invoke-static {}, Lcom/twitter/android/samsung/data/j;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->b:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->b:Ljava/util/List;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->i()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->b:Ljava/util/List;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->i()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    int-to-long v0, p1

    :goto_0
    return-wide v0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/samsung/model/WidgetViewModel;

    invoke-virtual {v0}, Lcom/twitter/android/samsung/model/WidgetViewModel;->e()J

    move-result-wide v0

    goto :goto_0
.end method

.method public getLoadingView()Landroid/widget/RemoteViews;
    .locals 3

    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f030195    # com.twitter.android.R.layout.widget_single_item_loading

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method

.method public getViewAt(I)Landroid/widget/RemoteViews;
    .locals 5

    const/16 v4, 0x8

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->i()V

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/samsung/model/WidgetViewModel;

    new-instance v1, Landroid/widget/RemoteViews;

    iget-object v2, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/twitter/android/samsung/model/WidgetViewModel;->a()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->c:Landroid/content/Context;

    invoke-virtual {v0, v2, v1}, Lcom/twitter/android/samsung/model/WidgetViewModel;->a(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    iget-boolean v2, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->l:Z

    if-eqz v2, :cond_2

    const v2, 0x7f0902f3    # com.twitter.android.R.id.logged_out_share

    invoke-virtual {v1, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v2, 0x7f0902f5    # com.twitter.android.R.id.logged_in_button_frame

    invoke-virtual {v1, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v2, 0x7f0902f4    # com.twitter.android.R.id.button_frame_divider

    invoke-virtual {v1, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    :goto_1
    sget-object v2, Lcom/twitter/android/samsung/single/g;->b:[I

    invoke-virtual {v0}, Lcom/twitter/android/samsung/model/WidgetViewModel;->d()Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move-object v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->c:Landroid/content/Context;

    iget v3, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->d:I

    invoke-virtual {v0, v2, v1, v3}, Lcom/twitter/android/samsung/model/WidgetViewModel;->a(Landroid/content/Context;Landroid/widget/RemoteViews;I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    invoke-static {}, Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;->values()[Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onCreate()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->c:Landroid/content/Context;

    iget v1, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->d:I

    invoke-static {v0, v1}, Lcom/twitter/android/samsung/single/p;->d(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->l:Z

    return-void
.end method

.method public onDataSetChanged()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->c:Landroid/content/Context;

    iget v1, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->d:I

    invoke-static {v0, v1}, Lcom/twitter/android/samsung/data/j;->a(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/android/samsung/single/p;->c(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->a:Landroid/util/SparseBooleanArray;

    iget v2, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->d:I

    invoke-virtual {v1, v2}, Landroid/util/SparseBooleanArray;->delete(I)V

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->b:Ljava/util/List;

    if-eq v1, v0, :cond_5

    iget-object v1, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->b:Ljava/util/List;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->b:Ljava/util/List;

    invoke-static {}, Lcom/twitter/android/samsung/data/j;->a()Ljava/util/List;

    move-result-object v2

    if-ne v1, v2, :cond_2

    :cond_1
    invoke-direct {p0, v0}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    :cond_2
    invoke-direct {p0, v0}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->b(Ljava/util/List;)V

    iput-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->b:Ljava/util/List;

    invoke-direct {p0}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->d()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/samsung/single/p;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    :goto_0
    return-void

    :cond_4
    invoke-direct {p0}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->e()V

    goto :goto_0

    :cond_5
    invoke-direct {p0, v0}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->b(Ljava/util/List;)V

    invoke-direct {p0}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->d()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->e()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 0

    return-void
.end method
