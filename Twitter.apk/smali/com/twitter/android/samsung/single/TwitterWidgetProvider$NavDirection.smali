.class final enum Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;
.super Ljava/lang/Enum;
.source "Twttr"


# static fields
.field public static final enum a:Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;

.field public static final enum b:Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;

.field private static final synthetic c:[Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;

    const-string/jumbo v1, "FORWARDS"

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;->a:Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;

    new-instance v0, Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;

    const-string/jumbo v1, "BACKWARDS"

    invoke-direct {v0, v1, v3}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;->b:Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;

    sget-object v1, Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;->a:Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;

    aput-object v1, v0, v2

    sget-object v1, Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;->b:Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;

    aput-object v1, v0, v3

    sput-object v0, Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;->c:[Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;
    .locals 1

    const-class v0, Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;

    return-object v0
.end method

.method public static values()[Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;
    .locals 1

    sget-object v0, Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;->c:[Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;

    invoke-virtual {v0}, [Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;

    return-object v0
.end method
