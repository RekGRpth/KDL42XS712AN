.class Lcom/twitter/android/samsung/single/i;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/samsung/single/h;


# direct methods
.method constructor <init>(Lcom/twitter/android/samsung/single/h;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/samsung/single/i;->a:Lcom/twitter/android/samsung/single/h;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/samsung/single/i;->a:Lcom/twitter/android/samsung/single/h;

    iget-object v0, v0, Lcom/twitter/android/samsung/single/h;->g:Lcom/twitter/android/samsung/single/RetweetOptionsActivity;

    invoke-static {v0}, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->a(Lcom/twitter/android/samsung/single/RetweetOptionsActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/samsung/single/i;->a:Lcom/twitter/android/samsung/single/h;

    iget-object v0, v0, Lcom/twitter/android/samsung/single/h;->g:Lcom/twitter/android/samsung/single/RetweetOptionsActivity;

    const v1, 0x7f0f04ee    # com.twitter.android.R.string.tweets_retweet_error

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/samsung/single/i;->a:Lcom/twitter/android/samsung/single/h;

    iget-object v0, v0, Lcom/twitter/android/samsung/single/h;->g:Lcom/twitter/android/samsung/single/RetweetOptionsActivity;

    invoke-virtual {v0}, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->finish()V

    :cond_0
    return-void

    :cond_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/samsung/single/i;->a:Lcom/twitter/android/samsung/single/h;

    iget-object v1, v1, Lcom/twitter/android/samsung/single/h;->g:Lcom/twitter/android/samsung/single/RetweetOptionsActivity;

    const-class v2, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v1, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "appWidgetId"

    iget-object v2, p0, Lcom/twitter/android/samsung/single/i;->a:Lcom/twitter/android/samsung/single/h;

    iget v2, v2, Lcom/twitter/android/samsung/single/h;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/samsung/single/i;->a:Lcom/twitter/android/samsung/single/h;

    iget-object v1, v1, Lcom/twitter/android/samsung/single/h;->g:Lcom/twitter/android/samsung/single/RetweetOptionsActivity;

    invoke-virtual {v1, v0}, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public b(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/samsung/single/i;->a:Lcom/twitter/android/samsung/single/h;

    iget-object v0, v0, Lcom/twitter/android/samsung/single/h;->g:Lcom/twitter/android/samsung/single/RetweetOptionsActivity;

    invoke-static {v0}, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->a(Lcom/twitter/android/samsung/single/RetweetOptionsActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/samsung/single/i;->a:Lcom/twitter/android/samsung/single/h;

    iget-object v1, v1, Lcom/twitter/android/samsung/single/h;->g:Lcom/twitter/android/samsung/single/RetweetOptionsActivity;

    const-class v2, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v1, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "appWidgetId"

    iget-object v2, p0, Lcom/twitter/android/samsung/single/i;->a:Lcom/twitter/android/samsung/single/h;

    iget v2, v2, Lcom/twitter/android/samsung/single/h;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/samsung/single/i;->a:Lcom/twitter/android/samsung/single/h;

    iget-object v1, v1, Lcom/twitter/android/samsung/single/h;->g:Lcom/twitter/android/samsung/single/RetweetOptionsActivity;

    invoke-virtual {v1, v0}, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/twitter/android/samsung/single/i;->a:Lcom/twitter/android/samsung/single/h;

    iget-object v0, v0, Lcom/twitter/android/samsung/single/h;->g:Lcom/twitter/android/samsung/single/RetweetOptionsActivity;

    invoke-virtual {v0}, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->finish()V

    :cond_0
    return-void
.end method
