.class public final enum Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;
.super Ljava/lang/Enum;
.source "Twttr"


# static fields
.field public static final enum a:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

.field public static final enum b:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

.field public static final enum c:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

.field public static final enum d:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

.field public static final enum e:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

.field public static final enum f:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

.field public static final enum g:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

.field public static final enum h:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

.field public static final enum i:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

.field private static final synthetic j:[Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    const-string/jumbo v1, "IMAGE"

    invoke-direct {v0, v1, v3}, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->a:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    new-instance v0, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    const-string/jumbo v1, "REPLY"

    invoke-direct {v0, v1, v4}, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->b:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    new-instance v0, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    const-string/jumbo v1, "RETWEET"

    invoke-direct {v0, v1, v5}, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->c:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    new-instance v0, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    const-string/jumbo v1, "FAVOURITE"

    invoke-direct {v0, v1, v6}, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->d:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    new-instance v0, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    const-string/jumbo v1, "SHARE"

    invoke-direct {v0, v1, v7}, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->e:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    new-instance v0, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    const-string/jumbo v1, "USER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->f:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    new-instance v0, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    const-string/jumbo v1, "SIGN_IN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->g:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    new-instance v0, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    const-string/jumbo v1, "SIGN_UP"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->h:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    new-instance v0, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    const-string/jumbo v1, "APP"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->i:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    sget-object v1, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->a:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->b:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->c:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->d:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->e:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->f:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->g:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->h:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->i:Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->j:[Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;
    .locals 1

    const-class v0, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    return-object v0
.end method

.method public static values()[Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;
    .locals 1

    sget-object v0, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->j:[Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    invoke-virtual {v0}, [Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    return-object v0
.end method
