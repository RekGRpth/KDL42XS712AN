.class Lcom/twitter/android/samsung/single/d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/util/ac;


# instance fields
.field final synthetic a:Lcom/twitter/android/samsung/single/FlipperViewsFactory;


# direct methods
.method constructor <init>(Lcom/twitter/android/samsung/single/FlipperViewsFactory;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/samsung/single/d;->a:Lcom/twitter/android/samsung/single/FlipperViewsFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/util/aa;Ljava/util/HashMap;)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/samsung/single/d;->a:Lcom/twitter/android/samsung/single/FlipperViewsFactory;

    invoke-static {v0}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->a(Lcom/twitter/android/samsung/single/FlipperViewsFactory;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/samsung/single/d;->a:Lcom/twitter/android/samsung/single/FlipperViewsFactory;

    invoke-static {v0}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->b(Lcom/twitter/android/samsung/single/FlipperViewsFactory;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iget-object v4, p0, Lcom/twitter/android/samsung/single/d;->a:Lcom/twitter/android/samsung/single/FlipperViewsFactory;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    invoke-static {v4, v1}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->a(Lcom/twitter/android/samsung/single/FlipperViewsFactory;Lcom/twitter/library/util/ae;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/samsung/single/d;->a:Lcom/twitter/android/samsung/single/FlipperViewsFactory;

    invoke-static {v1}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->b(Lcom/twitter/android/samsung/single/FlipperViewsFactory;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/samsung/single/d;->a:Lcom/twitter/android/samsung/single/FlipperViewsFactory;

    sget-object v1, Lcom/twitter/android/samsung/single/FlipperViewsFactory$ImageType;->a:Lcom/twitter/android/samsung/single/FlipperViewsFactory$ImageType;

    iget-object v3, p0, Lcom/twitter/android/samsung/single/d;->a:Lcom/twitter/android/samsung/single/FlipperViewsFactory;

    invoke-static {v3}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->b(Lcom/twitter/android/samsung/single/FlipperViewsFactory;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    invoke-static {v0, v1, v3}, Lcom/twitter/android/samsung/single/FlipperViewsFactory;->a(Lcom/twitter/android/samsung/single/FlipperViewsFactory;Lcom/twitter/android/samsung/single/FlipperViewsFactory$ImageType;Z)V

    :cond_2
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
