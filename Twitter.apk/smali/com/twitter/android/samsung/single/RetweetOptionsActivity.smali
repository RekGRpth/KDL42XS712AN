.class public Lcom/twitter/android/samsung/single/RetweetOptionsActivity;
.super Landroid/app/Activity;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private a:Landroid/app/Dialog;

.field private b:Lcom/twitter/library/client/j;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/samsung/single/RetweetOptionsActivity;Lcom/twitter/library/client/j;)Lcom/twitter/library/client/j;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->b:Lcom/twitter/library/client/j;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/samsung/single/RetweetOptionsActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/samsung/single/RetweetOptionsActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->c:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/twitter/android/samsung/single/RetweetOptionsActivity;)Lcom/twitter/library/client/j;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->b:Lcom/twitter/library/client/j;

    return-object v0
.end method


# virtual methods
.method public a(ZLandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;
    .locals 4

    const v3, 0x7f0f04ed    # com.twitter.android.R.string.tweets_retweet

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    if-eqz p1, :cond_0

    const v1, 0x7f0f0509    # com.twitter.android.R.string.undo_retweet_confirm_message

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f04fa    # com.twitter.android.R.string.tweets_undo_retweet

    invoke-virtual {v1, v2, p2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :goto_0
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f033d    # com.twitter.android.R.string.quote

    invoke-virtual {v0, v1, p2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f0089    # com.twitter.android.R.string.cancel

    invoke-virtual {v0, v1, p2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    :cond_0
    const v1, 0x7f0f0364    # com.twitter.android.R.string.retweet_confirm_message

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v3, p2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->finish()V

    return-void
.end method

.method public onBackPressed()V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v3

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->finish()V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "tweet"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Lcom/twitter/library/provider/Tweet;

    invoke-virtual {p0}, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "owner_id"

    const-wide/16 v7, -0x1

    invoke-virtual {v0, v1, v7, v8}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v4, "appWidgetId"

    const/4 v7, -0x1

    invoke-virtual {v2, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v5, v0, v1}, Lcom/twitter/library/provider/Tweet;->a(J)Z

    move-result v4

    invoke-static {p0, v2}, Lcom/twitter/android/samsung/single/k;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v7

    new-instance v0, Lcom/twitter/android/samsung/single/h;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/samsung/single/h;-><init>(Lcom/twitter/android/samsung/single/RetweetOptionsActivity;ILcom/twitter/android/client/c;ZLcom/twitter/library/provider/Tweet;Lcom/twitter/library/client/aa;Ljava/lang/String;)V

    invoke-virtual {p0, v4, v0}, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->a(ZLandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->a:Landroid/app/Dialog;

    iget-object v0, p0, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->a:Landroid/app/Dialog;

    new-instance v1, Lcom/twitter/android/samsung/single/j;

    invoke-direct {v1, p0}, Lcom/twitter/android/samsung/single/j;-><init>(Lcom/twitter/android/samsung/single/RetweetOptionsActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    iget-object v0, p0, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->a:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->a:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->b:Lcom/twitter/library/client/j;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/j;)V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->finish()V

    return-void
.end method
