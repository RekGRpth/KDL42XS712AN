.class public Lcom/twitter/android/samsung/single/TwitterWidgetProvider;
.super Landroid/appwidget/AppWidgetProvider;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field protected static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field public static final d:Ljava/lang/String;

.field public static final e:Ljava/lang/String;

.field private static final f:Ljava/lang/String;

.field private static final g:Ljava/lang/String;

.field private static final h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string/jumbo v0, ".widget.single.navigation.forward"

    invoke-static {v0}, Lcom/twitter/library/client/App;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->f:Ljava/lang/String;

    const-string/jumbo v0, ".widget.single.navigation.back"

    invoke-static {v0}, Lcom/twitter/library/client/App;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->g:Ljava/lang/String;

    const-string/jumbo v0, ".widget.single.flipper.item_click"

    invoke-static {v0}, Lcom/twitter/library/client/App;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->h:Ljava/lang/String;

    const-string/jumbo v0, ".widget.single.flipper.image_update"

    invoke-static {v0}, Lcom/twitter/library/client/App;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a:Ljava/lang/String;

    const-string/jumbo v0, ".widget.single.APPWIDGET_DATA_UPDATE"

    invoke-static {v0}, Lcom/twitter/library/client/App;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->b:Ljava/lang/String;

    const-string/jumbo v0, ".widget.single.update_feed"

    invoke-static {v0}, Lcom/twitter/library/client/App;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->c:Ljava/lang/String;

    const-string/jumbo v0, ".widget.single.logged_out_feed_updated"

    invoke-static {v0}, Lcom/twitter/library/client/App;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->d:Ljava/lang/String;

    const-string/jumbo v0, ".single.configuration_changed"

    invoke-static {v0}, Lcom/twitter/library/client/App;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method

.method private a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)Landroid/widget/RemoteViews;
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->b(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)Landroid/widget/RemoteViews;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->b(Landroid/content/Context;Landroid/widget/RemoteViews;I)V

    invoke-direct {p0, p1, p3, v0}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;ILandroid/widget/RemoteViews;)V

    return-object v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 8

    const/4 v1, 0x0

    const/4 v7, 0x1

    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    sget-object v2, Lcom/twitter/android/samsung/model/g;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    invoke-static {p1}, Lcom/twitter/android/samsung/single/p;->d(Landroid/content/Context;)[I

    move-result-object v3

    move v0, v1

    :goto_0
    array-length v4, v3

    if-ge v0, v4, :cond_3

    aget v4, v3, v0

    invoke-static {p1}, Lcom/twitter/android/samsung/model/g;->a(Landroid/content/Context;)I

    move-result v5

    if-lez v5, :cond_2

    aget-object v5, v2, v1

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p1, v4}, Lcom/twitter/android/samsung/single/k;->f(Landroid/content/Context;I)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-static {p1, v5, v4}, Lcom/twitter/android/samsung/single/k;->a(Landroid/content/Context;Ljava/lang/String;I)V

    invoke-direct {p0, p1, v4, v7}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;IZ)V

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-static {p1, v4}, Lcom/twitter/android/samsung/single/k;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    invoke-static {p1, v6}, Lcom/twitter/android/samsung/model/g;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v6

    if-nez v6, :cond_0

    invoke-static {p1, v5, v4}, Lcom/twitter/android/samsung/single/k;->a(Landroid/content/Context;Ljava/lang/String;I)V

    invoke-direct {p0, p1, v4, v7}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;IZ)V

    goto :goto_1

    :cond_2
    invoke-static {p1, v4}, Lcom/twitter/android/samsung/single/k;->g(Landroid/content/Context;I)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {p1, v4}, Lcom/twitter/android/samsung/single/k;->h(Landroid/content/Context;I)V

    invoke-direct {p0, p1, v4, v7}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;IZ)V

    goto :goto_1

    :cond_3
    return-void
.end method

.method private a(Landroid/content/Context;I)V
    .locals 4

    const v3, 0x7f0902f1    # com.twitter.android.R.id.stub_overlay

    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->b(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)Landroid/widget/RemoteViews;

    move-result-object v1

    invoke-direct {p0, p1, v1, p2}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->b(Landroid/content/Context;Landroid/widget/RemoteViews;I)V

    const/4 v2, 0x0

    invoke-virtual {v1, v3, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->d(Landroid/content/Context;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    invoke-virtual {v0, p2, v1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    return-void
.end method

.method private a(Landroid/content/Context;ILandroid/widget/RemoteViews;)V
    .locals 3

    const v2, 0x7f0902ef    # com.twitter.android.R.id.forward

    const v1, 0x7f0902ee    # com.twitter.android.R.id.backward

    invoke-virtual {p0, p1, p2, v1}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;II)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p3, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    invoke-virtual {p0, p1, p2, v2}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;II)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p3, v2, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    const v0, 0x7f0902ed    # com.twitter.android.R.id.configure_btn

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->c(Landroid/content/Context;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    const v0, 0x7f0902ec    # com.twitter.android.R.id.twitter_logo

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->b(Landroid/content/Context;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    return-void
.end method

.method private a(Landroid/content/Context;IZ)V
    .locals 6

    invoke-static {p2}, Lcom/twitter/android/samsung/data/j;->a(I)V

    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;IZZ)V

    return-void
.end method

.method private a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;IZZ)V
    .locals 3

    invoke-static {p1, p3}, Lcom/twitter/android/samsung/single/p;->c(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)Landroid/widget/RemoteViews;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;Landroid/widget/RemoteViews;I)V

    invoke-static {p1, p3}, Lcom/twitter/android/samsung/data/j;->b(Landroid/content/Context;I)Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz p4, :cond_2

    :cond_1
    const v1, 0x7f0902eb    # com.twitter.android.R.id.widget_viewflipper_forward

    invoke-virtual {p2, p3, v1}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    const v1, 0x7f0902ea    # com.twitter.android.R.id.widget_viewflipper_backward

    invoke-virtual {p2, p3, v1}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    :cond_2
    const v1, 0x7f0902f1    # com.twitter.android.R.id.stub_overlay

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-virtual {p2, p3, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    const-string/jumbo v0, "appWidgetId"

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {p1, v0}, Lcom/twitter/android/samsung/single/p;->c(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;IZ)V

    :cond_0
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 6

    const/4 v4, 0x0

    const-string/jumbo v0, "appWidgetId"

    invoke-virtual {p2, v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v3}, Lcom/twitter/android/samsung/data/j;->a(I)V

    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;IZZ)V

    :cond_0
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/widget/RemoteViews;I)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v1, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p1, v1, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const v1, 0x7f0902eb    # com.twitter.android.R.id.widget_viewflipper_forward

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setPendingIntentTemplate(ILandroid/app/PendingIntent;)V

    const v1, 0x7f0902ea    # com.twitter.android.R.id.widget_viewflipper_backward

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setPendingIntentTemplate(ILandroid/app/PendingIntent;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 6

    invoke-static {p0, p2}, Lcom/twitter/android/samsung/single/k;->f(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "loggedout"

    :goto_0
    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    aput-object v0, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    return-void

    :cond_0
    const-string/jumbo v0, "loggedin"

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Z)V
    .locals 8

    const/4 v4, 0x0

    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    invoke-static {p1}, Lcom/twitter/android/samsung/single/p;->d(Landroid/content/Context;)[I

    move-result-object v7

    move v6, v4

    :goto_0
    array-length v0, v7

    if-ge v6, v0, :cond_1

    aget v3, v7, v6

    if-eqz p2, :cond_0

    invoke-static {v3}, Lcom/twitter/android/samsung/data/j;->a(I)V

    :cond_0
    move-object v0, p0

    move-object v1, p1

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;IZZ)V

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private static a(Landroid/widget/RemoteViews;Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;)V
    .locals 5

    const/4 v2, 0x4

    const/4 v1, 0x0

    const v4, 0x7f0902eb    # com.twitter.android.R.id.widget_viewflipper_forward

    const v3, 0x7f0902ea    # com.twitter.android.R.id.widget_viewflipper_backward

    sget-object v0, Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;->b:Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;

    if-ne p1, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v3, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget-object v0, Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;->a:Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;

    if-ne p1, v0, :cond_1

    :goto_1
    invoke-virtual {p0, v4, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget-object v0, Lcom/twitter/android/samsung/single/l;->a:[I

    invoke-virtual {p1}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_2
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :pswitch_0
    invoke-virtual {p0, v3}, Landroid/widget/RemoteViews;->showPrevious(I)V

    invoke-virtual {p0, v4}, Landroid/widget/RemoteViews;->showPrevious(I)V

    goto :goto_2

    :pswitch_1
    invoke-virtual {p0, v4}, Landroid/widget/RemoteViews;->showNext(I)V

    invoke-virtual {p0, v3}, Landroid/widget/RemoteViews;->showNext(I)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 1

    sget-object v0, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->c:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/twitter/android/samsung/single/p;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1

    sget-object v0, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 1

    sget-object v0, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private b(Landroid/content/Context;I)Landroid/app/PendingIntent;
    .locals 2

    invoke-static {p1, p2}, Lcom/twitter/android/samsung/single/p;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x8000000

    invoke-static {p1, p2, v0, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private b(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)Landroid/widget/RemoteViews;
    .locals 3

    invoke-static {p1, p3}, Lcom/twitter/android/samsung/single/p;->d(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f030199    # com.twitter.android.R.layout.widget_single_keyguard_layout

    :goto_0
    new-instance v1, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    return-object v1

    :cond_0
    const v0, 0x7f030192    # com.twitter.android.R.layout.widget_single_flipper_layout

    goto :goto_0
.end method

.method private b(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    const-string/jumbo v0, "appWidgetId"

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-static {p1, v3}, Lcom/twitter/android/samsung/single/p;->c(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;IZZ)V

    :cond_0
    return-void
.end method

.method private b(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 3

    const-string/jumbo v0, "appWidgetId"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const-string/jumbo v1, "direction"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, p1, v2, v1}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;Ljava/lang/Integer;Ljava/lang/Integer;)V

    invoke-static {p1, v0}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a(Landroid/content/Context;I)V

    :cond_0
    return-void
.end method

.method private b(Landroid/content/Context;Landroid/widget/RemoteViews;I)V
    .locals 7

    const v6, 0x7f0902f0    # com.twitter.android.R.id.widget_flipper_item_loading_view

    const v5, 0x7f0902eb    # com.twitter.android.R.id.widget_viewflipper_forward

    const v4, 0x7f0902ea    # com.twitter.android.R.id.widget_viewflipper_backward

    const/4 v3, 0x1

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/samsung/single/BackFlipperService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/samsung/single/ForwardFlipperService;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "appWidgetId"

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {v0, v3}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string/jumbo v2, "appWidgetId"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {v1, v3}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p2, v5, v1}, Landroid/widget/RemoteViews;->setRemoteAdapter(ILandroid/content/Intent;)V

    invoke-virtual {p2, v4, v0}, Landroid/widget/RemoteViews;->setRemoteAdapter(ILandroid/content/Intent;)V

    invoke-virtual {p2, v5, v6}, Landroid/widget/RemoteViews;->setEmptyView(II)V

    invoke-virtual {p2, v4, v6}, Landroid/widget/RemoteViews;->setEmptyView(II)V

    return-void
.end method

.method private b(Ljava/lang/String;)Z
    .locals 1

    const-string/jumbo v0, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private b(Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 1

    sget-object v0, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private c(Landroid/content/Context;I)Landroid/app/PendingIntent;
    .locals 2

    invoke-static {p1, p2}, Lcom/twitter/android/samsung/single/p;->b(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x8000000

    invoke-static {p1, p2, v0, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private c(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 8

    const/4 v6, -0x1

    const-string/jumbo v0, "clickType"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const-string/jumbo v1, "appWidgetId"

    invoke-virtual {p2, v1, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-static {p1, v2}, Lcom/twitter/android/samsung/single/k;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->values()[Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;

    move-result-object v1

    aget-object v0, v1, v0

    const-string/jumbo v1, "widget::tweet::action"

    sget-object v4, Lcom/twitter/android/samsung/single/l;->b:[I

    invoke-virtual {v0}, Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    invoke-static {p1, p2, v0}, Lcom/twitter/android/samsung/single/p;->a(Landroid/content/Context;Landroid/os/Bundle;Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;)Landroid/content/Intent;

    move-result-object v0

    :goto_0
    invoke-static {p1, v1, v2}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;Ljava/lang/String;I)V

    if-eqz v0, :cond_0

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_0
    :goto_1
    return-void

    :pswitch_0
    const-string/jumbo v0, "widget::tweet::share"

    invoke-static {p1, v0, v2}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;Ljava/lang/String;I)V

    const-string/jumbo v0, "tweet"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/twitter/library/provider/Tweet;->p:Ljava/lang/String;

    iget-object v3, v0, Lcom/twitter/library/provider/Tweet;->d:Ljava/lang/String;

    iget-wide v4, v0, Lcom/twitter/library/provider/Tweet;->h:J

    iget-wide v6, v0, Lcom/twitter/library/provider/Tweet;->o:J

    move-object v0, p1

    invoke-static/range {v0 .. v7}, Lcom/twitter/android/samsung/single/p;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    goto :goto_1

    :pswitch_1
    invoke-static {p1}, Lcom/twitter/android/samsung/single/p;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f000c    # com.twitter.android.R.string.action_error

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :cond_1
    const-string/jumbo v0, "widget::tweet::favorite"

    invoke-static {p1, v0, v2}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;Ljava/lang/String;I)V

    invoke-static {p1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-static {p1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v2

    const-string/jumbo v1, "tweet"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/twitter/library/provider/Tweet;

    iget-boolean v1, v6, Lcom/twitter/library/provider/Tweet;->l:Z

    if-nez v1, :cond_2

    invoke-virtual {v2, v3}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v1

    iget-wide v2, v6, Lcom/twitter/library/provider/Tweet;->o:J

    iget-wide v4, v6, Lcom/twitter/library/provider/Tweet;->C:J

    iget-object v6, v6, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;JJLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    :goto_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, p2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2, v3}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v1

    iget-wide v2, v6, Lcom/twitter/library/provider/Tweet;->o:J

    iget-object v4, v6, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/Session;JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    goto :goto_2

    :pswitch_2
    invoke-static {p1}, Lcom/twitter/android/samsung/single/p;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_3
    invoke-static {p1}, Lcom/twitter/android/samsung/single/p;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_4
    const-string/jumbo v1, "widget::tweet::retweet"

    invoke-static {p1, p2, v0}, Lcom/twitter/android/samsung/single/p;->a(Landroid/content/Context;Landroid/os/Bundle;Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_5
    const-string/jumbo v1, "widget::tweet::click"

    invoke-static {p1, p2, v0}, Lcom/twitter/android/samsung/single/p;->a(Landroid/content/Context;Landroid/os/Bundle;Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_6
    const-string/jumbo v1, "widget::tweet:profileimage:click"

    invoke-static {p1, p2, v0}, Lcom/twitter/android/samsung/single/p;->a(Landroid/content/Context;Landroid/os/Bundle;Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_7
    const-string/jumbo v1, "widget::tweet::reply"

    invoke-static {p1, p2, v0}, Lcom/twitter/android/samsung/single/p;->a(Landroid/content/Context;Landroid/os/Bundle;Lcom/twitter/android/samsung/single/FlipperViewsFactory$FlipperClickType;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_8
    const-string/jumbo v0, "appWidgetId"

    invoke-virtual {p2, v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-static {p1, v0}, Lcom/twitter/android/samsung/single/p;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private c(Ljava/lang/String;)Z
    .locals 1

    sget-object v0, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 1

    const-string/jumbo v0, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(Landroid/content/Context;I)Landroid/app/PendingIntent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/samsung/single/DataChargesActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const v1, 0x10808000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string/jumbo v1, "appWidgetId"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/high16 v1, 0x8000000

    invoke-static {p1, p2, v0, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private d(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 5

    const/4 v1, 0x0

    invoke-static {p1}, Lcom/twitter/android/samsung/single/p;->d(Landroid/content/Context;)[I

    move-result-object v2

    move v0, v1

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_1

    aget v3, v2, v0

    invoke-static {p1, v3}, Lcom/twitter/android/samsung/single/k;->f(Landroid/content/Context;I)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0, p1, v3, v1}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;IZ)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private d(Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 1

    sget-object v0, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private e(Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 1

    sget-object v0, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private f(Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 1

    const-string/jumbo v0, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected a(Landroid/content/Context;II)Landroid/app/PendingIntent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "appWidgetId"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v1, "direction"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const v1, 0x7f0902ef    # com.twitter.android.R.id.forward

    if-ne p3, v1, :cond_0

    sget-object v1, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    :goto_0
    const/high16 v1, 0x8000000

    invoke-static {p1, p2, v0, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v1, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method protected a(Landroid/content/Context;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 4

    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->b(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)Landroid/widget/RemoteViews;

    move-result-object v1

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const v3, 0x7f0902ee    # com.twitter.android.R.id.backward

    if-ne v2, v3, :cond_1

    const-string/jumbo v2, "widget::tweet:prev:click"

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {p1, v2, v3}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;Ljava/lang/String;I)V

    sget-object v2, Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;->b:Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;

    invoke-static {v1, v2}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/widget/RemoteViews;Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;)V

    :cond_0
    :goto_0
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v2, v1}, Landroid/appwidget/AppWidgetManager;->partiallyUpdateAppWidget(ILandroid/widget/RemoteViews;)V

    return-void

    :cond_1
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const v3, 0x7f0902ef    # com.twitter.android.R.id.forward

    if-ne v2, v3, :cond_0

    const-string/jumbo v2, "widget::tweet:next:click"

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {p1, v2, v3}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;Ljava/lang/String;I)V

    sget-object v2, Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;->a:Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;

    invoke-static {v1, v2}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/widget/RemoteViews;Lcom/twitter/android/samsung/single/TwitterWidgetProvider$NavDirection;)V

    goto :goto_0
.end method

.method public onAppWidgetOptionsChanged(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/os/Bundle;)V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    const/4 v4, 0x0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/appwidget/AppWidgetProvider;->onAppWidgetOptionsChanged(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/os/Bundle;)V

    const-string/jumbo v0, "widget::::resize"

    invoke-static {p1, v0, p3}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;Ljava/lang/String;I)V

    invoke-static {p1, p3}, Lcom/twitter/android/samsung/single/k;->d(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;IZZ)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1, p3}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public onDeleted(Landroid/content/Context;[I)V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_0

    aget v1, p2, v0

    invoke-static {p1, v1}, Lcom/twitter/android/samsung/single/k;->h(Landroid/content/Context;I)V

    const-string/jumbo v2, "widget::::remove"

    invoke-static {p1, v2, v1}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;Ljava/lang/String;I)V

    invoke-static {p1, v1}, Lcom/twitter/android/samsung/data/PollingService;->b(Landroid/content/Context;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onDeleted(Landroid/content/Context;[I)V

    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "android.appwidget.action.APPWIDGET_UPDATE_OPTIONS"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string/jumbo v0, "appWidgetId"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {p1, v0}, Lcom/twitter/android/samsung/single/p;->d(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1, v0}, Lcom/twitter/android/samsung/single/k;->d(Landroid/content/Context;I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1, v0}, Lcom/twitter/android/samsung/single/k;->e(Landroid/content/Context;I)V

    invoke-static {p1, v0}, Lcom/twitter/android/samsung/single/k;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/samsung/model/g;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    array-length v2, v1

    if-lez v2, :cond_0

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p1, v1, v0}, Lcom/twitter/android/samsung/single/k;->a(Landroid/content/Context;Ljava/lang/String;I)V

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    return-void

    :cond_1
    invoke-direct {p0, v0}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0, p1, v1}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->b(Landroid/content/Context;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v0, v1}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    :cond_3
    invoke-direct {p0, v0, v1}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->b(Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->b(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    :cond_4
    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    :cond_5
    invoke-direct {p0, v0, v1}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->c(Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-static {p1}, Lcom/twitter/android/samsung/single/p;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;Z)V

    invoke-static {p1}, Lcom/twitter/android/samsung/data/PollingService;->a(Landroid/content/Context;)V

    goto :goto_0

    :cond_6
    invoke-static {p1}, Lcom/twitter/android/samsung/data/PollingService;->b(Landroid/content/Context;)V

    goto :goto_0

    :cond_7
    invoke-direct {p0, v0, v1}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->d(Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-direct {p0, p1, v1}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->c(Landroid/content/Context;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_8
    invoke-direct {p0, v0, v1}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->e(Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-direct {p0, p1, v1}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->d(Landroid/content/Context;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_9
    invoke-direct {p0, v0, v1}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->f(Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-direct {p0, p1}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;)V

    goto :goto_0

    :cond_a
    invoke-direct {p0, v0}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-static {p1}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a(Landroid/content/Context;)V

    goto :goto_0

    :cond_b
    invoke-direct {p0, v0}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, v1}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 4

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    array-length v2, p3

    if-ge v0, v2, :cond_1

    aget v2, p3, v0

    invoke-static {p1, v2}, Lcom/twitter/android/samsung/single/k;->d(Landroid/content/Context;I)Z

    move-result v3

    if-eqz v3, :cond_0

    aget v3, p3, v0

    invoke-static {p1, v3}, Lcom/twitter/android/samsung/data/PollingService;->a(Landroid/content/Context;I)V

    invoke-direct {p0, p1, v2, v1}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;IZ)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1, v2}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;I)V

    goto :goto_1

    :cond_1
    invoke-super {p0, p1, p2, p3}, Landroid/appwidget/AppWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    return-void
.end method
