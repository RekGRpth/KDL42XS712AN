.class Lcom/twitter/android/samsung/single/o;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/samsung/single/o;->a:Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/preference/CheckBoxPreference;)V
    .locals 4

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/samsung/single/o;->a:Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;

    iget-object v0, v0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->a:Landroid/preference/PreferenceGroup;

    invoke-virtual {v0}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/samsung/single/o;->a:Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;

    iget-object v0, v0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->a:Landroid/preference/PreferenceGroup;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4

    const/4 v3, 0x1

    move-object v0, p1

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-direct {p0, v0}, Lcom/twitter/android/samsung/single/o;->a(Landroid/preference/CheckBoxPreference;)V

    invoke-virtual {p1}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-interface {v0, v3, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/samsung/single/o;->a:Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;

    iget-object v2, p0, Lcom/twitter/android/samsung/single/o;->a:Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;

    iget v2, v2, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->c:I

    invoke-static {v1, v0, v2}, Lcom/twitter/android/samsung/single/k;->a(Landroid/content/Context;Ljava/lang/String;I)V

    check-cast p1, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p1, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    return v3
.end method
