.class Lcom/twitter/android/xp;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/TweetFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/TweetFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/TweetFragment;->d(Lcom/twitter/android/TweetFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    if-eqz v0, :cond_0

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0f04ee    # com.twitter.android.R.string.tweets_retweet_error

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    iget-object v1, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v1}, Lcom/twitter/android/TweetFragment;->v(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/client/c;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    iget-object v2, v2, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v2, v2, Lcom/twitter/library/provider/Tweet;->o:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/client/c;->i(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/TweetFragment;->e(Lcom/twitter/android/TweetFragment;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JI)V
    .locals 4

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/TweetFragment;->g(Lcom/twitter/android/TweetFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0xc8

    if-eq p3, v0, :cond_2

    const/16 v0, 0x194

    if-eq p3, v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iput-boolean v2, v0, Lcom/twitter/library/provider/Tweet;->l:Z

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->t(Lcom/twitter/android/TweetFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    const v1, 0x7f0200f1    # com.twitter.android.R.drawable.ic_action_fave_on

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0f04ec    # com.twitter.android.R.string.tweets_remove_favorite_error

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    iget-object v1, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v1}, Lcom/twitter/android/TweetFragment;->x(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/client/c;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    iget-object v2, v2, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v2, v2, Lcom/twitter/library/provider/Tweet;->o:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/client/c;->i(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/TweetFragment;->h(Lcom/twitter/android/TweetFragment;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JIZ)V
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/android/TweetFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v0, v0, Lcom/twitter/android/client/PendingRequest;->b:I

    packed-switch v0, :pswitch_data_0

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0, v1, v3, v2}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-lez p7, :cond_1

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0, v1, v3, v2}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->q()Lcom/twitter/android/xo;

    move-result-object v0

    iput-wide v4, v0, Lcom/twitter/android/xo;->c:J

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/widget/PageableListView;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/PageableListView;->a(Z)V

    goto :goto_0

    :pswitch_1
    if-lez p7, :cond_3

    if-eqz p8, :cond_2

    const/4 v0, 0x4

    :goto_1
    iget-object v1, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v1}, Lcom/twitter/android/TweetFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v1, v0, v3, v2}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->q()Lcom/twitter/android/xo;

    move-result-object v0

    iput-wide v4, v0, Lcom/twitter/android/xo;->b:J

    goto :goto_0

    :cond_2
    const/4 v0, 0x3

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/widget/PageableListView;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/PageableListView;->b(Z)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->s()V

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/widget/PageableListView;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/PageableListView;->a(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JLcom/twitter/library/api/ActivitySummary;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/TweetFragment;->i(Lcom/twitter/android/TweetFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    if-eqz v0, :cond_0

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    iget-object v1, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0, p7, v1}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/library/api/ActivitySummary;Lcom/twitter/android/widget/di;)V

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->r()V

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    iput-object p7, v0, Lcom/twitter/android/TweetFragment;->h:Lcom/twitter/library/api/ActivitySummary;

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iput-object p7, v0, Lcom/twitter/library/provider/Tweet;->U:Lcom/twitter/library/api/ActivitySummary;

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->y(Lcom/twitter/android/TweetFragment;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;Lcom/twitter/library/api/TwitterStatus$Translation;)V
    .locals 6

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/TweetFragment;->k(Lcom/twitter/android/TweetFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_2

    if-eqz p5, :cond_2

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0, p5}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/library/api/TwitterStatus$Translation;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetDetailView;->d()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p5, Lcom/twitter/library/api/TwitterStatus$Translation;->c:Ljava/lang/String;

    iget-object v2, p5, Lcom/twitter/library/api/TwitterStatus$Translation;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/twitter/library/util/ad;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0f04b4    # com.twitter.android.R.string.translate_tweet_same_language

    new-array v3, v5, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v1}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0f04b2    # com.twitter.android.R.string.translate_tweet_error

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;I[ILjava/lang/String;JI)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/TweetFragment;->b(Lcom/twitter/android/TweetFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0xc8

    if-eq p3, v0, :cond_2

    const/16 v0, 0x8b

    invoke-static {p4, v0}, Lcom/twitter/library/util/Util;->a([II)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/twitter/library/provider/Tweet;->l:Z

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->t(Lcom/twitter/android/TweetFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    const v1, 0x7f0200ec    # com.twitter.android.R.drawable.ic_action_fave_off

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0f04dd    # com.twitter.android.R.string.tweets_add_favorite_error

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    iget-object v1, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v1}, Lcom/twitter/android/TweetFragment;->u(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/client/c;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    iget-object v2, v2, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v2, v2, Lcom/twitter/library/provider/Tweet;->o:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/client/c;->i(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/TweetFragment;->c(Lcom/twitter/android/TweetFragment;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;I[ILjava/lang/String;Lcom/twitter/library/api/TwitterUser;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/TweetFragment;->j(Lcom/twitter/android/TweetFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_0

    if-eqz p6, :cond_0

    iget-wide v0, p6, Lcom/twitter/library/api/TwitterUser;->userId:J

    iget-object v2, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    iget-object v2, v2, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v2, v2, Lcom/twitter/library/provider/Tweet;->n:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget v1, p6, Lcom/twitter/library/api/TwitterUser;->friendship:I

    iput v1, v0, Lcom/twitter/library/provider/Tweet;->ac:I

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v1, p6, Lcom/twitter/library/api/TwitterUser;->friendshipTime:J

    iput-wide v1, v0, Lcom/twitter/library/provider/Tweet;->ad:J

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->z(Lcom/twitter/android/TweetFragment;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Z)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->A(Lcom/twitter/android/TweetFragment;)Landroid/support/v4/widget/CursorAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ye;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Lcom/twitter/android/ye;->b(Z)V

    :cond_0
    return-void
.end method

.method public a(Ljava/util/HashMap;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/TweetDetailView;->a(Ljava/util/Map;)V

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->q(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->S()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->r(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/client/au;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/au;->a()Z

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v1}, Lcom/twitter/android/TweetFragment;->s(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/client/au;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/au;->a(Z)V

    iget-object v1, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    iget-object v1, v1, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/xq;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    iget-object v1, v1, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/xq;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, p1, v0}, Lcom/twitter/android/xq;->a(Ljava/util/HashMap;Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    iget-object v1, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v1}, Lcom/twitter/android/TweetFragment;->w(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/client/c;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/xp;->a:Lcom/twitter/android/TweetFragment;

    iget-object v2, v2, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v2, v2, Lcom/twitter/library/provider/Tweet;->o:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/client/c;->i(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/TweetFragment;->f(Lcom/twitter/android/TweetFragment;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
