.class public Lcom/twitter/android/MentionTimelineFragment;
.super Lcom/twitter/android/TimelineFragment;
.source "Twttr"


# instance fields
.field private a:Lcom/twitter/android/util/w;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/TimelineFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(JJ)V
    .locals 5

    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/android/TimelineFragment;->a(JJ)V

    invoke-virtual {p0}, Lcom/twitter/android/MentionTimelineFragment;->au()Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/twitter/library/client/aa;->a(J)Lcom/twitter/library/client/Session;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/MentionTimelineFragment;->a:Lcom/twitter/android/util/w;

    iget v2, p0, Lcom/twitter/android/MentionTimelineFragment;->F:I

    invoke-static {v2}, Lcom/twitter/android/util/w;->a(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/twitter/android/util/w;->a(IJLjava/lang/String;)V

    return-void
.end method

.method public a(Landroid/view/View;Lcom/twitter/library/provider/Tweet;Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/TimelineFragment;->a(Landroid/view/View;Lcom/twitter/library/provider/Tweet;Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/MentionTimelineFragment;->a:Lcom/twitter/android/util/w;

    iget-wide v1, p2, Lcom/twitter/library/provider/Tweet;->h:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/util/w;->a(J)V

    return-void
.end method

.method public bridge synthetic a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V
    .locals 0

    check-cast p2, Lcom/twitter/library/provider/Tweet;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/MentionTimelineFragment;->a(Landroid/view/View;Lcom/twitter/library/provider/Tweet;Landroid/os/Bundle;)V

    return-void
.end method

.method protected a(Z)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/android/TimelineFragment;->a(Z)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MentionTimelineFragment;->a:Lcom/twitter/android/util/w;

    invoke-virtual {v0}, Lcom/twitter/android/util/w;->c()V

    :cond_0
    return-void
.end method

.method public a(Landroid/widget/AbsListView;III)Z
    .locals 1

    if-nez p2, :cond_0

    if-lez p3, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MentionTimelineFragment;->a:Lcom/twitter/android/util/w;

    invoke-virtual {v0}, Lcom/twitter/android/util/w;->b()V

    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/android/TimelineFragment;->a(Landroid/widget/AbsListView;III)Z

    move-result v0

    return v0
.end method

.method protected d()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/TimelineFragment;->d()V

    iget-object v0, p0, Lcom/twitter/android/MentionTimelineFragment;->a:Lcom/twitter/android/util/w;

    invoke-virtual {v0}, Lcom/twitter/android/util/w;->c()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6

    invoke-super {p0, p1, p2}, Lcom/twitter/android/TimelineFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/MentionTimelineFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v5

    new-instance v0, Lcom/twitter/android/util/w;

    invoke-virtual {p0}, Lcom/twitter/android/MentionTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget v2, p0, Lcom/twitter/android/MentionTimelineFragment;->F:I

    invoke-static {v2}, Lcom/twitter/android/util/w;->a(I)I

    move-result v2

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/util/w;-><init>(Landroid/content/Context;IJLjava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/MentionTimelineFragment;->a:Lcom/twitter/android/util/w;

    return-void
.end method
