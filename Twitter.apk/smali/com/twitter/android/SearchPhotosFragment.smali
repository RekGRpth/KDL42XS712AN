.class public Lcom/twitter/android/SearchPhotosFragment;
.super Lcom/twitter/android/SearchFragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/util/ar;


# instance fields
.field private J:Ljava/lang/String;

.field private K:Lcom/twitter/library/util/aa;

.field private L:Z

.field private final U:Ljava/util/ArrayList;

.field private final V:Ljava/util/HashSet;

.field z:Lcom/twitter/android/oi;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/SearchFragment;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->U:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->V:Ljava/util/HashSet;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/SearchPhotosFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/SearchPhotosFragment;)Ljava/util/HashSet;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->V:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/SearchPhotosFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/SearchPhotosFragment;)Lcom/twitter/library/scribe/ScribeAssociation;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/SearchPhotosFragment;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->U:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public L()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method protected M()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "photo_grid"

    return-object v0
.end method

.method protected N()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/SearchPhotosFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":photo_grid:::impression"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchPhotosFragment;->b(Ljava/lang/String;)V

    return-void
.end method

.method protected O()V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->U:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/SearchPhotosFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":photo_grid:stream::results"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/SearchPhotosFragment;->U:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/util/ArrayList;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/SearchPhotosFragment;->c:Ljava/lang/String;

    const/4 v3, 0x3

    invoke-static {v3}, Lcom/twitter/android/SearchPhotosFragment;->d_(I)Ljava/lang/String;

    move-result-object v3

    iget-boolean v4, p0, Lcom/twitter/android/SearchPhotosFragment;->j:Z

    iget-boolean v5, p0, Lcom/twitter/android/SearchPhotosFragment;->i:Z

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    iget-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->U:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method protected P()Z
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    const/4 v0, 0x1

    return v0
.end method

.method protected Q()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->K:Lcom/twitter/library/util/aa;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->f(Landroid/content/Context;)Lcom/twitter/library/util/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->K:Lcom/twitter/library/util/aa;

    iget-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->K:Lcom/twitter/library/util/aa;

    invoke-virtual {v0, p0}, Lcom/twitter/library/util/aa;->a(Lcom/twitter/library/util/ac;)V

    :cond_0
    return-void
.end method

.method protected R()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->K:Lcom/twitter/library/util/aa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->K:Lcom/twitter/library/util/aa;

    invoke-virtual {v0, p0}, Lcom/twitter/library/util/aa;->b(Lcom/twitter/library/util/ac;)V

    :cond_0
    return-void
.end method

.method protected a(Landroid/content/Context;)V
    .locals 13

    new-instance v0, Lcom/twitter/library/api/search/c;

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    iget-wide v3, p0, Lcom/twitter/android/SearchPhotosFragment;->q:J

    iget-object v5, p0, Lcom/twitter/android/SearchPhotosFragment;->c:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->G()I

    move-result v6

    iget-object v7, p0, Lcom/twitter/android/SearchPhotosFragment;->d:Ljava/lang/String;

    iget-object v8, p0, Lcom/twitter/android/SearchPhotosFragment;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/SearchPhotosFragment;->z:Lcom/twitter/android/oi;

    invoke-virtual {v1}, Lcom/twitter/android/oi;->b()J

    move-result-wide v9

    iget v1, p0, Lcom/twitter/android/SearchPhotosFragment;->t:I

    int-to-long v11, v1

    sub-long/2addr v9, v11

    long-to-int v9, v9

    const/4 v10, 0x1

    iget-object v11, p0, Lcom/twitter/android/SearchPhotosFragment;->g:Ljava/lang/String;

    move-object v1, p1

    invoke-direct/range {v0 .. v11}, Lcom/twitter/library/api/search/c;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/twitter/android/SearchPhotosFragment;->j:Z

    iget-boolean v3, p0, Lcom/twitter/android/SearchPhotosFragment;->k:Z

    iget-boolean v4, p0, Lcom/twitter/android/SearchPhotosFragment;->l:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/library/api/search/c;->a(IZZZ)Lcom/twitter/library/api/search/c;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/search/c;->c(Z)Lcom/twitter/library/service/b;

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->c(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    iget-boolean v2, p0, Lcom/twitter/android/SearchPhotosFragment;->i:Z

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->d(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    :cond_0
    const/4 v1, 0x1

    const/4 v2, 0x2

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/SearchPhotosFragment;->a(Lcom/twitter/library/service/b;II)Z

    return-void
.end method

.method protected a(Landroid/content/Context;IILcom/twitter/library/service/b;)V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x3

    const/4 v4, 0x1

    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/android/SearchFragment;->a(Landroid/content/Context;IILcom/twitter/library/service/b;)V

    const/4 v0, 0x2

    if-ne p2, v0, :cond_1

    iget-object v0, p4, Lcom/twitter/library/service/b;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchPhotosFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    check-cast p4, Lcom/twitter/library/api/search/c;

    invoke-virtual {p4}, Lcom/twitter/library/api/search/c;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-nez v0, :cond_2

    const v0, 0x7f0f03b8    # com.twitter.android.R.string.search_status_fetch_error

    invoke-static {p1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0, p3}, Lcom/twitter/android/SearchPhotosFragment;->b(I)V

    :cond_0
    :goto_0
    invoke-virtual {p4}, Lcom/twitter/library/api/search/c;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/SearchPhotosFragment;->y:J

    :cond_1
    return-void

    :cond_2
    invoke-virtual {p4}, Lcom/twitter/library/api/search/c;->e()I

    move-result v0

    if-nez v0, :cond_5

    if-ne p3, v6, :cond_4

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v2, v4, [Ljava/lang/String;

    new-array v3, v4, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/twitter/android/SearchPhotosFragment;->h:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ":photo_grid:stream::no_results"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v3}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/SearchPhotosFragment;->c:Ljava/lang/String;

    invoke-static {v6}, Lcom/twitter/android/SearchPhotosFragment;->d_(I)Ljava/lang/String;

    move-result-object v3

    iget-boolean v4, p0, Lcom/twitter/android/SearchPhotosFragment;->j:Z

    iget-boolean v5, p0, Lcom/twitter/android/SearchPhotosFragment;->i:Z

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_3
    :goto_1
    invoke-virtual {p0, p3}, Lcom/twitter/android/SearchPhotosFragment;->b(I)V

    goto :goto_0

    :cond_4
    if-ne p3, v4, :cond_3

    iput-boolean v4, p0, Lcom/twitter/android/SearchPhotosFragment;->r:Z

    goto :goto_1

    :cond_5
    iget-boolean v0, p0, Lcom/twitter/android/SearchPhotosFragment;->w:Z

    if-eqz v0, :cond_0

    if-ne p3, v6, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->r()V

    goto :goto_0
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x3

    iget v0, p0, Lcom/twitter/android/SearchPhotosFragment;->n:I

    iget-boolean v1, p0, Lcom/twitter/android/SearchPhotosFragment;->L:Z

    if-eqz v1, :cond_1

    if-ne v0, v4, :cond_0

    new-instance v1, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v1}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeAssociation;->a(I)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/SearchPhotosFragment;->Q:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeAssociation;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/SearchPhotosFragment;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v1

    const-string/jumbo v2, "photo_grid"

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchPhotosFragment;->b(Lcom/twitter/library/scribe/ScribeAssociation;)V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->q()V

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->A()Lcom/twitter/refresh/widget/a;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/SearchPhotosFragment;->z:Lcom/twitter/android/oi;

    invoke-virtual {v2, p2}, Lcom/twitter/android/oi;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    iget-boolean v2, p0, Lcom/twitter/android/SearchPhotosFragment;->L:Z

    if-eqz v2, :cond_3

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchPhotosFragment;->b(I)V

    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    invoke-virtual {p0, v1, v5}, Lcom/twitter/android/SearchPhotosFragment;->a(Lcom/twitter/refresh/widget/a;Z)V

    iput v4, p0, Lcom/twitter/android/SearchPhotosFragment;->n:I

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->z:Lcom/twitter/android/oi;

    invoke-virtual {v0}, Lcom/twitter/android/oi;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0, v4}, Lcom/twitter/android/SearchPhotosFragment;->d(I)Z

    :cond_4
    iput-boolean v5, p0, Lcom/twitter/android/SearchPhotosFragment;->L:Z

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/util/aa;Ljava/util/HashMap;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->z:Lcom/twitter/android/oi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->z:Lcom/twitter/android/oi;

    invoke-virtual {v0, p2}, Lcom/twitter/android/oi;->a(Ljava/util/HashMap;)V

    :cond_0
    return-void
.end method

.method protected a(Z)V
    .locals 3

    const/4 v2, 0x5

    invoke-super {p0, p1}, Lcom/twitter/android/SearchFragment;->a(Z)V

    iget-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->z:Lcom/twitter/android/oi;

    if-eqz p1, :cond_1

    invoke-virtual {p0, v2}, Lcom/twitter/android/SearchPhotosFragment;->a_(I)V

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->P()Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/twitter/android/oi;->a()Landroid/database/Cursor;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-virtual {p0, v2}, Lcom/twitter/android/SearchPhotosFragment;->a_(I)V

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->p()V

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/twitter/android/oi;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchPhotosFragment;->d(I)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/SearchPhotosFragment;->L:Z

    goto :goto_0
.end method

.method protected b(J)I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->z:Lcom/twitter/android/oi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->z:Lcom/twitter/android/oi;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/oi;->a(J)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(Ljava/lang/String;)V
    .locals 6

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    iget-object v2, p0, Lcom/twitter/android/SearchPhotosFragment;->J:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->h(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/SearchPhotosFragment;->c:Ljava/lang/String;

    const/4 v3, 0x3

    invoke-static {v3}, Lcom/twitter/android/SearchPhotosFragment;->d_(I)Ljava/lang/String;

    move-result-object v3

    iget-boolean v4, p0, Lcom/twitter/android/SearchPhotosFragment;->j:Z

    iget-boolean v5, p0, Lcom/twitter/android/SearchPhotosFragment;->i:Z

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/SearchPhotosFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method protected d(I)Z
    .locals 13

    invoke-virtual {p0, p1}, Lcom/twitter/android/SearchPhotosFragment;->c_(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput p1, p0, Lcom/twitter/android/SearchPhotosFragment;->n:I

    invoke-virtual {p0, p1}, Lcom/twitter/android/SearchPhotosFragment;->a_(I)V

    iget-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->z:Lcom/twitter/android/oi;

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid fetch type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/4 v10, 0x0

    const/4 v9, 0x0

    :goto_1
    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    iget-wide v1, p0, Lcom/twitter/android/SearchPhotosFragment;->Q:J

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/SearchPhotosFragment;->h:Ljava/lang/String;

    const-string/jumbo v4, "photo_grid"

    invoke-static {v3, v4, p1}, Lcom/twitter/android/SearchPhotosFragment;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/SearchPhotosFragment;->c:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-static {v2}, Lcom/twitter/android/SearchPhotosFragment;->d_(I)Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, p0, Lcom/twitter/android/SearchPhotosFragment;->j:Z

    iget-boolean v4, p0, Lcom/twitter/android/SearchPhotosFragment;->i:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v12

    new-instance v0, Lcom/twitter/library/api/search/c;

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    iget-wide v3, p0, Lcom/twitter/android/SearchPhotosFragment;->q:J

    iget-object v5, p0, Lcom/twitter/android/SearchPhotosFragment;->c:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->G()I

    move-result v6

    iget-object v7, p0, Lcom/twitter/android/SearchPhotosFragment;->d:Ljava/lang/String;

    iget-object v8, p0, Lcom/twitter/android/SearchPhotosFragment;->b:Ljava/lang/String;

    iget-object v11, p0, Lcom/twitter/android/SearchPhotosFragment;->g:Ljava/lang/String;

    invoke-direct/range {v0 .. v11}, Lcom/twitter/library/api/search/c;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/twitter/android/SearchPhotosFragment;->j:Z

    iget-boolean v3, p0, Lcom/twitter/android/SearchPhotosFragment;->k:Z

    iget-boolean v4, p0, Lcom/twitter/android/SearchPhotosFragment;->l:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/library/api/search/c;->a(IZZZ)Lcom/twitter/library/api/search/c;

    move-result-object v0

    const-string/jumbo v1, "scribe_log"

    invoke-virtual {v0, v1, v12}, Lcom/twitter/library/api/search/c;->a(Ljava/lang/String;Landroid/os/Parcelable;)Lcom/twitter/library/service/b;

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->c(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    iget-boolean v2, p0, Lcom/twitter/android/SearchPhotosFragment;->i:Z

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->d(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1, p1}, Lcom/twitter/android/SearchPhotosFragment;->a(Lcom/twitter/library/service/b;II)Z

    const/4 v0, 0x1

    goto/16 :goto_0

    :pswitch_1
    const/4 v10, 0x1

    invoke-virtual {v0}, Lcom/twitter/android/oi;->b()J

    move-result-wide v0

    long-to-int v9, v0

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->ak()V

    goto :goto_1

    :pswitch_2
    const/4 v10, 0x2

    invoke-virtual {v0}, Lcom/twitter/android/oi;->c()J

    move-result-wide v0

    long-to-int v9, v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected o()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->z:Lcom/twitter/android/oi;

    invoke-virtual {v0}, Lcom/twitter/android/oi;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 9

    invoke-super {p0, p1}, Lcom/twitter/android/SearchFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "scribe_context"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->J:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->z:Lcom/twitter/android/oi;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance v0, Lcom/twitter/android/oi;

    iget-object v2, p0, Lcom/twitter/android/SearchPhotosFragment;->K:Lcom/twitter/library/util/aa;

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0009    # com.twitter.android.R.integer.photo_target_ratio

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    int-to-float v3, v3

    sget v4, Lcom/twitter/library/provider/bs;->d:I

    const v5, 0x7fffffff

    new-instance v6, Lcom/twitter/android/sf;

    invoke-direct {v6, p0, v1}, Lcom/twitter/android/sf;-><init>(Lcom/twitter/android/SearchPhotosFragment;Landroid/content/Context;)V

    new-instance v7, Lcom/twitter/android/sg;

    const/4 v8, 0x0

    invoke-direct {v7, p0, v8}, Lcom/twitter/android/sg;-><init>(Lcom/twitter/android/SearchPhotosFragment;Lcom/twitter/android/sf;)V

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/oi;-><init>(Landroid/content/Context;Lcom/twitter/library/util/aa;FIILandroid/view/View$OnClickListener;Lcom/twitter/android/ob;)V

    iput-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->z:Lcom/twitter/android/oi;

    iget-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->z:Lcom/twitter/android/oi;

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchPhotosFragment;->a(Lcom/twitter/android/client/av;)Lcom/twitter/android/client/BaseListFragment;

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/SearchPhotosFragment;->z:Lcom/twitter/android/oi;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 9

    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/provider/aq;->a:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/library/provider/bs;->a:[Ljava/lang/String;

    const-string/jumbo v4, "flags&1|512!=0 AND search_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-wide v7, p0, Lcom/twitter/android/SearchPhotosFragment;->q:J

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const-string/jumbo v6, "type_id ASC, _id ASC"

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->z:Lcom/twitter/android/oi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->z:Lcom/twitter/android/oi;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/oi;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/SearchFragment;->onDestroy()V

    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/SearchPhotosFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->z:Lcom/twitter/android/oi;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/oi;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    return-void
.end method

.method protected p()V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method

.method protected u()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected y()V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/SearchPhotosFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/SearchPhotosFragment;->q:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->f(J)Ljava/lang/String;

    return-void
.end method

.method protected z()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->z:Lcom/twitter/android/oi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/SearchPhotosFragment;->z:Lcom/twitter/android/oi;

    invoke-virtual {v0}, Lcom/twitter/android/oi;->getCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
