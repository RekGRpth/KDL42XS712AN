.class Lcom/twitter/android/tc;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/SettingsActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/SettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/tc;->a:Lcom/twitter/android/SettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/tc;->a:Lcom/twitter/android/SettingsActivity;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/tc;->a:Lcom/twitter/android/SettingsActivity;

    invoke-virtual {v2}, Lcom/twitter/android/SettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/twitter/android/ExperimentSettingsActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/SettingsActivity;->startActivity(Landroid/content/Intent;)V

    const/4 v0, 0x0

    return v0
.end method
