.class public Lcom/twitter/android/ay;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/BaseEditProfileActivity;

.field private final b:Landroid/content/Context;

.field private final c:Landroid/content/Intent;

.field private final d:Landroid/app/ProgressDialog;

.field private final e:Ljava/lang/String;

.field private final f:J


# direct methods
.method constructor <init>(Lcom/twitter/android/BaseEditProfileActivity;)V
    .locals 2

    iput-object p1, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ay;->c:Landroid/content/Intent;

    iput-object p1, p0, Lcom/twitter/android/ay;->b:Landroid/content/Context;

    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/twitter/android/ay;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    const v1, 0x7f0f0331    # com.twitter.android.R.string.profile_updating

    invoke-virtual {p1, v1}, Lcom/twitter/android/BaseEditProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iput-object v0, p0, Lcom/twitter/android/ay;->d:Landroid/app/ProgressDialog;

    invoke-static {p1}, Lcom/twitter/android/BaseEditProfileActivity;->a(Lcom/twitter/android/BaseEditProfileActivity;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/ay;->f:J

    iget-object v0, p1, Lcom/twitter/android/BaseEditProfileActivity;->q:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/ay;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iget-object v0, v0, Lcom/twitter/android/BaseEditProfileActivity;->e:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ay;->b:Landroid/content/Context;

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iget-object v2, v2, Lcom/twitter/android/BaseEditProfileActivity;->e:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/twitter/android/ay;->f:J

    invoke-static {v0, v2, v3}, Lcom/twitter/library/util/Util;->c(Landroid/content/Context;J)Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iput-object v4, v0, Lcom/twitter/android/BaseEditProfileActivity;->e:Landroid/net/Uri;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_0
    :goto_0
    return-object v4

    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_2
    invoke-virtual {v1, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v1, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v1, Lcom/twitter/android/BaseEditProfileActivity;->e:Landroid/net/Uri;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iput-object v4, v0, Lcom/twitter/android/BaseEditProfileActivity;->e:Landroid/net/Uri;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method

.method protected a(Ljava/lang/Void;)V
    .locals 13

    const/4 v12, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    iget-object v0, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    invoke-static {v0}, Lcom/twitter/android/BaseEditProfileActivity;->b(Lcom/twitter/android/BaseEditProfileActivity;)Lcom/twitter/android/client/c;

    move-result-object v8

    iget-object v0, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    invoke-static {v0}, Lcom/twitter/android/BaseEditProfileActivity;->c(Lcom/twitter/android/BaseEditProfileActivity;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ay;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v9

    iget-object v0, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iget-object v0, v0, Lcom/twitter/android/BaseEditProfileActivity;->p:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iget-object v0, v0, Lcom/twitter/android/BaseEditProfileActivity;->e:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ay;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/ay;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/android/ProfileFragment;->b(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/ay;->c:Landroid/content/Intent;

    const-string/jumbo v1, "update_header"

    invoke-virtual {v0, v1, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-wide v0, p0, Lcom/twitter/android/ay;->f:J

    new-array v2, v11, [Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iget-object v3, v3, Lcom/twitter/android/BaseEditProfileActivity;->a:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v4, ""

    const-string/jumbo v6, "header_image"

    const-string/jumbo v7, "add"

    invoke-static {v3, v4, v6, v7}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v10

    invoke-virtual {v8, v0, v1, v2}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    invoke-virtual {v0}, Lcom/twitter/android/BaseEditProfileActivity;->m()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    invoke-virtual {v0}, Lcom/twitter/android/BaseEditProfileActivity;->f()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    invoke-virtual {v0}, Lcom/twitter/android/BaseEditProfileActivity;->g()Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    invoke-virtual {v0}, Lcom/twitter/android/BaseEditProfileActivity;->h()Ljava/lang/String;

    move-result-object v7

    new-instance v0, Lcom/twitter/library/client/v;

    iget-object v1, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iget-object v1, v1, Lcom/twitter/android/BaseEditProfileActivity;->c:Landroid/net/Uri;

    iget-object v2, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iget-object v2, v2, Lcom/twitter/android/BaseEditProfileActivity;->e:Landroid/net/Uri;

    iget-object v3, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iget-boolean v3, v3, Lcom/twitter/android/BaseEditProfileActivity;->g:Z

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/client/v;-><init>(Landroid/net/Uri;Landroid/net/Uri;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/ay;->b:Landroid/content/Context;

    invoke-static {v1, v9, v0}, Lcom/twitter/android/client/cd;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/v;)Ljava/lang/String;

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    invoke-virtual {v0}, Lcom/twitter/android/BaseEditProfileActivity;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-wide v0, p0, Lcom/twitter/android/ay;->f:J

    new-array v2, v11, [Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iget-object v3, v3, Lcom/twitter/android/BaseEditProfileActivity;->a:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v4, ""

    const-string/jumbo v5, "bio"

    const-string/jumbo v6, "add"

    invoke-static {v3, v4, v5, v6}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v10

    invoke-virtual {v8, v0, v1, v2}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iget-object v0, v0, Lcom/twitter/android/BaseEditProfileActivity;->c:Landroid/net/Uri;

    if-eqz v0, :cond_3

    iget-wide v0, p0, Lcom/twitter/android/ay;->f:J

    new-array v2, v11, [Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iget-object v3, v3, Lcom/twitter/android/BaseEditProfileActivity;->a:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v4, ""

    const-string/jumbo v5, "avatar"

    const-string/jumbo v6, "add"

    invoke-static {v3, v4, v5, v6}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v10

    invoke-virtual {v8, v0, v1, v2}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iget-boolean v0, v0, Lcom/twitter/android/BaseEditProfileActivity;->g:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iget-boolean v0, v0, Lcom/twitter/android/BaseEditProfileActivity;->h:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/ay;->c:Landroid/content/Intent;

    const-string/jumbo v1, "remove_header"

    invoke-virtual {v0, v1, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-wide v0, p0, Lcom/twitter/android/ay;->f:J

    new-array v2, v11, [Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iget-object v3, v3, Lcom/twitter/android/BaseEditProfileActivity;->a:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v4, ""

    const-string/jumbo v5, "header_image"

    const-string/jumbo v6, "remove"

    invoke-static {v3, v4, v5, v6}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v10

    invoke-virtual {v8, v0, v1, v2}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iput-boolean v10, v0, Lcom/twitter/android/BaseEditProfileActivity;->g:Z

    iget-object v0, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iput-boolean v10, v0, Lcom/twitter/android/BaseEditProfileActivity;->h:Z

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iput-object v12, v0, Lcom/twitter/android/BaseEditProfileActivity;->c:Landroid/net/Uri;

    iget-object v0, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iput-object v12, v0, Lcom/twitter/android/BaseEditProfileActivity;->b:Landroid/net/Uri;

    iget-object v0, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iput-object v12, v0, Lcom/twitter/android/BaseEditProfileActivity;->e:Landroid/net/Uri;

    iget-object v0, p0, Lcom/twitter/android/ay;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    iget-object v0, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iget-object v1, p0, Lcom/twitter/android/ay;->c:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lcom/twitter/android/BaseEditProfileActivity;->a(Landroid/content/Intent;)V

    return-void

    :cond_5
    iget-object v0, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    invoke-virtual {v0}, Lcom/twitter/android/BaseEditProfileActivity;->g_()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/twitter/library/client/v;

    iget-object v1, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iget-object v1, v1, Lcom/twitter/android/BaseEditProfileActivity;->c:Landroid/net/Uri;

    iget-object v2, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iget-object v2, v2, Lcom/twitter/android/BaseEditProfileActivity;->e:Landroid/net/Uri;

    iget-object v3, p0, Lcom/twitter/android/ay;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iget-boolean v3, v3, Lcom/twitter/android/BaseEditProfileActivity;->g:Z

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/client/v;-><init>(Landroid/net/Uri;Landroid/net/Uri;Z)V

    iget-object v1, p0, Lcom/twitter/android/ay;->b:Landroid/content/Context;

    invoke-static {v1, v9, v0}, Lcom/twitter/android/client/cd;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/v;)Ljava/lang/String;

    goto/16 :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/ay;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/ay;->a(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ay;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    return-void
.end method
