.class public Lcom/twitter/android/MessagesDetailActivity;
.super Lcom/twitter/android/BaseMessagesActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/md;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/BaseMessagesActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Intent;)V
    .locals 0

    return-void
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 6

    invoke-super {p0, p1, p2}, Lcom/twitter/android/BaseMessagesActivity;->a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V

    invoke-virtual {p0}, Lcom/twitter/android/MessagesDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-nez p1, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    :goto_0
    const-string/jumbo v1, "user_id"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v0, p1}, Lcom/twitter/android/MessagesDetailActivity;->a(Landroid/os/Bundle;Landroid/os/Bundle;)V

    const-string/jumbo v2, "user_fullname"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "user_name"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/MessagesDetailActivity;->a(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method

.method public a_(Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/MessagesDetailActivity;->finish()V

    return-void
.end method
