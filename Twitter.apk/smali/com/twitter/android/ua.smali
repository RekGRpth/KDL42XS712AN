.class Lcom/twitter/android/ua;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/widget/TabHost$TabContentFactory;


# instance fields
.field private final a:Lcom/twitter/android/client/BaseFragmentActivity;


# direct methods
.method public constructor <init>(Lcom/twitter/android/client/BaseFragmentActivity;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/ua;->a:Lcom/twitter/android/client/BaseFragmentActivity;

    return-void
.end method


# virtual methods
.method public createTabContent(Ljava/lang/String;)Landroid/view/View;
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/ua;->a:Lcom/twitter/android/client/BaseFragmentActivity;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setMinimumWidth(I)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setMinimumHeight(I)V

    return-object v0
.end method
