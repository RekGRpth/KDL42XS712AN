.class Lcom/twitter/android/dm;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/widget/k;


# instance fields
.field final synthetic a:Lcom/twitter/library/widget/PageableListView;

.field final synthetic b:Lcom/twitter/android/DMConversationFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/DMConversationFragment;Lcom/twitter/library/widget/PageableListView;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/dm;->b:Lcom/twitter/android/DMConversationFragment;

    iput-object p2, p0, Lcom/twitter/android/dm;->a:Lcom/twitter/library/widget/PageableListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/widget/AbsListView;)V
    .locals 5

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/twitter/android/dm;->b:Lcom/twitter/android/DMConversationFragment;

    invoke-static {v0}, Lcom/twitter/android/DMConversationFragment;->a(Lcom/twitter/android/DMConversationFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/library/api/conversations/m;

    iget-object v1, p0, Lcom/twitter/android/dm;->b:Lcom/twitter/android/DMConversationFragment;

    invoke-virtual {v1}, Lcom/twitter/android/DMConversationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/dm;->b:Lcom/twitter/android/DMConversationFragment;

    invoke-static {v2}, Lcom/twitter/android/DMConversationFragment;->b(Lcom/twitter/android/DMConversationFragment;)Lcom/twitter/library/client/Session;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/dm;->b:Lcom/twitter/android/DMConversationFragment;

    invoke-static {v3}, Lcom/twitter/android/DMConversationFragment;->c(Lcom/twitter/android/DMConversationFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/api/conversations/m;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/dm;->a:Lcom/twitter/library/widget/PageableListView;

    invoke-virtual {v1, v4}, Lcom/twitter/library/widget/PageableListView;->a(Z)V

    iget-object v1, p0, Lcom/twitter/android/dm;->b:Lcom/twitter/android/DMConversationFragment;

    invoke-static {v1, v4}, Lcom/twitter/android/DMConversationFragment;->a(Lcom/twitter/android/DMConversationFragment;Z)Z

    iget-object v1, p0, Lcom/twitter/android/dm;->b:Lcom/twitter/android/DMConversationFragment;

    const/16 v2, 0x8

    const/4 v3, 0x0

    invoke-static {v1, v0, v2, v3}, Lcom/twitter/android/DMConversationFragment;->a(Lcom/twitter/android/DMConversationFragment;Lcom/twitter/library/service/b;II)Z

    :cond_0
    return-void
.end method

.method public b(Landroid/widget/AbsListView;)V
    .locals 0

    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    return-void
.end method
