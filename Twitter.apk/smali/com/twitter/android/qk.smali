.class Lcom/twitter/android/qk;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Ljava/util/ArrayList;

.field final synthetic b:Landroid/content/Context;

.field final synthetic c:Z

.field final synthetic d:Z

.field final synthetic e:Z

.field final synthetic f:Lcom/twitter/android/ProfileFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/ProfileFragment;Ljava/util/ArrayList;Landroid/content/Context;ZZZ)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    iput-object p2, p0, Lcom/twitter/android/qk;->a:Ljava/util/ArrayList;

    iput-object p3, p0, Lcom/twitter/android/qk;->b:Landroid/content/Context;

    iput-boolean p4, p0, Lcom/twitter/android/qk;->c:Z

    iput-boolean p5, p0, Lcom/twitter/android/qk;->d:Z

    iput-boolean p6, p0, Lcom/twitter/android/qk;->e:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8

    const/16 v2, 0x200

    const/4 v6, 0x1

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/twitter/android/qk;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ProfileFragment$DialogMoreButtons;

    sget-object v1, Lcom/twitter/android/qf;->a:[I

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment$DialogMoreButtons;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->f(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    invoke-static {v1}, Lcom/twitter/android/ProfileFragment;->e(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "profile:::message:click"

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/conversations/ae;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/qk;->b:Landroid/content/Context;

    const-class v2, Lcom/twitter/android/DMConversationActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "user_ids"

    new-array v2, v6, [J

    iget-object v3, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    iget-wide v3, v3, Lcom/twitter/android/ProfileFragment;->I:J

    aput-wide v3, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v1, v0}, Lcom/twitter/android/ProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/qk;->b:Landroid/content/Context;

    const-class v2, Lcom/twitter/android/MessagesActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "user_name"

    iget-object v2, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    iget-object v2, v2, Lcom/twitter/android/ProfileFragment;->B:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "user_fullname"

    iget-object v2, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    invoke-static {v2}, Lcom/twitter/android/ProfileFragment;->g(Lcom/twitter/android/ProfileFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "user_id"

    iget-object v2, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    iget-wide v2, v2, Lcom/twitter/android/ProfileFragment;->I:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string/jumbo v1, "owner_id"

    iget-object v2, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    invoke-static {v2}, Lcom/twitter/android/ProfileFragment;->h(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string/jumbo v1, "user_profile_image"

    iget-object v2, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    iget-object v2, v2, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    iget-object v2, v2, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "keyboard_open"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v1, v0}, Lcom/twitter/android/ProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_1
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    iget-boolean v0, p0, Lcom/twitter/android/qk;->c:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    iget-object v1, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    iget v1, v1, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v1, v2}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v1

    iput v1, v0, Lcom/twitter/android/ProfileFragment;->H:I

    :goto_1
    iget-object v1, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    iget-object v0, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->j(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/client/c;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->i(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-object v0, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    iget-object v4, v0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    iget-boolean v0, p0, Lcom/twitter/android/qk;->c:Z

    if-nez v0, :cond_3

    move v0, v6

    :goto_2
    invoke-virtual {v2, v3, v4, v0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/TwitterUser;Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    iget-object v1, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    iget v1, v1, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v1, v2}, Lcom/twitter/library/provider/ay;->b(II)I

    move-result v1

    iput v1, v0, Lcom/twitter/android/ProfileFragment;->H:I

    goto :goto_1

    :cond_3
    move v0, v7

    goto :goto_2

    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/qk;->b:Landroid/content/Context;

    const-class v3, Lcom/twitter/android/ListsActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "android.intent.action.PICK"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "owner_id"

    iget-object v3, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    invoke-static {v3}, Lcom/twitter/android/ProfileFragment;->k(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "type"

    invoke-virtual {v1, v2, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "inquire_user_id"

    iget-object v3, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    iget-wide v3, v3, Lcom/twitter/android/ProfileFragment;->I:J

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Lcom/twitter/android/ProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V

    iget-object v0, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    const-string/jumbo v1, "profile::::add_to_list"

    iget-object v2, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    iget-wide v2, v2, Lcom/twitter/android/ProfileFragment;->I:J

    iget-object v4, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    iget-object v4, v4, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/library/api/PromotedContent;

    iget-object v5, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    iget-object v6, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    iget-object v6, v6, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-static {v5, v6}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/android/ProfileFragment;Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    invoke-static {v6}, Lcom/twitter/android/ProfileFragment;->l(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    goto/16 :goto_0

    :pswitch_3
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    iget-boolean v0, p0, Lcom/twitter/android/qk;->d:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->n(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    invoke-static {v1}, Lcom/twitter/android/ProfileFragment;->m(Lcom/twitter/android/ProfileFragment;)J

    move-result-wide v1

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "profile::tweet:unmute_dialog:open"

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->p(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    invoke-static {v1}, Lcom/twitter/android/ProfileFragment;->o(Lcom/twitter/android/ProfileFragment;)J

    move-result-wide v1

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "profile::tweet:unmute_dialog:unmute_user"

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->q(Lcom/twitter/android/ProfileFragment;)V

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->s(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    invoke-static {v1}, Lcom/twitter/android/ProfileFragment;->r(Lcom/twitter/android/ProfileFragment;)J

    move-result-wide v1

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "profile::tweet:mute_dialog:open"

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    iget-object v1, v1, Lcom/twitter/android/ProfileFragment;->B:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    iget v2, v2, Lcom/twitter/android/ProfileFragment;->H:I

    const/16 v3, 0xc

    iget-object v4, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v4}, Lcom/twitter/android/ProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/util/af;->a(Landroid/content/Context;Ljava/lang/String;IILandroid/support/v4/app/FragmentManager;Landroid/support/v4/app/Fragment;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->u(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    invoke-static {v1}, Lcom/twitter/android/ProfileFragment;->t(Lcom/twitter/android/ProfileFragment;)J

    move-result-wide v1

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "profile::tweet:mute_dialog:mute_user"

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->v(Lcom/twitter/android/ProfileFragment;)V

    goto/16 :goto_0

    :pswitch_4
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    iget-boolean v0, p0, Lcom/twitter/android/qk;->e:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/twitter/android/ProfileFragment;->f_(I)V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    iget-boolean v0, v0, Lcom/twitter/android/ProfileFragment;->L:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/library/api/PromotedContent;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_3
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v2}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/twitter/android/ReportTweetActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "spammer_id"

    iget-object v3, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    iget-wide v3, v3, Lcom/twitter/android/ProfileFragment;->I:J

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "spammer_username"

    iget-object v3, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    iget-object v3, v3, Lcom/twitter/android/ProfileFragment;->B:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "association"

    iget-object v3, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    invoke-static {v3}, Lcom/twitter/android/ProfileFragment;->l(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "pc_impression_id"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "pc_earned"

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "friendship"

    iget-object v2, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    iget v2, v2, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/ProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v0}, Lcom/twitter/library/api/PromotedContent;->b()Z

    move-result v7

    iget-object v0, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/library/api/PromotedContent;

    iget-object v0, v0, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    goto :goto_3

    :cond_7
    iget-object v0, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/twitter/android/ProfileFragment;->f_(I)V

    goto/16 :goto_0

    :pswitch_5
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    iget-object v0, p0, Lcom/twitter/android/qk;->f:Lcom/twitter/android/ProfileFragment;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/android/ProfileFragment;->f_(I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
