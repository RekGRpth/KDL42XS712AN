.class Lcom/twitter/android/sk;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Lcom/twitter/android/yd;

.field public final b:Lcom/twitter/android/zx;

.field public final c:Lcom/twitter/android/sl;

.field public final d:Landroid/support/v4/view/ViewPager;

.field public final e:Lcom/twitter/android/widget/PipView;

.field public final f:Landroid/widget/TextView;

.field public final g:Landroid/widget/TextView;

.field public final h:Lcom/twitter/internal/android/widget/HorizontalListView;

.field public final i:Landroid/view/View;

.field public final j:Lcom/twitter/android/widget/TopicView;

.field public final k:Lcom/twitter/android/widget/TextSwitcherView;

.field public l:Lcom/twitter/android/sp;


# direct methods
.method constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iput-object v0, p0, Lcom/twitter/android/sk;->b:Lcom/twitter/android/zx;

    iput-object v0, p0, Lcom/twitter/android/sk;->c:Lcom/twitter/android/sl;

    iput-object v0, p0, Lcom/twitter/android/sk;->d:Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/twitter/android/sk;->e:Lcom/twitter/android/widget/PipView;

    iput-object v0, p0, Lcom/twitter/android/sk;->f:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/sk;->g:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/sk;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    iput-object v0, p0, Lcom/twitter/android/sk;->i:Landroid/view/View;

    iput-object v0, p0, Lcom/twitter/android/sk;->j:Lcom/twitter/android/widget/TopicView;

    iput-object v0, p0, Lcom/twitter/android/sk;->k:Lcom/twitter/android/widget/TextSwitcherView;

    return-void
.end method

.method constructor <init>(Landroid/support/v4/view/ViewPager;Lcom/twitter/android/widget/PipView;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iput-object v0, p0, Lcom/twitter/android/sk;->b:Lcom/twitter/android/zx;

    iput-object v0, p0, Lcom/twitter/android/sk;->c:Lcom/twitter/android/sl;

    iput-object p1, p0, Lcom/twitter/android/sk;->d:Landroid/support/v4/view/ViewPager;

    iput-object p2, p0, Lcom/twitter/android/sk;->e:Lcom/twitter/android/widget/PipView;

    iput-object v0, p0, Lcom/twitter/android/sk;->f:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/sk;->g:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/sk;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    iput-object v0, p0, Lcom/twitter/android/sk;->i:Landroid/view/View;

    iput-object v0, p0, Lcom/twitter/android/sk;->j:Lcom/twitter/android/widget/TopicView;

    iput-object v0, p0, Lcom/twitter/android/sk;->k:Lcom/twitter/android/widget/TextSwitcherView;

    return-void
.end method

.method constructor <init>(Landroid/widget/TextView;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iput-object v0, p0, Lcom/twitter/android/sk;->b:Lcom/twitter/android/zx;

    iput-object v0, p0, Lcom/twitter/android/sk;->c:Lcom/twitter/android/sl;

    iput-object v0, p0, Lcom/twitter/android/sk;->d:Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/twitter/android/sk;->e:Lcom/twitter/android/widget/PipView;

    iput-object p1, p0, Lcom/twitter/android/sk;->f:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/sk;->g:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/sk;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    iput-object v0, p0, Lcom/twitter/android/sk;->i:Landroid/view/View;

    iput-object v0, p0, Lcom/twitter/android/sk;->j:Lcom/twitter/android/widget/TopicView;

    iput-object v0, p0, Lcom/twitter/android/sk;->k:Lcom/twitter/android/widget/TextSwitcherView;

    return-void
.end method

.method constructor <init>(Landroid/widget/TextView;Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iput-object v0, p0, Lcom/twitter/android/sk;->b:Lcom/twitter/android/zx;

    iput-object v0, p0, Lcom/twitter/android/sk;->c:Lcom/twitter/android/sl;

    iput-object v0, p0, Lcom/twitter/android/sk;->d:Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/twitter/android/sk;->e:Lcom/twitter/android/widget/PipView;

    iput-object p1, p0, Lcom/twitter/android/sk;->f:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/sk;->g:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/sk;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    iput-object p2, p0, Lcom/twitter/android/sk;->i:Landroid/view/View;

    iput-object v0, p0, Lcom/twitter/android/sk;->j:Lcom/twitter/android/widget/TopicView;

    iput-object v0, p0, Lcom/twitter/android/sk;->k:Lcom/twitter/android/widget/TextSwitcherView;

    return-void
.end method

.method constructor <init>(Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iput-object v0, p0, Lcom/twitter/android/sk;->b:Lcom/twitter/android/zx;

    iput-object v0, p0, Lcom/twitter/android/sk;->c:Lcom/twitter/android/sl;

    iput-object v0, p0, Lcom/twitter/android/sk;->d:Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/twitter/android/sk;->e:Lcom/twitter/android/widget/PipView;

    iput-object p1, p0, Lcom/twitter/android/sk;->f:Landroid/widget/TextView;

    iput-object p2, p0, Lcom/twitter/android/sk;->g:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/sk;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    iput-object v0, p0, Lcom/twitter/android/sk;->i:Landroid/view/View;

    iput-object v0, p0, Lcom/twitter/android/sk;->j:Lcom/twitter/android/widget/TopicView;

    iput-object v0, p0, Lcom/twitter/android/sk;->k:Lcom/twitter/android/widget/TextSwitcherView;

    return-void
.end method

.method constructor <init>(Lcom/twitter/android/widget/TextSwitcherView;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iput-object v0, p0, Lcom/twitter/android/sk;->b:Lcom/twitter/android/zx;

    iput-object v0, p0, Lcom/twitter/android/sk;->c:Lcom/twitter/android/sl;

    iput-object v0, p0, Lcom/twitter/android/sk;->d:Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/twitter/android/sk;->e:Lcom/twitter/android/widget/PipView;

    iput-object v0, p0, Lcom/twitter/android/sk;->f:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/sk;->g:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/sk;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    iput-object v0, p0, Lcom/twitter/android/sk;->i:Landroid/view/View;

    iput-object v0, p0, Lcom/twitter/android/sk;->j:Lcom/twitter/android/widget/TopicView;

    iput-object p1, p0, Lcom/twitter/android/sk;->k:Lcom/twitter/android/widget/TextSwitcherView;

    return-void
.end method

.method constructor <init>(Lcom/twitter/android/widget/TopicView;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iput-object v0, p0, Lcom/twitter/android/sk;->b:Lcom/twitter/android/zx;

    iput-object v0, p0, Lcom/twitter/android/sk;->c:Lcom/twitter/android/sl;

    iput-object v0, p0, Lcom/twitter/android/sk;->d:Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/twitter/android/sk;->e:Lcom/twitter/android/widget/PipView;

    iput-object v0, p0, Lcom/twitter/android/sk;->f:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/sk;->g:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/sk;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    iput-object v0, p0, Lcom/twitter/android/sk;->i:Landroid/view/View;

    iput-object p1, p0, Lcom/twitter/android/sk;->j:Lcom/twitter/android/widget/TopicView;

    iput-object v0, p0, Lcom/twitter/android/sk;->k:Lcom/twitter/android/widget/TextSwitcherView;

    return-void
.end method

.method constructor <init>(Lcom/twitter/android/yd;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iput-object v0, p0, Lcom/twitter/android/sk;->b:Lcom/twitter/android/zx;

    iput-object v0, p0, Lcom/twitter/android/sk;->c:Lcom/twitter/android/sl;

    iput-object v0, p0, Lcom/twitter/android/sk;->d:Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/twitter/android/sk;->e:Lcom/twitter/android/widget/PipView;

    iput-object v0, p0, Lcom/twitter/android/sk;->f:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/sk;->g:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/sk;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    iput-object v0, p0, Lcom/twitter/android/sk;->i:Landroid/view/View;

    iput-object v0, p0, Lcom/twitter/android/sk;->j:Lcom/twitter/android/widget/TopicView;

    iput-object v0, p0, Lcom/twitter/android/sk;->k:Lcom/twitter/android/widget/TextSwitcherView;

    return-void
.end method

.method constructor <init>(Lcom/twitter/android/zx;Lcom/twitter/android/sl;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iput-object p1, p0, Lcom/twitter/android/sk;->b:Lcom/twitter/android/zx;

    iput-object p2, p0, Lcom/twitter/android/sk;->c:Lcom/twitter/android/sl;

    iput-object v0, p0, Lcom/twitter/android/sk;->d:Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/twitter/android/sk;->e:Lcom/twitter/android/widget/PipView;

    iput-object v0, p0, Lcom/twitter/android/sk;->f:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/sk;->g:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/sk;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    iput-object v0, p0, Lcom/twitter/android/sk;->i:Landroid/view/View;

    iput-object v0, p0, Lcom/twitter/android/sk;->j:Lcom/twitter/android/widget/TopicView;

    iput-object v0, p0, Lcom/twitter/android/sk;->k:Lcom/twitter/android/widget/TextSwitcherView;

    return-void
.end method

.method constructor <init>(Lcom/twitter/internal/android/widget/HorizontalListView;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iput-object v0, p0, Lcom/twitter/android/sk;->b:Lcom/twitter/android/zx;

    iput-object v0, p0, Lcom/twitter/android/sk;->c:Lcom/twitter/android/sl;

    iput-object v0, p0, Lcom/twitter/android/sk;->d:Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/twitter/android/sk;->e:Lcom/twitter/android/widget/PipView;

    iput-object v0, p0, Lcom/twitter/android/sk;->f:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/sk;->g:Landroid/widget/TextView;

    iput-object p1, p0, Lcom/twitter/android/sk;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    iput-object v0, p0, Lcom/twitter/android/sk;->i:Landroid/view/View;

    iput-object v0, p0, Lcom/twitter/android/sk;->j:Lcom/twitter/android/widget/TopicView;

    iput-object v0, p0, Lcom/twitter/android/sk;->k:Lcom/twitter/android/widget/TextSwitcherView;

    return-void
.end method
