.class final Lcom/twitter/android/gl;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private a:Ljava/util/ArrayList;

.field private b:Ljava/util/LinkedList;

.field private c:I

.field private d:I


# direct methods
.method protected constructor <init>(I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x7

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/gl;->a:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/gl;->b:Ljava/util/LinkedList;

    iput p1, p0, Lcom/twitter/android/gl;->c:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/gl;->d:I

    return-void
.end method


# virtual methods
.method public a()Landroid/graphics/Bitmap;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/gl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/gl;->a:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/twitter/android/gl;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Z)Landroid/graphics/Bitmap;
    .locals 5

    const/4 v1, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/twitter/android/gl;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/gl;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/gh;

    iget-object v2, v0, Lcom/twitter/android/gh;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v2

    iget-object v4, v0, Lcom/twitter/android/gh;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    mul-int/2addr v2, v4

    iget v4, p0, Lcom/twitter/android/gl;->d:I

    add-int/2addr v2, v4

    iget v4, p0, Lcom/twitter/android/gl;->c:I

    if-le v2, v4, :cond_2

    const/4 v2, 0x1

    :goto_1
    if-nez v2, :cond_1

    if-nez p1, :cond_3

    :cond_1
    iget v1, p0, Lcom/twitter/android/gl;->d:I

    iget-object v2, v0, Lcom/twitter/android/gh;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v2

    iget-object v4, v0, Lcom/twitter/android/gh;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    mul-int/2addr v2, v4

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/twitter/android/gl;->d:I

    iget-object v1, p0, Lcom/twitter/android/gl;->b:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    iget-object v1, v0, Lcom/twitter/android/gh;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v3}, Landroid/graphics/Bitmap;->eraseColor(I)V

    iget-object v0, v0, Lcom/twitter/android/gh;->c:Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public a(I)Lcom/twitter/android/gh;
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/gl;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/gh;

    iget v2, v0, Lcom/twitter/android/gh;->b:I

    if-ne p1, v2, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(ILcom/twitter/android/gh;)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/gl;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/gh;

    iget v2, v0, Lcom/twitter/android/gh;->b:I

    if-ne p1, v2, :cond_0

    iget-object v1, p0, Lcom/twitter/android/gl;->b:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/twitter/android/gl;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, p2}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p2, Lcom/twitter/android/gh;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v0

    iget-object v1, p2, Lcom/twitter/android/gh;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    mul-int/2addr v1, v0

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/gl;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    iget v0, p0, Lcom/twitter/android/gl;->d:I

    add-int/2addr v0, v1

    iget v2, p0, Lcom/twitter/android/gl;->c:I

    if-le v0, v2, :cond_4

    iget-object v0, p0, Lcom/twitter/android/gl;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/gh;

    iget-object v0, v0, Lcom/twitter/android/gh;->c:Landroid/graphics/Bitmap;

    iget v2, p0, Lcom/twitter/android/gl;->d:I

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    mul-int/2addr v3, v4

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/twitter/android/gl;->d:I

    iget-object v2, p0, Lcom/twitter/android/gl;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    iget-object v2, p0, Lcom/twitter/android/gl;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_1

    :cond_4
    iget v0, p0, Lcom/twitter/android/gl;->c:I

    if-gt v1, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/gl;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, p2}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    iget v0, p0, Lcom/twitter/android/gl;->d:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/twitter/android/gl;->d:I

    goto :goto_0
.end method

.method public b(I)Lcom/twitter/android/gh;
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/gl;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/gh;

    iget v2, v0, Lcom/twitter/android/gh;->b:I

    if-ne p1, v2, :cond_0

    iget v1, p0, Lcom/twitter/android/gl;->d:I

    iget-object v2, v0, Lcom/twitter/android/gh;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v2

    iget-object v3, v0, Lcom/twitter/android/gh;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    mul-int/2addr v2, v3

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/twitter/android/gl;->d:I

    iget-object v1, p0, Lcom/twitter/android/gl;->b:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/gl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/gl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/twitter/android/gl;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/gl;->d:I

    return-void
.end method
