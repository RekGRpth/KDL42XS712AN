.class public Lcom/twitter/android/ip;
.super Lcom/twitter/android/aaa;
.source "Twttr"


# instance fields
.field private final a:Ljava/util/Map;

.field private final b:Lcom/twitter/library/widget/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/twitter/android/client/c;Lcom/twitter/library/widget/a;Ljava/util/Map;)V
    .locals 10

    const/4 v4, 0x0

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v5, v4

    move v6, v3

    move v7, v3

    move v8, v3

    move v9, v3

    invoke-direct/range {v0 .. v9}, Lcom/twitter/android/aaa;-><init>(Landroid/content/Context;IILcom/twitter/library/widget/a;Lcom/twitter/library/util/FriendshipCache;IIZZ)V

    iput-object p5, p0, Lcom/twitter/android/ip;->a:Ljava/util/Map;

    iput-object p4, p0, Lcom/twitter/android/ip;->b:Lcom/twitter/library/widget/a;

    return-void
.end method


# virtual methods
.method protected a(J)Ljava/lang/Integer;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/ip;->a:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x0

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    check-cast p1, Lcom/twitter/library/widget/UserApprovalView;

    invoke-virtual {p0, p1, p3, v0, v1}, Lcom/twitter/android/ip;->a(Lcom/twitter/library/widget/BaseUserView;Landroid/database/Cursor;J)V

    iget-wide v2, p0, Lcom/twitter/android/ip;->j:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_0

    invoke-virtual {p1, v4}, Lcom/twitter/library/widget/UserApprovalView;->setState(I)V

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserApprovalView;->e()V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/twitter/android/ip;->a:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p1, v4}, Lcom/twitter/library/widget/UserApprovalView;->setState(I)V

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserApprovalView;->c()V

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/UserApprovalView;->setState(I)V

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserApprovalView;->d()V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1, v5}, Lcom/twitter/library/widget/UserApprovalView;->setState(I)V

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserApprovalView;->e()V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/UserApprovalView;->setState(I)V

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserApprovalView;->d()V

    goto :goto_0

    :cond_1
    invoke-virtual {p1, v4}, Lcom/twitter/library/widget/UserApprovalView;->setState(I)V

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserApprovalView;->c()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x0

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03016a    # com.twitter.android.R.layout.user_approval_row_view

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/UserApprovalView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/UserApprovalView;->b()V

    iget-object v1, p0, Lcom/twitter/android/ip;->b:Lcom/twitter/library/widget/a;

    const v2, 0x7f0200df    # com.twitter.android.R.drawable.ic_accept_default

    invoke-virtual {v0, v3, v2, v1}, Lcom/twitter/library/widget/UserApprovalView;->a(IILcom/twitter/library/widget/a;)V

    const/4 v2, 0x1

    const v3, 0x7f020162    # com.twitter.android.R.drawable.ic_deny_default

    invoke-virtual {v0, v2, v3, v1}, Lcom/twitter/library/widget/UserApprovalView;->a(IILcom/twitter/library/widget/a;)V

    const v2, 0x7f020085    # com.twitter.android.R.drawable.btn_follow_action

    invoke-virtual {v0, v4, v2, v1}, Lcom/twitter/library/widget/UserApprovalView;->a(IILcom/twitter/library/widget/a;)V

    const v1, 0x7f020086    # com.twitter.android.R.drawable.btn_follow_action_bg

    invoke-virtual {v0, v4, v1}, Lcom/twitter/library/widget/UserApprovalView;->a(II)V

    new-instance v1, Lcom/twitter/android/zx;

    invoke-direct {v1, v0}, Lcom/twitter/android/zx;-><init>(Lcom/twitter/library/widget/BaseUserView;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/UserApprovalView;->setTag(Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/twitter/android/ip;->d:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method
