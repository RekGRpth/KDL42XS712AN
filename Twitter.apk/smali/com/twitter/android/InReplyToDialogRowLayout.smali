.class public Lcom/twitter/android/InReplyToDialogRowLayout;
.super Landroid/widget/RelativeLayout;
.source "Twttr"


# instance fields
.field private a:Landroid/widget/CheckBox;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/ImageView;

.field private e:Landroid/widget/ImageView;

.field private f:Ljava/lang/String;

.field private g:Z

.field private h:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    const v0, 0x7f0901a7    # com.twitter.android.R.id.in_reply_to_dialog_checkbox

    invoke-virtual {p0, v0}, Lcom/twitter/android/InReplyToDialogRowLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/twitter/android/InReplyToDialogRowLayout;->a:Landroid/widget/CheckBox;

    const v0, 0x7f0901a8    # com.twitter.android.R.id.in_reply_to_dialog_name

    invoke-virtual {p0, v0}, Lcom/twitter/android/InReplyToDialogRowLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/InReplyToDialogRowLayout;->b:Landroid/widget/TextView;

    const v0, 0x7f0901aa    # com.twitter.android.R.id.in_reply_to_dialog_screenname

    invoke-virtual {p0, v0}, Lcom/twitter/android/InReplyToDialogRowLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/InReplyToDialogRowLayout;->c:Landroid/widget/TextView;

    const v0, 0x7f0901a6    # com.twitter.android.R.id.in_reply_to_dialog_user_image

    invoke-virtual {p0, v0}, Lcom/twitter/android/InReplyToDialogRowLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/InReplyToDialogRowLayout;->d:Landroid/widget/ImageView;

    const v0, 0x7f0901a9    # com.twitter.android.R.id.in_reply_to_dialog_icon

    invoke-virtual {p0, v0}, Lcom/twitter/android/InReplyToDialogRowLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/InReplyToDialogRowLayout;->e:Landroid/widget/ImageView;

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/InReplyToDialogRowLayout;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    return-void
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/InReplyToDialogRowLayout;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    return v0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/InReplyToDialogRowLayout;->g:Z

    return v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/InReplyToDialogRowLayout;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getScreenName()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/InReplyToDialogRowLayout;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUserId()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/android/InReplyToDialogRowLayout;->h:J

    return-wide v0
.end method

.method public setAuthorName(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/InReplyToDialogRowLayout;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setCheckboxVisible(Z)V
    .locals 2

    iget-object v1, p0, Lcom/twitter/android/InReplyToDialogRowLayout;->a:Landroid/widget/CheckBox;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setChecked(Z)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/InReplyToDialogRowLayout;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    iget-object v0, p0, Lcom/twitter/android/InReplyToDialogRowLayout;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    if-eqz p1, :cond_0

    const/16 v0, 0xff

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/InReplyToDialogRowLayout;->b:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/twitter/android/InReplyToDialogRowLayout;->b:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/ColorStateList;->withAlpha(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    iget-object v1, p0, Lcom/twitter/android/InReplyToDialogRowLayout;->c:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/twitter/android/InReplyToDialogRowLayout;->c:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/ColorStateList;->withAlpha(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_1

    iget-object v1, p0, Lcom/twitter/android/InReplyToDialogRowLayout;->d:Landroid/widget/ImageView;

    int-to-float v0, v0

    const/high16 v2, 0x437f0000    # 255.0f

    div-float/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    :goto_1
    return-void

    :cond_0
    const/16 v0, 0x7e

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/InReplyToDialogRowLayout;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setAlpha(I)V

    goto :goto_1
.end method

.method public setImageUrl(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/InReplyToDialogRowLayout;->f:Ljava/lang/String;

    return-void
.end method

.method public setProfileImage(Landroid/graphics/Bitmap;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/InReplyToDialogRowLayout;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/InReplyToDialogRowLayout;->g:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setScreenName(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/InReplyToDialogRowLayout;->c:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x40

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setUserId(J)V
    .locals 0

    iput-wide p1, p0, Lcom/twitter/android/InReplyToDialogRowLayout;->h:J

    return-void
.end method

.method public setVerified(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/InReplyToDialogRowLayout;->e:Landroid/widget/ImageView;

    const v1, 0x7f02028c    # com.twitter.android.R.drawable.ic_verified

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/twitter/android/InReplyToDialogRowLayout;->e:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/InReplyToDialogRowLayout;->e:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
