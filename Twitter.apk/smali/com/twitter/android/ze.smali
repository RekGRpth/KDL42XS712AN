.class public Lcom/twitter/android/ze;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/support/v4/app/LoaderManager;

.field private final c:Lcom/twitter/android/zf;

.field private final d:I

.field private e:J

.field private f:Ljava/lang/String;

.field private g:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;Lcom/twitter/android/zf;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/ze;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/twitter/android/ze;->b:Landroid/support/v4/app/LoaderManager;

    iput-object p3, p0, Lcom/twitter/android/ze;->c:Lcom/twitter/android/zf;

    iput p4, p0, Lcom/twitter/android/ze;->d:I

    return-void
.end method


# virtual methods
.method public a(JLjava/lang/String;J)V
    .locals 3

    iput-wide p1, p0, Lcom/twitter/android/ze;->e:J

    iput-object p3, p0, Lcom/twitter/android/ze;->f:Ljava/lang/String;

    iput-wide p4, p0, Lcom/twitter/android/ze;->g:J

    iget-object v0, p0, Lcom/twitter/android/ze;->b:Landroid/support/v4/app/LoaderManager;

    iget v1, p0, Lcom/twitter/android/ze;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p2}, Lcom/twitter/library/provider/az;->a(Landroid/database/Cursor;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/ze;->c:Lcom/twitter/android/zf;

    invoke-interface {v1, v0}, Lcom/twitter/android/zf;->b(Lcom/twitter/library/api/TwitterUser;)V

    return-void
.end method

.method public b(JLjava/lang/String;J)V
    .locals 3

    iput-wide p1, p0, Lcom/twitter/android/ze;->e:J

    iput-object p3, p0, Lcom/twitter/android/ze;->f:Ljava/lang/String;

    iput-wide p4, p0, Lcom/twitter/android/ze;->g:J

    iget-object v0, p0, Lcom/twitter/android/ze;->b:Landroid/support/v4/app/LoaderManager;

    iget v1, p0, Lcom/twitter/android/ze;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/twitter/android/ze;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/library/provider/ay;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "ownerId"

    iget-wide v2, p0, Lcom/twitter/android/ze;->g:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    const-string/jumbo v4, "username=?"

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/android/ze;->f:Ljava/lang/String;

    aput-object v1, v5, v0

    :goto_0
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    iget-object v1, p0, Lcom/twitter/android/ze;->a:Landroid/content/Context;

    sget-object v3, Lcom/twitter/library/provider/cl;->a:[Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_0
    sget-object v0, Lcom/twitter/library/provider/ay;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/ze;->e:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "ownerId"

    iget-wide v2, p0, Lcom/twitter/android/ze;->g:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    move-object v5, v6

    move-object v4, v6

    goto :goto_0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/ze;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0

    return-void
.end method
