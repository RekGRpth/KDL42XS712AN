.class final Lcom/twitter/android/r;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public a:Lcom/twitter/android/widget/ActivityUserView;

.field public b:Lcom/twitter/library/api/TwitterUser;

.field public c:Ljava/lang/String;

.field public d:J

.field public e:Lcom/twitter/library/widget/SocialBylineView;


# direct methods
.method public constructor <init>(Lcom/twitter/android/widget/ActivityUserView;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/r;->a:Lcom/twitter/android/widget/ActivityUserView;

    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;Ljava/util/ArrayList;ILandroid/view/View$OnClickListener;Z)Landroid/view/View;
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/ActivityUserView;

    new-instance v1, Lcom/twitter/android/r;

    invoke-direct {v1, v0}, Lcom/twitter/android/r;-><init>(Lcom/twitter/android/widget/ActivityUserView;)V

    iget-object v2, v0, Lcom/twitter/android/widget/ActivityUserView;->m:Lcom/twitter/library/widget/ActionButton;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/twitter/android/widget/ActivityUserView;->m:Lcom/twitter/library/widget/ActionButton;

    new-instance v3, Lcom/twitter/android/l;

    invoke-direct {v3}, Lcom/twitter/android/l;-><init>()V

    invoke-virtual {v2, v3}, Lcom/twitter/library/widget/ActionButton;->setTag(Ljava/lang/Object;)V

    const v2, 0x7f020085    # com.twitter.android.R.drawable.btn_follow_action

    invoke-virtual {v0, v2, p3}, Lcom/twitter/android/widget/ActivityUserView;->a(ILandroid/view/View$OnClickListener;)V

    iget-object v2, v0, Lcom/twitter/android/widget/ActivityUserView;->m:Lcom/twitter/library/widget/ActionButton;

    const v3, 0x7f020086    # com.twitter.android.R.drawable.btn_follow_action_bg

    invoke-virtual {v2, v3}, Lcom/twitter/library/widget/ActionButton;->setBackgroundResource(I)V

    :cond_0
    invoke-virtual {v0, p4}, Lcom/twitter/android/widget/ActivityUserView;->setShowBio(Z)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ActivityUserView;->setTag(Ljava/lang/Object;)V

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public static a(Landroid/view/View;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/android/client/c;Lcom/twitter/library/util/FriendshipCache;Landroid/content/res/Resources;Lcom/twitter/library/view/c;ZJ)V
    .locals 9

    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/twitter/android/r;

    iget-object v0, v8, Lcom/twitter/android/r;->a:Lcom/twitter/android/widget/ActivityUserView;

    move-object v1, p2

    move-object v2, p3

    move-object v3, p1

    move-object v4, p5

    move v5, p6

    move-wide/from16 v6, p7

    invoke-virtual/range {v0 .. v7}, Lcom/twitter/android/widget/ActivityUserView;->a(Lcom/twitter/android/client/c;Lcom/twitter/library/util/FriendshipCache;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/view/c;ZJ)V

    iget-object v1, v0, Lcom/twitter/android/widget/ActivityUserView;->m:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v1}, Lcom/twitter/library/widget/ActionButton;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/l;

    iput-object p1, v1, Lcom/twitter/android/l;->a:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {p1}, Lcom/twitter/library/api/TwitterUser;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ActivityUserView;->setImageTag(Ljava/lang/Object;)V

    iput-object p1, v8, Lcom/twitter/android/r;->b:Lcom/twitter/library/api/TwitterUser;

    return-void
.end method
