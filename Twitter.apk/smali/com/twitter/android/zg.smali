.class Lcom/twitter/android/zg;
.super Landroid/support/v4/widget/CursorAdapter;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/UserPresenceFragment;


# direct methods
.method public constructor <init>(Lcom/twitter/android/UserPresenceFragment;Landroid/content/Context;Landroid/database/Cursor;I)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/zg;->a:Lcom/twitter/android/UserPresenceFragment;

    invoke-direct {p0, p2, p3, p4}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    iget-object v0, p0, Lcom/twitter/android/zg;->a:Lcom/twitter/android/UserPresenceFragment;

    invoke-virtual {v0}, Lcom/twitter/android/UserPresenceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f040010    # com.twitter.android.R.anim.quick_fade_out

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/zh;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/zh;-><init>(Lcom/twitter/android/zg;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/zg;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/zg;->a(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/zi;

    const/4 v1, 0x0

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/zi;->a(I)V

    return-void
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030176    # com.twitter.android.R.layout.user_presence_row

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/zi;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/zi;-><init>(Lcom/twitter/android/zg;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-object v0
.end method
