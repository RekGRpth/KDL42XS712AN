.class public Lcom/twitter/android/CardDebugFragment;
.super Landroid/support/v4/app/Fragment;
.source "Twttr"


# instance fields
.field private a:Lcom/twitter/android/bo;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    const v0, 0x7f03001e    # com.twitter.android.R.layout.card_debug_fragment

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0900b4    # com.twitter.android.R.id.clear_log

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v2, Lcom/twitter/android/bm;

    invoke-direct {v2, p0}, Lcom/twitter/android/bm;-><init>(Lcom/twitter/android/CardDebugFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/twitter/android/bo;

    invoke-virtual {p0}, Lcom/twitter/android/CardDebugFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lcom/twitter/android/bo;-><init>(Lcom/twitter/android/CardDebugFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/CardDebugFragment;->a:Lcom/twitter/android/bo;

    iget-object v0, p0, Lcom/twitter/android/CardDebugFragment;->a:Lcom/twitter/android/bo;

    invoke-static {v0}, Lcom/twitter/library/card/CardDebugLog;->a(Lcom/twitter/library/card/h;)V

    const v0, 0x7f0900b5    # com.twitter.android.R.id.entry_list

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iget-object v2, p0, Lcom/twitter/android/CardDebugFragment;->a:Lcom/twitter/android/bo;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v2, Lcom/twitter/android/bn;

    invoke-direct {v2, p0}, Lcom/twitter/android/bn;-><init>(Lcom/twitter/android/CardDebugFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-object v1
.end method

.method public onDestroyView()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    iget-object v0, p0, Lcom/twitter/android/CardDebugFragment;->a:Lcom/twitter/android/bo;

    invoke-static {v0}, Lcom/twitter/library/card/CardDebugLog;->b(Lcom/twitter/library/card/h;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/CardDebugFragment;->a:Lcom/twitter/android/bo;

    return-void
.end method
