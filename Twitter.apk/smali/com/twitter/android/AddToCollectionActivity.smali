.class public Lcom/twitter/android/AddToCollectionActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/cw;


# instance fields
.field private a:J

.field private b:Landroid/widget/RadioGroup;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a(I)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a(Z)V

    const v1, 0x7f030033    # com.twitter.android.R.layout.collections_list_activity

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    const v0, 0x7f0f0328    # com.twitter.android.R.string.profile_tab_title_collections_owned_by

    invoke-virtual {p0, v0}, Lcom/twitter/android/AddToCollectionActivity;->setTitle(I)V

    const v0, 0x7f090251    # com.twitter.android.R.id.scope_option

    invoke-virtual {p0, v0}, Lcom/twitter/android/AddToCollectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/twitter/android/AddToCollectionActivity;->b:Landroid/widget/RadioGroup;

    iget-object v0, p0, Lcom/twitter/android/AddToCollectionActivity;->b:Landroid/widget/RadioGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setVisibility(I)V

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/AddToCollectionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/CollectionsListFragment;

    invoke-direct {v1}, Lcom/twitter/android/CollectionsListFragment;-><init>()V

    const-string/jumbo v2, "user_id"

    const-wide/16 v3, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/android/AddToCollectionActivity;->a:J

    const-string/jumbo v2, "list_type"

    invoke-virtual {v0, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v2, "chmode"

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {v1, v0}, Lcom/twitter/android/CollectionsListFragment;->a(Landroid/content/Intent;)Lcom/twitter/android/client/BaseListFragment;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/twitter/android/client/BaseListFragment;->j(Z)Lcom/twitter/android/client/BaseListFragment;

    invoke-virtual {v1, p0}, Lcom/twitter/android/CollectionsListFragment;->a(Lcom/twitter/android/cw;)V

    invoke-virtual {p0}, Lcom/twitter/android/AddToCollectionActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v2, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    invoke-virtual {p0}, Lcom/twitter/android/AddToCollectionActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/AddToCollectionActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "tweet"

    aput-object v4, v3, v6

    const-string/jumbo v4, "custom_timeline_list"

    aput-object v4, v3, v5

    const/4 v4, 0x2

    aput-object v7, v3, v4

    const/4 v4, 0x3

    aput-object v7, v3, v4

    const/4 v4, 0x4

    const-string/jumbo v5, "impression"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v0, "user_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/AddToCollectionActivity;->a:J

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    const/4 v0, -0x1

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v2, "result_timeline_id"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/AddToCollectionActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/AddToCollectionActivity;->finish()V

    return-void
.end method

.method public a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z

    const v0, 0x7f110005    # com.twitter.android.R.menu.collections_list

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    iget-wide v3, p0, Lcom/twitter/android/AddToCollectionActivity;->a:J

    invoke-virtual {p0}, Lcom/twitter/android/AddToCollectionActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    cmp-long v0, v3, v5

    if-nez v0, :cond_1

    invoke-static {}, Lgj;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_0

    const v0, 0x7f090305    # com.twitter.android.R.id.menu_create_collection

    invoke-virtual {p2, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    invoke-virtual {v0, v2}, Lhn;->b(Z)Lhn;

    :cond_0
    return v1

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method public a(Lhn;)Z
    .locals 2

    invoke-virtual {p1}, Lhn;->a()I

    move-result v0

    const v1, 0x7f090305    # com.twitter.android.R.id.menu_create_collection

    if-ne v0, v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/CollectionCreateEditActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/AddToCollectionActivity;->startActivity(Landroid/content/Intent;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhn;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "user_id"

    iget-wide v1, p0, Lcom/twitter/android/AddToCollectionActivity;->a:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    return-void
.end method
