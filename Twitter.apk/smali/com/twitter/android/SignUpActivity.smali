.class public Lcom/twitter/android/SignUpActivity;
.super Lcom/twitter/android/BaseSignUpActivity;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field private A:Ljava/lang/String;

.field r:Ljava/lang/String;

.field s:Landroid/graphics/Bitmap;

.field t:Ljava/lang/String;

.field u:Z

.field private final v:Lcom/twitter/android/tl;

.field private w:Landroid/widget/Button;

.field private x:Ljava/lang/String;

.field private y:Z

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/BaseSignUpActivity;-><init>()V

    new-instance v0, Lcom/twitter/android/tl;

    invoke-direct {v0, p0}, Lcom/twitter/android/tl;-><init>(Lcom/twitter/android/SignUpActivity;)V

    iput-object v0, p0, Lcom/twitter/android/SignUpActivity;->v:Lcom/twitter/android/tl;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/SignUpActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/SignUpActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/SignUpActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/SignUpActivity;->y:Z

    return v0
.end method

.method static synthetic d(Lcom/twitter/android/SignUpActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/SignUpActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method private h()Z
    .locals 5

    const/4 v4, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/SignUpActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    const-string/jumbo v3, "com.google"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    if-eqz v2, :cond_1

    array-length v3, v2

    if-nez v3, :cond_2

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    aget-object v2, v2, v1

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/twitter/android/SignUpActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iput v4, p0, Lcom/twitter/android/SignUpActivity;->l:I

    iget-object v1, p0, Lcom/twitter/android/SignUpActivity;->p:Lcom/twitter/android/bj;

    invoke-virtual {v1, v4}, Lcom/twitter/android/bj;->a(I)V

    goto :goto_0
.end method

.method private k()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->e:Landroid/widget/CheckBox;

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->f:Lcom/twitter/android/util/d;

    invoke-interface {v0}, Lcom/twitter/android/util/d;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->e:Landroid/widget/CheckBox;

    const v1, 0x7f0f0438    # com.twitter.android.R.string.signup_addr_book_and_phone_text

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/android/SignUpActivity;->f:Lcom/twitter/android/util/d;

    invoke-interface {v3}, Lcom/twitter/android/util/d;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/twitter/android/SignUpActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    const v0, 0x7f09026e    # com.twitter.android.R.id.discovery_privacy

    invoke-virtual {p0, v0}, Lcom/twitter/android/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0f043d    # com.twitter.android.R.string.signup_digits_discovery_privacy

    invoke-virtual {p0, v1}, Lcom/twitter/android/SignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->e:Landroid/widget/CheckBox;

    const v1, 0x7f0f0439    # com.twitter.android.R.string.signup_addr_book_no_phone_text

    invoke-virtual {p0, v1}, Lcom/twitter/android/SignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private l()V
    .locals 3

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_1

    invoke-direct {p0}, Lcom/twitter/android/SignUpActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/SignUpActivity;->q:Z

    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->b()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const v1, 0x7f03013a    # com.twitter.android.R.layout.sign_up

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->c(Z)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->b(Z)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->a(Z)V

    return-object v0
.end method

.method a()V
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/twitter/android/SignUpActivity;->w:Landroid/widget/Button;

    iget v2, p0, Lcom/twitter/android/SignUpActivity;->k:I

    if-ne v2, v0, :cond_0

    iget v2, p0, Lcom/twitter/android/SignUpActivity;->l:I

    if-ne v2, v0, :cond_0

    iget v2, p0, Lcom/twitter/android/SignUpActivity;->m:I

    if-ne v2, v0, :cond_0

    iget v2, p0, Lcom/twitter/android/SignUpActivity;->n:I

    if-ne v2, v0, :cond_0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 10

    const/4 v9, 0x1

    const/4 v8, 0x0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/BaseSignUpActivity;->a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V

    const v0, 0x7f0f0449    # com.twitter.android.R.string.signup_sign_up

    invoke-virtual {p0, v0}, Lcom/twitter/android/SignUpActivity;->setTitle(I)V

    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "add_account"

    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SignUpActivity;->u:Z

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    const v0, 0x7f090080    # com.twitter.android.R.id.tos

    invoke-virtual {p0, v0}, Lcom/twitter/android/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    const v0, 0x7f09026d    # com.twitter.android.R.id.signup_button

    invoke-virtual {p0, v0}, Lcom/twitter/android/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/SignUpActivity;->w:Landroid/widget/Button;

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->w:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/SignedOutFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SignUpActivity;->A:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->A:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/twitter/android/SignedOutActivity;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/android/SignedOutFragment;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/SignUpActivity;->A:Ljava/lang/String;

    :cond_0
    invoke-static {}, Lcom/twitter/library/client/App;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/twitter/library/util/a;->b(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_1

    const v0, 0x7f09026c    # com.twitter.android.R.id.label_email

    invoke-virtual {p0, v0}, Lcom/twitter/android/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string/jumbo v2, "@twitter.com email"

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->f:Lcom/twitter/android/util/d;

    invoke-interface {v0}, Lcom/twitter/android/util/d;->g()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string/jumbo v0, "available"

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-instance v4, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v4, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v5, v9, [Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "signup:form:phone_number::"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v8

    invoke-virtual {v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    const/4 v4, 0x2

    invoke-virtual {v0, v4}, Lcom/twitter/library/scribe/ScribeLog;->c(I)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->f:Lcom/twitter/android/util/d;

    invoke-interface {v0}, Lcom/twitter/android/util/d;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/twitter/android/SignUpActivity;->k()V

    :cond_2
    invoke-direct {p0}, Lcom/twitter/android/SignUpActivity;->l()V

    const v0, 0x7f0901c5    # com.twitter.android.R.id.settings_button

    invoke-virtual {p0, v0}, Lcom/twitter/android/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/twitter/android/bk;

    invoke-direct {v0, p0}, Lcom/twitter/android/bk;-><init>(Lcom/twitter/android/BaseSignUpActivity;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/SignUpActivity;->a(Lcom/twitter/library/client/j;)V

    if-nez p1, :cond_6

    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v0, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v2, v9, [Ljava/lang/String;

    const-string/jumbo v3, "signup::::impression"

    aput-object v3, v2, v8

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "from_widget"

    invoke-virtual {v2, v3, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string/jumbo v2, "widget"

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->h(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_3
    iget-object v2, p0, Lcom/twitter/android/SignUpActivity;->A:Ljava/lang/String;

    if-eqz v2, :cond_4

    new-array v2, v9, [Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/SignUpActivity;->A:Ljava/lang/String;

    aput-object v3, v2, v8

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->c([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_4
    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :goto_1
    return-void

    :cond_5
    const-string/jumbo v0, "unavailable"

    goto/16 :goto_0

    :cond_6
    const-string/jumbo v0, "reqId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SignUpActivity;->z:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/SignUpActivity;->z:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/SignUpActivity;->v:Lcom/twitter/android/tl;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/aa;->a(Ljava/lang/String;Lcom/twitter/library/client/ah;)V

    goto :goto_1
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "display_name"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/SignUpActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/android/SignUpActivity;->k:I

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->p:Lcom/twitter/android/bj;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/bj;->a(I)V

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/SignUpActivity;->h()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SignUpActivity;->q:Z

    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->b()V

    return-void
.end method

.method f()V
    .locals 10

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-virtual {p0, v8}, Lcom/twitter/android/SignUpActivity;->showDialog(I)V

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->k(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/SignUpActivity;->x:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/SignUpActivity;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v8

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/SignUpActivity;->y:Z

    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v8, [Ljava/lang/String;

    const-string/jumbo v4, "signup:form:::submit"

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->A:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "signup:form:::submit"

    aput-object v3, v2, v7

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    new-array v2, v8, [Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/SignUpActivity;->A:Ljava/lang/String;

    aput-object v3, v2, v7

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->c([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/SignUpActivity;->x:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/SignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v2}, Lcom/twitter/internal/android/widget/PopupEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/SignUpActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/SignUpActivity;->d:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/SignUpActivity;->r:Ljava/lang/String;

    iget-object v6, p0, Lcom/twitter/android/SignUpActivity;->t:Ljava/lang/String;

    iget-object v9, p0, Lcom/twitter/android/SignUpActivity;->f:Lcom/twitter/android/util/d;

    invoke-interface {v9}, Lcom/twitter/android/util/d;->a()Z

    move-result v9

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/twitter/android/SignUpActivity;->e:Landroid/widget/CheckBox;

    invoke-virtual {v9}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v9

    if-nez v9, :cond_1

    iget-object v9, p0, Lcom/twitter/android/SignUpActivity;->e:Landroid/widget/CheckBox;

    invoke-virtual {v9}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v9

    if-eqz v9, :cond_1

    move v7, v8

    :cond_1
    iget-object v9, p0, Lcom/twitter/android/SignUpActivity;->v:Lcom/twitter/android/tl;

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/library/client/aa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/twitter/library/client/ak;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SignUpActivity;->z:Ljava/lang/String;

    return-void

    :cond_2
    move v0, v7

    goto/16 :goto_0
.end method

.method g()V
    .locals 2

    const v0, 0x7f0f0441    # com.twitter.android.R.string.signup_error

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public onBackPressed()V
    .locals 6

    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "signup:::back_button:click"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-super {p0}, Lcom/twitter/android/BaseSignUpActivity;->onBackPressed()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f09026d    # com.twitter.android.R.id.signup_button

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->f()V

    :goto_0
    return-void

    :cond_0
    const v1, 0x7f0901c5    # com.twitter.android.R.id.settings_button

    if-ne v0, v1, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/LoggedOutSettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/SignUpActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Lcom/twitter/android/BaseSignUpActivity;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5

    const/4 v2, 0x1

    const/4 v4, 0x0

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/twitter/android/BaseSignUpActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    const v1, 0x7f0f043c    # com.twitter.android.R.string.signup_creating

    invoke-virtual {p0, v1}, Lcom/twitter/android/SignUpActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0350    # com.twitter.android.R.string.removed_twitter_from_fullname_message

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/android/SignUpActivity;->x:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0f0351    # com.twitter.android.R.string.removed_twitter_from_fullname_title

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f02d5    # com.twitter.android.R.string.ok

    new-instance v2, Lcom/twitter/android/ti;

    invoke-direct {v2, p0}, Lcom/twitter/android/ti;-><init>(Lcom/twitter/android/SignUpActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03001c    # com.twitter.android.R.layout.captcha_dialog

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/tj;

    invoke-direct {v1, p0}, Lcom/twitter/android/tj;-><init>(Lcom/twitter/android/SignUpActivity;)V

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0f043b    # com.twitter.android.R.string.signup_captcha_title

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0f0449    # com.twitter.android.R.string.signup_sign_up

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0f0089    # com.twitter.android.R.string.cancel

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    const/4 v4, 0x0

    new-instance v0, Landroid/support/v4/content/CursorLoader;

    sget-object v2, Landroid/provider/ContactsContract$Profile;->CONTENT_URI:Landroid/net/Uri;

    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v5, "display_name"

    aput-object v5, v3, v1

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/BaseSignUpActivity;->onDestroy()V

    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/SignUpActivity;->z:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->e(Ljava/lang/String;)V

    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/widget/TextView;->getId()I

    move-result v0

    const v1, 0x7f0900c8    # com.twitter.android.R.id.password

    if-ne v0, v1, :cond_0

    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/SignUpActivity;->f()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/BaseSignUpActivity;->onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/SignUpActivity;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0

    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 2

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/BaseSignUpActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    :goto_0
    return-void

    :pswitch_0
    check-cast p2, Landroid/app/AlertDialog;

    const v0, 0x7f0900b1    # com.twitter.android.R.id.captcha_image

    invoke-virtual {p2, v0}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/twitter/android/SignUpActivity;->s:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const v0, 0x7f0900b2    # com.twitter.android.R.id.captcha_answer

    invoke-virtual {p2, v0}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SignUpActivity;->f:Lcom/twitter/android/util/d;

    invoke-interface {v0}, Lcom/twitter/android/util/d;->c()V

    invoke-super {p0}, Lcom/twitter/android/BaseSignUpActivity;->onResume()V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/BaseSignUpActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "reqId"

    iget-object v1, p0, Lcom/twitter/android/SignUpActivity;->z:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
