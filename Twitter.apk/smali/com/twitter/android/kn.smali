.class Lcom/twitter/android/kn;
.super Lcom/twitter/android/kg;
.source "Twttr"


# instance fields
.field public final a:Lhb;

.field private final c:Landroid/content/Intent;

.field private final d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lhb;ILandroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p3}, Lcom/twitter/android/kg;-><init>(I)V

    iput-object p2, p0, Lcom/twitter/android/kn;->a:Lhb;

    iput-object p1, p0, Lcom/twitter/android/kn;->d:Landroid/content/Context;

    iput-object p4, p0, Lcom/twitter/android/kn;->c:Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    const/4 v3, 0x0

    if-nez p2, :cond_0

    new-instance v1, Lcom/twitter/android/ko;

    invoke-direct {v1}, Lcom/twitter/android/ko;-><init>()V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f03005a    # com.twitter.android.R.layout.drawer_item

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    const v0, 0x7f090135    # com.twitter.android.R.id.drawer_label

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/twitter/android/ko;->a:Landroid/widget/TextView;

    const v0, 0x7f090134    # com.twitter.android.R.id.drawer_icon

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/twitter/android/ko;->b:Landroid/widget/ImageView;

    const v0, 0x7f090038    # com.twitter.android.R.id.drag_handle

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/twitter/android/ko;->c:Landroid/widget/ImageView;

    const v0, 0x7f090133    # com.twitter.android.R.id.unread_indicator

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/twitter/android/ko;->d:Landroid/widget/ImageView;

    const v0, 0x7f090136    # com.twitter.android.R.id.unread_badge

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/twitter/android/ko;->e:Landroid/widget/TextView;

    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    :goto_0
    iget-object v1, v0, Lcom/twitter/android/ko;->a:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/twitter/android/kn;->a:Lhb;

    iget-object v2, v2, Lhb;->d:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/twitter/android/kn;->a:Lhb;

    iget v1, v1, Lhb;->e:I

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/twitter/android/ko;->b:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/twitter/android/kn;->a:Lhb;

    iget v2, v2, Lhb;->e:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_1
    iget v1, p0, Lcom/twitter/android/kn;->b:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    iget-object v0, v0, Lcom/twitter/android/ko;->c:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_2
    invoke-virtual {p0, p2}, Lcom/twitter/android/kn;->a(Landroid/view/View;)V

    return-object p2

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ko;

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lcom/twitter/android/ko;->b:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1

    :cond_2
    iget-object v0, v0, Lcom/twitter/android/ko;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2
.end method

.method public a(Landroid/view/View;)V
    .locals 4

    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ko;

    iget-object v1, p0, Lcom/twitter/android/kn;->a:Lhb;

    iget v1, v1, Lhb;->k:I

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/twitter/android/ko;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/twitter/android/kn;->a:Lhb;

    iget-boolean v1, v1, Lhb;->g:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/kn;->a:Lhb;

    iget v1, v1, Lhb;->k:I

    const/16 v2, 0x63

    if-gt v1, v2, :cond_1

    iget-object v1, p0, Lcom/twitter/android/kn;->a:Lhb;

    iget v1, v1, Lhb;->k:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    iget-object v2, v0, Lcom/twitter/android/ko;->e:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v0, Lcom/twitter/android/ko;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const-string/jumbo v1, "99+"

    goto :goto_0

    :cond_2
    iget-object v1, v0, Lcom/twitter/android/ko;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, v0, Lcom/twitter/android/ko;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/kn;->d:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/kn;->c:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
