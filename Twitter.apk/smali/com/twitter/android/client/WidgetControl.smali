.class public Lcom/twitter/android/client/WidgetControl;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/util/ar;


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field public static final c:Z

.field private static final f:Ljava/text/SimpleDateFormat;

.field private static g:Landroid/text/style/TextAppearanceSpan;

.field private static h:I


# instance fields
.field public final d:Ljava/lang/String;

.field public final e:J

.field private final i:[Lcom/twitter/android/client/WidgetControl$WidgetList;

.field private final j:Lcom/twitter/android/client/cl;

.field private final k:Lcom/twitter/android/client/cl;

.field private final l:Landroid/content/Context;

.field private final m:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final n:Lcom/twitter/library/util/ao;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v1, 0x1

    const-string/jumbo v0, ".widget.button.next"

    invoke-static {v0}, Lcom/twitter/library/client/App;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/client/WidgetControl;->a:Ljava/lang/String;

    const-string/jumbo v0, ".widget.button.prev"

    invoke-static {v0}, Lcom/twitter/library/client/App;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/client/WidgetControl;->b:Ljava/lang/String;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v0, v2, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/client/WidgetControl;->c:Z

    new-instance v0, Lcom/twitter/internal/util/SynchronizedDateFormat;

    invoke-direct {v0}, Lcom/twitter/internal/util/SynchronizedDateFormat;-><init>()V

    sput-object v0, Lcom/twitter/android/client/WidgetControl;->f:Ljava/text/SimpleDateFormat;

    sput v1, Lcom/twitter/android/client/WidgetControl;->h:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/util/ao;Ljava/lang/String;J)V
    .locals 7

    const/4 v6, 0x0

    const v5, 0x7f060004    # com.twitter.android.R.xml.appwidget_large_provider

    const v3, 0x7f030189    # com.twitter.android.R.layout.widget_clear_large_view

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    iput-object p2, p0, Lcom/twitter/android/client/WidgetControl;->n:Lcom/twitter/library/util/ao;

    iput-object p3, p0, Lcom/twitter/android/client/WidgetControl;->d:Ljava/lang/String;

    iput-wide p4, p0, Lcom/twitter/android/client/WidgetControl;->e:J

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/twitter/android/client/WidgetControl$WidgetList;

    new-instance v1, Lcom/twitter/android/client/WidgetControl$WidgetList;

    invoke-direct {v1, v6}, Lcom/twitter/android/client/WidgetControl$WidgetList;-><init>(Lcom/twitter/android/client/ch;)V

    aput-object v1, v0, v4

    const/4 v1, 0x1

    new-instance v2, Lcom/twitter/android/client/WidgetControl$WidgetList;

    invoke-direct {v2, v6}, Lcom/twitter/android/client/WidgetControl$WidgetList;-><init>(Lcom/twitter/android/client/ch;)V

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/twitter/android/client/WidgetControl;->i:[Lcom/twitter/android/client/WidgetControl$WidgetList;

    sget-boolean v0, Lcom/twitter/android/client/WidgetControl;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/twitter/android/client/cl;

    const v1, 0x7f03018f    # com.twitter.android.R.layout.widget_scrollable_view

    invoke-direct {v0, v5, v1, v3, p3}, Lcom/twitter/android/client/cl;-><init>(IIILjava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/client/WidgetControl;->j:Lcom/twitter/android/client/cl;

    :goto_0
    new-instance v0, Lcom/twitter/android/client/cl;

    const v1, 0x7f060005    # com.twitter.android.R.xml.appwidget_small_provider

    const v2, 0x7f03019a    # com.twitter.android.R.layout.widget_small_view

    const v3, 0x7f03018a    # com.twitter.android.R.layout.widget_clear_small_view

    invoke-direct {v0, v1, v2, v3, p3}, Lcom/twitter/android/client/cl;-><init>(IIILjava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/client/WidgetControl;->k:Lcom/twitter/android/client/cl;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/twitter/android/client/WidgetControl;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    sget-object v0, Lcom/twitter/android/client/WidgetControl;->f:Ljava/text/SimpleDateFormat;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00eb    # com.twitter.android.R.string.datetime_format_long

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance v0, Lcom/twitter/android/client/cl;

    const v1, 0x7f03018d    # com.twitter.android.R.layout.widget_large_view

    invoke-direct {v0, v5, v1, v3, p3}, Lcom/twitter/android/client/cl;-><init>(IIILjava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/client/WidgetControl;->j:Lcom/twitter/android/client/cl;

    goto :goto_0
.end method

.method private a(Lcom/twitter/android/client/WidgetControl$WidgetList;)J
    .locals 2

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/twitter/android/client/WidgetControl$WidgetList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/Tweet;

    if-eqz v0, :cond_0

    iget-wide v0, v0, Lcom/twitter/library/provider/Tweet;->h:J

    :goto_0
    return-wide v0

    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 4

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/StartActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/twitter/android/client/WidgetControl;->e()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "ref_event"

    const-string/jumbo v3, "widget::::click"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "sb_account_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "page"

    sget-object v2, Lcom/twitter/android/MainActivity;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/NotificationActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;IIILjava/lang/String;I)Landroid/widget/RemoteViews;
    .locals 8

    const/16 v7, 0x8

    const/4 v6, 0x0

    const v5, 0x7f0902df    # com.twitter.android.R.id.header_text_item

    const v3, 0x7f090035    # com.twitter.android.R.id.compose

    const/high16 v4, 0x10000000

    new-instance v1, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, p2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    const v0, 0x7f060004    # com.twitter.android.R.xml.appwidget_large_provider

    if-ne p1, v0, :cond_0

    const v2, 0x7f0902de    # com.twitter.android.R.id.icon_item

    const/4 v0, 0x5

    if-ne p5, v0, :cond_1

    const v0, 0x7f0201d3    # com.twitter.android.R.drawable.ic_mentions_widget

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    invoke-virtual {v1, v5, p4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    invoke-static {p0, p4, p5}, Lcom/twitter/android/client/WidgetControl;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v6, v0, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const v2, 0x7f0902de    # com.twitter.android.R.id.icon_item

    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    invoke-virtual {v1, v5, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    packed-switch p3, :pswitch_data_0

    invoke-virtual {v1, v3, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.MAIN"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "android.intent.category.HOME"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Landroid/content/Context;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v2

    const/high16 v3, 0x14000000

    invoke-virtual {v2, v3}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(I)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v2

    invoke-virtual {v2, p4}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Ljava/lang/String;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v2

    const-string/jumbo v3, "widget::::click"

    invoke-virtual {v2, v3}, Lcom/twitter/android/composer/ComposerIntentWrapper;->b(Ljava/lang/String;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Landroid/content/Intent;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a()Landroid/content/Intent;

    move-result-object v0

    const v2, 0x7f0902e0    # com.twitter.android.R.id.compose_item

    invoke-static {}, Lcom/twitter/android/client/WidgetControl;->e()I

    move-result v3

    invoke-static {p0, v3, v0, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    :cond_0
    :goto_1
    return-object v1

    :cond_1
    const v0, 0x7f020289    # com.twitter.android.R.drawable.ic_tweets_widget

    goto :goto_0

    :pswitch_0
    invoke-virtual {v1, v3, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_1

    :pswitch_1
    invoke-virtual {v1, v3, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f0224    # com.twitter.android.R.string.loading

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v5, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lcom/twitter/android/client/WidgetControl;I)Lcom/twitter/android/client/WidgetControl$WidgetList;
    .locals 1

    invoke-direct {p0, p1}, Lcom/twitter/android/client/WidgetControl;->e(I)Lcom/twitter/android/client/WidgetControl$WidgetList;

    move-result-object v0

    return-object v0
.end method

.method private a(JLcom/twitter/android/client/WidgetControl$WidgetList;)V
    .locals 4

    invoke-virtual {p3}, Lcom/twitter/android/client/WidgetControl$WidgetList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/Tweet;

    iget-wide v2, v0, Lcom/twitter/library/provider/Tweet;->u:J

    cmp-long v0, v2, p1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/WidgetControl;->b(Z)V

    :cond_1
    return-void
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/client/WidgetService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "clear_logged_out"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "widget_provider"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method static a(Landroid/content/Context;Landroid/widget/RemoteViews;Lcom/twitter/library/provider/Tweet;Landroid/graphics/Bitmap;JLjava/lang/String;)V
    .locals 8

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v0, 0x7f090097    # com.twitter.android.R.id.name

    iget-object v1, p2, Lcom/twitter/library/provider/Tweet;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v0, 0x7f090098    # com.twitter.android.R.id.username

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "@"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p2, Lcom/twitter/library/provider/Tweet;->p:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    new-instance v3, Landroid/text/SpannableStringBuilder;

    iget-object v0, p2, Lcom/twitter/library/provider/Tweet;->d:Ljava/lang/String;

    invoke-direct {v3, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    iget-object v1, p2, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    if-eqz v1, :cond_0

    iget-object v0, v1, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iget-object v1, v1, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/UrlEntity;

    iget v5, v0, Lcom/twitter/library/api/UrlEntity;->start:I

    sub-int/2addr v5, v1

    iget v6, v0, Lcom/twitter/library/api/UrlEntity;->end:I

    sub-int/2addr v6, v1

    if-ltz v5, :cond_8

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    if-gt v6, v7, :cond_8

    iget-object v0, v0, Lcom/twitter/library/api/UrlEntity;->displayUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_8

    invoke-virtual {v3, v5, v6, v0}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v5

    sub-int v0, v6, v0

    add-int/2addr v1, v0

    move v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    const v0, 0x7f09009d    # com.twitter.android.R.id.text_item

    invoke-virtual {p1, v0, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v0, 0x7f090087    # com.twitter.android.R.id.time

    iget-wide v3, p2, Lcom/twitter/library/provider/Tweet;->h:J

    invoke-static {v2, v3, v4}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v1, 0x7f09009a    # com.twitter.android.R.id.photo_icon

    invoke-virtual {p2}, Lcom/twitter/library/provider/Tweet;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v1, 0x7f09009b    # com.twitter.android.R.id.media_icon

    invoke-virtual {p2}, Lcom/twitter/library/provider/Tweet;->r()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v1, 0x7f09009c    # com.twitter.android.R.id.summary_icon

    invoke-virtual {p2}, Lcom/twitter/library/provider/Tweet;->s()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    :goto_4
    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    if-eqz p3, :cond_4

    const v0, 0x7f09005e    # com.twitter.android.R.id.user_image

    invoke-virtual {p1, v0, p3}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    :goto_5
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/TweetActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "android.intent.action.VIEW"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/twitter/android/client/WidgetControl;->e()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-wide v3, p2, Lcom/twitter/library/provider/Tweet;->C:J

    invoke-static {v3, v4, p4, p5}, Lcom/twitter/library/provider/w;->a(JJ)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "sb_account_name"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "ref_event"

    const-string/jumbo v3, "widget::::click"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const v1, 0x7f0902e1    # com.twitter.android.R.id.user_status

    const/4 v3, 0x0

    const/high16 v4, 0x10000000

    invoke-static {p0, v3, v0, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    iget-boolean v0, p2, Lcom/twitter/library/provider/Tweet;->r:Z

    if-eqz v0, :cond_5

    const v0, 0x7f09009f    # com.twitter.android.R.id.retweeter_item

    const v1, 0x7f0f04f2    # com.twitter.android.R.string.tweets_retweeted_by

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p2, Lcom/twitter/library/provider/Tweet;->e:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v2, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v0, 0x7f09009f    # com.twitter.android.R.id.retweeter_item

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    :goto_6
    invoke-virtual {p2}, Lcom/twitter/library/provider/Tweet;->v()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p2, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v0, :cond_6

    iget-object v0, v0, Lcom/twitter/library/api/PromotedContent;->advertiserName:Ljava/lang/String;

    :goto_7
    const v1, 0x7f0900a0    # com.twitter.android.R.id.promoted_item

    const v3, 0x7f0f0333    # com.twitter.android.R.string.promoted_by

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v0, 0x7f0900a0    # com.twitter.android.R.id.promoted_item

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    :goto_8
    return-void

    :cond_1
    const/16 v0, 0x8

    goto/16 :goto_2

    :cond_2
    const/16 v0, 0x8

    goto/16 :goto_3

    :cond_3
    const/16 v0, 0x8

    goto/16 :goto_4

    :cond_4
    const v0, 0x7f09005e    # com.twitter.android.R.id.user_image

    const v1, 0x7f02003a    # com.twitter.android.R.drawable.bg_no_profile_photo_md

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto/16 :goto_5

    :cond_5
    const v0, 0x7f09009f    # com.twitter.android.R.id.retweeter_item

    const/16 v1, 0x8

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_6

    :cond_6
    iget-object v0, p2, Lcom/twitter/library/provider/Tweet;->f:Ljava/lang/String;

    goto :goto_7

    :cond_7
    const v0, 0x7f0900a0    # com.twitter.android.R.id.promoted_item

    const/16 v1, 0x8

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_8

    :cond_8
    move v0, v1

    goto/16 :goto_1
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/android/client/cl;ILjava/lang/String;I)V
    .locals 14

    const/4 v2, 0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_2

    invoke-virtual {p1}, Lcom/twitter/android/client/cl;->a()[I

    move-result-object v9

    if-eqz v9, :cond_0

    array-length v2, v9

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v10, 0x0

    const v2, 0x7f0f0568    # com.twitter.android.R.string.widget_stale_message

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    array-length v12, v9

    const/4 v2, 0x0

    move v8, v2

    :goto_1
    if-ge v8, v12, :cond_0

    aget v13, v9, v8

    iget v3, p1, Lcom/twitter/android/client/cl;->a:I

    iget v4, p1, Lcom/twitter/android/client/cl;->c:I

    const-string/jumbo v6, ""

    const/4 v7, 0x0

    move-object v2, p0

    move/from16 v5, p2

    invoke-static/range {v2 .. v7}, Lcom/twitter/android/client/WidgetControl;->a(Landroid/content/Context;IIILjava/lang/String;I)Landroid/widget/RemoteViews;

    move-result-object v2

    invoke-static {v2, v10, v11}, Lcom/twitter/android/client/WidgetControl;->a(Landroid/widget/RemoteViews;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/twitter/android/WidgetSettingsActivity;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "android.intent.action.VIEW"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/twitter/android/client/WidgetControl;->e()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "appWidgetId"

    invoke-virtual {v3, v4, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v3

    const v4, 0x7f0902dc    # com.twitter.android.R.id.clear_view

    const/4 v5, 0x0

    const/high16 v6, 0x4000000

    invoke-static {p0, v5, v3, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v3

    invoke-virtual {v3, v13, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto :goto_1

    :cond_2
    if-nez p2, :cond_3

    invoke-virtual {p1, p0}, Lcom/twitter/android/client/cl;->a(Landroid/content/Context;)[I

    move-result-object v8

    if-eqz v8, :cond_0

    array-length v2, v8

    if-eqz v2, :cond_0

    iget v3, p1, Lcom/twitter/android/client/cl;->a:I

    iget v4, p1, Lcom/twitter/android/client/cl;->c:I

    const-string/jumbo v6, ""

    const/4 v7, 0x0

    move-object v2, p0

    move/from16 v5, p2

    invoke-static/range {v2 .. v7}, Lcom/twitter/android/client/WidgetControl;->a(Landroid/content/Context;IIILjava/lang/String;I)Landroid/widget/RemoteViews;

    move-result-object v2

    const v3, 0x7f0f0563    # com.twitter.android.R.string.widget_logged_out_title

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0f0562    # com.twitter.android.R.string.widget_logged_out_message

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/twitter/android/client/WidgetControl;->a(Landroid/widget/RemoteViews;Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v8

    :goto_2
    const v4, 0x7f0902dc    # com.twitter.android.R.id.clear_view

    const/4 v5, 0x0

    move-object/from16 v0, p3

    move/from16 v1, p4

    invoke-static {p0, v0, v1}, Lcom/twitter/android/client/WidgetControl;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v6

    const/high16 v7, 0x4000000

    invoke-static {p0, v5, v6, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v4

    invoke-virtual {v4, v3, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    goto/16 :goto_0

    :cond_3
    move/from16 v0, p4

    invoke-static {p1, v0}, Lcom/twitter/android/client/WidgetControl;->a(Lcom/twitter/android/client/cl;I)[I

    move-result-object v8

    if-eqz v8, :cond_0

    array-length v2, v8

    if-eqz v2, :cond_0

    iget v3, p1, Lcom/twitter/android/client/cl;->a:I

    iget v4, p1, Lcom/twitter/android/client/cl;->c:I

    move-object v2, p0

    move/from16 v5, p2

    move-object/from16 v6, p3

    move/from16 v7, p4

    invoke-static/range {v2 .. v7}, Lcom/twitter/android/client/WidgetControl;->a(Landroid/content/Context;IIILjava/lang/String;I)Landroid/widget/RemoteViews;

    move-result-object v2

    packed-switch p2, :pswitch_data_0

    const/4 v3, 0x0

    const v4, 0x7f0f04e8    # com.twitter.android.R.string.tweets_no_content

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/twitter/android/client/WidgetControl;->a(Landroid/widget/RemoteViews;Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v8

    goto :goto_2

    :pswitch_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/twitter/android/client/WidgetControl;->a(Landroid/widget/RemoteViews;Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v8

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method private a(Landroid/graphics/Bitmap;Lcom/twitter/android/client/cl;I)V
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x0

    const v7, 0x7f0902ff    # com.twitter.android.R.id.reply_item

    invoke-static {p2, p3}, Lcom/twitter/android/client/WidgetControl;->a(Lcom/twitter/android/client/cl;I)[I

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v0, v1

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v2, Landroid/widget/RemoteViews;

    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const v3, 0x7f03019a    # com.twitter.android.R.layout.widget_small_view

    invoke-direct {v2, v0, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    invoke-direct {p0, p3}, Lcom/twitter/android/client/WidgetControl;->e(I)Lcom/twitter/android/client/WidgetControl$WidgetList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/WidgetControl$WidgetList;->size()I

    move-result v3

    if-lez v3, :cond_3

    invoke-virtual {v0, v8}, Lcom/twitter/android/client/WidgetControl$WidgetList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/Tweet;

    invoke-direct {p0, v2, v0, p1}, Lcom/twitter/android/client/WidgetControl;->a(Landroid/widget/RemoteViews;Lcom/twitter/library/provider/Tweet;Landroid/graphics/Bitmap;)V

    iget-wide v3, v0, Lcom/twitter/library/provider/Tweet;->n:J

    iget-wide v5, p0, Lcom/twitter/android/client/WidgetControl;->e:J

    cmp-long v3, v3, v5

    if-nez v3, :cond_2

    invoke-virtual {v2, v7, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v2, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    iget-object v3, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    sget-object v4, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->c:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    invoke-static {v3, v4}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Landroid/content/Context;Lcom/twitter/android/composer/ComposerIntentWrapper$Action;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v3

    sget-object v4, Lcom/twitter/library/provider/at;->c:Landroid/net/Uri;

    iget-wide v5, v0, Lcom/twitter/library/provider/Tweet;->o:J

    invoke-static {v4, v5, v6}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Landroid/net/Uri;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    const/high16 v3, 0x14000000

    invoke-virtual {v0, v3}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(I)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/client/WidgetControl;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Ljava/lang/String;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    const-string/jumbo v3, "widget::::click"

    invoke-virtual {v0, v3}, Lcom/twitter/android/composer/ComposerIntentWrapper;->b(Ljava/lang/String;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a()Landroid/content/Intent;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    const/high16 v4, 0x10000000

    invoke-static {v3, v8, v0, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v2, v7, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto :goto_1

    :cond_3
    invoke-virtual {v2, v7, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_1
.end method

.method private a(Landroid/widget/RemoteViews;Lcom/twitter/library/provider/Tweet;Landroid/graphics/Bitmap;)V
    .locals 11

    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget-object v0, Lcom/twitter/android/client/WidgetControl;->g:Landroid/text/style/TextAppearanceSpan;

    if-nez v0, :cond_0

    const/high16 v0, 0x41600000    # 14.0f

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v3, v0

    const v0, 0x7f0b0060    # com.twitter.android.R.color.link

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v0}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v4

    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Landroid/text/style/TextAppearanceSpan;-><init>(Ljava/lang/String;IILandroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;)V

    sput-object v0, Lcom/twitter/android/client/WidgetControl;->g:Landroid/text/style/TextAppearanceSpan;

    :cond_0
    iget-object v2, p2, Lcom/twitter/library/provider/Tweet;->p:Ljava/lang/String;

    iget-object v3, p2, Lcom/twitter/library/provider/Tweet;->d:Ljava/lang/String;

    iget-wide v4, p2, Lcom/twitter/library/provider/Tweet;->h:J

    iget-wide v0, p2, Lcom/twitter/library/provider/Tweet;->j:J

    const-wide/16 v7, 0x0

    cmp-long v0, v0, v7

    if-lez v0, :cond_3

    invoke-static {v3}, Lcom/twitter/library/api/ap;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_0
    new-instance v7, Landroid/text/SpannableStringBuilder;

    invoke-direct {v7}, Landroid/text/SpannableStringBuilder;-><init>()V

    if-eqz v2, :cond_1

    invoke-virtual {v7, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    sget-object v0, Lcom/twitter/android/client/WidgetControl;->g:Landroid/text/style/TextAppearanceSpan;

    const/4 v8, 0x0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v9, 0x21

    invoke-virtual {v7, v0, v8, v2, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_1
    if-eqz v3, :cond_2

    const/16 v0, 0x20

    invoke-virtual {v7, v0}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_2
    iget-object v2, p2, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    if-eqz v2, :cond_4

    iget-object v0, v2, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    iget-object v2, v2, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/UrlEntity;

    iget v8, v0, Lcom/twitter/library/api/UrlEntity;->start:I

    sub-int/2addr v8, v2

    iget v9, v0, Lcom/twitter/library/api/UrlEntity;->end:I

    sub-int/2addr v9, v2

    if-ltz v8, :cond_8

    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v10

    if-gt v9, v10, :cond_8

    iget-object v0, v0, Lcom/twitter/library/api/UrlEntity;->displayUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_8

    invoke-virtual {v7, v8, v9, v0}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v8

    sub-int v0, v9, v0

    add-int/2addr v2, v0

    move v0, v2

    :goto_2
    move v2, v0

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0

    :cond_4
    const v0, 0x7f09009d    # com.twitter.android.R.id.text_item

    invoke-virtual {p1, v0, v7}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    sget-object v0, Lcom/twitter/android/client/WidgetControl;->f:Ljava/text/SimpleDateFormat;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    if-eqz v1, :cond_5

    const v2, 0x7f090099    # com.twitter.android.R.id.time_item

    const v3, 0x7f0f04f8    # com.twitter.android.R.string.tweets_time_and_reply

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v1, v4, v0

    invoke-virtual {v6, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    :goto_3
    if-eqz p3, :cond_7

    const v0, 0x7f09005e    # com.twitter.android.R.id.user_image

    invoke-virtual {p1, v0, p3}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    :goto_4
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    const-class v2, Lcom/twitter/android/TweetActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/twitter/android/client/WidgetControl;->e()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-wide v1, p2, Lcom/twitter/library/provider/Tweet;->C:J

    iget-wide v3, p0, Lcom/twitter/android/client/WidgetControl;->e:J

    invoke-static {v1, v2, v3, v4}, Lcom/twitter/library/provider/w;->a(JJ)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "sb_account_name"

    iget-object v2, p0, Lcom/twitter/android/client/WidgetControl;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "ref_event"

    const-string/jumbo v2, "widget::::click"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const v1, 0x7f0902e1    # com.twitter.android.R.id.user_status

    iget-object v2, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    const/4 v3, 0x0

    const/high16 v4, 0x10000000

    invoke-static {v2, v3, v0, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    return-void

    :cond_5
    iget-object v1, p2, Lcom/twitter/library/provider/Tweet;->E:Lcom/twitter/library/api/geo/TwitterPlace;

    if-eqz v1, :cond_6

    iget-object v1, p2, Lcom/twitter/library/provider/Tweet;->E:Lcom/twitter/library/api/geo/TwitterPlace;

    iget-object v1, v1, Lcom/twitter/library/api/geo/TwitterPlace;->fullName:Ljava/lang/String;

    if-eqz v1, :cond_6

    const v1, 0x7f090099    # com.twitter.android.R.id.time_item

    const v2, 0x7f0f04f7    # com.twitter.android.R.string.tweets_time_and_location

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    iget-object v4, p2, Lcom/twitter/library/provider/Tweet;->E:Lcom/twitter/library/api/geo/TwitterPlace;

    iget-object v4, v4, Lcom/twitter/library/api/geo/TwitterPlace;->fullName:Ljava/lang/String;

    aput-object v4, v3, v0

    invoke-virtual {v6, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_6
    const v1, 0x7f090099    # com.twitter.android.R.id.time_item

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_7
    const v0, 0x7f09005e    # com.twitter.android.R.id.user_image

    const v1, 0x7f02003b    # com.twitter.android.R.drawable.bg_no_profile_photo_sm

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto/16 :goto_4

    :cond_8
    move v0, v2

    goto/16 :goto_2
.end method

.method private static a(Landroid/widget/RemoteViews;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    const/4 v1, 0x1

    const v6, 0x7f0902db    # com.twitter.android.R.id.text_content

    const v5, 0x7f0902da    # com.twitter.android.R.id.text_title

    const/16 v4, 0x8

    const/4 v2, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    :goto_1
    if-eqz v0, :cond_3

    invoke-virtual {p0, v5, p1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    invoke-virtual {p0, v5, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    :goto_2
    if-eqz v1, :cond_4

    invoke-virtual {p0, v6, p2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    invoke-virtual {p0, v6, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    :goto_3
    if-nez v0, :cond_0

    if-eqz v1, :cond_5

    :cond_0
    const v0, 0x7f0902dd    # com.twitter.android.R.id.text_title_content

    invoke-virtual {p0, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v0, 0x7f0900e0    # com.twitter.android.R.id.loading

    invoke-virtual {p0, v0, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    :goto_4
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    invoke-virtual {p0, v5, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_2

    :cond_4
    invoke-virtual {p0, v6, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_3

    :cond_5
    const v0, 0x7f0902dd    # com.twitter.android.R.id.text_title_content

    invoke-virtual {p0, v0, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v0, 0x7f0900e0    # com.twitter.android.R.id.loading

    invoke-virtual {p0, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_4
.end method

.method private a(Lcom/twitter/library/provider/Tweet;Landroid/graphics/Bitmap;Lcom/twitter/android/client/cl;I)V
    .locals 3

    iget v0, p3, Lcom/twitter/android/client/cl;->a:I

    const v1, 0x7f060004    # com.twitter.android.R.xml.appwidget_large_provider

    if-ne v0, v1, :cond_1

    sget-boolean v0, Lcom/twitter/android/client/WidgetControl;->c:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p3, p4}, Lcom/twitter/android/client/WidgetControl;->c(Lcom/twitter/android/client/cl;I)V

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    const/4 v1, 0x1

    const-string/jumbo v2, ""

    invoke-static {v0, p3, v1, v2, p4}, Lcom/twitter/android/client/WidgetControl;->a(Landroid/content/Context;Lcom/twitter/android/client/cl;ILjava/lang/String;I)V

    return-void

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/client/WidgetControl;->b(Lcom/twitter/library/provider/Tweet;Landroid/graphics/Bitmap;Lcom/twitter/android/client/cl;I)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, p2, p3, p4}, Lcom/twitter/android/client/WidgetControl;->a(Landroid/graphics/Bitmap;Lcom/twitter/android/client/cl;I)V

    goto :goto_0
.end method

.method private a(Ljava/util/HashMap;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/client/WidgetControl;->a(Ljava/util/HashMap;I)V

    const/4 v0, 0x5

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/client/WidgetControl;->a(Ljava/util/HashMap;I)V

    return-void
.end method

.method private a(Ljava/util/HashMap;I)V
    .locals 4

    invoke-direct {p0, p2}, Lcom/twitter/android/client/WidgetControl;->e(I)Lcom/twitter/android/client/WidgetControl$WidgetList;

    move-result-object v2

    iget v0, v2, Lcom/twitter/android/client/WidgetControl$WidgetList;->mCurrentListIndex:I

    invoke-virtual {v2}, Lcom/twitter/android/client/WidgetControl$WidgetList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {v2, v0}, Lcom/twitter/android/client/WidgetControl$WidgetList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->y()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/twitter/android/client/WidgetControl;->j:Lcom/twitter/android/client/cl;

    invoke-direct {p0, v0, v1, v3, p2}, Lcom/twitter/android/client/WidgetControl;->a(Lcom/twitter/library/provider/Tweet;Landroid/graphics/Bitmap;Lcom/twitter/android/client/cl;I)V

    :cond_0
    invoke-virtual {v2}, Lcom/twitter/android/client/WidgetControl$WidgetList;->size()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/twitter/android/client/WidgetControl$WidgetList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->y()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    if-eqz v1, :cond_1

    iget-object v1, v1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/twitter/android/client/WidgetControl;->k:Lcom/twitter/android/client/cl;

    invoke-direct {p0, v0, v1, v2, p2}, Lcom/twitter/android/client/WidgetControl;->a(Lcom/twitter/library/provider/Tweet;Landroid/graphics/Bitmap;Lcom/twitter/android/client/cl;I)V

    :cond_1
    return-void
.end method

.method private static a(Lcom/twitter/android/client/cl;I)[I
    .locals 1

    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/client/cl;->c()[I

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/client/cl;->b()[I

    move-result-object v0

    goto :goto_0
.end method

.method private b(Landroid/widget/RemoteViews;Lcom/twitter/library/provider/Tweet;Landroid/graphics/Bitmap;)V
    .locals 7

    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    iget-wide v4, p0, Lcom/twitter/android/client/WidgetControl;->e:J

    iget-object v6, p0, Lcom/twitter/android/client/WidgetControl;->d:Ljava/lang/String;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-static/range {v0 .. v6}, Lcom/twitter/android/client/WidgetControl;->a(Landroid/content/Context;Landroid/widget/RemoteViews;Lcom/twitter/library/provider/Tweet;Landroid/graphics/Bitmap;JLjava/lang/String;)V

    return-void
.end method

.method private b(Lcom/twitter/android/client/cl;I)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p2}, Lcom/twitter/android/client/WidgetControl;->e(I)Lcom/twitter/android/client/WidgetControl$WidgetList;

    move-result-object v1

    iget v0, p1, Lcom/twitter/android/client/cl;->a:I

    const v2, 0x7f060004    # com.twitter.android.R.xml.appwidget_large_provider

    if-ne v0, v2, :cond_1

    iget v0, v1, Lcom/twitter/android/client/WidgetControl$WidgetList;->mCurrentListIndex:I

    :goto_1
    invoke-virtual {v1}, Lcom/twitter/android/client/WidgetControl$WidgetList;->size()I

    move-result v2

    if-nez v2, :cond_2

    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/twitter/android/client/WidgetControl;->d:Ljava/lang/String;

    invoke-static {v0, p1, v1, v2, p2}, Lcom/twitter/android/client/WidgetControl;->a(Landroid/content/Context;Lcom/twitter/android/client/cl;ILjava/lang/String;I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    invoke-virtual {v1, v0}, Lcom/twitter/android/client/WidgetControl$WidgetList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/Tweet;

    iget-object v1, v0, Lcom/twitter/library/provider/Tweet;->k:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->y()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/client/WidgetControl;->n:Lcom/twitter/library/util/ao;

    iget-wide v3, p0, Lcom/twitter/android/client/WidgetControl;->e:J

    iget-object v5, v0, Lcom/twitter/library/provider/Tweet;->k:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v1, v5}, Lcom/twitter/library/util/ao;->a(JLjava/lang/String;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    :goto_2
    invoke-direct {p0, v0, v1, p1, p2}, Lcom/twitter/android/client/WidgetControl;->a(Lcom/twitter/library/provider/Tweet;Landroid/graphics/Bitmap;Lcom/twitter/android/client/cl;I)V

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_2
.end method

.method private b(Lcom/twitter/library/provider/Tweet;Landroid/graphics/Bitmap;Lcom/twitter/android/client/cl;I)V
    .locals 9

    invoke-static {p3, p4}, Lcom/twitter/android/client/WidgetControl;->a(Lcom/twitter/android/client/cl;I)[I

    move-result-object v6

    if-eqz v6, :cond_0

    array-length v0, v6

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    iget v1, p3, Lcom/twitter/android/client/cl;->a:I

    iget v2, p3, Lcom/twitter/android/client/cl;->b:I

    const/4 v3, 0x4

    iget-object v4, p0, Lcom/twitter/android/client/WidgetControl;->d:Ljava/lang/String;

    move v5, p4

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/client/WidgetControl;->a(Landroid/content/Context;IIILjava/lang/String;I)Landroid/widget/RemoteViews;

    move-result-object v0

    if-nez p1, :cond_2

    const v1, 0x7f0902e2    # com.twitter.android.R.id.widget_button_prev

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v1, 0x7f0902e3    # com.twitter.android.R.id.widget_button_next

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    iget-object v1, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    invoke-static {v1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    invoke-virtual {v1, v6, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    goto :goto_0

    :cond_2
    const v1, 0x7f0902e2    # com.twitter.android.R.id.widget_button_prev

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v1, 0x7f0902e3    # com.twitter.android.R.id.widget_button_next

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-direct {p0, v0, p1, p2}, Lcom/twitter/android/client/WidgetControl;->b(Landroid/widget/RemoteViews;Lcom/twitter/library/provider/Tweet;Landroid/graphics/Bitmap;)V

    invoke-static {}, Lcom/twitter/android/client/WidgetControl;->e()I

    move-result v1

    invoke-direct {p0, p4}, Lcom/twitter/android/client/WidgetControl;->e(I)Lcom/twitter/android/client/WidgetControl$WidgetList;

    move-result-object v2

    iget v3, v2, Lcom/twitter/android/client/WidgetControl$WidgetList;->mCurrentListIndex:I

    if-nez v3, :cond_3

    const v4, 0x7f0902e2    # com.twitter.android.R.id.widget_button_prev

    const v5, 0x7f0201fd    # com.twitter.android.R.drawable.ic_prev_widget_disabled

    invoke-virtual {v0, v4, v5}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    :goto_1
    invoke-virtual {v2}, Lcom/twitter/android/client/WidgetControl$WidgetList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v3, v2, :cond_4

    const v1, 0x7f0902e3    # com.twitter.android.R.id.widget_button_next

    const v2, 0x7f0201e1    # com.twitter.android.R.drawable.ic_next_widget_disabled

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    :goto_2
    iget-object v1, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    invoke-static {v1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    invoke-virtual {v1, v6, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    goto :goto_0

    :cond_3
    const v4, 0x7f0902e2    # com.twitter.android.R.id.widget_button_prev

    const v5, 0x7f020302    # com.twitter.android.R.drawable.widget_prev_button

    invoke-virtual {v0, v4, v5}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    new-instance v4, Landroid/content/Intent;

    sget-object v5, Lcom/twitter/android/client/WidgetControl;->b:Ljava/lang/String;

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, "owner_id"

    iget-wide v7, p0, Lcom/twitter/android/client/WidgetControl;->e:J

    invoke-virtual {v4, v5, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string/jumbo v5, "status_type"

    invoke-virtual {v4, v5, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const v5, 0x7f0902e2    # com.twitter.android.R.id.widget_button_prev

    iget-object v7, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    const/high16 v8, 0x10000000

    invoke-static {v7, v1, v4, v8}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v0, v5, v4}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto :goto_1

    :cond_4
    const v2, 0x7f0902e3    # com.twitter.android.R.id.widget_button_next

    const v3, 0x7f020300    # com.twitter.android.R.drawable.widget_next_button

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    new-instance v2, Landroid/content/Intent;

    sget-object v3, Lcom/twitter/android/client/WidgetControl;->a:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "owner_id"

    iget-wide v4, p0, Lcom/twitter/android/client/WidgetControl;->e:J

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string/jumbo v3, "status_type"

    invoke-virtual {v2, v3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const v3, 0x7f0902e3    # com.twitter.android.R.id.widget_button_next

    iget-object v4, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    const/high16 v5, 0x10000000

    invoke-static {v4, v1, v2, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto :goto_2
.end method

.method public static c()I
    .locals 1

    sget-boolean v0, Lcom/twitter/android/client/WidgetControl;->c:Z

    if-eqz v0, :cond_0

    const v0, 0x7f03018f    # com.twitter.android.R.layout.widget_scrollable_view

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f03018d    # com.twitter.android.R.layout.widget_large_view

    goto :goto_0
.end method

.method private c(Lcom/twitter/android/client/cl;I)V
    .locals 12
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    const/4 v7, 0x0

    const v11, 0x7f0902e4    # com.twitter.android.R.id.list_view

    invoke-static {p1, p2}, Lcom/twitter/android/client/WidgetControl;->a(Lcom/twitter/android/client/cl;I)[I

    move-result-object v8

    if-nez v8, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v9

    move v6, v7

    :goto_1
    array-length v0, v8

    if-ge v6, v0, :cond_1

    new-instance v10, Landroid/content/Intent;

    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    invoke-static {}, Lcom/twitter/android/client/bl;->a()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v10, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v0, "appWidgetId"

    aget v1, v8, v6

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v0, "ownerId"

    iget-wide v1, p0, Lcom/twitter/android/client/WidgetControl;->e:J

    invoke-virtual {v10, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string/jumbo v0, "contentType"

    invoke-virtual {v10, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v0, "accountName"

    iget-object v1, p0, Lcom/twitter/android/client/WidgetControl;->d:Ljava/lang/String;

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v0, 0x1

    invoke-virtual {v10, v0}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    iget v1, p1, Lcom/twitter/android/client/cl;->a:I

    iget v2, p1, Lcom/twitter/android/client/cl;->b:I

    const/4 v3, 0x4

    iget-object v4, p0, Lcom/twitter/android/client/WidgetControl;->d:Ljava/lang/String;

    move v5, p2

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/client/WidgetControl;->a(Landroid/content/Context;IIILjava/lang/String;I)Landroid/widget/RemoteViews;

    move-result-object v0

    aget v1, v8, v6

    invoke-virtual {v0, v1, v11, v10}, Landroid/widget/RemoteViews;->setRemoteAdapter(IILandroid/content/Intent;)V

    const v1, 0x7f0902e5    # com.twitter.android.R.id.empty_view

    invoke-virtual {v0, v11, v1}, Landroid/widget/RemoteViews;->setEmptyView(II)V

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    const-class v3, Lcom/twitter/android/TweetActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "android.intent.action.VIEW"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/twitter/android/client/WidgetControl;->e()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    const/high16 v3, 0x10000000

    invoke-static {v2, v7, v1, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v11, v1}, Landroid/widget/RemoteViews;->setPendingIntentTemplate(ILandroid/app/PendingIntent;)V

    aget v1, v8, v6

    invoke-virtual {v9, v1, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto/16 :goto_1

    :cond_1
    invoke-virtual {p1}, Lcom/twitter/android/client/cl;->b()[I

    move-result-object v0

    invoke-virtual {v9, v0, v11}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged([II)V

    goto/16 :goto_0
.end method

.method private static declared-synchronized e()I
    .locals 2

    const-class v1, Lcom/twitter/android/client/WidgetControl;

    monitor-enter v1

    :try_start_0
    sget v0, Lcom/twitter/android/client/WidgetControl;->h:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/twitter/android/client/WidgetControl;->h:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private e(I)Lcom/twitter/android/client/WidgetControl$WidgetList;
    .locals 2

    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->i:[Lcom/twitter/android/client/WidgetControl$WidgetList;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->i:[Lcom/twitter/android/client/WidgetControl$WidgetList;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_0
.end method


# virtual methods
.method a()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/client/WidgetControl;->j:Lcom/twitter/android/client/cl;

    iget-object v2, p0, Lcom/twitter/android/client/WidgetControl;->j:Lcom/twitter/android/client/cl;

    invoke-virtual {v2, v0}, Lcom/twitter/android/client/cl;->a(Landroid/content/Context;)[I

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/client/cl;->a(Landroid/content/Context;[I)V

    iget-object v1, p0, Lcom/twitter/android/client/WidgetControl;->k:Lcom/twitter/android/client/cl;

    iget-object v2, p0, Lcom/twitter/android/client/WidgetControl;->k:Lcom/twitter/android/client/cl;

    invoke-virtual {v2, v0}, Lcom/twitter/android/client/cl;->a(Landroid/content/Context;)[I

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/client/cl;->a(Landroid/content/Context;[I)V

    return-void
.end method

.method public a(I)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/twitter/android/client/WidgetControl;->e(I)Lcom/twitter/android/client/WidgetControl$WidgetList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/WidgetControl$WidgetList;->size()I

    move-result v1

    iget v2, v0, Lcom/twitter/android/client/WidgetControl$WidgetList;->mCurrentListIndex:I

    if-eqz v1, :cond_0

    add-int/lit8 v1, v1, -0x1

    if-ge v2, v1, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/client/WidgetControl$WidgetList;->a()V

    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->j:Lcom/twitter/android/client/cl;

    invoke-direct {p0, v0, p1}, Lcom/twitter/android/client/WidgetControl;->b(Lcom/twitter/android/client/cl;I)V

    goto :goto_0
.end method

.method a(IJLjava/util/ArrayList;)V
    .locals 8

    const/4 v7, 0x3

    const/16 v6, 0x14

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/android/client/WidgetControl;->e(I)Lcom/twitter/android/client/WidgetControl$WidgetList;

    move-result-object v3

    const-wide/16 v4, 0x0

    cmp-long v0, p2, v4

    if-nez v0, :cond_1

    invoke-virtual {v3}, Lcom/twitter/android/client/WidgetControl$WidgetList;->clear()V

    :cond_1
    invoke-virtual {v3}, Lcom/twitter/android/client/WidgetControl$WidgetList;->size()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v3, v1, p4}, Lcom/twitter/android/client/WidgetControl$WidgetList;->addAll(ILjava/util/Collection;)Z

    invoke-virtual {v3}, Lcom/twitter/android/client/WidgetControl$WidgetList;->size()I

    move-result v2

    if-nez v2, :cond_3

    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/client/WidgetControl;->j:Lcom/twitter/android/client/cl;

    iget-object v2, p0, Lcom/twitter/android/client/WidgetControl;->d:Ljava/lang/String;

    invoke-static {v0, v1, v7, v2, p1}, Lcom/twitter/android/client/WidgetControl;->a(Landroid/content/Context;Lcom/twitter/android/client/cl;ILjava/lang/String;I)V

    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/client/WidgetControl;->k:Lcom/twitter/android/client/cl;

    iget-object v2, p0, Lcom/twitter/android/client/WidgetControl;->d:Ljava/lang/String;

    invoke-static {v0, v1, v7, v2, p1}, Lcom/twitter/android/client/WidgetControl;->a(Landroid/content/Context;Lcom/twitter/android/client/cl;ILjava/lang/String;I)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    :goto_2
    if-le v2, v6, :cond_4

    invoke-virtual {v3, v6}, Lcom/twitter/android/client/WidgetControl$WidgetList;->remove(I)Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/twitter/android/client/WidgetControl$WidgetList;->size()I

    move-result v2

    goto :goto_2

    :cond_4
    iget v2, v3, Lcom/twitter/android/client/WidgetControl$WidgetList;->mCurrentListIndex:I

    invoke-virtual {p4}, Ljava/util/ArrayList;->size()I

    move-result v4

    sget-boolean v5, Lcom/twitter/android/client/WidgetControl;->c:Z

    if-nez v5, :cond_5

    if-nez v0, :cond_5

    if-le v2, v4, :cond_5

    add-int v0, v2, v4

    if-lt v0, v6, :cond_6

    :cond_5
    iput v1, v3, Lcom/twitter/android/client/WidgetControl$WidgetList;->mCurrentListIndex:I

    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->j:Lcom/twitter/android/client/cl;

    invoke-direct {p0, v0, p1}, Lcom/twitter/android/client/WidgetControl;->b(Lcom/twitter/android/client/cl;I)V

    :cond_6
    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->k:Lcom/twitter/android/client/cl;

    invoke-direct {p0, v0, p1}, Lcom/twitter/android/client/WidgetControl;->b(Lcom/twitter/android/client/cl;I)V

    goto :goto_0
.end method

.method public a(J)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/WidgetControl;->e(I)Lcom/twitter/android/client/WidgetControl$WidgetList;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/client/WidgetControl;->a(JLcom/twitter/android/client/WidgetControl$WidgetList;)V

    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/twitter/android/client/WidgetControl;->e(I)Lcom/twitter/android/client/WidgetControl$WidgetList;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/client/WidgetControl;->a(JLcom/twitter/android/client/WidgetControl$WidgetList;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    invoke-direct {p0, p2}, Lcom/twitter/android/client/WidgetControl;->a(Ljava/util/HashMap;)V

    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/android/client/WidgetControl;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/client/WidgetControl;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v1, p0, Lcom/twitter/android/client/WidgetControl;->n:Lcom/twitter/library/util/ao;

    invoke-virtual {v1, p0}, Lcom/twitter/library/util/ao;->b(Lcom/twitter/library/util/ar;)V

    iget-object v1, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    const-class v4, Lcom/twitter/android/client/WidgetService;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "close"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "owner_id"

    iget-wide v4, p0, Lcom/twitter/android/client/WidgetControl;->e:J

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "widget_state"

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public b()V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->n:Lcom/twitter/library/util/ao;

    invoke-virtual {v0, p0}, Lcom/twitter/library/util/ao;->a(Lcom/twitter/library/util/ar;)V

    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    const-class v3, Lcom/twitter/android/client/WidgetService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "open"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "owner_id"

    iget-wide v3, p0, Lcom/twitter/android/client/WidgetControl;->e:J

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method public b(I)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/twitter/android/client/WidgetControl;->e(I)Lcom/twitter/android/client/WidgetControl$WidgetList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/WidgetControl$WidgetList;->size()I

    move-result v1

    iget v2, v0, Lcom/twitter/android/client/WidgetControl$WidgetList;->mCurrentListIndex:I

    if-eqz v1, :cond_0

    if-lez v2, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/client/WidgetControl$WidgetList;->b()V

    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->j:Lcom/twitter/android/client/cl;

    invoke-direct {p0, v0, p1}, Lcom/twitter/android/client/WidgetControl;->b(Lcom/twitter/android/client/cl;I)V

    goto :goto_0
.end method

.method public b(Z)V
    .locals 7

    const-wide/16 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    const-class v5, Lcom/twitter/android/client/WidgetService;

    invoke-direct {v0, v1, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "refresh"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "owner_id"

    iget-wide v5, p0, Lcom/twitter/android/client/WidgetControl;->e:J

    invoke-virtual {v0, v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v5

    const-string/jumbo v6, "latest_time_tweets"

    if-eqz p1, :cond_1

    move-wide v0, v2

    :goto_0
    invoke-virtual {v5, v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "latest_time_mentions"

    if-eqz p1, :cond_2

    :goto_1
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->i:[Lcom/twitter/android/client/WidgetControl$WidgetList;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-direct {p0, v0}, Lcom/twitter/android/client/WidgetControl;->a(Lcom/twitter/android/client/WidgetControl$WidgetList;)J

    move-result-wide v0

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/twitter/android/client/WidgetControl;->i:[Lcom/twitter/android/client/WidgetControl$WidgetList;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-direct {p0, v2}, Lcom/twitter/android/client/WidgetControl;->a(Lcom/twitter/android/client/WidgetControl$WidgetList;)J

    move-result-wide v2

    goto :goto_1
.end method

.method public c(I)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const v0, 0x7f060004    # com.twitter.android.R.xml.appwidget_large_provider

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->j:Lcom/twitter/android/client/cl;

    :goto_1
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/client/WidgetControl;->b(Lcom/twitter/android/client/cl;I)V

    const/4 v1, 0x5

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/client/WidgetControl;->b(Lcom/twitter/android/client/cl;I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->k:Lcom/twitter/android/client/cl;

    goto :goto_1
.end method

.method public d()Lcom/twitter/library/util/ao;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->n:Lcom/twitter/library/util/ao;

    return-object v0
.end method

.method public d(I)V
    .locals 5

    const/4 v4, 0x5

    const/4 v3, 0x0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/WidgetControl;->l:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/client/WidgetControl;->j:Lcom/twitter/android/client/cl;

    iget-object v2, p0, Lcom/twitter/android/client/WidgetControl;->d:Ljava/lang/String;

    invoke-static {v0, v1, p1, v2, v3}, Lcom/twitter/android/client/WidgetControl;->a(Landroid/content/Context;Lcom/twitter/android/client/cl;ILjava/lang/String;I)V

    iget-object v1, p0, Lcom/twitter/android/client/WidgetControl;->j:Lcom/twitter/android/client/cl;

    iget-object v2, p0, Lcom/twitter/android/client/WidgetControl;->d:Ljava/lang/String;

    invoke-static {v0, v1, p1, v2, v4}, Lcom/twitter/android/client/WidgetControl;->a(Landroid/content/Context;Lcom/twitter/android/client/cl;ILjava/lang/String;I)V

    iget-object v1, p0, Lcom/twitter/android/client/WidgetControl;->k:Lcom/twitter/android/client/cl;

    iget-object v2, p0, Lcom/twitter/android/client/WidgetControl;->d:Ljava/lang/String;

    invoke-static {v0, v1, p1, v2, v3}, Lcom/twitter/android/client/WidgetControl;->a(Landroid/content/Context;Lcom/twitter/android/client/cl;ILjava/lang/String;I)V

    iget-object v1, p0, Lcom/twitter/android/client/WidgetControl;->k:Lcom/twitter/android/client/cl;

    iget-object v2, p0, Lcom/twitter/android/client/WidgetControl;->d:Ljava/lang/String;

    invoke-static {v0, v1, p1, v2, v4}, Lcom/twitter/android/client/WidgetControl;->a(Landroid/content/Context;Lcom/twitter/android/client/cl;ILjava/lang/String;I)V

    goto :goto_0
.end method
