.class Lcom/twitter/android/client/cc;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/twitter/library/util/aa;

.field final synthetic c:J

.field final synthetic d:Lcom/twitter/android/client/cb;


# direct methods
.method constructor <init>(Lcom/twitter/android/client/cb;Ljava/lang/String;Lcom/twitter/library/util/aa;J)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/client/cc;->d:Lcom/twitter/android/client/cb;

    iput-object p2, p0, Lcom/twitter/android/client/cc;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/twitter/android/client/cc;->b:Lcom/twitter/library/util/aa;

    iput-wide p4, p0, Lcom/twitter/android/client/cc;->c:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/client/cc;->d:Lcom/twitter/android/client/cb;

    invoke-static {v0}, Lcom/twitter/android/client/cb;->a(Lcom/twitter/android/client/cb;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/cc;->d:Lcom/twitter/android/client/cb;

    new-instance v2, Lcom/twitter/library/util/m;

    iget-object v3, p0, Lcom/twitter/android/client/cc;->a:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getWidth()I

    move-result v4

    invoke-virtual {v0}, Landroid/widget/ImageView;->getHeight()I

    move-result v5

    invoke-direct {v2, v3, v4, v5}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;II)V

    invoke-static {v1, v2}, Lcom/twitter/android/client/cb;->a(Lcom/twitter/android/client/cb;Lcom/twitter/library/util/m;)Lcom/twitter/library/util/m;

    iget-object v1, p0, Lcom/twitter/android/client/cc;->b:Lcom/twitter/library/util/aa;

    iget-wide v2, p0, Lcom/twitter/android/client/cc;->c:J

    iget-object v4, p0, Lcom/twitter/android/client/cc;->d:Lcom/twitter/android/client/cb;

    invoke-static {v4}, Lcom/twitter/android/client/cb;->b(Lcom/twitter/android/client/cb;)Lcom/twitter/library/util/m;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/util/aa;->a(JLcom/twitter/library/util/m;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method
