.class public Lcom/twitter/android/client/bx;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/client/bn;


# direct methods
.method public constructor <init>(Lcom/twitter/android/client/bn;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/client/bx;->a:Lcom/twitter/android/client/bn;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;IZLjava/lang/String;Lcom/twitter/library/api/search/TwitterTypeAheadGroup;)V
    .locals 1

    if-nez p5, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/bx;->a:Lcom/twitter/android/client/bn;

    invoke-static {v0}, Lcom/twitter/android/client/bn;->c(Lcom/twitter/android/client/bn;)Ljava/util/HashSet;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_0

    if-nez p6, :cond_0

    invoke-static {p7, p8}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Ljava/lang/String;Lcom/twitter/library/api/search/TwitterTypeAheadGroup;)V

    invoke-virtual {p8}, Lcom/twitter/library/api/search/TwitterTypeAheadGroup;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/bx;->a:Lcom/twitter/android/client/bn;

    invoke-virtual {v0}, Lcom/twitter/android/client/bn;->a()V

    :cond_0
    return-void
.end method

.method public l(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/bx;->a:Lcom/twitter/android/client/bn;

    invoke-static {v0}, Lcom/twitter/android/client/bn;->c(Lcom/twitter/android/client/bn;)Ljava/util/HashSet;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/bx;->a:Lcom/twitter/android/client/bn;

    invoke-virtual {v0}, Lcom/twitter/android/client/bn;->a()V

    :cond_0
    return-void
.end method
