.class public Lcom/twitter/android/client/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/util/ac;


# static fields
.field private static u:Lcom/twitter/android/client/c;


# instance fields
.field private final A:Ljava/util/HashMap;

.field private final B:Ljava/util/HashMap;

.field private final C:Ljava/util/HashMap;

.field private final D:Lcom/twitter/library/client/w;

.field private E:Landroid/support/v4/view/accessibility/AccessibilityManagerCompat$AccessibilityStateChangeListenerCompat;

.field private F:Lcom/twitter/library/client/aa;

.field private G:Lcom/twitter/android/util/d;

.field private H:Lcom/twitter/android/ii;

.field private final I:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private final J:Ljava/util/WeakHashMap;

.field private K:Lcom/twitter/android/client/aq;

.field private L:F

.field private M:Ljava/util/ArrayList;

.field private N:Lcom/twitter/library/api/UrlConfiguration;

.field private O:Z

.field private P:Z

.field private Q:Z

.field private R:Ljava/lang/String;

.field private S:Lcom/twitter/library/api/b;

.field private T:Lcom/twitter/library/api/ad;

.field private U:Z

.field private V:Lcom/twitter/library/platform/LocationProducer;

.field private W:Ljava/util/regex/Pattern;

.field private X:Z

.field private Y:Lcom/twitter/library/metrics/h;

.field public final a:Lcom/twitter/library/widget/ap;

.field public final b:Lcom/twitter/library/util/aa;

.field public final c:Lcom/twitter/library/util/as;

.field public final d:F

.field public final e:F

.field public f:Z

.field final g:Landroid/content/Context;

.field final h:Landroid/app/NotificationManager;

.field final i:Lcom/twitter/library/service/a;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field j:Z

.field k:Z

.field l:Z

.field m:Z

.field n:I

.field o:Z

.field p:Z

.field q:Z

.field r:Z

.field s:Z

.field t:Z

.field private v:Z

.field private final w:Landroid/util/SparseArray;

.field private final x:Ljava/util/ArrayList;

.field private final y:Ljava/util/HashMap;

.field private final z:Landroid/os/Handler;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 10

    const/high16 v6, 0x100000

    const/4 v2, 0x2

    const/4 v9, -0x1

    const/4 v8, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/twitter/android/client/p;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/p;-><init>(Lcom/twitter/android/client/c;)V

    iput-object v0, p0, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    iput-boolean v8, p0, Lcom/twitter/android/client/c;->q:Z

    iput-boolean v1, p0, Lcom/twitter/android/client/c;->v:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/c;->A:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/c;->B:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/c;->C:Ljava/util/HashMap;

    new-instance v0, Lcom/twitter/android/client/d;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/d;-><init>(Lcom/twitter/android/client/c;)V

    iput-object v0, p0, Lcom/twitter/android/client/c;->I:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/c;->J:Ljava/util/WeakHashMap;

    iput-boolean v8, p0, Lcom/twitter/android/client/c;->U:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/client/c;->W:Ljava/util/regex/Pattern;

    invoke-static {p1}, Lcom/twitter/library/util/b;->a(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/twitter/library/featureswitch/a;->a(Landroid/content/Context;)V

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/c;->a(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {p0}, Lcom/twitter/android/client/c;->am()V

    invoke-static {p1}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;)Lcom/twitter/android/util/d;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/c;->G:Lcom/twitter/android/util/d;

    const-string/jumbo v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/twitter/android/client/c;->h:Landroid/app/NotificationManager;

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/platform/LocationProducer;->a(Landroid/content/Context;)Lcom/twitter/library/platform/LocationProducer;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/c;->V:Lcom/twitter/library/platform/LocationProducer;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/android/client/c;->z:Landroid/os/Handler;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/c;->x:Ljava/util/ArrayList;

    invoke-static {p1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    new-instance v3, Lcom/twitter/android/client/o;

    invoke-direct {v3, p0}, Lcom/twitter/android/client/o;-><init>(Lcom/twitter/android/client/c;)V

    invoke-virtual {v0, v3}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/z;)V

    const-string/jumbo v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    new-instance v3, Landroid/util/SparseArray;

    const/4 v4, 0x3

    invoke-direct {v3, v4}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v3, p0, Lcom/twitter/android/client/c;->w:Landroid/util/SparseArray;

    new-instance v4, Lcom/twitter/library/util/ao;

    const-string/jumbo v5, "tweets"

    invoke-direct {v4, p1, v1, v6, v5}, Lcom/twitter/library/util/ao;-><init>(Landroid/content/Context;IILjava/lang/String;)V

    invoke-virtual {v3, v1, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v4, Lcom/twitter/library/util/ao;

    const-string/jumbo v5, "mini"

    invoke-direct {v4, p1, v2, v6, v5}, Lcom/twitter/library/util/ao;-><init>(Landroid/content/Context;IILjava/lang/String;)V

    invoke-virtual {v3, v2, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v3, Lcom/twitter/library/util/aa;

    invoke-direct {v3, p1, v0, v8}, Lcom/twitter/library/util/aa;-><init>(Landroid/content/Context;II)V

    iput-object v3, p0, Lcom/twitter/android/client/c;->b:Lcom/twitter/library/util/aa;

    iget-object v0, p0, Lcom/twitter/android/client/c;->b:Lcom/twitter/library/util/aa;

    invoke-virtual {v0, p0}, Lcom/twitter/library/util/aa;->a(Lcom/twitter/library/util/ac;)V

    new-instance v0, Lcom/twitter/library/util/as;

    invoke-direct {v0, p1, v8}, Lcom/twitter/library/util/as;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/twitter/android/client/c;->c:Lcom/twitter/library/util/as;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/c;->y:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/telephony/a;->a(Landroid/content/Context;)V

    new-instance v0, Lcom/twitter/android/ii;

    iget-object v3, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/twitter/android/ii;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/client/c;->H:Lcom/twitter/android/ii;

    invoke-virtual {p0}, Lcom/twitter/android/client/c;->d()V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v4, v3, Landroid/util/DisplayMetrics;->density:F

    iput v4, p0, Lcom/twitter/android/client/c;->d:F

    iget v3, v3, Landroid/util/DisplayMetrics;->scaledDensity:F

    iput v3, p0, Lcom/twitter/android/client/c;->e:F

    const v3, 0x7f0c0069    # com.twitter.android.R.dimen.font_size_medium

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/twitter/android/client/c;->L:F

    invoke-static {p1}, Lcom/twitter/library/client/w;->a(Landroid/content/Context;)Lcom/twitter/library/client/w;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/c;->D:Lcom/twitter/library/client/w;

    new-instance v0, Lcom/twitter/android/client/g;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/g;-><init>(Lcom/twitter/android/client/c;)V

    iput-object v0, p0, Lcom/twitter/android/client/c;->i:Lcom/twitter/library/service/a;

    new-instance v0, Lcom/twitter/android/client/h;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/h;-><init>(Lcom/twitter/android/client/c;)V

    iget-object v3, p0, Lcom/twitter/android/client/c;->D:Lcom/twitter/library/client/w;

    invoke-virtual {v3, v0}, Lcom/twitter/library/client/w;->a(Lcom/twitter/library/service/a;)V

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v3, "android_id"

    invoke-static {v0, v3}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/c;->R:Ljava/lang/String;

    invoke-static {p1}, Lcom/twitter/android/card/n;->a(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/twitter/android/client/al;->a(Landroid/content/Context;)V

    new-instance v0, Lcom/twitter/android/amplify/e;

    invoke-direct {v0}, Lcom/twitter/android/amplify/e;-><init>()V

    invoke-static {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Lcom/twitter/library/amplify/k;)V

    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string/jumbo v0, "sound_effects"

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/client/c;->j:Z

    const-string/jumbo v0, "font_size"

    const/4 v4, 0x0

    invoke-interface {v3, v0, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    iget v4, p0, Lcom/twitter/android/client/c;->e:F

    mul-float/2addr v0, v4

    iput v0, p0, Lcom/twitter/android/client/c;->L:F

    :cond_0
    const-string/jumbo v0, "location"

    invoke-interface {v3, v0, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/client/c;->k:Z

    iget-object v0, p0, Lcom/twitter/android/client/c;->V:Lcom/twitter/library/platform/LocationProducer;

    iget-boolean v4, p0, Lcom/twitter/android/client/c;->k:Z

    invoke-virtual {v0, v4}, Lcom/twitter/library/platform/LocationProducer;->a(Z)V

    const-string/jumbo v0, "lifeline_alerts_enabled"

    invoke-interface {v3, v0, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/client/c;->m:Z

    const-string/jumbo v0, "pb_enabled"

    invoke-interface {v3, v0, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/c;->b(I)V

    iget-object v0, p0, Lcom/twitter/android/client/c;->V:Lcom/twitter/library/platform/LocationProducer;

    const-string/jumbo v4, "geo_data_provider_enabled"

    invoke-interface {v3, v4, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    const-string/jumbo v5, "geo_data_provider_update_delay"

    invoke-interface {v3, v5, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    const-string/jumbo v6, "geo_data_provider_update_duration"

    invoke-interface {v3, v6, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    const-string/jumbo v7, "geo_data_provider_update_interval"

    invoke-interface {v3, v7, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    invoke-virtual {v0, v4, v5, v6, v7}, Lcom/twitter/library/platform/LocationProducer;->a(ZIII)V

    const-string/jumbo v0, "inline_follow_enabled"

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/client/c;->r:Z

    const-string/jumbo v0, "inline_follow_user_fetch_enabled"

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/client/c;->s:Z

    const-string/jumbo v0, "select_all_invited_friends"

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/client/c;->t:Z

    const-string/jumbo v0, "amplify_player_enabled"

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/client/c;->X:Z

    iget-object v0, p0, Lcom/twitter/android/client/c;->H:Lcom/twitter/android/ii;

    invoke-virtual {v0}, Lcom/twitter/android/ii;->a()V

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/twitter/android/client/c;->s(J)V

    const-string/jumbo v6, "app::::launch"

    invoke-virtual {p0, v4, v5, v6}, Lcom/twitter/android/client/c;->c(JLjava/lang/String;)V

    iget-object v6, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v6}, Lcom/twitter/library/telephony/TelephonyUtil;->d(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/twitter/android/client/c;->Z()V

    invoke-virtual {p0}, Lcom/twitter/android/client/c;->q()V

    invoke-static {}, Ljy;->a()V

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v6, v4, v5}, Lcom/twitter/library/provider/c;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/c;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/provider/c;->a()I

    invoke-virtual {p0, v0, v8}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/Session;Z)V

    :goto_0
    invoke-virtual {p0, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;)V

    new-instance v0, Lcom/twitter/android/client/i;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/client/i;-><init>(Lcom/twitter/android/client/c;Landroid/content/Context;)V

    invoke-static {v0}, Lju;->a(Ljv;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-lt v0, v4, :cond_1

    const-string/jumbo v0, "day_dream_service_2052"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v0, "day_dream_service_2052"

    invoke-static {v0}, Lkk;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_1
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    new-instance v4, Landroid/content/ComponentName;

    const-class v5, Lcom/twitter/android/client/TweetDreamService;

    invoke-direct {v4, p1, v5}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v4, v0, v1}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    :cond_1
    invoke-static {}, Lcom/twitter/library/featureswitch/a;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/twitter/android/client/aq;

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/twitter/android/client/aq;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/client/c;->K:Lcom/twitter/android/client/aq;

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    iget-object v2, p0, Lcom/twitter/android/client/c;->K:Lcom/twitter/android/client/aq;

    iget-object v2, v2, Lcom/twitter/android/client/aq;->b:Lcom/twitter/library/client/z;

    invoke-virtual {v0, v2}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/z;)V

    iget-object v0, p0, Lcom/twitter/android/client/c;->K:Lcom/twitter/android/client/aq;

    iget-object v0, v0, Lcom/twitter/android/client/aq;->a:Lcom/twitter/library/client/j;

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/j;)V

    :cond_2
    new-instance v0, Lcom/twitter/android/client/j;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/j;-><init>(Lcom/twitter/android/client/c;)V

    invoke-static {v0}, Lcom/twitter/library/client/l;->a(Lcom/twitter/library/client/k;)V

    const-string/jumbo v0, "media_forward_enabled"

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/client/c;->o:Z

    const-string/jumbo v0, "media_forward"

    sget-boolean v1, Lcom/twitter/library/provider/t;->a:Z

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->d(Z)V

    invoke-static {p1}, Lcom/twitter/library/platform/TwitterDataSyncService;->a(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/twitter/android/client/c;->I:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v3, v0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    return-void

    :cond_3
    const-wide/16 v4, 0x0

    invoke-virtual {p0, v4, v5}, Lcom/twitter/android/client/c;->n(J)V

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method static synthetic a(Lcom/twitter/android/client/c;F)F
    .locals 0

    iput p1, p0, Lcom/twitter/android/client/c;->L:F

    return p1
.end method

.method static synthetic a(Lcom/twitter/android/client/c;)Lcom/twitter/library/api/ad;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/c;->T:Lcom/twitter/library/api/ad;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/client/c;Lcom/twitter/library/api/ad;)Lcom/twitter/library/api/ad;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/client/c;->T:Lcom/twitter/library/api/ad;

    return-object p1
.end method

.method private a(JLcom/twitter/library/client/Session;)Lcom/twitter/library/client/u;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v0, v1, p3}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    const-wide/16 v1, 0x0

    cmp-long v1, p1, v1

    if-lez v1, :cond_0

    const-string/jumbo v1, "owner_id"

    invoke-virtual {v0, v1, p1, p2}, Lcom/twitter/library/client/u;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    :cond_0
    return-object v0
.end method

.method private a(Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/client/Session;)Lcom/twitter/library/client/u;
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v0, v1, p2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    if-eqz p1, :cond_0

    const-string/jumbo v1, "impression_id"

    iget-object v2, p1, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/u;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "earned"

    invoke-virtual {p1}, Lcom/twitter/library/api/PromotedContent;->b()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    :cond_0
    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/client/c;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/client/c;->d(Z)V

    return-void
.end method

.method private a(Lcom/twitter/library/api/Decider;)V
    .locals 5

    const/4 v4, -0x1

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/f;

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const-string/jumbo v3, "decider"

    invoke-direct {v1, v2, v0, v3}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "cache_version"

    invoke-virtual {v1, v0, v4}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v4, :cond_1

    iget v2, p1, Lcom/twitter/library/api/Decider;->q:I

    if-ge v0, v2, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v1, "cache_version"

    iget v2, p1, Lcom/twitter/library/api/Decider;->q:I

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;I)Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v1, "cache_dirty"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;Z)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v1}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v1, "cache_version"

    iget v2, p1, Lcom/twitter/library/api/Decider;->q:I

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;I)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V

    goto :goto_0
.end method

.method private a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/UserSettings;)V
    .locals 3

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/library/api/UserSettings;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/twitter/library/api/UserSettings;->p:Z

    iput-boolean v0, p2, Lcom/twitter/library/api/UserSettings;->p:Z

    :cond_0
    invoke-virtual {p1, p2}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/api/UserSettings;)V

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, p2}, Lcom/twitter/library/util/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/api/UserSettings;)V

    return-void
.end method

.method private a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/UserSettings;Ljava/lang/String;)V
    .locals 4

    iget-object v0, p2, Lcom/twitter/library/api/UserSettings;->m:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v1

    iput-object v0, v1, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/api/TwitterUser;)V

    invoke-static {v1}, Lcom/twitter/library/api/ap;->a(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->h()Lcom/twitter/library/network/OAuthToken;

    move-result-object v3

    invoke-static {v2, v0, v3, v1}, Lcom/twitter/library/util/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/network/OAuthToken;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, v0, v3, p2}, Lcom/twitter/library/util/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/api/UserSettings;)V

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    sget-object v3, Lcom/twitter/library/provider/w;->c:Ljava/lang/String;

    invoke-static {v2, p3, v3}, Lcom/twitter/library/util/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/twitter/library/provider/w;->c:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/twitter/library/util/a;->a(Landroid/accounts/Account;Ljava/lang/String;Z)V

    :cond_0
    invoke-virtual {p1, v0}, Lcom/twitter/library/client/Session;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0, p3}, Lcom/twitter/library/util/a;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/AccountManagerFuture;

    return-void
.end method

.method private a(Lcom/twitter/library/client/Session;Lcom/twitter/library/platform/e;)V
    .locals 12

    const-wide/16 v4, 0x0

    iget v9, p2, Lcom/twitter/library/platform/e;->c:I

    new-instance v0, Lcom/twitter/library/service/e;

    invoke-direct {v0}, Lcom/twitter/library/service/e;-><init>()V

    new-instance v10, Lcom/twitter/internal/android/service/k;

    invoke-direct {v10}, Lcom/twitter/internal/android/service/k;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/e;->a(Z)V

    invoke-virtual {v10, v0}, Lcom/twitter/internal/android/service/k;->a(Ljava/lang/Object;)V

    iget-object v11, p0, Lcom/twitter/android/client/c;->D:Lcom/twitter/library/client/w;

    new-instance v0, Ljl;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v3

    const/4 v8, 0x0

    move-object v2, p1

    move-wide v6, v4

    invoke-direct/range {v0 .. v8}, Ljl;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/api/TwitterUser;JJI)V

    invoke-virtual {v0, v9}, Ljl;->a(I)Ljl;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljl;->b(I)Ljl;

    move-result-object v0

    invoke-virtual {v11, v0, v10}, Lcom/twitter/library/client/w;->a(Lcom/twitter/library/service/b;Lcom/twitter/internal/android/service/k;)V

    return-void
.end method

.method private a(Lcom/twitter/library/platform/DataSyncResult;Landroid/content/Intent;)V
    .locals 16

    const-string/jumbo v1, "from_push"

    const/4 v2, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v12

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/twitter/library/platform/DataSyncResult;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v1, v14}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/twitter/android/client/c;->x:Ljava/util/ArrayList;

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/twitter/library/platform/DataSyncResult;->f:Lcom/twitter/library/platform/e;

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v1}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/Session;Z)V

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/twitter/library/platform/DataSyncResult;->f:Lcom/twitter/library/platform/e;

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/platform/e;)V

    :cond_2
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/twitter/library/platform/DataSyncResult;->i:Lcom/twitter/library/platform/e;

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v1}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/Session;Z)V

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v13, v1

    :goto_1
    if-ltz v13, :cond_3

    invoke-virtual {v15, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/client/j;

    const/4 v3, 0x0

    const/16 v4, 0xc8

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-wide/16 v7, 0x0

    const-wide/16 v9, 0x0

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/twitter/library/platform/DataSyncResult;->i:Lcom/twitter/library/platform/e;

    iget v11, v11, Lcom/twitter/library/platform/e;->c:I

    invoke-virtual/range {v1 .. v12}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;IJJIZ)V

    add-int/lit8 v1, v13, -0x1

    move v13, v1

    goto :goto_1

    :cond_3
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/twitter/library/platform/DataSyncResult;->g:Lcom/twitter/library/platform/e;

    if-eqz v1, :cond_4

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v3, v1

    :goto_2
    if-ltz v3, :cond_4

    invoke-virtual {v15, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/client/j;

    const/4 v4, 0x0

    const/16 v5, 0xc8

    const/4 v6, 0x0

    invoke-virtual {v1, v2, v4, v5, v6}, Lcom/twitter/library/client/j;->h(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V

    add-int/lit8 v1, v3, -0x1

    move v3, v1

    goto :goto_2

    :cond_4
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/twitter/library/platform/DataSyncResult;->h:Lcom/twitter/library/platform/e;

    if-eqz v1, :cond_5

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v7, v1

    :goto_3
    if-ltz v7, :cond_5

    invoke-virtual {v15, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/client/j;

    const/4 v3, 0x0

    const/16 v4, 0xc8

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;Z)V

    add-int/lit8 v1, v7, -0x1

    move v7, v1

    goto :goto_3

    :cond_5
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/twitter/library/platform/DataSyncResult;->j:Lcom/twitter/library/platform/e;

    if-eqz v1, :cond_6

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_4
    if-ltz v2, :cond_6

    invoke-virtual {v15, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/client/j;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/twitter/library/platform/DataSyncResult;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/twitter/library/client/j;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v1, v14}, Lcom/twitter/library/util/a;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v2}, Lcom/twitter/library/platform/PushService;->c(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v3, v1}, Lcom/twitter/library/platform/PushService;->d(Landroid/content/Context;Landroid/accounts/Account;)Z

    move-result v3

    if-nez v3, :cond_8

    :cond_7
    if-nez v2, :cond_a

    sget-object v2, Lcom/twitter/library/provider/w;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_a

    sget-object v2, Lcom/twitter/library/provider/w;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    :cond_8
    const/4 v1, 0x1

    :goto_5
    if-eqz v1, :cond_0

    const-string/jumbo v1, "show_notif"

    const/4 v2, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/util/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v1, v1

    if-lez v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/android/client/aw;->a(Landroid/content/Context;)Lcom/twitter/android/client/aw;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/aw;->a(Lcom/twitter/library/platform/DataSyncResult;)V

    goto/16 :goto_0

    :cond_9
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_4

    :cond_a
    const/4 v1, 0x0

    goto :goto_5
.end method

.method private am()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/util/Util;->o(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/twitter/android/client/c;->E:Landroid/support/v4/view/accessibility/AccessibilityManagerCompat$AccessibilityStateChangeListenerCompat;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/client/k;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/k;-><init>(Lcom/twitter/android/client/c;)V

    iput-object v0, p0, Lcom/twitter/android/client/c;->E:Landroid/support/v4/view/accessibility/AccessibilityManagerCompat$AccessibilityStateChangeListenerCompat;

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const-string/jumbo v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iget-object v1, p0, Lcom/twitter/android/client/c;->E:Landroid/support/v4/view/accessibility/AccessibilityManagerCompat$AccessibilityStateChangeListenerCompat;

    invoke-static {v0, v1}, Landroid/support/v4/view/accessibility/AccessibilityManagerCompat;->addAccessibilityStateChangeListener(Landroid/view/accessibility/AccessibilityManager;Landroid/support/v4/view/accessibility/AccessibilityManagerCompat$AccessibilityStateChangeListenerCompat;)Z

    :cond_0
    return-void
.end method

.method private an()Lcom/twitter/library/api/UrlConfiguration;
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/client/c;->N:Lcom/twitter/library/api/UrlConfiguration;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/c;->N:Lcom/twitter/library/api/UrlConfiguration;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const-string/jumbo v1, "config"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "short_url_len"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    const-string/jumbo v2, "scribe_url"

    const-string/jumbo v3, "https://twitter.com/scribe"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "url_whitelist"

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    if-eqz v0, :cond_1

    new-instance v4, Ljava/util/StringTokenizer;

    const-string/jumbo v5, ","

    invoke-direct {v4, v0, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/client/c;->o()V

    :cond_2
    new-instance v0, Lcom/twitter/library/api/UrlConfiguration;

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/api/UrlConfiguration;-><init>(ILjava/lang/String;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/twitter/android/client/c;->N:Lcom/twitter/library/api/UrlConfiguration;

    iget-object v0, p0, Lcom/twitter/android/client/c;->N:Lcom/twitter/library/api/UrlConfiguration;

    goto :goto_0
.end method

.method private ao()V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/featureswitch/b;->d(J)V

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/client/c;->m(J)V

    return-void
.end method

.method private declared-synchronized ap()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/twitter/android/client/c;->U:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/client/c;->o()V

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/c;->a(Landroid/content/SharedPreferences;)Lcom/twitter/library/api/ad;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, v1, Lcom/twitter/library/api/ad;->i:Ljava/util/HashMap;

    invoke-static {v2}, Lcom/twitter/library/network/d;->a(Ljava/util/HashMap;)V

    :goto_0
    const-string/jumbo v2, "data_alerts_links"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/android/client/c;->P:Z

    const-string/jumbo v2, "data_alerts_inline"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/client/c;->Q:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/client/c;->U:Z

    iput-object v1, p0, Lcom/twitter/android/client/c;->T:Lcom/twitter/library/api/ad;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    const/4 v2, 0x0

    :try_start_1
    invoke-static {v2}, Lcom/twitter/library/network/d;->a(Ljava/util/HashMap;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private aq()V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    iget-boolean v0, p0, Lcom/twitter/android/client/c;->P:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/c;->T:Lcom/twitter/library/api/ad;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/c;->T:Lcom/twitter/library/api/ad;

    iget-boolean v0, v0, Lcom/twitter/library/api/ad;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    iget-object v4, p0, Lcom/twitter/android/client/c;->x:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_1
    if-ltz v2, :cond_1

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/j;

    invoke-virtual {v0, v3, v1}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Z)V

    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private ar()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Lju;->d(J)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "experimentation"

    invoke-static {v1, v0}, Lcom/crashlytics/android/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static declared-synchronized b(Landroid/content/Context;)Lcom/twitter/android/client/c;
    .locals 7

    const-class v1, Lcom/twitter/android/client/c;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/android/client/c;->u:Lcom/twitter/android/client/c;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/twitter/library/client/App;->p()Lcom/twitter/library/client/AppFlavor;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/client/AppFlavor;)V

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v2, Lcom/twitter/library/metrics/o;

    const-string/jumbo v3, "app:init"

    const-wide/16 v4, 0x0

    const/4 v6, 0x2

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/twitter/library/metrics/o;-><init>(Ljava/lang/String;JI)V

    invoke-virtual {v2}, Lcom/twitter/library/metrics/o;->i()V

    new-instance v3, Lcom/twitter/android/client/c;

    invoke-direct {v3, v0}, Lcom/twitter/android/client/c;-><init>(Landroid/content/Context;)V

    sput-object v3, Lcom/twitter/android/client/c;->u:Lcom/twitter/android/client/c;

    sget-object v0, Lcom/twitter/android/client/c;->u:Lcom/twitter/android/client/c;

    iget-object v0, v0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/metrics/o;->b(J)V

    invoke-virtual {v2}, Lcom/twitter/library/metrics/o;->j()V

    sget-object v0, Lcom/twitter/android/client/c;->u:Lcom/twitter/android/client/c;

    iget-object v0, v0, Lcom/twitter/android/client/c;->Y:Lcom/twitter/library/metrics/h;

    invoke-virtual {v0, v2}, Lcom/twitter/library/metrics/h;->a(Lcom/twitter/library/metrics/e;)V

    :cond_0
    sget-object v0, Lcom/twitter/android/client/c;->u:Lcom/twitter/android/client/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic b(Lcom/twitter/android/client/c;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/c;->aq()V

    return-void
.end method

.method static synthetic c(Lcom/twitter/android/client/c;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/c;->ar()V

    return-void
.end method

.method static synthetic d(Lcom/twitter/android/client/c;)Lcom/twitter/library/client/aa;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    return-object v0
.end method

.method private d(Z)V
    .locals 6

    const/4 v5, 0x1

    iget-boolean v0, p0, Lcom/twitter/android/client/c;->q:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/client/c;->p:Z

    if-eq v0, p1, :cond_1

    :cond_0
    iput-boolean p1, p0, Lcom/twitter/android/client/c;->p:Z

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/telephony/TelephonyUtil;->d(Landroid/content/Context;)V

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v2, v5, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "settings::::"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz p1, :cond_2

    const-string/jumbo v0, "enable_media_forward"

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "network_type:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/twitter/library/telephony/TelephonyUtil;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lcom/twitter/android/client/c;->q:Z

    if-eqz v0, :cond_3

    const-string/jumbo v0, "change"

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/scribe/ScribeLog;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    iput-boolean v5, p0, Lcom/twitter/android/client/c;->q:Z

    :cond_1
    return-void

    :cond_2
    const-string/jumbo v0, "disable_media_forward"

    goto :goto_0

    :cond_3
    const-string/jumbo v0, "init"

    goto :goto_1
.end method

.method private e(Lcom/twitter/library/service/b;)Ljava/lang/String;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/twitter/android/client/c;->D:Lcom/twitter/library/client/w;

    iget-object v1, p0, Lcom/twitter/android/client/c;->i:Lcom/twitter/library/service/a;

    invoke-virtual {p1, v1}, Lcom/twitter/library/service/b;->a(Lcom/twitter/internal/android/service/m;)Lcom/twitter/internal/android/service/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/client/c;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/c;->C:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/client/c;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/c;->x:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/client/c;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/c;->ao()V

    return-void
.end method

.method static synthetic h(Lcom/twitter/android/client/c;)Landroid/util/SparseArray;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/c;->w:Landroid/util/SparseArray;

    return-object v0
.end method

.method private s(J)V
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v0, v2, :cond_2

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const-string/jumbo v2, "display"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0, v7}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;

    move-result-object v2

    if-eqz v2, :cond_2

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {v2, v0}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V

    :goto_0
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v1, v2, :cond_0

    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    :goto_1
    iget-object v3, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    new-instance v4, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v4, p1, p2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const-string/jumbo v6, "app::::launch"

    aput-object v6, v5, v7

    invoke-virtual {v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "display_info:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v5, "x"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/twitter/library/scribe/ScribeLog;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/client/c;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "location_enabled"

    :goto_2
    invoke-virtual {v1, v0}, Lcom/twitter/library/scribe/ScribeLog;->h(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->b(Landroid/content/Context;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    return-void

    :cond_0
    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    goto :goto_1

    :cond_1
    const-string/jumbo v0, "location_disabled"

    goto :goto_2

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public A()I
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "typeahead_max_compose"

    const/16 v2, 0x14

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public B()J
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "typeahead_throttle"

    const-wide/16 v2, 0xfa

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public C()I
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "suggestions_max_trend"

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public D()J
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "find_friends_interval"

    const-wide v2, 0x1cf7c5800L

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public E()J
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "fft"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public F()Ljava/util/regex/Pattern;
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/c;->W:Ljava/util/regex/Pattern;

    if-nez v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "ihb"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/c;->W:Ljava/util/regex/Pattern;
    :try_end_0
    .catch Ljava/util/regex/PatternSyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/client/c;->W:Ljava/util/regex/Pattern;

    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v0, ""

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/c;->W:Ljava/util/regex/Pattern;

    goto :goto_0
.end method

.method public G()Z
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "age_gating_failure_time"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    const-wide/32 v2, 0x5265c00

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public H()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "age_gating_failure_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public I()Z
    .locals 4

    const/4 v0, 0x1

    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "dm_sync_user_fraction"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const-string/jumbo v3, "dm_sync_user_fraction"

    invoke-static {v1, v2, v3, v0}, Lcom/twitter/library/api/Decider;->a(JLjava/lang/String;I)Z

    move-result v0

    goto :goto_0
.end method

.method public J()Z
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "message_compose_suggestions_enabled"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public K()Z
    .locals 4

    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "dm_photo_compose_user_fraction"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const-string/jumbo v3, "dm_photo_compose_user_fraction"

    invoke-static {v1, v2, v3, v0}, Lcom/twitter/library/api/Decider;->a(JLjava/lang/String;I)Z

    move-result v0

    goto :goto_0
.end method

.method public L()Z
    .locals 4

    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "dm_photo_render_user_fraction"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const-string/jumbo v3, "dm_photo_render_user_fraction"

    invoke-static {v1, v2, v3, v0}, Lcom/twitter/library/api/Decider;->a(JLjava/lang/String;I)Z

    move-result v0

    goto :goto_0
.end method

.method public M()Z
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "message_compose_retry_enabled"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public N()Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v3, "report_tweet_user_fraction"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    iget-object v3, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->d()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    const-string/jumbo v5, "report_tweet_user_fraction"

    invoke-static {v3, v4, v5, v2}, Lcom/twitter/library/api/Decider;->a(JLjava/lang/String;I)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public O()Z
    .locals 4

    const/4 v0, 0x1

    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "linger_user_fraction"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const-string/jumbo v3, "linger_impression_fraction"

    invoke-static {v1, v2, v3, v0}, Lcom/twitter/library/api/Decider;->a(JLjava/lang/String;I)Z

    move-result v0

    goto :goto_0
.end method

.method public P()Z
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const-string/jumbo v1, "config"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "device_operator_mt_sms"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public Q()F
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "status_linger_minimum_threshold"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public R()F
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "status_linger_maximum_threshold"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public S()Z
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "media_forward_tweet_detail"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public T()Z
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "show_reply_count_with_media"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public U()Ljava/util/ArrayList;
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/client/c;->M:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "web_view_url_whitelist"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-eqz v0, :cond_0

    new-instance v2, Ljava/util/StringTokenizer;

    const-string/jumbo v3, ","

    invoke-direct {v2, v0, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iput-object v1, p0, Lcom/twitter/android/client/c;->M:Ljava/util/ArrayList;

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/client/c;->M:Ljava/util/ArrayList;

    return-object v0
.end method

.method public V()F
    .locals 1

    iget v0, p0, Lcom/twitter/android/client/c;->L:F

    return v0
.end method

.method public W()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const-class v2, Lcom/twitter/android/DialogFragmentActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "blocked_needs_phone_verification"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public declared-synchronized X()Ljava/util/Collection;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/client/c;->y:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public Y()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "url_hints"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x3

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const v3, 0x7f0f0316    # com.twitter.android.R.string.post_link_hint

    invoke-static {v2, v3}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;I)V

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "url_hints"

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return-void
.end method

.method public Z()V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/client/f;

    iget-object v3, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const-string/jumbo v4, "decider"

    invoke-direct {v2, v3, v1, v4}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "cache_dirty"

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-static {v1, v3, v4}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/provider/az;->h()V

    invoke-virtual {v2}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v1, "cache_dirty"

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/f;->c(Ljava/lang/String;)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)I
    .locals 3

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "https"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/client/c;->a(Z)I

    move-result v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sub-int v0, v1, v0

    return v0
.end method

.method public a(Z)I
    .locals 2

    invoke-direct {p0}, Lcom/twitter/android/client/c;->an()Lcom/twitter/library/api/UrlConfiguration;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v1, v0, Lcom/twitter/library/api/UrlConfiguration;->a:I

    if-lez v1, :cond_1

    iget v0, v0, Lcom/twitter/library/api/UrlConfiguration;->a:I

    :goto_0
    if-eqz p1, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    return v0

    :cond_1
    const/16 v0, 0x16

    goto :goto_0
.end method

.method public a(Landroid/app/Activity;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;
    .locals 2

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0f0101    # com.twitter.android.R.string.dialog_location_message

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f02d5    # com.twitter.android.R.string.ok

    invoke-virtual {v0, v1, p2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f01ba    # com.twitter.android.R.string.grant_permission_negative

    invoke-virtual {v0, v1, p2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method a(ILjava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/c;->w:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ao;

    invoke-virtual {v0, p2}, Lcom/twitter/library/util/ao;->b(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/util/m;)Landroid/graphics/Bitmap;
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/c;->b:Lcom/twitter/library/util/aa;

    iget-object v1, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2, p1}, Lcom/twitter/library/util/aa;->a(JLcom/twitter/library/util/m;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/SharedPreferences;)Lcom/twitter/library/api/ad;
    .locals 2

    const-string/jumbo v0, "twitter_access_config"

    const-string/jumbo v1, ""

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/library/api/ad;

    invoke-direct {v0, v1}, Lcom/twitter/library/api/ad;-><init>(Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Lcom/twitter/library/metrics/h;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/c;->Y:Lcom/twitter/library/metrics/h;

    return-object v0
.end method

.method public a(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    if-eqz v0, :cond_0

    const-string/jumbo v1, "lang"

    invoke-static {v0}, Lcom/twitter/library/util/Util;->b(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method public a(Ljava/lang/String;Lcom/twitter/library/util/ab;)Lcom/twitter/library/util/aa;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/c;->J:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/aa;

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    invoke-interface {p2}, Lcom/twitter/library/util/ab;->a()Lcom/twitter/library/util/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/c;->J:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method public a(I)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x4f

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "page"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(IJ)Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/u;

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v2, 0x7a

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "list_id"

    invoke-virtual {v1, v2, p2, p3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "i_type"

    invoke-virtual {v1, v2, p1}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "owner_id"

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(IJJ)Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/u;

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v2, 0x23

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "user_type"

    invoke-virtual {v1, v2, p1}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "list_id"

    invoke-virtual {v1, v2, p2, p3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "user_id"

    invoke-virtual {v1, v2, p4, p5}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "owner_id"

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(IJJI)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x22

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user_type"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1, p2, p3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "list_id"

    invoke-virtual {v0, v1, p4, p5}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "page"

    invoke-virtual {v0, v1, p6}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(IJJILjava/lang/String;)Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/u;

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    if-eqz p7, :cond_0

    const-string/jumbo v2, "scribe_event"

    invoke-virtual {v1, v2, p7}, Lcom/twitter/library/client/u;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    :cond_0
    const/16 v2, 0x42

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "owner_id"

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "i_type"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "count"

    invoke-virtual {v0, v1, p6}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "max_id"

    invoke-virtual {v0, v1, p4, p5}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "since_id"

    invoke-virtual {v0, v1, p2, p3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/service/l;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lcom/twitter/library/service/l;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/b;->a(Lcom/twitter/internal/android/service/l;)Lcom/twitter/internal/android/service/a;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/client/c;->i:Lcom/twitter/library/service/a;

    invoke-virtual {v1, v2}, Lcom/twitter/internal/android/service/a;->a(Lcom/twitter/internal/android/service/m;)Lcom/twitter/internal/android/service/a;

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/c;->c(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    iget-object v1, p0, Lcom/twitter/android/client/c;->D:Lcom/twitter/library/client/w;

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(I[JIJJI)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x45

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user_type"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1, p4, p5}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "limit"

    invoke-virtual {v0, v1, p3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "users"

    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;[J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "persist_data"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "row_id"

    invoke-virtual {v0, v1, p6, p7}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "cluster_follow_type"

    invoke-virtual {v0, v1, p8}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(J)Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/u;

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v2, 0x16

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "owner_id"

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1, p1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(JJLjava/lang/String;ZLjava/lang/String;Z)Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/u;

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v2, 0x70

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "status_id"

    invoke-virtual {v1, v2, p1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "owner_id"

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1, p3, p4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "impression_id"

    invoke-virtual {v0, v1, p5}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "earned"

    invoke-virtual {v0, v1, p6}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "report_as"

    invoke-virtual {v0, v1, p7}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "block"

    invoke-virtual {v0, v1, p8}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/client/al;->a(Lcom/twitter/library/client/Session;)Lcom/twitter/android/client/al;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, p2, v2}, Lcom/twitter/android/client/al;->c(JI)V

    invoke-direct {p0, p3, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/client/Session;)Lcom/twitter/library/client/u;

    move-result-object v1

    const/16 v2, 0xf

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "user_id"

    invoke-virtual {v1, v2, p1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "owner_id"

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(JLjava/lang/String;)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x6c

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "message_id"

    invoke-virtual {v0, v1, p1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "report_as"

    invoke-virtual {v0, v1, p3}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(JLjava/lang/String;II)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1, p1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "page"

    invoke-virtual {v0, v1, p4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user_type"

    invoke-virtual {v0, v1, p5}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "name"

    invoke-virtual {v0, v1, p3}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(JLjava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/u;

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v2, 0x1f

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "name"

    invoke-virtual {v1, v2, p3}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "list_mode"

    invoke-virtual {v1, v2, p4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "desc"

    invoke-virtual {v1, v2, p5}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "owner_id"

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "list_id"

    invoke-virtual {v0, v1, p1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(JLjava/lang/String;ZLjava/lang/String;Z)Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/u;

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v2, 0x1c

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "owner_id"

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1, p1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "impression_id"

    invoke-virtual {v0, v1, p3}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "earned"

    invoke-virtual {v0, v1, p4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "report_as"

    invoke-virtual {v0, v1, p5}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "block"

    invoke-virtual {v0, v1, p6}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(JZLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;
    .locals 7

    const/4 v5, 0x0

    move-object v0, p0

    move-wide v1, p1

    move v3, p3

    move-object v4, p4

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/c;->a(JZLcom/twitter/library/api/PromotedContent;ZZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(JZLcom/twitter/library/api/PromotedContent;ZZ)Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/client/al;->a(Lcom/twitter/library/client/Session;)Lcom/twitter/android/client/al;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, p2, v2}, Lcom/twitter/android/client/al;->b(JI)V

    invoke-direct {p0, p4, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/client/Session;)Lcom/twitter/library/client/u;

    move-result-object v1

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/api/TwitterUser;)Lcom/twitter/library/client/u;

    move-result-object v1

    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "user_id"

    invoke-virtual {v1, v2, p1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "owner_id"

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "device_follow"

    invoke-virtual {v0, v1, p3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "show_toast"

    invoke-virtual {v0, v1, p5}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "challenges_passed"

    invoke-virtual {v0, v1, p6}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x5f

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "account"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Landroid/os/Parcelable;)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/accounts/Account;I)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x5e

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "account"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Landroid/os/Parcelable;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "settings"

    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/u;

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v2, 0x4d

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "owner_id"

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Landroid/os/Parcelable;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "lifeline_follow"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/client/Session;III)Ljava/lang/String;
    .locals 4

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "page"

    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user_type"

    invoke-virtual {v0, v1, p3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "count"

    invoke-virtual {v0, v1, p4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/client/Session;IIJ)Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/client/c;->D:Lcom/twitter/library/client/w;

    new-instance v1, Lcom/twitter/library/client/u;

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v1, v2, p1}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v2, 0x57

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "i_type"

    invoke-virtual {v1, v2, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "count"

    invoke-virtual {v1, v2, p3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "cache_age"

    invoke-virtual {v1, v2, p4, p5}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/service/l;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Lcom/twitter/library/service/l;-><init>(I)V

    invoke-virtual {v1, v2}, Lcom/twitter/library/service/b;->a(Lcom/twitter/internal/android/service/l;)Lcom/twitter/internal/android/service/a;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/client/c;->i:Lcom/twitter/library/service/a;

    invoke-virtual {v1, v2}, Lcom/twitter/internal/android/service/a;->a(Lcom/twitter/internal/android/service/m;)Lcom/twitter/internal/android/service/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/client/Session;IJJLjava/lang/String;)Ljava/lang/String;
    .locals 4

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    if-eqz p7, :cond_0

    const-string/jumbo v1, "scribe_event"

    invoke-virtual {v0, v1, p7}, Lcom/twitter/library/client/u;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/client/c;->D:Lcom/twitter/library/client/w;

    const/16 v2, 0x43

    invoke-virtual {v0, v2}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v2, "page"

    invoke-virtual {v0, v2, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v2, "gap_id"

    invoke-virtual {v0, v2, p3, p4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v2, "max_id"

    invoke-virtual {v0, v2, p5, p6}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    new-instance v2, Lcom/twitter/library/service/l;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Lcom/twitter/library/service/l;-><init>(I)V

    invoke-virtual {v0, v2}, Lcom/twitter/library/service/b;->a(Lcom/twitter/internal/android/service/l;)Lcom/twitter/internal/android/service/a;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/client/c;->i:Lcom/twitter/library/service/a;

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/service/a;->a(Lcom/twitter/internal/android/service/m;)Lcom/twitter/internal/android/service/a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/client/Session;J)Ljava/lang/String;
    .locals 2

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x1a

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "status_id"

    invoke-virtual {v0, v1, p2, p3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/client/Session;JIJJLjava/lang/Integer;)Ljava/lang/String;
    .locals 2

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    if-eqz p9, :cond_0

    const-string/jumbo v1, "page"

    invoke-virtual {v0, v1, p9}, Lcom/twitter/library/client/u;->a(Ljava/lang/String;Ljava/io/Serializable;)Lcom/twitter/library/service/b;

    :cond_0
    const/16 v1, 0x56

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "owner_id"

    invoke-virtual {v0, v1, p2, p3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user_type"

    invoke-virtual {v0, v1, p4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user_tag"

    invoke-virtual {v0, v1, p5, p6}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1, p7, p8}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/client/Session;JJJJ)Ljava/lang/String;
    .locals 11

    const/4 v10, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move-wide/from16 v6, p6

    move-wide/from16 v8, p8

    invoke-virtual/range {v0 .. v10}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;JJJJZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/client/Session;JJJJZ)Ljava/lang/String;
    .locals 4

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {v0, p10}, Lcom/twitter/library/client/u;->c(Z)Lcom/twitter/library/service/b;

    move-result-object v0

    const/16 v1, 0x44

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/b;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "status_id"

    invoke-virtual {v0, v1, p2, p3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "q"

    invoke-virtual {v0, v1, p8, p9}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "since_id"

    invoke-virtual {v0, v1, p4, p5}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "max_id"

    invoke-virtual {v0, v1, p6, p7}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/client/Session;JJLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;
    .locals 2

    invoke-direct {p0, p6, p1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/client/Session;)Lcom/twitter/library/client/u;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "status_id"

    invoke-virtual {v0, v1, p2, p3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "rt_status_id"

    invoke-virtual {v0, v1, p4, p5}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/client/Session;JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;
    .locals 2

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x19

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "status_id"

    invoke-virtual {v0, v1, p2, p3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "promoted_content"

    invoke-virtual {v0, v1, p4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Ljava/io/Serializable;)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/TwitterUser;Z)Ljava/lang/String;
    .locals 4

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x4d

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "owner_id"

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Landroid/os/Parcelable;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "device_follow"

    invoke-virtual {v0, v1, p3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/UserSettings;Z)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/UserSettings;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/UserSettings;ZLjava/lang/String;)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    if-eqz p4, :cond_0

    if-eqz p2, :cond_0

    iput-object p4, p2, Lcom/twitter/library/api/UserSettings;->m:Ljava/lang/String;

    const-string/jumbo v1, "old_screen_name"

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/u;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    :cond_0
    const/16 v1, 0x3c

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "settings"

    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Landroid/os/Parcelable;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "trends"

    invoke-virtual {v0, v1, p3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x82

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "old_password"

    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "new_password"

    invoke-virtual {v0, v1, p3}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZ)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    const/16 v1, 0x84

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "old_password"

    invoke-virtual {v0, v1, p3}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "new_password"

    invoke-virtual {v0, v1, p4}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "new_screen_name"

    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "old_screen_name"

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "protected"

    invoke-virtual {v0, v1, p6}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "discoverable_by_email"

    invoke-virtual {v0, v1, p7}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "discoverable_by_mobile_phone"

    invoke-virtual {v0, v1, p8}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "email"

    invoke-virtual {v0, v1, p5}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/client/Session;[J)Ljava/lang/String;
    .locals 2

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x49

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "q"

    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;[J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/client/Session;[JZ)Ljava/lang/String;
    .locals 4

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x75

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "owner_id"

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;[J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "notify_content_provider"

    invoke-virtual {v0, v1, p3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;IIJ)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x36

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "q"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "owner_id"

    invoke-virtual {v0, v1, p4, p5}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "page"

    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user_type"

    invoke-virtual {v0, v1, p3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;IILjava/lang/String;)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x4c

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "i_type"

    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "q"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "count"

    invoke-virtual {v0, v1, p3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "src"

    invoke-virtual {v0, v1, p4}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/u;

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v2, 0x1e

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "name"

    invoke-virtual {v1, v2, p1}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "list_mode"

    invoke-virtual {v1, v2, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "desc"

    invoke-virtual {v1, v2, p3}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "owner_id"

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;J)Ljava/lang/String;
    .locals 5

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-gtz v0, :cond_0

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/u;

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v2, 0x17

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "owner_id"

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "screen_name"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1, p2, p3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2, p1}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x83

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "email"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "pass"

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/u;

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v2, 0x79

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "normalized_phone_number"

    invoke-virtual {v1, v2, p1}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "text_message"

    invoke-virtual {v1, v2, p2}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "owner_id"

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    const/16 v1, 0x47

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "screen_name"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "name"

    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "email"

    invoke-virtual {v0, v1, p3}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(ZIIIJ)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x45

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "connections"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "page"

    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "limit"

    invoke-virtual {v0, v1, p4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1, p5, p6}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user_type"

    invoke-virtual {v0, v1, p3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(ZII[J)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    if-eqz p4, :cond_0

    const-string/jumbo v1, "users"

    invoke-virtual {v0, v1, p4}, Lcom/twitter/library/client/u;->a(Ljava/lang/String;[J)Lcom/twitter/library/service/b;

    :cond_0
    const/16 v1, 0x45

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "connections"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "page"

    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user_type"

    invoke-virtual {v0, v1, p3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a([J)Ljava/lang/String;
    .locals 7

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/client/al;->a(Lcom/twitter/library/client/Session;)Lcom/twitter/android/client/al;

    move-result-object v2

    array-length v3, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-wide v4, p1, v0

    const/4 v6, 0x1

    invoke-virtual {v2, v4, v5, v6}, Lcom/twitter/android/client/al;->b(JI)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v0, v2, v1}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/api/TwitterUser;)Lcom/twitter/library/client/u;

    move-result-object v0

    const/16 v2, 0x3a

    invoke-virtual {v0, v2}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v2, "user_id"

    invoke-virtual {v0, v2, p1}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;[J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v2, "owner_id"

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a([Lcom/twitter/library/api/TwitterContact;)Ljava/lang/String;
    .locals 2

    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/twitter/android/client/c;->a([Lcom/twitter/library/api/TwitterContact;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a([Lcom/twitter/library/api/TwitterContact;J)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x54

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "invite"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;[Landroid/os/Parcelable;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "status_id"

    invoke-virtual {v0, v1, p2, p3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a([Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x6e

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "thread_id"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;[Ljava/lang/CharSequence;)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a([Ljava/lang/String;[Ljava/lang/String;[JIJZII)Ljava/lang/String;
    .locals 5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/client/c;->p(J)V

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/u;

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v2, 0x38

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "user_id"

    invoke-virtual {v1, v2, p3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;[J)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "list_id"

    invoke-virtual {v1, v2, p5, p6}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "i_type"

    invoke-virtual {v1, v2, p4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "owner_id"

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "email"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;[Ljava/lang/CharSequence;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "phone"

    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;[Ljava/lang/CharSequence;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "map_contacts"

    invoke-virtual {v0, v1, p7}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "page"

    invoke-virtual {v0, v1, p8}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "pages"

    invoke-virtual {v0, v1, p9}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(IJLjava/lang/String;)V
    .locals 7

    iget-object v6, p0, Lcom/twitter/android/client/c;->D:Lcom/twitter/library/client/w;

    new-instance v0, Liw;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    move v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Liw;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;IJ)V

    invoke-virtual {v0, p4}, Liw;->a(Ljava/lang/String;)Liw;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    return-void
.end method

.method public a(ILcom/twitter/library/util/ar;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/c;->w:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ao;

    invoke-virtual {v0, p2}, Lcom/twitter/library/util/ao;->a(Lcom/twitter/library/util/ar;)V

    return-void
.end method

.method public a(ILjava/lang/String;Landroid/net/Uri;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/twitter/android/client/c;->w:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ao;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/library/util/ao;->a(Ljava/lang/String;Landroid/net/Uri;)V

    return-void
.end method

.method public a(I[ILcom/twitter/library/client/Session;Landroid/os/Bundle;)V
    .locals 5

    const v1, 0x7f0f0527    # com.twitter.android.R.string.users_create_friendship_error

    const/high16 v4, 0x10000000

    const/4 v2, 0x1

    const/16 v0, 0x193

    if-ne p1, v0, :cond_6

    const/16 v0, 0xe2

    invoke-static {p2, v0}, Lcom/twitter/library/util/Util;->a([II)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const-class v3, Lcom/twitter/android/DialogActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "blocked_spammer_follow"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0xe1

    invoke-static {p2, v0}, Lcom/twitter/library/util/Util;->a([II)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const-class v3, Lcom/twitter/android/DialogActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "blocked_automated_spammer"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    const/16 v0, 0xa2

    invoke-static {p2, v0}, Lcom/twitter/library/util/Util;->a([II)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const v1, 0x7f0f0528    # com.twitter.android.R.string.users_create_friendship_error_blocked

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    const/16 v0, 0xfa

    invoke-static {p2, v0}, Lcom/twitter/library/util/Util;->a([II)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/twitter/android/client/c;->G()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const v1, 0x7f0f001e    # com.twitter.android.R.string.age_gating_failed

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_4
    invoke-virtual {p3}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/client/aw;->a(Landroid/content/Context;)Lcom/twitter/android/client/aw;

    move-result-object v0

    invoke-virtual {v0, p4, p3}, Lcom/twitter/android/client/aw;->a(Landroid/os/Bundle;Lcom/twitter/library/client/Session;)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0, p2}, Lcom/twitter/android/client/c;->b([I)V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public a(JLjava/lang/String;Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v1, p1, p2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v2, v4, [Ljava/lang/String;

    aput-object p3, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/String;

    aput-object p4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->c([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method public varargs a(J[Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v1, p1, p2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    invoke-virtual {v1, p3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method public a(Landroid/content/ComponentName;)V
    .locals 7

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->a()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v4, v3, [J

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    aput-wide v5, v4, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x74

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "owner_ids"

    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;[J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "widget_provider"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Landroid/os/Parcelable;)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 5

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/library/metrics/h;

    invoke-direct {v0, p1}, Lcom/twitter/library/metrics/h;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lcom/twitter/library/client/App;->m()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/twitter/library/metrics/a;

    invoke-direct {v1}, Lcom/twitter/library/metrics/a;-><init>()V

    invoke-virtual {v0, v1}, Lcom/twitter/library/metrics/h;->a(Lcom/twitter/library/metrics/k;)V

    :cond_0
    invoke-static {}, Lcom/twitter/library/client/App;->h()Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Lcom/twitter/library/metrics/n;

    invoke-direct {v1, p1}, Lcom/twitter/library/metrics/n;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/metrics/h;->a(Lcom/twitter/library/metrics/k;)V

    :cond_1
    invoke-static {}, Lcom/twitter/library/client/App;->h()Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Lcom/twitter/library/metrics/c;

    invoke-direct {v1, p1}, Lcom/twitter/library/metrics/c;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/metrics/h;->a(Lcom/twitter/library/metrics/k;)V

    :cond_2
    iput-object v0, p0, Lcom/twitter/android/client/c;->Y:Lcom/twitter/library/metrics/h;

    invoke-static {v0, v2}, Lhf;->a(Lcom/twitter/library/metrics/h;Z)Lhf;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/metrics/b;->i()V

    invoke-static {v1, v2}, Lcom/twitter/library/network/c;->a(Lcom/twitter/library/metrics/b;Z)V

    invoke-static {v0, v3}, Lhf;->a(Lcom/twitter/library/metrics/h;Z)Lhf;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/metrics/b;->i()V

    invoke-static {v1, v3}, Lcom/twitter/library/network/c;->a(Lcom/twitter/library/metrics/b;Z)V

    const-string/jumbo v1, "api::scribe::size"

    const-wide/16 v2, 0x0

    const/4 v4, 0x2

    invoke-static {v1, v0, v2, v3, v4}, Lcom/twitter/library/metrics/b;->a(Ljava/lang/String;Lcom/twitter/library/metrics/h;JI)Lcom/twitter/library/metrics/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/metrics/b;->i()V

    invoke-static {v0}, Lcom/twitter/library/scribe/ScribeService;->a(Lcom/twitter/library/metrics/b;)V

    return-void
.end method

.method a(Landroid/content/Intent;)V
    .locals 1

    const-string/jumbo v0, "data"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/platform/DataSyncResult;

    if-eqz v0, :cond_0

    invoke-direct {p0, v0, p1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/platform/DataSyncResult;Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/api/PromotedEvent;J)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/client/c;->D:Lcom/twitter/library/client/w;

    new-instance v1, Lix;

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-direct {v1, v2, v3, p1}, Lix;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/api/PromotedEvent;)V

    invoke-virtual {v1, p2, p3}, Lix;->a(J)Lix;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    return-void
.end method

.method public a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;Ljava/lang/String;)V

    return-void
.end method

.method public a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/client/c;->D:Lcom/twitter/library/client/w;

    new-instance v1, Lix;

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-direct {v1, v2, v3, p1}, Lix;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/api/PromotedEvent;)V

    iget-object v2, p2, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lix;->b(Ljava/lang/String;)Lix;

    move-result-object v1

    invoke-virtual {p2}, Lcom/twitter/library/api/PromotedContent;->b()Z

    move-result v2

    invoke-virtual {v1, v2}, Lix;->a(Z)Lix;

    move-result-object v1

    invoke-virtual {v1, p3}, Lix;->a(Ljava/lang/String;)Lix;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    return-void
.end method

.method public a(Lcom/twitter/library/api/ad;Landroid/content/SharedPreferences;)V
    .locals 3

    invoke-interface {p2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    if-eqz p1, :cond_0

    const-string/jumbo v1, "twitter_access_config"

    invoke-virtual {p1}, Lcom/twitter/library/api/ad;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "data_alerts_inline"

    iget-boolean v2, p1, Lcom/twitter/library/api/ad;->e:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "data_alerts_links"

    iget-boolean v2, p1, Lcom/twitter/library/api/ad;->f:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    iput-object p1, p0, Lcom/twitter/android/client/c;->T:Lcom/twitter/library/api/ad;

    return-void

    :cond_0
    const-string/jumbo v1, "twitter_access_config"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/api/b;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/client/c;->S:Lcom/twitter/library/api/b;

    return-void
.end method

.method a(Lcom/twitter/library/client/Session;)V
    .locals 4

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/twitter/library/client/App;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v2}, Lcom/crashlytics/android/d;->d(Ljava/lang/String;)V

    :cond_0
    iget-object v3, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v3, v0, v1, v2}, Lkk;->a(Landroid/content/Context;JLjava/lang/String;)V

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v2}, Lkj;->a(Landroid/content/Context;)V

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/twitter/library/featureswitch/b;->b(J)V

    invoke-direct {p0}, Lcom/twitter/android/client/c;->ar()V

    iget-object v0, p0, Lcom/twitter/android/client/c;->b:Lcom/twitter/library/util/aa;

    new-instance v1, Lcom/twitter/library/network/n;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->h()Lcom/twitter/library/network/OAuthToken;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/util/aa;->a(Lcom/twitter/library/network/n;)V

    invoke-static {}, Lcom/twitter/library/client/l;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const-wide/32 v0, 0xea60

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/client/c;->n(J)V

    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;IJZ)V
    .locals 2

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x6a

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "act_read_pos"

    invoke-virtual {v0, v1, p3, p4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "i_type"

    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "clear_all_notif"

    invoke-virtual {v0, v1, p5}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "lv_puvlic_key"

    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/client/u;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    :cond_0
    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;JJLcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/PromotedContent;)V
    .locals 2

    invoke-direct {p0, p8, p1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/client/Session;)Lcom/twitter/library/client/u;

    move-result-object v0

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "android.intent.extra.UID"

    invoke-virtual {v0, v1, p5, p6}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "status_id"

    invoke-virtual {v0, v1, p3, p4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "_data"

    invoke-virtual {v0, v1, p7}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Ljava/io/Serializable;)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x61

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "lv_puvlic_key"

    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "lv_offline_code"

    invoke-virtual {v0, v1, p3}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "lv_offline_code_count"

    invoke-virtual {v0, v1, p4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    return-void
.end method

.method public a(Lcom/twitter/library/client/j;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/c;->x:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/c;->x:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/scribe/ScribeLog;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method a(Lcom/twitter/library/service/b;Lcom/twitter/internal/android/service/k;)V
    .locals 23

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/service/b;->r()I

    move-result v6

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/twitter/library/service/b;->k:Landroid/os/Bundle;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/service/b;->s()Lcom/twitter/library/service/p;

    move-result-object v3

    iget-object v3, v3, Lcom/twitter/library/service/p;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/twitter/library/client/aa;->c(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {v3}, Lcom/twitter/android/client/al;->a(Lcom/twitter/library/client/Session;)Lcom/twitter/android/client/al;

    move-result-object v10

    const-string/jumbo v2, "rate_limit"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    move-object/from16 v21, v2

    check-cast v21, Lcom/twitter/library/api/RateLimit;

    invoke-virtual/range {p2 .. p2}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/service/e;

    invoke-virtual {v2}, Lcom/twitter/library/service/e;->e()Lcom/twitter/internal/network/k;

    move-result-object v4

    if-eqz v4, :cond_2

    iget-object v7, v4, Lcom/twitter/internal/network/k;->b:Ljava/lang/String;

    iget v5, v4, Lcom/twitter/internal/network/k;->a:I

    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v4, v3}, Lcom/twitter/library/client/aa;->c(Lcom/twitter/library/client/Session;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string/jumbo v2, "AC"

    const-string/jumbo v3, "Expired session. Ignoring response"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const/4 v7, 0x0

    invoke-virtual {v2}, Lcom/twitter/library/service/e;->c()I

    move-result v5

    goto :goto_1

    :cond_3
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/twitter/library/service/b;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/client/c;->x:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    sparse-switch v6, :sswitch_data_0

    :cond_4
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/client/c;->D:Lcom/twitter/library/client/w;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v2, v0, v1, v3}, Lcom/twitter/library/client/w;->a(Lcom/twitter/library/service/b;Lcom/twitter/internal/android/service/k;Lcom/twitter/library/client/Session;)V

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/api/RateLimit;)V

    const/16 v2, 0x191

    if-ne v5, v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2, v3}, Lcom/twitter/library/client/aa;->b(Lcom/twitter/library/client/Session;)Ljava/lang/String;

    goto :goto_0

    :sswitch_0
    const-string/jumbo v2, "status_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    const-string/jumbo v2, "count"

    const/4 v6, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v10

    invoke-static/range {v20 .. v20}, Lcom/twitter/library/service/b;->b(Landroid/os/Bundle;)[I

    move-result-object v6

    const/16 v2, 0x193

    if-ne v5, v2, :cond_5

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/twitter/android/client/c;->b([I)V

    :cond_5
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v11, v2

    :goto_3
    if-ltz v11, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/client/j;

    invoke-virtual/range {v2 .. v10}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;I[ILjava/lang/String;JI)V

    add-int/lit8 v2, v11, -0x1

    move v11, v2

    goto :goto_3

    :sswitch_1
    const-string/jumbo v2, "status_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v13

    const-string/jumbo v2, "count"

    const/4 v6, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v15

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_4
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v15}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JI)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_4

    :sswitch_2
    const-string/jumbo v2, "user_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v13

    const-string/jumbo v2, "user_type"

    const/4 v6, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v15

    const-string/jumbo v2, "count"

    const/4 v6, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v16

    const-string/jumbo v2, "users"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v17

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_5
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v17}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JIILjava/util/ArrayList;)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_5

    :sswitch_3
    invoke-static/range {v20 .. v20}, Lcom/twitter/library/service/b;->b(Landroid/os/Bundle;)[I

    move-result-object v6

    packed-switch v5, :pswitch_data_0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v5, v6, v3, v1}, Lcom/twitter/android/client/c;->a(I[ILcom/twitter/library/client/Session;Landroid/os/Bundle;)V

    :cond_6
    :goto_6
    const-string/jumbo v2, "user_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    const/16 v2, 0xc8

    if-eq v5, v2, :cond_7

    const/4 v2, 0x1

    invoke-virtual {v10, v8, v9, v2}, Lcom/twitter/android/client/al;->c(JI)V

    :cond_7
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v10, v2

    :goto_7
    if-ltz v10, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/client/j;

    invoke-virtual/range {v2 .. v9}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;I[ILjava/lang/String;J)V

    add-int/lit8 v2, v10, -0x1

    move v10, v2

    goto :goto_7

    :pswitch_0
    const-string/jumbo v2, "show_toast"

    const/4 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    const-string/jumbo v2, "user"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/TwitterUser;

    if-eqz v2, :cond_6

    if-eqz v8, :cond_6

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const v11, 0x7f0f0529    # com.twitter.android.R.string.users_create_friendship_success

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-virtual {v2}, Lcom/twitter/library/api/TwitterUser;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v12, v13

    invoke-virtual {v9, v11, v12}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v9, 0x1

    invoke-static {v8, v2, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_6

    :sswitch_4
    const-string/jumbo v2, "user_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v13

    const/16 v2, 0xc8

    if-eq v5, v2, :cond_8

    const/4 v2, 0x1

    invoke-virtual {v10, v13, v14, v2}, Lcom/twitter/android/client/al;->b(JI)V

    :cond_8
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_8
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v14}, Lcom/twitter/library/client/j;->c(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_8

    :sswitch_5
    const-string/jumbo v2, "user"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/TwitterUser;

    invoke-static {}, Lgl;->a()Z

    move-result v6

    const-string/jumbo v8, "device_follow"

    const/4 v9, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_9

    if-eqz v6, :cond_9

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v6}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/client/c;->D:Lcom/twitter/library/client/w;

    new-instance v8, Ljl;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-virtual {v10}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v11

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    const/16 v16, -0x2

    invoke-direct/range {v8 .. v16}, Ljl;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/api/TwitterUser;JJI)V

    sget-object v9, Lcom/twitter/library/service/TimelineHelper$FilterType;->b:Lcom/twitter/library/service/TimelineHelper$FilterType;

    invoke-virtual {v8, v9}, Ljl;->a(Lcom/twitter/library/service/TimelineHelper$FilterType;)Ljl;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Ljl;->c(Z)Lcom/twitter/library/service/b;

    move-result-object v8

    const-string/jumbo v9, "include_reply_count"

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->F()Z

    move-result v10

    invoke-virtual {v8, v9, v10}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/twitter/android/client/c;->d(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    :cond_9
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    :goto_9
    if-ltz v6, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    move-object v13, v2

    invoke-virtual/range {v8 .. v13}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;Lcom/twitter/library/api/TwitterUser;)V

    add-int/lit8 v6, v6, -0x1

    goto :goto_9

    :sswitch_6
    const-string/jumbo v2, "user"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v13

    check-cast v13, Lcom/twitter/library/api/TwitterUser;

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_a
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v13}, Lcom/twitter/library/client/j;->b(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;Lcom/twitter/library/api/TwitterUser;)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_a

    :sswitch_7
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v6, v2

    :goto_b
    if-ltz v6, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/client/j;

    invoke-virtual {v2, v3, v4, v5, v7}, Lcom/twitter/library/client/j;->h(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V

    add-int/lit8 v2, v6, -0x1

    move v6, v2

    goto :goto_b

    :sswitch_8
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v6, v2

    :goto_c
    if-ltz v6, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/client/j;

    invoke-virtual {v2, v3, v4, v5, v7}, Lcom/twitter/library/client/j;->g(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V

    add-int/lit8 v2, v6, -0x1

    move v6, v2

    goto :goto_c

    :sswitch_9
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v6, v2

    :goto_d
    if-ltz v6, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/client/j;

    invoke-virtual {v2, v3, v4, v5, v7}, Lcom/twitter/library/client/j;->i(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V

    add-int/lit8 v2, v6, -0x1

    move v6, v2

    goto :goto_d

    :sswitch_a
    const/16 v2, 0xc8

    if-ne v5, v2, :cond_4

    const-string/jumbo v2, "unread_dm"

    const/4 v4, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v4, v2

    :goto_e
    if-ltz v4, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/client/j;

    invoke-virtual {v2, v3, v6}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;I)V

    add-int/lit8 v2, v4, -0x1

    move v4, v2

    goto :goto_e

    :sswitch_b
    const-string/jumbo v2, "user_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v13

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_f
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v14}, Lcom/twitter/library/client/j;->d(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_f

    :sswitch_c
    const-string/jumbo v2, "user"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/api/TwitterUser;

    const/16 v2, 0xc8

    if-ne v5, v2, :cond_a

    if-eqz v8, :cond_a

    invoke-virtual {v8}, Lcom/twitter/library/api/TwitterUser;->a()J

    move-result-wide v9

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v11

    cmp-long v2, v9, v11

    if-nez v2, :cond_a

    invoke-virtual {v3, v8}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/api/TwitterUser;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6, v8}, Lcom/twitter/library/util/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/api/TwitterUser;)V

    :cond_a
    invoke-static/range {v20 .. v20}, Lcom/twitter/library/service/b;->b(Landroid/os/Bundle;)[I

    move-result-object v6

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v9, v2

    :goto_10
    if-ltz v9, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/client/j;

    invoke-virtual/range {v2 .. v8}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;I[ILjava/lang/String;Lcom/twitter/library/api/TwitterUser;)V

    add-int/lit8 v2, v9, -0x1

    move v9, v2

    goto :goto_10

    :sswitch_d
    sparse-switch v5, :sswitch_data_1

    :goto_11
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_12
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    const-string/jumbo v6, "status_id"

    const-wide/16 v9, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v6, v9, v10}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v13

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v14}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_12

    :sswitch_e
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/Session;Z)V

    goto :goto_11

    :sswitch_f
    invoke-static/range {v20 .. v20}, Lcom/twitter/library/service/b;->b(Landroid/os/Bundle;)[I

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/c;->b([I)V

    goto :goto_11

    :sswitch_10
    const-string/jumbo v2, "status_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v13

    const/16 v2, 0xc8

    if-ne v5, v2, :cond_b

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v8, v9}, Lcom/twitter/android/client/c;->q(J)Lcom/twitter/android/client/WidgetControl;

    move-result-object v2

    if-eqz v2, :cond_b

    invoke-virtual {v2, v13, v14}, Lcom/twitter/android/client/WidgetControl;->a(J)V

    :cond_b
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_13
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v14}, Lcom/twitter/library/client/j;->b(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_13

    :sswitch_11
    const/16 v2, 0xc8

    if-eq v5, v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const v6, 0x7f0f0520    # com.twitter.android.R.string.users_block_error

    const/4 v8, 0x1

    invoke-static {v2, v6, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    :goto_14
    const-string/jumbo v2, "user_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v13

    const/16 v2, 0xc8

    if-eq v5, v2, :cond_c

    const/4 v2, 0x4

    invoke-virtual {v10, v13, v14, v2}, Lcom/twitter/android/client/al;->c(JI)V

    :cond_c
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_15
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v14}, Lcom/twitter/library/client/j;->f(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_15

    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const v6, 0x7f0f0050    # com.twitter.android.R.string.block_success

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string/jumbo v11, "screen_name"

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v8, v9

    invoke-virtual {v2, v6, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const/4 v8, 0x1

    invoke-static {v6, v2, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_14

    :sswitch_12
    const/16 v2, 0xc8

    if-eq v5, v2, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const v6, 0x7f0f0548    # com.twitter.android.R.string.users_unblock_error

    const/4 v8, 0x1

    invoke-static {v2, v6, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    :cond_e
    const-string/jumbo v2, "user_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v13

    const/16 v2, 0xc8

    if-eq v5, v2, :cond_f

    const/4 v2, 0x4

    invoke-virtual {v10, v13, v14, v2}, Lcom/twitter/android/client/al;->b(JI)V

    :cond_f
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_16
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v14}, Lcom/twitter/library/client/j;->g(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_16

    :sswitch_13
    const/16 v2, 0xc8

    if-ne v5, v2, :cond_10

    const-string/jumbo v2, "report_as"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v4, "abuse"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string/jumbo v2, "block"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const v4, 0x7f0f0050    # com.twitter.android.R.string.block_success

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string/jumbo v8, "screen_name"

    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v2, v4, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const/4 v6, 0x0

    invoke-static {v4, v2, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_2

    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const v4, 0x7f0f04d6    # com.twitter.android.R.string.tweet_report_failure

    const/4 v6, 0x0

    invoke-static {v2, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_2

    :sswitch_14
    const-string/jumbo v2, "user_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v13

    const-string/jumbo v2, "block"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    const-string/jumbo v6, "report_as"

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/16 v8, 0xc8

    if-ne v5, v8, :cond_12

    const-string/jumbo v8, "abuse"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_11

    if-eqz v2, :cond_11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const v6, 0x7f0f0050    # com.twitter.android.R.string.block_success

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string/jumbo v10, "screen_name"

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v2, v6, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const/4 v8, 0x1

    invoke-static {v6, v2, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    :cond_11
    :goto_17
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_18
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v14}, Lcom/twitter/library/client/j;->e(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_18

    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const v6, 0x7f0f053c    # com.twitter.android.R.string.users_report_spam_error

    const/4 v8, 0x1

    invoke-static {v2, v6, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_17

    :sswitch_15
    const/16 v2, 0xc8

    if-ne v5, v2, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const v4, 0x7f0f035a    # com.twitter.android.R.string.report_success

    const/4 v6, 0x0

    invoke-static {v2, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    :cond_13
    const-string/jumbo v2, "message_id"

    const-wide/16 v6, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7}, Lcom/twitter/android/client/c;->c(J)Ljava/lang/String;

    goto/16 :goto_2

    :sswitch_16
    const/16 v2, 0xc8

    if-eq v5, v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const v4, 0x7f0f00dd    # com.twitter.android.R.string.create_edit_list_create_error

    const/4 v6, 0x1

    invoke-static {v2, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_2

    :sswitch_17
    const/16 v2, 0xc8

    if-eq v5, v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const v4, 0x7f0f00e0    # com.twitter.android.R.string.create_edit_list_edit_error

    const/4 v6, 0x1

    invoke-static {v2, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_2

    :sswitch_18
    const/16 v2, 0xc8

    if-eq v5, v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const v4, 0x7f0f0215    # com.twitter.android.R.string.lists_delete_error

    const/4 v6, 0x1

    invoke-static {v2, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_2

    :sswitch_19
    const-string/jumbo v2, "list_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v13

    const-string/jumbo v2, "user_type"

    const/4 v6, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v15

    const-string/jumbo v2, "count"

    const/4 v6, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v16

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_19
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v16}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JII)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_19

    :sswitch_1a
    const-string/jumbo v2, "list_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v14

    const-string/jumbo v2, "user_type"

    const/4 v6, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v13

    const-string/jumbo v2, "user_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v16

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_1a
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v17}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;IJJ)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_1a

    :sswitch_1b
    const-string/jumbo v2, "list_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v14

    const-string/jumbo v2, "user_type"

    const/4 v6, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v13

    const-string/jumbo v2, "user_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v16

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_1b
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v17}, Lcom/twitter/library/client/j;->b(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;IJJ)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_1b

    :sswitch_1c
    const-string/jumbo v2, "list_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v14

    const-string/jumbo v2, "user_type"

    const/4 v6, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v13

    const-string/jumbo v2, "user_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v16

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_1c
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v17}, Lcom/twitter/library/client/j;->c(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;IJJ)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_1c

    :sswitch_1d
    const-string/jumbo v2, "i_type"

    const/4 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v13

    const-string/jumbo v2, "q"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    const-string/jumbo v2, "typeahead"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v16

    check-cast v16, Lcom/twitter/library/api/search/TwitterTypeAheadGroup;

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_1d
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    const/16 v9, 0x57

    if-ne v6, v9, :cond_14

    const/4 v14, 0x1

    :goto_1e
    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v16}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;IZLjava/lang/String;Lcom/twitter/library/api/search/TwitterTypeAheadGroup;)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_1d

    :cond_14
    const/4 v14, 0x0

    goto :goto_1e

    :sswitch_1e
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v6, v2

    :goto_1f
    if-ltz v6, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/client/j;

    invoke-virtual {v2, v3, v4, v5, v7}, Lcom/twitter/library/client/j;->l(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V

    add-int/lit8 v2, v6, -0x1

    move v6, v2

    goto :goto_1f

    :sswitch_1f
    const-string/jumbo v2, "count"

    const/4 v6, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v13

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_20
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v13}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;I)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_20

    :sswitch_20
    const-string/jumbo v2, "count"

    const/4 v6, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v13

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_21
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v13}, Lcom/twitter/library/client/j;->b(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;I)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_21

    :sswitch_21
    const-string/jumbo v2, "count"

    const/4 v6, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v13

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_22
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v13}, Lcom/twitter/library/client/j;->c(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;I)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_22

    :sswitch_22
    const-string/jumbo v2, "count"

    const/4 v6, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v13

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_23
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v13}, Lcom/twitter/library/client/j;->d(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;I)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_23

    :sswitch_23
    const-string/jumbo v2, "user_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v13

    const-string/jumbo v2, "row_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v15

    const-string/jumbo v2, "user_type"

    const/4 v6, -0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v17

    const-string/jumbo v2, "count"

    const/4 v6, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v18

    const-string/jumbo v2, "users"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, [Lcom/twitter/library/api/TwitterUser;

    move-object/from16 v19, v2

    check-cast v19, [Lcom/twitter/library/api/TwitterUser;

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_24
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v19}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JJII[Lcom/twitter/library/api/TwitterUser;)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_24

    :sswitch_24
    const-string/jumbo v2, "owner_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v13

    const-string/jumbo v2, "list_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v15

    const-string/jumbo v2, "name"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_25
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v17}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JJLjava/lang/String;)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_25

    :sswitch_25
    const-string/jumbo v2, "count"

    const/4 v6, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v13

    const-string/jumbo v2, "page"

    const/4 v6, -0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v14

    const-string/jumbo v2, "pages"

    const/4 v6, -0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v15

    const-string/jumbo v2, "contact"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v16

    const-string/jumbo v2, "users"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v18

    const-string/jumbo v2, "email"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_26
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v18}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;IIILjava/util/ArrayList;[Ljava/lang/String;Ljava/util/ArrayList;)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_26

    :sswitch_26
    const-string/jumbo v2, "count"

    const/4 v6, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v13

    const-string/jumbo v2, "users"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v14

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_27
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v14}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;I[J)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_27

    :sswitch_27
    const/16 v2, 0xc8

    if-ne v5, v2, :cond_4

    const-string/jumbo v2, "status_id"

    const-wide/16 v6, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v2, v6, v8

    if-eqz v2, :cond_15

    const v2, 0x7f0f04d4    # com.twitter.android.R.string.tweet_mention_contact_sent

    :goto_28
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const/4 v6, 0x1

    invoke-static {v4, v2, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_2

    :cond_15
    const v2, 0x7f0f01f7    # com.twitter.android.R.string.invites_sent

    goto :goto_28

    :sswitch_28
    const-string/jumbo v2, "normalized_phone_number"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v6, v2

    :goto_29
    if-ltz v6, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/client/j;

    invoke-virtual {v2, v3, v4, v5, v7}, Lcom/twitter/library/client/j;->e(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V

    add-int/lit8 v2, v6, -0x1

    move v6, v2

    goto :goto_29

    :sswitch_29
    if-eqz v2, :cond_16

    invoke-virtual {v2}, Lcom/twitter/library/service/e;->a()Z

    move-result v2

    if-eqz v2, :cond_16

    new-instance v2, Lcom/twitter/library/util/z;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v2, v6}, Lcom/twitter/library/util/z;-><init>(Landroid/content/Context;)V

    const/4 v6, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v2, v6, v8, v9}, Lcom/twitter/library/util/z;->a(ZJ)V

    :cond_16
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v6, v2

    :goto_2a
    if-ltz v6, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/client/j;

    invoke-virtual {v2, v3, v4, v5, v7}, Lcom/twitter/library/client/j;->f(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V

    add-int/lit8 v2, v6, -0x1

    move v6, v2

    goto :goto_2a

    :sswitch_2a
    const-string/jumbo v2, "custom_errors"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v6

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v8, v2

    :goto_2b
    if-ltz v8, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/client/j;

    invoke-virtual/range {v2 .. v7}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;I[ILjava/lang/String;)V

    add-int/lit8 v2, v8, -0x1

    move v8, v2

    goto :goto_2b

    :sswitch_2b
    invoke-static/range {v20 .. v20}, Lcom/twitter/library/service/b;->b(Landroid/os/Bundle;)[I

    move-result-object v6

    const-string/jumbo v2, "user_id"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v8

    const/16 v2, 0xc8

    if-eq v5, v2, :cond_17

    array-length v9, v8

    const/4 v2, 0x0

    :goto_2c
    if-ge v2, v9, :cond_17

    aget-wide v11, v8, v2

    const/4 v13, 0x1

    invoke-virtual {v10, v11, v12, v13}, Lcom/twitter/android/client/al;->c(JI)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_2c

    :cond_17
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v9, v2

    :goto_2d
    if-ltz v9, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/client/j;

    invoke-virtual/range {v2 .. v8}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;I[ILjava/lang/String;[J)V

    add-int/lit8 v2, v9, -0x1

    move v9, v2

    goto :goto_2d

    :sswitch_2c
    const-string/jumbo v2, "settings"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/ClientConfiguration;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const-string/jumbo v6, "config"

    const/4 v7, 0x0

    invoke-virtual {v4, v6, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    if-eqz v2, :cond_1a

    iget-object v4, v2, Lcom/twitter/library/api/ClientConfiguration;->a:Lcom/twitter/library/api/UrlConfiguration;

    iget-object v2, v2, Lcom/twitter/library/api/ClientConfiguration;->b:Lcom/twitter/library/api/ad;

    :goto_2e
    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/android/client/c;->N:Lcom/twitter/library/api/UrlConfiguration;

    const/16 v6, 0xc8

    if-ne v5, v6, :cond_4

    if-eqz v4, :cond_18

    iget-object v6, v4, Lcom/twitter/library/api/UrlConfiguration;->c:Ljava/util/ArrayList;

    if-nez v6, :cond_1b

    const-string/jumbo v6, ""

    :goto_2f
    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    const-string/jumbo v9, "short_url_len"

    iget v10, v4, Lcom/twitter/library/api/UrlConfiguration;->a:I

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    const-string/jumbo v9, "url_whitelist"

    invoke-interface {v8, v9, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string/jumbo v8, "scribe_url"

    iget-object v4, v4, Lcom/twitter/library/api/UrlConfiguration;->b:Ljava/lang/String;

    invoke-interface {v6, v8, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_18
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    if-eqz v2, :cond_1c

    iget-object v4, v2, Lcom/twitter/library/api/ad;->i:Ljava/util/HashMap;

    invoke-static {v4}, Lcom/twitter/library/network/d;->a(Ljava/util/HashMap;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/client/c;->T:Lcom/twitter/library/api/ad;

    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string/jumbo v6, "twitter_access_timestamp"

    invoke-interface {v4, v6, v8, v9}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_19
    :goto_30
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/client/c;->T:Lcom/twitter/library/api/ad;

    goto/16 :goto_2

    :cond_1a
    const/4 v4, 0x0

    const/4 v2, 0x0

    goto :goto_2e

    :cond_1b
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, ","

    iget-object v9, v4, Lcom/twitter/library/api/UrlConfiguration;->c:Ljava/util/ArrayList;

    invoke-static {v8, v9}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v8, 0x2c

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_2f

    :cond_1c
    const-string/jumbo v4, "twitter_access_timestamp"

    const-wide/16 v10, 0x0

    invoke-interface {v7, v4, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/client/c;->T:Lcom/twitter/library/api/ad;

    if-eqz v4, :cond_1d

    iget-wide v10, v4, Lcom/twitter/library/api/ad;->k:J

    add-long/2addr v6, v10

    cmp-long v4, v6, v8

    if-gez v4, :cond_19

    :cond_1d
    const/4 v4, 0x0

    invoke-static {v4}, Lcom/twitter/library/network/d;->a(Ljava/util/HashMap;)V

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/android/client/c;->T:Lcom/twitter/library/api/ad;

    goto :goto_30

    :sswitch_2d
    const-string/jumbo v2, "device_operator_configuration"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/DeviceOperatorConfiguration;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const-string/jumbo v6, "config"

    const/4 v7, 0x0

    invoke-virtual {v4, v6, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string/jumbo v6, "device_operator_shortcode"

    iget-object v7, v2, Lcom/twitter/library/api/DeviceOperatorConfiguration;->a:Ljava/lang/String;

    invoke-interface {v4, v6, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string/jumbo v6, "device_operator_mt_sms"

    iget-boolean v7, v2, Lcom/twitter/library/api/DeviceOperatorConfiguration;->b:Z

    invoke-interface {v4, v6, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string/jumbo v6, "device_operator_mode"

    iget-object v2, v2, Lcom/twitter/library/api/DeviceOperatorConfiguration;->c:Ljava/lang/String;

    invoke-interface {v4, v6, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_2

    :sswitch_2e
    const-string/jumbo v2, "old_screen_name"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const-string/jumbo v2, "settings"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v13

    check-cast v13, Lcom/twitter/library/api/UserSettings;

    const/16 v2, 0xc8

    if-ne v5, v2, :cond_1e

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v13}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/UserSettings;)V

    iget-object v2, v13, Lcom/twitter/library/api/UserSettings;->m:Ljava/lang/String;

    if-eqz v14, :cond_1e

    if-eqz v2, :cond_1e

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1e

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v13, v14}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/UserSettings;Ljava/lang/String;)V

    :cond_1e
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v6, v2

    :goto_31
    if-ltz v6, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v13}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;Lcom/twitter/library/api/UserSettings;)V

    if-eqz v14, :cond_1f

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/client/j;

    invoke-virtual {v2, v3, v4, v5}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;I)V

    :cond_1f
    add-int/lit8 v2, v6, -0x1

    move v6, v2

    goto :goto_31

    :sswitch_2f
    const-string/jumbo v2, "settings"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v13

    check-cast v13, Lcom/twitter/library/api/UserSettings;

    const/16 v2, 0xc8

    if-ne v5, v2, :cond_20

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v13}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/UserSettings;)V

    const-string/jumbo v2, "force_e_discoverable"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_20

    iget-boolean v2, v13, Lcom/twitter/library/api/UserSettings;->l:Z

    if-nez v2, :cond_20

    const/4 v2, 0x1

    iput-boolean v2, v13, Lcom/twitter/library/api/UserSettings;->l:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v13, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/UserSettings;Z)Ljava/lang/String;

    :cond_20
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_32
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v13}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;Lcom/twitter/library/api/UserSettings;)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_32

    :sswitch_30
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_33
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/client/j;

    invoke-virtual {v2, v3, v4, v5, v7}, Lcom/twitter/library/client/j;->d(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_33

    :sswitch_31
    const-string/jumbo v2, "custom_errors"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v13

    check-cast v13, Ljava/util/ArrayList;

    const/16 v2, 0xc8

    if-eq v5, v2, :cond_21

    const/16 v2, 0x193

    if-ne v5, v2, :cond_23

    :cond_21
    const-string/jumbo v2, "old_screen_name"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v2, "new_screen_name"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/library/api/UserSettings;

    move-result-object v2

    const/16 v9, 0xc8

    if-ne v5, v9, :cond_24

    const-string/jumbo v2, "settings"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/UserSettings;

    :cond_22
    :goto_34
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/UserSettings;)V

    invoke-static {v13}, Lcom/twitter/android/tg;->c(Ljava/util/ArrayList;)Lcom/twitter/library/api/al;

    move-result-object v9

    if-nez v9, :cond_23

    if-eqz v6, :cond_23

    if-eqz v8, :cond_23

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_23

    iput-object v8, v2, Lcom/twitter/library/api/UserSettings;->m:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2, v6}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/UserSettings;Ljava/lang/String;)V

    :cond_23
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_35
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v13}, Lcom/twitter/library/client/j;->b(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;Ljava/util/ArrayList;)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_35

    :cond_24
    const/16 v9, 0x193

    if-ne v5, v9, :cond_22

    if-eqz v2, :cond_22

    const-string/jumbo v9, "protected"

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_25

    const-string/jumbo v9, "protected"

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    iput-boolean v9, v2, Lcom/twitter/library/api/UserSettings;->j:Z

    :cond_25
    const-string/jumbo v9, "discoverable_by_email"

    const/4 v10, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    iput-boolean v9, v2, Lcom/twitter/library/api/UserSettings;->i:Z

    const-string/jumbo v9, "discoverable_by_mobile_phone"

    const/4 v10, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    iput-boolean v9, v2, Lcom/twitter/library/api/UserSettings;->l:Z

    goto :goto_34

    :sswitch_32
    const-string/jumbo v2, "unread_dm"

    const/4 v6, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v6, v2

    :goto_36
    if-ltz v6, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/client/j;

    invoke-virtual {v2, v3, v8}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;I)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/client/j;

    invoke-virtual {v2, v3, v4, v5, v7}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V

    add-int/lit8 v2, v6, -0x1

    move v6, v2

    goto :goto_36

    :sswitch_33
    const-string/jumbo v2, "settings"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/Decider;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-boolean v7, v2, Lcom/twitter/library/api/Decider;->a:Z

    iget-wide v8, v2, Lcom/twitter/library/api/Decider;->b:J

    iget v10, v2, Lcom/twitter/library/api/Decider;->c:I

    iget v11, v2, Lcom/twitter/library/api/Decider;->d:I

    iget v12, v2, Lcom/twitter/library/api/Decider;->e:I

    invoke-static/range {v6 .. v12}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;ZJIII)V

    const/4 v4, 0x0

    invoke-static {v6, v4}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Z)V

    invoke-static {v6}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const/4 v4, 0x0

    iget v8, v2, Lcom/twitter/library/api/Decider;->f:I

    if-lez v8, :cond_26

    const-string/jumbo v4, "typeahead_users_size"

    iget v8, v2, Lcom/twitter/library/api/Decider;->f:I

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_26
    iget-wide v8, v2, Lcom/twitter/library/api/Decider;->g:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-lez v8, :cond_27

    const-string/jumbo v4, "typeahead_users_ttl"

    iget-wide v8, v2, Lcom/twitter/library/api/Decider;->g:J

    invoke-interface {v7, v4, v8, v9}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_27
    iget v8, v2, Lcom/twitter/library/api/Decider;->h:I

    if-lez v8, :cond_28

    const-string/jumbo v4, "typeahead_topics_size"

    iget v8, v2, Lcom/twitter/library/api/Decider;->h:I

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_28
    iget-wide v8, v2, Lcom/twitter/library/api/Decider;->i:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-lez v8, :cond_29

    const-string/jumbo v4, "typeahead_topics_ttl"

    iget-wide v8, v2, Lcom/twitter/library/api/Decider;->i:J

    invoke-interface {v7, v4, v8, v9}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_29
    iget v8, v2, Lcom/twitter/library/api/Decider;->j:I

    if-lez v8, :cond_2a

    const-string/jumbo v4, "typeahead_max_recent"

    iget v8, v2, Lcom/twitter/library/api/Decider;->j:I

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_2a
    iget v8, v2, Lcom/twitter/library/api/Decider;->k:I

    if-lez v8, :cond_2b

    const-string/jumbo v4, "typeahead_max_topics"

    iget v8, v2, Lcom/twitter/library/api/Decider;->k:I

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_2b
    iget v8, v2, Lcom/twitter/library/api/Decider;->l:I

    if-lez v8, :cond_2c

    const-string/jumbo v4, "typeahead_max_user"

    iget v8, v2, Lcom/twitter/library/api/Decider;->l:I

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_2c
    iget v8, v2, Lcom/twitter/library/api/Decider;->m:I

    if-lez v8, :cond_2d

    const-string/jumbo v4, "typeahead_max_compose"

    iget v8, v2, Lcom/twitter/library/api/Decider;->m:I

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_2d
    iget-wide v8, v2, Lcom/twitter/library/api/Decider;->n:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-lez v8, :cond_2e

    const-string/jumbo v4, "typeahead_throttle"

    iget-wide v8, v2, Lcom/twitter/library/api/Decider;->n:J

    invoke-interface {v7, v4, v8, v9}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_2e
    iget v8, v2, Lcom/twitter/library/api/Decider;->o:I

    if-lez v8, :cond_2f

    const-string/jumbo v4, "suggestions_max_trend"

    iget v8, v2, Lcom/twitter/library/api/Decider;->o:I

    int-to-long v8, v8

    invoke-interface {v7, v4, v8, v9}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_2f
    iget-wide v8, v2, Lcom/twitter/library/api/Decider;->p:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-lez v8, :cond_30

    const-string/jumbo v4, "find_friends_interval"

    iget-wide v8, v2, Lcom/twitter/library/api/Decider;->p:J

    invoke-interface {v7, v4, v8, v9}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_30
    iget v8, v2, Lcom/twitter/library/api/Decider;->s:I

    if-ltz v8, :cond_31

    const-string/jumbo v4, "abd_enabled_d"

    iget v8, v2, Lcom/twitter/library/api/Decider;->s:I

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_31
    iget v8, v2, Lcom/twitter/library/api/Decider;->u:I

    if-ltz v8, :cond_32

    const-string/jumbo v4, "pb_enabled"

    iget v8, v2, Lcom/twitter/library/api/Decider;->u:I

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_32
    iget v8, v2, Lcom/twitter/library/api/Decider;->A:I

    if-ltz v8, :cond_33

    const-string/jumbo v4, "antispam_connect_tweet_count"

    iget v8, v2, Lcom/twitter/library/api/Decider;->A:I

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_33
    iget v8, v2, Lcom/twitter/library/api/Decider;->B:I

    if-ltz v8, :cond_34

    const-string/jumbo v4, "antispam_connect_user_count"

    iget v8, v2, Lcom/twitter/library/api/Decider;->B:I

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_34
    iget v8, v2, Lcom/twitter/library/api/Decider;->C:I

    if-ltz v8, :cond_35

    const-string/jumbo v4, "antispam_query_frequency"

    iget v8, v2, Lcom/twitter/library/api/Decider;->C:I

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_35
    iget v8, v2, Lcom/twitter/library/api/Decider;->F:I

    if-ltz v8, :cond_36

    const-string/jumbo v4, "report_tweet_user_fraction"

    iget v8, v2, Lcom/twitter/library/api/Decider;->F:I

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_36
    iget-object v8, v2, Lcom/twitter/library/api/Decider;->t:Ljava/util/ArrayList;

    if-eqz v8, :cond_38

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v2, Lcom/twitter/library/api/Decider;->t:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_37
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_37

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v10, ","

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_37

    :cond_37
    const-string/jumbo v4, "abd_whitelist"

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_38
    iget-object v8, v2, Lcom/twitter/library/api/Decider;->D:Ljava/lang/String;

    if-eqz v8, :cond_39

    const-string/jumbo v4, "ihb"

    iget-object v8, v2, Lcom/twitter/library/api/Decider;->D:Ljava/lang/String;

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_39
    iget v8, v2, Lcom/twitter/library/api/Decider;->E:I

    if-ltz v8, :cond_3a

    const-string/jumbo v4, "dm_sync_user_fraction"

    iget v8, v2, Lcom/twitter/library/api/Decider;->E:I

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_3a
    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->G:Z

    const-string/jumbo v9, "lifeline_alerts_enabled"

    const/4 v10, 0x0

    invoke-interface {v6, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eq v8, v9, :cond_3b

    const-string/jumbo v4, "lifeline_alerts_enabled"

    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->G:Z

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_3b
    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->H:Z

    const-string/jumbo v9, "rt_fav_settings_enabled"

    const/4 v10, 0x0

    invoke-interface {v6, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eq v8, v9, :cond_3c

    const-string/jumbo v4, "rt_fav_settings_enabled"

    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->H:Z

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_3c
    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->I:Z

    const-string/jumbo v9, "conversations_enabled"

    const/4 v10, 0x1

    invoke-interface {v6, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eq v8, v9, :cond_3d

    const-string/jumbo v4, "conversations_enabled"

    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->I:Z

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_3d
    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->J:Z

    const-string/jumbo v9, "new_connect_types_enabled"

    const/4 v10, 0x1

    invoke-interface {v6, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eq v8, v9, :cond_3e

    const-string/jumbo v4, "new_connect_types_enabled"

    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->J:Z

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_3e
    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->K:Z

    const-string/jumbo v9, "perch_blur_enabled"

    const/4 v10, 0x1

    invoke-interface {v6, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eq v8, v9, :cond_3f

    const-string/jumbo v4, "perch_blur_enabled"

    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->K:Z

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_3f
    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->L:Z

    const-string/jumbo v9, "media_forward_enabled"

    const/4 v10, 0x1

    invoke-interface {v6, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eq v8, v9, :cond_40

    const-string/jumbo v4, "media_forward_enabled"

    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->L:Z

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_40
    iget v8, v2, Lcom/twitter/library/api/Decider;->M:I

    if-ltz v8, :cond_41

    const-string/jumbo v4, "linger_user_fraction"

    iget v8, v2, Lcom/twitter/library/api/Decider;->M:I

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_41
    iget v8, v2, Lcom/twitter/library/api/Decider;->N:F

    const/4 v9, 0x0

    cmpl-float v8, v8, v9

    if-ltz v8, :cond_42

    const-string/jumbo v4, "status_linger_minimum_threshold"

    iget v8, v2, Lcom/twitter/library/api/Decider;->N:F

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_42
    iget v8, v2, Lcom/twitter/library/api/Decider;->O:F

    const/4 v9, 0x0

    cmpl-float v8, v8, v9

    if-ltz v8, :cond_43

    const-string/jumbo v4, "status_linger_maximum_threshold"

    iget v8, v2, Lcom/twitter/library/api/Decider;->O:F

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_43
    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->v:Z

    const-string/jumbo v9, "geo_data_provider_enabled"

    const/4 v10, 0x0

    invoke-interface {v6, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eq v8, v9, :cond_44

    const-string/jumbo v4, "geo_data_provider_enabled"

    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->v:Z

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_44
    iget v8, v2, Lcom/twitter/library/api/Decider;->w:I

    const-string/jumbo v9, "geo_data_provider_update_delay"

    const/4 v10, -0x1

    invoke-interface {v6, v9, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v9

    if-eq v8, v9, :cond_45

    const-string/jumbo v4, "geo_data_provider_update_delay"

    iget v8, v2, Lcom/twitter/library/api/Decider;->w:I

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_45
    iget v8, v2, Lcom/twitter/library/api/Decider;->x:I

    const-string/jumbo v9, "geo_data_provider_update_duration"

    const/4 v10, -0x1

    invoke-interface {v6, v9, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v9

    if-eq v8, v9, :cond_46

    const-string/jumbo v4, "geo_data_provider_update_duration"

    iget v8, v2, Lcom/twitter/library/api/Decider;->x:I

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_46
    iget v8, v2, Lcom/twitter/library/api/Decider;->y:I

    const-string/jumbo v9, "geo_data_provider_update_interval"

    const/4 v10, -0x1

    invoke-interface {v6, v9, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v9

    if-eq v8, v9, :cond_47

    const-string/jumbo v4, "geo_data_provider_update_interval"

    iget v8, v2, Lcom/twitter/library/api/Decider;->y:I

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_47
    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->P:Z

    const-string/jumbo v9, "device_follow_button_enabled"

    const/4 v10, 0x0

    invoke-interface {v6, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eq v8, v9, :cond_48

    const-string/jumbo v4, "device_follow_button_enabled"

    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->P:Z

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_48
    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->Q:Z

    const-string/jumbo v9, "inline_follow_enabled"

    const/4 v10, 0x0

    invoke-interface {v6, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eq v8, v9, :cond_49

    const-string/jumbo v4, "inline_follow_enabled"

    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->Q:Z

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_49
    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->R:Z

    const-string/jumbo v9, "inline_follow_user_fetch_enabled"

    const/4 v10, 0x0

    invoke-interface {v6, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eq v8, v9, :cond_4a

    const-string/jumbo v4, "inline_follow_user_fetch_enabled"

    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->R:Z

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_4a
    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->T:Z

    const-string/jumbo v9, "media_smart_crop_enabled"

    const/4 v10, 0x1

    invoke-interface {v6, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eq v8, v9, :cond_4b

    const-string/jumbo v4, "media_smart_crop_enabled"

    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->T:Z

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_4b
    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->U:Z

    const-string/jumbo v9, "amplify_player_enabled"

    const/4 v10, 0x1

    invoke-interface {v6, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eq v8, v9, :cond_4c

    const-string/jumbo v4, "amplify_player_enabled"

    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->U:Z

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_4c
    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->V:Z

    const-string/jumbo v9, "alerts_activation_enabled"

    const/4 v10, 0x0

    invoke-interface {v6, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eq v8, v9, :cond_4d

    const-string/jumbo v4, "alerts_activation_enabled"

    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->V:Z

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_4d
    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->W:Z

    const-string/jumbo v9, "message_compose_suggestions_enabled"

    const/4 v10, 0x1

    invoke-interface {v6, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eq v8, v9, :cond_4e

    const-string/jumbo v4, "message_compose_suggestions_enabled"

    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->W:Z

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_4e
    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->X:Z

    const-string/jumbo v9, "message_compose_retry_enabled"

    const/4 v10, 0x1

    invoke-interface {v6, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eq v8, v9, :cond_4f

    const-string/jumbo v4, "message_compose_retry_enabled"

    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->X:Z

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_4f
    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->Y:Z

    const-string/jumbo v9, "show_reply_count_with_media"

    const/4 v10, 0x0

    invoke-interface {v6, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eq v8, v9, :cond_50

    const-string/jumbo v4, "show_reply_count_with_media"

    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->Y:Z

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_50
    iget v8, v2, Lcom/twitter/library/api/Decider;->Z:I

    if-ltz v8, :cond_51

    const-string/jumbo v4, "dm_photo_compose_user_fraction"

    iget v8, v2, Lcom/twitter/library/api/Decider;->Z:I

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_51
    iget v8, v2, Lcom/twitter/library/api/Decider;->aa:I

    if-ltz v8, :cond_52

    const-string/jumbo v4, "dm_photo_render_user_fraction"

    iget v8, v2, Lcom/twitter/library/api/Decider;->aa:I

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_52
    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->ab:Z

    const-string/jumbo v9, "media_forward_tweet_detail"

    const/4 v10, 0x1

    invoke-interface {v6, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eq v8, v9, :cond_53

    const-string/jumbo v4, "media_forward_tweet_detail"

    iget-boolean v8, v2, Lcom/twitter/library/api/Decider;->ab:Z

    invoke-interface {v7, v4, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const/4 v4, 0x1

    :cond_53
    if-eqz v4, :cond_54

    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_54
    iget-object v4, v2, Lcom/twitter/library/api/Decider;->r:Ljava/util/ArrayList;

    if-eqz v4, :cond_56

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_38
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_55

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v9, ","

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_38

    :cond_55
    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string/jumbo v6, "web_view_url_whitelist"

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v4, v6, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_56
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/client/c;->V:Lcom/twitter/library/platform/LocationProducer;

    iget-boolean v6, v2, Lcom/twitter/library/api/Decider;->v:Z

    iget v7, v2, Lcom/twitter/library/api/Decider;->w:I

    iget v8, v2, Lcom/twitter/library/api/Decider;->x:I

    iget v9, v2, Lcom/twitter/library/api/Decider;->y:I

    invoke-virtual {v4, v6, v7, v8, v9}, Lcom/twitter/library/platform/LocationProducer;->a(ZIII)V

    iget-boolean v4, v2, Lcom/twitter/library/api/Decider;->G:Z

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/twitter/android/client/c;->m:Z

    iget-boolean v4, v2, Lcom/twitter/library/api/Decider;->L:Z

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/twitter/android/client/c;->o:Z

    iget v4, v2, Lcom/twitter/library/api/Decider;->z:I

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/android/client/c;->n:I

    iget-boolean v4, v2, Lcom/twitter/library/api/Decider;->Q:Z

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/twitter/android/client/c;->r:Z

    iget-boolean v4, v2, Lcom/twitter/library/api/Decider;->R:Z

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/twitter/android/client/c;->s:Z

    iget-boolean v4, v2, Lcom/twitter/library/api/Decider;->T:Z

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/twitter/android/client/c;->v:Z

    iget-boolean v4, v2, Lcom/twitter/library/api/Decider;->S:Z

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/twitter/android/client/c;->t:Z

    iget-boolean v4, v2, Lcom/twitter/library/api/Decider;->U:Z

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/twitter/android/client/c;->X:Z

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/Decider;)V

    goto/16 :goto_2

    :sswitch_34
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v6, v2

    :goto_39
    if-ltz v6, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/client/j;

    invoke-virtual {v2, v3, v4, v5, v7}, Lcom/twitter/library/client/j;->j(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V

    add-int/lit8 v2, v6, -0x1

    move v6, v2

    goto :goto_39

    :sswitch_35
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v6, v2

    :goto_3a
    if-ltz v6, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/client/j;

    invoke-virtual {v2, v3, v4, v5, v7}, Lcom/twitter/library/client/j;->k(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V

    add-int/lit8 v2, v6, -0x1

    move v6, v2

    goto :goto_3a

    :sswitch_36
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_3b
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    const/4 v13, 0x0

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v13}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;Z)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_3b

    :sswitch_37
    const-string/jumbo v2, "owner_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v13

    const-string/jumbo v2, "user_type"

    const/4 v6, -0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v15

    const-string/jumbo v2, "user_tag"

    const-wide/16 v8, -0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v16

    const-string/jumbo v2, "user_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v18

    const-string/jumbo v2, "user"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v20

    check-cast v20, Lcom/twitter/library/api/TwitterUser;

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_3c
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v20}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JIJJLcom/twitter/library/api/TwitterUser;)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_3c

    :sswitch_38
    const-string/jumbo v2, "i_type"

    const/4 v6, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v13

    const-string/jumbo v2, "since_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v14

    const-string/jumbo v2, "max_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v16

    const-string/jumbo v2, "count"

    const/4 v6, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v18

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_3d
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    const/16 v19, 0x0

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v19}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;IJJIZ)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_3d

    :sswitch_39
    const-string/jumbo v2, "status_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v13

    const-string/jumbo v2, "new_tweet"

    const/4 v6, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v15

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_3e
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/service/b;->t()Z

    move-result v16

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v16}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JIZ)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_3e

    :sswitch_3a
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v6, v2

    :goto_3f
    if-ltz v6, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/client/j;

    invoke-virtual {v2, v3, v4, v5, v7}, Lcom/twitter/library/client/j;->b(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V

    add-int/lit8 v2, v6, -0x1

    move v6, v2

    goto :goto_3f

    :sswitch_3b
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v6, v2

    :goto_40
    if-ltz v6, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/client/j;

    invoke-virtual {v2, v3, v4, v5, v7}, Lcom/twitter/library/client/j;->c(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V

    add-int/lit8 v2, v6, -0x1

    move v6, v2

    goto :goto_40

    :sswitch_3c
    const-string/jumbo v2, "screen_name_suggestions"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v13

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_41
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v13}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;Ljava/util/ArrayList;)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_41

    :sswitch_3d
    const-string/jumbo v2, "q"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v15

    check-cast v15, Lcom/twitter/library/api/ActivitySummary;

    const-string/jumbo v2, "status_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v13

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_42
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v15}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JLcom/twitter/library/api/ActivitySummary;)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_42

    :sswitch_3e
    const-string/jumbo v2, "users"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v13

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_43
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v13}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;[J)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_43

    :sswitch_3f
    const-string/jumbo v2, "lang"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string/jumbo v2, "locale"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const-string/jumbo v2, "locations"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v15

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_44
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v15}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_44

    :sswitch_40
    const-string/jumbo v2, "reason_phrase"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_57

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const v4, 0x7f0f000c    # com.twitter.android.R.string.action_error

    const/4 v6, 0x1

    invoke-static {v2, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_2

    :cond_57
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const/4 v6, 0x1

    invoke-static {v4, v2, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_2

    :sswitch_41
    const-string/jumbo v2, "reason_phrase"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v20 .. v20}, Lcom/twitter/library/service/b;->b(Landroid/os/Bundle;)[I

    move-result-object v7

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v4, v2

    :goto_45
    if-ltz v4, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/client/j;

    invoke-virtual {v2, v3, v5, v6, v7}, Lcom/twitter/library/client/j;->b(Lcom/twitter/library/client/Session;ILjava/lang/String;[I)V

    add-int/lit8 v2, v4, -0x1

    move v4, v2

    goto :goto_45

    :sswitch_42
    const-string/jumbo v2, "reason_phrase"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v20 .. v20}, Lcom/twitter/library/service/b;->b(Landroid/os/Bundle;)[I

    move-result-object v7

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v4, v2

    :goto_46
    if-ltz v4, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/client/j;

    invoke-virtual {v2, v3, v5, v6, v7}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;ILjava/lang/String;[I)V

    add-int/lit8 v2, v4, -0x1

    move v4, v2

    goto :goto_46

    :sswitch_43
    const-string/jumbo v2, "reason_phrase"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static/range {v20 .. v20}, Lcom/twitter/library/service/b;->b(Landroid/os/Bundle;)[I

    move-result-object v10

    const-string/jumbo v2, "login_verification_enrolled"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v11

    check-cast v11, Lcom/twitter/library/api/account/LvEligibilityResponse;

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_47
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/library/client/j;

    move-object v7, v3

    move v8, v5

    invoke-virtual/range {v6 .. v11}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;ILjava/lang/String;[ILcom/twitter/library/api/account/LvEligibilityResponse;)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_47

    :sswitch_44
    invoke-static {}, Lcom/twitter/library/client/App;->b()Z

    move-result v2

    if-eqz v2, :cond_4

    const-string/jumbo v2, "name"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v4}, Lcom/twitter/android/client/aw;->a(Landroid/content/Context;)Lcom/twitter/android/client/aw;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/twitter/android/client/aw;->a(Ljava/io/File;)V

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move v6, v4

    :goto_48
    if-ltz v6, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/library/client/j;

    invoke-virtual {v4, v3, v2}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/io/File;)V

    add-int/lit8 v4, v6, -0x1

    move v6, v4

    goto :goto_48

    :sswitch_45
    const-string/jumbo v2, "users"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    move v7, v6

    :goto_49
    if-ltz v7, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/library/client/j;

    invoke-virtual {v6, v3, v4, v5, v2}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/util/HashMap;)V

    add-int/lit8 v6, v7, -0x1

    move v7, v6

    goto :goto_49

    :sswitch_46
    const-string/jumbo v2, "act_read_pos"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v8, v2

    :goto_4a
    if-ltz v8, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/client/j;

    invoke-virtual/range {v2 .. v7}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;IJ)V

    add-int/lit8 v2, v8, -0x1

    move v8, v2

    goto :goto_4a

    :sswitch_47
    const-string/jumbo v2, "translated"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v13

    check-cast v13, Lcom/twitter/library/api/TwitterStatus$Translation;

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_4b
    if-ltz v2, :cond_4

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/client/j;

    move-object v9, v3

    move-object v10, v4

    move v11, v5

    move-object v12, v7

    invoke-virtual/range {v8 .. v13}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;Lcom/twitter/library/api/TwitterStatus$Translation;)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_4b

    :sswitch_48
    const-string/jumbo v2, "app_updated"

    const/4 v4, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/twitter/android/client/c;->f(Lcom/twitter/library/client/Session;)V

    goto/16 :goto_2

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_a
        0x7 -> :sswitch_0
        0x8 -> :sswitch_1
        0xc -> :sswitch_2
        0xe -> :sswitch_3
        0xf -> :sswitch_4
        0x10 -> :sswitch_7
        0x11 -> :sswitch_8
        0x14 -> :sswitch_9
        0x16 -> :sswitch_b
        0x17 -> :sswitch_c
        0x19 -> :sswitch_d
        0x1a -> :sswitch_10
        0x1b -> :sswitch_11
        0x1c -> :sswitch_14
        0x1d -> :sswitch_12
        0x1e -> :sswitch_16
        0x1f -> :sswitch_17
        0x20 -> :sswitch_18
        0x22 -> :sswitch_19
        0x23 -> :sswitch_1a
        0x24 -> :sswitch_1b
        0x25 -> :sswitch_1c
        0x32 -> :sswitch_1e
        0x33 -> :sswitch_1f
        0x34 -> :sswitch_21
        0x35 -> :sswitch_20
        0x36 -> :sswitch_22
        0x37 -> :sswitch_24
        0x38 -> :sswitch_25
        0x3a -> :sswitch_2b
        0x3b -> :sswitch_2c
        0x3c -> :sswitch_2e
        0x3d -> :sswitch_2f
        0x3e -> :sswitch_32
        0x3f -> :sswitch_33
        0x40 -> :sswitch_34
        0x41 -> :sswitch_35
        0x42 -> :sswitch_38
        0x43 -> :sswitch_36
        0x44 -> :sswitch_39
        0x45 -> :sswitch_23
        0x46 -> :sswitch_3a
        0x47 -> :sswitch_3b
        0x48 -> :sswitch_3d
        0x4c -> :sswitch_1d
        0x4d -> :sswitch_5
        0x4f -> :sswitch_3e
        0x53 -> :sswitch_3c
        0x54 -> :sswitch_27
        0x56 -> :sswitch_37
        0x57 -> :sswitch_1d
        0x59 -> :sswitch_3f
        0x61 -> :sswitch_41
        0x62 -> :sswitch_42
        0x66 -> :sswitch_43
        0x69 -> :sswitch_a
        0x6c -> :sswitch_15
        0x6e -> :sswitch_32
        0x6f -> :sswitch_40
        0x70 -> :sswitch_13
        0x74 -> :sswitch_48
        0x75 -> :sswitch_45
        0x76 -> :sswitch_46
        0x78 -> :sswitch_28
        0x79 -> :sswitch_29
        0x7a -> :sswitch_26
        0x7f -> :sswitch_47
        0x80 -> :sswitch_6
        0x81 -> :sswitch_2d
        0x82 -> :sswitch_2a
        0x83 -> :sswitch_30
        0x84 -> :sswitch_31
        0x3e7 -> :sswitch_44
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0xc8
        :pswitch_0
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        0xc8 -> :sswitch_e
        0x193 -> :sswitch_f
    .end sparse-switch
.end method

.method public a(Lcom/twitter/library/util/aa;Ljava/util/HashMap;)V
    .locals 3

    iget-object v2, p0, Lcom/twitter/android/client/c;->x:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/j;

    invoke-virtual {v0, p2}, Lcom/twitter/library/client/j;->a(Ljava/util/HashMap;)V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/util/ac;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/c;->b:Lcom/twitter/library/util/aa;

    invoke-virtual {v0, p1}, Lcom/twitter/library/util/aa;->a(Lcom/twitter/library/util/ac;)V

    return-void
.end method

.method a(Ljava/lang/String;JJ)V
    .locals 7

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    new-instance v6, Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v6, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v0, Lcom/twitter/android/client/n;

    move-object v1, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/client/n;-><init>(Lcom/twitter/android/client/c;Landroid/content/Context;Ljava/lang/String;J)V

    invoke-virtual {v6, v0, p4, p5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Ljava/util/ArrayList;)V

    return-void
.end method

.method public a(ZZ)V
    .locals 3

    iget-boolean v0, p0, Lcom/twitter/android/client/c;->P:Z

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "data_alerts_links"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    if-eqz p2, :cond_0

    if-nez p1, :cond_0

    iget-boolean v1, p0, Lcom/twitter/android/client/c;->Q:Z

    if-nez v1, :cond_0

    const-string/jumbo v1, "data_charges_alerts"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    iput-boolean p1, p0, Lcom/twitter/android/client/c;->P:Z

    invoke-direct {p0}, Lcom/twitter/android/client/c;->aq()V

    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/library/api/TwitterUser;[I)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/api/TwitterUser;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p2}, Lcom/twitter/android/client/c;->a([I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(Lcom/twitter/library/client/Session;Z)Z
    .locals 7

    iget-object v6, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    iget-object v4, p0, Lcom/twitter/android/client/c;->y:Ljava/util/HashMap;

    monitor-enter v4

    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/client/c;->y:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/WidgetControl;

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Lcom/twitter/android/client/WidgetControl;->a(Z)V

    :cond_0
    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/twitter/android/client/c;->D:Lcom/twitter/library/client/w;

    invoke-virtual {v0}, Lcom/twitter/library/client/w;->a()V

    iget-object v0, p0, Lcom/twitter/android/client/c;->w:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v5

    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v5, :cond_1

    iget-object v0, p0, Lcom/twitter/android/client/c;->w:Landroid/util/SparseArray;

    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ao;

    invoke-virtual {v0}, Lcom/twitter/library/util/ao;->a()V

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/client/c;->b:Lcom/twitter/library/util/aa;

    invoke-virtual {v0}, Lcom/twitter/library/util/aa;->a()V

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;)V

    :cond_2
    new-instance v0, Lcom/twitter/library/client/f;

    iget-object v4, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v0, v4, v1}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->c()Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V

    const-wide/32 v4, 0x493e0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/client/c;->a(Ljava/lang/String;JJ)V

    invoke-static {v2, v3}, Lju;->c(J)V

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/featureswitch/b;->f(J)V

    invoke-static {v6}, Lcom/twitter/library/platform/TwitterDataSyncService;->a(Landroid/content/Context;)V

    return p2
.end method

.method public a([I)Z
    .locals 1

    if-eqz p1, :cond_0

    const/16 v0, 0x40

    invoke-static {p1, v0}, Lcom/twitter/library/util/Util;->a([II)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aa()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/client/c;->o:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/client/c;->p:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ab()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/client/c;->r:Z

    return v0
.end method

.method public ac()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/client/c;->s:Z

    return v0
.end method

.method public ad()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/client/c;->v:Z

    return v0
.end method

.method public ae()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/client/c;->t:Z

    return v0
.end method

.method public af()Z
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/client/c;->ap()V

    iget-object v0, p0, Lcom/twitter/android/client/c;->T:Lcom/twitter/library/api/ad;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/c;->T:Lcom/twitter/library/api/ad;

    iget-boolean v0, v0, Lcom/twitter/library/api/ad;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ag()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/c;->T:Lcom/twitter/library/api/ad;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/c;->T:Lcom/twitter/library/api/ad;

    iget-object v0, v0, Lcom/twitter/library/api/ad;->j:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ah()Z
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/client/c;->ap()V

    iget-object v0, p0, Lcom/twitter/android/client/c;->T:Lcom/twitter/library/api/ad;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/c;->T:Lcom/twitter/library/api/ad;

    iget-boolean v0, v0, Lcom/twitter/library/api/ad;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/c;->T:Lcom/twitter/library/api/ad;

    iget-boolean v0, v0, Lcom/twitter/library/api/ad;->f:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/client/c;->P:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ai()Z
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/client/c;->ap()V

    iget-object v0, p0, Lcom/twitter/android/client/c;->T:Lcom/twitter/library/api/ad;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/c;->T:Lcom/twitter/library/api/ad;

    iget-boolean v0, v0, Lcom/twitter/library/api/ad;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/c;->T:Lcom/twitter/library/api/ad;

    iget-boolean v0, v0, Lcom/twitter/library/api/ad;->e:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/client/c;->Q:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aj()Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    const-string/jumbo v2, "android_readability_v2_2009"

    invoke-static {v2}, Lkk;->b(Ljava/lang/String;)V

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->W()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v3, "readability_mode"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    const-string/jumbo v2, "android_readability_v2_2009"

    new-array v3, v1, [Ljava/lang/String;

    const-string/jumbo v4, "readability_mode_bucket"

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    return v0
.end method

.method public ak()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/c;->b:Lcom/twitter/library/util/aa;

    invoke-virtual {v0}, Lcom/twitter/library/util/aa;->a()V

    iget-object v2, p0, Lcom/twitter/android/client/c;->w:Landroid/util/SparseArray;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ao;

    invoke-virtual {v0}, Lcom/twitter/library/util/ao;->a()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public al()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/client/c;->X:Z

    return v0
.end method

.method public b(Landroid/app/Activity;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;
    .locals 2

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0f0102    # com.twitter.android.R.string.dialog_no_location_service_message

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f03c6    # com.twitter.android.R.string.settings

    invoke-virtual {v0, v1, p2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f0089    # com.twitter.android.R.string.cancel

    invoke-virtual {v0, v1, p2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    if-eqz v0, :cond_0

    const-string/jumbo v1, "locale"

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "lang"

    invoke-static {v0}, Lcom/twitter/library/util/Util;->b(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method public b(Lcom/twitter/library/util/m;)Lcom/twitter/library/util/ae;
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/c;->b:Lcom/twitter/library/util/aa;

    iget-object v1, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2, p1}, Lcom/twitter/library/util/aa;->b(JLcom/twitter/library/util/m;)Lcom/twitter/library/util/ae;

    move-result-object v0

    return-object v0
.end method

.method public b(IJJ)Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/u;

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v2, 0x24

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "user_type"

    invoke-virtual {v1, v2, p1}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "list_id"

    invoke-virtual {v1, v2, p2, p3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "user_id"

    invoke-virtual {v1, v2, p4, p5}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "owner_id"

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(J)Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    if-eqz v0, :cond_0

    :goto_0
    invoke-static {v0}, Lcom/twitter/library/util/Util;->b(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/u;

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v2, 0x7f

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "status_id"

    invoke-virtual {v1, v2, p1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "lang"

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    goto :goto_0
.end method

.method public b(JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-direct {p0, p3, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/client/Session;)Lcom/twitter/library/client/u;

    move-result-object v1

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/api/TwitterUser;)Lcom/twitter/library/client/u;

    move-result-object v1

    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "user_id"

    invoke-virtual {v1, v2, p1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "owner_id"

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "lifeline_follow"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(JLjava/lang/String;)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/client/c;->a(JLcom/twitter/library/client/Session;)Lcom/twitter/library/client/u;

    move-result-object v0

    const/16 v1, 0x34

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "q"

    invoke-virtual {v0, v1, p3}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/twitter/library/client/Session;)Ljava/lang/String;
    .locals 2

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    const/16 v1, 0x59

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/twitter/library/client/Session;JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;
    .locals 2

    invoke-direct {p0, p4, p1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/client/Session;)Lcom/twitter/library/client/u;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "status_id"

    invoke-virtual {v0, v1, p2, p3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/TwitterUser;Z)Ljava/lang/String;
    .locals 4

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "owner_id"

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Landroid/os/Parcelable;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "want_retweets"

    invoke-virtual {v0, v1, p3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "q"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/u;

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v2, 0x78

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "phone"

    invoke-virtual {v1, v2, p1}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "owner_id"

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "text_message"

    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Z)Ljava/lang/String;
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/c;->D:Lcom/twitter/library/client/w;

    new-instance v2, Lcom/twitter/library/client/u;

    iget-object v3, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/twitter/library/service/b;->c(Z)Lcom/twitter/library/service/b;

    move-result-object v2

    const-string/jumbo v3, "owner_id"

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v2, "dm_sync_enabled"

    invoke-virtual {p0}, Lcom/twitter/android/client/c;->I()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/client/c;->i:Lcom/twitter/library/service/a;

    invoke-virtual {v0, v2}, Lcom/twitter/library/service/b;->a(Lcom/twitter/internal/android/service/m;)Lcom/twitter/internal/android/service/a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/c;->H:Lcom/twitter/android/ii;

    invoke-virtual {v0}, Lcom/twitter/android/ii;->b()V

    return-void
.end method

.method public b(I)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    const-wide/16 v2, 0x2710

    rem-long/2addr v0, v2

    int-to-long v2, p1

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/client/c;->l:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(ILcom/twitter/library/util/ar;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/c;->w:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ao;

    invoke-virtual {v0, p2}, Lcom/twitter/library/util/ao;->b(Lcom/twitter/library/util/ar;)V

    return-void
.end method

.method b(Landroid/content/Intent;)V
    .locals 5

    const/4 v4, 0x0

    const-string/jumbo v0, "owner_id"

    const-wide/16 v1, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/client/c;->q(J)Lcom/twitter/android/client/WidgetControl;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string/jumbo v3, "status_type"

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/client/WidgetControl;->a(I)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "widget::::click"

    aput-object v3, v2, v4

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/library/client/Session;J)V
    .locals 2

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x30

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "status_id"

    invoke-virtual {v0, v1, p2, p3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    return-void
.end method

.method public b(Lcom/twitter/library/client/Session;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "lv_puvlic_key"

    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/client/u;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    :cond_0
    const/16 v1, 0x62

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    return-void
.end method

.method public b(Lcom/twitter/library/client/Session;Z)V
    .locals 7

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-lez v0, :cond_0

    iget-object v6, p0, Lcom/twitter/android/client/c;->y:Ljava/util/HashMap;

    monitor-enter v6

    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/client/c;->y:Ljava/util/HashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/WidgetControl;

    if-nez v0, :cond_1

    new-instance v0, Lcom/twitter/android/client/WidgetControl;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->w:Landroid/util/SparseArray;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/util/ao;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/client/WidgetControl;-><init>(Landroid/content/Context;Lcom/twitter/library/util/ao;Ljava/lang/String;J)V

    iget-object v1, p0, Lcom/twitter/android/client/c;->y:Ljava/util/HashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/twitter/android/client/WidgetControl;->b()V

    monitor-exit v6

    :cond_0
    :goto_0
    return-void

    :cond_1
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0, p2}, Lcom/twitter/android/client/WidgetControl;->b(Z)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b(Lcom/twitter/library/client/j;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/c;->x:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public b(Lcom/twitter/library/scribe/ScribeLog;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/twitter/library/scribe/ScribeService;->b(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method public b(Lcom/twitter/library/util/ac;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/c;->b:Lcom/twitter/library/util/aa;

    invoke-virtual {v0, p1}, Lcom/twitter/library/util/aa;->b(Lcom/twitter/library/util/ac;)V

    return-void
.end method

.method public b(ZZ)V
    .locals 3

    iget-boolean v0, p0, Lcom/twitter/android/client/c;->Q:Z

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "data_alerts_inline"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    if-eqz p2, :cond_0

    if-nez p1, :cond_0

    iget-boolean v1, p0, Lcom/twitter/android/client/c;->P:Z

    if-nez v1, :cond_0

    const-string/jumbo v1, "data_charges_alerts"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    iput-boolean p1, p0, Lcom/twitter/android/client/c;->Q:Z

    :cond_1
    return-void
.end method

.method public b([I)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/c;->d(Lcom/twitter/library/api/TwitterUser;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/client/c;->W()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, v0, p1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/TwitterUser;[I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/api/TwitterUser;)V

    goto :goto_0
.end method

.method public b([Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/c;->w:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/c;->w:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ao;

    invoke-virtual {v0, p1}, Lcom/twitter/library/util/ao;->a([Ljava/lang/String;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/library/api/TwitterUser;)Z
    .locals 1

    if-eqz p1, :cond_0

    iget-boolean v0, p1, Lcom/twitter/library/api/TwitterUser;->suspended:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;
    .locals 2

    const/4 v1, 0x1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "include_descendent_reply_count"

    invoke-virtual {p1, v0, v1}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    :cond_0
    :goto_0
    return-object p1

    :cond_1
    const-string/jumbo v0, "include_reply_count"

    invoke-virtual {p1, v0, v1}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    goto :goto_0
.end method

.method public c(IJJ)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x25

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user_type"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "list_id"

    invoke-virtual {v0, v1, p2, p3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1, p4, p5}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c(J)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "message_id"

    invoke-virtual {v0, v1, p1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c(JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/client/al;->a(Lcom/twitter/library/client/Session;)Lcom/twitter/android/client/al;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, p1, p2, v2}, Lcom/twitter/android/client/al;->b(JI)V

    invoke-direct {p0, p3, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/client/Session;)Lcom/twitter/library/client/u;

    move-result-object v1

    const/16 v2, 0x1b

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "owner_id"

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1, p1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c(Lcom/twitter/library/client/Session;)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const-string/jumbo v1, "force_e_discoverable"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/u;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c(Lcom/twitter/library/client/Session;J)Ljava/lang/String;
    .locals 2

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x77

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "act_read_pos"

    invoke-virtual {v0, v1, p2, p3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x41

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "q"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/u;

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v0, 0x37

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "screen_name"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "q"

    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c(I)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/c;->D:Lcom/twitter/library/client/w;

    new-instance v2, Lcom/twitter/library/api/q;

    iget-object v3, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lcom/twitter/library/api/q;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/twitter/library/api/q;->c(I)Lcom/twitter/library/service/b;

    move-result-object v2

    const-string/jumbo v3, "user_id"

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v2, "prompt_id"

    invoke-virtual {v0, v2, p1}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    return-void
.end method

.method public c(JLjava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v1, p1, p2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p3, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    const-string/jumbo v2, "app_download_client_event"

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->g(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    const-string/jumbo v2, "4"

    iget-object v3, p0, Lcom/twitter/android/client/c;->R:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method public c(Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, Lcom/twitter/library/util/v;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/twitter/library/util/v;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    const v0, 0x7f0f030b    # com.twitter.android.R.string.play_store_missing_error

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method c(Landroid/content/Intent;)V
    .locals 5

    const/4 v4, 0x0

    const-string/jumbo v0, "owner_id"

    const-wide/16 v1, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/client/c;->q(J)Lcom/twitter/android/client/WidgetControl;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string/jumbo v3, "status_type"

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/client/WidgetControl;->b(I)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "widget::::click"

    aput-object v3, v2, v4

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public c(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/twitter/android/client/c;->k:Z

    iget-object v0, p0, Lcom/twitter/android/client/c;->V:Lcom/twitter/library/platform/LocationProducer;

    iget-boolean v1, p0, Lcom/twitter/android/client/c;->k:Z

    invoke-virtual {v0, v1}, Lcom/twitter/library/platform/LocationProducer;->a(Z)V

    return-void
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/client/c;->j:Z

    return v0
.end method

.method public c(Lcom/twitter/library/api/TwitterUser;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/api/TwitterUser;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p1, Lcom/twitter/library/api/TwitterUser;->needsPhoneVerification:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;
    .locals 5

    iget-boolean v0, p0, Lcom/twitter/android/client/c;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/c;->V:Lcom/twitter/library/platform/LocationProducer;

    invoke-virtual {v0}, Lcom/twitter/library/platform/LocationProducer;->a()Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "lat"

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;D)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "long"

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;D)Lcom/twitter/library/service/b;

    :cond_0
    return-object p1
.end method

.method public d(J)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x69

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1, p1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "dm_sync_enabled"

    invoke-virtual {p0}, Lcom/twitter/android/client/c;->I()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d(JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/client/al;->a(Lcom/twitter/library/client/Session;)Lcom/twitter/android/client/al;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, p1, p2, v2}, Lcom/twitter/android/client/al;->c(JI)V

    invoke-direct {p0, p3, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/client/Session;)Lcom/twitter/library/client/u;

    move-result-object v1

    const/16 v2, 0x1d

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "owner_id"

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1, p1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d(Lcom/twitter/library/client/Session;)Ljava/lang/String;
    .locals 2

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x7e

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "q"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x53

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "email"

    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "name"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 1

    invoke-static {}, Lcom/twitter/library/util/Util;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/client/c;->f:Z

    return-void
.end method

.method public d(I)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/c;->D:Lcom/twitter/library/client/w;

    new-instance v2, Lcom/twitter/library/api/q;

    iget-object v3, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lcom/twitter/library/api/q;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/twitter/library/api/q;->c(I)Lcom/twitter/library/service/b;

    move-result-object v2

    const-string/jumbo v3, "user_id"

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v2, "prompt_id"

    invoke-virtual {v0, v2, p1}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    return-void
.end method

.method public d(Landroid/content/Context;)V
    .locals 13

    const/4 v12, 0x1

    iget-boolean v0, p0, Lcom/twitter/android/client/c;->O:Z

    if-nez v0, :cond_3

    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v7

    sget-object v0, Lcom/twitter/library/util/a;->a:Ljava/lang/String;

    invoke-virtual {v7, v0}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v8

    array-length v9, v8

    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v9, :cond_2

    aget-object v0, v8, v6

    const-string/jumbo v1, "account_user_info"

    invoke-virtual {v7, v0, v1}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    :try_start_0
    invoke-static {v0}, Lcom/twitter/library/api/ap;->b(Ljava/lang/String;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v10

    if-eqz v10, :cond_1

    iget-object v11, p0, Lcom/twitter/android/client/c;->y:Ljava/util/HashMap;

    monitor-enter v11
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lcom/twitter/android/client/c;->y:Ljava/util/HashMap;

    iget-wide v1, v10, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/WidgetControl;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/client/WidgetControl;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->w:Landroid/util/SparseArray;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/util/ao;

    iget-object v3, v10, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    iget-wide v4, v10, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/client/WidgetControl;-><init>(Landroid/content/Context;Lcom/twitter/library/util/ao;Ljava/lang/String;J)V

    iget-object v1, p0, Lcom/twitter/android/client/c;->y:Ljava/util/HashMap;

    iget-wide v2, v10, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/twitter/android/client/WidgetControl;->b()V

    :cond_0
    monitor-exit v11

    :cond_1
    :goto_1
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto :goto_1

    :cond_2
    iput-boolean v12, p0, Lcom/twitter/android/client/c;->O:Z

    :cond_3
    return-void
.end method

.method public d(Lcom/twitter/library/api/TwitterUser;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/c;->c(Lcom/twitter/library/api/TwitterUser;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/client/c;->P()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/c;->G:Lcom/twitter/android/util/d;

    invoke-interface {v0}, Lcom/twitter/android/util/d;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/c;->G:Lcom/twitter/android/util/d;

    invoke-interface {v0}, Lcom/twitter/android/util/d;->f()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e(J)Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/u;

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "owner_id"

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "list_id"

    invoke-virtual {v0, v1, p1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e(Lcom/twitter/library/client/Session;)Ljava/lang/String;
    .locals 2

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x76

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    const/16 v1, 0x46

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "email"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e(I)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/c;->D:Lcom/twitter/library/client/w;

    new-instance v2, Lcom/twitter/library/api/q;

    iget-object v3, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lcom/twitter/library/api/q;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/twitter/library/api/q;->c(I)Lcom/twitter/library/service/b;

    move-result-object v2

    const-string/jumbo v3, "user_id"

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v2, "prompt_id"

    invoke-virtual {v0, v2, p1}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    return-void
.end method

.method public e(Landroid/content/Context;)V
    .locals 2

    const-string/jumbo v0, "search"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    new-instance v1, Lcom/twitter/android/client/e;

    invoke-direct {v1, p0}, Lcom/twitter/android/client/e;-><init>(Lcom/twitter/android/client/c;)V

    invoke-virtual {v0, v1}, Landroid/app/SearchManager;->setOnCancelListener(Landroid/app/SearchManager$OnCancelListener;)V

    new-instance v1, Lcom/twitter/android/client/f;

    invoke-direct {v1, p0}, Lcom/twitter/android/client/f;-><init>(Lcom/twitter/android/client/c;)V

    invoke-virtual {v0, v1}, Landroid/app/SearchManager;->setOnDismissListener(Landroid/app/SearchManager$OnDismissListener;)V

    return-void
.end method

.method public e(Lcom/twitter/library/api/TwitterUser;)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const-class v2, Lcom/twitter/android/DialogActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "blocked_suspended"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "username"

    iget-object v2, p1, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/client/c;->k:Z

    return v0
.end method

.method public f(Landroid/content/Context;)Lcom/twitter/library/util/aa;
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/c;->J:Ljava/util/WeakHashMap;

    const-string/jumbo v1, "thumbs"

    invoke-virtual {v0, v1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/aa;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/library/util/aa;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c008d    # com.twitter.android.R.dimen.media_thumbnail_size

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const/high16 v2, 0x200000

    invoke-direct {v0, p1, v1, v2}, Lcom/twitter/library/util/aa;-><init>(Landroid/content/Context;II)V

    iget-object v1, p0, Lcom/twitter/android/client/c;->J:Ljava/util/WeakHashMap;

    const-string/jumbo v2, "thumbs"

    invoke-virtual {v1, v2, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method public f(J)Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/u;

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v2, 0x28

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "owner_id"

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "search_id"

    invoke-virtual {v0, v1, p1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method f(Lcom/twitter/library/client/Session;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/c;->K:Lcom/twitter/android/client/aq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/c;->K:Lcom/twitter/android/client/aq;

    invoke-virtual {v0, p1}, Lcom/twitter/android/client/aq;->a(Lcom/twitter/library/client/Session;)V

    :cond_0
    return-void
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/client/c;->m:Z

    return v0
.end method

.method public f(Ljava/lang/String;)Z
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/client/c;->U()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "https"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    const v2, 0x7f0f00f0    # com.twitter.android.R.string.default_promotion_url

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public g(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    const/4 v1, 0x1

    invoke-interface {v0, v1, p1}, Lcom/twitter/library/widget/ap;->a(ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public g()Lcom/twitter/library/api/b;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/c;->S:Lcom/twitter/library/api/b;

    return-object v0
.end method

.method public g(J)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/client/c;->a(JLcom/twitter/library/client/Session;)Lcom/twitter/library/client/u;

    move-result-object v0

    const/16 v1, 0x35

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    const/4 v1, 0x2

    invoke-interface {v0, v1, p1}, Lcom/twitter/library/widget/ap;->a(ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/c;->R:Ljava/lang/String;

    return-object v0
.end method

.method public h(J)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/client/c;->a(JLcom/twitter/library/client/Session;)Lcom/twitter/library/client/u;

    move-result-object v0

    const/16 v1, 0x33

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public i(J)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/c;->c(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    const/16 v1, 0x48

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "status_id"

    invoke-virtual {v0, v1, p1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public i(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "message_id"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public i()Ljava/util/ArrayList;
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/client/c;->an()Lcom/twitter/library/api/UrlConfiguration;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/api/UrlConfiguration;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 9

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-wide v3, v2, Lcom/twitter/library/api/TwitterUser;->userId:J

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-gtz v0, :cond_1

    iget-object v0, v2, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/client/c;->B:Ljava/util/HashMap;

    iget-wide v3, v2, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    sub-long v5, v3, v5

    const-wide/32 v7, 0xea60

    cmp-long v0, v5, v7

    if-lez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/client/c;->B:Ljava/util/HashMap;

    iget-wide v5, v2, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v2, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    iget-wide v1, v2, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/client/c;->a(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public j(J)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x50

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1, p1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/u;

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "owner_id"

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public k(J)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x51

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1, p1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "dm_sync_enabled"

    invoke-virtual {p0}, Lcom/twitter/android/client/c;->I()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public l(J)Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/client/c;->C:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/c;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/library/util/c;

    const-wide/32 v1, 0x36ee80

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/util/c;-><init>(J)V

    iget-object v1, p0, Lcom/twitter/android/client/c;->C:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {v0}, Lcom/twitter/library/util/c;->b()V

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/client/w;->a(Landroid/content/Context;)Lcom/twitter/library/client/w;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/api/i;

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v3, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/api/i;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    new-instance v2, Lcom/twitter/android/client/l;

    invoke-direct {v2, p0, p1, p2}, Lcom/twitter/android/client/l;-><init>(Lcom/twitter/android/client/c;J)V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/w;->a(Lcom/twitter/library/service/b;Lcom/twitter/library/service/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x32

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m(J)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/c;->C:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/c;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/library/util/c;

    const-wide/32 v1, 0x36ee80

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/util/c;-><init>(J)V

    iget-object v1, p0, Lcom/twitter/android/client/c;->C:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {v0}, Lcom/twitter/library/util/c;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/client/c;->l(J)Ljava/lang/String;

    :cond_1
    return-void
.end method

.method public n()Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x3e7

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public n(J)V
    .locals 2

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/client/c;->ao()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/c;->z:Landroid/os/Handler;

    new-instance v1, Lcom/twitter/android/client/m;

    invoke-direct {v1, p0}, Lcom/twitter/android/client/m;-><init>(Lcom/twitter/android/client/c;)V

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public o()V
    .locals 8

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/client/c;->A:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long v4, v2, v4

    const-wide/32 v6, 0xea60

    cmp-long v0, v4, v6

    if-lez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/c;->A:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    new-instance v2, Lcom/twitter/library/client/u;

    iget-object v3, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v2, v3, v1}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "twitter_access_carrier"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "carrier"

    const-string/jumbo v3, "twitter_access_carrier"

    const-string/jumbo v4, ""

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/twitter/library/client/u;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    :cond_1
    const/16 v0, 0x3b

    invoke-virtual {v2, v0}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    :cond_2
    return-void
.end method

.method public o(J)V
    .locals 1

    const/4 v0, 0x1

    invoke-static {p1, p2, v0}, Lkk;->a(JZ)V

    return-void
.end method

.method public p()V
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x81

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    return-void
.end method

.method public p(J)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "fft"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public declared-synchronized q(J)Lcom/twitter/android/client/WidgetControl;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/client/c;->y:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/WidgetControl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public q()V
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0x3f

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    return-void
.end method

.method public r()I
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "typeahead_users_size"

    const/16 v2, 0x3e8

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public r(J)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/client/u;

    iget-object v1, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v1, "entity_id"

    invoke-virtual {v0, v1, p1, p2}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public s()Z
    .locals 1

    invoke-static {}, Lcom/twitter/library/client/App;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/client/c;->l:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public t()Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/c;->F:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/u;

    iget-object v2, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {p0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    const/16 v2, 0x6f

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "user_id"

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/library/service/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u()J
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "typeahead_users_ttl"

    const-wide/32 v2, 0xa4cb800

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public v()I
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "typeahead_topics_size"

    const/16 v2, 0x7d0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public w()J
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "typeahead_topics_ttl"

    const-wide/32 v2, 0x36ee80

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public x()I
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "typeahead_max_recent"

    const/4 v2, 0x5

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public y()I
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "typeahead_max_topics"

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public z()I
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "typeahead_max_user"

    const/16 v2, 0xa

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method
