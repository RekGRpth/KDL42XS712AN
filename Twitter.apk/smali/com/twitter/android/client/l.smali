.class Lcom/twitter/android/client/l;
.super Lcom/twitter/library/service/a;
.source "Twttr"


# instance fields
.field final synthetic a:J

.field final synthetic b:Lcom/twitter/android/client/c;


# direct methods
.method constructor <init>(Lcom/twitter/android/client/c;J)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/client/l;->b:Lcom/twitter/android/client/c;

    iput-wide p2, p0, Lcom/twitter/android/client/l;->a:J

    invoke-direct {p0}, Lcom/twitter/library/service/a;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/internal/android/service/a;)V
    .locals 0

    check-cast p1, Lcom/twitter/library/service/b;

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/l;->a(Lcom/twitter/library/service/b;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/b;)V
    .locals 10

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/twitter/library/service/e;

    invoke-virtual {v7}, Lcom/twitter/library/service/e;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/l;->b:Lcom/twitter/android/client/c;

    iget-wide v2, p0, Lcom/twitter/android/client/l;->a:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/client/c;->o(J)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/l;->b:Lcom/twitter/android/client/c;

    invoke-static {v0}, Lcom/twitter/android/client/c;->e(Lcom/twitter/android/client/c;)Ljava/util/HashMap;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/client/l;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/c;

    invoke-virtual {v0, v1}, Lcom/twitter/library/util/c;->a(Z)V

    iget-object v0, p0, Lcom/twitter/android/client/l;->b:Lcom/twitter/android/client/c;

    invoke-static {v0}, Lcom/twitter/android/client/c;->f(Lcom/twitter/android/client/c;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/l;->b:Lcom/twitter/android/client/c;

    invoke-static {v1}, Lcom/twitter/android/client/c;->d(Lcom/twitter/android/client/c;)Lcom/twitter/library/client/aa;

    move-result-object v1

    iget-object v0, v0, Lcom/twitter/library/service/p;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/aa;->c(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v1

    if-nez v1, :cond_2

    :cond_1
    return-void

    :cond_2
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v8, v0

    :goto_0
    if-ltz v8, :cond_1

    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/j;

    iget-wide v2, p0, Lcom/twitter/android/client/l;->a:J

    iget-object v4, p1, Lcom/twitter/library/service/b;->a:Ljava/lang/String;

    invoke-virtual {v7}, Lcom/twitter/library/service/e;->c()I

    move-result v5

    invoke-virtual {v7}, Lcom/twitter/library/service/e;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/library/client/j;->a(Lcom/twitter/library/client/Session;JLjava/lang/String;ILjava/lang/String;)V

    add-int/lit8 v0, v8, -0x1

    move v8, v0

    goto :goto_0
.end method
