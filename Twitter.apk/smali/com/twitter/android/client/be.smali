.class public abstract Lcom/twitter/android/client/be;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Landroid/app/Activity;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;
    .locals 6

    const/4 v5, 0x0

    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0f0100    # com.twitter.android.R.string.dialog_data_charges

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f02d5    # com.twitter.android.R.string.ok

    invoke-virtual {v1, v2, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f0089    # com.twitter.android.R.string.cancel

    invoke-virtual {v1, v2, p1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030049    # com.twitter.android.R.layout.data_charges_dialog

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f09011a    # com.twitter.android.R.id.carrier_text

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    return-object v1
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/android/client/ak;)V
    .locals 2

    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/client/bf;

    invoke-direct {v1, p1, v0}, Lcom/twitter/android/client/bf;-><init>(Lcom/twitter/android/client/ak;Lcom/twitter/android/client/c;)V

    check-cast p0, Landroid/app/Activity;

    invoke-static {p0, v1}, Lcom/twitter/android/client/be;->a(Landroid/app/Activity;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/UrlEntity;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)V
    .locals 7

    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v6

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->b:Lcom/twitter/library/api/PromotedEvent;

    iget-object v1, p1, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    iget-object v2, p2, Lcom/twitter/library/api/UrlEntity;->url:Ljava/lang/String;

    invoke-virtual {v6, v0, v1, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;Ljava/lang/String;)V

    :cond_0
    instance-of v0, p2, Lcom/twitter/library/api/MediaEntity;

    if-eqz v0, :cond_3

    if-eqz p1, :cond_3

    new-instance v1, Lcom/twitter/android/client/bi;

    move-object v0, p2

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    invoke-direct {v1, p0, p1, p7, v0}, Lcom/twitter/android/client/bi;-><init>(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Lcom/twitter/library/api/MediaEntity;)V

    invoke-virtual {v6}, Lcom/twitter/android/client/c;->ah()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0, v1}, Lcom/twitter/android/client/be;->a(Landroid/content/Context;Lcom/twitter/android/client/ak;)V

    :goto_0
    if-eqz p5, :cond_1

    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v0, p3, p4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p5, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p6, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->c([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, p7, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, p7}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/library/api/UrlEntity;->expandedUrl:Ljava/lang/String;

    iget-object v2, p2, Lcom/twitter/library/api/UrlEntity;->url:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, p8}, Lcom/twitter/library/scribe/ScribeLog;->f(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_1
    return-void

    :cond_2
    invoke-virtual {v1}, Lcom/twitter/android/client/bi;->a()V

    goto :goto_0

    :cond_3
    iget-object v0, p2, Lcom/twitter/library/api/UrlEntity;->url:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/library/util/Util;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6}, Lcom/twitter/android/client/c;->aj()Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v5, Lcom/twitter/android/client/bh;

    invoke-direct {v5, p0, v1}, Lcom/twitter/android/client/bh;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    :goto_1
    iget-object v2, p2, Lcom/twitter/library/api/UrlEntity;->expandedUrl:Ljava/lang/String;

    move-object v0, p0

    move-wide v3, p3

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/client/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLcom/twitter/android/client/ak;)V

    goto :goto_0

    :cond_4
    new-instance v5, Lcom/twitter/android/client/bg;

    invoke-direct {v5, p0, v1}, Lcom/twitter/android/client/bg;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V
    .locals 7

    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v6

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->k:Lcom/twitter/library/api/PromotedEvent;

    iget-object v1, p1, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v6, v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V

    :cond_0
    invoke-static {p2}, Lcom/twitter/library/util/Util;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-instance v5, Lcom/twitter/android/client/bj;

    invoke-direct {v5, p0, v1}, Lcom/twitter/android/client/bj;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    move-object v0, p0

    move-wide v3, p3

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/client/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLcom/twitter/android/client/ak;)V

    if-eqz p5, :cond_1

    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v0, p3, p4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p5, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p6, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->c([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, p7, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, p7}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/library/scribe/ScribeLog;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_1
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;J)V
    .locals 6

    invoke-static {p1}, Lcom/twitter/library/util/Util;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-instance v5, Lcom/twitter/android/client/bj;

    invoke-direct {v5, p0, v1}, Lcom/twitter/android/client/bj;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    move-object v0, p0

    move-wide v3, p2

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/client/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLcom/twitter/android/client/ak;)V

    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLcom/twitter/android/client/ak;)V
    .locals 4

    if-nez p2, :cond_0

    move-object p2, p1

    :cond_0
    sget-object v0, Lcom/twitter/library/util/x;->d:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/TweetActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v2, Lcom/twitter/library/provider/at;->c:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const/16 v3, 0x2f

    invoke-virtual {v0, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v2, "ownerId"

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/twitter/library/util/x;->e:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lgm;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string/jumbo v1, "twitter"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "mentions"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/MainActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    instance-of v1, p0, Lcom/twitter/android/client/BaseFragmentActivity;

    if-eqz v1, :cond_2

    check-cast p0, Lcom/twitter/android/client/BaseFragmentActivity;

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/BaseFragmentActivity;->d(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/NotificationActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_4
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-static {v0}, Lcom/twitter/android/UrlInterpreterActivity;->b(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_5

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/UrlInterpreterActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_5
    invoke-virtual {v1}, Lcom/twitter/android/client/c;->ah()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {p1}, Lcom/twitter/library/util/Util;->j(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {p0, p5}, Lcom/twitter/android/client/be;->a(Landroid/content/Context;Lcom/twitter/android/client/ak;)V

    goto :goto_0

    :cond_6
    invoke-interface {p5}, Lcom/twitter/android/client/ak;->a()V

    goto :goto_0
.end method
