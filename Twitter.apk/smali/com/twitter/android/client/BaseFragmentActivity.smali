.class public Lcom/twitter/android/client/BaseFragmentActivity;
.super Lcom/twitter/library/client/AbsFragmentActivity;
.source "Twttr"


# instance fields
.field protected D:Lcom/twitter/library/platform/LocationProducer;

.field private final a:Lcom/twitter/android/client/b;

.field private final b:Ljava/util/ArrayList;

.field private c:Lcom/twitter/android/client/c;

.field private d:Lcom/twitter/android/client/bn;

.field private e:Lcom/twitter/android/client/z;

.field private f:Landroid/view/View;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/view/View;

.field private i:Landroid/widget/ImageView;

.field private j:Landroid/view/View;

.field private k:Z

.field private l:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/library/client/AbsFragmentActivity;-><init>()V

    new-instance v0, Lcom/twitter/android/client/b;

    invoke-direct {v0}, Lcom/twitter/android/client/b;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->a:Lcom/twitter/android/client/b;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->b:Ljava/util/ArrayList;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->l:Ljava/lang/CharSequence;

    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 2

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string/jumbo v0, "sb_notification"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/notifications/StatusBarNotif;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/twitter/android/client/NotificationService;->a(Landroid/content/Context;Landroid/os/Bundle;)V

    :cond_0
    const-string/jumbo v0, "sb_account_name"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    const-string/jumbo v0, "sb_notification"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    const-string/jumbo v0, "notif_scribe_log"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    const-string/jumbo v0, "notif_scribe_log_for_preview_experiment"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/client/BaseFragmentActivity;Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/client/BaseFragmentActivity;Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;Landroid/net/Uri;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/client/BaseFragmentActivity;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->b(Z)V

    return-void
.end method

.method private a(Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;Landroid/net/Uri;)V

    return-void
.end method

.method private a(Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;Landroid/net/Uri;)V
    .locals 6

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, ":composition:image_attachment::done"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_0
    invoke-static {p0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Landroid/content/Context;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/android/composer/ComposerIntentWrapper;->b(Landroid/net/Uri;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->c_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->k_()[I

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Ljava/lang/String;[I)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->b(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/client/BaseFragmentActivity;)Z
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->g()Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/client/BaseFragmentActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private b(Z)V
    .locals 3

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/twitter/android/client/BaseFragmentActivity;->h:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/twitter/android/client/BaseFragmentActivity;->j:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->i:Landroid/widget/ImageView;

    const v1, 0x7f020133    # com.twitter.android.R.drawable.ic_bottom_bar_gallery_on

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_1
    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    :cond_1
    iput-boolean v1, p0, Lcom/twitter/android/client/BaseFragmentActivity;->k:Z

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->i:Landroid/widget/ImageView;

    const v1, 0x7f020132    # com.twitter.android.R.drawable.ic_bottom_bar_gallery_off

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method

.method static synthetic c(Lcom/twitter/android/client/BaseFragmentActivity;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->c:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/client/BaseFragmentActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->k:Z

    return v0
.end method

.method static synthetic e(Lcom/twitter/android/client/BaseFragmentActivity;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->f:Landroid/view/View;

    return-object v0
.end method

.method private f()V
    .locals 7

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->g()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p0}, Lgp;->e(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-direct {p0, v0}, Lcom/twitter/android/client/BaseFragmentActivity;->b(Z)V

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    new-array v5, v1, [Ljava/lang/String;

    const-string/jumbo v6, "::compose_bar:gallery_bar:show"

    aput-object v6, v5, v2

    invoke-virtual {v0, v3, v4, v5}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iput-boolean v1, p0, Lcom/twitter/android/client/BaseFragmentActivity;->k:Z

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method private g()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()V
    .locals 5

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    const v2, 0x7f0f0082    # com.twitter.android.R.string.button_toolbar_title_description_format

    invoke-virtual {p0, v2}, Lcom/twitter/android/client/BaseFragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    if-eqz v0, :cond_1

    :goto_0
    aput-object v0, v3, v4

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->l:Ljava/lang/CharSequence;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->l:Ljava/lang/CharSequence;

    :goto_1
    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setTitleDescription(Ljava/lang/CharSequence;)V

    :cond_0
    return-void

    :cond_1
    const-string/jumbo v0, ""

    goto :goto_0

    :cond_2
    const-string/jumbo v0, ""

    goto :goto_1
.end method


# virtual methods
.method public final C()Landroid/content/Intent;
    .locals 6

    const/4 v2, 0x1

    const/4 v5, 0x0

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.SEND_MULTIPLE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "text/xml"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {}, Lcom/twitter/library/client/App;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "android.intent.extra.EMAIL"

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f004a    # com.twitter.android.R.string.beta_adopters_bug_report_email

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    :goto_0
    const-string/jumbo v1, "android.intent.extra.SUBJECT"

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->D()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.extra.TEXT"

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->E()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/BaseFragmentActivity;->b(Landroid/content/Intent;)Landroid/content/Intent;

    return-object v0

    :cond_0
    const-string/jumbo v1, "android.intent.extra.EMAIL"

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0123    # com.twitter.android.R.string.dogfooders_bug_report_email

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public final D()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Reporting bug in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " with v"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0}, Lcom/twitter/library/util/Util;->m(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final E()Ljava/lang/String;
    .locals 8

    const/4 v7, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v0, "Thanks for filing a bug!\n\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "Summary: \n\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "Steps to reproduce: \n\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "Expected results: \n\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "Actual results: \n\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const-string/jumbo v0, "\n\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const-string/jumbo v0, "\n\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "package: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "\nversion: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lcom/twitter/library/util/Util;->m(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-static {p0}, Lcom/twitter/library/telephony/TelephonyUtil;->c(Landroid/content/Context;)Z

    move-result v0

    const-string/jumbo v2, "\nconnectivity: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v0, :cond_1

    const-string/jumbo v0, "\nconnectivityType: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/twitter/library/telephony/TelephonyUtil;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    const-string/jumbo v3, "\nuserId: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v0, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string/jumbo v3, "\nusername: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v3, "\nprotected: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, v0, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string/jumbo v3, "\nsuspended: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v0, v0, Lcom/twitter/library/api/TwitterUser;->suspended:Z

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    :cond_2
    invoke-static {p0}, Lcom/twitter/library/network/aa;->a(Landroid/content/Context;)Lcom/twitter/library/network/aa;

    move-result-object v0

    const-string/jumbo v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/twitter/library/network/aa;->e:Lcom/twitter/library/network/ac;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/twitter/library/network/aa;->b()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Lcom/twitter/library/network/aa;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    if-lez v3, :cond_3

    const-string/jumbo v4, "\nRecent traces (only work if requests sent from Dodo):"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0, v7, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    const-string/jumbo v4, "http://go/zipkin/%1$s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/net/URI;

    invoke-virtual {v0}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v4, ")"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_3
    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string/jumbo v0, "\nexperiments: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {v2, v3}, Lju;->d(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected F()Ljava/util/ArrayList;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final G()V
    .locals 3

    invoke-static {p0}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Ljava/io/File;

    const-string/jumbo v2, "bug_reports"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :try_start_0
    invoke-static {v1}, Llj;->a(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->H()V

    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected H()V
    .locals 0

    return-void
.end method

.method protected final I()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->d:Lcom/twitter/android/client/bn;

    invoke-virtual {v0}, Lcom/twitter/android/client/bn;->c()Z

    move-result v0

    return v0
.end method

.method protected final J()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->d:Lcom/twitter/android/client/bn;

    invoke-virtual {v0}, Lcom/twitter/android/client/bn;->d()Z

    move-result v0

    return v0
.end method

.method protected K()Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->c:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method protected L()Lcom/twitter/android/client/bn;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->d:Lcom/twitter/android/client/bn;

    return-object v0
.end method

.method protected M()Lcom/twitter/android/client/z;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->e:Lcom/twitter/android/client/z;

    return-object v0
.end method

.method protected N()Lcom/twitter/library/platform/LocationProducer;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->D:Lcom/twitter/library/platform/LocationProducer;

    return-object v0
.end method

.method protected O()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/android/StartActivity;->a(Landroid/app/Activity;Landroid/content/Intent;)V

    return-void
.end method

.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 0

    return-void
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/library/client/e;)V
    .locals 9

    const v8, 0x7f0900ea    # com.twitter.android.R.id.gallery_bar_container

    const v7, 0x7f0900e6    # com.twitter.android.R.id.gallery

    const/4 v6, 0x3

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->e:Lcom/twitter/android/client/z;

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V

    invoke-static {}, Lgp;->a()Z

    move-result v2

    const v0, 0x7f0900e4    # com.twitter.android.R.id.compose_stub

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/BaseFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    const v1, 0x7f0900e1    # com.twitter.android.R.id.dock

    invoke-virtual {p0, v1}, Lcom/twitter/android/client/BaseFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/widget/ComposerBarLayout;

    if-eqz v0, :cond_5

    if-eqz v2, :cond_0

    const v3, 0x7f030035    # com.twitter.android.R.layout.compose_bar_with_gallery

    invoke-virtual {v0, v3}, Landroid/view/ViewStub;->setLayoutResource(I)V

    invoke-virtual {v0}, Landroid/view/ViewStub;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    const/4 v4, -0x2

    iput v4, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    :cond_0
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1}, Lcom/twitter/library/widget/ComposerBarLayout;->d()V

    move-object v3, v0

    :goto_0
    if-eqz v3, :cond_4

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->e:Lcom/twitter/android/client/z;

    iget v0, v0, Lcom/twitter/android/client/z;->f:I

    const/4 v4, 0x2

    if-eq v0, v4, :cond_1

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->e:Lcom/twitter/android/client/z;

    iget v0, v0, Lcom/twitter/android/client/z;->f:I

    if-ne v0, v6, :cond_7

    :cond_1
    const v0, 0x7f0900e5    # com.twitter.android.R.id.tweet

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v4, 0x7f0f00c0    # com.twitter.android.R.string.composer_hint

    invoke-virtual {p0, v4}, Lcom/twitter/android/client/BaseFragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    new-instance v4, Lcom/twitter/android/client/r;

    invoke-direct {v4, p0}, Lcom/twitter/android/client/r;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {p0}, Lcom/twitter/internal/android/widget/ax;->a(Landroid/content/Context;)Lcom/twitter/internal/android/widget/ax;

    move-result-object v4

    iget-object v4, v4, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iput-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->g:Landroid/widget/TextView;

    invoke-virtual {p0, v8}, Lcom/twitter/android/client/BaseFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/client/BaseFragmentActivity;->h:Landroid/view/View;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/twitter/android/client/BaseFragmentActivity;->h:Landroid/view/View;

    if-eqz v2, :cond_6

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string/jumbo v2, "gallery_bar"

    invoke-virtual {v4, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/widget/GalleryBarFragment;

    if-nez v2, :cond_2

    new-instance v2, Lcom/twitter/android/widget/GalleryBarFragment;

    invoke-direct {v2}, Lcom/twitter/android/widget/GalleryBarFragment;-><init>()V

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    const-string/jumbo v5, "gallery_bar"

    invoke-virtual {v4, v8, v2, v5}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_2
    new-instance v4, Lcom/twitter/android/client/s;

    invoke-direct {v4, p0}, Lcom/twitter/android/client/s;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0900e8    # com.twitter.android.R.id.gallery_button

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v4, Lcom/twitter/android/client/t;

    invoke-direct {v4, p0}, Lcom/twitter/android/client/t;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->i:Landroid/widget/ImageView;

    const v0, 0x7f0900e9    # com.twitter.android.R.id.gallery_bar_divot

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->j:Landroid/view/View;

    new-instance v0, Lcom/twitter/android/client/u;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/u;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    invoke-virtual {v2, v0}, Lcom/twitter/android/widget/GalleryBarFragment;->a(Lcom/twitter/android/widget/z;)V

    new-instance v0, Lcom/twitter/android/client/v;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/v;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    invoke-virtual {v1, v0}, Lcom/twitter/library/widget/ComposerBarLayout;->setDockListener(Lcom/twitter/internal/android/widget/h;)V

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->e:Lcom/twitter/android/client/z;

    iget v0, v0, Lcom/twitter/android/client/z;->f:I

    if-ne v0, v6, :cond_3

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/twitter/library/widget/ComposerBarLayout;->setBottomLocked(Z)V

    :cond_3
    iput-object v3, p0, Lcom/twitter/android/client/BaseFragmentActivity;->f:Landroid/view/View;

    :cond_4
    :goto_2
    return-void

    :cond_5
    const v0, 0x7f090035    # com.twitter.android.R.id.compose

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/BaseFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    goto/16 :goto_0

    :cond_6
    const v0, 0x7f0900e7    # com.twitter.android.R.id.camera

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v2, Lcom/twitter/android/client/w;

    invoke-direct {v2, p0}, Lcom/twitter/android/client/w;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v2, Lcom/twitter/android/client/x;

    invoke-direct {v2, p0}, Lcom/twitter/android/client/x;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    :cond_7
    const/16 v0, 0x8

    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method public a(Lcom/twitter/android/client/a;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->a:Lcom/twitter/android/client/b;

    invoke-virtual {v0, p1}, Lcom/twitter/android/client/b;->a(Lcom/twitter/android/client/a;)V

    return-void
.end method

.method protected a(Lcom/twitter/library/client/j;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/ToolBar;->setSubtitle(Ljava/lang/CharSequence;)V

    iput-object p1, p0, Lcom/twitter/android/client/BaseFragmentActivity;->l:Ljava/lang/CharSequence;

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->h()V

    return-void
.end method

.method protected a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->e:Lcom/twitter/android/client/z;

    iget-boolean v0, v0, Lcom/twitter/android/client/z;->d:Z

    if-eqz v0, :cond_2

    const v0, 0x7f110007    # com.twitter.android.R.menu.default_toolbar

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->d:Lcom/twitter/android/client/bn;

    invoke-virtual {v0, p2}, Lcom/twitter/android/client/bn;->a(Lcom/twitter/internal/android/widget/ToolBar;)V

    invoke-static {}, Lgu;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->d:Lcom/twitter/android/client/bn;

    iget-object v1, p0, Lcom/twitter/android/client/BaseFragmentActivity;->e:Lcom/twitter/android/client/z;

    invoke-virtual {v1}, Lcom/twitter/android/client/z;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/bn;->a(I)V

    :cond_0
    const v0, 0x7f090308    # com.twitter.android.R.id.toolbar_compose

    invoke-virtual {p2, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lhn;->d()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    const v2, 0x7f0f0071    # com.twitter.android.R.string.button_new_tweet

    invoke-virtual {p0, v2}, Lcom/twitter/android/client/BaseFragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/client/BaseFragmentActivity;->e:Lcom/twitter/android/client/z;

    iget v1, v1, Lcom/twitter/android/client/z;->f:I

    if-eq v1, v3, :cond_2

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhn;->b(Z)Lhn;

    :cond_2
    invoke-static {}, Lcom/twitter/library/client/App;->c()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    invoke-static {}, Lcom/twitter/library/client/App;->d()Z

    move-result v0

    if-eqz v0, :cond_6

    const v0, 0x7f11000f    # com.twitter.android.R.menu.give_feedback_menu

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->e:Lcom/twitter/android/client/z;

    iget-boolean v0, v0, Lcom/twitter/android/client/z;->e:Z

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    const v0, 0x7f11001f    # com.twitter.android.R.menu.settings_toolbar

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    :cond_5
    return v3

    :cond_6
    const v0, 0x7f110002    # com.twitter.android.R.menu.bug_report_menu

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    goto :goto_0
.end method

.method public a(Lhn;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p1}, Lhn;->a()I

    move-result v1

    const v2, 0x7f09006b    # com.twitter.android.R.id.settings

    if-ne v1, v2, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/SettingsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/twitter/android/client/BaseFragmentActivity;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return v0

    :cond_0
    const v2, 0x7f090309    # com.twitter.android.R.id.toolbar_search

    if-ne v1, v2, :cond_1

    invoke-static {}, Lgu;->b()V

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onSearchRequested()Z

    goto :goto_0

    :cond_1
    const v2, 0x7f090302    # com.twitter.android.R.id.report_bug

    if-ne v1, v2, :cond_3

    invoke-static {}, Lcom/twitter/library/client/App;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x7f0f0024    # com.twitter.android.R.string.alpha_build_forum

    invoke-virtual {p0, v1}, Lcom/twitter/android/client/BaseFragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->C()Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/client/BaseFragmentActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_3
    const v2, 0x7f090308    # com.twitter.android.R.id.toolbar_compose

    if-ne v1, v2, :cond_4

    sget-object v1, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;->a:Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;

    invoke-direct {p0, v1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;)V

    goto :goto_0

    :cond_4
    invoke-super {p0, p1}, Lcom/twitter/library/client/AbsFragmentActivity;->a(Lhn;)Z

    move-result v0

    goto :goto_0
.end method

.method protected a_(Landroid/net/Uri;)V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->aa()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p0, v0}, Lcom/twitter/android/client/BaseFragmentActivity;->b(Landroid/app/Activity;Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->getParent()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    :goto_1
    invoke-static {p0, p1}, Lcom/twitter/android/MainActivity;->a(Landroid/app/Activity;Landroid/net/Uri;)V

    goto :goto_0

    :cond_1
    move-object p0, v0

    goto :goto_1
.end method

.method public final b(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 6

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->G()V

    const/4 v0, 0x0

    invoke-static {p0}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v0, Ljava/io/File;

    const-string/jumbo v3, "bug_reports"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ".jpg"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x5a

    invoke-static {v0, v2, v3, v4}, Lcom/twitter/library/util/Util;->a(Landroid/view/View;Ljava/io/File;Landroid/graphics/Bitmap$CompressFormat;I)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->F()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "android.intent.extra.STREAM"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    return-object p1
.end method

.method public final b(Landroid/os/Bundle;)Lcom/twitter/library/client/e;
    .locals 2

    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->c:Lcom/twitter/android/client/c;

    invoke-static {p0}, Lcom/twitter/library/util/Util;->a(Landroid/app/Activity;)V

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->c:Lcom/twitter/android/client/c;

    invoke-virtual {v0, p0}, Lcom/twitter/android/client/c;->e(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/twitter/library/platform/LocationProducer;->a(Landroid/content/Context;)Lcom/twitter/library/platform/LocationProducer;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->D:Lcom/twitter/library/platform/LocationProducer;

    new-instance v0, Lcom/twitter/android/js;

    invoke-direct {v0, p0}, Lcom/twitter/android/js;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lcom/twitter/library/client/j;)V

    new-instance v0, Lcom/twitter/android/client/bn;

    const/high16 v1, -0x80000000

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/client/bn;-><init>(Landroid/support/v4/app/FragmentActivity;I)V

    iput-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->d:Lcom/twitter/android/client/bn;

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->d:Lcom/twitter/android/client/bn;

    invoke-virtual {v0, p1}, Lcom/twitter/android/client/bn;->a(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->d:Lcom/twitter/android/client/bn;

    new-instance v1, Lcom/twitter/android/client/y;

    invoke-direct {v1, p0}, Lcom/twitter/android/client/y;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/bn;->a(Lcom/twitter/android/client/bw;)V

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    iput-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->e:Lcom/twitter/android/client/z;

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->e:Lcom/twitter/android/client/z;

    return-object v0

    :cond_0
    iput-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->e:Lcom/twitter/android/client/z;

    goto :goto_0
.end method

.method public b(Lcom/twitter/android/client/a;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->a:Lcom/twitter/android/client/b;

    invoke-virtual {v0, p1}, Lcom/twitter/android/client/b;->b(Lcom/twitter/android/client/a;)V

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->g:Landroid/widget/TextView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected c_()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public e(I)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected h_()V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/twitter/android/client/BaseListFragment;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/twitter/android/client/BaseListFragment;

    invoke-virtual {v0}, Lcom/twitter/android/client/BaseListFragment;->aa()V

    :cond_0
    return-void
.end method

.method protected i()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, ""

    return-object v0
.end method

.method protected j()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/BaseFragmentActivity;->a_(Landroid/net/Uri;)V

    return-void
.end method

.method protected k_()[I
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected m_()V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/twitter/android/client/BaseListFragment;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/twitter/android/client/BaseListFragment;

    invoke-virtual {v0}, Lcom/twitter/android/client/BaseListFragment;->Z()V

    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->a:Lcom/twitter/android/client/b;

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/android/client/b;->a(IILandroid/content/Intent;)V

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/library/client/AbsFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->d:Lcom/twitter/android/client/bn;

    invoke-virtual {v0}, Lcom/twitter/android/client/bn;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/twitter/library/client/AbsFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->a:Lcom/twitter/android/client/b;

    invoke-virtual {v0}, Lcom/twitter/android/client/b;->c()V

    invoke-super {p0}, Lcom/twitter/library/client/AbsFragmentActivity;->onDestroy()V

    invoke-static {}, Lcom/twitter/library/client/App;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/twitter/library/client/App;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->G()V

    :cond_1
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->U()Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v1, 0x52

    if-ne p1, v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/client/BaseFragmentActivity;->d:Lcom/twitter/android/client/bn;

    invoke-virtual {v1}, Lcom/twitter/android/client/bn;->b()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->T()Z

    move-result v1

    :goto_0
    if-nez v1, :cond_0

    invoke-super {p0, p1, p2}, Lcom/twitter/library/client/AbsFragmentActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/twitter/library/client/AbsFragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    invoke-direct {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Landroid/content/Intent;)V

    return-void
.end method

.method protected onPause()V
    .locals 3

    invoke-super {p0}, Lcom/twitter/library/client/AbsFragmentActivity;->onPause()V

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->I()Z

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/j;

    iget-object v2, p0, Lcom/twitter/android/client/BaseFragmentActivity;->c:Lcom/twitter/android/client/c;

    invoke-virtual {v2, v0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/j;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->D:Lcom/twitter/library/platform/LocationProducer;

    invoke-virtual {v0}, Lcom/twitter/library/platform/LocationProducer;->f()V

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->h_()V

    return-void
.end method

.method protected onResume()V
    .locals 3

    invoke-super {p0}, Lcom/twitter/library/client/AbsFragmentActivity;->onResume()V

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/j;

    iget-object v2, p0, Lcom/twitter/android/client/BaseFragmentActivity;->c:Lcom/twitter/android/client/c;

    invoke-virtual {v2, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/j;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->D:Lcom/twitter/library/platform/LocationProducer;

    invoke-virtual {v0}, Lcom/twitter/library/platform/LocationProducer;->g()V

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->f()V

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->m_()V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/library/client/AbsFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->a:Lcom/twitter/android/client/b;

    invoke-virtual {v0, p1}, Lcom/twitter/android/client/b;->a(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->d:Lcom/twitter/android/client/bn;

    invoke-virtual {v0, p1}, Lcom/twitter/android/client/bn;->b(Landroid/os/Bundle;)V

    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->d:Lcom/twitter/android/client/bn;

    invoke-virtual {v0}, Lcom/twitter/android/client/bn;->d()Z

    move-result v0

    return v0
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/library/client/AbsFragmentActivity;->onStart()V

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->a:Lcom/twitter/android/client/b;

    invoke-virtual {v0}, Lcom/twitter/android/client/b;->a()V

    return-void
.end method

.method protected onStop()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;->a:Lcom/twitter/android/client/b;

    invoke-virtual {v0}, Lcom/twitter/android/client/b;->b()V

    invoke-super {p0}, Lcom/twitter/library/client/AbsFragmentActivity;->onStop()V

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/twitter/library/client/AbsFragmentActivity;->setTitle(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->h()V

    return-void
.end method
