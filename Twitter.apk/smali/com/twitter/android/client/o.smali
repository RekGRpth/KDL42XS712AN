.class final Lcom/twitter/android/client/o;
.super Lcom/twitter/library/client/z;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/client/c;


# direct methods
.method public constructor <init>(Lcom/twitter/android/client/c;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/client/o;->a:Lcom/twitter/android/client/c;

    invoke-direct {p0}, Lcom/twitter/library/client/z;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/o;->a:Lcom/twitter/android/client/c;

    invoke-virtual {v0, p1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;)V

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Z)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/o;->a:Lcom/twitter/android/client/c;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Z)Z

    return-void
.end method

.method public b(Lcom/twitter/library/client/Session;Z)V
    .locals 1

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/o;->a:Lcom/twitter/android/client/c;

    invoke-virtual {v0, p1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/o;->a:Lcom/twitter/android/client/c;

    iget-object v0, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/platform/TwitterDataSyncService;->a(Landroid/content/Context;)V

    return-void
.end method

.method public c(Lcom/twitter/library/client/Session;Z)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/o;->a:Lcom/twitter/android/client/c;

    iget-object v0, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/library/util/a;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/AccountManagerFuture;

    iget-object v0, p0, Lcom/twitter/android/client/o;->a:Lcom/twitter/android/client/c;

    iget-object v0, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/client/aw;->a(Landroid/content/Context;)Lcom/twitter/android/client/aw;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/client/aw;->a(Lcom/twitter/library/client/Session;)V

    iget-object v0, p0, Lcom/twitter/android/client/o;->a:Lcom/twitter/android/client/c;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Z)Z

    return-void
.end method
