.class public Lcom/twitter/android/client/notifications/MediaTagNotif;
.super Lcom/twitter/android/client/notifications/StatusBarNotif;
.source "Twttr"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/android/client/notifications/m;

    invoke-direct {v0}, Lcom/twitter/android/client/notifications/m;-><init>()V

    sput-object v0, Lcom/twitter/android/client/notifications/MediaTagNotif;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/platform/e;JLjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/client/notifications/StatusBarNotif;-><init>(Lcom/twitter/library/platform/e;JLjava/lang/String;)V

    return-void
.end method


# virtual methods
.method public A_()Lcom/twitter/android/client/notifications/w;
    .locals 5

    new-instance v0, Lcom/twitter/android/client/notifications/i;

    iget-object v1, p0, Lcom/twitter/android/client/notifications/MediaTagNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v2, p0, Lcom/twitter/android/client/notifications/MediaTagNotif;->c:Ljava/lang/String;

    iget-wide v3, p0, Lcom/twitter/android/client/notifications/MediaTagNotif;->b:J

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/android/client/notifications/i;-><init>(Lcom/twitter/library/platform/e;Ljava/lang/String;J)V

    return-object v0
.end method

.method protected c()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/notifications/MediaTagNotif;->d:Landroid/content/Context;

    const v1, 0x7f0f02a4    # com.twitter.android.R.string.notif_media_tag_text

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected d()I
    .locals 1

    const v0, 0x7f02024e    # com.twitter.android.R.drawable.ic_stat_twitter

    return v0
.end method

.method protected e()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "media_tagged"

    return-object v0
.end method

.method protected f()Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/notifications/MediaTagNotif;->d:Landroid/content/Context;

    const v1, 0x7f0f02b7    # com.twitter.android.R.string.notif_single_media_tag_format

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/client/notifications/MediaTagNotif;->a:Lcom/twitter/library/platform/e;

    invoke-virtual {v4}, Lcom/twitter/library/platform/e;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected g()Landroid/content/Intent;
    .locals 5

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/client/notifications/MediaTagNotif;->d:Landroid/content/Context;

    const-class v2, Lcom/twitter/android/TweetActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/twitter/android/client/notifications/MediaTagNotif;->a:Lcom/twitter/library/platform/e;

    iget-wide v1, v1, Lcom/twitter/library/platform/e;->f:J

    iget-wide v3, p0, Lcom/twitter/android/client/notifications/MediaTagNotif;->b:J

    invoke-static {v1, v2, v3, v4}, Lcom/twitter/library/provider/w;->a(JJ)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "com.twitter.android.home.mediatags."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/client/notifications/MediaTagNotif;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected z_()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/notifications/MediaTagNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v0, v0, Lcom/twitter/library/platform/e;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/MediaTagNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v0, v0, Lcom/twitter/library/platform/e;->d:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/notifications/MediaTagNotif;->a:Lcom/twitter/library/platform/e;

    invoke-virtual {v0}, Lcom/twitter/library/platform/e;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
