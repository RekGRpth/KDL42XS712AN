.class public Lcom/twitter/android/client/notifications/MessageNotif;
.super Lcom/twitter/android/client/notifications/StatusBarNotif;
.source "Twttr"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/android/client/notifications/p;

    invoke-direct {v0}, Lcom/twitter/android/client/notifications/p;-><init>()V

    sput-object v0, Lcom/twitter/android/client/notifications/MessageNotif;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/platform/e;JLjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/client/notifications/StatusBarNotif;-><init>(Lcom/twitter/library/platform/e;JLjava/lang/String;)V

    return-void
.end method

.method static a(Landroid/content/Context;Lcom/twitter/library/platform/e;J)Landroid/content/Intent;
    .locals 4

    new-instance v0, Landroid/content/Intent;

    invoke-static {p0}, Lcom/twitter/android/MessagesActivity;->a(Landroid/content/Context;)Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "owner_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    iget-wide v2, p1, Lcom/twitter/library/platform/e;->g:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "user_fullname"

    invoke-virtual {p1}, Lcom/twitter/library/platform/e;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "user_name"

    iget-object v2, p1, Lcom/twitter/library/platform/e;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "com.twitter.android.home.messages."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/twitter/library/platform/e;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public A_()Lcom/twitter/android/client/notifications/w;
    .locals 5

    invoke-static {}, Lgq;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/twitter/android/client/notifications/r;

    iget-object v1, p0, Lcom/twitter/android/client/notifications/MessageNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v2, p0, Lcom/twitter/android/client/notifications/MessageNotif;->c:Ljava/lang/String;

    iget-wide v3, p0, Lcom/twitter/android/client/notifications/MessageNotif;->b:J

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/android/client/notifications/r;-><init>(Lcom/twitter/library/platform/e;Ljava/lang/String;J)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/twitter/android/client/notifications/q;

    iget-object v1, p0, Lcom/twitter/android/client/notifications/MessageNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v2, p0, Lcom/twitter/android/client/notifications/MessageNotif;->c:Ljava/lang/String;

    iget-wide v3, p0, Lcom/twitter/android/client/notifications/MessageNotif;->b:J

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/android/client/notifications/q;-><init>(Lcom/twitter/library/platform/e;Ljava/lang/String;J)V

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/notifications/MessageNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v0, v0, Lcom/twitter/library/platform/e;->e:Ljava/lang/String;

    return-object v0
.end method

.method public d()I
    .locals 1

    const v0, 0x7f020240    # com.twitter.android.R.drawable.ic_stat_dm

    return v0
.end method

.method protected e()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "message"

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/notifications/MessageNotif;->d:Landroid/content/Context;

    const v1, 0x7f0f0284    # com.twitter.android.R.string.message_from

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/client/notifications/MessageNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v4, v4, Lcom/twitter/library/platform/e;->h:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/twitter/android/client/notifications/MessageNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v4, v4, Lcom/twitter/library/platform/e;->e:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected g()Landroid/content/Intent;
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/client/notifications/MessageNotif;->d:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/client/notifications/MessageNotif;->a:Lcom/twitter/library/platform/e;

    iget-wide v2, p0, Lcom/twitter/android/client/notifications/MessageNotif;->b:J

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/client/notifications/MessageNotif;->a(Landroid/content/Context;Lcom/twitter/library/platform/e;J)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public z_()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/notifications/MessageNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v0, v0, Lcom/twitter/library/platform/e;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/MessageNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v0, v0, Lcom/twitter/library/platform/e;->d:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/notifications/MessageNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v0, v0, Lcom/twitter/library/platform/e;->h:Ljava/lang/String;

    goto :goto_0
.end method
