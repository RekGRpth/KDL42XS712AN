.class public abstract Lcom/twitter/android/client/notifications/StatusBarNotif;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# instance fields
.field protected final a:Lcom/twitter/library/platform/e;

.field protected final b:J

.field protected final c:Ljava/lang/String;

.field protected d:Landroid/content/Context;

.field private final e:Lcom/twitter/android/client/notifications/w;

.field private f:Z


# direct methods
.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/twitter/library/platform/DataSyncResult;->a(Landroid/os/Parcel;)Lcom/twitter/library/platform/e;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/e;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->b:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->c:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->A_()Lcom/twitter/android/client/notifications/w;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/w;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/platform/e;JLjava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/e;

    iput-wide p2, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->b:J

    iput-object p4, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->c:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->A_()Lcom/twitter/android/client/notifications/w;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/w;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->f:Z

    return-void
.end method

.method private B()Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/w;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/w;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/w;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/w;->f()Landroid/content/Intent;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->g()Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method private C()Ljava/util/ArrayList;
    .locals 13
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iget-object v8, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/e;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    if-eqz v8, :cond_0

    iget-object v0, v8, Lcom/twitter/library/platform/e;->t:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, v8, Lcom/twitter/library/platform/e;->s:Lcom/twitter/library/platform/notifications/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/w;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/w;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v7

    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->D()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->d:Landroid/content/Context;

    iget-object v0, v8, Lcom/twitter/library/platform/e;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_2
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/platform/notifications/a;

    iget-object v1, v0, Lcom/twitter/library/platform/notifications/a;->e:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    iget v0, v0, Lcom/twitter/library/platform/notifications/a;->d:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    :pswitch_1
    iget-object v0, v8, Lcom/twitter/library/platform/e;->s:Lcom/twitter/library/platform/notifications/g;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/g;->b:Lcom/twitter/library/platform/notifications/f;

    iget-object v1, v8, Lcom/twitter/library/platform/e;->r:Lcom/twitter/library/platform/notifications/e;

    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    iget-object v2, v8, Lcom/twitter/library/platform/e;->s:Lcom/twitter/library/platform/notifications/g;

    iget-object v2, v2, Lcom/twitter/library/platform/notifications/g;->c:Lcom/twitter/library/platform/notifications/f;

    invoke-static {v1, v0, v2}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Lcom/twitter/library/platform/notifications/e;Lcom/twitter/library/platform/notifications/f;Lcom/twitter/library/platform/notifications/f;)Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;

    move-result-object v0

    if-eqz v3, :cond_3

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v10, v0, v1, v2}, Lcom/twitter/library/scribe/ScribeItem;->a(Landroid/content/Context;Lcom/twitter/library/provider/ParcelableTweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_3
    sget-object v1, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->c:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    invoke-static {v10, v1}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Landroid/content/Context;Lcom/twitter/android/composer/ComposerIntentWrapper$Action;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Lcom/twitter/library/provider/ParcelableTweet;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a()Landroid/content/Intent;

    move-result-object v0

    invoke-static {}, Lcom/twitter/android/composer/ComposerIntentWrapper;->p()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {p0, v10, v1, v0, v3}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/content/Context;Ljava/lang/Class;Landroid/content/Intent;Lcom/twitter/library/scribe/ScribeLog;)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v1, Landroid/support/v4/app/NotificationCompat$Action;

    const v2, 0x7f02024a    # com.twitter.android.R.drawable.ic_stat_notify_reply

    const v3, 0x7f0f005d    # com.twitter.android.R.string.button_action_reply

    invoke-virtual {v10, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Landroid/support/v4/app/NotificationCompat$Action;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_2
    iget-object v0, v8, Lcom/twitter/library/platform/e;->s:Lcom/twitter/library/platform/notifications/g;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/g;->b:Lcom/twitter/library/platform/notifications/f;

    iget-object v2, v8, Lcom/twitter/library/platform/e;->r:Lcom/twitter/library/platform/notifications/e;

    if-eqz v2, :cond_2

    if-eqz v0, :cond_2

    iget-boolean v1, v0, Lcom/twitter/library/platform/notifications/f;->f:Z

    if-nez v1, :cond_2

    iget-object v1, v8, Lcom/twitter/library/platform/e;->s:Lcom/twitter/library/platform/notifications/g;

    iget-object v1, v1, Lcom/twitter/library/platform/notifications/g;->c:Lcom/twitter/library/platform/notifications/f;

    invoke-static {v2, v0, v1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Lcom/twitter/library/platform/notifications/e;Lcom/twitter/library/platform/notifications/f;Lcom/twitter/library/platform/notifications/f;)Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;

    move-result-object v12

    if-eqz v9, :cond_4

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {v10, v12, v0, v1}, Lcom/twitter/library/scribe/ScribeItem;->a(Landroid/content/Context;Lcom/twitter/library/provider/ParcelableTweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    const-string/jumbo v1, "retweet"

    invoke-virtual {p0, v1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    const-string/jumbo v1, "quote"

    invoke-virtual {p0, v1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    move-object v6, v0

    :goto_2
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v0, "status_id"

    iget-wide v4, v2, Lcom/twitter/library/platform/notifications/e;->a:J

    invoke-virtual {v1, v0, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v0, "action_code"

    const/16 v2, 0x19

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    sget-object v2, Lcom/twitter/android/client/NotificationService;->c:Ljava/lang/String;

    const v4, 0x7f02024b    # com.twitter.android.R.drawable.ic_stat_notify_retweet

    const v0, 0x7f0f02cf    # com.twitter.android.R.string.notification_retweet_confirmation

    invoke-virtual {v10, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeLog;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/DialogFragmentActivity;

    invoke-direct {v1, v10, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v2, Lcom/twitter/android/DialogFragmentActivity;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "tweet"

    invoke-virtual {v1, v2, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "notif_service_intent"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v10, v1, v0, v6}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/content/Context;Ljava/lang/Class;Landroid/content/Intent;Lcom/twitter/library/scribe/ScribeLog;)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v1, Landroid/support/v4/app/NotificationCompat$Action;

    const v2, 0x7f02024b    # com.twitter.android.R.drawable.ic_stat_notify_retweet

    const v3, 0x7f0f005e    # com.twitter.android.R.string.button_action_retweet

    invoke-virtual {v10, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Landroid/support/v4/app/NotificationCompat$Action;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_4
    const/4 v3, 0x0

    const/4 v0, 0x0

    move-object v6, v0

    goto :goto_2

    :pswitch_3
    iget-object v0, v8, Lcom/twitter/library/platform/e;->r:Lcom/twitter/library/platform/notifications/e;

    iget-object v1, v8, Lcom/twitter/library/platform/e;->s:Lcom/twitter/library/platform/notifications/g;

    iget-object v2, v1, Lcom/twitter/library/platform/notifications/g;->b:Lcom/twitter/library/platform/notifications/f;

    if-eqz v0, :cond_2

    if-eqz v2, :cond_2

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v4, "status_id"

    iget-wide v5, v0, Lcom/twitter/library/platform/notifications/e;->b:J

    invoke-virtual {v1, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v4, "rt_status_id"

    iget-wide v5, v0, Lcom/twitter/library/platform/notifications/e;->a:J

    invoke-virtual {v1, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v4, "action_code"

    const/4 v5, 0x7

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    if-eqz v3, :cond_5

    iget-object v4, v8, Lcom/twitter/library/platform/e;->s:Lcom/twitter/library/platform/notifications/g;

    iget-object v4, v4, Lcom/twitter/library/platform/notifications/g;->c:Lcom/twitter/library/platform/notifications/f;

    invoke-static {v0, v2, v4}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Lcom/twitter/library/platform/notifications/e;Lcom/twitter/library/platform/notifications/f;Lcom/twitter/library/platform/notifications/f;)Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-static {v10, v0, v2, v4}, Lcom/twitter/library/scribe/ScribeItem;->a(Landroid/content/Context;Lcom/twitter/library/provider/ParcelableTweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_5
    sget-object v2, Lcom/twitter/android/client/NotificationService;->d:Ljava/lang/String;

    const v4, 0x7f020247    # com.twitter.android.R.drawable.ic_stat_notify_favorite

    const v0, 0x7f0f02bf    # com.twitter.android.R.string.notification_favorite_confirmation

    invoke-virtual {v10, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeLog;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    const/high16 v2, 0x10000000

    invoke-static {v10, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v1, Landroid/support/v4/app/NotificationCompat$Action;

    const v2, 0x7f020247    # com.twitter.android.R.drawable.ic_stat_notify_favorite

    const v3, 0x7f0f0058    # com.twitter.android.R.string.button_action_fave

    invoke-virtual {v10, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Landroid/support/v4/app/NotificationCompat$Action;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :pswitch_4
    iget-object v0, v8, Lcom/twitter/library/platform/e;->s:Lcom/twitter/library/platform/notifications/g;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/g;->b:Lcom/twitter/library/platform/notifications/f;

    if-eqz v0, :cond_2

    iget-boolean v1, v0, Lcom/twitter/library/platform/notifications/f;->g:Z

    if-nez v1, :cond_2

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v2, "user_id"

    iget-wide v4, v0, Lcom/twitter/library/platform/notifications/f;->a:J

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v2, "owner_id"

    iget-wide v4, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->b:J

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v2, "action_code"

    const/16 v4, 0xe

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-boolean v0, v0, Lcom/twitter/library/platform/notifications/f;->f:Z

    if-eqz v0, :cond_6

    const v0, 0x7f0f02c9    # com.twitter.android.R.string.notification_follow_request_sent

    :goto_3
    sget-object v2, Lcom/twitter/android/client/NotificationService;->e:Ljava/lang/String;

    const v4, 0x7f020249    # com.twitter.android.R.drawable.ic_stat_notify_follow_checked

    invoke-virtual {v10, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeLog;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    const/high16 v2, 0x10000000

    invoke-static {v10, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v1, Landroid/support/v4/app/NotificationCompat$Action;

    const v2, 0x7f020248    # com.twitter.android.R.drawable.ic_stat_notify_follow

    const v3, 0x7f0f019f    # com.twitter.android.R.string.follow

    invoke-virtual {v10, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Landroid/support/v4/app/NotificationCompat$Action;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_6
    const v0, 0x7f0f02c6    # com.twitter.android.R.string.notification_follow_confirmation

    goto :goto_3

    :pswitch_5
    iget-object v0, v8, Lcom/twitter/library/platform/e;->s:Lcom/twitter/library/platform/notifications/g;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/g;->b:Lcom/twitter/library/platform/notifications/f;

    if-eqz v0, :cond_2

    invoke-static {v10}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Landroid/content/Context;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "@"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/f;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Ljava/lang/String;[I)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a()Landroid/content/Intent;

    move-result-object v0

    invoke-static {}, Lcom/twitter/android/composer/ComposerIntentWrapper;->p()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {p0, v10, v1, v0, v3}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/content/Context;Ljava/lang/Class;Landroid/content/Intent;Lcom/twitter/library/scribe/ScribeLog;)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v1, Landroid/support/v4/app/NotificationCompat$Action;

    const v2, 0x7f020245    # com.twitter.android.R.drawable.ic_stat_notify_compose

    const v3, 0x7f0f02d0    # com.twitter.android.R.string.notification_tweet_to

    invoke-virtual {v10, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Landroid/support/v4/app/NotificationCompat$Action;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :pswitch_6
    iget-object v0, v8, Lcom/twitter/library/platform/e;->s:Lcom/twitter/library/platform/notifications/g;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/g;->b:Lcom/twitter/library/platform/notifications/f;

    if-eqz v0, :cond_2

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v2, "user_id"

    iget-wide v4, v0, Lcom/twitter/library/platform/notifications/f;->a:J

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v0, "action_code"

    const/16 v2, 0x50

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    sget-object v2, Lcom/twitter/android/client/NotificationService;->f:Ljava/lang/String;

    const v4, 0x7f020244    # com.twitter.android.R.drawable.ic_stat_notify_accept

    const v0, 0x7f0f02c7    # com.twitter.android.R.string.notification_follow_request_accepted

    invoke-virtual {v10, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeLog;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    const/high16 v2, 0x10000000

    invoke-static {v10, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v1, Landroid/support/v4/app/NotificationCompat$Action;

    const v2, 0x7f020244    # com.twitter.android.R.drawable.ic_stat_notify_accept

    const v3, 0x7f0f0337    # com.twitter.android.R.string.protected_follower_request_accept

    invoke-virtual {v10, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Landroid/support/v4/app/NotificationCompat$Action;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :pswitch_7
    iget-object v0, v8, Lcom/twitter/library/platform/e;->s:Lcom/twitter/library/platform/notifications/g;

    iget-object v0, v0, Lcom/twitter/library/platform/notifications/g;->b:Lcom/twitter/library/platform/notifications/f;

    if-eqz v0, :cond_2

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v2, "user_id"

    iget-wide v4, v0, Lcom/twitter/library/platform/notifications/f;->a:J

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v0, "action_code"

    const/16 v2, 0x51

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    sget-object v2, Lcom/twitter/android/client/NotificationService;->g:Ljava/lang/String;

    const v4, 0x7f020246    # com.twitter.android.R.drawable.ic_stat_notify_decline

    const v0, 0x7f0f02c8    # com.twitter.android.R.string.notification_follow_request_declined

    invoke-virtual {v10, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeLog;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    const/high16 v2, 0x10000000

    invoke-static {v10, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v1, Landroid/support/v4/app/NotificationCompat$Action;

    const v2, 0x7f020246    # com.twitter.android.R.drawable.ic_stat_notify_decline

    const v3, 0x7f0f0338    # com.twitter.android.R.string.protected_follower_request_deny

    invoke-virtual {v10, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Landroid/support/v4/app/NotificationCompat$Action;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_7
    move-object v0, v7

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private D()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/w;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/w;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/w;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/w;->m()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v0, v0, Lcom/twitter/library/platform/e;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v0, v0, Lcom/twitter/library/platform/e;->i:Ljava/lang/String;

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Ljava/lang/Class;Landroid/content/Intent;Lcom/twitter/library/scribe/ScribeLog;)Landroid/app/PendingIntent;
    .locals 6

    const/high16 v5, 0x10000000

    const/4 v4, 0x0

    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    invoke-direct {p0, v0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/os/Bundle;)V

    const-string/jumbo v1, "notif_scribe_log"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v1, "log_actions_experiment"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    sget-object v1, Lcom/twitter/library/provider/an;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/e;

    iget v2, v2, Lcom/twitter/library/platform/e;->n:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p3, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    if-eqz p2, :cond_0

    invoke-static {p1}, Landroid/support/v4/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/support/v4/app/TaskStackBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/support/v4/app/TaskStackBuilder;->addParentStack(Ljava/lang/Class;)Landroid/support/v4/app/TaskStackBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/support/v4/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/support/v4/app/TaskStackBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/support/v4/app/TaskStackBuilder;->editIntentAt(I)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "sb_account_name"

    iget-object v3, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, v4, v5}, Landroid/support/v4/app/TaskStackBuilder;->getPendingIntent(II)Landroid/app/PendingIntent;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1, v4, p3, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/os/Bundle;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 4

    invoke-direct {p0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/os/Bundle;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->d:Landroid/content/Context;

    const-class v2, Lcom/twitter/android/client/NotificationService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, p2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/provider/an;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/e;

    iget v2, v2, Lcom/twitter/library/platform/e;->n:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->d:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x10000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/os/Bundle;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeLog;ILjava/lang/String;)Landroid/content/Intent;
    .locals 3

    const/4 v1, 0x1

    invoke-direct {p0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/os/Bundle;)V

    const-string/jumbo v0, "notif_scribe_log"

    invoke-virtual {p1, v0, p3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v0, "log_actions_experiment"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    if-eqz p4, :cond_0

    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "undo_allowed"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "undo_icon"

    invoke-virtual {p1, v0, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "undo_text"

    invoke-virtual {p1, v0, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->d:Landroid/content/Context;

    const-class v2, Lcom/twitter/android/client/NotificationService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, p2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/provider/an;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/e;

    iget v2, v2, Lcom/twitter/library/platform/e;->n:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/android/client/notifications/StatusBarNotif;)Landroid/support/v4/app/NotificationCompat$Builder;
    .locals 5

    invoke-virtual {p1, p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/content/Context;)V

    new-instance v0, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v0, p0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->v()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->x()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->p()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setPriority(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->r()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setSubText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->l()Landroid/support/v4/app/NotificationCompat$Style;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setStyle(Landroid/support/v4/app/NotificationCompat$Style;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->q()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setNumber(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-direct {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->C()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/NotificationCompat$Action;

    iget v3, v0, Landroid/support/v4/app/NotificationCompat$Action;->icon:I

    iget-object v4, v0, Landroid/support/v4/app/NotificationCompat$Action;->title:Ljava/lang/CharSequence;

    iget-object v0, v0, Landroid/support/v4/app/NotificationCompat$Action;->actionIntent:Landroid/app/PendingIntent;

    invoke-virtual {v1, v3, v4, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private static a(Lcom/twitter/library/platform/notifications/e;Lcom/twitter/library/platform/notifications/f;Lcom/twitter/library/platform/notifications/f;)Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;
    .locals 13

    new-instance v0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;

    iget-object v1, p0, Lcom/twitter/library/platform/notifications/e;->d:Ljava/lang/String;

    iget-wide v2, p0, Lcom/twitter/library/platform/notifications/e;->a:J

    iget-wide v4, p0, Lcom/twitter/library/platform/notifications/e;->b:J

    iget-wide v6, p1, Lcom/twitter/library/platform/notifications/f;->a:J

    iget-wide v8, p2, Lcom/twitter/library/platform/notifications/f;->a:J

    iget-object v10, p1, Lcom/twitter/library/platform/notifications/f;->b:Ljava/lang/String;

    iget-object v11, p2, Lcom/twitter/library/platform/notifications/f;->b:Ljava/lang/String;

    iget-object v12, p0, Lcom/twitter/library/platform/notifications/e;->f:Ljava/util/ArrayList;

    invoke-direct/range {v0 .. v12}, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;-><init>(Ljava/lang/String;JJJJLjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    return-object v0
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 2

    const-string/jumbo v0, "sb_account_name"

    iget-object v1, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "sb_notification"

    invoke-virtual {p1, v0, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method


# virtual methods
.method public A()Z
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v0, v3, :cond_0

    :goto_0
    return v2

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->t()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->j()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/twitter/library/client/aa;->a(J)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/library/api/UserSettings;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-boolean v0, v0, Lcom/twitter/library/api/UserSettings;->k:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->u()Z

    move-result v4

    if-eqz v4, :cond_1

    if-eqz v0, :cond_3

    :cond_1
    move v0, v1

    :goto_2
    if-eqz v3, :cond_4

    if-eqz v0, :cond_4

    :goto_3
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v1, v2

    goto :goto_3
.end method

.method protected A_()Lcom/twitter/android/client/notifications/w;
    .locals 5

    new-instance v0, Lcom/twitter/android/client/notifications/v;

    iget-object v1, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v2, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->c:Ljava/lang/String;

    iget-wide v3, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->b:J

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/android/client/notifications/v;-><init>(Lcom/twitter/library/platform/e;Ljava/lang/String;J)V

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-direct {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->D()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    iget-wide v2, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->b:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v2, v6, [Ljava/lang/String;

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "notification:status_bar"

    aput-object v4, v3, v5

    const-string/jumbo v4, ""

    aput-object v4, v3, v6

    aput-object v0, v3, v7

    const/4 v0, 0x3

    aput-object p1, v3, v0

    invoke-static {v3}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/twitter/library/scribe/ScribeLog;->c(I)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/scribe/ScribeItem;

    invoke-virtual {v1, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method a(Landroid/content/Context;)V
    .locals 1

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->d:Landroid/content/Context;

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/w;

    invoke-virtual {v0, p1}, Lcom/twitter/android/client/notifications/w;->a(Landroid/content/Context;)V

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->f:Z

    return-void
.end method

.method protected c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v0, v0, Lcom/twitter/library/platform/e;->e:Ljava/lang/String;

    return-object v0
.end method

.method protected abstract d()I
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected abstract e()Ljava/lang/String;
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v0, v0, Lcom/twitter/library/platform/e;->e:Ljava/lang/String;

    return-object v0
.end method

.method protected abstract g()Landroid/content/Intent;
.end method

.method public h()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected i()Ljava/util/List;
    .locals 1

    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    return-object v0
.end method

.method public j()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->b:J

    return-wide v0
.end method

.method public k()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/e;

    iget v0, v0, Lcom/twitter/library/platform/e;->n:I

    return v0
.end method

.method public final l()Landroid/support/v4/app/NotificationCompat$Style;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/w;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/w;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/w;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/w;->j()Landroid/support/v4/app/NotificationCompat$InboxStyle;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    invoke-direct {v0}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    move-result-object v0

    goto :goto_0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/w;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/w;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/w;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/w;->e()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/w;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/w;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/w;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/w;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->z_()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/w;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/w;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/w;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/w;->i()[I

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/w;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/w;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final p()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/w;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/w;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/w;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/w;->h()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/e;

    iget v0, v0, Lcom/twitter/library/platform/e;->o:I

    goto :goto_0
.end method

.method public final q()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/w;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/w;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/w;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/w;->k()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/w;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/w;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/w;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/w;->a()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->d()I

    move-result v0

    goto :goto_0
.end method

.method public s()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/w;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/w;->g()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v0, v0, Lcom/twitter/library/platform/e;->s:Lcom/twitter/library/platform/notifications/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v0, v0, Lcom/twitter/library/platform/e;->s:Lcom/twitter/library/platform/notifications/g;

    invoke-virtual {v0}, Lcom/twitter/library/platform/notifications/g;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public t()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/w;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/w;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v0, v0, Lcom/twitter/library/platform/e;->k:Ljava/lang/String;

    goto :goto_0
.end method

.method public u()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/e;

    iget-boolean v0, v0, Lcom/twitter/library/platform/e;->l:Z

    return v0
.end method

.method public final v()Landroid/app/PendingIntent;
    .locals 6

    const/4 v5, 0x0

    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    const-string/jumbo v1, "open"

    invoke-virtual {p0, v1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string/jumbo v2, "notif_scribe_log"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    iget-boolean v1, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->f:Z

    if-eqz v1, :cond_4

    const-string/jumbo v1, "preview_open_image_loaded"

    invoke-virtual {p0, v1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string/jumbo v2, "notif_scribe_log_for_preview_experiment"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_1
    :goto_0
    const-string/jumbo v1, "log_actions_experiment"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-direct {p0, v0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->B()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "ref_event"

    const-string/jumbo v3, "notification::::open"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "sb_account_name"

    iget-object v3, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->w()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->d:Landroid/content/Context;

    invoke-static {v1}, Landroid/support/v4/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/support/v4/app/TaskStackBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v1, v2}, Landroid/support/v4/app/TaskStackBuilder;->addParentStack(Landroid/content/ComponentName;)Landroid/support/v4/app/TaskStackBuilder;

    :cond_2
    :goto_1
    invoke-virtual {v1}, Landroid/support/v4/app/TaskStackBuilder;->getIntentCount()I

    move-result v2

    if-lez v2, :cond_3

    invoke-virtual {v1, v5}, Landroid/support/v4/app/TaskStackBuilder;->editIntentAt(I)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "sb_account_name"

    iget-object v4, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->c:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_3
    invoke-virtual {v1, v0}, Landroid/support/v4/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/support/v4/app/TaskStackBuilder;

    const/high16 v0, 0x10000000

    invoke-virtual {v1, v5, v0}, Landroid/support/v4/app/TaskStackBuilder;->getPendingIntent(II)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0

    :cond_4
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->A()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lgn;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "preview_open"

    invoke-virtual {p0, v1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string/jumbo v2, "notif_scribe_log_for_preview_experiment"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0

    :cond_5
    const-class v2, Lcom/twitter/android/MainActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->d:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->d:Landroid/content/Context;

    const-class v4, Lcom/twitter/android/MainActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/support/v4/app/TaskStackBuilder;

    goto :goto_1
.end method

.method protected w()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/w;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/w;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/w;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/w;->l()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x14000000

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/e;

    invoke-static {p1, v0}, Lcom/twitter/library/platform/DataSyncResult;->a(Landroid/os/Parcel;Lcom/twitter/library/platform/e;)V

    iget-wide v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public final x()Landroid/app/PendingIntent;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    const-string/jumbo v1, "dismiss"

    invoke-virtual {p0, v1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string/jumbo v2, "notif_scribe_log"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    iget-boolean v1, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->f:Z

    if-eqz v1, :cond_2

    const-string/jumbo v1, "preview_dismiss_image_loaded"

    invoke-virtual {p0, v1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string/jumbo v2, "notif_scribe_log_for_preview_experiment"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_1
    :goto_0
    const-string/jumbo v1, "log_actions_experiment"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    sget-object v1, Lcom/twitter/android/client/NotificationService;->a:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/os/Bundle;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->A()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lgn;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "preview_dismiss"

    invoke-virtual {p0, v1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string/jumbo v2, "notif_scribe_log_for_preview_experiment"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public y()[I
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/w;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/w;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->e:Lcom/twitter/android/client/notifications/w;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/w;->i()[I

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/e;

    iget v2, v2, Lcom/twitter/library/platform/e;->n:I

    aput v2, v0, v1

    goto :goto_0
.end method

.method public z()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->f:Z

    return v0
.end method

.method protected z_()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/notifications/StatusBarNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v0, v0, Lcom/twitter/library/platform/e;->d:Ljava/lang/String;

    return-object v0
.end method
