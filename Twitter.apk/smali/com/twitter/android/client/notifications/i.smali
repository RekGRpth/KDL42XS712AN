.class public Lcom/twitter/android/client/notifications/i;
.super Lcom/twitter/android/client/notifications/w;
.source "Twttr"


# direct methods
.method public constructor <init>(Lcom/twitter/library/platform/e;Ljava/lang/String;J)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/client/notifications/w;-><init>(Lcom/twitter/library/platform/e;Ljava/lang/String;J)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    const v0, 0x7f02024e    # com.twitter.android.R.drawable.ic_stat_twitter

    return v0
.end method

.method protected b(Lcom/twitter/library/platform/d;)Landroid/text/SpannableString;
    .locals 6

    const/4 v3, 0x1

    const/4 v5, 0x0

    iget-object v1, p1, Lcom/twitter/library/platform/d;->d:Ljava/lang/String;

    iget v0, p1, Lcom/twitter/library/platform/d;->a:I

    packed-switch v0, :pswitch_data_0

    iget-object v0, p1, Lcom/twitter/library/platform/d;->c:Ljava/lang/String;

    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/client/notifications/i;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v0

    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/client/notifications/i;->d:Landroid/content/Context;

    const v2, 0x7f0f02a1    # com.twitter.android.R.string.notif_fav_text_format

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p1, Lcom/twitter/library/platform/d;->c:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/client/notifications/i;->d:Landroid/content/Context;

    const v2, 0x7f0f02ae    # com.twitter.android.R.string.notif_rt_text_format

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p1, Lcom/twitter/library/platform/d;->c:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/client/notifications/i;->d:Landroid/content/Context;

    const v2, 0x7f0f02a2    # com.twitter.android.R.string.notif_follow_text

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public b()Ljava/lang/String;
    .locals 5

    const v4, 0x7f0f02a7    # com.twitter.android.R.string.notif_new_interactions

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/i;->d()[Lcom/twitter/library/platform/d;

    move-result-object v0

    array-length v0, v0

    if-le v0, v2, :cond_0

    iget-object v1, p0, Lcom/twitter/android/client/notifications/i;->d:Landroid/content/Context;

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {v1, v4, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/notifications/i;->d:Landroid/content/Context;

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/twitter/android/client/notifications/i;->a:Lcom/twitter/library/platform/e;

    iget v2, v2, Lcom/twitter/library/platform/e;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v4, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "interactions"

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/notifications/i;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/client/notifications/i;->d:Landroid/content/Context;

    const-class v2, Lcom/twitter/android/NotificationActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method
