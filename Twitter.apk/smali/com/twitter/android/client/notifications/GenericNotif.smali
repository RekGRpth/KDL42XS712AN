.class public Lcom/twitter/android/client/notifications/GenericNotif;
.super Lcom/twitter/android/client/notifications/StatusBarNotif;
.source "Twttr"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/android/client/notifications/g;

    invoke-direct {v0}, Lcom/twitter/android/client/notifications/g;-><init>()V

    sput-object v0, Lcom/twitter/android/client/notifications/GenericNotif;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/platform/e;JLjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/client/notifications/StatusBarNotif;-><init>(Lcom/twitter/library/platform/e;JLjava/lang/String;)V

    return-void
.end method


# virtual methods
.method public d()I
    .locals 1

    const v0, 0x7f02024e    # com.twitter.android.R.drawable.ic_stat_twitter

    return v0
.end method

.method protected e()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected g()Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/client/notifications/GenericNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v1, v1, Lcom/twitter/library/platform/e;->j:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "reason"

    iget-object v2, p0, Lcom/twitter/android/client/notifications/GenericNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v2, v2, Lcom/twitter/library/platform/e;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "notification_setting_key"

    iget-object v2, p0, Lcom/twitter/android/client/notifications/GenericNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v2, v2, Lcom/twitter/library/platform/e;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/notifications/GenericNotif;->c:Ljava/lang/String;

    return-object v0
.end method

.method protected i()Ljava/util/List;
    .locals 7

    const/4 v6, 0x3

    const-wide/16 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/GenericNotif;->a:Lcom/twitter/library/platform/e;

    iget-wide v0, v0, Lcom/twitter/library/platform/e;->g:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/GenericNotif;->a:Lcom/twitter/library/platform/e;

    iget-wide v0, v0, Lcom/twitter/library/platform/e;->f:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v6}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v1, p0, Lcom/twitter/android/client/notifications/GenericNotif;->a:Lcom/twitter/library/platform/e;

    iget-wide v1, v1, Lcom/twitter/library/platform/e;->g:J

    cmp-long v1, v1, v4

    if-eqz v1, :cond_1

    new-instance v1, Lcom/twitter/library/scribe/ScribeItem;

    invoke-direct {v1}, Lcom/twitter/library/scribe/ScribeItem;-><init>()V

    iget-object v2, p0, Lcom/twitter/android/client/notifications/GenericNotif;->a:Lcom/twitter/library/platform/e;

    iget-wide v2, v2, Lcom/twitter/library/platform/e;->g:J

    iput-wide v2, v1, Lcom/twitter/library/scribe/ScribeItem;->a:J

    const-string/jumbo v2, "sender_id"

    iput-object v2, v1, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    iput v6, v1, Lcom/twitter/library/scribe/ScribeItem;->c:I

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/client/notifications/GenericNotif;->a:Lcom/twitter/library/platform/e;

    iget-wide v1, v1, Lcom/twitter/library/platform/e;->f:J

    cmp-long v1, v1, v4

    if-eqz v1, :cond_2

    new-instance v1, Lcom/twitter/library/scribe/ScribeItem;

    invoke-direct {v1}, Lcom/twitter/library/scribe/ScribeItem;-><init>()V

    iget-object v2, p0, Lcom/twitter/android/client/notifications/GenericNotif;->a:Lcom/twitter/library/platform/e;

    iget-wide v2, v2, Lcom/twitter/library/platform/e;->f:J

    iput-wide v2, v1, Lcom/twitter/library/scribe/ScribeItem;->a:J

    const-string/jumbo v2, "status_id"

    iput-object v2, v1, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    const/4 v2, 0x0

    iput v2, v1, Lcom/twitter/library/scribe/ScribeItem;->c:I

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    new-instance v1, Lcom/twitter/library/scribe/ScribeItem;

    invoke-direct {v1}, Lcom/twitter/library/scribe/ScribeItem;-><init>()V

    iget-object v2, p0, Lcom/twitter/android/client/notifications/GenericNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v2, v2, Lcom/twitter/library/platform/e;->e:Ljava/lang/String;

    iput-object v2, v1, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    const/4 v2, 0x6

    iput v2, v1, Lcom/twitter/library/scribe/ScribeItem;->c:I

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    return-object v0

    :cond_3
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    goto :goto_0
.end method

.method public z_()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/notifications/GenericNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v0, v0, Lcom/twitter/library/platform/e;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/GenericNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v0, v0, Lcom/twitter/library/platform/e;->d:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/notifications/GenericNotif;->d:Landroid/content/Context;

    const v1, 0x7f0f0032    # com.twitter.android.R.string.app_name

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
