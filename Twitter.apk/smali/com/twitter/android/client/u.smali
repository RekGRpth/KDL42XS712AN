.class Lcom/twitter/android/client/u;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/z;


# instance fields
.field final synthetic a:Lcom/twitter/android/client/BaseFragmentActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/client/BaseFragmentActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/client/u;->a:Lcom/twitter/android/client/BaseFragmentActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/u;->a:Lcom/twitter/android/client/BaseFragmentActivity;

    invoke-static {v0}, Lcom/twitter/android/client/BaseFragmentActivity;->d(Lcom/twitter/android/client/BaseFragmentActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/u;->a:Lcom/twitter/android/client/BaseFragmentActivity;

    invoke-static {v0}, Lgp;->f(Landroid/content/Context;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 2

    invoke-direct {p0}, Lcom/twitter/android/client/u;->a()V

    iget-object v0, p0, Lcom/twitter/android/client/u;->a:Lcom/twitter/android/client/BaseFragmentActivity;

    sget-object v1, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;->c:Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;

    invoke-static {v0, v1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lcom/twitter/android/client/BaseFragmentActivity;Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;)V

    return-void
.end method

.method public a(Landroid/net/Uri;)V
    .locals 2

    invoke-direct {p0}, Lcom/twitter/android/client/u;->a()V

    iget-object v0, p0, Lcom/twitter/android/client/u;->a:Lcom/twitter/android/client/BaseFragmentActivity;

    sget-object v1, Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;->a:Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;

    invoke-static {v0, v1, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lcom/twitter/android/client/BaseFragmentActivity;Lcom/twitter/android/composer/ComposerIntentWrapper$Mode;Landroid/net/Uri;)V

    return-void
.end method
