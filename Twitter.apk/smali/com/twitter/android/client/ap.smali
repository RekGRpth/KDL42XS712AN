.class public abstract Lcom/twitter/android/client/ap;
.super Lcom/twitter/library/service/a;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/library/service/a;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/client/ap;->a:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/internal/android/service/a;)V
    .locals 0

    check-cast p1, Lcom/twitter/library/service/b;

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/ap;->a(Lcom/twitter/library/service/b;)V

    return-void
.end method

.method protected a(Lcom/twitter/library/api/geo/b;)V
    .locals 0

    return-void
.end method

.method protected a(Lcom/twitter/library/api/geo/c;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/service/b;)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/ap;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    iget-wide v1, v1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    cmp-long v0, v1, v3

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/twitter/library/service/b;->r()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :pswitch_0
    check-cast p1, Lcom/twitter/library/api/geo/b;

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/ap;->a(Lcom/twitter/library/api/geo/b;)V

    goto :goto_0

    :pswitch_1
    check-cast p1, Lcom/twitter/library/api/geo/c;

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/ap;->a(Lcom/twitter/library/api/geo/c;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
