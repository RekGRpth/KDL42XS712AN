.class Lcom/twitter/android/client/ca;
.super Landroid/support/v4/widget/CursorAdapter;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/util/ac;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/client/aa;

.field private final c:Landroid/view/View$OnClickListener;

.field private final d:Lcom/twitter/library/util/aa;

.field private final e:Landroid/view/LayoutInflater;

.field private final f:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View$OnClickListener;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/ca;->f:Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/twitter/android/client/ca;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/twitter/android/client/ca;->c:Landroid/view/View$OnClickListener;

    invoke-static {p1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/ca;->b:Lcom/twitter/library/client/aa;

    invoke-static {p1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/android/client/c;->b:Lcom/twitter/library/util/aa;

    iput-object v0, p0, Lcom/twitter/android/client/ca;->d:Lcom/twitter/library/util/aa;

    iget-object v0, p0, Lcom/twitter/android/client/ca;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/ca;->e:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/util/aa;Ljava/util/HashMap;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/ca;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/cb;

    if-nez v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_0
    invoke-virtual {v0, p2}, Lcom/twitter/android/client/cb;->a(Ljava/util/HashMap;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 5

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/cb;

    new-instance v1, Lcom/twitter/library/provider/Tweet;

    invoke-direct {v1, p3}, Lcom/twitter/library/provider/Tweet;-><init>(Landroid/database/Cursor;)V

    iget-object v2, p0, Lcom/twitter/android/client/ca;->b:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    iget-object v4, p0, Lcom/twitter/android/client/ca;->d:Lcom/twitter/library/util/aa;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/client/cb;->a(Lcom/twitter/library/provider/Tweet;JLcom/twitter/library/util/aa;)V

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/ca;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/ca;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/client/ca;->e:Landroid/view/LayoutInflater;

    const v1, 0x7f03005d    # com.twitter.android.R.layout.dream_tweet

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/ca;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v1, Lcom/twitter/android/client/cb;

    invoke-direct {v1, v0}, Lcom/twitter/android/client/cb;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/twitter/android/client/ca;->f:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method
