.class public Lcom/twitter/android/client/TweetDreamService;
.super Landroid/service/dreams/DreamService;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/library/service/c;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x11
.end annotation


# instance fields
.field private final a:Lcom/twitter/android/client/c;

.field private final b:Lcom/twitter/library/client/aa;

.field private final c:Lcom/twitter/library/client/w;

.field private d:Lcom/twitter/library/provider/az;

.field private e:Lcom/twitter/android/widget/DreamViewAnimator;

.field private f:Lcom/twitter/android/client/ca;

.field private g:J

.field private h:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/service/dreams/DreamService;-><init>()V

    new-instance v0, Lcom/twitter/android/client/bz;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/bz;-><init>(Lcom/twitter/android/client/TweetDreamService;)V

    iput-object v0, p0, Lcom/twitter/android/client/TweetDreamService;->h:Ljava/lang/Runnable;

    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/TweetDreamService;->a:Lcom/twitter/android/client/c;

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/TweetDreamService;->b:Lcom/twitter/library/client/aa;

    invoke-static {p0}, Lcom/twitter/library/client/w;->a(Landroid/content/Context;)Lcom/twitter/library/client/w;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/TweetDreamService;->c:Lcom/twitter/library/client/w;

    return-void
.end method

.method private a()Landroid/database/Cursor;
    .locals 8

    invoke-virtual {p0}, Lcom/twitter/android/client/TweetDreamService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/provider/aq;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/twitter/android/client/TweetDreamService;->b:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/provider/bs;->a:[Ljava/lang/String;

    const-string/jumbo v3, "flags&1|512!=0 AND search_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-wide v6, p0, Lcom/twitter/android/client/TweetDreamService;->g:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const-string/jumbo v5, "type_id ASC, _id ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/client/TweetDreamService;)Lcom/twitter/android/widget/DreamViewAnimator;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/TweetDreamService;->e:Lcom/twitter/android/widget/DreamViewAnimator;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/client/TweetDreamService;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/TweetDreamService;->h:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method public a(IILcom/twitter/library/service/b;)V
    .locals 0

    return-void
.end method

.method public b(IILcom/twitter/library/service/b;)V
    .locals 2

    check-cast p3, Lcom/twitter/library/api/search/c;

    invoke-virtual {p3}, Lcom/twitter/library/api/search/c;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/TweetDreamService;->f:Lcom/twitter/android/client/ca;

    invoke-direct {p0}, Lcom/twitter/android/client/TweetDreamService;->a()Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/ca;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_0
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 12

    const/4 v0, 0x1

    const/4 v8, 0x0

    const/4 v6, 0x0

    invoke-super {p0}, Landroid/service/dreams/DreamService;->onAttachedToWindow()V

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/TweetDreamService;->setFullscreen(Z)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/TweetDreamService;->setInteractive(Z)V

    invoke-virtual {p0, v6}, Lcom/twitter/android/client/TweetDreamService;->setScreenBright(Z)V

    invoke-virtual {p0}, Lcom/twitter/android/client/TweetDreamService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/client/TweetDreamService;->b:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-static {v1, v3, v4}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/TweetDreamService;->d:Lcom/twitter/library/provider/az;

    sget-object v0, Lcom/twitter/internal/util/j;->a:Ljava/security/SecureRandom;

    invoke-virtual {v0}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/twitter/android/client/TweetDreamService;->g:J

    new-instance v0, Lcom/twitter/library/api/search/c;

    iget-wide v3, p0, Lcom/twitter/android/client/TweetDreamService;->g:J

    const-string/jumbo v5, "MEDIA"

    const-string/jumbo v7, "timeline"

    move v9, v6

    move v10, v6

    move-object v11, v8

    invoke-direct/range {v0 .. v11}, Lcom/twitter/library/api/search/c;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    const/4 v2, 0x3

    invoke-virtual {v0, v2, v6, v6, v6}, Lcom/twitter/library/api/search/c;->a(IZZZ)Lcom/twitter/library/api/search/c;

    move-result-object v0

    const-string/jumbo v2, "media"

    invoke-virtual {v0, v2}, Lcom/twitter/library/api/search/c;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/twitter/android/client/TweetDreamService;->a:Lcom/twitter/android/client/c;

    invoke-virtual {v2, v0}, Lcom/twitter/android/client/c;->d(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    iget-object v2, p0, Lcom/twitter/android/client/TweetDreamService;->c:Lcom/twitter/library/client/w;

    invoke-virtual {v2, v0, v6, v6, p0}, Lcom/twitter/library/client/w;->a(Lcom/twitter/library/service/b;IILcom/twitter/library/service/c;)Z

    iget-object v0, p0, Lcom/twitter/android/client/TweetDreamService;->d:Lcom/twitter/library/provider/az;

    iget-wide v2, p0, Lcom/twitter/android/client/TweetDreamService;->g:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/provider/az;->r(J)V

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f03005c    # com.twitter.android.R.layout.dream_animator

    invoke-virtual {v0, v2, v8, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x802

    invoke-virtual {v0, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/TweetDreamService;->setContentView(Landroid/view/View;)V

    const v2, 0x7f090137    # com.twitter.android.R.id.animator

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/DreamViewAnimator;

    new-instance v2, Lcom/twitter/android/client/ca;

    invoke-direct {v2, v1, p0}, Lcom/twitter/android/client/ca;-><init>(Landroid/content/Context;Landroid/view/View$OnClickListener;)V

    iput-object v2, p0, Lcom/twitter/android/client/TweetDreamService;->f:Lcom/twitter/android/client/ca;

    iget-object v1, p0, Lcom/twitter/android/client/TweetDreamService;->f:Lcom/twitter/android/client/ca;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/DreamViewAnimator;->setAdapter(Landroid/widget/Adapter;)V

    iput-object v0, p0, Lcom/twitter/android/client/TweetDreamService;->e:Lcom/twitter/android/widget/DreamViewAnimator;

    iget-object v0, p0, Lcom/twitter/android/client/TweetDreamService;->f:Lcom/twitter/android/client/ca;

    invoke-direct {p0}, Lcom/twitter/android/client/TweetDreamService;->a()Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/ca;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/cb;

    invoke-virtual {p0}, Lcom/twitter/android/client/TweetDreamService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/twitter/android/TweetActivity;

    invoke-direct {v2, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "tw"

    iget-object v0, v0, Lcom/twitter/android/client/cb;->a:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v2, 0x14000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/client/TweetDreamService;->finish()V

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 3

    invoke-super {p0}, Landroid/service/dreams/DreamService;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/twitter/android/client/TweetDreamService;->f:Lcom/twitter/android/client/ca;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/ca;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/TweetDreamService;->d:Lcom/twitter/library/provider/az;

    iget-wide v1, p0, Lcom/twitter/android/client/TweetDreamService;->g:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/provider/az;->s(J)V

    return-void
.end method

.method public onDreamingStarted()V
    .locals 4

    invoke-super {p0}, Landroid/service/dreams/DreamService;->onDreamingStarted()V

    iget-object v0, p0, Lcom/twitter/android/client/TweetDreamService;->a:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/client/TweetDreamService;->f:Lcom/twitter/android/client/ca;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/util/ac;)V

    iget-object v0, p0, Lcom/twitter/android/client/TweetDreamService;->e:Lcom/twitter/android/widget/DreamViewAnimator;

    iget-object v1, p0, Lcom/twitter/android/client/TweetDreamService;->h:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1b58

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/widget/DreamViewAnimator;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public onDreamingStopped()V
    .locals 2

    invoke-super {p0}, Landroid/service/dreams/DreamService;->onDreamingStopped()V

    iget-object v0, p0, Lcom/twitter/android/client/TweetDreamService;->e:Lcom/twitter/android/widget/DreamViewAnimator;

    iget-object v1, p0, Lcom/twitter/android/client/TweetDreamService;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/DreamViewAnimator;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/twitter/android/client/TweetDreamService;->a:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/client/TweetDreamService;->f:Lcom/twitter/android/client/ca;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/util/ac;)V

    return-void
.end method
