.class Lcom/twitter/android/client/aa;
.super Lcom/twitter/library/view/k;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/client/BaseListFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/client/BaseListFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/client/aa;->a:Lcom/twitter/android/client/BaseListFragment;

    invoke-direct {p0}, Lcom/twitter/library/view/k;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/client/aa;->a:Lcom/twitter/android/client/BaseListFragment;

    move-object v1, p1

    check-cast v1, Landroid/widget/ListView;

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/client/BaseListFragment;->b(Landroid/widget/ListView;Landroid/view/View;IJ)Z

    move-result v0

    return v0
.end method

.method public b(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/client/aa;->a:Lcom/twitter/android/client/BaseListFragment;

    move-object v1, p1

    check-cast v1, Landroid/widget/ListView;

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/client/BaseListFragment;->c(Landroid/widget/ListView;Landroid/view/View;IJ)V

    return-void
.end method

.method public c(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/client/aa;->a:Lcom/twitter/android/client/BaseListFragment;

    move-object v1, p1

    check-cast v1, Landroid/widget/ListView;

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/client/BaseListFragment;->d(Landroid/widget/ListView;Landroid/view/View;IJ)V

    return-void
.end method

.method public d(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/client/aa;->a:Lcom/twitter/android/client/BaseListFragment;

    move-object v1, p1

    check-cast v1, Landroid/widget/ListView;

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/client/BaseListFragment;->e(Landroid/widget/ListView;Landroid/view/View;IJ)V

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/client/aa;->a:Lcom/twitter/android/client/BaseListFragment;

    move-object v1, p1

    check-cast v1, Landroid/widget/ListView;

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/client/BaseListFragment;->a(Landroid/widget/ListView;Landroid/view/View;IJ)V

    return-void
.end method
