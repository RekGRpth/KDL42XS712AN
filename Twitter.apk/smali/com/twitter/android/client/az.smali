.class Lcom/twitter/android/client/az;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/internal/android/service/m;


# instance fields
.field final synthetic a:Lcom/twitter/android/client/aw;


# direct methods
.method private constructor <init>(Lcom/twitter/android/client/aw;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/client/az;->a:Lcom/twitter/android/client/aw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/client/aw;Lcom/twitter/android/client/ax;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/client/az;-><init>(Lcom/twitter/android/client/aw;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/internal/android/service/a;)V
    .locals 0

    check-cast p1, Lcom/twitter/library/service/n;

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/az;->a(Lcom/twitter/library/service/n;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/n;)V
    .locals 4

    invoke-virtual {p1}, Lcom/twitter/library/service/n;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v2, p0, Lcom/twitter/android/client/az;->a:Lcom/twitter/android/client/aw;

    invoke-static {v2}, Lcom/twitter/android/client/aw;->a(Lcom/twitter/android/client/aw;)Landroid/util/SparseArray;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/az;->a:Lcom/twitter/android/client/aw;

    invoke-static {v0}, Lcom/twitter/android/client/aw;->a(Lcom/twitter/android/client/aw;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/client/az;->a:Lcom/twitter/android/client/aw;

    invoke-static {v0}, Lcom/twitter/android/client/aw;->c(Lcom/twitter/android/client/aw;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/az;->a:Lcom/twitter/android/client/aw;

    invoke-static {v1}, Lcom/twitter/android/client/aw;->b(Lcom/twitter/android/client/aw;)Lcom/twitter/library/client/z;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->b(Lcom/twitter/library/client/z;)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/client/az;->a:Lcom/twitter/android/client/aw;

    invoke-static {v0}, Lcom/twitter/android/client/aw;->d(Lcom/twitter/android/client/aw;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/az;->a:Lcom/twitter/android/client/aw;

    invoke-static {v1}, Lcom/twitter/android/client/aw;->c(Lcom/twitter/android/client/aw;)Lcom/twitter/library/client/aa;

    move-result-object v1

    iget-wide v2, p1, Lcom/twitter/library/service/n;->d:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/client/aa;->a(J)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->f(Lcom/twitter/library/client/Session;)V

    return-void
.end method
