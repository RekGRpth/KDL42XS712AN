.class public Lcom/twitter/android/client/bs;
.super Landroid/support/v4/widget/CursorAdapter;
.source "Twttr"


# static fields
.field private static final a:Landroid/util/SparseIntArray;


# instance fields
.field private final b:Ljava/util/ArrayList;

.field private final c:Lcom/twitter/android/client/c;

.field private final d:Landroid/view/View$OnClickListener;

.field private final e:Lcom/twitter/android/ob;

.field private f:Ljava/lang/String;

.field private final g:Z

.field private final h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Landroid/util/SparseIntArray;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/util/SparseIntArray;-><init>(I)V

    sput-object v0, Lcom/twitter/android/client/bs;->a:Landroid/util/SparseIntArray;

    sget-object v0, Lcom/twitter/android/client/bs;->a:Landroid/util/SparseIntArray;

    const/4 v1, 0x6

    const v2, 0x7f0f00c4    # com.twitter.android.R.string.concierge_news_about

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/twitter/android/client/bs;->a:Landroid/util/SparseIntArray;

    const/4 v1, 0x3

    const v2, 0x7f0f00c9    # com.twitter.android.R.string.concierge_photos_about

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/twitter/android/client/bs;->a:Landroid/util/SparseIntArray;

    const/4 v1, 0x5

    const v2, 0x7f0f00cc    # com.twitter.android.R.string.concierge_videos_about

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/twitter/android/client/bs;->a:Landroid/util/SparseIntArray;

    const/4 v1, 0x2

    const v2, 0x7f0f00c7    # com.twitter.android.R.string.concierge_people_about

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/twitter/android/client/c;Landroid/view/View$OnClickListener;Lcom/twitter/android/ob;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v4}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/bs;->b:Ljava/util/ArrayList;

    iput-object p2, p0, Lcom/twitter/android/client/bs;->c:Lcom/twitter/android/client/c;

    iput-object p3, p0, Lcom/twitter/android/client/bs;->d:Landroid/view/View$OnClickListener;

    iput-object p4, p0, Lcom/twitter/android/client/bs;->e:Lcom/twitter/android/ob;

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->aF()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v3, p0, Lcom/twitter/android/client/bs;->h:Z

    :goto_0
    invoke-static {}, Lcom/twitter/library/featureswitch/a;->aG()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-boolean v3, p0, Lcom/twitter/android/client/bs;->g:Z

    :goto_1
    return-void

    :cond_0
    const-string/jumbo v0, "android_search_bolding_1842"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v0, "android_search_bolding_1842"

    new-array v1, v3, [Ljava/lang/String;

    const-string/jumbo v2, "reverse_bolding"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/client/bs;->h:Z

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "android_search_tapahead_1949"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v0, "android_search_tapahead_1949"

    new-array v1, v3, [Ljava/lang/String;

    const-string/jumbo v2, "tap_ahead"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/client/bs;->g:Z

    goto :goto_1
.end method

.method private a(I)I
    .locals 1

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const v0, 0x7f02021d    # com.twitter.android.R.drawable.ic_search_filters_everything

    :goto_0
    return v0

    :pswitch_1
    const v0, 0x7f020220    # com.twitter.android.R.drawable.ic_search_filters_news

    goto :goto_0

    :pswitch_2
    const v0, 0x7f020223    # com.twitter.android.R.drawable.ic_search_filters_people

    goto :goto_0

    :pswitch_3
    const v0, 0x7f020226    # com.twitter.android.R.drawable.ic_search_filters_photos

    goto :goto_0

    :pswitch_4
    const v0, 0x7f02022c    # com.twitter.android.R.drawable.ic_search_filters_video

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method private a(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 6

    iget-boolean v0, p0, Lcom/twitter/android/client/bs;->h:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/client/bs;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/client/bs;->f:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/client/bs;->f:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-le v3, v4, :cond_0

    add-int v4, v3, v2

    if-ge v4, v1, :cond_0

    new-instance v4, Landroid/text/style/StyleSpan;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    add-int/2addr v2, v3

    const/16 v3, 0x21

    invoke-virtual {v0, v4, v2, v1, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    :cond_0
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private a(Lcom/twitter/android/client/bt;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/client/bs;->a(Lcom/twitter/android/client/bt;Ljava/lang/String;I)V

    return-void
.end method

.method private a(Lcom/twitter/android/client/bt;Ljava/lang/String;I)V
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/android/client/bs;->g:Z

    if-eqz v0, :cond_1

    if-nez p3, :cond_1

    iget-object v0, p1, Lcom/twitter/android/client/bt;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p1, Lcom/twitter/android/client/bt;->c:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/client/bs;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p1, Lcom/twitter/android/client/bt;->c:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/twitter/android/client/bs;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/twitter/android/client/bt;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;)V
    .locals 4

    iget v0, p1, Lcom/twitter/library/util/ao;->g:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/client/bs;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/zx;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/twitter/android/zx;->g:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/zx;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/util/ae;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/twitter/android/zx;->b:Lcom/twitter/library/widget/BaseUserView;

    iget-object v0, v1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v3, v0}, Lcom/twitter/library/widget/BaseUserView;->setUserImage(Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/twitter/android/client/bs;->f:Ljava/lang/String;

    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 12

    const/16 v5, 0x8

    const/4 v4, 0x0

    const/16 v11, 0xc

    const/4 v9, 0x2

    const/4 v1, 0x1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    add-int/lit8 v6, v0, 0x1

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    invoke-interface {p3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/bt;

    if-eq v2, v1, :cond_8

    iget-object v2, v0, Lcom/twitter/android/client/bt;->a:Landroid/widget/TextView;

    invoke-direct {p0, v2, v3}, Lcom/twitter/android/client/bs;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/twitter/android/client/bs;->g:Z

    if-eqz v2, :cond_0

    invoke-direct {p0, v0, v3}, Lcom/twitter/android/client/bs;->a(Lcom/twitter/android/client/bt;Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-static {v3, v11, v6}, Lcom/twitter/library/scribe/ScribeItem;->a(Ljava/lang/String;II)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    :goto_1
    iget-object v2, p0, Lcom/twitter/android/client/bs;->e:Lcom/twitter/android/ob;

    if-eqz v2, :cond_1

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2, v1}, Landroid/os/Bundle;-><init>(I)V

    const-string/jumbo v1, "position"

    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/twitter/android/client/bs;->e:Lcom/twitter/android/ob;

    invoke-interface {v1, p1, v0, v2}, Lcom/twitter/android/ob;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    :cond_1
    return-void

    :pswitch_1
    move-object v0, p1

    check-cast v0, Lcom/twitter/library/widget/UserSocialView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/UserSocialView;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/zx;

    const/16 v3, 0xb

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    invoke-virtual {v0, v7, v8}, Lcom/twitter/library/widget/UserSocialView;->setUserId(J)V

    iput-wide v7, v2, Lcom/twitter/android/zx;->d:J

    invoke-interface {p3, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, p0, Lcom/twitter/android/client/bs;->c:Lcom/twitter/android/client/c;

    iget-object v5, v5, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    invoke-interface {v5, v9, v3}, Lcom/twitter/library/widget/ap;->a(ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/twitter/library/widget/UserSocialView;->setUserImage(Landroid/graphics/Bitmap;)V

    iput-object v3, v2, Lcom/twitter/android/zx;->g:Ljava/lang/String;

    :goto_2
    invoke-interface {p3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v3, 0x3

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v7}, Lcom/twitter/library/widget/UserSocialView;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v3, 0xd

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-ne v3, v1, :cond_4

    move v3, v1

    :goto_3
    invoke-virtual {v0, v3}, Lcom/twitter/library/widget/UserSocialView;->setVerified(Z)V

    const/16 v3, 0xe

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/twitter/android/zx;->e:I

    invoke-static {v3}, Lcom/twitter/library/provider/ay;->c(I)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {v3}, Lcom/twitter/library/provider/ay;->b(I)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_2
    const v2, 0x7f020117    # com.twitter.android.R.drawable.ic_activity_follow_tweet_default

    iget-object v4, p0, Lcom/twitter/android/client/bs;->c:Lcom/twitter/android/client/c;

    iget-boolean v4, v4, Lcom/twitter/android/client/c;->f:Z

    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/library/widget/UserSocialView;->a(IIZ)V

    :goto_4
    const/4 v0, 0x3

    invoke-static {v7, v0, v6}, Lcom/twitter/library/scribe/ScribeItem;->a(Ljava/lang/String;II)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/twitter/library/widget/UserSocialView;->setUserImage(Landroid/graphics/Bitmap;)V

    goto :goto_2

    :cond_4
    move v3, v4

    goto :goto_3

    :cond_5
    const v2, 0x7f020117    # com.twitter.android.R.drawable.ic_activity_follow_tweet_default

    const/16 v3, 0xf

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/twitter/android/client/bs;->c:Lcom/twitter/android/client/c;

    iget-boolean v5, v5, Lcom/twitter/android/client/c;->f:Z

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/widget/UserSocialView;->a(IILjava/lang/String;IZ)V

    goto :goto_4

    :pswitch_2
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/bv;

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/twitter/android/client/bv;->a:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v0, Lcom/twitter/android/client/bv;->b:Landroid/widget/TextView;

    const/4 v3, 0x5

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {v2, v5, v6}, Lcom/twitter/library/scribe/ScribeItem;->a(Ljava/lang/String;II)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    goto/16 :goto_1

    :pswitch_3
    invoke-interface {p3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/bt;

    iget-object v3, v0, Lcom/twitter/android/client/bt;->a:Landroid/widget/TextView;

    invoke-direct {p0, v3, v2}, Lcom/twitter/android/client/bs;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    invoke-direct {p0, v0, v2}, Lcom/twitter/android/client/bs;->a(Lcom/twitter/android/client/bt;Ljava/lang/String;)V

    const/16 v0, 0xd

    invoke-static {v2, v0, v6}, Lcom/twitter/library/scribe/ScribeItem;->a(Ljava/lang/String;II)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    goto/16 :goto_1

    :pswitch_4
    invoke-interface {p3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/bu;

    const/4 v3, 0x3

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/twitter/android/client/bu;->b:Landroid/widget/TextView;

    invoke-direct {p0, v4, v2}, Lcom/twitter/android/client/bs;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    iget-object v4, v0, Lcom/twitter/android/client/bu;->a:Landroid/view/View;

    iget-object v5, p0, Lcom/twitter/android/client/bs;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, v0, Lcom/twitter/android/client/bu;->a:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-static {v2, v11, v6}, Lcom/twitter/library/scribe/ScribeItem;->a(Ljava/lang/String;II)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    goto/16 :goto_1

    :pswitch_5
    invoke-interface {p3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/bt;

    invoke-direct {p0, v3}, Lcom/twitter/android/client/bs;->a(I)I

    move-result v4

    iget-object v5, v0, Lcom/twitter/android/client/bt;->a:Landroid/widget/TextView;

    invoke-direct {p0, v5, v2}, Lcom/twitter/android/client/bs;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    iget-object v5, v0, Lcom/twitter/android/client/bt;->b:Landroid/widget/ImageView;

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-direct {p0, v0, v2, v3}, Lcom/twitter/android/client/bs;->a(Lcom/twitter/android/client/bt;Ljava/lang/String;I)V

    invoke-static {v2, v11, v6}, Lcom/twitter/library/scribe/ScribeItem;->a(Ljava/lang/String;II)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    goto/16 :goto_1

    :pswitch_6
    invoke-interface {p3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/bt;

    sget-object v5, Lcom/twitter/android/client/bs;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v5, v3, v4}, Landroid/util/SparseIntArray;->get(II)I

    move-result v5

    invoke-direct {p0, v3}, Lcom/twitter/android/client/bs;->a(I)I

    move-result v7

    if-lez v5, :cond_7

    new-array v8, v1, [Ljava/lang/Object;

    aput-object v2, v8, v4

    invoke-virtual {p2, v5, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Landroid/text/SpannableString;

    invoke-direct {v5, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    if-lez v8, :cond_6

    new-instance v9, Landroid/text/style/StyleSpan;

    invoke-direct {v9, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v10, 0x21

    invoke-virtual {v5, v9, v8, v4, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    :cond_6
    iget-object v4, v0, Lcom/twitter/android/client/bt;->a:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_5
    iget-object v4, v0, Lcom/twitter/android/client/bt;->b:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-direct {p0, v0, v2, v3}, Lcom/twitter/android/client/bs;->a(Lcom/twitter/android/client/bt;Ljava/lang/String;I)V

    invoke-static {v2, v11, v6}, Lcom/twitter/library/scribe/ScribeItem;->a(Ljava/lang/String;II)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    goto/16 :goto_1

    :cond_7
    iget-object v4, v0, Lcom/twitter/android/client/bt;->a:Landroid/widget/TextView;

    invoke-direct {p0, v4, v2}, Lcom/twitter/android/client/bs;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    goto :goto_5

    :cond_8
    iget-object v2, v0, Lcom/twitter/android/client/bt;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v0, Lcom/twitter/android/client/bt;->c:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_6
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_5
    .end packed-switch
.end method

.method public getItemViewType(I)I
    .locals 2

    const/4 v1, 0x1

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/bs;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_1
    move v0, v1

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x5

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x6

    return v0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v0, 0x1

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03014b    # com.twitter.android.R.layout.suggestion_row_view

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/client/bt;

    invoke-direct {v1, v0}, Lcom/twitter/android/client/bt;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    return-object v0

    :pswitch_1
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030169    # com.twitter.android.R.layout.typeahead_user_social_row_view

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/UserSocialView;

    new-instance v1, Lcom/twitter/android/zx;

    invoke-direct {v1, v0}, Lcom/twitter/android/zx;-><init>(Lcom/twitter/library/widget/BaseUserView;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/UserSocialView;->setTag(Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/twitter/android/client/bs;->b:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_2
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03012d    # com.twitter.android.R.layout.search_query_row_view

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/client/bv;

    invoke-direct {v1, v0}, Lcom/twitter/android/client/bv;-><init>(Landroid/view/View;)V

    const v2, 0x7f09004d    # com.twitter.android.R.id.promoted

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, v1, Lcom/twitter/android/client/bv;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_3
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300fc    # com.twitter.android.R.layout.recent_search_row_view

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/client/bu;

    invoke-direct {v1, v0}, Lcom/twitter/android/client/bu;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
