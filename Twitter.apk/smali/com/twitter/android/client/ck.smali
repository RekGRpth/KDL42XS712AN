.class Lcom/twitter/android/client/ck;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/internal/network/i;


# instance fields
.field final synthetic a:Lcom/twitter/android/client/cj;

.field private b:Landroid/graphics/Bitmap;


# direct methods
.method private constructor <init>(Lcom/twitter/android/client/cj;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/client/ck;->a:Lcom/twitter/android/client/cj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/client/cj;Lcom/twitter/android/client/ch;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/client/ck;-><init>(Lcom/twitter/android/client/cj;)V

    return-void
.end method


# virtual methods
.method public a()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/ck;->b:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public a(ILjava/io/InputStream;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/ck;->a:Lcom/twitter/android/client/cj;

    invoke-static {v0}, Lcom/twitter/android/client/cj;->a(Lcom/twitter/android/client/cj;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c00d4    # com.twitter.android.R.dimen.user_image_size

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/client/ck;->a:Lcom/twitter/android/client/cj;

    invoke-static {v1}, Lcom/twitter/android/client/cj;->a(Lcom/twitter/android/client/cj;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p2}, Lkw;->a(Landroid/content/Context;Ljava/io/InputStream;)Lkw;

    move-result-object v1

    invoke-virtual {v1, v0}, Lkw;->a(I)Lkw;

    move-result-object v0

    invoke-virtual {v0}, Lkw;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/ck;->b:Landroid/graphics/Bitmap;

    return-void
.end method

.method public a(Lcom/twitter/internal/network/k;)V
    .locals 0

    return-void
.end method
