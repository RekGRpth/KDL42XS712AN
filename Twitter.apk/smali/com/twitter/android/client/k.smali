.class Lcom/twitter/android/client/k;
.super Landroid/support/v4/view/accessibility/AccessibilityManagerCompat$AccessibilityStateChangeListenerCompat;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/client/c;


# direct methods
.method constructor <init>(Lcom/twitter/android/client/c;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/client/k;->a:Lcom/twitter/android/client/c;

    invoke-direct {p0}, Landroid/support/v4/view/accessibility/AccessibilityManagerCompat$AccessibilityStateChangeListenerCompat;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccessibilityStateChanged(Z)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/k;->a:Lcom/twitter/android/client/c;

    iget-object v0, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/util/Util;->o(Landroid/content/Context;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/k;->a:Lcom/twitter/android/client/c;

    invoke-static {v0}, Lcom/twitter/android/client/c;->d(Lcom/twitter/android/client/c;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iget-object v2, p0, Lcom/twitter/android/client/k;->a:Lcom/twitter/android/client/c;

    iget-object v2, v2, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v3, v0, v1}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v4, "app::::explorebytouch_enabled"

    aput-object v4, v0, v1

    invoke-virtual {v3, v0}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_0
    return-void
.end method
