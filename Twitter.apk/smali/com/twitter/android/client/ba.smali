.class Lcom/twitter/android/client/ba;
.super Lcom/twitter/library/client/z;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/client/aw;


# direct methods
.method private constructor <init>(Lcom/twitter/android/client/aw;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/client/ba;->a:Lcom/twitter/android/client/aw;

    invoke-direct {p0}, Lcom/twitter/library/client/z;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/client/aw;Lcom/twitter/android/client/ax;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/client/ba;-><init>(Lcom/twitter/android/client/aw;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;Z)V
    .locals 7

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    iget-object v0, p0, Lcom/twitter/android/client/ba;->a:Lcom/twitter/android/client/aw;

    invoke-static {v0}, Lcom/twitter/android/client/aw;->a(Lcom/twitter/android/client/aw;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/client/ba;->a:Lcom/twitter/android/client/aw;

    invoke-static {v0}, Lcom/twitter/android/client/aw;->a(Lcom/twitter/android/client/aw;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    iget-object v0, p0, Lcom/twitter/android/client/ba;->a:Lcom/twitter/android/client/aw;

    invoke-static {v0}, Lcom/twitter/android/client/aw;->a(Lcom/twitter/android/client/aw;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/notifications/StatusBarNotif;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->j()J

    move-result-wide v5

    cmp-long v0, v5, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/ba;->a:Lcom/twitter/android/client/aw;

    invoke-static {v0}, Lcom/twitter/android/client/aw;->a(Lcom/twitter/android/client/aw;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->remove(I)V

    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/client/ba;->a:Lcom/twitter/android/client/aw;

    invoke-static {v0}, Lcom/twitter/android/client/aw;->a(Lcom/twitter/android/client/aw;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/client/ba;->a:Lcom/twitter/android/client/aw;

    invoke-static {v0}, Lcom/twitter/android/client/aw;->c(Lcom/twitter/android/client/aw;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/ba;->a:Lcom/twitter/android/client/aw;

    invoke-static {v1}, Lcom/twitter/android/client/aw;->b(Lcom/twitter/android/client/aw;)Lcom/twitter/library/client/z;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->b(Lcom/twitter/library/client/z;)V

    :cond_2
    return-void
.end method
