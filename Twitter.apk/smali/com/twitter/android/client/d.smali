.class Lcom/twitter/android/client/d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/client/c;


# direct methods
.method constructor <init>(Lcom/twitter/android/client/c;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/client/d;->a:Lcom/twitter/android/client/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    const-string/jumbo v0, "sound_effects"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/client/d;->a:Lcom/twitter/android/client/c;

    invoke-interface {p1, p2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, Lcom/twitter/android/client/c;->j:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string/jumbo v0, "font_size"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "font_size"

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/client/d;->a:Lcom/twitter/android/client/c;

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    iget-object v2, p0, Lcom/twitter/android/client/d;->a:Lcom/twitter/android/client/c;

    iget v2, v2, Lcom/twitter/android/client/c;->e:F

    mul-float/2addr v0, v2

    invoke-static {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/android/client/c;F)F

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "location"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/client/d;->a:Lcom/twitter/android/client/c;

    invoke-interface {p1, p2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->c(Z)V

    goto :goto_0

    :cond_3
    const-string/jumbo v0, "twitter_access_config"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/client/d;->a:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/client/d;->a:Lcom/twitter/android/client/c;

    invoke-virtual {v1, p1}, Lcom/twitter/android/client/c;->a(Landroid/content/SharedPreferences;)Lcom/twitter/library/api/ad;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/android/client/c;Lcom/twitter/library/api/ad;)Lcom/twitter/library/api/ad;

    iget-object v0, p0, Lcom/twitter/android/client/d;->a:Lcom/twitter/android/client/c;

    invoke-static {v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/android/client/c;)Lcom/twitter/library/api/ad;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/client/d;->a:Lcom/twitter/android/client/c;

    invoke-static {v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/android/client/c;)Lcom/twitter/library/api/ad;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/api/ad;->i:Ljava/util/HashMap;

    invoke-static {v0}, Lcom/twitter/library/network/d;->a(Ljava/util/HashMap;)V

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/client/d;->a:Lcom/twitter/android/client/c;

    invoke-static {v0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/android/client/c;)V

    goto :goto_0

    :cond_4
    invoke-static {v2}, Lcom/twitter/library/network/d;->a(Ljava/util/HashMap;)V

    goto :goto_1

    :cond_5
    const-string/jumbo v0, "pb_enabled"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/twitter/android/client/d;->a:Lcom/twitter/android/client/c;

    const/4 v1, 0x0

    invoke-interface {p1, p2, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->b(I)V

    goto :goto_0

    :cond_6
    const-string/jumbo v0, "lifeline_alerts_enabled"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/twitter/android/client/d;->a:Lcom/twitter/android/client/c;

    invoke-interface {p1, p2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, Lcom/twitter/android/client/c;->m:Z

    goto/16 :goto_0

    :cond_7
    const-string/jumbo v0, "media_forward"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/d;->a:Lcom/twitter/android/client/c;

    sget-boolean v1, Lcom/twitter/library/provider/t;->a:Z

    invoke-interface {p1, p2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/android/client/c;Z)V

    goto/16 :goto_0
.end method
