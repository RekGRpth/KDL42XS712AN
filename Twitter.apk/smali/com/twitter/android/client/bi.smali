.class Lcom/twitter/android/client/bi;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/client/ak;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/provider/Tweet;

.field private final c:Lcom/twitter/library/scribe/ScribeAssociation;

.field private final d:Lcom/twitter/library/api/MediaEntity;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Lcom/twitter/library/api/MediaEntity;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/client/bi;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/twitter/android/client/bi;->b:Lcom/twitter/library/provider/Tweet;

    iput-object p3, p0, Lcom/twitter/android/client/bi;->c:Lcom/twitter/library/scribe/ScribeAssociation;

    iput-object p4, p0, Lcom/twitter/android/client/bi;->d:Lcom/twitter/library/api/MediaEntity;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    const/4 v1, 0x2

    const/4 v0, 0x0

    const/4 v2, -0x1

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/twitter/android/client/bi;->a:Landroid/content/Context;

    const-class v5, Lcom/twitter/android/GalleryActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v4, "tw"

    iget-object v5, p0, Lcom/twitter/android/client/bi;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "media"

    iget-object v5, p0, Lcom/twitter/android/client/bi;->d:Lcom/twitter/library/api/MediaEntity;

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "association"

    iget-object v5, p0, Lcom/twitter/android/client/bi;->c:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v4

    iget-object v3, p0, Lcom/twitter/android/client/bi;->c:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v3}, Lcom/twitter/library/scribe/ScribeAssociation;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_0
    move v3, v2

    :goto_0
    packed-switch v3, :pswitch_data_0

    move v0, v2

    :goto_1
    :pswitch_0
    if-lez v0, :cond_1

    const-string/jumbo v1, "context"

    invoke-virtual {v4, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/client/bi;->a:Landroid/content/Context;

    invoke-virtual {v0, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void

    :sswitch_0
    const-string/jumbo v5, "home"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v3, v0

    goto :goto_0

    :sswitch_1
    const-string/jumbo v5, "profile"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :sswitch_2
    const-string/jumbo v5, "search"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v3, v1

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x4

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/client/bi;->c:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0}, Lcom/twitter/library/scribe/ScribeAssociation;->c()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "cluster"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x36059a58 -> :sswitch_2
        -0x12717657 -> :sswitch_1
        0x30f4df -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public b()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
