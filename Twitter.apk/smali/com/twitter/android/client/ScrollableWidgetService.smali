.class public Lcom/twitter/android/client/ScrollableWidgetService;
.super Landroid/widget/RemoteViewsService;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/widget/RemoteViewsService;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetViewFactory(Landroid/content/Intent;)Landroid/widget/RemoteViewsService$RemoteViewsFactory;
    .locals 6

    new-instance v0, Lcom/twitter/android/client/cj;

    invoke-virtual {p0}, Lcom/twitter/android/client/ScrollableWidgetService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "contentType"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const-string/jumbo v3, "ownerId"

    const-wide/16 v4, 0x0

    invoke-virtual {p1, v3, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    const-string/jumbo v5, "accountName"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/client/cj;-><init>(Landroid/content/Context;IJLjava/lang/String;)V

    return-object v0
.end method
