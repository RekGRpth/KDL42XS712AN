.class Lcom/twitter/android/client/aq;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Lcom/twitter/library/client/j;

.field public final b:Lcom/twitter/library/client/z;

.field private final c:Landroid/content/Context;

.field private final d:Landroid/content/pm/PackageManager;

.field private final e:Ljava/lang/String;

.field private final f:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/twitter/android/client/as;

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/client/as;-><init>(Lcom/twitter/android/client/aq;Lcom/twitter/android/client/ar;)V

    iput-object v0, p0, Lcom/twitter/android/client/aq;->a:Lcom/twitter/library/client/j;

    new-instance v0, Lcom/twitter/android/client/at;

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/client/at;-><init>(Lcom/twitter/android/client/aq;Lcom/twitter/android/client/ar;)V

    iput-object v0, p0, Lcom/twitter/android/client/aq;->b:Lcom/twitter/library/client/z;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/aq;->c:Landroid/content/Context;

    iget-object v0, p0, Lcom/twitter/android/client/aq;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/aq;->d:Landroid/content/pm/PackageManager;

    iget-object v0, p0, Lcom/twitter/android/client/aq;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/aq;->e:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/client/aq;->d:Landroid/content/pm/PackageManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/aq;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/aq;->d:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lcom/twitter/android/client/aq;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/aq;->f:Landroid/content/Intent;

    :goto_0
    return-void

    :cond_0
    iput-object v1, p0, Lcom/twitter/android/client/aq;->f:Landroid/content/Intent;

    goto :goto_0
.end method

.method private a()Z
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/aq;->d:Landroid/content/pm/PackageManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/aq;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/aq;->d:Landroid/content/pm/PackageManager;

    const-string/jumbo v1, "com.sonyericsson.home.permission.BROADCAST_BADGE"

    iget-object v2, p0, Lcom/twitter/android/client/aq;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()Z
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/aq;->d:Landroid/content/pm/PackageManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/aq;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/aq;->d:Landroid/content/pm/PackageManager;

    const-string/jumbo v1, "com.sec.android.provider.badge.permission.READ"

    iget-object v2, p0, Lcom/twitter/android/client/aq;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/aq;->d:Landroid/content/pm/PackageManager;

    const-string/jumbo v1, "com.sec.android.provider.badge.permission.WRITE"

    iget-object v2, p0, Lcom/twitter/android/client/aq;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()Z
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/aq;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/yn;->a(Landroid/content/Context;)Landroid/content/ContentProviderClient;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()Z
    .locals 1

    invoke-static {}, Lcom/twitter/library/util/v;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/client/aq;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()Z
    .locals 1

    invoke-static {}, Lcom/twitter/library/util/v;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/client/aq;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/client/aq;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/client/aq;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/aq;->f:Landroid/content/Intent;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/twitter/android/client/aq;->e()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/client/aq;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;)V
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/twitter/android/client/aq;->c:Landroid/content/Context;

    invoke-static {v2}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/twitter/library/client/Session;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/client/aq;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "android_outside_badge_1232"

    invoke-static {v2}, Lkk;->b(Ljava/lang/String;)V

    new-instance v2, Lcom/twitter/android/yn;

    iget-object v3, p0, Lcom/twitter/android/client/aq;->c:Landroid/content/Context;

    iget-object v4, p0, Lcom/twitter/android/client/aq;->f:Landroid/content/Intent;

    iget-object v5, p0, Lcom/twitter/android/client/aq;->e:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v5}, Lcom/twitter/android/yn;-><init>(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)V

    new-array v3, v0, [Ljava/lang/Boolean;

    const-string/jumbo v4, "android_outside_badge_1232"

    invoke-static {v4}, Lkk;->c(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v3, v1

    invoke-virtual {v2, v3}, Lcom/twitter/android/yn;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method
