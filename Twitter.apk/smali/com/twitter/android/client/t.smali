.class Lcom/twitter/android/client/t;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/client/BaseFragmentActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/client/BaseFragmentActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/client/t;->a:Lcom/twitter/android/client/BaseFragmentActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/client/t;->a:Lcom/twitter/android/client/BaseFragmentActivity;

    invoke-static {v0}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lcom/twitter/android/client/BaseFragmentActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/twitter/android/client/t;->a:Lcom/twitter/android/client/BaseFragmentActivity;

    invoke-static {v3, v0}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lcom/twitter/android/client/BaseFragmentActivity;Z)V

    if-eqz v0, :cond_1

    const-string/jumbo v0, "::compose_bar:gallery_bar:open"

    :goto_1
    iget-object v3, p0, Lcom/twitter/android/client/t;->a:Lcom/twitter/android/client/BaseFragmentActivity;

    invoke-static {v3}, Lcom/twitter/android/client/BaseFragmentActivity;->c(Lcom/twitter/android/client/BaseFragmentActivity;)Lcom/twitter/android/client/c;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/client/t;->a:Lcom/twitter/android/client/BaseFragmentActivity;

    invoke-static {v4}, Lcom/twitter/android/client/BaseFragmentActivity;->b(Lcom/twitter/android/client/BaseFragmentActivity;)Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    new-array v1, v1, [Ljava/lang/String;

    aput-object v0, v1, v2

    invoke-virtual {v3, v4, v5, v1}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "::compose_bar:gallery_bar:close"

    goto :goto_1
.end method
