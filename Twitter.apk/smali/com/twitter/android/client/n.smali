.class Lcom/twitter/android/client/n;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:J

.field final synthetic d:Lcom/twitter/android/client/c;


# direct methods
.method constructor <init>(Lcom/twitter/android/client/c;Landroid/content/Context;Ljava/lang/String;J)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/client/n;->d:Lcom/twitter/android/client/c;

    iput-object p2, p0, Lcom/twitter/android/client/n;->a:Landroid/content/Context;

    iput-object p3, p0, Lcom/twitter/android/client/n;->b:Ljava/lang/String;

    iput-wide p4, p0, Lcom/twitter/android/client/n;->c:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/n;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/n;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->b()Lcom/twitter/library/client/Session$LoginStatus;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/client/Session$LoginStatus;->a:Lcom/twitter/library/client/Session$LoginStatus;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/n;->a:Landroid/content/Context;

    iget-wide v1, p0, Lcom/twitter/android/client/n;->c:J

    invoke-static {v0, v1, v2}, Lcom/twitter/library/provider/az;->b(Landroid/content/Context;J)V

    iget-object v0, p0, Lcom/twitter/android/client/n;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/provider/f;->a(Landroid/content/Context;)Lcom/twitter/library/provider/f;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/n;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/f;->d(Ljava/lang/String;)I

    iget-object v1, p0, Lcom/twitter/android/client/n;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/f;->c(Ljava/lang/String;)I

    :cond_0
    return-void
.end method
