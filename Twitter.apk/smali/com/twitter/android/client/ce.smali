.class Lcom/twitter/android/client/ce;
.super Lcom/twitter/library/service/a;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/library/service/a;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/ce;->a:Landroid/content/Context;

    return-void
.end method

.method private a(Lcom/twitter/library/service/e;Lcom/twitter/library/client/Session;)V
    .locals 6

    const v4, 0x7f0f0128    # com.twitter.android.R.string.drafts

    iget-object v0, p0, Lcom/twitter/android/client/ce;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/client/aw;->a(Landroid/content/Context;)Lcom/twitter/android/client/aw;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/library/service/e;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/client/ce;->a:Landroid/content/Context;

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0f02a0    # com.twitter.android.R.string.notif_drafts_sent

    invoke-virtual {v0, p2, v1, v2}, Lcom/twitter/android/client/aw;->b(Lcom/twitter/library/client/Session;Ljava/lang/String;I)V

    :goto_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/aw;->a(Ljava/lang/String;)V

    return-void

    :cond_0
    const-wide/16 v2, 0x0

    iget-object v1, p0, Lcom/twitter/android/client/ce;->a:Landroid/content/Context;

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f0f02b0    # com.twitter.android.R.string.notif_sending_drafts_failed

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/client/aw;->b(Lcom/twitter/library/client/Session;JLjava/lang/String;I)V

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/internal/android/service/a;)V
    .locals 0

    check-cast p1, Lcom/twitter/library/service/b;

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/ce;->a(Lcom/twitter/library/service/b;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/b;)V
    .locals 6

    move-object v0, p1

    check-cast v0, Lcom/twitter/library/api/upload/c;

    invoke-virtual {v0}, Lcom/twitter/library/api/upload/c;->f()Lcom/twitter/library/api/upload/w;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->r()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :pswitch_0
    new-instance v2, Lcom/twitter/android/client/cg;

    iget-object v3, p0, Lcom/twitter/android/client/ce;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/twitter/android/client/cg;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1, v0}, Lcom/twitter/android/client/cg;->a(Lcom/twitter/library/service/b;Lcom/twitter/library/service/e;)V

    :cond_0
    :goto_0
    return-void

    :pswitch_1
    iget-object v1, p0, Lcom/twitter/android/client/ce;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->s()Lcom/twitter/library/service/p;

    move-result-object v2

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    iget-wide v2, v2, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/client/ce;->a(Lcom/twitter/library/service/e;Lcom/twitter/library/client/Session;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
