.class public Lcom/twitter/android/client/z;
.super Lcom/twitter/library/client/e;
.source "Twttr"


# instance fields
.field d:Z

.field e:Z

.field f:I

.field g:I


# direct methods
.method public constructor <init>(Lcom/twitter/android/client/BaseFragmentActivity;)V
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    invoke-direct {p0}, Lcom/twitter/library/client/e;-><init>()V

    iput-boolean v2, p0, Lcom/twitter/android/client/z;->d:Z

    iput-boolean v2, p0, Lcom/twitter/android/client/z;->e:Z

    iput v3, p0, Lcom/twitter/android/client/z;->g:I

    invoke-static {}, Lgu;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    const/16 v1, 0x10

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/client/z;->a(II)V

    :cond_0
    invoke-virtual {p1}, Lcom/twitter/android/client/BaseFragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v3, :cond_1

    iput v2, p0, Lcom/twitter/android/client/z;->f:I

    :goto_0
    const v0, 0x7f030079    # com.twitter.android.R.layout.fragment_list_layout

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/z;->d(I)V

    return-void

    :cond_1
    invoke-static {}, Lcom/twitter/library/util/Util;->f()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lgm;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x3

    iput v0, p0, Lcom/twitter/android/client/z;->f:I

    goto :goto_0

    :cond_3
    invoke-static {}, Lgm;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    iput v2, p0, Lcom/twitter/android/client/z;->f:I

    goto :goto_0

    :cond_4
    iput v3, p0, Lcom/twitter/android/client/z;->f:I

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/twitter/android/client/z;->g:I

    return v0
.end method

.method public final a(I)V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/client/z;->c()V

    iput p1, p0, Lcom/twitter/android/client/z;->f:I

    return-void
.end method

.method public final a(Z)V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/client/z;->c()V

    iput-boolean p1, p0, Lcom/twitter/android/client/z;->d:Z

    return-void
.end method

.method public b(I)V
    .locals 0

    iput p1, p0, Lcom/twitter/android/client/z;->g:I

    return-void
.end method

.method public final b(Z)V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/client/z;->c()V

    iput-boolean p1, p0, Lcom/twitter/android/client/z;->e:Z

    return-void
.end method
