.class public Lcom/twitter/android/client/aw;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/util/ac;
.implements Lcom/twitter/library/util/ar;


# static fields
.field private static a:Lcom/twitter/android/client/aw;


# instance fields
.field private final b:Lcom/twitter/android/client/c;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final c:Landroid/content/Context;

.field private final d:Landroid/app/NotificationManager;

.field private final e:Lcom/twitter/library/client/aa;

.field private final f:Landroid/os/Handler;

.field private final g:Ljava/util/HashMap;

.field private final h:Lcom/twitter/android/client/az;

.field private final i:Lcom/twitter/library/client/z;

.field private final j:Landroid/util/SparseArray;

.field private final k:Ljava/util/HashMap;

.field private final l:Ljava/util/HashMap;

.field private m:Lcom/twitter/library/client/w;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/android/client/aw;->f:Landroid/os/Handler;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/aw;->g:Ljava/util/HashMap;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/aw;->j:Landroid/util/SparseArray;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/aw;->k:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/aw;->l:Ljava/util/HashMap;

    iput-object p1, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    const-string/jumbo v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/twitter/android/client/aw;->d:Landroid/app/NotificationManager;

    invoke-static {p1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/aw;->e:Lcom/twitter/library/client/aa;

    invoke-static {p1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/aw;->b:Lcom/twitter/android/client/c;

    iget-object v0, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/client/w;->a(Landroid/content/Context;)Lcom/twitter/library/client/w;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/aw;->m:Lcom/twitter/library/client/w;

    new-instance v0, Lcom/twitter/android/client/az;

    invoke-direct {v0, p0, v2}, Lcom/twitter/android/client/az;-><init>(Lcom/twitter/android/client/aw;Lcom/twitter/android/client/ax;)V

    iput-object v0, p0, Lcom/twitter/android/client/aw;->h:Lcom/twitter/android/client/az;

    new-instance v0, Lcom/twitter/android/client/ba;

    invoke-direct {v0, p0, v2}, Lcom/twitter/android/client/ba;-><init>(Lcom/twitter/android/client/aw;Lcom/twitter/android/client/ax;)V

    iput-object v0, p0, Lcom/twitter/android/client/aw;->i:Lcom/twitter/library/client/z;

    return-void
.end method

.method private a(ILandroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/twitter/android/client/aw;->a(Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/twitter/android/client/notifications/StatusBarNotif;Lcom/twitter/android/client/bb;Landroid/graphics/Bitmap;)Landroid/support/v4/app/NotificationCompat$Builder;
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Landroid/content/Context;Lcom/twitter/android/client/notifications/StatusBarNotif;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->s()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/twitter/android/client/aw;->b:Lcom/twitter/android/client/c;

    iget-object v2, v2, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    invoke-interface {v2, v3, v1}, Lcom/twitter/library/widget/ap;->a(ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/support/v4/app/NotificationCompat$Builder;

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->A()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lgn;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz p3, :cond_4

    :goto_1
    if-eqz p3, :cond_5

    new-instance v1, Landroid/support/v4/app/NotificationCompat$BigPictureStyle;

    invoke-direct {v1}, Landroid/support/v4/app/NotificationCompat$BigPictureStyle;-><init>()V

    invoke-virtual {v1, p3}, Landroid/support/v4/app/NotificationCompat$BigPictureStyle;->bigPicture(Landroid/graphics/Bitmap;)Landroid/support/v4/app/NotificationCompat$BigPictureStyle;

    move-result-object v1

    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$BigPictureStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$BigPictureStyle;

    move-result-object v1

    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$BigPictureStyle;->setSummaryText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$BigPictureStyle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setStyle(Landroid/support/v4/app/NotificationCompat$Style;)Landroid/support/v4/app/NotificationCompat$Builder;

    :cond_1
    :goto_2
    invoke-direct {p0, p2, v0}, Lcom/twitter/android/client/aw;->a(Lcom/twitter/android/client/bb;Landroid/support/v4/app/NotificationCompat$Builder;)V

    return-object v0

    :cond_2
    iget-object v2, p0, Lcom/twitter/android/client/aw;->k:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/twitter/android/client/aw;->b:Lcom/twitter/android/client/c;

    invoke-virtual {v2, v3, p0}, Lcom/twitter/android/client/c;->a(ILcom/twitter/library/util/ar;)V

    :cond_3
    iget-object v2, p0, Lcom/twitter/android/client/aw;->k:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->k()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/twitter/android/client/aw;->b:Lcom/twitter/android/client/c;

    new-instance v2, Lcom/twitter/library/util/m;

    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->t()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/util/m;)Landroid/graphics/Bitmap;

    move-result-object p3

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lcom/twitter/android/client/aw;->l:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/twitter/android/client/aw;->b:Lcom/twitter/android/client/c;

    invoke-virtual {v1, p0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/util/ac;)V

    :cond_6
    iget-object v1, p0, Lcom/twitter/android/client/aw;->l:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->t()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->k()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2
.end method

.method private a(Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;
    .locals 2

    new-instance v0, Landroid/support/v4/app/NotificationCompat$Builder;

    iget-object v1, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f02024e    # com.twitter.android.R.drawable.ic_stat_twitter

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/client/aw;)Landroid/util/SparseArray;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/aw;->j:Landroid/util/SparseArray;

    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/twitter/android/client/aw;
    .locals 3

    const-class v1, Lcom/twitter/android/client/aw;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/android/client/aw;->a:Lcom/twitter/android/client/aw;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/client/aw;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/twitter/android/client/aw;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/android/client/aw;->a:Lcom/twitter/android/client/aw;

    :cond_0
    sget-object v0, Lcom/twitter/android/client/aw;->a:Lcom/twitter/android/client/aw;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Lcom/twitter/android/client/bb;Landroid/support/v4/app/NotificationCompat$Builder;)V
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p1, Lcom/twitter/android/client/bb;->d:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    :cond_0
    invoke-virtual {p2, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setDefaults(I)Landroid/support/v4/app/NotificationCompat$Builder;

    iget-boolean v0, p1, Lcom/twitter/android/client/bb;->b:Z

    if-eqz v0, :cond_1

    const v0, -0xff0100

    const/16 v1, 0x1f4

    const/16 v2, 0x7d0

    invoke-virtual {p2, v0, v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setLights(III)Landroid/support/v4/app/NotificationCompat$Builder;

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/client/aw;->b:Lcom/twitter/android/client/c;

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    const-string/jumbo v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p1, Lcom/twitter/android/client/bb;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p1, Lcom/twitter/android/client/bb;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {p2, v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setSound(Landroid/net/Uri;I)Landroid/support/v4/app/NotificationCompat$Builder;

    :cond_2
    return-void
.end method

.method private a(Lcom/twitter/android/client/bb;Lcom/twitter/android/client/notifications/StatusBarNotif;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/client/aw;->a(Lcom/twitter/android/client/bb;Lcom/twitter/android/client/notifications/StatusBarNotif;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private a(Lcom/twitter/android/client/bb;Lcom/twitter/android/client/notifications/StatusBarNotif;Landroid/graphics/Bitmap;)V
    .locals 7

    const/4 v0, 0x0

    invoke-virtual {p2}, Lcom/twitter/android/client/notifications/StatusBarNotif;->k()I

    move-result v2

    invoke-virtual {p2}, Lcom/twitter/android/client/notifications/StatusBarNotif;->y()[I

    move-result-object v3

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget v5, v3, v1

    if-eq v5, v2, :cond_0

    iget-object v6, p0, Lcom/twitter/android/client/aw;->j:Landroid/util/SparseArray;

    invoke-virtual {v6, v5}, Landroid/util/SparseArray;->remove(I)V

    iget-object v6, p0, Lcom/twitter/android/client/aw;->d:Landroid/app/NotificationManager;

    invoke-virtual {v6, v5}, Landroid/app/NotificationManager;->cancel(I)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-direct {p0, p2, p1, p3}, Lcom/twitter/android/client/aw;->a(Lcom/twitter/android/client/notifications/StatusBarNotif;Lcom/twitter/android/client/bb;Landroid/graphics/Bitmap;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    sget-object v3, Lcom/twitter/android/client/bb;->a:Lcom/twitter/android/client/bb;

    if-ne p1, v3, :cond_2

    const/4 v0, 0x1

    :cond_2
    if-eqz v0, :cond_3

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    :cond_3
    iget-object v3, p0, Lcom/twitter/android/client/aw;->j:Landroid/util/SparseArray;

    invoke-virtual {v3, v2, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v3, p0, Lcom/twitter/android/client/aw;->e:Lcom/twitter/library/client/aa;

    iget-object v4, p0, Lcom/twitter/android/client/aw;->i:Lcom/twitter/library/client/z;

    invoke-virtual {v3, v4}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/z;)V

    iget-object v3, p0, Lcom/twitter/android/client/aw;->d:Landroid/app/NotificationManager;

    invoke-virtual {v1}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v3, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    if-nez v0, :cond_4

    const-string/jumbo v0, "impression"

    invoke-virtual {p2, v0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_4
    invoke-virtual {p2}, Lcom/twitter/android/client/notifications/StatusBarNotif;->z()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {}, Lgn;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string/jumbo v0, "preview_impression_image_loaded"

    invoke-virtual {p2, v0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_5
    :goto_1
    return-void

    :cond_6
    invoke-virtual {p2}, Lcom/twitter/android/client/notifications/StatusBarNotif;->A()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {}, Lgn;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string/jumbo v0, "preview_impression"

    invoke-virtual {p2, v0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    goto :goto_1
.end method

.method private a(Lcom/twitter/android/client/notifications/StatusBarNotif;Landroid/graphics/Bitmap;)V
    .locals 1

    sget-object v0, Lcom/twitter/android/client/bb;->a:Lcom/twitter/android/client/bb;

    invoke-direct {p0, v0, p1, p2}, Lcom/twitter/android/client/aw;->a(Lcom/twitter/android/client/bb;Lcom/twitter/android/client/notifications/StatusBarNotif;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private a(Lcom/twitter/library/platform/DataSyncResult;Lcom/twitter/android/client/bb;I)V
    .locals 5

    iget-object v0, p1, Lcom/twitter/library/platform/DataSyncResult;->g:Lcom/twitter/library/platform/e;

    iget v1, p1, Lcom/twitter/library/platform/DataSyncResult;->d:I

    if-eqz v0, :cond_0

    iget v2, v0, Lcom/twitter/library/platform/e;->b:I

    if-lez v2, :cond_0

    and-int/lit8 v2, p3, 0x4

    if-eqz v2, :cond_0

    and-int/lit8 v1, v1, 0x4

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p1, Lcom/twitter/library/platform/DataSyncResult;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/client/aw;->e:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/client/aw;->b:Lcom/twitter/android/client/c;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/twitter/android/client/c;->b(Z)Ljava/lang/String;

    :cond_2
    iget-wide v1, v0, Lcom/twitter/library/platform/e;->g:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    new-instance v1, Lcom/twitter/android/client/notifications/MessageNotif;

    iget-wide v2, p1, Lcom/twitter/library/platform/DataSyncResult;->b:J

    iget-object v4, p1, Lcom/twitter/library/platform/DataSyncResult;->a:Ljava/lang/String;

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/twitter/android/client/notifications/MessageNotif;-><init>(Lcom/twitter/library/platform/e;JLjava/lang/String;)V

    invoke-direct {p0, p2, v1}, Lcom/twitter/android/client/aw;->a(Lcom/twitter/android/client/bb;Lcom/twitter/android/client/notifications/StatusBarNotif;)V

    invoke-virtual {v1}, Lcom/twitter/android/client/notifications/MessageNotif;->m()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/platform/e;->p:Ljava/lang/String;

    goto :goto_0
.end method

.method private b(Landroid/os/Bundle;Lcom/twitter/library/client/Session;)Landroid/content/Intent;
    .locals 5

    const/4 v4, 0x1

    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    const-class v3, Lcom/twitter/android/EditProfileActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "failure"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "sb_account_name"

    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "avatar_uri"

    const-string/jumbo v3, "avatar_uri"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "header_uri"

    const-string/jumbo v3, "header_uri"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "crop_rect"

    const-string/jumbo v3, "crop"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/provider/ay;->b:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string/jumbo v3, "ownerId"

    invoke-virtual {v2, v3, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "name"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "desc"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "url"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "place"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const-string/jumbo v1, "update_profile"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "name"

    const-string/jumbo v3, "name"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "description"

    const-string/jumbo v3, "desc"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "url"

    const-string/jumbo v3, "url"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "location"

    const-string/jumbo v3, "place"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/client/aw;)Lcom/twitter/library/client/z;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/aw;->i:Lcom/twitter/library/client/z;

    return-object v0
.end method

.method private b(Lcom/twitter/library/platform/DataSyncResult;Lcom/twitter/android/client/bb;I)V
    .locals 5

    iget-object v0, p1, Lcom/twitter/library/platform/DataSyncResult;->j:Lcom/twitter/library/platform/e;

    iget v1, p1, Lcom/twitter/library/platform/DataSyncResult;->d:I

    if-eqz v0, :cond_0

    iget v2, v0, Lcom/twitter/library/platform/e;->b:I

    if-lez v2, :cond_0

    and-int/lit8 v2, p3, 0x8

    if-eqz v2, :cond_0

    and-int/lit8 v1, v1, 0x40

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Lcom/twitter/android/client/notifications/LoginVerificationNotif;

    iget-wide v2, p1, Lcom/twitter/library/platform/DataSyncResult;->b:J

    iget-object v4, p1, Lcom/twitter/library/platform/DataSyncResult;->a:Ljava/lang/String;

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/twitter/android/client/notifications/LoginVerificationNotif;-><init>(Lcom/twitter/library/platform/e;JLjava/lang/String;)V

    invoke-direct {p0, p2, v1}, Lcom/twitter/android/client/aw;->a(Lcom/twitter/android/client/bb;Lcom/twitter/android/client/notifications/StatusBarNotif;)V

    invoke-virtual {v1}, Lcom/twitter/android/client/notifications/LoginVerificationNotif;->m()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/platform/e;->p:Ljava/lang/String;

    goto :goto_0
.end method

.method private c(Landroid/os/Bundle;Lcom/twitter/library/client/Session;)Landroid/content/Intent;
    .locals 11

    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    const-string/jumbo v3, "username"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "impression_id"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "earned"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    const-string/jumbo v6, "age_before_timestamp"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    new-instance v8, Landroid/content/Intent;

    iget-object v9, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    const-class v10, Lcom/twitter/android/AgeGateActivity;

    invoke-direct {v8, v9, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v9, "user_id"

    invoke-virtual {v8, v9, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "user_name"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "impression_id"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "is_earned"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "age_gate_timestamp"

    invoke-virtual {v1, v2, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/provider/ay;->b:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string/jumbo v3, "ownerId"

    invoke-virtual {v2, v3, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/client/aw;)Lcom/twitter/library/client/aa;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/aw;->e:Lcom/twitter/library/client/aa;

    return-object v0
.end method

.method private c(Lcom/twitter/library/platform/DataSyncResult;Lcom/twitter/android/client/bb;I)V
    .locals 9

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v7, p1, Lcom/twitter/library/platform/DataSyncResult;->i:Lcom/twitter/library/platform/e;

    iget v2, p1, Lcom/twitter/library/platform/DataSyncResult;->d:I

    and-int/lit8 v3, v2, 0x2

    if-eqz v3, :cond_2

    move v6, v0

    :goto_0
    and-int/lit8 v3, v2, 0x8

    if-eqz v3, :cond_3

    move v5, v0

    :goto_1
    and-int/lit8 v3, v2, 0x10

    if-eqz v3, :cond_4

    move v4, v0

    :goto_2
    and-int/lit8 v3, v2, 0x20

    if-eqz v3, :cond_5

    move v3, v0

    :goto_3
    and-int/lit16 v2, v2, 0x200

    if-eqz v2, :cond_6

    move v2, v0

    :goto_4
    and-int/lit8 v8, p3, 0x2

    if-eqz v8, :cond_7

    if-nez v6, :cond_0

    if-nez v5, :cond_0

    if-nez v4, :cond_0

    if-nez v3, :cond_0

    if-eqz v2, :cond_7

    :cond_0
    :goto_5
    if-eqz v7, :cond_1

    iget v1, v7, Lcom/twitter/library/platform/e;->b:I

    if-lez v1, :cond_1

    and-int/lit8 v1, p3, 0x2

    if-eqz v1, :cond_1

    if-nez v0, :cond_8

    :cond_1
    :goto_6
    return-void

    :cond_2
    move v6, v1

    goto :goto_0

    :cond_3
    move v5, v1

    goto :goto_1

    :cond_4
    move v4, v1

    goto :goto_2

    :cond_5
    move v3, v1

    goto :goto_3

    :cond_6
    move v2, v1

    goto :goto_4

    :cond_7
    move v0, v1

    goto :goto_5

    :cond_8
    if-eqz v6, :cond_9

    iget v0, p1, Lcom/twitter/library/platform/DataSyncResult;->e:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_9

    new-instance v0, Lcom/twitter/android/client/notifications/MentionNotif;

    iget-wide v1, p1, Lcom/twitter/library/platform/DataSyncResult;->b:J

    iget-object v3, p1, Lcom/twitter/library/platform/DataSyncResult;->a:Ljava/lang/String;

    invoke-direct {v0, v7, v1, v2, v3}, Lcom/twitter/android/client/notifications/MentionNotif;-><init>(Lcom/twitter/library/platform/e;JLjava/lang/String;)V

    :goto_7
    if-eqz v0, :cond_1

    invoke-direct {p0, p2, v0}, Lcom/twitter/android/client/aw;->a(Lcom/twitter/android/client/bb;Lcom/twitter/android/client/notifications/StatusBarNotif;)V

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->m()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/twitter/library/platform/e;->p:Ljava/lang/String;

    goto :goto_6

    :cond_9
    if-eqz v4, :cond_a

    iget v0, p1, Lcom/twitter/library/platform/DataSyncResult;->e:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_a

    new-instance v0, Lcom/twitter/android/client/notifications/FavoriteNotif;

    iget-wide v1, p1, Lcom/twitter/library/platform/DataSyncResult;->b:J

    iget-object v3, p1, Lcom/twitter/library/platform/DataSyncResult;->a:Ljava/lang/String;

    invoke-direct {v0, v7, v1, v2, v3}, Lcom/twitter/android/client/notifications/FavoriteNotif;-><init>(Lcom/twitter/library/platform/e;JLjava/lang/String;)V

    goto :goto_7

    :cond_a
    if-eqz v5, :cond_b

    iget v0, p1, Lcom/twitter/library/platform/DataSyncResult;->e:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_b

    new-instance v0, Lcom/twitter/android/client/notifications/RetweetNotif;

    iget-wide v1, p1, Lcom/twitter/library/platform/DataSyncResult;->b:J

    iget-object v3, p1, Lcom/twitter/library/platform/DataSyncResult;->a:Ljava/lang/String;

    invoke-direct {v0, v7, v1, v2, v3}, Lcom/twitter/android/client/notifications/RetweetNotif;-><init>(Lcom/twitter/library/platform/e;JLjava/lang/String;)V

    goto :goto_7

    :cond_b
    if-eqz v3, :cond_c

    iget v0, p1, Lcom/twitter/library/platform/DataSyncResult;->e:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_c

    new-instance v0, Lcom/twitter/android/client/notifications/FollowNotif;

    iget-wide v1, p1, Lcom/twitter/library/platform/DataSyncResult;->b:J

    iget-object v3, p1, Lcom/twitter/library/platform/DataSyncResult;->a:Ljava/lang/String;

    invoke-direct {v0, v7, v1, v2, v3}, Lcom/twitter/android/client/notifications/FollowNotif;-><init>(Lcom/twitter/library/platform/e;JLjava/lang/String;)V

    goto :goto_7

    :cond_c
    if-eqz v3, :cond_d

    iget v0, p1, Lcom/twitter/library/platform/DataSyncResult;->e:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_d

    new-instance v0, Lcom/twitter/android/client/notifications/FollowRequestNotif;

    iget-wide v1, p1, Lcom/twitter/library/platform/DataSyncResult;->b:J

    iget-object v3, p1, Lcom/twitter/library/platform/DataSyncResult;->a:Ljava/lang/String;

    invoke-direct {v0, v7, v1, v2, v3}, Lcom/twitter/android/client/notifications/FollowRequestNotif;-><init>(Lcom/twitter/library/platform/e;JLjava/lang/String;)V

    goto :goto_7

    :cond_d
    if-eqz v2, :cond_e

    iget v0, p1, Lcom/twitter/library/platform/DataSyncResult;->e:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_e

    new-instance v0, Lcom/twitter/android/client/notifications/MediaTagNotif;

    iget-wide v1, p1, Lcom/twitter/library/platform/DataSyncResult;->b:J

    iget-object v3, p1, Lcom/twitter/library/platform/DataSyncResult;->a:Ljava/lang/String;

    invoke-direct {v0, v7, v1, v2, v3}, Lcom/twitter/android/client/notifications/MediaTagNotif;-><init>(Lcom/twitter/library/platform/e;JLjava/lang/String;)V

    goto :goto_7

    :cond_e
    const/4 v0, 0x0

    goto :goto_7
.end method

.method static synthetic d(Lcom/twitter/android/client/aw;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/aw;->b:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method private d(Lcom/twitter/library/platform/DataSyncResult;Lcom/twitter/android/client/bb;I)V
    .locals 5

    iget-object v0, p1, Lcom/twitter/library/platform/DataSyncResult;->f:Lcom/twitter/library/platform/e;

    iget v1, p1, Lcom/twitter/library/platform/DataSyncResult;->d:I

    if-eqz v0, :cond_0

    iget v2, v0, Lcom/twitter/library/platform/e;->b:I

    if-lez v2, :cond_0

    and-int/lit8 v2, p3, 0x1

    if-eqz v2, :cond_0

    and-int/lit8 v1, v1, 0x1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Lcom/twitter/android/client/notifications/DeviceTweetNotif;

    iget-wide v2, p1, Lcom/twitter/library/platform/DataSyncResult;->b:J

    iget-object v4, p1, Lcom/twitter/library/platform/DataSyncResult;->a:Ljava/lang/String;

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/twitter/android/client/notifications/DeviceTweetNotif;-><init>(Lcom/twitter/library/platform/e;JLjava/lang/String;)V

    invoke-direct {p0, p2, v1}, Lcom/twitter/android/client/aw;->a(Lcom/twitter/android/client/bb;Lcom/twitter/android/client/notifications/StatusBarNotif;)V

    invoke-virtual {v1}, Lcom/twitter/android/client/notifications/DeviceTweetNotif;->m()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/platform/e;->p:Ljava/lang/String;

    goto :goto_0
.end method

.method private e(Ljava/lang/String;)Lcom/twitter/android/client/bb;
    .locals 9

    const/4 v3, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    iget-object v0, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/provider/i;->a:Landroid/net/Uri;

    invoke-static {v1, p1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v4, "vibrate"

    aput-object v4, v2, v7

    const-string/jumbo v4, "ringtone"

    aput-object v4, v2, v6

    const-string/jumbo v4, "light"

    aput-object v4, v2, v8

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    sget-object v1, Lcom/twitter/library/provider/i;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2, v7}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v6, :cond_2

    move v0, v6

    :goto_0
    invoke-interface {v2, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    :cond_0
    invoke-interface {v2, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-interface {v2, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-ne v3, v6, :cond_3

    :cond_1
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :goto_2
    new-instance v2, Lcom/twitter/android/client/bb;

    invoke-direct {v2, v6, v1, v0}, Lcom/twitter/android/client/bb;-><init>(ZLjava/lang/String;Z)V

    return-object v2

    :cond_2
    move v0, v7

    goto :goto_0

    :cond_3
    move v6, v7

    goto :goto_1

    :cond_4
    move v0, v6

    goto :goto_0

    :cond_5
    move v0, v6

    goto :goto_1

    :cond_6
    move v0, v6

    goto :goto_2
.end method

.method private e(Lcom/twitter/library/platform/DataSyncResult;Lcom/twitter/android/client/bb;I)V
    .locals 5

    iget-object v0, p1, Lcom/twitter/library/platform/DataSyncResult;->k:Lcom/twitter/library/platform/e;

    iget v1, p1, Lcom/twitter/library/platform/DataSyncResult;->d:I

    if-eqz v0, :cond_0

    iget v2, v0, Lcom/twitter/library/platform/e;->b:I

    if-lez v2, :cond_0

    and-int/lit8 v2, p3, 0x10

    if-eqz v2, :cond_0

    and-int/lit16 v1, v1, 0x80

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Lcom/twitter/android/client/notifications/LifelineTweetNotif;

    iget-wide v2, p1, Lcom/twitter/library/platform/DataSyncResult;->b:J

    iget-object v4, p1, Lcom/twitter/library/platform/DataSyncResult;->a:Ljava/lang/String;

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/twitter/android/client/notifications/LifelineTweetNotif;-><init>(Lcom/twitter/library/platform/e;JLjava/lang/String;)V

    invoke-direct {p0, p2, v1}, Lcom/twitter/android/client/aw;->a(Lcom/twitter/android/client/bb;Lcom/twitter/android/client/notifications/StatusBarNotif;)V

    invoke-virtual {v1}, Lcom/twitter/android/client/notifications/LifelineTweetNotif;->m()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/platform/e;->p:Ljava/lang/String;

    goto :goto_0
.end method

.method private f(Ljava/lang/String;)I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/aw;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/16 v0, 0x3f

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method private f(Lcom/twitter/library/platform/DataSyncResult;Lcom/twitter/android/client/bb;I)V
    .locals 5

    iget-object v0, p1, Lcom/twitter/library/platform/DataSyncResult;->l:Lcom/twitter/library/platform/e;

    iget v1, p1, Lcom/twitter/library/platform/DataSyncResult;->d:I

    if-eqz v0, :cond_0

    iget v2, v0, Lcom/twitter/library/platform/e;->b:I

    if-lez v2, :cond_0

    and-int/lit8 v2, p3, 0x20

    if-eqz v2, :cond_0

    and-int/lit16 v1, v1, 0x100

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Lcom/twitter/android/client/notifications/GenericNotif;

    iget-wide v2, p1, Lcom/twitter/library/platform/DataSyncResult;->b:J

    iget-object v4, p1, Lcom/twitter/library/platform/DataSyncResult;->a:Ljava/lang/String;

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/twitter/android/client/notifications/GenericNotif;-><init>(Lcom/twitter/library/platform/e;JLjava/lang/String;)V

    const/4 v2, 0x0

    iput-boolean v2, p2, Lcom/twitter/android/client/bb;->d:Z

    invoke-direct {p0, p2, v1}, Lcom/twitter/android/client/aw;->a(Lcom/twitter/android/client/bb;Lcom/twitter/android/client/notifications/StatusBarNotif;)V

    invoke-virtual {v1}, Lcom/twitter/android/client/notifications/GenericNotif;->m()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/platform/e;->p:Ljava/lang/String;

    goto :goto_0
.end method

.method private g(Ljava/lang/String;)Lcom/twitter/library/service/n;
    .locals 4

    new-instance v0, Lcom/twitter/library/service/n;

    iget-object v1, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/aw;->e:Lcom/twitter/library/client/aa;

    invoke-virtual {v2, p1}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/twitter/library/service/n;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/client/aw;->h:Lcom/twitter/android/client/az;

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/n;->a(Lcom/twitter/internal/android/service/m;)Lcom/twitter/internal/android/service/a;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/DialogActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "ff"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x14000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v1

    const v2, 0x7f0f0391    # com.twitter.android.R.string.scan_contacts_label

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v3, v0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f020242    # com.twitter.android.R.drawable.ic_stat_follow

    invoke-virtual {v3, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    const v3, 0x7f0f0390    # com.twitter.android.R.string.scan_contacts_item

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    invoke-static {v0, v5, v1, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/aw;->d:Landroid/app/NotificationManager;

    const/16 v2, 0x3e8

    invoke-virtual {v1, v2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    iget-object v0, p0, Lcom/twitter/android/client/aw;->b:Lcom/twitter/android/client/c;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->p(J)V

    return-void
.end method

.method a(I)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-static {v0, v2, v1, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/client/aw;->a(ILandroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setOngoing(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/aw;->d:Landroid/app/NotificationManager;

    const/16 v2, 0x3ec

    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method

.method public a(J)V
    .locals 3

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/client/aw;->d:Landroid/app/NotificationManager;

    const/16 v2, 0x3ea

    invoke-virtual {v1, v0, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(Landroid/os/Bundle;Lcom/twitter/library/client/Session;)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    const v1, 0x7f0f001f    # com.twitter.android.R.string.age_gating_required

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/client/aw;->c(Landroid/os/Bundle;Lcom/twitter/library/client/Session;)Landroid/content/Intent;

    move-result-object v3

    const/high16 v4, 0x10000000

    invoke-static {v0, v2, v3, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/twitter/android/client/aw;->a(Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/aw;->d:Landroid/app/NotificationManager;

    const/16 v2, 0x3ef

    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method

.method a(Lcom/twitter/android/client/notifications/StatusBarNotif;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/client/aw;->a(Lcom/twitter/android/client/notifications/StatusBarNotif;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/LoginActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "screen_name"

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v5, v1, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f0341    # com.twitter.android.R.string.re_login_title

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/twitter/android/client/aw;->a(Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    const v2, 0x7f0f0340    # com.twitter.android.R.string.re_login_body

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setPriority(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/aw;->d:Landroid/app/NotificationManager;

    const/16 v2, 0x3f0

    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;JLjava/lang/String;I)V
    .locals 7

    const/4 v4, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    invoke-virtual {v0, p5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v2, v0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f02024e    # com.twitter.android.R.drawable.ic_stat_twitter

    invoke-virtual {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, p4}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-static {v0, v6, v2, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    const/16 v2, 0x64

    invoke-virtual {v1, v6, v2, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setProgress(IIZ)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setOngoing(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/client/aw;->b:Lcom/twitter/android/client/c;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/api/TwitterUser;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Lcom/twitter/android/client/c;->a(ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/support/v4/app/NotificationCompat$Builder;

    :cond_0
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/twitter/android/client/AppService;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "com.twitter.android.abort."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "action_type"

    const-string/jumbo v4, "ABORT"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "owner_id"

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "android.intent.extra.TEXT"

    invoke-virtual {v2, v3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "status_id"

    invoke-virtual {v2, v3, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v2

    const v3, 0x7f0200e7    # com.twitter.android.R.drawable.ic_action_dismiss

    const v4, 0x7f0f0089    # com.twitter.android.R.string.cancel

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v0, v6, v2, v5}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v1, v3, v4, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    iget-object v0, p0, Lcom/twitter/android/client/aw;->d:Landroid/app/NotificationManager;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x3e9

    invoke-virtual {v1}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;I)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    invoke-virtual {v0, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v2, v0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f02024e    # com.twitter.android.R.drawable.ic_stat_twitter

    invoke-virtual {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-static {v0, v4, v2, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    const/16 v1, 0x64

    invoke-virtual {v0, v4, v1, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setProgress(IIZ)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setOngoing(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/aw;->b:Lcom/twitter/android/client/c;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/api/TwitterUser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v5, v2}, Lcom/twitter/android/client/c;->a(ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/support/v4/app/NotificationCompat$Builder;

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/client/aw;->d:Landroid/app/NotificationManager;

    const/16 v2, 0x3e9

    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method

.method public a(Lcom/twitter/library/platform/DataSyncResult;)V
    .locals 10

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/twitter/library/platform/DataSyncResult;->a:Ljava/lang/String;

    iget v7, p1, Lcom/twitter/library/platform/DataSyncResult;->d:I

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    if-nez v7, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, v2}, Lcom/twitter/android/client/aw;->f(Ljava/lang/String;)I

    move-result v8

    invoke-direct {p0, v2}, Lcom/twitter/android/client/aw;->e(Ljava/lang/String;)Lcom/twitter/android/client/bb;

    move-result-object v2

    invoke-direct {p0, p1, v2, v8}, Lcom/twitter/android/client/aw;->a(Lcom/twitter/library/platform/DataSyncResult;Lcom/twitter/android/client/bb;I)V

    invoke-direct {p0, p1, v2, v8}, Lcom/twitter/android/client/aw;->b(Lcom/twitter/library/platform/DataSyncResult;Lcom/twitter/android/client/bb;I)V

    invoke-direct {p0, p1, v2, v8}, Lcom/twitter/android/client/aw;->c(Lcom/twitter/library/platform/DataSyncResult;Lcom/twitter/android/client/bb;I)V

    invoke-direct {p0, p1, v2, v8}, Lcom/twitter/android/client/aw;->d(Lcom/twitter/library/platform/DataSyncResult;Lcom/twitter/android/client/bb;I)V

    invoke-direct {p0, p1, v2, v8}, Lcom/twitter/android/client/aw;->e(Lcom/twitter/library/platform/DataSyncResult;Lcom/twitter/android/client/bb;I)V

    invoke-direct {p0, p1, v2, v8}, Lcom/twitter/android/client/aw;->f(Lcom/twitter/library/platform/DataSyncResult;Lcom/twitter/android/client/bb;I)V

    and-int/lit8 v2, v7, 0x2

    if-eqz v2, :cond_4

    move v6, v0

    :goto_1
    and-int/lit8 v2, v7, 0x8

    if-eqz v2, :cond_5

    move v5, v0

    :goto_2
    and-int/lit8 v2, v7, 0x10

    if-eqz v2, :cond_6

    move v4, v0

    :goto_3
    and-int/lit8 v2, v7, 0x20

    if-eqz v2, :cond_7

    move v3, v0

    :goto_4
    and-int/lit16 v2, v7, 0x200

    if-eqz v2, :cond_8

    move v2, v0

    :goto_5
    and-int/lit8 v9, v8, 0x2

    if-eqz v9, :cond_9

    if-nez v6, :cond_2

    if-nez v5, :cond_2

    if-nez v4, :cond_2

    if-nez v3, :cond_2

    if-eqz v2, :cond_9

    :cond_2
    move v2, v0

    :goto_6
    and-int/lit8 v3, v8, 0x4

    if-eqz v3, :cond_a

    and-int/lit8 v3, v7, 0x4

    if-eqz v3, :cond_a

    :goto_7
    if-nez v2, :cond_3

    if-eqz v0, :cond_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/client/aw;->b:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/client/aw;->e:Lcom/twitter/library/client/aa;

    iget-object v2, p1, Lcom/twitter/library/platform/DataSyncResult;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->f(Lcom/twitter/library/client/Session;)V

    goto :goto_0

    :cond_4
    move v6, v1

    goto :goto_1

    :cond_5
    move v5, v1

    goto :goto_2

    :cond_6
    move v4, v1

    goto :goto_3

    :cond_7
    move v3, v1

    goto :goto_4

    :cond_8
    move v2, v1

    goto :goto_5

    :cond_9
    move v2, v1

    goto :goto_6

    :cond_a
    move v0, v1

    goto :goto_7
.end method

.method public a(Lcom/twitter/library/util/aa;Ljava/util/HashMap;)V
    .locals 6

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/twitter/android/client/aw;->l:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v1, Lcom/twitter/library/util/m;

    invoke-direct {v1, v0}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/util/ae;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/client/aw;->l:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v5, p0, Lcom/twitter/android/client/aw;->j:Landroid/util/SparseArray;

    invoke-virtual {v5, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/client/notifications/StatusBarNotif;

    if-eqz v2, :cond_1

    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Lcom/twitter/android/client/notifications/StatusBarNotif;->a(Z)V

    iget-object v1, v1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-direct {p0, v2, v1}, Lcom/twitter/android/client/aw;->a(Lcom/twitter/android/client/notifications/StatusBarNotif;Landroid/graphics/Bitmap;)V

    :cond_1
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/client/aw;->l:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/client/aw;->l:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/client/aw;->b:Lcom/twitter/android/client/c;

    invoke-virtual {v0, p0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/util/ac;)V

    :cond_4
    return-void
.end method

.method public a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;)V
    .locals 5

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/twitter/android/client/aw;->k:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/util/ae;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/client/aw;->k:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v4, p0, Lcom/twitter/android/client/aw;->j:Landroid/util/SparseArray;

    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/client/notifications/StatusBarNotif;

    if-eqz v1, :cond_1

    invoke-virtual {p0, v1}, Lcom/twitter/android/client/aw;->a(Lcom/twitter/android/client/notifications/StatusBarNotif;)V

    :cond_1
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/client/aw;->k:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/client/aw;->k:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/client/aw;->b:Lcom/twitter/android/client/c;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Lcom/twitter/android/client/c;->b(ILcom/twitter/library/util/ar;)V

    :cond_4
    return-void
.end method

.method a(Ljava/io/File;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    const-string/jumbo v2, "application/vnd.android.package-archive"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    invoke-static {v1, v3, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const-string/jumbo v1, "New update available"

    invoke-direct {p0, v1, v0}, Lcom/twitter/android/client/aw;->a(Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    const-string/jumbo v1, "Tap to install"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setOngoing(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setPriority(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/aw;->d:Landroid/app/NotificationManager;

    const-string/jumbo v2, "DebugNotifications"

    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/aw;->d:Landroid/app/NotificationManager;

    const/16 v1, 0x3e9

    invoke-virtual {v0, p1, v1}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/aw;->g:Ljava/util/HashMap;

    invoke-direct {p0, p1}, Lcom/twitter/android/client/aw;->f(Ljava/lang/String;)I

    move-result v1

    or-int/2addr v1, p2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/twitter/library/util/a;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/library/provider/f;->a(Landroid/content/Context;)Lcom/twitter/library/provider/f;

    move-result-object v1

    const-string/jumbo v2, "notif_tweet"

    invoke-virtual {v1, p1, v2, v3}, Lcom/twitter/library/provider/f;->a(Ljava/lang/String;Ljava/lang/String;I)I

    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/library/platform/PushService;->e(Landroid/content/Context;)V

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/library/platform/PushService;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/twitter/library/platform/PushService;->b(Landroid/content/Context;Landroid/accounts/Account;)I

    move-result v1

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->g:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v2, v3}, Lcom/twitter/library/provider/NotificationSetting;->a(I)I

    move-result v2

    or-int/2addr v1, v2

    iget-object v2, p0, Lcom/twitter/android/client/aw;->b:Lcom/twitter/android/client/c;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/android/client/c;->a(Landroid/accounts/Account;I)Ljava/lang/String;

    :cond_1
    return-void
.end method

.method a(ZILandroid/os/Bundle;Lcom/twitter/library/client/Session;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/client/aw;->d:Landroid/app/NotificationManager;

    const/16 v1, 0x3ec

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-static {v1, v4, v2, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-direct {p0, p2, v1}, Lcom/twitter/android/client/aw;->a(ILandroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    const/16 v2, 0x3ed

    invoke-virtual {v1}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    iget-object v1, p0, Lcom/twitter/android/client/aw;->f:Landroid/os/Handler;

    new-instance v2, Lcom/twitter/android/client/ay;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/client/ay;-><init>(Lcom/twitter/android/client/aw;Landroid/app/NotificationManager;)V

    const-wide/16 v3, 0x3e8

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p4}, Lcom/twitter/library/client/Session;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p4}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    invoke-direct {p0, p3, p4}, Lcom/twitter/android/client/aw;->b(Landroid/os/Bundle;Lcom/twitter/library/client/Session;)Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x10000000

    invoke-static {v1, v4, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-direct {p0, p2, v1}, Lcom/twitter/android/client/aw;->a(ILandroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    const/16 v2, 0x3ee

    invoke-virtual {v1}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method

.method public a([ILjava/lang/String;)V
    .locals 2

    invoke-direct {p0, p2}, Lcom/twitter/android/client/aw;->g(Ljava/lang/String;)Lcom/twitter/library/service/n;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/library/service/n;->a([I)Lcom/twitter/library/service/n;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/n;->a(I)Lcom/twitter/library/service/n;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/aw;->m:Lcom/twitter/library/client/w;

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    return-void
.end method

.method public b(Lcom/twitter/library/client/Session;JLjava/lang/String;I)V
    .locals 7

    const/4 v4, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/DraftsActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "sb_account_name"

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "return_to_drafts"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v6, v1, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-direct {p0, p5, v1}, Lcom/twitter/android/client/aw;->a(ILandroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, p4}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/client/aw;->b:Lcom/twitter/android/client/c;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/api/TwitterUser;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Lcom/twitter/android/client/c;->a(ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/support/v4/app/NotificationCompat$Builder;

    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-eqz v2, :cond_1

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/twitter/android/client/AppService;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "com.twitter.android.resend."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "action_type"

    const-string/jumbo v4, "RESEND"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "owner_id"

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "android.intent.extra.TEXT"

    invoke-virtual {v2, v3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "status_id"

    invoke-virtual {v2, v3, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v2

    const v3, 0x7f0200ff    # com.twitter.android.R.drawable.ic_action_retry

    const v4, 0x7f0f029e    # com.twitter.android.R.string.notif_action_retry

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v0, v6, v2, v5}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v1, v3, v4, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/client/aw;->d:Landroid/app/NotificationManager;

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x3ea

    invoke-virtual {v1}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    return-void
.end method

.method public b(Lcom/twitter/library/client/Session;Ljava/lang/String;I)V
    .locals 5

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-static {v0, v2, v1, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-direct {p0, p3, v0}, Lcom/twitter/android/client/aw;->a(ILandroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/aw;->b:Lcom/twitter/android/client/c;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/api/TwitterUser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Lcom/twitter/android/client/c;->a(ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/support/v4/app/NotificationCompat$Builder;

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/client/aw;->d:Landroid/app/NotificationManager;

    const/16 v2, 0x3eb

    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    iget-object v0, p0, Lcom/twitter/android/client/aw;->f:Landroid/os/Handler;

    new-instance v2, Lcom/twitter/android/client/ax;

    invoke-direct {v2, p0, v1}, Lcom/twitter/android/client/ax;-><init>(Lcom/twitter/android/client/aw;Landroid/app/NotificationManager;)V

    const-wide/16 v3, 0x3e8

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/twitter/library/util/a;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/library/provider/f;->a(Landroid/content/Context;)Lcom/twitter/library/provider/f;

    move-result-object v1

    const-string/jumbo v2, "notif_tweet"

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, v3}, Lcom/twitter/library/provider/f;->a(Ljava/lang/String;Ljava/lang/String;I)I

    iget-object v1, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/library/platform/PushService;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/client/aw;->c:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/twitter/library/platform/PushService;->b(Landroid/content/Context;Landroid/accounts/Account;)I

    move-result v1

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->g:Lcom/twitter/library/provider/NotificationSetting;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/twitter/library/provider/NotificationSetting;->a(I)I

    move-result v2

    xor-int/lit8 v2, v2, -0x1

    and-int/2addr v1, v2

    iget-object v2, p0, Lcom/twitter/android/client/aw;->b:Lcom/twitter/android/client/c;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/android/client/c;->a(Landroid/accounts/Account;I)Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;I)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/aw;->g:Ljava/util/HashMap;

    invoke-direct {p0, p1}, Lcom/twitter/android/client/aw;->f(Ljava/lang/String;)I

    move-result v1

    xor-int/lit8 v2, p2, -0x1

    and-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/twitter/android/client/aw;->g(Ljava/lang/String;)Lcom/twitter/library/service/n;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/n;->a(I)Lcom/twitter/library/service/n;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/aw;->m:Lcom/twitter/library/client/w;

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/twitter/android/client/aw;->g(Ljava/lang/String;)Lcom/twitter/library/service/n;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/n;->a(I)Lcom/twitter/library/service/n;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/aw;->m:Lcom/twitter/library/client/w;

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    return-void
.end method
