.class public Lcom/twitter/android/client/NotificationService;
.super Landroid/app/Service;
.source "Twttr"


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field public static final d:Ljava/lang/String;

.field public static final e:Ljava/lang/String;

.field public static final f:Ljava/lang/String;

.field public static final g:Ljava/lang/String;

.field private static final h:Ljava/util/HashMap;


# instance fields
.field private i:Lcom/twitter/android/client/bd;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x2

    const-string/jumbo v0, ".notif.dismiss"

    invoke-static {v0}, Lcom/twitter/library/client/App;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/client/NotificationService;->a:Ljava/lang/String;

    const-string/jumbo v0, ".notif.undo"

    invoke-static {v0}, Lcom/twitter/library/client/App;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/client/NotificationService;->b:Ljava/lang/String;

    const-string/jumbo v0, ".notif.retweet"

    invoke-static {v0}, Lcom/twitter/library/client/App;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/client/NotificationService;->c:Ljava/lang/String;

    const-string/jumbo v0, ".notif.favorite"

    invoke-static {v0}, Lcom/twitter/library/client/App;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/client/NotificationService;->d:Ljava/lang/String;

    const-string/jumbo v0, ".notif.follow"

    invoke-static {v0}, Lcom/twitter/library/client/App;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/client/NotificationService;->e:Ljava/lang/String;

    const-string/jumbo v0, ".notif.follow.accept"

    invoke-static {v0}, Lcom/twitter/library/client/App;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/client/NotificationService;->f:Ljava/lang/String;

    const-string/jumbo v0, ".notif.follow.decline"

    invoke-static {v0}, Lcom/twitter/library/client/App;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/client/NotificationService;->g:Ljava/lang/String;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/twitter/android/client/NotificationService;->h:Ljava/util/HashMap;

    sget-object v0, Lcom/twitter/android/client/NotificationService;->h:Ljava/util/HashMap;

    sget-object v1, Lcom/twitter/android/client/NotificationService;->a:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/client/NotificationService;->h:Ljava/util/HashMap;

    sget-object v1, Lcom/twitter/android/client/NotificationService;->b:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/client/NotificationService;->h:Ljava/util/HashMap;

    sget-object v1, Lcom/twitter/android/client/NotificationService;->c:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/client/NotificationService;->h:Ljava/util/HashMap;

    sget-object v1, Lcom/twitter/android/client/NotificationService;->d:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/client/NotificationService;->h:Ljava/util/HashMap;

    sget-object v1, Lcom/twitter/android/client/NotificationService;->e:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/client/NotificationService;->h:Ljava/util/HashMap;

    sget-object v1, Lcom/twitter/android/client/NotificationService;->f:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/client/NotificationService;->h:Ljava/util/HashMap;

    sget-object v1, Lcom/twitter/android/client/NotificationService;->g:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 6

    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v2

    const-string/jumbo v0, "sb_account_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    const-string/jumbo v4, "log_actions_experiment"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {p0, v0, v1}, Lju;->b(Landroid/content/Context;J)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "android_notification_actions_v2_1718"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    :cond_0
    const-string/jumbo v0, "sb_notification"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/notifications/StatusBarNotif;

    const-string/jumbo v1, "android_push_priority_1635"

    invoke-static {v1}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v1, "android_push_notification_disaggregation_1753"

    invoke-static {v1}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v1, "notif_scribe_log"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/scribe/ScribeLog;

    if-eqz v1, :cond_1

    invoke-virtual {v2, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_1
    invoke-static {}, Lgn;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v1, "notif_scribe_log_for_preview_experiment"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/scribe/ScribeLog;

    if-eqz v1, :cond_2

    invoke-virtual {v2, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_2
    invoke-static {p0}, Lcom/twitter/android/client/aw;->a(Landroid/content/Context;)Lcom/twitter/android/client/aw;

    move-result-object v1

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->y()[I

    move-result-object v0

    invoke-virtual {v1, v0, v3}, Lcom/twitter/android/client/aw;->a([ILjava/lang/String;)V

    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/twitter/android/client/notifications/StatusBarNotif;Ljava/lang/String;ILcom/twitter/library/scribe/ScribeLog;)V
    .locals 5

    invoke-virtual {p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;->k()I

    move-result v1

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/client/NotificationService;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v2, Lcom/twitter/android/client/NotificationService;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sget-object v2, Lcom/twitter/library/provider/an;->a:Landroid/net/Uri;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "sb_notification"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "notif_scribe_log"

    invoke-virtual {v0, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const/4 v2, 0x0

    const/high16 v3, 0x10000000

    invoke-static {p0, v2, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v2, Landroid/widget/RemoteViews;

    invoke-static {}, Lcom/twitter/library/client/App;->j()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0300d5    # com.twitter.android.R.layout.notification_undo

    invoke-direct {v2, v3, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    const v3, 0x7f0901fd    # com.twitter.android.R.id.notif_undo

    invoke-virtual {v2, v3, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    const v0, 0x7f09001e    # com.twitter.android.R.id.text

    invoke-virtual {v2, v0, p2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v0, 0x7f09001f    # com.twitter.android.R.id.icon

    invoke-virtual {v2, v0, p3}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    new-instance v0, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v0, p0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p3}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->setContent(Landroid/widget/RemoteViews;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    const-string/jumbo v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v2}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/client/NotificationService;Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/client/NotificationService;->b(Landroid/content/Context;Landroid/os/Bundle;)V

    return-void
.end method

.method private b(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 4

    const/4 v1, -0x1

    const-string/jumbo v0, "action_code"

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v1

    const-string/jumbo v2, "sb_account_name"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-static {p1}, Lcom/twitter/library/client/w;->a(Landroid/content/Context;)Lcom/twitter/library/client/w;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/client/u;

    invoke-direct {v3, p1, v1}, Lcom/twitter/library/client/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/api/TwitterUser;)Lcom/twitter/library/client/u;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/u;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/library/service/b;->c(Landroid/os/Bundle;)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-static {p1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/android/client/c;->i:Lcom/twitter/library/service/a;

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/b;->a(Lcom/twitter/internal/android/service/m;)Lcom/twitter/internal/android/service/a;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    new-instance v0, Lcom/twitter/android/client/bd;

    invoke-virtual {p0}, Lcom/twitter/android/client/NotificationService;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/client/bd;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/android/client/NotificationService;->i:Lcom/twitter/android/client/bd;

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 11

    const/4 v2, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    sget-object v0, Lcom/twitter/android/client/NotificationService;->h:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_0

    invoke-virtual {p1, v9}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "PHOTO-818: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/crashlytics/android/d;->a(Ljava/lang/Throwable;)V

    invoke-virtual {p0, p3}, Lcom/twitter/android/client/NotificationService;->stopSelf(I)V

    move v0, v2

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/client/NotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    invoke-static {v3}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v5

    invoke-static {v3}, Lcom/twitter/android/client/aw;->a(Landroid/content/Context;)Lcom/twitter/android/client/aw;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_1
    invoke-virtual {p0, p3}, Lcom/twitter/android/client/NotificationService;->stopSelf(I)V

    move v0, v2

    goto :goto_0

    :pswitch_0
    invoke-static {v3, v4}, Lcom/twitter/android/client/NotificationService;->a(Landroid/content/Context;Landroid/os/Bundle;)V

    goto :goto_1

    :pswitch_1
    const-string/jumbo v0, "sb_notification"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/notifications/StatusBarNotif;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->k()I

    move-result v1

    iget-object v3, p0, Lcom/twitter/android/client/NotificationService;->i:Lcom/twitter/android/client/bd;

    invoke-virtual {v3, v1}, Lcom/twitter/android/client/bd;->hasMessages(I)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/twitter/android/client/NotificationService;->i:Lcom/twitter/android/client/bd;

    invoke-virtual {v3, v1}, Lcom/twitter/android/client/bd;->removeMessages(I)V

    const-string/jumbo v1, "notif_scribe_log"

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/scribe/ScribeLog;

    if-eqz v1, :cond_2

    new-array v3, v10, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/twitter/library/scribe/ScribeLog;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v7, "_undo"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v9

    invoke-virtual {v1, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {v5, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_2
    invoke-virtual {v6, v0}, Lcom/twitter/android/client/aw;->a(Lcom/twitter/android/client/notifications/StatusBarNotif;)V

    goto :goto_1

    :pswitch_2
    const-string/jumbo v0, "sb_notification"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/notifications/StatusBarNotif;

    if-eqz v0, :cond_4

    const-string/jumbo v1, "undo_allowed"

    invoke-virtual {v4, v1, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string/jumbo v1, "notif_scribe_log"

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/scribe/ScribeLog;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/twitter/library/scribe/ScribeLog;->a()Ljava/lang/String;

    move-result-object v2

    new-array v6, v10, [Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "_tap"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v1, v6}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {v5, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    new-array v5, v10, [Ljava/lang/String;

    aput-object v2, v5, v9

    invoke-virtual {v1, v5}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_3
    const-string/jumbo v2, "undo_text"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v5, "undo_icon"

    invoke-virtual {v4, v5, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    invoke-static {v3, v0, v2, v5, v1}, Lcom/twitter/android/client/NotificationService;->a(Landroid/content/Context;Lcom/twitter/android/client/notifications/StatusBarNotif;Ljava/lang/String;ILcom/twitter/library/scribe/ScribeLog;)V

    new-instance v1, Lcom/twitter/android/client/bc;

    invoke-direct {v1, p0, p3, v3, v4}, Lcom/twitter/android/client/bc;-><init>(Lcom/twitter/android/client/NotificationService;ILandroid/content/Context;Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/twitter/android/client/NotificationService;->i:Lcom/twitter/android/client/bd;

    iget-object v3, p0, Lcom/twitter/android/client/NotificationService;->i:Lcom/twitter/android/client/bd;

    invoke-virtual {v0}, Lcom/twitter/android/client/notifications/StatusBarNotif;->k()I

    move-result v0

    invoke-virtual {v3, v0, v1}, Lcom/twitter/android/client/bd;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v3, 0x1388

    invoke-virtual {v2, v0, v3, v4}, Lcom/twitter/android/client/bd;->sendMessageDelayed(Landroid/os/Message;J)Z

    const/4 v0, 0x3

    goto/16 :goto_0

    :cond_4
    invoke-static {v3, v4}, Lcom/twitter/android/client/NotificationService;->a(Landroid/content/Context;Landroid/os/Bundle;)V

    invoke-direct {p0, v3, v4}, Lcom/twitter/android/client/NotificationService;->b(Landroid/content/Context;Landroid/os/Bundle;)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
