.class public Lcom/twitter/android/client/AppBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "Twttr"


# static fields
.field private static final a:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x5

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v3}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/twitter/android/client/AppBroadcastReceiver;->a:Ljava/util/HashMap;

    sget-object v0, Lcom/twitter/android/client/AppBroadcastReceiver;->a:Ljava/util/HashMap;

    sget-object v1, Lcom/twitter/library/platform/TwitterDataSyncService;->a:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/client/AppBroadcastReceiver;->a:Ljava/util/HashMap;

    sget-object v1, Lcom/twitter/android/client/WidgetControl;->a:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/client/AppBroadcastReceiver;->a:Ljava/util/HashMap;

    sget-object v1, Lcom/twitter/android/client/WidgetControl;->b:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/client/AppBroadcastReceiver;->a:Ljava/util/HashMap;

    const-string/jumbo v1, "android.net.conn.CONNECTIVITY_CHANGE"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/client/AppBroadcastReceiver;->a:Ljava/util/HashMap;

    sget-object v1, Lcom/twitter/library/provider/w;->b:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    sget-object v0, Lcom/twitter/android/client/AppBroadcastReceiver;->a:Ljava/util/HashMap;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {p1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/android/client/c;->a(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_1
    invoke-static {p1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/android/client/c;->b(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_2
    invoke-static {p1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/android/client/c;->c(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_3
    invoke-static {}, Lcom/twitter/library/client/App;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p1}, Lcom/twitter/library/telephony/TelephonyUtil;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string/jumbo v1, "\""

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Dodo"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->n()Ljava/lang/String;

    :cond_2
    invoke-static {}, Lcom/twitter/library/telephony/TelephonyUtil;->d()Z

    move-result v0

    invoke-static {p1}, Lcom/twitter/library/telephony/TelephonyUtil;->d(Landroid/content/Context;)V

    if-nez v0, :cond_0

    invoke-static {}, Lcom/twitter/library/telephony/TelephonyUtil;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/twitter/android/xu;->a(Landroid/content/Context;)Lcom/twitter/android/xu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/xu;->a()V

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Z)V

    goto :goto_0

    :pswitch_4
    invoke-static {p1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    const-string/jumbo v1, "url"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->b([Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
