.class Lcom/twitter/android/lz;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/twitter/android/mb;

.field final synthetic c:Lcom/twitter/android/lx;


# direct methods
.method constructor <init>(Lcom/twitter/android/lx;Landroid/content/Context;Lcom/twitter/android/mb;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/lz;->c:Lcom/twitter/android/lx;

    iput-object p2, p0, Lcom/twitter/android/lz;->a:Landroid/content/Context;

    iput-object p3, p0, Lcom/twitter/android/lz;->b:Lcom/twitter/android/mb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/lz;->a:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/lz;->a:Landroid/content/Context;

    const-class v3, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "user_id"

    iget-object v3, p0, Lcom/twitter/android/lz;->b:Lcom/twitter/android/mb;

    iget-wide v3, v3, Lcom/twitter/android/mb;->b:J

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
